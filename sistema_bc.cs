/*
               File: Sistema_BC
        Description: Sistema
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:16:27.56
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class sistema_bc : GXHttpHandler, IGxSilentTrn
   {
      public sistema_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public sistema_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow0O25( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey0O25( ) ;
         standaloneModal( ) ;
         AddRow0O25( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E110O2 */
            E110O2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z127Sistema_Codigo = A127Sistema_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_0O0( )
      {
         BeforeValidate0O25( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0O25( ) ;
            }
            else
            {
               CheckExtendedTable0O25( ) ;
               if ( AnyError == 0 )
               {
                  ZM0O25( 21) ;
                  ZM0O25( 22) ;
                  ZM0O25( 23) ;
                  ZM0O25( 24) ;
                  ZM0O25( 25) ;
                  ZM0O25( 26) ;
                  ZM0O25( 27) ;
               }
               CloseExtendedTableCursors0O25( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            /* Save parent mode. */
            sMode25 = Gx_mode;
            CONFIRM_0O233( ) ;
            if ( AnyError == 0 )
            {
               /* Restore parent mode. */
               Gx_mode = sMode25;
               IsConfirmed = 1;
            }
            /* Restore parent mode. */
            Gx_mode = sMode25;
         }
      }

      protected void CONFIRM_0O233( )
      {
         s2130Sistema_Ultimo = O2130Sistema_Ultimo;
         nGXsfl_233_idx = 0;
         while ( nGXsfl_233_idx < bcSistema.gxTpr_Tecnologia.Count )
         {
            ReadRow0O233( ) ;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( Gx_mode)) )
            {
               if ( RcdFound233 == 0 )
               {
                  Gx_mode = "INS";
               }
               else
               {
                  Gx_mode = "UPD";
               }
            }
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) || ( nIsMod_233 != 0 ) )
            {
               GetKey0O233( ) ;
               if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 ) )
               {
                  if ( RcdFound233 == 0 )
                  {
                     Gx_mode = "INS";
                     BeforeValidate0O233( ) ;
                     if ( AnyError == 0 )
                     {
                        CheckExtendedTable0O233( ) ;
                        if ( AnyError == 0 )
                        {
                           ZM0O233( 29) ;
                        }
                        CloseExtendedTableCursors0O233( ) ;
                        if ( AnyError == 0 )
                        {
                           IsConfirmed = 1;
                        }
                        O2130Sistema_Ultimo = A2130Sistema_Ultimo;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                     AnyError = 1;
                  }
               }
               else
               {
                  if ( RcdFound233 != 0 )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                     {
                        Gx_mode = "DLT";
                        getByPrimaryKey0O233( ) ;
                        Load0O233( ) ;
                        BeforeValidate0O233( ) ;
                        if ( AnyError == 0 )
                        {
                           OnDeleteControls0O233( ) ;
                           O2130Sistema_Ultimo = A2130Sistema_Ultimo;
                        }
                     }
                     else
                     {
                        if ( nIsMod_233 != 0 )
                        {
                           Gx_mode = "UPD";
                           BeforeValidate0O233( ) ;
                           if ( AnyError == 0 )
                           {
                              CheckExtendedTable0O233( ) ;
                              if ( AnyError == 0 )
                              {
                                 ZM0O233( 29) ;
                              }
                              CloseExtendedTableCursors0O233( ) ;
                              if ( AnyError == 0 )
                              {
                                 IsConfirmed = 1;
                              }
                              O2130Sistema_Ultimo = A2130Sistema_Ultimo;
                           }
                        }
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
               VarsToRow233( ((SdtSistema_Tecnologia)bcSistema.gxTpr_Tecnologia.Item(nGXsfl_233_idx))) ;
            }
         }
         O2130Sistema_Ultimo = s2130Sistema_Ultimo;
         /* Start of After( level) rules */
         /* End of After( level) rules */
      }

      protected void E120O2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV60Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV61GXV1 = 1;
            while ( AV61GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV14TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV61GXV1));
               if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "Sistema_AreaTrabalhoCod") == 0 )
               {
                  AV11Insert_Sistema_AreaTrabalhoCod = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "AmbienteTecnologico_Codigo") == 0 )
               {
                  AV16Insert_AmbienteTecnologico_Codigo = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "Metodologia_Codigo") == 0 )
               {
                  AV13Insert_Metodologia_Codigo = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "Sistema_ImpUserCod") == 0 )
               {
                  AV40Insert_Sistema_ImpUserCod = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "Sistema_GpoObjCtrlCod") == 0 )
               {
                  AV58Insert_Sistema_GpoObjCtrlCod = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "SistemaVersao_Codigo") == 0 )
               {
                  AV56Insert_SistemaVersao_Codigo = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               AV61GXV1 = (int)(AV61GXV1+1);
            }
         }
      }

      protected void E110O2( )
      {
         /* After Trn Routine */
         new wwpbaseobjects.audittransaction(context ).execute(  AV54AuditingObject,  AV60Pgmname) ;
      }

      protected void E130O2( )
      {
         /* 'DoCopiarColar' Routine */
         context.wjLoc = formatLink("wp_copiarcolar.aspx") + "?" + UrlEncode("" +AV7Sistema_Codigo) + "," + UrlEncode(StringUtil.RTrim("Sis"));
         context.wjLocDisableFrm = 1;
      }

      protected void E140O2( )
      {
         /* 'DoImportarExcel' Routine */
      }

      protected void E150O2( )
      {
         /* 'DoImportar' Routine */
      }

      protected void ZM0O25( short GX_JID )
      {
         if ( ( GX_JID == 20 ) || ( GX_JID == 0 ) )
         {
            Z416Sistema_Nome = A416Sistema_Nome;
            Z129Sistema_Sigla = A129Sistema_Sigla;
            Z513Sistema_Coordenacao = A513Sistema_Coordenacao;
            Z699Sistema_Tipo = A699Sistema_Tipo;
            Z700Sistema_Tecnica = A700Sistema_Tecnica;
            Z686Sistema_FatorAjuste = A686Sistema_FatorAjuste;
            Z687Sistema_Custo = A687Sistema_Custo;
            Z688Sistema_Prazo = A688Sistema_Prazo;
            Z689Sistema_Esforco = A689Sistema_Esforco;
            Z1401Sistema_ImpData = A1401Sistema_ImpData;
            Z1831Sistema_Responsavel = A1831Sistema_Responsavel;
            Z130Sistema_Ativo = A130Sistema_Ativo;
            Z2109Sistema_Repositorio = A2109Sistema_Repositorio;
            Z2130Sistema_Ultimo = A2130Sistema_Ultimo;
            Z137Metodologia_Codigo = A137Metodologia_Codigo;
            Z351AmbienteTecnologico_Codigo = A351AmbienteTecnologico_Codigo;
            Z1859SistemaVersao_Codigo = A1859SistemaVersao_Codigo;
            Z135Sistema_AreaTrabalhoCod = A135Sistema_AreaTrabalhoCod;
            Z1399Sistema_ImpUserCod = A1399Sistema_ImpUserCod;
            Z2161Sistema_GpoObjCtrlCod = A2161Sistema_GpoObjCtrlCod;
            Z146Modulo_Codigo = A146Modulo_Codigo;
            Z395Sistema_PF = A395Sistema_PF;
            Z690Sistema_PFA = A690Sistema_PFA;
            Z707Sistema_TipoSigla = A707Sistema_TipoSigla;
            Z2162Sistema_GpoObjCtrlRsp = A2162Sistema_GpoObjCtrlRsp;
            Z2128SistemaTecnologiaLinha = A2128SistemaTecnologiaLinha;
            Z131Tecnologia_Codigo = A131Tecnologia_Codigo;
            Z132Tecnologia_Nome = A132Tecnologia_Nome;
            Z355Tecnologia_TipoTecnologia = A355Tecnologia_TipoTecnologia;
         }
         if ( ( GX_JID == 21 ) || ( GX_JID == 0 ) )
         {
            Z138Metodologia_Descricao = A138Metodologia_Descricao;
            Z146Modulo_Codigo = A146Modulo_Codigo;
            Z395Sistema_PF = A395Sistema_PF;
            Z690Sistema_PFA = A690Sistema_PFA;
            Z707Sistema_TipoSigla = A707Sistema_TipoSigla;
            Z2162Sistema_GpoObjCtrlRsp = A2162Sistema_GpoObjCtrlRsp;
            Z2128SistemaTecnologiaLinha = A2128SistemaTecnologiaLinha;
            Z131Tecnologia_Codigo = A131Tecnologia_Codigo;
            Z132Tecnologia_Nome = A132Tecnologia_Nome;
            Z355Tecnologia_TipoTecnologia = A355Tecnologia_TipoTecnologia;
         }
         if ( ( GX_JID == 22 ) || ( GX_JID == 0 ) )
         {
            Z352AmbienteTecnologico_Descricao = A352AmbienteTecnologico_Descricao;
            Z146Modulo_Codigo = A146Modulo_Codigo;
            Z395Sistema_PF = A395Sistema_PF;
            Z690Sistema_PFA = A690Sistema_PFA;
            Z707Sistema_TipoSigla = A707Sistema_TipoSigla;
            Z2162Sistema_GpoObjCtrlRsp = A2162Sistema_GpoObjCtrlRsp;
            Z2128SistemaTecnologiaLinha = A2128SistemaTecnologiaLinha;
            Z131Tecnologia_Codigo = A131Tecnologia_Codigo;
            Z132Tecnologia_Nome = A132Tecnologia_Nome;
            Z355Tecnologia_TipoTecnologia = A355Tecnologia_TipoTecnologia;
         }
         if ( ( GX_JID == 23 ) || ( GX_JID == 0 ) )
         {
            Z1860SistemaVersao_Id = A1860SistemaVersao_Id;
            Z146Modulo_Codigo = A146Modulo_Codigo;
            Z395Sistema_PF = A395Sistema_PF;
            Z690Sistema_PFA = A690Sistema_PFA;
            Z707Sistema_TipoSigla = A707Sistema_TipoSigla;
            Z2162Sistema_GpoObjCtrlRsp = A2162Sistema_GpoObjCtrlRsp;
            Z2128SistemaTecnologiaLinha = A2128SistemaTecnologiaLinha;
            Z131Tecnologia_Codigo = A131Tecnologia_Codigo;
            Z132Tecnologia_Nome = A132Tecnologia_Nome;
            Z355Tecnologia_TipoTecnologia = A355Tecnologia_TipoTecnologia;
         }
         if ( ( GX_JID == 24 ) || ( GX_JID == 0 ) )
         {
            Z136Sistema_AreaTrabalhoDes = A136Sistema_AreaTrabalhoDes;
            Z146Modulo_Codigo = A146Modulo_Codigo;
            Z395Sistema_PF = A395Sistema_PF;
            Z690Sistema_PFA = A690Sistema_PFA;
            Z707Sistema_TipoSigla = A707Sistema_TipoSigla;
            Z2162Sistema_GpoObjCtrlRsp = A2162Sistema_GpoObjCtrlRsp;
            Z2128SistemaTecnologiaLinha = A2128SistemaTecnologiaLinha;
            Z131Tecnologia_Codigo = A131Tecnologia_Codigo;
            Z132Tecnologia_Nome = A132Tecnologia_Nome;
            Z355Tecnologia_TipoTecnologia = A355Tecnologia_TipoTecnologia;
         }
         if ( ( GX_JID == 25 ) || ( GX_JID == 0 ) )
         {
            Z1402Sistema_ImpUserPesCod = A1402Sistema_ImpUserPesCod;
            Z146Modulo_Codigo = A146Modulo_Codigo;
            Z395Sistema_PF = A395Sistema_PF;
            Z690Sistema_PFA = A690Sistema_PFA;
            Z707Sistema_TipoSigla = A707Sistema_TipoSigla;
            Z2162Sistema_GpoObjCtrlRsp = A2162Sistema_GpoObjCtrlRsp;
            Z2128SistemaTecnologiaLinha = A2128SistemaTecnologiaLinha;
            Z131Tecnologia_Codigo = A131Tecnologia_Codigo;
            Z132Tecnologia_Nome = A132Tecnologia_Nome;
            Z355Tecnologia_TipoTecnologia = A355Tecnologia_TipoTecnologia;
         }
         if ( ( GX_JID == 26 ) || ( GX_JID == 0 ) )
         {
            Z146Modulo_Codigo = A146Modulo_Codigo;
            Z395Sistema_PF = A395Sistema_PF;
            Z690Sistema_PFA = A690Sistema_PFA;
            Z707Sistema_TipoSigla = A707Sistema_TipoSigla;
            Z2162Sistema_GpoObjCtrlRsp = A2162Sistema_GpoObjCtrlRsp;
            Z2128SistemaTecnologiaLinha = A2128SistemaTecnologiaLinha;
            Z131Tecnologia_Codigo = A131Tecnologia_Codigo;
            Z132Tecnologia_Nome = A132Tecnologia_Nome;
            Z355Tecnologia_TipoTecnologia = A355Tecnologia_TipoTecnologia;
         }
         if ( ( GX_JID == 27 ) || ( GX_JID == 0 ) )
         {
            Z1403Sistema_ImpUserPesNom = A1403Sistema_ImpUserPesNom;
            Z146Modulo_Codigo = A146Modulo_Codigo;
            Z395Sistema_PF = A395Sistema_PF;
            Z690Sistema_PFA = A690Sistema_PFA;
            Z707Sistema_TipoSigla = A707Sistema_TipoSigla;
            Z2162Sistema_GpoObjCtrlRsp = A2162Sistema_GpoObjCtrlRsp;
            Z2128SistemaTecnologiaLinha = A2128SistemaTecnologiaLinha;
            Z131Tecnologia_Codigo = A131Tecnologia_Codigo;
            Z132Tecnologia_Nome = A132Tecnologia_Nome;
            Z355Tecnologia_TipoTecnologia = A355Tecnologia_TipoTecnologia;
         }
         if ( GX_JID == -20 )
         {
            Z127Sistema_Codigo = A127Sistema_Codigo;
            Z416Sistema_Nome = A416Sistema_Nome;
            Z128Sistema_Descricao = A128Sistema_Descricao;
            Z129Sistema_Sigla = A129Sistema_Sigla;
            Z513Sistema_Coordenacao = A513Sistema_Coordenacao;
            Z699Sistema_Tipo = A699Sistema_Tipo;
            Z700Sistema_Tecnica = A700Sistema_Tecnica;
            Z686Sistema_FatorAjuste = A686Sistema_FatorAjuste;
            Z687Sistema_Custo = A687Sistema_Custo;
            Z688Sistema_Prazo = A688Sistema_Prazo;
            Z689Sistema_Esforco = A689Sistema_Esforco;
            Z1401Sistema_ImpData = A1401Sistema_ImpData;
            Z1831Sistema_Responsavel = A1831Sistema_Responsavel;
            Z130Sistema_Ativo = A130Sistema_Ativo;
            Z2109Sistema_Repositorio = A2109Sistema_Repositorio;
            Z2130Sistema_Ultimo = A2130Sistema_Ultimo;
            Z137Metodologia_Codigo = A137Metodologia_Codigo;
            Z351AmbienteTecnologico_Codigo = A351AmbienteTecnologico_Codigo;
            Z1859SistemaVersao_Codigo = A1859SistemaVersao_Codigo;
            Z135Sistema_AreaTrabalhoCod = A135Sistema_AreaTrabalhoCod;
            Z1399Sistema_ImpUserCod = A1399Sistema_ImpUserCod;
            Z2161Sistema_GpoObjCtrlCod = A2161Sistema_GpoObjCtrlCod;
            Z136Sistema_AreaTrabalhoDes = A136Sistema_AreaTrabalhoDes;
            Z352AmbienteTecnologico_Descricao = A352AmbienteTecnologico_Descricao;
            Z138Metodologia_Descricao = A138Metodologia_Descricao;
            Z1402Sistema_ImpUserPesCod = A1402Sistema_ImpUserPesCod;
            Z1403Sistema_ImpUserPesNom = A1403Sistema_ImpUserPesNom;
            Z1860SistemaVersao_Id = A1860SistemaVersao_Id;
         }
      }

      protected void standaloneNotModal( )
      {
         AV60Pgmname = "Sistema_BC";
         Gx_BScreen = 0;
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A130Sistema_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A130Sistema_Ativo = true;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A699Sistema_Tipo)) && ( Gx_BScreen == 0 ) )
         {
            A699Sistema_Tipo = "A";
            n699Sistema_Tipo = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A135Sistema_AreaTrabalhoCod) && ( Gx_BScreen == 0 ) )
         {
            A135Sistema_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Convert.ToDecimal(0)==A686Sistema_FatorAjuste) && ( Gx_BScreen == 0 ) )
         {
            GXt_decimal1 = A686Sistema_FatorAjuste;
            new prc_fapadrao(context ).execute( out  GXt_decimal1) ;
            A686Sistema_FatorAjuste = GXt_decimal1;
            n686Sistema_FatorAjuste = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            if ( StringUtil.StrCmp(A699Sistema_Tipo, "D") == 0 )
            {
               A707Sistema_TipoSigla = "PD";
            }
            else
            {
               if ( StringUtil.StrCmp(A699Sistema_Tipo, "M") == 0 )
               {
                  A707Sistema_TipoSigla = "PM";
               }
               else
               {
                  if ( StringUtil.StrCmp(A699Sistema_Tipo, "C") == 0 )
                  {
                     A707Sistema_TipoSigla = "PC";
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(A699Sistema_Tipo, "A") == 0 )
                     {
                        A707Sistema_TipoSigla = "A";
                     }
                     else
                     {
                        A707Sistema_TipoSigla = "";
                     }
                  }
               }
            }
            /* Using cursor BC000O10 */
            pr_default.execute(8, new Object[] {A135Sistema_AreaTrabalhoCod});
            A136Sistema_AreaTrabalhoDes = BC000O10_A136Sistema_AreaTrabalhoDes[0];
            n136Sistema_AreaTrabalhoDes = BC000O10_n136Sistema_AreaTrabalhoDes[0];
            pr_default.close(8);
         }
      }

      protected void Load0O25( )
      {
         /* Using cursor BC000O14 */
         pr_default.execute(12, new Object[] {A127Sistema_Codigo});
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound25 = 1;
            A416Sistema_Nome = BC000O14_A416Sistema_Nome[0];
            A128Sistema_Descricao = BC000O14_A128Sistema_Descricao[0];
            n128Sistema_Descricao = BC000O14_n128Sistema_Descricao[0];
            A129Sistema_Sigla = BC000O14_A129Sistema_Sigla[0];
            A513Sistema_Coordenacao = BC000O14_A513Sistema_Coordenacao[0];
            n513Sistema_Coordenacao = BC000O14_n513Sistema_Coordenacao[0];
            A136Sistema_AreaTrabalhoDes = BC000O14_A136Sistema_AreaTrabalhoDes[0];
            n136Sistema_AreaTrabalhoDes = BC000O14_n136Sistema_AreaTrabalhoDes[0];
            A352AmbienteTecnologico_Descricao = BC000O14_A352AmbienteTecnologico_Descricao[0];
            A138Metodologia_Descricao = BC000O14_A138Metodologia_Descricao[0];
            A699Sistema_Tipo = BC000O14_A699Sistema_Tipo[0];
            n699Sistema_Tipo = BC000O14_n699Sistema_Tipo[0];
            A700Sistema_Tecnica = BC000O14_A700Sistema_Tecnica[0];
            n700Sistema_Tecnica = BC000O14_n700Sistema_Tecnica[0];
            A686Sistema_FatorAjuste = BC000O14_A686Sistema_FatorAjuste[0];
            n686Sistema_FatorAjuste = BC000O14_n686Sistema_FatorAjuste[0];
            A687Sistema_Custo = BC000O14_A687Sistema_Custo[0];
            n687Sistema_Custo = BC000O14_n687Sistema_Custo[0];
            A688Sistema_Prazo = BC000O14_A688Sistema_Prazo[0];
            n688Sistema_Prazo = BC000O14_n688Sistema_Prazo[0];
            A689Sistema_Esforco = BC000O14_A689Sistema_Esforco[0];
            n689Sistema_Esforco = BC000O14_n689Sistema_Esforco[0];
            A1401Sistema_ImpData = BC000O14_A1401Sistema_ImpData[0];
            n1401Sistema_ImpData = BC000O14_n1401Sistema_ImpData[0];
            A1403Sistema_ImpUserPesNom = BC000O14_A1403Sistema_ImpUserPesNom[0];
            n1403Sistema_ImpUserPesNom = BC000O14_n1403Sistema_ImpUserPesNom[0];
            A1831Sistema_Responsavel = BC000O14_A1831Sistema_Responsavel[0];
            n1831Sistema_Responsavel = BC000O14_n1831Sistema_Responsavel[0];
            A1860SistemaVersao_Id = BC000O14_A1860SistemaVersao_Id[0];
            A130Sistema_Ativo = BC000O14_A130Sistema_Ativo[0];
            A2109Sistema_Repositorio = BC000O14_A2109Sistema_Repositorio[0];
            n2109Sistema_Repositorio = BC000O14_n2109Sistema_Repositorio[0];
            A2130Sistema_Ultimo = BC000O14_A2130Sistema_Ultimo[0];
            A137Metodologia_Codigo = BC000O14_A137Metodologia_Codigo[0];
            n137Metodologia_Codigo = BC000O14_n137Metodologia_Codigo[0];
            A351AmbienteTecnologico_Codigo = BC000O14_A351AmbienteTecnologico_Codigo[0];
            n351AmbienteTecnologico_Codigo = BC000O14_n351AmbienteTecnologico_Codigo[0];
            A1859SistemaVersao_Codigo = BC000O14_A1859SistemaVersao_Codigo[0];
            n1859SistemaVersao_Codigo = BC000O14_n1859SistemaVersao_Codigo[0];
            A135Sistema_AreaTrabalhoCod = BC000O14_A135Sistema_AreaTrabalhoCod[0];
            A1399Sistema_ImpUserCod = BC000O14_A1399Sistema_ImpUserCod[0];
            n1399Sistema_ImpUserCod = BC000O14_n1399Sistema_ImpUserCod[0];
            A2161Sistema_GpoObjCtrlCod = BC000O14_A2161Sistema_GpoObjCtrlCod[0];
            n2161Sistema_GpoObjCtrlCod = BC000O14_n2161Sistema_GpoObjCtrlCod[0];
            A1402Sistema_ImpUserPesCod = BC000O14_A1402Sistema_ImpUserPesCod[0];
            n1402Sistema_ImpUserPesCod = BC000O14_n1402Sistema_ImpUserPesCod[0];
            ZM0O25( -20) ;
         }
         pr_default.close(12);
         OnLoadActions0O25( ) ;
      }

      protected void OnLoadActions0O25( )
      {
         GXt_decimal1 = A395Sistema_PF;
         new prc_sistema_pf(context ).execute( ref  A127Sistema_Codigo, ref  GXt_decimal1) ;
         A395Sistema_PF = GXt_decimal1;
         A690Sistema_PFA = (decimal)(A395Sistema_PF*A686Sistema_FatorAjuste);
         if ( StringUtil.StrCmp(A699Sistema_Tipo, "D") == 0 )
         {
            A707Sistema_TipoSigla = "PD";
         }
         else
         {
            if ( StringUtil.StrCmp(A699Sistema_Tipo, "M") == 0 )
            {
               A707Sistema_TipoSigla = "PM";
            }
            else
            {
               if ( StringUtil.StrCmp(A699Sistema_Tipo, "C") == 0 )
               {
                  A707Sistema_TipoSigla = "PC";
               }
               else
               {
                  if ( StringUtil.StrCmp(A699Sistema_Tipo, "A") == 0 )
                  {
                     A707Sistema_TipoSigla = "A";
                  }
                  else
                  {
                     A707Sistema_TipoSigla = "";
                  }
               }
            }
         }
         GXt_int2 = A2162Sistema_GpoObjCtrlRsp;
         GXt_int3 = (short)(A2161Sistema_GpoObjCtrlCod);
         new prc_getgpoobjctrl_responsavel(context ).execute( ref  GXt_int3, out  GXt_int2) ;
         A2161Sistema_GpoObjCtrlCod = GXt_int3;
         A2162Sistema_GpoObjCtrlRsp = GXt_int2;
      }

      protected void CheckExtendedTable0O25( )
      {
         standaloneModal( ) ;
         GXt_decimal1 = A395Sistema_PF;
         new prc_sistema_pf(context ).execute( ref  A127Sistema_Codigo, ref  GXt_decimal1) ;
         A395Sistema_PF = GXt_decimal1;
         A690Sistema_PFA = (decimal)(A395Sistema_PF*A686Sistema_FatorAjuste);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A416Sistema_Nome)) )
         {
            GX_msglist.addItem("Nome � obrigat�rio.", 1, "");
            AnyError = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A129Sistema_Sigla)) )
         {
            GX_msglist.addItem("Sigla � obrigat�rio.", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC000O10 */
         pr_default.execute(8, new Object[] {A135Sistema_AreaTrabalhoCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Sistema_Area Trabalho'.", "ForeignKeyNotFound", 1, "SISTEMA_AREATRABALHOCOD");
            AnyError = 1;
         }
         A136Sistema_AreaTrabalhoDes = BC000O10_A136Sistema_AreaTrabalhoDes[0];
         n136Sistema_AreaTrabalhoDes = BC000O10_n136Sistema_AreaTrabalhoDes[0];
         pr_default.close(8);
         /* Using cursor BC000O8 */
         pr_default.execute(6, new Object[] {n351AmbienteTecnologico_Codigo, A351AmbienteTecnologico_Codigo});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( (0==A351AmbienteTecnologico_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe ' T62'.", "ForeignKeyNotFound", 1, "AMBIENTETECNOLOGICO_CODIGO");
               AnyError = 1;
            }
         }
         A352AmbienteTecnologico_Descricao = BC000O8_A352AmbienteTecnologico_Descricao[0];
         pr_default.close(6);
         /* Using cursor BC000O7 */
         pr_default.execute(5, new Object[] {n137Metodologia_Codigo, A137Metodologia_Codigo});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A137Metodologia_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Metodologia'.", "ForeignKeyNotFound", 1, "METODOLOGIA_CODIGO");
               AnyError = 1;
            }
         }
         A138Metodologia_Descricao = BC000O7_A138Metodologia_Descricao[0];
         pr_default.close(5);
         if ( StringUtil.StrCmp(A699Sistema_Tipo, "D") == 0 )
         {
            A707Sistema_TipoSigla = "PD";
         }
         else
         {
            if ( StringUtil.StrCmp(A699Sistema_Tipo, "M") == 0 )
            {
               A707Sistema_TipoSigla = "PM";
            }
            else
            {
               if ( StringUtil.StrCmp(A699Sistema_Tipo, "C") == 0 )
               {
                  A707Sistema_TipoSigla = "PC";
               }
               else
               {
                  if ( StringUtil.StrCmp(A699Sistema_Tipo, "A") == 0 )
                  {
                     A707Sistema_TipoSigla = "A";
                  }
                  else
                  {
                     A707Sistema_TipoSigla = "";
                  }
               }
            }
         }
         if ( ! ( (DateTime.MinValue==A1401Sistema_ImpData) || ( A1401Sistema_ImpData >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Importado em fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC000O11 */
         pr_default.execute(9, new Object[] {n1399Sistema_ImpUserCod, A1399Sistema_ImpUserCod});
         if ( (pr_default.getStatus(9) == 101) )
         {
            if ( ! ( (0==A1399Sistema_ImpUserCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Sistema_Usuario da Importa��o'.", "ForeignKeyNotFound", 1, "SISTEMA_IMPUSERCOD");
               AnyError = 1;
            }
         }
         A1402Sistema_ImpUserPesCod = BC000O11_A1402Sistema_ImpUserPesCod[0];
         n1402Sistema_ImpUserPesCod = BC000O11_n1402Sistema_ImpUserPesCod[0];
         pr_default.close(9);
         /* Using cursor BC000O13 */
         pr_default.execute(11, new Object[] {n1402Sistema_ImpUserPesCod, A1402Sistema_ImpUserPesCod});
         if ( (pr_default.getStatus(11) == 101) )
         {
            if ( ! ( (0==A1402Sistema_ImpUserPesCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1403Sistema_ImpUserPesNom = BC000O13_A1403Sistema_ImpUserPesNom[0];
         n1403Sistema_ImpUserPesNom = BC000O13_n1403Sistema_ImpUserPesNom[0];
         pr_default.close(11);
         /* Using cursor BC000O12 */
         pr_default.execute(10, new Object[] {n2161Sistema_GpoObjCtrlCod, A2161Sistema_GpoObjCtrlCod});
         if ( (pr_default.getStatus(10) == 101) )
         {
            if ( ! ( (0==A2161Sistema_GpoObjCtrlCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Sistema_GpoObjCtrl'.", "ForeignKeyNotFound", 1, "SISTEMA_GPOOBJCTRLCOD");
               AnyError = 1;
            }
         }
         pr_default.close(10);
         GXt_int2 = A2162Sistema_GpoObjCtrlRsp;
         GXt_int3 = (short)(A2161Sistema_GpoObjCtrlCod);
         new prc_getgpoobjctrl_responsavel(context ).execute( ref  GXt_int3, out  GXt_int2) ;
         A2161Sistema_GpoObjCtrlCod = GXt_int3;
         A2162Sistema_GpoObjCtrlRsp = GXt_int2;
         /* Using cursor BC000O9 */
         pr_default.execute(7, new Object[] {n1859SistemaVersao_Codigo, A1859SistemaVersao_Codigo});
         if ( (pr_default.getStatus(7) == 101) )
         {
            if ( ! ( (0==A1859SistemaVersao_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe ' T207'.", "ForeignKeyNotFound", 1, "SISTEMAVERSAO_CODIGO");
               AnyError = 1;
            }
         }
         A1860SistemaVersao_Id = BC000O9_A1860SistemaVersao_Id[0];
         pr_default.close(7);
      }

      protected void CloseExtendedTableCursors0O25( )
      {
         pr_default.close(8);
         pr_default.close(6);
         pr_default.close(5);
         pr_default.close(9);
         pr_default.close(11);
         pr_default.close(10);
         pr_default.close(7);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey0O25( )
      {
         /* Using cursor BC000O15 */
         pr_default.execute(13, new Object[] {A127Sistema_Codigo});
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound25 = 1;
         }
         else
         {
            RcdFound25 = 0;
         }
         pr_default.close(13);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC000O6 */
         pr_default.execute(4, new Object[] {A127Sistema_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            ZM0O25( 20) ;
            RcdFound25 = 1;
            A127Sistema_Codigo = BC000O6_A127Sistema_Codigo[0];
            A416Sistema_Nome = BC000O6_A416Sistema_Nome[0];
            A128Sistema_Descricao = BC000O6_A128Sistema_Descricao[0];
            n128Sistema_Descricao = BC000O6_n128Sistema_Descricao[0];
            A129Sistema_Sigla = BC000O6_A129Sistema_Sigla[0];
            A513Sistema_Coordenacao = BC000O6_A513Sistema_Coordenacao[0];
            n513Sistema_Coordenacao = BC000O6_n513Sistema_Coordenacao[0];
            A699Sistema_Tipo = BC000O6_A699Sistema_Tipo[0];
            n699Sistema_Tipo = BC000O6_n699Sistema_Tipo[0];
            A700Sistema_Tecnica = BC000O6_A700Sistema_Tecnica[0];
            n700Sistema_Tecnica = BC000O6_n700Sistema_Tecnica[0];
            A686Sistema_FatorAjuste = BC000O6_A686Sistema_FatorAjuste[0];
            n686Sistema_FatorAjuste = BC000O6_n686Sistema_FatorAjuste[0];
            A687Sistema_Custo = BC000O6_A687Sistema_Custo[0];
            n687Sistema_Custo = BC000O6_n687Sistema_Custo[0];
            A688Sistema_Prazo = BC000O6_A688Sistema_Prazo[0];
            n688Sistema_Prazo = BC000O6_n688Sistema_Prazo[0];
            A689Sistema_Esforco = BC000O6_A689Sistema_Esforco[0];
            n689Sistema_Esforco = BC000O6_n689Sistema_Esforco[0];
            A1401Sistema_ImpData = BC000O6_A1401Sistema_ImpData[0];
            n1401Sistema_ImpData = BC000O6_n1401Sistema_ImpData[0];
            A1831Sistema_Responsavel = BC000O6_A1831Sistema_Responsavel[0];
            n1831Sistema_Responsavel = BC000O6_n1831Sistema_Responsavel[0];
            A130Sistema_Ativo = BC000O6_A130Sistema_Ativo[0];
            A2109Sistema_Repositorio = BC000O6_A2109Sistema_Repositorio[0];
            n2109Sistema_Repositorio = BC000O6_n2109Sistema_Repositorio[0];
            A2130Sistema_Ultimo = BC000O6_A2130Sistema_Ultimo[0];
            A137Metodologia_Codigo = BC000O6_A137Metodologia_Codigo[0];
            n137Metodologia_Codigo = BC000O6_n137Metodologia_Codigo[0];
            A351AmbienteTecnologico_Codigo = BC000O6_A351AmbienteTecnologico_Codigo[0];
            n351AmbienteTecnologico_Codigo = BC000O6_n351AmbienteTecnologico_Codigo[0];
            A1859SistemaVersao_Codigo = BC000O6_A1859SistemaVersao_Codigo[0];
            n1859SistemaVersao_Codigo = BC000O6_n1859SistemaVersao_Codigo[0];
            A135Sistema_AreaTrabalhoCod = BC000O6_A135Sistema_AreaTrabalhoCod[0];
            A1399Sistema_ImpUserCod = BC000O6_A1399Sistema_ImpUserCod[0];
            n1399Sistema_ImpUserCod = BC000O6_n1399Sistema_ImpUserCod[0];
            A2161Sistema_GpoObjCtrlCod = BC000O6_A2161Sistema_GpoObjCtrlCod[0];
            n2161Sistema_GpoObjCtrlCod = BC000O6_n2161Sistema_GpoObjCtrlCod[0];
            O2130Sistema_Ultimo = A2130Sistema_Ultimo;
            Z127Sistema_Codigo = A127Sistema_Codigo;
            sMode25 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load0O25( ) ;
            if ( AnyError == 1 )
            {
               RcdFound25 = 0;
               InitializeNonKey0O25( ) ;
            }
            Gx_mode = sMode25;
         }
         else
         {
            RcdFound25 = 0;
            InitializeNonKey0O25( ) ;
            sMode25 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode25;
         }
         pr_default.close(4);
      }

      protected void getEqualNoModal( )
      {
         GetKey0O25( ) ;
         if ( RcdFound25 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_0O0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency0O25( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC000O5 */
            pr_default.execute(3, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(3) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Sistema"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(3) == 101) || ( StringUtil.StrCmp(Z416Sistema_Nome, BC000O5_A416Sistema_Nome[0]) != 0 ) || ( StringUtil.StrCmp(Z129Sistema_Sigla, BC000O5_A129Sistema_Sigla[0]) != 0 ) || ( StringUtil.StrCmp(Z513Sistema_Coordenacao, BC000O5_A513Sistema_Coordenacao[0]) != 0 ) || ( StringUtil.StrCmp(Z699Sistema_Tipo, BC000O5_A699Sistema_Tipo[0]) != 0 ) || ( StringUtil.StrCmp(Z700Sistema_Tecnica, BC000O5_A700Sistema_Tecnica[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z686Sistema_FatorAjuste != BC000O5_A686Sistema_FatorAjuste[0] ) || ( Z687Sistema_Custo != BC000O5_A687Sistema_Custo[0] ) || ( Z688Sistema_Prazo != BC000O5_A688Sistema_Prazo[0] ) || ( Z689Sistema_Esforco != BC000O5_A689Sistema_Esforco[0] ) || ( Z1401Sistema_ImpData != BC000O5_A1401Sistema_ImpData[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1831Sistema_Responsavel != BC000O5_A1831Sistema_Responsavel[0] ) || ( Z130Sistema_Ativo != BC000O5_A130Sistema_Ativo[0] ) || ( StringUtil.StrCmp(Z2109Sistema_Repositorio, BC000O5_A2109Sistema_Repositorio[0]) != 0 ) || ( Z2130Sistema_Ultimo != BC000O5_A2130Sistema_Ultimo[0] ) || ( Z137Metodologia_Codigo != BC000O5_A137Metodologia_Codigo[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z351AmbienteTecnologico_Codigo != BC000O5_A351AmbienteTecnologico_Codigo[0] ) || ( Z1859SistemaVersao_Codigo != BC000O5_A1859SistemaVersao_Codigo[0] ) || ( Z135Sistema_AreaTrabalhoCod != BC000O5_A135Sistema_AreaTrabalhoCod[0] ) || ( Z1399Sistema_ImpUserCod != BC000O5_A1399Sistema_ImpUserCod[0] ) || ( Z2161Sistema_GpoObjCtrlCod != BC000O5_A2161Sistema_GpoObjCtrlCod[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Sistema"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0O25( )
      {
         BeforeValidate0O25( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0O25( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0O25( 0) ;
            CheckOptimisticConcurrency0O25( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0O25( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0O25( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000O16 */
                     pr_default.execute(14, new Object[] {A416Sistema_Nome, n128Sistema_Descricao, A128Sistema_Descricao, A129Sistema_Sigla, n513Sistema_Coordenacao, A513Sistema_Coordenacao, n699Sistema_Tipo, A699Sistema_Tipo, n700Sistema_Tecnica, A700Sistema_Tecnica, n686Sistema_FatorAjuste, A686Sistema_FatorAjuste, n687Sistema_Custo, A687Sistema_Custo, n688Sistema_Prazo, A688Sistema_Prazo, n689Sistema_Esforco, A689Sistema_Esforco, n1401Sistema_ImpData, A1401Sistema_ImpData, n1831Sistema_Responsavel, A1831Sistema_Responsavel, A130Sistema_Ativo, n2109Sistema_Repositorio, A2109Sistema_Repositorio, A2130Sistema_Ultimo, n137Metodologia_Codigo, A137Metodologia_Codigo, n351AmbienteTecnologico_Codigo, A351AmbienteTecnologico_Codigo, n1859SistemaVersao_Codigo, A1859SistemaVersao_Codigo, A135Sistema_AreaTrabalhoCod, n1399Sistema_ImpUserCod, A1399Sistema_ImpUserCod, n2161Sistema_GpoObjCtrlCod, A2161Sistema_GpoObjCtrlCod});
                     A127Sistema_Codigo = BC000O16_A127Sistema_Codigo[0];
                     pr_default.close(14);
                     dsDefault.SmartCacheProvider.SetUpdated("Sistema") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel0O25( ) ;
                           if ( AnyError == 0 )
                           {
                              /* Save values for previous() function. */
                              GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0O25( ) ;
            }
            EndLevel0O25( ) ;
         }
         CloseExtendedTableCursors0O25( ) ;
      }

      protected void Update0O25( )
      {
         BeforeValidate0O25( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0O25( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0O25( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0O25( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0O25( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000O17 */
                     pr_default.execute(15, new Object[] {A416Sistema_Nome, n128Sistema_Descricao, A128Sistema_Descricao, A129Sistema_Sigla, n513Sistema_Coordenacao, A513Sistema_Coordenacao, n699Sistema_Tipo, A699Sistema_Tipo, n700Sistema_Tecnica, A700Sistema_Tecnica, n686Sistema_FatorAjuste, A686Sistema_FatorAjuste, n687Sistema_Custo, A687Sistema_Custo, n688Sistema_Prazo, A688Sistema_Prazo, n689Sistema_Esforco, A689Sistema_Esforco, n1401Sistema_ImpData, A1401Sistema_ImpData, n1831Sistema_Responsavel, A1831Sistema_Responsavel, A130Sistema_Ativo, n2109Sistema_Repositorio, A2109Sistema_Repositorio, A2130Sistema_Ultimo, n137Metodologia_Codigo, A137Metodologia_Codigo, n351AmbienteTecnologico_Codigo, A351AmbienteTecnologico_Codigo, n1859SistemaVersao_Codigo, A1859SistemaVersao_Codigo, A135Sistema_AreaTrabalhoCod, n1399Sistema_ImpUserCod, A1399Sistema_ImpUserCod, n2161Sistema_GpoObjCtrlCod, A2161Sistema_GpoObjCtrlCod, A127Sistema_Codigo});
                     pr_default.close(15);
                     dsDefault.SmartCacheProvider.SetUpdated("Sistema") ;
                     if ( (pr_default.getStatus(15) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Sistema"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0O25( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel0O25( ) ;
                           if ( AnyError == 0 )
                           {
                              getByPrimaryKey( ) ;
                              GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0O25( ) ;
         }
         CloseExtendedTableCursors0O25( ) ;
      }

      protected void DeferredUpdate0O25( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate0O25( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0O25( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0O25( ) ;
            AfterConfirm0O25( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0O25( ) ;
               if ( AnyError == 0 )
               {
                  A2130Sistema_Ultimo = O2130Sistema_Ultimo;
                  ScanKeyStart0O233( ) ;
                  while ( RcdFound233 != 0 )
                  {
                     getByPrimaryKey0O233( ) ;
                     Delete0O233( ) ;
                     ScanKeyNext0O233( ) ;
                     O2130Sistema_Ultimo = A2130Sistema_Ultimo;
                  }
                  ScanKeyEnd0O233( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000O18 */
                     pr_default.execute(16, new Object[] {A127Sistema_Codigo});
                     pr_default.close(16);
                     dsDefault.SmartCacheProvider.SetUpdated("Sistema") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( delete) rules */
                        /* End of After( delete) rules */
                        if ( AnyError == 0 )
                        {
                           GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
         sMode25 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel0O25( ) ;
         Gx_mode = sMode25;
      }

      protected void OnDeleteControls0O25( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            GXt_decimal1 = A395Sistema_PF;
            new prc_sistema_pf(context ).execute( ref  A127Sistema_Codigo, ref  GXt_decimal1) ;
            A395Sistema_PF = GXt_decimal1;
            /* Using cursor BC000O19 */
            pr_default.execute(17, new Object[] {A135Sistema_AreaTrabalhoCod});
            A136Sistema_AreaTrabalhoDes = BC000O19_A136Sistema_AreaTrabalhoDes[0];
            n136Sistema_AreaTrabalhoDes = BC000O19_n136Sistema_AreaTrabalhoDes[0];
            pr_default.close(17);
            /* Using cursor BC000O20 */
            pr_default.execute(18, new Object[] {n351AmbienteTecnologico_Codigo, A351AmbienteTecnologico_Codigo});
            A352AmbienteTecnologico_Descricao = BC000O20_A352AmbienteTecnologico_Descricao[0];
            pr_default.close(18);
            /* Using cursor BC000O21 */
            pr_default.execute(19, new Object[] {n137Metodologia_Codigo, A137Metodologia_Codigo});
            A138Metodologia_Descricao = BC000O21_A138Metodologia_Descricao[0];
            pr_default.close(19);
            if ( StringUtil.StrCmp(A699Sistema_Tipo, "D") == 0 )
            {
               A707Sistema_TipoSigla = "PD";
            }
            else
            {
               if ( StringUtil.StrCmp(A699Sistema_Tipo, "M") == 0 )
               {
                  A707Sistema_TipoSigla = "PM";
               }
               else
               {
                  if ( StringUtil.StrCmp(A699Sistema_Tipo, "C") == 0 )
                  {
                     A707Sistema_TipoSigla = "PC";
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(A699Sistema_Tipo, "A") == 0 )
                     {
                        A707Sistema_TipoSigla = "A";
                     }
                     else
                     {
                        A707Sistema_TipoSigla = "";
                     }
                  }
               }
            }
            A690Sistema_PFA = (decimal)(A395Sistema_PF*A686Sistema_FatorAjuste);
            /* Using cursor BC000O22 */
            pr_default.execute(20, new Object[] {n1399Sistema_ImpUserCod, A1399Sistema_ImpUserCod});
            A1402Sistema_ImpUserPesCod = BC000O22_A1402Sistema_ImpUserPesCod[0];
            n1402Sistema_ImpUserPesCod = BC000O22_n1402Sistema_ImpUserPesCod[0];
            pr_default.close(20);
            /* Using cursor BC000O23 */
            pr_default.execute(21, new Object[] {n1402Sistema_ImpUserPesCod, A1402Sistema_ImpUserPesCod});
            A1403Sistema_ImpUserPesNom = BC000O23_A1403Sistema_ImpUserPesNom[0];
            n1403Sistema_ImpUserPesNom = BC000O23_n1403Sistema_ImpUserPesNom[0];
            pr_default.close(21);
            GXt_int2 = A2162Sistema_GpoObjCtrlRsp;
            GXt_int3 = (short)(A2161Sistema_GpoObjCtrlCod);
            new prc_getgpoobjctrl_responsavel(context ).execute( ref  GXt_int3, out  GXt_int2) ;
            A2161Sistema_GpoObjCtrlCod = GXt_int3;
            A2162Sistema_GpoObjCtrlRsp = GXt_int2;
            /* Using cursor BC000O24 */
            pr_default.execute(22, new Object[] {n1859SistemaVersao_Codigo, A1859SistemaVersao_Codigo});
            A1860SistemaVersao_Id = BC000O24_A1860SistemaVersao_Id[0];
            pr_default.close(22);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor BC000O25 */
            pr_default.execute(23, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(23) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Projeto"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(23);
            /* Using cursor BC000O26 */
            pr_default.execute(24, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(24) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {" T207"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(24);
            /* Using cursor BC000O27 */
            pr_default.execute(25, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(25) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Sistemas atendidos pelo Contrato"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(25);
            /* Using cursor BC000O28 */
            pr_default.execute(26, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(26) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Sistemas do Servi�o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(26);
            /* Using cursor BC000O29 */
            pr_default.execute(27, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(27) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Solicita��o de Mudan�a"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(27);
            /* Using cursor BC000O30 */
            pr_default.execute(28, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(28) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(28);
            /* Using cursor BC000O31 */
            pr_default.execute(29, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(29) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Projeto Melhoria"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(29);
            /* Using cursor BC000O32 */
            pr_default.execute(30, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(30) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Resultado das Contagens"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(30);
            /* Using cursor BC000O33 */
            pr_default.execute(31, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(31) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Grupo L�gico de Dados"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(31);
            /* Using cursor BC000O34 */
            pr_default.execute(32, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(32) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Fun��es APF - An�lise de Ponto de Fun��o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(32);
            /* Using cursor BC000O35 */
            pr_default.execute(33, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(33) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Tabela"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(33);
            /* Using cursor BC000O36 */
            pr_default.execute(34, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(34) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Solicitacoes"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(34);
            /* Using cursor BC000O37 */
            pr_default.execute(35, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(35) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Modulo Funcoes"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(35);
            /* Using cursor BC000O38 */
            pr_default.execute(36, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(36) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Modulo"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(36);
            /* Using cursor BC000O39 */
            pr_default.execute(37, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(37) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Sistemas do Usuario"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(37);
            /* Using cursor BC000O40 */
            pr_default.execute(38, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(38) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"De Para"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(38);
         }
      }

      protected void ProcessNestedLevel0O233( )
      {
         s2130Sistema_Ultimo = O2130Sistema_Ultimo;
         nGXsfl_233_idx = 0;
         while ( nGXsfl_233_idx < bcSistema.gxTpr_Tecnologia.Count )
         {
            ReadRow0O233( ) ;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( Gx_mode)) )
            {
               if ( RcdFound233 == 0 )
               {
                  Gx_mode = "INS";
               }
               else
               {
                  Gx_mode = "UPD";
               }
            }
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) || ( nIsMod_233 != 0 ) )
            {
               standaloneNotModal0O233( ) ;
               if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
               {
                  Gx_mode = "INS";
                  Insert0O233( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                  {
                     Gx_mode = "DLT";
                     Delete0O233( ) ;
                  }
                  else
                  {
                     Gx_mode = "UPD";
                     Update0O233( ) ;
                  }
               }
               O2130Sistema_Ultimo = A2130Sistema_Ultimo;
            }
            KeyVarsToRow233( ((SdtSistema_Tecnologia)bcSistema.gxTpr_Tecnologia.Item(nGXsfl_233_idx))) ;
         }
         if ( AnyError == 0 )
         {
            /* Batch update SDT rows */
            nGXsfl_233_idx = 0;
            while ( nGXsfl_233_idx < bcSistema.gxTpr_Tecnologia.Count )
            {
               ReadRow0O233( ) ;
               if ( String.IsNullOrEmpty(StringUtil.RTrim( Gx_mode)) )
               {
                  if ( RcdFound233 == 0 )
                  {
                     Gx_mode = "INS";
                  }
                  else
                  {
                     Gx_mode = "UPD";
                  }
               }
               /* Update SDT row */
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  bcSistema.gxTpr_Tecnologia.RemoveElement(nGXsfl_233_idx);
                  nGXsfl_233_idx = (short)(nGXsfl_233_idx-1);
               }
               else
               {
                  Gx_mode = "UPD";
                  getByPrimaryKey0O233( ) ;
                  VarsToRow233( ((SdtSistema_Tecnologia)bcSistema.gxTpr_Tecnologia.Item(nGXsfl_233_idx))) ;
               }
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
         InitAll0O233( ) ;
         if ( AnyError != 0 )
         {
            O2130Sistema_Ultimo = s2130Sistema_Ultimo;
         }
         nRcdExists_233 = 0;
         nIsMod_233 = 0;
         Gxremove233 = 0;
      }

      protected void ProcessLevel0O25( )
      {
         /* Save parent mode. */
         sMode25 = Gx_mode;
         ProcessNestedLevel0O233( ) ;
         if ( AnyError != 0 )
         {
            O2130Sistema_Ultimo = s2130Sistema_Ultimo;
         }
         /* Restore parent mode. */
         Gx_mode = sMode25;
         /* ' Update level parameters */
         /* Using cursor BC000O41 */
         pr_default.execute(39, new Object[] {A2130Sistema_Ultimo, A127Sistema_Codigo});
         pr_default.close(39);
         dsDefault.SmartCacheProvider.SetUpdated("Sistema") ;
      }

      protected void EndLevel0O25( )
      {
         pr_default.close(3);
         if ( AnyError == 0 )
         {
            BeforeComplete0O25( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart0O25( )
      {
         /* Scan By routine */
         /* Using cursor BC000O42 */
         pr_default.execute(40, new Object[] {A127Sistema_Codigo});
         RcdFound25 = 0;
         if ( (pr_default.getStatus(40) != 101) )
         {
            RcdFound25 = 1;
            A127Sistema_Codigo = BC000O42_A127Sistema_Codigo[0];
            A416Sistema_Nome = BC000O42_A416Sistema_Nome[0];
            A128Sistema_Descricao = BC000O42_A128Sistema_Descricao[0];
            n128Sistema_Descricao = BC000O42_n128Sistema_Descricao[0];
            A129Sistema_Sigla = BC000O42_A129Sistema_Sigla[0];
            A513Sistema_Coordenacao = BC000O42_A513Sistema_Coordenacao[0];
            n513Sistema_Coordenacao = BC000O42_n513Sistema_Coordenacao[0];
            A136Sistema_AreaTrabalhoDes = BC000O42_A136Sistema_AreaTrabalhoDes[0];
            n136Sistema_AreaTrabalhoDes = BC000O42_n136Sistema_AreaTrabalhoDes[0];
            A352AmbienteTecnologico_Descricao = BC000O42_A352AmbienteTecnologico_Descricao[0];
            A138Metodologia_Descricao = BC000O42_A138Metodologia_Descricao[0];
            A699Sistema_Tipo = BC000O42_A699Sistema_Tipo[0];
            n699Sistema_Tipo = BC000O42_n699Sistema_Tipo[0];
            A700Sistema_Tecnica = BC000O42_A700Sistema_Tecnica[0];
            n700Sistema_Tecnica = BC000O42_n700Sistema_Tecnica[0];
            A686Sistema_FatorAjuste = BC000O42_A686Sistema_FatorAjuste[0];
            n686Sistema_FatorAjuste = BC000O42_n686Sistema_FatorAjuste[0];
            A687Sistema_Custo = BC000O42_A687Sistema_Custo[0];
            n687Sistema_Custo = BC000O42_n687Sistema_Custo[0];
            A688Sistema_Prazo = BC000O42_A688Sistema_Prazo[0];
            n688Sistema_Prazo = BC000O42_n688Sistema_Prazo[0];
            A689Sistema_Esforco = BC000O42_A689Sistema_Esforco[0];
            n689Sistema_Esforco = BC000O42_n689Sistema_Esforco[0];
            A1401Sistema_ImpData = BC000O42_A1401Sistema_ImpData[0];
            n1401Sistema_ImpData = BC000O42_n1401Sistema_ImpData[0];
            A1403Sistema_ImpUserPesNom = BC000O42_A1403Sistema_ImpUserPesNom[0];
            n1403Sistema_ImpUserPesNom = BC000O42_n1403Sistema_ImpUserPesNom[0];
            A1831Sistema_Responsavel = BC000O42_A1831Sistema_Responsavel[0];
            n1831Sistema_Responsavel = BC000O42_n1831Sistema_Responsavel[0];
            A1860SistemaVersao_Id = BC000O42_A1860SistemaVersao_Id[0];
            A130Sistema_Ativo = BC000O42_A130Sistema_Ativo[0];
            A2109Sistema_Repositorio = BC000O42_A2109Sistema_Repositorio[0];
            n2109Sistema_Repositorio = BC000O42_n2109Sistema_Repositorio[0];
            A2130Sistema_Ultimo = BC000O42_A2130Sistema_Ultimo[0];
            A137Metodologia_Codigo = BC000O42_A137Metodologia_Codigo[0];
            n137Metodologia_Codigo = BC000O42_n137Metodologia_Codigo[0];
            A351AmbienteTecnologico_Codigo = BC000O42_A351AmbienteTecnologico_Codigo[0];
            n351AmbienteTecnologico_Codigo = BC000O42_n351AmbienteTecnologico_Codigo[0];
            A1859SistemaVersao_Codigo = BC000O42_A1859SistemaVersao_Codigo[0];
            n1859SistemaVersao_Codigo = BC000O42_n1859SistemaVersao_Codigo[0];
            A135Sistema_AreaTrabalhoCod = BC000O42_A135Sistema_AreaTrabalhoCod[0];
            A1399Sistema_ImpUserCod = BC000O42_A1399Sistema_ImpUserCod[0];
            n1399Sistema_ImpUserCod = BC000O42_n1399Sistema_ImpUserCod[0];
            A2161Sistema_GpoObjCtrlCod = BC000O42_A2161Sistema_GpoObjCtrlCod[0];
            n2161Sistema_GpoObjCtrlCod = BC000O42_n2161Sistema_GpoObjCtrlCod[0];
            A1402Sistema_ImpUserPesCod = BC000O42_A1402Sistema_ImpUserPesCod[0];
            n1402Sistema_ImpUserPesCod = BC000O42_n1402Sistema_ImpUserPesCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext0O25( )
      {
         /* Scan next routine */
         pr_default.readNext(40);
         RcdFound25 = 0;
         ScanKeyLoad0O25( ) ;
      }

      protected void ScanKeyLoad0O25( )
      {
         sMode25 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(40) != 101) )
         {
            RcdFound25 = 1;
            A127Sistema_Codigo = BC000O42_A127Sistema_Codigo[0];
            A416Sistema_Nome = BC000O42_A416Sistema_Nome[0];
            A128Sistema_Descricao = BC000O42_A128Sistema_Descricao[0];
            n128Sistema_Descricao = BC000O42_n128Sistema_Descricao[0];
            A129Sistema_Sigla = BC000O42_A129Sistema_Sigla[0];
            A513Sistema_Coordenacao = BC000O42_A513Sistema_Coordenacao[0];
            n513Sistema_Coordenacao = BC000O42_n513Sistema_Coordenacao[0];
            A136Sistema_AreaTrabalhoDes = BC000O42_A136Sistema_AreaTrabalhoDes[0];
            n136Sistema_AreaTrabalhoDes = BC000O42_n136Sistema_AreaTrabalhoDes[0];
            A352AmbienteTecnologico_Descricao = BC000O42_A352AmbienteTecnologico_Descricao[0];
            A138Metodologia_Descricao = BC000O42_A138Metodologia_Descricao[0];
            A699Sistema_Tipo = BC000O42_A699Sistema_Tipo[0];
            n699Sistema_Tipo = BC000O42_n699Sistema_Tipo[0];
            A700Sistema_Tecnica = BC000O42_A700Sistema_Tecnica[0];
            n700Sistema_Tecnica = BC000O42_n700Sistema_Tecnica[0];
            A686Sistema_FatorAjuste = BC000O42_A686Sistema_FatorAjuste[0];
            n686Sistema_FatorAjuste = BC000O42_n686Sistema_FatorAjuste[0];
            A687Sistema_Custo = BC000O42_A687Sistema_Custo[0];
            n687Sistema_Custo = BC000O42_n687Sistema_Custo[0];
            A688Sistema_Prazo = BC000O42_A688Sistema_Prazo[0];
            n688Sistema_Prazo = BC000O42_n688Sistema_Prazo[0];
            A689Sistema_Esforco = BC000O42_A689Sistema_Esforco[0];
            n689Sistema_Esforco = BC000O42_n689Sistema_Esforco[0];
            A1401Sistema_ImpData = BC000O42_A1401Sistema_ImpData[0];
            n1401Sistema_ImpData = BC000O42_n1401Sistema_ImpData[0];
            A1403Sistema_ImpUserPesNom = BC000O42_A1403Sistema_ImpUserPesNom[0];
            n1403Sistema_ImpUserPesNom = BC000O42_n1403Sistema_ImpUserPesNom[0];
            A1831Sistema_Responsavel = BC000O42_A1831Sistema_Responsavel[0];
            n1831Sistema_Responsavel = BC000O42_n1831Sistema_Responsavel[0];
            A1860SistemaVersao_Id = BC000O42_A1860SistemaVersao_Id[0];
            A130Sistema_Ativo = BC000O42_A130Sistema_Ativo[0];
            A2109Sistema_Repositorio = BC000O42_A2109Sistema_Repositorio[0];
            n2109Sistema_Repositorio = BC000O42_n2109Sistema_Repositorio[0];
            A2130Sistema_Ultimo = BC000O42_A2130Sistema_Ultimo[0];
            A137Metodologia_Codigo = BC000O42_A137Metodologia_Codigo[0];
            n137Metodologia_Codigo = BC000O42_n137Metodologia_Codigo[0];
            A351AmbienteTecnologico_Codigo = BC000O42_A351AmbienteTecnologico_Codigo[0];
            n351AmbienteTecnologico_Codigo = BC000O42_n351AmbienteTecnologico_Codigo[0];
            A1859SistemaVersao_Codigo = BC000O42_A1859SistemaVersao_Codigo[0];
            n1859SistemaVersao_Codigo = BC000O42_n1859SistemaVersao_Codigo[0];
            A135Sistema_AreaTrabalhoCod = BC000O42_A135Sistema_AreaTrabalhoCod[0];
            A1399Sistema_ImpUserCod = BC000O42_A1399Sistema_ImpUserCod[0];
            n1399Sistema_ImpUserCod = BC000O42_n1399Sistema_ImpUserCod[0];
            A2161Sistema_GpoObjCtrlCod = BC000O42_A2161Sistema_GpoObjCtrlCod[0];
            n2161Sistema_GpoObjCtrlCod = BC000O42_n2161Sistema_GpoObjCtrlCod[0];
            A1402Sistema_ImpUserPesCod = BC000O42_A1402Sistema_ImpUserPesCod[0];
            n1402Sistema_ImpUserPesCod = BC000O42_n1402Sistema_ImpUserPesCod[0];
         }
         Gx_mode = sMode25;
      }

      protected void ScanKeyEnd0O25( )
      {
         pr_default.close(40);
      }

      protected void AfterConfirm0O25( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0O25( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0O25( )
      {
         /* Before Update Rules */
         new loadauditsistema(context ).execute(  "Y", ref  AV54AuditingObject,  A127Sistema_Codigo,  Gx_mode) ;
      }

      protected void BeforeDelete0O25( )
      {
         /* Before Delete Rules */
         new loadauditsistema(context ).execute(  "Y", ref  AV54AuditingObject,  A127Sistema_Codigo,  Gx_mode) ;
      }

      protected void BeforeComplete0O25( )
      {
         /* Before Complete Rules */
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditsistema(context ).execute(  "N", ref  AV54AuditingObject,  A127Sistema_Codigo,  Gx_mode) ;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditsistema(context ).execute(  "N", ref  AV54AuditingObject,  A127Sistema_Codigo,  Gx_mode) ;
         }
      }

      protected void BeforeValidate0O25( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0O25( )
      {
      }

      protected void ZM0O233( short GX_JID )
      {
         if ( ( GX_JID == 28 ) || ( GX_JID == 0 ) )
         {
            Z131Tecnologia_Codigo = A131Tecnologia_Codigo;
            Z146Modulo_Codigo = A146Modulo_Codigo;
            Z416Sistema_Nome = A416Sistema_Nome;
            Z416Sistema_Nome = A416Sistema_Nome;
            Z129Sistema_Sigla = A129Sistema_Sigla;
            Z513Sistema_Coordenacao = A513Sistema_Coordenacao;
            Z135Sistema_AreaTrabalhoCod = A135Sistema_AreaTrabalhoCod;
            Z136Sistema_AreaTrabalhoDes = A136Sistema_AreaTrabalhoDes;
            Z351AmbienteTecnologico_Codigo = A351AmbienteTecnologico_Codigo;
            Z352AmbienteTecnologico_Descricao = A352AmbienteTecnologico_Descricao;
            Z137Metodologia_Codigo = A137Metodologia_Codigo;
            Z138Metodologia_Descricao = A138Metodologia_Descricao;
            Z395Sistema_PF = A395Sistema_PF;
            Z690Sistema_PFA = A690Sistema_PFA;
            Z699Sistema_Tipo = A699Sistema_Tipo;
            Z707Sistema_TipoSigla = A707Sistema_TipoSigla;
            Z700Sistema_Tecnica = A700Sistema_Tecnica;
            Z686Sistema_FatorAjuste = A686Sistema_FatorAjuste;
            Z687Sistema_Custo = A687Sistema_Custo;
            Z688Sistema_Prazo = A688Sistema_Prazo;
            Z689Sistema_Esforco = A689Sistema_Esforco;
            Z1401Sistema_ImpData = A1401Sistema_ImpData;
            Z1399Sistema_ImpUserCod = A1399Sistema_ImpUserCod;
            Z1402Sistema_ImpUserPesCod = A1402Sistema_ImpUserPesCod;
            Z1403Sistema_ImpUserPesNom = A1403Sistema_ImpUserPesNom;
            Z1831Sistema_Responsavel = A1831Sistema_Responsavel;
            Z2161Sistema_GpoObjCtrlCod = A2161Sistema_GpoObjCtrlCod;
            Z2162Sistema_GpoObjCtrlRsp = A2162Sistema_GpoObjCtrlRsp;
            Z1859SistemaVersao_Codigo = A1859SistemaVersao_Codigo;
            Z1860SistemaVersao_Id = A1860SistemaVersao_Id;
            Z130Sistema_Ativo = A130Sistema_Ativo;
            Z2109Sistema_Repositorio = A2109Sistema_Repositorio;
            Z2130Sistema_Ultimo = A2130Sistema_Ultimo;
         }
         if ( ( GX_JID == 29 ) || ( GX_JID == 0 ) )
         {
            Z132Tecnologia_Nome = A132Tecnologia_Nome;
            Z355Tecnologia_TipoTecnologia = A355Tecnologia_TipoTecnologia;
            Z146Modulo_Codigo = A146Modulo_Codigo;
            Z416Sistema_Nome = A416Sistema_Nome;
            Z416Sistema_Nome = A416Sistema_Nome;
            Z129Sistema_Sigla = A129Sistema_Sigla;
            Z513Sistema_Coordenacao = A513Sistema_Coordenacao;
            Z135Sistema_AreaTrabalhoCod = A135Sistema_AreaTrabalhoCod;
            Z136Sistema_AreaTrabalhoDes = A136Sistema_AreaTrabalhoDes;
            Z351AmbienteTecnologico_Codigo = A351AmbienteTecnologico_Codigo;
            Z352AmbienteTecnologico_Descricao = A352AmbienteTecnologico_Descricao;
            Z137Metodologia_Codigo = A137Metodologia_Codigo;
            Z138Metodologia_Descricao = A138Metodologia_Descricao;
            Z395Sistema_PF = A395Sistema_PF;
            Z690Sistema_PFA = A690Sistema_PFA;
            Z699Sistema_Tipo = A699Sistema_Tipo;
            Z707Sistema_TipoSigla = A707Sistema_TipoSigla;
            Z700Sistema_Tecnica = A700Sistema_Tecnica;
            Z686Sistema_FatorAjuste = A686Sistema_FatorAjuste;
            Z687Sistema_Custo = A687Sistema_Custo;
            Z688Sistema_Prazo = A688Sistema_Prazo;
            Z689Sistema_Esforco = A689Sistema_Esforco;
            Z1401Sistema_ImpData = A1401Sistema_ImpData;
            Z1399Sistema_ImpUserCod = A1399Sistema_ImpUserCod;
            Z1402Sistema_ImpUserPesCod = A1402Sistema_ImpUserPesCod;
            Z1403Sistema_ImpUserPesNom = A1403Sistema_ImpUserPesNom;
            Z1831Sistema_Responsavel = A1831Sistema_Responsavel;
            Z2161Sistema_GpoObjCtrlCod = A2161Sistema_GpoObjCtrlCod;
            Z2162Sistema_GpoObjCtrlRsp = A2162Sistema_GpoObjCtrlRsp;
            Z1859SistemaVersao_Codigo = A1859SistemaVersao_Codigo;
            Z1860SistemaVersao_Id = A1860SistemaVersao_Id;
            Z130Sistema_Ativo = A130Sistema_Ativo;
            Z2109Sistema_Repositorio = A2109Sistema_Repositorio;
            Z2130Sistema_Ultimo = A2130Sistema_Ultimo;
         }
         if ( GX_JID == -28 )
         {
            Z127Sistema_Codigo = A127Sistema_Codigo;
            Z2128SistemaTecnologiaLinha = A2128SistemaTecnologiaLinha;
            Z131Tecnologia_Codigo = A131Tecnologia_Codigo;
            Z132Tecnologia_Nome = A132Tecnologia_Nome;
            Z355Tecnologia_TipoTecnologia = A355Tecnologia_TipoTecnologia;
         }
      }

      protected void standaloneNotModal0O233( )
      {
      }

      protected void standaloneModal0O233( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A2130Sistema_Ultimo = (short)(O2130Sistema_Ultimo+1);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A2128SistemaTecnologiaLinha = A2130Sistema_Ultimo;
         }
      }

      protected void Load0O233( )
      {
         /* Using cursor BC000O43 */
         pr_default.execute(41, new Object[] {A127Sistema_Codigo, A2128SistemaTecnologiaLinha});
         if ( (pr_default.getStatus(41) != 101) )
         {
            RcdFound233 = 1;
            A132Tecnologia_Nome = BC000O43_A132Tecnologia_Nome[0];
            A355Tecnologia_TipoTecnologia = BC000O43_A355Tecnologia_TipoTecnologia[0];
            n355Tecnologia_TipoTecnologia = BC000O43_n355Tecnologia_TipoTecnologia[0];
            A131Tecnologia_Codigo = BC000O43_A131Tecnologia_Codigo[0];
            ZM0O233( -28) ;
         }
         pr_default.close(41);
         OnLoadActions0O233( ) ;
      }

      protected void OnLoadActions0O233( )
      {
      }

      protected void CheckExtendedTable0O233( )
      {
         Gx_BScreen = 1;
         standaloneModal0O233( ) ;
         Gx_BScreen = 0;
         /* Using cursor BC000O4 */
         pr_default.execute(2, new Object[] {A131Tecnologia_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Tecnologia'.", "ForeignKeyNotFound", 1, "TECNOLOGIA_CODIGO");
            AnyError = 1;
         }
         A132Tecnologia_Nome = BC000O4_A132Tecnologia_Nome[0];
         A355Tecnologia_TipoTecnologia = BC000O4_A355Tecnologia_TipoTecnologia[0];
         n355Tecnologia_TipoTecnologia = BC000O4_n355Tecnologia_TipoTecnologia[0];
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors0O233( )
      {
         pr_default.close(2);
      }

      protected void enableDisable0O233( )
      {
      }

      protected void GetKey0O233( )
      {
         /* Using cursor BC000O44 */
         pr_default.execute(42, new Object[] {A127Sistema_Codigo, A2128SistemaTecnologiaLinha});
         if ( (pr_default.getStatus(42) != 101) )
         {
            RcdFound233 = 1;
         }
         else
         {
            RcdFound233 = 0;
         }
         pr_default.close(42);
      }

      protected void getByPrimaryKey0O233( )
      {
         /* Using cursor BC000O3 */
         pr_default.execute(1, new Object[] {A127Sistema_Codigo, A2128SistemaTecnologiaLinha});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0O233( 28) ;
            RcdFound233 = 1;
            InitializeNonKey0O233( ) ;
            A2128SistemaTecnologiaLinha = BC000O3_A2128SistemaTecnologiaLinha[0];
            A131Tecnologia_Codigo = BC000O3_A131Tecnologia_Codigo[0];
            Z127Sistema_Codigo = A127Sistema_Codigo;
            Z2128SistemaTecnologiaLinha = A2128SistemaTecnologiaLinha;
            sMode233 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal0O233( ) ;
            Load0O233( ) ;
            Gx_mode = sMode233;
         }
         else
         {
            RcdFound233 = 0;
            InitializeNonKey0O233( ) ;
            sMode233 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal0O233( ) ;
            Gx_mode = sMode233;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            DisableAttributes0O233( ) ;
         }
         pr_default.close(1);
      }

      protected void CheckOptimisticConcurrency0O233( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC000O2 */
            pr_default.execute(0, new Object[] {A127Sistema_Codigo, A2128SistemaTecnologiaLinha});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"SistemaTecnologia"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z131Tecnologia_Codigo != BC000O2_A131Tecnologia_Codigo[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"SistemaTecnologia"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0O233( )
      {
         BeforeValidate0O233( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0O233( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0O233( 0) ;
            CheckOptimisticConcurrency0O233( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0O233( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0O233( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000O45 */
                     pr_default.execute(43, new Object[] {A127Sistema_Codigo, A2128SistemaTecnologiaLinha, A131Tecnologia_Codigo});
                     pr_default.close(43);
                     dsDefault.SmartCacheProvider.SetUpdated("SistemaTecnologia") ;
                     if ( (pr_default.getStatus(43) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0O233( ) ;
            }
            EndLevel0O233( ) ;
         }
         CloseExtendedTableCursors0O233( ) ;
      }

      protected void Update0O233( )
      {
         BeforeValidate0O233( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0O233( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0O233( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0O233( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0O233( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000O46 */
                     pr_default.execute(44, new Object[] {A131Tecnologia_Codigo, A127Sistema_Codigo, A2128SistemaTecnologiaLinha});
                     pr_default.close(44);
                     dsDefault.SmartCacheProvider.SetUpdated("SistemaTecnologia") ;
                     if ( (pr_default.getStatus(44) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"SistemaTecnologia"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0O233( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey0O233( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0O233( ) ;
         }
         CloseExtendedTableCursors0O233( ) ;
      }

      protected void DeferredUpdate0O233( )
      {
      }

      protected void Delete0O233( )
      {
         Gx_mode = "DLT";
         BeforeValidate0O233( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0O233( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0O233( ) ;
            AfterConfirm0O233( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0O233( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC000O47 */
                  pr_default.execute(45, new Object[] {A127Sistema_Codigo, A2128SistemaTecnologiaLinha});
                  pr_default.close(45);
                  dsDefault.SmartCacheProvider.SetUpdated("SistemaTecnologia") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode233 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel0O233( ) ;
         Gx_mode = sMode233;
      }

      protected void OnDeleteControls0O233( )
      {
         standaloneModal0O233( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC000O48 */
            pr_default.execute(46, new Object[] {A131Tecnologia_Codigo});
            A132Tecnologia_Nome = BC000O48_A132Tecnologia_Nome[0];
            A355Tecnologia_TipoTecnologia = BC000O48_A355Tecnologia_TipoTecnologia[0];
            n355Tecnologia_TipoTecnologia = BC000O48_n355Tecnologia_TipoTecnologia[0];
            pr_default.close(46);
         }
      }

      protected void EndLevel0O233( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart0O233( )
      {
         /* Scan By routine */
         /* Using cursor BC000O49 */
         pr_default.execute(47, new Object[] {A127Sistema_Codigo});
         RcdFound233 = 0;
         if ( (pr_default.getStatus(47) != 101) )
         {
            RcdFound233 = 1;
            A2128SistemaTecnologiaLinha = BC000O49_A2128SistemaTecnologiaLinha[0];
            A132Tecnologia_Nome = BC000O49_A132Tecnologia_Nome[0];
            A355Tecnologia_TipoTecnologia = BC000O49_A355Tecnologia_TipoTecnologia[0];
            n355Tecnologia_TipoTecnologia = BC000O49_n355Tecnologia_TipoTecnologia[0];
            A131Tecnologia_Codigo = BC000O49_A131Tecnologia_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext0O233( )
      {
         /* Scan next routine */
         pr_default.readNext(47);
         RcdFound233 = 0;
         ScanKeyLoad0O233( ) ;
      }

      protected void ScanKeyLoad0O233( )
      {
         sMode233 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(47) != 101) )
         {
            RcdFound233 = 1;
            A2128SistemaTecnologiaLinha = BC000O49_A2128SistemaTecnologiaLinha[0];
            A132Tecnologia_Nome = BC000O49_A132Tecnologia_Nome[0];
            A355Tecnologia_TipoTecnologia = BC000O49_A355Tecnologia_TipoTecnologia[0];
            n355Tecnologia_TipoTecnologia = BC000O49_n355Tecnologia_TipoTecnologia[0];
            A131Tecnologia_Codigo = BC000O49_A131Tecnologia_Codigo[0];
         }
         Gx_mode = sMode233;
      }

      protected void ScanKeyEnd0O233( )
      {
         pr_default.close(47);
      }

      protected void AfterConfirm0O233( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0O233( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0O233( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0O233( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0O233( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0O233( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0O233( )
      {
      }

      protected void AddRow0O25( )
      {
         VarsToRow25( bcSistema) ;
      }

      protected void ReadRow0O25( )
      {
         RowToVars25( bcSistema, 1) ;
      }

      protected void AddRow0O233( )
      {
         SdtSistema_Tecnologia obj233 ;
         obj233 = new SdtSistema_Tecnologia(context);
         VarsToRow233( obj233) ;
         bcSistema.gxTpr_Tecnologia.Add(obj233, 0);
         obj233.gxTpr_Mode = "UPD";
         obj233.gxTpr_Modified = 0;
      }

      protected void ReadRow0O233( )
      {
         nGXsfl_233_idx = (short)(nGXsfl_233_idx+1);
         RowToVars233( ((SdtSistema_Tecnologia)bcSistema.gxTpr_Tecnologia.Item(nGXsfl_233_idx)), 1) ;
      }

      protected void InitializeNonKey0O25( )
      {
         AV54AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         A707Sistema_TipoSigla = "";
         A690Sistema_PFA = 0;
         A395Sistema_PF = 0;
         A416Sistema_Nome = "";
         A128Sistema_Descricao = "";
         n128Sistema_Descricao = false;
         A129Sistema_Sigla = "";
         A513Sistema_Coordenacao = "";
         n513Sistema_Coordenacao = false;
         A136Sistema_AreaTrabalhoDes = "";
         n136Sistema_AreaTrabalhoDes = false;
         A351AmbienteTecnologico_Codigo = 0;
         n351AmbienteTecnologico_Codigo = false;
         A352AmbienteTecnologico_Descricao = "";
         A137Metodologia_Codigo = 0;
         n137Metodologia_Codigo = false;
         A138Metodologia_Descricao = "";
         A700Sistema_Tecnica = "";
         n700Sistema_Tecnica = false;
         A687Sistema_Custo = 0;
         n687Sistema_Custo = false;
         A688Sistema_Prazo = 0;
         n688Sistema_Prazo = false;
         A689Sistema_Esforco = 0;
         n689Sistema_Esforco = false;
         A1401Sistema_ImpData = (DateTime)(DateTime.MinValue);
         n1401Sistema_ImpData = false;
         A1399Sistema_ImpUserCod = 0;
         n1399Sistema_ImpUserCod = false;
         A1402Sistema_ImpUserPesCod = 0;
         n1402Sistema_ImpUserPesCod = false;
         A1403Sistema_ImpUserPesNom = "";
         n1403Sistema_ImpUserPesNom = false;
         A1831Sistema_Responsavel = 0;
         n1831Sistema_Responsavel = false;
         A2161Sistema_GpoObjCtrlCod = 0;
         n2161Sistema_GpoObjCtrlCod = false;
         A2162Sistema_GpoObjCtrlRsp = 0;
         A1859SistemaVersao_Codigo = 0;
         n1859SistemaVersao_Codigo = false;
         A1860SistemaVersao_Id = "";
         A2109Sistema_Repositorio = "";
         n2109Sistema_Repositorio = false;
         A2130Sistema_Ultimo = 0;
         A135Sistema_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         A699Sistema_Tipo = "A";
         n699Sistema_Tipo = false;
         A686Sistema_FatorAjuste = new prc_fapadrao(context).executeUdp( );
         n686Sistema_FatorAjuste = false;
         A130Sistema_Ativo = true;
         O2130Sistema_Ultimo = A2130Sistema_Ultimo;
         Z416Sistema_Nome = "";
         Z129Sistema_Sigla = "";
         Z513Sistema_Coordenacao = "";
         Z699Sistema_Tipo = "";
         Z700Sistema_Tecnica = "";
         Z686Sistema_FatorAjuste = 0;
         Z687Sistema_Custo = 0;
         Z688Sistema_Prazo = 0;
         Z689Sistema_Esforco = 0;
         Z1401Sistema_ImpData = (DateTime)(DateTime.MinValue);
         Z1831Sistema_Responsavel = 0;
         Z130Sistema_Ativo = false;
         Z2109Sistema_Repositorio = "";
         Z2130Sistema_Ultimo = 0;
         Z137Metodologia_Codigo = 0;
         Z351AmbienteTecnologico_Codigo = 0;
         Z1859SistemaVersao_Codigo = 0;
         Z135Sistema_AreaTrabalhoCod = 0;
         Z1399Sistema_ImpUserCod = 0;
         Z2161Sistema_GpoObjCtrlCod = 0;
      }

      protected void InitAll0O25( )
      {
         A127Sistema_Codigo = 0;
         InitializeNonKey0O25( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A130Sistema_Ativo = i130Sistema_Ativo;
         A699Sistema_Tipo = i699Sistema_Tipo;
         n699Sistema_Tipo = false;
         A135Sistema_AreaTrabalhoCod = i135Sistema_AreaTrabalhoCod;
         A686Sistema_FatorAjuste = i686Sistema_FatorAjuste;
         n686Sistema_FatorAjuste = false;
      }

      protected void InitializeNonKey0O233( )
      {
         A131Tecnologia_Codigo = 0;
         A132Tecnologia_Nome = "";
         A355Tecnologia_TipoTecnologia = "";
         n355Tecnologia_TipoTecnologia = false;
         Z131Tecnologia_Codigo = 0;
      }

      protected void InitAll0O233( )
      {
         A2128SistemaTecnologiaLinha = 0;
         InitializeNonKey0O233( ) ;
      }

      protected void StandaloneModalInsert0O233( )
      {
         A2130Sistema_Ultimo = i2130Sistema_Ultimo;
      }

      public void VarsToRow25( SdtSistema obj25 )
      {
         obj25.gxTpr_Mode = Gx_mode;
         obj25.gxTpr_Sistema_tiposigla = A707Sistema_TipoSigla;
         obj25.gxTpr_Sistema_pfa = A690Sistema_PFA;
         obj25.gxTpr_Sistema_pf = A395Sistema_PF;
         obj25.gxTpr_Sistema_nome = A416Sistema_Nome;
         obj25.gxTpr_Sistema_descricao = A128Sistema_Descricao;
         obj25.gxTpr_Sistema_sigla = A129Sistema_Sigla;
         obj25.gxTpr_Sistema_coordenacao = A513Sistema_Coordenacao;
         obj25.gxTpr_Sistema_areatrabalhodes = A136Sistema_AreaTrabalhoDes;
         obj25.gxTpr_Ambientetecnologico_codigo = A351AmbienteTecnologico_Codigo;
         obj25.gxTpr_Ambientetecnologico_descricao = A352AmbienteTecnologico_Descricao;
         obj25.gxTpr_Metodologia_codigo = A137Metodologia_Codigo;
         obj25.gxTpr_Metodologia_descricao = A138Metodologia_Descricao;
         obj25.gxTpr_Sistema_tecnica = A700Sistema_Tecnica;
         obj25.gxTpr_Sistema_custo = A687Sistema_Custo;
         obj25.gxTpr_Sistema_prazo = A688Sistema_Prazo;
         obj25.gxTpr_Sistema_esforco = A689Sistema_Esforco;
         obj25.gxTpr_Sistema_impdata = A1401Sistema_ImpData;
         obj25.gxTpr_Sistema_impusercod = A1399Sistema_ImpUserCod;
         obj25.gxTpr_Sistema_impuserpescod = A1402Sistema_ImpUserPesCod;
         obj25.gxTpr_Sistema_impuserpesnom = A1403Sistema_ImpUserPesNom;
         obj25.gxTpr_Sistema_responsavel = A1831Sistema_Responsavel;
         obj25.gxTpr_Sistema_gpoobjctrlcod = A2161Sistema_GpoObjCtrlCod;
         obj25.gxTpr_Sistema_gpoobjctrlrsp = A2162Sistema_GpoObjCtrlRsp;
         obj25.gxTpr_Sistemaversao_codigo = A1859SistemaVersao_Codigo;
         obj25.gxTpr_Sistemaversao_id = A1860SistemaVersao_Id;
         obj25.gxTpr_Sistema_repositorio = A2109Sistema_Repositorio;
         obj25.gxTpr_Sistema_ultimo = A2130Sistema_Ultimo;
         obj25.gxTpr_Sistema_areatrabalhocod = A135Sistema_AreaTrabalhoCod;
         obj25.gxTpr_Sistema_tipo = A699Sistema_Tipo;
         obj25.gxTpr_Sistema_fatorajuste = A686Sistema_FatorAjuste;
         obj25.gxTpr_Sistema_ativo = A130Sistema_Ativo;
         obj25.gxTpr_Sistema_codigo = A127Sistema_Codigo;
         obj25.gxTpr_Sistema_codigo_Z = Z127Sistema_Codigo;
         obj25.gxTpr_Sistema_nome_Z = Z416Sistema_Nome;
         obj25.gxTpr_Sistema_codigo_Z = Z127Sistema_Codigo;
         obj25.gxTpr_Sistema_nome_Z = Z416Sistema_Nome;
         obj25.gxTpr_Sistema_sigla_Z = Z129Sistema_Sigla;
         obj25.gxTpr_Sistema_coordenacao_Z = Z513Sistema_Coordenacao;
         obj25.gxTpr_Sistema_areatrabalhocod_Z = Z135Sistema_AreaTrabalhoCod;
         obj25.gxTpr_Sistema_areatrabalhodes_Z = Z136Sistema_AreaTrabalhoDes;
         obj25.gxTpr_Ambientetecnologico_codigo_Z = Z351AmbienteTecnologico_Codigo;
         obj25.gxTpr_Ambientetecnologico_descricao_Z = Z352AmbienteTecnologico_Descricao;
         obj25.gxTpr_Metodologia_codigo_Z = Z137Metodologia_Codigo;
         obj25.gxTpr_Metodologia_descricao_Z = Z138Metodologia_Descricao;
         obj25.gxTpr_Sistema_pf_Z = Z395Sistema_PF;
         obj25.gxTpr_Sistema_pfa_Z = Z690Sistema_PFA;
         obj25.gxTpr_Sistema_tipo_Z = Z699Sistema_Tipo;
         obj25.gxTpr_Sistema_tiposigla_Z = Z707Sistema_TipoSigla;
         obj25.gxTpr_Sistema_tecnica_Z = Z700Sistema_Tecnica;
         obj25.gxTpr_Sistema_fatorajuste_Z = Z686Sistema_FatorAjuste;
         obj25.gxTpr_Sistema_custo_Z = Z687Sistema_Custo;
         obj25.gxTpr_Sistema_prazo_Z = Z688Sistema_Prazo;
         obj25.gxTpr_Sistema_esforco_Z = Z689Sistema_Esforco;
         obj25.gxTpr_Sistema_impdata_Z = Z1401Sistema_ImpData;
         obj25.gxTpr_Sistema_impusercod_Z = Z1399Sistema_ImpUserCod;
         obj25.gxTpr_Sistema_impuserpescod_Z = Z1402Sistema_ImpUserPesCod;
         obj25.gxTpr_Sistema_impuserpesnom_Z = Z1403Sistema_ImpUserPesNom;
         obj25.gxTpr_Sistema_responsavel_Z = Z1831Sistema_Responsavel;
         obj25.gxTpr_Sistema_gpoobjctrlcod_Z = Z2161Sistema_GpoObjCtrlCod;
         obj25.gxTpr_Sistema_gpoobjctrlrsp_Z = Z2162Sistema_GpoObjCtrlRsp;
         obj25.gxTpr_Sistemaversao_codigo_Z = Z1859SistemaVersao_Codigo;
         obj25.gxTpr_Sistemaversao_id_Z = Z1860SistemaVersao_Id;
         obj25.gxTpr_Sistema_ativo_Z = Z130Sistema_Ativo;
         obj25.gxTpr_Sistema_repositorio_Z = Z2109Sistema_Repositorio;
         obj25.gxTpr_Sistema_ultimo_Z = Z2130Sistema_Ultimo;
         obj25.gxTpr_Sistema_descricao_N = (short)(Convert.ToInt16(n128Sistema_Descricao));
         obj25.gxTpr_Sistema_coordenacao_N = (short)(Convert.ToInt16(n513Sistema_Coordenacao));
         obj25.gxTpr_Sistema_areatrabalhodes_N = (short)(Convert.ToInt16(n136Sistema_AreaTrabalhoDes));
         obj25.gxTpr_Ambientetecnologico_codigo_N = (short)(Convert.ToInt16(n351AmbienteTecnologico_Codigo));
         obj25.gxTpr_Metodologia_codigo_N = (short)(Convert.ToInt16(n137Metodologia_Codigo));
         obj25.gxTpr_Sistema_tipo_N = (short)(Convert.ToInt16(n699Sistema_Tipo));
         obj25.gxTpr_Sistema_tecnica_N = (short)(Convert.ToInt16(n700Sistema_Tecnica));
         obj25.gxTpr_Sistema_fatorajuste_N = (short)(Convert.ToInt16(n686Sistema_FatorAjuste));
         obj25.gxTpr_Sistema_custo_N = (short)(Convert.ToInt16(n687Sistema_Custo));
         obj25.gxTpr_Sistema_prazo_N = (short)(Convert.ToInt16(n688Sistema_Prazo));
         obj25.gxTpr_Sistema_esforco_N = (short)(Convert.ToInt16(n689Sistema_Esforco));
         obj25.gxTpr_Sistema_impdata_N = (short)(Convert.ToInt16(n1401Sistema_ImpData));
         obj25.gxTpr_Sistema_impusercod_N = (short)(Convert.ToInt16(n1399Sistema_ImpUserCod));
         obj25.gxTpr_Sistema_impuserpescod_N = (short)(Convert.ToInt16(n1402Sistema_ImpUserPesCod));
         obj25.gxTpr_Sistema_impuserpesnom_N = (short)(Convert.ToInt16(n1403Sistema_ImpUserPesNom));
         obj25.gxTpr_Sistema_responsavel_N = (short)(Convert.ToInt16(n1831Sistema_Responsavel));
         obj25.gxTpr_Sistema_gpoobjctrlcod_N = (short)(Convert.ToInt16(n2161Sistema_GpoObjCtrlCod));
         obj25.gxTpr_Sistemaversao_codigo_N = (short)(Convert.ToInt16(n1859SistemaVersao_Codigo));
         obj25.gxTpr_Sistema_repositorio_N = (short)(Convert.ToInt16(n2109Sistema_Repositorio));
         obj25.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow25( SdtSistema obj25 )
      {
         obj25.gxTpr_Sistema_codigo = A127Sistema_Codigo;
         return  ;
      }

      public void RowToVars25( SdtSistema obj25 ,
                               int forceLoad )
      {
         Gx_mode = obj25.gxTpr_Mode;
         A707Sistema_TipoSigla = obj25.gxTpr_Sistema_tiposigla;
         A690Sistema_PFA = obj25.gxTpr_Sistema_pfa;
         A395Sistema_PF = obj25.gxTpr_Sistema_pf;
         A416Sistema_Nome = obj25.gxTpr_Sistema_nome;
         A128Sistema_Descricao = obj25.gxTpr_Sistema_descricao;
         n128Sistema_Descricao = false;
         A129Sistema_Sigla = obj25.gxTpr_Sistema_sigla;
         A513Sistema_Coordenacao = obj25.gxTpr_Sistema_coordenacao;
         n513Sistema_Coordenacao = false;
         A136Sistema_AreaTrabalhoDes = obj25.gxTpr_Sistema_areatrabalhodes;
         n136Sistema_AreaTrabalhoDes = false;
         A351AmbienteTecnologico_Codigo = obj25.gxTpr_Ambientetecnologico_codigo;
         n351AmbienteTecnologico_Codigo = false;
         A352AmbienteTecnologico_Descricao = obj25.gxTpr_Ambientetecnologico_descricao;
         A137Metodologia_Codigo = obj25.gxTpr_Metodologia_codigo;
         n137Metodologia_Codigo = false;
         A138Metodologia_Descricao = obj25.gxTpr_Metodologia_descricao;
         A700Sistema_Tecnica = obj25.gxTpr_Sistema_tecnica;
         n700Sistema_Tecnica = false;
         A687Sistema_Custo = obj25.gxTpr_Sistema_custo;
         n687Sistema_Custo = false;
         A688Sistema_Prazo = obj25.gxTpr_Sistema_prazo;
         n688Sistema_Prazo = false;
         A689Sistema_Esforco = obj25.gxTpr_Sistema_esforco;
         n689Sistema_Esforco = false;
         A1401Sistema_ImpData = obj25.gxTpr_Sistema_impdata;
         n1401Sistema_ImpData = false;
         A1399Sistema_ImpUserCod = obj25.gxTpr_Sistema_impusercod;
         n1399Sistema_ImpUserCod = false;
         A1402Sistema_ImpUserPesCod = obj25.gxTpr_Sistema_impuserpescod;
         n1402Sistema_ImpUserPesCod = false;
         A1403Sistema_ImpUserPesNom = obj25.gxTpr_Sistema_impuserpesnom;
         n1403Sistema_ImpUserPesNom = false;
         A1831Sistema_Responsavel = obj25.gxTpr_Sistema_responsavel;
         n1831Sistema_Responsavel = false;
         A2161Sistema_GpoObjCtrlCod = obj25.gxTpr_Sistema_gpoobjctrlcod;
         n2161Sistema_GpoObjCtrlCod = false;
         A2162Sistema_GpoObjCtrlRsp = obj25.gxTpr_Sistema_gpoobjctrlrsp;
         A1859SistemaVersao_Codigo = obj25.gxTpr_Sistemaversao_codigo;
         n1859SistemaVersao_Codigo = false;
         A1860SistemaVersao_Id = obj25.gxTpr_Sistemaversao_id;
         A2109Sistema_Repositorio = obj25.gxTpr_Sistema_repositorio;
         n2109Sistema_Repositorio = false;
         if ( forceLoad == 1 )
         {
            A2130Sistema_Ultimo = obj25.gxTpr_Sistema_ultimo;
         }
         A135Sistema_AreaTrabalhoCod = obj25.gxTpr_Sistema_areatrabalhocod;
         A699Sistema_Tipo = obj25.gxTpr_Sistema_tipo;
         n699Sistema_Tipo = false;
         A686Sistema_FatorAjuste = obj25.gxTpr_Sistema_fatorajuste;
         n686Sistema_FatorAjuste = false;
         A130Sistema_Ativo = obj25.gxTpr_Sistema_ativo;
         A127Sistema_Codigo = obj25.gxTpr_Sistema_codigo;
         Z127Sistema_Codigo = obj25.gxTpr_Sistema_codigo_Z;
         Z416Sistema_Nome = obj25.gxTpr_Sistema_nome_Z;
         Z127Sistema_Codigo = obj25.gxTpr_Sistema_codigo_Z;
         Z416Sistema_Nome = obj25.gxTpr_Sistema_nome_Z;
         Z129Sistema_Sigla = obj25.gxTpr_Sistema_sigla_Z;
         Z513Sistema_Coordenacao = obj25.gxTpr_Sistema_coordenacao_Z;
         Z135Sistema_AreaTrabalhoCod = obj25.gxTpr_Sistema_areatrabalhocod_Z;
         Z136Sistema_AreaTrabalhoDes = obj25.gxTpr_Sistema_areatrabalhodes_Z;
         Z351AmbienteTecnologico_Codigo = obj25.gxTpr_Ambientetecnologico_codigo_Z;
         Z352AmbienteTecnologico_Descricao = obj25.gxTpr_Ambientetecnologico_descricao_Z;
         Z137Metodologia_Codigo = obj25.gxTpr_Metodologia_codigo_Z;
         Z138Metodologia_Descricao = obj25.gxTpr_Metodologia_descricao_Z;
         Z395Sistema_PF = obj25.gxTpr_Sistema_pf_Z;
         Z690Sistema_PFA = obj25.gxTpr_Sistema_pfa_Z;
         Z699Sistema_Tipo = obj25.gxTpr_Sistema_tipo_Z;
         Z707Sistema_TipoSigla = obj25.gxTpr_Sistema_tiposigla_Z;
         Z700Sistema_Tecnica = obj25.gxTpr_Sistema_tecnica_Z;
         Z686Sistema_FatorAjuste = obj25.gxTpr_Sistema_fatorajuste_Z;
         Z687Sistema_Custo = obj25.gxTpr_Sistema_custo_Z;
         Z688Sistema_Prazo = obj25.gxTpr_Sistema_prazo_Z;
         Z689Sistema_Esforco = obj25.gxTpr_Sistema_esforco_Z;
         Z1401Sistema_ImpData = obj25.gxTpr_Sistema_impdata_Z;
         Z1399Sistema_ImpUserCod = obj25.gxTpr_Sistema_impusercod_Z;
         Z1402Sistema_ImpUserPesCod = obj25.gxTpr_Sistema_impuserpescod_Z;
         Z1403Sistema_ImpUserPesNom = obj25.gxTpr_Sistema_impuserpesnom_Z;
         Z1831Sistema_Responsavel = obj25.gxTpr_Sistema_responsavel_Z;
         Z2161Sistema_GpoObjCtrlCod = obj25.gxTpr_Sistema_gpoobjctrlcod_Z;
         Z2162Sistema_GpoObjCtrlRsp = obj25.gxTpr_Sistema_gpoobjctrlrsp_Z;
         Z1859SistemaVersao_Codigo = obj25.gxTpr_Sistemaversao_codigo_Z;
         Z1860SistemaVersao_Id = obj25.gxTpr_Sistemaversao_id_Z;
         Z130Sistema_Ativo = obj25.gxTpr_Sistema_ativo_Z;
         Z2109Sistema_Repositorio = obj25.gxTpr_Sistema_repositorio_Z;
         Z2130Sistema_Ultimo = obj25.gxTpr_Sistema_ultimo_Z;
         O2130Sistema_Ultimo = obj25.gxTpr_Sistema_ultimo_Z;
         n128Sistema_Descricao = (bool)(Convert.ToBoolean(obj25.gxTpr_Sistema_descricao_N));
         n513Sistema_Coordenacao = (bool)(Convert.ToBoolean(obj25.gxTpr_Sistema_coordenacao_N));
         n136Sistema_AreaTrabalhoDes = (bool)(Convert.ToBoolean(obj25.gxTpr_Sistema_areatrabalhodes_N));
         n351AmbienteTecnologico_Codigo = (bool)(Convert.ToBoolean(obj25.gxTpr_Ambientetecnologico_codigo_N));
         n137Metodologia_Codigo = (bool)(Convert.ToBoolean(obj25.gxTpr_Metodologia_codigo_N));
         n699Sistema_Tipo = (bool)(Convert.ToBoolean(obj25.gxTpr_Sistema_tipo_N));
         n700Sistema_Tecnica = (bool)(Convert.ToBoolean(obj25.gxTpr_Sistema_tecnica_N));
         n686Sistema_FatorAjuste = (bool)(Convert.ToBoolean(obj25.gxTpr_Sistema_fatorajuste_N));
         n687Sistema_Custo = (bool)(Convert.ToBoolean(obj25.gxTpr_Sistema_custo_N));
         n688Sistema_Prazo = (bool)(Convert.ToBoolean(obj25.gxTpr_Sistema_prazo_N));
         n689Sistema_Esforco = (bool)(Convert.ToBoolean(obj25.gxTpr_Sistema_esforco_N));
         n1401Sistema_ImpData = (bool)(Convert.ToBoolean(obj25.gxTpr_Sistema_impdata_N));
         n1399Sistema_ImpUserCod = (bool)(Convert.ToBoolean(obj25.gxTpr_Sistema_impusercod_N));
         n1402Sistema_ImpUserPesCod = (bool)(Convert.ToBoolean(obj25.gxTpr_Sistema_impuserpescod_N));
         n1403Sistema_ImpUserPesNom = (bool)(Convert.ToBoolean(obj25.gxTpr_Sistema_impuserpesnom_N));
         n1831Sistema_Responsavel = (bool)(Convert.ToBoolean(obj25.gxTpr_Sistema_responsavel_N));
         n2161Sistema_GpoObjCtrlCod = (bool)(Convert.ToBoolean(obj25.gxTpr_Sistema_gpoobjctrlcod_N));
         n1859SistemaVersao_Codigo = (bool)(Convert.ToBoolean(obj25.gxTpr_Sistemaversao_codigo_N));
         n2109Sistema_Repositorio = (bool)(Convert.ToBoolean(obj25.gxTpr_Sistema_repositorio_N));
         Gx_mode = obj25.gxTpr_Mode;
         return  ;
      }

      public void VarsToRow233( SdtSistema_Tecnologia obj233 )
      {
         obj233.gxTpr_Mode = Gx_mode;
         obj233.gxTpr_Tecnologia_codigo = A131Tecnologia_Codigo;
         obj233.gxTpr_Tecnologia_nome = A132Tecnologia_Nome;
         obj233.gxTpr_Tecnologia_tipotecnologia = A355Tecnologia_TipoTecnologia;
         obj233.gxTpr_Sistematecnologialinha = A2128SistemaTecnologiaLinha;
         obj233.gxTpr_Sistematecnologialinha_Z = Z2128SistemaTecnologiaLinha;
         obj233.gxTpr_Tecnologia_codigo_Z = Z131Tecnologia_Codigo;
         obj233.gxTpr_Tecnologia_nome_Z = Z132Tecnologia_Nome;
         obj233.gxTpr_Tecnologia_tipotecnologia_Z = Z355Tecnologia_TipoTecnologia;
         obj233.gxTpr_Tecnologia_tipotecnologia_N = (short)(Convert.ToInt16(n355Tecnologia_TipoTecnologia));
         obj233.gxTpr_Modified = nIsMod_233;
         return  ;
      }

      public void KeyVarsToRow233( SdtSistema_Tecnologia obj233 )
      {
         obj233.gxTpr_Sistematecnologialinha = A2128SistemaTecnologiaLinha;
         return  ;
      }

      public void RowToVars233( SdtSistema_Tecnologia obj233 ,
                                int forceLoad )
      {
         Gx_mode = obj233.gxTpr_Mode;
         A131Tecnologia_Codigo = obj233.gxTpr_Tecnologia_codigo;
         A132Tecnologia_Nome = obj233.gxTpr_Tecnologia_nome;
         A355Tecnologia_TipoTecnologia = obj233.gxTpr_Tecnologia_tipotecnologia;
         n355Tecnologia_TipoTecnologia = false;
         A2128SistemaTecnologiaLinha = obj233.gxTpr_Sistematecnologialinha;
         Z2128SistemaTecnologiaLinha = obj233.gxTpr_Sistematecnologialinha_Z;
         Z131Tecnologia_Codigo = obj233.gxTpr_Tecnologia_codigo_Z;
         Z132Tecnologia_Nome = obj233.gxTpr_Tecnologia_nome_Z;
         Z355Tecnologia_TipoTecnologia = obj233.gxTpr_Tecnologia_tipotecnologia_Z;
         n355Tecnologia_TipoTecnologia = (bool)(Convert.ToBoolean(obj233.gxTpr_Tecnologia_tipotecnologia_N));
         nIsMod_233 = obj233.gxTpr_Modified;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A127Sistema_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey0O25( ) ;
         ScanKeyStart0O25( ) ;
         if ( RcdFound25 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z127Sistema_Codigo = A127Sistema_Codigo;
            O2130Sistema_Ultimo = A2130Sistema_Ultimo;
         }
         ZM0O25( -20) ;
         OnLoadActions0O25( ) ;
         AddRow0O25( ) ;
         bcSistema.gxTpr_Tecnologia.ClearCollection();
         if ( RcdFound25 == 1 )
         {
            ScanKeyStart0O233( ) ;
            nGXsfl_233_idx = 1;
            while ( RcdFound233 != 0 )
            {
               Z127Sistema_Codigo = A127Sistema_Codigo;
               Z2128SistemaTecnologiaLinha = A2128SistemaTecnologiaLinha;
               ZM0O233( -28) ;
               OnLoadActions0O233( ) ;
               nRcdExists_233 = 1;
               nIsMod_233 = 0;
               AddRow0O233( ) ;
               nGXsfl_233_idx = (short)(nGXsfl_233_idx+1);
               ScanKeyNext0O233( ) ;
            }
            ScanKeyEnd0O233( ) ;
         }
         ScanKeyEnd0O25( ) ;
         if ( RcdFound25 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars25( bcSistema, 0) ;
         ScanKeyStart0O25( ) ;
         if ( RcdFound25 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z127Sistema_Codigo = A127Sistema_Codigo;
            O2130Sistema_Ultimo = A2130Sistema_Ultimo;
         }
         ZM0O25( -20) ;
         OnLoadActions0O25( ) ;
         AddRow0O25( ) ;
         bcSistema.gxTpr_Tecnologia.ClearCollection();
         if ( RcdFound25 == 1 )
         {
            ScanKeyStart0O233( ) ;
            nGXsfl_233_idx = 1;
            while ( RcdFound233 != 0 )
            {
               Z127Sistema_Codigo = A127Sistema_Codigo;
               Z2128SistemaTecnologiaLinha = A2128SistemaTecnologiaLinha;
               ZM0O233( -28) ;
               OnLoadActions0O233( ) ;
               nRcdExists_233 = 1;
               nIsMod_233 = 0;
               AddRow0O233( ) ;
               nGXsfl_233_idx = (short)(nGXsfl_233_idx+1);
               ScanKeyNext0O233( ) ;
            }
            ScanKeyEnd0O233( ) ;
         }
         ScanKeyEnd0O25( ) ;
         if ( RcdFound25 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars25( bcSistema, 0) ;
         nKeyPressed = 1;
         GetKey0O25( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            A2130Sistema_Ultimo = O2130Sistema_Ultimo;
            Insert0O25( ) ;
         }
         else
         {
            if ( RcdFound25 == 1 )
            {
               if ( A127Sistema_Codigo != Z127Sistema_Codigo )
               {
                  A127Sistema_Codigo = Z127Sistema_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  A2130Sistema_Ultimo = O2130Sistema_Ultimo;
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  A2130Sistema_Ultimo = O2130Sistema_Ultimo;
                  Update0O25( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A127Sistema_Codigo != Z127Sistema_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        A2130Sistema_Ultimo = O2130Sistema_Ultimo;
                        Insert0O25( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        A2130Sistema_Ultimo = O2130Sistema_Ultimo;
                        Insert0O25( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow25( bcSistema) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars25( bcSistema, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey0O25( ) ;
         if ( RcdFound25 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A127Sistema_Codigo != Z127Sistema_Codigo )
            {
               A127Sistema_Codigo = Z127Sistema_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A127Sistema_Codigo != Z127Sistema_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(4);
         pr_default.close(1);
         pr_default.close(19);
         pr_default.close(18);
         pr_default.close(22);
         pr_default.close(17);
         pr_default.close(20);
         pr_default.close(21);
         pr_default.close(46);
         context.RollbackDataStores( "Sistema_BC");
         VarsToRow25( bcSistema) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcSistema.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcSistema.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcSistema )
         {
            bcSistema = (SdtSistema)(sdt);
            if ( StringUtil.StrCmp(bcSistema.gxTpr_Mode, "") == 0 )
            {
               bcSistema.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow25( bcSistema) ;
            }
            else
            {
               RowToVars25( bcSistema, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcSistema.gxTpr_Mode, "") == 0 )
            {
               bcSistema.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars25( bcSistema, 1) ;
         return  ;
      }

      public SdtSistema Sistema_BC
      {
         get {
            return bcSistema ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityHigh ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "sistema_Execute" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(46);
         pr_default.close(4);
         pr_default.close(19);
         pr_default.close(18);
         pr_default.close(22);
         pr_default.close(17);
         pr_default.close(20);
         pr_default.close(21);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         sMode25 = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV60Pgmname = "";
         AV14TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV54AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         Z416Sistema_Nome = "";
         A416Sistema_Nome = "";
         Z129Sistema_Sigla = "";
         A129Sistema_Sigla = "";
         Z513Sistema_Coordenacao = "";
         A513Sistema_Coordenacao = "";
         Z699Sistema_Tipo = "";
         A699Sistema_Tipo = "";
         Z700Sistema_Tecnica = "";
         A700Sistema_Tecnica = "";
         Z1401Sistema_ImpData = (DateTime)(DateTime.MinValue);
         A1401Sistema_ImpData = (DateTime)(DateTime.MinValue);
         Z2109Sistema_Repositorio = "";
         A2109Sistema_Repositorio = "";
         Z707Sistema_TipoSigla = "";
         A707Sistema_TipoSigla = "";
         Z132Tecnologia_Nome = "";
         A132Tecnologia_Nome = "";
         Z355Tecnologia_TipoTecnologia = "";
         A355Tecnologia_TipoTecnologia = "";
         Z138Metodologia_Descricao = "";
         A138Metodologia_Descricao = "";
         Z352AmbienteTecnologico_Descricao = "";
         A352AmbienteTecnologico_Descricao = "";
         Z1860SistemaVersao_Id = "";
         A1860SistemaVersao_Id = "";
         Z136Sistema_AreaTrabalhoDes = "";
         A136Sistema_AreaTrabalhoDes = "";
         Z1403Sistema_ImpUserPesNom = "";
         A1403Sistema_ImpUserPesNom = "";
         Z128Sistema_Descricao = "";
         A128Sistema_Descricao = "";
         BC000O10_A136Sistema_AreaTrabalhoDes = new String[] {""} ;
         BC000O10_n136Sistema_AreaTrabalhoDes = new bool[] {false} ;
         BC000O14_A127Sistema_Codigo = new int[1] ;
         BC000O14_A416Sistema_Nome = new String[] {""} ;
         BC000O14_A128Sistema_Descricao = new String[] {""} ;
         BC000O14_n128Sistema_Descricao = new bool[] {false} ;
         BC000O14_A129Sistema_Sigla = new String[] {""} ;
         BC000O14_A513Sistema_Coordenacao = new String[] {""} ;
         BC000O14_n513Sistema_Coordenacao = new bool[] {false} ;
         BC000O14_A136Sistema_AreaTrabalhoDes = new String[] {""} ;
         BC000O14_n136Sistema_AreaTrabalhoDes = new bool[] {false} ;
         BC000O14_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         BC000O14_A138Metodologia_Descricao = new String[] {""} ;
         BC000O14_A699Sistema_Tipo = new String[] {""} ;
         BC000O14_n699Sistema_Tipo = new bool[] {false} ;
         BC000O14_A700Sistema_Tecnica = new String[] {""} ;
         BC000O14_n700Sistema_Tecnica = new bool[] {false} ;
         BC000O14_A686Sistema_FatorAjuste = new decimal[1] ;
         BC000O14_n686Sistema_FatorAjuste = new bool[] {false} ;
         BC000O14_A687Sistema_Custo = new decimal[1] ;
         BC000O14_n687Sistema_Custo = new bool[] {false} ;
         BC000O14_A688Sistema_Prazo = new short[1] ;
         BC000O14_n688Sistema_Prazo = new bool[] {false} ;
         BC000O14_A689Sistema_Esforco = new short[1] ;
         BC000O14_n689Sistema_Esforco = new bool[] {false} ;
         BC000O14_A1401Sistema_ImpData = new DateTime[] {DateTime.MinValue} ;
         BC000O14_n1401Sistema_ImpData = new bool[] {false} ;
         BC000O14_A1403Sistema_ImpUserPesNom = new String[] {""} ;
         BC000O14_n1403Sistema_ImpUserPesNom = new bool[] {false} ;
         BC000O14_A1831Sistema_Responsavel = new int[1] ;
         BC000O14_n1831Sistema_Responsavel = new bool[] {false} ;
         BC000O14_A1860SistemaVersao_Id = new String[] {""} ;
         BC000O14_A130Sistema_Ativo = new bool[] {false} ;
         BC000O14_A2109Sistema_Repositorio = new String[] {""} ;
         BC000O14_n2109Sistema_Repositorio = new bool[] {false} ;
         BC000O14_A2130Sistema_Ultimo = new short[1] ;
         BC000O14_A137Metodologia_Codigo = new int[1] ;
         BC000O14_n137Metodologia_Codigo = new bool[] {false} ;
         BC000O14_A351AmbienteTecnologico_Codigo = new int[1] ;
         BC000O14_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         BC000O14_A1859SistemaVersao_Codigo = new int[1] ;
         BC000O14_n1859SistemaVersao_Codigo = new bool[] {false} ;
         BC000O14_A135Sistema_AreaTrabalhoCod = new int[1] ;
         BC000O14_A1399Sistema_ImpUserCod = new int[1] ;
         BC000O14_n1399Sistema_ImpUserCod = new bool[] {false} ;
         BC000O14_A2161Sistema_GpoObjCtrlCod = new int[1] ;
         BC000O14_n2161Sistema_GpoObjCtrlCod = new bool[] {false} ;
         BC000O14_A1402Sistema_ImpUserPesCod = new int[1] ;
         BC000O14_n1402Sistema_ImpUserPesCod = new bool[] {false} ;
         BC000O8_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         BC000O7_A138Metodologia_Descricao = new String[] {""} ;
         BC000O11_A1402Sistema_ImpUserPesCod = new int[1] ;
         BC000O11_n1402Sistema_ImpUserPesCod = new bool[] {false} ;
         BC000O13_A1403Sistema_ImpUserPesNom = new String[] {""} ;
         BC000O13_n1403Sistema_ImpUserPesNom = new bool[] {false} ;
         BC000O12_A2161Sistema_GpoObjCtrlCod = new int[1] ;
         BC000O12_n2161Sistema_GpoObjCtrlCod = new bool[] {false} ;
         BC000O9_A1860SistemaVersao_Id = new String[] {""} ;
         BC000O15_A127Sistema_Codigo = new int[1] ;
         BC000O6_A127Sistema_Codigo = new int[1] ;
         BC000O6_A416Sistema_Nome = new String[] {""} ;
         BC000O6_A128Sistema_Descricao = new String[] {""} ;
         BC000O6_n128Sistema_Descricao = new bool[] {false} ;
         BC000O6_A129Sistema_Sigla = new String[] {""} ;
         BC000O6_A513Sistema_Coordenacao = new String[] {""} ;
         BC000O6_n513Sistema_Coordenacao = new bool[] {false} ;
         BC000O6_A699Sistema_Tipo = new String[] {""} ;
         BC000O6_n699Sistema_Tipo = new bool[] {false} ;
         BC000O6_A700Sistema_Tecnica = new String[] {""} ;
         BC000O6_n700Sistema_Tecnica = new bool[] {false} ;
         BC000O6_A686Sistema_FatorAjuste = new decimal[1] ;
         BC000O6_n686Sistema_FatorAjuste = new bool[] {false} ;
         BC000O6_A687Sistema_Custo = new decimal[1] ;
         BC000O6_n687Sistema_Custo = new bool[] {false} ;
         BC000O6_A688Sistema_Prazo = new short[1] ;
         BC000O6_n688Sistema_Prazo = new bool[] {false} ;
         BC000O6_A689Sistema_Esforco = new short[1] ;
         BC000O6_n689Sistema_Esforco = new bool[] {false} ;
         BC000O6_A1401Sistema_ImpData = new DateTime[] {DateTime.MinValue} ;
         BC000O6_n1401Sistema_ImpData = new bool[] {false} ;
         BC000O6_A1831Sistema_Responsavel = new int[1] ;
         BC000O6_n1831Sistema_Responsavel = new bool[] {false} ;
         BC000O6_A130Sistema_Ativo = new bool[] {false} ;
         BC000O6_A2109Sistema_Repositorio = new String[] {""} ;
         BC000O6_n2109Sistema_Repositorio = new bool[] {false} ;
         BC000O6_A2130Sistema_Ultimo = new short[1] ;
         BC000O6_A137Metodologia_Codigo = new int[1] ;
         BC000O6_n137Metodologia_Codigo = new bool[] {false} ;
         BC000O6_A351AmbienteTecnologico_Codigo = new int[1] ;
         BC000O6_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         BC000O6_A1859SistemaVersao_Codigo = new int[1] ;
         BC000O6_n1859SistemaVersao_Codigo = new bool[] {false} ;
         BC000O6_A135Sistema_AreaTrabalhoCod = new int[1] ;
         BC000O6_A1399Sistema_ImpUserCod = new int[1] ;
         BC000O6_n1399Sistema_ImpUserCod = new bool[] {false} ;
         BC000O6_A2161Sistema_GpoObjCtrlCod = new int[1] ;
         BC000O6_n2161Sistema_GpoObjCtrlCod = new bool[] {false} ;
         BC000O5_A127Sistema_Codigo = new int[1] ;
         BC000O5_A416Sistema_Nome = new String[] {""} ;
         BC000O5_A128Sistema_Descricao = new String[] {""} ;
         BC000O5_n128Sistema_Descricao = new bool[] {false} ;
         BC000O5_A129Sistema_Sigla = new String[] {""} ;
         BC000O5_A513Sistema_Coordenacao = new String[] {""} ;
         BC000O5_n513Sistema_Coordenacao = new bool[] {false} ;
         BC000O5_A699Sistema_Tipo = new String[] {""} ;
         BC000O5_n699Sistema_Tipo = new bool[] {false} ;
         BC000O5_A700Sistema_Tecnica = new String[] {""} ;
         BC000O5_n700Sistema_Tecnica = new bool[] {false} ;
         BC000O5_A686Sistema_FatorAjuste = new decimal[1] ;
         BC000O5_n686Sistema_FatorAjuste = new bool[] {false} ;
         BC000O5_A687Sistema_Custo = new decimal[1] ;
         BC000O5_n687Sistema_Custo = new bool[] {false} ;
         BC000O5_A688Sistema_Prazo = new short[1] ;
         BC000O5_n688Sistema_Prazo = new bool[] {false} ;
         BC000O5_A689Sistema_Esforco = new short[1] ;
         BC000O5_n689Sistema_Esforco = new bool[] {false} ;
         BC000O5_A1401Sistema_ImpData = new DateTime[] {DateTime.MinValue} ;
         BC000O5_n1401Sistema_ImpData = new bool[] {false} ;
         BC000O5_A1831Sistema_Responsavel = new int[1] ;
         BC000O5_n1831Sistema_Responsavel = new bool[] {false} ;
         BC000O5_A130Sistema_Ativo = new bool[] {false} ;
         BC000O5_A2109Sistema_Repositorio = new String[] {""} ;
         BC000O5_n2109Sistema_Repositorio = new bool[] {false} ;
         BC000O5_A2130Sistema_Ultimo = new short[1] ;
         BC000O5_A137Metodologia_Codigo = new int[1] ;
         BC000O5_n137Metodologia_Codigo = new bool[] {false} ;
         BC000O5_A351AmbienteTecnologico_Codigo = new int[1] ;
         BC000O5_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         BC000O5_A1859SistemaVersao_Codigo = new int[1] ;
         BC000O5_n1859SistemaVersao_Codigo = new bool[] {false} ;
         BC000O5_A135Sistema_AreaTrabalhoCod = new int[1] ;
         BC000O5_A1399Sistema_ImpUserCod = new int[1] ;
         BC000O5_n1399Sistema_ImpUserCod = new bool[] {false} ;
         BC000O5_A2161Sistema_GpoObjCtrlCod = new int[1] ;
         BC000O5_n2161Sistema_GpoObjCtrlCod = new bool[] {false} ;
         BC000O16_A127Sistema_Codigo = new int[1] ;
         BC000O19_A136Sistema_AreaTrabalhoDes = new String[] {""} ;
         BC000O19_n136Sistema_AreaTrabalhoDes = new bool[] {false} ;
         BC000O20_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         BC000O21_A138Metodologia_Descricao = new String[] {""} ;
         BC000O22_A1402Sistema_ImpUserPesCod = new int[1] ;
         BC000O22_n1402Sistema_ImpUserPesCod = new bool[] {false} ;
         BC000O23_A1403Sistema_ImpUserPesNom = new String[] {""} ;
         BC000O23_n1403Sistema_ImpUserPesNom = new bool[] {false} ;
         BC000O24_A1860SistemaVersao_Id = new String[] {""} ;
         BC000O25_A648Projeto_Codigo = new int[1] ;
         BC000O26_A1859SistemaVersao_Codigo = new int[1] ;
         BC000O26_n1859SistemaVersao_Codigo = new bool[] {false} ;
         BC000O27_A1725ContratoSistemas_CntCod = new int[1] ;
         BC000O27_A1726ContratoSistemas_SistemaCod = new int[1] ;
         BC000O28_A160ContratoServicos_Codigo = new int[1] ;
         BC000O28_A1067ContratoServicosSistemas_ServicoCod = new int[1] ;
         BC000O28_A1063ContratoServicosSistemas_SistemaCod = new int[1] ;
         BC000O29_A996SolicitacaoMudanca_Codigo = new int[1] ;
         BC000O30_A192Contagem_Codigo = new int[1] ;
         BC000O31_A736ProjetoMelhoria_Codigo = new int[1] ;
         BC000O32_A456ContagemResultado_Codigo = new int[1] ;
         BC000O33_A368FuncaoDados_Codigo = new int[1] ;
         BC000O34_A165FuncaoAPF_Codigo = new int[1] ;
         BC000O35_A172Tabela_Codigo = new int[1] ;
         BC000O36_A439Solicitacoes_Codigo = new int[1] ;
         BC000O37_A161FuncaoUsuario_Codigo = new int[1] ;
         BC000O38_A146Modulo_Codigo = new int[1] ;
         BC000O39_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         BC000O39_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         BC000O39_A127Sistema_Codigo = new int[1] ;
         BC000O40_A127Sistema_Codigo = new int[1] ;
         BC000O40_A1460SistemaDePara_Codigo = new int[1] ;
         BC000O42_A127Sistema_Codigo = new int[1] ;
         BC000O42_A416Sistema_Nome = new String[] {""} ;
         BC000O42_A128Sistema_Descricao = new String[] {""} ;
         BC000O42_n128Sistema_Descricao = new bool[] {false} ;
         BC000O42_A129Sistema_Sigla = new String[] {""} ;
         BC000O42_A513Sistema_Coordenacao = new String[] {""} ;
         BC000O42_n513Sistema_Coordenacao = new bool[] {false} ;
         BC000O42_A136Sistema_AreaTrabalhoDes = new String[] {""} ;
         BC000O42_n136Sistema_AreaTrabalhoDes = new bool[] {false} ;
         BC000O42_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         BC000O42_A138Metodologia_Descricao = new String[] {""} ;
         BC000O42_A699Sistema_Tipo = new String[] {""} ;
         BC000O42_n699Sistema_Tipo = new bool[] {false} ;
         BC000O42_A700Sistema_Tecnica = new String[] {""} ;
         BC000O42_n700Sistema_Tecnica = new bool[] {false} ;
         BC000O42_A686Sistema_FatorAjuste = new decimal[1] ;
         BC000O42_n686Sistema_FatorAjuste = new bool[] {false} ;
         BC000O42_A687Sistema_Custo = new decimal[1] ;
         BC000O42_n687Sistema_Custo = new bool[] {false} ;
         BC000O42_A688Sistema_Prazo = new short[1] ;
         BC000O42_n688Sistema_Prazo = new bool[] {false} ;
         BC000O42_A689Sistema_Esforco = new short[1] ;
         BC000O42_n689Sistema_Esforco = new bool[] {false} ;
         BC000O42_A1401Sistema_ImpData = new DateTime[] {DateTime.MinValue} ;
         BC000O42_n1401Sistema_ImpData = new bool[] {false} ;
         BC000O42_A1403Sistema_ImpUserPesNom = new String[] {""} ;
         BC000O42_n1403Sistema_ImpUserPesNom = new bool[] {false} ;
         BC000O42_A1831Sistema_Responsavel = new int[1] ;
         BC000O42_n1831Sistema_Responsavel = new bool[] {false} ;
         BC000O42_A1860SistemaVersao_Id = new String[] {""} ;
         BC000O42_A130Sistema_Ativo = new bool[] {false} ;
         BC000O42_A2109Sistema_Repositorio = new String[] {""} ;
         BC000O42_n2109Sistema_Repositorio = new bool[] {false} ;
         BC000O42_A2130Sistema_Ultimo = new short[1] ;
         BC000O42_A137Metodologia_Codigo = new int[1] ;
         BC000O42_n137Metodologia_Codigo = new bool[] {false} ;
         BC000O42_A351AmbienteTecnologico_Codigo = new int[1] ;
         BC000O42_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         BC000O42_A1859SistemaVersao_Codigo = new int[1] ;
         BC000O42_n1859SistemaVersao_Codigo = new bool[] {false} ;
         BC000O42_A135Sistema_AreaTrabalhoCod = new int[1] ;
         BC000O42_A1399Sistema_ImpUserCod = new int[1] ;
         BC000O42_n1399Sistema_ImpUserCod = new bool[] {false} ;
         BC000O42_A2161Sistema_GpoObjCtrlCod = new int[1] ;
         BC000O42_n2161Sistema_GpoObjCtrlCod = new bool[] {false} ;
         BC000O42_A1402Sistema_ImpUserPesCod = new int[1] ;
         BC000O42_n1402Sistema_ImpUserPesCod = new bool[] {false} ;
         BC000O43_A127Sistema_Codigo = new int[1] ;
         BC000O43_A2128SistemaTecnologiaLinha = new short[1] ;
         BC000O43_A132Tecnologia_Nome = new String[] {""} ;
         BC000O43_A355Tecnologia_TipoTecnologia = new String[] {""} ;
         BC000O43_n355Tecnologia_TipoTecnologia = new bool[] {false} ;
         BC000O43_A131Tecnologia_Codigo = new int[1] ;
         BC000O4_A132Tecnologia_Nome = new String[] {""} ;
         BC000O4_A355Tecnologia_TipoTecnologia = new String[] {""} ;
         BC000O4_n355Tecnologia_TipoTecnologia = new bool[] {false} ;
         BC000O44_A127Sistema_Codigo = new int[1] ;
         BC000O44_A2128SistemaTecnologiaLinha = new short[1] ;
         BC000O3_A127Sistema_Codigo = new int[1] ;
         BC000O3_A2128SistemaTecnologiaLinha = new short[1] ;
         BC000O3_A131Tecnologia_Codigo = new int[1] ;
         sMode233 = "";
         BC000O2_A127Sistema_Codigo = new int[1] ;
         BC000O2_A2128SistemaTecnologiaLinha = new short[1] ;
         BC000O2_A131Tecnologia_Codigo = new int[1] ;
         BC000O48_A132Tecnologia_Nome = new String[] {""} ;
         BC000O48_A355Tecnologia_TipoTecnologia = new String[] {""} ;
         BC000O48_n355Tecnologia_TipoTecnologia = new bool[] {false} ;
         BC000O49_A127Sistema_Codigo = new int[1] ;
         BC000O49_A2128SistemaTecnologiaLinha = new short[1] ;
         BC000O49_A132Tecnologia_Nome = new String[] {""} ;
         BC000O49_A355Tecnologia_TipoTecnologia = new String[] {""} ;
         BC000O49_n355Tecnologia_TipoTecnologia = new bool[] {false} ;
         BC000O49_A131Tecnologia_Codigo = new int[1] ;
         i699Sistema_Tipo = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.sistema_bc__default(),
            new Object[][] {
                new Object[] {
               BC000O2_A127Sistema_Codigo, BC000O2_A2128SistemaTecnologiaLinha, BC000O2_A131Tecnologia_Codigo
               }
               , new Object[] {
               BC000O3_A127Sistema_Codigo, BC000O3_A2128SistemaTecnologiaLinha, BC000O3_A131Tecnologia_Codigo
               }
               , new Object[] {
               BC000O4_A132Tecnologia_Nome, BC000O4_A355Tecnologia_TipoTecnologia, BC000O4_n355Tecnologia_TipoTecnologia
               }
               , new Object[] {
               BC000O5_A127Sistema_Codigo, BC000O5_A416Sistema_Nome, BC000O5_A128Sistema_Descricao, BC000O5_n128Sistema_Descricao, BC000O5_A129Sistema_Sigla, BC000O5_A513Sistema_Coordenacao, BC000O5_n513Sistema_Coordenacao, BC000O5_A699Sistema_Tipo, BC000O5_n699Sistema_Tipo, BC000O5_A700Sistema_Tecnica,
               BC000O5_n700Sistema_Tecnica, BC000O5_A686Sistema_FatorAjuste, BC000O5_n686Sistema_FatorAjuste, BC000O5_A687Sistema_Custo, BC000O5_n687Sistema_Custo, BC000O5_A688Sistema_Prazo, BC000O5_n688Sistema_Prazo, BC000O5_A689Sistema_Esforco, BC000O5_n689Sistema_Esforco, BC000O5_A1401Sistema_ImpData,
               BC000O5_n1401Sistema_ImpData, BC000O5_A1831Sistema_Responsavel, BC000O5_n1831Sistema_Responsavel, BC000O5_A130Sistema_Ativo, BC000O5_A2109Sistema_Repositorio, BC000O5_n2109Sistema_Repositorio, BC000O5_A2130Sistema_Ultimo, BC000O5_A137Metodologia_Codigo, BC000O5_n137Metodologia_Codigo, BC000O5_A351AmbienteTecnologico_Codigo,
               BC000O5_n351AmbienteTecnologico_Codigo, BC000O5_A1859SistemaVersao_Codigo, BC000O5_n1859SistemaVersao_Codigo, BC000O5_A135Sistema_AreaTrabalhoCod, BC000O5_A1399Sistema_ImpUserCod, BC000O5_n1399Sistema_ImpUserCod, BC000O5_A2161Sistema_GpoObjCtrlCod, BC000O5_n2161Sistema_GpoObjCtrlCod
               }
               , new Object[] {
               BC000O6_A127Sistema_Codigo, BC000O6_A416Sistema_Nome, BC000O6_A128Sistema_Descricao, BC000O6_n128Sistema_Descricao, BC000O6_A129Sistema_Sigla, BC000O6_A513Sistema_Coordenacao, BC000O6_n513Sistema_Coordenacao, BC000O6_A699Sistema_Tipo, BC000O6_n699Sistema_Tipo, BC000O6_A700Sistema_Tecnica,
               BC000O6_n700Sistema_Tecnica, BC000O6_A686Sistema_FatorAjuste, BC000O6_n686Sistema_FatorAjuste, BC000O6_A687Sistema_Custo, BC000O6_n687Sistema_Custo, BC000O6_A688Sistema_Prazo, BC000O6_n688Sistema_Prazo, BC000O6_A689Sistema_Esforco, BC000O6_n689Sistema_Esforco, BC000O6_A1401Sistema_ImpData,
               BC000O6_n1401Sistema_ImpData, BC000O6_A1831Sistema_Responsavel, BC000O6_n1831Sistema_Responsavel, BC000O6_A130Sistema_Ativo, BC000O6_A2109Sistema_Repositorio, BC000O6_n2109Sistema_Repositorio, BC000O6_A2130Sistema_Ultimo, BC000O6_A137Metodologia_Codigo, BC000O6_n137Metodologia_Codigo, BC000O6_A351AmbienteTecnologico_Codigo,
               BC000O6_n351AmbienteTecnologico_Codigo, BC000O6_A1859SistemaVersao_Codigo, BC000O6_n1859SistemaVersao_Codigo, BC000O6_A135Sistema_AreaTrabalhoCod, BC000O6_A1399Sistema_ImpUserCod, BC000O6_n1399Sistema_ImpUserCod, BC000O6_A2161Sistema_GpoObjCtrlCod, BC000O6_n2161Sistema_GpoObjCtrlCod
               }
               , new Object[] {
               BC000O7_A138Metodologia_Descricao
               }
               , new Object[] {
               BC000O8_A352AmbienteTecnologico_Descricao
               }
               , new Object[] {
               BC000O9_A1860SistemaVersao_Id
               }
               , new Object[] {
               BC000O10_A136Sistema_AreaTrabalhoDes, BC000O10_n136Sistema_AreaTrabalhoDes
               }
               , new Object[] {
               BC000O11_A1402Sistema_ImpUserPesCod, BC000O11_n1402Sistema_ImpUserPesCod
               }
               , new Object[] {
               BC000O12_A2161Sistema_GpoObjCtrlCod
               }
               , new Object[] {
               BC000O13_A1403Sistema_ImpUserPesNom, BC000O13_n1403Sistema_ImpUserPesNom
               }
               , new Object[] {
               BC000O14_A127Sistema_Codigo, BC000O14_A416Sistema_Nome, BC000O14_A128Sistema_Descricao, BC000O14_n128Sistema_Descricao, BC000O14_A129Sistema_Sigla, BC000O14_A513Sistema_Coordenacao, BC000O14_n513Sistema_Coordenacao, BC000O14_A136Sistema_AreaTrabalhoDes, BC000O14_n136Sistema_AreaTrabalhoDes, BC000O14_A352AmbienteTecnologico_Descricao,
               BC000O14_A138Metodologia_Descricao, BC000O14_A699Sistema_Tipo, BC000O14_n699Sistema_Tipo, BC000O14_A700Sistema_Tecnica, BC000O14_n700Sistema_Tecnica, BC000O14_A686Sistema_FatorAjuste, BC000O14_n686Sistema_FatorAjuste, BC000O14_A687Sistema_Custo, BC000O14_n687Sistema_Custo, BC000O14_A688Sistema_Prazo,
               BC000O14_n688Sistema_Prazo, BC000O14_A689Sistema_Esforco, BC000O14_n689Sistema_Esforco, BC000O14_A1401Sistema_ImpData, BC000O14_n1401Sistema_ImpData, BC000O14_A1403Sistema_ImpUserPesNom, BC000O14_n1403Sistema_ImpUserPesNom, BC000O14_A1831Sistema_Responsavel, BC000O14_n1831Sistema_Responsavel, BC000O14_A1860SistemaVersao_Id,
               BC000O14_A130Sistema_Ativo, BC000O14_A2109Sistema_Repositorio, BC000O14_n2109Sistema_Repositorio, BC000O14_A2130Sistema_Ultimo, BC000O14_A137Metodologia_Codigo, BC000O14_n137Metodologia_Codigo, BC000O14_A351AmbienteTecnologico_Codigo, BC000O14_n351AmbienteTecnologico_Codigo, BC000O14_A1859SistemaVersao_Codigo, BC000O14_n1859SistemaVersao_Codigo,
               BC000O14_A135Sistema_AreaTrabalhoCod, BC000O14_A1399Sistema_ImpUserCod, BC000O14_n1399Sistema_ImpUserCod, BC000O14_A2161Sistema_GpoObjCtrlCod, BC000O14_n2161Sistema_GpoObjCtrlCod, BC000O14_A1402Sistema_ImpUserPesCod, BC000O14_n1402Sistema_ImpUserPesCod
               }
               , new Object[] {
               BC000O15_A127Sistema_Codigo
               }
               , new Object[] {
               BC000O16_A127Sistema_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC000O19_A136Sistema_AreaTrabalhoDes, BC000O19_n136Sistema_AreaTrabalhoDes
               }
               , new Object[] {
               BC000O20_A352AmbienteTecnologico_Descricao
               }
               , new Object[] {
               BC000O21_A138Metodologia_Descricao
               }
               , new Object[] {
               BC000O22_A1402Sistema_ImpUserPesCod, BC000O22_n1402Sistema_ImpUserPesCod
               }
               , new Object[] {
               BC000O23_A1403Sistema_ImpUserPesNom, BC000O23_n1403Sistema_ImpUserPesNom
               }
               , new Object[] {
               BC000O24_A1860SistemaVersao_Id
               }
               , new Object[] {
               BC000O25_A648Projeto_Codigo
               }
               , new Object[] {
               BC000O26_A1859SistemaVersao_Codigo
               }
               , new Object[] {
               BC000O27_A1725ContratoSistemas_CntCod, BC000O27_A1726ContratoSistemas_SistemaCod
               }
               , new Object[] {
               BC000O28_A160ContratoServicos_Codigo, BC000O28_A1067ContratoServicosSistemas_ServicoCod, BC000O28_A1063ContratoServicosSistemas_SistemaCod
               }
               , new Object[] {
               BC000O29_A996SolicitacaoMudanca_Codigo
               }
               , new Object[] {
               BC000O30_A192Contagem_Codigo
               }
               , new Object[] {
               BC000O31_A736ProjetoMelhoria_Codigo
               }
               , new Object[] {
               BC000O32_A456ContagemResultado_Codigo
               }
               , new Object[] {
               BC000O33_A368FuncaoDados_Codigo
               }
               , new Object[] {
               BC000O34_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               BC000O35_A172Tabela_Codigo
               }
               , new Object[] {
               BC000O36_A439Solicitacoes_Codigo
               }
               , new Object[] {
               BC000O37_A161FuncaoUsuario_Codigo
               }
               , new Object[] {
               BC000O38_A146Modulo_Codigo
               }
               , new Object[] {
               BC000O39_A63ContratanteUsuario_ContratanteCod, BC000O39_A60ContratanteUsuario_UsuarioCod, BC000O39_A127Sistema_Codigo
               }
               , new Object[] {
               BC000O40_A127Sistema_Codigo, BC000O40_A1460SistemaDePara_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               BC000O42_A127Sistema_Codigo, BC000O42_A416Sistema_Nome, BC000O42_A128Sistema_Descricao, BC000O42_n128Sistema_Descricao, BC000O42_A129Sistema_Sigla, BC000O42_A513Sistema_Coordenacao, BC000O42_n513Sistema_Coordenacao, BC000O42_A136Sistema_AreaTrabalhoDes, BC000O42_n136Sistema_AreaTrabalhoDes, BC000O42_A352AmbienteTecnologico_Descricao,
               BC000O42_A138Metodologia_Descricao, BC000O42_A699Sistema_Tipo, BC000O42_n699Sistema_Tipo, BC000O42_A700Sistema_Tecnica, BC000O42_n700Sistema_Tecnica, BC000O42_A686Sistema_FatorAjuste, BC000O42_n686Sistema_FatorAjuste, BC000O42_A687Sistema_Custo, BC000O42_n687Sistema_Custo, BC000O42_A688Sistema_Prazo,
               BC000O42_n688Sistema_Prazo, BC000O42_A689Sistema_Esforco, BC000O42_n689Sistema_Esforco, BC000O42_A1401Sistema_ImpData, BC000O42_n1401Sistema_ImpData, BC000O42_A1403Sistema_ImpUserPesNom, BC000O42_n1403Sistema_ImpUserPesNom, BC000O42_A1831Sistema_Responsavel, BC000O42_n1831Sistema_Responsavel, BC000O42_A1860SistemaVersao_Id,
               BC000O42_A130Sistema_Ativo, BC000O42_A2109Sistema_Repositorio, BC000O42_n2109Sistema_Repositorio, BC000O42_A2130Sistema_Ultimo, BC000O42_A137Metodologia_Codigo, BC000O42_n137Metodologia_Codigo, BC000O42_A351AmbienteTecnologico_Codigo, BC000O42_n351AmbienteTecnologico_Codigo, BC000O42_A1859SistemaVersao_Codigo, BC000O42_n1859SistemaVersao_Codigo,
               BC000O42_A135Sistema_AreaTrabalhoCod, BC000O42_A1399Sistema_ImpUserCod, BC000O42_n1399Sistema_ImpUserCod, BC000O42_A2161Sistema_GpoObjCtrlCod, BC000O42_n2161Sistema_GpoObjCtrlCod, BC000O42_A1402Sistema_ImpUserPesCod, BC000O42_n1402Sistema_ImpUserPesCod
               }
               , new Object[] {
               BC000O43_A127Sistema_Codigo, BC000O43_A2128SistemaTecnologiaLinha, BC000O43_A132Tecnologia_Nome, BC000O43_A355Tecnologia_TipoTecnologia, BC000O43_n355Tecnologia_TipoTecnologia, BC000O43_A131Tecnologia_Codigo
               }
               , new Object[] {
               BC000O44_A127Sistema_Codigo, BC000O44_A2128SistemaTecnologiaLinha
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC000O48_A132Tecnologia_Nome, BC000O48_A355Tecnologia_TipoTecnologia, BC000O48_n355Tecnologia_TipoTecnologia
               }
               , new Object[] {
               BC000O49_A127Sistema_Codigo, BC000O49_A2128SistemaTecnologiaLinha, BC000O49_A132Tecnologia_Nome, BC000O49_A355Tecnologia_TipoTecnologia, BC000O49_n355Tecnologia_TipoTecnologia, BC000O49_A131Tecnologia_Codigo
               }
            }
         );
         Z130Sistema_Ativo = true;
         A130Sistema_Ativo = true;
         i130Sistema_Ativo = true;
         Z699Sistema_Tipo = "A";
         n699Sistema_Tipo = false;
         A699Sistema_Tipo = "A";
         n699Sistema_Tipo = false;
         i699Sistema_Tipo = "A";
         n699Sistema_Tipo = false;
         AV60Pgmname = "Sistema_BC";
         Z686Sistema_FatorAjuste = new prc_fapadrao(context).executeUdp( );
         n686Sistema_FatorAjuste = false;
         A686Sistema_FatorAjuste = new prc_fapadrao(context).executeUdp( );
         n686Sistema_FatorAjuste = false;
         i686Sistema_FatorAjuste = new prc_fapadrao(context).executeUdp( );
         n686Sistema_FatorAjuste = false;
         Z135Sistema_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         A135Sistema_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         i135Sistema_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E120O2 */
         E120O2 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short s2130Sistema_Ultimo ;
      private short O2130Sistema_Ultimo ;
      private short A2130Sistema_Ultimo ;
      private short nGXsfl_233_idx=1 ;
      private short nIsMod_233 ;
      private short RcdFound233 ;
      private short GX_JID ;
      private short Z688Sistema_Prazo ;
      private short A688Sistema_Prazo ;
      private short Z689Sistema_Esforco ;
      private short A689Sistema_Esforco ;
      private short Z2130Sistema_Ultimo ;
      private short Z2128SistemaTecnologiaLinha ;
      private short A2128SistemaTecnologiaLinha ;
      private short Gx_BScreen ;
      private short RcdFound25 ;
      private short GXt_int3 ;
      private short nRcdExists_233 ;
      private short Gxremove233 ;
      private short i2130Sistema_Ultimo ;
      private int trnEnded ;
      private int Z127Sistema_Codigo ;
      private int A127Sistema_Codigo ;
      private int AV61GXV1 ;
      private int AV11Insert_Sistema_AreaTrabalhoCod ;
      private int AV16Insert_AmbienteTecnologico_Codigo ;
      private int AV13Insert_Metodologia_Codigo ;
      private int AV40Insert_Sistema_ImpUserCod ;
      private int AV58Insert_Sistema_GpoObjCtrlCod ;
      private int AV56Insert_SistemaVersao_Codigo ;
      private int AV7Sistema_Codigo ;
      private int Z1831Sistema_Responsavel ;
      private int A1831Sistema_Responsavel ;
      private int Z137Metodologia_Codigo ;
      private int A137Metodologia_Codigo ;
      private int Z351AmbienteTecnologico_Codigo ;
      private int A351AmbienteTecnologico_Codigo ;
      private int Z1859SistemaVersao_Codigo ;
      private int A1859SistemaVersao_Codigo ;
      private int Z135Sistema_AreaTrabalhoCod ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int Z1399Sistema_ImpUserCod ;
      private int A1399Sistema_ImpUserCod ;
      private int Z2161Sistema_GpoObjCtrlCod ;
      private int A2161Sistema_GpoObjCtrlCod ;
      private int Z146Modulo_Codigo ;
      private int A146Modulo_Codigo ;
      private int Z2162Sistema_GpoObjCtrlRsp ;
      private int A2162Sistema_GpoObjCtrlRsp ;
      private int Z131Tecnologia_Codigo ;
      private int A131Tecnologia_Codigo ;
      private int Z1402Sistema_ImpUserPesCod ;
      private int A1402Sistema_ImpUserPesCod ;
      private int GXt_int2 ;
      private int i135Sistema_AreaTrabalhoCod ;
      private decimal Z686Sistema_FatorAjuste ;
      private decimal A686Sistema_FatorAjuste ;
      private decimal Z687Sistema_Custo ;
      private decimal A687Sistema_Custo ;
      private decimal Z395Sistema_PF ;
      private decimal A395Sistema_PF ;
      private decimal Z690Sistema_PFA ;
      private decimal A690Sistema_PFA ;
      private decimal GXt_decimal1 ;
      private decimal i686Sistema_FatorAjuste ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode25 ;
      private String AV60Pgmname ;
      private String Z129Sistema_Sigla ;
      private String A129Sistema_Sigla ;
      private String Z699Sistema_Tipo ;
      private String A699Sistema_Tipo ;
      private String Z700Sistema_Tecnica ;
      private String A700Sistema_Tecnica ;
      private String Z707Sistema_TipoSigla ;
      private String A707Sistema_TipoSigla ;
      private String Z132Tecnologia_Nome ;
      private String A132Tecnologia_Nome ;
      private String Z355Tecnologia_TipoTecnologia ;
      private String A355Tecnologia_TipoTecnologia ;
      private String Z1860SistemaVersao_Id ;
      private String A1860SistemaVersao_Id ;
      private String Z1403Sistema_ImpUserPesNom ;
      private String A1403Sistema_ImpUserPesNom ;
      private String sMode233 ;
      private String i699Sistema_Tipo ;
      private DateTime Z1401Sistema_ImpData ;
      private DateTime A1401Sistema_ImpData ;
      private bool Z130Sistema_Ativo ;
      private bool A130Sistema_Ativo ;
      private bool n699Sistema_Tipo ;
      private bool n686Sistema_FatorAjuste ;
      private bool n136Sistema_AreaTrabalhoDes ;
      private bool n128Sistema_Descricao ;
      private bool n513Sistema_Coordenacao ;
      private bool n700Sistema_Tecnica ;
      private bool n687Sistema_Custo ;
      private bool n688Sistema_Prazo ;
      private bool n689Sistema_Esforco ;
      private bool n1401Sistema_ImpData ;
      private bool n1403Sistema_ImpUserPesNom ;
      private bool n1831Sistema_Responsavel ;
      private bool n2109Sistema_Repositorio ;
      private bool n137Metodologia_Codigo ;
      private bool n351AmbienteTecnologico_Codigo ;
      private bool n1859SistemaVersao_Codigo ;
      private bool n1399Sistema_ImpUserCod ;
      private bool n2161Sistema_GpoObjCtrlCod ;
      private bool n1402Sistema_ImpUserPesCod ;
      private bool Gx_longc ;
      private bool n355Tecnologia_TipoTecnologia ;
      private bool i130Sistema_Ativo ;
      private String Z128Sistema_Descricao ;
      private String A128Sistema_Descricao ;
      private String Z416Sistema_Nome ;
      private String A416Sistema_Nome ;
      private String Z513Sistema_Coordenacao ;
      private String A513Sistema_Coordenacao ;
      private String Z2109Sistema_Repositorio ;
      private String A2109Sistema_Repositorio ;
      private String Z138Metodologia_Descricao ;
      private String A138Metodologia_Descricao ;
      private String Z352AmbienteTecnologico_Descricao ;
      private String A352AmbienteTecnologico_Descricao ;
      private String Z136Sistema_AreaTrabalhoDes ;
      private String A136Sistema_AreaTrabalhoDes ;
      private IGxSession AV10WebSession ;
      private SdtSistema bcSistema ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] BC000O10_A136Sistema_AreaTrabalhoDes ;
      private bool[] BC000O10_n136Sistema_AreaTrabalhoDes ;
      private int[] BC000O14_A127Sistema_Codigo ;
      private String[] BC000O14_A416Sistema_Nome ;
      private String[] BC000O14_A128Sistema_Descricao ;
      private bool[] BC000O14_n128Sistema_Descricao ;
      private String[] BC000O14_A129Sistema_Sigla ;
      private String[] BC000O14_A513Sistema_Coordenacao ;
      private bool[] BC000O14_n513Sistema_Coordenacao ;
      private String[] BC000O14_A136Sistema_AreaTrabalhoDes ;
      private bool[] BC000O14_n136Sistema_AreaTrabalhoDes ;
      private String[] BC000O14_A352AmbienteTecnologico_Descricao ;
      private String[] BC000O14_A138Metodologia_Descricao ;
      private String[] BC000O14_A699Sistema_Tipo ;
      private bool[] BC000O14_n699Sistema_Tipo ;
      private String[] BC000O14_A700Sistema_Tecnica ;
      private bool[] BC000O14_n700Sistema_Tecnica ;
      private decimal[] BC000O14_A686Sistema_FatorAjuste ;
      private bool[] BC000O14_n686Sistema_FatorAjuste ;
      private decimal[] BC000O14_A687Sistema_Custo ;
      private bool[] BC000O14_n687Sistema_Custo ;
      private short[] BC000O14_A688Sistema_Prazo ;
      private bool[] BC000O14_n688Sistema_Prazo ;
      private short[] BC000O14_A689Sistema_Esforco ;
      private bool[] BC000O14_n689Sistema_Esforco ;
      private DateTime[] BC000O14_A1401Sistema_ImpData ;
      private bool[] BC000O14_n1401Sistema_ImpData ;
      private String[] BC000O14_A1403Sistema_ImpUserPesNom ;
      private bool[] BC000O14_n1403Sistema_ImpUserPesNom ;
      private int[] BC000O14_A1831Sistema_Responsavel ;
      private bool[] BC000O14_n1831Sistema_Responsavel ;
      private String[] BC000O14_A1860SistemaVersao_Id ;
      private bool[] BC000O14_A130Sistema_Ativo ;
      private String[] BC000O14_A2109Sistema_Repositorio ;
      private bool[] BC000O14_n2109Sistema_Repositorio ;
      private short[] BC000O14_A2130Sistema_Ultimo ;
      private int[] BC000O14_A137Metodologia_Codigo ;
      private bool[] BC000O14_n137Metodologia_Codigo ;
      private int[] BC000O14_A351AmbienteTecnologico_Codigo ;
      private bool[] BC000O14_n351AmbienteTecnologico_Codigo ;
      private int[] BC000O14_A1859SistemaVersao_Codigo ;
      private bool[] BC000O14_n1859SistemaVersao_Codigo ;
      private int[] BC000O14_A135Sistema_AreaTrabalhoCod ;
      private int[] BC000O14_A1399Sistema_ImpUserCod ;
      private bool[] BC000O14_n1399Sistema_ImpUserCod ;
      private int[] BC000O14_A2161Sistema_GpoObjCtrlCod ;
      private bool[] BC000O14_n2161Sistema_GpoObjCtrlCod ;
      private int[] BC000O14_A1402Sistema_ImpUserPesCod ;
      private bool[] BC000O14_n1402Sistema_ImpUserPesCod ;
      private String[] BC000O8_A352AmbienteTecnologico_Descricao ;
      private String[] BC000O7_A138Metodologia_Descricao ;
      private int[] BC000O11_A1402Sistema_ImpUserPesCod ;
      private bool[] BC000O11_n1402Sistema_ImpUserPesCod ;
      private String[] BC000O13_A1403Sistema_ImpUserPesNom ;
      private bool[] BC000O13_n1403Sistema_ImpUserPesNom ;
      private int[] BC000O12_A2161Sistema_GpoObjCtrlCod ;
      private bool[] BC000O12_n2161Sistema_GpoObjCtrlCod ;
      private String[] BC000O9_A1860SistemaVersao_Id ;
      private int[] BC000O15_A127Sistema_Codigo ;
      private int[] BC000O6_A127Sistema_Codigo ;
      private String[] BC000O6_A416Sistema_Nome ;
      private String[] BC000O6_A128Sistema_Descricao ;
      private bool[] BC000O6_n128Sistema_Descricao ;
      private String[] BC000O6_A129Sistema_Sigla ;
      private String[] BC000O6_A513Sistema_Coordenacao ;
      private bool[] BC000O6_n513Sistema_Coordenacao ;
      private String[] BC000O6_A699Sistema_Tipo ;
      private bool[] BC000O6_n699Sistema_Tipo ;
      private String[] BC000O6_A700Sistema_Tecnica ;
      private bool[] BC000O6_n700Sistema_Tecnica ;
      private decimal[] BC000O6_A686Sistema_FatorAjuste ;
      private bool[] BC000O6_n686Sistema_FatorAjuste ;
      private decimal[] BC000O6_A687Sistema_Custo ;
      private bool[] BC000O6_n687Sistema_Custo ;
      private short[] BC000O6_A688Sistema_Prazo ;
      private bool[] BC000O6_n688Sistema_Prazo ;
      private short[] BC000O6_A689Sistema_Esforco ;
      private bool[] BC000O6_n689Sistema_Esforco ;
      private DateTime[] BC000O6_A1401Sistema_ImpData ;
      private bool[] BC000O6_n1401Sistema_ImpData ;
      private int[] BC000O6_A1831Sistema_Responsavel ;
      private bool[] BC000O6_n1831Sistema_Responsavel ;
      private bool[] BC000O6_A130Sistema_Ativo ;
      private String[] BC000O6_A2109Sistema_Repositorio ;
      private bool[] BC000O6_n2109Sistema_Repositorio ;
      private short[] BC000O6_A2130Sistema_Ultimo ;
      private int[] BC000O6_A137Metodologia_Codigo ;
      private bool[] BC000O6_n137Metodologia_Codigo ;
      private int[] BC000O6_A351AmbienteTecnologico_Codigo ;
      private bool[] BC000O6_n351AmbienteTecnologico_Codigo ;
      private int[] BC000O6_A1859SistemaVersao_Codigo ;
      private bool[] BC000O6_n1859SistemaVersao_Codigo ;
      private int[] BC000O6_A135Sistema_AreaTrabalhoCod ;
      private int[] BC000O6_A1399Sistema_ImpUserCod ;
      private bool[] BC000O6_n1399Sistema_ImpUserCod ;
      private int[] BC000O6_A2161Sistema_GpoObjCtrlCod ;
      private bool[] BC000O6_n2161Sistema_GpoObjCtrlCod ;
      private int[] BC000O5_A127Sistema_Codigo ;
      private String[] BC000O5_A416Sistema_Nome ;
      private String[] BC000O5_A128Sistema_Descricao ;
      private bool[] BC000O5_n128Sistema_Descricao ;
      private String[] BC000O5_A129Sistema_Sigla ;
      private String[] BC000O5_A513Sistema_Coordenacao ;
      private bool[] BC000O5_n513Sistema_Coordenacao ;
      private String[] BC000O5_A699Sistema_Tipo ;
      private bool[] BC000O5_n699Sistema_Tipo ;
      private String[] BC000O5_A700Sistema_Tecnica ;
      private bool[] BC000O5_n700Sistema_Tecnica ;
      private decimal[] BC000O5_A686Sistema_FatorAjuste ;
      private bool[] BC000O5_n686Sistema_FatorAjuste ;
      private decimal[] BC000O5_A687Sistema_Custo ;
      private bool[] BC000O5_n687Sistema_Custo ;
      private short[] BC000O5_A688Sistema_Prazo ;
      private bool[] BC000O5_n688Sistema_Prazo ;
      private short[] BC000O5_A689Sistema_Esforco ;
      private bool[] BC000O5_n689Sistema_Esforco ;
      private DateTime[] BC000O5_A1401Sistema_ImpData ;
      private bool[] BC000O5_n1401Sistema_ImpData ;
      private int[] BC000O5_A1831Sistema_Responsavel ;
      private bool[] BC000O5_n1831Sistema_Responsavel ;
      private bool[] BC000O5_A130Sistema_Ativo ;
      private String[] BC000O5_A2109Sistema_Repositorio ;
      private bool[] BC000O5_n2109Sistema_Repositorio ;
      private short[] BC000O5_A2130Sistema_Ultimo ;
      private int[] BC000O5_A137Metodologia_Codigo ;
      private bool[] BC000O5_n137Metodologia_Codigo ;
      private int[] BC000O5_A351AmbienteTecnologico_Codigo ;
      private bool[] BC000O5_n351AmbienteTecnologico_Codigo ;
      private int[] BC000O5_A1859SistemaVersao_Codigo ;
      private bool[] BC000O5_n1859SistemaVersao_Codigo ;
      private int[] BC000O5_A135Sistema_AreaTrabalhoCod ;
      private int[] BC000O5_A1399Sistema_ImpUserCod ;
      private bool[] BC000O5_n1399Sistema_ImpUserCod ;
      private int[] BC000O5_A2161Sistema_GpoObjCtrlCod ;
      private bool[] BC000O5_n2161Sistema_GpoObjCtrlCod ;
      private int[] BC000O16_A127Sistema_Codigo ;
      private String[] BC000O19_A136Sistema_AreaTrabalhoDes ;
      private bool[] BC000O19_n136Sistema_AreaTrabalhoDes ;
      private String[] BC000O20_A352AmbienteTecnologico_Descricao ;
      private String[] BC000O21_A138Metodologia_Descricao ;
      private int[] BC000O22_A1402Sistema_ImpUserPesCod ;
      private bool[] BC000O22_n1402Sistema_ImpUserPesCod ;
      private String[] BC000O23_A1403Sistema_ImpUserPesNom ;
      private bool[] BC000O23_n1403Sistema_ImpUserPesNom ;
      private String[] BC000O24_A1860SistemaVersao_Id ;
      private int[] BC000O25_A648Projeto_Codigo ;
      private int[] BC000O26_A1859SistemaVersao_Codigo ;
      private bool[] BC000O26_n1859SistemaVersao_Codigo ;
      private int[] BC000O27_A1725ContratoSistemas_CntCod ;
      private int[] BC000O27_A1726ContratoSistemas_SistemaCod ;
      private int[] BC000O28_A160ContratoServicos_Codigo ;
      private int[] BC000O28_A1067ContratoServicosSistemas_ServicoCod ;
      private int[] BC000O28_A1063ContratoServicosSistemas_SistemaCod ;
      private int[] BC000O29_A996SolicitacaoMudanca_Codigo ;
      private int[] BC000O30_A192Contagem_Codigo ;
      private int[] BC000O31_A736ProjetoMelhoria_Codigo ;
      private int[] BC000O32_A456ContagemResultado_Codigo ;
      private int[] BC000O33_A368FuncaoDados_Codigo ;
      private int[] BC000O34_A165FuncaoAPF_Codigo ;
      private int[] BC000O35_A172Tabela_Codigo ;
      private int[] BC000O36_A439Solicitacoes_Codigo ;
      private int[] BC000O37_A161FuncaoUsuario_Codigo ;
      private int[] BC000O38_A146Modulo_Codigo ;
      private int[] BC000O39_A63ContratanteUsuario_ContratanteCod ;
      private int[] BC000O39_A60ContratanteUsuario_UsuarioCod ;
      private int[] BC000O39_A127Sistema_Codigo ;
      private int[] BC000O40_A127Sistema_Codigo ;
      private int[] BC000O40_A1460SistemaDePara_Codigo ;
      private int[] BC000O42_A127Sistema_Codigo ;
      private String[] BC000O42_A416Sistema_Nome ;
      private String[] BC000O42_A128Sistema_Descricao ;
      private bool[] BC000O42_n128Sistema_Descricao ;
      private String[] BC000O42_A129Sistema_Sigla ;
      private String[] BC000O42_A513Sistema_Coordenacao ;
      private bool[] BC000O42_n513Sistema_Coordenacao ;
      private String[] BC000O42_A136Sistema_AreaTrabalhoDes ;
      private bool[] BC000O42_n136Sistema_AreaTrabalhoDes ;
      private String[] BC000O42_A352AmbienteTecnologico_Descricao ;
      private String[] BC000O42_A138Metodologia_Descricao ;
      private String[] BC000O42_A699Sistema_Tipo ;
      private bool[] BC000O42_n699Sistema_Tipo ;
      private String[] BC000O42_A700Sistema_Tecnica ;
      private bool[] BC000O42_n700Sistema_Tecnica ;
      private decimal[] BC000O42_A686Sistema_FatorAjuste ;
      private bool[] BC000O42_n686Sistema_FatorAjuste ;
      private decimal[] BC000O42_A687Sistema_Custo ;
      private bool[] BC000O42_n687Sistema_Custo ;
      private short[] BC000O42_A688Sistema_Prazo ;
      private bool[] BC000O42_n688Sistema_Prazo ;
      private short[] BC000O42_A689Sistema_Esforco ;
      private bool[] BC000O42_n689Sistema_Esforco ;
      private DateTime[] BC000O42_A1401Sistema_ImpData ;
      private bool[] BC000O42_n1401Sistema_ImpData ;
      private String[] BC000O42_A1403Sistema_ImpUserPesNom ;
      private bool[] BC000O42_n1403Sistema_ImpUserPesNom ;
      private int[] BC000O42_A1831Sistema_Responsavel ;
      private bool[] BC000O42_n1831Sistema_Responsavel ;
      private String[] BC000O42_A1860SistemaVersao_Id ;
      private bool[] BC000O42_A130Sistema_Ativo ;
      private String[] BC000O42_A2109Sistema_Repositorio ;
      private bool[] BC000O42_n2109Sistema_Repositorio ;
      private short[] BC000O42_A2130Sistema_Ultimo ;
      private int[] BC000O42_A137Metodologia_Codigo ;
      private bool[] BC000O42_n137Metodologia_Codigo ;
      private int[] BC000O42_A351AmbienteTecnologico_Codigo ;
      private bool[] BC000O42_n351AmbienteTecnologico_Codigo ;
      private int[] BC000O42_A1859SistemaVersao_Codigo ;
      private bool[] BC000O42_n1859SistemaVersao_Codigo ;
      private int[] BC000O42_A135Sistema_AreaTrabalhoCod ;
      private int[] BC000O42_A1399Sistema_ImpUserCod ;
      private bool[] BC000O42_n1399Sistema_ImpUserCod ;
      private int[] BC000O42_A2161Sistema_GpoObjCtrlCod ;
      private bool[] BC000O42_n2161Sistema_GpoObjCtrlCod ;
      private int[] BC000O42_A1402Sistema_ImpUserPesCod ;
      private bool[] BC000O42_n1402Sistema_ImpUserPesCod ;
      private int[] BC000O43_A127Sistema_Codigo ;
      private short[] BC000O43_A2128SistemaTecnologiaLinha ;
      private String[] BC000O43_A132Tecnologia_Nome ;
      private String[] BC000O43_A355Tecnologia_TipoTecnologia ;
      private bool[] BC000O43_n355Tecnologia_TipoTecnologia ;
      private int[] BC000O43_A131Tecnologia_Codigo ;
      private String[] BC000O4_A132Tecnologia_Nome ;
      private String[] BC000O4_A355Tecnologia_TipoTecnologia ;
      private bool[] BC000O4_n355Tecnologia_TipoTecnologia ;
      private int[] BC000O44_A127Sistema_Codigo ;
      private short[] BC000O44_A2128SistemaTecnologiaLinha ;
      private int[] BC000O3_A127Sistema_Codigo ;
      private short[] BC000O3_A2128SistemaTecnologiaLinha ;
      private int[] BC000O3_A131Tecnologia_Codigo ;
      private int[] BC000O2_A127Sistema_Codigo ;
      private short[] BC000O2_A2128SistemaTecnologiaLinha ;
      private int[] BC000O2_A131Tecnologia_Codigo ;
      private String[] BC000O48_A132Tecnologia_Nome ;
      private String[] BC000O48_A355Tecnologia_TipoTecnologia ;
      private bool[] BC000O48_n355Tecnologia_TipoTecnologia ;
      private int[] BC000O49_A127Sistema_Codigo ;
      private short[] BC000O49_A2128SistemaTecnologiaLinha ;
      private String[] BC000O49_A132Tecnologia_Nome ;
      private String[] BC000O49_A355Tecnologia_TipoTecnologia ;
      private bool[] BC000O49_n355Tecnologia_TipoTecnologia ;
      private int[] BC000O49_A131Tecnologia_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtAuditingObject AV54AuditingObject ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV14TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class sistema_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new UpdateCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
         ,new ForEachCursor(def[32])
         ,new ForEachCursor(def[33])
         ,new ForEachCursor(def[34])
         ,new ForEachCursor(def[35])
         ,new ForEachCursor(def[36])
         ,new ForEachCursor(def[37])
         ,new ForEachCursor(def[38])
         ,new UpdateCursor(def[39])
         ,new ForEachCursor(def[40])
         ,new ForEachCursor(def[41])
         ,new ForEachCursor(def[42])
         ,new UpdateCursor(def[43])
         ,new UpdateCursor(def[44])
         ,new UpdateCursor(def[45])
         ,new ForEachCursor(def[46])
         ,new ForEachCursor(def[47])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC000O14 ;
          prmBC000O14 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O10 ;
          prmBC000O10 = new Object[] {
          new Object[] {"@Sistema_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O8 ;
          prmBC000O8 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O7 ;
          prmBC000O7 = new Object[] {
          new Object[] {"@Metodologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O11 ;
          prmBC000O11 = new Object[] {
          new Object[] {"@Sistema_ImpUserCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O13 ;
          prmBC000O13 = new Object[] {
          new Object[] {"@Sistema_ImpUserPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O12 ;
          prmBC000O12 = new Object[] {
          new Object[] {"@Sistema_GpoObjCtrlCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O9 ;
          prmBC000O9 = new Object[] {
          new Object[] {"@SistemaVersao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O15 ;
          prmBC000O15 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O6 ;
          prmBC000O6 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O5 ;
          prmBC000O5 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O16 ;
          prmBC000O16 = new Object[] {
          new Object[] {"@Sistema_Nome",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Sistema_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Sistema_Sigla",SqlDbType.Char,25,0} ,
          new Object[] {"@Sistema_Coordenacao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Sistema_Tipo",SqlDbType.Char,1,0} ,
          new Object[] {"@Sistema_Tecnica",SqlDbType.Char,1,0} ,
          new Object[] {"@Sistema_FatorAjuste",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Sistema_Custo",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Sistema_Prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Sistema_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Sistema_ImpData",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Sistema_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Sistema_Repositorio",SqlDbType.VarChar,250,0} ,
          new Object[] {"@Sistema_Ultimo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Metodologia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaVersao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_ImpUserCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_GpoObjCtrlCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O17 ;
          prmBC000O17 = new Object[] {
          new Object[] {"@Sistema_Nome",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Sistema_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Sistema_Sigla",SqlDbType.Char,25,0} ,
          new Object[] {"@Sistema_Coordenacao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Sistema_Tipo",SqlDbType.Char,1,0} ,
          new Object[] {"@Sistema_Tecnica",SqlDbType.Char,1,0} ,
          new Object[] {"@Sistema_FatorAjuste",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Sistema_Custo",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Sistema_Prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Sistema_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Sistema_ImpData",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Sistema_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Sistema_Repositorio",SqlDbType.VarChar,250,0} ,
          new Object[] {"@Sistema_Ultimo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Metodologia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaVersao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_ImpUserCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_GpoObjCtrlCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O18 ;
          prmBC000O18 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O19 ;
          prmBC000O19 = new Object[] {
          new Object[] {"@Sistema_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O20 ;
          prmBC000O20 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O21 ;
          prmBC000O21 = new Object[] {
          new Object[] {"@Metodologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O22 ;
          prmBC000O22 = new Object[] {
          new Object[] {"@Sistema_ImpUserCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O23 ;
          prmBC000O23 = new Object[] {
          new Object[] {"@Sistema_ImpUserPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O24 ;
          prmBC000O24 = new Object[] {
          new Object[] {"@SistemaVersao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O25 ;
          prmBC000O25 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O26 ;
          prmBC000O26 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O27 ;
          prmBC000O27 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O28 ;
          prmBC000O28 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O29 ;
          prmBC000O29 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O30 ;
          prmBC000O30 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O31 ;
          prmBC000O31 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O32 ;
          prmBC000O32 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O33 ;
          prmBC000O33 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O34 ;
          prmBC000O34 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O35 ;
          prmBC000O35 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O36 ;
          prmBC000O36 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O37 ;
          prmBC000O37 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O38 ;
          prmBC000O38 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O39 ;
          prmBC000O39 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O40 ;
          prmBC000O40 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O41 ;
          prmBC000O41 = new Object[] {
          new Object[] {"@Sistema_Ultimo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O42 ;
          prmBC000O42 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O43 ;
          prmBC000O43 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaTecnologiaLinha",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmBC000O4 ;
          prmBC000O4 = new Object[] {
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O44 ;
          prmBC000O44 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaTecnologiaLinha",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmBC000O3 ;
          prmBC000O3 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaTecnologiaLinha",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmBC000O2 ;
          prmBC000O2 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaTecnologiaLinha",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmBC000O45 ;
          prmBC000O45 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaTecnologiaLinha",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O46 ;
          prmBC000O46 = new Object[] {
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaTecnologiaLinha",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmBC000O47 ;
          prmBC000O47 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaTecnologiaLinha",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmBC000O48 ;
          prmBC000O48 = new Object[] {
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000O49 ;
          prmBC000O49 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC000O2", "SELECT [Sistema_Codigo], [SistemaTecnologiaLinha], [Tecnologia_Codigo] FROM [SistemaTecnologia] WITH (UPDLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo AND [SistemaTecnologiaLinha] = @SistemaTecnologiaLinha ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O2,1,0,true,false )
             ,new CursorDef("BC000O3", "SELECT [Sistema_Codigo], [SistemaTecnologiaLinha], [Tecnologia_Codigo] FROM [SistemaTecnologia] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo AND [SistemaTecnologiaLinha] = @SistemaTecnologiaLinha ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O3,1,0,true,false )
             ,new CursorDef("BC000O4", "SELECT [Tecnologia_Nome], [Tecnologia_TipoTecnologia] FROM [Tecnologia] WITH (NOLOCK) WHERE [Tecnologia_Codigo] = @Tecnologia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O4,1,0,true,false )
             ,new CursorDef("BC000O5", "SELECT [Sistema_Codigo], [Sistema_Nome], [Sistema_Descricao], [Sistema_Sigla], [Sistema_Coordenacao], [Sistema_Tipo], [Sistema_Tecnica], [Sistema_FatorAjuste], [Sistema_Custo], [Sistema_Prazo], [Sistema_Esforco], [Sistema_ImpData], [Sistema_Responsavel], [Sistema_Ativo], [Sistema_Repositorio], [Sistema_Ultimo], [Metodologia_Codigo], [AmbienteTecnologico_Codigo], [SistemaVersao_Codigo], [Sistema_AreaTrabalhoCod] AS Sistema_AreaTrabalhoCod, [Sistema_ImpUserCod] AS Sistema_ImpUserCod, [Sistema_GpoObjCtrlCod] AS Sistema_GpoObjCtrlCod FROM [Sistema] WITH (UPDLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O5,1,0,true,false )
             ,new CursorDef("BC000O6", "SELECT [Sistema_Codigo], [Sistema_Nome], [Sistema_Descricao], [Sistema_Sigla], [Sistema_Coordenacao], [Sistema_Tipo], [Sistema_Tecnica], [Sistema_FatorAjuste], [Sistema_Custo], [Sistema_Prazo], [Sistema_Esforco], [Sistema_ImpData], [Sistema_Responsavel], [Sistema_Ativo], [Sistema_Repositorio], [Sistema_Ultimo], [Metodologia_Codigo], [AmbienteTecnologico_Codigo], [SistemaVersao_Codigo], [Sistema_AreaTrabalhoCod] AS Sistema_AreaTrabalhoCod, [Sistema_ImpUserCod] AS Sistema_ImpUserCod, [Sistema_GpoObjCtrlCod] AS Sistema_GpoObjCtrlCod FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O6,1,0,true,false )
             ,new CursorDef("BC000O7", "SELECT [Metodologia_Descricao] FROM [Metodologia] WITH (NOLOCK) WHERE [Metodologia_Codigo] = @Metodologia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O7,1,0,true,false )
             ,new CursorDef("BC000O8", "SELECT [AmbienteTecnologico_Descricao] FROM [AmbienteTecnologico] WITH (NOLOCK) WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O8,1,0,true,false )
             ,new CursorDef("BC000O9", "SELECT [SistemaVersao_Id] FROM [SistemaVersao] WITH (NOLOCK) WHERE [SistemaVersao_Codigo] = @SistemaVersao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O9,1,0,true,false )
             ,new CursorDef("BC000O10", "SELECT [AreaTrabalho_Descricao] AS Sistema_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Sistema_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O10,1,0,true,false )
             ,new CursorDef("BC000O11", "SELECT [Usuario_PessoaCod] AS Sistema_ImpUserPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Sistema_ImpUserCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O11,1,0,true,false )
             ,new CursorDef("BC000O12", "SELECT [GpoObjCtrl_Codigo] AS Sistema_GpoObjCtrlCod FROM [GrupoObjetoControle] WITH (NOLOCK) WHERE [GpoObjCtrl_Codigo] = @Sistema_GpoObjCtrlCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O12,1,0,true,false )
             ,new CursorDef("BC000O13", "SELECT [Pessoa_Nome] AS Sistema_ImpUserPesNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Sistema_ImpUserPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O13,1,0,true,false )
             ,new CursorDef("BC000O14", "SELECT TM1.[Sistema_Codigo], TM1.[Sistema_Nome], TM1.[Sistema_Descricao], TM1.[Sistema_Sigla], TM1.[Sistema_Coordenacao], T2.[AreaTrabalho_Descricao] AS Sistema_AreaTrabalhoDes, T3.[AmbienteTecnologico_Descricao], T4.[Metodologia_Descricao], TM1.[Sistema_Tipo], TM1.[Sistema_Tecnica], TM1.[Sistema_FatorAjuste], TM1.[Sistema_Custo], TM1.[Sistema_Prazo], TM1.[Sistema_Esforco], TM1.[Sistema_ImpData], T6.[Pessoa_Nome] AS Sistema_ImpUserPesNom, TM1.[Sistema_Responsavel], T7.[SistemaVersao_Id], TM1.[Sistema_Ativo], TM1.[Sistema_Repositorio], TM1.[Sistema_Ultimo], TM1.[Metodologia_Codigo], TM1.[AmbienteTecnologico_Codigo], TM1.[SistemaVersao_Codigo], TM1.[Sistema_AreaTrabalhoCod] AS Sistema_AreaTrabalhoCod, TM1.[Sistema_ImpUserCod] AS Sistema_ImpUserCod, TM1.[Sistema_GpoObjCtrlCod] AS Sistema_GpoObjCtrlCod, T5.[Usuario_PessoaCod] AS Sistema_ImpUserPesCod FROM (((((([Sistema] TM1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = TM1.[Sistema_AreaTrabalhoCod]) LEFT JOIN [AmbienteTecnologico] T3 WITH (NOLOCK) ON T3.[AmbienteTecnologico_Codigo] = TM1.[AmbienteTecnologico_Codigo]) LEFT JOIN [Metodologia] T4 WITH (NOLOCK) ON T4.[Metodologia_Codigo] = TM1.[Metodologia_Codigo]) LEFT JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = TM1.[Sistema_ImpUserCod]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) LEFT JOIN [SistemaVersao] T7 WITH (NOLOCK) ON T7.[SistemaVersao_Codigo] = TM1.[SistemaVersao_Codigo]) WHERE TM1.[Sistema_Codigo] = @Sistema_Codigo ORDER BY TM1.[Sistema_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O14,100,0,true,false )
             ,new CursorDef("BC000O15", "SELECT [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O15,1,0,true,false )
             ,new CursorDef("BC000O16", "INSERT INTO [Sistema]([Sistema_Nome], [Sistema_Descricao], [Sistema_Sigla], [Sistema_Coordenacao], [Sistema_Tipo], [Sistema_Tecnica], [Sistema_FatorAjuste], [Sistema_Custo], [Sistema_Prazo], [Sistema_Esforco], [Sistema_ImpData], [Sistema_Responsavel], [Sistema_Ativo], [Sistema_Repositorio], [Sistema_Ultimo], [Metodologia_Codigo], [AmbienteTecnologico_Codigo], [SistemaVersao_Codigo], [Sistema_AreaTrabalhoCod], [Sistema_ImpUserCod], [Sistema_GpoObjCtrlCod]) VALUES(@Sistema_Nome, @Sistema_Descricao, @Sistema_Sigla, @Sistema_Coordenacao, @Sistema_Tipo, @Sistema_Tecnica, @Sistema_FatorAjuste, @Sistema_Custo, @Sistema_Prazo, @Sistema_Esforco, @Sistema_ImpData, @Sistema_Responsavel, @Sistema_Ativo, @Sistema_Repositorio, @Sistema_Ultimo, @Metodologia_Codigo, @AmbienteTecnologico_Codigo, @SistemaVersao_Codigo, @Sistema_AreaTrabalhoCod, @Sistema_ImpUserCod, @Sistema_GpoObjCtrlCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC000O16)
             ,new CursorDef("BC000O17", "UPDATE [Sistema] SET [Sistema_Nome]=@Sistema_Nome, [Sistema_Descricao]=@Sistema_Descricao, [Sistema_Sigla]=@Sistema_Sigla, [Sistema_Coordenacao]=@Sistema_Coordenacao, [Sistema_Tipo]=@Sistema_Tipo, [Sistema_Tecnica]=@Sistema_Tecnica, [Sistema_FatorAjuste]=@Sistema_FatorAjuste, [Sistema_Custo]=@Sistema_Custo, [Sistema_Prazo]=@Sistema_Prazo, [Sistema_Esforco]=@Sistema_Esforco, [Sistema_ImpData]=@Sistema_ImpData, [Sistema_Responsavel]=@Sistema_Responsavel, [Sistema_Ativo]=@Sistema_Ativo, [Sistema_Repositorio]=@Sistema_Repositorio, [Sistema_Ultimo]=@Sistema_Ultimo, [Metodologia_Codigo]=@Metodologia_Codigo, [AmbienteTecnologico_Codigo]=@AmbienteTecnologico_Codigo, [SistemaVersao_Codigo]=@SistemaVersao_Codigo, [Sistema_AreaTrabalhoCod]=@Sistema_AreaTrabalhoCod, [Sistema_ImpUserCod]=@Sistema_ImpUserCod, [Sistema_GpoObjCtrlCod]=@Sistema_GpoObjCtrlCod  WHERE [Sistema_Codigo] = @Sistema_Codigo", GxErrorMask.GX_NOMASK,prmBC000O17)
             ,new CursorDef("BC000O18", "DELETE FROM [Sistema]  WHERE [Sistema_Codigo] = @Sistema_Codigo", GxErrorMask.GX_NOMASK,prmBC000O18)
             ,new CursorDef("BC000O19", "SELECT [AreaTrabalho_Descricao] AS Sistema_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Sistema_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O19,1,0,true,false )
             ,new CursorDef("BC000O20", "SELECT [AmbienteTecnologico_Descricao] FROM [AmbienteTecnologico] WITH (NOLOCK) WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O20,1,0,true,false )
             ,new CursorDef("BC000O21", "SELECT [Metodologia_Descricao] FROM [Metodologia] WITH (NOLOCK) WHERE [Metodologia_Codigo] = @Metodologia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O21,1,0,true,false )
             ,new CursorDef("BC000O22", "SELECT [Usuario_PessoaCod] AS Sistema_ImpUserPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Sistema_ImpUserCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O22,1,0,true,false )
             ,new CursorDef("BC000O23", "SELECT [Pessoa_Nome] AS Sistema_ImpUserPesNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Sistema_ImpUserPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O23,1,0,true,false )
             ,new CursorDef("BC000O24", "SELECT [SistemaVersao_Id] FROM [SistemaVersao] WITH (NOLOCK) WHERE [SistemaVersao_Codigo] = @SistemaVersao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O24,1,0,true,false )
             ,new CursorDef("BC000O25", "SELECT TOP 1 [Projeto_Codigo] FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_SistemaCodigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O25,1,0,true,true )
             ,new CursorDef("BC000O26", "SELECT TOP 1 [SistemaVersao_Codigo] FROM [SistemaVersao] WITH (NOLOCK) WHERE [SistemaVersao_SistemaCod] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O26,1,0,true,true )
             ,new CursorDef("BC000O27", "SELECT TOP 1 [ContratoSistemas_CntCod], [ContratoSistemas_SistemaCod] FROM [ContratoSistemas] WITH (NOLOCK) WHERE [ContratoSistemas_SistemaCod] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O27,1,0,true,true )
             ,new CursorDef("BC000O28", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicosSistemas_ServicoCod], [ContratoServicosSistemas_SistemaCod] FROM [ContratoServicosSistemas] WITH (NOLOCK) WHERE [ContratoServicosSistemas_SistemaCod] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O28,1,0,true,true )
             ,new CursorDef("BC000O29", "SELECT TOP 1 [SolicitacaoMudanca_Codigo] FROM [SolicitacaoMudanca] WITH (NOLOCK) WHERE [SolicitacaoMudanca_SistemaCod] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O29,1,0,true,true )
             ,new CursorDef("BC000O30", "SELECT TOP 1 [Contagem_Codigo] FROM [Contagem] WITH (NOLOCK) WHERE [Contagem_SistemaCod] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O30,1,0,true,true )
             ,new CursorDef("BC000O31", "SELECT TOP 1 [ProjetoMelhoria_Codigo] FROM [ProjetoMelhoria] WITH (NOLOCK) WHERE [ProjetoMelhoria_SistemaCod] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O31,1,0,true,true )
             ,new CursorDef("BC000O32", "SELECT TOP 1 [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_SistemaCod] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O32,1,0,true,true )
             ,new CursorDef("BC000O33", "SELECT TOP 1 [FuncaoDados_Codigo] FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_SistemaCod] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O33,1,0,true,true )
             ,new CursorDef("BC000O34", "SELECT TOP 1 [FuncaoAPF_Codigo] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_SistemaCod] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O34,1,0,true,true )
             ,new CursorDef("BC000O35", "SELECT TOP 1 [Tabela_Codigo] FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_SistemaCod] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O35,1,0,true,true )
             ,new CursorDef("BC000O36", "SELECT TOP 1 [Solicitacoes_Codigo] FROM [Solicitacoes] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O36,1,0,true,true )
             ,new CursorDef("BC000O37", "SELECT TOP 1 [FuncaoUsuario_Codigo] FROM [ModuloFuncoes] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O37,1,0,true,true )
             ,new CursorDef("BC000O38", "SELECT TOP 1 [Modulo_Codigo] FROM [Modulo] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O38,1,0,true,true )
             ,new CursorDef("BC000O39", "SELECT TOP 1 [ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod], [Sistema_Codigo] FROM [ContratanteUsuarioSistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O39,1,0,true,true )
             ,new CursorDef("BC000O40", "SELECT TOP 1 [Sistema_Codigo], [SistemaDePara_Codigo] FROM [SistemaDePara] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O40,1,0,true,true )
             ,new CursorDef("BC000O41", "UPDATE [Sistema] SET [Sistema_Ultimo]=@Sistema_Ultimo  WHERE [Sistema_Codigo] = @Sistema_Codigo", GxErrorMask.GX_NOMASK,prmBC000O41)
             ,new CursorDef("BC000O42", "SELECT TM1.[Sistema_Codigo], TM1.[Sistema_Nome], TM1.[Sistema_Descricao], TM1.[Sistema_Sigla], TM1.[Sistema_Coordenacao], T2.[AreaTrabalho_Descricao] AS Sistema_AreaTrabalhoDes, T3.[AmbienteTecnologico_Descricao], T4.[Metodologia_Descricao], TM1.[Sistema_Tipo], TM1.[Sistema_Tecnica], TM1.[Sistema_FatorAjuste], TM1.[Sistema_Custo], TM1.[Sistema_Prazo], TM1.[Sistema_Esforco], TM1.[Sistema_ImpData], T6.[Pessoa_Nome] AS Sistema_ImpUserPesNom, TM1.[Sistema_Responsavel], T7.[SistemaVersao_Id], TM1.[Sistema_Ativo], TM1.[Sistema_Repositorio], TM1.[Sistema_Ultimo], TM1.[Metodologia_Codigo], TM1.[AmbienteTecnologico_Codigo], TM1.[SistemaVersao_Codigo], TM1.[Sistema_AreaTrabalhoCod] AS Sistema_AreaTrabalhoCod, TM1.[Sistema_ImpUserCod] AS Sistema_ImpUserCod, TM1.[Sistema_GpoObjCtrlCod] AS Sistema_GpoObjCtrlCod, T5.[Usuario_PessoaCod] AS Sistema_ImpUserPesCod FROM (((((([Sistema] TM1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = TM1.[Sistema_AreaTrabalhoCod]) LEFT JOIN [AmbienteTecnologico] T3 WITH (NOLOCK) ON T3.[AmbienteTecnologico_Codigo] = TM1.[AmbienteTecnologico_Codigo]) LEFT JOIN [Metodologia] T4 WITH (NOLOCK) ON T4.[Metodologia_Codigo] = TM1.[Metodologia_Codigo]) LEFT JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = TM1.[Sistema_ImpUserCod]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) LEFT JOIN [SistemaVersao] T7 WITH (NOLOCK) ON T7.[SistemaVersao_Codigo] = TM1.[SistemaVersao_Codigo]) WHERE TM1.[Sistema_Codigo] = @Sistema_Codigo ORDER BY TM1.[Sistema_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O42,100,0,true,false )
             ,new CursorDef("BC000O43", "SELECT T1.[Sistema_Codigo], T1.[SistemaTecnologiaLinha], T2.[Tecnologia_Nome], T2.[Tecnologia_TipoTecnologia], T1.[Tecnologia_Codigo] FROM ([SistemaTecnologia] T1 WITH (NOLOCK) INNER JOIN [Tecnologia] T2 WITH (NOLOCK) ON T2.[Tecnologia_Codigo] = T1.[Tecnologia_Codigo]) WHERE T1.[Sistema_Codigo] = @Sistema_Codigo and T1.[SistemaTecnologiaLinha] = @SistemaTecnologiaLinha ORDER BY T1.[Sistema_Codigo], T1.[SistemaTecnologiaLinha]  OPTION (FAST 11)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O43,11,0,true,false )
             ,new CursorDef("BC000O44", "SELECT [Sistema_Codigo], [SistemaTecnologiaLinha] FROM [SistemaTecnologia] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo AND [SistemaTecnologiaLinha] = @SistemaTecnologiaLinha  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O44,1,0,true,false )
             ,new CursorDef("BC000O45", "INSERT INTO [SistemaTecnologia]([Sistema_Codigo], [SistemaTecnologiaLinha], [Tecnologia_Codigo]) VALUES(@Sistema_Codigo, @SistemaTecnologiaLinha, @Tecnologia_Codigo)", GxErrorMask.GX_NOMASK,prmBC000O45)
             ,new CursorDef("BC000O46", "UPDATE [SistemaTecnologia] SET [Tecnologia_Codigo]=@Tecnologia_Codigo  WHERE [Sistema_Codigo] = @Sistema_Codigo AND [SistemaTecnologiaLinha] = @SistemaTecnologiaLinha", GxErrorMask.GX_NOMASK,prmBC000O46)
             ,new CursorDef("BC000O47", "DELETE FROM [SistemaTecnologia]  WHERE [Sistema_Codigo] = @Sistema_Codigo AND [SistemaTecnologiaLinha] = @SistemaTecnologiaLinha", GxErrorMask.GX_NOMASK,prmBC000O47)
             ,new CursorDef("BC000O48", "SELECT [Tecnologia_Nome], [Tecnologia_TipoTecnologia] FROM [Tecnologia] WITH (NOLOCK) WHERE [Tecnologia_Codigo] = @Tecnologia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O48,1,0,true,false )
             ,new CursorDef("BC000O49", "SELECT T1.[Sistema_Codigo], T1.[SistemaTecnologiaLinha], T2.[Tecnologia_Nome], T2.[Tecnologia_TipoTecnologia], T1.[Tecnologia_Codigo] FROM ([SistemaTecnologia] T1 WITH (NOLOCK) INNER JOIN [Tecnologia] T2 WITH (NOLOCK) ON T2.[Tecnologia_Codigo] = T1.[Tecnologia_Codigo]) WHERE T1.[Sistema_Codigo] = @Sistema_Codigo ORDER BY T1.[Sistema_Codigo], T1.[SistemaTecnologiaLinha]  OPTION (FAST 11)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000O49,11,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 25) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((decimal[]) buf[13])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((short[]) buf[15])[0] = rslt.getShort(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((short[]) buf[17])[0] = rslt.getShort(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[19])[0] = rslt.getGXDateTime(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((int[]) buf[21])[0] = rslt.getInt(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((bool[]) buf[23])[0] = rslt.getBool(14) ;
                ((String[]) buf[24])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((short[]) buf[26])[0] = rslt.getShort(16) ;
                ((int[]) buf[27])[0] = rslt.getInt(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((int[]) buf[29])[0] = rslt.getInt(18) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((int[]) buf[31])[0] = rslt.getInt(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((int[]) buf[33])[0] = rslt.getInt(20) ;
                ((int[]) buf[34])[0] = rslt.getInt(21) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(21);
                ((int[]) buf[36])[0] = rslt.getInt(22) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(22);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 25) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((decimal[]) buf[13])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((short[]) buf[15])[0] = rslt.getShort(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((short[]) buf[17])[0] = rslt.getShort(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[19])[0] = rslt.getGXDateTime(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((int[]) buf[21])[0] = rslt.getInt(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((bool[]) buf[23])[0] = rslt.getBool(14) ;
                ((String[]) buf[24])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((short[]) buf[26])[0] = rslt.getShort(16) ;
                ((int[]) buf[27])[0] = rslt.getInt(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((int[]) buf[29])[0] = rslt.getInt(18) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((int[]) buf[31])[0] = rslt.getInt(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((int[]) buf[33])[0] = rslt.getInt(20) ;
                ((int[]) buf[34])[0] = rslt.getInt(21) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(21);
                ((int[]) buf[36])[0] = rslt.getInt(22) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(22);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 25) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[11])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((decimal[]) buf[17])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((short[]) buf[19])[0] = rslt.getShort(13) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((short[]) buf[21])[0] = rslt.getShort(14) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(14);
                ((DateTime[]) buf[23])[0] = rslt.getGXDateTime(15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                ((String[]) buf[25])[0] = rslt.getString(16, 100) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(16);
                ((int[]) buf[27])[0] = rslt.getInt(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((String[]) buf[29])[0] = rslt.getString(18, 20) ;
                ((bool[]) buf[30])[0] = rslt.getBool(19) ;
                ((String[]) buf[31])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(20);
                ((short[]) buf[33])[0] = rslt.getShort(21) ;
                ((int[]) buf[34])[0] = rslt.getInt(22) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(22);
                ((int[]) buf[36])[0] = rslt.getInt(23) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(23);
                ((int[]) buf[38])[0] = rslt.getInt(24) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(24);
                ((int[]) buf[40])[0] = rslt.getInt(25) ;
                ((int[]) buf[41])[0] = rslt.getInt(26) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(26);
                ((int[]) buf[43])[0] = rslt.getInt(27) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(27);
                ((int[]) buf[45])[0] = rslt.getInt(28) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(28);
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 19 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 21 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 22 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 25 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 26 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 27 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 28 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 29 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 31 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 32 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 33 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 34 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 35 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 36 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 37 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 38 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 40 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 25) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[11])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((decimal[]) buf[17])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((short[]) buf[19])[0] = rslt.getShort(13) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((short[]) buf[21])[0] = rslt.getShort(14) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(14);
                ((DateTime[]) buf[23])[0] = rslt.getGXDateTime(15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                ((String[]) buf[25])[0] = rslt.getString(16, 100) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(16);
                ((int[]) buf[27])[0] = rslt.getInt(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((String[]) buf[29])[0] = rslt.getString(18, 20) ;
                ((bool[]) buf[30])[0] = rslt.getBool(19) ;
                ((String[]) buf[31])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(20);
                ((short[]) buf[33])[0] = rslt.getShort(21) ;
                ((int[]) buf[34])[0] = rslt.getInt(22) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(22);
                ((int[]) buf[36])[0] = rslt.getInt(23) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(23);
                ((int[]) buf[38])[0] = rslt.getInt(24) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(24);
                ((int[]) buf[40])[0] = rslt.getInt(25) ;
                ((int[]) buf[41])[0] = rslt.getInt(26) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(26);
                ((int[]) buf[43])[0] = rslt.getInt(27) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(27);
                ((int[]) buf[45])[0] = rslt.getInt(28) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(28);
                return;
             case 41 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 42 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 46 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 47 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (String)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(8, (decimal)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(9, (short)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 10 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(10, (short)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 11 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(11, (DateTime)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 12 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(12, (int)parms[21]);
                }
                stmt.SetParameter(13, (bool)parms[22]);
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 14 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[24]);
                }
                stmt.SetParameter(15, (short)parms[25]);
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 16 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(16, (int)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 17 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(17, (int)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 18 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(18, (int)parms[31]);
                }
                stmt.SetParameter(19, (int)parms[32]);
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 20 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(20, (int)parms[34]);
                }
                if ( (bool)parms[35] )
                {
                   stmt.setNull( 21 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(21, (int)parms[36]);
                }
                return;
             case 15 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (String)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(8, (decimal)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(9, (short)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 10 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(10, (short)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 11 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(11, (DateTime)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 12 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(12, (int)parms[21]);
                }
                stmt.SetParameter(13, (bool)parms[22]);
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 14 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[24]);
                }
                stmt.SetParameter(15, (short)parms[25]);
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 16 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(16, (int)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 17 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(17, (int)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 18 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(18, (int)parms[31]);
                }
                stmt.SetParameter(19, (int)parms[32]);
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 20 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(20, (int)parms[34]);
                }
                if ( (bool)parms[35] )
                {
                   stmt.setNull( 21 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(21, (int)parms[36]);
                }
                stmt.SetParameter(22, (int)parms[37]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 19 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 20 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 21 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 22 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 23 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 24 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 25 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 26 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 27 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 28 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 29 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 31 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 32 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 33 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 34 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 35 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 36 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 37 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 38 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 39 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 40 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 41 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 42 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 43 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 44 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                return;
             case 45 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 46 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 47 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
