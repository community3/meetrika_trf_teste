/*
               File: type_SdtGAMRepositoryConnectionAddressList
        Description: GAMRepositoryConnectionAddressList
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 1:32:15.64
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMRepositoryConnectionAddressList : GxUserType, IGxExternalObject
   {
      public SdtGAMRepositoryConnectionAddressList( )
      {
         initialize();
      }

      public SdtGAMRepositoryConnectionAddressList( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMRepositoryConnectionAddressList_externalReference == null )
         {
            GAMRepositoryConnectionAddressList_externalReference = new Artech.Security.GAMRepositoryConnectionAddressList(context);
         }
         returntostring = "";
         returntostring = (String)(GAMRepositoryConnectionAddressList_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Address
      {
         get {
            if ( GAMRepositoryConnectionAddressList_externalReference == null )
            {
               GAMRepositoryConnectionAddressList_externalReference = new Artech.Security.GAMRepositoryConnectionAddressList(context);
            }
            return GAMRepositoryConnectionAddressList_externalReference.Address ;
         }

         set {
            if ( GAMRepositoryConnectionAddressList_externalReference == null )
            {
               GAMRepositoryConnectionAddressList_externalReference = new Artech.Security.GAMRepositoryConnectionAddressList(context);
            }
            GAMRepositoryConnectionAddressList_externalReference.Address = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMRepositoryConnectionAddressList_externalReference == null )
            {
               GAMRepositoryConnectionAddressList_externalReference = new Artech.Security.GAMRepositoryConnectionAddressList(context);
            }
            return GAMRepositoryConnectionAddressList_externalReference ;
         }

         set {
            GAMRepositoryConnectionAddressList_externalReference = (Artech.Security.GAMRepositoryConnectionAddressList)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMRepositoryConnectionAddressList GAMRepositoryConnectionAddressList_externalReference=null ;
   }

}
