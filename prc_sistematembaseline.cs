/*
               File: PRC_SistemaTemBaseline
        Description: Sistema Tem Baseline
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:14.44
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_sistematembaseline : GXProcedure
   {
      public prc_sistematembaseline( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_sistematembaseline( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_SistemaCod ,
                           out bool aP1_Flag )
      {
         this.A489ContagemResultado_SistemaCod = aP0_ContagemResultado_SistemaCod;
         this.AV8Flag = false ;
         initialize();
         executePrivate();
         aP0_ContagemResultado_SistemaCod=this.A489ContagemResultado_SistemaCod;
         aP1_Flag=this.AV8Flag;
      }

      public bool executeUdp( ref int aP0_ContagemResultado_SistemaCod )
      {
         this.A489ContagemResultado_SistemaCod = aP0_ContagemResultado_SistemaCod;
         this.AV8Flag = false ;
         initialize();
         executePrivate();
         aP0_ContagemResultado_SistemaCod=this.A489ContagemResultado_SistemaCod;
         aP1_Flag=this.AV8Flag;
         return AV8Flag ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_SistemaCod ,
                                 out bool aP1_Flag )
      {
         prc_sistematembaseline objprc_sistematembaseline;
         objprc_sistematembaseline = new prc_sistematembaseline();
         objprc_sistematembaseline.A489ContagemResultado_SistemaCod = aP0_ContagemResultado_SistemaCod;
         objprc_sistematembaseline.AV8Flag = false ;
         objprc_sistematembaseline.context.SetSubmitInitialConfig(context);
         objprc_sistematembaseline.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_sistematembaseline);
         aP0_ContagemResultado_SistemaCod=this.A489ContagemResultado_SistemaCod;
         aP1_Flag=this.AV8Flag;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_sistematembaseline)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00592 */
         pr_default.execute(0, new Object[] {n489ContagemResultado_SistemaCod, A489ContagemResultado_SistemaCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A598ContagemResultado_Baseline = P00592_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = P00592_n598ContagemResultado_Baseline[0];
            A456ContagemResultado_Codigo = P00592_A456ContagemResultado_Codigo[0];
            AV8Flag = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00592_A489ContagemResultado_SistemaCod = new int[1] ;
         P00592_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00592_A598ContagemResultado_Baseline = new bool[] {false} ;
         P00592_n598ContagemResultado_Baseline = new bool[] {false} ;
         P00592_A456ContagemResultado_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_sistematembaseline__default(),
            new Object[][] {
                new Object[] {
               P00592_A489ContagemResultado_SistemaCod, P00592_n489ContagemResultado_SistemaCod, P00592_A598ContagemResultado_Baseline, P00592_n598ContagemResultado_Baseline, P00592_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A489ContagemResultado_SistemaCod ;
      private int A456ContagemResultado_Codigo ;
      private String scmdbuf ;
      private bool AV8Flag ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool A598ContagemResultado_Baseline ;
      private bool n598ContagemResultado_Baseline ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_SistemaCod ;
      private IDataStoreProvider pr_default ;
      private int[] P00592_A489ContagemResultado_SistemaCod ;
      private bool[] P00592_n489ContagemResultado_SistemaCod ;
      private bool[] P00592_A598ContagemResultado_Baseline ;
      private bool[] P00592_n598ContagemResultado_Baseline ;
      private int[] P00592_A456ContagemResultado_Codigo ;
      private bool aP1_Flag ;
   }

   public class prc_sistematembaseline__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00592 ;
          prmP00592 = new Object[] {
          new Object[] {"@ContagemResultado_SistemaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00592", "SELECT TOP 1 [ContagemResultado_SistemaCod], [ContagemResultado_Baseline], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE ([ContagemResultado_SistemaCod] = @ContagemResultado_SistemaCod) AND ([ContagemResultado_Baseline] = 1) ORDER BY [ContagemResultado_SistemaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00592,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
