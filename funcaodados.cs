/*
               File: FuncaoDados
        Description: Grupo L�gico de Dados
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/4/2020 8:38:54.10
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class funcaodados : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCOMBO_SISTEMACOD") == 0 )
         {
            A370FuncaoDados_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A370FuncaoDados_SistemaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLVvCOMBO_SISTEMACOD1L59( A370FuncaoDados_SistemaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel6"+"_"+"FUNCAODADOS_PF") == 0 )
         {
            A755FuncaoDados_Contar = (bool)(BooleanUtil.Val(GetNextPar( )));
            n755FuncaoDados_Contar = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A755FuncaoDados_Contar", A755FuncaoDados_Contar);
            A368FuncaoDados_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX6ASAFUNCAODADOS_PF1L59( A755FuncaoDados_Contar, A368FuncaoDados_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel7"+"_"+"FUNCAODADOS_COMPLEXIDADE") == 0 )
         {
            A368FuncaoDados_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX7ASAFUNCAODADOS_COMPLEXIDADE1L59( A368FuncaoDados_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel8"+"_"+"FUNCAODADOS_RLR") == 0 )
         {
            A368FuncaoDados_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX8ASAFUNCAODADOS_RLR1L59( A368FuncaoDados_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel9"+"_"+"FUNCAODADOS_DER") == 0 )
         {
            A368FuncaoDados_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX9ASAFUNCAODADOS_DER1L59( A368FuncaoDados_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel12"+"_"+"FUNCAODADOS_SOLUCAOTECNICA") == 0 )
         {
            A745FuncaoDados_MelhoraCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n745FuncaoDados_MelhoraCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A745FuncaoDados_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A745FuncaoDados_MelhoraCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX12ASAFUNCAODADOS_SOLUCAOTECNICA1L59( A745FuncaoDados_MelhoraCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel22"+"_"+"") == 0 )
         {
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_46") == 0 )
         {
            A370FuncaoDados_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A370FuncaoDados_SistemaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_46( A370FuncaoDados_SistemaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_47") == 0 )
         {
            A391FuncaoDados_FuncaoDadosCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n391FuncaoDados_FuncaoDadosCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A391FuncaoDados_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_47( A391FuncaoDados_FuncaoDadosCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_49") == 0 )
         {
            A404FuncaoDados_FuncaoDadosSisCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n404FuncaoDados_FuncaoDadosSisCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A404FuncaoDados_FuncaoDadosSisCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A404FuncaoDados_FuncaoDadosSisCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_49( A404FuncaoDados_FuncaoDadosSisCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_48") == 0 )
         {
            A745FuncaoDados_MelhoraCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n745FuncaoDados_MelhoraCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A745FuncaoDados_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A745FuncaoDados_MelhoraCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_48( A745FuncaoDados_MelhoraCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7FuncaoDados_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoDados_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFUNCAODADOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7FuncaoDados_Codigo), "ZZZZZ9")));
               AV13FuncaoDados_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13FuncaoDados_SistemaCod), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynavCombo_sistemacod.Name = "vCOMBO_SISTEMACOD";
         dynavCombo_sistemacod.WebTags = "";
         cmbavFuncaodados_tipo.Name = "vFUNCAODADOS_TIPO";
         cmbavFuncaodados_tipo.WebTags = "";
         cmbavFuncaodados_tipo.addItem("", "(Nenhum)", 0);
         cmbavFuncaodados_tipo.addItem("ALI", "ALI", 0);
         cmbavFuncaodados_tipo.addItem("AIE", "AIE", 0);
         cmbavFuncaodados_tipo.addItem("DC", "DC", 0);
         if ( cmbavFuncaodados_tipo.ItemCount > 0 )
         {
            AV16FuncaoDados_Tipo = cmbavFuncaodados_tipo.getValidValue(AV16FuncaoDados_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16FuncaoDados_Tipo", AV16FuncaoDados_Tipo);
         }
         cmbFuncaoDados_Complexidade.Name = "FUNCAODADOS_COMPLEXIDADE";
         cmbFuncaoDados_Complexidade.WebTags = "";
         cmbFuncaoDados_Complexidade.addItem("E", "", 0);
         cmbFuncaoDados_Complexidade.addItem("B", "Baixa", 0);
         cmbFuncaoDados_Complexidade.addItem("M", "M�dia", 0);
         cmbFuncaoDados_Complexidade.addItem("A", "Alta", 0);
         if ( cmbFuncaoDados_Complexidade.ItemCount > 0 )
         {
            A376FuncaoDados_Complexidade = cmbFuncaoDados_Complexidade.getValidValue(A376FuncaoDados_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A376FuncaoDados_Complexidade", A376FuncaoDados_Complexidade);
         }
         cmbFuncaoDados_Ativo.Name = "FUNCAODADOS_ATIVO";
         cmbFuncaoDados_Ativo.WebTags = "";
         cmbFuncaoDados_Ativo.addItem("A", "Ativa", 0);
         cmbFuncaoDados_Ativo.addItem("E", "Exclu�da", 0);
         cmbFuncaoDados_Ativo.addItem("R", "Rejeitada", 0);
         if ( cmbFuncaoDados_Ativo.ItemCount > 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A394FuncaoDados_Ativo)) )
            {
               A394FuncaoDados_Ativo = "A";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A394FuncaoDados_Ativo", A394FuncaoDados_Ativo);
            }
            A394FuncaoDados_Ativo = cmbFuncaoDados_Ativo.getValidValue(A394FuncaoDados_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A394FuncaoDados_Ativo", A394FuncaoDados_Ativo);
         }
         cmbFuncaoDados_UpdAoImpBsln.Name = "FUNCAODADOS_UPDAOIMPBSLN";
         cmbFuncaoDados_UpdAoImpBsln.WebTags = "";
         cmbFuncaoDados_UpdAoImpBsln.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         cmbFuncaoDados_UpdAoImpBsln.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         if ( cmbFuncaoDados_UpdAoImpBsln.ItemCount > 0 )
         {
            if ( (false==A1267FuncaoDados_UpdAoImpBsln) )
            {
               A1267FuncaoDados_UpdAoImpBsln = true;
               n1267FuncaoDados_UpdAoImpBsln = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1267FuncaoDados_UpdAoImpBsln", A1267FuncaoDados_UpdAoImpBsln);
            }
            A1267FuncaoDados_UpdAoImpBsln = StringUtil.StrToBool( cmbFuncaoDados_UpdAoImpBsln.getValidValue(StringUtil.BoolToStr( A1267FuncaoDados_UpdAoImpBsln)));
            n1267FuncaoDados_UpdAoImpBsln = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1267FuncaoDados_UpdAoImpBsln", A1267FuncaoDados_UpdAoImpBsln);
         }
         chkavFlag.Name = "vFLAG";
         chkavFlag.WebTags = "";
         chkavFlag.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavFlag_Internalname, "TitleCaption", chkavFlag.Caption);
         chkavFlag.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Grupo L�gico de Dados", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = cmbFuncaoDados_Ativo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public funcaodados( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public funcaodados( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_FuncaoDados_Codigo ,
                           ref int aP2_FuncaoDados_SistemaCod )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7FuncaoDados_Codigo = aP1_FuncaoDados_Codigo;
         this.AV13FuncaoDados_SistemaCod = aP2_FuncaoDados_SistemaCod;
         executePrivate();
         aP2_FuncaoDados_SistemaCod=this.AV13FuncaoDados_SistemaCod;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavCombo_sistemacod = new GXCombobox();
         cmbavFuncaodados_tipo = new GXCombobox();
         cmbFuncaoDados_Complexidade = new GXCombobox();
         cmbFuncaoDados_Ativo = new GXCombobox();
         cmbFuncaoDados_UpdAoImpBsln = new GXCombobox();
         chkavFlag = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavCombo_sistemacod.ItemCount > 0 )
         {
            AV15Combo_SistemaCod = (int)(NumberUtil.Val( dynavCombo_sistemacod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15Combo_SistemaCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Combo_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Combo_SistemaCod), 6, 0)));
         }
         if ( cmbavFuncaodados_tipo.ItemCount > 0 )
         {
            AV16FuncaoDados_Tipo = cmbavFuncaodados_tipo.getValidValue(AV16FuncaoDados_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16FuncaoDados_Tipo", AV16FuncaoDados_Tipo);
         }
         if ( cmbFuncaoDados_Complexidade.ItemCount > 0 )
         {
            A376FuncaoDados_Complexidade = cmbFuncaoDados_Complexidade.getValidValue(A376FuncaoDados_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A376FuncaoDados_Complexidade", A376FuncaoDados_Complexidade);
         }
         if ( cmbFuncaoDados_Ativo.ItemCount > 0 )
         {
            A394FuncaoDados_Ativo = cmbFuncaoDados_Ativo.getValidValue(A394FuncaoDados_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A394FuncaoDados_Ativo", A394FuncaoDados_Ativo);
         }
         if ( cmbFuncaoDados_UpdAoImpBsln.ItemCount > 0 )
         {
            A1267FuncaoDados_UpdAoImpBsln = StringUtil.StrToBool( cmbFuncaoDados_UpdAoImpBsln.getValidValue(StringUtil.BoolToStr( A1267FuncaoDados_UpdAoImpBsln)));
            n1267FuncaoDados_UpdAoImpBsln = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1267FuncaoDados_UpdAoImpBsln", A1267FuncaoDados_UpdAoImpBsln);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_1L59( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1L59e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoDados_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A368FuncaoDados_Codigo), 6, 0, ",", "")), ((edtFuncaoDados_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoDados_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtFuncaoDados_Codigo_Visible, edtFuncaoDados_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_FuncaoDados.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1L59( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_1L59( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_1L59e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_92_1L59( true) ;
         }
         return  ;
      }

      protected void wb_table3_92_1L59e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1L59e( true) ;
         }
         else
         {
            wb_table1_2_1L59e( false) ;
         }
      }

      protected void wb_table3_92_1L59( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", bttBtn_trn_enter_Caption, bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtneliminar_Internalname, "", "Eliminar", bttBtneliminar_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtneliminar_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOELIMINAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 0, "HLP_FuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmessage_Internalname, "", "", "", lblTextblockmessage_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavMessage_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26Message), 4, 0, ",", "")), ((edtavMessage_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV26Message), "ZZZ9")) : context.localUtil.Format( (decimal)(AV26Message), "ZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavMessage_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavMessage_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_92_1L59e( true) ;
         }
         else
         {
            wb_table3_92_1L59e( false) ;
         }
      }

      protected void wb_table2_5_1L59( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_1L59( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_1L59e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_1L59e( true) ;
         }
         else
         {
            wb_table2_5_1L59e( false) ;
         }
      }

      protected void wb_table4_13_1L59( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(90), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_nome_Internalname, "Nome", "", "", lblTextblockfuncaodados_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='RequiredDataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaodados_nome_Internalname, AV20FuncaoDados_Nome, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", 0, 1, edtavFuncaodados_nome_Enabled, 1, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_FuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcombo_sistemacod_Internalname, lblTextblockcombo_sistemacod_Caption, "", "", lblTextblockcombo_sistemacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockcombo_sistemacod_Visible, 1, 0, "HLP_FuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavCombo_sistemacod, dynavCombo_sistemacod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV15Combo_SistemaCod), 6, 0)), 1, dynavCombo_sistemacod_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCOMBO_SISTEMACOD.CLICK."+"'", "int", "", dynavCombo_sistemacod.Visible, dynavCombo_sistemacod.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,22);\"", "", true, "HLP_FuncaoDados.htm");
            dynavCombo_sistemacod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15Combo_SistemaCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCombo_sistemacod_Internalname, "Values", (String)(dynavCombo_sistemacod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_descricao_Internalname, "Descri��o", "", "", lblTextblockfuncaodados_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"7\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaodados_descricao_Internalname, AV17FuncaoDados_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"", 0, 1, edtavFuncaodados_descricao_Enabled, 1, 80, "chr", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "", "HLP_FuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_tipo_Internalname, "Tipo da fun��o", "", "", lblTextblockfuncaodados_tipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaodados_tipo, cmbavFuncaodados_tipo_Internalname, StringUtil.RTrim( AV16FuncaoDados_Tipo), 1, cmbavFuncaodados_tipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbavFuncaodados_tipo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,32);\"", "", true, "HLP_FuncaoDados.htm");
            cmbavFuncaodados_tipo.CurrentValue = StringUtil.RTrim( AV16FuncaoDados_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaodados_tipo_Internalname, "Values", (String)(cmbavFuncaodados_tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_der_Internalname, "DER", "", "", lblTextblockfuncaodados_der_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoDados_DER_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A374FuncaoDados_DER), 4, 0, ",", "")), ((edtFuncaoDados_DER_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A374FuncaoDados_DER), "ZZZ9")) : context.localUtil.Format( (decimal)(A374FuncaoDados_DER), "ZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoDados_DER_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtFuncaoDados_DER_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_rlr_Internalname, "RLR", "", "", lblTextblockfuncaodados_rlr_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoDados_RLR_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A375FuncaoDados_RLR), 4, 0, ",", "")), ((edtFuncaoDados_RLR_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A375FuncaoDados_RLR), "ZZZ9")) : context.localUtil.Format( (decimal)(A375FuncaoDados_RLR), "ZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoDados_RLR_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtFuncaoDados_RLR_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_complexidade_Internalname, "Complexidade", "", "", lblTextblockfuncaodados_complexidade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbFuncaoDados_Complexidade, cmbFuncaoDados_Complexidade_Internalname, StringUtil.RTrim( A376FuncaoDados_Complexidade), 1, cmbFuncaoDados_Complexidade_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbFuncaoDados_Complexidade.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_FuncaoDados.htm");
            cmbFuncaoDados_Complexidade.CurrentValue = StringUtil.RTrim( A376FuncaoDados_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_Complexidade_Internalname, "Values", (String)(cmbFuncaoDados_Complexidade.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_pf_Internalname, "PF", "", "", lblTextblockfuncaodados_pf_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoDados_PF_Internalname, StringUtil.LTrim( StringUtil.NToC( A377FuncaoDados_PF, 14, 5, ",", "")), ((edtFuncaoDados_PF_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A377FuncaoDados_PF, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A377FuncaoDados_PF, "ZZ,ZZZ,ZZ9.999")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoDados_PF_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtFuncaoDados_PF_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_FuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_observacao_Internalname, "Observa��o", "", "", lblTextblockfuncaodados_observacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"7\"  class='DataContentCell'>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"FUNCAODADOS_OBSERVACAOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_ativo_Internalname, "Status", "", "", lblTextblockfuncaodados_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockfuncaodados_ativo_Visible, 1, 0, "HLP_FuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_65_1L59( true) ;
         }
         return  ;
      }

      protected void wb_table5_65_1L59e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockflag_Internalname, "Criar tabela com mesmo nome", "", "", lblTextblockflag_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockflag_Visible, 1, 0, "HLP_FuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavFlag_Internalname, StringUtil.BoolToStr( AV21Flag), "", "", chkavFlag.Visible, chkavFlag.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(83, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,83);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_1L59e( true) ;
         }
         else
         {
            wb_table4_13_1L59e( false) ;
         }
      }

      protected void wb_table5_65_1L59( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedfuncaodados_ativo_Internalname, tblTablemergedfuncaodados_ativo_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbFuncaoDados_Ativo, cmbFuncaoDados_Ativo_Internalname, StringUtil.RTrim( A394FuncaoDados_Ativo), 1, cmbFuncaoDados_Ativo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbFuncaoDados_Ativo.Visible, cmbFuncaoDados_Ativo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,68);\"", "", true, "HLP_FuncaoDados.htm");
            cmbFuncaoDados_Ativo.CurrentValue = StringUtil.RTrim( A394FuncaoDados_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_Ativo_Internalname, "Values", (String)(cmbFuncaoDados_Ativo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_updaoimpbsln_Internalname, "Atualizar ao importar Baseline", "", "", lblTextblockfuncaodados_updaoimpbsln_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbFuncaoDados_UpdAoImpBsln, cmbFuncaoDados_UpdAoImpBsln_Internalname, StringUtil.BoolToStr( A1267FuncaoDados_UpdAoImpBsln), 1, cmbFuncaoDados_UpdAoImpBsln_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbFuncaoDados_UpdAoImpBsln.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,72);\"", "", true, "HLP_FuncaoDados.htm");
            cmbFuncaoDados_UpdAoImpBsln.CurrentValue = StringUtil.BoolToStr( A1267FuncaoDados_UpdAoImpBsln);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_UpdAoImpBsln_Internalname, "Values", (String)(cmbFuncaoDados_UpdAoImpBsln.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_65_1L59e( true) ;
         }
         else
         {
            wb_table5_65_1L59e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E111L2 */
         E111L2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               AV20FuncaoDados_Nome = cgiGet( edtavFuncaodados_nome_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20FuncaoDados_Nome", AV20FuncaoDados_Nome);
               dynavCombo_sistemacod.CurrentValue = cgiGet( dynavCombo_sistemacod_Internalname);
               AV15Combo_SistemaCod = (int)(NumberUtil.Val( cgiGet( dynavCombo_sistemacod_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Combo_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Combo_SistemaCod), 6, 0)));
               AV17FuncaoDados_Descricao = cgiGet( edtavFuncaodados_descricao_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoDados_Descricao", AV17FuncaoDados_Descricao);
               cmbavFuncaodados_tipo.CurrentValue = cgiGet( cmbavFuncaodados_tipo_Internalname);
               AV16FuncaoDados_Tipo = cgiGet( cmbavFuncaodados_tipo_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16FuncaoDados_Tipo", AV16FuncaoDados_Tipo);
               A374FuncaoDados_DER = (short)(context.localUtil.CToN( cgiGet( edtFuncaoDados_DER_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A374FuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(A374FuncaoDados_DER), 4, 0)));
               A375FuncaoDados_RLR = (short)(context.localUtil.CToN( cgiGet( edtFuncaoDados_RLR_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A375FuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(A375FuncaoDados_RLR), 4, 0)));
               cmbFuncaoDados_Complexidade.CurrentValue = cgiGet( cmbFuncaoDados_Complexidade_Internalname);
               A376FuncaoDados_Complexidade = cgiGet( cmbFuncaoDados_Complexidade_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A376FuncaoDados_Complexidade", A376FuncaoDados_Complexidade);
               A377FuncaoDados_PF = context.localUtil.CToN( cgiGet( edtFuncaoDados_PF_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A377FuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( A377FuncaoDados_PF, 14, 5)));
               cmbFuncaoDados_Ativo.CurrentValue = cgiGet( cmbFuncaoDados_Ativo_Internalname);
               A394FuncaoDados_Ativo = cgiGet( cmbFuncaoDados_Ativo_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A394FuncaoDados_Ativo", A394FuncaoDados_Ativo);
               cmbFuncaoDados_UpdAoImpBsln.CurrentValue = cgiGet( cmbFuncaoDados_UpdAoImpBsln_Internalname);
               A1267FuncaoDados_UpdAoImpBsln = StringUtil.StrToBool( cgiGet( cmbFuncaoDados_UpdAoImpBsln_Internalname));
               n1267FuncaoDados_UpdAoImpBsln = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1267FuncaoDados_UpdAoImpBsln", A1267FuncaoDados_UpdAoImpBsln);
               n1267FuncaoDados_UpdAoImpBsln = ((false==A1267FuncaoDados_UpdAoImpBsln) ? true : false);
               AV21Flag = StringUtil.StrToBool( cgiGet( chkavFlag_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Flag", AV21Flag);
               AV26Message = (short)(context.localUtil.CToN( cgiGet( edtavMessage_Internalname), ",", "."));
               A368FuncaoDados_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoDados_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
               /* Read saved values. */
               Z368FuncaoDados_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z368FuncaoDados_Codigo"), ",", "."));
               Z373FuncaoDados_Tipo = cgiGet( "Z373FuncaoDados_Tipo");
               Z369FuncaoDados_Nome = cgiGet( "Z369FuncaoDados_Nome");
               Z1024FuncaoDados_DERImp = (short)(context.localUtil.CToN( cgiGet( "Z1024FuncaoDados_DERImp"), ",", "."));
               n1024FuncaoDados_DERImp = ((0==A1024FuncaoDados_DERImp) ? true : false);
               Z1025FuncaoDados_RAImp = (short)(context.localUtil.CToN( cgiGet( "Z1025FuncaoDados_RAImp"), ",", "."));
               n1025FuncaoDados_RAImp = ((0==A1025FuncaoDados_RAImp) ? true : false);
               Z755FuncaoDados_Contar = StringUtil.StrToBool( cgiGet( "Z755FuncaoDados_Contar"));
               n755FuncaoDados_Contar = ((false==A755FuncaoDados_Contar) ? true : false);
               Z1147FuncaoDados_Tecnica = (short)(context.localUtil.CToN( cgiGet( "Z1147FuncaoDados_Tecnica"), ",", "."));
               n1147FuncaoDados_Tecnica = ((0==A1147FuncaoDados_Tecnica) ? true : false);
               Z1258FuncaoDados_Importada = context.localUtil.CToD( cgiGet( "Z1258FuncaoDados_Importada"), 0);
               n1258FuncaoDados_Importada = ((DateTime.MinValue==A1258FuncaoDados_Importada) ? true : false);
               Z1267FuncaoDados_UpdAoImpBsln = StringUtil.StrToBool( cgiGet( "Z1267FuncaoDados_UpdAoImpBsln"));
               n1267FuncaoDados_UpdAoImpBsln = ((false==A1267FuncaoDados_UpdAoImpBsln) ? true : false);
               Z394FuncaoDados_Ativo = cgiGet( "Z394FuncaoDados_Ativo");
               Z370FuncaoDados_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "Z370FuncaoDados_SistemaCod"), ",", "."));
               Z391FuncaoDados_FuncaoDadosCod = (int)(context.localUtil.CToN( cgiGet( "Z391FuncaoDados_FuncaoDadosCod"), ",", "."));
               n391FuncaoDados_FuncaoDadosCod = ((0==A391FuncaoDados_FuncaoDadosCod) ? true : false);
               Z745FuncaoDados_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( "Z745FuncaoDados_MelhoraCod"), ",", "."));
               n745FuncaoDados_MelhoraCod = ((0==A745FuncaoDados_MelhoraCod) ? true : false);
               A373FuncaoDados_Tipo = cgiGet( "Z373FuncaoDados_Tipo");
               A369FuncaoDados_Nome = cgiGet( "Z369FuncaoDados_Nome");
               A1024FuncaoDados_DERImp = (short)(context.localUtil.CToN( cgiGet( "Z1024FuncaoDados_DERImp"), ",", "."));
               n1024FuncaoDados_DERImp = false;
               n1024FuncaoDados_DERImp = ((0==A1024FuncaoDados_DERImp) ? true : false);
               A1025FuncaoDados_RAImp = (short)(context.localUtil.CToN( cgiGet( "Z1025FuncaoDados_RAImp"), ",", "."));
               n1025FuncaoDados_RAImp = false;
               n1025FuncaoDados_RAImp = ((0==A1025FuncaoDados_RAImp) ? true : false);
               A755FuncaoDados_Contar = StringUtil.StrToBool( cgiGet( "Z755FuncaoDados_Contar"));
               n755FuncaoDados_Contar = false;
               n755FuncaoDados_Contar = ((false==A755FuncaoDados_Contar) ? true : false);
               A1147FuncaoDados_Tecnica = (short)(context.localUtil.CToN( cgiGet( "Z1147FuncaoDados_Tecnica"), ",", "."));
               n1147FuncaoDados_Tecnica = false;
               n1147FuncaoDados_Tecnica = ((0==A1147FuncaoDados_Tecnica) ? true : false);
               A1258FuncaoDados_Importada = context.localUtil.CToD( cgiGet( "Z1258FuncaoDados_Importada"), 0);
               n1258FuncaoDados_Importada = false;
               n1258FuncaoDados_Importada = ((DateTime.MinValue==A1258FuncaoDados_Importada) ? true : false);
               A370FuncaoDados_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "Z370FuncaoDados_SistemaCod"), ",", "."));
               A391FuncaoDados_FuncaoDadosCod = (int)(context.localUtil.CToN( cgiGet( "Z391FuncaoDados_FuncaoDadosCod"), ",", "."));
               n391FuncaoDados_FuncaoDadosCod = false;
               n391FuncaoDados_FuncaoDadosCod = ((0==A391FuncaoDados_FuncaoDadosCod) ? true : false);
               A745FuncaoDados_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( "Z745FuncaoDados_MelhoraCod"), ",", "."));
               n745FuncaoDados_MelhoraCod = false;
               n745FuncaoDados_MelhoraCod = ((0==A745FuncaoDados_MelhoraCod) ? true : false);
               O397FuncaoDados_Descricao = cgiGet( "O397FuncaoDados_Descricao");
               n397FuncaoDados_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A397FuncaoDados_Descricao)) ? true : false);
               O369FuncaoDados_Nome = cgiGet( "O369FuncaoDados_Nome");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N370FuncaoDados_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "N370FuncaoDados_SistemaCod"), ",", "."));
               N391FuncaoDados_FuncaoDadosCod = (int)(context.localUtil.CToN( cgiGet( "N391FuncaoDados_FuncaoDadosCod"), ",", "."));
               n391FuncaoDados_FuncaoDadosCod = ((0==A391FuncaoDados_FuncaoDadosCod) ? true : false);
               N745FuncaoDados_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( "N745FuncaoDados_MelhoraCod"), ",", "."));
               n745FuncaoDados_MelhoraCod = ((0==A745FuncaoDados_MelhoraCod) ? true : false);
               A391FuncaoDados_FuncaoDadosCod = (int)(context.localUtil.CToN( cgiGet( "FUNCAODADOS_FUNCAODADOSCOD"), ",", "."));
               n391FuncaoDados_FuncaoDadosCod = ((0==A391FuncaoDados_FuncaoDadosCod) ? true : false);
               A420FuncaoDados_CodigoAliAie = (short)(context.localUtil.CToN( cgiGet( "FUNCAODADOS_CODIGOALIAIE"), ",", "."));
               A755FuncaoDados_Contar = StringUtil.StrToBool( cgiGet( "FUNCAODADOS_CONTAR"));
               n755FuncaoDados_Contar = ((false==A755FuncaoDados_Contar) ? true : false);
               A369FuncaoDados_Nome = cgiGet( "FUNCAODADOS_NOME");
               A419FuncaoDados_Nome50 = cgiGet( "FUNCAODADOS_NOME50");
               A753FuncaoDados_SolucaoTecnica = cgiGet( "FUNCAODADOS_SOLUCAOTECNICA");
               A754FuncaoDados_SolucaoTecnica30 = cgiGet( "FUNCAODADOS_SOLUCAOTECNICA30");
               A745FuncaoDados_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( "FUNCAODADOS_MELHORACOD"), ",", "."));
               n745FuncaoDados_MelhoraCod = ((0==A745FuncaoDados_MelhoraCod) ? true : false);
               AV7FuncaoDados_Codigo = (int)(context.localUtil.CToN( cgiGet( "vFUNCAODADOS_CODIGO"), ",", "."));
               AV11Insert_FuncaoDados_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_FUNCAODADOS_SISTEMACOD"), ",", "."));
               A370FuncaoDados_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "FUNCAODADOS_SISTEMACOD"), ",", "."));
               AV13FuncaoDados_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "vFUNCAODADOS_SISTEMACOD"), ",", "."));
               AV14Insert_FuncaoDados_FuncaoDadosCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_FUNCAODADOS_FUNCAODADOSCOD"), ",", "."));
               AV18FuncaoDados_FuncaoDadosCod = (int)(context.localUtil.CToN( cgiGet( "vFUNCAODADOS_FUNCAODADOSCOD"), ",", "."));
               AV23Insert_FuncaoDados_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_FUNCAODADOS_MELHORACOD"), ",", "."));
               A371FuncaoDados_SistemaDes = cgiGet( "FUNCAODADOS_SISTEMADES");
               n371FuncaoDados_SistemaDes = false;
               A705FuncaoDados_SistemaTecnica = cgiGet( "FUNCAODADOS_SISTEMATECNICA");
               n705FuncaoDados_SistemaTecnica = false;
               A404FuncaoDados_FuncaoDadosSisCod = (int)(context.localUtil.CToN( cgiGet( "FUNCAODADOS_FUNCAODADOSSISCOD"), ",", "."));
               A373FuncaoDados_Tipo = cgiGet( "FUNCAODADOS_TIPO");
               A397FuncaoDados_Descricao = cgiGet( "FUNCAODADOS_DESCRICAO");
               n397FuncaoDados_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A397FuncaoDados_Descricao)) ? true : false);
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A1024FuncaoDados_DERImp = (short)(context.localUtil.CToN( cgiGet( "FUNCAODADOS_DERIMP"), ",", "."));
               n1024FuncaoDados_DERImp = ((0==A1024FuncaoDados_DERImp) ? true : false);
               A1025FuncaoDados_RAImp = (short)(context.localUtil.CToN( cgiGet( "FUNCAODADOS_RAIMP"), ",", "."));
               n1025FuncaoDados_RAImp = ((0==A1025FuncaoDados_RAImp) ? true : false);
               A1147FuncaoDados_Tecnica = (short)(context.localUtil.CToN( cgiGet( "FUNCAODADOS_TECNICA"), ",", "."));
               n1147FuncaoDados_Tecnica = ((0==A1147FuncaoDados_Tecnica) ? true : false);
               A1245FuncaoDados_Observacao = cgiGet( "FUNCAODADOS_OBSERVACAO");
               n1245FuncaoDados_Observacao = false;
               n1245FuncaoDados_Observacao = (String.IsNullOrEmpty(StringUtil.RTrim( A1245FuncaoDados_Observacao)) ? true : false);
               A1258FuncaoDados_Importada = context.localUtil.CToD( cgiGet( "FUNCAODADOS_IMPORTADA"), 0);
               n1258FuncaoDados_Importada = ((DateTime.MinValue==A1258FuncaoDados_Importada) ? true : false);
               A372FuncaoDados_SistemaAreaCod = (int)(context.localUtil.CToN( cgiGet( "FUNCAODADOS_SISTEMAAREACOD"), ",", "."));
               n372FuncaoDados_SistemaAreaCod = false;
               A402FuncaoDados_FuncaoDadosNom = cgiGet( "FUNCAODADOS_FUNCAODADOSNOM");
               n402FuncaoDados_FuncaoDadosNom = false;
               A403FuncaoDados_FuncaoDadosDes = cgiGet( "FUNCAODADOS_FUNCAODADOSDES");
               n403FuncaoDados_FuncaoDadosDes = false;
               A405FuncaoDados_FuncaoDadosSisDes = cgiGet( "FUNCAODADOS_FUNCAODADOSSISDES");
               n405FuncaoDados_FuncaoDadosSisDes = false;
               AV28Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Funcaodados_observacao_Width = cgiGet( "FUNCAODADOS_OBSERVACAO_Width");
               Funcaodados_observacao_Height = cgiGet( "FUNCAODADOS_OBSERVACAO_Height");
               Funcaodados_observacao_Skin = cgiGet( "FUNCAODADOS_OBSERVACAO_Skin");
               Funcaodados_observacao_Toolbar = cgiGet( "FUNCAODADOS_OBSERVACAO_Toolbar");
               Funcaodados_observacao_Color = (int)(context.localUtil.CToN( cgiGet( "FUNCAODADOS_OBSERVACAO_Color"), ",", "."));
               Funcaodados_observacao_Enabled = StringUtil.StrToBool( cgiGet( "FUNCAODADOS_OBSERVACAO_Enabled"));
               Funcaodados_observacao_Class = cgiGet( "FUNCAODADOS_OBSERVACAO_Class");
               Funcaodados_observacao_Customtoolbar = cgiGet( "FUNCAODADOS_OBSERVACAO_Customtoolbar");
               Funcaodados_observacao_Customconfiguration = cgiGet( "FUNCAODADOS_OBSERVACAO_Customconfiguration");
               Funcaodados_observacao_Toolbarcancollapse = StringUtil.StrToBool( cgiGet( "FUNCAODADOS_OBSERVACAO_Toolbarcancollapse"));
               Funcaodados_observacao_Toolbarexpanded = StringUtil.StrToBool( cgiGet( "FUNCAODADOS_OBSERVACAO_Toolbarexpanded"));
               Funcaodados_observacao_Buttonpressedid = cgiGet( "FUNCAODADOS_OBSERVACAO_Buttonpressedid");
               Funcaodados_observacao_Captionvalue = cgiGet( "FUNCAODADOS_OBSERVACAO_Captionvalue");
               Funcaodados_observacao_Captionclass = cgiGet( "FUNCAODADOS_OBSERVACAO_Captionclass");
               Funcaodados_observacao_Captionposition = cgiGet( "FUNCAODADOS_OBSERVACAO_Captionposition");
               Funcaodados_observacao_Coltitle = cgiGet( "FUNCAODADOS_OBSERVACAO_Coltitle");
               Funcaodados_observacao_Coltitlefont = cgiGet( "FUNCAODADOS_OBSERVACAO_Coltitlefont");
               Funcaodados_observacao_Coltitlecolor = (int)(context.localUtil.CToN( cgiGet( "FUNCAODADOS_OBSERVACAO_Coltitlecolor"), ",", "."));
               Funcaodados_observacao_Usercontroliscolumn = StringUtil.StrToBool( cgiGet( "FUNCAODADOS_OBSERVACAO_Usercontroliscolumn"));
               Funcaodados_observacao_Visible = StringUtil.StrToBool( cgiGet( "FUNCAODADOS_OBSERVACAO_Visible"));
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "FuncaoDados";
               A368FuncaoDados_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoDados_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A745FuncaoDados_MelhoraCod), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1024FuncaoDados_DERImp), "ZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1025FuncaoDados_RAImp), "ZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A755FuncaoDados_Contar);
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1147FuncaoDados_Tecnica), "9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format(A1258FuncaoDados_Importada, "99/99/99");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A368FuncaoDados_Codigo != Z368FuncaoDados_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("funcaodados:[SecurityCheckFailed value for]"+"FuncaoDados_Codigo:"+context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("funcaodados:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("funcaodados:[SecurityCheckFailed value for]"+"FuncaoDados_MelhoraCod:"+context.localUtil.Format( (decimal)(A745FuncaoDados_MelhoraCod), "ZZZZZ9"));
                  GXUtil.WriteLog("funcaodados:[SecurityCheckFailed value for]"+"FuncaoDados_DERImp:"+context.localUtil.Format( (decimal)(A1024FuncaoDados_DERImp), "ZZZ9"));
                  GXUtil.WriteLog("funcaodados:[SecurityCheckFailed value for]"+"FuncaoDados_RAImp:"+context.localUtil.Format( (decimal)(A1025FuncaoDados_RAImp), "ZZZ9"));
                  GXUtil.WriteLog("funcaodados:[SecurityCheckFailed value for]"+"FuncaoDados_Contar:"+StringUtil.BoolToStr( A755FuncaoDados_Contar));
                  GXUtil.WriteLog("funcaodados:[SecurityCheckFailed value for]"+"FuncaoDados_Tecnica:"+context.localUtil.Format( (decimal)(A1147FuncaoDados_Tecnica), "9"));
                  GXUtil.WriteLog("funcaodados:[SecurityCheckFailed value for]"+"FuncaoDados_Importada:"+context.localUtil.Format(A1258FuncaoDados_Importada, "99/99/99"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A368FuncaoDados_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode59 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode59;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound59 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_1L0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "FUNCAODADOS_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtFuncaoDados_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E111L2 */
                           E111L2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E121L2 */
                           E121L2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOELIMINAR'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E131L2 */
                           E131L2 ();
                           nKeyPressed = 3;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VCOMBO_SISTEMACOD.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E141L2 */
                           E141L2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E121L2 */
            E121L2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1L59( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes1L59( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaodados_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_nome_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCombo_sistemacod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavCombo_sistemacod.Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaodados_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_descricao_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaodados_tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaodados_tipo.Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavFlag_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavFlag.Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMessage_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMessage_Enabled), 5, 0)));
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_1L0( )
      {
         BeforeValidate1L59( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1L59( ) ;
            }
            else
            {
               CheckExtendedTable1L59( ) ;
               CloseExtendedTableCursors1L59( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption1L0( )
      {
      }

      protected void E111L2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV28Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV29GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29GXV1), 8, 0)));
            while ( AV29GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV29GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "FuncaoDados_SistemaCod") == 0 )
               {
                  AV11Insert_FuncaoDados_SistemaCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_FuncaoDados_SistemaCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "FuncaoDados_FuncaoDadosCod") == 0 )
               {
                  AV14Insert_FuncaoDados_FuncaoDadosCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Insert_FuncaoDados_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Insert_FuncaoDados_FuncaoDadosCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "FuncaoDados_MelhoraCod") == 0 )
               {
                  AV23Insert_FuncaoDados_MelhoraCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Insert_FuncaoDados_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Insert_FuncaoDados_MelhoraCod), 6, 0)));
               }
               AV29GXV1 = (int)(AV29GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29GXV1), 8, 0)));
            }
         }
         edtFuncaoDados_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_Codigo_Visible), 5, 0)));
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script language=\"javascript\">"+StringUtil.NewLine( )+"var conteudoDiv=document.getElementById('div1').innerHTML;"+StringUtil.NewLine( )+"document.forms[0]."+edtavMessage_Internalname+".value = conteudoDiv"+StringUtil.NewLine( )+"</script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            cmbavFuncaodados_tipo.removeAllItems();
            cmbavFuncaodados_tipo.addItem("ALI", "ALI", 0);
            cmbavFuncaodados_tipo.addItem("DC", "DC", 0);
            AV16FuncaoDados_Tipo = "ALI";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16FuncaoDados_Tipo", AV16FuncaoDados_Tipo);
            lblTextblockfuncaodados_ativo_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockfuncaodados_ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockfuncaodados_ativo_Visible), 5, 0)));
            cmbFuncaoDados_Ativo.Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_Ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbFuncaoDados_Ativo.Visible), 5, 0)));
            lblTextblockcombo_sistemacod_Caption = "- Selecionar fun��o externa do Sistema:";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcombo_sistemacod_Internalname, "Caption", lblTextblockcombo_sistemacod_Caption);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            cmbavFuncaodados_tipo.removeAllItems();
            if ( (0==A391FuncaoDados_FuncaoDadosCod) )
            {
               cmbavFuncaodados_tipo.addItem("ALI", "ALI", 0);
               cmbavFuncaodados_tipo.addItem("DC", "DC", 0);
            }
            else
            {
               cmbavFuncaodados_tipo.addItem("AIE", "AIE", 0);
               cmbavFuncaodados_tipo.addItem("DC", "DC", 0);
            }
         }
         edtavFuncaodados_nome_Width = 570;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaodados_nome_Internalname, "Width", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_nome_Width), 9, 0)));
         edtavFuncaodados_nome_Height = 27;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaodados_nome_Internalname, "Height", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_nome_Height), 9, 0)));
      }

      protected void E121L2( )
      {
         /* After Trn Routine */
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            if ( AV21Flag )
            {
               AV10WebSession.Set("FiltroRecebido", AV20FuncaoDados_Nome);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20FuncaoDados_Nome", AV20FuncaoDados_Nome);
               new prc_newtabeladc(context ).execute(  A368FuncaoDados_Codigo,  AV13FuncaoDados_SistemaCod) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13FuncaoDados_SistemaCod), 6, 0)));
            }
            context.PopUp(formatLink("wp_funcaodadostabelains.aspx") + "?" + UrlEncode("" +AV13FuncaoDados_SistemaCod) + "," + UrlEncode("" +A368FuncaoDados_Codigo), new Object[] {});
         }
         else
         {
            if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
            {
               if ( AV13FuncaoDados_SistemaCod == AV15Combo_SistemaCod )
               {
               }
               else
               {
                  context.wjLoc = formatLink("viewsistema.aspx") + "?" + UrlEncode("" +AV13FuncaoDados_SistemaCod) + "," + UrlEncode(StringUtil.RTrim("FuncaoDados"));
                  context.wjLocDisableFrm = 1;
               }
            }
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwfuncaodados.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)AV13FuncaoDados_SistemaCod});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E131L2( )
      {
         /* 'DoEliminar' Routine */
         new prc_desativarregistro(context ).execute(  "FnD",  AV7FuncaoDados_Codigo,  0) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoDados_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFUNCAODADOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7FuncaoDados_Codigo), "ZZZZZ9")));
         context.setWebReturnParms(new Object[] {(int)AV13FuncaoDados_SistemaCod});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E141L2( )
      {
         /* Combo_sistemacod_Click Routine */
         if ( (0==AV15Combo_SistemaCod) || ( AV15Combo_SistemaCod == AV13FuncaoDados_SistemaCod ) )
         {
            edtavFuncaodados_nome_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaodados_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_nome_Enabled), 5, 0)));
            edtavFuncaodados_descricao_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaodados_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_descricao_Enabled), 5, 0)));
            bttBtn_trn_enter_Caption = "Confirmar e selecionar tabelas";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Caption", bttBtn_trn_enter_Caption);
            AV20FuncaoDados_Nome = O369FuncaoDados_Nome;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20FuncaoDados_Nome", AV20FuncaoDados_Nome);
            AV17FuncaoDados_Descricao = O397FuncaoDados_Descricao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoDados_Descricao", AV17FuncaoDados_Descricao);
            AV18FuncaoDados_FuncaoDadosCod = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18FuncaoDados_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18FuncaoDados_FuncaoDadosCod), 6, 0)));
            cmbavFuncaodados_tipo.removeAllItems();
            cmbavFuncaodados_tipo.addItem("ALI", "ALI", 0);
            cmbavFuncaodados_tipo.addItem("DC", "DC", 0);
            AV16FuncaoDados_Tipo = "ALI";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16FuncaoDados_Tipo", AV16FuncaoDados_Tipo);
         }
         else
         {
            edtavFuncaodados_nome_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaodados_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_nome_Enabled), 5, 0)));
            edtavFuncaodados_descricao_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaodados_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_descricao_Enabled), 5, 0)));
            bttBtn_trn_enter_Caption = "Confirmar";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Caption", bttBtn_trn_enter_Caption);
            cmbavFuncaodados_tipo.removeAllItems();
            cmbavFuncaodados_tipo.addItem("AIE", "AIE", 0);
            cmbavFuncaodados_tipo.addItem("DC", "DC", 0);
            context.PopUp(formatLink("promptfuncaodados.aspx") + "?" + UrlEncode("" +AV18FuncaoDados_FuncaoDadosCod) + "," + UrlEncode(StringUtil.RTrim(AV20FuncaoDados_Nome)) + "," + UrlEncode(StringUtil.RTrim(AV17FuncaoDados_Descricao)) + "," + UrlEncode(StringUtil.RTrim(AV16FuncaoDados_Tipo)) + "," + UrlEncode("" +AV15Combo_SistemaCod), new Object[] {"AV18FuncaoDados_FuncaoDadosCod","AV20FuncaoDados_Nome","AV17FuncaoDados_Descricao","AV16FuncaoDados_Tipo",});
         }
         cmbavFuncaodados_tipo.CurrentValue = StringUtil.RTrim( AV16FuncaoDados_Tipo);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaodados_tipo_Internalname, "Values", cmbavFuncaodados_tipo.ToJavascriptSource());
      }

      protected void ZM1L59( short GX_JID )
      {
         if ( ( GX_JID == 45 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z373FuncaoDados_Tipo = T001L3_A373FuncaoDados_Tipo[0];
               Z369FuncaoDados_Nome = T001L3_A369FuncaoDados_Nome[0];
               Z1024FuncaoDados_DERImp = T001L3_A1024FuncaoDados_DERImp[0];
               Z1025FuncaoDados_RAImp = T001L3_A1025FuncaoDados_RAImp[0];
               Z755FuncaoDados_Contar = T001L3_A755FuncaoDados_Contar[0];
               Z1147FuncaoDados_Tecnica = T001L3_A1147FuncaoDados_Tecnica[0];
               Z1258FuncaoDados_Importada = T001L3_A1258FuncaoDados_Importada[0];
               Z1267FuncaoDados_UpdAoImpBsln = T001L3_A1267FuncaoDados_UpdAoImpBsln[0];
               Z394FuncaoDados_Ativo = T001L3_A394FuncaoDados_Ativo[0];
               Z370FuncaoDados_SistemaCod = T001L3_A370FuncaoDados_SistemaCod[0];
               Z391FuncaoDados_FuncaoDadosCod = T001L3_A391FuncaoDados_FuncaoDadosCod[0];
               Z745FuncaoDados_MelhoraCod = T001L3_A745FuncaoDados_MelhoraCod[0];
            }
            else
            {
               Z373FuncaoDados_Tipo = A373FuncaoDados_Tipo;
               Z369FuncaoDados_Nome = A369FuncaoDados_Nome;
               Z1024FuncaoDados_DERImp = A1024FuncaoDados_DERImp;
               Z1025FuncaoDados_RAImp = A1025FuncaoDados_RAImp;
               Z755FuncaoDados_Contar = A755FuncaoDados_Contar;
               Z1147FuncaoDados_Tecnica = A1147FuncaoDados_Tecnica;
               Z1258FuncaoDados_Importada = A1258FuncaoDados_Importada;
               Z1267FuncaoDados_UpdAoImpBsln = A1267FuncaoDados_UpdAoImpBsln;
               Z394FuncaoDados_Ativo = A394FuncaoDados_Ativo;
               Z370FuncaoDados_SistemaCod = A370FuncaoDados_SistemaCod;
               Z391FuncaoDados_FuncaoDadosCod = A391FuncaoDados_FuncaoDadosCod;
               Z745FuncaoDados_MelhoraCod = A745FuncaoDados_MelhoraCod;
            }
         }
         if ( GX_JID == -45 )
         {
            Z368FuncaoDados_Codigo = A368FuncaoDados_Codigo;
            Z373FuncaoDados_Tipo = A373FuncaoDados_Tipo;
            Z369FuncaoDados_Nome = A369FuncaoDados_Nome;
            Z397FuncaoDados_Descricao = A397FuncaoDados_Descricao;
            Z1024FuncaoDados_DERImp = A1024FuncaoDados_DERImp;
            Z1025FuncaoDados_RAImp = A1025FuncaoDados_RAImp;
            Z755FuncaoDados_Contar = A755FuncaoDados_Contar;
            Z1147FuncaoDados_Tecnica = A1147FuncaoDados_Tecnica;
            Z1245FuncaoDados_Observacao = A1245FuncaoDados_Observacao;
            Z1258FuncaoDados_Importada = A1258FuncaoDados_Importada;
            Z1267FuncaoDados_UpdAoImpBsln = A1267FuncaoDados_UpdAoImpBsln;
            Z394FuncaoDados_Ativo = A394FuncaoDados_Ativo;
            Z370FuncaoDados_SistemaCod = A370FuncaoDados_SistemaCod;
            Z391FuncaoDados_FuncaoDadosCod = A391FuncaoDados_FuncaoDadosCod;
            Z745FuncaoDados_MelhoraCod = A745FuncaoDados_MelhoraCod;
            Z371FuncaoDados_SistemaDes = A371FuncaoDados_SistemaDes;
            Z705FuncaoDados_SistemaTecnica = A705FuncaoDados_SistemaTecnica;
            Z372FuncaoDados_SistemaAreaCod = A372FuncaoDados_SistemaAreaCod;
            Z402FuncaoDados_FuncaoDadosNom = A402FuncaoDados_FuncaoDadosNom;
            Z403FuncaoDados_FuncaoDadosDes = A403FuncaoDados_FuncaoDadosDes;
            Z404FuncaoDados_FuncaoDadosSisCod = A404FuncaoDados_FuncaoDadosSisCod;
            Z405FuncaoDados_FuncaoDadosSisDes = A405FuncaoDados_FuncaoDadosSisDes;
         }
      }

      protected void standaloneNotModal( )
      {
         edtFuncaoDados_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_Codigo_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         AV28Pgmname = "FuncaoDados";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Pgmname", AV28Pgmname);
         context.Gx_err = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_err", StringUtil.LTrim( StringUtil.Str( (decimal)(context.Gx_err), 3, 0)));
         edtFuncaoDados_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_Codigo_Enabled), 5, 0)));
         if ( ! (0==AV7FuncaoDados_Codigo) )
         {
            A368FuncaoDados_Codigo = AV7FuncaoDados_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         }
         if ( ! ( ( StringUtil.StrCmp(AV16FuncaoDados_Tipo, "ALI") == 0 ) || ( StringUtil.StrCmp(AV16FuncaoDados_Tipo, "AIE") == 0 ) || ( StringUtil.StrCmp(AV16FuncaoDados_Tipo, "DC") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( AV16FuncaoDados_Tipo)) ) )
         {
            GX_msglist.addItem("Campo Funcao Dados_Tipo fora do intervalo", "OutOfRange", 1, "vFUNCAODADOS_TIPO");
            AnyError = 1;
            GX_FocusControl = cmbavFuncaodados_tipo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void standaloneModal( )
      {
         bttBtn_trn_enter_Visible = (!( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV23Insert_FuncaoDados_MelhoraCod) )
         {
            A745FuncaoDados_MelhoraCod = AV23Insert_FuncaoDados_MelhoraCod;
            n745FuncaoDados_MelhoraCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A745FuncaoDados_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A745FuncaoDados_MelhoraCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV14Insert_FuncaoDados_FuncaoDadosCod) )
         {
            A391FuncaoDados_FuncaoDadosCod = AV14Insert_FuncaoDados_FuncaoDadosCod;
            n391FuncaoDados_FuncaoDadosCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A391FuncaoDados_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_FuncaoDados_SistemaCod) )
         {
            A370FuncaoDados_SistemaCod = AV11Insert_FuncaoDados_SistemaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A370FuncaoDados_SistemaCod), 6, 0)));
         }
         GXt_boolean1 = false;
         new prc_setnaopodedeletar(context ).execute(  "FnD",  AV7FuncaoDados_Codigo, out  GXt_boolean1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoDados_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFUNCAODADOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7FuncaoDados_Codigo), "ZZZZZ9")));
         bttBtneliminar_Visible = (( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) &&GXt_boolean1 ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtneliminar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtneliminar_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A394FuncaoDados_Ativo)) && ( Gx_BScreen == 0 ) )
         {
            A394FuncaoDados_Ativo = "A";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A394FuncaoDados_Ativo", A394FuncaoDados_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A1267FuncaoDados_UpdAoImpBsln) && ( Gx_BScreen == 0 ) )
         {
            A1267FuncaoDados_UpdAoImpBsln = true;
            n1267FuncaoDados_UpdAoImpBsln = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1267FuncaoDados_UpdAoImpBsln", A1267FuncaoDados_UpdAoImpBsln);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A755FuncaoDados_Contar) && ( Gx_BScreen == 0 ) )
         {
            A755FuncaoDados_Contar = true;
            n755FuncaoDados_Contar = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A755FuncaoDados_Contar", A755FuncaoDados_Contar);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            GXt_char2 = A376FuncaoDados_Complexidade;
            new prc_fdcomplexidade(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
            A376FuncaoDados_Complexidade = GXt_char2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A376FuncaoDados_Complexidade", A376FuncaoDados_Complexidade);
            GXt_int3 = A375FuncaoDados_RLR;
            new prc_funcaodados_rlr(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int3) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
            A375FuncaoDados_RLR = GXt_int3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A375FuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(A375FuncaoDados_RLR), 4, 0)));
            GXt_int3 = A374FuncaoDados_DER;
            new prc_funcaodados_der(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int3) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
            A374FuncaoDados_DER = GXt_int3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A374FuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(A374FuncaoDados_DER), 4, 0)));
            GXt_char2 = A753FuncaoDados_SolucaoTecnica;
            GXt_int4 = 0;
            new prc_solucaotecnica(context ).execute( ref  A745FuncaoDados_MelhoraCod, ref  GXt_int4,  "D", out  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A745FuncaoDados_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A745FuncaoDados_MelhoraCod), 6, 0)));
            A753FuncaoDados_SolucaoTecnica = GXt_char2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A753FuncaoDados_SolucaoTecnica", A753FuncaoDados_SolucaoTecnica);
            A754FuncaoDados_SolucaoTecnica30 = A753FuncaoDados_SolucaoTecnica;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A754FuncaoDados_SolucaoTecnica30", A754FuncaoDados_SolucaoTecnica30);
            /* Using cursor T001L5 */
            pr_default.execute(3, new Object[] {n391FuncaoDados_FuncaoDadosCod, A391FuncaoDados_FuncaoDadosCod});
            A402FuncaoDados_FuncaoDadosNom = T001L5_A402FuncaoDados_FuncaoDadosNom[0];
            n402FuncaoDados_FuncaoDadosNom = T001L5_n402FuncaoDados_FuncaoDadosNom[0];
            A403FuncaoDados_FuncaoDadosDes = T001L5_A403FuncaoDados_FuncaoDadosDes[0];
            n403FuncaoDados_FuncaoDadosDes = T001L5_n403FuncaoDados_FuncaoDadosDes[0];
            A404FuncaoDados_FuncaoDadosSisCod = T001L5_A404FuncaoDados_FuncaoDadosSisCod[0];
            n404FuncaoDados_FuncaoDadosSisCod = T001L5_n404FuncaoDados_FuncaoDadosSisCod[0];
            pr_default.close(3);
            /* Using cursor T001L7 */
            pr_default.execute(5, new Object[] {n404FuncaoDados_FuncaoDadosSisCod, A404FuncaoDados_FuncaoDadosSisCod});
            A405FuncaoDados_FuncaoDadosSisDes = T001L7_A405FuncaoDados_FuncaoDadosSisDes[0];
            n405FuncaoDados_FuncaoDadosSisDes = T001L7_n405FuncaoDados_FuncaoDadosSisDes[0];
            pr_default.close(5);
            A420FuncaoDados_CodigoAliAie = (short)((T001L3_n391FuncaoDados_FuncaoDadosCod[0] ? A368FuncaoDados_Codigo : A391FuncaoDados_FuncaoDadosCod));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A420FuncaoDados_CodigoAliAie", StringUtil.LTrim( StringUtil.Str( (decimal)(A420FuncaoDados_CodigoAliAie), 4, 0)));
            /* Using cursor T001L4 */
            pr_default.execute(2, new Object[] {A370FuncaoDados_SistemaCod});
            A371FuncaoDados_SistemaDes = T001L4_A371FuncaoDados_SistemaDes[0];
            n371FuncaoDados_SistemaDes = T001L4_n371FuncaoDados_SistemaDes[0];
            A705FuncaoDados_SistemaTecnica = T001L4_A705FuncaoDados_SistemaTecnica[0];
            n705FuncaoDados_SistemaTecnica = T001L4_n705FuncaoDados_SistemaTecnica[0];
            A372FuncaoDados_SistemaAreaCod = T001L4_A372FuncaoDados_SistemaAreaCod[0];
            n372FuncaoDados_SistemaAreaCod = T001L4_n372FuncaoDados_SistemaAreaCod[0];
            pr_default.close(2);
            Dvpanel_tableattributes_Title = "Fun��o de Dados do Sistema: "+A371FuncaoDados_SistemaDes;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvpanel_tableattributes_Internalname, "Title", Dvpanel_tableattributes_Title);
            chkavFlag.Visible = (((StringUtil.StringSearch( A705FuncaoDados_SistemaTecnica, "IE", 1)==0)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavFlag_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavFlag.Visible), 5, 0)));
            lblTextblockflag_Visible = (((StringUtil.StringSearch( A705FuncaoDados_SistemaTecnica, "IE", 1)==0)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockflag_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockflag_Visible), 5, 0)));
            GXVvCOMBO_SISTEMACOD_html1L59( A370FuncaoDados_SistemaCod) ;
            if ( A755FuncaoDados_Contar )
            {
               GXt_int3 = (short)(A377FuncaoDados_PF);
               new prc_fdpf(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_int3) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
               A377FuncaoDados_PF = (decimal)(GXt_int3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A377FuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( A377FuncaoDados_PF, 14, 5)));
            }
            else
            {
               A377FuncaoDados_PF = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A377FuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( A377FuncaoDados_PF, 14, 5)));
            }
         }
      }

      protected void Load1L59( )
      {
         /* Using cursor T001L8 */
         pr_default.execute(6, new Object[] {A368FuncaoDados_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound59 = 1;
            A373FuncaoDados_Tipo = T001L8_A373FuncaoDados_Tipo[0];
            A369FuncaoDados_Nome = T001L8_A369FuncaoDados_Nome[0];
            A397FuncaoDados_Descricao = T001L8_A397FuncaoDados_Descricao[0];
            n397FuncaoDados_Descricao = T001L8_n397FuncaoDados_Descricao[0];
            A371FuncaoDados_SistemaDes = T001L8_A371FuncaoDados_SistemaDes[0];
            n371FuncaoDados_SistemaDes = T001L8_n371FuncaoDados_SistemaDes[0];
            A705FuncaoDados_SistemaTecnica = T001L8_A705FuncaoDados_SistemaTecnica[0];
            n705FuncaoDados_SistemaTecnica = T001L8_n705FuncaoDados_SistemaTecnica[0];
            A1024FuncaoDados_DERImp = T001L8_A1024FuncaoDados_DERImp[0];
            n1024FuncaoDados_DERImp = T001L8_n1024FuncaoDados_DERImp[0];
            A1025FuncaoDados_RAImp = T001L8_A1025FuncaoDados_RAImp[0];
            n1025FuncaoDados_RAImp = T001L8_n1025FuncaoDados_RAImp[0];
            A402FuncaoDados_FuncaoDadosNom = T001L8_A402FuncaoDados_FuncaoDadosNom[0];
            n402FuncaoDados_FuncaoDadosNom = T001L8_n402FuncaoDados_FuncaoDadosNom[0];
            A403FuncaoDados_FuncaoDadosDes = T001L8_A403FuncaoDados_FuncaoDadosDes[0];
            n403FuncaoDados_FuncaoDadosDes = T001L8_n403FuncaoDados_FuncaoDadosDes[0];
            A405FuncaoDados_FuncaoDadosSisDes = T001L8_A405FuncaoDados_FuncaoDadosSisDes[0];
            n405FuncaoDados_FuncaoDadosSisDes = T001L8_n405FuncaoDados_FuncaoDadosSisDes[0];
            A755FuncaoDados_Contar = T001L8_A755FuncaoDados_Contar[0];
            n755FuncaoDados_Contar = T001L8_n755FuncaoDados_Contar[0];
            A1147FuncaoDados_Tecnica = T001L8_A1147FuncaoDados_Tecnica[0];
            n1147FuncaoDados_Tecnica = T001L8_n1147FuncaoDados_Tecnica[0];
            A1245FuncaoDados_Observacao = T001L8_A1245FuncaoDados_Observacao[0];
            n1245FuncaoDados_Observacao = T001L8_n1245FuncaoDados_Observacao[0];
            A1258FuncaoDados_Importada = T001L8_A1258FuncaoDados_Importada[0];
            n1258FuncaoDados_Importada = T001L8_n1258FuncaoDados_Importada[0];
            A1267FuncaoDados_UpdAoImpBsln = T001L8_A1267FuncaoDados_UpdAoImpBsln[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1267FuncaoDados_UpdAoImpBsln", A1267FuncaoDados_UpdAoImpBsln);
            n1267FuncaoDados_UpdAoImpBsln = T001L8_n1267FuncaoDados_UpdAoImpBsln[0];
            A394FuncaoDados_Ativo = T001L8_A394FuncaoDados_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A394FuncaoDados_Ativo", A394FuncaoDados_Ativo);
            A370FuncaoDados_SistemaCod = T001L8_A370FuncaoDados_SistemaCod[0];
            A391FuncaoDados_FuncaoDadosCod = T001L8_A391FuncaoDados_FuncaoDadosCod[0];
            n391FuncaoDados_FuncaoDadosCod = T001L8_n391FuncaoDados_FuncaoDadosCod[0];
            A745FuncaoDados_MelhoraCod = T001L8_A745FuncaoDados_MelhoraCod[0];
            n745FuncaoDados_MelhoraCod = T001L8_n745FuncaoDados_MelhoraCod[0];
            A372FuncaoDados_SistemaAreaCod = T001L8_A372FuncaoDados_SistemaAreaCod[0];
            n372FuncaoDados_SistemaAreaCod = T001L8_n372FuncaoDados_SistemaAreaCod[0];
            A404FuncaoDados_FuncaoDadosSisCod = T001L8_A404FuncaoDados_FuncaoDadosSisCod[0];
            n404FuncaoDados_FuncaoDadosSisCod = T001L8_n404FuncaoDados_FuncaoDadosSisCod[0];
            ZM1L59( -45) ;
         }
         pr_default.close(6);
         OnLoadActions1L59( ) ;
      }

      protected void OnLoadActions1L59( )
      {
         A420FuncaoDados_CodigoAliAie = (short)((T001L3_n391FuncaoDados_FuncaoDadosCod[0] ? A368FuncaoDados_Codigo : A391FuncaoDados_FuncaoDadosCod));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A420FuncaoDados_CodigoAliAie", StringUtil.LTrim( StringUtil.Str( (decimal)(A420FuncaoDados_CodigoAliAie), 4, 0)));
         if ( A755FuncaoDados_Contar )
         {
            GXt_int3 = (short)(A377FuncaoDados_PF);
            new prc_fdpf(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_int3) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
            A377FuncaoDados_PF = (decimal)(GXt_int3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A377FuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( A377FuncaoDados_PF, 14, 5)));
         }
         else
         {
            A377FuncaoDados_PF = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A377FuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( A377FuncaoDados_PF, 14, 5)));
         }
         GXt_char2 = A376FuncaoDados_Complexidade;
         new prc_fdcomplexidade(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_char2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         A376FuncaoDados_Complexidade = GXt_char2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A376FuncaoDados_Complexidade", A376FuncaoDados_Complexidade);
         GXt_int3 = A375FuncaoDados_RLR;
         new prc_funcaodados_rlr(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int3) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         A375FuncaoDados_RLR = GXt_int3;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A375FuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(A375FuncaoDados_RLR), 4, 0)));
         GXt_int3 = A374FuncaoDados_DER;
         new prc_funcaodados_der(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int3) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         A374FuncaoDados_DER = GXt_int3;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A374FuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(A374FuncaoDados_DER), 4, 0)));
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV16FuncaoDados_Tipo = A373FuncaoDados_Tipo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16FuncaoDados_Tipo", AV16FuncaoDados_Tipo);
         }
         A419FuncaoDados_Nome50 = StringUtil.Substring( A369FuncaoDados_Nome, 1, 50);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A419FuncaoDados_Nome50", A419FuncaoDados_Nome50);
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV20FuncaoDados_Nome = A369FuncaoDados_Nome;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20FuncaoDados_Nome", AV20FuncaoDados_Nome);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV17FuncaoDados_Descricao = A397FuncaoDados_Descricao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoDados_Descricao", AV17FuncaoDados_Descricao);
         }
         Dvpanel_tableattributes_Title = "Fun��o de Dados do Sistema: "+A371FuncaoDados_SistemaDes;
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvpanel_tableattributes_Internalname, "Title", Dvpanel_tableattributes_Title);
         chkavFlag.Visible = (((StringUtil.StringSearch( A705FuncaoDados_SistemaTecnica, "IE", 1)==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavFlag_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavFlag.Visible), 5, 0)));
         lblTextblockflag_Visible = (((StringUtil.StringSearch( A705FuncaoDados_SistemaTecnica, "IE", 1)==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockflag_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockflag_Visible), 5, 0)));
         GXVvCOMBO_SISTEMACOD_html1L59( A370FuncaoDados_SistemaCod) ;
         if ( ! (0==A404FuncaoDados_FuncaoDadosSisCod) && ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV15Combo_SistemaCod = A404FuncaoDados_FuncaoDadosSisCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Combo_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Combo_SistemaCod), 6, 0)));
         }
         else
         {
            if ( (0==A404FuncaoDados_FuncaoDadosSisCod) && ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV15Combo_SistemaCod = AV13FuncaoDados_SistemaCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Combo_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Combo_SistemaCod), 6, 0)));
            }
         }
         lblTextblockcombo_sistemacod_Visible = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||!(0==A391FuncaoDados_FuncaoDadosCod) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcombo_sistemacod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcombo_sistemacod_Visible), 5, 0)));
         dynavCombo_sistemacod.Visible = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||!(0==A391FuncaoDados_FuncaoDadosCod) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCombo_sistemacod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavCombo_sistemacod.Visible), 5, 0)));
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV18FuncaoDados_FuncaoDadosCod = A391FuncaoDados_FuncaoDadosCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18FuncaoDados_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18FuncaoDados_FuncaoDadosCod), 6, 0)));
         }
         GXt_char2 = A753FuncaoDados_SolucaoTecnica;
         GXt_int4 = 0;
         new prc_solucaotecnica(context ).execute( ref  A745FuncaoDados_MelhoraCod, ref  GXt_int4,  "D", out  GXt_char2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A745FuncaoDados_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A745FuncaoDados_MelhoraCod), 6, 0)));
         A753FuncaoDados_SolucaoTecnica = GXt_char2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A753FuncaoDados_SolucaoTecnica", A753FuncaoDados_SolucaoTecnica);
         A754FuncaoDados_SolucaoTecnica30 = A753FuncaoDados_SolucaoTecnica;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A754FuncaoDados_SolucaoTecnica30", A754FuncaoDados_SolucaoTecnica30);
      }

      protected void CheckExtendedTable1L59( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         A420FuncaoDados_CodigoAliAie = (short)((T001L3_n391FuncaoDados_FuncaoDadosCod[0] ? A368FuncaoDados_Codigo : A391FuncaoDados_FuncaoDadosCod));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A420FuncaoDados_CodigoAliAie", StringUtil.LTrim( StringUtil.Str( (decimal)(A420FuncaoDados_CodigoAliAie), 4, 0)));
         if ( A755FuncaoDados_Contar )
         {
            GXt_int3 = (short)(A377FuncaoDados_PF);
            new prc_fdpf(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_int3) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
            A377FuncaoDados_PF = (decimal)(GXt_int3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A377FuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( A377FuncaoDados_PF, 14, 5)));
         }
         else
         {
            A377FuncaoDados_PF = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A377FuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( A377FuncaoDados_PF, 14, 5)));
         }
         GXt_char2 = A376FuncaoDados_Complexidade;
         new prc_fdcomplexidade(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_char2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         A376FuncaoDados_Complexidade = GXt_char2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A376FuncaoDados_Complexidade", A376FuncaoDados_Complexidade);
         GXt_int3 = A375FuncaoDados_RLR;
         new prc_funcaodados_rlr(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int3) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         A375FuncaoDados_RLR = GXt_int3;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A375FuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(A375FuncaoDados_RLR), 4, 0)));
         GXt_int3 = A374FuncaoDados_DER;
         new prc_funcaodados_der(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int3) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         A374FuncaoDados_DER = GXt_int3;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A374FuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(A374FuncaoDados_DER), 4, 0)));
         if ( ! ( ( StringUtil.StrCmp(A394FuncaoDados_Ativo, "A") == 0 ) || ( StringUtil.StrCmp(A394FuncaoDados_Ativo, "E") == 0 ) || ( StringUtil.StrCmp(A394FuncaoDados_Ativo, "R") == 0 ) ) )
         {
            GX_msglist.addItem("Campo Status fora do intervalo", "OutOfRange", 1, "FUNCAODADOS_ATIVO");
            AnyError = 1;
            GX_FocusControl = cmbFuncaoDados_Ativo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV20FuncaoDados_Nome)) )
         {
            GX_msglist.addItem("Nome � obrigat�rio.", 1, "vFUNCAODADOS_NOME");
            AnyError = 1;
            GX_FocusControl = edtavFuncaodados_nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV16FuncaoDados_Tipo = A373FuncaoDados_Tipo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16FuncaoDados_Tipo", AV16FuncaoDados_Tipo);
         }
         A419FuncaoDados_Nome50 = StringUtil.Substring( A369FuncaoDados_Nome, 1, 50);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A419FuncaoDados_Nome50", A419FuncaoDados_Nome50);
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV20FuncaoDados_Nome = A369FuncaoDados_Nome;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20FuncaoDados_Nome", AV20FuncaoDados_Nome);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV17FuncaoDados_Descricao = A397FuncaoDados_Descricao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoDados_Descricao", AV17FuncaoDados_Descricao);
         }
         /* Using cursor T001L4 */
         pr_default.execute(2, new Object[] {A370FuncaoDados_SistemaCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Funcao Dados_Sistema'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A371FuncaoDados_SistemaDes = T001L4_A371FuncaoDados_SistemaDes[0];
         n371FuncaoDados_SistemaDes = T001L4_n371FuncaoDados_SistemaDes[0];
         A705FuncaoDados_SistemaTecnica = T001L4_A705FuncaoDados_SistemaTecnica[0];
         n705FuncaoDados_SistemaTecnica = T001L4_n705FuncaoDados_SistemaTecnica[0];
         A372FuncaoDados_SistemaAreaCod = T001L4_A372FuncaoDados_SistemaAreaCod[0];
         n372FuncaoDados_SistemaAreaCod = T001L4_n372FuncaoDados_SistemaAreaCod[0];
         pr_default.close(2);
         Dvpanel_tableattributes_Title = "Fun��o de Dados do Sistema: "+A371FuncaoDados_SistemaDes;
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvpanel_tableattributes_Internalname, "Title", Dvpanel_tableattributes_Title);
         chkavFlag.Visible = (((StringUtil.StringSearch( A705FuncaoDados_SistemaTecnica, "IE", 1)==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavFlag_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavFlag.Visible), 5, 0)));
         lblTextblockflag_Visible = (((StringUtil.StringSearch( A705FuncaoDados_SistemaTecnica, "IE", 1)==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockflag_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockflag_Visible), 5, 0)));
         GXVvCOMBO_SISTEMACOD_html1L59( A370FuncaoDados_SistemaCod) ;
         /* Using cursor T001L5 */
         pr_default.execute(3, new Object[] {n391FuncaoDados_FuncaoDadosCod, A391FuncaoDados_FuncaoDadosCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A391FuncaoDados_FuncaoDadosCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Autorelacionamento fun��es de dados externas'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A402FuncaoDados_FuncaoDadosNom = T001L5_A402FuncaoDados_FuncaoDadosNom[0];
         n402FuncaoDados_FuncaoDadosNom = T001L5_n402FuncaoDados_FuncaoDadosNom[0];
         A403FuncaoDados_FuncaoDadosDes = T001L5_A403FuncaoDados_FuncaoDadosDes[0];
         n403FuncaoDados_FuncaoDadosDes = T001L5_n403FuncaoDados_FuncaoDadosDes[0];
         A404FuncaoDados_FuncaoDadosSisCod = T001L5_A404FuncaoDados_FuncaoDadosSisCod[0];
         n404FuncaoDados_FuncaoDadosSisCod = T001L5_n404FuncaoDados_FuncaoDadosSisCod[0];
         pr_default.close(3);
         /* Using cursor T001L7 */
         pr_default.execute(5, new Object[] {n404FuncaoDados_FuncaoDadosSisCod, A404FuncaoDados_FuncaoDadosSisCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A404FuncaoDados_FuncaoDadosSisCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A405FuncaoDados_FuncaoDadosSisDes = T001L7_A405FuncaoDados_FuncaoDadosSisDes[0];
         n405FuncaoDados_FuncaoDadosSisDes = T001L7_n405FuncaoDados_FuncaoDadosSisDes[0];
         pr_default.close(5);
         if ( ! (0==A404FuncaoDados_FuncaoDadosSisCod) && ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV15Combo_SistemaCod = A404FuncaoDados_FuncaoDadosSisCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Combo_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Combo_SistemaCod), 6, 0)));
         }
         else
         {
            if ( (0==A404FuncaoDados_FuncaoDadosSisCod) && ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV15Combo_SistemaCod = AV13FuncaoDados_SistemaCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Combo_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Combo_SistemaCod), 6, 0)));
            }
         }
         lblTextblockcombo_sistemacod_Visible = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||!(0==A391FuncaoDados_FuncaoDadosCod) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcombo_sistemacod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcombo_sistemacod_Visible), 5, 0)));
         dynavCombo_sistemacod.Visible = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||!(0==A391FuncaoDados_FuncaoDadosCod) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCombo_sistemacod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavCombo_sistemacod.Visible), 5, 0)));
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV18FuncaoDados_FuncaoDadosCod = A391FuncaoDados_FuncaoDadosCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18FuncaoDados_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18FuncaoDados_FuncaoDadosCod), 6, 0)));
         }
         /* Using cursor T001L6 */
         pr_default.execute(4, new Object[] {n745FuncaoDados_MelhoraCod, A745FuncaoDados_MelhoraCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A745FuncaoDados_MelhoraCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Funcao DAdos_Funcao Dados Melhora'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         pr_default.close(4);
         GXt_char2 = A753FuncaoDados_SolucaoTecnica;
         GXt_int4 = 0;
         new prc_solucaotecnica(context ).execute( ref  A745FuncaoDados_MelhoraCod, ref  GXt_int4,  "D", out  GXt_char2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A745FuncaoDados_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A745FuncaoDados_MelhoraCod), 6, 0)));
         A753FuncaoDados_SolucaoTecnica = GXt_char2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A753FuncaoDados_SolucaoTecnica", A753FuncaoDados_SolucaoTecnica);
         A754FuncaoDados_SolucaoTecnica30 = A753FuncaoDados_SolucaoTecnica;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A754FuncaoDados_SolucaoTecnica30", A754FuncaoDados_SolucaoTecnica30);
      }

      protected void CloseExtendedTableCursors1L59( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(5);
         pr_default.close(4);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_46( int A370FuncaoDados_SistemaCod )
      {
         /* Using cursor T001L9 */
         pr_default.execute(7, new Object[] {A370FuncaoDados_SistemaCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Funcao Dados_Sistema'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A371FuncaoDados_SistemaDes = T001L9_A371FuncaoDados_SistemaDes[0];
         n371FuncaoDados_SistemaDes = T001L9_n371FuncaoDados_SistemaDes[0];
         A705FuncaoDados_SistemaTecnica = T001L9_A705FuncaoDados_SistemaTecnica[0];
         n705FuncaoDados_SistemaTecnica = T001L9_n705FuncaoDados_SistemaTecnica[0];
         A372FuncaoDados_SistemaAreaCod = T001L9_A372FuncaoDados_SistemaAreaCod[0];
         n372FuncaoDados_SistemaAreaCod = T001L9_n372FuncaoDados_SistemaAreaCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A371FuncaoDados_SistemaDes)+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A705FuncaoDados_SistemaTecnica))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A372FuncaoDados_SistemaAreaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void gxLoad_47( int A391FuncaoDados_FuncaoDadosCod )
      {
         /* Using cursor T001L10 */
         pr_default.execute(8, new Object[] {n391FuncaoDados_FuncaoDadosCod, A391FuncaoDados_FuncaoDadosCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            if ( ! ( (0==A391FuncaoDados_FuncaoDadosCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Autorelacionamento fun��es de dados externas'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A402FuncaoDados_FuncaoDadosNom = T001L10_A402FuncaoDados_FuncaoDadosNom[0];
         n402FuncaoDados_FuncaoDadosNom = T001L10_n402FuncaoDados_FuncaoDadosNom[0];
         A403FuncaoDados_FuncaoDadosDes = T001L10_A403FuncaoDados_FuncaoDadosDes[0];
         n403FuncaoDados_FuncaoDadosDes = T001L10_n403FuncaoDados_FuncaoDadosDes[0];
         A404FuncaoDados_FuncaoDadosSisCod = T001L10_A404FuncaoDados_FuncaoDadosSisCod[0];
         n404FuncaoDados_FuncaoDadosSisCod = T001L10_n404FuncaoDados_FuncaoDadosSisCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A402FuncaoDados_FuncaoDadosNom)+"\""+","+"\""+GXUtil.EncodeJSConstant( A403FuncaoDados_FuncaoDadosDes)+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A404FuncaoDados_FuncaoDadosSisCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void gxLoad_49( int A404FuncaoDados_FuncaoDadosSisCod )
      {
         /* Using cursor T001L11 */
         pr_default.execute(9, new Object[] {n404FuncaoDados_FuncaoDadosSisCod, A404FuncaoDados_FuncaoDadosSisCod});
         if ( (pr_default.getStatus(9) == 101) )
         {
            if ( ! ( (0==A404FuncaoDados_FuncaoDadosSisCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A405FuncaoDados_FuncaoDadosSisDes = T001L11_A405FuncaoDados_FuncaoDadosSisDes[0];
         n405FuncaoDados_FuncaoDadosSisDes = T001L11_n405FuncaoDados_FuncaoDadosSisDes[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A405FuncaoDados_FuncaoDadosSisDes)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(9) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(9);
      }

      protected void gxLoad_48( int A745FuncaoDados_MelhoraCod )
      {
         /* Using cursor T001L12 */
         pr_default.execute(10, new Object[] {n745FuncaoDados_MelhoraCod, A745FuncaoDados_MelhoraCod});
         if ( (pr_default.getStatus(10) == 101) )
         {
            if ( ! ( (0==A745FuncaoDados_MelhoraCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Funcao DAdos_Funcao Dados Melhora'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(10) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(10);
      }

      protected void GetKey1L59( )
      {
         /* Using cursor T001L13 */
         pr_default.execute(11, new Object[] {A368FuncaoDados_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound59 = 1;
         }
         else
         {
            RcdFound59 = 0;
         }
         pr_default.close(11);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T001L3 */
         pr_default.execute(1, new Object[] {A368FuncaoDados_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1L59( 45) ;
            RcdFound59 = 1;
            A368FuncaoDados_Codigo = T001L3_A368FuncaoDados_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
            A373FuncaoDados_Tipo = T001L3_A373FuncaoDados_Tipo[0];
            A369FuncaoDados_Nome = T001L3_A369FuncaoDados_Nome[0];
            A397FuncaoDados_Descricao = T001L3_A397FuncaoDados_Descricao[0];
            n397FuncaoDados_Descricao = T001L3_n397FuncaoDados_Descricao[0];
            A1024FuncaoDados_DERImp = T001L3_A1024FuncaoDados_DERImp[0];
            n1024FuncaoDados_DERImp = T001L3_n1024FuncaoDados_DERImp[0];
            A1025FuncaoDados_RAImp = T001L3_A1025FuncaoDados_RAImp[0];
            n1025FuncaoDados_RAImp = T001L3_n1025FuncaoDados_RAImp[0];
            A755FuncaoDados_Contar = T001L3_A755FuncaoDados_Contar[0];
            n755FuncaoDados_Contar = T001L3_n755FuncaoDados_Contar[0];
            A1147FuncaoDados_Tecnica = T001L3_A1147FuncaoDados_Tecnica[0];
            n1147FuncaoDados_Tecnica = T001L3_n1147FuncaoDados_Tecnica[0];
            A1245FuncaoDados_Observacao = T001L3_A1245FuncaoDados_Observacao[0];
            n1245FuncaoDados_Observacao = T001L3_n1245FuncaoDados_Observacao[0];
            A1258FuncaoDados_Importada = T001L3_A1258FuncaoDados_Importada[0];
            n1258FuncaoDados_Importada = T001L3_n1258FuncaoDados_Importada[0];
            A1267FuncaoDados_UpdAoImpBsln = T001L3_A1267FuncaoDados_UpdAoImpBsln[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1267FuncaoDados_UpdAoImpBsln", A1267FuncaoDados_UpdAoImpBsln);
            n1267FuncaoDados_UpdAoImpBsln = T001L3_n1267FuncaoDados_UpdAoImpBsln[0];
            A394FuncaoDados_Ativo = T001L3_A394FuncaoDados_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A394FuncaoDados_Ativo", A394FuncaoDados_Ativo);
            A370FuncaoDados_SistemaCod = T001L3_A370FuncaoDados_SistemaCod[0];
            A391FuncaoDados_FuncaoDadosCod = T001L3_A391FuncaoDados_FuncaoDadosCod[0];
            n391FuncaoDados_FuncaoDadosCod = T001L3_n391FuncaoDados_FuncaoDadosCod[0];
            A745FuncaoDados_MelhoraCod = T001L3_A745FuncaoDados_MelhoraCod[0];
            n745FuncaoDados_MelhoraCod = T001L3_n745FuncaoDados_MelhoraCod[0];
            O369FuncaoDados_Nome = A369FuncaoDados_Nome;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A369FuncaoDados_Nome", A369FuncaoDados_Nome);
            O397FuncaoDados_Descricao = A397FuncaoDados_Descricao;
            n397FuncaoDados_Descricao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A397FuncaoDados_Descricao", A397FuncaoDados_Descricao);
            Z368FuncaoDados_Codigo = A368FuncaoDados_Codigo;
            sMode59 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load1L59( ) ;
            if ( AnyError == 1 )
            {
               RcdFound59 = 0;
               InitializeNonKey1L59( ) ;
            }
            Gx_mode = sMode59;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound59 = 0;
            InitializeNonKey1L59( ) ;
            sMode59 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode59;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1L59( ) ;
         if ( RcdFound59 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound59 = 0;
         /* Using cursor T001L14 */
         pr_default.execute(12, new Object[] {A368FuncaoDados_Codigo});
         if ( (pr_default.getStatus(12) != 101) )
         {
            while ( (pr_default.getStatus(12) != 101) && ( ( T001L14_A368FuncaoDados_Codigo[0] < A368FuncaoDados_Codigo ) ) )
            {
               pr_default.readNext(12);
            }
            if ( (pr_default.getStatus(12) != 101) && ( ( T001L14_A368FuncaoDados_Codigo[0] > A368FuncaoDados_Codigo ) ) )
            {
               A368FuncaoDados_Codigo = T001L14_A368FuncaoDados_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
               RcdFound59 = 1;
            }
         }
         pr_default.close(12);
      }

      protected void move_previous( )
      {
         RcdFound59 = 0;
         /* Using cursor T001L15 */
         pr_default.execute(13, new Object[] {A368FuncaoDados_Codigo});
         if ( (pr_default.getStatus(13) != 101) )
         {
            while ( (pr_default.getStatus(13) != 101) && ( ( T001L15_A368FuncaoDados_Codigo[0] > A368FuncaoDados_Codigo ) ) )
            {
               pr_default.readNext(13);
            }
            if ( (pr_default.getStatus(13) != 101) && ( ( T001L15_A368FuncaoDados_Codigo[0] < A368FuncaoDados_Codigo ) ) )
            {
               A368FuncaoDados_Codigo = T001L15_A368FuncaoDados_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
               RcdFound59 = 1;
            }
         }
         pr_default.close(13);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1L59( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = cmbFuncaoDados_Ativo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert1L59( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound59 == 1 )
            {
               if ( A368FuncaoDados_Codigo != Z368FuncaoDados_Codigo )
               {
                  A368FuncaoDados_Codigo = Z368FuncaoDados_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "FUNCAODADOS_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtFuncaoDados_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = cmbFuncaoDados_Ativo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update1L59( ) ;
                  GX_FocusControl = cmbFuncaoDados_Ativo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A368FuncaoDados_Codigo != Z368FuncaoDados_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = cmbFuncaoDados_Ativo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert1L59( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "FUNCAODADOS_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtFuncaoDados_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = cmbFuncaoDados_Ativo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert1L59( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A368FuncaoDados_Codigo != Z368FuncaoDados_Codigo )
         {
            A368FuncaoDados_Codigo = Z368FuncaoDados_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "FUNCAODADOS_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtFuncaoDados_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = cmbFuncaoDados_Ativo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency1L59( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T001L2 */
            pr_default.execute(0, new Object[] {A368FuncaoDados_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"FuncaoDados"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z373FuncaoDados_Tipo, T001L2_A373FuncaoDados_Tipo[0]) != 0 ) || ( StringUtil.StrCmp(Z369FuncaoDados_Nome, T001L2_A369FuncaoDados_Nome[0]) != 0 ) || ( Z1024FuncaoDados_DERImp != T001L2_A1024FuncaoDados_DERImp[0] ) || ( Z1025FuncaoDados_RAImp != T001L2_A1025FuncaoDados_RAImp[0] ) || ( Z755FuncaoDados_Contar != T001L2_A755FuncaoDados_Contar[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1147FuncaoDados_Tecnica != T001L2_A1147FuncaoDados_Tecnica[0] ) || ( Z1258FuncaoDados_Importada != T001L2_A1258FuncaoDados_Importada[0] ) || ( Z1267FuncaoDados_UpdAoImpBsln != T001L2_A1267FuncaoDados_UpdAoImpBsln[0] ) || ( StringUtil.StrCmp(Z394FuncaoDados_Ativo, T001L2_A394FuncaoDados_Ativo[0]) != 0 ) || ( Z370FuncaoDados_SistemaCod != T001L2_A370FuncaoDados_SistemaCod[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z391FuncaoDados_FuncaoDadosCod != T001L2_A391FuncaoDados_FuncaoDadosCod[0] ) || ( Z745FuncaoDados_MelhoraCod != T001L2_A745FuncaoDados_MelhoraCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z373FuncaoDados_Tipo, T001L2_A373FuncaoDados_Tipo[0]) != 0 )
               {
                  GXUtil.WriteLog("funcaodados:[seudo value changed for attri]"+"FuncaoDados_Tipo");
                  GXUtil.WriteLogRaw("Old: ",Z373FuncaoDados_Tipo);
                  GXUtil.WriteLogRaw("Current: ",T001L2_A373FuncaoDados_Tipo[0]);
               }
               if ( StringUtil.StrCmp(Z369FuncaoDados_Nome, T001L2_A369FuncaoDados_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("funcaodados:[seudo value changed for attri]"+"FuncaoDados_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z369FuncaoDados_Nome);
                  GXUtil.WriteLogRaw("Current: ",T001L2_A369FuncaoDados_Nome[0]);
               }
               if ( Z1024FuncaoDados_DERImp != T001L2_A1024FuncaoDados_DERImp[0] )
               {
                  GXUtil.WriteLog("funcaodados:[seudo value changed for attri]"+"FuncaoDados_DERImp");
                  GXUtil.WriteLogRaw("Old: ",Z1024FuncaoDados_DERImp);
                  GXUtil.WriteLogRaw("Current: ",T001L2_A1024FuncaoDados_DERImp[0]);
               }
               if ( Z1025FuncaoDados_RAImp != T001L2_A1025FuncaoDados_RAImp[0] )
               {
                  GXUtil.WriteLog("funcaodados:[seudo value changed for attri]"+"FuncaoDados_RAImp");
                  GXUtil.WriteLogRaw("Old: ",Z1025FuncaoDados_RAImp);
                  GXUtil.WriteLogRaw("Current: ",T001L2_A1025FuncaoDados_RAImp[0]);
               }
               if ( Z755FuncaoDados_Contar != T001L2_A755FuncaoDados_Contar[0] )
               {
                  GXUtil.WriteLog("funcaodados:[seudo value changed for attri]"+"FuncaoDados_Contar");
                  GXUtil.WriteLogRaw("Old: ",Z755FuncaoDados_Contar);
                  GXUtil.WriteLogRaw("Current: ",T001L2_A755FuncaoDados_Contar[0]);
               }
               if ( Z1147FuncaoDados_Tecnica != T001L2_A1147FuncaoDados_Tecnica[0] )
               {
                  GXUtil.WriteLog("funcaodados:[seudo value changed for attri]"+"FuncaoDados_Tecnica");
                  GXUtil.WriteLogRaw("Old: ",Z1147FuncaoDados_Tecnica);
                  GXUtil.WriteLogRaw("Current: ",T001L2_A1147FuncaoDados_Tecnica[0]);
               }
               if ( Z1258FuncaoDados_Importada != T001L2_A1258FuncaoDados_Importada[0] )
               {
                  GXUtil.WriteLog("funcaodados:[seudo value changed for attri]"+"FuncaoDados_Importada");
                  GXUtil.WriteLogRaw("Old: ",Z1258FuncaoDados_Importada);
                  GXUtil.WriteLogRaw("Current: ",T001L2_A1258FuncaoDados_Importada[0]);
               }
               if ( Z1267FuncaoDados_UpdAoImpBsln != T001L2_A1267FuncaoDados_UpdAoImpBsln[0] )
               {
                  GXUtil.WriteLog("funcaodados:[seudo value changed for attri]"+"FuncaoDados_UpdAoImpBsln");
                  GXUtil.WriteLogRaw("Old: ",Z1267FuncaoDados_UpdAoImpBsln);
                  GXUtil.WriteLogRaw("Current: ",T001L2_A1267FuncaoDados_UpdAoImpBsln[0]);
               }
               if ( StringUtil.StrCmp(Z394FuncaoDados_Ativo, T001L2_A394FuncaoDados_Ativo[0]) != 0 )
               {
                  GXUtil.WriteLog("funcaodados:[seudo value changed for attri]"+"FuncaoDados_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z394FuncaoDados_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T001L2_A394FuncaoDados_Ativo[0]);
               }
               if ( Z370FuncaoDados_SistemaCod != T001L2_A370FuncaoDados_SistemaCod[0] )
               {
                  GXUtil.WriteLog("funcaodados:[seudo value changed for attri]"+"FuncaoDados_SistemaCod");
                  GXUtil.WriteLogRaw("Old: ",Z370FuncaoDados_SistemaCod);
                  GXUtil.WriteLogRaw("Current: ",T001L2_A370FuncaoDados_SistemaCod[0]);
               }
               if ( Z391FuncaoDados_FuncaoDadosCod != T001L2_A391FuncaoDados_FuncaoDadosCod[0] )
               {
                  GXUtil.WriteLog("funcaodados:[seudo value changed for attri]"+"FuncaoDados_FuncaoDadosCod");
                  GXUtil.WriteLogRaw("Old: ",Z391FuncaoDados_FuncaoDadosCod);
                  GXUtil.WriteLogRaw("Current: ",T001L2_A391FuncaoDados_FuncaoDadosCod[0]);
               }
               if ( Z745FuncaoDados_MelhoraCod != T001L2_A745FuncaoDados_MelhoraCod[0] )
               {
                  GXUtil.WriteLog("funcaodados:[seudo value changed for attri]"+"FuncaoDados_MelhoraCod");
                  GXUtil.WriteLogRaw("Old: ",Z745FuncaoDados_MelhoraCod);
                  GXUtil.WriteLogRaw("Current: ",T001L2_A745FuncaoDados_MelhoraCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"FuncaoDados"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1L59( )
      {
         BeforeValidate1L59( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1L59( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1L59( 0) ;
            CheckOptimisticConcurrency1L59( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1L59( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1L59( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001L16 */
                     pr_default.execute(14, new Object[] {A373FuncaoDados_Tipo, A369FuncaoDados_Nome, n397FuncaoDados_Descricao, A397FuncaoDados_Descricao, n1024FuncaoDados_DERImp, A1024FuncaoDados_DERImp, n1025FuncaoDados_RAImp, A1025FuncaoDados_RAImp, n755FuncaoDados_Contar, A755FuncaoDados_Contar, n1147FuncaoDados_Tecnica, A1147FuncaoDados_Tecnica, n1245FuncaoDados_Observacao, A1245FuncaoDados_Observacao, n1258FuncaoDados_Importada, A1258FuncaoDados_Importada, n1267FuncaoDados_UpdAoImpBsln, A1267FuncaoDados_UpdAoImpBsln, A394FuncaoDados_Ativo, A370FuncaoDados_SistemaCod, n391FuncaoDados_FuncaoDadosCod, A391FuncaoDados_FuncaoDadosCod, n745FuncaoDados_MelhoraCod, A745FuncaoDados_MelhoraCod});
                     A368FuncaoDados_Codigo = T001L16_A368FuncaoDados_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
                     pr_default.close(14);
                     dsDefault.SmartCacheProvider.SetUpdated("FuncaoDados") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption1L0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1L59( ) ;
            }
            EndLevel1L59( ) ;
         }
         CloseExtendedTableCursors1L59( ) ;
      }

      protected void Update1L59( )
      {
         BeforeValidate1L59( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1L59( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1L59( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1L59( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1L59( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001L17 */
                     pr_default.execute(15, new Object[] {A373FuncaoDados_Tipo, A369FuncaoDados_Nome, n397FuncaoDados_Descricao, A397FuncaoDados_Descricao, n1024FuncaoDados_DERImp, A1024FuncaoDados_DERImp, n1025FuncaoDados_RAImp, A1025FuncaoDados_RAImp, n755FuncaoDados_Contar, A755FuncaoDados_Contar, n1147FuncaoDados_Tecnica, A1147FuncaoDados_Tecnica, n1245FuncaoDados_Observacao, A1245FuncaoDados_Observacao, n1258FuncaoDados_Importada, A1258FuncaoDados_Importada, n1267FuncaoDados_UpdAoImpBsln, A1267FuncaoDados_UpdAoImpBsln, A394FuncaoDados_Ativo, A370FuncaoDados_SistemaCod, n391FuncaoDados_FuncaoDadosCod, A391FuncaoDados_FuncaoDadosCod, n745FuncaoDados_MelhoraCod, A745FuncaoDados_MelhoraCod, A368FuncaoDados_Codigo});
                     pr_default.close(15);
                     dsDefault.SmartCacheProvider.SetUpdated("FuncaoDados") ;
                     if ( (pr_default.getStatus(15) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"FuncaoDados"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1L59( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1L59( ) ;
         }
         CloseExtendedTableCursors1L59( ) ;
      }

      protected void DeferredUpdate1L59( )
      {
      }

      protected void delete( )
      {
         BeforeValidate1L59( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1L59( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1L59( ) ;
            AfterConfirm1L59( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1L59( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001L18 */
                  pr_default.execute(16, new Object[] {A368FuncaoDados_Codigo});
                  pr_default.close(16);
                  dsDefault.SmartCacheProvider.SetUpdated("FuncaoDados") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode59 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel1L59( ) ;
         Gx_mode = sMode59;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls1L59( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            GXt_char2 = A376FuncaoDados_Complexidade;
            new prc_fdcomplexidade(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
            A376FuncaoDados_Complexidade = GXt_char2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A376FuncaoDados_Complexidade", A376FuncaoDados_Complexidade);
            GXt_int3 = A375FuncaoDados_RLR;
            new prc_funcaodados_rlr(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int3) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
            A375FuncaoDados_RLR = GXt_int3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A375FuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(A375FuncaoDados_RLR), 4, 0)));
            GXt_int3 = A374FuncaoDados_DER;
            new prc_funcaodados_der(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int3) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
            A374FuncaoDados_DER = GXt_int3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A374FuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(A374FuncaoDados_DER), 4, 0)));
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV16FuncaoDados_Tipo = A373FuncaoDados_Tipo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16FuncaoDados_Tipo", AV16FuncaoDados_Tipo);
            }
            A419FuncaoDados_Nome50 = StringUtil.Substring( A369FuncaoDados_Nome, 1, 50);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A419FuncaoDados_Nome50", A419FuncaoDados_Nome50);
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV20FuncaoDados_Nome = A369FuncaoDados_Nome;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20FuncaoDados_Nome", AV20FuncaoDados_Nome);
            }
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV17FuncaoDados_Descricao = A397FuncaoDados_Descricao;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoDados_Descricao", AV17FuncaoDados_Descricao);
            }
            if ( A755FuncaoDados_Contar )
            {
               GXt_int3 = (short)(A377FuncaoDados_PF);
               new prc_fdpf(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_int3) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
               A377FuncaoDados_PF = (decimal)(GXt_int3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A377FuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( A377FuncaoDados_PF, 14, 5)));
            }
            else
            {
               A377FuncaoDados_PF = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A377FuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( A377FuncaoDados_PF, 14, 5)));
            }
            /* Using cursor T001L19 */
            pr_default.execute(17, new Object[] {A370FuncaoDados_SistemaCod});
            A371FuncaoDados_SistemaDes = T001L19_A371FuncaoDados_SistemaDes[0];
            n371FuncaoDados_SistemaDes = T001L19_n371FuncaoDados_SistemaDes[0];
            A705FuncaoDados_SistemaTecnica = T001L19_A705FuncaoDados_SistemaTecnica[0];
            n705FuncaoDados_SistemaTecnica = T001L19_n705FuncaoDados_SistemaTecnica[0];
            A372FuncaoDados_SistemaAreaCod = T001L19_A372FuncaoDados_SistemaAreaCod[0];
            n372FuncaoDados_SistemaAreaCod = T001L19_n372FuncaoDados_SistemaAreaCod[0];
            pr_default.close(17);
            Dvpanel_tableattributes_Title = "Fun��o de Dados do Sistema: "+A371FuncaoDados_SistemaDes;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvpanel_tableattributes_Internalname, "Title", Dvpanel_tableattributes_Title);
            chkavFlag.Visible = (((StringUtil.StringSearch( A705FuncaoDados_SistemaTecnica, "IE", 1)==0)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavFlag_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavFlag.Visible), 5, 0)));
            lblTextblockflag_Visible = (((StringUtil.StringSearch( A705FuncaoDados_SistemaTecnica, "IE", 1)==0)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockflag_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockflag_Visible), 5, 0)));
            GXVvCOMBO_SISTEMACOD_html1L59( A370FuncaoDados_SistemaCod) ;
            /* Using cursor T001L20 */
            pr_default.execute(18, new Object[] {n391FuncaoDados_FuncaoDadosCod, A391FuncaoDados_FuncaoDadosCod});
            A402FuncaoDados_FuncaoDadosNom = T001L20_A402FuncaoDados_FuncaoDadosNom[0];
            n402FuncaoDados_FuncaoDadosNom = T001L20_n402FuncaoDados_FuncaoDadosNom[0];
            A403FuncaoDados_FuncaoDadosDes = T001L20_A403FuncaoDados_FuncaoDadosDes[0];
            n403FuncaoDados_FuncaoDadosDes = T001L20_n403FuncaoDados_FuncaoDadosDes[0];
            A404FuncaoDados_FuncaoDadosSisCod = T001L20_A404FuncaoDados_FuncaoDadosSisCod[0];
            n404FuncaoDados_FuncaoDadosSisCod = T001L20_n404FuncaoDados_FuncaoDadosSisCod[0];
            pr_default.close(18);
            /* Using cursor T001L21 */
            pr_default.execute(19, new Object[] {n404FuncaoDados_FuncaoDadosSisCod, A404FuncaoDados_FuncaoDadosSisCod});
            A405FuncaoDados_FuncaoDadosSisDes = T001L21_A405FuncaoDados_FuncaoDadosSisDes[0];
            n405FuncaoDados_FuncaoDadosSisDes = T001L21_n405FuncaoDados_FuncaoDadosSisDes[0];
            pr_default.close(19);
            if ( ! (0==A404FuncaoDados_FuncaoDadosSisCod) && ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV15Combo_SistemaCod = A404FuncaoDados_FuncaoDadosSisCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Combo_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Combo_SistemaCod), 6, 0)));
            }
            else
            {
               if ( (0==A404FuncaoDados_FuncaoDadosSisCod) && ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
               {
                  AV15Combo_SistemaCod = AV13FuncaoDados_SistemaCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Combo_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Combo_SistemaCod), 6, 0)));
               }
            }
            A420FuncaoDados_CodigoAliAie = (short)((T001L3_n391FuncaoDados_FuncaoDadosCod[0] ? A368FuncaoDados_Codigo : A391FuncaoDados_FuncaoDadosCod));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A420FuncaoDados_CodigoAliAie", StringUtil.LTrim( StringUtil.Str( (decimal)(A420FuncaoDados_CodigoAliAie), 4, 0)));
            lblTextblockcombo_sistemacod_Visible = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||!(0==A391FuncaoDados_FuncaoDadosCod) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcombo_sistemacod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcombo_sistemacod_Visible), 5, 0)));
            dynavCombo_sistemacod.Visible = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||!(0==A391FuncaoDados_FuncaoDadosCod) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCombo_sistemacod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavCombo_sistemacod.Visible), 5, 0)));
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV18FuncaoDados_FuncaoDadosCod = A391FuncaoDados_FuncaoDadosCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18FuncaoDados_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18FuncaoDados_FuncaoDadosCod), 6, 0)));
            }
            GXt_char2 = A753FuncaoDados_SolucaoTecnica;
            GXt_int4 = 0;
            new prc_solucaotecnica(context ).execute( ref  A745FuncaoDados_MelhoraCod, ref  GXt_int4,  "D", out  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A745FuncaoDados_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A745FuncaoDados_MelhoraCod), 6, 0)));
            A753FuncaoDados_SolucaoTecnica = GXt_char2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A753FuncaoDados_SolucaoTecnica", A753FuncaoDados_SolucaoTecnica);
            A754FuncaoDados_SolucaoTecnica30 = A753FuncaoDados_SolucaoTecnica;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A754FuncaoDados_SolucaoTecnica30", A754FuncaoDados_SolucaoTecnica30);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T001L22 */
            pr_default.execute(20, new Object[] {A368FuncaoDados_Codigo});
            if ( (pr_default.getStatus(20) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Grupo L�gico de Dados"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(20);
            /* Using cursor T001L23 */
            pr_default.execute(21, new Object[] {A368FuncaoDados_Codigo});
            if ( (pr_default.getStatus(21) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Grupo L�gico de Dados"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(21);
            /* Using cursor T001L24 */
            pr_default.execute(22, new Object[] {A368FuncaoDados_Codigo});
            if ( (pr_default.getStatus(22) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Funcao Dados Pre Importa��o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(22);
            /* Using cursor T001L25 */
            pr_default.execute(23, new Object[] {A368FuncaoDados_Codigo});
            if ( (pr_default.getStatus(23) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Projeto Melhoria"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(23);
            /* Using cursor T001L26 */
            pr_default.execute(24, new Object[] {A368FuncaoDados_Codigo});
            if ( (pr_default.getStatus(24) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Atributos das Fun��es de APF"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(24);
            /* Using cursor T001L27 */
            pr_default.execute(25, new Object[] {A368FuncaoDados_Codigo});
            if ( (pr_default.getStatus(25) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Funcao Dados Tabela"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(25);
         }
      }

      protected void EndLevel1L59( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1L59( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(17);
            pr_default.close(18);
            pr_default.close(19);
            context.CommitDataStores( "FuncaoDados");
            if ( AnyError == 0 )
            {
               ConfirmValues1L0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(17);
            pr_default.close(18);
            pr_default.close(19);
            context.RollbackDataStores( "FuncaoDados");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1L59( )
      {
         /* Scan By routine */
         /* Using cursor T001L28 */
         pr_default.execute(26);
         RcdFound59 = 0;
         if ( (pr_default.getStatus(26) != 101) )
         {
            RcdFound59 = 1;
            A368FuncaoDados_Codigo = T001L28_A368FuncaoDados_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1L59( )
      {
         /* Scan next routine */
         pr_default.readNext(26);
         RcdFound59 = 0;
         if ( (pr_default.getStatus(26) != 101) )
         {
            RcdFound59 = 1;
            A368FuncaoDados_Codigo = T001L28_A368FuncaoDados_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd1L59( )
      {
         pr_default.close(26);
      }

      protected void AfterConfirm1L59( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1L59( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1L59( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1L59( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1L59( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1L59( )
      {
         /* Before Validate Rules */
         A373FuncaoDados_Tipo = AV16FuncaoDados_Tipo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A373FuncaoDados_Tipo", A373FuncaoDados_Tipo);
         A370FuncaoDados_SistemaCod = AV13FuncaoDados_SistemaCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A370FuncaoDados_SistemaCod), 6, 0)));
         A369FuncaoDados_Nome = AV20FuncaoDados_Nome;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A369FuncaoDados_Nome", A369FuncaoDados_Nome);
         A397FuncaoDados_Descricao = AV17FuncaoDados_Descricao;
         n397FuncaoDados_Descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A397FuncaoDados_Descricao", A397FuncaoDados_Descricao);
         if ( ! (0==AV18FuncaoDados_FuncaoDadosCod) )
         {
            A391FuncaoDados_FuncaoDadosCod = AV18FuncaoDados_FuncaoDadosCod;
            n391FuncaoDados_FuncaoDadosCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A391FuncaoDados_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0)));
         }
         if ( ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  ) && ( StringUtil.StrCmp(A369FuncaoDados_Nome, O369FuncaoDados_Nome) != 0 ) && new prc_existenomecadastrado(context).executeUdp(  "FnD",  A369FuncaoDados_Nome) )
         {
            GX_msglist.addItem("Nome de Fun��o de Dados j� cadastrado!", 1, "");
            AnyError = 1;
         }
      }

      protected void DisableAttributes1L59( )
      {
         edtavFuncaodados_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaodados_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_nome_Enabled), 5, 0)));
         dynavCombo_sistemacod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCombo_sistemacod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavCombo_sistemacod.Enabled), 5, 0)));
         edtavFuncaodados_descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaodados_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_descricao_Enabled), 5, 0)));
         cmbavFuncaodados_tipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaodados_tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaodados_tipo.Enabled), 5, 0)));
         edtFuncaoDados_DER_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_DER_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_DER_Enabled), 5, 0)));
         edtFuncaoDados_RLR_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_RLR_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_RLR_Enabled), 5, 0)));
         cmbFuncaoDados_Complexidade.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_Complexidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbFuncaoDados_Complexidade.Enabled), 5, 0)));
         edtFuncaoDados_PF_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_PF_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_PF_Enabled), 5, 0)));
         cmbFuncaoDados_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbFuncaoDados_Ativo.Enabled), 5, 0)));
         cmbFuncaoDados_UpdAoImpBsln.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_UpdAoImpBsln_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbFuncaoDados_UpdAoImpBsln.Enabled), 5, 0)));
         chkavFlag.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavFlag_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavFlag.Enabled), 5, 0)));
         edtFuncaoDados_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_Codigo_Enabled), 5, 0)));
         Funcaodados_observacao_Enabled = Convert.ToBoolean( 0);
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Funcaodados_observacao_Internalname, "Enabled", StringUtil.BoolToStr( Funcaodados_observacao_Enabled));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues1L0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020548385827");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("funcaodados.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7FuncaoDados_Codigo) + "," + UrlEncode("" +AV13FuncaoDados_SistemaCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z368FuncaoDados_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z373FuncaoDados_Tipo", StringUtil.RTrim( Z373FuncaoDados_Tipo));
         GxWebStd.gx_hidden_field( context, "Z369FuncaoDados_Nome", Z369FuncaoDados_Nome);
         GxWebStd.gx_hidden_field( context, "Z1024FuncaoDados_DERImp", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1024FuncaoDados_DERImp), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1025FuncaoDados_RAImp", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1025FuncaoDados_RAImp), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z755FuncaoDados_Contar", Z755FuncaoDados_Contar);
         GxWebStd.gx_hidden_field( context, "Z1147FuncaoDados_Tecnica", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1147FuncaoDados_Tecnica), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1258FuncaoDados_Importada", context.localUtil.DToC( Z1258FuncaoDados_Importada, 0, "/"));
         GxWebStd.gx_boolean_hidden_field( context, "Z1267FuncaoDados_UpdAoImpBsln", Z1267FuncaoDados_UpdAoImpBsln);
         GxWebStd.gx_hidden_field( context, "Z394FuncaoDados_Ativo", StringUtil.RTrim( Z394FuncaoDados_Ativo));
         GxWebStd.gx_hidden_field( context, "Z370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z370FuncaoDados_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z391FuncaoDados_FuncaoDadosCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z391FuncaoDados_FuncaoDadosCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z745FuncaoDados_MelhoraCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z745FuncaoDados_MelhoraCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "O397FuncaoDados_Descricao", O397FuncaoDados_Descricao);
         GxWebStd.gx_hidden_field( context, "O369FuncaoDados_Nome", O369FuncaoDados_Nome);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A370FuncaoDados_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N391FuncaoDados_FuncaoDadosCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N745FuncaoDados_MelhoraCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A745FuncaoDados_MelhoraCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_FUNCAODADOSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_CODIGOALIAIE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A420FuncaoDados_CodigoAliAie), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "FUNCAODADOS_CONTAR", A755FuncaoDados_Contar);
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_NOME", A369FuncaoDados_Nome);
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_NOME50", A419FuncaoDados_Nome50);
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_SOLUCAOTECNICA", A753FuncaoDados_SolucaoTecnica);
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_SOLUCAOTECNICA30", StringUtil.RTrim( A754FuncaoDados_SolucaoTecnica30));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_MELHORACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A745FuncaoDados_MelhoraCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vFUNCAODADOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7FuncaoDados_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_FUNCAODADOS_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_FuncaoDados_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A370FuncaoDados_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vFUNCAODADOS_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13FuncaoDados_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_FUNCAODADOS_FUNCAODADOSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14Insert_FuncaoDados_FuncaoDadosCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vFUNCAODADOS_FUNCAODADOSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18FuncaoDados_FuncaoDadosCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_FUNCAODADOS_MELHORACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23Insert_FuncaoDados_MelhoraCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_SISTEMADES", A371FuncaoDados_SistemaDes);
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_SISTEMATECNICA", StringUtil.RTrim( A705FuncaoDados_SistemaTecnica));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_FUNCAODADOSSISCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A404FuncaoDados_FuncaoDadosSisCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_TIPO", StringUtil.RTrim( A373FuncaoDados_Tipo));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_DESCRICAO", A397FuncaoDados_Descricao);
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_DERIMP", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1024FuncaoDados_DERImp), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_RAIMP", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1025FuncaoDados_RAImp), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_TECNICA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1147FuncaoDados_Tecnica), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_OBSERVACAO", A1245FuncaoDados_Observacao);
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_IMPORTADA", context.localUtil.DToC( A1258FuncaoDados_Importada, 0, "/"));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_SISTEMAAREACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A372FuncaoDados_SistemaAreaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_FUNCAODADOSNOM", A402FuncaoDados_FuncaoDadosNom);
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_FUNCAODADOSDES", A403FuncaoDados_FuncaoDadosDes);
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_FUNCAODADOSSISDES", A405FuncaoDados_FuncaoDadosSisDes);
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV28Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vFUNCAODADOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7FuncaoDados_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_OBSERVACAO_Enabled", StringUtil.BoolToStr( Funcaodados_observacao_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "FuncaoDados";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A745FuncaoDados_MelhoraCod), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1024FuncaoDados_DERImp), "ZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1025FuncaoDados_RAImp), "ZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A755FuncaoDados_Contar);
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1147FuncaoDados_Tecnica), "9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format(A1258FuncaoDados_Importada, "99/99/99");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("funcaodados:[SendSecurityCheck value for]"+"FuncaoDados_Codigo:"+context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("funcaodados:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("funcaodados:[SendSecurityCheck value for]"+"FuncaoDados_MelhoraCod:"+context.localUtil.Format( (decimal)(A745FuncaoDados_MelhoraCod), "ZZZZZ9"));
         GXUtil.WriteLog("funcaodados:[SendSecurityCheck value for]"+"FuncaoDados_DERImp:"+context.localUtil.Format( (decimal)(A1024FuncaoDados_DERImp), "ZZZ9"));
         GXUtil.WriteLog("funcaodados:[SendSecurityCheck value for]"+"FuncaoDados_RAImp:"+context.localUtil.Format( (decimal)(A1025FuncaoDados_RAImp), "ZZZ9"));
         GXUtil.WriteLog("funcaodados:[SendSecurityCheck value for]"+"FuncaoDados_Contar:"+StringUtil.BoolToStr( A755FuncaoDados_Contar));
         GXUtil.WriteLog("funcaodados:[SendSecurityCheck value for]"+"FuncaoDados_Tecnica:"+context.localUtil.Format( (decimal)(A1147FuncaoDados_Tecnica), "9"));
         GXUtil.WriteLog("funcaodados:[SendSecurityCheck value for]"+"FuncaoDados_Importada:"+context.localUtil.Format(A1258FuncaoDados_Importada, "99/99/99"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("funcaodados.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7FuncaoDados_Codigo) + "," + UrlEncode("" +AV13FuncaoDados_SistemaCod) ;
      }

      public override String GetPgmname( )
      {
         return "FuncaoDados" ;
      }

      public override String GetPgmdesc( )
      {
         return "Grupo L�gico de Dados" ;
      }

      protected void InitializeNonKey1L59( )
      {
         A370FuncaoDados_SistemaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A370FuncaoDados_SistemaCod), 6, 0)));
         A391FuncaoDados_FuncaoDadosCod = 0;
         n391FuncaoDados_FuncaoDadosCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A391FuncaoDados_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0)));
         A745FuncaoDados_MelhoraCod = 0;
         n745FuncaoDados_MelhoraCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A745FuncaoDados_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A745FuncaoDados_MelhoraCod), 6, 0)));
         AV20FuncaoDados_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20FuncaoDados_Nome", AV20FuncaoDados_Nome);
         AV15Combo_SistemaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Combo_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Combo_SistemaCod), 6, 0)));
         AV17FuncaoDados_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoDados_Descricao", AV17FuncaoDados_Descricao);
         AV21Flag = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Flag", AV21Flag);
         AV18FuncaoDados_FuncaoDadosCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18FuncaoDados_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18FuncaoDados_FuncaoDadosCod), 6, 0)));
         A373FuncaoDados_Tipo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A373FuncaoDados_Tipo", A373FuncaoDados_Tipo);
         A753FuncaoDados_SolucaoTecnica = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A753FuncaoDados_SolucaoTecnica", A753FuncaoDados_SolucaoTecnica);
         A754FuncaoDados_SolucaoTecnica30 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A754FuncaoDados_SolucaoTecnica30", A754FuncaoDados_SolucaoTecnica30);
         A419FuncaoDados_Nome50 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A419FuncaoDados_Nome50", A419FuncaoDados_Nome50);
         A374FuncaoDados_DER = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A374FuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(A374FuncaoDados_DER), 4, 0)));
         A375FuncaoDados_RLR = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A375FuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(A375FuncaoDados_RLR), 4, 0)));
         A376FuncaoDados_Complexidade = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A376FuncaoDados_Complexidade", A376FuncaoDados_Complexidade);
         A377FuncaoDados_PF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A377FuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( A377FuncaoDados_PF, 14, 5)));
         A420FuncaoDados_CodigoAliAie = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A420FuncaoDados_CodigoAliAie", StringUtil.LTrim( StringUtil.Str( (decimal)(A420FuncaoDados_CodigoAliAie), 4, 0)));
         A369FuncaoDados_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A369FuncaoDados_Nome", A369FuncaoDados_Nome);
         A397FuncaoDados_Descricao = "";
         n397FuncaoDados_Descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A397FuncaoDados_Descricao", A397FuncaoDados_Descricao);
         A371FuncaoDados_SistemaDes = "";
         n371FuncaoDados_SistemaDes = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A371FuncaoDados_SistemaDes", A371FuncaoDados_SistemaDes);
         A372FuncaoDados_SistemaAreaCod = 0;
         n372FuncaoDados_SistemaAreaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A372FuncaoDados_SistemaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A372FuncaoDados_SistemaAreaCod), 6, 0)));
         A705FuncaoDados_SistemaTecnica = "";
         n705FuncaoDados_SistemaTecnica = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A705FuncaoDados_SistemaTecnica", A705FuncaoDados_SistemaTecnica);
         A1024FuncaoDados_DERImp = 0;
         n1024FuncaoDados_DERImp = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1024FuncaoDados_DERImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1024FuncaoDados_DERImp), 4, 0)));
         A1025FuncaoDados_RAImp = 0;
         n1025FuncaoDados_RAImp = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1025FuncaoDados_RAImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1025FuncaoDados_RAImp), 4, 0)));
         A402FuncaoDados_FuncaoDadosNom = "";
         n402FuncaoDados_FuncaoDadosNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A402FuncaoDados_FuncaoDadosNom", A402FuncaoDados_FuncaoDadosNom);
         A403FuncaoDados_FuncaoDadosDes = "";
         n403FuncaoDados_FuncaoDadosDes = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A403FuncaoDados_FuncaoDadosDes", A403FuncaoDados_FuncaoDadosDes);
         A404FuncaoDados_FuncaoDadosSisCod = 0;
         n404FuncaoDados_FuncaoDadosSisCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A404FuncaoDados_FuncaoDadosSisCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A404FuncaoDados_FuncaoDadosSisCod), 6, 0)));
         A405FuncaoDados_FuncaoDadosSisDes = "";
         n405FuncaoDados_FuncaoDadosSisDes = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A405FuncaoDados_FuncaoDadosSisDes", A405FuncaoDados_FuncaoDadosSisDes);
         A1147FuncaoDados_Tecnica = 0;
         n1147FuncaoDados_Tecnica = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1147FuncaoDados_Tecnica", StringUtil.Str( (decimal)(A1147FuncaoDados_Tecnica), 1, 0));
         A1245FuncaoDados_Observacao = "";
         n1245FuncaoDados_Observacao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1245FuncaoDados_Observacao", A1245FuncaoDados_Observacao);
         A1258FuncaoDados_Importada = DateTime.MinValue;
         n1258FuncaoDados_Importada = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1258FuncaoDados_Importada", context.localUtil.Format(A1258FuncaoDados_Importada, "99/99/99"));
         A755FuncaoDados_Contar = true;
         n755FuncaoDados_Contar = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A755FuncaoDados_Contar", A755FuncaoDados_Contar);
         A1267FuncaoDados_UpdAoImpBsln = true;
         n1267FuncaoDados_UpdAoImpBsln = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1267FuncaoDados_UpdAoImpBsln", A1267FuncaoDados_UpdAoImpBsln);
         A394FuncaoDados_Ativo = "A";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A394FuncaoDados_Ativo", A394FuncaoDados_Ativo);
         O397FuncaoDados_Descricao = A397FuncaoDados_Descricao;
         n397FuncaoDados_Descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A397FuncaoDados_Descricao", A397FuncaoDados_Descricao);
         O369FuncaoDados_Nome = A369FuncaoDados_Nome;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A369FuncaoDados_Nome", A369FuncaoDados_Nome);
         Z373FuncaoDados_Tipo = "";
         Z369FuncaoDados_Nome = "";
         Z1024FuncaoDados_DERImp = 0;
         Z1025FuncaoDados_RAImp = 0;
         Z755FuncaoDados_Contar = false;
         Z1147FuncaoDados_Tecnica = 0;
         Z1258FuncaoDados_Importada = DateTime.MinValue;
         Z1267FuncaoDados_UpdAoImpBsln = false;
         Z394FuncaoDados_Ativo = "";
         Z370FuncaoDados_SistemaCod = 0;
         Z391FuncaoDados_FuncaoDadosCod = 0;
         Z745FuncaoDados_MelhoraCod = 0;
      }

      protected void InitAll1L59( )
      {
         A368FuncaoDados_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         InitializeNonKey1L59( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A394FuncaoDados_Ativo = i394FuncaoDados_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A394FuncaoDados_Ativo", A394FuncaoDados_Ativo);
         A1267FuncaoDados_UpdAoImpBsln = i1267FuncaoDados_UpdAoImpBsln;
         n1267FuncaoDados_UpdAoImpBsln = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1267FuncaoDados_UpdAoImpBsln", A1267FuncaoDados_UpdAoImpBsln);
         A755FuncaoDados_Contar = i755FuncaoDados_Contar;
         n755FuncaoDados_Contar = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A755FuncaoDados_Contar", A755FuncaoDados_Contar);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020548385879");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("funcaodados.js", "?2020548385880");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockfuncaodados_nome_Internalname = "TEXTBLOCKFUNCAODADOS_NOME";
         edtavFuncaodados_nome_Internalname = "vFUNCAODADOS_NOME";
         lblTextblockcombo_sistemacod_Internalname = "TEXTBLOCKCOMBO_SISTEMACOD";
         dynavCombo_sistemacod_Internalname = "vCOMBO_SISTEMACOD";
         lblTextblockfuncaodados_descricao_Internalname = "TEXTBLOCKFUNCAODADOS_DESCRICAO";
         edtavFuncaodados_descricao_Internalname = "vFUNCAODADOS_DESCRICAO";
         lblTextblockfuncaodados_tipo_Internalname = "TEXTBLOCKFUNCAODADOS_TIPO";
         cmbavFuncaodados_tipo_Internalname = "vFUNCAODADOS_TIPO";
         lblTextblockfuncaodados_der_Internalname = "TEXTBLOCKFUNCAODADOS_DER";
         edtFuncaoDados_DER_Internalname = "FUNCAODADOS_DER";
         lblTextblockfuncaodados_rlr_Internalname = "TEXTBLOCKFUNCAODADOS_RLR";
         edtFuncaoDados_RLR_Internalname = "FUNCAODADOS_RLR";
         lblTextblockfuncaodados_complexidade_Internalname = "TEXTBLOCKFUNCAODADOS_COMPLEXIDADE";
         cmbFuncaoDados_Complexidade_Internalname = "FUNCAODADOS_COMPLEXIDADE";
         lblTextblockfuncaodados_pf_Internalname = "TEXTBLOCKFUNCAODADOS_PF";
         edtFuncaoDados_PF_Internalname = "FUNCAODADOS_PF";
         lblTextblockfuncaodados_observacao_Internalname = "TEXTBLOCKFUNCAODADOS_OBSERVACAO";
         Funcaodados_observacao_Internalname = "FUNCAODADOS_OBSERVACAO";
         lblTextblockfuncaodados_ativo_Internalname = "TEXTBLOCKFUNCAODADOS_ATIVO";
         cmbFuncaoDados_Ativo_Internalname = "FUNCAODADOS_ATIVO";
         lblTextblockfuncaodados_updaoimpbsln_Internalname = "TEXTBLOCKFUNCAODADOS_UPDAOIMPBSLN";
         cmbFuncaoDados_UpdAoImpBsln_Internalname = "FUNCAODADOS_UPDAOIMPBSLN";
         tblTablemergedfuncaodados_ativo_Internalname = "TABLEMERGEDFUNCAODADOS_ATIVO";
         lblTextblockflag_Internalname = "TEXTBLOCKFLAG";
         chkavFlag_Internalname = "vFLAG";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtneliminar_Internalname = "BTNELIMINAR";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         lblTbjava_Internalname = "TBJAVA";
         lblTextblockmessage_Internalname = "TEXTBLOCKMESSAGE";
         edtavMessage_Internalname = "vMESSAGE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtFuncaoDados_Codigo_Internalname = "FUNCAODADOS_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Grupo L�gico de Dados";
         Dvpanel_tableattributes_Title = "Fun��o de Dados";
         edtavFuncaodados_nome_Height = 3;
         edtavFuncaodados_nome_Width = 80;
         cmbFuncaoDados_UpdAoImpBsln_Jsonclick = "";
         cmbFuncaoDados_UpdAoImpBsln.Enabled = 1;
         cmbFuncaoDados_Ativo_Jsonclick = "";
         cmbFuncaoDados_Ativo.Enabled = 1;
         cmbFuncaoDados_Ativo.Visible = 1;
         chkavFlag.Enabled = 1;
         chkavFlag.Visible = 1;
         lblTextblockflag_Visible = 1;
         lblTextblockfuncaodados_ativo_Visible = 1;
         Funcaodados_observacao_Enabled = Convert.ToBoolean( 1);
         edtFuncaoDados_PF_Jsonclick = "";
         edtFuncaoDados_PF_Enabled = 0;
         cmbFuncaoDados_Complexidade_Jsonclick = "";
         cmbFuncaoDados_Complexidade.Enabled = 0;
         edtFuncaoDados_RLR_Jsonclick = "";
         edtFuncaoDados_RLR_Enabled = 0;
         edtFuncaoDados_DER_Jsonclick = "";
         edtFuncaoDados_DER_Enabled = 0;
         cmbavFuncaodados_tipo_Jsonclick = "";
         cmbavFuncaodados_tipo.Enabled = 1;
         edtavFuncaodados_descricao_Enabled = 1;
         dynavCombo_sistemacod_Jsonclick = "";
         dynavCombo_sistemacod.Enabled = 1;
         dynavCombo_sistemacod.Visible = 1;
         lblTextblockcombo_sistemacod_Caption = "Do Sistema:";
         lblTextblockcombo_sistemacod_Visible = 1;
         edtavFuncaodados_nome_Enabled = 1;
         edtavMessage_Jsonclick = "";
         edtavMessage_Enabled = 0;
         lblTbjava_Caption = "Java";
         lblTbjava_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtneliminar_Visible = 1;
         bttBtn_trn_enter_Caption = "Confirmar";
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtFuncaoDados_Codigo_Jsonclick = "";
         edtFuncaoDados_Codigo_Enabled = 0;
         edtFuncaoDados_Codigo_Visible = 1;
         chkavFlag.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         GXVvCOMBO_SISTEMACOD_html1L59( A370FuncaoDados_SistemaCod) ;
         /* End function dynload_actions */
      }

      protected void GXDLVvCOMBO_SISTEMACOD1L59( int A370FuncaoDados_SistemaCod )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCOMBO_SISTEMACOD_data1L59( A370FuncaoDados_SistemaCod) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCOMBO_SISTEMACOD_html1L59( int A370FuncaoDados_SistemaCod )
      {
         int gxdynajaxvalue ;
         GXDLVvCOMBO_SISTEMACOD_data1L59( A370FuncaoDados_SistemaCod) ;
         gxdynajaxindex = 1;
         dynavCombo_sistemacod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavCombo_sistemacod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLVvCOMBO_SISTEMACOD_data1L59( int A370FuncaoDados_SistemaCod )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhum");
         /* Using cursor T001L29 */
         pr_default.execute(27, new Object[] {A370FuncaoDados_SistemaCod});
         while ( (pr_default.getStatus(27) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T001L29_A127Sistema_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T001L29_A129Sistema_Sigla[0]));
            pr_default.readNext(27);
         }
         pr_default.close(27);
      }

      protected void GX6ASAFUNCAODADOS_PF1L59( bool A755FuncaoDados_Contar ,
                                               int A368FuncaoDados_Codigo )
      {
         if ( A755FuncaoDados_Contar )
         {
            GXt_int3 = (short)(A377FuncaoDados_PF);
            new prc_fdpf(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_int3) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
            A377FuncaoDados_PF = (decimal)(GXt_int3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A377FuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( A377FuncaoDados_PF, 14, 5)));
         }
         else
         {
            A377FuncaoDados_PF = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A377FuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( A377FuncaoDados_PF, 14, 5)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( A377FuncaoDados_PF, 14, 5, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX7ASAFUNCAODADOS_COMPLEXIDADE1L59( int A368FuncaoDados_Codigo )
      {
         GXt_char2 = A376FuncaoDados_Complexidade;
         new prc_fdcomplexidade(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_char2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         A376FuncaoDados_Complexidade = GXt_char2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A376FuncaoDados_Complexidade", A376FuncaoDados_Complexidade);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A376FuncaoDados_Complexidade))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX8ASAFUNCAODADOS_RLR1L59( int A368FuncaoDados_Codigo )
      {
         GXt_int3 = A375FuncaoDados_RLR;
         new prc_funcaodados_rlr(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int3) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         A375FuncaoDados_RLR = GXt_int3;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A375FuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(A375FuncaoDados_RLR), 4, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A375FuncaoDados_RLR), 4, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX9ASAFUNCAODADOS_DER1L59( int A368FuncaoDados_Codigo )
      {
         GXt_int3 = A374FuncaoDados_DER;
         new prc_funcaodados_der(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int3) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         A374FuncaoDados_DER = GXt_int3;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A374FuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(A374FuncaoDados_DER), 4, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A374FuncaoDados_DER), 4, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX12ASAFUNCAODADOS_SOLUCAOTECNICA1L59( int A745FuncaoDados_MelhoraCod )
      {
         GXt_char2 = A753FuncaoDados_SolucaoTecnica;
         GXt_int4 = 0;
         new prc_solucaotecnica(context ).execute( ref  A745FuncaoDados_MelhoraCod, ref  GXt_int4,  "D", out  GXt_char2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A745FuncaoDados_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A745FuncaoDados_MelhoraCod), 6, 0)));
         A753FuncaoDados_SolucaoTecnica = GXt_char2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A753FuncaoDados_SolucaoTecnica", A753FuncaoDados_SolucaoTecnica);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A753FuncaoDados_SolucaoTecnica)+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      public void Valid_Funcaodados_codigo( int GX_Parm1 ,
                                            GXCombobox cmbGX_Parm2 ,
                                            short GX_Parm3 ,
                                            short GX_Parm4 )
      {
         A368FuncaoDados_Codigo = GX_Parm1;
         cmbFuncaoDados_Complexidade = cmbGX_Parm2;
         A376FuncaoDados_Complexidade = cmbFuncaoDados_Complexidade.CurrentValue;
         cmbFuncaoDados_Complexidade.CurrentValue = A376FuncaoDados_Complexidade;
         A375FuncaoDados_RLR = GX_Parm3;
         A374FuncaoDados_DER = GX_Parm4;
         GXt_char2 = A376FuncaoDados_Complexidade;
         new prc_fdcomplexidade(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_char2) ;
         A376FuncaoDados_Complexidade = GXt_char2;
         cmbFuncaoDados_Complexidade.CurrentValue = A376FuncaoDados_Complexidade;
         GXt_int3 = A375FuncaoDados_RLR;
         new prc_funcaodados_rlr(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int3) ;
         A375FuncaoDados_RLR = GXt_int3;
         GXt_int3 = A374FuncaoDados_DER;
         new prc_funcaodados_der(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int3) ;
         A374FuncaoDados_DER = GXt_int3;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         cmbFuncaoDados_Complexidade.CurrentValue = A376FuncaoDados_Complexidade;
         isValidOutput.Add(cmbFuncaoDados_Complexidade);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A375FuncaoDados_RLR), 4, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A374FuncaoDados_DER), 4, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7FuncaoDados_Codigo',fld:'vFUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV13FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E121L2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV21Flag',fld:'vFLAG',pic:'',nv:false},{av:'AV20FuncaoDados_Nome',fld:'vFUNCAODADOS_NOME',pic:'',nv:''},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV15Combo_SistemaCod',fld:'vCOMBO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("'DOELIMINAR'","{handler:'E131L2',iparms:[{av:'AV7FuncaoDados_Codigo',fld:'vFUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("VCOMBO_SISTEMACOD.CLICK","{handler:'E141L2',iparms:[{av:'AV15Combo_SistemaCod',fld:'vCOMBO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV13FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A369FuncaoDados_Nome',fld:'FUNCAODADOS_NOME',pic:'',nv:''},{av:'A397FuncaoDados_Descricao',fld:'FUNCAODADOS_DESCRICAO',pic:'',nv:''},{av:'AV18FuncaoDados_FuncaoDadosCod',fld:'vFUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV20FuncaoDados_Nome',fld:'vFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV17FuncaoDados_Descricao',fld:'vFUNCAODADOS_DESCRICAO',pic:'',nv:''}],oparms:[{av:'edtavFuncaodados_nome_Enabled',ctrl:'vFUNCAODADOS_NOME',prop:'Enabled'},{av:'edtavFuncaodados_descricao_Enabled',ctrl:'vFUNCAODADOS_DESCRICAO',prop:'Enabled'},{ctrl:'BTN_TRN_ENTER',prop:'Caption'},{av:'AV20FuncaoDados_Nome',fld:'vFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV17FuncaoDados_Descricao',fld:'vFUNCAODADOS_DESCRICAO',pic:'',nv:''},{av:'AV18FuncaoDados_FuncaoDadosCod',fld:'vFUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV16FuncaoDados_Tipo',fld:'vFUNCAODADOS_TIPO',pic:'',nv:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(17);
         pr_default.close(18);
         pr_default.close(19);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z373FuncaoDados_Tipo = "";
         Z369FuncaoDados_Nome = "";
         Z1258FuncaoDados_Importada = DateTime.MinValue;
         Z394FuncaoDados_Ativo = "";
         O397FuncaoDados_Descricao = "";
         O369FuncaoDados_Nome = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         AV16FuncaoDados_Tipo = "";
         A376FuncaoDados_Complexidade = "";
         A394FuncaoDados_Ativo = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtneliminar_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         lblTbjava_Jsonclick = "";
         lblTextblockmessage_Jsonclick = "";
         lblTextblockfuncaodados_nome_Jsonclick = "";
         AV20FuncaoDados_Nome = "";
         lblTextblockcombo_sistemacod_Jsonclick = "";
         lblTextblockfuncaodados_descricao_Jsonclick = "";
         AV17FuncaoDados_Descricao = "";
         lblTextblockfuncaodados_tipo_Jsonclick = "";
         lblTextblockfuncaodados_der_Jsonclick = "";
         lblTextblockfuncaodados_rlr_Jsonclick = "";
         lblTextblockfuncaodados_complexidade_Jsonclick = "";
         lblTextblockfuncaodados_pf_Jsonclick = "";
         lblTextblockfuncaodados_observacao_Jsonclick = "";
         lblTextblockfuncaodados_ativo_Jsonclick = "";
         lblTextblockflag_Jsonclick = "";
         lblTextblockfuncaodados_updaoimpbsln_Jsonclick = "";
         A1258FuncaoDados_Importada = DateTime.MinValue;
         A373FuncaoDados_Tipo = "";
         A369FuncaoDados_Nome = "";
         A397FuncaoDados_Descricao = "";
         A419FuncaoDados_Nome50 = "";
         A753FuncaoDados_SolucaoTecnica = "";
         A754FuncaoDados_SolucaoTecnica30 = "";
         A371FuncaoDados_SistemaDes = "";
         A705FuncaoDados_SistemaTecnica = "";
         A1245FuncaoDados_Observacao = "";
         A402FuncaoDados_FuncaoDadosNom = "";
         A403FuncaoDados_FuncaoDadosDes = "";
         A405FuncaoDados_FuncaoDadosSisDes = "";
         AV28Pgmname = "";
         Funcaodados_observacao_Width = "";
         Funcaodados_observacao_Height = "";
         Funcaodados_observacao_Skin = "";
         Funcaodados_observacao_Toolbar = "";
         Funcaodados_observacao_Class = "";
         Funcaodados_observacao_Customtoolbar = "";
         Funcaodados_observacao_Customconfiguration = "";
         Funcaodados_observacao_Buttonpressedid = "";
         Funcaodados_observacao_Captionvalue = "";
         Funcaodados_observacao_Captionclass = "";
         Funcaodados_observacao_Captionposition = "";
         Funcaodados_observacao_Coltitle = "";
         Funcaodados_observacao_Coltitlefont = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode59 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z397FuncaoDados_Descricao = "";
         Z1245FuncaoDados_Observacao = "";
         Z371FuncaoDados_SistemaDes = "";
         Z705FuncaoDados_SistemaTecnica = "";
         Z402FuncaoDados_FuncaoDadosNom = "";
         Z403FuncaoDados_FuncaoDadosDes = "";
         Z405FuncaoDados_FuncaoDadosSisDes = "";
         T001L5_A402FuncaoDados_FuncaoDadosNom = new String[] {""} ;
         T001L5_n402FuncaoDados_FuncaoDadosNom = new bool[] {false} ;
         T001L5_A403FuncaoDados_FuncaoDadosDes = new String[] {""} ;
         T001L5_n403FuncaoDados_FuncaoDadosDes = new bool[] {false} ;
         T001L5_A404FuncaoDados_FuncaoDadosSisCod = new int[1] ;
         T001L5_n404FuncaoDados_FuncaoDadosSisCod = new bool[] {false} ;
         T001L7_A405FuncaoDados_FuncaoDadosSisDes = new String[] {""} ;
         T001L7_n405FuncaoDados_FuncaoDadosSisDes = new bool[] {false} ;
         T001L3_n391FuncaoDados_FuncaoDadosCod = new bool[] {false} ;
         T001L4_A371FuncaoDados_SistemaDes = new String[] {""} ;
         T001L4_n371FuncaoDados_SistemaDes = new bool[] {false} ;
         T001L4_A705FuncaoDados_SistemaTecnica = new String[] {""} ;
         T001L4_n705FuncaoDados_SistemaTecnica = new bool[] {false} ;
         T001L4_A372FuncaoDados_SistemaAreaCod = new int[1] ;
         T001L4_n372FuncaoDados_SistemaAreaCod = new bool[] {false} ;
         T001L8_A368FuncaoDados_Codigo = new int[1] ;
         T001L8_A373FuncaoDados_Tipo = new String[] {""} ;
         T001L8_A369FuncaoDados_Nome = new String[] {""} ;
         T001L8_A397FuncaoDados_Descricao = new String[] {""} ;
         T001L8_n397FuncaoDados_Descricao = new bool[] {false} ;
         T001L8_A371FuncaoDados_SistemaDes = new String[] {""} ;
         T001L8_n371FuncaoDados_SistemaDes = new bool[] {false} ;
         T001L8_A705FuncaoDados_SistemaTecnica = new String[] {""} ;
         T001L8_n705FuncaoDados_SistemaTecnica = new bool[] {false} ;
         T001L8_A1024FuncaoDados_DERImp = new short[1] ;
         T001L8_n1024FuncaoDados_DERImp = new bool[] {false} ;
         T001L8_A1025FuncaoDados_RAImp = new short[1] ;
         T001L8_n1025FuncaoDados_RAImp = new bool[] {false} ;
         T001L8_A402FuncaoDados_FuncaoDadosNom = new String[] {""} ;
         T001L8_n402FuncaoDados_FuncaoDadosNom = new bool[] {false} ;
         T001L8_A403FuncaoDados_FuncaoDadosDes = new String[] {""} ;
         T001L8_n403FuncaoDados_FuncaoDadosDes = new bool[] {false} ;
         T001L8_A405FuncaoDados_FuncaoDadosSisDes = new String[] {""} ;
         T001L8_n405FuncaoDados_FuncaoDadosSisDes = new bool[] {false} ;
         T001L8_A755FuncaoDados_Contar = new bool[] {false} ;
         T001L8_n755FuncaoDados_Contar = new bool[] {false} ;
         T001L8_A1147FuncaoDados_Tecnica = new short[1] ;
         T001L8_n1147FuncaoDados_Tecnica = new bool[] {false} ;
         T001L8_A1245FuncaoDados_Observacao = new String[] {""} ;
         T001L8_n1245FuncaoDados_Observacao = new bool[] {false} ;
         T001L8_A1258FuncaoDados_Importada = new DateTime[] {DateTime.MinValue} ;
         T001L8_n1258FuncaoDados_Importada = new bool[] {false} ;
         T001L8_A1267FuncaoDados_UpdAoImpBsln = new bool[] {false} ;
         T001L8_n1267FuncaoDados_UpdAoImpBsln = new bool[] {false} ;
         T001L8_A394FuncaoDados_Ativo = new String[] {""} ;
         T001L8_A370FuncaoDados_SistemaCod = new int[1] ;
         T001L8_A391FuncaoDados_FuncaoDadosCod = new int[1] ;
         T001L8_n391FuncaoDados_FuncaoDadosCod = new bool[] {false} ;
         T001L8_A745FuncaoDados_MelhoraCod = new int[1] ;
         T001L8_n745FuncaoDados_MelhoraCod = new bool[] {false} ;
         T001L8_A372FuncaoDados_SistemaAreaCod = new int[1] ;
         T001L8_n372FuncaoDados_SistemaAreaCod = new bool[] {false} ;
         T001L8_A404FuncaoDados_FuncaoDadosSisCod = new int[1] ;
         T001L8_n404FuncaoDados_FuncaoDadosSisCod = new bool[] {false} ;
         T001L6_A745FuncaoDados_MelhoraCod = new int[1] ;
         T001L6_n745FuncaoDados_MelhoraCod = new bool[] {false} ;
         T001L9_A371FuncaoDados_SistemaDes = new String[] {""} ;
         T001L9_n371FuncaoDados_SistemaDes = new bool[] {false} ;
         T001L9_A705FuncaoDados_SistemaTecnica = new String[] {""} ;
         T001L9_n705FuncaoDados_SistemaTecnica = new bool[] {false} ;
         T001L9_A372FuncaoDados_SistemaAreaCod = new int[1] ;
         T001L9_n372FuncaoDados_SistemaAreaCod = new bool[] {false} ;
         T001L10_A402FuncaoDados_FuncaoDadosNom = new String[] {""} ;
         T001L10_n402FuncaoDados_FuncaoDadosNom = new bool[] {false} ;
         T001L10_A403FuncaoDados_FuncaoDadosDes = new String[] {""} ;
         T001L10_n403FuncaoDados_FuncaoDadosDes = new bool[] {false} ;
         T001L10_A404FuncaoDados_FuncaoDadosSisCod = new int[1] ;
         T001L10_n404FuncaoDados_FuncaoDadosSisCod = new bool[] {false} ;
         T001L11_A405FuncaoDados_FuncaoDadosSisDes = new String[] {""} ;
         T001L11_n405FuncaoDados_FuncaoDadosSisDes = new bool[] {false} ;
         T001L12_A745FuncaoDados_MelhoraCod = new int[1] ;
         T001L12_n745FuncaoDados_MelhoraCod = new bool[] {false} ;
         T001L13_A368FuncaoDados_Codigo = new int[1] ;
         T001L3_A368FuncaoDados_Codigo = new int[1] ;
         T001L3_A373FuncaoDados_Tipo = new String[] {""} ;
         T001L3_A369FuncaoDados_Nome = new String[] {""} ;
         T001L3_A397FuncaoDados_Descricao = new String[] {""} ;
         T001L3_n397FuncaoDados_Descricao = new bool[] {false} ;
         T001L3_A1024FuncaoDados_DERImp = new short[1] ;
         T001L3_n1024FuncaoDados_DERImp = new bool[] {false} ;
         T001L3_A1025FuncaoDados_RAImp = new short[1] ;
         T001L3_n1025FuncaoDados_RAImp = new bool[] {false} ;
         T001L3_A755FuncaoDados_Contar = new bool[] {false} ;
         T001L3_n755FuncaoDados_Contar = new bool[] {false} ;
         T001L3_A1147FuncaoDados_Tecnica = new short[1] ;
         T001L3_n1147FuncaoDados_Tecnica = new bool[] {false} ;
         T001L3_A1245FuncaoDados_Observacao = new String[] {""} ;
         T001L3_n1245FuncaoDados_Observacao = new bool[] {false} ;
         T001L3_A1258FuncaoDados_Importada = new DateTime[] {DateTime.MinValue} ;
         T001L3_n1258FuncaoDados_Importada = new bool[] {false} ;
         T001L3_A1267FuncaoDados_UpdAoImpBsln = new bool[] {false} ;
         T001L3_n1267FuncaoDados_UpdAoImpBsln = new bool[] {false} ;
         T001L3_A394FuncaoDados_Ativo = new String[] {""} ;
         T001L3_A370FuncaoDados_SistemaCod = new int[1] ;
         T001L3_A391FuncaoDados_FuncaoDadosCod = new int[1] ;
         T001L3_A745FuncaoDados_MelhoraCod = new int[1] ;
         T001L3_n745FuncaoDados_MelhoraCod = new bool[] {false} ;
         T001L14_A368FuncaoDados_Codigo = new int[1] ;
         T001L15_A368FuncaoDados_Codigo = new int[1] ;
         T001L2_A368FuncaoDados_Codigo = new int[1] ;
         T001L2_A373FuncaoDados_Tipo = new String[] {""} ;
         T001L2_A369FuncaoDados_Nome = new String[] {""} ;
         T001L2_A397FuncaoDados_Descricao = new String[] {""} ;
         T001L2_n397FuncaoDados_Descricao = new bool[] {false} ;
         T001L2_A1024FuncaoDados_DERImp = new short[1] ;
         T001L2_n1024FuncaoDados_DERImp = new bool[] {false} ;
         T001L2_A1025FuncaoDados_RAImp = new short[1] ;
         T001L2_n1025FuncaoDados_RAImp = new bool[] {false} ;
         T001L2_A755FuncaoDados_Contar = new bool[] {false} ;
         T001L2_n755FuncaoDados_Contar = new bool[] {false} ;
         T001L2_A1147FuncaoDados_Tecnica = new short[1] ;
         T001L2_n1147FuncaoDados_Tecnica = new bool[] {false} ;
         T001L2_A1245FuncaoDados_Observacao = new String[] {""} ;
         T001L2_n1245FuncaoDados_Observacao = new bool[] {false} ;
         T001L2_A1258FuncaoDados_Importada = new DateTime[] {DateTime.MinValue} ;
         T001L2_n1258FuncaoDados_Importada = new bool[] {false} ;
         T001L2_A1267FuncaoDados_UpdAoImpBsln = new bool[] {false} ;
         T001L2_n1267FuncaoDados_UpdAoImpBsln = new bool[] {false} ;
         T001L2_A394FuncaoDados_Ativo = new String[] {""} ;
         T001L2_A370FuncaoDados_SistemaCod = new int[1] ;
         T001L2_A391FuncaoDados_FuncaoDadosCod = new int[1] ;
         T001L2_n391FuncaoDados_FuncaoDadosCod = new bool[] {false} ;
         T001L2_A745FuncaoDados_MelhoraCod = new int[1] ;
         T001L2_n745FuncaoDados_MelhoraCod = new bool[] {false} ;
         T001L16_A368FuncaoDados_Codigo = new int[1] ;
         T001L19_A371FuncaoDados_SistemaDes = new String[] {""} ;
         T001L19_n371FuncaoDados_SistemaDes = new bool[] {false} ;
         T001L19_A705FuncaoDados_SistemaTecnica = new String[] {""} ;
         T001L19_n705FuncaoDados_SistemaTecnica = new bool[] {false} ;
         T001L19_A372FuncaoDados_SistemaAreaCod = new int[1] ;
         T001L19_n372FuncaoDados_SistemaAreaCod = new bool[] {false} ;
         T001L20_A402FuncaoDados_FuncaoDadosNom = new String[] {""} ;
         T001L20_n402FuncaoDados_FuncaoDadosNom = new bool[] {false} ;
         T001L20_A403FuncaoDados_FuncaoDadosDes = new String[] {""} ;
         T001L20_n403FuncaoDados_FuncaoDadosDes = new bool[] {false} ;
         T001L20_A404FuncaoDados_FuncaoDadosSisCod = new int[1] ;
         T001L20_n404FuncaoDados_FuncaoDadosSisCod = new bool[] {false} ;
         T001L21_A405FuncaoDados_FuncaoDadosSisDes = new String[] {""} ;
         T001L21_n405FuncaoDados_FuncaoDadosSisDes = new bool[] {false} ;
         T001L22_A745FuncaoDados_MelhoraCod = new int[1] ;
         T001L22_n745FuncaoDados_MelhoraCod = new bool[] {false} ;
         T001L23_A391FuncaoDados_FuncaoDadosCod = new int[1] ;
         T001L23_n391FuncaoDados_FuncaoDadosCod = new bool[] {false} ;
         T001L24_A1261FuncaoDadosPreImp_FnDadosCodigo = new int[1] ;
         T001L24_A1259FuncaoDadosPreImp_Sequencial = new short[1] ;
         T001L25_A736ProjetoMelhoria_Codigo = new int[1] ;
         T001L26_A165FuncaoAPF_Codigo = new int[1] ;
         T001L26_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         T001L27_A368FuncaoDados_Codigo = new int[1] ;
         T001L27_A172Tabela_Codigo = new int[1] ;
         T001L28_A368FuncaoDados_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         i394FuncaoDados_Ativo = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T001L29_A127Sistema_Codigo = new int[1] ;
         T001L29_A129Sistema_Sigla = new String[] {""} ;
         T001L29_A699Sistema_Tipo = new String[] {""} ;
         T001L29_n699Sistema_Tipo = new bool[] {false} ;
         GXt_char2 = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.funcaodados__default(),
            new Object[][] {
                new Object[] {
               T001L2_A368FuncaoDados_Codigo, T001L2_A373FuncaoDados_Tipo, T001L2_A369FuncaoDados_Nome, T001L2_A397FuncaoDados_Descricao, T001L2_n397FuncaoDados_Descricao, T001L2_A1024FuncaoDados_DERImp, T001L2_n1024FuncaoDados_DERImp, T001L2_A1025FuncaoDados_RAImp, T001L2_n1025FuncaoDados_RAImp, T001L2_A755FuncaoDados_Contar,
               T001L2_n755FuncaoDados_Contar, T001L2_A1147FuncaoDados_Tecnica, T001L2_n1147FuncaoDados_Tecnica, T001L2_A1245FuncaoDados_Observacao, T001L2_n1245FuncaoDados_Observacao, T001L2_A1258FuncaoDados_Importada, T001L2_n1258FuncaoDados_Importada, T001L2_A1267FuncaoDados_UpdAoImpBsln, T001L2_n1267FuncaoDados_UpdAoImpBsln, T001L2_A394FuncaoDados_Ativo,
               T001L2_A370FuncaoDados_SistemaCod, T001L2_A391FuncaoDados_FuncaoDadosCod, T001L2_n391FuncaoDados_FuncaoDadosCod, T001L2_A745FuncaoDados_MelhoraCod, T001L2_n745FuncaoDados_MelhoraCod
               }
               , new Object[] {
               T001L3_A368FuncaoDados_Codigo, T001L3_A373FuncaoDados_Tipo, T001L3_A369FuncaoDados_Nome, T001L3_A397FuncaoDados_Descricao, T001L3_n397FuncaoDados_Descricao, T001L3_A1024FuncaoDados_DERImp, T001L3_n1024FuncaoDados_DERImp, T001L3_A1025FuncaoDados_RAImp, T001L3_n1025FuncaoDados_RAImp, T001L3_A755FuncaoDados_Contar,
               T001L3_n755FuncaoDados_Contar, T001L3_A1147FuncaoDados_Tecnica, T001L3_n1147FuncaoDados_Tecnica, T001L3_A1245FuncaoDados_Observacao, T001L3_n1245FuncaoDados_Observacao, T001L3_A1258FuncaoDados_Importada, T001L3_n1258FuncaoDados_Importada, T001L3_A1267FuncaoDados_UpdAoImpBsln, T001L3_n1267FuncaoDados_UpdAoImpBsln, T001L3_A394FuncaoDados_Ativo,
               T001L3_A370FuncaoDados_SistemaCod, T001L3_A391FuncaoDados_FuncaoDadosCod, T001L3_n391FuncaoDados_FuncaoDadosCod, T001L3_A745FuncaoDados_MelhoraCod, T001L3_n745FuncaoDados_MelhoraCod
               }
               , new Object[] {
               T001L4_A371FuncaoDados_SistemaDes, T001L4_n371FuncaoDados_SistemaDes, T001L4_A705FuncaoDados_SistemaTecnica, T001L4_n705FuncaoDados_SistemaTecnica, T001L4_A372FuncaoDados_SistemaAreaCod, T001L4_n372FuncaoDados_SistemaAreaCod
               }
               , new Object[] {
               T001L5_A402FuncaoDados_FuncaoDadosNom, T001L5_n402FuncaoDados_FuncaoDadosNom, T001L5_A403FuncaoDados_FuncaoDadosDes, T001L5_n403FuncaoDados_FuncaoDadosDes, T001L5_A404FuncaoDados_FuncaoDadosSisCod, T001L5_n404FuncaoDados_FuncaoDadosSisCod
               }
               , new Object[] {
               T001L6_A745FuncaoDados_MelhoraCod
               }
               , new Object[] {
               T001L7_A405FuncaoDados_FuncaoDadosSisDes, T001L7_n405FuncaoDados_FuncaoDadosSisDes
               }
               , new Object[] {
               T001L8_A368FuncaoDados_Codigo, T001L8_A373FuncaoDados_Tipo, T001L8_A369FuncaoDados_Nome, T001L8_A397FuncaoDados_Descricao, T001L8_n397FuncaoDados_Descricao, T001L8_A371FuncaoDados_SistemaDes, T001L8_n371FuncaoDados_SistemaDes, T001L8_A705FuncaoDados_SistemaTecnica, T001L8_n705FuncaoDados_SistemaTecnica, T001L8_A1024FuncaoDados_DERImp,
               T001L8_n1024FuncaoDados_DERImp, T001L8_A1025FuncaoDados_RAImp, T001L8_n1025FuncaoDados_RAImp, T001L8_A402FuncaoDados_FuncaoDadosNom, T001L8_n402FuncaoDados_FuncaoDadosNom, T001L8_A403FuncaoDados_FuncaoDadosDes, T001L8_n403FuncaoDados_FuncaoDadosDes, T001L8_A405FuncaoDados_FuncaoDadosSisDes, T001L8_n405FuncaoDados_FuncaoDadosSisDes, T001L8_A755FuncaoDados_Contar,
               T001L8_n755FuncaoDados_Contar, T001L8_A1147FuncaoDados_Tecnica, T001L8_n1147FuncaoDados_Tecnica, T001L8_A1245FuncaoDados_Observacao, T001L8_n1245FuncaoDados_Observacao, T001L8_A1258FuncaoDados_Importada, T001L8_n1258FuncaoDados_Importada, T001L8_A1267FuncaoDados_UpdAoImpBsln, T001L8_n1267FuncaoDados_UpdAoImpBsln, T001L8_A394FuncaoDados_Ativo,
               T001L8_A370FuncaoDados_SistemaCod, T001L8_A391FuncaoDados_FuncaoDadosCod, T001L8_n391FuncaoDados_FuncaoDadosCod, T001L8_A745FuncaoDados_MelhoraCod, T001L8_n745FuncaoDados_MelhoraCod, T001L8_A372FuncaoDados_SistemaAreaCod, T001L8_n372FuncaoDados_SistemaAreaCod, T001L8_A404FuncaoDados_FuncaoDadosSisCod, T001L8_n404FuncaoDados_FuncaoDadosSisCod
               }
               , new Object[] {
               T001L9_A371FuncaoDados_SistemaDes, T001L9_n371FuncaoDados_SistemaDes, T001L9_A705FuncaoDados_SistemaTecnica, T001L9_n705FuncaoDados_SistemaTecnica, T001L9_A372FuncaoDados_SistemaAreaCod, T001L9_n372FuncaoDados_SistemaAreaCod
               }
               , new Object[] {
               T001L10_A402FuncaoDados_FuncaoDadosNom, T001L10_n402FuncaoDados_FuncaoDadosNom, T001L10_A403FuncaoDados_FuncaoDadosDes, T001L10_n403FuncaoDados_FuncaoDadosDes, T001L10_A404FuncaoDados_FuncaoDadosSisCod, T001L10_n404FuncaoDados_FuncaoDadosSisCod
               }
               , new Object[] {
               T001L11_A405FuncaoDados_FuncaoDadosSisDes, T001L11_n405FuncaoDados_FuncaoDadosSisDes
               }
               , new Object[] {
               T001L12_A745FuncaoDados_MelhoraCod
               }
               , new Object[] {
               T001L13_A368FuncaoDados_Codigo
               }
               , new Object[] {
               T001L14_A368FuncaoDados_Codigo
               }
               , new Object[] {
               T001L15_A368FuncaoDados_Codigo
               }
               , new Object[] {
               T001L16_A368FuncaoDados_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001L19_A371FuncaoDados_SistemaDes, T001L19_n371FuncaoDados_SistemaDes, T001L19_A705FuncaoDados_SistemaTecnica, T001L19_n705FuncaoDados_SistemaTecnica, T001L19_A372FuncaoDados_SistemaAreaCod, T001L19_n372FuncaoDados_SistemaAreaCod
               }
               , new Object[] {
               T001L20_A402FuncaoDados_FuncaoDadosNom, T001L20_n402FuncaoDados_FuncaoDadosNom, T001L20_A403FuncaoDados_FuncaoDadosDes, T001L20_n403FuncaoDados_FuncaoDadosDes, T001L20_A404FuncaoDados_FuncaoDadosSisCod, T001L20_n404FuncaoDados_FuncaoDadosSisCod
               }
               , new Object[] {
               T001L21_A405FuncaoDados_FuncaoDadosSisDes, T001L21_n405FuncaoDados_FuncaoDadosSisDes
               }
               , new Object[] {
               T001L22_A745FuncaoDados_MelhoraCod
               }
               , new Object[] {
               T001L23_A391FuncaoDados_FuncaoDadosCod
               }
               , new Object[] {
               T001L24_A1261FuncaoDadosPreImp_FnDadosCodigo, T001L24_A1259FuncaoDadosPreImp_Sequencial
               }
               , new Object[] {
               T001L25_A736ProjetoMelhoria_Codigo
               }
               , new Object[] {
               T001L26_A165FuncaoAPF_Codigo, T001L26_A364FuncaoAPFAtributos_AtributosCod
               }
               , new Object[] {
               T001L27_A368FuncaoDados_Codigo, T001L27_A172Tabela_Codigo
               }
               , new Object[] {
               T001L28_A368FuncaoDados_Codigo
               }
               , new Object[] {
               T001L29_A127Sistema_Codigo, T001L29_A129Sistema_Sigla, T001L29_A699Sistema_Tipo, T001L29_n699Sistema_Tipo
               }
            }
         );
         Z394FuncaoDados_Ativo = "A";
         A394FuncaoDados_Ativo = "A";
         i394FuncaoDados_Ativo = "A";
         Z1267FuncaoDados_UpdAoImpBsln = true;
         n1267FuncaoDados_UpdAoImpBsln = false;
         A1267FuncaoDados_UpdAoImpBsln = true;
         n1267FuncaoDados_UpdAoImpBsln = false;
         i1267FuncaoDados_UpdAoImpBsln = true;
         n1267FuncaoDados_UpdAoImpBsln = false;
         Z755FuncaoDados_Contar = true;
         n755FuncaoDados_Contar = false;
         i755FuncaoDados_Contar = true;
         n755FuncaoDados_Contar = false;
         A755FuncaoDados_Contar = true;
         n755FuncaoDados_Contar = false;
         AV28Pgmname = "FuncaoDados";
      }

      private short Z1024FuncaoDados_DERImp ;
      private short Z1025FuncaoDados_RAImp ;
      private short Z1147FuncaoDados_Tecnica ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short AV26Message ;
      private short A374FuncaoDados_DER ;
      private short A375FuncaoDados_RLR ;
      private short A1024FuncaoDados_DERImp ;
      private short A1025FuncaoDados_RAImp ;
      private short A1147FuncaoDados_Tecnica ;
      private short A420FuncaoDados_CodigoAliAie ;
      private short Gx_BScreen ;
      private short RcdFound59 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private short GXt_int3 ;
      private short wbTemp ;
      private int wcpOAV7FuncaoDados_Codigo ;
      private int wcpOAV13FuncaoDados_SistemaCod ;
      private int Z368FuncaoDados_Codigo ;
      private int Z370FuncaoDados_SistemaCod ;
      private int Z391FuncaoDados_FuncaoDadosCod ;
      private int Z745FuncaoDados_MelhoraCod ;
      private int N370FuncaoDados_SistemaCod ;
      private int N391FuncaoDados_FuncaoDadosCod ;
      private int N745FuncaoDados_MelhoraCod ;
      private int A370FuncaoDados_SistemaCod ;
      private int A368FuncaoDados_Codigo ;
      private int A745FuncaoDados_MelhoraCod ;
      private int A391FuncaoDados_FuncaoDadosCod ;
      private int A404FuncaoDados_FuncaoDadosSisCod ;
      private int AV7FuncaoDados_Codigo ;
      private int AV13FuncaoDados_SistemaCod ;
      private int trnEnded ;
      private int AV15Combo_SistemaCod ;
      private int edtFuncaoDados_Codigo_Enabled ;
      private int edtFuncaoDados_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtneliminar_Visible ;
      private int bttBtn_trn_cancel_Visible ;
      private int lblTbjava_Visible ;
      private int edtavMessage_Enabled ;
      private int edtavFuncaodados_nome_Enabled ;
      private int lblTextblockcombo_sistemacod_Visible ;
      private int edtavFuncaodados_descricao_Enabled ;
      private int edtFuncaoDados_DER_Enabled ;
      private int edtFuncaoDados_RLR_Enabled ;
      private int edtFuncaoDados_PF_Enabled ;
      private int lblTextblockfuncaodados_ativo_Visible ;
      private int lblTextblockflag_Visible ;
      private int AV11Insert_FuncaoDados_SistemaCod ;
      private int AV14Insert_FuncaoDados_FuncaoDadosCod ;
      private int AV18FuncaoDados_FuncaoDadosCod ;
      private int AV23Insert_FuncaoDados_MelhoraCod ;
      private int A372FuncaoDados_SistemaAreaCod ;
      private int Funcaodados_observacao_Color ;
      private int Funcaodados_observacao_Coltitlecolor ;
      private int AV29GXV1 ;
      private int edtavFuncaodados_nome_Width ;
      private int edtavFuncaodados_nome_Height ;
      private int Z372FuncaoDados_SistemaAreaCod ;
      private int Z404FuncaoDados_FuncaoDadosSisCod ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private int GXt_int4 ;
      private decimal A377FuncaoDados_PF ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z373FuncaoDados_Tipo ;
      private String Z394FuncaoDados_Ativo ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String AV16FuncaoDados_Tipo ;
      private String A376FuncaoDados_Complexidade ;
      private String A394FuncaoDados_Ativo ;
      private String chkavFlag_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String cmbFuncaoDados_Ativo_Internalname ;
      private String edtFuncaoDados_Codigo_Internalname ;
      private String edtFuncaoDados_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Caption ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtneliminar_Internalname ;
      private String bttBtneliminar_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String lblTbjava_Jsonclick ;
      private String lblTextblockmessage_Internalname ;
      private String lblTextblockmessage_Jsonclick ;
      private String edtavMessage_Internalname ;
      private String edtavMessage_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockfuncaodados_nome_Internalname ;
      private String lblTextblockfuncaodados_nome_Jsonclick ;
      private String edtavFuncaodados_nome_Internalname ;
      private String lblTextblockcombo_sistemacod_Internalname ;
      private String lblTextblockcombo_sistemacod_Caption ;
      private String lblTextblockcombo_sistemacod_Jsonclick ;
      private String dynavCombo_sistemacod_Internalname ;
      private String dynavCombo_sistemacod_Jsonclick ;
      private String lblTextblockfuncaodados_descricao_Internalname ;
      private String lblTextblockfuncaodados_descricao_Jsonclick ;
      private String edtavFuncaodados_descricao_Internalname ;
      private String lblTextblockfuncaodados_tipo_Internalname ;
      private String lblTextblockfuncaodados_tipo_Jsonclick ;
      private String cmbavFuncaodados_tipo_Internalname ;
      private String cmbavFuncaodados_tipo_Jsonclick ;
      private String lblTextblockfuncaodados_der_Internalname ;
      private String lblTextblockfuncaodados_der_Jsonclick ;
      private String edtFuncaoDados_DER_Internalname ;
      private String edtFuncaoDados_DER_Jsonclick ;
      private String lblTextblockfuncaodados_rlr_Internalname ;
      private String lblTextblockfuncaodados_rlr_Jsonclick ;
      private String edtFuncaoDados_RLR_Internalname ;
      private String edtFuncaoDados_RLR_Jsonclick ;
      private String lblTextblockfuncaodados_complexidade_Internalname ;
      private String lblTextblockfuncaodados_complexidade_Jsonclick ;
      private String cmbFuncaoDados_Complexidade_Internalname ;
      private String cmbFuncaoDados_Complexidade_Jsonclick ;
      private String lblTextblockfuncaodados_pf_Internalname ;
      private String lblTextblockfuncaodados_pf_Jsonclick ;
      private String edtFuncaoDados_PF_Internalname ;
      private String edtFuncaoDados_PF_Jsonclick ;
      private String lblTextblockfuncaodados_observacao_Internalname ;
      private String lblTextblockfuncaodados_observacao_Jsonclick ;
      private String lblTextblockfuncaodados_ativo_Internalname ;
      private String lblTextblockfuncaodados_ativo_Jsonclick ;
      private String lblTextblockflag_Internalname ;
      private String lblTextblockflag_Jsonclick ;
      private String tblTablemergedfuncaodados_ativo_Internalname ;
      private String cmbFuncaoDados_Ativo_Jsonclick ;
      private String lblTextblockfuncaodados_updaoimpbsln_Internalname ;
      private String lblTextblockfuncaodados_updaoimpbsln_Jsonclick ;
      private String cmbFuncaoDados_UpdAoImpBsln_Internalname ;
      private String cmbFuncaoDados_UpdAoImpBsln_Jsonclick ;
      private String A373FuncaoDados_Tipo ;
      private String A754FuncaoDados_SolucaoTecnica30 ;
      private String A705FuncaoDados_SistemaTecnica ;
      private String AV28Pgmname ;
      private String Funcaodados_observacao_Width ;
      private String Funcaodados_observacao_Height ;
      private String Funcaodados_observacao_Skin ;
      private String Funcaodados_observacao_Toolbar ;
      private String Funcaodados_observacao_Class ;
      private String Funcaodados_observacao_Customtoolbar ;
      private String Funcaodados_observacao_Customconfiguration ;
      private String Funcaodados_observacao_Buttonpressedid ;
      private String Funcaodados_observacao_Captionvalue ;
      private String Funcaodados_observacao_Captionclass ;
      private String Funcaodados_observacao_Captionposition ;
      private String Funcaodados_observacao_Coltitle ;
      private String Funcaodados_observacao_Coltitlefont ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode59 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z705FuncaoDados_SistemaTecnica ;
      private String Dvpanel_tableattributes_Internalname ;
      private String Funcaodados_observacao_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String i394FuncaoDados_Ativo ;
      private String gxwrpcisep ;
      private String GXt_char2 ;
      private DateTime Z1258FuncaoDados_Importada ;
      private DateTime A1258FuncaoDados_Importada ;
      private bool Z755FuncaoDados_Contar ;
      private bool Z1267FuncaoDados_UpdAoImpBsln ;
      private bool entryPointCalled ;
      private bool A755FuncaoDados_Contar ;
      private bool n755FuncaoDados_Contar ;
      private bool n745FuncaoDados_MelhoraCod ;
      private bool n391FuncaoDados_FuncaoDadosCod ;
      private bool n404FuncaoDados_FuncaoDadosSisCod ;
      private bool toggleJsOutput ;
      private bool A1267FuncaoDados_UpdAoImpBsln ;
      private bool n1267FuncaoDados_UpdAoImpBsln ;
      private bool wbErr ;
      private bool AV21Flag ;
      private bool n1024FuncaoDados_DERImp ;
      private bool n1025FuncaoDados_RAImp ;
      private bool n1147FuncaoDados_Tecnica ;
      private bool n1258FuncaoDados_Importada ;
      private bool n397FuncaoDados_Descricao ;
      private bool n371FuncaoDados_SistemaDes ;
      private bool n705FuncaoDados_SistemaTecnica ;
      private bool n1245FuncaoDados_Observacao ;
      private bool n372FuncaoDados_SistemaAreaCod ;
      private bool n402FuncaoDados_FuncaoDadosNom ;
      private bool n403FuncaoDados_FuncaoDadosDes ;
      private bool n405FuncaoDados_FuncaoDadosSisDes ;
      private bool Funcaodados_observacao_Enabled ;
      private bool Funcaodados_observacao_Toolbarcancollapse ;
      private bool Funcaodados_observacao_Toolbarexpanded ;
      private bool Funcaodados_observacao_Usercontroliscolumn ;
      private bool Funcaodados_observacao_Visible ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool GXt_boolean1 ;
      private bool Gx_longc ;
      private bool i1267FuncaoDados_UpdAoImpBsln ;
      private bool i755FuncaoDados_Contar ;
      private String O397FuncaoDados_Descricao ;
      private String AV17FuncaoDados_Descricao ;
      private String A397FuncaoDados_Descricao ;
      private String A753FuncaoDados_SolucaoTecnica ;
      private String A1245FuncaoDados_Observacao ;
      private String A403FuncaoDados_FuncaoDadosDes ;
      private String Z397FuncaoDados_Descricao ;
      private String Z1245FuncaoDados_Observacao ;
      private String Z403FuncaoDados_FuncaoDadosDes ;
      private String Z369FuncaoDados_Nome ;
      private String O369FuncaoDados_Nome ;
      private String AV20FuncaoDados_Nome ;
      private String A369FuncaoDados_Nome ;
      private String A419FuncaoDados_Nome50 ;
      private String A371FuncaoDados_SistemaDes ;
      private String A402FuncaoDados_FuncaoDadosNom ;
      private String A405FuncaoDados_FuncaoDadosSisDes ;
      private String Z371FuncaoDados_SistemaDes ;
      private String Z402FuncaoDados_FuncaoDadosNom ;
      private String Z405FuncaoDados_FuncaoDadosSisDes ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP2_FuncaoDados_SistemaCod ;
      private GXCombobox dynavCombo_sistemacod ;
      private GXCombobox cmbavFuncaodados_tipo ;
      private GXCombobox cmbFuncaoDados_Complexidade ;
      private GXCombobox cmbFuncaoDados_Ativo ;
      private GXCombobox cmbFuncaoDados_UpdAoImpBsln ;
      private GXCheckbox chkavFlag ;
      private IDataStoreProvider pr_default ;
      private String[] T001L5_A402FuncaoDados_FuncaoDadosNom ;
      private bool[] T001L5_n402FuncaoDados_FuncaoDadosNom ;
      private String[] T001L5_A403FuncaoDados_FuncaoDadosDes ;
      private bool[] T001L5_n403FuncaoDados_FuncaoDadosDes ;
      private int[] T001L5_A404FuncaoDados_FuncaoDadosSisCod ;
      private bool[] T001L5_n404FuncaoDados_FuncaoDadosSisCod ;
      private String[] T001L7_A405FuncaoDados_FuncaoDadosSisDes ;
      private bool[] T001L7_n405FuncaoDados_FuncaoDadosSisDes ;
      private bool[] T001L3_n391FuncaoDados_FuncaoDadosCod ;
      private String[] T001L4_A371FuncaoDados_SistemaDes ;
      private bool[] T001L4_n371FuncaoDados_SistemaDes ;
      private String[] T001L4_A705FuncaoDados_SistemaTecnica ;
      private bool[] T001L4_n705FuncaoDados_SistemaTecnica ;
      private int[] T001L4_A372FuncaoDados_SistemaAreaCod ;
      private bool[] T001L4_n372FuncaoDados_SistemaAreaCod ;
      private int[] T001L8_A368FuncaoDados_Codigo ;
      private String[] T001L8_A373FuncaoDados_Tipo ;
      private String[] T001L8_A369FuncaoDados_Nome ;
      private String[] T001L8_A397FuncaoDados_Descricao ;
      private bool[] T001L8_n397FuncaoDados_Descricao ;
      private String[] T001L8_A371FuncaoDados_SistemaDes ;
      private bool[] T001L8_n371FuncaoDados_SistemaDes ;
      private String[] T001L8_A705FuncaoDados_SistemaTecnica ;
      private bool[] T001L8_n705FuncaoDados_SistemaTecnica ;
      private short[] T001L8_A1024FuncaoDados_DERImp ;
      private bool[] T001L8_n1024FuncaoDados_DERImp ;
      private short[] T001L8_A1025FuncaoDados_RAImp ;
      private bool[] T001L8_n1025FuncaoDados_RAImp ;
      private String[] T001L8_A402FuncaoDados_FuncaoDadosNom ;
      private bool[] T001L8_n402FuncaoDados_FuncaoDadosNom ;
      private String[] T001L8_A403FuncaoDados_FuncaoDadosDes ;
      private bool[] T001L8_n403FuncaoDados_FuncaoDadosDes ;
      private String[] T001L8_A405FuncaoDados_FuncaoDadosSisDes ;
      private bool[] T001L8_n405FuncaoDados_FuncaoDadosSisDes ;
      private bool[] T001L8_A755FuncaoDados_Contar ;
      private bool[] T001L8_n755FuncaoDados_Contar ;
      private short[] T001L8_A1147FuncaoDados_Tecnica ;
      private bool[] T001L8_n1147FuncaoDados_Tecnica ;
      private String[] T001L8_A1245FuncaoDados_Observacao ;
      private bool[] T001L8_n1245FuncaoDados_Observacao ;
      private DateTime[] T001L8_A1258FuncaoDados_Importada ;
      private bool[] T001L8_n1258FuncaoDados_Importada ;
      private bool[] T001L8_A1267FuncaoDados_UpdAoImpBsln ;
      private bool[] T001L8_n1267FuncaoDados_UpdAoImpBsln ;
      private String[] T001L8_A394FuncaoDados_Ativo ;
      private int[] T001L8_A370FuncaoDados_SistemaCod ;
      private int[] T001L8_A391FuncaoDados_FuncaoDadosCod ;
      private bool[] T001L8_n391FuncaoDados_FuncaoDadosCod ;
      private int[] T001L8_A745FuncaoDados_MelhoraCod ;
      private bool[] T001L8_n745FuncaoDados_MelhoraCod ;
      private int[] T001L8_A372FuncaoDados_SistemaAreaCod ;
      private bool[] T001L8_n372FuncaoDados_SistemaAreaCod ;
      private int[] T001L8_A404FuncaoDados_FuncaoDadosSisCod ;
      private bool[] T001L8_n404FuncaoDados_FuncaoDadosSisCod ;
      private int[] T001L6_A745FuncaoDados_MelhoraCod ;
      private bool[] T001L6_n745FuncaoDados_MelhoraCod ;
      private String[] T001L9_A371FuncaoDados_SistemaDes ;
      private bool[] T001L9_n371FuncaoDados_SistemaDes ;
      private String[] T001L9_A705FuncaoDados_SistemaTecnica ;
      private bool[] T001L9_n705FuncaoDados_SistemaTecnica ;
      private int[] T001L9_A372FuncaoDados_SistemaAreaCod ;
      private bool[] T001L9_n372FuncaoDados_SistemaAreaCod ;
      private String[] T001L10_A402FuncaoDados_FuncaoDadosNom ;
      private bool[] T001L10_n402FuncaoDados_FuncaoDadosNom ;
      private String[] T001L10_A403FuncaoDados_FuncaoDadosDes ;
      private bool[] T001L10_n403FuncaoDados_FuncaoDadosDes ;
      private int[] T001L10_A404FuncaoDados_FuncaoDadosSisCod ;
      private bool[] T001L10_n404FuncaoDados_FuncaoDadosSisCod ;
      private String[] T001L11_A405FuncaoDados_FuncaoDadosSisDes ;
      private bool[] T001L11_n405FuncaoDados_FuncaoDadosSisDes ;
      private int[] T001L12_A745FuncaoDados_MelhoraCod ;
      private bool[] T001L12_n745FuncaoDados_MelhoraCod ;
      private int[] T001L13_A368FuncaoDados_Codigo ;
      private int[] T001L3_A368FuncaoDados_Codigo ;
      private String[] T001L3_A373FuncaoDados_Tipo ;
      private String[] T001L3_A369FuncaoDados_Nome ;
      private String[] T001L3_A397FuncaoDados_Descricao ;
      private bool[] T001L3_n397FuncaoDados_Descricao ;
      private short[] T001L3_A1024FuncaoDados_DERImp ;
      private bool[] T001L3_n1024FuncaoDados_DERImp ;
      private short[] T001L3_A1025FuncaoDados_RAImp ;
      private bool[] T001L3_n1025FuncaoDados_RAImp ;
      private bool[] T001L3_A755FuncaoDados_Contar ;
      private bool[] T001L3_n755FuncaoDados_Contar ;
      private short[] T001L3_A1147FuncaoDados_Tecnica ;
      private bool[] T001L3_n1147FuncaoDados_Tecnica ;
      private String[] T001L3_A1245FuncaoDados_Observacao ;
      private bool[] T001L3_n1245FuncaoDados_Observacao ;
      private DateTime[] T001L3_A1258FuncaoDados_Importada ;
      private bool[] T001L3_n1258FuncaoDados_Importada ;
      private bool[] T001L3_A1267FuncaoDados_UpdAoImpBsln ;
      private bool[] T001L3_n1267FuncaoDados_UpdAoImpBsln ;
      private String[] T001L3_A394FuncaoDados_Ativo ;
      private int[] T001L3_A370FuncaoDados_SistemaCod ;
      private int[] T001L3_A391FuncaoDados_FuncaoDadosCod ;
      private int[] T001L3_A745FuncaoDados_MelhoraCod ;
      private bool[] T001L3_n745FuncaoDados_MelhoraCod ;
      private int[] T001L14_A368FuncaoDados_Codigo ;
      private int[] T001L15_A368FuncaoDados_Codigo ;
      private int[] T001L2_A368FuncaoDados_Codigo ;
      private String[] T001L2_A373FuncaoDados_Tipo ;
      private String[] T001L2_A369FuncaoDados_Nome ;
      private String[] T001L2_A397FuncaoDados_Descricao ;
      private bool[] T001L2_n397FuncaoDados_Descricao ;
      private short[] T001L2_A1024FuncaoDados_DERImp ;
      private bool[] T001L2_n1024FuncaoDados_DERImp ;
      private short[] T001L2_A1025FuncaoDados_RAImp ;
      private bool[] T001L2_n1025FuncaoDados_RAImp ;
      private bool[] T001L2_A755FuncaoDados_Contar ;
      private bool[] T001L2_n755FuncaoDados_Contar ;
      private short[] T001L2_A1147FuncaoDados_Tecnica ;
      private bool[] T001L2_n1147FuncaoDados_Tecnica ;
      private String[] T001L2_A1245FuncaoDados_Observacao ;
      private bool[] T001L2_n1245FuncaoDados_Observacao ;
      private DateTime[] T001L2_A1258FuncaoDados_Importada ;
      private bool[] T001L2_n1258FuncaoDados_Importada ;
      private bool[] T001L2_A1267FuncaoDados_UpdAoImpBsln ;
      private bool[] T001L2_n1267FuncaoDados_UpdAoImpBsln ;
      private String[] T001L2_A394FuncaoDados_Ativo ;
      private int[] T001L2_A370FuncaoDados_SistemaCod ;
      private int[] T001L2_A391FuncaoDados_FuncaoDadosCod ;
      private bool[] T001L2_n391FuncaoDados_FuncaoDadosCod ;
      private int[] T001L2_A745FuncaoDados_MelhoraCod ;
      private bool[] T001L2_n745FuncaoDados_MelhoraCod ;
      private int[] T001L16_A368FuncaoDados_Codigo ;
      private String[] T001L19_A371FuncaoDados_SistemaDes ;
      private bool[] T001L19_n371FuncaoDados_SistemaDes ;
      private String[] T001L19_A705FuncaoDados_SistemaTecnica ;
      private bool[] T001L19_n705FuncaoDados_SistemaTecnica ;
      private int[] T001L19_A372FuncaoDados_SistemaAreaCod ;
      private bool[] T001L19_n372FuncaoDados_SistemaAreaCod ;
      private String[] T001L20_A402FuncaoDados_FuncaoDadosNom ;
      private bool[] T001L20_n402FuncaoDados_FuncaoDadosNom ;
      private String[] T001L20_A403FuncaoDados_FuncaoDadosDes ;
      private bool[] T001L20_n403FuncaoDados_FuncaoDadosDes ;
      private int[] T001L20_A404FuncaoDados_FuncaoDadosSisCod ;
      private bool[] T001L20_n404FuncaoDados_FuncaoDadosSisCod ;
      private String[] T001L21_A405FuncaoDados_FuncaoDadosSisDes ;
      private bool[] T001L21_n405FuncaoDados_FuncaoDadosSisDes ;
      private int[] T001L22_A745FuncaoDados_MelhoraCod ;
      private bool[] T001L22_n745FuncaoDados_MelhoraCod ;
      private int[] T001L23_A391FuncaoDados_FuncaoDadosCod ;
      private bool[] T001L23_n391FuncaoDados_FuncaoDadosCod ;
      private int[] T001L24_A1261FuncaoDadosPreImp_FnDadosCodigo ;
      private short[] T001L24_A1259FuncaoDadosPreImp_Sequencial ;
      private int[] T001L25_A736ProjetoMelhoria_Codigo ;
      private int[] T001L26_A165FuncaoAPF_Codigo ;
      private int[] T001L26_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] T001L27_A368FuncaoDados_Codigo ;
      private int[] T001L27_A172Tabela_Codigo ;
      private int[] T001L28_A368FuncaoDados_Codigo ;
      private int[] T001L29_A127Sistema_Codigo ;
      private String[] T001L29_A129Sistema_Sigla ;
      private String[] T001L29_A699Sistema_Tipo ;
      private bool[] T001L29_n699Sistema_Tipo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class funcaodados__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new UpdateCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT001L8 ;
          prmT001L8 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001L4 ;
          prmT001L4 = new Object[] {
          new Object[] {"@FuncaoDados_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001L5 ;
          prmT001L5 = new Object[] {
          new Object[] {"@FuncaoDados_FuncaoDadosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001L7 ;
          prmT001L7 = new Object[] {
          new Object[] {"@FuncaoDados_FuncaoDadosSisCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001L6 ;
          prmT001L6 = new Object[] {
          new Object[] {"@FuncaoDados_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001L9 ;
          prmT001L9 = new Object[] {
          new Object[] {"@FuncaoDados_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001L10 ;
          prmT001L10 = new Object[] {
          new Object[] {"@FuncaoDados_FuncaoDadosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001L11 ;
          prmT001L11 = new Object[] {
          new Object[] {"@FuncaoDados_FuncaoDadosSisCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001L12 ;
          prmT001L12 = new Object[] {
          new Object[] {"@FuncaoDados_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001L13 ;
          prmT001L13 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001L3 ;
          prmT001L3 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001L14 ;
          prmT001L14 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001L15 ;
          prmT001L15 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001L2 ;
          prmT001L2 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001L16 ;
          prmT001L16 = new Object[] {
          new Object[] {"@FuncaoDados_Tipo",SqlDbType.Char,3,0} ,
          new Object[] {"@FuncaoDados_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@FuncaoDados_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@FuncaoDados_DERImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDados_RAImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDados_Contar",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncaoDados_Tecnica",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@FuncaoDados_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@FuncaoDados_Importada",SqlDbType.DateTime,8,0} ,
          new Object[] {"@FuncaoDados_UpdAoImpBsln",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncaoDados_Ativo",SqlDbType.Char,1,0} ,
          new Object[] {"@FuncaoDados_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDados_FuncaoDadosCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDados_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001L17 ;
          prmT001L17 = new Object[] {
          new Object[] {"@FuncaoDados_Tipo",SqlDbType.Char,3,0} ,
          new Object[] {"@FuncaoDados_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@FuncaoDados_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@FuncaoDados_DERImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDados_RAImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDados_Contar",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncaoDados_Tecnica",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@FuncaoDados_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@FuncaoDados_Importada",SqlDbType.DateTime,8,0} ,
          new Object[] {"@FuncaoDados_UpdAoImpBsln",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncaoDados_Ativo",SqlDbType.Char,1,0} ,
          new Object[] {"@FuncaoDados_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDados_FuncaoDadosCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDados_MelhoraCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001L18 ;
          prmT001L18 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001L19 ;
          prmT001L19 = new Object[] {
          new Object[] {"@FuncaoDados_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001L20 ;
          prmT001L20 = new Object[] {
          new Object[] {"@FuncaoDados_FuncaoDadosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001L21 ;
          prmT001L21 = new Object[] {
          new Object[] {"@FuncaoDados_FuncaoDadosSisCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001L22 ;
          prmT001L22 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001L23 ;
          prmT001L23 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001L24 ;
          prmT001L24 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001L25 ;
          prmT001L25 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001L26 ;
          prmT001L26 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001L27 ;
          prmT001L27 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001L28 ;
          prmT001L28 = new Object[] {
          } ;
          Object[] prmT001L29 ;
          prmT001L29 = new Object[] {
          new Object[] {"@FuncaoDados_SistemaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T001L2", "SELECT [FuncaoDados_Codigo], [FuncaoDados_Tipo], [FuncaoDados_Nome], [FuncaoDados_Descricao], [FuncaoDados_DERImp], [FuncaoDados_RAImp], [FuncaoDados_Contar], [FuncaoDados_Tecnica], [FuncaoDados_Observacao], [FuncaoDados_Importada], [FuncaoDados_UpdAoImpBsln], [FuncaoDados_Ativo], [FuncaoDados_SistemaCod] AS FuncaoDados_SistemaCod, [FuncaoDados_FuncaoDadosCod] AS FuncaoDados_FuncaoDadosCod, [FuncaoDados_MelhoraCod] AS FuncaoDados_MelhoraCod FROM [FuncaoDados] WITH (UPDLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDados_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001L2,1,0,true,false )
             ,new CursorDef("T001L3", "SELECT [FuncaoDados_Codigo], [FuncaoDados_Tipo], [FuncaoDados_Nome], [FuncaoDados_Descricao], [FuncaoDados_DERImp], [FuncaoDados_RAImp], [FuncaoDados_Contar], [FuncaoDados_Tecnica], [FuncaoDados_Observacao], [FuncaoDados_Importada], [FuncaoDados_UpdAoImpBsln], [FuncaoDados_Ativo], [FuncaoDados_SistemaCod] AS FuncaoDados_SistemaCod, [FuncaoDados_FuncaoDadosCod] AS FuncaoDados_FuncaoDadosCod, [FuncaoDados_MelhoraCod] AS FuncaoDados_MelhoraCod FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDados_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001L3,1,0,true,false )
             ,new CursorDef("T001L4", "SELECT [Sistema_Nome] AS FuncaoDados_SistemaDes, [Sistema_Tecnica] AS FuncaoDados_SistemaTecnica, [Sistema_AreaTrabalhoCod] AS FuncaoDados_SistemaAreaCod FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @FuncaoDados_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001L4,1,0,true,false )
             ,new CursorDef("T001L5", "SELECT [FuncaoDados_Nome] AS FuncaoDados_FuncaoDadosNom, [FuncaoDados_Descricao] AS FuncaoDados_FuncaoDadosDes, [FuncaoDados_SistemaCod] AS FuncaoDados_FuncaoDadosSisCod FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDados_FuncaoDadosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001L5,1,0,true,false )
             ,new CursorDef("T001L6", "SELECT [FuncaoDados_Codigo] AS FuncaoDados_MelhoraCod FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDados_MelhoraCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001L6,1,0,true,false )
             ,new CursorDef("T001L7", "SELECT [Sistema_Nome] AS FuncaoDados_FuncaoDadosSisDes FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @FuncaoDados_FuncaoDadosSisCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001L7,1,0,true,false )
             ,new CursorDef("T001L8", "SELECT TM1.[FuncaoDados_Codigo], TM1.[FuncaoDados_Tipo], TM1.[FuncaoDados_Nome], TM1.[FuncaoDados_Descricao], T2.[Sistema_Nome] AS FuncaoDados_SistemaDes, T2.[Sistema_Tecnica] AS FuncaoDados_SistemaTecnica, TM1.[FuncaoDados_DERImp], TM1.[FuncaoDados_RAImp], T3.[FuncaoDados_Nome] AS FuncaoDados_FuncaoDadosNom, T3.[FuncaoDados_Descricao] AS FuncaoDados_FuncaoDadosDes, T4.[Sistema_Nome] AS FuncaoDados_FuncaoDadosSisDes, TM1.[FuncaoDados_Contar], TM1.[FuncaoDados_Tecnica], TM1.[FuncaoDados_Observacao], TM1.[FuncaoDados_Importada], TM1.[FuncaoDados_UpdAoImpBsln], TM1.[FuncaoDados_Ativo], TM1.[FuncaoDados_SistemaCod] AS FuncaoDados_SistemaCod, TM1.[FuncaoDados_FuncaoDadosCod] AS FuncaoDados_FuncaoDadosCod, TM1.[FuncaoDados_MelhoraCod] AS FuncaoDados_MelhoraCod, T2.[Sistema_AreaTrabalhoCod] AS FuncaoDados_SistemaAreaCod, T3.[FuncaoDados_SistemaCod] AS FuncaoDados_FuncaoDadosSisCod FROM ((([FuncaoDados] TM1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = TM1.[FuncaoDados_SistemaCod]) LEFT JOIN [FuncaoDados] T3 WITH (NOLOCK) ON T3.[FuncaoDados_Codigo] = TM1.[FuncaoDados_FuncaoDadosCod]) LEFT JOIN [Sistema] T4 WITH (NOLOCK) ON T4.[Sistema_Codigo] = T3.[FuncaoDados_SistemaCod]) WHERE TM1.[FuncaoDados_Codigo] = @FuncaoDados_Codigo ORDER BY TM1.[FuncaoDados_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001L8,100,0,true,false )
             ,new CursorDef("T001L9", "SELECT [Sistema_Nome] AS FuncaoDados_SistemaDes, [Sistema_Tecnica] AS FuncaoDados_SistemaTecnica, [Sistema_AreaTrabalhoCod] AS FuncaoDados_SistemaAreaCod FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @FuncaoDados_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001L9,1,0,true,false )
             ,new CursorDef("T001L10", "SELECT [FuncaoDados_Nome] AS FuncaoDados_FuncaoDadosNom, [FuncaoDados_Descricao] AS FuncaoDados_FuncaoDadosDes, [FuncaoDados_SistemaCod] AS FuncaoDados_FuncaoDadosSisCod FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDados_FuncaoDadosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001L10,1,0,true,false )
             ,new CursorDef("T001L11", "SELECT [Sistema_Nome] AS FuncaoDados_FuncaoDadosSisDes FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @FuncaoDados_FuncaoDadosSisCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001L11,1,0,true,false )
             ,new CursorDef("T001L12", "SELECT [FuncaoDados_Codigo] AS FuncaoDados_MelhoraCod FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDados_MelhoraCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001L12,1,0,true,false )
             ,new CursorDef("T001L13", "SELECT [FuncaoDados_Codigo] FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDados_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001L13,1,0,true,false )
             ,new CursorDef("T001L14", "SELECT TOP 1 [FuncaoDados_Codigo] FROM [FuncaoDados] WITH (NOLOCK) WHERE ( [FuncaoDados_Codigo] > @FuncaoDados_Codigo) ORDER BY [FuncaoDados_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001L14,1,0,true,true )
             ,new CursorDef("T001L15", "SELECT TOP 1 [FuncaoDados_Codigo] FROM [FuncaoDados] WITH (NOLOCK) WHERE ( [FuncaoDados_Codigo] < @FuncaoDados_Codigo) ORDER BY [FuncaoDados_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001L15,1,0,true,true )
             ,new CursorDef("T001L16", "INSERT INTO [FuncaoDados]([FuncaoDados_Tipo], [FuncaoDados_Nome], [FuncaoDados_Descricao], [FuncaoDados_DERImp], [FuncaoDados_RAImp], [FuncaoDados_Contar], [FuncaoDados_Tecnica], [FuncaoDados_Observacao], [FuncaoDados_Importada], [FuncaoDados_UpdAoImpBsln], [FuncaoDados_Ativo], [FuncaoDados_SistemaCod], [FuncaoDados_FuncaoDadosCod], [FuncaoDados_MelhoraCod]) VALUES(@FuncaoDados_Tipo, @FuncaoDados_Nome, @FuncaoDados_Descricao, @FuncaoDados_DERImp, @FuncaoDados_RAImp, @FuncaoDados_Contar, @FuncaoDados_Tecnica, @FuncaoDados_Observacao, @FuncaoDados_Importada, @FuncaoDados_UpdAoImpBsln, @FuncaoDados_Ativo, @FuncaoDados_SistemaCod, @FuncaoDados_FuncaoDadosCod, @FuncaoDados_MelhoraCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT001L16)
             ,new CursorDef("T001L17", "UPDATE [FuncaoDados] SET [FuncaoDados_Tipo]=@FuncaoDados_Tipo, [FuncaoDados_Nome]=@FuncaoDados_Nome, [FuncaoDados_Descricao]=@FuncaoDados_Descricao, [FuncaoDados_DERImp]=@FuncaoDados_DERImp, [FuncaoDados_RAImp]=@FuncaoDados_RAImp, [FuncaoDados_Contar]=@FuncaoDados_Contar, [FuncaoDados_Tecnica]=@FuncaoDados_Tecnica, [FuncaoDados_Observacao]=@FuncaoDados_Observacao, [FuncaoDados_Importada]=@FuncaoDados_Importada, [FuncaoDados_UpdAoImpBsln]=@FuncaoDados_UpdAoImpBsln, [FuncaoDados_Ativo]=@FuncaoDados_Ativo, [FuncaoDados_SistemaCod]=@FuncaoDados_SistemaCod, [FuncaoDados_FuncaoDadosCod]=@FuncaoDados_FuncaoDadosCod, [FuncaoDados_MelhoraCod]=@FuncaoDados_MelhoraCod  WHERE [FuncaoDados_Codigo] = @FuncaoDados_Codigo", GxErrorMask.GX_NOMASK,prmT001L17)
             ,new CursorDef("T001L18", "DELETE FROM [FuncaoDados]  WHERE [FuncaoDados_Codigo] = @FuncaoDados_Codigo", GxErrorMask.GX_NOMASK,prmT001L18)
             ,new CursorDef("T001L19", "SELECT [Sistema_Nome] AS FuncaoDados_SistemaDes, [Sistema_Tecnica] AS FuncaoDados_SistemaTecnica, [Sistema_AreaTrabalhoCod] AS FuncaoDados_SistemaAreaCod FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @FuncaoDados_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001L19,1,0,true,false )
             ,new CursorDef("T001L20", "SELECT [FuncaoDados_Nome] AS FuncaoDados_FuncaoDadosNom, [FuncaoDados_Descricao] AS FuncaoDados_FuncaoDadosDes, [FuncaoDados_SistemaCod] AS FuncaoDados_FuncaoDadosSisCod FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDados_FuncaoDadosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001L20,1,0,true,false )
             ,new CursorDef("T001L21", "SELECT [Sistema_Nome] AS FuncaoDados_FuncaoDadosSisDes FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @FuncaoDados_FuncaoDadosSisCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001L21,1,0,true,false )
             ,new CursorDef("T001L22", "SELECT TOP 1 [FuncaoDados_Codigo] AS FuncaoDados_MelhoraCod FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_MelhoraCod] = @FuncaoDados_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001L22,1,0,true,true )
             ,new CursorDef("T001L23", "SELECT TOP 1 [FuncaoDados_Codigo] AS FuncaoDados_FuncaoDadosCod FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_FuncaoDadosCod] = @FuncaoDados_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001L23,1,0,true,true )
             ,new CursorDef("T001L24", "SELECT TOP 1 [FuncaoDadosPreImp_FnDadosCodigo], [FuncaoDadosPreImp_Sequencial] FROM [FuncaoDadosPreImp] WITH (NOLOCK) WHERE [FuncaoDadosPreImp_FnDadosCodigo] = @FuncaoDados_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001L24,1,0,true,true )
             ,new CursorDef("T001L25", "SELECT TOP 1 [ProjetoMelhoria_Codigo] FROM [ProjetoMelhoria] WITH (NOLOCK) WHERE [ProjetoMelhoria_FnDadosCod] = @FuncaoDados_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001L25,1,0,true,true )
             ,new CursorDef("T001L26", "SELECT TOP 1 [FuncaoAPF_Codigo], [FuncaoAPFAtributos_AtributosCod] FROM [FuncoesAPFAtributos] WITH (NOLOCK) WHERE [FuncaoAPFAtributos_FuncaoDadosCod] = @FuncaoDados_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001L26,1,0,true,true )
             ,new CursorDef("T001L27", "SELECT TOP 1 [FuncaoDados_Codigo], [Tabela_Codigo] FROM [FuncaoDadosTabela] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDados_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001L27,1,0,true,true )
             ,new CursorDef("T001L28", "SELECT [FuncaoDados_Codigo] FROM [FuncaoDados] WITH (NOLOCK) ORDER BY [FuncaoDados_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001L28,100,0,true,false )
             ,new CursorDef("T001L29", "SELECT [Sistema_Codigo], [Sistema_Sigla], [Sistema_Tipo] FROM [Sistema] WITH (NOLOCK) WHERE ([Sistema_Codigo] <> @FuncaoDados_SistemaCod) AND ([Sistema_Tipo] = 'A') ORDER BY [Sistema_Sigla] ",true, GxErrorMask.GX_NOMASK, false, this,prmT001L29,0,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((bool[]) buf[9])[0] = rslt.getBool(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((short[]) buf[11])[0] = rslt.getShort(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getLongVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[15])[0] = rslt.getGXDate(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((bool[]) buf[17])[0] = rslt.getBool(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getString(12, 1) ;
                ((int[]) buf[20])[0] = rslt.getInt(13) ;
                ((int[]) buf[21])[0] = rslt.getInt(14) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(14);
                ((int[]) buf[23])[0] = rslt.getInt(15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((bool[]) buf[9])[0] = rslt.getBool(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((short[]) buf[11])[0] = rslt.getShort(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getLongVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[15])[0] = rslt.getGXDate(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((bool[]) buf[17])[0] = rslt.getBool(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getString(12, 1) ;
                ((int[]) buf[20])[0] = rslt.getInt(13) ;
                ((int[]) buf[21])[0] = rslt.getInt(14) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(14);
                ((int[]) buf[23])[0] = rslt.getInt(15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((short[]) buf[11])[0] = rslt.getShort(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getLongVarchar(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((bool[]) buf[19])[0] = rslt.getBool(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((short[]) buf[21])[0] = rslt.getShort(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getLongVarchar(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((DateTime[]) buf[25])[0] = rslt.getGXDate(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((bool[]) buf[27])[0] = rslt.getBool(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((String[]) buf[29])[0] = rslt.getString(17, 1) ;
                ((int[]) buf[30])[0] = rslt.getInt(18) ;
                ((int[]) buf[31])[0] = rslt.getInt(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((int[]) buf[33])[0] = rslt.getInt(20) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(20);
                ((int[]) buf[35])[0] = rslt.getInt(21) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(21);
                ((int[]) buf[37])[0] = rslt.getInt(22) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(22);
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 19 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 25 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 26 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 27 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 25) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(5, (short)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(6, (bool)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(7, (short)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(9, (DateTime)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 10 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(10, (bool)parms[17]);
                }
                stmt.SetParameter(11, (String)parms[18]);
                stmt.SetParameter(12, (int)parms[19]);
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 13 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(13, (int)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 14 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(14, (int)parms[23]);
                }
                return;
             case 15 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(5, (short)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(6, (bool)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(7, (short)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(9, (DateTime)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 10 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(10, (bool)parms[17]);
                }
                stmt.SetParameter(11, (String)parms[18]);
                stmt.SetParameter(12, (int)parms[19]);
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 13 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(13, (int)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 14 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(14, (int)parms[23]);
                }
                stmt.SetParameter(15, (int)parms[24]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 19 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 23 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 24 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 25 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 27 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
