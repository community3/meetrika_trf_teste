/*
               File: Caixa
        Description: Fluxo de Caixa
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:51:46.1
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class caixa : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxSuggest"+"_"+"CAIXA_TIPODECONTACOD") == 0 )
         {
            A871TipodeConta_Nome = GetNextPar( );
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXSGACAIXA_TIPODECONTACOD2S0( A871TipodeConta_Nome) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxSuggest"+"_"+"CAIXA_TIPODECONTACOD") == 0 )
         {
            A871TipodeConta_Nome = GetNextPar( );
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXSGACAIXA_TIPODECONTACOD2S0( A871TipodeConta_Nome) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxHideCode"+"_"+"CAIXA_TIPODECONTACOD") == 0 )
         {
            h881Caixa_TipoDeContaCod = GetNextPar( );
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXHCACAIXA_TIPODECONTACOD2S110( h881Caixa_TipoDeContaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_12") == 0 )
         {
            A881Caixa_TipoDeContaCod = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A881Caixa_TipoDeContaCod", A881Caixa_TipoDeContaCod);
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_12( A881Caixa_TipoDeContaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV13Caixa_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Caixa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Caixa_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCAIXA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13Caixa_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Fluxo de Caixa", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtCaixa_Documento_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public caixa( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public caixa( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Caixa_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV13Caixa_Codigo = aP1_Caixa_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2S110( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2S110e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2S110( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2S110( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2S110e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_53_2S110( true) ;
         }
         return  ;
      }

      protected void wb_table3_53_2S110e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2S110e( true) ;
         }
         else
         {
            wb_table1_2_2S110e( false) ;
         }
      }

      protected void wb_table3_53_2S110( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Caixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Caixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Caixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_53_2S110e( true) ;
         }
         else
         {
            wb_table3_53_2S110e( false) ;
         }
      }

      protected void wb_table2_5_2S110( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_2S110( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_2S110e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2S110e( true) ;
         }
         else
         {
            wb_table2_5_2S110e( false) ;
         }
      }

      protected void wb_table4_13_2S110( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcaixa_codigo_Internalname, "Codigo", "", "", lblTextblockcaixa_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Caixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtCaixa_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A874Caixa_Codigo), 6, 0, ",", "")), ((edtCaixa_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A874Caixa_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A874Caixa_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCaixa_Codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtCaixa_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Caixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcaixa_documento_Internalname, "Documento", "", "", lblTextblockcaixa_documento_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Caixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtCaixa_Documento_Internalname, A875Caixa_Documento, StringUtil.RTrim( context.localUtil.Format( A875Caixa_Documento, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCaixa_Documento_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtCaixa_Documento_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_Caixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcaixa_emissao_Internalname, "Data de Emiss�o", "", "", lblTextblockcaixa_emissao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Caixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_28_2S110( true) ;
         }
         return  ;
      }

      protected void wb_table5_28_2S110e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcaixa_tipodecontacod_Internalname, "Conta", "", "", lblTextblockcaixa_tipodecontacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Caixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtCaixa_TipoDeContaCod_Internalname, StringUtil.RTrim( h881Caixa_TipoDeContaCod), StringUtil.RTrim( context.localUtil.Format( h881Caixa_TipoDeContaCod, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCaixa_TipoDeContaCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtCaixa_TipoDeContaCod_Enabled, 1, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_Caixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcaixa_descricao_Internalname, "Descri��o", "", "", lblTextblockcaixa_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Caixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtCaixa_Descricao_Internalname, A879Caixa_Descricao, StringUtil.RTrim( context.localUtil.Format( A879Caixa_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,45);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCaixa_Descricao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtCaixa_Descricao_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_Caixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcaixa_valor_Internalname, "Valor", "", "", lblTextblockcaixa_valor_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Caixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtCaixa_Valor_Internalname, StringUtil.LTrim( StringUtil.NToC( A880Caixa_Valor, 18, 5, ",", "")), ((edtCaixa_Valor_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A880Caixa_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A880Caixa_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCaixa_Valor_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtCaixa_Valor_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_Caixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_2S110e( true) ;
         }
         else
         {
            wb_table4_13_2S110e( false) ;
         }
      }

      protected void wb_table5_28_2S110( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcaixa_emissao_Internalname, tblTablemergedcaixa_emissao_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtCaixa_Emissao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtCaixa_Emissao_Internalname, context.localUtil.Format(A876Caixa_Emissao, "99/99/99"), context.localUtil.Format( A876Caixa_Emissao, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,31);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCaixa_Emissao_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtCaixa_Emissao_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Caixa.htm");
            GxWebStd.gx_bitmap( context, edtCaixa_Emissao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtCaixa_Emissao_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Caixa.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcaixa_vencimento_Internalname, "Data de Vencimento", "", "", lblTextblockcaixa_vencimento_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Caixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtCaixa_Vencimento_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtCaixa_Vencimento_Internalname, context.localUtil.Format(A877Caixa_Vencimento, "99/99/99"), context.localUtil.Format( A877Caixa_Vencimento, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,35);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCaixa_Vencimento_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtCaixa_Vencimento_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Caixa.htm");
            GxWebStd.gx_bitmap( context, edtCaixa_Vencimento_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtCaixa_Vencimento_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Caixa.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_28_2S110e( true) ;
         }
         else
         {
            wb_table5_28_2S110e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E112S2 */
         E112S2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A874Caixa_Codigo = (int)(context.localUtil.CToN( cgiGet( edtCaixa_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A874Caixa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A874Caixa_Codigo), 6, 0)));
               A875Caixa_Documento = StringUtil.Upper( cgiGet( edtCaixa_Documento_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A875Caixa_Documento", A875Caixa_Documento);
               if ( context.localUtil.VCDate( cgiGet( edtCaixa_Emissao_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data de Emiss�o"}), 1, "CAIXA_EMISSAO");
                  AnyError = 1;
                  GX_FocusControl = edtCaixa_Emissao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A876Caixa_Emissao = DateTime.MinValue;
                  n876Caixa_Emissao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A876Caixa_Emissao", context.localUtil.Format(A876Caixa_Emissao, "99/99/99"));
               }
               else
               {
                  A876Caixa_Emissao = context.localUtil.CToD( cgiGet( edtCaixa_Emissao_Internalname), 2);
                  n876Caixa_Emissao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A876Caixa_Emissao", context.localUtil.Format(A876Caixa_Emissao, "99/99/99"));
               }
               n876Caixa_Emissao = ((DateTime.MinValue==A876Caixa_Emissao) ? true : false);
               if ( context.localUtil.VCDate( cgiGet( edtCaixa_Vencimento_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data de Vencimento"}), 1, "CAIXA_VENCIMENTO");
                  AnyError = 1;
                  GX_FocusControl = edtCaixa_Vencimento_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A877Caixa_Vencimento = DateTime.MinValue;
                  n877Caixa_Vencimento = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A877Caixa_Vencimento", context.localUtil.Format(A877Caixa_Vencimento, "99/99/99"));
               }
               else
               {
                  A877Caixa_Vencimento = context.localUtil.CToD( cgiGet( edtCaixa_Vencimento_Internalname), 2);
                  n877Caixa_Vencimento = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A877Caixa_Vencimento", context.localUtil.Format(A877Caixa_Vencimento, "99/99/99"));
               }
               n877Caixa_Vencimento = ((DateTime.MinValue==A877Caixa_Vencimento) ? true : false);
               h881Caixa_TipoDeContaCod = StringUtil.Upper( cgiGet( edtCaixa_TipoDeContaCod_Internalname));
               A879Caixa_Descricao = StringUtil.Upper( cgiGet( edtCaixa_Descricao_Internalname));
               n879Caixa_Descricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A879Caixa_Descricao", A879Caixa_Descricao);
               n879Caixa_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A879Caixa_Descricao)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtCaixa_Valor_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtCaixa_Valor_Internalname), ",", ".") > 999999999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CAIXA_VALOR");
                  AnyError = 1;
                  GX_FocusControl = edtCaixa_Valor_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A880Caixa_Valor = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A880Caixa_Valor", StringUtil.LTrim( StringUtil.Str( A880Caixa_Valor, 18, 5)));
               }
               else
               {
                  A880Caixa_Valor = context.localUtil.CToN( cgiGet( edtCaixa_Valor_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A880Caixa_Valor", StringUtil.LTrim( StringUtil.Str( A880Caixa_Valor, 18, 5)));
               }
               /* Read saved values. */
               A881Caixa_TipoDeContaCod = cgiGet( "GXHCCAIXA_TIPODECONTACOD");
               Z874Caixa_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z874Caixa_Codigo"), ",", "."));
               Z875Caixa_Documento = cgiGet( "Z875Caixa_Documento");
               Z876Caixa_Emissao = context.localUtil.CToD( cgiGet( "Z876Caixa_Emissao"), 0);
               n876Caixa_Emissao = ((DateTime.MinValue==A876Caixa_Emissao) ? true : false);
               Z877Caixa_Vencimento = context.localUtil.CToD( cgiGet( "Z877Caixa_Vencimento"), 0);
               n877Caixa_Vencimento = ((DateTime.MinValue==A877Caixa_Vencimento) ? true : false);
               Z879Caixa_Descricao = cgiGet( "Z879Caixa_Descricao");
               n879Caixa_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A879Caixa_Descricao)) ? true : false);
               Z880Caixa_Valor = context.localUtil.CToN( cgiGet( "Z880Caixa_Valor"), ",", ".");
               Z881Caixa_TipoDeContaCod = cgiGet( "Z881Caixa_TipoDeContaCod");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N881Caixa_TipoDeContaCod = cgiGet( "N881Caixa_TipoDeContaCod");
               AV13Caixa_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCAIXA_CODIGO"), ",", "."));
               AV11Insert_Caixa_TipoDeContaCod = cgiGet( "vINSERT_CAIXA_TIPODECONTACOD");
               AV14Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Caixa";
               A874Caixa_Codigo = (int)(context.localUtil.CToN( cgiGet( edtCaixa_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A874Caixa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A874Caixa_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A874Caixa_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A874Caixa_Codigo != Z874Caixa_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("caixa:[SecurityCheckFailed value for]"+"Caixa_Codigo:"+context.localUtil.Format( (decimal)(A874Caixa_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("caixa:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A874Caixa_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A874Caixa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A874Caixa_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode110 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode110;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound110 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_2S0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "CAIXA_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtCaixa_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E112S2 */
                           E112S2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E122S2 */
                           E122S2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E122S2 */
            E122S2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2S110( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes2S110( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_2S0( )
      {
         BeforeValidate2S110( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2S110( ) ;
            }
            else
            {
               CheckExtendedTable2S110( ) ;
               CloseExtendedTableCursors2S110( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption2S0( )
      {
      }

      protected void E112S2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV14Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV15GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            while ( AV15GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV15GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Caixa_TipoDeContaCod") == 0 )
               {
                  AV11Insert_Caixa_TipoDeContaCod = AV12TrnContextAtt.gxTpr_Attributevalue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Caixa_TipoDeContaCod", AV11Insert_Caixa_TipoDeContaCod);
               }
               AV15GXV1 = (int)(AV15GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            }
         }
      }

      protected void E122S2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwcaixa.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM2S110( short GX_JID )
      {
         if ( ( GX_JID == 11 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z875Caixa_Documento = T002S3_A875Caixa_Documento[0];
               Z876Caixa_Emissao = T002S3_A876Caixa_Emissao[0];
               Z877Caixa_Vencimento = T002S3_A877Caixa_Vencimento[0];
               Z879Caixa_Descricao = T002S3_A879Caixa_Descricao[0];
               Z880Caixa_Valor = T002S3_A880Caixa_Valor[0];
               Z881Caixa_TipoDeContaCod = T002S3_A881Caixa_TipoDeContaCod[0];
            }
            else
            {
               Z875Caixa_Documento = A875Caixa_Documento;
               Z876Caixa_Emissao = A876Caixa_Emissao;
               Z877Caixa_Vencimento = A877Caixa_Vencimento;
               Z879Caixa_Descricao = A879Caixa_Descricao;
               Z880Caixa_Valor = A880Caixa_Valor;
               Z881Caixa_TipoDeContaCod = A881Caixa_TipoDeContaCod;
            }
         }
         if ( GX_JID == -11 )
         {
            Z874Caixa_Codigo = A874Caixa_Codigo;
            Z875Caixa_Documento = A875Caixa_Documento;
            Z876Caixa_Emissao = A876Caixa_Emissao;
            Z877Caixa_Vencimento = A877Caixa_Vencimento;
            Z879Caixa_Descricao = A879Caixa_Descricao;
            Z880Caixa_Valor = A880Caixa_Valor;
            Z881Caixa_TipoDeContaCod = A881Caixa_TipoDeContaCod;
         }
      }

      protected void standaloneNotModal( )
      {
         edtCaixa_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCaixa_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCaixa_Codigo_Enabled), 5, 0)));
         AV14Pgmname = "Caixa";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
         edtCaixa_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCaixa_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCaixa_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV13Caixa_Codigo) )
         {
            A874Caixa_Codigo = AV13Caixa_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A874Caixa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A874Caixa_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV11Insert_Caixa_TipoDeContaCod)) )
         {
            edtCaixa_TipoDeContaCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCaixa_TipoDeContaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCaixa_TipoDeContaCod_Enabled), 5, 0)));
         }
         else
         {
            edtCaixa_TipoDeContaCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCaixa_TipoDeContaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCaixa_TipoDeContaCod_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV11Insert_Caixa_TipoDeContaCod)) )
         {
            A881Caixa_TipoDeContaCod = AV11Insert_Caixa_TipoDeContaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A881Caixa_TipoDeContaCod", A881Caixa_TipoDeContaCod);
            /* Using cursor T002S5 */
            pr_default.execute(3, new Object[] {A881Caixa_TipoDeContaCod});
            h881Caixa_TipoDeContaCod = "";
            while ( (pr_default.getStatus(3) != 101) )
            {
               A871TipodeConta_Nome = T002S5_A871TipodeConta_Nome[0];
               A870TipodeConta_Codigo = T002S5_A870TipodeConta_Codigo[0];
               h881Caixa_TipoDeContaCod = A871TipodeConta_Nome;
               if (true) break;
            }
            pr_default.close(3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "h881Caixa_TipoDeContaCod", h881Caixa_TipoDeContaCod);
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
      }

      protected void Load2S110( )
      {
         /* Using cursor T002S6 */
         pr_default.execute(4, new Object[] {A874Caixa_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound110 = 1;
            A875Caixa_Documento = T002S6_A875Caixa_Documento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A875Caixa_Documento", A875Caixa_Documento);
            A876Caixa_Emissao = T002S6_A876Caixa_Emissao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A876Caixa_Emissao", context.localUtil.Format(A876Caixa_Emissao, "99/99/99"));
            n876Caixa_Emissao = T002S6_n876Caixa_Emissao[0];
            A877Caixa_Vencimento = T002S6_A877Caixa_Vencimento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A877Caixa_Vencimento", context.localUtil.Format(A877Caixa_Vencimento, "99/99/99"));
            n877Caixa_Vencimento = T002S6_n877Caixa_Vencimento[0];
            A879Caixa_Descricao = T002S6_A879Caixa_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A879Caixa_Descricao", A879Caixa_Descricao);
            n879Caixa_Descricao = T002S6_n879Caixa_Descricao[0];
            A880Caixa_Valor = T002S6_A880Caixa_Valor[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A880Caixa_Valor", StringUtil.LTrim( StringUtil.Str( A880Caixa_Valor, 18, 5)));
            A881Caixa_TipoDeContaCod = T002S6_A881Caixa_TipoDeContaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A881Caixa_TipoDeContaCod", A881Caixa_TipoDeContaCod);
            ZM2S110( -11) ;
         }
         pr_default.close(4);
         OnLoadActions2S110( ) ;
      }

      protected void OnLoadActions2S110( )
      {
         /* Using cursor T002S7 */
         pr_default.execute(5, new Object[] {A881Caixa_TipoDeContaCod});
         h881Caixa_TipoDeContaCod = "";
         while ( (pr_default.getStatus(5) != 101) )
         {
            A871TipodeConta_Nome = T002S7_A871TipodeConta_Nome[0];
            A870TipodeConta_Codigo = T002S7_A870TipodeConta_Codigo[0];
            h881Caixa_TipoDeContaCod = A871TipodeConta_Nome;
            if (true) break;
         }
         pr_default.close(5);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "h881Caixa_TipoDeContaCod", h881Caixa_TipoDeContaCod);
      }

      protected void CheckExtendedTable2S110( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A876Caixa_Emissao) || ( A876Caixa_Emissao >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data de Emiss�o fora do intervalo", "OutOfRange", 1, "CAIXA_EMISSAO");
            AnyError = 1;
            GX_FocusControl = edtCaixa_Emissao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A877Caixa_Vencimento) || ( A877Caixa_Vencimento >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data de Vencimento fora do intervalo", "OutOfRange", 1, "CAIXA_VENCIMENTO");
            AnyError = 1;
            GX_FocusControl = edtCaixa_Vencimento_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T002S4 */
         pr_default.execute(2, new Object[] {A881Caixa_TipoDeContaCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Caixa_Tipo De Conta'.", "ForeignKeyNotFound", 1, "CAIXA_TIPODECONTACOD");
            AnyError = 1;
            GX_FocusControl = edtCaixa_TipoDeContaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors2S110( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_12( String A881Caixa_TipoDeContaCod )
      {
         /* Using cursor T002S8 */
         pr_default.execute(6, new Object[] {A881Caixa_TipoDeContaCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Caixa_Tipo De Conta'.", "ForeignKeyNotFound", 1, "CAIXA_TIPODECONTACOD");
            AnyError = 1;
            GX_FocusControl = edtCaixa_TipoDeContaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey2S110( )
      {
         /* Using cursor T002S9 */
         pr_default.execute(7, new Object[] {A874Caixa_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound110 = 1;
         }
         else
         {
            RcdFound110 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T002S3 */
         pr_default.execute(1, new Object[] {A874Caixa_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2S110( 11) ;
            RcdFound110 = 1;
            A874Caixa_Codigo = T002S3_A874Caixa_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A874Caixa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A874Caixa_Codigo), 6, 0)));
            A875Caixa_Documento = T002S3_A875Caixa_Documento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A875Caixa_Documento", A875Caixa_Documento);
            A876Caixa_Emissao = T002S3_A876Caixa_Emissao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A876Caixa_Emissao", context.localUtil.Format(A876Caixa_Emissao, "99/99/99"));
            n876Caixa_Emissao = T002S3_n876Caixa_Emissao[0];
            A877Caixa_Vencimento = T002S3_A877Caixa_Vencimento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A877Caixa_Vencimento", context.localUtil.Format(A877Caixa_Vencimento, "99/99/99"));
            n877Caixa_Vencimento = T002S3_n877Caixa_Vencimento[0];
            A879Caixa_Descricao = T002S3_A879Caixa_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A879Caixa_Descricao", A879Caixa_Descricao);
            n879Caixa_Descricao = T002S3_n879Caixa_Descricao[0];
            A880Caixa_Valor = T002S3_A880Caixa_Valor[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A880Caixa_Valor", StringUtil.LTrim( StringUtil.Str( A880Caixa_Valor, 18, 5)));
            A881Caixa_TipoDeContaCod = T002S3_A881Caixa_TipoDeContaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A881Caixa_TipoDeContaCod", A881Caixa_TipoDeContaCod);
            Z874Caixa_Codigo = A874Caixa_Codigo;
            sMode110 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load2S110( ) ;
            if ( AnyError == 1 )
            {
               RcdFound110 = 0;
               InitializeNonKey2S110( ) ;
            }
            Gx_mode = sMode110;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound110 = 0;
            InitializeNonKey2S110( ) ;
            sMode110 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode110;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2S110( ) ;
         if ( RcdFound110 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound110 = 0;
         /* Using cursor T002S10 */
         pr_default.execute(8, new Object[] {A874Caixa_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T002S10_A874Caixa_Codigo[0] < A874Caixa_Codigo ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T002S10_A874Caixa_Codigo[0] > A874Caixa_Codigo ) ) )
            {
               A874Caixa_Codigo = T002S10_A874Caixa_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A874Caixa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A874Caixa_Codigo), 6, 0)));
               RcdFound110 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound110 = 0;
         /* Using cursor T002S11 */
         pr_default.execute(9, new Object[] {A874Caixa_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T002S11_A874Caixa_Codigo[0] > A874Caixa_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T002S11_A874Caixa_Codigo[0] < A874Caixa_Codigo ) ) )
            {
               A874Caixa_Codigo = T002S11_A874Caixa_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A874Caixa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A874Caixa_Codigo), 6, 0)));
               RcdFound110 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2S110( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtCaixa_Documento_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2S110( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound110 == 1 )
            {
               if ( A874Caixa_Codigo != Z874Caixa_Codigo )
               {
                  A874Caixa_Codigo = Z874Caixa_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A874Caixa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A874Caixa_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CAIXA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtCaixa_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtCaixa_Documento_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update2S110( ) ;
                  GX_FocusControl = edtCaixa_Documento_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A874Caixa_Codigo != Z874Caixa_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtCaixa_Documento_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2S110( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CAIXA_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtCaixa_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtCaixa_Documento_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2S110( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A874Caixa_Codigo != Z874Caixa_Codigo )
         {
            A874Caixa_Codigo = Z874Caixa_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A874Caixa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A874Caixa_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CAIXA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtCaixa_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtCaixa_Documento_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency2S110( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
         {
         }
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T002S2 */
            pr_default.execute(0, new Object[] {A874Caixa_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Caixa"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z875Caixa_Documento, T002S2_A875Caixa_Documento[0]) != 0 ) || ( Z876Caixa_Emissao != T002S2_A876Caixa_Emissao[0] ) || ( Z877Caixa_Vencimento != T002S2_A877Caixa_Vencimento[0] ) || ( StringUtil.StrCmp(Z879Caixa_Descricao, T002S2_A879Caixa_Descricao[0]) != 0 ) || ( Z880Caixa_Valor != T002S2_A880Caixa_Valor[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z881Caixa_TipoDeContaCod, T002S2_A881Caixa_TipoDeContaCod[0]) != 0 ) )
            {
               if ( StringUtil.StrCmp(Z875Caixa_Documento, T002S2_A875Caixa_Documento[0]) != 0 )
               {
                  GXUtil.WriteLog("caixa:[seudo value changed for attri]"+"Caixa_Documento");
                  GXUtil.WriteLogRaw("Old: ",Z875Caixa_Documento);
                  GXUtil.WriteLogRaw("Current: ",T002S2_A875Caixa_Documento[0]);
               }
               if ( Z876Caixa_Emissao != T002S2_A876Caixa_Emissao[0] )
               {
                  GXUtil.WriteLog("caixa:[seudo value changed for attri]"+"Caixa_Emissao");
                  GXUtil.WriteLogRaw("Old: ",Z876Caixa_Emissao);
                  GXUtil.WriteLogRaw("Current: ",T002S2_A876Caixa_Emissao[0]);
               }
               if ( Z877Caixa_Vencimento != T002S2_A877Caixa_Vencimento[0] )
               {
                  GXUtil.WriteLog("caixa:[seudo value changed for attri]"+"Caixa_Vencimento");
                  GXUtil.WriteLogRaw("Old: ",Z877Caixa_Vencimento);
                  GXUtil.WriteLogRaw("Current: ",T002S2_A877Caixa_Vencimento[0]);
               }
               if ( StringUtil.StrCmp(Z879Caixa_Descricao, T002S2_A879Caixa_Descricao[0]) != 0 )
               {
                  GXUtil.WriteLog("caixa:[seudo value changed for attri]"+"Caixa_Descricao");
                  GXUtil.WriteLogRaw("Old: ",Z879Caixa_Descricao);
                  GXUtil.WriteLogRaw("Current: ",T002S2_A879Caixa_Descricao[0]);
               }
               if ( Z880Caixa_Valor != T002S2_A880Caixa_Valor[0] )
               {
                  GXUtil.WriteLog("caixa:[seudo value changed for attri]"+"Caixa_Valor");
                  GXUtil.WriteLogRaw("Old: ",Z880Caixa_Valor);
                  GXUtil.WriteLogRaw("Current: ",T002S2_A880Caixa_Valor[0]);
               }
               if ( StringUtil.StrCmp(Z881Caixa_TipoDeContaCod, T002S2_A881Caixa_TipoDeContaCod[0]) != 0 )
               {
                  GXUtil.WriteLog("caixa:[seudo value changed for attri]"+"Caixa_TipoDeContaCod");
                  GXUtil.WriteLogRaw("Old: ",Z881Caixa_TipoDeContaCod);
                  GXUtil.WriteLogRaw("Current: ",T002S2_A881Caixa_TipoDeContaCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Caixa"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2S110( )
      {
         BeforeValidate2S110( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2S110( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2S110( 0) ;
            CheckOptimisticConcurrency2S110( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2S110( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2S110( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002S12 */
                     pr_default.execute(10, new Object[] {A875Caixa_Documento, n876Caixa_Emissao, A876Caixa_Emissao, n877Caixa_Vencimento, A877Caixa_Vencimento, n879Caixa_Descricao, A879Caixa_Descricao, A880Caixa_Valor, A881Caixa_TipoDeContaCod});
                     A874Caixa_Codigo = T002S12_A874Caixa_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A874Caixa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A874Caixa_Codigo), 6, 0)));
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("Caixa") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption2S0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2S110( ) ;
            }
            EndLevel2S110( ) ;
         }
         CloseExtendedTableCursors2S110( ) ;
      }

      protected void Update2S110( )
      {
         BeforeValidate2S110( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2S110( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2S110( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2S110( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2S110( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002S13 */
                     pr_default.execute(11, new Object[] {A875Caixa_Documento, n876Caixa_Emissao, A876Caixa_Emissao, n877Caixa_Vencimento, A877Caixa_Vencimento, n879Caixa_Descricao, A879Caixa_Descricao, A880Caixa_Valor, A881Caixa_TipoDeContaCod, A874Caixa_Codigo});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("Caixa") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Caixa"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2S110( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2S110( ) ;
         }
         CloseExtendedTableCursors2S110( ) ;
      }

      protected void DeferredUpdate2S110( )
      {
      }

      protected void delete( )
      {
         BeforeValidate2S110( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2S110( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2S110( ) ;
            AfterConfirm2S110( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2S110( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002S14 */
                  pr_default.execute(12, new Object[] {A874Caixa_Codigo});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("Caixa") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode110 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel2S110( ) ;
         Gx_mode = sMode110;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls2S110( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel2S110( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2S110( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "Caixa");
            if ( AnyError == 0 )
            {
               ConfirmValues2S0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "Caixa");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2S110( )
      {
         /* Scan By routine */
         /* Using cursor T002S15 */
         pr_default.execute(13);
         RcdFound110 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound110 = 1;
            A874Caixa_Codigo = T002S15_A874Caixa_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A874Caixa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A874Caixa_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2S110( )
      {
         /* Scan next routine */
         pr_default.readNext(13);
         RcdFound110 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound110 = 1;
            A874Caixa_Codigo = T002S15_A874Caixa_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A874Caixa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A874Caixa_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd2S110( )
      {
         pr_default.close(13);
      }

      protected void AfterConfirm2S110( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2S110( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2S110( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2S110( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2S110( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2S110( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2S110( )
      {
         edtCaixa_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCaixa_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCaixa_Codigo_Enabled), 5, 0)));
         edtCaixa_Documento_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCaixa_Documento_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCaixa_Documento_Enabled), 5, 0)));
         edtCaixa_Emissao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCaixa_Emissao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCaixa_Emissao_Enabled), 5, 0)));
         edtCaixa_Vencimento_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCaixa_Vencimento_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCaixa_Vencimento_Enabled), 5, 0)));
         edtCaixa_TipoDeContaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCaixa_TipoDeContaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCaixa_TipoDeContaCod_Enabled), 5, 0)));
         edtCaixa_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCaixa_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCaixa_Descricao_Enabled), 5, 0)));
         edtCaixa_Valor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCaixa_Valor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCaixa_Valor_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues2S0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051812514752");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("caixa.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV13Caixa_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "GXHCCAIXA_TIPODECONTACOD", StringUtil.RTrim( A881Caixa_TipoDeContaCod));
         GxWebStd.gx_hidden_field( context, "Z874Caixa_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z874Caixa_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z875Caixa_Documento", Z875Caixa_Documento);
         GxWebStd.gx_hidden_field( context, "Z876Caixa_Emissao", context.localUtil.DToC( Z876Caixa_Emissao, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z877Caixa_Vencimento", context.localUtil.DToC( Z877Caixa_Vencimento, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z879Caixa_Descricao", Z879Caixa_Descricao);
         GxWebStd.gx_hidden_field( context, "Z880Caixa_Valor", StringUtil.LTrim( StringUtil.NToC( Z880Caixa_Valor, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z881Caixa_TipoDeContaCod", StringUtil.RTrim( Z881Caixa_TipoDeContaCod));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N881Caixa_TipoDeContaCod", StringUtil.RTrim( A881Caixa_TipoDeContaCod));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vCAIXA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Caixa_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CAIXA_TIPODECONTACOD", StringUtil.RTrim( AV11Insert_Caixa_TipoDeContaCod));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV14Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCAIXA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13Caixa_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Caixa";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A874Caixa_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("caixa:[SendSecurityCheck value for]"+"Caixa_Codigo:"+context.localUtil.Format( (decimal)(A874Caixa_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("caixa:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("caixa.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV13Caixa_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Caixa" ;
      }

      public override String GetPgmdesc( )
      {
         return "Fluxo de Caixa" ;
      }

      protected void InitializeNonKey2S110( )
      {
         A881Caixa_TipoDeContaCod = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A881Caixa_TipoDeContaCod", A881Caixa_TipoDeContaCod);
         A875Caixa_Documento = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A875Caixa_Documento", A875Caixa_Documento);
         A876Caixa_Emissao = DateTime.MinValue;
         n876Caixa_Emissao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A876Caixa_Emissao", context.localUtil.Format(A876Caixa_Emissao, "99/99/99"));
         n876Caixa_Emissao = ((DateTime.MinValue==A876Caixa_Emissao) ? true : false);
         A877Caixa_Vencimento = DateTime.MinValue;
         n877Caixa_Vencimento = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A877Caixa_Vencimento", context.localUtil.Format(A877Caixa_Vencimento, "99/99/99"));
         n877Caixa_Vencimento = ((DateTime.MinValue==A877Caixa_Vencimento) ? true : false);
         A879Caixa_Descricao = "";
         n879Caixa_Descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A879Caixa_Descricao", A879Caixa_Descricao);
         n879Caixa_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A879Caixa_Descricao)) ? true : false);
         A880Caixa_Valor = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A880Caixa_Valor", StringUtil.LTrim( StringUtil.Str( A880Caixa_Valor, 18, 5)));
         Z875Caixa_Documento = "";
         Z876Caixa_Emissao = DateTime.MinValue;
         Z877Caixa_Vencimento = DateTime.MinValue;
         Z879Caixa_Descricao = "";
         Z880Caixa_Valor = 0;
         Z881Caixa_TipoDeContaCod = "";
      }

      protected void InitAll2S110( )
      {
         A874Caixa_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A874Caixa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A874Caixa_Codigo), 6, 0)));
         InitializeNonKey2S110( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202051812514777");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("caixa.js", "?202051812514777");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcaixa_codigo_Internalname = "TEXTBLOCKCAIXA_CODIGO";
         edtCaixa_Codigo_Internalname = "CAIXA_CODIGO";
         lblTextblockcaixa_documento_Internalname = "TEXTBLOCKCAIXA_DOCUMENTO";
         edtCaixa_Documento_Internalname = "CAIXA_DOCUMENTO";
         lblTextblockcaixa_emissao_Internalname = "TEXTBLOCKCAIXA_EMISSAO";
         edtCaixa_Emissao_Internalname = "CAIXA_EMISSAO";
         lblTextblockcaixa_vencimento_Internalname = "TEXTBLOCKCAIXA_VENCIMENTO";
         edtCaixa_Vencimento_Internalname = "CAIXA_VENCIMENTO";
         tblTablemergedcaixa_emissao_Internalname = "TABLEMERGEDCAIXA_EMISSAO";
         lblTextblockcaixa_tipodecontacod_Internalname = "TEXTBLOCKCAIXA_TIPODECONTACOD";
         edtCaixa_TipoDeContaCod_Internalname = "CAIXA_TIPODECONTACOD";
         lblTextblockcaixa_descricao_Internalname = "TEXTBLOCKCAIXA_DESCRICAO";
         edtCaixa_Descricao_Internalname = "CAIXA_DESCRICAO";
         lblTextblockcaixa_valor_Internalname = "TEXTBLOCKCAIXA_VALOR";
         edtCaixa_Valor_Internalname = "CAIXA_VALOR";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Fluxo de Caixa";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Fluxo de Caixa";
         edtCaixa_Vencimento_Jsonclick = "";
         edtCaixa_Vencimento_Enabled = 1;
         edtCaixa_Emissao_Jsonclick = "";
         edtCaixa_Emissao_Enabled = 1;
         edtCaixa_Valor_Jsonclick = "";
         edtCaixa_Valor_Enabled = 1;
         edtCaixa_Descricao_Jsonclick = "";
         edtCaixa_Descricao_Enabled = 1;
         edtCaixa_TipoDeContaCod_Jsonclick = "";
         edtCaixa_TipoDeContaCod_Enabled = 1;
         edtCaixa_Documento_Jsonclick = "";
         edtCaixa_Documento_Enabled = 1;
         edtCaixa_Codigo_Jsonclick = "";
         edtCaixa_Codigo_Enabled = 0;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXSGACAIXA_TIPODECONTACOD2S0( String A871TipodeConta_Nome )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXSGACAIXA_TIPODECONTACOD_data2S0( A871TipodeConta_Nome) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXSGACAIXA_TIPODECONTACOD_data2S0( String A871TipodeConta_Nome )
      {
         l871TipodeConta_Nome = StringUtil.PadR( StringUtil.RTrim( A871TipodeConta_Nome), 50, "%");
         /* Using cursor T002S16 */
         pr_default.execute(14, new Object[] {l871TipodeConta_Nome});
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         while ( (pr_default.getStatus(14) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.RTrim( T002S16_A871TipodeConta_Nome[0]));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T002S16_A871TipodeConta_Nome[0]));
            pr_default.readNext(14);
         }
         pr_default.close(14);
      }

      protected void GXHCACAIXA_TIPODECONTACOD2S110( String A871TipodeConta_Nome )
      {
         /* Using cursor T002S17 */
         pr_default.execute(15, new Object[] {A871TipodeConta_Nome});
         gxhchits = 0;
         while ( (pr_default.getStatus(15) != 101) )
         {
            gxhchits = (short)(gxhchits+1);
            if ( gxhchits > 1 )
            {
               if (true) break;
            }
            A871TipodeConta_Nome = T002S17_A871TipodeConta_Nome[0];
            A870TipodeConta_Codigo = T002S17_A870TipodeConta_Codigo[0];
            pr_default.readNext(15);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A870TipodeConta_Codigo))+"\"");
         context.GX_webresponse.AddString(")");
         if ( gxhchits > 1 )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("\"ambiguousck\"");
         }
         if ( gxhchits == 0 )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(15);
      }

      public void Valid_Caixa_tipodecontacod( String GX_Parm1 ,
                                              String GX_Parm2 )
      {
         h881Caixa_TipoDeContaCod = GX_Parm1;
         A881Caixa_TipoDeContaCod = GX_Parm2;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( h881Caixa_TipoDeContaCod)) )
         {
            A881Caixa_TipoDeContaCod = "";
         }
         else
         {
            A871TipodeConta_Nome = h881Caixa_TipoDeContaCod;
            /* Using cursor T002S18 */
            pr_default.execute(16, new Object[] {A871TipodeConta_Nome});
            A881Caixa_TipoDeContaCod = T002S18_A870TipodeConta_Codigo[0];
            if ( ! ( (pr_default.getStatus(16) == 101) ) )
            {
               pr_default.readNext(16);
               if ( ! ( (pr_default.getStatus(16) == 101) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_ambiguousck", new   object[]  {"Nome"}), 1, "CAIXA_TIPODECONTACOD");
                  AnyError = 1;
                  GX_FocusControl = edtCaixa_TipoDeContaCod_Internalname;
               }
            }
            else
            {
            }
            pr_default.close(16);
         }
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "h881Caixa_TipoDeContaCod", h881Caixa_TipoDeContaCod);
         /* Using cursor T002S19 */
         pr_default.execute(17, new Object[] {A881Caixa_TipoDeContaCod});
         if ( (pr_default.getStatus(17) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Caixa_Tipo De Conta'.", "ForeignKeyNotFound", 1, "CAIXA_TIPODECONTACOD");
            AnyError = 1;
            GX_FocusControl = edtCaixa_TipoDeContaCod_Internalname;
         }
         pr_default.close(17);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(StringUtil.RTrim( A881Caixa_TipoDeContaCod));
         isValidOutput.Add(StringUtil.RTrim( h881Caixa_TipoDeContaCod));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV13Caixa_Codigo',fld:'vCAIXA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E122S2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(17);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z875Caixa_Documento = "";
         Z876Caixa_Emissao = DateTime.MinValue;
         Z877Caixa_Vencimento = DateTime.MinValue;
         Z879Caixa_Descricao = "";
         Z881Caixa_TipoDeContaCod = "";
         N881Caixa_TipoDeContaCod = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         A871TipodeConta_Nome = "";
         h881Caixa_TipoDeContaCod = "";
         A881Caixa_TipoDeContaCod = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockcaixa_codigo_Jsonclick = "";
         lblTextblockcaixa_documento_Jsonclick = "";
         A875Caixa_Documento = "";
         lblTextblockcaixa_emissao_Jsonclick = "";
         lblTextblockcaixa_tipodecontacod_Jsonclick = "";
         lblTextblockcaixa_descricao_Jsonclick = "";
         A879Caixa_Descricao = "";
         lblTextblockcaixa_valor_Jsonclick = "";
         A876Caixa_Emissao = DateTime.MinValue;
         lblTextblockcaixa_vencimento_Jsonclick = "";
         A877Caixa_Vencimento = DateTime.MinValue;
         AV11Insert_Caixa_TipoDeContaCod = "";
         AV14Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode110 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         T002S5_A871TipodeConta_Nome = new String[] {""} ;
         T002S5_A870TipodeConta_Codigo = new String[] {""} ;
         A870TipodeConta_Codigo = "";
         T002S6_A874Caixa_Codigo = new int[1] ;
         T002S6_A875Caixa_Documento = new String[] {""} ;
         T002S6_A876Caixa_Emissao = new DateTime[] {DateTime.MinValue} ;
         T002S6_n876Caixa_Emissao = new bool[] {false} ;
         T002S6_A877Caixa_Vencimento = new DateTime[] {DateTime.MinValue} ;
         T002S6_n877Caixa_Vencimento = new bool[] {false} ;
         T002S6_A879Caixa_Descricao = new String[] {""} ;
         T002S6_n879Caixa_Descricao = new bool[] {false} ;
         T002S6_A880Caixa_Valor = new decimal[1] ;
         T002S6_A881Caixa_TipoDeContaCod = new String[] {""} ;
         T002S7_A871TipodeConta_Nome = new String[] {""} ;
         T002S7_A870TipodeConta_Codigo = new String[] {""} ;
         T002S4_A881Caixa_TipoDeContaCod = new String[] {""} ;
         T002S8_A881Caixa_TipoDeContaCod = new String[] {""} ;
         T002S9_A874Caixa_Codigo = new int[1] ;
         T002S3_A874Caixa_Codigo = new int[1] ;
         T002S3_A875Caixa_Documento = new String[] {""} ;
         T002S3_A876Caixa_Emissao = new DateTime[] {DateTime.MinValue} ;
         T002S3_n876Caixa_Emissao = new bool[] {false} ;
         T002S3_A877Caixa_Vencimento = new DateTime[] {DateTime.MinValue} ;
         T002S3_n877Caixa_Vencimento = new bool[] {false} ;
         T002S3_A879Caixa_Descricao = new String[] {""} ;
         T002S3_n879Caixa_Descricao = new bool[] {false} ;
         T002S3_A880Caixa_Valor = new decimal[1] ;
         T002S3_A881Caixa_TipoDeContaCod = new String[] {""} ;
         T002S10_A874Caixa_Codigo = new int[1] ;
         T002S11_A874Caixa_Codigo = new int[1] ;
         T002S2_A874Caixa_Codigo = new int[1] ;
         T002S2_A875Caixa_Documento = new String[] {""} ;
         T002S2_A876Caixa_Emissao = new DateTime[] {DateTime.MinValue} ;
         T002S2_n876Caixa_Emissao = new bool[] {false} ;
         T002S2_A877Caixa_Vencimento = new DateTime[] {DateTime.MinValue} ;
         T002S2_n877Caixa_Vencimento = new bool[] {false} ;
         T002S2_A879Caixa_Descricao = new String[] {""} ;
         T002S2_n879Caixa_Descricao = new bool[] {false} ;
         T002S2_A880Caixa_Valor = new decimal[1] ;
         T002S2_A881Caixa_TipoDeContaCod = new String[] {""} ;
         T002S12_A874Caixa_Codigo = new int[1] ;
         T002S15_A874Caixa_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         l871TipodeConta_Nome = "";
         T002S16_A871TipodeConta_Nome = new String[] {""} ;
         T002S17_A871TipodeConta_Nome = new String[] {""} ;
         T002S17_A870TipodeConta_Codigo = new String[] {""} ;
         T002S18_A871TipodeConta_Nome = new String[] {""} ;
         T002S18_A870TipodeConta_Codigo = new String[] {""} ;
         T002S19_A881Caixa_TipoDeContaCod = new String[] {""} ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.caixa__default(),
            new Object[][] {
                new Object[] {
               T002S2_A874Caixa_Codigo, T002S2_A875Caixa_Documento, T002S2_A876Caixa_Emissao, T002S2_n876Caixa_Emissao, T002S2_A877Caixa_Vencimento, T002S2_n877Caixa_Vencimento, T002S2_A879Caixa_Descricao, T002S2_n879Caixa_Descricao, T002S2_A880Caixa_Valor, T002S2_A881Caixa_TipoDeContaCod
               }
               , new Object[] {
               T002S3_A874Caixa_Codigo, T002S3_A875Caixa_Documento, T002S3_A876Caixa_Emissao, T002S3_n876Caixa_Emissao, T002S3_A877Caixa_Vencimento, T002S3_n877Caixa_Vencimento, T002S3_A879Caixa_Descricao, T002S3_n879Caixa_Descricao, T002S3_A880Caixa_Valor, T002S3_A881Caixa_TipoDeContaCod
               }
               , new Object[] {
               T002S4_A881Caixa_TipoDeContaCod
               }
               , new Object[] {
               T002S5_A871TipodeConta_Nome, T002S5_A870TipodeConta_Codigo
               }
               , new Object[] {
               T002S6_A874Caixa_Codigo, T002S6_A875Caixa_Documento, T002S6_A876Caixa_Emissao, T002S6_n876Caixa_Emissao, T002S6_A877Caixa_Vencimento, T002S6_n877Caixa_Vencimento, T002S6_A879Caixa_Descricao, T002S6_n879Caixa_Descricao, T002S6_A880Caixa_Valor, T002S6_A881Caixa_TipoDeContaCod
               }
               , new Object[] {
               T002S7_A871TipodeConta_Nome, T002S7_A870TipodeConta_Codigo
               }
               , new Object[] {
               T002S8_A881Caixa_TipoDeContaCod
               }
               , new Object[] {
               T002S9_A874Caixa_Codigo
               }
               , new Object[] {
               T002S10_A874Caixa_Codigo
               }
               , new Object[] {
               T002S11_A874Caixa_Codigo
               }
               , new Object[] {
               T002S12_A874Caixa_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002S15_A874Caixa_Codigo
               }
               , new Object[] {
               T002S16_A871TipodeConta_Nome
               }
               , new Object[] {
               T002S17_A871TipodeConta_Nome, T002S17_A870TipodeConta_Codigo
               }
               , new Object[] {
               T002S18_A871TipodeConta_Nome, T002S18_A870TipodeConta_Codigo
               }
               , new Object[] {
               T002S19_A881Caixa_TipoDeContaCod
               }
            }
         );
         AV14Pgmname = "Caixa";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound110 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short gxhchits ;
      private short wbTemp ;
      private int wcpOAV13Caixa_Codigo ;
      private int Z874Caixa_Codigo ;
      private int AV13Caixa_Codigo ;
      private int trnEnded ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int A874Caixa_Codigo ;
      private int edtCaixa_Codigo_Enabled ;
      private int edtCaixa_Documento_Enabled ;
      private int edtCaixa_TipoDeContaCod_Enabled ;
      private int edtCaixa_Descricao_Enabled ;
      private int edtCaixa_Valor_Enabled ;
      private int edtCaixa_Emissao_Enabled ;
      private int edtCaixa_Vencimento_Enabled ;
      private int AV15GXV1 ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private decimal Z880Caixa_Valor ;
      private decimal A880Caixa_Valor ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z881Caixa_TipoDeContaCod ;
      private String N881Caixa_TipoDeContaCod ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String A871TipodeConta_Nome ;
      private String h881Caixa_TipoDeContaCod ;
      private String A881Caixa_TipoDeContaCod ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtCaixa_Documento_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcaixa_codigo_Internalname ;
      private String lblTextblockcaixa_codigo_Jsonclick ;
      private String edtCaixa_Codigo_Internalname ;
      private String edtCaixa_Codigo_Jsonclick ;
      private String lblTextblockcaixa_documento_Internalname ;
      private String lblTextblockcaixa_documento_Jsonclick ;
      private String edtCaixa_Documento_Jsonclick ;
      private String lblTextblockcaixa_emissao_Internalname ;
      private String lblTextblockcaixa_emissao_Jsonclick ;
      private String lblTextblockcaixa_tipodecontacod_Internalname ;
      private String lblTextblockcaixa_tipodecontacod_Jsonclick ;
      private String edtCaixa_TipoDeContaCod_Internalname ;
      private String edtCaixa_TipoDeContaCod_Jsonclick ;
      private String lblTextblockcaixa_descricao_Internalname ;
      private String lblTextblockcaixa_descricao_Jsonclick ;
      private String edtCaixa_Descricao_Internalname ;
      private String edtCaixa_Descricao_Jsonclick ;
      private String lblTextblockcaixa_valor_Internalname ;
      private String lblTextblockcaixa_valor_Jsonclick ;
      private String edtCaixa_Valor_Internalname ;
      private String edtCaixa_Valor_Jsonclick ;
      private String tblTablemergedcaixa_emissao_Internalname ;
      private String edtCaixa_Emissao_Internalname ;
      private String edtCaixa_Emissao_Jsonclick ;
      private String lblTextblockcaixa_vencimento_Internalname ;
      private String lblTextblockcaixa_vencimento_Jsonclick ;
      private String edtCaixa_Vencimento_Internalname ;
      private String edtCaixa_Vencimento_Jsonclick ;
      private String AV11Insert_Caixa_TipoDeContaCod ;
      private String AV14Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode110 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String A870TipodeConta_Codigo ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private String l871TipodeConta_Nome ;
      private DateTime Z876Caixa_Emissao ;
      private DateTime Z877Caixa_Vencimento ;
      private DateTime A876Caixa_Emissao ;
      private DateTime A877Caixa_Vencimento ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n876Caixa_Emissao ;
      private bool n877Caixa_Vencimento ;
      private bool n879Caixa_Descricao ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private String Z875Caixa_Documento ;
      private String Z879Caixa_Descricao ;
      private String A875Caixa_Documento ;
      private String A879Caixa_Descricao ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] T002S5_A871TipodeConta_Nome ;
      private String[] T002S5_A870TipodeConta_Codigo ;
      private int[] T002S6_A874Caixa_Codigo ;
      private String[] T002S6_A875Caixa_Documento ;
      private DateTime[] T002S6_A876Caixa_Emissao ;
      private bool[] T002S6_n876Caixa_Emissao ;
      private DateTime[] T002S6_A877Caixa_Vencimento ;
      private bool[] T002S6_n877Caixa_Vencimento ;
      private String[] T002S6_A879Caixa_Descricao ;
      private bool[] T002S6_n879Caixa_Descricao ;
      private decimal[] T002S6_A880Caixa_Valor ;
      private String[] T002S6_A881Caixa_TipoDeContaCod ;
      private String[] T002S7_A871TipodeConta_Nome ;
      private String[] T002S7_A870TipodeConta_Codigo ;
      private String[] T002S4_A881Caixa_TipoDeContaCod ;
      private String[] T002S8_A881Caixa_TipoDeContaCod ;
      private int[] T002S9_A874Caixa_Codigo ;
      private int[] T002S3_A874Caixa_Codigo ;
      private String[] T002S3_A875Caixa_Documento ;
      private DateTime[] T002S3_A876Caixa_Emissao ;
      private bool[] T002S3_n876Caixa_Emissao ;
      private DateTime[] T002S3_A877Caixa_Vencimento ;
      private bool[] T002S3_n877Caixa_Vencimento ;
      private String[] T002S3_A879Caixa_Descricao ;
      private bool[] T002S3_n879Caixa_Descricao ;
      private decimal[] T002S3_A880Caixa_Valor ;
      private String[] T002S3_A881Caixa_TipoDeContaCod ;
      private int[] T002S10_A874Caixa_Codigo ;
      private int[] T002S11_A874Caixa_Codigo ;
      private int[] T002S2_A874Caixa_Codigo ;
      private String[] T002S2_A875Caixa_Documento ;
      private DateTime[] T002S2_A876Caixa_Emissao ;
      private bool[] T002S2_n876Caixa_Emissao ;
      private DateTime[] T002S2_A877Caixa_Vencimento ;
      private bool[] T002S2_n877Caixa_Vencimento ;
      private String[] T002S2_A879Caixa_Descricao ;
      private bool[] T002S2_n879Caixa_Descricao ;
      private decimal[] T002S2_A880Caixa_Valor ;
      private String[] T002S2_A881Caixa_TipoDeContaCod ;
      private int[] T002S12_A874Caixa_Codigo ;
      private int[] T002S15_A874Caixa_Codigo ;
      private String[] T002S16_A871TipodeConta_Nome ;
      private String[] T002S17_A871TipodeConta_Nome ;
      private String[] T002S17_A870TipodeConta_Codigo ;
      private String[] T002S18_A871TipodeConta_Nome ;
      private String[] T002S18_A870TipodeConta_Codigo ;
      private String[] T002S19_A881Caixa_TipoDeContaCod ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class caixa__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT002S5 ;
          prmT002S5 = new Object[] {
          new Object[] {"@Caixa_TipoDeContaCod",SqlDbType.Char,20,0}
          } ;
          Object[] prmT002S6 ;
          prmT002S6 = new Object[] {
          new Object[] {"@Caixa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002S7 ;
          prmT002S7 = new Object[] {
          new Object[] {"@Caixa_TipoDeContaCod",SqlDbType.Char,20,0}
          } ;
          Object[] prmT002S4 ;
          prmT002S4 = new Object[] {
          new Object[] {"@Caixa_TipoDeContaCod",SqlDbType.Char,20,0}
          } ;
          Object[] prmT002S8 ;
          prmT002S8 = new Object[] {
          new Object[] {"@Caixa_TipoDeContaCod",SqlDbType.Char,20,0}
          } ;
          Object[] prmT002S9 ;
          prmT002S9 = new Object[] {
          new Object[] {"@Caixa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002S3 ;
          prmT002S3 = new Object[] {
          new Object[] {"@Caixa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002S10 ;
          prmT002S10 = new Object[] {
          new Object[] {"@Caixa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002S11 ;
          prmT002S11 = new Object[] {
          new Object[] {"@Caixa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002S2 ;
          prmT002S2 = new Object[] {
          new Object[] {"@Caixa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002S12 ;
          prmT002S12 = new Object[] {
          new Object[] {"@Caixa_Documento",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Caixa_Emissao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Caixa_Vencimento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Caixa_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Caixa_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Caixa_TipoDeContaCod",SqlDbType.Char,20,0}
          } ;
          Object[] prmT002S13 ;
          prmT002S13 = new Object[] {
          new Object[] {"@Caixa_Documento",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Caixa_Emissao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Caixa_Vencimento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Caixa_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Caixa_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Caixa_TipoDeContaCod",SqlDbType.Char,20,0} ,
          new Object[] {"@Caixa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002S14 ;
          prmT002S14 = new Object[] {
          new Object[] {"@Caixa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002S15 ;
          prmT002S15 = new Object[] {
          } ;
          Object[] prmT002S16 ;
          prmT002S16 = new Object[] {
          new Object[] {"@l871TipodeConta_Nome",SqlDbType.Char,50,0}
          } ;
          Object[] prmT002S17 ;
          prmT002S17 = new Object[] {
          new Object[] {"@TipodeConta_Nome",SqlDbType.Char,50,0}
          } ;
          Object[] prmT002S18 ;
          prmT002S18 = new Object[] {
          new Object[] {"@TipodeConta_Nome",SqlDbType.Char,50,0}
          } ;
          Object[] prmT002S19 ;
          prmT002S19 = new Object[] {
          new Object[] {"@Caixa_TipoDeContaCod",SqlDbType.Char,20,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T002S2", "SELECT [Caixa_Codigo], [Caixa_Documento], [Caixa_Emissao], [Caixa_Vencimento], [Caixa_Descricao], [Caixa_Valor], [Caixa_TipoDeContaCod] AS Caixa_TipoDeContaCod FROM [Caixa] WITH (UPDLOCK) WHERE [Caixa_Codigo] = @Caixa_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002S2,1,0,true,false )
             ,new CursorDef("T002S3", "SELECT [Caixa_Codigo], [Caixa_Documento], [Caixa_Emissao], [Caixa_Vencimento], [Caixa_Descricao], [Caixa_Valor], [Caixa_TipoDeContaCod] AS Caixa_TipoDeContaCod FROM [Caixa] WITH (NOLOCK) WHERE [Caixa_Codigo] = @Caixa_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002S3,1,0,true,false )
             ,new CursorDef("T002S4", "SELECT [TipodeConta_Codigo] AS Caixa_TipoDeContaCod FROM [TipodeConta] WITH (NOLOCK) WHERE [TipodeConta_Codigo] = @Caixa_TipoDeContaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002S4,1,0,true,false )
             ,new CursorDef("T002S5", "SELECT [TipodeConta_Nome], [TipodeConta_Codigo] FROM [TipodeConta] WITH (NOLOCK) WHERE [TipodeConta_Codigo] = @Caixa_TipoDeContaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002S5,0,0,true,false )
             ,new CursorDef("T002S6", "SELECT TM1.[Caixa_Codigo], TM1.[Caixa_Documento], TM1.[Caixa_Emissao], TM1.[Caixa_Vencimento], TM1.[Caixa_Descricao], TM1.[Caixa_Valor], TM1.[Caixa_TipoDeContaCod] AS Caixa_TipoDeContaCod FROM [Caixa] TM1 WITH (NOLOCK) WHERE TM1.[Caixa_Codigo] = @Caixa_Codigo ORDER BY TM1.[Caixa_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002S6,100,0,true,false )
             ,new CursorDef("T002S7", "SELECT [TipodeConta_Nome], [TipodeConta_Codigo] FROM [TipodeConta] WITH (NOLOCK) WHERE [TipodeConta_Codigo] = @Caixa_TipoDeContaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002S7,0,0,true,false )
             ,new CursorDef("T002S8", "SELECT [TipodeConta_Codigo] AS Caixa_TipoDeContaCod FROM [TipodeConta] WITH (NOLOCK) WHERE [TipodeConta_Codigo] = @Caixa_TipoDeContaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002S8,1,0,true,false )
             ,new CursorDef("T002S9", "SELECT [Caixa_Codigo] FROM [Caixa] WITH (NOLOCK) WHERE [Caixa_Codigo] = @Caixa_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002S9,1,0,true,false )
             ,new CursorDef("T002S10", "SELECT TOP 1 [Caixa_Codigo] FROM [Caixa] WITH (NOLOCK) WHERE ( [Caixa_Codigo] > @Caixa_Codigo) ORDER BY [Caixa_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002S10,1,0,true,true )
             ,new CursorDef("T002S11", "SELECT TOP 1 [Caixa_Codigo] FROM [Caixa] WITH (NOLOCK) WHERE ( [Caixa_Codigo] < @Caixa_Codigo) ORDER BY [Caixa_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002S11,1,0,true,true )
             ,new CursorDef("T002S12", "INSERT INTO [Caixa]([Caixa_Documento], [Caixa_Emissao], [Caixa_Vencimento], [Caixa_Descricao], [Caixa_Valor], [Caixa_TipoDeContaCod]) VALUES(@Caixa_Documento, @Caixa_Emissao, @Caixa_Vencimento, @Caixa_Descricao, @Caixa_Valor, @Caixa_TipoDeContaCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT002S12)
             ,new CursorDef("T002S13", "UPDATE [Caixa] SET [Caixa_Documento]=@Caixa_Documento, [Caixa_Emissao]=@Caixa_Emissao, [Caixa_Vencimento]=@Caixa_Vencimento, [Caixa_Descricao]=@Caixa_Descricao, [Caixa_Valor]=@Caixa_Valor, [Caixa_TipoDeContaCod]=@Caixa_TipoDeContaCod  WHERE [Caixa_Codigo] = @Caixa_Codigo", GxErrorMask.GX_NOMASK,prmT002S13)
             ,new CursorDef("T002S14", "DELETE FROM [Caixa]  WHERE [Caixa_Codigo] = @Caixa_Codigo", GxErrorMask.GX_NOMASK,prmT002S14)
             ,new CursorDef("T002S15", "SELECT [Caixa_Codigo] FROM [Caixa] WITH (NOLOCK) ORDER BY [Caixa_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002S15,100,0,true,false )
             ,new CursorDef("T002S16", "SELECT DISTINCT TOP 5 [TipodeConta_Nome] FROM [TipodeConta] WITH (NOLOCK) WHERE UPPER([TipodeConta_Nome]) like UPPER(@l871TipodeConta_Nome) ORDER BY [TipodeConta_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002S16,0,0,true,false )
             ,new CursorDef("T002S17", "SELECT [TipodeConta_Nome], [TipodeConta_Codigo] FROM [TipodeConta] WITH (NOLOCK) WHERE [TipodeConta_Nome] = @TipodeConta_Nome ",true, GxErrorMask.GX_NOMASK, false, this,prmT002S17,0,0,true,false )
             ,new CursorDef("T002S18", "SELECT [TipodeConta_Nome], [TipodeConta_Codigo] FROM [TipodeConta] WITH (NOLOCK) WHERE [TipodeConta_Nome] = @TipodeConta_Nome  OPTION (FAST 0)",true, GxErrorMask.GX_NOMASK, false, this,prmT002S18,0,0,true,false )
             ,new CursorDef("T002S19", "SELECT [TipodeConta_Codigo] AS Caixa_TipoDeContaCod FROM [TipodeConta] WITH (NOLOCK) WHERE [TipodeConta_Codigo] = @Caixa_TipoDeContaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002S19,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((String[]) buf[9])[0] = rslt.getString(7, 20) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((String[]) buf[9])[0] = rslt.getString(7, 20) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((String[]) buf[9])[0] = rslt.getString(7, 20) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                return;
             case 16 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(2, (DateTime)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(3, (DateTime)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                stmt.SetParameter(5, (decimal)parms[7]);
                stmt.SetParameter(6, (String)parms[8]);
                return;
             case 11 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(2, (DateTime)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(3, (DateTime)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                stmt.SetParameter(5, (decimal)parms[7]);
                stmt.SetParameter(6, (String)parms[8]);
                stmt.SetParameter(7, (int)parms[9]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
       }
    }

 }

}
