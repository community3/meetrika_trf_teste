/*
               File: PRC_GestorPrepostoContrato
        Description: Gestor Preposto do Contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:13:25.46
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_gestorprepostocontrato : GXProcedure
   {
      public prc_gestorprepostocontrato( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_gestorprepostocontrato( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo ,
                           out short aP1_Responsavel )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV8Responsavel = 0 ;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_Responsavel=this.AV8Responsavel;
      }

      public short executeUdp( ref int aP0_ContagemResultado_Codigo )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV8Responsavel = 0 ;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_Responsavel=this.AV8Responsavel;
         return AV8Responsavel ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo ,
                                 out short aP1_Responsavel )
      {
         prc_gestorprepostocontrato objprc_gestorprepostocontrato;
         objprc_gestorprepostocontrato = new prc_gestorprepostocontrato();
         objprc_gestorprepostocontrato.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_gestorprepostocontrato.AV8Responsavel = 0 ;
         objprc_gestorprepostocontrato.context.SetSubmitInitialConfig(context);
         objprc_gestorprepostocontrato.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_gestorprepostocontrato);
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_Responsavel=this.AV8Responsavel;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_gestorprepostocontrato)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00DG2 */
         pr_default.execute(0, new Object[] {AV9Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A490ContagemResultado_ContratadaCod = P00DG2_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00DG2_n490ContagemResultado_ContratadaCod[0];
            A1553ContagemResultado_CntSrvCod = P00DG2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00DG2_n1553ContagemResultado_CntSrvCod[0];
            A1481Contratada_UsaOSistema = P00DG2_A1481Contratada_UsaOSistema[0];
            n1481Contratada_UsaOSistema = P00DG2_n1481Contratada_UsaOSistema[0];
            A1603ContagemResultado_CntCod = P00DG2_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P00DG2_n1603ContagemResultado_CntCod[0];
            A1604ContagemResultado_CntPrpCod = P00DG2_A1604ContagemResultado_CntPrpCod[0];
            n1604ContagemResultado_CntPrpCod = P00DG2_n1604ContagemResultado_CntPrpCod[0];
            A1481Contratada_UsaOSistema = P00DG2_A1481Contratada_UsaOSistema[0];
            n1481Contratada_UsaOSistema = P00DG2_n1481Contratada_UsaOSistema[0];
            A1603ContagemResultado_CntCod = P00DG2_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P00DG2_n1603ContagemResultado_CntCod[0];
            A1604ContagemResultado_CntPrpCod = P00DG2_A1604ContagemResultado_CntPrpCod[0];
            n1604ContagemResultado_CntPrpCod = P00DG2_n1604ContagemResultado_CntPrpCod[0];
            if ( A1481Contratada_UsaOSistema && ( A1604ContagemResultado_CntPrpCod > 0 ) )
            {
               AV8Responsavel = (short)(A1604ContagemResultado_CntPrpCod);
            }
            else
            {
               AV13GXLvl7 = 0;
               /* Using cursor P00DG3 */
               pr_default.execute(1, new Object[] {n1603ContagemResultado_CntCod, A1603ContagemResultado_CntCod, n1481Contratada_UsaOSistema, A1481Contratada_UsaOSistema});
               while ( (pr_default.getStatus(1) != 101) )
               {
                  A1078ContratoGestor_ContratoCod = P00DG3_A1078ContratoGestor_ContratoCod[0];
                  A1079ContratoGestor_UsuarioCod = P00DG3_A1079ContratoGestor_UsuarioCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = P00DG3_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = P00DG3_n1446ContratoGestor_ContratadaAreaCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = P00DG3_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = P00DG3_n1446ContratoGestor_ContratadaAreaCod[0];
                  GXt_boolean1 = A1135ContratoGestor_UsuarioEhContratante;
                  new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean1) ;
                  A1135ContratoGestor_UsuarioEhContratante = GXt_boolean1;
                  if ( ! A1135ContratoGestor_UsuarioEhContratante )
                  {
                     AV13GXLvl7 = 1;
                     AV8Responsavel = (short)(A1079ContratoGestor_UsuarioCod);
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
                  pr_default.readNext(1);
               }
               pr_default.close(1);
               if ( AV13GXLvl7 == 0 )
               {
                  AV14GXLvl14 = 0;
                  /* Using cursor P00DG4 */
                  pr_default.execute(2, new Object[] {A456ContagemResultado_Codigo});
                  while ( (pr_default.getStatus(2) != 101) )
                  {
                     A896LogResponsavel_Owner = P00DG4_A896LogResponsavel_Owner[0];
                     A1797LogResponsavel_Codigo = P00DG4_A1797LogResponsavel_Codigo[0];
                     A891LogResponsavel_UsuarioCod = P00DG4_A891LogResponsavel_UsuarioCod[0];
                     n891LogResponsavel_UsuarioCod = P00DG4_n891LogResponsavel_UsuarioCod[0];
                     A892LogResponsavel_DemandaCod = P00DG4_A892LogResponsavel_DemandaCod[0];
                     n892LogResponsavel_DemandaCod = P00DG4_n892LogResponsavel_DemandaCod[0];
                     GXt_boolean1 = A1148LogResponsavel_UsuarioEhContratante;
                     new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A891LogResponsavel_UsuarioCod, out  GXt_boolean1) ;
                     A1148LogResponsavel_UsuarioEhContratante = GXt_boolean1;
                     if ( A1148LogResponsavel_UsuarioEhContratante )
                     {
                        AV14GXLvl14 = 1;
                        AV8Responsavel = (short)(A896LogResponsavel_Owner);
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                     }
                     pr_default.readNext(2);
                  }
                  pr_default.close(2);
                  if ( AV14GXLvl14 == 0 )
                  {
                     /* Using cursor P00DG5 */
                     pr_default.execute(3, new Object[] {n1603ContagemResultado_CntCod, A1603ContagemResultado_CntCod});
                     while ( (pr_default.getStatus(3) != 101) )
                     {
                        A1078ContratoGestor_ContratoCod = P00DG5_A1078ContratoGestor_ContratoCod[0];
                        A1079ContratoGestor_UsuarioCod = P00DG5_A1079ContratoGestor_UsuarioCod[0];
                        A1446ContratoGestor_ContratadaAreaCod = P00DG5_A1446ContratoGestor_ContratadaAreaCod[0];
                        n1446ContratoGestor_ContratadaAreaCod = P00DG5_n1446ContratoGestor_ContratadaAreaCod[0];
                        A1446ContratoGestor_ContratadaAreaCod = P00DG5_A1446ContratoGestor_ContratadaAreaCod[0];
                        n1446ContratoGestor_ContratadaAreaCod = P00DG5_n1446ContratoGestor_ContratadaAreaCod[0];
                        GXt_boolean1 = A1135ContratoGestor_UsuarioEhContratante;
                        new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean1) ;
                        A1135ContratoGestor_UsuarioEhContratante = GXt_boolean1;
                        if ( A1135ContratoGestor_UsuarioEhContratante )
                        {
                           AV8Responsavel = (short)(A1079ContratoGestor_UsuarioCod);
                           /* Exit For each command. Update data (if necessary), close cursors & exit. */
                           if (true) break;
                        }
                        pr_default.readNext(3);
                     }
                     pr_default.close(3);
                  }
               }
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00DG2_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00DG2_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00DG2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00DG2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00DG2_A456ContagemResultado_Codigo = new int[1] ;
         P00DG2_A1481Contratada_UsaOSistema = new bool[] {false} ;
         P00DG2_n1481Contratada_UsaOSistema = new bool[] {false} ;
         P00DG2_A1603ContagemResultado_CntCod = new int[1] ;
         P00DG2_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P00DG2_A1604ContagemResultado_CntPrpCod = new int[1] ;
         P00DG2_n1604ContagemResultado_CntPrpCod = new bool[] {false} ;
         P00DG3_A1078ContratoGestor_ContratoCod = new int[1] ;
         P00DG3_A1079ContratoGestor_UsuarioCod = new int[1] ;
         P00DG3_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         P00DG3_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         P00DG4_A896LogResponsavel_Owner = new int[1] ;
         P00DG4_A1797LogResponsavel_Codigo = new long[1] ;
         P00DG4_A891LogResponsavel_UsuarioCod = new int[1] ;
         P00DG4_n891LogResponsavel_UsuarioCod = new bool[] {false} ;
         P00DG4_A892LogResponsavel_DemandaCod = new int[1] ;
         P00DG4_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         P00DG5_A1078ContratoGestor_ContratoCod = new int[1] ;
         P00DG5_A1079ContratoGestor_UsuarioCod = new int[1] ;
         P00DG5_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         P00DG5_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_gestorprepostocontrato__default(),
            new Object[][] {
                new Object[] {
               P00DG2_A490ContagemResultado_ContratadaCod, P00DG2_n490ContagemResultado_ContratadaCod, P00DG2_A1553ContagemResultado_CntSrvCod, P00DG2_n1553ContagemResultado_CntSrvCod, P00DG2_A456ContagemResultado_Codigo, P00DG2_A1481Contratada_UsaOSistema, P00DG2_n1481Contratada_UsaOSistema, P00DG2_A1603ContagemResultado_CntCod, P00DG2_n1603ContagemResultado_CntCod, P00DG2_A1604ContagemResultado_CntPrpCod,
               P00DG2_n1604ContagemResultado_CntPrpCod
               }
               , new Object[] {
               P00DG3_A1078ContratoGestor_ContratoCod, P00DG3_A1079ContratoGestor_UsuarioCod, P00DG3_A1446ContratoGestor_ContratadaAreaCod, P00DG3_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               P00DG4_A896LogResponsavel_Owner, P00DG4_A1797LogResponsavel_Codigo, P00DG4_A891LogResponsavel_UsuarioCod, P00DG4_n891LogResponsavel_UsuarioCod, P00DG4_A892LogResponsavel_DemandaCod, P00DG4_n892LogResponsavel_DemandaCod
               }
               , new Object[] {
               P00DG5_A1078ContratoGestor_ContratoCod, P00DG5_A1079ContratoGestor_UsuarioCod, P00DG5_A1446ContratoGestor_ContratadaAreaCod, P00DG5_n1446ContratoGestor_ContratadaAreaCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV8Responsavel ;
      private short AV13GXLvl7 ;
      private short AV14GXLvl14 ;
      private int A456ContagemResultado_Codigo ;
      private int AV9Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1603ContagemResultado_CntCod ;
      private int A1604ContagemResultado_CntPrpCod ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A1446ContratoGestor_ContratadaAreaCod ;
      private int A896LogResponsavel_Owner ;
      private int A891LogResponsavel_UsuarioCod ;
      private int A892LogResponsavel_DemandaCod ;
      private long A1797LogResponsavel_Codigo ;
      private String scmdbuf ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool A1481Contratada_UsaOSistema ;
      private bool n1481Contratada_UsaOSistema ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n1604ContagemResultado_CntPrpCod ;
      private bool n1446ContratoGestor_ContratadaAreaCod ;
      private bool A1135ContratoGestor_UsuarioEhContratante ;
      private bool n891LogResponsavel_UsuarioCod ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool A1148LogResponsavel_UsuarioEhContratante ;
      private bool GXt_boolean1 ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00DG2_A490ContagemResultado_ContratadaCod ;
      private bool[] P00DG2_n490ContagemResultado_ContratadaCod ;
      private int[] P00DG2_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00DG2_n1553ContagemResultado_CntSrvCod ;
      private int[] P00DG2_A456ContagemResultado_Codigo ;
      private bool[] P00DG2_A1481Contratada_UsaOSistema ;
      private bool[] P00DG2_n1481Contratada_UsaOSistema ;
      private int[] P00DG2_A1603ContagemResultado_CntCod ;
      private bool[] P00DG2_n1603ContagemResultado_CntCod ;
      private int[] P00DG2_A1604ContagemResultado_CntPrpCod ;
      private bool[] P00DG2_n1604ContagemResultado_CntPrpCod ;
      private int[] P00DG3_A1078ContratoGestor_ContratoCod ;
      private int[] P00DG3_A1079ContratoGestor_UsuarioCod ;
      private int[] P00DG3_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] P00DG3_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] P00DG4_A896LogResponsavel_Owner ;
      private long[] P00DG4_A1797LogResponsavel_Codigo ;
      private int[] P00DG4_A891LogResponsavel_UsuarioCod ;
      private bool[] P00DG4_n891LogResponsavel_UsuarioCod ;
      private int[] P00DG4_A892LogResponsavel_DemandaCod ;
      private bool[] P00DG4_n892LogResponsavel_DemandaCod ;
      private int[] P00DG5_A1078ContratoGestor_ContratoCod ;
      private int[] P00DG5_A1079ContratoGestor_UsuarioCod ;
      private int[] P00DG5_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] P00DG5_n1446ContratoGestor_ContratadaAreaCod ;
      private short aP1_Responsavel ;
   }

   public class prc_gestorprepostocontrato__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00DG2 ;
          prmP00DG2 = new Object[] {
          new Object[] {"@AV9Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00DG3 ;
          prmP00DG3 = new Object[] {
          new Object[] {"@ContagemResultado_CntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratada_UsaOSistema",SqlDbType.Bit,4,0}
          } ;
          Object[] prmP00DG4 ;
          prmP00DG4 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00DG5 ;
          prmP00DG5 = new Object[] {
          new Object[] {"@ContagemResultado_CntCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00DG2", "SELECT TOP 1 T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCo, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T2.[Contratada_UsaOSistema], T3.[Contrato_Codigo] AS ContagemResultado_CntCod, T4.[Contrato_PrepostoCod] AS ContagemResultado_CntPrpCod FROM ((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T4 WITH (NOLOCK) ON T4.[Contrato_Codigo] = T3.[Contrato_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @AV9Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00DG2,1,0,true,true )
             ,new CursorDef("P00DG3", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE (T1.[ContratoGestor_ContratoCod] = @ContagemResultado_CntCod) AND (@Contratada_UsaOSistema = 1) ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00DG3,100,0,true,false )
             ,new CursorDef("P00DG4", "SELECT [LogResponsavel_Owner], [LogResponsavel_Codigo], [LogResponsavel_UsuarioCod], [LogResponsavel_DemandaCod] FROM [LogResponsavel] WITH (NOLOCK) WHERE [LogResponsavel_DemandaCod] = @ContagemResultado_Codigo ORDER BY [LogResponsavel_DemandaCod], [LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00DG4,100,0,true,false )
             ,new CursorDef("P00DG5", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE T1.[ContratoGestor_ContratoCod] = @ContagemResultado_CntCod ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00DG5,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((long[]) buf[1])[0] = rslt.getLong(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(2, (bool)parms[3]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
