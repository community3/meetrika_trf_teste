/*
               File: WebPanel1
        Description: Web Panel1
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:52:36.81
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class webpanel1 : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public webpanel1( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public webpanel1( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavAno = new GXCombobox();
         chkavSelected = new GXCheckbox();
         chkavFixo = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_11 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_11_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_11_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               AV9url = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9url", AV9url);
               AV5Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Descricao", AV5Descricao);
               AV11i = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11i", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11i), 8, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( AV9url, AV5Descricao, AV11i) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAMQ2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTMQ2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042823523683");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "white-space: nowrap;" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("webpanel1.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_11", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_11), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vURL", AV9url);
         GxWebStd.gx_hidden_field( context, "vDESCRICAO", AV5Descricao);
         GxWebStd.gx_hidden_field( context, "vI", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11i), 8, 0, ",", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEMQ2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTMQ2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("webpanel1.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WebPanel1" ;
      }

      public override String GetPgmdesc( )
      {
         return "Web Panel1" ;
      }

      protected void WBMQ0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_MQ2( true) ;
         }
         else
         {
            wb_table1_2_MQ2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_MQ2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTMQ2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Web Panel1", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPMQ0( ) ;
      }

      protected void WSMQ2( )
      {
         STARTMQ2( ) ;
         EVTMQ2( ) ;
      }

      protected void EVTMQ2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11MQ2 */
                              E11MQ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'INSERIRFERIADOS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12MQ2 */
                              E12MQ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VANO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13MQ2 */
                              E13MQ2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 12), "GRID.REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                           {
                              nGXsfl_11_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
                              SubsflControlProps_112( ) ;
                              AV14Selected = StringUtil.StrToBool( cgiGet( chkavSelected_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavSelected_Internalname, AV14Selected);
                              AV15Dia = StringUtil.Upper( cgiGet( edtavDia_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDia_Internalname, AV15Dia);
                              if ( context.localUtil.VCDateTime( cgiGet( edtavFeriado_Internalname), 0, 0) == 0 )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Feriado"}), 1, "vFERIADO");
                                 GX_FocusControl = edtavFeriado_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV10Feriado = (DateTime)(DateTime.MinValue);
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavFeriado_Internalname, context.localUtil.Format(AV10Feriado, "99/99/99"));
                              }
                              else
                              {
                                 AV10Feriado = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavFeriado_Internalname), 0));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavFeriado_Internalname, context.localUtil.Format(AV10Feriado, "99/99/99"));
                              }
                              AV13Nome = StringUtil.Upper( cgiGet( edtavNome_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV13Nome);
                              AV17Fixo = StringUtil.StrToBool( cgiGet( chkavFixo_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavFixo_Internalname, AV17Fixo);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14MQ2 */
                                    E14MQ2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E15MQ2 */
                                    E15MQ2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E16MQ2 */
                                    E16MQ2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEMQ2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAMQ2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavAno.Name = "vANO";
            cmbavAno.WebTags = "";
            if ( cmbavAno.ItemCount > 0 )
            {
               AV18ano = (short)(NumberUtil.Val( cmbavAno.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV18ano), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ano), 4, 0)));
            }
            GXCCtl = "vSELECTED_" + sGXsfl_11_idx;
            chkavSelected.Name = GXCCtl;
            chkavSelected.WebTags = "";
            chkavSelected.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavSelected_Internalname, "TitleCaption", chkavSelected.Caption);
            chkavSelected.CheckedValue = "false";
            GXCCtl = "vFIXO_" + sGXsfl_11_idx;
            chkavFixo.Name = GXCCtl;
            chkavFixo.WebTags = "";
            chkavFixo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavFixo_Internalname, "TitleCaption", chkavFixo.Caption);
            chkavFixo.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavAno_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_112( ) ;
         while ( nGXsfl_11_idx <= nRC_GXsfl_11 )
         {
            sendrow_112( ) ;
            nGXsfl_11_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_11_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_11_idx+1));
            sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
            SubsflControlProps_112( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( String AV9url ,
                                       String AV5Descricao ,
                                       int AV11i )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID_nCurrentRecord = 0;
         RFMQ2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavAno.ItemCount > 0 )
         {
            AV18ano = (short)(NumberUtil.Val( cmbavAno.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV18ano), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ano), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         /* Execute user event: E11MQ2 */
         E11MQ2 ();
         RFMQ2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavFeriado_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFeriado_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFeriado_Enabled), 5, 0)));
         edtavNome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNome_Enabled), 5, 0)));
      }

      protected void RFMQ2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 11;
         /* Execute user event: E15MQ2 */
         E15MQ2 ();
         nGXsfl_11_idx = 1;
         sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
         SubsflControlProps_112( ) ;
         nGXsfl_11_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "Grid");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Titlebackcolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Titlebackcolor), 9, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorodd", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorodd), 9, 0, ".", "")));
         GridContainer.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Titleforecolor), 9, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_112( ) ;
            /* Execute user event: E16MQ2 */
            E16MQ2 ();
            wbEnd = 11;
            WBMQ0( ) ;
         }
         nGXsfl_11_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPMQ0( )
      {
         /* Before Start, stand alone formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavFeriado_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFeriado_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFeriado_Enabled), 5, 0)));
         edtavNome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNome_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E14MQ2 */
         E14MQ2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavAno.Name = cmbavAno_Internalname;
            cmbavAno.CurrentValue = cgiGet( cmbavAno_Internalname);
            AV18ano = (short)(NumberUtil.Val( cgiGet( cmbavAno_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ano), 4, 0)));
            /* Read saved values. */
            nRC_GXsfl_11 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_11"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E14MQ2 */
         E14MQ2 ();
         if (returnInSub) return;
      }

      protected void E14MQ2( )
      {
         /* Start Routine */
         AV18ano = (short)(DateTimeUtil.Year( Gx_date));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ano), 4, 0)));
         cmbavAno.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV18ano), 4, 0)), StringUtil.Trim( StringUtil.Str( (decimal)(AV18ano), 4, 0)), 0);
         AV18ano = (short)(DateTimeUtil.Year( Gx_date)+1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ano), 4, 0)));
         cmbavAno.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV18ano), 4, 0)), StringUtil.Trim( StringUtil.Str( (decimal)(AV18ano), 4, 0)), 0);
         AV18ano = (short)(DateTimeUtil.Year( Gx_date)+2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ano), 4, 0)));
         cmbavAno.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV18ano), 4, 0)), StringUtil.Trim( StringUtil.Str( (decimal)(AV18ano), 4, 0)), 0);
         AV18ano = (short)(DateTimeUtil.Year( Gx_date)+3);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ano), 4, 0)));
         cmbavAno.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV18ano), 4, 0)), StringUtil.Trim( StringUtil.Str( (decimal)(AV18ano), 4, 0)), 0);
         AV18ano = (short)(DateTimeUtil.Year( Gx_date));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ano), 4, 0)));
         AV9url = "http://www.calendario.com.br/" + StringUtil.Trim( StringUtil.Str( (decimal)(AV18ano), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9url", AV9url);
      }

      protected void E11MQ2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         bttInsert_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttInsert_Visible), 5, 0)));
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV9url)) )
         {
            if ( StringUtil.StringSearch( AV9url, "http", 1) == 0 )
            {
               AV9url = StringUtil.StringReplace( AV9url, "//", "");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9url", AV9url);
               AV9url = StringUtil.StringReplace( AV9url, "://", "");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9url", AV9url);
               AV9url = StringUtil.StringReplace( AV9url, ":/", "");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9url", AV9url);
               AV9url = "http://" + AV9url;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9url", AV9url);
            }
            AV7httpclient.Execute("GET", AV9url);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9url", AV9url);
            AV5Descricao = AV7httpclient.ToString();
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Descricao", AV5Descricao);
         }
      }

      protected void E15MQ2( )
      {
         /* Grid_Refresh Routine */
         AV11i = StringUtil.StringSearch( AV5Descricao, "<b> ", 1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11i", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11i), 8, 0)));
      }

      private void E16MQ2( )
      {
         /* Grid_Load Routine */
         while ( AV11i > 0 )
         {
            AV11i = (int)(AV11i+3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11i", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11i), 8, 0)));
            while ( String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Substring( AV5Descricao, AV11i, 1))) )
            {
               AV11i = (int)(AV11i+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11i", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11i), 8, 0)));
            }
            AV12str = "";
            while ( ! String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Substring( AV5Descricao, AV11i, 1))) )
            {
               AV12str = AV12str + StringUtil.Substring( AV5Descricao, AV11i, 1);
               AV11i = (int)(AV11i+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11i", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11i), 8, 0)));
            }
            AV10Feriado = context.localUtil.CToD( StringUtil.Trim( AV12str), 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavFeriado_Internalname, context.localUtil.Format(AV10Feriado, "99/99/99"));
            AV15Dia = DateTimeUtil.CDow( AV10Feriado, "por");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDia_Internalname, AV15Dia);
            AV17Fixo = (bool)(Convert.ToBoolean(StringUtil.StringSearch( StringUtil.Substring( AV5Descricao, AV11i, 40), "Feriado Nacional", 1)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavFixo_Internalname, AV17Fixo);
            AV11i = StringUtil.StringSearch( AV5Descricao, "<b>", AV11i);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11i", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11i), 8, 0)));
            AV11i = (int)(StringUtil.StringSearch( AV5Descricao, "<b>", AV11i+3)+3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11i", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11i), 8, 0)));
            AV13Nome = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV13Nome);
            while ( StringUtil.StrCmp(StringUtil.Substring( AV5Descricao, AV11i, 1), "<") != 0 )
            {
               AV13Nome = AV13Nome + StringUtil.Substring( AV5Descricao, AV11i, 1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV13Nome);
               AV11i = (int)(AV11i+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11i", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11i), 8, 0)));
            }
            AV14Selected = (bool)(AV17Fixo||((DateTimeUtil.Dow( AV10Feriado)!=1)&&(DateTimeUtil.Dow( AV10Feriado)!=7)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavSelected_Internalname, AV14Selected);
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 11;
            }
            sendrow_112( ) ;
            if ( isFullAjaxMode( ) && ( nGXsfl_11_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(11, GridRow);
            }
            bttInsert_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttInsert_Visible), 5, 0)));
            AV11i = StringUtil.StringSearch( AV5Descricao, "<b> ", AV11i);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11i", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11i), 8, 0)));
         }
      }

      protected void E12MQ2( )
      {
         /* 'InserirFeriados' Routine */
         /* Start For Each Line */
         nRC_GXsfl_11 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_11"), ",", "."));
         nGXsfl_11_fel_idx = 0;
         while ( nGXsfl_11_fel_idx < nRC_GXsfl_11 )
         {
            nGXsfl_11_fel_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_11_fel_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_11_fel_idx+1));
            sGXsfl_11_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_fel_idx), 4, 0)), 4, "0");
            SubsflControlProps_fel_112( ) ;
            AV14Selected = StringUtil.StrToBool( cgiGet( chkavSelected_Internalname));
            AV15Dia = StringUtil.Upper( cgiGet( edtavDia_Internalname));
            if ( context.localUtil.VCDateTime( cgiGet( edtavFeriado_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Feriado"}), 1, "vFERIADO");
               GX_FocusControl = edtavFeriado_Internalname;
               wbErr = true;
               AV10Feriado = (DateTime)(DateTime.MinValue);
            }
            else
            {
               AV10Feriado = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavFeriado_Internalname), 0));
            }
            AV13Nome = StringUtil.Upper( cgiGet( edtavNome_Internalname));
            AV17Fixo = StringUtil.StrToBool( cgiGet( chkavFixo_Internalname));
            if ( AV14Selected )
            {
               AV16Feriados.Load(AV10Feriado);
               if ( ! AV16Feriados.Success() )
               {
                  AV16Feriados.gxTpr_Feriado_data = AV10Feriado;
                  AV16Feriados.gxTpr_Feriado_nome = AV13Nome;
                  AV16Feriados.gxTpr_Feriado_fixo = AV17Fixo;
                  AV16Feriados.Save();
                  AV16Feriados = new SdtFeriados(context);
               }
            }
            /* End For Each Line */
         }
         if ( nGXsfl_11_fel_idx == 0 )
         {
            nGXsfl_11_idx = 1;
            sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
            SubsflControlProps_112( ) ;
         }
         nGXsfl_11_fel_idx = 1;
         context.CommitDataStores( "WebPanel1");
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E13MQ2( )
      {
         /* Ano_Click Routine */
         AV9url = "http://www.calendario.com.br/" + StringUtil.Trim( StringUtil.Str( (decimal)(AV18ano), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9url", AV9url);
         context.DoAjaxRefresh();
      }

      protected void wb_table1_2_MQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain1_Internalname, tblTablemain1_Internalname, "", "", 0, "", "", 0, 10, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;height:51px")+"\">") ;
            context.WriteHtmlText( "&nbsp;&nbsp; ") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 5,'',false,'" + sGXsfl_11_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavAno, cmbavAno_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV18ano), 4, 0)), 1, cmbavAno_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVANO.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,5);\"", "", true, "HLP_WebPanel1.htm");
            cmbavAno.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV18ano), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAno_Internalname, "Values", (String)(cmbavAno.ToJavascriptSource()));
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 6,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBuscar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(11), 2, 0)+","+"null"+");", "Procurar", bttBuscar_Jsonclick, 5, "Procurar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EREFRESH."+"'", TempTags, "", context.GetButtonType( ), "HLP_WebPanel1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:18px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"11\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "Grid", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"background-color:#000000; color:#00FFFFFF;"+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"background-color:#000000; color:#00FFFFFF;"+"\" "+">") ;
               context.SendWebValue( "Dia") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"background-color:#000000; color:#000000;"+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"background-color:#000000; color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Nome") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"background-color:#000000; color:#00FFFFFF;"+"\" "+">") ;
               context.SendWebValue( "Fixo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "Grid");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Titlebackcolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Titlebackcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorodd", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorodd), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Titleforecolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( AV14Selected));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV15Dia));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(AV10Feriado, "99/99/99"));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavFeriado_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV13Nome));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavNome_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( AV17Fixo));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 11 )
         {
            wbEnd = 0;
            nRC_GXsfl_11 = (short)(nGXsfl_11_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            context.WriteHtmlText( "<p>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttInsert_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(11), 2, 0)+","+"null"+");", "Importar feriados", bttInsert_Jsonclick, 5, "Importar feriados", "", StyleString, ClassString, bttInsert_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'INSERIRFERIADOS\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WebPanel1.htm");
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton1_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(11), 2, 0)+","+"null"+");", "Fechar", bttButton1_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WebPanel1.htm");
            context.WriteHtmlText( "</p>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_MQ2e( true) ;
         }
         else
         {
            wb_table1_2_MQ2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAMQ2( ) ;
         WSMQ2( ) ;
         WEMQ2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282352377");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("webpanel1.js", "?20204282352377");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_112( )
      {
         chkavSelected_Internalname = "vSELECTED_"+sGXsfl_11_idx;
         edtavDia_Internalname = "vDIA_"+sGXsfl_11_idx;
         edtavFeriado_Internalname = "vFERIADO_"+sGXsfl_11_idx;
         edtavNome_Internalname = "vNOME_"+sGXsfl_11_idx;
         chkavFixo_Internalname = "vFIXO_"+sGXsfl_11_idx;
      }

      protected void SubsflControlProps_fel_112( )
      {
         chkavSelected_Internalname = "vSELECTED_"+sGXsfl_11_fel_idx;
         edtavDia_Internalname = "vDIA_"+sGXsfl_11_fel_idx;
         edtavFeriado_Internalname = "vFERIADO_"+sGXsfl_11_fel_idx;
         edtavNome_Internalname = "vNOME_"+sGXsfl_11_fel_idx;
         chkavFixo_Internalname = "vFIXO_"+sGXsfl_11_fel_idx;
      }

      protected void sendrow_112( )
      {
         SubsflControlProps_112( ) ;
         WBMQ0( ) ;
         GridRow = GXWebRow.GetNew(context,GridContainer);
         if ( subGrid_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
         }
         else if ( subGrid_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid_Backstyle = 0;
            subGrid_Backcolor = subGrid_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Uniform";
            }
         }
         else if ( subGrid_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
            subGrid_Backcolor = (int)(0xF5F5F5);
         }
         else if ( subGrid_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( ((int)((nGXsfl_11_idx) % (2))) == 0 )
            {
               subGrid_Backcolor = (int)(0xE5E5E5);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Even";
               }
            }
            else
            {
               subGrid_Backcolor = (int)(0xF5F5F5);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
         }
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+((subGrid_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid_Backcolor)+";")+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_11_idx+"\">") ;
         }
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
         }
         /* Check box */
         TempTags = " " + ((chkavSelected.Enabled!=0)&&(chkavSelected.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 12,'',false,'"+sGXsfl_11_idx+"',11)\"" : " ");
         ClassString = "Attribute";
         StyleString = ((subGrid_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid_Backcolor)+";");
         GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkavSelected_Internalname,StringUtil.BoolToStr( AV14Selected),(String)"",(String)"",(short)-1,(short)1,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",TempTags+((chkavSelected.Enabled!=0)&&(chkavSelected.Visible!=0) ? " onclick=\"gx.fn.checkboxClick(12, this, 'true', 'false');gx.evt.onchange(this);\" " : " ")+((chkavSelected.Enabled!=0)&&(chkavSelected.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,12);\"" : " ")});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavDia_Enabled!=0)&&(edtavDia_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 13,'',false,'"+sGXsfl_11_idx+"',11)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavDia_Internalname,StringUtil.RTrim( AV15Dia),StringUtil.RTrim( context.localUtil.Format( AV15Dia, "@!")),TempTags+((edtavDia_Enabled!=0)&&(edtavDia_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavDia_Enabled!=0)&&(edtavDia_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,13);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavDia_Jsonclick,(short)0,(String)"Attribute",((subGrid_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid_Backcolor)+";"),(String)ROClassString,(String)"",(short)-1,(short)1,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavFeriado_Enabled!=0)&&(edtavFeriado_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 14,'',false,'"+sGXsfl_11_idx+"',11)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavFeriado_Internalname,context.localUtil.Format(AV10Feriado, "99/99/99"),context.localUtil.Format( AV10Feriado, "99/99/99"),TempTags+((edtavFeriado_Enabled!=0)&&(edtavFeriado_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavFeriado_Enabled!=0)&&(edtavFeriado_Visible!=0) ? " onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,14);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavFeriado_Jsonclick,(short)0,(String)"Attribute",((subGrid_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid_Backcolor)+";"),(String)ROClassString,(String)"",(short)-1,(int)edtavFeriado_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"left",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavNome_Enabled!=0)&&(edtavNome_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 15,'',false,'"+sGXsfl_11_idx+"',11)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavNome_Internalname,StringUtil.RTrim( AV13Nome),StringUtil.RTrim( context.localUtil.Format( AV13Nome, "@!")),TempTags+((edtavNome_Enabled!=0)&&(edtavNome_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavNome_Enabled!=0)&&(edtavNome_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,15);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavNome_Jsonclick,(short)0,(String)"Attribute",((subGrid_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid_Backcolor)+";"),(String)ROClassString,(String)"",(short)-1,(int)edtavNome_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
         }
         /* Check box */
         TempTags = " " + ((chkavFixo.Enabled!=0)&&(chkavFixo.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 16,'',false,'"+sGXsfl_11_idx+"',11)\"" : " ");
         ClassString = "Attribute";
         StyleString = ((subGrid_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid_Backcolor)+";");
         GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkavFixo_Internalname,StringUtil.BoolToStr( AV17Fixo),(String)"",(String)"",(short)-1,(short)1,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",TempTags+((chkavFixo.Enabled!=0)&&(chkavFixo.Visible!=0) ? " onclick=\"gx.fn.checkboxClick(16, this, 'true', 'false');gx.evt.onchange(this);\" " : " ")+((chkavFixo.Enabled!=0)&&(chkavFixo.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,16);\"" : " ")});
         GridContainer.AddRow(GridRow);
         nGXsfl_11_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_11_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_11_idx+1));
         sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
         SubsflControlProps_112( ) ;
         /* End function sendrow_112 */
      }

      protected void init_default_properties( )
      {
         cmbavAno_Internalname = "vANO";
         bttBuscar_Internalname = "BUSCAR";
         chkavSelected_Internalname = "vSELECTED";
         edtavDia_Internalname = "vDIA";
         edtavFeriado_Internalname = "vFERIADO";
         edtavNome_Internalname = "vNOME";
         chkavFixo_Internalname = "vFIXO";
         bttInsert_Internalname = "INSERT";
         bttButton1_Internalname = "BUTTON1";
         tblTablemain1_Internalname = "TABLEMAIN1";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         chkavFixo.Visible = -1;
         chkavFixo.Enabled = 1;
         edtavNome_Jsonclick = "";
         edtavNome_Visible = -1;
         edtavFeriado_Jsonclick = "";
         edtavFeriado_Visible = -1;
         edtavDia_Jsonclick = "";
         edtavDia_Visible = -1;
         edtavDia_Enabled = 1;
         chkavSelected.Visible = -1;
         chkavSelected.Enabled = 1;
         subGrid_Backcolor = (int)(0x0);
         bttInsert_Visible = 1;
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavNome_Enabled = 1;
         edtavFeriado_Enabled = 1;
         subGrid_Class = "Grid";
         cmbavAno_Jsonclick = "";
         subGrid_Titleforecolor = (int)(0x00FFFFFF);
         subGrid_Backcolorodd = (int)(0xF5F5F5);
         subGrid_Titlebackcolor = (int)(0x000000);
         subGrid_Backcolorstyle = 3;
         chkavFixo.Caption = "";
         chkavSelected.Caption = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Web Panel1";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'AV5Descricao',fld:'vDESCRICAO',pic:'',nv:''},{av:'AV11i',fld:'vI',pic:'ZZZZZZZ9',nv:0},{av:'AV9url',fld:'vURL',pic:'',nv:''}],oparms:[{ctrl:'INSERT',prop:'Visible'},{av:'AV9url',fld:'vURL',pic:'',nv:''},{av:'AV5Descricao',fld:'vDESCRICAO',pic:'',nv:''}]}");
         setEventMetadata("GRID.REFRESH","{handler:'E15MQ2',iparms:[{av:'AV5Descricao',fld:'vDESCRICAO',pic:'',nv:''}],oparms:[{av:'AV11i',fld:'vI',pic:'ZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRID.LOAD","{handler:'E16MQ2',iparms:[{av:'AV11i',fld:'vI',pic:'ZZZZZZZ9',nv:0},{av:'AV5Descricao',fld:'vDESCRICAO',pic:'',nv:''}],oparms:[{av:'AV11i',fld:'vI',pic:'ZZZZZZZ9',nv:0},{av:'AV10Feriado',fld:'vFERIADO',pic:'',nv:''},{av:'AV15Dia',fld:'vDIA',pic:'@!',nv:''},{av:'AV17Fixo',fld:'vFIXO',pic:'',nv:false},{av:'AV13Nome',fld:'vNOME',pic:'@!',nv:''},{av:'AV14Selected',fld:'vSELECTED',pic:'',nv:false},{ctrl:'INSERT',prop:'Visible'}]}");
         setEventMetadata("'INSERIRFERIADOS'","{handler:'E12MQ2',iparms:[{av:'AV14Selected',fld:'vSELECTED',grid:11,pic:'',nv:false},{av:'AV10Feriado',fld:'vFERIADO',grid:11,pic:'',nv:''},{av:'AV13Nome',fld:'vNOME',grid:11,pic:'@!',nv:''},{av:'AV17Fixo',fld:'vFIXO',grid:11,pic:'',nv:false}],oparms:[]}");
         setEventMetadata("VANO.CLICK","{handler:'E13MQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'AV9url',fld:'vURL',pic:'',nv:''},{av:'AV5Descricao',fld:'vDESCRICAO',pic:'',nv:''},{av:'AV11i',fld:'vI',pic:'ZZZZZZZ9',nv:0},{av:'AV18ano',fld:'vANO',pic:'ZZZ9',nv:0}],oparms:[{av:'AV9url',fld:'vURL',pic:'',nv:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV9url = "";
         AV5Descricao = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV15Dia = "";
         AV10Feriado = DateTime.MinValue;
         AV13Nome = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         Gx_date = DateTime.MinValue;
         AV7httpclient = new GxHttpClient( context);
         AV12str = "";
         GridRow = new GXWebRow();
         AV16Feriados = new SdtFeriados(context);
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBuscar_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         bttInsert_Jsonclick = "";
         bttButton1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.webpanel1__default(),
            new Object[][] {
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavFeriado_Enabled = 0;
         edtavNome_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_11 ;
      private short nGXsfl_11_idx=1 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV18ano ;
      private short nGXsfl_11_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short GRID_nEOF ;
      private short nGXsfl_11_fel_idx=1 ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV11i ;
      private int subGrid_Islastpage ;
      private int edtavFeriado_Enabled ;
      private int edtavNome_Enabled ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Backcolorodd ;
      private int subGrid_Titleforecolor ;
      private int bttInsert_Visible ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavDia_Enabled ;
      private int edtavDia_Visible ;
      private int edtavFeriado_Visible ;
      private int edtavNome_Visible ;
      private long GRID_nCurrentRecord ;
      private long GRID_nFirstRecordOnPage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_11_idx="0001" ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkavSelected_Internalname ;
      private String AV15Dia ;
      private String edtavDia_Internalname ;
      private String edtavFeriado_Internalname ;
      private String AV13Nome ;
      private String edtavNome_Internalname ;
      private String chkavFixo_Internalname ;
      private String GXCCtl ;
      private String cmbavAno_Internalname ;
      private String bttInsert_Internalname ;
      private String AV12str ;
      private String sGXsfl_11_fel_idx="0001" ;
      private String sStyleString ;
      private String tblTablemain1_Internalname ;
      private String TempTags ;
      private String cmbavAno_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String bttBuscar_Internalname ;
      private String bttBuscar_Jsonclick ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String bttInsert_Jsonclick ;
      private String bttButton1_Internalname ;
      private String bttButton1_Jsonclick ;
      private String ROClassString ;
      private String edtavDia_Jsonclick ;
      private String edtavFeriado_Jsonclick ;
      private String edtavNome_Jsonclick ;
      private DateTime AV10Feriado ;
      private DateTime Gx_date ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV14Selected ;
      private bool AV17Fixo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String AV5Descricao ;
      private String AV9url ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavAno ;
      private GXCheckbox chkavSelected ;
      private GXCheckbox chkavFixo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IDataStoreProvider pr_default ;
      private GxHttpClient AV7httpclient ;
      private GXWebForm Form ;
      private SdtFeriados AV16Feriados ;
   }

   public class webpanel1__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
