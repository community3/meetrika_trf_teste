/*
               File: WWGeral_Cargo
        Description:  Cargos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:37:45.3
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwgeral_cargo : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwgeral_cargo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwgeral_cargo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         dynavGrupocargo_codigo1 = new GXCombobox();
         dynavCargo_uocod1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         dynavGrupocargo_codigo2 = new GXCombobox();
         dynavCargo_uocod2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         dynavGrupocargo_codigo3 = new GXCombobox();
         dynavCargo_uocod3 = new GXCombobox();
         chkCargo_Ativo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vGRUPOCARGO_CODIGO1") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvGRUPOCARGO_CODIGO1CW2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCARGO_UOCOD1") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCARGO_UOCOD1CW2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vGRUPOCARGO_CODIGO2") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvGRUPOCARGO_CODIGO2CW2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCARGO_UOCOD2") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCARGO_UOCOD2CW2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vGRUPOCARGO_CODIGO3") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvGRUPOCARGO_CODIGO3CW2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCARGO_UOCOD3") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCARGO_UOCOD3CW2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_79 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_79_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_79_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16Cargo_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Cargo_Nome1", AV16Cargo_Nome1);
               AV31GrupoCargo_Codigo1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31GrupoCargo_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31GrupoCargo_Codigo1), 6, 0)));
               AV34Cargo_UOCod1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Cargo_UOCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Cargo_UOCod1), 6, 0)));
               AV18DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
               AV19Cargo_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Cargo_Nome2", AV19Cargo_Nome2);
               AV32GrupoCargo_Codigo2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32GrupoCargo_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32GrupoCargo_Codigo2), 6, 0)));
               AV35Cargo_UOCod2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Cargo_UOCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35Cargo_UOCod2), 6, 0)));
               AV21DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
               AV22Cargo_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Cargo_Nome3", AV22Cargo_Nome3);
               AV33GrupoCargo_Codigo3 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33GrupoCargo_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33GrupoCargo_Codigo3), 6, 0)));
               AV36Cargo_UOCod3 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Cargo_UOCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Cargo_UOCod3), 6, 0)));
               AV17DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
               AV20DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
               AV40TFCargo_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFCargo_Nome", AV40TFCargo_Nome);
               AV41TFCargo_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFCargo_Nome_Sel", AV41TFCargo_Nome_Sel);
               AV44TFGrupoCargo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFGrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFGrupoCargo_Codigo), 6, 0)));
               AV45TFGrupoCargo_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFGrupoCargo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFGrupoCargo_Codigo_To), 6, 0)));
               AV48TFCargo_UONom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFCargo_UONom", AV48TFCargo_UONom);
               AV49TFCargo_UONom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFCargo_UONom_Sel", AV49TFCargo_UONom_Sel);
               AV52TFCargo_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFCargo_Ativo_Sel", StringUtil.Str( (decimal)(AV52TFCargo_Ativo_Sel), 1, 0));
               AV42ddo_Cargo_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ddo_Cargo_NomeTitleControlIdToReplace", AV42ddo_Cargo_NomeTitleControlIdToReplace);
               AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace", AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace);
               AV50ddo_Cargo_UONomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_Cargo_UONomTitleControlIdToReplace", AV50ddo_Cargo_UONomTitleControlIdToReplace);
               AV53ddo_Cargo_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_Cargo_AtivoTitleControlIdToReplace", AV53ddo_Cargo_AtivoTitleControlIdToReplace);
               AV83Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV24DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
               AV23DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
               A617Cargo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A1002Cargo_UOCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n1002Cargo_UOCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1002Cargo_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1002Cargo_UOCod), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Cargo_Nome1, AV31GrupoCargo_Codigo1, AV34Cargo_UOCod1, AV18DynamicFiltersSelector2, AV19Cargo_Nome2, AV32GrupoCargo_Codigo2, AV35Cargo_UOCod2, AV21DynamicFiltersSelector3, AV22Cargo_Nome3, AV33GrupoCargo_Codigo3, AV36Cargo_UOCod3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV40TFCargo_Nome, AV41TFCargo_Nome_Sel, AV44TFGrupoCargo_Codigo, AV45TFGrupoCargo_Codigo_To, AV48TFCargo_UONom, AV49TFCargo_UONom_Sel, AV52TFCargo_Ativo_Sel, AV42ddo_Cargo_NomeTitleControlIdToReplace, AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace, AV50ddo_Cargo_UONomTitleControlIdToReplace, AV53ddo_Cargo_AtivoTitleControlIdToReplace, AV83Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A617Cargo_Codigo, A1002Cargo_UOCod) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PACW2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTCW2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299374531");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwgeral_cargo.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vCARGO_NOME1", AV16Cargo_Nome1);
         GxWebStd.gx_hidden_field( context, "GXH_vGRUPOCARGO_CODIGO1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31GrupoCargo_Codigo1), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCARGO_UOCOD1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34Cargo_UOCod1), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV18DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vCARGO_NOME2", AV19Cargo_Nome2);
         GxWebStd.gx_hidden_field( context, "GXH_vGRUPOCARGO_CODIGO2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32GrupoCargo_Codigo2), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCARGO_UOCOD2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35Cargo_UOCod2), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV21DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vCARGO_NOME3", AV22Cargo_Nome3);
         GxWebStd.gx_hidden_field( context, "GXH_vGRUPOCARGO_CODIGO3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33GrupoCargo_Codigo3), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCARGO_UOCOD3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36Cargo_UOCod3), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV17DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV20DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCARGO_NOME", AV40TFCargo_Nome);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCARGO_NOME_SEL", AV41TFCargo_Nome_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFGRUPOCARGO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44TFGrupoCargo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFGRUPOCARGO_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45TFGrupoCargo_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCARGO_UONOM", StringUtil.RTrim( AV48TFCargo_UONom));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCARGO_UONOM_SEL", StringUtil.RTrim( AV49TFCargo_UONom_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCARGO_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV52TFCargo_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_79", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_79), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV57GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV54DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV54DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCARGO_NOMETITLEFILTERDATA", AV39Cargo_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCARGO_NOMETITLEFILTERDATA", AV39Cargo_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRUPOCARGO_CODIGOTITLEFILTERDATA", AV43GrupoCargo_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRUPOCARGO_CODIGOTITLEFILTERDATA", AV43GrupoCargo_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCARGO_UONOMTITLEFILTERDATA", AV47Cargo_UONomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCARGO_UONOMTITLEFILTERDATA", AV47Cargo_UONomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCARGO_ATIVOTITLEFILTERDATA", AV51Cargo_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCARGO_ATIVOTITLEFILTERDATA", AV51Cargo_AtivoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV83Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV24DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV23DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "CARGO_UOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1002Cargo_UOCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_NOME_Caption", StringUtil.RTrim( Ddo_cargo_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_NOME_Tooltip", StringUtil.RTrim( Ddo_cargo_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_NOME_Cls", StringUtil.RTrim( Ddo_cargo_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_cargo_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_cargo_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_cargo_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_cargo_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_cargo_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_cargo_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_cargo_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_cargo_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_NOME_Filtertype", StringUtil.RTrim( Ddo_cargo_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_cargo_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_cargo_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_NOME_Datalisttype", StringUtil.RTrim( Ddo_cargo_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_NOME_Datalistproc", StringUtil.RTrim( Ddo_cargo_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_cargo_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_NOME_Sortasc", StringUtil.RTrim( Ddo_cargo_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_NOME_Sortdsc", StringUtil.RTrim( Ddo_cargo_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_NOME_Loadingdata", StringUtil.RTrim( Ddo_cargo_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_cargo_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_cargo_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_cargo_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOCARGO_CODIGO_Caption", StringUtil.RTrim( Ddo_grupocargo_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOCARGO_CODIGO_Tooltip", StringUtil.RTrim( Ddo_grupocargo_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOCARGO_CODIGO_Cls", StringUtil.RTrim( Ddo_grupocargo_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOCARGO_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_grupocargo_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOCARGO_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_grupocargo_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOCARGO_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_grupocargo_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOCARGO_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_grupocargo_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOCARGO_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_grupocargo_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOCARGO_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_grupocargo_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOCARGO_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_grupocargo_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOCARGO_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_grupocargo_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOCARGO_CODIGO_Filtertype", StringUtil.RTrim( Ddo_grupocargo_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOCARGO_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_grupocargo_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOCARGO_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_grupocargo_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOCARGO_CODIGO_Sortasc", StringUtil.RTrim( Ddo_grupocargo_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOCARGO_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_grupocargo_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOCARGO_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_grupocargo_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOCARGO_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_grupocargo_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOCARGO_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_grupocargo_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOCARGO_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_grupocargo_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_UONOM_Caption", StringUtil.RTrim( Ddo_cargo_uonom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_UONOM_Tooltip", StringUtil.RTrim( Ddo_cargo_uonom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_UONOM_Cls", StringUtil.RTrim( Ddo_cargo_uonom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_UONOM_Filteredtext_set", StringUtil.RTrim( Ddo_cargo_uonom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_UONOM_Selectedvalue_set", StringUtil.RTrim( Ddo_cargo_uonom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_UONOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_cargo_uonom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_UONOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_cargo_uonom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_UONOM_Includesortasc", StringUtil.BoolToStr( Ddo_cargo_uonom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_UONOM_Includesortdsc", StringUtil.BoolToStr( Ddo_cargo_uonom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_UONOM_Sortedstatus", StringUtil.RTrim( Ddo_cargo_uonom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_UONOM_Includefilter", StringUtil.BoolToStr( Ddo_cargo_uonom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_UONOM_Filtertype", StringUtil.RTrim( Ddo_cargo_uonom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_UONOM_Filterisrange", StringUtil.BoolToStr( Ddo_cargo_uonom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_UONOM_Includedatalist", StringUtil.BoolToStr( Ddo_cargo_uonom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_UONOM_Datalisttype", StringUtil.RTrim( Ddo_cargo_uonom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_UONOM_Datalistproc", StringUtil.RTrim( Ddo_cargo_uonom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_UONOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_cargo_uonom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_UONOM_Sortasc", StringUtil.RTrim( Ddo_cargo_uonom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_UONOM_Sortdsc", StringUtil.RTrim( Ddo_cargo_uonom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_UONOM_Loadingdata", StringUtil.RTrim( Ddo_cargo_uonom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_UONOM_Cleanfilter", StringUtil.RTrim( Ddo_cargo_uonom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_UONOM_Noresultsfound", StringUtil.RTrim( Ddo_cargo_uonom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_UONOM_Searchbuttontext", StringUtil.RTrim( Ddo_cargo_uonom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_ATIVO_Caption", StringUtil.RTrim( Ddo_cargo_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_ATIVO_Tooltip", StringUtil.RTrim( Ddo_cargo_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_ATIVO_Cls", StringUtil.RTrim( Ddo_cargo_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_cargo_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_cargo_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_cargo_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_cargo_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_cargo_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_cargo_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_cargo_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_cargo_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_cargo_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_cargo_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_ATIVO_Sortasc", StringUtil.RTrim( Ddo_cargo_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_cargo_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_cargo_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_cargo_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_cargo_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_cargo_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_cargo_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOCARGO_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_grupocargo_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOCARGO_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_grupocargo_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOCARGO_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_grupocargo_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_UONOM_Activeeventkey", StringUtil.RTrim( Ddo_cargo_uonom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_UONOM_Filteredtext_get", StringUtil.RTrim( Ddo_cargo_uonom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_UONOM_Selectedvalue_get", StringUtil.RTrim( Ddo_cargo_uonom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_cargo_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CARGO_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_cargo_ativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WECW2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTCW2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwgeral_cargo.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWGeral_Cargo" ;
      }

      public override String GetPgmdesc( )
      {
         return " Cargos" ;
      }

      protected void WBCW0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_CW2( true) ;
         }
         else
         {
            wb_table1_2_CW2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_CW2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'" + sGXsfl_79_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV17DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(91, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,91);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_79_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(92, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,92);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcargo_nome_Internalname, AV40TFCargo_Nome, StringUtil.RTrim( context.localUtil.Format( AV40TFCargo_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,93);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcargo_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfcargo_nome_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGeral_Cargo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcargo_nome_sel_Internalname, AV41TFCargo_Nome_Sel, StringUtil.RTrim( context.localUtil.Format( AV41TFCargo_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,94);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcargo_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcargo_nome_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGeral_Cargo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfgrupocargo_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44TFGrupoCargo_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV44TFGrupoCargo_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,95);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfgrupocargo_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfgrupocargo_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWGeral_Cargo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfgrupocargo_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45TFGrupoCargo_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV45TFGrupoCargo_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,96);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfgrupocargo_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfgrupocargo_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWGeral_Cargo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcargo_uonom_Internalname, StringUtil.RTrim( AV48TFCargo_UONom), StringUtil.RTrim( context.localUtil.Format( AV48TFCargo_UONom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcargo_uonom_Jsonclick, 0, "Attribute", "", "", "", edtavTfcargo_uonom_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGeral_Cargo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcargo_uonom_sel_Internalname, StringUtil.RTrim( AV49TFCargo_UONom_Sel), StringUtil.RTrim( context.localUtil.Format( AV49TFCargo_UONom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,98);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcargo_uonom_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcargo_uonom_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGeral_Cargo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcargo_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV52TFCargo_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV52TFCargo_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcargo_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcargo_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWGeral_Cargo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CARGO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_79_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_cargo_nometitlecontrolidtoreplace_Internalname, AV42ddo_Cargo_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,101);\"", 0, edtavDdo_cargo_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWGeral_Cargo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_GRUPOCARGO_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_79_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_grupocargo_codigotitlecontrolidtoreplace_Internalname, AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,103);\"", 0, edtavDdo_grupocargo_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWGeral_Cargo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CARGO_UONOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_79_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_cargo_uonomtitlecontrolidtoreplace_Internalname, AV50ddo_Cargo_UONomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,105);\"", 0, edtavDdo_cargo_uonomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWGeral_Cargo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CARGO_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_79_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_cargo_ativotitlecontrolidtoreplace_Internalname, AV53ddo_Cargo_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,107);\"", 0, edtavDdo_cargo_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWGeral_Cargo.htm");
         }
         wbLoad = true;
      }

      protected void STARTCW2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Cargos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPCW0( ) ;
      }

      protected void WSCW2( )
      {
         STARTCW2( ) ;
         EVTCW2( ) ;
      }

      protected void EVTCW2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11CW2 */
                              E11CW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CARGO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12CW2 */
                              E12CW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_GRUPOCARGO_CODIGO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13CW2 */
                              E13CW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CARGO_UONOM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14CW2 */
                              E14CW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CARGO_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15CW2 */
                              E15CW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16CW2 */
                              E16CW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17CW2 */
                              E17CW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18CW2 */
                              E18CW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19CW2 */
                              E19CW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20CW2 */
                              E20CW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21CW2 */
                              E21CW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22CW2 */
                              E22CW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23CW2 */
                              E23CW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24CW2 */
                              E24CW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25CW2 */
                              E25CW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E26CW2 */
                              E26CW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_79_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_79_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_79_idx), 4, 0)), 4, "0");
                              SubsflControlProps_792( ) ;
                              AV25Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV25Update)) ? AV81Update_GXI : context.convertURL( context.PathToRelativeUrl( AV25Update))));
                              AV26Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV26Delete)) ? AV82Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV26Delete))));
                              A617Cargo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtCargo_Codigo_Internalname), ",", "."));
                              A618Cargo_Nome = StringUtil.Upper( cgiGet( edtCargo_Nome_Internalname));
                              A615GrupoCargo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtGrupoCargo_Codigo_Internalname), ",", "."));
                              A1003Cargo_UONom = StringUtil.Upper( cgiGet( edtCargo_UONom_Internalname));
                              n1003Cargo_UONom = false;
                              A628Cargo_Ativo = StringUtil.StrToBool( cgiGet( chkCargo_Ativo_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E27CW2 */
                                    E27CW2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E28CW2 */
                                    E28CW2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E29CW2 */
                                    E29CW2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Cargo_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCARGO_NOME1"), AV16Cargo_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Grupocargo_codigo1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vGRUPOCARGO_CODIGO1"), ",", ".") != Convert.ToDecimal( AV31GrupoCargo_Codigo1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Cargo_uocod1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCARGO_UOCOD1"), ",", ".") != Convert.ToDecimal( AV34Cargo_UOCod1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV18DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Cargo_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCARGO_NOME2"), AV19Cargo_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Grupocargo_codigo2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vGRUPOCARGO_CODIGO2"), ",", ".") != Convert.ToDecimal( AV32GrupoCargo_Codigo2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Cargo_uocod2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCARGO_UOCOD2"), ",", ".") != Convert.ToDecimal( AV35Cargo_UOCod2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV21DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Cargo_nome3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCARGO_NOME3"), AV22Cargo_Nome3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Grupocargo_codigo3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vGRUPOCARGO_CODIGO3"), ",", ".") != Convert.ToDecimal( AV33GrupoCargo_Codigo3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Cargo_uocod3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCARGO_UOCOD3"), ",", ".") != Convert.ToDecimal( AV36Cargo_UOCod3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV17DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV20DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcargo_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCARGO_NOME"), AV40TFCargo_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcargo_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCARGO_NOME_SEL"), AV41TFCargo_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfgrupocargo_codigo Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFGRUPOCARGO_CODIGO"), ",", ".") != Convert.ToDecimal( AV44TFGrupoCargo_Codigo )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfgrupocargo_codigo_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFGRUPOCARGO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV45TFGrupoCargo_Codigo_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcargo_uonom Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCARGO_UONOM"), AV48TFCargo_UONom) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcargo_uonom_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCARGO_UONOM_SEL"), AV49TFCargo_UONom_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcargo_ativo_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCARGO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV52TFCargo_Ativo_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WECW2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PACW2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CARGO_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("GRUPOCARGO_CODIGO", "Grupo", 0);
            cmbavDynamicfiltersselector1.addItem("CARGO_UOCOD", "Organizacional", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            dynavGrupocargo_codigo1.Name = "vGRUPOCARGO_CODIGO1";
            dynavGrupocargo_codigo1.WebTags = "";
            dynavCargo_uocod1.Name = "vCARGO_UOCOD1";
            dynavCargo_uocod1.WebTags = "";
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CARGO_NOME", "Nome", 0);
            cmbavDynamicfiltersselector2.addItem("GRUPOCARGO_CODIGO", "Grupo", 0);
            cmbavDynamicfiltersselector2.addItem("CARGO_UOCOD", "Organizacional", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV18DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV18DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
            }
            dynavGrupocargo_codigo2.Name = "vGRUPOCARGO_CODIGO2";
            dynavGrupocargo_codigo2.WebTags = "";
            dynavCargo_uocod2.Name = "vCARGO_UOCOD2";
            dynavCargo_uocod2.WebTags = "";
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CARGO_NOME", "Nome", 0);
            cmbavDynamicfiltersselector3.addItem("GRUPOCARGO_CODIGO", "Grupo", 0);
            cmbavDynamicfiltersselector3.addItem("CARGO_UOCOD", "Organizacional", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV21DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
            }
            dynavGrupocargo_codigo3.Name = "vGRUPOCARGO_CODIGO3";
            dynavGrupocargo_codigo3.WebTags = "";
            dynavCargo_uocod3.Name = "vCARGO_UOCOD3";
            dynavCargo_uocod3.WebTags = "";
            GXCCtl = "CARGO_ATIVO_" + sGXsfl_79_idx;
            chkCargo_Ativo.Name = GXCCtl;
            chkCargo_Ativo.WebTags = "";
            chkCargo_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkCargo_Ativo_Internalname, "TitleCaption", chkCargo_Ativo.Caption);
            chkCargo_Ativo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvGRUPOCARGO_CODIGO1CW2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvGRUPOCARGO_CODIGO1_dataCW2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvGRUPOCARGO_CODIGO1_htmlCW2( )
      {
         int gxdynajaxvalue ;
         GXDLVvGRUPOCARGO_CODIGO1_dataCW2( ) ;
         gxdynajaxindex = 1;
         dynavGrupocargo_codigo1.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavGrupocargo_codigo1.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavGrupocargo_codigo1.ItemCount > 0 )
         {
            AV31GrupoCargo_Codigo1 = (int)(NumberUtil.Val( dynavGrupocargo_codigo1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV31GrupoCargo_Codigo1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31GrupoCargo_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31GrupoCargo_Codigo1), 6, 0)));
         }
      }

      protected void GXDLVvGRUPOCARGO_CODIGO1_dataCW2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00CW2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00CW2_A615GrupoCargo_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00CW2_A616GrupoCargo_Nome[0]);
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvCARGO_UOCOD1CW2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCARGO_UOCOD1_dataCW2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCARGO_UOCOD1_htmlCW2( )
      {
         int gxdynajaxvalue ;
         GXDLVvCARGO_UOCOD1_dataCW2( ) ;
         gxdynajaxindex = 1;
         dynavCargo_uocod1.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavCargo_uocod1.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavCargo_uocod1.ItemCount > 0 )
         {
            AV34Cargo_UOCod1 = (int)(NumberUtil.Val( dynavCargo_uocod1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV34Cargo_UOCod1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Cargo_UOCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Cargo_UOCod1), 6, 0)));
         }
      }

      protected void GXDLVvCARGO_UOCOD1_dataCW2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H00CW3 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00CW3_A611UnidadeOrganizacional_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00CW3_A612UnidadeOrganizacional_Nome[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXDLVvGRUPOCARGO_CODIGO2CW2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvGRUPOCARGO_CODIGO2_dataCW2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvGRUPOCARGO_CODIGO2_htmlCW2( )
      {
         int gxdynajaxvalue ;
         GXDLVvGRUPOCARGO_CODIGO2_dataCW2( ) ;
         gxdynajaxindex = 1;
         dynavGrupocargo_codigo2.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavGrupocargo_codigo2.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavGrupocargo_codigo2.ItemCount > 0 )
         {
            AV32GrupoCargo_Codigo2 = (int)(NumberUtil.Val( dynavGrupocargo_codigo2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV32GrupoCargo_Codigo2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32GrupoCargo_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32GrupoCargo_Codigo2), 6, 0)));
         }
      }

      protected void GXDLVvGRUPOCARGO_CODIGO2_dataCW2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00CW4 */
         pr_default.execute(2);
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00CW4_A615GrupoCargo_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00CW4_A616GrupoCargo_Nome[0]);
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void GXDLVvCARGO_UOCOD2CW2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCARGO_UOCOD2_dataCW2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCARGO_UOCOD2_htmlCW2( )
      {
         int gxdynajaxvalue ;
         GXDLVvCARGO_UOCOD2_dataCW2( ) ;
         gxdynajaxindex = 1;
         dynavCargo_uocod2.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavCargo_uocod2.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavCargo_uocod2.ItemCount > 0 )
         {
            AV35Cargo_UOCod2 = (int)(NumberUtil.Val( dynavCargo_uocod2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV35Cargo_UOCod2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Cargo_UOCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35Cargo_UOCod2), 6, 0)));
         }
      }

      protected void GXDLVvCARGO_UOCOD2_dataCW2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H00CW5 */
         pr_default.execute(3);
         while ( (pr_default.getStatus(3) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00CW5_A611UnidadeOrganizacional_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00CW5_A612UnidadeOrganizacional_Nome[0]));
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void GXDLVvGRUPOCARGO_CODIGO3CW2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvGRUPOCARGO_CODIGO3_dataCW2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvGRUPOCARGO_CODIGO3_htmlCW2( )
      {
         int gxdynajaxvalue ;
         GXDLVvGRUPOCARGO_CODIGO3_dataCW2( ) ;
         gxdynajaxindex = 1;
         dynavGrupocargo_codigo3.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavGrupocargo_codigo3.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavGrupocargo_codigo3.ItemCount > 0 )
         {
            AV33GrupoCargo_Codigo3 = (int)(NumberUtil.Val( dynavGrupocargo_codigo3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV33GrupoCargo_Codigo3), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33GrupoCargo_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33GrupoCargo_Codigo3), 6, 0)));
         }
      }

      protected void GXDLVvGRUPOCARGO_CODIGO3_dataCW2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00CW6 */
         pr_default.execute(4);
         while ( (pr_default.getStatus(4) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00CW6_A615GrupoCargo_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00CW6_A616GrupoCargo_Nome[0]);
            pr_default.readNext(4);
         }
         pr_default.close(4);
      }

      protected void GXDLVvCARGO_UOCOD3CW2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCARGO_UOCOD3_dataCW2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCARGO_UOCOD3_htmlCW2( )
      {
         int gxdynajaxvalue ;
         GXDLVvCARGO_UOCOD3_dataCW2( ) ;
         gxdynajaxindex = 1;
         dynavCargo_uocod3.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavCargo_uocod3.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavCargo_uocod3.ItemCount > 0 )
         {
            AV36Cargo_UOCod3 = (int)(NumberUtil.Val( dynavCargo_uocod3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV36Cargo_UOCod3), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Cargo_UOCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Cargo_UOCod3), 6, 0)));
         }
      }

      protected void GXDLVvCARGO_UOCOD3_dataCW2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H00CW7 */
         pr_default.execute(5);
         while ( (pr_default.getStatus(5) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00CW7_A611UnidadeOrganizacional_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00CW7_A612UnidadeOrganizacional_Nome[0]));
            pr_default.readNext(5);
         }
         pr_default.close(5);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_792( ) ;
         while ( nGXsfl_79_idx <= nRC_GXsfl_79 )
         {
            sendrow_792( ) ;
            nGXsfl_79_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_79_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_79_idx+1));
            sGXsfl_79_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_79_idx), 4, 0)), 4, "0");
            SubsflControlProps_792( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV16Cargo_Nome1 ,
                                       int AV31GrupoCargo_Codigo1 ,
                                       int AV34Cargo_UOCod1 ,
                                       String AV18DynamicFiltersSelector2 ,
                                       String AV19Cargo_Nome2 ,
                                       int AV32GrupoCargo_Codigo2 ,
                                       int AV35Cargo_UOCod2 ,
                                       String AV21DynamicFiltersSelector3 ,
                                       String AV22Cargo_Nome3 ,
                                       int AV33GrupoCargo_Codigo3 ,
                                       int AV36Cargo_UOCod3 ,
                                       bool AV17DynamicFiltersEnabled2 ,
                                       bool AV20DynamicFiltersEnabled3 ,
                                       String AV40TFCargo_Nome ,
                                       String AV41TFCargo_Nome_Sel ,
                                       int AV44TFGrupoCargo_Codigo ,
                                       int AV45TFGrupoCargo_Codigo_To ,
                                       String AV48TFCargo_UONom ,
                                       String AV49TFCargo_UONom_Sel ,
                                       short AV52TFCargo_Ativo_Sel ,
                                       String AV42ddo_Cargo_NomeTitleControlIdToReplace ,
                                       String AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace ,
                                       String AV50ddo_Cargo_UONomTitleControlIdToReplace ,
                                       String AV53ddo_Cargo_AtivoTitleControlIdToReplace ,
                                       String AV83Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV24DynamicFiltersIgnoreFirst ,
                                       bool AV23DynamicFiltersRemoving ,
                                       int A617Cargo_Codigo ,
                                       int A1002Cargo_UOCod )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFCW2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CARGO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A617Cargo_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CARGO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A617Cargo_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CARGO_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A618Cargo_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "CARGO_NOME", A618Cargo_Nome);
         GxWebStd.gx_hidden_field( context, "gxhash_GRUPOCARGO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A615GrupoCargo_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "GRUPOCARGO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A615GrupoCargo_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CARGO_ATIVO", GetSecureSignedToken( "", A628Cargo_Ativo));
         GxWebStd.gx_hidden_field( context, "CARGO_ATIVO", StringUtil.BoolToStr( A628Cargo_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( dynavGrupocargo_codigo1.ItemCount > 0 )
         {
            AV31GrupoCargo_Codigo1 = (int)(NumberUtil.Val( dynavGrupocargo_codigo1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV31GrupoCargo_Codigo1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31GrupoCargo_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31GrupoCargo_Codigo1), 6, 0)));
         }
         if ( dynavCargo_uocod1.ItemCount > 0 )
         {
            AV34Cargo_UOCod1 = (int)(NumberUtil.Val( dynavCargo_uocod1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV34Cargo_UOCod1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Cargo_UOCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Cargo_UOCod1), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV18DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV18DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         }
         if ( dynavGrupocargo_codigo2.ItemCount > 0 )
         {
            AV32GrupoCargo_Codigo2 = (int)(NumberUtil.Val( dynavGrupocargo_codigo2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV32GrupoCargo_Codigo2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32GrupoCargo_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32GrupoCargo_Codigo2), 6, 0)));
         }
         if ( dynavCargo_uocod2.ItemCount > 0 )
         {
            AV35Cargo_UOCod2 = (int)(NumberUtil.Val( dynavCargo_uocod2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV35Cargo_UOCod2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Cargo_UOCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35Cargo_UOCod2), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV21DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         }
         if ( dynavGrupocargo_codigo3.ItemCount > 0 )
         {
            AV33GrupoCargo_Codigo3 = (int)(NumberUtil.Val( dynavGrupocargo_codigo3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV33GrupoCargo_Codigo3), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33GrupoCargo_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33GrupoCargo_Codigo3), 6, 0)));
         }
         if ( dynavCargo_uocod3.ItemCount > 0 )
         {
            AV36Cargo_UOCod3 = (int)(NumberUtil.Val( dynavCargo_uocod3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV36Cargo_UOCod3), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Cargo_UOCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Cargo_UOCod3), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFCW2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV83Pgmname = "WWGeral_Cargo";
         context.Gx_err = 0;
      }

      protected void RFCW2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 79;
         /* Execute user event: E28CW2 */
         E28CW2 ();
         nGXsfl_79_idx = 1;
         sGXsfl_79_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_79_idx), 4, 0)), 4, "0");
         SubsflControlProps_792( ) ;
         nGXsfl_79_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_792( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(6, new Object[]{ new Object[]{
                                                 AV60WWGeral_CargoDS_1_Dynamicfiltersselector1 ,
                                                 AV61WWGeral_CargoDS_2_Cargo_nome1 ,
                                                 AV62WWGeral_CargoDS_3_Grupocargo_codigo1 ,
                                                 AV63WWGeral_CargoDS_4_Cargo_uocod1 ,
                                                 AV64WWGeral_CargoDS_5_Dynamicfiltersenabled2 ,
                                                 AV65WWGeral_CargoDS_6_Dynamicfiltersselector2 ,
                                                 AV66WWGeral_CargoDS_7_Cargo_nome2 ,
                                                 AV67WWGeral_CargoDS_8_Grupocargo_codigo2 ,
                                                 AV68WWGeral_CargoDS_9_Cargo_uocod2 ,
                                                 AV69WWGeral_CargoDS_10_Dynamicfiltersenabled3 ,
                                                 AV70WWGeral_CargoDS_11_Dynamicfiltersselector3 ,
                                                 AV71WWGeral_CargoDS_12_Cargo_nome3 ,
                                                 AV72WWGeral_CargoDS_13_Grupocargo_codigo3 ,
                                                 AV73WWGeral_CargoDS_14_Cargo_uocod3 ,
                                                 AV75WWGeral_CargoDS_16_Tfcargo_nome_sel ,
                                                 AV74WWGeral_CargoDS_15_Tfcargo_nome ,
                                                 AV76WWGeral_CargoDS_17_Tfgrupocargo_codigo ,
                                                 AV77WWGeral_CargoDS_18_Tfgrupocargo_codigo_to ,
                                                 AV79WWGeral_CargoDS_20_Tfcargo_uonom_sel ,
                                                 AV78WWGeral_CargoDS_19_Tfcargo_uonom ,
                                                 AV80WWGeral_CargoDS_21_Tfcargo_ativo_sel ,
                                                 A618Cargo_Nome ,
                                                 A615GrupoCargo_Codigo ,
                                                 A1002Cargo_UOCod ,
                                                 A1003Cargo_UONom ,
                                                 A628Cargo_Ativo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV61WWGeral_CargoDS_2_Cargo_nome1 = StringUtil.Concat( StringUtil.RTrim( AV61WWGeral_CargoDS_2_Cargo_nome1), "%", "");
            lV66WWGeral_CargoDS_7_Cargo_nome2 = StringUtil.Concat( StringUtil.RTrim( AV66WWGeral_CargoDS_7_Cargo_nome2), "%", "");
            lV71WWGeral_CargoDS_12_Cargo_nome3 = StringUtil.Concat( StringUtil.RTrim( AV71WWGeral_CargoDS_12_Cargo_nome3), "%", "");
            lV74WWGeral_CargoDS_15_Tfcargo_nome = StringUtil.Concat( StringUtil.RTrim( AV74WWGeral_CargoDS_15_Tfcargo_nome), "%", "");
            lV78WWGeral_CargoDS_19_Tfcargo_uonom = StringUtil.PadR( StringUtil.RTrim( AV78WWGeral_CargoDS_19_Tfcargo_uonom), 50, "%");
            /* Using cursor H00CW8 */
            pr_default.execute(6, new Object[] {lV61WWGeral_CargoDS_2_Cargo_nome1, AV62WWGeral_CargoDS_3_Grupocargo_codigo1, AV63WWGeral_CargoDS_4_Cargo_uocod1, lV66WWGeral_CargoDS_7_Cargo_nome2, AV67WWGeral_CargoDS_8_Grupocargo_codigo2, AV68WWGeral_CargoDS_9_Cargo_uocod2, lV71WWGeral_CargoDS_12_Cargo_nome3, AV72WWGeral_CargoDS_13_Grupocargo_codigo3, AV73WWGeral_CargoDS_14_Cargo_uocod3, lV74WWGeral_CargoDS_15_Tfcargo_nome, AV75WWGeral_CargoDS_16_Tfcargo_nome_sel, AV76WWGeral_CargoDS_17_Tfgrupocargo_codigo, AV77WWGeral_CargoDS_18_Tfgrupocargo_codigo_to, lV78WWGeral_CargoDS_19_Tfcargo_uonom, AV79WWGeral_CargoDS_20_Tfcargo_uonom_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_79_idx = 1;
            while ( ( (pr_default.getStatus(6) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1002Cargo_UOCod = H00CW8_A1002Cargo_UOCod[0];
               n1002Cargo_UOCod = H00CW8_n1002Cargo_UOCod[0];
               A628Cargo_Ativo = H00CW8_A628Cargo_Ativo[0];
               A1003Cargo_UONom = H00CW8_A1003Cargo_UONom[0];
               n1003Cargo_UONom = H00CW8_n1003Cargo_UONom[0];
               A615GrupoCargo_Codigo = H00CW8_A615GrupoCargo_Codigo[0];
               A618Cargo_Nome = H00CW8_A618Cargo_Nome[0];
               A617Cargo_Codigo = H00CW8_A617Cargo_Codigo[0];
               A1003Cargo_UONom = H00CW8_A1003Cargo_UONom[0];
               n1003Cargo_UONom = H00CW8_n1003Cargo_UONom[0];
               /* Execute user event: E29CW2 */
               E29CW2 ();
               pr_default.readNext(6);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(6) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(6);
            wbEnd = 79;
            WBCW0( ) ;
         }
         nGXsfl_79_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV60WWGeral_CargoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV61WWGeral_CargoDS_2_Cargo_nome1 = AV16Cargo_Nome1;
         AV62WWGeral_CargoDS_3_Grupocargo_codigo1 = AV31GrupoCargo_Codigo1;
         AV63WWGeral_CargoDS_4_Cargo_uocod1 = AV34Cargo_UOCod1;
         AV64WWGeral_CargoDS_5_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV65WWGeral_CargoDS_6_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV66WWGeral_CargoDS_7_Cargo_nome2 = AV19Cargo_Nome2;
         AV67WWGeral_CargoDS_8_Grupocargo_codigo2 = AV32GrupoCargo_Codigo2;
         AV68WWGeral_CargoDS_9_Cargo_uocod2 = AV35Cargo_UOCod2;
         AV69WWGeral_CargoDS_10_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV70WWGeral_CargoDS_11_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV71WWGeral_CargoDS_12_Cargo_nome3 = AV22Cargo_Nome3;
         AV72WWGeral_CargoDS_13_Grupocargo_codigo3 = AV33GrupoCargo_Codigo3;
         AV73WWGeral_CargoDS_14_Cargo_uocod3 = AV36Cargo_UOCod3;
         AV74WWGeral_CargoDS_15_Tfcargo_nome = AV40TFCargo_Nome;
         AV75WWGeral_CargoDS_16_Tfcargo_nome_sel = AV41TFCargo_Nome_Sel;
         AV76WWGeral_CargoDS_17_Tfgrupocargo_codigo = AV44TFGrupoCargo_Codigo;
         AV77WWGeral_CargoDS_18_Tfgrupocargo_codigo_to = AV45TFGrupoCargo_Codigo_To;
         AV78WWGeral_CargoDS_19_Tfcargo_uonom = AV48TFCargo_UONom;
         AV79WWGeral_CargoDS_20_Tfcargo_uonom_sel = AV49TFCargo_UONom_Sel;
         AV80WWGeral_CargoDS_21_Tfcargo_ativo_sel = AV52TFCargo_Ativo_Sel;
         pr_default.dynParam(7, new Object[]{ new Object[]{
                                              AV60WWGeral_CargoDS_1_Dynamicfiltersselector1 ,
                                              AV61WWGeral_CargoDS_2_Cargo_nome1 ,
                                              AV62WWGeral_CargoDS_3_Grupocargo_codigo1 ,
                                              AV63WWGeral_CargoDS_4_Cargo_uocod1 ,
                                              AV64WWGeral_CargoDS_5_Dynamicfiltersenabled2 ,
                                              AV65WWGeral_CargoDS_6_Dynamicfiltersselector2 ,
                                              AV66WWGeral_CargoDS_7_Cargo_nome2 ,
                                              AV67WWGeral_CargoDS_8_Grupocargo_codigo2 ,
                                              AV68WWGeral_CargoDS_9_Cargo_uocod2 ,
                                              AV69WWGeral_CargoDS_10_Dynamicfiltersenabled3 ,
                                              AV70WWGeral_CargoDS_11_Dynamicfiltersselector3 ,
                                              AV71WWGeral_CargoDS_12_Cargo_nome3 ,
                                              AV72WWGeral_CargoDS_13_Grupocargo_codigo3 ,
                                              AV73WWGeral_CargoDS_14_Cargo_uocod3 ,
                                              AV75WWGeral_CargoDS_16_Tfcargo_nome_sel ,
                                              AV74WWGeral_CargoDS_15_Tfcargo_nome ,
                                              AV76WWGeral_CargoDS_17_Tfgrupocargo_codigo ,
                                              AV77WWGeral_CargoDS_18_Tfgrupocargo_codigo_to ,
                                              AV79WWGeral_CargoDS_20_Tfcargo_uonom_sel ,
                                              AV78WWGeral_CargoDS_19_Tfcargo_uonom ,
                                              AV80WWGeral_CargoDS_21_Tfcargo_ativo_sel ,
                                              A618Cargo_Nome ,
                                              A615GrupoCargo_Codigo ,
                                              A1002Cargo_UOCod ,
                                              A1003Cargo_UONom ,
                                              A628Cargo_Ativo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV61WWGeral_CargoDS_2_Cargo_nome1 = StringUtil.Concat( StringUtil.RTrim( AV61WWGeral_CargoDS_2_Cargo_nome1), "%", "");
         lV66WWGeral_CargoDS_7_Cargo_nome2 = StringUtil.Concat( StringUtil.RTrim( AV66WWGeral_CargoDS_7_Cargo_nome2), "%", "");
         lV71WWGeral_CargoDS_12_Cargo_nome3 = StringUtil.Concat( StringUtil.RTrim( AV71WWGeral_CargoDS_12_Cargo_nome3), "%", "");
         lV74WWGeral_CargoDS_15_Tfcargo_nome = StringUtil.Concat( StringUtil.RTrim( AV74WWGeral_CargoDS_15_Tfcargo_nome), "%", "");
         lV78WWGeral_CargoDS_19_Tfcargo_uonom = StringUtil.PadR( StringUtil.RTrim( AV78WWGeral_CargoDS_19_Tfcargo_uonom), 50, "%");
         /* Using cursor H00CW9 */
         pr_default.execute(7, new Object[] {lV61WWGeral_CargoDS_2_Cargo_nome1, AV62WWGeral_CargoDS_3_Grupocargo_codigo1, AV63WWGeral_CargoDS_4_Cargo_uocod1, lV66WWGeral_CargoDS_7_Cargo_nome2, AV67WWGeral_CargoDS_8_Grupocargo_codigo2, AV68WWGeral_CargoDS_9_Cargo_uocod2, lV71WWGeral_CargoDS_12_Cargo_nome3, AV72WWGeral_CargoDS_13_Grupocargo_codigo3, AV73WWGeral_CargoDS_14_Cargo_uocod3, lV74WWGeral_CargoDS_15_Tfcargo_nome, AV75WWGeral_CargoDS_16_Tfcargo_nome_sel, AV76WWGeral_CargoDS_17_Tfgrupocargo_codigo, AV77WWGeral_CargoDS_18_Tfgrupocargo_codigo_to, lV78WWGeral_CargoDS_19_Tfcargo_uonom, AV79WWGeral_CargoDS_20_Tfcargo_uonom_sel});
         GRID_nRecordCount = H00CW9_AGRID_nRecordCount[0];
         pr_default.close(7);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV60WWGeral_CargoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV61WWGeral_CargoDS_2_Cargo_nome1 = AV16Cargo_Nome1;
         AV62WWGeral_CargoDS_3_Grupocargo_codigo1 = AV31GrupoCargo_Codigo1;
         AV63WWGeral_CargoDS_4_Cargo_uocod1 = AV34Cargo_UOCod1;
         AV64WWGeral_CargoDS_5_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV65WWGeral_CargoDS_6_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV66WWGeral_CargoDS_7_Cargo_nome2 = AV19Cargo_Nome2;
         AV67WWGeral_CargoDS_8_Grupocargo_codigo2 = AV32GrupoCargo_Codigo2;
         AV68WWGeral_CargoDS_9_Cargo_uocod2 = AV35Cargo_UOCod2;
         AV69WWGeral_CargoDS_10_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV70WWGeral_CargoDS_11_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV71WWGeral_CargoDS_12_Cargo_nome3 = AV22Cargo_Nome3;
         AV72WWGeral_CargoDS_13_Grupocargo_codigo3 = AV33GrupoCargo_Codigo3;
         AV73WWGeral_CargoDS_14_Cargo_uocod3 = AV36Cargo_UOCod3;
         AV74WWGeral_CargoDS_15_Tfcargo_nome = AV40TFCargo_Nome;
         AV75WWGeral_CargoDS_16_Tfcargo_nome_sel = AV41TFCargo_Nome_Sel;
         AV76WWGeral_CargoDS_17_Tfgrupocargo_codigo = AV44TFGrupoCargo_Codigo;
         AV77WWGeral_CargoDS_18_Tfgrupocargo_codigo_to = AV45TFGrupoCargo_Codigo_To;
         AV78WWGeral_CargoDS_19_Tfcargo_uonom = AV48TFCargo_UONom;
         AV79WWGeral_CargoDS_20_Tfcargo_uonom_sel = AV49TFCargo_UONom_Sel;
         AV80WWGeral_CargoDS_21_Tfcargo_ativo_sel = AV52TFCargo_Ativo_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Cargo_Nome1, AV31GrupoCargo_Codigo1, AV34Cargo_UOCod1, AV18DynamicFiltersSelector2, AV19Cargo_Nome2, AV32GrupoCargo_Codigo2, AV35Cargo_UOCod2, AV21DynamicFiltersSelector3, AV22Cargo_Nome3, AV33GrupoCargo_Codigo3, AV36Cargo_UOCod3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV40TFCargo_Nome, AV41TFCargo_Nome_Sel, AV44TFGrupoCargo_Codigo, AV45TFGrupoCargo_Codigo_To, AV48TFCargo_UONom, AV49TFCargo_UONom_Sel, AV52TFCargo_Ativo_Sel, AV42ddo_Cargo_NomeTitleControlIdToReplace, AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace, AV50ddo_Cargo_UONomTitleControlIdToReplace, AV53ddo_Cargo_AtivoTitleControlIdToReplace, AV83Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A617Cargo_Codigo, A1002Cargo_UOCod) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV60WWGeral_CargoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV61WWGeral_CargoDS_2_Cargo_nome1 = AV16Cargo_Nome1;
         AV62WWGeral_CargoDS_3_Grupocargo_codigo1 = AV31GrupoCargo_Codigo1;
         AV63WWGeral_CargoDS_4_Cargo_uocod1 = AV34Cargo_UOCod1;
         AV64WWGeral_CargoDS_5_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV65WWGeral_CargoDS_6_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV66WWGeral_CargoDS_7_Cargo_nome2 = AV19Cargo_Nome2;
         AV67WWGeral_CargoDS_8_Grupocargo_codigo2 = AV32GrupoCargo_Codigo2;
         AV68WWGeral_CargoDS_9_Cargo_uocod2 = AV35Cargo_UOCod2;
         AV69WWGeral_CargoDS_10_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV70WWGeral_CargoDS_11_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV71WWGeral_CargoDS_12_Cargo_nome3 = AV22Cargo_Nome3;
         AV72WWGeral_CargoDS_13_Grupocargo_codigo3 = AV33GrupoCargo_Codigo3;
         AV73WWGeral_CargoDS_14_Cargo_uocod3 = AV36Cargo_UOCod3;
         AV74WWGeral_CargoDS_15_Tfcargo_nome = AV40TFCargo_Nome;
         AV75WWGeral_CargoDS_16_Tfcargo_nome_sel = AV41TFCargo_Nome_Sel;
         AV76WWGeral_CargoDS_17_Tfgrupocargo_codigo = AV44TFGrupoCargo_Codigo;
         AV77WWGeral_CargoDS_18_Tfgrupocargo_codigo_to = AV45TFGrupoCargo_Codigo_To;
         AV78WWGeral_CargoDS_19_Tfcargo_uonom = AV48TFCargo_UONom;
         AV79WWGeral_CargoDS_20_Tfcargo_uonom_sel = AV49TFCargo_UONom_Sel;
         AV80WWGeral_CargoDS_21_Tfcargo_ativo_sel = AV52TFCargo_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Cargo_Nome1, AV31GrupoCargo_Codigo1, AV34Cargo_UOCod1, AV18DynamicFiltersSelector2, AV19Cargo_Nome2, AV32GrupoCargo_Codigo2, AV35Cargo_UOCod2, AV21DynamicFiltersSelector3, AV22Cargo_Nome3, AV33GrupoCargo_Codigo3, AV36Cargo_UOCod3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV40TFCargo_Nome, AV41TFCargo_Nome_Sel, AV44TFGrupoCargo_Codigo, AV45TFGrupoCargo_Codigo_To, AV48TFCargo_UONom, AV49TFCargo_UONom_Sel, AV52TFCargo_Ativo_Sel, AV42ddo_Cargo_NomeTitleControlIdToReplace, AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace, AV50ddo_Cargo_UONomTitleControlIdToReplace, AV53ddo_Cargo_AtivoTitleControlIdToReplace, AV83Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A617Cargo_Codigo, A1002Cargo_UOCod) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV60WWGeral_CargoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV61WWGeral_CargoDS_2_Cargo_nome1 = AV16Cargo_Nome1;
         AV62WWGeral_CargoDS_3_Grupocargo_codigo1 = AV31GrupoCargo_Codigo1;
         AV63WWGeral_CargoDS_4_Cargo_uocod1 = AV34Cargo_UOCod1;
         AV64WWGeral_CargoDS_5_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV65WWGeral_CargoDS_6_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV66WWGeral_CargoDS_7_Cargo_nome2 = AV19Cargo_Nome2;
         AV67WWGeral_CargoDS_8_Grupocargo_codigo2 = AV32GrupoCargo_Codigo2;
         AV68WWGeral_CargoDS_9_Cargo_uocod2 = AV35Cargo_UOCod2;
         AV69WWGeral_CargoDS_10_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV70WWGeral_CargoDS_11_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV71WWGeral_CargoDS_12_Cargo_nome3 = AV22Cargo_Nome3;
         AV72WWGeral_CargoDS_13_Grupocargo_codigo3 = AV33GrupoCargo_Codigo3;
         AV73WWGeral_CargoDS_14_Cargo_uocod3 = AV36Cargo_UOCod3;
         AV74WWGeral_CargoDS_15_Tfcargo_nome = AV40TFCargo_Nome;
         AV75WWGeral_CargoDS_16_Tfcargo_nome_sel = AV41TFCargo_Nome_Sel;
         AV76WWGeral_CargoDS_17_Tfgrupocargo_codigo = AV44TFGrupoCargo_Codigo;
         AV77WWGeral_CargoDS_18_Tfgrupocargo_codigo_to = AV45TFGrupoCargo_Codigo_To;
         AV78WWGeral_CargoDS_19_Tfcargo_uonom = AV48TFCargo_UONom;
         AV79WWGeral_CargoDS_20_Tfcargo_uonom_sel = AV49TFCargo_UONom_Sel;
         AV80WWGeral_CargoDS_21_Tfcargo_ativo_sel = AV52TFCargo_Ativo_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Cargo_Nome1, AV31GrupoCargo_Codigo1, AV34Cargo_UOCod1, AV18DynamicFiltersSelector2, AV19Cargo_Nome2, AV32GrupoCargo_Codigo2, AV35Cargo_UOCod2, AV21DynamicFiltersSelector3, AV22Cargo_Nome3, AV33GrupoCargo_Codigo3, AV36Cargo_UOCod3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV40TFCargo_Nome, AV41TFCargo_Nome_Sel, AV44TFGrupoCargo_Codigo, AV45TFGrupoCargo_Codigo_To, AV48TFCargo_UONom, AV49TFCargo_UONom_Sel, AV52TFCargo_Ativo_Sel, AV42ddo_Cargo_NomeTitleControlIdToReplace, AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace, AV50ddo_Cargo_UONomTitleControlIdToReplace, AV53ddo_Cargo_AtivoTitleControlIdToReplace, AV83Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A617Cargo_Codigo, A1002Cargo_UOCod) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV60WWGeral_CargoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV61WWGeral_CargoDS_2_Cargo_nome1 = AV16Cargo_Nome1;
         AV62WWGeral_CargoDS_3_Grupocargo_codigo1 = AV31GrupoCargo_Codigo1;
         AV63WWGeral_CargoDS_4_Cargo_uocod1 = AV34Cargo_UOCod1;
         AV64WWGeral_CargoDS_5_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV65WWGeral_CargoDS_6_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV66WWGeral_CargoDS_7_Cargo_nome2 = AV19Cargo_Nome2;
         AV67WWGeral_CargoDS_8_Grupocargo_codigo2 = AV32GrupoCargo_Codigo2;
         AV68WWGeral_CargoDS_9_Cargo_uocod2 = AV35Cargo_UOCod2;
         AV69WWGeral_CargoDS_10_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV70WWGeral_CargoDS_11_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV71WWGeral_CargoDS_12_Cargo_nome3 = AV22Cargo_Nome3;
         AV72WWGeral_CargoDS_13_Grupocargo_codigo3 = AV33GrupoCargo_Codigo3;
         AV73WWGeral_CargoDS_14_Cargo_uocod3 = AV36Cargo_UOCod3;
         AV74WWGeral_CargoDS_15_Tfcargo_nome = AV40TFCargo_Nome;
         AV75WWGeral_CargoDS_16_Tfcargo_nome_sel = AV41TFCargo_Nome_Sel;
         AV76WWGeral_CargoDS_17_Tfgrupocargo_codigo = AV44TFGrupoCargo_Codigo;
         AV77WWGeral_CargoDS_18_Tfgrupocargo_codigo_to = AV45TFGrupoCargo_Codigo_To;
         AV78WWGeral_CargoDS_19_Tfcargo_uonom = AV48TFCargo_UONom;
         AV79WWGeral_CargoDS_20_Tfcargo_uonom_sel = AV49TFCargo_UONom_Sel;
         AV80WWGeral_CargoDS_21_Tfcargo_ativo_sel = AV52TFCargo_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Cargo_Nome1, AV31GrupoCargo_Codigo1, AV34Cargo_UOCod1, AV18DynamicFiltersSelector2, AV19Cargo_Nome2, AV32GrupoCargo_Codigo2, AV35Cargo_UOCod2, AV21DynamicFiltersSelector3, AV22Cargo_Nome3, AV33GrupoCargo_Codigo3, AV36Cargo_UOCod3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV40TFCargo_Nome, AV41TFCargo_Nome_Sel, AV44TFGrupoCargo_Codigo, AV45TFGrupoCargo_Codigo_To, AV48TFCargo_UONom, AV49TFCargo_UONom_Sel, AV52TFCargo_Ativo_Sel, AV42ddo_Cargo_NomeTitleControlIdToReplace, AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace, AV50ddo_Cargo_UONomTitleControlIdToReplace, AV53ddo_Cargo_AtivoTitleControlIdToReplace, AV83Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A617Cargo_Codigo, A1002Cargo_UOCod) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV60WWGeral_CargoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV61WWGeral_CargoDS_2_Cargo_nome1 = AV16Cargo_Nome1;
         AV62WWGeral_CargoDS_3_Grupocargo_codigo1 = AV31GrupoCargo_Codigo1;
         AV63WWGeral_CargoDS_4_Cargo_uocod1 = AV34Cargo_UOCod1;
         AV64WWGeral_CargoDS_5_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV65WWGeral_CargoDS_6_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV66WWGeral_CargoDS_7_Cargo_nome2 = AV19Cargo_Nome2;
         AV67WWGeral_CargoDS_8_Grupocargo_codigo2 = AV32GrupoCargo_Codigo2;
         AV68WWGeral_CargoDS_9_Cargo_uocod2 = AV35Cargo_UOCod2;
         AV69WWGeral_CargoDS_10_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV70WWGeral_CargoDS_11_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV71WWGeral_CargoDS_12_Cargo_nome3 = AV22Cargo_Nome3;
         AV72WWGeral_CargoDS_13_Grupocargo_codigo3 = AV33GrupoCargo_Codigo3;
         AV73WWGeral_CargoDS_14_Cargo_uocod3 = AV36Cargo_UOCod3;
         AV74WWGeral_CargoDS_15_Tfcargo_nome = AV40TFCargo_Nome;
         AV75WWGeral_CargoDS_16_Tfcargo_nome_sel = AV41TFCargo_Nome_Sel;
         AV76WWGeral_CargoDS_17_Tfgrupocargo_codigo = AV44TFGrupoCargo_Codigo;
         AV77WWGeral_CargoDS_18_Tfgrupocargo_codigo_to = AV45TFGrupoCargo_Codigo_To;
         AV78WWGeral_CargoDS_19_Tfcargo_uonom = AV48TFCargo_UONom;
         AV79WWGeral_CargoDS_20_Tfcargo_uonom_sel = AV49TFCargo_UONom_Sel;
         AV80WWGeral_CargoDS_21_Tfcargo_ativo_sel = AV52TFCargo_Ativo_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Cargo_Nome1, AV31GrupoCargo_Codigo1, AV34Cargo_UOCod1, AV18DynamicFiltersSelector2, AV19Cargo_Nome2, AV32GrupoCargo_Codigo2, AV35Cargo_UOCod2, AV21DynamicFiltersSelector3, AV22Cargo_Nome3, AV33GrupoCargo_Codigo3, AV36Cargo_UOCod3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV40TFCargo_Nome, AV41TFCargo_Nome_Sel, AV44TFGrupoCargo_Codigo, AV45TFGrupoCargo_Codigo_To, AV48TFCargo_UONom, AV49TFCargo_UONom_Sel, AV52TFCargo_Ativo_Sel, AV42ddo_Cargo_NomeTitleControlIdToReplace, AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace, AV50ddo_Cargo_UONomTitleControlIdToReplace, AV53ddo_Cargo_AtivoTitleControlIdToReplace, AV83Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A617Cargo_Codigo, A1002Cargo_UOCod) ;
         }
         return (int)(0) ;
      }

      protected void STRUPCW0( )
      {
         /* Before Start, stand alone formulas. */
         AV83Pgmname = "WWGeral_Cargo";
         context.Gx_err = 0;
         GXVvGRUPOCARGO_CODIGO1_htmlCW2( ) ;
         GXVvCARGO_UOCOD1_htmlCW2( ) ;
         GXVvGRUPOCARGO_CODIGO2_htmlCW2( ) ;
         GXVvCARGO_UOCOD2_htmlCW2( ) ;
         GXVvGRUPOCARGO_CODIGO3_htmlCW2( ) ;
         GXVvCARGO_UOCOD3_htmlCW2( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E27CW2 */
         E27CW2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV54DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCARGO_NOMETITLEFILTERDATA"), AV39Cargo_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vGRUPOCARGO_CODIGOTITLEFILTERDATA"), AV43GrupoCargo_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCARGO_UONOMTITLEFILTERDATA"), AV47Cargo_UONomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCARGO_ATIVOTITLEFILTERDATA"), AV51Cargo_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            AV16Cargo_Nome1 = StringUtil.Upper( cgiGet( edtavCargo_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Cargo_Nome1", AV16Cargo_Nome1);
            dynavGrupocargo_codigo1.Name = dynavGrupocargo_codigo1_Internalname;
            dynavGrupocargo_codigo1.CurrentValue = cgiGet( dynavGrupocargo_codigo1_Internalname);
            AV31GrupoCargo_Codigo1 = (int)(NumberUtil.Val( cgiGet( dynavGrupocargo_codigo1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31GrupoCargo_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31GrupoCargo_Codigo1), 6, 0)));
            dynavCargo_uocod1.Name = dynavCargo_uocod1_Internalname;
            dynavCargo_uocod1.CurrentValue = cgiGet( dynavCargo_uocod1_Internalname);
            AV34Cargo_UOCod1 = (int)(NumberUtil.Val( cgiGet( dynavCargo_uocod1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Cargo_UOCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Cargo_UOCod1), 6, 0)));
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV18DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
            AV19Cargo_Nome2 = StringUtil.Upper( cgiGet( edtavCargo_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Cargo_Nome2", AV19Cargo_Nome2);
            dynavGrupocargo_codigo2.Name = dynavGrupocargo_codigo2_Internalname;
            dynavGrupocargo_codigo2.CurrentValue = cgiGet( dynavGrupocargo_codigo2_Internalname);
            AV32GrupoCargo_Codigo2 = (int)(NumberUtil.Val( cgiGet( dynavGrupocargo_codigo2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32GrupoCargo_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32GrupoCargo_Codigo2), 6, 0)));
            dynavCargo_uocod2.Name = dynavCargo_uocod2_Internalname;
            dynavCargo_uocod2.CurrentValue = cgiGet( dynavCargo_uocod2_Internalname);
            AV35Cargo_UOCod2 = (int)(NumberUtil.Val( cgiGet( dynavCargo_uocod2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Cargo_UOCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35Cargo_UOCod2), 6, 0)));
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV21DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
            AV22Cargo_Nome3 = StringUtil.Upper( cgiGet( edtavCargo_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Cargo_Nome3", AV22Cargo_Nome3);
            dynavGrupocargo_codigo3.Name = dynavGrupocargo_codigo3_Internalname;
            dynavGrupocargo_codigo3.CurrentValue = cgiGet( dynavGrupocargo_codigo3_Internalname);
            AV33GrupoCargo_Codigo3 = (int)(NumberUtil.Val( cgiGet( dynavGrupocargo_codigo3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33GrupoCargo_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33GrupoCargo_Codigo3), 6, 0)));
            dynavCargo_uocod3.Name = dynavCargo_uocod3_Internalname;
            dynavCargo_uocod3.CurrentValue = cgiGet( dynavCargo_uocod3_Internalname);
            AV36Cargo_UOCod3 = (int)(NumberUtil.Val( cgiGet( dynavCargo_uocod3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Cargo_UOCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Cargo_UOCod3), 6, 0)));
            AV17DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
            AV20DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
            AV40TFCargo_Nome = StringUtil.Upper( cgiGet( edtavTfcargo_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFCargo_Nome", AV40TFCargo_Nome);
            AV41TFCargo_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfcargo_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFCargo_Nome_Sel", AV41TFCargo_Nome_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfgrupocargo_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfgrupocargo_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFGRUPOCARGO_CODIGO");
               GX_FocusControl = edtavTfgrupocargo_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44TFGrupoCargo_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFGrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFGrupoCargo_Codigo), 6, 0)));
            }
            else
            {
               AV44TFGrupoCargo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfgrupocargo_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFGrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFGrupoCargo_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfgrupocargo_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfgrupocargo_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFGRUPOCARGO_CODIGO_TO");
               GX_FocusControl = edtavTfgrupocargo_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV45TFGrupoCargo_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFGrupoCargo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFGrupoCargo_Codigo_To), 6, 0)));
            }
            else
            {
               AV45TFGrupoCargo_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfgrupocargo_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFGrupoCargo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFGrupoCargo_Codigo_To), 6, 0)));
            }
            AV48TFCargo_UONom = StringUtil.Upper( cgiGet( edtavTfcargo_uonom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFCargo_UONom", AV48TFCargo_UONom);
            AV49TFCargo_UONom_Sel = StringUtil.Upper( cgiGet( edtavTfcargo_uonom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFCargo_UONom_Sel", AV49TFCargo_UONom_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcargo_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcargo_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCARGO_ATIVO_SEL");
               GX_FocusControl = edtavTfcargo_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV52TFCargo_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFCargo_Ativo_Sel", StringUtil.Str( (decimal)(AV52TFCargo_Ativo_Sel), 1, 0));
            }
            else
            {
               AV52TFCargo_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfcargo_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFCargo_Ativo_Sel", StringUtil.Str( (decimal)(AV52TFCargo_Ativo_Sel), 1, 0));
            }
            AV42ddo_Cargo_NomeTitleControlIdToReplace = cgiGet( edtavDdo_cargo_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ddo_Cargo_NomeTitleControlIdToReplace", AV42ddo_Cargo_NomeTitleControlIdToReplace);
            AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_grupocargo_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace", AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace);
            AV50ddo_Cargo_UONomTitleControlIdToReplace = cgiGet( edtavDdo_cargo_uonomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_Cargo_UONomTitleControlIdToReplace", AV50ddo_Cargo_UONomTitleControlIdToReplace);
            AV53ddo_Cargo_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_cargo_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_Cargo_AtivoTitleControlIdToReplace", AV53ddo_Cargo_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_79 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_79"), ",", "."));
            AV56GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV57GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_cargo_nome_Caption = cgiGet( "DDO_CARGO_NOME_Caption");
            Ddo_cargo_nome_Tooltip = cgiGet( "DDO_CARGO_NOME_Tooltip");
            Ddo_cargo_nome_Cls = cgiGet( "DDO_CARGO_NOME_Cls");
            Ddo_cargo_nome_Filteredtext_set = cgiGet( "DDO_CARGO_NOME_Filteredtext_set");
            Ddo_cargo_nome_Selectedvalue_set = cgiGet( "DDO_CARGO_NOME_Selectedvalue_set");
            Ddo_cargo_nome_Dropdownoptionstype = cgiGet( "DDO_CARGO_NOME_Dropdownoptionstype");
            Ddo_cargo_nome_Titlecontrolidtoreplace = cgiGet( "DDO_CARGO_NOME_Titlecontrolidtoreplace");
            Ddo_cargo_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CARGO_NOME_Includesortasc"));
            Ddo_cargo_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CARGO_NOME_Includesortdsc"));
            Ddo_cargo_nome_Sortedstatus = cgiGet( "DDO_CARGO_NOME_Sortedstatus");
            Ddo_cargo_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CARGO_NOME_Includefilter"));
            Ddo_cargo_nome_Filtertype = cgiGet( "DDO_CARGO_NOME_Filtertype");
            Ddo_cargo_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CARGO_NOME_Filterisrange"));
            Ddo_cargo_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CARGO_NOME_Includedatalist"));
            Ddo_cargo_nome_Datalisttype = cgiGet( "DDO_CARGO_NOME_Datalisttype");
            Ddo_cargo_nome_Datalistproc = cgiGet( "DDO_CARGO_NOME_Datalistproc");
            Ddo_cargo_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CARGO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_cargo_nome_Sortasc = cgiGet( "DDO_CARGO_NOME_Sortasc");
            Ddo_cargo_nome_Sortdsc = cgiGet( "DDO_CARGO_NOME_Sortdsc");
            Ddo_cargo_nome_Loadingdata = cgiGet( "DDO_CARGO_NOME_Loadingdata");
            Ddo_cargo_nome_Cleanfilter = cgiGet( "DDO_CARGO_NOME_Cleanfilter");
            Ddo_cargo_nome_Noresultsfound = cgiGet( "DDO_CARGO_NOME_Noresultsfound");
            Ddo_cargo_nome_Searchbuttontext = cgiGet( "DDO_CARGO_NOME_Searchbuttontext");
            Ddo_grupocargo_codigo_Caption = cgiGet( "DDO_GRUPOCARGO_CODIGO_Caption");
            Ddo_grupocargo_codigo_Tooltip = cgiGet( "DDO_GRUPOCARGO_CODIGO_Tooltip");
            Ddo_grupocargo_codigo_Cls = cgiGet( "DDO_GRUPOCARGO_CODIGO_Cls");
            Ddo_grupocargo_codigo_Filteredtext_set = cgiGet( "DDO_GRUPOCARGO_CODIGO_Filteredtext_set");
            Ddo_grupocargo_codigo_Filteredtextto_set = cgiGet( "DDO_GRUPOCARGO_CODIGO_Filteredtextto_set");
            Ddo_grupocargo_codigo_Dropdownoptionstype = cgiGet( "DDO_GRUPOCARGO_CODIGO_Dropdownoptionstype");
            Ddo_grupocargo_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_GRUPOCARGO_CODIGO_Titlecontrolidtoreplace");
            Ddo_grupocargo_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_GRUPOCARGO_CODIGO_Includesortasc"));
            Ddo_grupocargo_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_GRUPOCARGO_CODIGO_Includesortdsc"));
            Ddo_grupocargo_codigo_Sortedstatus = cgiGet( "DDO_GRUPOCARGO_CODIGO_Sortedstatus");
            Ddo_grupocargo_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_GRUPOCARGO_CODIGO_Includefilter"));
            Ddo_grupocargo_codigo_Filtertype = cgiGet( "DDO_GRUPOCARGO_CODIGO_Filtertype");
            Ddo_grupocargo_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_GRUPOCARGO_CODIGO_Filterisrange"));
            Ddo_grupocargo_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_GRUPOCARGO_CODIGO_Includedatalist"));
            Ddo_grupocargo_codigo_Sortasc = cgiGet( "DDO_GRUPOCARGO_CODIGO_Sortasc");
            Ddo_grupocargo_codigo_Sortdsc = cgiGet( "DDO_GRUPOCARGO_CODIGO_Sortdsc");
            Ddo_grupocargo_codigo_Cleanfilter = cgiGet( "DDO_GRUPOCARGO_CODIGO_Cleanfilter");
            Ddo_grupocargo_codigo_Rangefilterfrom = cgiGet( "DDO_GRUPOCARGO_CODIGO_Rangefilterfrom");
            Ddo_grupocargo_codigo_Rangefilterto = cgiGet( "DDO_GRUPOCARGO_CODIGO_Rangefilterto");
            Ddo_grupocargo_codigo_Searchbuttontext = cgiGet( "DDO_GRUPOCARGO_CODIGO_Searchbuttontext");
            Ddo_cargo_uonom_Caption = cgiGet( "DDO_CARGO_UONOM_Caption");
            Ddo_cargo_uonom_Tooltip = cgiGet( "DDO_CARGO_UONOM_Tooltip");
            Ddo_cargo_uonom_Cls = cgiGet( "DDO_CARGO_UONOM_Cls");
            Ddo_cargo_uonom_Filteredtext_set = cgiGet( "DDO_CARGO_UONOM_Filteredtext_set");
            Ddo_cargo_uonom_Selectedvalue_set = cgiGet( "DDO_CARGO_UONOM_Selectedvalue_set");
            Ddo_cargo_uonom_Dropdownoptionstype = cgiGet( "DDO_CARGO_UONOM_Dropdownoptionstype");
            Ddo_cargo_uonom_Titlecontrolidtoreplace = cgiGet( "DDO_CARGO_UONOM_Titlecontrolidtoreplace");
            Ddo_cargo_uonom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CARGO_UONOM_Includesortasc"));
            Ddo_cargo_uonom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CARGO_UONOM_Includesortdsc"));
            Ddo_cargo_uonom_Sortedstatus = cgiGet( "DDO_CARGO_UONOM_Sortedstatus");
            Ddo_cargo_uonom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CARGO_UONOM_Includefilter"));
            Ddo_cargo_uonom_Filtertype = cgiGet( "DDO_CARGO_UONOM_Filtertype");
            Ddo_cargo_uonom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CARGO_UONOM_Filterisrange"));
            Ddo_cargo_uonom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CARGO_UONOM_Includedatalist"));
            Ddo_cargo_uonom_Datalisttype = cgiGet( "DDO_CARGO_UONOM_Datalisttype");
            Ddo_cargo_uonom_Datalistproc = cgiGet( "DDO_CARGO_UONOM_Datalistproc");
            Ddo_cargo_uonom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CARGO_UONOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_cargo_uonom_Sortasc = cgiGet( "DDO_CARGO_UONOM_Sortasc");
            Ddo_cargo_uonom_Sortdsc = cgiGet( "DDO_CARGO_UONOM_Sortdsc");
            Ddo_cargo_uonom_Loadingdata = cgiGet( "DDO_CARGO_UONOM_Loadingdata");
            Ddo_cargo_uonom_Cleanfilter = cgiGet( "DDO_CARGO_UONOM_Cleanfilter");
            Ddo_cargo_uonom_Noresultsfound = cgiGet( "DDO_CARGO_UONOM_Noresultsfound");
            Ddo_cargo_uonom_Searchbuttontext = cgiGet( "DDO_CARGO_UONOM_Searchbuttontext");
            Ddo_cargo_ativo_Caption = cgiGet( "DDO_CARGO_ATIVO_Caption");
            Ddo_cargo_ativo_Tooltip = cgiGet( "DDO_CARGO_ATIVO_Tooltip");
            Ddo_cargo_ativo_Cls = cgiGet( "DDO_CARGO_ATIVO_Cls");
            Ddo_cargo_ativo_Selectedvalue_set = cgiGet( "DDO_CARGO_ATIVO_Selectedvalue_set");
            Ddo_cargo_ativo_Dropdownoptionstype = cgiGet( "DDO_CARGO_ATIVO_Dropdownoptionstype");
            Ddo_cargo_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_CARGO_ATIVO_Titlecontrolidtoreplace");
            Ddo_cargo_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CARGO_ATIVO_Includesortasc"));
            Ddo_cargo_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CARGO_ATIVO_Includesortdsc"));
            Ddo_cargo_ativo_Sortedstatus = cgiGet( "DDO_CARGO_ATIVO_Sortedstatus");
            Ddo_cargo_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CARGO_ATIVO_Includefilter"));
            Ddo_cargo_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CARGO_ATIVO_Includedatalist"));
            Ddo_cargo_ativo_Datalisttype = cgiGet( "DDO_CARGO_ATIVO_Datalisttype");
            Ddo_cargo_ativo_Datalistfixedvalues = cgiGet( "DDO_CARGO_ATIVO_Datalistfixedvalues");
            Ddo_cargo_ativo_Sortasc = cgiGet( "DDO_CARGO_ATIVO_Sortasc");
            Ddo_cargo_ativo_Sortdsc = cgiGet( "DDO_CARGO_ATIVO_Sortdsc");
            Ddo_cargo_ativo_Cleanfilter = cgiGet( "DDO_CARGO_ATIVO_Cleanfilter");
            Ddo_cargo_ativo_Searchbuttontext = cgiGet( "DDO_CARGO_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_cargo_nome_Activeeventkey = cgiGet( "DDO_CARGO_NOME_Activeeventkey");
            Ddo_cargo_nome_Filteredtext_get = cgiGet( "DDO_CARGO_NOME_Filteredtext_get");
            Ddo_cargo_nome_Selectedvalue_get = cgiGet( "DDO_CARGO_NOME_Selectedvalue_get");
            Ddo_grupocargo_codigo_Activeeventkey = cgiGet( "DDO_GRUPOCARGO_CODIGO_Activeeventkey");
            Ddo_grupocargo_codigo_Filteredtext_get = cgiGet( "DDO_GRUPOCARGO_CODIGO_Filteredtext_get");
            Ddo_grupocargo_codigo_Filteredtextto_get = cgiGet( "DDO_GRUPOCARGO_CODIGO_Filteredtextto_get");
            Ddo_cargo_uonom_Activeeventkey = cgiGet( "DDO_CARGO_UONOM_Activeeventkey");
            Ddo_cargo_uonom_Filteredtext_get = cgiGet( "DDO_CARGO_UONOM_Filteredtext_get");
            Ddo_cargo_uonom_Selectedvalue_get = cgiGet( "DDO_CARGO_UONOM_Selectedvalue_get");
            Ddo_cargo_ativo_Activeeventkey = cgiGet( "DDO_CARGO_ATIVO_Activeeventkey");
            Ddo_cargo_ativo_Selectedvalue_get = cgiGet( "DDO_CARGO_ATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCARGO_NOME1"), AV16Cargo_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vGRUPOCARGO_CODIGO1"), ",", ".") != Convert.ToDecimal( AV31GrupoCargo_Codigo1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCARGO_UOCOD1"), ",", ".") != Convert.ToDecimal( AV34Cargo_UOCod1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV18DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCARGO_NOME2"), AV19Cargo_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vGRUPOCARGO_CODIGO2"), ",", ".") != Convert.ToDecimal( AV32GrupoCargo_Codigo2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCARGO_UOCOD2"), ",", ".") != Convert.ToDecimal( AV35Cargo_UOCod2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV21DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCARGO_NOME3"), AV22Cargo_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vGRUPOCARGO_CODIGO3"), ",", ".") != Convert.ToDecimal( AV33GrupoCargo_Codigo3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCARGO_UOCOD3"), ",", ".") != Convert.ToDecimal( AV36Cargo_UOCod3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV17DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV20DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCARGO_NOME"), AV40TFCargo_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCARGO_NOME_SEL"), AV41TFCargo_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFGRUPOCARGO_CODIGO"), ",", ".") != Convert.ToDecimal( AV44TFGrupoCargo_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFGRUPOCARGO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV45TFGrupoCargo_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCARGO_UONOM"), AV48TFCargo_UONom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCARGO_UONOM_SEL"), AV49TFCargo_UONom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCARGO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV52TFCargo_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E27CW2 */
         E27CW2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E27CW2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "CARGO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV18DynamicFiltersSelector2 = "CARGO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector3 = "CARGO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfcargo_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcargo_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcargo_nome_Visible), 5, 0)));
         edtavTfcargo_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcargo_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcargo_nome_sel_Visible), 5, 0)));
         edtavTfgrupocargo_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfgrupocargo_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfgrupocargo_codigo_Visible), 5, 0)));
         edtavTfgrupocargo_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfgrupocargo_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfgrupocargo_codigo_to_Visible), 5, 0)));
         edtavTfcargo_uonom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcargo_uonom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcargo_uonom_Visible), 5, 0)));
         edtavTfcargo_uonom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcargo_uonom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcargo_uonom_sel_Visible), 5, 0)));
         edtavTfcargo_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcargo_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcargo_ativo_sel_Visible), 5, 0)));
         Ddo_cargo_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Cargo_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_cargo_nome_Internalname, "TitleControlIdToReplace", Ddo_cargo_nome_Titlecontrolidtoreplace);
         AV42ddo_Cargo_NomeTitleControlIdToReplace = Ddo_cargo_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ddo_Cargo_NomeTitleControlIdToReplace", AV42ddo_Cargo_NomeTitleControlIdToReplace);
         edtavDdo_cargo_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_cargo_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_cargo_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_grupocargo_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_GrupoCargo_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_grupocargo_codigo_Internalname, "TitleControlIdToReplace", Ddo_grupocargo_codigo_Titlecontrolidtoreplace);
         AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace = Ddo_grupocargo_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace", AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace);
         edtavDdo_grupocargo_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_grupocargo_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_grupocargo_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_cargo_uonom_Titlecontrolidtoreplace = subGrid_Internalname+"_Cargo_UONom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_cargo_uonom_Internalname, "TitleControlIdToReplace", Ddo_cargo_uonom_Titlecontrolidtoreplace);
         AV50ddo_Cargo_UONomTitleControlIdToReplace = Ddo_cargo_uonom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_Cargo_UONomTitleControlIdToReplace", AV50ddo_Cargo_UONomTitleControlIdToReplace);
         edtavDdo_cargo_uonomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_cargo_uonomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_cargo_uonomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_cargo_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_Cargo_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_cargo_ativo_Internalname, "TitleControlIdToReplace", Ddo_cargo_ativo_Titlecontrolidtoreplace);
         AV53ddo_Cargo_AtivoTitleControlIdToReplace = Ddo_cargo_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_Cargo_AtivoTitleControlIdToReplace", AV53ddo_Cargo_AtivoTitleControlIdToReplace);
         edtavDdo_cargo_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_cargo_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_cargo_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Cargos";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Cargo", 0);
         cmbavOrderedby.addItem("2", "Grupo", 0);
         cmbavOrderedby.addItem("3", "Unidade Organizacional", 0);
         cmbavOrderedby.addItem("4", "Ativo?", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV54DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV54DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E28CW2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV39Cargo_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV43GrupoCargo_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV47Cargo_UONomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV51Cargo_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtCargo_Nome_Titleformat = 2;
         edtCargo_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV42ddo_Cargo_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCargo_Nome_Internalname, "Title", edtCargo_Nome_Title);
         edtGrupoCargo_Codigo_Titleformat = 2;
         edtGrupoCargo_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Grupo", AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGrupoCargo_Codigo_Internalname, "Title", edtGrupoCargo_Codigo_Title);
         edtCargo_UONom_Titleformat = 2;
         edtCargo_UONom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Unidade Organizacional", AV50ddo_Cargo_UONomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCargo_UONom_Internalname, "Title", edtCargo_UONom_Title);
         chkCargo_Ativo_Titleformat = 2;
         chkCargo_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo?", AV53ddo_Cargo_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkCargo_Ativo_Internalname, "Title", chkCargo_Ativo.Title.Text);
         AV56GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56GridCurrentPage), 10, 0)));
         AV57GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57GridPageCount), 10, 0)));
         AV60WWGeral_CargoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV61WWGeral_CargoDS_2_Cargo_nome1 = AV16Cargo_Nome1;
         AV62WWGeral_CargoDS_3_Grupocargo_codigo1 = AV31GrupoCargo_Codigo1;
         AV63WWGeral_CargoDS_4_Cargo_uocod1 = AV34Cargo_UOCod1;
         AV64WWGeral_CargoDS_5_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV65WWGeral_CargoDS_6_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV66WWGeral_CargoDS_7_Cargo_nome2 = AV19Cargo_Nome2;
         AV67WWGeral_CargoDS_8_Grupocargo_codigo2 = AV32GrupoCargo_Codigo2;
         AV68WWGeral_CargoDS_9_Cargo_uocod2 = AV35Cargo_UOCod2;
         AV69WWGeral_CargoDS_10_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV70WWGeral_CargoDS_11_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV71WWGeral_CargoDS_12_Cargo_nome3 = AV22Cargo_Nome3;
         AV72WWGeral_CargoDS_13_Grupocargo_codigo3 = AV33GrupoCargo_Codigo3;
         AV73WWGeral_CargoDS_14_Cargo_uocod3 = AV36Cargo_UOCod3;
         AV74WWGeral_CargoDS_15_Tfcargo_nome = AV40TFCargo_Nome;
         AV75WWGeral_CargoDS_16_Tfcargo_nome_sel = AV41TFCargo_Nome_Sel;
         AV76WWGeral_CargoDS_17_Tfgrupocargo_codigo = AV44TFGrupoCargo_Codigo;
         AV77WWGeral_CargoDS_18_Tfgrupocargo_codigo_to = AV45TFGrupoCargo_Codigo_To;
         AV78WWGeral_CargoDS_19_Tfcargo_uonom = AV48TFCargo_UONom;
         AV79WWGeral_CargoDS_20_Tfcargo_uonom_sel = AV49TFCargo_UONom_Sel;
         AV80WWGeral_CargoDS_21_Tfcargo_ativo_sel = AV52TFCargo_Ativo_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV39Cargo_NomeTitleFilterData", AV39Cargo_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV43GrupoCargo_CodigoTitleFilterData", AV43GrupoCargo_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV47Cargo_UONomTitleFilterData", AV47Cargo_UONomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV51Cargo_AtivoTitleFilterData", AV51Cargo_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11CW2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV55PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV55PageToGo) ;
         }
      }

      protected void E12CW2( )
      {
         /* Ddo_cargo_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_cargo_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_cargo_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_cargo_nome_Internalname, "SortedStatus", Ddo_cargo_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_cargo_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_cargo_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_cargo_nome_Internalname, "SortedStatus", Ddo_cargo_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_cargo_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV40TFCargo_Nome = Ddo_cargo_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFCargo_Nome", AV40TFCargo_Nome);
            AV41TFCargo_Nome_Sel = Ddo_cargo_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFCargo_Nome_Sel", AV41TFCargo_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13CW2( )
      {
         /* Ddo_grupocargo_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_grupocargo_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_grupocargo_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_grupocargo_codigo_Internalname, "SortedStatus", Ddo_grupocargo_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_grupocargo_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_grupocargo_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_grupocargo_codigo_Internalname, "SortedStatus", Ddo_grupocargo_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_grupocargo_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV44TFGrupoCargo_Codigo = (int)(NumberUtil.Val( Ddo_grupocargo_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFGrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFGrupoCargo_Codigo), 6, 0)));
            AV45TFGrupoCargo_Codigo_To = (int)(NumberUtil.Val( Ddo_grupocargo_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFGrupoCargo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFGrupoCargo_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14CW2( )
      {
         /* Ddo_cargo_uonom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_cargo_uonom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_cargo_uonom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_cargo_uonom_Internalname, "SortedStatus", Ddo_cargo_uonom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_cargo_uonom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_cargo_uonom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_cargo_uonom_Internalname, "SortedStatus", Ddo_cargo_uonom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_cargo_uonom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV48TFCargo_UONom = Ddo_cargo_uonom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFCargo_UONom", AV48TFCargo_UONom);
            AV49TFCargo_UONom_Sel = Ddo_cargo_uonom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFCargo_UONom_Sel", AV49TFCargo_UONom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15CW2( )
      {
         /* Ddo_cargo_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_cargo_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_cargo_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_cargo_ativo_Internalname, "SortedStatus", Ddo_cargo_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_cargo_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_cargo_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_cargo_ativo_Internalname, "SortedStatus", Ddo_cargo_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_cargo_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV52TFCargo_Ativo_Sel = (short)(NumberUtil.Val( Ddo_cargo_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFCargo_Ativo_Sel", StringUtil.Str( (decimal)(AV52TFCargo_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E29CW2( )
      {
         /* Grid_Load Routine */
         AV25Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV25Update);
         AV81Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("geral_cargo.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A617Cargo_Codigo);
         AV26Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV26Delete);
         AV82Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("geral_cargo.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A617Cargo_Codigo);
         edtCargo_Nome_Link = formatLink("viewgeral_cargo.aspx") + "?" + UrlEncode("" +A617Cargo_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtCargo_UONom_Link = formatLink("viewgeral_unidadeorganizacional.aspx") + "?" + UrlEncode("" +A1002Cargo_UOCod) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 79;
         }
         sendrow_792( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_79_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(79, GridRow);
         }
      }

      protected void E16CW2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E22CW2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV17DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E17CW2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV24DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV24DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Cargo_Nome1, AV31GrupoCargo_Codigo1, AV34Cargo_UOCod1, AV18DynamicFiltersSelector2, AV19Cargo_Nome2, AV32GrupoCargo_Codigo2, AV35Cargo_UOCod2, AV21DynamicFiltersSelector3, AV22Cargo_Nome3, AV33GrupoCargo_Codigo3, AV36Cargo_UOCod3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV40TFCargo_Nome, AV41TFCargo_Nome_Sel, AV44TFGrupoCargo_Codigo, AV45TFGrupoCargo_Codigo_To, AV48TFCargo_UONom, AV49TFCargo_UONom_Sel, AV52TFCargo_Ativo_Sel, AV42ddo_Cargo_NomeTitleControlIdToReplace, AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace, AV50ddo_Cargo_UONomTitleControlIdToReplace, AV53ddo_Cargo_AtivoTitleControlIdToReplace, AV83Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A617Cargo_Codigo, A1002Cargo_UOCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavGrupocargo_codigo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV31GrupoCargo_Codigo1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavGrupocargo_codigo1_Internalname, "Values", dynavGrupocargo_codigo1.ToJavascriptSource());
         dynavCargo_uocod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV34Cargo_UOCod1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCargo_uocod1_Internalname, "Values", dynavCargo_uocod1.ToJavascriptSource());
         dynavGrupocargo_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV32GrupoCargo_Codigo2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavGrupocargo_codigo2_Internalname, "Values", dynavGrupocargo_codigo2.ToJavascriptSource());
         dynavCargo_uocod2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV35Cargo_UOCod2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCargo_uocod2_Internalname, "Values", dynavCargo_uocod2.ToJavascriptSource());
         dynavGrupocargo_codigo3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV33GrupoCargo_Codigo3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavGrupocargo_codigo3_Internalname, "Values", dynavGrupocargo_codigo3.ToJavascriptSource());
         dynavCargo_uocod3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV36Cargo_UOCod3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCargo_uocod3_Internalname, "Values", dynavCargo_uocod3.ToJavascriptSource());
      }

      protected void E23CW2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E24CW2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV20DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E18CW2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV17DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Cargo_Nome1, AV31GrupoCargo_Codigo1, AV34Cargo_UOCod1, AV18DynamicFiltersSelector2, AV19Cargo_Nome2, AV32GrupoCargo_Codigo2, AV35Cargo_UOCod2, AV21DynamicFiltersSelector3, AV22Cargo_Nome3, AV33GrupoCargo_Codigo3, AV36Cargo_UOCod3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV40TFCargo_Nome, AV41TFCargo_Nome_Sel, AV44TFGrupoCargo_Codigo, AV45TFGrupoCargo_Codigo_To, AV48TFCargo_UONom, AV49TFCargo_UONom_Sel, AV52TFCargo_Ativo_Sel, AV42ddo_Cargo_NomeTitleControlIdToReplace, AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace, AV50ddo_Cargo_UONomTitleControlIdToReplace, AV53ddo_Cargo_AtivoTitleControlIdToReplace, AV83Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A617Cargo_Codigo, A1002Cargo_UOCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavGrupocargo_codigo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV31GrupoCargo_Codigo1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavGrupocargo_codigo1_Internalname, "Values", dynavGrupocargo_codigo1.ToJavascriptSource());
         dynavCargo_uocod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV34Cargo_UOCod1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCargo_uocod1_Internalname, "Values", dynavCargo_uocod1.ToJavascriptSource());
         dynavGrupocargo_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV32GrupoCargo_Codigo2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavGrupocargo_codigo2_Internalname, "Values", dynavGrupocargo_codigo2.ToJavascriptSource());
         dynavCargo_uocod2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV35Cargo_UOCod2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCargo_uocod2_Internalname, "Values", dynavCargo_uocod2.ToJavascriptSource());
         dynavGrupocargo_codigo3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV33GrupoCargo_Codigo3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavGrupocargo_codigo3_Internalname, "Values", dynavGrupocargo_codigo3.ToJavascriptSource());
         dynavCargo_uocod3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV36Cargo_UOCod3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCargo_uocod3_Internalname, "Values", dynavCargo_uocod3.ToJavascriptSource());
      }

      protected void E25CW2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19CW2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Cargo_Nome1, AV31GrupoCargo_Codigo1, AV34Cargo_UOCod1, AV18DynamicFiltersSelector2, AV19Cargo_Nome2, AV32GrupoCargo_Codigo2, AV35Cargo_UOCod2, AV21DynamicFiltersSelector3, AV22Cargo_Nome3, AV33GrupoCargo_Codigo3, AV36Cargo_UOCod3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV40TFCargo_Nome, AV41TFCargo_Nome_Sel, AV44TFGrupoCargo_Codigo, AV45TFGrupoCargo_Codigo_To, AV48TFCargo_UONom, AV49TFCargo_UONom_Sel, AV52TFCargo_Ativo_Sel, AV42ddo_Cargo_NomeTitleControlIdToReplace, AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace, AV50ddo_Cargo_UONomTitleControlIdToReplace, AV53ddo_Cargo_AtivoTitleControlIdToReplace, AV83Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A617Cargo_Codigo, A1002Cargo_UOCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavGrupocargo_codigo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV31GrupoCargo_Codigo1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavGrupocargo_codigo1_Internalname, "Values", dynavGrupocargo_codigo1.ToJavascriptSource());
         dynavCargo_uocod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV34Cargo_UOCod1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCargo_uocod1_Internalname, "Values", dynavCargo_uocod1.ToJavascriptSource());
         dynavGrupocargo_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV32GrupoCargo_Codigo2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavGrupocargo_codigo2_Internalname, "Values", dynavGrupocargo_codigo2.ToJavascriptSource());
         dynavCargo_uocod2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV35Cargo_UOCod2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCargo_uocod2_Internalname, "Values", dynavCargo_uocod2.ToJavascriptSource());
         dynavGrupocargo_codigo3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV33GrupoCargo_Codigo3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavGrupocargo_codigo3_Internalname, "Values", dynavGrupocargo_codigo3.ToJavascriptSource());
         dynavCargo_uocod3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV36Cargo_UOCod3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCargo_uocod3_Internalname, "Values", dynavCargo_uocod3.ToJavascriptSource());
      }

      protected void E26CW2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E20CW2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         dynavGrupocargo_codigo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV31GrupoCargo_Codigo1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavGrupocargo_codigo1_Internalname, "Values", dynavGrupocargo_codigo1.ToJavascriptSource());
         dynavCargo_uocod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV34Cargo_UOCod1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCargo_uocod1_Internalname, "Values", dynavCargo_uocod1.ToJavascriptSource());
         dynavGrupocargo_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV32GrupoCargo_Codigo2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavGrupocargo_codigo2_Internalname, "Values", dynavGrupocargo_codigo2.ToJavascriptSource());
         dynavCargo_uocod2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV35Cargo_UOCod2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCargo_uocod2_Internalname, "Values", dynavCargo_uocod2.ToJavascriptSource());
         dynavGrupocargo_codigo3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV33GrupoCargo_Codigo3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavGrupocargo_codigo3_Internalname, "Values", dynavGrupocargo_codigo3.ToJavascriptSource());
         dynavCargo_uocod3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV36Cargo_UOCod3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCargo_uocod3_Internalname, "Values", dynavCargo_uocod3.ToJavascriptSource());
      }

      protected void E21CW2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("geral_cargo.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_cargo_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_cargo_nome_Internalname, "SortedStatus", Ddo_cargo_nome_Sortedstatus);
         Ddo_grupocargo_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_grupocargo_codigo_Internalname, "SortedStatus", Ddo_grupocargo_codigo_Sortedstatus);
         Ddo_cargo_uonom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_cargo_uonom_Internalname, "SortedStatus", Ddo_cargo_uonom_Sortedstatus);
         Ddo_cargo_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_cargo_ativo_Internalname, "SortedStatus", Ddo_cargo_ativo_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_cargo_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_cargo_nome_Internalname, "SortedStatus", Ddo_cargo_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_grupocargo_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_grupocargo_codigo_Internalname, "SortedStatus", Ddo_grupocargo_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_cargo_uonom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_cargo_uonom_Internalname, "SortedStatus", Ddo_cargo_uonom_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_cargo_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_cargo_ativo_Internalname, "SortedStatus", Ddo_cargo_ativo_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavCargo_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCargo_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCargo_nome1_Visible), 5, 0)));
         dynavGrupocargo_codigo1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavGrupocargo_codigo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavGrupocargo_codigo1.Visible), 5, 0)));
         dynavCargo_uocod1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCargo_uocod1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavCargo_uocod1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CARGO_NOME") == 0 )
         {
            edtavCargo_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCargo_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCargo_nome1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "GRUPOCARGO_CODIGO") == 0 )
         {
            dynavGrupocargo_codigo1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavGrupocargo_codigo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavGrupocargo_codigo1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CARGO_UOCOD") == 0 )
         {
            dynavCargo_uocod1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCargo_uocod1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavCargo_uocod1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavCargo_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCargo_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCargo_nome2_Visible), 5, 0)));
         dynavGrupocargo_codigo2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavGrupocargo_codigo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavGrupocargo_codigo2.Visible), 5, 0)));
         dynavCargo_uocod2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCargo_uocod2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavCargo_uocod2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "CARGO_NOME") == 0 )
         {
            edtavCargo_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCargo_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCargo_nome2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "GRUPOCARGO_CODIGO") == 0 )
         {
            dynavGrupocargo_codigo2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavGrupocargo_codigo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavGrupocargo_codigo2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "CARGO_UOCOD") == 0 )
         {
            dynavCargo_uocod2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCargo_uocod2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavCargo_uocod2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavCargo_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCargo_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCargo_nome3_Visible), 5, 0)));
         dynavGrupocargo_codigo3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavGrupocargo_codigo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavGrupocargo_codigo3.Visible), 5, 0)));
         dynavCargo_uocod3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCargo_uocod3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavCargo_uocod3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "CARGO_NOME") == 0 )
         {
            edtavCargo_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCargo_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCargo_nome3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "GRUPOCARGO_CODIGO") == 0 )
         {
            dynavGrupocargo_codigo3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavGrupocargo_codigo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavGrupocargo_codigo3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "CARGO_UOCOD") == 0 )
         {
            dynavCargo_uocod3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCargo_uocod3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavCargo_uocod3.Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV17DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         AV18DynamicFiltersSelector2 = "CARGO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         AV19Cargo_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Cargo_Nome2", AV19Cargo_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         AV21DynamicFiltersSelector3 = "CARGO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         AV22Cargo_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Cargo_Nome3", AV22Cargo_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV40TFCargo_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFCargo_Nome", AV40TFCargo_Nome);
         Ddo_cargo_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_cargo_nome_Internalname, "FilteredText_set", Ddo_cargo_nome_Filteredtext_set);
         AV41TFCargo_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFCargo_Nome_Sel", AV41TFCargo_Nome_Sel);
         Ddo_cargo_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_cargo_nome_Internalname, "SelectedValue_set", Ddo_cargo_nome_Selectedvalue_set);
         AV44TFGrupoCargo_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFGrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFGrupoCargo_Codigo), 6, 0)));
         Ddo_grupocargo_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_grupocargo_codigo_Internalname, "FilteredText_set", Ddo_grupocargo_codigo_Filteredtext_set);
         AV45TFGrupoCargo_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFGrupoCargo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFGrupoCargo_Codigo_To), 6, 0)));
         Ddo_grupocargo_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_grupocargo_codigo_Internalname, "FilteredTextTo_set", Ddo_grupocargo_codigo_Filteredtextto_set);
         AV48TFCargo_UONom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFCargo_UONom", AV48TFCargo_UONom);
         Ddo_cargo_uonom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_cargo_uonom_Internalname, "FilteredText_set", Ddo_cargo_uonom_Filteredtext_set);
         AV49TFCargo_UONom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFCargo_UONom_Sel", AV49TFCargo_UONom_Sel);
         Ddo_cargo_uonom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_cargo_uonom_Internalname, "SelectedValue_set", Ddo_cargo_uonom_Selectedvalue_set);
         AV52TFCargo_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFCargo_Ativo_Sel", StringUtil.Str( (decimal)(AV52TFCargo_Ativo_Sel), 1, 0));
         Ddo_cargo_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_cargo_ativo_Internalname, "SelectedValue_set", Ddo_cargo_ativo_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "CARGO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16Cargo_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Cargo_Nome1", AV16Cargo_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get(AV83Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV83Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV27Session.Get(AV83Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV84GXV1 = 1;
         while ( AV84GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV84GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCARGO_NOME") == 0 )
            {
               AV40TFCargo_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFCargo_Nome", AV40TFCargo_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFCargo_Nome)) )
               {
                  Ddo_cargo_nome_Filteredtext_set = AV40TFCargo_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_cargo_nome_Internalname, "FilteredText_set", Ddo_cargo_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCARGO_NOME_SEL") == 0 )
            {
               AV41TFCargo_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFCargo_Nome_Sel", AV41TFCargo_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFCargo_Nome_Sel)) )
               {
                  Ddo_cargo_nome_Selectedvalue_set = AV41TFCargo_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_cargo_nome_Internalname, "SelectedValue_set", Ddo_cargo_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFGRUPOCARGO_CODIGO") == 0 )
            {
               AV44TFGrupoCargo_Codigo = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFGrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFGrupoCargo_Codigo), 6, 0)));
               AV45TFGrupoCargo_Codigo_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFGrupoCargo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFGrupoCargo_Codigo_To), 6, 0)));
               if ( ! (0==AV44TFGrupoCargo_Codigo) )
               {
                  Ddo_grupocargo_codigo_Filteredtext_set = StringUtil.Str( (decimal)(AV44TFGrupoCargo_Codigo), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_grupocargo_codigo_Internalname, "FilteredText_set", Ddo_grupocargo_codigo_Filteredtext_set);
               }
               if ( ! (0==AV45TFGrupoCargo_Codigo_To) )
               {
                  Ddo_grupocargo_codigo_Filteredtextto_set = StringUtil.Str( (decimal)(AV45TFGrupoCargo_Codigo_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_grupocargo_codigo_Internalname, "FilteredTextTo_set", Ddo_grupocargo_codigo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCARGO_UONOM") == 0 )
            {
               AV48TFCargo_UONom = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFCargo_UONom", AV48TFCargo_UONom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFCargo_UONom)) )
               {
                  Ddo_cargo_uonom_Filteredtext_set = AV48TFCargo_UONom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_cargo_uonom_Internalname, "FilteredText_set", Ddo_cargo_uonom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCARGO_UONOM_SEL") == 0 )
            {
               AV49TFCargo_UONom_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFCargo_UONom_Sel", AV49TFCargo_UONom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFCargo_UONom_Sel)) )
               {
                  Ddo_cargo_uonom_Selectedvalue_set = AV49TFCargo_UONom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_cargo_uonom_Internalname, "SelectedValue_set", Ddo_cargo_uonom_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCARGO_ATIVO_SEL") == 0 )
            {
               AV52TFCargo_Ativo_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFCargo_Ativo_Sel", StringUtil.Str( (decimal)(AV52TFCargo_Ativo_Sel), 1, 0));
               if ( ! (0==AV52TFCargo_Ativo_Sel) )
               {
                  Ddo_cargo_ativo_Selectedvalue_set = StringUtil.Str( (decimal)(AV52TFCargo_Ativo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_cargo_ativo_Internalname, "SelectedValue_set", Ddo_cargo_ativo_Selectedvalue_set);
               }
            }
            AV84GXV1 = (int)(AV84GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CARGO_NOME") == 0 )
            {
               AV16Cargo_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Cargo_Nome1", AV16Cargo_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "GRUPOCARGO_CODIGO") == 0 )
            {
               AV31GrupoCargo_Codigo1 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31GrupoCargo_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31GrupoCargo_Codigo1), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CARGO_UOCOD") == 0 )
            {
               AV34Cargo_UOCod1 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Cargo_UOCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Cargo_UOCod1), 6, 0)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV17DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV18DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "CARGO_NOME") == 0 )
               {
                  AV19Cargo_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Cargo_Nome2", AV19Cargo_Nome2);
               }
               else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "GRUPOCARGO_CODIGO") == 0 )
               {
                  AV32GrupoCargo_Codigo2 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32GrupoCargo_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32GrupoCargo_Codigo2), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "CARGO_UOCOD") == 0 )
               {
                  AV35Cargo_UOCod2 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Cargo_UOCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35Cargo_UOCod2), 6, 0)));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV20DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV21DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "CARGO_NOME") == 0 )
                  {
                     AV22Cargo_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Cargo_Nome3", AV22Cargo_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "GRUPOCARGO_CODIGO") == 0 )
                  {
                     AV33GrupoCargo_Codigo3 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33GrupoCargo_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33GrupoCargo_Codigo3), 6, 0)));
                  }
                  else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "CARGO_UOCOD") == 0 )
                  {
                     AV36Cargo_UOCod3 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Cargo_UOCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Cargo_UOCod3), 6, 0)));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV23DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV27Session.Get(AV83Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFCargo_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCARGO_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV40TFCargo_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFCargo_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCARGO_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV41TFCargo_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV44TFGrupoCargo_Codigo) && (0==AV45TFGrupoCargo_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFGRUPOCARGO_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV44TFGrupoCargo_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV45TFGrupoCargo_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFCargo_UONom)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCARGO_UONOM";
            AV11GridStateFilterValue.gxTpr_Value = AV48TFCargo_UONom;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFCargo_UONom_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCARGO_UONOM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV49TFCargo_UONom_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV52TFCargo_Ativo_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCARGO_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV52TFCargo_Ativo_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV83Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV24DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CARGO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV16Cargo_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV16Cargo_Nome1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "GRUPOCARGO_CODIGO") == 0 ) && ! (0==AV31GrupoCargo_Codigo1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV31GrupoCargo_Codigo1), 6, 0);
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CARGO_UOCOD") == 0 ) && ! (0==AV34Cargo_UOCod1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV34Cargo_UOCod1), 6, 0);
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV17DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV18DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "CARGO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Cargo_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV19Cargo_Nome2;
            }
            else if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "GRUPOCARGO_CODIGO") == 0 ) && ! (0==AV32GrupoCargo_Codigo2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV32GrupoCargo_Codigo2), 6, 0);
            }
            else if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "CARGO_UOCOD") == 0 ) && ! (0==AV35Cargo_UOCod2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV35Cargo_UOCod2), 6, 0);
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "CARGO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Cargo_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV22Cargo_Nome3;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "GRUPOCARGO_CODIGO") == 0 ) && ! (0==AV33GrupoCargo_Codigo3) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV33GrupoCargo_Codigo3), 6, 0);
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "CARGO_UOCOD") == 0 ) && ! (0==AV36Cargo_UOCod3) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV36Cargo_UOCod3), 6, 0);
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV83Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Geral_Cargo";
         AV27Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_CW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_CW2( true) ;
         }
         else
         {
            wb_table2_8_CW2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_CW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_73_CW2( true) ;
         }
         else
         {
            wb_table3_73_CW2( false) ;
         }
         return  ;
      }

      protected void wb_table3_73_CW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_CW2e( true) ;
         }
         else
         {
            wb_table1_2_CW2e( false) ;
         }
      }

      protected void wb_table3_73_CW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_76_CW2( true) ;
         }
         else
         {
            wb_table4_76_CW2( false) ;
         }
         return  ;
      }

      protected void wb_table4_76_CW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_73_CW2e( true) ;
         }
         else
         {
            wb_table3_73_CW2e( false) ;
         }
      }

      protected void wb_table4_76_CW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"79\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Cargo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtCargo_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtCargo_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtCargo_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtGrupoCargo_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtGrupoCargo_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtGrupoCargo_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtCargo_UONom_Titleformat == 0 )
               {
                  context.SendWebValue( edtCargo_UONom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtCargo_UONom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkCargo_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkCargo_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkCargo_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV25Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV26Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A617Cargo_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A618Cargo_Nome);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtCargo_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCargo_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtCargo_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A615GrupoCargo_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtGrupoCargo_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtGrupoCargo_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1003Cargo_UONom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtCargo_UONom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCargo_UONom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtCargo_UONom_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A628Cargo_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkCargo_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkCargo_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 79 )
         {
            wbEnd = 0;
            nRC_GXsfl_79 = (short)(nGXsfl_79_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_76_CW2e( true) ;
         }
         else
         {
            wb_table4_76_CW2e( false) ;
         }
      }

      protected void wb_table2_8_CW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblGeral_cargotitle_Internalname, "Cargos", "", "", lblGeral_cargotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWGeral_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_13_CW2( true) ;
         }
         else
         {
            wb_table5_13_CW2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_CW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWGeral_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWGeral_Cargo.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWGeral_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_CW2( true) ;
         }
         else
         {
            wb_table6_23_CW2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_CW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_CW2e( true) ;
         }
         else
         {
            wb_table2_8_CW2e( false) ;
         }
      }

      protected void wb_table6_23_CW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGeral_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_CW2( true) ;
         }
         else
         {
            wb_table7_28_CW2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_CW2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWGeral_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_CW2e( true) ;
         }
         else
         {
            wb_table6_23_CW2e( false) ;
         }
      }

      protected void wb_table7_28_CW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGeral_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWGeral_Cargo.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGeral_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCargo_nome1_Internalname, AV16Cargo_Nome1, StringUtil.RTrim( context.localUtil.Format( AV16Cargo_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCargo_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavCargo_nome1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGeral_Cargo.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavGrupocargo_codigo1, dynavGrupocargo_codigo1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV31GrupoCargo_Codigo1), 6, 0)), 1, dynavGrupocargo_codigo1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavGrupocargo_codigo1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "", true, "HLP_WWGeral_Cargo.htm");
            dynavGrupocargo_codigo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV31GrupoCargo_Codigo1), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavGrupocargo_codigo1_Internalname, "Values", (String)(dynavGrupocargo_codigo1.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavCargo_uocod1, dynavCargo_uocod1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV34Cargo_UOCod1), 6, 0)), 1, dynavCargo_uocod1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavCargo_uocod1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "", true, "HLP_WWGeral_Cargo.htm");
            dynavCargo_uocod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV34Cargo_UOCod1), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCargo_uocod1_Internalname, "Values", (String)(dynavCargo_uocod1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGeral_Cargo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGeral_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGeral_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV18DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", "", true, "HLP_WWGeral_Cargo.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGeral_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCargo_nome2_Internalname, AV19Cargo_Nome2, StringUtil.RTrim( context.localUtil.Format( AV19Cargo_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCargo_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavCargo_nome2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGeral_Cargo.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavGrupocargo_codigo2, dynavGrupocargo_codigo2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV32GrupoCargo_Codigo2), 6, 0)), 1, dynavGrupocargo_codigo2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavGrupocargo_codigo2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", "", true, "HLP_WWGeral_Cargo.htm");
            dynavGrupocargo_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV32GrupoCargo_Codigo2), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavGrupocargo_codigo2_Internalname, "Values", (String)(dynavGrupocargo_codigo2.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavCargo_uocod2, dynavCargo_uocod2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV35Cargo_UOCod2), 6, 0)), 1, dynavCargo_uocod2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavCargo_uocod2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"", "", true, "HLP_WWGeral_Cargo.htm");
            dynavCargo_uocod2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV35Cargo_UOCod2), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCargo_uocod2_Internalname, "Values", (String)(dynavCargo_uocod2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGeral_Cargo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGeral_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGeral_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", "", true, "HLP_WWGeral_Cargo.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGeral_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCargo_nome3_Internalname, AV22Cargo_Nome3, StringUtil.RTrim( context.localUtil.Format( AV22Cargo_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,65);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCargo_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavCargo_nome3_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGeral_Cargo.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavGrupocargo_codigo3, dynavGrupocargo_codigo3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV33GrupoCargo_Codigo3), 6, 0)), 1, dynavGrupocargo_codigo3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavGrupocargo_codigo3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,66);\"", "", true, "HLP_WWGeral_Cargo.htm");
            dynavGrupocargo_codigo3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV33GrupoCargo_Codigo3), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavGrupocargo_codigo3_Internalname, "Values", (String)(dynavGrupocargo_codigo3.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavCargo_uocod3, dynavCargo_uocod3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV36Cargo_UOCod3), 6, 0)), 1, dynavCargo_uocod3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavCargo_uocod3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "", true, "HLP_WWGeral_Cargo.htm");
            dynavCargo_uocod3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV36Cargo_UOCod3), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCargo_uocod3_Internalname, "Values", (String)(dynavCargo_uocod3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGeral_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_CW2e( true) ;
         }
         else
         {
            wb_table7_28_CW2e( false) ;
         }
      }

      protected void wb_table5_13_CW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGeral_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_CW2e( true) ;
         }
         else
         {
            wb_table5_13_CW2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PACW2( ) ;
         WSCW2( ) ;
         WECW2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299375136");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwgeral_cargo.js", "?20205299375137");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_792( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_79_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_79_idx;
         edtCargo_Codigo_Internalname = "CARGO_CODIGO_"+sGXsfl_79_idx;
         edtCargo_Nome_Internalname = "CARGO_NOME_"+sGXsfl_79_idx;
         edtGrupoCargo_Codigo_Internalname = "GRUPOCARGO_CODIGO_"+sGXsfl_79_idx;
         edtCargo_UONom_Internalname = "CARGO_UONOM_"+sGXsfl_79_idx;
         chkCargo_Ativo_Internalname = "CARGO_ATIVO_"+sGXsfl_79_idx;
      }

      protected void SubsflControlProps_fel_792( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_79_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_79_fel_idx;
         edtCargo_Codigo_Internalname = "CARGO_CODIGO_"+sGXsfl_79_fel_idx;
         edtCargo_Nome_Internalname = "CARGO_NOME_"+sGXsfl_79_fel_idx;
         edtGrupoCargo_Codigo_Internalname = "GRUPOCARGO_CODIGO_"+sGXsfl_79_fel_idx;
         edtCargo_UONom_Internalname = "CARGO_UONOM_"+sGXsfl_79_fel_idx;
         chkCargo_Ativo_Internalname = "CARGO_ATIVO_"+sGXsfl_79_fel_idx;
      }

      protected void sendrow_792( )
      {
         SubsflControlProps_792( ) ;
         WBCW0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_79_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_79_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_79_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV25Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV25Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV81Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV25Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV25Update)) ? AV81Update_GXI : context.PathToRelativeUrl( AV25Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV25Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV26Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV26Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV82Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV26Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV26Delete)) ? AV82Delete_GXI : context.PathToRelativeUrl( AV26Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV26Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCargo_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A617Cargo_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A617Cargo_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtCargo_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)79,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCargo_Nome_Internalname,(String)A618Cargo_Nome,StringUtil.RTrim( context.localUtil.Format( A618Cargo_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtCargo_Nome_Link,(String)"",(String)"",(String)"",(String)edtCargo_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)80,(short)0,(short)0,(short)79,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtGrupoCargo_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A615GrupoCargo_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A615GrupoCargo_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtGrupoCargo_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)79,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCargo_UONom_Internalname,StringUtil.RTrim( A1003Cargo_UONom),StringUtil.RTrim( context.localUtil.Format( A1003Cargo_UONom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtCargo_UONom_Link,(String)"",(String)"",(String)"",(String)edtCargo_UONom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)79,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkCargo_Ativo_Internalname,StringUtil.BoolToStr( A628Cargo_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, "gxhash_CARGO_CODIGO"+"_"+sGXsfl_79_idx, GetSecureSignedToken( sGXsfl_79_idx, context.localUtil.Format( (decimal)(A617Cargo_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CARGO_NOME"+"_"+sGXsfl_79_idx, GetSecureSignedToken( sGXsfl_79_idx, StringUtil.RTrim( context.localUtil.Format( A618Cargo_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_GRUPOCARGO_CODIGO"+"_"+sGXsfl_79_idx, GetSecureSignedToken( sGXsfl_79_idx, context.localUtil.Format( (decimal)(A615GrupoCargo_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CARGO_ATIVO"+"_"+sGXsfl_79_idx, GetSecureSignedToken( sGXsfl_79_idx, A628Cargo_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_79_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_79_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_79_idx+1));
            sGXsfl_79_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_79_idx), 4, 0)), 4, "0");
            SubsflControlProps_792( ) ;
         }
         /* End function sendrow_792 */
      }

      protected void init_default_properties( )
      {
         lblGeral_cargotitle_Internalname = "GERAL_CARGOTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavCargo_nome1_Internalname = "vCARGO_NOME1";
         dynavGrupocargo_codigo1_Internalname = "vGRUPOCARGO_CODIGO1";
         dynavCargo_uocod1_Internalname = "vCARGO_UOCOD1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavCargo_nome2_Internalname = "vCARGO_NOME2";
         dynavGrupocargo_codigo2_Internalname = "vGRUPOCARGO_CODIGO2";
         dynavCargo_uocod2_Internalname = "vCARGO_UOCOD2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavCargo_nome3_Internalname = "vCARGO_NOME3";
         dynavGrupocargo_codigo3_Internalname = "vGRUPOCARGO_CODIGO3";
         dynavCargo_uocod3_Internalname = "vCARGO_UOCOD3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtCargo_Codigo_Internalname = "CARGO_CODIGO";
         edtCargo_Nome_Internalname = "CARGO_NOME";
         edtGrupoCargo_Codigo_Internalname = "GRUPOCARGO_CODIGO";
         edtCargo_UONom_Internalname = "CARGO_UONOM";
         chkCargo_Ativo_Internalname = "CARGO_ATIVO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfcargo_nome_Internalname = "vTFCARGO_NOME";
         edtavTfcargo_nome_sel_Internalname = "vTFCARGO_NOME_SEL";
         edtavTfgrupocargo_codigo_Internalname = "vTFGRUPOCARGO_CODIGO";
         edtavTfgrupocargo_codigo_to_Internalname = "vTFGRUPOCARGO_CODIGO_TO";
         edtavTfcargo_uonom_Internalname = "vTFCARGO_UONOM";
         edtavTfcargo_uonom_sel_Internalname = "vTFCARGO_UONOM_SEL";
         edtavTfcargo_ativo_sel_Internalname = "vTFCARGO_ATIVO_SEL";
         Ddo_cargo_nome_Internalname = "DDO_CARGO_NOME";
         edtavDdo_cargo_nometitlecontrolidtoreplace_Internalname = "vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_grupocargo_codigo_Internalname = "DDO_GRUPOCARGO_CODIGO";
         edtavDdo_grupocargo_codigotitlecontrolidtoreplace_Internalname = "vDDO_GRUPOCARGO_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_cargo_uonom_Internalname = "DDO_CARGO_UONOM";
         edtavDdo_cargo_uonomtitlecontrolidtoreplace_Internalname = "vDDO_CARGO_UONOMTITLECONTROLIDTOREPLACE";
         Ddo_cargo_ativo_Internalname = "DDO_CARGO_ATIVO";
         edtavDdo_cargo_ativotitlecontrolidtoreplace_Internalname = "vDDO_CARGO_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtCargo_UONom_Jsonclick = "";
         edtGrupoCargo_Codigo_Jsonclick = "";
         edtCargo_Nome_Jsonclick = "";
         edtCargo_Codigo_Jsonclick = "";
         dynavCargo_uocod3_Jsonclick = "";
         dynavGrupocargo_codigo3_Jsonclick = "";
         edtavCargo_nome3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         dynavCargo_uocod2_Jsonclick = "";
         dynavGrupocargo_codigo2_Jsonclick = "";
         edtavCargo_nome2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         dynavCargo_uocod1_Jsonclick = "";
         dynavGrupocargo_codigo1_Jsonclick = "";
         edtavCargo_nome1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtCargo_UONom_Link = "";
         edtCargo_Nome_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         chkCargo_Ativo_Titleformat = 0;
         edtCargo_UONom_Titleformat = 0;
         edtGrupoCargo_Codigo_Titleformat = 0;
         edtCargo_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         dynavCargo_uocod3.Visible = 1;
         dynavGrupocargo_codigo3.Visible = 1;
         edtavCargo_nome3_Visible = 1;
         dynavCargo_uocod2.Visible = 1;
         dynavGrupocargo_codigo2.Visible = 1;
         edtavCargo_nome2_Visible = 1;
         dynavCargo_uocod1.Visible = 1;
         dynavGrupocargo_codigo1.Visible = 1;
         edtavCargo_nome1_Visible = 1;
         chkCargo_Ativo.Title.Text = "Ativo?";
         edtCargo_UONom_Title = "Unidade Organizacional";
         edtGrupoCargo_Codigo_Title = "Grupo";
         edtCargo_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkCargo_Ativo.Caption = "";
         edtavDdo_cargo_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_cargo_uonomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_grupocargo_codigotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_cargo_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfcargo_ativo_sel_Jsonclick = "";
         edtavTfcargo_ativo_sel_Visible = 1;
         edtavTfcargo_uonom_sel_Jsonclick = "";
         edtavTfcargo_uonom_sel_Visible = 1;
         edtavTfcargo_uonom_Jsonclick = "";
         edtavTfcargo_uonom_Visible = 1;
         edtavTfgrupocargo_codigo_to_Jsonclick = "";
         edtavTfgrupocargo_codigo_to_Visible = 1;
         edtavTfgrupocargo_codigo_Jsonclick = "";
         edtavTfgrupocargo_codigo_Visible = 1;
         edtavTfcargo_nome_sel_Jsonclick = "";
         edtavTfcargo_nome_sel_Visible = 1;
         edtavTfcargo_nome_Jsonclick = "";
         edtavTfcargo_nome_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_cargo_ativo_Searchbuttontext = "Pesquisar";
         Ddo_cargo_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_cargo_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_cargo_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_cargo_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_cargo_ativo_Datalisttype = "FixedValues";
         Ddo_cargo_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_cargo_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_cargo_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_cargo_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_cargo_ativo_Titlecontrolidtoreplace = "";
         Ddo_cargo_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_cargo_ativo_Cls = "ColumnSettings";
         Ddo_cargo_ativo_Tooltip = "Op��es";
         Ddo_cargo_ativo_Caption = "";
         Ddo_cargo_uonom_Searchbuttontext = "Pesquisar";
         Ddo_cargo_uonom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_cargo_uonom_Cleanfilter = "Limpar pesquisa";
         Ddo_cargo_uonom_Loadingdata = "Carregando dados...";
         Ddo_cargo_uonom_Sortdsc = "Ordenar de Z � A";
         Ddo_cargo_uonom_Sortasc = "Ordenar de A � Z";
         Ddo_cargo_uonom_Datalistupdateminimumcharacters = 0;
         Ddo_cargo_uonom_Datalistproc = "GetWWGeral_CargoFilterData";
         Ddo_cargo_uonom_Datalisttype = "Dynamic";
         Ddo_cargo_uonom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_cargo_uonom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_cargo_uonom_Filtertype = "Character";
         Ddo_cargo_uonom_Includefilter = Convert.ToBoolean( -1);
         Ddo_cargo_uonom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_cargo_uonom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_cargo_uonom_Titlecontrolidtoreplace = "";
         Ddo_cargo_uonom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_cargo_uonom_Cls = "ColumnSettings";
         Ddo_cargo_uonom_Tooltip = "Op��es";
         Ddo_cargo_uonom_Caption = "";
         Ddo_grupocargo_codigo_Searchbuttontext = "Pesquisar";
         Ddo_grupocargo_codigo_Rangefilterto = "At�";
         Ddo_grupocargo_codigo_Rangefilterfrom = "Desde";
         Ddo_grupocargo_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_grupocargo_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_grupocargo_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_grupocargo_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_grupocargo_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_grupocargo_codigo_Filtertype = "Numeric";
         Ddo_grupocargo_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_grupocargo_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_grupocargo_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_grupocargo_codigo_Titlecontrolidtoreplace = "";
         Ddo_grupocargo_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_grupocargo_codigo_Cls = "ColumnSettings";
         Ddo_grupocargo_codigo_Tooltip = "Op��es";
         Ddo_grupocargo_codigo_Caption = "";
         Ddo_cargo_nome_Searchbuttontext = "Pesquisar";
         Ddo_cargo_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_cargo_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_cargo_nome_Loadingdata = "Carregando dados...";
         Ddo_cargo_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_cargo_nome_Sortasc = "Ordenar de A � Z";
         Ddo_cargo_nome_Datalistupdateminimumcharacters = 0;
         Ddo_cargo_nome_Datalistproc = "GetWWGeral_CargoFilterData";
         Ddo_cargo_nome_Datalisttype = "Dynamic";
         Ddo_cargo_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_cargo_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_cargo_nome_Filtertype = "Character";
         Ddo_cargo_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_cargo_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_cargo_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_cargo_nome_Titlecontrolidtoreplace = "";
         Ddo_cargo_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_cargo_nome_Cls = "ColumnSettings";
         Ddo_cargo_nome_Tooltip = "Op��es";
         Ddo_cargo_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Cargos";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1002Cargo_UOCod',fld:'CARGO_UOCOD',pic:'ZZZZZ9',nv:0},{av:'AV42ddo_Cargo_NomeTitleControlIdToReplace',fld:'vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace',fld:'vDDO_GRUPOCARGO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Cargo_UONomTitleControlIdToReplace',fld:'vDDO_CARGO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Cargo_AtivoTitleControlIdToReplace',fld:'vDDO_CARGO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV31GrupoCargo_Codigo1',fld:'vGRUPOCARGO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV34Cargo_UOCod1',fld:'vCARGO_UOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV32GrupoCargo_Codigo2',fld:'vGRUPOCARGO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV35Cargo_UOCod2',fld:'vCARGO_UOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'AV33GrupoCargo_Codigo3',fld:'vGRUPOCARGO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV36Cargo_UOCod3',fld:'vCARGO_UOCOD3',pic:'ZZZZZ9',nv:0},{av:'AV40TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV41TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFGrupoCargo_Codigo',fld:'vTFGRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45TFGrupoCargo_Codigo_To',fld:'vTFGRUPOCARGO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFCargo_UONom',fld:'vTFCARGO_UONOM',pic:'@!',nv:''},{av:'AV49TFCargo_UONom_Sel',fld:'vTFCARGO_UONOM_SEL',pic:'@!',nv:''},{av:'AV52TFCargo_Ativo_Sel',fld:'vTFCARGO_ATIVO_SEL',pic:'9',nv:0},{av:'AV83Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV39Cargo_NomeTitleFilterData',fld:'vCARGO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV43GrupoCargo_CodigoTitleFilterData',fld:'vGRUPOCARGO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV47Cargo_UONomTitleFilterData',fld:'vCARGO_UONOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV51Cargo_AtivoTitleFilterData',fld:'vCARGO_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'edtCargo_Nome_Titleformat',ctrl:'CARGO_NOME',prop:'Titleformat'},{av:'edtCargo_Nome_Title',ctrl:'CARGO_NOME',prop:'Title'},{av:'edtGrupoCargo_Codigo_Titleformat',ctrl:'GRUPOCARGO_CODIGO',prop:'Titleformat'},{av:'edtGrupoCargo_Codigo_Title',ctrl:'GRUPOCARGO_CODIGO',prop:'Title'},{av:'edtCargo_UONom_Titleformat',ctrl:'CARGO_UONOM',prop:'Titleformat'},{av:'edtCargo_UONom_Title',ctrl:'CARGO_UONOM',prop:'Title'},{av:'chkCargo_Ativo_Titleformat',ctrl:'CARGO_ATIVO',prop:'Titleformat'},{av:'chkCargo_Ativo.Title.Text',ctrl:'CARGO_ATIVO',prop:'Title'},{av:'AV56GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV57GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11CW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV31GrupoCargo_Codigo1',fld:'vGRUPOCARGO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV34Cargo_UOCod1',fld:'vCARGO_UOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV32GrupoCargo_Codigo2',fld:'vGRUPOCARGO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV35Cargo_UOCod2',fld:'vCARGO_UOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'AV33GrupoCargo_Codigo3',fld:'vGRUPOCARGO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV36Cargo_UOCod3',fld:'vCARGO_UOCOD3',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV41TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFGrupoCargo_Codigo',fld:'vTFGRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45TFGrupoCargo_Codigo_To',fld:'vTFGRUPOCARGO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFCargo_UONom',fld:'vTFCARGO_UONOM',pic:'@!',nv:''},{av:'AV49TFCargo_UONom_Sel',fld:'vTFCARGO_UONOM_SEL',pic:'@!',nv:''},{av:'AV52TFCargo_Ativo_Sel',fld:'vTFCARGO_ATIVO_SEL',pic:'9',nv:0},{av:'AV42ddo_Cargo_NomeTitleControlIdToReplace',fld:'vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace',fld:'vDDO_GRUPOCARGO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Cargo_UONomTitleControlIdToReplace',fld:'vDDO_CARGO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Cargo_AtivoTitleControlIdToReplace',fld:'vDDO_CARGO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1002Cargo_UOCod',fld:'CARGO_UOCOD',pic:'ZZZZZ9',nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CARGO_NOME.ONOPTIONCLICKED","{handler:'E12CW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV31GrupoCargo_Codigo1',fld:'vGRUPOCARGO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV34Cargo_UOCod1',fld:'vCARGO_UOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV32GrupoCargo_Codigo2',fld:'vGRUPOCARGO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV35Cargo_UOCod2',fld:'vCARGO_UOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'AV33GrupoCargo_Codigo3',fld:'vGRUPOCARGO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV36Cargo_UOCod3',fld:'vCARGO_UOCOD3',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV41TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFGrupoCargo_Codigo',fld:'vTFGRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45TFGrupoCargo_Codigo_To',fld:'vTFGRUPOCARGO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFCargo_UONom',fld:'vTFCARGO_UONOM',pic:'@!',nv:''},{av:'AV49TFCargo_UONom_Sel',fld:'vTFCARGO_UONOM_SEL',pic:'@!',nv:''},{av:'AV52TFCargo_Ativo_Sel',fld:'vTFCARGO_ATIVO_SEL',pic:'9',nv:0},{av:'AV42ddo_Cargo_NomeTitleControlIdToReplace',fld:'vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace',fld:'vDDO_GRUPOCARGO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Cargo_UONomTitleControlIdToReplace',fld:'vDDO_CARGO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Cargo_AtivoTitleControlIdToReplace',fld:'vDDO_CARGO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1002Cargo_UOCod',fld:'CARGO_UOCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_cargo_nome_Activeeventkey',ctrl:'DDO_CARGO_NOME',prop:'ActiveEventKey'},{av:'Ddo_cargo_nome_Filteredtext_get',ctrl:'DDO_CARGO_NOME',prop:'FilteredText_get'},{av:'Ddo_cargo_nome_Selectedvalue_get',ctrl:'DDO_CARGO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_cargo_nome_Sortedstatus',ctrl:'DDO_CARGO_NOME',prop:'SortedStatus'},{av:'AV40TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV41TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_grupocargo_codigo_Sortedstatus',ctrl:'DDO_GRUPOCARGO_CODIGO',prop:'SortedStatus'},{av:'Ddo_cargo_uonom_Sortedstatus',ctrl:'DDO_CARGO_UONOM',prop:'SortedStatus'},{av:'Ddo_cargo_ativo_Sortedstatus',ctrl:'DDO_CARGO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_GRUPOCARGO_CODIGO.ONOPTIONCLICKED","{handler:'E13CW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV31GrupoCargo_Codigo1',fld:'vGRUPOCARGO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV34Cargo_UOCod1',fld:'vCARGO_UOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV32GrupoCargo_Codigo2',fld:'vGRUPOCARGO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV35Cargo_UOCod2',fld:'vCARGO_UOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'AV33GrupoCargo_Codigo3',fld:'vGRUPOCARGO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV36Cargo_UOCod3',fld:'vCARGO_UOCOD3',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV41TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFGrupoCargo_Codigo',fld:'vTFGRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45TFGrupoCargo_Codigo_To',fld:'vTFGRUPOCARGO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFCargo_UONom',fld:'vTFCARGO_UONOM',pic:'@!',nv:''},{av:'AV49TFCargo_UONom_Sel',fld:'vTFCARGO_UONOM_SEL',pic:'@!',nv:''},{av:'AV52TFCargo_Ativo_Sel',fld:'vTFCARGO_ATIVO_SEL',pic:'9',nv:0},{av:'AV42ddo_Cargo_NomeTitleControlIdToReplace',fld:'vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace',fld:'vDDO_GRUPOCARGO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Cargo_UONomTitleControlIdToReplace',fld:'vDDO_CARGO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Cargo_AtivoTitleControlIdToReplace',fld:'vDDO_CARGO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1002Cargo_UOCod',fld:'CARGO_UOCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_grupocargo_codigo_Activeeventkey',ctrl:'DDO_GRUPOCARGO_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_grupocargo_codigo_Filteredtext_get',ctrl:'DDO_GRUPOCARGO_CODIGO',prop:'FilteredText_get'},{av:'Ddo_grupocargo_codigo_Filteredtextto_get',ctrl:'DDO_GRUPOCARGO_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_grupocargo_codigo_Sortedstatus',ctrl:'DDO_GRUPOCARGO_CODIGO',prop:'SortedStatus'},{av:'AV44TFGrupoCargo_Codigo',fld:'vTFGRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45TFGrupoCargo_Codigo_To',fld:'vTFGRUPOCARGO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_cargo_nome_Sortedstatus',ctrl:'DDO_CARGO_NOME',prop:'SortedStatus'},{av:'Ddo_cargo_uonom_Sortedstatus',ctrl:'DDO_CARGO_UONOM',prop:'SortedStatus'},{av:'Ddo_cargo_ativo_Sortedstatus',ctrl:'DDO_CARGO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CARGO_UONOM.ONOPTIONCLICKED","{handler:'E14CW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV31GrupoCargo_Codigo1',fld:'vGRUPOCARGO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV34Cargo_UOCod1',fld:'vCARGO_UOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV32GrupoCargo_Codigo2',fld:'vGRUPOCARGO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV35Cargo_UOCod2',fld:'vCARGO_UOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'AV33GrupoCargo_Codigo3',fld:'vGRUPOCARGO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV36Cargo_UOCod3',fld:'vCARGO_UOCOD3',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV41TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFGrupoCargo_Codigo',fld:'vTFGRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45TFGrupoCargo_Codigo_To',fld:'vTFGRUPOCARGO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFCargo_UONom',fld:'vTFCARGO_UONOM',pic:'@!',nv:''},{av:'AV49TFCargo_UONom_Sel',fld:'vTFCARGO_UONOM_SEL',pic:'@!',nv:''},{av:'AV52TFCargo_Ativo_Sel',fld:'vTFCARGO_ATIVO_SEL',pic:'9',nv:0},{av:'AV42ddo_Cargo_NomeTitleControlIdToReplace',fld:'vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace',fld:'vDDO_GRUPOCARGO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Cargo_UONomTitleControlIdToReplace',fld:'vDDO_CARGO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Cargo_AtivoTitleControlIdToReplace',fld:'vDDO_CARGO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1002Cargo_UOCod',fld:'CARGO_UOCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_cargo_uonom_Activeeventkey',ctrl:'DDO_CARGO_UONOM',prop:'ActiveEventKey'},{av:'Ddo_cargo_uonom_Filteredtext_get',ctrl:'DDO_CARGO_UONOM',prop:'FilteredText_get'},{av:'Ddo_cargo_uonom_Selectedvalue_get',ctrl:'DDO_CARGO_UONOM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_cargo_uonom_Sortedstatus',ctrl:'DDO_CARGO_UONOM',prop:'SortedStatus'},{av:'AV48TFCargo_UONom',fld:'vTFCARGO_UONOM',pic:'@!',nv:''},{av:'AV49TFCargo_UONom_Sel',fld:'vTFCARGO_UONOM_SEL',pic:'@!',nv:''},{av:'Ddo_cargo_nome_Sortedstatus',ctrl:'DDO_CARGO_NOME',prop:'SortedStatus'},{av:'Ddo_grupocargo_codigo_Sortedstatus',ctrl:'DDO_GRUPOCARGO_CODIGO',prop:'SortedStatus'},{av:'Ddo_cargo_ativo_Sortedstatus',ctrl:'DDO_CARGO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CARGO_ATIVO.ONOPTIONCLICKED","{handler:'E15CW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV31GrupoCargo_Codigo1',fld:'vGRUPOCARGO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV34Cargo_UOCod1',fld:'vCARGO_UOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV32GrupoCargo_Codigo2',fld:'vGRUPOCARGO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV35Cargo_UOCod2',fld:'vCARGO_UOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'AV33GrupoCargo_Codigo3',fld:'vGRUPOCARGO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV36Cargo_UOCod3',fld:'vCARGO_UOCOD3',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV41TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFGrupoCargo_Codigo',fld:'vTFGRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45TFGrupoCargo_Codigo_To',fld:'vTFGRUPOCARGO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFCargo_UONom',fld:'vTFCARGO_UONOM',pic:'@!',nv:''},{av:'AV49TFCargo_UONom_Sel',fld:'vTFCARGO_UONOM_SEL',pic:'@!',nv:''},{av:'AV52TFCargo_Ativo_Sel',fld:'vTFCARGO_ATIVO_SEL',pic:'9',nv:0},{av:'AV42ddo_Cargo_NomeTitleControlIdToReplace',fld:'vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace',fld:'vDDO_GRUPOCARGO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Cargo_UONomTitleControlIdToReplace',fld:'vDDO_CARGO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Cargo_AtivoTitleControlIdToReplace',fld:'vDDO_CARGO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1002Cargo_UOCod',fld:'CARGO_UOCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_cargo_ativo_Activeeventkey',ctrl:'DDO_CARGO_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_cargo_ativo_Selectedvalue_get',ctrl:'DDO_CARGO_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_cargo_ativo_Sortedstatus',ctrl:'DDO_CARGO_ATIVO',prop:'SortedStatus'},{av:'AV52TFCargo_Ativo_Sel',fld:'vTFCARGO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_cargo_nome_Sortedstatus',ctrl:'DDO_CARGO_NOME',prop:'SortedStatus'},{av:'Ddo_grupocargo_codigo_Sortedstatus',ctrl:'DDO_GRUPOCARGO_CODIGO',prop:'SortedStatus'},{av:'Ddo_cargo_uonom_Sortedstatus',ctrl:'DDO_CARGO_UONOM',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E29CW2',iparms:[{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1002Cargo_UOCod',fld:'CARGO_UOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV25Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV26Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtCargo_Nome_Link',ctrl:'CARGO_NOME',prop:'Link'},{av:'edtCargo_UONom_Link',ctrl:'CARGO_UONOM',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E16CW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV31GrupoCargo_Codigo1',fld:'vGRUPOCARGO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV34Cargo_UOCod1',fld:'vCARGO_UOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV32GrupoCargo_Codigo2',fld:'vGRUPOCARGO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV35Cargo_UOCod2',fld:'vCARGO_UOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'AV33GrupoCargo_Codigo3',fld:'vGRUPOCARGO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV36Cargo_UOCod3',fld:'vCARGO_UOCOD3',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV41TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFGrupoCargo_Codigo',fld:'vTFGRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45TFGrupoCargo_Codigo_To',fld:'vTFGRUPOCARGO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFCargo_UONom',fld:'vTFCARGO_UONOM',pic:'@!',nv:''},{av:'AV49TFCargo_UONom_Sel',fld:'vTFCARGO_UONOM_SEL',pic:'@!',nv:''},{av:'AV52TFCargo_Ativo_Sel',fld:'vTFCARGO_ATIVO_SEL',pic:'9',nv:0},{av:'AV42ddo_Cargo_NomeTitleControlIdToReplace',fld:'vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace',fld:'vDDO_GRUPOCARGO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Cargo_UONomTitleControlIdToReplace',fld:'vDDO_CARGO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Cargo_AtivoTitleControlIdToReplace',fld:'vDDO_CARGO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1002Cargo_UOCod',fld:'CARGO_UOCOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E22CW2',iparms:[],oparms:[{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E17CW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV31GrupoCargo_Codigo1',fld:'vGRUPOCARGO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV34Cargo_UOCod1',fld:'vCARGO_UOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV32GrupoCargo_Codigo2',fld:'vGRUPOCARGO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV35Cargo_UOCod2',fld:'vCARGO_UOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'AV33GrupoCargo_Codigo3',fld:'vGRUPOCARGO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV36Cargo_UOCod3',fld:'vCARGO_UOCOD3',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV41TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFGrupoCargo_Codigo',fld:'vTFGRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45TFGrupoCargo_Codigo_To',fld:'vTFGRUPOCARGO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFCargo_UONom',fld:'vTFCARGO_UONOM',pic:'@!',nv:''},{av:'AV49TFCargo_UONom_Sel',fld:'vTFCARGO_UONOM_SEL',pic:'@!',nv:''},{av:'AV52TFCargo_Ativo_Sel',fld:'vTFCARGO_ATIVO_SEL',pic:'9',nv:0},{av:'AV42ddo_Cargo_NomeTitleControlIdToReplace',fld:'vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace',fld:'vDDO_GRUPOCARGO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Cargo_UONomTitleControlIdToReplace',fld:'vDDO_CARGO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Cargo_AtivoTitleControlIdToReplace',fld:'vDDO_CARGO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1002Cargo_UOCod',fld:'CARGO_UOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV31GrupoCargo_Codigo1',fld:'vGRUPOCARGO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV34Cargo_UOCod1',fld:'vCARGO_UOCOD1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV32GrupoCargo_Codigo2',fld:'vGRUPOCARGO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV35Cargo_UOCod2',fld:'vCARGO_UOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV33GrupoCargo_Codigo3',fld:'vGRUPOCARGO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV36Cargo_UOCod3',fld:'vCARGO_UOCOD3',pic:'ZZZZZ9',nv:0},{av:'edtavCargo_nome2_Visible',ctrl:'vCARGO_NOME2',prop:'Visible'},{av:'dynavGrupocargo_codigo2'},{av:'dynavCargo_uocod2'},{av:'edtavCargo_nome3_Visible',ctrl:'vCARGO_NOME3',prop:'Visible'},{av:'dynavGrupocargo_codigo3'},{av:'dynavCargo_uocod3'},{av:'edtavCargo_nome1_Visible',ctrl:'vCARGO_NOME1',prop:'Visible'},{av:'dynavGrupocargo_codigo1'},{av:'dynavCargo_uocod1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E23CW2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavCargo_nome1_Visible',ctrl:'vCARGO_NOME1',prop:'Visible'},{av:'dynavGrupocargo_codigo1'},{av:'dynavCargo_uocod1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E24CW2',iparms:[],oparms:[{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E18CW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV31GrupoCargo_Codigo1',fld:'vGRUPOCARGO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV34Cargo_UOCod1',fld:'vCARGO_UOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV32GrupoCargo_Codigo2',fld:'vGRUPOCARGO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV35Cargo_UOCod2',fld:'vCARGO_UOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'AV33GrupoCargo_Codigo3',fld:'vGRUPOCARGO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV36Cargo_UOCod3',fld:'vCARGO_UOCOD3',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV41TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFGrupoCargo_Codigo',fld:'vTFGRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45TFGrupoCargo_Codigo_To',fld:'vTFGRUPOCARGO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFCargo_UONom',fld:'vTFCARGO_UONOM',pic:'@!',nv:''},{av:'AV49TFCargo_UONom_Sel',fld:'vTFCARGO_UONOM_SEL',pic:'@!',nv:''},{av:'AV52TFCargo_Ativo_Sel',fld:'vTFCARGO_ATIVO_SEL',pic:'9',nv:0},{av:'AV42ddo_Cargo_NomeTitleControlIdToReplace',fld:'vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace',fld:'vDDO_GRUPOCARGO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Cargo_UONomTitleControlIdToReplace',fld:'vDDO_CARGO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Cargo_AtivoTitleControlIdToReplace',fld:'vDDO_CARGO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1002Cargo_UOCod',fld:'CARGO_UOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV31GrupoCargo_Codigo1',fld:'vGRUPOCARGO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV34Cargo_UOCod1',fld:'vCARGO_UOCOD1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV32GrupoCargo_Codigo2',fld:'vGRUPOCARGO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV35Cargo_UOCod2',fld:'vCARGO_UOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV33GrupoCargo_Codigo3',fld:'vGRUPOCARGO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV36Cargo_UOCod3',fld:'vCARGO_UOCOD3',pic:'ZZZZZ9',nv:0},{av:'edtavCargo_nome2_Visible',ctrl:'vCARGO_NOME2',prop:'Visible'},{av:'dynavGrupocargo_codigo2'},{av:'dynavCargo_uocod2'},{av:'edtavCargo_nome3_Visible',ctrl:'vCARGO_NOME3',prop:'Visible'},{av:'dynavGrupocargo_codigo3'},{av:'dynavCargo_uocod3'},{av:'edtavCargo_nome1_Visible',ctrl:'vCARGO_NOME1',prop:'Visible'},{av:'dynavGrupocargo_codigo1'},{av:'dynavCargo_uocod1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E25CW2',iparms:[{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavCargo_nome2_Visible',ctrl:'vCARGO_NOME2',prop:'Visible'},{av:'dynavGrupocargo_codigo2'},{av:'dynavCargo_uocod2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E19CW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV31GrupoCargo_Codigo1',fld:'vGRUPOCARGO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV34Cargo_UOCod1',fld:'vCARGO_UOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV32GrupoCargo_Codigo2',fld:'vGRUPOCARGO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV35Cargo_UOCod2',fld:'vCARGO_UOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'AV33GrupoCargo_Codigo3',fld:'vGRUPOCARGO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV36Cargo_UOCod3',fld:'vCARGO_UOCOD3',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV41TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFGrupoCargo_Codigo',fld:'vTFGRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45TFGrupoCargo_Codigo_To',fld:'vTFGRUPOCARGO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFCargo_UONom',fld:'vTFCARGO_UONOM',pic:'@!',nv:''},{av:'AV49TFCargo_UONom_Sel',fld:'vTFCARGO_UONOM_SEL',pic:'@!',nv:''},{av:'AV52TFCargo_Ativo_Sel',fld:'vTFCARGO_ATIVO_SEL',pic:'9',nv:0},{av:'AV42ddo_Cargo_NomeTitleControlIdToReplace',fld:'vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace',fld:'vDDO_GRUPOCARGO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Cargo_UONomTitleControlIdToReplace',fld:'vDDO_CARGO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Cargo_AtivoTitleControlIdToReplace',fld:'vDDO_CARGO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1002Cargo_UOCod',fld:'CARGO_UOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV31GrupoCargo_Codigo1',fld:'vGRUPOCARGO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV34Cargo_UOCod1',fld:'vCARGO_UOCOD1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV32GrupoCargo_Codigo2',fld:'vGRUPOCARGO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV35Cargo_UOCod2',fld:'vCARGO_UOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV33GrupoCargo_Codigo3',fld:'vGRUPOCARGO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV36Cargo_UOCod3',fld:'vCARGO_UOCOD3',pic:'ZZZZZ9',nv:0},{av:'edtavCargo_nome2_Visible',ctrl:'vCARGO_NOME2',prop:'Visible'},{av:'dynavGrupocargo_codigo2'},{av:'dynavCargo_uocod2'},{av:'edtavCargo_nome3_Visible',ctrl:'vCARGO_NOME3',prop:'Visible'},{av:'dynavGrupocargo_codigo3'},{av:'dynavCargo_uocod3'},{av:'edtavCargo_nome1_Visible',ctrl:'vCARGO_NOME1',prop:'Visible'},{av:'dynavGrupocargo_codigo1'},{av:'dynavCargo_uocod1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E26CW2',iparms:[{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavCargo_nome3_Visible',ctrl:'vCARGO_NOME3',prop:'Visible'},{av:'dynavGrupocargo_codigo3'},{av:'dynavCargo_uocod3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E20CW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV31GrupoCargo_Codigo1',fld:'vGRUPOCARGO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV34Cargo_UOCod1',fld:'vCARGO_UOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV32GrupoCargo_Codigo2',fld:'vGRUPOCARGO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV35Cargo_UOCod2',fld:'vCARGO_UOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'AV33GrupoCargo_Codigo3',fld:'vGRUPOCARGO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV36Cargo_UOCod3',fld:'vCARGO_UOCOD3',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV41TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFGrupoCargo_Codigo',fld:'vTFGRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45TFGrupoCargo_Codigo_To',fld:'vTFGRUPOCARGO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFCargo_UONom',fld:'vTFCARGO_UONOM',pic:'@!',nv:''},{av:'AV49TFCargo_UONom_Sel',fld:'vTFCARGO_UONOM_SEL',pic:'@!',nv:''},{av:'AV52TFCargo_Ativo_Sel',fld:'vTFCARGO_ATIVO_SEL',pic:'9',nv:0},{av:'AV42ddo_Cargo_NomeTitleControlIdToReplace',fld:'vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace',fld:'vDDO_GRUPOCARGO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Cargo_UONomTitleControlIdToReplace',fld:'vDDO_CARGO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Cargo_AtivoTitleControlIdToReplace',fld:'vDDO_CARGO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1002Cargo_UOCod',fld:'CARGO_UOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV40TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'Ddo_cargo_nome_Filteredtext_set',ctrl:'DDO_CARGO_NOME',prop:'FilteredText_set'},{av:'AV41TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_cargo_nome_Selectedvalue_set',ctrl:'DDO_CARGO_NOME',prop:'SelectedValue_set'},{av:'AV44TFGrupoCargo_Codigo',fld:'vTFGRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_grupocargo_codigo_Filteredtext_set',ctrl:'DDO_GRUPOCARGO_CODIGO',prop:'FilteredText_set'},{av:'AV45TFGrupoCargo_Codigo_To',fld:'vTFGRUPOCARGO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_grupocargo_codigo_Filteredtextto_set',ctrl:'DDO_GRUPOCARGO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV48TFCargo_UONom',fld:'vTFCARGO_UONOM',pic:'@!',nv:''},{av:'Ddo_cargo_uonom_Filteredtext_set',ctrl:'DDO_CARGO_UONOM',prop:'FilteredText_set'},{av:'AV49TFCargo_UONom_Sel',fld:'vTFCARGO_UONOM_SEL',pic:'@!',nv:''},{av:'Ddo_cargo_uonom_Selectedvalue_set',ctrl:'DDO_CARGO_UONOM',prop:'SelectedValue_set'},{av:'AV52TFCargo_Ativo_Sel',fld:'vTFCARGO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_cargo_ativo_Selectedvalue_set',ctrl:'DDO_CARGO_ATIVO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavCargo_nome1_Visible',ctrl:'vCARGO_NOME1',prop:'Visible'},{av:'dynavGrupocargo_codigo1'},{av:'dynavCargo_uocod1'},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV31GrupoCargo_Codigo1',fld:'vGRUPOCARGO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV34Cargo_UOCod1',fld:'vCARGO_UOCOD1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV32GrupoCargo_Codigo2',fld:'vGRUPOCARGO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV35Cargo_UOCod2',fld:'vCARGO_UOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV33GrupoCargo_Codigo3',fld:'vGRUPOCARGO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV36Cargo_UOCod3',fld:'vCARGO_UOCOD3',pic:'ZZZZZ9',nv:0},{av:'edtavCargo_nome2_Visible',ctrl:'vCARGO_NOME2',prop:'Visible'},{av:'dynavGrupocargo_codigo2'},{av:'dynavCargo_uocod2'},{av:'edtavCargo_nome3_Visible',ctrl:'vCARGO_NOME3',prop:'Visible'},{av:'dynavGrupocargo_codigo3'},{av:'dynavCargo_uocod3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E21CW2',iparms:[{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_cargo_nome_Activeeventkey = "";
         Ddo_cargo_nome_Filteredtext_get = "";
         Ddo_cargo_nome_Selectedvalue_get = "";
         Ddo_grupocargo_codigo_Activeeventkey = "";
         Ddo_grupocargo_codigo_Filteredtext_get = "";
         Ddo_grupocargo_codigo_Filteredtextto_get = "";
         Ddo_cargo_uonom_Activeeventkey = "";
         Ddo_cargo_uonom_Filteredtext_get = "";
         Ddo_cargo_uonom_Selectedvalue_get = "";
         Ddo_cargo_ativo_Activeeventkey = "";
         Ddo_cargo_ativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV16Cargo_Nome1 = "";
         AV18DynamicFiltersSelector2 = "";
         AV19Cargo_Nome2 = "";
         AV21DynamicFiltersSelector3 = "";
         AV22Cargo_Nome3 = "";
         AV40TFCargo_Nome = "";
         AV41TFCargo_Nome_Sel = "";
         AV48TFCargo_UONom = "";
         AV49TFCargo_UONom_Sel = "";
         AV42ddo_Cargo_NomeTitleControlIdToReplace = "";
         AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace = "";
         AV50ddo_Cargo_UONomTitleControlIdToReplace = "";
         AV53ddo_Cargo_AtivoTitleControlIdToReplace = "";
         AV83Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV54DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV39Cargo_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV43GrupoCargo_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV47Cargo_UONomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV51Cargo_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_cargo_nome_Filteredtext_set = "";
         Ddo_cargo_nome_Selectedvalue_set = "";
         Ddo_cargo_nome_Sortedstatus = "";
         Ddo_grupocargo_codigo_Filteredtext_set = "";
         Ddo_grupocargo_codigo_Filteredtextto_set = "";
         Ddo_grupocargo_codigo_Sortedstatus = "";
         Ddo_cargo_uonom_Filteredtext_set = "";
         Ddo_cargo_uonom_Selectedvalue_set = "";
         Ddo_cargo_uonom_Sortedstatus = "";
         Ddo_cargo_ativo_Selectedvalue_set = "";
         Ddo_cargo_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV25Update = "";
         AV81Update_GXI = "";
         AV26Delete = "";
         AV82Delete_GXI = "";
         A618Cargo_Nome = "";
         A1003Cargo_UONom = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00CW2_A615GrupoCargo_Codigo = new int[1] ;
         H00CW2_A616GrupoCargo_Nome = new String[] {""} ;
         H00CW2_A626GrupoCargo_Ativo = new bool[] {false} ;
         H00CW3_A611UnidadeOrganizacional_Codigo = new int[1] ;
         H00CW3_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         H00CW3_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         H00CW4_A615GrupoCargo_Codigo = new int[1] ;
         H00CW4_A616GrupoCargo_Nome = new String[] {""} ;
         H00CW4_A626GrupoCargo_Ativo = new bool[] {false} ;
         H00CW5_A611UnidadeOrganizacional_Codigo = new int[1] ;
         H00CW5_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         H00CW5_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         H00CW6_A615GrupoCargo_Codigo = new int[1] ;
         H00CW6_A616GrupoCargo_Nome = new String[] {""} ;
         H00CW6_A626GrupoCargo_Ativo = new bool[] {false} ;
         H00CW7_A611UnidadeOrganizacional_Codigo = new int[1] ;
         H00CW7_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         H00CW7_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         GridContainer = new GXWebGrid( context);
         lV61WWGeral_CargoDS_2_Cargo_nome1 = "";
         lV66WWGeral_CargoDS_7_Cargo_nome2 = "";
         lV71WWGeral_CargoDS_12_Cargo_nome3 = "";
         lV74WWGeral_CargoDS_15_Tfcargo_nome = "";
         lV78WWGeral_CargoDS_19_Tfcargo_uonom = "";
         AV60WWGeral_CargoDS_1_Dynamicfiltersselector1 = "";
         AV61WWGeral_CargoDS_2_Cargo_nome1 = "";
         AV65WWGeral_CargoDS_6_Dynamicfiltersselector2 = "";
         AV66WWGeral_CargoDS_7_Cargo_nome2 = "";
         AV70WWGeral_CargoDS_11_Dynamicfiltersselector3 = "";
         AV71WWGeral_CargoDS_12_Cargo_nome3 = "";
         AV75WWGeral_CargoDS_16_Tfcargo_nome_sel = "";
         AV74WWGeral_CargoDS_15_Tfcargo_nome = "";
         AV79WWGeral_CargoDS_20_Tfcargo_uonom_sel = "";
         AV78WWGeral_CargoDS_19_Tfcargo_uonom = "";
         H00CW8_A1002Cargo_UOCod = new int[1] ;
         H00CW8_n1002Cargo_UOCod = new bool[] {false} ;
         H00CW8_A628Cargo_Ativo = new bool[] {false} ;
         H00CW8_A1003Cargo_UONom = new String[] {""} ;
         H00CW8_n1003Cargo_UONom = new bool[] {false} ;
         H00CW8_A615GrupoCargo_Codigo = new int[1] ;
         H00CW8_A618Cargo_Nome = new String[] {""} ;
         H00CW8_A617Cargo_Codigo = new int[1] ;
         H00CW9_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV27Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblGeral_cargotitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwgeral_cargo__default(),
            new Object[][] {
                new Object[] {
               H00CW2_A615GrupoCargo_Codigo, H00CW2_A616GrupoCargo_Nome, H00CW2_A626GrupoCargo_Ativo
               }
               , new Object[] {
               H00CW3_A611UnidadeOrganizacional_Codigo, H00CW3_A612UnidadeOrganizacional_Nome, H00CW3_A629UnidadeOrganizacional_Ativo
               }
               , new Object[] {
               H00CW4_A615GrupoCargo_Codigo, H00CW4_A616GrupoCargo_Nome, H00CW4_A626GrupoCargo_Ativo
               }
               , new Object[] {
               H00CW5_A611UnidadeOrganizacional_Codigo, H00CW5_A612UnidadeOrganizacional_Nome, H00CW5_A629UnidadeOrganizacional_Ativo
               }
               , new Object[] {
               H00CW6_A615GrupoCargo_Codigo, H00CW6_A616GrupoCargo_Nome, H00CW6_A626GrupoCargo_Ativo
               }
               , new Object[] {
               H00CW7_A611UnidadeOrganizacional_Codigo, H00CW7_A612UnidadeOrganizacional_Nome, H00CW7_A629UnidadeOrganizacional_Ativo
               }
               , new Object[] {
               H00CW8_A1002Cargo_UOCod, H00CW8_n1002Cargo_UOCod, H00CW8_A628Cargo_Ativo, H00CW8_A1003Cargo_UONom, H00CW8_n1003Cargo_UONom, H00CW8_A615GrupoCargo_Codigo, H00CW8_A618Cargo_Nome, H00CW8_A617Cargo_Codigo
               }
               , new Object[] {
               H00CW9_AGRID_nRecordCount
               }
            }
         );
         AV83Pgmname = "WWGeral_Cargo";
         /* GeneXus formulas. */
         AV83Pgmname = "WWGeral_Cargo";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_79 ;
      private short nGXsfl_79_idx=1 ;
      private short AV13OrderedBy ;
      private short AV52TFCargo_Ativo_Sel ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_79_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV80WWGeral_CargoDS_21_Tfcargo_ativo_sel ;
      private short edtCargo_Nome_Titleformat ;
      private short edtGrupoCargo_Codigo_Titleformat ;
      private short edtCargo_UONom_Titleformat ;
      private short chkCargo_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV31GrupoCargo_Codigo1 ;
      private int AV34Cargo_UOCod1 ;
      private int AV32GrupoCargo_Codigo2 ;
      private int AV35Cargo_UOCod2 ;
      private int AV33GrupoCargo_Codigo3 ;
      private int AV36Cargo_UOCod3 ;
      private int AV44TFGrupoCargo_Codigo ;
      private int AV45TFGrupoCargo_Codigo_To ;
      private int A617Cargo_Codigo ;
      private int A1002Cargo_UOCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_cargo_nome_Datalistupdateminimumcharacters ;
      private int Ddo_cargo_uonom_Datalistupdateminimumcharacters ;
      private int edtavTfcargo_nome_Visible ;
      private int edtavTfcargo_nome_sel_Visible ;
      private int edtavTfgrupocargo_codigo_Visible ;
      private int edtavTfgrupocargo_codigo_to_Visible ;
      private int edtavTfcargo_uonom_Visible ;
      private int edtavTfcargo_uonom_sel_Visible ;
      private int edtavTfcargo_ativo_sel_Visible ;
      private int edtavDdo_cargo_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_grupocargo_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_cargo_uonomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_cargo_ativotitlecontrolidtoreplace_Visible ;
      private int A615GrupoCargo_Codigo ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV62WWGeral_CargoDS_3_Grupocargo_codigo1 ;
      private int AV63WWGeral_CargoDS_4_Cargo_uocod1 ;
      private int AV67WWGeral_CargoDS_8_Grupocargo_codigo2 ;
      private int AV68WWGeral_CargoDS_9_Cargo_uocod2 ;
      private int AV72WWGeral_CargoDS_13_Grupocargo_codigo3 ;
      private int AV73WWGeral_CargoDS_14_Cargo_uocod3 ;
      private int AV76WWGeral_CargoDS_17_Tfgrupocargo_codigo ;
      private int AV77WWGeral_CargoDS_18_Tfgrupocargo_codigo_to ;
      private int edtavOrdereddsc_Visible ;
      private int AV55PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavCargo_nome1_Visible ;
      private int edtavCargo_nome2_Visible ;
      private int edtavCargo_nome3_Visible ;
      private int AV84GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV56GridCurrentPage ;
      private long AV57GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_cargo_nome_Activeeventkey ;
      private String Ddo_cargo_nome_Filteredtext_get ;
      private String Ddo_cargo_nome_Selectedvalue_get ;
      private String Ddo_grupocargo_codigo_Activeeventkey ;
      private String Ddo_grupocargo_codigo_Filteredtext_get ;
      private String Ddo_grupocargo_codigo_Filteredtextto_get ;
      private String Ddo_cargo_uonom_Activeeventkey ;
      private String Ddo_cargo_uonom_Filteredtext_get ;
      private String Ddo_cargo_uonom_Selectedvalue_get ;
      private String Ddo_cargo_ativo_Activeeventkey ;
      private String Ddo_cargo_ativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_79_idx="0001" ;
      private String AV48TFCargo_UONom ;
      private String AV49TFCargo_UONom_Sel ;
      private String AV83Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_cargo_nome_Caption ;
      private String Ddo_cargo_nome_Tooltip ;
      private String Ddo_cargo_nome_Cls ;
      private String Ddo_cargo_nome_Filteredtext_set ;
      private String Ddo_cargo_nome_Selectedvalue_set ;
      private String Ddo_cargo_nome_Dropdownoptionstype ;
      private String Ddo_cargo_nome_Titlecontrolidtoreplace ;
      private String Ddo_cargo_nome_Sortedstatus ;
      private String Ddo_cargo_nome_Filtertype ;
      private String Ddo_cargo_nome_Datalisttype ;
      private String Ddo_cargo_nome_Datalistproc ;
      private String Ddo_cargo_nome_Sortasc ;
      private String Ddo_cargo_nome_Sortdsc ;
      private String Ddo_cargo_nome_Loadingdata ;
      private String Ddo_cargo_nome_Cleanfilter ;
      private String Ddo_cargo_nome_Noresultsfound ;
      private String Ddo_cargo_nome_Searchbuttontext ;
      private String Ddo_grupocargo_codigo_Caption ;
      private String Ddo_grupocargo_codigo_Tooltip ;
      private String Ddo_grupocargo_codigo_Cls ;
      private String Ddo_grupocargo_codigo_Filteredtext_set ;
      private String Ddo_grupocargo_codigo_Filteredtextto_set ;
      private String Ddo_grupocargo_codigo_Dropdownoptionstype ;
      private String Ddo_grupocargo_codigo_Titlecontrolidtoreplace ;
      private String Ddo_grupocargo_codigo_Sortedstatus ;
      private String Ddo_grupocargo_codigo_Filtertype ;
      private String Ddo_grupocargo_codigo_Sortasc ;
      private String Ddo_grupocargo_codigo_Sortdsc ;
      private String Ddo_grupocargo_codigo_Cleanfilter ;
      private String Ddo_grupocargo_codigo_Rangefilterfrom ;
      private String Ddo_grupocargo_codigo_Rangefilterto ;
      private String Ddo_grupocargo_codigo_Searchbuttontext ;
      private String Ddo_cargo_uonom_Caption ;
      private String Ddo_cargo_uonom_Tooltip ;
      private String Ddo_cargo_uonom_Cls ;
      private String Ddo_cargo_uonom_Filteredtext_set ;
      private String Ddo_cargo_uonom_Selectedvalue_set ;
      private String Ddo_cargo_uonom_Dropdownoptionstype ;
      private String Ddo_cargo_uonom_Titlecontrolidtoreplace ;
      private String Ddo_cargo_uonom_Sortedstatus ;
      private String Ddo_cargo_uonom_Filtertype ;
      private String Ddo_cargo_uonom_Datalisttype ;
      private String Ddo_cargo_uonom_Datalistproc ;
      private String Ddo_cargo_uonom_Sortasc ;
      private String Ddo_cargo_uonom_Sortdsc ;
      private String Ddo_cargo_uonom_Loadingdata ;
      private String Ddo_cargo_uonom_Cleanfilter ;
      private String Ddo_cargo_uonom_Noresultsfound ;
      private String Ddo_cargo_uonom_Searchbuttontext ;
      private String Ddo_cargo_ativo_Caption ;
      private String Ddo_cargo_ativo_Tooltip ;
      private String Ddo_cargo_ativo_Cls ;
      private String Ddo_cargo_ativo_Selectedvalue_set ;
      private String Ddo_cargo_ativo_Dropdownoptionstype ;
      private String Ddo_cargo_ativo_Titlecontrolidtoreplace ;
      private String Ddo_cargo_ativo_Sortedstatus ;
      private String Ddo_cargo_ativo_Datalisttype ;
      private String Ddo_cargo_ativo_Datalistfixedvalues ;
      private String Ddo_cargo_ativo_Sortasc ;
      private String Ddo_cargo_ativo_Sortdsc ;
      private String Ddo_cargo_ativo_Cleanfilter ;
      private String Ddo_cargo_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfcargo_nome_Internalname ;
      private String edtavTfcargo_nome_Jsonclick ;
      private String edtavTfcargo_nome_sel_Internalname ;
      private String edtavTfcargo_nome_sel_Jsonclick ;
      private String edtavTfgrupocargo_codigo_Internalname ;
      private String edtavTfgrupocargo_codigo_Jsonclick ;
      private String edtavTfgrupocargo_codigo_to_Internalname ;
      private String edtavTfgrupocargo_codigo_to_Jsonclick ;
      private String edtavTfcargo_uonom_Internalname ;
      private String edtavTfcargo_uonom_Jsonclick ;
      private String edtavTfcargo_uonom_sel_Internalname ;
      private String edtavTfcargo_uonom_sel_Jsonclick ;
      private String edtavTfcargo_ativo_sel_Internalname ;
      private String edtavTfcargo_ativo_sel_Jsonclick ;
      private String edtavDdo_cargo_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_grupocargo_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_cargo_uonomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_cargo_ativotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtCargo_Codigo_Internalname ;
      private String edtCargo_Nome_Internalname ;
      private String edtGrupoCargo_Codigo_Internalname ;
      private String A1003Cargo_UONom ;
      private String edtCargo_UONom_Internalname ;
      private String chkCargo_Ativo_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String lV78WWGeral_CargoDS_19_Tfcargo_uonom ;
      private String AV79WWGeral_CargoDS_20_Tfcargo_uonom_sel ;
      private String AV78WWGeral_CargoDS_19_Tfcargo_uonom ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavCargo_nome1_Internalname ;
      private String dynavGrupocargo_codigo1_Internalname ;
      private String dynavCargo_uocod1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavCargo_nome2_Internalname ;
      private String dynavGrupocargo_codigo2_Internalname ;
      private String dynavCargo_uocod2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavCargo_nome3_Internalname ;
      private String dynavGrupocargo_codigo3_Internalname ;
      private String dynavCargo_uocod3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_cargo_nome_Internalname ;
      private String Ddo_grupocargo_codigo_Internalname ;
      private String Ddo_cargo_uonom_Internalname ;
      private String Ddo_cargo_ativo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtCargo_Nome_Title ;
      private String edtGrupoCargo_Codigo_Title ;
      private String edtCargo_UONom_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtCargo_Nome_Link ;
      private String edtCargo_UONom_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblGeral_cargotitle_Internalname ;
      private String lblGeral_cargotitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavCargo_nome1_Jsonclick ;
      private String dynavGrupocargo_codigo1_Jsonclick ;
      private String dynavCargo_uocod1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavCargo_nome2_Jsonclick ;
      private String dynavGrupocargo_codigo2_Jsonclick ;
      private String dynavCargo_uocod2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavCargo_nome3_Jsonclick ;
      private String dynavGrupocargo_codigo3_Jsonclick ;
      private String dynavCargo_uocod3_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_79_fel_idx="0001" ;
      private String ROClassString ;
      private String edtCargo_Codigo_Jsonclick ;
      private String edtCargo_Nome_Jsonclick ;
      private String edtGrupoCargo_Codigo_Jsonclick ;
      private String edtCargo_UONom_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV17DynamicFiltersEnabled2 ;
      private bool AV20DynamicFiltersEnabled3 ;
      private bool AV24DynamicFiltersIgnoreFirst ;
      private bool AV23DynamicFiltersRemoving ;
      private bool n1002Cargo_UOCod ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_cargo_nome_Includesortasc ;
      private bool Ddo_cargo_nome_Includesortdsc ;
      private bool Ddo_cargo_nome_Includefilter ;
      private bool Ddo_cargo_nome_Filterisrange ;
      private bool Ddo_cargo_nome_Includedatalist ;
      private bool Ddo_grupocargo_codigo_Includesortasc ;
      private bool Ddo_grupocargo_codigo_Includesortdsc ;
      private bool Ddo_grupocargo_codigo_Includefilter ;
      private bool Ddo_grupocargo_codigo_Filterisrange ;
      private bool Ddo_grupocargo_codigo_Includedatalist ;
      private bool Ddo_cargo_uonom_Includesortasc ;
      private bool Ddo_cargo_uonom_Includesortdsc ;
      private bool Ddo_cargo_uonom_Includefilter ;
      private bool Ddo_cargo_uonom_Filterisrange ;
      private bool Ddo_cargo_uonom_Includedatalist ;
      private bool Ddo_cargo_ativo_Includesortasc ;
      private bool Ddo_cargo_ativo_Includesortdsc ;
      private bool Ddo_cargo_ativo_Includefilter ;
      private bool Ddo_cargo_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1003Cargo_UONom ;
      private bool A628Cargo_Ativo ;
      private bool AV64WWGeral_CargoDS_5_Dynamicfiltersenabled2 ;
      private bool AV69WWGeral_CargoDS_10_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV25Update_IsBlob ;
      private bool AV26Delete_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV16Cargo_Nome1 ;
      private String AV18DynamicFiltersSelector2 ;
      private String AV19Cargo_Nome2 ;
      private String AV21DynamicFiltersSelector3 ;
      private String AV22Cargo_Nome3 ;
      private String AV40TFCargo_Nome ;
      private String AV41TFCargo_Nome_Sel ;
      private String AV42ddo_Cargo_NomeTitleControlIdToReplace ;
      private String AV46ddo_GrupoCargo_CodigoTitleControlIdToReplace ;
      private String AV50ddo_Cargo_UONomTitleControlIdToReplace ;
      private String AV53ddo_Cargo_AtivoTitleControlIdToReplace ;
      private String AV81Update_GXI ;
      private String AV82Delete_GXI ;
      private String A618Cargo_Nome ;
      private String lV61WWGeral_CargoDS_2_Cargo_nome1 ;
      private String lV66WWGeral_CargoDS_7_Cargo_nome2 ;
      private String lV71WWGeral_CargoDS_12_Cargo_nome3 ;
      private String lV74WWGeral_CargoDS_15_Tfcargo_nome ;
      private String AV60WWGeral_CargoDS_1_Dynamicfiltersselector1 ;
      private String AV61WWGeral_CargoDS_2_Cargo_nome1 ;
      private String AV65WWGeral_CargoDS_6_Dynamicfiltersselector2 ;
      private String AV66WWGeral_CargoDS_7_Cargo_nome2 ;
      private String AV70WWGeral_CargoDS_11_Dynamicfiltersselector3 ;
      private String AV71WWGeral_CargoDS_12_Cargo_nome3 ;
      private String AV75WWGeral_CargoDS_16_Tfcargo_nome_sel ;
      private String AV74WWGeral_CargoDS_15_Tfcargo_nome ;
      private String AV25Update ;
      private String AV26Delete ;
      private IGxSession AV27Session ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox dynavGrupocargo_codigo1 ;
      private GXCombobox dynavCargo_uocod1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox dynavGrupocargo_codigo2 ;
      private GXCombobox dynavCargo_uocod2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox dynavGrupocargo_codigo3 ;
      private GXCombobox dynavCargo_uocod3 ;
      private GXCheckbox chkCargo_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00CW2_A615GrupoCargo_Codigo ;
      private String[] H00CW2_A616GrupoCargo_Nome ;
      private bool[] H00CW2_A626GrupoCargo_Ativo ;
      private int[] H00CW3_A611UnidadeOrganizacional_Codigo ;
      private String[] H00CW3_A612UnidadeOrganizacional_Nome ;
      private bool[] H00CW3_A629UnidadeOrganizacional_Ativo ;
      private int[] H00CW4_A615GrupoCargo_Codigo ;
      private String[] H00CW4_A616GrupoCargo_Nome ;
      private bool[] H00CW4_A626GrupoCargo_Ativo ;
      private int[] H00CW5_A611UnidadeOrganizacional_Codigo ;
      private String[] H00CW5_A612UnidadeOrganizacional_Nome ;
      private bool[] H00CW5_A629UnidadeOrganizacional_Ativo ;
      private int[] H00CW6_A615GrupoCargo_Codigo ;
      private String[] H00CW6_A616GrupoCargo_Nome ;
      private bool[] H00CW6_A626GrupoCargo_Ativo ;
      private int[] H00CW7_A611UnidadeOrganizacional_Codigo ;
      private String[] H00CW7_A612UnidadeOrganizacional_Nome ;
      private bool[] H00CW7_A629UnidadeOrganizacional_Ativo ;
      private int[] H00CW8_A1002Cargo_UOCod ;
      private bool[] H00CW8_n1002Cargo_UOCod ;
      private bool[] H00CW8_A628Cargo_Ativo ;
      private String[] H00CW8_A1003Cargo_UONom ;
      private bool[] H00CW8_n1003Cargo_UONom ;
      private int[] H00CW8_A615GrupoCargo_Codigo ;
      private String[] H00CW8_A618Cargo_Nome ;
      private int[] H00CW8_A617Cargo_Codigo ;
      private long[] H00CW9_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV39Cargo_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV43GrupoCargo_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV47Cargo_UONomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV51Cargo_AtivoTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV54DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwgeral_cargo__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00CW8( IGxContext context ,
                                             String AV60WWGeral_CargoDS_1_Dynamicfiltersselector1 ,
                                             String AV61WWGeral_CargoDS_2_Cargo_nome1 ,
                                             int AV62WWGeral_CargoDS_3_Grupocargo_codigo1 ,
                                             int AV63WWGeral_CargoDS_4_Cargo_uocod1 ,
                                             bool AV64WWGeral_CargoDS_5_Dynamicfiltersenabled2 ,
                                             String AV65WWGeral_CargoDS_6_Dynamicfiltersselector2 ,
                                             String AV66WWGeral_CargoDS_7_Cargo_nome2 ,
                                             int AV67WWGeral_CargoDS_8_Grupocargo_codigo2 ,
                                             int AV68WWGeral_CargoDS_9_Cargo_uocod2 ,
                                             bool AV69WWGeral_CargoDS_10_Dynamicfiltersenabled3 ,
                                             String AV70WWGeral_CargoDS_11_Dynamicfiltersselector3 ,
                                             String AV71WWGeral_CargoDS_12_Cargo_nome3 ,
                                             int AV72WWGeral_CargoDS_13_Grupocargo_codigo3 ,
                                             int AV73WWGeral_CargoDS_14_Cargo_uocod3 ,
                                             String AV75WWGeral_CargoDS_16_Tfcargo_nome_sel ,
                                             String AV74WWGeral_CargoDS_15_Tfcargo_nome ,
                                             int AV76WWGeral_CargoDS_17_Tfgrupocargo_codigo ,
                                             int AV77WWGeral_CargoDS_18_Tfgrupocargo_codigo_to ,
                                             String AV79WWGeral_CargoDS_20_Tfcargo_uonom_sel ,
                                             String AV78WWGeral_CargoDS_19_Tfcargo_uonom ,
                                             short AV80WWGeral_CargoDS_21_Tfcargo_ativo_sel ,
                                             String A618Cargo_Nome ,
                                             int A615GrupoCargo_Codigo ,
                                             int A1002Cargo_UOCod ,
                                             String A1003Cargo_UONom ,
                                             bool A628Cargo_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [20] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Cargo_UOCod] AS Cargo_UOCod, T1.[Cargo_Ativo], T2.[UnidadeOrganizacional_Nome] AS Cargo_UONom, T1.[GrupoCargo_Codigo], T1.[Cargo_Nome], T1.[Cargo_Codigo]";
         sFromString = " FROM ([Geral_Cargo] T1 WITH (NOLOCK) LEFT JOIN [Geral_UnidadeOrganizacional] T2 WITH (NOLOCK) ON T2.[UnidadeOrganizacional_Codigo] = T1.[Cargo_UOCod])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV60WWGeral_CargoDS_1_Dynamicfiltersselector1, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWGeral_CargoDS_2_Cargo_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV61WWGeral_CargoDS_2_Cargo_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_Nome] like '%' + @lV61WWGeral_CargoDS_2_Cargo_nome1 + '%')";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV60WWGeral_CargoDS_1_Dynamicfiltersselector1, "GRUPOCARGO_CODIGO") == 0 ) && ( ! (0==AV62WWGeral_CargoDS_3_Grupocargo_codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] = @AV62WWGeral_CargoDS_3_Grupocargo_codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[GrupoCargo_Codigo] = @AV62WWGeral_CargoDS_3_Grupocargo_codigo1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV60WWGeral_CargoDS_1_Dynamicfiltersselector1, "CARGO_UOCOD") == 0 ) && ( ! (0==AV63WWGeral_CargoDS_4_Cargo_uocod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_UOCod] = @AV63WWGeral_CargoDS_4_Cargo_uocod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_UOCod] = @AV63WWGeral_CargoDS_4_Cargo_uocod1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV64WWGeral_CargoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWGeral_CargoDS_6_Dynamicfiltersselector2, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWGeral_CargoDS_7_Cargo_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV66WWGeral_CargoDS_7_Cargo_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_Nome] like '%' + @lV66WWGeral_CargoDS_7_Cargo_nome2 + '%')";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV64WWGeral_CargoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWGeral_CargoDS_6_Dynamicfiltersselector2, "GRUPOCARGO_CODIGO") == 0 ) && ( ! (0==AV67WWGeral_CargoDS_8_Grupocargo_codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] = @AV67WWGeral_CargoDS_8_Grupocargo_codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[GrupoCargo_Codigo] = @AV67WWGeral_CargoDS_8_Grupocargo_codigo2)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV64WWGeral_CargoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWGeral_CargoDS_6_Dynamicfiltersselector2, "CARGO_UOCOD") == 0 ) && ( ! (0==AV68WWGeral_CargoDS_9_Cargo_uocod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_UOCod] = @AV68WWGeral_CargoDS_9_Cargo_uocod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_UOCod] = @AV68WWGeral_CargoDS_9_Cargo_uocod2)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV69WWGeral_CargoDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWGeral_CargoDS_11_Dynamicfiltersselector3, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWGeral_CargoDS_12_Cargo_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV71WWGeral_CargoDS_12_Cargo_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_Nome] like '%' + @lV71WWGeral_CargoDS_12_Cargo_nome3 + '%')";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV69WWGeral_CargoDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWGeral_CargoDS_11_Dynamicfiltersselector3, "GRUPOCARGO_CODIGO") == 0 ) && ( ! (0==AV72WWGeral_CargoDS_13_Grupocargo_codigo3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] = @AV72WWGeral_CargoDS_13_Grupocargo_codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[GrupoCargo_Codigo] = @AV72WWGeral_CargoDS_13_Grupocargo_codigo3)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV69WWGeral_CargoDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWGeral_CargoDS_11_Dynamicfiltersselector3, "CARGO_UOCOD") == 0 ) && ( ! (0==AV73WWGeral_CargoDS_14_Cargo_uocod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_UOCod] = @AV73WWGeral_CargoDS_14_Cargo_uocod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_UOCod] = @AV73WWGeral_CargoDS_14_Cargo_uocod3)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV75WWGeral_CargoDS_16_Tfcargo_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWGeral_CargoDS_15_Tfcargo_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_Nome] like @lV74WWGeral_CargoDS_15_Tfcargo_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_Nome] like @lV74WWGeral_CargoDS_15_Tfcargo_nome)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWGeral_CargoDS_16_Tfcargo_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_Nome] = @AV75WWGeral_CargoDS_16_Tfcargo_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_Nome] = @AV75WWGeral_CargoDS_16_Tfcargo_nome_sel)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! (0==AV76WWGeral_CargoDS_17_Tfgrupocargo_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] >= @AV76WWGeral_CargoDS_17_Tfgrupocargo_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[GrupoCargo_Codigo] >= @AV76WWGeral_CargoDS_17_Tfgrupocargo_codigo)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (0==AV77WWGeral_CargoDS_18_Tfgrupocargo_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] <= @AV77WWGeral_CargoDS_18_Tfgrupocargo_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[GrupoCargo_Codigo] <= @AV77WWGeral_CargoDS_18_Tfgrupocargo_codigo_to)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79WWGeral_CargoDS_20_Tfcargo_uonom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWGeral_CargoDS_19_Tfcargo_uonom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV78WWGeral_CargoDS_19_Tfcargo_uonom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeOrganizacional_Nome] like @lV78WWGeral_CargoDS_19_Tfcargo_uonom)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWGeral_CargoDS_20_Tfcargo_uonom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] = @AV79WWGeral_CargoDS_20_Tfcargo_uonom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeOrganizacional_Nome] = @AV79WWGeral_CargoDS_20_Tfcargo_uonom_sel)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( AV80WWGeral_CargoDS_21_Tfcargo_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_Ativo] = 1)";
            }
         }
         if ( AV80WWGeral_CargoDS_21_Tfcargo_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Cargo_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Cargo_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[GrupoCargo_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[GrupoCargo_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[UnidadeOrganizacional_Nome]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[UnidadeOrganizacional_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Cargo_Ativo]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Cargo_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Cargo_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00CW9( IGxContext context ,
                                             String AV60WWGeral_CargoDS_1_Dynamicfiltersselector1 ,
                                             String AV61WWGeral_CargoDS_2_Cargo_nome1 ,
                                             int AV62WWGeral_CargoDS_3_Grupocargo_codigo1 ,
                                             int AV63WWGeral_CargoDS_4_Cargo_uocod1 ,
                                             bool AV64WWGeral_CargoDS_5_Dynamicfiltersenabled2 ,
                                             String AV65WWGeral_CargoDS_6_Dynamicfiltersselector2 ,
                                             String AV66WWGeral_CargoDS_7_Cargo_nome2 ,
                                             int AV67WWGeral_CargoDS_8_Grupocargo_codigo2 ,
                                             int AV68WWGeral_CargoDS_9_Cargo_uocod2 ,
                                             bool AV69WWGeral_CargoDS_10_Dynamicfiltersenabled3 ,
                                             String AV70WWGeral_CargoDS_11_Dynamicfiltersselector3 ,
                                             String AV71WWGeral_CargoDS_12_Cargo_nome3 ,
                                             int AV72WWGeral_CargoDS_13_Grupocargo_codigo3 ,
                                             int AV73WWGeral_CargoDS_14_Cargo_uocod3 ,
                                             String AV75WWGeral_CargoDS_16_Tfcargo_nome_sel ,
                                             String AV74WWGeral_CargoDS_15_Tfcargo_nome ,
                                             int AV76WWGeral_CargoDS_17_Tfgrupocargo_codigo ,
                                             int AV77WWGeral_CargoDS_18_Tfgrupocargo_codigo_to ,
                                             String AV79WWGeral_CargoDS_20_Tfcargo_uonom_sel ,
                                             String AV78WWGeral_CargoDS_19_Tfcargo_uonom ,
                                             short AV80WWGeral_CargoDS_21_Tfcargo_ativo_sel ,
                                             String A618Cargo_Nome ,
                                             int A615GrupoCargo_Codigo ,
                                             int A1002Cargo_UOCod ,
                                             String A1003Cargo_UONom ,
                                             bool A628Cargo_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [15] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([Geral_Cargo] T1 WITH (NOLOCK) LEFT JOIN [Geral_UnidadeOrganizacional] T2 WITH (NOLOCK) ON T2.[UnidadeOrganizacional_Codigo] = T1.[Cargo_UOCod])";
         if ( ( StringUtil.StrCmp(AV60WWGeral_CargoDS_1_Dynamicfiltersselector1, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWGeral_CargoDS_2_Cargo_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV61WWGeral_CargoDS_2_Cargo_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_Nome] like '%' + @lV61WWGeral_CargoDS_2_Cargo_nome1 + '%')";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV60WWGeral_CargoDS_1_Dynamicfiltersselector1, "GRUPOCARGO_CODIGO") == 0 ) && ( ! (0==AV62WWGeral_CargoDS_3_Grupocargo_codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] = @AV62WWGeral_CargoDS_3_Grupocargo_codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[GrupoCargo_Codigo] = @AV62WWGeral_CargoDS_3_Grupocargo_codigo1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV60WWGeral_CargoDS_1_Dynamicfiltersselector1, "CARGO_UOCOD") == 0 ) && ( ! (0==AV63WWGeral_CargoDS_4_Cargo_uocod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_UOCod] = @AV63WWGeral_CargoDS_4_Cargo_uocod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_UOCod] = @AV63WWGeral_CargoDS_4_Cargo_uocod1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV64WWGeral_CargoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWGeral_CargoDS_6_Dynamicfiltersselector2, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWGeral_CargoDS_7_Cargo_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV66WWGeral_CargoDS_7_Cargo_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_Nome] like '%' + @lV66WWGeral_CargoDS_7_Cargo_nome2 + '%')";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV64WWGeral_CargoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWGeral_CargoDS_6_Dynamicfiltersselector2, "GRUPOCARGO_CODIGO") == 0 ) && ( ! (0==AV67WWGeral_CargoDS_8_Grupocargo_codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] = @AV67WWGeral_CargoDS_8_Grupocargo_codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[GrupoCargo_Codigo] = @AV67WWGeral_CargoDS_8_Grupocargo_codigo2)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV64WWGeral_CargoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWGeral_CargoDS_6_Dynamicfiltersselector2, "CARGO_UOCOD") == 0 ) && ( ! (0==AV68WWGeral_CargoDS_9_Cargo_uocod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_UOCod] = @AV68WWGeral_CargoDS_9_Cargo_uocod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_UOCod] = @AV68WWGeral_CargoDS_9_Cargo_uocod2)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV69WWGeral_CargoDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWGeral_CargoDS_11_Dynamicfiltersselector3, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWGeral_CargoDS_12_Cargo_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV71WWGeral_CargoDS_12_Cargo_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_Nome] like '%' + @lV71WWGeral_CargoDS_12_Cargo_nome3 + '%')";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV69WWGeral_CargoDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWGeral_CargoDS_11_Dynamicfiltersselector3, "GRUPOCARGO_CODIGO") == 0 ) && ( ! (0==AV72WWGeral_CargoDS_13_Grupocargo_codigo3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] = @AV72WWGeral_CargoDS_13_Grupocargo_codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[GrupoCargo_Codigo] = @AV72WWGeral_CargoDS_13_Grupocargo_codigo3)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV69WWGeral_CargoDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWGeral_CargoDS_11_Dynamicfiltersselector3, "CARGO_UOCOD") == 0 ) && ( ! (0==AV73WWGeral_CargoDS_14_Cargo_uocod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_UOCod] = @AV73WWGeral_CargoDS_14_Cargo_uocod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_UOCod] = @AV73WWGeral_CargoDS_14_Cargo_uocod3)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV75WWGeral_CargoDS_16_Tfcargo_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWGeral_CargoDS_15_Tfcargo_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_Nome] like @lV74WWGeral_CargoDS_15_Tfcargo_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_Nome] like @lV74WWGeral_CargoDS_15_Tfcargo_nome)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWGeral_CargoDS_16_Tfcargo_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_Nome] = @AV75WWGeral_CargoDS_16_Tfcargo_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_Nome] = @AV75WWGeral_CargoDS_16_Tfcargo_nome_sel)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! (0==AV76WWGeral_CargoDS_17_Tfgrupocargo_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] >= @AV76WWGeral_CargoDS_17_Tfgrupocargo_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[GrupoCargo_Codigo] >= @AV76WWGeral_CargoDS_17_Tfgrupocargo_codigo)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (0==AV77WWGeral_CargoDS_18_Tfgrupocargo_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] <= @AV77WWGeral_CargoDS_18_Tfgrupocargo_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[GrupoCargo_Codigo] <= @AV77WWGeral_CargoDS_18_Tfgrupocargo_codigo_to)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79WWGeral_CargoDS_20_Tfcargo_uonom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWGeral_CargoDS_19_Tfcargo_uonom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV78WWGeral_CargoDS_19_Tfcargo_uonom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeOrganizacional_Nome] like @lV78WWGeral_CargoDS_19_Tfcargo_uonom)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWGeral_CargoDS_20_Tfcargo_uonom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] = @AV79WWGeral_CargoDS_20_Tfcargo_uonom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeOrganizacional_Nome] = @AV79WWGeral_CargoDS_20_Tfcargo_uonom_sel)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( AV80WWGeral_CargoDS_21_Tfcargo_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_Ativo] = 1)";
            }
         }
         if ( AV80WWGeral_CargoDS_21_Tfcargo_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 6 :
                     return conditional_H00CW8(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (bool)dynConstraints[25] , (short)dynConstraints[26] , (bool)dynConstraints[27] );
               case 7 :
                     return conditional_H00CW9(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (bool)dynConstraints[25] , (short)dynConstraints[26] , (bool)dynConstraints[27] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00CW2 ;
          prmH00CW2 = new Object[] {
          } ;
          Object[] prmH00CW3 ;
          prmH00CW3 = new Object[] {
          } ;
          Object[] prmH00CW4 ;
          prmH00CW4 = new Object[] {
          } ;
          Object[] prmH00CW5 ;
          prmH00CW5 = new Object[] {
          } ;
          Object[] prmH00CW6 ;
          prmH00CW6 = new Object[] {
          } ;
          Object[] prmH00CW7 ;
          prmH00CW7 = new Object[] {
          } ;
          Object[] prmH00CW8 ;
          prmH00CW8 = new Object[] {
          new Object[] {"@lV61WWGeral_CargoDS_2_Cargo_nome1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV62WWGeral_CargoDS_3_Grupocargo_codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV63WWGeral_CargoDS_4_Cargo_uocod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV66WWGeral_CargoDS_7_Cargo_nome2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV67WWGeral_CargoDS_8_Grupocargo_codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV68WWGeral_CargoDS_9_Cargo_uocod2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV71WWGeral_CargoDS_12_Cargo_nome3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV72WWGeral_CargoDS_13_Grupocargo_codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV73WWGeral_CargoDS_14_Cargo_uocod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV74WWGeral_CargoDS_15_Tfcargo_nome",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV75WWGeral_CargoDS_16_Tfcargo_nome_sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV76WWGeral_CargoDS_17_Tfgrupocargo_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV77WWGeral_CargoDS_18_Tfgrupocargo_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV78WWGeral_CargoDS_19_Tfcargo_uonom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV79WWGeral_CargoDS_20_Tfcargo_uonom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00CW9 ;
          prmH00CW9 = new Object[] {
          new Object[] {"@lV61WWGeral_CargoDS_2_Cargo_nome1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV62WWGeral_CargoDS_3_Grupocargo_codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV63WWGeral_CargoDS_4_Cargo_uocod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV66WWGeral_CargoDS_7_Cargo_nome2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV67WWGeral_CargoDS_8_Grupocargo_codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV68WWGeral_CargoDS_9_Cargo_uocod2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV71WWGeral_CargoDS_12_Cargo_nome3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV72WWGeral_CargoDS_13_Grupocargo_codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV73WWGeral_CargoDS_14_Cargo_uocod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV74WWGeral_CargoDS_15_Tfcargo_nome",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV75WWGeral_CargoDS_16_Tfcargo_nome_sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV76WWGeral_CargoDS_17_Tfgrupocargo_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV77WWGeral_CargoDS_18_Tfgrupocargo_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV78WWGeral_CargoDS_19_Tfcargo_uonom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV79WWGeral_CargoDS_20_Tfcargo_uonom_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00CW2", "SELECT [GrupoCargo_Codigo], [GrupoCargo_Nome], [GrupoCargo_Ativo] FROM [Geral_Grupo_Cargo] WITH (NOLOCK) WHERE [GrupoCargo_Ativo] = 1 ORDER BY [GrupoCargo_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CW2,0,0,true,false )
             ,new CursorDef("H00CW3", "SELECT [UnidadeOrganizacional_Codigo], [UnidadeOrganizacional_Nome], [UnidadeOrganizacional_Ativo] FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Ativo] = 1 ORDER BY [UnidadeOrganizacional_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CW3,0,0,true,false )
             ,new CursorDef("H00CW4", "SELECT [GrupoCargo_Codigo], [GrupoCargo_Nome], [GrupoCargo_Ativo] FROM [Geral_Grupo_Cargo] WITH (NOLOCK) WHERE [GrupoCargo_Ativo] = 1 ORDER BY [GrupoCargo_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CW4,0,0,true,false )
             ,new CursorDef("H00CW5", "SELECT [UnidadeOrganizacional_Codigo], [UnidadeOrganizacional_Nome], [UnidadeOrganizacional_Ativo] FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Ativo] = 1 ORDER BY [UnidadeOrganizacional_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CW5,0,0,true,false )
             ,new CursorDef("H00CW6", "SELECT [GrupoCargo_Codigo], [GrupoCargo_Nome], [GrupoCargo_Ativo] FROM [Geral_Grupo_Cargo] WITH (NOLOCK) WHERE [GrupoCargo_Ativo] = 1 ORDER BY [GrupoCargo_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CW6,0,0,true,false )
             ,new CursorDef("H00CW7", "SELECT [UnidadeOrganizacional_Codigo], [UnidadeOrganizacional_Nome], [UnidadeOrganizacional_Ativo] FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Ativo] = 1 ORDER BY [UnidadeOrganizacional_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CW7,0,0,true,false )
             ,new CursorDef("H00CW8", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CW8,11,0,true,false )
             ,new CursorDef("H00CW9", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CW9,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
             case 7 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 6 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                return;
             case 7 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                return;
       }
    }

 }

}
