/*
               File: GetWWCaixaFilterData
        Description: Get WWCaixa Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:54:13.40
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwcaixafilterdata : GXProcedure
   {
      public getwwcaixafilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwcaixafilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV26DDOName = aP0_DDOName;
         this.AV24SearchTxt = aP1_SearchTxt;
         this.AV25SearchTxtTo = aP2_SearchTxtTo;
         this.AV30OptionsJson = "" ;
         this.AV33OptionsDescJson = "" ;
         this.AV35OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV26DDOName = aP0_DDOName;
         this.AV24SearchTxt = aP1_SearchTxt;
         this.AV25SearchTxtTo = aP2_SearchTxtTo;
         this.AV30OptionsJson = "" ;
         this.AV33OptionsDescJson = "" ;
         this.AV35OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
         return AV35OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwcaixafilterdata objgetwwcaixafilterdata;
         objgetwwcaixafilterdata = new getwwcaixafilterdata();
         objgetwwcaixafilterdata.AV26DDOName = aP0_DDOName;
         objgetwwcaixafilterdata.AV24SearchTxt = aP1_SearchTxt;
         objgetwwcaixafilterdata.AV25SearchTxtTo = aP2_SearchTxtTo;
         objgetwwcaixafilterdata.AV30OptionsJson = "" ;
         objgetwwcaixafilterdata.AV33OptionsDescJson = "" ;
         objgetwwcaixafilterdata.AV35OptionIndexesJson = "" ;
         objgetwwcaixafilterdata.context.SetSubmitInitialConfig(context);
         objgetwwcaixafilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwcaixafilterdata);
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwcaixafilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV29Options = (IGxCollection)(new GxSimpleCollection());
         AV32OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV34OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_CAIXA_DOCUMENTO") == 0 )
         {
            /* Execute user subroutine: 'LOADCAIXA_DOCUMENTOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_CAIXA_TIPODECONTACOD") == 0 )
         {
            /* Execute user subroutine: 'LOADCAIXA_TIPODECONTACODOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_CAIXA_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADCAIXA_DESCRICAOOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV30OptionsJson = AV29Options.ToJSonString(false);
         AV33OptionsDescJson = AV32OptionsDesc.ToJSonString(false);
         AV35OptionIndexesJson = AV34OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV37Session.Get("WWCaixaGridState"), "") == 0 )
         {
            AV39GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWCaixaGridState"), "");
         }
         else
         {
            AV39GridState.FromXml(AV37Session.Get("WWCaixaGridState"), "");
         }
         AV55GXV1 = 1;
         while ( AV55GXV1 <= AV39GridState.gxTpr_Filtervalues.Count )
         {
            AV40GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV39GridState.gxTpr_Filtervalues.Item(AV55GXV1));
            if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCAIXA_CODIGO") == 0 )
            {
               AV10TFCaixa_Codigo = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
               AV11TFCaixa_Codigo_To = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCAIXA_DOCUMENTO") == 0 )
            {
               AV12TFCaixa_Documento = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCAIXA_DOCUMENTO_SEL") == 0 )
            {
               AV13TFCaixa_Documento_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCAIXA_EMISSAO") == 0 )
            {
               AV14TFCaixa_Emissao = context.localUtil.CToD( AV40GridStateFilterValue.gxTpr_Value, 2);
               AV15TFCaixa_Emissao_To = context.localUtil.CToD( AV40GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCAIXA_VENCIMENTO") == 0 )
            {
               AV16TFCaixa_Vencimento = context.localUtil.CToD( AV40GridStateFilterValue.gxTpr_Value, 2);
               AV17TFCaixa_Vencimento_To = context.localUtil.CToD( AV40GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCAIXA_TIPODECONTACOD") == 0 )
            {
               AV18TFCaixa_TipoDeContaCod = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCAIXA_TIPODECONTACOD_SEL") == 0 )
            {
               AV19TFCaixa_TipoDeContaCod_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCAIXA_DESCRICAO") == 0 )
            {
               AV20TFCaixa_Descricao = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCAIXA_DESCRICAO_SEL") == 0 )
            {
               AV21TFCaixa_Descricao_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCAIXA_VALOR") == 0 )
            {
               AV22TFCaixa_Valor = NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, ".");
               AV23TFCaixa_Valor_To = NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, ".");
            }
            AV55GXV1 = (int)(AV55GXV1+1);
         }
         if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(1));
            AV42DynamicFiltersSelector1 = AV41GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "CAIXA_DOCUMENTO") == 0 )
            {
               AV43DynamicFiltersOperator1 = AV41GridStateDynamicFilter.gxTpr_Operator;
               AV44Caixa_Documento1 = AV41GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV45DynamicFiltersEnabled2 = true;
               AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(2));
               AV46DynamicFiltersSelector2 = AV41GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV46DynamicFiltersSelector2, "CAIXA_DOCUMENTO") == 0 )
               {
                  AV47DynamicFiltersOperator2 = AV41GridStateDynamicFilter.gxTpr_Operator;
                  AV48Caixa_Documento2 = AV41GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV49DynamicFiltersEnabled3 = true;
                  AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(3));
                  AV50DynamicFiltersSelector3 = AV41GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CAIXA_DOCUMENTO") == 0 )
                  {
                     AV51DynamicFiltersOperator3 = AV41GridStateDynamicFilter.gxTpr_Operator;
                     AV52Caixa_Documento3 = AV41GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCAIXA_DOCUMENTOOPTIONS' Routine */
         AV12TFCaixa_Documento = AV24SearchTxt;
         AV13TFCaixa_Documento_Sel = "";
         AV57WWCaixaDS_1_Dynamicfiltersselector1 = AV42DynamicFiltersSelector1;
         AV58WWCaixaDS_2_Dynamicfiltersoperator1 = AV43DynamicFiltersOperator1;
         AV59WWCaixaDS_3_Caixa_documento1 = AV44Caixa_Documento1;
         AV60WWCaixaDS_4_Dynamicfiltersenabled2 = AV45DynamicFiltersEnabled2;
         AV61WWCaixaDS_5_Dynamicfiltersselector2 = AV46DynamicFiltersSelector2;
         AV62WWCaixaDS_6_Dynamicfiltersoperator2 = AV47DynamicFiltersOperator2;
         AV63WWCaixaDS_7_Caixa_documento2 = AV48Caixa_Documento2;
         AV64WWCaixaDS_8_Dynamicfiltersenabled3 = AV49DynamicFiltersEnabled3;
         AV65WWCaixaDS_9_Dynamicfiltersselector3 = AV50DynamicFiltersSelector3;
         AV66WWCaixaDS_10_Dynamicfiltersoperator3 = AV51DynamicFiltersOperator3;
         AV67WWCaixaDS_11_Caixa_documento3 = AV52Caixa_Documento3;
         AV68WWCaixaDS_12_Tfcaixa_codigo = AV10TFCaixa_Codigo;
         AV69WWCaixaDS_13_Tfcaixa_codigo_to = AV11TFCaixa_Codigo_To;
         AV70WWCaixaDS_14_Tfcaixa_documento = AV12TFCaixa_Documento;
         AV71WWCaixaDS_15_Tfcaixa_documento_sel = AV13TFCaixa_Documento_Sel;
         AV72WWCaixaDS_16_Tfcaixa_emissao = AV14TFCaixa_Emissao;
         AV73WWCaixaDS_17_Tfcaixa_emissao_to = AV15TFCaixa_Emissao_To;
         AV74WWCaixaDS_18_Tfcaixa_vencimento = AV16TFCaixa_Vencimento;
         AV75WWCaixaDS_19_Tfcaixa_vencimento_to = AV17TFCaixa_Vencimento_To;
         AV76WWCaixaDS_20_Tfcaixa_tipodecontacod = AV18TFCaixa_TipoDeContaCod;
         AV77WWCaixaDS_21_Tfcaixa_tipodecontacod_sel = AV19TFCaixa_TipoDeContaCod_Sel;
         AV78WWCaixaDS_22_Tfcaixa_descricao = AV20TFCaixa_Descricao;
         AV79WWCaixaDS_23_Tfcaixa_descricao_sel = AV21TFCaixa_Descricao_Sel;
         AV80WWCaixaDS_24_Tfcaixa_valor = AV22TFCaixa_Valor;
         AV81WWCaixaDS_25_Tfcaixa_valor_to = AV23TFCaixa_Valor_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV57WWCaixaDS_1_Dynamicfiltersselector1 ,
                                              AV58WWCaixaDS_2_Dynamicfiltersoperator1 ,
                                              AV59WWCaixaDS_3_Caixa_documento1 ,
                                              AV60WWCaixaDS_4_Dynamicfiltersenabled2 ,
                                              AV61WWCaixaDS_5_Dynamicfiltersselector2 ,
                                              AV62WWCaixaDS_6_Dynamicfiltersoperator2 ,
                                              AV63WWCaixaDS_7_Caixa_documento2 ,
                                              AV64WWCaixaDS_8_Dynamicfiltersenabled3 ,
                                              AV65WWCaixaDS_9_Dynamicfiltersselector3 ,
                                              AV66WWCaixaDS_10_Dynamicfiltersoperator3 ,
                                              AV67WWCaixaDS_11_Caixa_documento3 ,
                                              AV68WWCaixaDS_12_Tfcaixa_codigo ,
                                              AV69WWCaixaDS_13_Tfcaixa_codigo_to ,
                                              AV71WWCaixaDS_15_Tfcaixa_documento_sel ,
                                              AV70WWCaixaDS_14_Tfcaixa_documento ,
                                              AV72WWCaixaDS_16_Tfcaixa_emissao ,
                                              AV73WWCaixaDS_17_Tfcaixa_emissao_to ,
                                              AV74WWCaixaDS_18_Tfcaixa_vencimento ,
                                              AV75WWCaixaDS_19_Tfcaixa_vencimento_to ,
                                              AV77WWCaixaDS_21_Tfcaixa_tipodecontacod_sel ,
                                              AV76WWCaixaDS_20_Tfcaixa_tipodecontacod ,
                                              AV79WWCaixaDS_23_Tfcaixa_descricao_sel ,
                                              AV78WWCaixaDS_22_Tfcaixa_descricao ,
                                              AV80WWCaixaDS_24_Tfcaixa_valor ,
                                              AV81WWCaixaDS_25_Tfcaixa_valor_to ,
                                              A875Caixa_Documento ,
                                              A874Caixa_Codigo ,
                                              A876Caixa_Emissao ,
                                              A877Caixa_Vencimento ,
                                              A881Caixa_TipoDeContaCod ,
                                              A879Caixa_Descricao ,
                                              A880Caixa_Valor },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL
                                              }
         });
         lV59WWCaixaDS_3_Caixa_documento1 = StringUtil.Concat( StringUtil.RTrim( AV59WWCaixaDS_3_Caixa_documento1), "%", "");
         lV59WWCaixaDS_3_Caixa_documento1 = StringUtil.Concat( StringUtil.RTrim( AV59WWCaixaDS_3_Caixa_documento1), "%", "");
         lV63WWCaixaDS_7_Caixa_documento2 = StringUtil.Concat( StringUtil.RTrim( AV63WWCaixaDS_7_Caixa_documento2), "%", "");
         lV63WWCaixaDS_7_Caixa_documento2 = StringUtil.Concat( StringUtil.RTrim( AV63WWCaixaDS_7_Caixa_documento2), "%", "");
         lV67WWCaixaDS_11_Caixa_documento3 = StringUtil.Concat( StringUtil.RTrim( AV67WWCaixaDS_11_Caixa_documento3), "%", "");
         lV67WWCaixaDS_11_Caixa_documento3 = StringUtil.Concat( StringUtil.RTrim( AV67WWCaixaDS_11_Caixa_documento3), "%", "");
         lV70WWCaixaDS_14_Tfcaixa_documento = StringUtil.Concat( StringUtil.RTrim( AV70WWCaixaDS_14_Tfcaixa_documento), "%", "");
         lV76WWCaixaDS_20_Tfcaixa_tipodecontacod = StringUtil.PadR( StringUtil.RTrim( AV76WWCaixaDS_20_Tfcaixa_tipodecontacod), 20, "%");
         lV78WWCaixaDS_22_Tfcaixa_descricao = StringUtil.Concat( StringUtil.RTrim( AV78WWCaixaDS_22_Tfcaixa_descricao), "%", "");
         /* Using cursor P00P32 */
         pr_default.execute(0, new Object[] {lV59WWCaixaDS_3_Caixa_documento1, lV59WWCaixaDS_3_Caixa_documento1, lV63WWCaixaDS_7_Caixa_documento2, lV63WWCaixaDS_7_Caixa_documento2, lV67WWCaixaDS_11_Caixa_documento3, lV67WWCaixaDS_11_Caixa_documento3, AV68WWCaixaDS_12_Tfcaixa_codigo, AV69WWCaixaDS_13_Tfcaixa_codigo_to, lV70WWCaixaDS_14_Tfcaixa_documento, AV71WWCaixaDS_15_Tfcaixa_documento_sel, AV72WWCaixaDS_16_Tfcaixa_emissao, AV73WWCaixaDS_17_Tfcaixa_emissao_to, AV74WWCaixaDS_18_Tfcaixa_vencimento, AV75WWCaixaDS_19_Tfcaixa_vencimento_to, lV76WWCaixaDS_20_Tfcaixa_tipodecontacod, AV77WWCaixaDS_21_Tfcaixa_tipodecontacod_sel, lV78WWCaixaDS_22_Tfcaixa_descricao, AV79WWCaixaDS_23_Tfcaixa_descricao_sel, AV80WWCaixaDS_24_Tfcaixa_valor, AV81WWCaixaDS_25_Tfcaixa_valor_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKP32 = false;
            A875Caixa_Documento = P00P32_A875Caixa_Documento[0];
            A880Caixa_Valor = P00P32_A880Caixa_Valor[0];
            A879Caixa_Descricao = P00P32_A879Caixa_Descricao[0];
            n879Caixa_Descricao = P00P32_n879Caixa_Descricao[0];
            A881Caixa_TipoDeContaCod = P00P32_A881Caixa_TipoDeContaCod[0];
            A877Caixa_Vencimento = P00P32_A877Caixa_Vencimento[0];
            n877Caixa_Vencimento = P00P32_n877Caixa_Vencimento[0];
            A876Caixa_Emissao = P00P32_A876Caixa_Emissao[0];
            n876Caixa_Emissao = P00P32_n876Caixa_Emissao[0];
            A874Caixa_Codigo = P00P32_A874Caixa_Codigo[0];
            AV36count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00P32_A875Caixa_Documento[0], A875Caixa_Documento) == 0 ) )
            {
               BRKP32 = false;
               A874Caixa_Codigo = P00P32_A874Caixa_Codigo[0];
               AV36count = (long)(AV36count+1);
               BRKP32 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A875Caixa_Documento)) )
            {
               AV28Option = A875Caixa_Documento;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKP32 )
            {
               BRKP32 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCAIXA_TIPODECONTACODOPTIONS' Routine */
         AV18TFCaixa_TipoDeContaCod = AV24SearchTxt;
         AV19TFCaixa_TipoDeContaCod_Sel = "";
         AV57WWCaixaDS_1_Dynamicfiltersselector1 = AV42DynamicFiltersSelector1;
         AV58WWCaixaDS_2_Dynamicfiltersoperator1 = AV43DynamicFiltersOperator1;
         AV59WWCaixaDS_3_Caixa_documento1 = AV44Caixa_Documento1;
         AV60WWCaixaDS_4_Dynamicfiltersenabled2 = AV45DynamicFiltersEnabled2;
         AV61WWCaixaDS_5_Dynamicfiltersselector2 = AV46DynamicFiltersSelector2;
         AV62WWCaixaDS_6_Dynamicfiltersoperator2 = AV47DynamicFiltersOperator2;
         AV63WWCaixaDS_7_Caixa_documento2 = AV48Caixa_Documento2;
         AV64WWCaixaDS_8_Dynamicfiltersenabled3 = AV49DynamicFiltersEnabled3;
         AV65WWCaixaDS_9_Dynamicfiltersselector3 = AV50DynamicFiltersSelector3;
         AV66WWCaixaDS_10_Dynamicfiltersoperator3 = AV51DynamicFiltersOperator3;
         AV67WWCaixaDS_11_Caixa_documento3 = AV52Caixa_Documento3;
         AV68WWCaixaDS_12_Tfcaixa_codigo = AV10TFCaixa_Codigo;
         AV69WWCaixaDS_13_Tfcaixa_codigo_to = AV11TFCaixa_Codigo_To;
         AV70WWCaixaDS_14_Tfcaixa_documento = AV12TFCaixa_Documento;
         AV71WWCaixaDS_15_Tfcaixa_documento_sel = AV13TFCaixa_Documento_Sel;
         AV72WWCaixaDS_16_Tfcaixa_emissao = AV14TFCaixa_Emissao;
         AV73WWCaixaDS_17_Tfcaixa_emissao_to = AV15TFCaixa_Emissao_To;
         AV74WWCaixaDS_18_Tfcaixa_vencimento = AV16TFCaixa_Vencimento;
         AV75WWCaixaDS_19_Tfcaixa_vencimento_to = AV17TFCaixa_Vencimento_To;
         AV76WWCaixaDS_20_Tfcaixa_tipodecontacod = AV18TFCaixa_TipoDeContaCod;
         AV77WWCaixaDS_21_Tfcaixa_tipodecontacod_sel = AV19TFCaixa_TipoDeContaCod_Sel;
         AV78WWCaixaDS_22_Tfcaixa_descricao = AV20TFCaixa_Descricao;
         AV79WWCaixaDS_23_Tfcaixa_descricao_sel = AV21TFCaixa_Descricao_Sel;
         AV80WWCaixaDS_24_Tfcaixa_valor = AV22TFCaixa_Valor;
         AV81WWCaixaDS_25_Tfcaixa_valor_to = AV23TFCaixa_Valor_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV57WWCaixaDS_1_Dynamicfiltersselector1 ,
                                              AV58WWCaixaDS_2_Dynamicfiltersoperator1 ,
                                              AV59WWCaixaDS_3_Caixa_documento1 ,
                                              AV60WWCaixaDS_4_Dynamicfiltersenabled2 ,
                                              AV61WWCaixaDS_5_Dynamicfiltersselector2 ,
                                              AV62WWCaixaDS_6_Dynamicfiltersoperator2 ,
                                              AV63WWCaixaDS_7_Caixa_documento2 ,
                                              AV64WWCaixaDS_8_Dynamicfiltersenabled3 ,
                                              AV65WWCaixaDS_9_Dynamicfiltersselector3 ,
                                              AV66WWCaixaDS_10_Dynamicfiltersoperator3 ,
                                              AV67WWCaixaDS_11_Caixa_documento3 ,
                                              AV68WWCaixaDS_12_Tfcaixa_codigo ,
                                              AV69WWCaixaDS_13_Tfcaixa_codigo_to ,
                                              AV71WWCaixaDS_15_Tfcaixa_documento_sel ,
                                              AV70WWCaixaDS_14_Tfcaixa_documento ,
                                              AV72WWCaixaDS_16_Tfcaixa_emissao ,
                                              AV73WWCaixaDS_17_Tfcaixa_emissao_to ,
                                              AV74WWCaixaDS_18_Tfcaixa_vencimento ,
                                              AV75WWCaixaDS_19_Tfcaixa_vencimento_to ,
                                              AV77WWCaixaDS_21_Tfcaixa_tipodecontacod_sel ,
                                              AV76WWCaixaDS_20_Tfcaixa_tipodecontacod ,
                                              AV79WWCaixaDS_23_Tfcaixa_descricao_sel ,
                                              AV78WWCaixaDS_22_Tfcaixa_descricao ,
                                              AV80WWCaixaDS_24_Tfcaixa_valor ,
                                              AV81WWCaixaDS_25_Tfcaixa_valor_to ,
                                              A875Caixa_Documento ,
                                              A874Caixa_Codigo ,
                                              A876Caixa_Emissao ,
                                              A877Caixa_Vencimento ,
                                              A881Caixa_TipoDeContaCod ,
                                              A879Caixa_Descricao ,
                                              A880Caixa_Valor },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL
                                              }
         });
         lV59WWCaixaDS_3_Caixa_documento1 = StringUtil.Concat( StringUtil.RTrim( AV59WWCaixaDS_3_Caixa_documento1), "%", "");
         lV59WWCaixaDS_3_Caixa_documento1 = StringUtil.Concat( StringUtil.RTrim( AV59WWCaixaDS_3_Caixa_documento1), "%", "");
         lV63WWCaixaDS_7_Caixa_documento2 = StringUtil.Concat( StringUtil.RTrim( AV63WWCaixaDS_7_Caixa_documento2), "%", "");
         lV63WWCaixaDS_7_Caixa_documento2 = StringUtil.Concat( StringUtil.RTrim( AV63WWCaixaDS_7_Caixa_documento2), "%", "");
         lV67WWCaixaDS_11_Caixa_documento3 = StringUtil.Concat( StringUtil.RTrim( AV67WWCaixaDS_11_Caixa_documento3), "%", "");
         lV67WWCaixaDS_11_Caixa_documento3 = StringUtil.Concat( StringUtil.RTrim( AV67WWCaixaDS_11_Caixa_documento3), "%", "");
         lV70WWCaixaDS_14_Tfcaixa_documento = StringUtil.Concat( StringUtil.RTrim( AV70WWCaixaDS_14_Tfcaixa_documento), "%", "");
         lV76WWCaixaDS_20_Tfcaixa_tipodecontacod = StringUtil.PadR( StringUtil.RTrim( AV76WWCaixaDS_20_Tfcaixa_tipodecontacod), 20, "%");
         lV78WWCaixaDS_22_Tfcaixa_descricao = StringUtil.Concat( StringUtil.RTrim( AV78WWCaixaDS_22_Tfcaixa_descricao), "%", "");
         /* Using cursor P00P33 */
         pr_default.execute(1, new Object[] {lV59WWCaixaDS_3_Caixa_documento1, lV59WWCaixaDS_3_Caixa_documento1, lV63WWCaixaDS_7_Caixa_documento2, lV63WWCaixaDS_7_Caixa_documento2, lV67WWCaixaDS_11_Caixa_documento3, lV67WWCaixaDS_11_Caixa_documento3, AV68WWCaixaDS_12_Tfcaixa_codigo, AV69WWCaixaDS_13_Tfcaixa_codigo_to, lV70WWCaixaDS_14_Tfcaixa_documento, AV71WWCaixaDS_15_Tfcaixa_documento_sel, AV72WWCaixaDS_16_Tfcaixa_emissao, AV73WWCaixaDS_17_Tfcaixa_emissao_to, AV74WWCaixaDS_18_Tfcaixa_vencimento, AV75WWCaixaDS_19_Tfcaixa_vencimento_to, lV76WWCaixaDS_20_Tfcaixa_tipodecontacod, AV77WWCaixaDS_21_Tfcaixa_tipodecontacod_sel, lV78WWCaixaDS_22_Tfcaixa_descricao, AV79WWCaixaDS_23_Tfcaixa_descricao_sel, AV80WWCaixaDS_24_Tfcaixa_valor, AV81WWCaixaDS_25_Tfcaixa_valor_to});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKP34 = false;
            A881Caixa_TipoDeContaCod = P00P33_A881Caixa_TipoDeContaCod[0];
            A880Caixa_Valor = P00P33_A880Caixa_Valor[0];
            A879Caixa_Descricao = P00P33_A879Caixa_Descricao[0];
            n879Caixa_Descricao = P00P33_n879Caixa_Descricao[0];
            A877Caixa_Vencimento = P00P33_A877Caixa_Vencimento[0];
            n877Caixa_Vencimento = P00P33_n877Caixa_Vencimento[0];
            A876Caixa_Emissao = P00P33_A876Caixa_Emissao[0];
            n876Caixa_Emissao = P00P33_n876Caixa_Emissao[0];
            A874Caixa_Codigo = P00P33_A874Caixa_Codigo[0];
            A875Caixa_Documento = P00P33_A875Caixa_Documento[0];
            AV36count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00P33_A881Caixa_TipoDeContaCod[0], A881Caixa_TipoDeContaCod) == 0 ) )
            {
               BRKP34 = false;
               A874Caixa_Codigo = P00P33_A874Caixa_Codigo[0];
               AV36count = (long)(AV36count+1);
               BRKP34 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A881Caixa_TipoDeContaCod)) )
            {
               AV28Option = A881Caixa_TipoDeContaCod;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKP34 )
            {
               BRKP34 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCAIXA_DESCRICAOOPTIONS' Routine */
         AV20TFCaixa_Descricao = AV24SearchTxt;
         AV21TFCaixa_Descricao_Sel = "";
         AV57WWCaixaDS_1_Dynamicfiltersselector1 = AV42DynamicFiltersSelector1;
         AV58WWCaixaDS_2_Dynamicfiltersoperator1 = AV43DynamicFiltersOperator1;
         AV59WWCaixaDS_3_Caixa_documento1 = AV44Caixa_Documento1;
         AV60WWCaixaDS_4_Dynamicfiltersenabled2 = AV45DynamicFiltersEnabled2;
         AV61WWCaixaDS_5_Dynamicfiltersselector2 = AV46DynamicFiltersSelector2;
         AV62WWCaixaDS_6_Dynamicfiltersoperator2 = AV47DynamicFiltersOperator2;
         AV63WWCaixaDS_7_Caixa_documento2 = AV48Caixa_Documento2;
         AV64WWCaixaDS_8_Dynamicfiltersenabled3 = AV49DynamicFiltersEnabled3;
         AV65WWCaixaDS_9_Dynamicfiltersselector3 = AV50DynamicFiltersSelector3;
         AV66WWCaixaDS_10_Dynamicfiltersoperator3 = AV51DynamicFiltersOperator3;
         AV67WWCaixaDS_11_Caixa_documento3 = AV52Caixa_Documento3;
         AV68WWCaixaDS_12_Tfcaixa_codigo = AV10TFCaixa_Codigo;
         AV69WWCaixaDS_13_Tfcaixa_codigo_to = AV11TFCaixa_Codigo_To;
         AV70WWCaixaDS_14_Tfcaixa_documento = AV12TFCaixa_Documento;
         AV71WWCaixaDS_15_Tfcaixa_documento_sel = AV13TFCaixa_Documento_Sel;
         AV72WWCaixaDS_16_Tfcaixa_emissao = AV14TFCaixa_Emissao;
         AV73WWCaixaDS_17_Tfcaixa_emissao_to = AV15TFCaixa_Emissao_To;
         AV74WWCaixaDS_18_Tfcaixa_vencimento = AV16TFCaixa_Vencimento;
         AV75WWCaixaDS_19_Tfcaixa_vencimento_to = AV17TFCaixa_Vencimento_To;
         AV76WWCaixaDS_20_Tfcaixa_tipodecontacod = AV18TFCaixa_TipoDeContaCod;
         AV77WWCaixaDS_21_Tfcaixa_tipodecontacod_sel = AV19TFCaixa_TipoDeContaCod_Sel;
         AV78WWCaixaDS_22_Tfcaixa_descricao = AV20TFCaixa_Descricao;
         AV79WWCaixaDS_23_Tfcaixa_descricao_sel = AV21TFCaixa_Descricao_Sel;
         AV80WWCaixaDS_24_Tfcaixa_valor = AV22TFCaixa_Valor;
         AV81WWCaixaDS_25_Tfcaixa_valor_to = AV23TFCaixa_Valor_To;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV57WWCaixaDS_1_Dynamicfiltersselector1 ,
                                              AV58WWCaixaDS_2_Dynamicfiltersoperator1 ,
                                              AV59WWCaixaDS_3_Caixa_documento1 ,
                                              AV60WWCaixaDS_4_Dynamicfiltersenabled2 ,
                                              AV61WWCaixaDS_5_Dynamicfiltersselector2 ,
                                              AV62WWCaixaDS_6_Dynamicfiltersoperator2 ,
                                              AV63WWCaixaDS_7_Caixa_documento2 ,
                                              AV64WWCaixaDS_8_Dynamicfiltersenabled3 ,
                                              AV65WWCaixaDS_9_Dynamicfiltersselector3 ,
                                              AV66WWCaixaDS_10_Dynamicfiltersoperator3 ,
                                              AV67WWCaixaDS_11_Caixa_documento3 ,
                                              AV68WWCaixaDS_12_Tfcaixa_codigo ,
                                              AV69WWCaixaDS_13_Tfcaixa_codigo_to ,
                                              AV71WWCaixaDS_15_Tfcaixa_documento_sel ,
                                              AV70WWCaixaDS_14_Tfcaixa_documento ,
                                              AV72WWCaixaDS_16_Tfcaixa_emissao ,
                                              AV73WWCaixaDS_17_Tfcaixa_emissao_to ,
                                              AV74WWCaixaDS_18_Tfcaixa_vencimento ,
                                              AV75WWCaixaDS_19_Tfcaixa_vencimento_to ,
                                              AV77WWCaixaDS_21_Tfcaixa_tipodecontacod_sel ,
                                              AV76WWCaixaDS_20_Tfcaixa_tipodecontacod ,
                                              AV79WWCaixaDS_23_Tfcaixa_descricao_sel ,
                                              AV78WWCaixaDS_22_Tfcaixa_descricao ,
                                              AV80WWCaixaDS_24_Tfcaixa_valor ,
                                              AV81WWCaixaDS_25_Tfcaixa_valor_to ,
                                              A875Caixa_Documento ,
                                              A874Caixa_Codigo ,
                                              A876Caixa_Emissao ,
                                              A877Caixa_Vencimento ,
                                              A881Caixa_TipoDeContaCod ,
                                              A879Caixa_Descricao ,
                                              A880Caixa_Valor },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL
                                              }
         });
         lV59WWCaixaDS_3_Caixa_documento1 = StringUtil.Concat( StringUtil.RTrim( AV59WWCaixaDS_3_Caixa_documento1), "%", "");
         lV59WWCaixaDS_3_Caixa_documento1 = StringUtil.Concat( StringUtil.RTrim( AV59WWCaixaDS_3_Caixa_documento1), "%", "");
         lV63WWCaixaDS_7_Caixa_documento2 = StringUtil.Concat( StringUtil.RTrim( AV63WWCaixaDS_7_Caixa_documento2), "%", "");
         lV63WWCaixaDS_7_Caixa_documento2 = StringUtil.Concat( StringUtil.RTrim( AV63WWCaixaDS_7_Caixa_documento2), "%", "");
         lV67WWCaixaDS_11_Caixa_documento3 = StringUtil.Concat( StringUtil.RTrim( AV67WWCaixaDS_11_Caixa_documento3), "%", "");
         lV67WWCaixaDS_11_Caixa_documento3 = StringUtil.Concat( StringUtil.RTrim( AV67WWCaixaDS_11_Caixa_documento3), "%", "");
         lV70WWCaixaDS_14_Tfcaixa_documento = StringUtil.Concat( StringUtil.RTrim( AV70WWCaixaDS_14_Tfcaixa_documento), "%", "");
         lV76WWCaixaDS_20_Tfcaixa_tipodecontacod = StringUtil.PadR( StringUtil.RTrim( AV76WWCaixaDS_20_Tfcaixa_tipodecontacod), 20, "%");
         lV78WWCaixaDS_22_Tfcaixa_descricao = StringUtil.Concat( StringUtil.RTrim( AV78WWCaixaDS_22_Tfcaixa_descricao), "%", "");
         /* Using cursor P00P34 */
         pr_default.execute(2, new Object[] {lV59WWCaixaDS_3_Caixa_documento1, lV59WWCaixaDS_3_Caixa_documento1, lV63WWCaixaDS_7_Caixa_documento2, lV63WWCaixaDS_7_Caixa_documento2, lV67WWCaixaDS_11_Caixa_documento3, lV67WWCaixaDS_11_Caixa_documento3, AV68WWCaixaDS_12_Tfcaixa_codigo, AV69WWCaixaDS_13_Tfcaixa_codigo_to, lV70WWCaixaDS_14_Tfcaixa_documento, AV71WWCaixaDS_15_Tfcaixa_documento_sel, AV72WWCaixaDS_16_Tfcaixa_emissao, AV73WWCaixaDS_17_Tfcaixa_emissao_to, AV74WWCaixaDS_18_Tfcaixa_vencimento, AV75WWCaixaDS_19_Tfcaixa_vencimento_to, lV76WWCaixaDS_20_Tfcaixa_tipodecontacod, AV77WWCaixaDS_21_Tfcaixa_tipodecontacod_sel, lV78WWCaixaDS_22_Tfcaixa_descricao, AV79WWCaixaDS_23_Tfcaixa_descricao_sel, AV80WWCaixaDS_24_Tfcaixa_valor, AV81WWCaixaDS_25_Tfcaixa_valor_to});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKP36 = false;
            A879Caixa_Descricao = P00P34_A879Caixa_Descricao[0];
            n879Caixa_Descricao = P00P34_n879Caixa_Descricao[0];
            A880Caixa_Valor = P00P34_A880Caixa_Valor[0];
            A881Caixa_TipoDeContaCod = P00P34_A881Caixa_TipoDeContaCod[0];
            A877Caixa_Vencimento = P00P34_A877Caixa_Vencimento[0];
            n877Caixa_Vencimento = P00P34_n877Caixa_Vencimento[0];
            A876Caixa_Emissao = P00P34_A876Caixa_Emissao[0];
            n876Caixa_Emissao = P00P34_n876Caixa_Emissao[0];
            A874Caixa_Codigo = P00P34_A874Caixa_Codigo[0];
            A875Caixa_Documento = P00P34_A875Caixa_Documento[0];
            AV36count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00P34_A879Caixa_Descricao[0], A879Caixa_Descricao) == 0 ) )
            {
               BRKP36 = false;
               A874Caixa_Codigo = P00P34_A874Caixa_Codigo[0];
               AV36count = (long)(AV36count+1);
               BRKP36 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A879Caixa_Descricao)) )
            {
               AV28Option = A879Caixa_Descricao;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKP36 )
            {
               BRKP36 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV29Options = new GxSimpleCollection();
         AV32OptionsDesc = new GxSimpleCollection();
         AV34OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV37Session = context.GetSession();
         AV39GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV40GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12TFCaixa_Documento = "";
         AV13TFCaixa_Documento_Sel = "";
         AV14TFCaixa_Emissao = DateTime.MinValue;
         AV15TFCaixa_Emissao_To = DateTime.MinValue;
         AV16TFCaixa_Vencimento = DateTime.MinValue;
         AV17TFCaixa_Vencimento_To = DateTime.MinValue;
         AV18TFCaixa_TipoDeContaCod = "";
         AV19TFCaixa_TipoDeContaCod_Sel = "";
         AV20TFCaixa_Descricao = "";
         AV21TFCaixa_Descricao_Sel = "";
         AV41GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV42DynamicFiltersSelector1 = "";
         AV44Caixa_Documento1 = "";
         AV46DynamicFiltersSelector2 = "";
         AV48Caixa_Documento2 = "";
         AV50DynamicFiltersSelector3 = "";
         AV52Caixa_Documento3 = "";
         AV57WWCaixaDS_1_Dynamicfiltersselector1 = "";
         AV59WWCaixaDS_3_Caixa_documento1 = "";
         AV61WWCaixaDS_5_Dynamicfiltersselector2 = "";
         AV63WWCaixaDS_7_Caixa_documento2 = "";
         AV65WWCaixaDS_9_Dynamicfiltersselector3 = "";
         AV67WWCaixaDS_11_Caixa_documento3 = "";
         AV70WWCaixaDS_14_Tfcaixa_documento = "";
         AV71WWCaixaDS_15_Tfcaixa_documento_sel = "";
         AV72WWCaixaDS_16_Tfcaixa_emissao = DateTime.MinValue;
         AV73WWCaixaDS_17_Tfcaixa_emissao_to = DateTime.MinValue;
         AV74WWCaixaDS_18_Tfcaixa_vencimento = DateTime.MinValue;
         AV75WWCaixaDS_19_Tfcaixa_vencimento_to = DateTime.MinValue;
         AV76WWCaixaDS_20_Tfcaixa_tipodecontacod = "";
         AV77WWCaixaDS_21_Tfcaixa_tipodecontacod_sel = "";
         AV78WWCaixaDS_22_Tfcaixa_descricao = "";
         AV79WWCaixaDS_23_Tfcaixa_descricao_sel = "";
         scmdbuf = "";
         lV59WWCaixaDS_3_Caixa_documento1 = "";
         lV63WWCaixaDS_7_Caixa_documento2 = "";
         lV67WWCaixaDS_11_Caixa_documento3 = "";
         lV70WWCaixaDS_14_Tfcaixa_documento = "";
         lV76WWCaixaDS_20_Tfcaixa_tipodecontacod = "";
         lV78WWCaixaDS_22_Tfcaixa_descricao = "";
         A875Caixa_Documento = "";
         A876Caixa_Emissao = DateTime.MinValue;
         A877Caixa_Vencimento = DateTime.MinValue;
         A881Caixa_TipoDeContaCod = "";
         A879Caixa_Descricao = "";
         P00P32_A875Caixa_Documento = new String[] {""} ;
         P00P32_A880Caixa_Valor = new decimal[1] ;
         P00P32_A879Caixa_Descricao = new String[] {""} ;
         P00P32_n879Caixa_Descricao = new bool[] {false} ;
         P00P32_A881Caixa_TipoDeContaCod = new String[] {""} ;
         P00P32_A877Caixa_Vencimento = new DateTime[] {DateTime.MinValue} ;
         P00P32_n877Caixa_Vencimento = new bool[] {false} ;
         P00P32_A876Caixa_Emissao = new DateTime[] {DateTime.MinValue} ;
         P00P32_n876Caixa_Emissao = new bool[] {false} ;
         P00P32_A874Caixa_Codigo = new int[1] ;
         AV28Option = "";
         P00P33_A881Caixa_TipoDeContaCod = new String[] {""} ;
         P00P33_A880Caixa_Valor = new decimal[1] ;
         P00P33_A879Caixa_Descricao = new String[] {""} ;
         P00P33_n879Caixa_Descricao = new bool[] {false} ;
         P00P33_A877Caixa_Vencimento = new DateTime[] {DateTime.MinValue} ;
         P00P33_n877Caixa_Vencimento = new bool[] {false} ;
         P00P33_A876Caixa_Emissao = new DateTime[] {DateTime.MinValue} ;
         P00P33_n876Caixa_Emissao = new bool[] {false} ;
         P00P33_A874Caixa_Codigo = new int[1] ;
         P00P33_A875Caixa_Documento = new String[] {""} ;
         P00P34_A879Caixa_Descricao = new String[] {""} ;
         P00P34_n879Caixa_Descricao = new bool[] {false} ;
         P00P34_A880Caixa_Valor = new decimal[1] ;
         P00P34_A881Caixa_TipoDeContaCod = new String[] {""} ;
         P00P34_A877Caixa_Vencimento = new DateTime[] {DateTime.MinValue} ;
         P00P34_n877Caixa_Vencimento = new bool[] {false} ;
         P00P34_A876Caixa_Emissao = new DateTime[] {DateTime.MinValue} ;
         P00P34_n876Caixa_Emissao = new bool[] {false} ;
         P00P34_A874Caixa_Codigo = new int[1] ;
         P00P34_A875Caixa_Documento = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwcaixafilterdata__default(),
            new Object[][] {
                new Object[] {
               P00P32_A875Caixa_Documento, P00P32_A880Caixa_Valor, P00P32_A879Caixa_Descricao, P00P32_n879Caixa_Descricao, P00P32_A881Caixa_TipoDeContaCod, P00P32_A877Caixa_Vencimento, P00P32_n877Caixa_Vencimento, P00P32_A876Caixa_Emissao, P00P32_n876Caixa_Emissao, P00P32_A874Caixa_Codigo
               }
               , new Object[] {
               P00P33_A881Caixa_TipoDeContaCod, P00P33_A880Caixa_Valor, P00P33_A879Caixa_Descricao, P00P33_n879Caixa_Descricao, P00P33_A877Caixa_Vencimento, P00P33_n877Caixa_Vencimento, P00P33_A876Caixa_Emissao, P00P33_n876Caixa_Emissao, P00P33_A874Caixa_Codigo, P00P33_A875Caixa_Documento
               }
               , new Object[] {
               P00P34_A879Caixa_Descricao, P00P34_n879Caixa_Descricao, P00P34_A880Caixa_Valor, P00P34_A881Caixa_TipoDeContaCod, P00P34_A877Caixa_Vencimento, P00P34_n877Caixa_Vencimento, P00P34_A876Caixa_Emissao, P00P34_n876Caixa_Emissao, P00P34_A874Caixa_Codigo, P00P34_A875Caixa_Documento
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV43DynamicFiltersOperator1 ;
      private short AV47DynamicFiltersOperator2 ;
      private short AV51DynamicFiltersOperator3 ;
      private short AV58WWCaixaDS_2_Dynamicfiltersoperator1 ;
      private short AV62WWCaixaDS_6_Dynamicfiltersoperator2 ;
      private short AV66WWCaixaDS_10_Dynamicfiltersoperator3 ;
      private int AV55GXV1 ;
      private int AV10TFCaixa_Codigo ;
      private int AV11TFCaixa_Codigo_To ;
      private int AV68WWCaixaDS_12_Tfcaixa_codigo ;
      private int AV69WWCaixaDS_13_Tfcaixa_codigo_to ;
      private int A874Caixa_Codigo ;
      private long AV36count ;
      private decimal AV22TFCaixa_Valor ;
      private decimal AV23TFCaixa_Valor_To ;
      private decimal AV80WWCaixaDS_24_Tfcaixa_valor ;
      private decimal AV81WWCaixaDS_25_Tfcaixa_valor_to ;
      private decimal A880Caixa_Valor ;
      private String AV18TFCaixa_TipoDeContaCod ;
      private String AV19TFCaixa_TipoDeContaCod_Sel ;
      private String AV76WWCaixaDS_20_Tfcaixa_tipodecontacod ;
      private String AV77WWCaixaDS_21_Tfcaixa_tipodecontacod_sel ;
      private String scmdbuf ;
      private String lV76WWCaixaDS_20_Tfcaixa_tipodecontacod ;
      private String A881Caixa_TipoDeContaCod ;
      private DateTime AV14TFCaixa_Emissao ;
      private DateTime AV15TFCaixa_Emissao_To ;
      private DateTime AV16TFCaixa_Vencimento ;
      private DateTime AV17TFCaixa_Vencimento_To ;
      private DateTime AV72WWCaixaDS_16_Tfcaixa_emissao ;
      private DateTime AV73WWCaixaDS_17_Tfcaixa_emissao_to ;
      private DateTime AV74WWCaixaDS_18_Tfcaixa_vencimento ;
      private DateTime AV75WWCaixaDS_19_Tfcaixa_vencimento_to ;
      private DateTime A876Caixa_Emissao ;
      private DateTime A877Caixa_Vencimento ;
      private bool returnInSub ;
      private bool AV45DynamicFiltersEnabled2 ;
      private bool AV49DynamicFiltersEnabled3 ;
      private bool AV60WWCaixaDS_4_Dynamicfiltersenabled2 ;
      private bool AV64WWCaixaDS_8_Dynamicfiltersenabled3 ;
      private bool BRKP32 ;
      private bool n879Caixa_Descricao ;
      private bool n877Caixa_Vencimento ;
      private bool n876Caixa_Emissao ;
      private bool BRKP34 ;
      private bool BRKP36 ;
      private String AV35OptionIndexesJson ;
      private String AV30OptionsJson ;
      private String AV33OptionsDescJson ;
      private String AV26DDOName ;
      private String AV24SearchTxt ;
      private String AV25SearchTxtTo ;
      private String AV12TFCaixa_Documento ;
      private String AV13TFCaixa_Documento_Sel ;
      private String AV20TFCaixa_Descricao ;
      private String AV21TFCaixa_Descricao_Sel ;
      private String AV42DynamicFiltersSelector1 ;
      private String AV44Caixa_Documento1 ;
      private String AV46DynamicFiltersSelector2 ;
      private String AV48Caixa_Documento2 ;
      private String AV50DynamicFiltersSelector3 ;
      private String AV52Caixa_Documento3 ;
      private String AV57WWCaixaDS_1_Dynamicfiltersselector1 ;
      private String AV59WWCaixaDS_3_Caixa_documento1 ;
      private String AV61WWCaixaDS_5_Dynamicfiltersselector2 ;
      private String AV63WWCaixaDS_7_Caixa_documento2 ;
      private String AV65WWCaixaDS_9_Dynamicfiltersselector3 ;
      private String AV67WWCaixaDS_11_Caixa_documento3 ;
      private String AV70WWCaixaDS_14_Tfcaixa_documento ;
      private String AV71WWCaixaDS_15_Tfcaixa_documento_sel ;
      private String AV78WWCaixaDS_22_Tfcaixa_descricao ;
      private String AV79WWCaixaDS_23_Tfcaixa_descricao_sel ;
      private String lV59WWCaixaDS_3_Caixa_documento1 ;
      private String lV63WWCaixaDS_7_Caixa_documento2 ;
      private String lV67WWCaixaDS_11_Caixa_documento3 ;
      private String lV70WWCaixaDS_14_Tfcaixa_documento ;
      private String lV78WWCaixaDS_22_Tfcaixa_descricao ;
      private String A875Caixa_Documento ;
      private String A879Caixa_Descricao ;
      private String AV28Option ;
      private IGxSession AV37Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00P32_A875Caixa_Documento ;
      private decimal[] P00P32_A880Caixa_Valor ;
      private String[] P00P32_A879Caixa_Descricao ;
      private bool[] P00P32_n879Caixa_Descricao ;
      private String[] P00P32_A881Caixa_TipoDeContaCod ;
      private DateTime[] P00P32_A877Caixa_Vencimento ;
      private bool[] P00P32_n877Caixa_Vencimento ;
      private DateTime[] P00P32_A876Caixa_Emissao ;
      private bool[] P00P32_n876Caixa_Emissao ;
      private int[] P00P32_A874Caixa_Codigo ;
      private String[] P00P33_A881Caixa_TipoDeContaCod ;
      private decimal[] P00P33_A880Caixa_Valor ;
      private String[] P00P33_A879Caixa_Descricao ;
      private bool[] P00P33_n879Caixa_Descricao ;
      private DateTime[] P00P33_A877Caixa_Vencimento ;
      private bool[] P00P33_n877Caixa_Vencimento ;
      private DateTime[] P00P33_A876Caixa_Emissao ;
      private bool[] P00P33_n876Caixa_Emissao ;
      private int[] P00P33_A874Caixa_Codigo ;
      private String[] P00P33_A875Caixa_Documento ;
      private String[] P00P34_A879Caixa_Descricao ;
      private bool[] P00P34_n879Caixa_Descricao ;
      private decimal[] P00P34_A880Caixa_Valor ;
      private String[] P00P34_A881Caixa_TipoDeContaCod ;
      private DateTime[] P00P34_A877Caixa_Vencimento ;
      private bool[] P00P34_n877Caixa_Vencimento ;
      private DateTime[] P00P34_A876Caixa_Emissao ;
      private bool[] P00P34_n876Caixa_Emissao ;
      private int[] P00P34_A874Caixa_Codigo ;
      private String[] P00P34_A875Caixa_Documento ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV29Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV34OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV39GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV40GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV41GridStateDynamicFilter ;
   }

   public class getwwcaixafilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00P32( IGxContext context ,
                                             String AV57WWCaixaDS_1_Dynamicfiltersselector1 ,
                                             short AV58WWCaixaDS_2_Dynamicfiltersoperator1 ,
                                             String AV59WWCaixaDS_3_Caixa_documento1 ,
                                             bool AV60WWCaixaDS_4_Dynamicfiltersenabled2 ,
                                             String AV61WWCaixaDS_5_Dynamicfiltersselector2 ,
                                             short AV62WWCaixaDS_6_Dynamicfiltersoperator2 ,
                                             String AV63WWCaixaDS_7_Caixa_documento2 ,
                                             bool AV64WWCaixaDS_8_Dynamicfiltersenabled3 ,
                                             String AV65WWCaixaDS_9_Dynamicfiltersselector3 ,
                                             short AV66WWCaixaDS_10_Dynamicfiltersoperator3 ,
                                             String AV67WWCaixaDS_11_Caixa_documento3 ,
                                             int AV68WWCaixaDS_12_Tfcaixa_codigo ,
                                             int AV69WWCaixaDS_13_Tfcaixa_codigo_to ,
                                             String AV71WWCaixaDS_15_Tfcaixa_documento_sel ,
                                             String AV70WWCaixaDS_14_Tfcaixa_documento ,
                                             DateTime AV72WWCaixaDS_16_Tfcaixa_emissao ,
                                             DateTime AV73WWCaixaDS_17_Tfcaixa_emissao_to ,
                                             DateTime AV74WWCaixaDS_18_Tfcaixa_vencimento ,
                                             DateTime AV75WWCaixaDS_19_Tfcaixa_vencimento_to ,
                                             String AV77WWCaixaDS_21_Tfcaixa_tipodecontacod_sel ,
                                             String AV76WWCaixaDS_20_Tfcaixa_tipodecontacod ,
                                             String AV79WWCaixaDS_23_Tfcaixa_descricao_sel ,
                                             String AV78WWCaixaDS_22_Tfcaixa_descricao ,
                                             decimal AV80WWCaixaDS_24_Tfcaixa_valor ,
                                             decimal AV81WWCaixaDS_25_Tfcaixa_valor_to ,
                                             String A875Caixa_Documento ,
                                             int A874Caixa_Codigo ,
                                             DateTime A876Caixa_Emissao ,
                                             DateTime A877Caixa_Vencimento ,
                                             String A881Caixa_TipoDeContaCod ,
                                             String A879Caixa_Descricao ,
                                             decimal A880Caixa_Valor )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [20] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Caixa_Documento], [Caixa_Valor], [Caixa_Descricao], [Caixa_TipoDeContaCod], [Caixa_Vencimento], [Caixa_Emissao], [Caixa_Codigo] FROM [Caixa] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV57WWCaixaDS_1_Dynamicfiltersselector1, "CAIXA_DOCUMENTO") == 0 ) && ( AV58WWCaixaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWCaixaDS_3_Caixa_documento1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV59WWCaixaDS_3_Caixa_documento1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV59WWCaixaDS_3_Caixa_documento1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWCaixaDS_1_Dynamicfiltersselector1, "CAIXA_DOCUMENTO") == 0 ) && ( AV58WWCaixaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWCaixaDS_3_Caixa_documento1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like '%' + @lV59WWCaixaDS_3_Caixa_documento1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like '%' + @lV59WWCaixaDS_3_Caixa_documento1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV60WWCaixaDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV61WWCaixaDS_5_Dynamicfiltersselector2, "CAIXA_DOCUMENTO") == 0 ) && ( AV62WWCaixaDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWCaixaDS_7_Caixa_documento2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV63WWCaixaDS_7_Caixa_documento2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV63WWCaixaDS_7_Caixa_documento2)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV60WWCaixaDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV61WWCaixaDS_5_Dynamicfiltersselector2, "CAIXA_DOCUMENTO") == 0 ) && ( AV62WWCaixaDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWCaixaDS_7_Caixa_documento2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like '%' + @lV63WWCaixaDS_7_Caixa_documento2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like '%' + @lV63WWCaixaDS_7_Caixa_documento2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV64WWCaixaDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV65WWCaixaDS_9_Dynamicfiltersselector3, "CAIXA_DOCUMENTO") == 0 ) && ( AV66WWCaixaDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWCaixaDS_11_Caixa_documento3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV67WWCaixaDS_11_Caixa_documento3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV67WWCaixaDS_11_Caixa_documento3)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV64WWCaixaDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV65WWCaixaDS_9_Dynamicfiltersselector3, "CAIXA_DOCUMENTO") == 0 ) && ( AV66WWCaixaDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWCaixaDS_11_Caixa_documento3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like '%' + @lV67WWCaixaDS_11_Caixa_documento3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like '%' + @lV67WWCaixaDS_11_Caixa_documento3)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (0==AV68WWCaixaDS_12_Tfcaixa_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Codigo] >= @AV68WWCaixaDS_12_Tfcaixa_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Codigo] >= @AV68WWCaixaDS_12_Tfcaixa_codigo)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! (0==AV69WWCaixaDS_13_Tfcaixa_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Codigo] <= @AV69WWCaixaDS_13_Tfcaixa_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Codigo] <= @AV69WWCaixaDS_13_Tfcaixa_codigo_to)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71WWCaixaDS_15_Tfcaixa_documento_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWCaixaDS_14_Tfcaixa_documento)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV70WWCaixaDS_14_Tfcaixa_documento)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV70WWCaixaDS_14_Tfcaixa_documento)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWCaixaDS_15_Tfcaixa_documento_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] = @AV71WWCaixaDS_15_Tfcaixa_documento_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] = @AV71WWCaixaDS_15_Tfcaixa_documento_sel)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV72WWCaixaDS_16_Tfcaixa_emissao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Emissao] >= @AV72WWCaixaDS_16_Tfcaixa_emissao)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Emissao] >= @AV72WWCaixaDS_16_Tfcaixa_emissao)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV73WWCaixaDS_17_Tfcaixa_emissao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Emissao] <= @AV73WWCaixaDS_17_Tfcaixa_emissao_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Emissao] <= @AV73WWCaixaDS_17_Tfcaixa_emissao_to)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV74WWCaixaDS_18_Tfcaixa_vencimento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Vencimento] >= @AV74WWCaixaDS_18_Tfcaixa_vencimento)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Vencimento] >= @AV74WWCaixaDS_18_Tfcaixa_vencimento)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! (DateTime.MinValue==AV75WWCaixaDS_19_Tfcaixa_vencimento_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Vencimento] <= @AV75WWCaixaDS_19_Tfcaixa_vencimento_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Vencimento] <= @AV75WWCaixaDS_19_Tfcaixa_vencimento_to)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV77WWCaixaDS_21_Tfcaixa_tipodecontacod_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWCaixaDS_20_Tfcaixa_tipodecontacod)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_TipoDeContaCod] like @lV76WWCaixaDS_20_Tfcaixa_tipodecontacod)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_TipoDeContaCod] like @lV76WWCaixaDS_20_Tfcaixa_tipodecontacod)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWCaixaDS_21_Tfcaixa_tipodecontacod_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_TipoDeContaCod] = @AV77WWCaixaDS_21_Tfcaixa_tipodecontacod_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_TipoDeContaCod] = @AV77WWCaixaDS_21_Tfcaixa_tipodecontacod_sel)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79WWCaixaDS_23_Tfcaixa_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWCaixaDS_22_Tfcaixa_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Descricao] like @lV78WWCaixaDS_22_Tfcaixa_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Descricao] like @lV78WWCaixaDS_22_Tfcaixa_descricao)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWCaixaDS_23_Tfcaixa_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Descricao] = @AV79WWCaixaDS_23_Tfcaixa_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Descricao] = @AV79WWCaixaDS_23_Tfcaixa_descricao_sel)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV80WWCaixaDS_24_Tfcaixa_valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Valor] >= @AV80WWCaixaDS_24_Tfcaixa_valor)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Valor] >= @AV80WWCaixaDS_24_Tfcaixa_valor)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV81WWCaixaDS_25_Tfcaixa_valor_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Valor] <= @AV81WWCaixaDS_25_Tfcaixa_valor_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Valor] <= @AV81WWCaixaDS_25_Tfcaixa_valor_to)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [Caixa_Documento]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00P33( IGxContext context ,
                                             String AV57WWCaixaDS_1_Dynamicfiltersselector1 ,
                                             short AV58WWCaixaDS_2_Dynamicfiltersoperator1 ,
                                             String AV59WWCaixaDS_3_Caixa_documento1 ,
                                             bool AV60WWCaixaDS_4_Dynamicfiltersenabled2 ,
                                             String AV61WWCaixaDS_5_Dynamicfiltersselector2 ,
                                             short AV62WWCaixaDS_6_Dynamicfiltersoperator2 ,
                                             String AV63WWCaixaDS_7_Caixa_documento2 ,
                                             bool AV64WWCaixaDS_8_Dynamicfiltersenabled3 ,
                                             String AV65WWCaixaDS_9_Dynamicfiltersselector3 ,
                                             short AV66WWCaixaDS_10_Dynamicfiltersoperator3 ,
                                             String AV67WWCaixaDS_11_Caixa_documento3 ,
                                             int AV68WWCaixaDS_12_Tfcaixa_codigo ,
                                             int AV69WWCaixaDS_13_Tfcaixa_codigo_to ,
                                             String AV71WWCaixaDS_15_Tfcaixa_documento_sel ,
                                             String AV70WWCaixaDS_14_Tfcaixa_documento ,
                                             DateTime AV72WWCaixaDS_16_Tfcaixa_emissao ,
                                             DateTime AV73WWCaixaDS_17_Tfcaixa_emissao_to ,
                                             DateTime AV74WWCaixaDS_18_Tfcaixa_vencimento ,
                                             DateTime AV75WWCaixaDS_19_Tfcaixa_vencimento_to ,
                                             String AV77WWCaixaDS_21_Tfcaixa_tipodecontacod_sel ,
                                             String AV76WWCaixaDS_20_Tfcaixa_tipodecontacod ,
                                             String AV79WWCaixaDS_23_Tfcaixa_descricao_sel ,
                                             String AV78WWCaixaDS_22_Tfcaixa_descricao ,
                                             decimal AV80WWCaixaDS_24_Tfcaixa_valor ,
                                             decimal AV81WWCaixaDS_25_Tfcaixa_valor_to ,
                                             String A875Caixa_Documento ,
                                             int A874Caixa_Codigo ,
                                             DateTime A876Caixa_Emissao ,
                                             DateTime A877Caixa_Vencimento ,
                                             String A881Caixa_TipoDeContaCod ,
                                             String A879Caixa_Descricao ,
                                             decimal A880Caixa_Valor )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [20] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [Caixa_TipoDeContaCod], [Caixa_Valor], [Caixa_Descricao], [Caixa_Vencimento], [Caixa_Emissao], [Caixa_Codigo], [Caixa_Documento] FROM [Caixa] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV57WWCaixaDS_1_Dynamicfiltersselector1, "CAIXA_DOCUMENTO") == 0 ) && ( AV58WWCaixaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWCaixaDS_3_Caixa_documento1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV59WWCaixaDS_3_Caixa_documento1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV59WWCaixaDS_3_Caixa_documento1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWCaixaDS_1_Dynamicfiltersselector1, "CAIXA_DOCUMENTO") == 0 ) && ( AV58WWCaixaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWCaixaDS_3_Caixa_documento1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like '%' + @lV59WWCaixaDS_3_Caixa_documento1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like '%' + @lV59WWCaixaDS_3_Caixa_documento1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV60WWCaixaDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV61WWCaixaDS_5_Dynamicfiltersselector2, "CAIXA_DOCUMENTO") == 0 ) && ( AV62WWCaixaDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWCaixaDS_7_Caixa_documento2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV63WWCaixaDS_7_Caixa_documento2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV63WWCaixaDS_7_Caixa_documento2)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV60WWCaixaDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV61WWCaixaDS_5_Dynamicfiltersselector2, "CAIXA_DOCUMENTO") == 0 ) && ( AV62WWCaixaDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWCaixaDS_7_Caixa_documento2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like '%' + @lV63WWCaixaDS_7_Caixa_documento2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like '%' + @lV63WWCaixaDS_7_Caixa_documento2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV64WWCaixaDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV65WWCaixaDS_9_Dynamicfiltersselector3, "CAIXA_DOCUMENTO") == 0 ) && ( AV66WWCaixaDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWCaixaDS_11_Caixa_documento3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV67WWCaixaDS_11_Caixa_documento3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV67WWCaixaDS_11_Caixa_documento3)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV64WWCaixaDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV65WWCaixaDS_9_Dynamicfiltersselector3, "CAIXA_DOCUMENTO") == 0 ) && ( AV66WWCaixaDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWCaixaDS_11_Caixa_documento3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like '%' + @lV67WWCaixaDS_11_Caixa_documento3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like '%' + @lV67WWCaixaDS_11_Caixa_documento3)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (0==AV68WWCaixaDS_12_Tfcaixa_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Codigo] >= @AV68WWCaixaDS_12_Tfcaixa_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Codigo] >= @AV68WWCaixaDS_12_Tfcaixa_codigo)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! (0==AV69WWCaixaDS_13_Tfcaixa_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Codigo] <= @AV69WWCaixaDS_13_Tfcaixa_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Codigo] <= @AV69WWCaixaDS_13_Tfcaixa_codigo_to)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71WWCaixaDS_15_Tfcaixa_documento_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWCaixaDS_14_Tfcaixa_documento)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV70WWCaixaDS_14_Tfcaixa_documento)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV70WWCaixaDS_14_Tfcaixa_documento)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWCaixaDS_15_Tfcaixa_documento_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] = @AV71WWCaixaDS_15_Tfcaixa_documento_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] = @AV71WWCaixaDS_15_Tfcaixa_documento_sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV72WWCaixaDS_16_Tfcaixa_emissao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Emissao] >= @AV72WWCaixaDS_16_Tfcaixa_emissao)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Emissao] >= @AV72WWCaixaDS_16_Tfcaixa_emissao)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV73WWCaixaDS_17_Tfcaixa_emissao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Emissao] <= @AV73WWCaixaDS_17_Tfcaixa_emissao_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Emissao] <= @AV73WWCaixaDS_17_Tfcaixa_emissao_to)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV74WWCaixaDS_18_Tfcaixa_vencimento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Vencimento] >= @AV74WWCaixaDS_18_Tfcaixa_vencimento)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Vencimento] >= @AV74WWCaixaDS_18_Tfcaixa_vencimento)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! (DateTime.MinValue==AV75WWCaixaDS_19_Tfcaixa_vencimento_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Vencimento] <= @AV75WWCaixaDS_19_Tfcaixa_vencimento_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Vencimento] <= @AV75WWCaixaDS_19_Tfcaixa_vencimento_to)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV77WWCaixaDS_21_Tfcaixa_tipodecontacod_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWCaixaDS_20_Tfcaixa_tipodecontacod)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_TipoDeContaCod] like @lV76WWCaixaDS_20_Tfcaixa_tipodecontacod)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_TipoDeContaCod] like @lV76WWCaixaDS_20_Tfcaixa_tipodecontacod)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWCaixaDS_21_Tfcaixa_tipodecontacod_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_TipoDeContaCod] = @AV77WWCaixaDS_21_Tfcaixa_tipodecontacod_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_TipoDeContaCod] = @AV77WWCaixaDS_21_Tfcaixa_tipodecontacod_sel)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79WWCaixaDS_23_Tfcaixa_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWCaixaDS_22_Tfcaixa_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Descricao] like @lV78WWCaixaDS_22_Tfcaixa_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Descricao] like @lV78WWCaixaDS_22_Tfcaixa_descricao)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWCaixaDS_23_Tfcaixa_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Descricao] = @AV79WWCaixaDS_23_Tfcaixa_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Descricao] = @AV79WWCaixaDS_23_Tfcaixa_descricao_sel)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV80WWCaixaDS_24_Tfcaixa_valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Valor] >= @AV80WWCaixaDS_24_Tfcaixa_valor)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Valor] >= @AV80WWCaixaDS_24_Tfcaixa_valor)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV81WWCaixaDS_25_Tfcaixa_valor_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Valor] <= @AV81WWCaixaDS_25_Tfcaixa_valor_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Valor] <= @AV81WWCaixaDS_25_Tfcaixa_valor_to)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [Caixa_TipoDeContaCod]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00P34( IGxContext context ,
                                             String AV57WWCaixaDS_1_Dynamicfiltersselector1 ,
                                             short AV58WWCaixaDS_2_Dynamicfiltersoperator1 ,
                                             String AV59WWCaixaDS_3_Caixa_documento1 ,
                                             bool AV60WWCaixaDS_4_Dynamicfiltersenabled2 ,
                                             String AV61WWCaixaDS_5_Dynamicfiltersselector2 ,
                                             short AV62WWCaixaDS_6_Dynamicfiltersoperator2 ,
                                             String AV63WWCaixaDS_7_Caixa_documento2 ,
                                             bool AV64WWCaixaDS_8_Dynamicfiltersenabled3 ,
                                             String AV65WWCaixaDS_9_Dynamicfiltersselector3 ,
                                             short AV66WWCaixaDS_10_Dynamicfiltersoperator3 ,
                                             String AV67WWCaixaDS_11_Caixa_documento3 ,
                                             int AV68WWCaixaDS_12_Tfcaixa_codigo ,
                                             int AV69WWCaixaDS_13_Tfcaixa_codigo_to ,
                                             String AV71WWCaixaDS_15_Tfcaixa_documento_sel ,
                                             String AV70WWCaixaDS_14_Tfcaixa_documento ,
                                             DateTime AV72WWCaixaDS_16_Tfcaixa_emissao ,
                                             DateTime AV73WWCaixaDS_17_Tfcaixa_emissao_to ,
                                             DateTime AV74WWCaixaDS_18_Tfcaixa_vencimento ,
                                             DateTime AV75WWCaixaDS_19_Tfcaixa_vencimento_to ,
                                             String AV77WWCaixaDS_21_Tfcaixa_tipodecontacod_sel ,
                                             String AV76WWCaixaDS_20_Tfcaixa_tipodecontacod ,
                                             String AV79WWCaixaDS_23_Tfcaixa_descricao_sel ,
                                             String AV78WWCaixaDS_22_Tfcaixa_descricao ,
                                             decimal AV80WWCaixaDS_24_Tfcaixa_valor ,
                                             decimal AV81WWCaixaDS_25_Tfcaixa_valor_to ,
                                             String A875Caixa_Documento ,
                                             int A874Caixa_Codigo ,
                                             DateTime A876Caixa_Emissao ,
                                             DateTime A877Caixa_Vencimento ,
                                             String A881Caixa_TipoDeContaCod ,
                                             String A879Caixa_Descricao ,
                                             decimal A880Caixa_Valor )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [20] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT [Caixa_Descricao], [Caixa_Valor], [Caixa_TipoDeContaCod], [Caixa_Vencimento], [Caixa_Emissao], [Caixa_Codigo], [Caixa_Documento] FROM [Caixa] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV57WWCaixaDS_1_Dynamicfiltersselector1, "CAIXA_DOCUMENTO") == 0 ) && ( AV58WWCaixaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWCaixaDS_3_Caixa_documento1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV59WWCaixaDS_3_Caixa_documento1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV59WWCaixaDS_3_Caixa_documento1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWCaixaDS_1_Dynamicfiltersselector1, "CAIXA_DOCUMENTO") == 0 ) && ( AV58WWCaixaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWCaixaDS_3_Caixa_documento1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like '%' + @lV59WWCaixaDS_3_Caixa_documento1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like '%' + @lV59WWCaixaDS_3_Caixa_documento1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( AV60WWCaixaDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV61WWCaixaDS_5_Dynamicfiltersselector2, "CAIXA_DOCUMENTO") == 0 ) && ( AV62WWCaixaDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWCaixaDS_7_Caixa_documento2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV63WWCaixaDS_7_Caixa_documento2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV63WWCaixaDS_7_Caixa_documento2)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( AV60WWCaixaDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV61WWCaixaDS_5_Dynamicfiltersselector2, "CAIXA_DOCUMENTO") == 0 ) && ( AV62WWCaixaDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWCaixaDS_7_Caixa_documento2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like '%' + @lV63WWCaixaDS_7_Caixa_documento2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like '%' + @lV63WWCaixaDS_7_Caixa_documento2)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV64WWCaixaDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV65WWCaixaDS_9_Dynamicfiltersselector3, "CAIXA_DOCUMENTO") == 0 ) && ( AV66WWCaixaDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWCaixaDS_11_Caixa_documento3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV67WWCaixaDS_11_Caixa_documento3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV67WWCaixaDS_11_Caixa_documento3)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV64WWCaixaDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV65WWCaixaDS_9_Dynamicfiltersselector3, "CAIXA_DOCUMENTO") == 0 ) && ( AV66WWCaixaDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWCaixaDS_11_Caixa_documento3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like '%' + @lV67WWCaixaDS_11_Caixa_documento3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like '%' + @lV67WWCaixaDS_11_Caixa_documento3)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( ! (0==AV68WWCaixaDS_12_Tfcaixa_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Codigo] >= @AV68WWCaixaDS_12_Tfcaixa_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Codigo] >= @AV68WWCaixaDS_12_Tfcaixa_codigo)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( ! (0==AV69WWCaixaDS_13_Tfcaixa_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Codigo] <= @AV69WWCaixaDS_13_Tfcaixa_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Codigo] <= @AV69WWCaixaDS_13_Tfcaixa_codigo_to)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71WWCaixaDS_15_Tfcaixa_documento_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWCaixaDS_14_Tfcaixa_documento)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV70WWCaixaDS_14_Tfcaixa_documento)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV70WWCaixaDS_14_Tfcaixa_documento)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWCaixaDS_15_Tfcaixa_documento_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] = @AV71WWCaixaDS_15_Tfcaixa_documento_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] = @AV71WWCaixaDS_15_Tfcaixa_documento_sel)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV72WWCaixaDS_16_Tfcaixa_emissao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Emissao] >= @AV72WWCaixaDS_16_Tfcaixa_emissao)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Emissao] >= @AV72WWCaixaDS_16_Tfcaixa_emissao)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV73WWCaixaDS_17_Tfcaixa_emissao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Emissao] <= @AV73WWCaixaDS_17_Tfcaixa_emissao_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Emissao] <= @AV73WWCaixaDS_17_Tfcaixa_emissao_to)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV74WWCaixaDS_18_Tfcaixa_vencimento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Vencimento] >= @AV74WWCaixaDS_18_Tfcaixa_vencimento)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Vencimento] >= @AV74WWCaixaDS_18_Tfcaixa_vencimento)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ! (DateTime.MinValue==AV75WWCaixaDS_19_Tfcaixa_vencimento_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Vencimento] <= @AV75WWCaixaDS_19_Tfcaixa_vencimento_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Vencimento] <= @AV75WWCaixaDS_19_Tfcaixa_vencimento_to)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV77WWCaixaDS_21_Tfcaixa_tipodecontacod_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWCaixaDS_20_Tfcaixa_tipodecontacod)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_TipoDeContaCod] like @lV76WWCaixaDS_20_Tfcaixa_tipodecontacod)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_TipoDeContaCod] like @lV76WWCaixaDS_20_Tfcaixa_tipodecontacod)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWCaixaDS_21_Tfcaixa_tipodecontacod_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_TipoDeContaCod] = @AV77WWCaixaDS_21_Tfcaixa_tipodecontacod_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_TipoDeContaCod] = @AV77WWCaixaDS_21_Tfcaixa_tipodecontacod_sel)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79WWCaixaDS_23_Tfcaixa_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWCaixaDS_22_Tfcaixa_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Descricao] like @lV78WWCaixaDS_22_Tfcaixa_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Descricao] like @lV78WWCaixaDS_22_Tfcaixa_descricao)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWCaixaDS_23_Tfcaixa_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Descricao] = @AV79WWCaixaDS_23_Tfcaixa_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Descricao] = @AV79WWCaixaDS_23_Tfcaixa_descricao_sel)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV80WWCaixaDS_24_Tfcaixa_valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Valor] >= @AV80WWCaixaDS_24_Tfcaixa_valor)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Valor] >= @AV80WWCaixaDS_24_Tfcaixa_valor)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV81WWCaixaDS_25_Tfcaixa_valor_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Valor] <= @AV81WWCaixaDS_25_Tfcaixa_valor_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Valor] <= @AV81WWCaixaDS_25_Tfcaixa_valor_to)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [Caixa_Descricao]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00P32(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (decimal)dynConstraints[31] );
               case 1 :
                     return conditional_P00P33(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (decimal)dynConstraints[31] );
               case 2 :
                     return conditional_P00P34(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (decimal)dynConstraints[31] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00P32 ;
          prmP00P32 = new Object[] {
          new Object[] {"@lV59WWCaixaDS_3_Caixa_documento1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV59WWCaixaDS_3_Caixa_documento1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV63WWCaixaDS_7_Caixa_documento2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV63WWCaixaDS_7_Caixa_documento2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV67WWCaixaDS_11_Caixa_documento3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV67WWCaixaDS_11_Caixa_documento3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV68WWCaixaDS_12_Tfcaixa_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV69WWCaixaDS_13_Tfcaixa_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV70WWCaixaDS_14_Tfcaixa_documento",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV71WWCaixaDS_15_Tfcaixa_documento_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV72WWCaixaDS_16_Tfcaixa_emissao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV73WWCaixaDS_17_Tfcaixa_emissao_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV74WWCaixaDS_18_Tfcaixa_vencimento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV75WWCaixaDS_19_Tfcaixa_vencimento_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV76WWCaixaDS_20_Tfcaixa_tipodecontacod",SqlDbType.Char,20,0} ,
          new Object[] {"@AV77WWCaixaDS_21_Tfcaixa_tipodecontacod_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV78WWCaixaDS_22_Tfcaixa_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV79WWCaixaDS_23_Tfcaixa_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV80WWCaixaDS_24_Tfcaixa_valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV81WWCaixaDS_25_Tfcaixa_valor_to",SqlDbType.Decimal,18,5}
          } ;
          Object[] prmP00P33 ;
          prmP00P33 = new Object[] {
          new Object[] {"@lV59WWCaixaDS_3_Caixa_documento1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV59WWCaixaDS_3_Caixa_documento1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV63WWCaixaDS_7_Caixa_documento2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV63WWCaixaDS_7_Caixa_documento2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV67WWCaixaDS_11_Caixa_documento3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV67WWCaixaDS_11_Caixa_documento3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV68WWCaixaDS_12_Tfcaixa_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV69WWCaixaDS_13_Tfcaixa_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV70WWCaixaDS_14_Tfcaixa_documento",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV71WWCaixaDS_15_Tfcaixa_documento_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV72WWCaixaDS_16_Tfcaixa_emissao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV73WWCaixaDS_17_Tfcaixa_emissao_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV74WWCaixaDS_18_Tfcaixa_vencimento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV75WWCaixaDS_19_Tfcaixa_vencimento_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV76WWCaixaDS_20_Tfcaixa_tipodecontacod",SqlDbType.Char,20,0} ,
          new Object[] {"@AV77WWCaixaDS_21_Tfcaixa_tipodecontacod_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV78WWCaixaDS_22_Tfcaixa_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV79WWCaixaDS_23_Tfcaixa_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV80WWCaixaDS_24_Tfcaixa_valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV81WWCaixaDS_25_Tfcaixa_valor_to",SqlDbType.Decimal,18,5}
          } ;
          Object[] prmP00P34 ;
          prmP00P34 = new Object[] {
          new Object[] {"@lV59WWCaixaDS_3_Caixa_documento1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV59WWCaixaDS_3_Caixa_documento1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV63WWCaixaDS_7_Caixa_documento2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV63WWCaixaDS_7_Caixa_documento2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV67WWCaixaDS_11_Caixa_documento3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV67WWCaixaDS_11_Caixa_documento3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV68WWCaixaDS_12_Tfcaixa_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV69WWCaixaDS_13_Tfcaixa_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV70WWCaixaDS_14_Tfcaixa_documento",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV71WWCaixaDS_15_Tfcaixa_documento_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV72WWCaixaDS_16_Tfcaixa_emissao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV73WWCaixaDS_17_Tfcaixa_emissao_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV74WWCaixaDS_18_Tfcaixa_vencimento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV75WWCaixaDS_19_Tfcaixa_vencimento_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV76WWCaixaDS_20_Tfcaixa_tipodecontacod",SqlDbType.Char,20,0} ,
          new Object[] {"@AV77WWCaixaDS_21_Tfcaixa_tipodecontacod_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV78WWCaixaDS_22_Tfcaixa_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV79WWCaixaDS_23_Tfcaixa_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV80WWCaixaDS_24_Tfcaixa_valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV81WWCaixaDS_25_Tfcaixa_valor_to",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00P32", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00P32,100,0,true,false )
             ,new CursorDef("P00P33", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00P33,100,0,true,false )
             ,new CursorDef("P00P34", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00P34,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 20) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 20) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[38]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[39]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[38]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[39]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[38]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[39]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwcaixafilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwcaixafilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwcaixafilterdata") )
          {
             return  ;
          }
          getwwcaixafilterdata worker = new getwwcaixafilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
