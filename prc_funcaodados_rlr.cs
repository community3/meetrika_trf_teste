/*
               File: PRC_FuncaoDados_RLR
        Description: Funcao Dados_RLR
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:49.89
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_funcaodados_rlr : GXProcedure
   {
      public prc_funcaodados_rlr( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_funcaodados_rlr( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_FuncaoDados_Codigo ,
                           out short aP1_FuncaoDados_RLR )
      {
         this.AV8FuncaoDados_Codigo = aP0_FuncaoDados_Codigo;
         this.AV9FuncaoDados_RLR = 0 ;
         initialize();
         executePrivate();
         aP1_FuncaoDados_RLR=this.AV9FuncaoDados_RLR;
      }

      public short executeUdp( int aP0_FuncaoDados_Codigo )
      {
         this.AV8FuncaoDados_Codigo = aP0_FuncaoDados_Codigo;
         this.AV9FuncaoDados_RLR = 0 ;
         initialize();
         executePrivate();
         aP1_FuncaoDados_RLR=this.AV9FuncaoDados_RLR;
         return AV9FuncaoDados_RLR ;
      }

      public void executeSubmit( int aP0_FuncaoDados_Codigo ,
                                 out short aP1_FuncaoDados_RLR )
      {
         prc_funcaodados_rlr objprc_funcaodados_rlr;
         objprc_funcaodados_rlr = new prc_funcaodados_rlr();
         objprc_funcaodados_rlr.AV8FuncaoDados_Codigo = aP0_FuncaoDados_Codigo;
         objprc_funcaodados_rlr.AV9FuncaoDados_RLR = 0 ;
         objprc_funcaodados_rlr.context.SetSubmitInitialConfig(context);
         objprc_funcaodados_rlr.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_funcaodados_rlr);
         aP1_FuncaoDados_RLR=this.AV9FuncaoDados_RLR;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_funcaodados_rlr)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV9FuncaoDados_RLR = 0;
         /* Using cursor P00222 */
         pr_default.execute(0, new Object[] {AV8FuncaoDados_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A368FuncaoDados_Codigo = P00222_A368FuncaoDados_Codigo[0];
            A391FuncaoDados_FuncaoDadosCod = P00222_A391FuncaoDados_FuncaoDadosCod[0];
            n391FuncaoDados_FuncaoDadosCod = P00222_n391FuncaoDados_FuncaoDadosCod[0];
            A1025FuncaoDados_RAImp = P00222_A1025FuncaoDados_RAImp[0];
            n1025FuncaoDados_RAImp = P00222_n1025FuncaoDados_RAImp[0];
            if ( (0==A391FuncaoDados_FuncaoDadosCod) )
            {
               /* Optimized group. */
               /* Using cursor P00223 */
               pr_default.execute(1, new Object[] {A368FuncaoDados_Codigo});
               cV9FuncaoDados_RLR = P00223_AV9FuncaoDados_RLR[0];
               pr_default.close(1);
               AV9FuncaoDados_RLR = (short)(AV9FuncaoDados_RLR+cV9FuncaoDados_RLR*1);
               /* End optimized group. */
            }
            else
            {
               AV10FuncaoDados_FuncaoDadosCod = A391FuncaoDados_FuncaoDadosCod;
               /* Execute user subroutine: 'FUNCAOEXTERNA' */
               S111 ();
               if ( returnInSub )
               {
                  pr_default.close(0);
                  this.cleanup();
                  if (true) return;
               }
            }
            if ( (0==AV9FuncaoDados_RLR) )
            {
               AV9FuncaoDados_RLR = A1025FuncaoDados_RAImp;
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'FUNCAOEXTERNA' Routine */
         /* Using cursor P00224 */
         pr_default.execute(2, new Object[] {AV10FuncaoDados_FuncaoDadosCod});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A368FuncaoDados_Codigo = P00224_A368FuncaoDados_Codigo[0];
            GXt_int1 = A375FuncaoDados_RLR;
            new prc_funcaodados_rlr(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int1) ;
            A375FuncaoDados_RLR = GXt_int1;
            AV9FuncaoDados_RLR = A375FuncaoDados_RLR;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
         returnInSub = true;
         if (true) return;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00222_A368FuncaoDados_Codigo = new int[1] ;
         P00222_A391FuncaoDados_FuncaoDadosCod = new int[1] ;
         P00222_n391FuncaoDados_FuncaoDadosCod = new bool[] {false} ;
         P00222_A1025FuncaoDados_RAImp = new short[1] ;
         P00222_n1025FuncaoDados_RAImp = new bool[] {false} ;
         P00223_AV9FuncaoDados_RLR = new short[1] ;
         P00224_A368FuncaoDados_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_funcaodados_rlr__default(),
            new Object[][] {
                new Object[] {
               P00222_A368FuncaoDados_Codigo, P00222_A391FuncaoDados_FuncaoDadosCod, P00222_n391FuncaoDados_FuncaoDadosCod, P00222_A1025FuncaoDados_RAImp, P00222_n1025FuncaoDados_RAImp
               }
               , new Object[] {
               P00223_AV9FuncaoDados_RLR
               }
               , new Object[] {
               P00224_A368FuncaoDados_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV9FuncaoDados_RLR ;
      private short A1025FuncaoDados_RAImp ;
      private short cV9FuncaoDados_RLR ;
      private short A375FuncaoDados_RLR ;
      private short GXt_int1 ;
      private int AV8FuncaoDados_Codigo ;
      private int A368FuncaoDados_Codigo ;
      private int A391FuncaoDados_FuncaoDadosCod ;
      private int AV10FuncaoDados_FuncaoDadosCod ;
      private String scmdbuf ;
      private bool n391FuncaoDados_FuncaoDadosCod ;
      private bool n1025FuncaoDados_RAImp ;
      private bool returnInSub ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00222_A368FuncaoDados_Codigo ;
      private int[] P00222_A391FuncaoDados_FuncaoDadosCod ;
      private bool[] P00222_n391FuncaoDados_FuncaoDadosCod ;
      private short[] P00222_A1025FuncaoDados_RAImp ;
      private bool[] P00222_n1025FuncaoDados_RAImp ;
      private short[] P00223_AV9FuncaoDados_RLR ;
      private int[] P00224_A368FuncaoDados_Codigo ;
      private short aP1_FuncaoDados_RLR ;
   }

   public class prc_funcaodados_rlr__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00222 ;
          prmP00222 = new Object[] {
          new Object[] {"@AV8FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00223 ;
          prmP00223 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00224 ;
          prmP00224 = new Object[] {
          new Object[] {"@AV10FuncaoDados_FuncaoDadosCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00222", "SELECT [FuncaoDados_Codigo], [FuncaoDados_FuncaoDadosCod], [FuncaoDados_RAImp] FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @AV8FuncaoDados_Codigo ORDER BY [FuncaoDados_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00222,1,0,true,true )
             ,new CursorDef("P00223", "SELECT COUNT(*) FROM [FuncaoDadosTabela] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDados_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00223,1,0,true,false )
             ,new CursorDef("P00224", "SELECT [FuncaoDados_Codigo] FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @AV10FuncaoDados_FuncaoDadosCod ORDER BY [FuncaoDados_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00224,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
