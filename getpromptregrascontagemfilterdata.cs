/*
               File: GetPromptRegrasContagemFilterData
        Description: Get Prompt Regras Contagem Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:54:11.71
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptregrascontagemfilterdata : GXProcedure
   {
      public getpromptregrascontagemfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptregrascontagemfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV22DDOName = aP0_DDOName;
         this.AV20SearchTxt = aP1_SearchTxt;
         this.AV21SearchTxtTo = aP2_SearchTxtTo;
         this.AV26OptionsJson = "" ;
         this.AV29OptionsDescJson = "" ;
         this.AV31OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV22DDOName = aP0_DDOName;
         this.AV20SearchTxt = aP1_SearchTxt;
         this.AV21SearchTxtTo = aP2_SearchTxtTo;
         this.AV26OptionsJson = "" ;
         this.AV29OptionsDescJson = "" ;
         this.AV31OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
         return AV31OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptregrascontagemfilterdata objgetpromptregrascontagemfilterdata;
         objgetpromptregrascontagemfilterdata = new getpromptregrascontagemfilterdata();
         objgetpromptregrascontagemfilterdata.AV22DDOName = aP0_DDOName;
         objgetpromptregrascontagemfilterdata.AV20SearchTxt = aP1_SearchTxt;
         objgetpromptregrascontagemfilterdata.AV21SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptregrascontagemfilterdata.AV26OptionsJson = "" ;
         objgetpromptregrascontagemfilterdata.AV29OptionsDescJson = "" ;
         objgetpromptregrascontagemfilterdata.AV31OptionIndexesJson = "" ;
         objgetpromptregrascontagemfilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptregrascontagemfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptregrascontagemfilterdata);
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptregrascontagemfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV25Options = (IGxCollection)(new GxSimpleCollection());
         AV28OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV30OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV22DDOName), "DDO_REGRASCONTAGEM_REGRA") == 0 )
         {
            /* Execute user subroutine: 'LOADREGRASCONTAGEM_REGRAOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV22DDOName), "DDO_REGRASCONTAGEM_RESPONSAVEL") == 0 )
         {
            /* Execute user subroutine: 'LOADREGRASCONTAGEM_RESPONSAVELOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV22DDOName), "DDO_REGRASCONTAGEM_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADREGRASCONTAGEM_DESCRICAOOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV26OptionsJson = AV25Options.ToJSonString(false);
         AV29OptionsDescJson = AV28OptionsDesc.ToJSonString(false);
         AV31OptionIndexesJson = AV30OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV33Session.Get("PromptRegrasContagemGridState"), "") == 0 )
         {
            AV35GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptRegrasContagemGridState"), "");
         }
         else
         {
            AV35GridState.FromXml(AV33Session.Get("PromptRegrasContagemGridState"), "");
         }
         AV58GXV1 = 1;
         while ( AV58GXV1 <= AV35GridState.gxTpr_Filtervalues.Count )
         {
            AV36GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV35GridState.gxTpr_Filtervalues.Item(AV58GXV1));
            if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "REGRASCONTAGEM_AREATRABALHOCOD") == 0 )
            {
               AV38RegrasContagem_AreaTrabalhoCod = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFREGRASCONTAGEM_REGRA") == 0 )
            {
               AV10TFRegrasContagem_Regra = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFREGRASCONTAGEM_REGRA_SEL") == 0 )
            {
               AV11TFRegrasContagem_Regra_Sel = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFREGRASCONTAGEM_DATA") == 0 )
            {
               AV12TFRegrasContagem_Data = context.localUtil.CToD( AV36GridStateFilterValue.gxTpr_Value, 2);
               AV13TFRegrasContagem_Data_To = context.localUtil.CToD( AV36GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFREGRASCONTAGEM_VALIDADE") == 0 )
            {
               AV14TFRegrasContagem_Validade = context.localUtil.CToD( AV36GridStateFilterValue.gxTpr_Value, 2);
               AV15TFRegrasContagem_Validade_To = context.localUtil.CToD( AV36GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFREGRASCONTAGEM_RESPONSAVEL") == 0 )
            {
               AV16TFRegrasContagem_Responsavel = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFREGRASCONTAGEM_RESPONSAVEL_SEL") == 0 )
            {
               AV17TFRegrasContagem_Responsavel_Sel = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFREGRASCONTAGEM_DESCRICAO") == 0 )
            {
               AV18TFRegrasContagem_Descricao = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFREGRASCONTAGEM_DESCRICAO_SEL") == 0 )
            {
               AV19TFRegrasContagem_Descricao_Sel = AV36GridStateFilterValue.gxTpr_Value;
            }
            AV58GXV1 = (int)(AV58GXV1+1);
         }
         if ( AV35GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV37GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV35GridState.gxTpr_Dynamicfilters.Item(1));
            AV39DynamicFiltersSelector1 = AV37GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "REGRASCONTAGEM_REGRA") == 0 )
            {
               AV40RegrasContagem_Regra1 = AV37GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "REGRASCONTAGEM_DATA") == 0 )
            {
               AV41RegrasContagem_Data1 = context.localUtil.CToD( AV37GridStateDynamicFilter.gxTpr_Value, 2);
               AV42RegrasContagem_Data_To1 = context.localUtil.CToD( AV37GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "REGRASCONTAGEM_RESPONSAVEL") == 0 )
            {
               AV43RegrasContagem_Responsavel1 = AV37GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV35GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV44DynamicFiltersEnabled2 = true;
               AV37GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV35GridState.gxTpr_Dynamicfilters.Item(2));
               AV45DynamicFiltersSelector2 = AV37GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "REGRASCONTAGEM_REGRA") == 0 )
               {
                  AV46RegrasContagem_Regra2 = AV37GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "REGRASCONTAGEM_DATA") == 0 )
               {
                  AV47RegrasContagem_Data2 = context.localUtil.CToD( AV37GridStateDynamicFilter.gxTpr_Value, 2);
                  AV48RegrasContagem_Data_To2 = context.localUtil.CToD( AV37GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "REGRASCONTAGEM_RESPONSAVEL") == 0 )
               {
                  AV49RegrasContagem_Responsavel2 = AV37GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV35GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV50DynamicFiltersEnabled3 = true;
                  AV37GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV35GridState.gxTpr_Dynamicfilters.Item(3));
                  AV51DynamicFiltersSelector3 = AV37GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "REGRASCONTAGEM_REGRA") == 0 )
                  {
                     AV52RegrasContagem_Regra3 = AV37GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "REGRASCONTAGEM_DATA") == 0 )
                  {
                     AV53RegrasContagem_Data3 = context.localUtil.CToD( AV37GridStateDynamicFilter.gxTpr_Value, 2);
                     AV54RegrasContagem_Data_To3 = context.localUtil.CToD( AV37GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "REGRASCONTAGEM_RESPONSAVEL") == 0 )
                  {
                     AV55RegrasContagem_Responsavel3 = AV37GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADREGRASCONTAGEM_REGRAOPTIONS' Routine */
         AV10TFRegrasContagem_Regra = AV20SearchTxt;
         AV11TFRegrasContagem_Regra_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV39DynamicFiltersSelector1 ,
                                              AV40RegrasContagem_Regra1 ,
                                              AV41RegrasContagem_Data1 ,
                                              AV42RegrasContagem_Data_To1 ,
                                              AV43RegrasContagem_Responsavel1 ,
                                              AV44DynamicFiltersEnabled2 ,
                                              AV45DynamicFiltersSelector2 ,
                                              AV46RegrasContagem_Regra2 ,
                                              AV47RegrasContagem_Data2 ,
                                              AV48RegrasContagem_Data_To2 ,
                                              AV49RegrasContagem_Responsavel2 ,
                                              AV50DynamicFiltersEnabled3 ,
                                              AV51DynamicFiltersSelector3 ,
                                              AV52RegrasContagem_Regra3 ,
                                              AV53RegrasContagem_Data3 ,
                                              AV54RegrasContagem_Data_To3 ,
                                              AV55RegrasContagem_Responsavel3 ,
                                              AV11TFRegrasContagem_Regra_Sel ,
                                              AV10TFRegrasContagem_Regra ,
                                              AV12TFRegrasContagem_Data ,
                                              AV13TFRegrasContagem_Data_To ,
                                              AV14TFRegrasContagem_Validade ,
                                              AV15TFRegrasContagem_Validade_To ,
                                              AV17TFRegrasContagem_Responsavel_Sel ,
                                              AV16TFRegrasContagem_Responsavel ,
                                              AV19TFRegrasContagem_Descricao_Sel ,
                                              AV18TFRegrasContagem_Descricao ,
                                              A860RegrasContagem_Regra ,
                                              A861RegrasContagem_Data ,
                                              A863RegrasContagem_Responsavel ,
                                              A862RegrasContagem_Validade ,
                                              A864RegrasContagem_Descricao ,
                                              A865RegrasContagem_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV40RegrasContagem_Regra1 = StringUtil.Concat( StringUtil.RTrim( AV40RegrasContagem_Regra1), "%", "");
         lV43RegrasContagem_Responsavel1 = StringUtil.PadR( StringUtil.RTrim( AV43RegrasContagem_Responsavel1), 50, "%");
         lV46RegrasContagem_Regra2 = StringUtil.Concat( StringUtil.RTrim( AV46RegrasContagem_Regra2), "%", "");
         lV49RegrasContagem_Responsavel2 = StringUtil.PadR( StringUtil.RTrim( AV49RegrasContagem_Responsavel2), 50, "%");
         lV52RegrasContagem_Regra3 = StringUtil.Concat( StringUtil.RTrim( AV52RegrasContagem_Regra3), "%", "");
         lV55RegrasContagem_Responsavel3 = StringUtil.PadR( StringUtil.RTrim( AV55RegrasContagem_Responsavel3), 50, "%");
         lV10TFRegrasContagem_Regra = StringUtil.Concat( StringUtil.RTrim( AV10TFRegrasContagem_Regra), "%", "");
         lV16TFRegrasContagem_Responsavel = StringUtil.PadR( StringUtil.RTrim( AV16TFRegrasContagem_Responsavel), 50, "%");
         lV18TFRegrasContagem_Descricao = StringUtil.Concat( StringUtil.RTrim( AV18TFRegrasContagem_Descricao), "%", "");
         /* Using cursor P00OW2 */
         pr_default.execute(0, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV40RegrasContagem_Regra1, AV41RegrasContagem_Data1, AV42RegrasContagem_Data_To1, lV43RegrasContagem_Responsavel1, lV46RegrasContagem_Regra2, AV47RegrasContagem_Data2, AV48RegrasContagem_Data_To2, lV49RegrasContagem_Responsavel2, lV52RegrasContagem_Regra3, AV53RegrasContagem_Data3, AV54RegrasContagem_Data_To3, lV55RegrasContagem_Responsavel3, lV10TFRegrasContagem_Regra, AV11TFRegrasContagem_Regra_Sel, AV12TFRegrasContagem_Data, AV13TFRegrasContagem_Data_To, AV14TFRegrasContagem_Validade, AV15TFRegrasContagem_Validade_To, lV16TFRegrasContagem_Responsavel, AV17TFRegrasContagem_Responsavel_Sel, lV18TFRegrasContagem_Descricao, AV19TFRegrasContagem_Descricao_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A864RegrasContagem_Descricao = P00OW2_A864RegrasContagem_Descricao[0];
            A862RegrasContagem_Validade = P00OW2_A862RegrasContagem_Validade[0];
            n862RegrasContagem_Validade = P00OW2_n862RegrasContagem_Validade[0];
            A863RegrasContagem_Responsavel = P00OW2_A863RegrasContagem_Responsavel[0];
            A861RegrasContagem_Data = P00OW2_A861RegrasContagem_Data[0];
            A860RegrasContagem_Regra = P00OW2_A860RegrasContagem_Regra[0];
            A865RegrasContagem_AreaTrabalhoCod = P00OW2_A865RegrasContagem_AreaTrabalhoCod[0];
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A860RegrasContagem_Regra)) )
            {
               AV24Option = A860RegrasContagem_Regra;
               AV25Options.Add(AV24Option, 0);
            }
            if ( AV25Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADREGRASCONTAGEM_RESPONSAVELOPTIONS' Routine */
         AV16TFRegrasContagem_Responsavel = AV20SearchTxt;
         AV17TFRegrasContagem_Responsavel_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV39DynamicFiltersSelector1 ,
                                              AV40RegrasContagem_Regra1 ,
                                              AV41RegrasContagem_Data1 ,
                                              AV42RegrasContagem_Data_To1 ,
                                              AV43RegrasContagem_Responsavel1 ,
                                              AV44DynamicFiltersEnabled2 ,
                                              AV45DynamicFiltersSelector2 ,
                                              AV46RegrasContagem_Regra2 ,
                                              AV47RegrasContagem_Data2 ,
                                              AV48RegrasContagem_Data_To2 ,
                                              AV49RegrasContagem_Responsavel2 ,
                                              AV50DynamicFiltersEnabled3 ,
                                              AV51DynamicFiltersSelector3 ,
                                              AV52RegrasContagem_Regra3 ,
                                              AV53RegrasContagem_Data3 ,
                                              AV54RegrasContagem_Data_To3 ,
                                              AV55RegrasContagem_Responsavel3 ,
                                              AV11TFRegrasContagem_Regra_Sel ,
                                              AV10TFRegrasContagem_Regra ,
                                              AV12TFRegrasContagem_Data ,
                                              AV13TFRegrasContagem_Data_To ,
                                              AV14TFRegrasContagem_Validade ,
                                              AV15TFRegrasContagem_Validade_To ,
                                              AV17TFRegrasContagem_Responsavel_Sel ,
                                              AV16TFRegrasContagem_Responsavel ,
                                              AV19TFRegrasContagem_Descricao_Sel ,
                                              AV18TFRegrasContagem_Descricao ,
                                              A860RegrasContagem_Regra ,
                                              A861RegrasContagem_Data ,
                                              A863RegrasContagem_Responsavel ,
                                              A862RegrasContagem_Validade ,
                                              A864RegrasContagem_Descricao ,
                                              A865RegrasContagem_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV40RegrasContagem_Regra1 = StringUtil.Concat( StringUtil.RTrim( AV40RegrasContagem_Regra1), "%", "");
         lV43RegrasContagem_Responsavel1 = StringUtil.PadR( StringUtil.RTrim( AV43RegrasContagem_Responsavel1), 50, "%");
         lV46RegrasContagem_Regra2 = StringUtil.Concat( StringUtil.RTrim( AV46RegrasContagem_Regra2), "%", "");
         lV49RegrasContagem_Responsavel2 = StringUtil.PadR( StringUtil.RTrim( AV49RegrasContagem_Responsavel2), 50, "%");
         lV52RegrasContagem_Regra3 = StringUtil.Concat( StringUtil.RTrim( AV52RegrasContagem_Regra3), "%", "");
         lV55RegrasContagem_Responsavel3 = StringUtil.PadR( StringUtil.RTrim( AV55RegrasContagem_Responsavel3), 50, "%");
         lV10TFRegrasContagem_Regra = StringUtil.Concat( StringUtil.RTrim( AV10TFRegrasContagem_Regra), "%", "");
         lV16TFRegrasContagem_Responsavel = StringUtil.PadR( StringUtil.RTrim( AV16TFRegrasContagem_Responsavel), 50, "%");
         lV18TFRegrasContagem_Descricao = StringUtil.Concat( StringUtil.RTrim( AV18TFRegrasContagem_Descricao), "%", "");
         /* Using cursor P00OW3 */
         pr_default.execute(1, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV40RegrasContagem_Regra1, AV41RegrasContagem_Data1, AV42RegrasContagem_Data_To1, lV43RegrasContagem_Responsavel1, lV46RegrasContagem_Regra2, AV47RegrasContagem_Data2, AV48RegrasContagem_Data_To2, lV49RegrasContagem_Responsavel2, lV52RegrasContagem_Regra3, AV53RegrasContagem_Data3, AV54RegrasContagem_Data_To3, lV55RegrasContagem_Responsavel3, lV10TFRegrasContagem_Regra, AV11TFRegrasContagem_Regra_Sel, AV12TFRegrasContagem_Data, AV13TFRegrasContagem_Data_To, AV14TFRegrasContagem_Validade, AV15TFRegrasContagem_Validade_To, lV16TFRegrasContagem_Responsavel, AV17TFRegrasContagem_Responsavel_Sel, lV18TFRegrasContagem_Descricao, AV19TFRegrasContagem_Descricao_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKOW3 = false;
            A865RegrasContagem_AreaTrabalhoCod = P00OW3_A865RegrasContagem_AreaTrabalhoCod[0];
            A863RegrasContagem_Responsavel = P00OW3_A863RegrasContagem_Responsavel[0];
            A864RegrasContagem_Descricao = P00OW3_A864RegrasContagem_Descricao[0];
            A862RegrasContagem_Validade = P00OW3_A862RegrasContagem_Validade[0];
            n862RegrasContagem_Validade = P00OW3_n862RegrasContagem_Validade[0];
            A861RegrasContagem_Data = P00OW3_A861RegrasContagem_Data[0];
            A860RegrasContagem_Regra = P00OW3_A860RegrasContagem_Regra[0];
            AV32count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00OW3_A863RegrasContagem_Responsavel[0], A863RegrasContagem_Responsavel) == 0 ) )
            {
               BRKOW3 = false;
               A860RegrasContagem_Regra = P00OW3_A860RegrasContagem_Regra[0];
               AV32count = (long)(AV32count+1);
               BRKOW3 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A863RegrasContagem_Responsavel)) )
            {
               AV24Option = A863RegrasContagem_Responsavel;
               AV25Options.Add(AV24Option, 0);
               AV30OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV32count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV25Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKOW3 )
            {
               BRKOW3 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADREGRASCONTAGEM_DESCRICAOOPTIONS' Routine */
         AV18TFRegrasContagem_Descricao = AV20SearchTxt;
         AV19TFRegrasContagem_Descricao_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV39DynamicFiltersSelector1 ,
                                              AV40RegrasContagem_Regra1 ,
                                              AV41RegrasContagem_Data1 ,
                                              AV42RegrasContagem_Data_To1 ,
                                              AV43RegrasContagem_Responsavel1 ,
                                              AV44DynamicFiltersEnabled2 ,
                                              AV45DynamicFiltersSelector2 ,
                                              AV46RegrasContagem_Regra2 ,
                                              AV47RegrasContagem_Data2 ,
                                              AV48RegrasContagem_Data_To2 ,
                                              AV49RegrasContagem_Responsavel2 ,
                                              AV50DynamicFiltersEnabled3 ,
                                              AV51DynamicFiltersSelector3 ,
                                              AV52RegrasContagem_Regra3 ,
                                              AV53RegrasContagem_Data3 ,
                                              AV54RegrasContagem_Data_To3 ,
                                              AV55RegrasContagem_Responsavel3 ,
                                              AV11TFRegrasContagem_Regra_Sel ,
                                              AV10TFRegrasContagem_Regra ,
                                              AV12TFRegrasContagem_Data ,
                                              AV13TFRegrasContagem_Data_To ,
                                              AV14TFRegrasContagem_Validade ,
                                              AV15TFRegrasContagem_Validade_To ,
                                              AV17TFRegrasContagem_Responsavel_Sel ,
                                              AV16TFRegrasContagem_Responsavel ,
                                              AV19TFRegrasContagem_Descricao_Sel ,
                                              AV18TFRegrasContagem_Descricao ,
                                              A860RegrasContagem_Regra ,
                                              A861RegrasContagem_Data ,
                                              A863RegrasContagem_Responsavel ,
                                              A862RegrasContagem_Validade ,
                                              A864RegrasContagem_Descricao ,
                                              A865RegrasContagem_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV40RegrasContagem_Regra1 = StringUtil.Concat( StringUtil.RTrim( AV40RegrasContagem_Regra1), "%", "");
         lV43RegrasContagem_Responsavel1 = StringUtil.PadR( StringUtil.RTrim( AV43RegrasContagem_Responsavel1), 50, "%");
         lV46RegrasContagem_Regra2 = StringUtil.Concat( StringUtil.RTrim( AV46RegrasContagem_Regra2), "%", "");
         lV49RegrasContagem_Responsavel2 = StringUtil.PadR( StringUtil.RTrim( AV49RegrasContagem_Responsavel2), 50, "%");
         lV52RegrasContagem_Regra3 = StringUtil.Concat( StringUtil.RTrim( AV52RegrasContagem_Regra3), "%", "");
         lV55RegrasContagem_Responsavel3 = StringUtil.PadR( StringUtil.RTrim( AV55RegrasContagem_Responsavel3), 50, "%");
         lV10TFRegrasContagem_Regra = StringUtil.Concat( StringUtil.RTrim( AV10TFRegrasContagem_Regra), "%", "");
         lV16TFRegrasContagem_Responsavel = StringUtil.PadR( StringUtil.RTrim( AV16TFRegrasContagem_Responsavel), 50, "%");
         lV18TFRegrasContagem_Descricao = StringUtil.Concat( StringUtil.RTrim( AV18TFRegrasContagem_Descricao), "%", "");
         /* Using cursor P00OW4 */
         pr_default.execute(2, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV40RegrasContagem_Regra1, AV41RegrasContagem_Data1, AV42RegrasContagem_Data_To1, lV43RegrasContagem_Responsavel1, lV46RegrasContagem_Regra2, AV47RegrasContagem_Data2, AV48RegrasContagem_Data_To2, lV49RegrasContagem_Responsavel2, lV52RegrasContagem_Regra3, AV53RegrasContagem_Data3, AV54RegrasContagem_Data_To3, lV55RegrasContagem_Responsavel3, lV10TFRegrasContagem_Regra, AV11TFRegrasContagem_Regra_Sel, AV12TFRegrasContagem_Data, AV13TFRegrasContagem_Data_To, AV14TFRegrasContagem_Validade, AV15TFRegrasContagem_Validade_To, lV16TFRegrasContagem_Responsavel, AV17TFRegrasContagem_Responsavel_Sel, lV18TFRegrasContagem_Descricao, AV19TFRegrasContagem_Descricao_Sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKOW5 = false;
            A865RegrasContagem_AreaTrabalhoCod = P00OW4_A865RegrasContagem_AreaTrabalhoCod[0];
            A864RegrasContagem_Descricao = P00OW4_A864RegrasContagem_Descricao[0];
            A862RegrasContagem_Validade = P00OW4_A862RegrasContagem_Validade[0];
            n862RegrasContagem_Validade = P00OW4_n862RegrasContagem_Validade[0];
            A863RegrasContagem_Responsavel = P00OW4_A863RegrasContagem_Responsavel[0];
            A861RegrasContagem_Data = P00OW4_A861RegrasContagem_Data[0];
            A860RegrasContagem_Regra = P00OW4_A860RegrasContagem_Regra[0];
            AV32count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00OW4_A864RegrasContagem_Descricao[0], A864RegrasContagem_Descricao) == 0 ) )
            {
               BRKOW5 = false;
               A860RegrasContagem_Regra = P00OW4_A860RegrasContagem_Regra[0];
               AV32count = (long)(AV32count+1);
               BRKOW5 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A864RegrasContagem_Descricao)) )
            {
               AV24Option = A864RegrasContagem_Descricao;
               AV25Options.Add(AV24Option, 0);
               AV30OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV32count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV25Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKOW5 )
            {
               BRKOW5 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV25Options = new GxSimpleCollection();
         AV28OptionsDesc = new GxSimpleCollection();
         AV30OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV33Session = context.GetSession();
         AV35GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV36GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFRegrasContagem_Regra = "";
         AV11TFRegrasContagem_Regra_Sel = "";
         AV12TFRegrasContagem_Data = DateTime.MinValue;
         AV13TFRegrasContagem_Data_To = DateTime.MinValue;
         AV14TFRegrasContagem_Validade = DateTime.MinValue;
         AV15TFRegrasContagem_Validade_To = DateTime.MinValue;
         AV16TFRegrasContagem_Responsavel = "";
         AV17TFRegrasContagem_Responsavel_Sel = "";
         AV18TFRegrasContagem_Descricao = "";
         AV19TFRegrasContagem_Descricao_Sel = "";
         AV37GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV39DynamicFiltersSelector1 = "";
         AV40RegrasContagem_Regra1 = "";
         AV41RegrasContagem_Data1 = DateTime.MinValue;
         AV42RegrasContagem_Data_To1 = DateTime.MinValue;
         AV43RegrasContagem_Responsavel1 = "";
         AV45DynamicFiltersSelector2 = "";
         AV46RegrasContagem_Regra2 = "";
         AV47RegrasContagem_Data2 = DateTime.MinValue;
         AV48RegrasContagem_Data_To2 = DateTime.MinValue;
         AV49RegrasContagem_Responsavel2 = "";
         AV51DynamicFiltersSelector3 = "";
         AV52RegrasContagem_Regra3 = "";
         AV53RegrasContagem_Data3 = DateTime.MinValue;
         AV54RegrasContagem_Data_To3 = DateTime.MinValue;
         AV55RegrasContagem_Responsavel3 = "";
         scmdbuf = "";
         lV40RegrasContagem_Regra1 = "";
         lV43RegrasContagem_Responsavel1 = "";
         lV46RegrasContagem_Regra2 = "";
         lV49RegrasContagem_Responsavel2 = "";
         lV52RegrasContagem_Regra3 = "";
         lV55RegrasContagem_Responsavel3 = "";
         lV10TFRegrasContagem_Regra = "";
         lV16TFRegrasContagem_Responsavel = "";
         lV18TFRegrasContagem_Descricao = "";
         A860RegrasContagem_Regra = "";
         A861RegrasContagem_Data = DateTime.MinValue;
         A863RegrasContagem_Responsavel = "";
         A862RegrasContagem_Validade = DateTime.MinValue;
         A864RegrasContagem_Descricao = "";
         P00OW2_A864RegrasContagem_Descricao = new String[] {""} ;
         P00OW2_A862RegrasContagem_Validade = new DateTime[] {DateTime.MinValue} ;
         P00OW2_n862RegrasContagem_Validade = new bool[] {false} ;
         P00OW2_A863RegrasContagem_Responsavel = new String[] {""} ;
         P00OW2_A861RegrasContagem_Data = new DateTime[] {DateTime.MinValue} ;
         P00OW2_A860RegrasContagem_Regra = new String[] {""} ;
         P00OW2_A865RegrasContagem_AreaTrabalhoCod = new int[1] ;
         AV24Option = "";
         P00OW3_A865RegrasContagem_AreaTrabalhoCod = new int[1] ;
         P00OW3_A863RegrasContagem_Responsavel = new String[] {""} ;
         P00OW3_A864RegrasContagem_Descricao = new String[] {""} ;
         P00OW3_A862RegrasContagem_Validade = new DateTime[] {DateTime.MinValue} ;
         P00OW3_n862RegrasContagem_Validade = new bool[] {false} ;
         P00OW3_A861RegrasContagem_Data = new DateTime[] {DateTime.MinValue} ;
         P00OW3_A860RegrasContagem_Regra = new String[] {""} ;
         P00OW4_A865RegrasContagem_AreaTrabalhoCod = new int[1] ;
         P00OW4_A864RegrasContagem_Descricao = new String[] {""} ;
         P00OW4_A862RegrasContagem_Validade = new DateTime[] {DateTime.MinValue} ;
         P00OW4_n862RegrasContagem_Validade = new bool[] {false} ;
         P00OW4_A863RegrasContagem_Responsavel = new String[] {""} ;
         P00OW4_A861RegrasContagem_Data = new DateTime[] {DateTime.MinValue} ;
         P00OW4_A860RegrasContagem_Regra = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptregrascontagemfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00OW2_A864RegrasContagem_Descricao, P00OW2_A862RegrasContagem_Validade, P00OW2_n862RegrasContagem_Validade, P00OW2_A863RegrasContagem_Responsavel, P00OW2_A861RegrasContagem_Data, P00OW2_A860RegrasContagem_Regra, P00OW2_A865RegrasContagem_AreaTrabalhoCod
               }
               , new Object[] {
               P00OW3_A865RegrasContagem_AreaTrabalhoCod, P00OW3_A863RegrasContagem_Responsavel, P00OW3_A864RegrasContagem_Descricao, P00OW3_A862RegrasContagem_Validade, P00OW3_n862RegrasContagem_Validade, P00OW3_A861RegrasContagem_Data, P00OW3_A860RegrasContagem_Regra
               }
               , new Object[] {
               P00OW4_A865RegrasContagem_AreaTrabalhoCod, P00OW4_A864RegrasContagem_Descricao, P00OW4_A862RegrasContagem_Validade, P00OW4_n862RegrasContagem_Validade, P00OW4_A863RegrasContagem_Responsavel, P00OW4_A861RegrasContagem_Data, P00OW4_A860RegrasContagem_Regra
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV58GXV1 ;
      private int AV38RegrasContagem_AreaTrabalhoCod ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A865RegrasContagem_AreaTrabalhoCod ;
      private long AV32count ;
      private String AV16TFRegrasContagem_Responsavel ;
      private String AV17TFRegrasContagem_Responsavel_Sel ;
      private String AV43RegrasContagem_Responsavel1 ;
      private String AV49RegrasContagem_Responsavel2 ;
      private String AV55RegrasContagem_Responsavel3 ;
      private String scmdbuf ;
      private String lV43RegrasContagem_Responsavel1 ;
      private String lV49RegrasContagem_Responsavel2 ;
      private String lV55RegrasContagem_Responsavel3 ;
      private String lV16TFRegrasContagem_Responsavel ;
      private String A863RegrasContagem_Responsavel ;
      private DateTime AV12TFRegrasContagem_Data ;
      private DateTime AV13TFRegrasContagem_Data_To ;
      private DateTime AV14TFRegrasContagem_Validade ;
      private DateTime AV15TFRegrasContagem_Validade_To ;
      private DateTime AV41RegrasContagem_Data1 ;
      private DateTime AV42RegrasContagem_Data_To1 ;
      private DateTime AV47RegrasContagem_Data2 ;
      private DateTime AV48RegrasContagem_Data_To2 ;
      private DateTime AV53RegrasContagem_Data3 ;
      private DateTime AV54RegrasContagem_Data_To3 ;
      private DateTime A861RegrasContagem_Data ;
      private DateTime A862RegrasContagem_Validade ;
      private bool returnInSub ;
      private bool AV44DynamicFiltersEnabled2 ;
      private bool AV50DynamicFiltersEnabled3 ;
      private bool n862RegrasContagem_Validade ;
      private bool BRKOW3 ;
      private bool BRKOW5 ;
      private String AV31OptionIndexesJson ;
      private String AV26OptionsJson ;
      private String AV29OptionsDescJson ;
      private String A864RegrasContagem_Descricao ;
      private String AV22DDOName ;
      private String AV20SearchTxt ;
      private String AV21SearchTxtTo ;
      private String AV10TFRegrasContagem_Regra ;
      private String AV11TFRegrasContagem_Regra_Sel ;
      private String AV18TFRegrasContagem_Descricao ;
      private String AV19TFRegrasContagem_Descricao_Sel ;
      private String AV39DynamicFiltersSelector1 ;
      private String AV40RegrasContagem_Regra1 ;
      private String AV45DynamicFiltersSelector2 ;
      private String AV46RegrasContagem_Regra2 ;
      private String AV51DynamicFiltersSelector3 ;
      private String AV52RegrasContagem_Regra3 ;
      private String lV40RegrasContagem_Regra1 ;
      private String lV46RegrasContagem_Regra2 ;
      private String lV52RegrasContagem_Regra3 ;
      private String lV10TFRegrasContagem_Regra ;
      private String lV18TFRegrasContagem_Descricao ;
      private String A860RegrasContagem_Regra ;
      private String AV24Option ;
      private IGxSession AV33Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00OW2_A864RegrasContagem_Descricao ;
      private DateTime[] P00OW2_A862RegrasContagem_Validade ;
      private bool[] P00OW2_n862RegrasContagem_Validade ;
      private String[] P00OW2_A863RegrasContagem_Responsavel ;
      private DateTime[] P00OW2_A861RegrasContagem_Data ;
      private String[] P00OW2_A860RegrasContagem_Regra ;
      private int[] P00OW2_A865RegrasContagem_AreaTrabalhoCod ;
      private int[] P00OW3_A865RegrasContagem_AreaTrabalhoCod ;
      private String[] P00OW3_A863RegrasContagem_Responsavel ;
      private String[] P00OW3_A864RegrasContagem_Descricao ;
      private DateTime[] P00OW3_A862RegrasContagem_Validade ;
      private bool[] P00OW3_n862RegrasContagem_Validade ;
      private DateTime[] P00OW3_A861RegrasContagem_Data ;
      private String[] P00OW3_A860RegrasContagem_Regra ;
      private int[] P00OW4_A865RegrasContagem_AreaTrabalhoCod ;
      private String[] P00OW4_A864RegrasContagem_Descricao ;
      private DateTime[] P00OW4_A862RegrasContagem_Validade ;
      private bool[] P00OW4_n862RegrasContagem_Validade ;
      private String[] P00OW4_A863RegrasContagem_Responsavel ;
      private DateTime[] P00OW4_A861RegrasContagem_Data ;
      private String[] P00OW4_A860RegrasContagem_Regra ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV35GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV36GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV37GridStateDynamicFilter ;
   }

   public class getpromptregrascontagemfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00OW2( IGxContext context ,
                                             String AV39DynamicFiltersSelector1 ,
                                             String AV40RegrasContagem_Regra1 ,
                                             DateTime AV41RegrasContagem_Data1 ,
                                             DateTime AV42RegrasContagem_Data_To1 ,
                                             String AV43RegrasContagem_Responsavel1 ,
                                             bool AV44DynamicFiltersEnabled2 ,
                                             String AV45DynamicFiltersSelector2 ,
                                             String AV46RegrasContagem_Regra2 ,
                                             DateTime AV47RegrasContagem_Data2 ,
                                             DateTime AV48RegrasContagem_Data_To2 ,
                                             String AV49RegrasContagem_Responsavel2 ,
                                             bool AV50DynamicFiltersEnabled3 ,
                                             String AV51DynamicFiltersSelector3 ,
                                             String AV52RegrasContagem_Regra3 ,
                                             DateTime AV53RegrasContagem_Data3 ,
                                             DateTime AV54RegrasContagem_Data_To3 ,
                                             String AV55RegrasContagem_Responsavel3 ,
                                             String AV11TFRegrasContagem_Regra_Sel ,
                                             String AV10TFRegrasContagem_Regra ,
                                             DateTime AV12TFRegrasContagem_Data ,
                                             DateTime AV13TFRegrasContagem_Data_To ,
                                             DateTime AV14TFRegrasContagem_Validade ,
                                             DateTime AV15TFRegrasContagem_Validade_To ,
                                             String AV17TFRegrasContagem_Responsavel_Sel ,
                                             String AV16TFRegrasContagem_Responsavel ,
                                             String AV19TFRegrasContagem_Descricao_Sel ,
                                             String AV18TFRegrasContagem_Descricao ,
                                             String A860RegrasContagem_Regra ,
                                             DateTime A861RegrasContagem_Data ,
                                             String A863RegrasContagem_Responsavel ,
                                             DateTime A862RegrasContagem_Validade ,
                                             String A864RegrasContagem_Descricao ,
                                             int A865RegrasContagem_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [23] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT DISTINCT NULL AS [RegrasContagem_Descricao], NULL AS [RegrasContagem_Validade], NULL AS [RegrasContagem_Responsavel], NULL AS [RegrasContagem_Data], [RegrasContagem_Regra], NULL AS [RegrasContagem_AreaTrabalhoCod] FROM ( SELECT TOP(100) PERCENT [RegrasContagem_Descricao], [RegrasContagem_Validade], [RegrasContagem_Responsavel], [RegrasContagem_Data], [RegrasContagem_Regra], [RegrasContagem_AreaTrabalhoCod] FROM [RegrasContagem] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([RegrasContagem_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "REGRASCONTAGEM_REGRA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40RegrasContagem_Regra1)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] like '%' + @lV40RegrasContagem_Regra1 + '%')";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV41RegrasContagem_Data1) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] >= @AV41RegrasContagem_Data1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV42RegrasContagem_Data_To1) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] <= @AV42RegrasContagem_Data_To1)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "REGRASCONTAGEM_RESPONSAVEL") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43RegrasContagem_Responsavel1)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] like '%' + @lV43RegrasContagem_Responsavel1 + '%')";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "REGRASCONTAGEM_REGRA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46RegrasContagem_Regra2)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] like '%' + @lV46RegrasContagem_Regra2 + '%')";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV47RegrasContagem_Data2) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] >= @AV47RegrasContagem_Data2)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV48RegrasContagem_Data_To2) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] <= @AV48RegrasContagem_Data_To2)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "REGRASCONTAGEM_RESPONSAVEL") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49RegrasContagem_Responsavel2)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] like '%' + @lV49RegrasContagem_Responsavel2 + '%')";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV50DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "REGRASCONTAGEM_REGRA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52RegrasContagem_Regra3)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] like '%' + @lV52RegrasContagem_Regra3 + '%')";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV50DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV53RegrasContagem_Data3) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] >= @AV53RegrasContagem_Data3)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV50DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV54RegrasContagem_Data_To3) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] <= @AV54RegrasContagem_Data_To3)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV50DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "REGRASCONTAGEM_RESPONSAVEL") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55RegrasContagem_Responsavel3)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] like '%' + @lV55RegrasContagem_Responsavel3 + '%')";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFRegrasContagem_Regra_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFRegrasContagem_Regra)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] like @lV10TFRegrasContagem_Regra)";
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFRegrasContagem_Regra_Sel)) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] = @AV11TFRegrasContagem_Regra_Sel)";
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV12TFRegrasContagem_Data) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] >= @AV12TFRegrasContagem_Data)";
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV13TFRegrasContagem_Data_To) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] <= @AV13TFRegrasContagem_Data_To)";
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV14TFRegrasContagem_Validade) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Validade] >= @AV14TFRegrasContagem_Validade)";
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! (DateTime.MinValue==AV15TFRegrasContagem_Validade_To) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Validade] <= @AV15TFRegrasContagem_Validade_To)";
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFRegrasContagem_Responsavel_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFRegrasContagem_Responsavel)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] like @lV16TFRegrasContagem_Responsavel)";
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFRegrasContagem_Responsavel_Sel)) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] = @AV17TFRegrasContagem_Responsavel_Sel)";
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFRegrasContagem_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFRegrasContagem_Descricao)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Descricao] like @lV18TFRegrasContagem_Descricao)";
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFRegrasContagem_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Descricao] = @AV19TFRegrasContagem_Descricao_Sel)";
         }
         else
         {
            GXv_int1[22] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [RegrasContagem_Regra]";
         scmdbuf = scmdbuf + ") DistinctT";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00OW3( IGxContext context ,
                                             String AV39DynamicFiltersSelector1 ,
                                             String AV40RegrasContagem_Regra1 ,
                                             DateTime AV41RegrasContagem_Data1 ,
                                             DateTime AV42RegrasContagem_Data_To1 ,
                                             String AV43RegrasContagem_Responsavel1 ,
                                             bool AV44DynamicFiltersEnabled2 ,
                                             String AV45DynamicFiltersSelector2 ,
                                             String AV46RegrasContagem_Regra2 ,
                                             DateTime AV47RegrasContagem_Data2 ,
                                             DateTime AV48RegrasContagem_Data_To2 ,
                                             String AV49RegrasContagem_Responsavel2 ,
                                             bool AV50DynamicFiltersEnabled3 ,
                                             String AV51DynamicFiltersSelector3 ,
                                             String AV52RegrasContagem_Regra3 ,
                                             DateTime AV53RegrasContagem_Data3 ,
                                             DateTime AV54RegrasContagem_Data_To3 ,
                                             String AV55RegrasContagem_Responsavel3 ,
                                             String AV11TFRegrasContagem_Regra_Sel ,
                                             String AV10TFRegrasContagem_Regra ,
                                             DateTime AV12TFRegrasContagem_Data ,
                                             DateTime AV13TFRegrasContagem_Data_To ,
                                             DateTime AV14TFRegrasContagem_Validade ,
                                             DateTime AV15TFRegrasContagem_Validade_To ,
                                             String AV17TFRegrasContagem_Responsavel_Sel ,
                                             String AV16TFRegrasContagem_Responsavel ,
                                             String AV19TFRegrasContagem_Descricao_Sel ,
                                             String AV18TFRegrasContagem_Descricao ,
                                             String A860RegrasContagem_Regra ,
                                             DateTime A861RegrasContagem_Data ,
                                             String A863RegrasContagem_Responsavel ,
                                             DateTime A862RegrasContagem_Validade ,
                                             String A864RegrasContagem_Descricao ,
                                             int A865RegrasContagem_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [23] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [RegrasContagem_AreaTrabalhoCod], [RegrasContagem_Responsavel], [RegrasContagem_Descricao], [RegrasContagem_Validade], [RegrasContagem_Data], [RegrasContagem_Regra] FROM [RegrasContagem] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([RegrasContagem_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "REGRASCONTAGEM_REGRA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40RegrasContagem_Regra1)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] like '%' + @lV40RegrasContagem_Regra1 + '%')";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV41RegrasContagem_Data1) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] >= @AV41RegrasContagem_Data1)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV42RegrasContagem_Data_To1) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] <= @AV42RegrasContagem_Data_To1)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "REGRASCONTAGEM_RESPONSAVEL") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43RegrasContagem_Responsavel1)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] like '%' + @lV43RegrasContagem_Responsavel1 + '%')";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "REGRASCONTAGEM_REGRA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46RegrasContagem_Regra2)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] like '%' + @lV46RegrasContagem_Regra2 + '%')";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV47RegrasContagem_Data2) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] >= @AV47RegrasContagem_Data2)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV48RegrasContagem_Data_To2) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] <= @AV48RegrasContagem_Data_To2)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "REGRASCONTAGEM_RESPONSAVEL") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49RegrasContagem_Responsavel2)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] like '%' + @lV49RegrasContagem_Responsavel2 + '%')";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV50DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "REGRASCONTAGEM_REGRA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52RegrasContagem_Regra3)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] like '%' + @lV52RegrasContagem_Regra3 + '%')";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV50DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV53RegrasContagem_Data3) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] >= @AV53RegrasContagem_Data3)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV50DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV54RegrasContagem_Data_To3) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] <= @AV54RegrasContagem_Data_To3)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV50DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "REGRASCONTAGEM_RESPONSAVEL") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55RegrasContagem_Responsavel3)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] like '%' + @lV55RegrasContagem_Responsavel3 + '%')";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFRegrasContagem_Regra_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFRegrasContagem_Regra)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] like @lV10TFRegrasContagem_Regra)";
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFRegrasContagem_Regra_Sel)) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] = @AV11TFRegrasContagem_Regra_Sel)";
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV12TFRegrasContagem_Data) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] >= @AV12TFRegrasContagem_Data)";
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV13TFRegrasContagem_Data_To) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] <= @AV13TFRegrasContagem_Data_To)";
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV14TFRegrasContagem_Validade) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Validade] >= @AV14TFRegrasContagem_Validade)";
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! (DateTime.MinValue==AV15TFRegrasContagem_Validade_To) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Validade] <= @AV15TFRegrasContagem_Validade_To)";
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFRegrasContagem_Responsavel_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFRegrasContagem_Responsavel)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] like @lV16TFRegrasContagem_Responsavel)";
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFRegrasContagem_Responsavel_Sel)) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] = @AV17TFRegrasContagem_Responsavel_Sel)";
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFRegrasContagem_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFRegrasContagem_Descricao)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Descricao] like @lV18TFRegrasContagem_Descricao)";
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFRegrasContagem_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Descricao] = @AV19TFRegrasContagem_Descricao_Sel)";
         }
         else
         {
            GXv_int3[22] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [RegrasContagem_Responsavel]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00OW4( IGxContext context ,
                                             String AV39DynamicFiltersSelector1 ,
                                             String AV40RegrasContagem_Regra1 ,
                                             DateTime AV41RegrasContagem_Data1 ,
                                             DateTime AV42RegrasContagem_Data_To1 ,
                                             String AV43RegrasContagem_Responsavel1 ,
                                             bool AV44DynamicFiltersEnabled2 ,
                                             String AV45DynamicFiltersSelector2 ,
                                             String AV46RegrasContagem_Regra2 ,
                                             DateTime AV47RegrasContagem_Data2 ,
                                             DateTime AV48RegrasContagem_Data_To2 ,
                                             String AV49RegrasContagem_Responsavel2 ,
                                             bool AV50DynamicFiltersEnabled3 ,
                                             String AV51DynamicFiltersSelector3 ,
                                             String AV52RegrasContagem_Regra3 ,
                                             DateTime AV53RegrasContagem_Data3 ,
                                             DateTime AV54RegrasContagem_Data_To3 ,
                                             String AV55RegrasContagem_Responsavel3 ,
                                             String AV11TFRegrasContagem_Regra_Sel ,
                                             String AV10TFRegrasContagem_Regra ,
                                             DateTime AV12TFRegrasContagem_Data ,
                                             DateTime AV13TFRegrasContagem_Data_To ,
                                             DateTime AV14TFRegrasContagem_Validade ,
                                             DateTime AV15TFRegrasContagem_Validade_To ,
                                             String AV17TFRegrasContagem_Responsavel_Sel ,
                                             String AV16TFRegrasContagem_Responsavel ,
                                             String AV19TFRegrasContagem_Descricao_Sel ,
                                             String AV18TFRegrasContagem_Descricao ,
                                             String A860RegrasContagem_Regra ,
                                             DateTime A861RegrasContagem_Data ,
                                             String A863RegrasContagem_Responsavel ,
                                             DateTime A862RegrasContagem_Validade ,
                                             String A864RegrasContagem_Descricao ,
                                             int A865RegrasContagem_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [23] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT [RegrasContagem_AreaTrabalhoCod], [RegrasContagem_Descricao], [RegrasContagem_Validade], [RegrasContagem_Responsavel], [RegrasContagem_Data], [RegrasContagem_Regra] FROM [RegrasContagem] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([RegrasContagem_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "REGRASCONTAGEM_REGRA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40RegrasContagem_Regra1)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] like '%' + @lV40RegrasContagem_Regra1 + '%')";
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV41RegrasContagem_Data1) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] >= @AV41RegrasContagem_Data1)";
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV42RegrasContagem_Data_To1) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] <= @AV42RegrasContagem_Data_To1)";
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "REGRASCONTAGEM_RESPONSAVEL") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43RegrasContagem_Responsavel1)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] like '%' + @lV43RegrasContagem_Responsavel1 + '%')";
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "REGRASCONTAGEM_REGRA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46RegrasContagem_Regra2)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] like '%' + @lV46RegrasContagem_Regra2 + '%')";
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV47RegrasContagem_Data2) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] >= @AV47RegrasContagem_Data2)";
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV48RegrasContagem_Data_To2) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] <= @AV48RegrasContagem_Data_To2)";
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "REGRASCONTAGEM_RESPONSAVEL") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49RegrasContagem_Responsavel2)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] like '%' + @lV49RegrasContagem_Responsavel2 + '%')";
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV50DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "REGRASCONTAGEM_REGRA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52RegrasContagem_Regra3)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] like '%' + @lV52RegrasContagem_Regra3 + '%')";
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV50DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV53RegrasContagem_Data3) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] >= @AV53RegrasContagem_Data3)";
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( AV50DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV54RegrasContagem_Data_To3) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] <= @AV54RegrasContagem_Data_To3)";
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( AV50DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "REGRASCONTAGEM_RESPONSAVEL") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55RegrasContagem_Responsavel3)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] like '%' + @lV55RegrasContagem_Responsavel3 + '%')";
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFRegrasContagem_Regra_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFRegrasContagem_Regra)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] like @lV10TFRegrasContagem_Regra)";
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFRegrasContagem_Regra_Sel)) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] = @AV11TFRegrasContagem_Regra_Sel)";
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV12TFRegrasContagem_Data) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] >= @AV12TFRegrasContagem_Data)";
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV13TFRegrasContagem_Data_To) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] <= @AV13TFRegrasContagem_Data_To)";
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV14TFRegrasContagem_Validade) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Validade] >= @AV14TFRegrasContagem_Validade)";
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( ! (DateTime.MinValue==AV15TFRegrasContagem_Validade_To) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Validade] <= @AV15TFRegrasContagem_Validade_To)";
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFRegrasContagem_Responsavel_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFRegrasContagem_Responsavel)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] like @lV16TFRegrasContagem_Responsavel)";
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFRegrasContagem_Responsavel_Sel)) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] = @AV17TFRegrasContagem_Responsavel_Sel)";
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFRegrasContagem_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFRegrasContagem_Descricao)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Descricao] like @lV18TFRegrasContagem_Descricao)";
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFRegrasContagem_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Descricao] = @AV19TFRegrasContagem_Descricao_Sel)";
         }
         else
         {
            GXv_int5[22] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [RegrasContagem_Descricao]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00OW2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (DateTime)dynConstraints[28] , (String)dynConstraints[29] , (DateTime)dynConstraints[30] , (String)dynConstraints[31] , (int)dynConstraints[32] , (int)dynConstraints[33] );
               case 1 :
                     return conditional_P00OW3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (DateTime)dynConstraints[28] , (String)dynConstraints[29] , (DateTime)dynConstraints[30] , (String)dynConstraints[31] , (int)dynConstraints[32] , (int)dynConstraints[33] );
               case 2 :
                     return conditional_P00OW4(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (DateTime)dynConstraints[28] , (String)dynConstraints[29] , (DateTime)dynConstraints[30] , (String)dynConstraints[31] , (int)dynConstraints[32] , (int)dynConstraints[33] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00OW2 ;
          prmP00OW2 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV40RegrasContagem_Regra1",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV41RegrasContagem_Data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV42RegrasContagem_Data_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV43RegrasContagem_Responsavel1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46RegrasContagem_Regra2",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV47RegrasContagem_Data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV48RegrasContagem_Data_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV49RegrasContagem_Responsavel2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV52RegrasContagem_Regra3",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV53RegrasContagem_Data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV54RegrasContagem_Data_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV55RegrasContagem_Responsavel3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFRegrasContagem_Regra",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV11TFRegrasContagem_Regra_Sel",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV12TFRegrasContagem_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV13TFRegrasContagem_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV14TFRegrasContagem_Validade",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV15TFRegrasContagem_Validade_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV16TFRegrasContagem_Responsavel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV17TFRegrasContagem_Responsavel_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV18TFRegrasContagem_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV19TFRegrasContagem_Descricao_Sel",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmP00OW3 ;
          prmP00OW3 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV40RegrasContagem_Regra1",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV41RegrasContagem_Data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV42RegrasContagem_Data_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV43RegrasContagem_Responsavel1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46RegrasContagem_Regra2",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV47RegrasContagem_Data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV48RegrasContagem_Data_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV49RegrasContagem_Responsavel2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV52RegrasContagem_Regra3",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV53RegrasContagem_Data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV54RegrasContagem_Data_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV55RegrasContagem_Responsavel3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFRegrasContagem_Regra",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV11TFRegrasContagem_Regra_Sel",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV12TFRegrasContagem_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV13TFRegrasContagem_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV14TFRegrasContagem_Validade",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV15TFRegrasContagem_Validade_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV16TFRegrasContagem_Responsavel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV17TFRegrasContagem_Responsavel_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV18TFRegrasContagem_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV19TFRegrasContagem_Descricao_Sel",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmP00OW4 ;
          prmP00OW4 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV40RegrasContagem_Regra1",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV41RegrasContagem_Data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV42RegrasContagem_Data_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV43RegrasContagem_Responsavel1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46RegrasContagem_Regra2",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV47RegrasContagem_Data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV48RegrasContagem_Data_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV49RegrasContagem_Responsavel2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV52RegrasContagem_Regra3",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV53RegrasContagem_Data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV54RegrasContagem_Data_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV55RegrasContagem_Responsavel3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFRegrasContagem_Regra",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV11TFRegrasContagem_Regra_Sel",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV12TFRegrasContagem_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV13TFRegrasContagem_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV14TFRegrasContagem_Validade",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV15TFRegrasContagem_Validade_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV16TFRegrasContagem_Responsavel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV17TFRegrasContagem_Responsavel_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV18TFRegrasContagem_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV19TFRegrasContagem_Descricao_Sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00OW2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00OW2,100,0,false,false )
             ,new CursorDef("P00OW3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00OW3,100,0,true,false )
             ,new CursorDef("P00OW4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00OW4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(5) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(5) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptregrascontagemfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptregrascontagemfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptregrascontagemfilterdata") )
          {
             return  ;
          }
          getpromptregrascontagemfilterdata worker = new getpromptregrascontagemfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
