/*
               File: WWContagemItemParecer
        Description:  Contagem Item Parecer
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:34:34.4
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwcontagemitemparecer : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwcontagemitemparecer( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwcontagemitemparecer( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_91 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_91_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_91_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV16DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
               AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV18ContagemItemParecer_Comentario1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemItemParecer_Comentario1", AV18ContagemItemParecer_Comentario1);
               AV19ContagemItemParecer_UsuarioPessoaNom1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemItemParecer_UsuarioPessoaNom1", AV19ContagemItemParecer_UsuarioPessoaNom1);
               AV21DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
               AV23ContagemItemParecer_Comentario2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemItemParecer_Comentario2", AV23ContagemItemParecer_Comentario2);
               AV24ContagemItemParecer_UsuarioPessoaNom2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemItemParecer_UsuarioPessoaNom2", AV24ContagemItemParecer_UsuarioPessoaNom2);
               AV26DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
               AV27DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
               AV28ContagemItemParecer_Comentario3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ContagemItemParecer_Comentario3", AV28ContagemItemParecer_Comentario3);
               AV29ContagemItemParecer_UsuarioPessoaNom3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemItemParecer_UsuarioPessoaNom3", AV29ContagemItemParecer_UsuarioPessoaNom3);
               AV20DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV25DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
               AV58TFContagemItemParecer_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFContagemItemParecer_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFContagemItemParecer_Codigo), 6, 0)));
               AV59TFContagemItemParecer_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContagemItemParecer_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFContagemItemParecer_Codigo_To), 6, 0)));
               AV62TFContagemItem_Lancamento = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFContagemItem_Lancamento), 6, 0)));
               AV63TFContagemItem_Lancamento_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContagemItem_Lancamento_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63TFContagemItem_Lancamento_To), 6, 0)));
               AV66TFContagemItemParecer_Comentario = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContagemItemParecer_Comentario", AV66TFContagemItemParecer_Comentario);
               AV67TFContagemItemParecer_Comentario_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContagemItemParecer_Comentario_Sel", AV67TFContagemItemParecer_Comentario_Sel);
               AV70TFContagemItemParecer_Data = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContagemItemParecer_Data", context.localUtil.TToC( AV70TFContagemItemParecer_Data, 8, 5, 0, 3, "/", ":", " "));
               AV71TFContagemItemParecer_Data_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContagemItemParecer_Data_To", context.localUtil.TToC( AV71TFContagemItemParecer_Data_To, 8, 5, 0, 3, "/", ":", " "));
               AV76TFContagemItemParecer_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContagemItemParecer_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76TFContagemItemParecer_UsuarioCod), 6, 0)));
               AV77TFContagemItemParecer_UsuarioCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFContagemItemParecer_UsuarioCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77TFContagemItemParecer_UsuarioCod_To), 6, 0)));
               AV80TFContagemItemParecer_UsuarioPessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContagemItemParecer_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV80TFContagemItemParecer_UsuarioPessoaCod), 6, 0)));
               AV81TFContagemItemParecer_UsuarioPessoaCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContagemItemParecer_UsuarioPessoaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81TFContagemItemParecer_UsuarioPessoaCod_To), 6, 0)));
               AV84TFContagemItemParecer_UsuarioPessoaNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContagemItemParecer_UsuarioPessoaNom", AV84TFContagemItemParecer_UsuarioPessoaNom);
               AV85TFContagemItemParecer_UsuarioPessoaNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFContagemItemParecer_UsuarioPessoaNom_Sel", AV85TFContagemItemParecer_UsuarioPessoaNom_Sel);
               AV92ManageFiltersExecutionStep = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV92ManageFiltersExecutionStep), 1, 0));
               AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace", AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace);
               AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace", AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace);
               AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace", AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace);
               AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace", AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace);
               AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace", AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace);
               AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace", AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace);
               AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace", AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               AV131Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV31DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
               AV30DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
               A243ContagemItemParecer_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A224ContagemItem_Lancamento = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A246ContagemItemParecer_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemItemParecer_Comentario1, AV19ContagemItemParecer_UsuarioPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemItemParecer_Comentario2, AV24ContagemItemParecer_UsuarioPessoaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28ContagemItemParecer_Comentario3, AV29ContagemItemParecer_UsuarioPessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV58TFContagemItemParecer_Codigo, AV59TFContagemItemParecer_Codigo_To, AV62TFContagemItem_Lancamento, AV63TFContagemItem_Lancamento_To, AV66TFContagemItemParecer_Comentario, AV67TFContagemItemParecer_Comentario_Sel, AV70TFContagemItemParecer_Data, AV71TFContagemItemParecer_Data_To, AV76TFContagemItemParecer_UsuarioCod, AV77TFContagemItemParecer_UsuarioCod_To, AV80TFContagemItemParecer_UsuarioPessoaCod, AV81TFContagemItemParecer_UsuarioPessoaCod_To, AV84TFContagemItemParecer_UsuarioPessoaNom, AV85TFContagemItemParecer_UsuarioPessoaNom_Sel, AV92ManageFiltersExecutionStep, AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace, AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace, AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace, AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace, AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace, AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace, AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace, AV6WWPContext, AV131Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A243ContagemItemParecer_Codigo, A224ContagemItem_Lancamento, A246ContagemItemParecer_UsuarioCod) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA5V2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START5V2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020529934354");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwcontagemitemparecer.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMITEMPARECER_COMENTARIO1", AV18ContagemItemParecer_Comentario1);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMITEMPARECER_USUARIOPESSOANOM1", StringUtil.RTrim( AV19ContagemItemParecer_UsuarioPessoaNom1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV21DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMITEMPARECER_COMENTARIO2", AV23ContagemItemParecer_Comentario2);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMITEMPARECER_USUARIOPESSOANOM2", StringUtil.RTrim( AV24ContagemItemParecer_UsuarioPessoaNom2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV26DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV27DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMITEMPARECER_COMENTARIO3", AV28ContagemItemParecer_Comentario3);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMITEMPARECER_USUARIOPESSOANOM3", StringUtil.RTrim( AV29ContagemItemParecer_UsuarioPessoaNom3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV20DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV25DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMITEMPARECER_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV58TFContagemItemParecer_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMITEMPARECER_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV59TFContagemItemParecer_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMITEM_LANCAMENTO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV62TFContagemItem_Lancamento), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMITEM_LANCAMENTO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV63TFContagemItem_Lancamento_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMITEMPARECER_COMENTARIO", AV66TFContagemItemParecer_Comentario);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMITEMPARECER_COMENTARIO_SEL", AV67TFContagemItemParecer_Comentario_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMITEMPARECER_DATA", context.localUtil.TToC( AV70TFContagemItemParecer_Data, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMITEMPARECER_DATA_TO", context.localUtil.TToC( AV71TFContagemItemParecer_Data_To, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMITEMPARECER_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV76TFContagemItemParecer_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMITEMPARECER_USUARIOCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV77TFContagemItemParecer_UsuarioCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV80TFContagemItemParecer_UsuarioPessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV81TFContagemItemParecer_UsuarioPessoaCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM", StringUtil.RTrim( AV84TFContagemItemParecer_UsuarioPessoaNom));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM_SEL", StringUtil.RTrim( AV85TFContagemItemParecer_UsuarioPessoaNom_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_91", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_91), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMANAGEFILTERSDATA", AV96ManageFiltersData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMANAGEFILTERSDATA", AV96ManageFiltersData);
         }
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV89GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV90GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV87DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV87DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMITEMPARECER_CODIGOTITLEFILTERDATA", AV57ContagemItemParecer_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMITEMPARECER_CODIGOTITLEFILTERDATA", AV57ContagemItemParecer_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMITEM_LANCAMENTOTITLEFILTERDATA", AV61ContagemItem_LancamentoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMITEM_LANCAMENTOTITLEFILTERDATA", AV61ContagemItem_LancamentoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMITEMPARECER_COMENTARIOTITLEFILTERDATA", AV65ContagemItemParecer_ComentarioTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMITEMPARECER_COMENTARIOTITLEFILTERDATA", AV65ContagemItemParecer_ComentarioTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMITEMPARECER_DATATITLEFILTERDATA", AV69ContagemItemParecer_DataTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMITEMPARECER_DATATITLEFILTERDATA", AV69ContagemItemParecer_DataTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMITEMPARECER_USUARIOCODTITLEFILTERDATA", AV75ContagemItemParecer_UsuarioCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMITEMPARECER_USUARIOCODTITLEFILTERDATA", AV75ContagemItemParecer_UsuarioCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMITEMPARECER_USUARIOPESSOACODTITLEFILTERDATA", AV79ContagemItemParecer_UsuarioPessoaCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMITEMPARECER_USUARIOPESSOACODTITLEFILTERDATA", AV79ContagemItemParecer_UsuarioPessoaCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMITEMPARECER_USUARIOPESSOANOMTITLEFILTERDATA", AV83ContagemItemParecer_UsuarioPessoaNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMITEMPARECER_USUARIOPESSOANOMTITLEFILTERDATA", AV83ContagemItemParecer_UsuarioPessoaNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV131Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV31DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV30DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Icon", StringUtil.RTrim( Ddo_managefilters_Icon));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Caption", StringUtil.RTrim( Ddo_managefilters_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Tooltip", StringUtil.RTrim( Ddo_managefilters_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Cls", StringUtil.RTrim( Ddo_managefilters_Cls));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_CODIGO_Caption", StringUtil.RTrim( Ddo_contagemitemparecer_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_CODIGO_Tooltip", StringUtil.RTrim( Ddo_contagemitemparecer_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_CODIGO_Cls", StringUtil.RTrim( Ddo_contagemitemparecer_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_contagemitemparecer_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemitemparecer_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemitemparecer_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemitemparecer_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_contagemitemparecer_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemitemparecer_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_contagemitemparecer_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_contagemitemparecer_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_CODIGO_Filtertype", StringUtil.RTrim( Ddo_contagemitemparecer_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_contagemitemparecer_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_contagemitemparecer_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_CODIGO_Sortasc", StringUtil.RTrim( Ddo_contagemitemparecer_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_contagemitemparecer_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_contagemitemparecer_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemitemparecer_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_contagemitemparecer_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_contagemitemparecer_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_LANCAMENTO_Caption", StringUtil.RTrim( Ddo_contagemitem_lancamento_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_LANCAMENTO_Tooltip", StringUtil.RTrim( Ddo_contagemitem_lancamento_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_LANCAMENTO_Cls", StringUtil.RTrim( Ddo_contagemitem_lancamento_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_LANCAMENTO_Filteredtext_set", StringUtil.RTrim( Ddo_contagemitem_lancamento_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_LANCAMENTO_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemitem_lancamento_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_LANCAMENTO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemitem_lancamento_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_LANCAMENTO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemitem_lancamento_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_LANCAMENTO_Includesortasc", StringUtil.BoolToStr( Ddo_contagemitem_lancamento_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_LANCAMENTO_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemitem_lancamento_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_LANCAMENTO_Sortedstatus", StringUtil.RTrim( Ddo_contagemitem_lancamento_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_LANCAMENTO_Includefilter", StringUtil.BoolToStr( Ddo_contagemitem_lancamento_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_LANCAMENTO_Filtertype", StringUtil.RTrim( Ddo_contagemitem_lancamento_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_LANCAMENTO_Filterisrange", StringUtil.BoolToStr( Ddo_contagemitem_lancamento_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_LANCAMENTO_Includedatalist", StringUtil.BoolToStr( Ddo_contagemitem_lancamento_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_LANCAMENTO_Sortasc", StringUtil.RTrim( Ddo_contagemitem_lancamento_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_LANCAMENTO_Sortdsc", StringUtil.RTrim( Ddo_contagemitem_lancamento_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_LANCAMENTO_Cleanfilter", StringUtil.RTrim( Ddo_contagemitem_lancamento_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_LANCAMENTO_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemitem_lancamento_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_LANCAMENTO_Rangefilterto", StringUtil.RTrim( Ddo_contagemitem_lancamento_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_LANCAMENTO_Searchbuttontext", StringUtil.RTrim( Ddo_contagemitem_lancamento_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_COMENTARIO_Caption", StringUtil.RTrim( Ddo_contagemitemparecer_comentario_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_COMENTARIO_Tooltip", StringUtil.RTrim( Ddo_contagemitemparecer_comentario_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_COMENTARIO_Cls", StringUtil.RTrim( Ddo_contagemitemparecer_comentario_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_COMENTARIO_Filteredtext_set", StringUtil.RTrim( Ddo_contagemitemparecer_comentario_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_COMENTARIO_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemitemparecer_comentario_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_COMENTARIO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemitemparecer_comentario_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_COMENTARIO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemitemparecer_comentario_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_COMENTARIO_Includesortasc", StringUtil.BoolToStr( Ddo_contagemitemparecer_comentario_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_COMENTARIO_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemitemparecer_comentario_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_COMENTARIO_Sortedstatus", StringUtil.RTrim( Ddo_contagemitemparecer_comentario_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_COMENTARIO_Includefilter", StringUtil.BoolToStr( Ddo_contagemitemparecer_comentario_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_COMENTARIO_Filtertype", StringUtil.RTrim( Ddo_contagemitemparecer_comentario_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_COMENTARIO_Filterisrange", StringUtil.BoolToStr( Ddo_contagemitemparecer_comentario_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_COMENTARIO_Includedatalist", StringUtil.BoolToStr( Ddo_contagemitemparecer_comentario_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_COMENTARIO_Datalisttype", StringUtil.RTrim( Ddo_contagemitemparecer_comentario_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_COMENTARIO_Datalistproc", StringUtil.RTrim( Ddo_contagemitemparecer_comentario_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_COMENTARIO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemitemparecer_comentario_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_COMENTARIO_Sortasc", StringUtil.RTrim( Ddo_contagemitemparecer_comentario_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_COMENTARIO_Sortdsc", StringUtil.RTrim( Ddo_contagemitemparecer_comentario_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_COMENTARIO_Loadingdata", StringUtil.RTrim( Ddo_contagemitemparecer_comentario_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_COMENTARIO_Cleanfilter", StringUtil.RTrim( Ddo_contagemitemparecer_comentario_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_COMENTARIO_Noresultsfound", StringUtil.RTrim( Ddo_contagemitemparecer_comentario_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_COMENTARIO_Searchbuttontext", StringUtil.RTrim( Ddo_contagemitemparecer_comentario_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_DATA_Caption", StringUtil.RTrim( Ddo_contagemitemparecer_data_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_DATA_Tooltip", StringUtil.RTrim( Ddo_contagemitemparecer_data_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_DATA_Cls", StringUtil.RTrim( Ddo_contagemitemparecer_data_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_DATA_Filteredtext_set", StringUtil.RTrim( Ddo_contagemitemparecer_data_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_DATA_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemitemparecer_data_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_DATA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemitemparecer_data_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_DATA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemitemparecer_data_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_DATA_Includesortasc", StringUtil.BoolToStr( Ddo_contagemitemparecer_data_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_DATA_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemitemparecer_data_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_DATA_Sortedstatus", StringUtil.RTrim( Ddo_contagemitemparecer_data_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_DATA_Includefilter", StringUtil.BoolToStr( Ddo_contagemitemparecer_data_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_DATA_Filtertype", StringUtil.RTrim( Ddo_contagemitemparecer_data_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_DATA_Filterisrange", StringUtil.BoolToStr( Ddo_contagemitemparecer_data_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_DATA_Includedatalist", StringUtil.BoolToStr( Ddo_contagemitemparecer_data_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_DATA_Sortasc", StringUtil.RTrim( Ddo_contagemitemparecer_data_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_DATA_Sortdsc", StringUtil.RTrim( Ddo_contagemitemparecer_data_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_DATA_Cleanfilter", StringUtil.RTrim( Ddo_contagemitemparecer_data_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_DATA_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemitemparecer_data_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_DATA_Rangefilterto", StringUtil.RTrim( Ddo_contagemitemparecer_data_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_DATA_Searchbuttontext", StringUtil.RTrim( Ddo_contagemitemparecer_data_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Caption", StringUtil.RTrim( Ddo_contagemitemparecer_usuariocod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Tooltip", StringUtil.RTrim( Ddo_contagemitemparecer_usuariocod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Cls", StringUtil.RTrim( Ddo_contagemitemparecer_usuariocod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Filteredtext_set", StringUtil.RTrim( Ddo_contagemitemparecer_usuariocod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemitemparecer_usuariocod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemitemparecer_usuariocod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemitemparecer_usuariocod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Includesortasc", StringUtil.BoolToStr( Ddo_contagemitemparecer_usuariocod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemitemparecer_usuariocod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Sortedstatus", StringUtil.RTrim( Ddo_contagemitemparecer_usuariocod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Includefilter", StringUtil.BoolToStr( Ddo_contagemitemparecer_usuariocod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Filtertype", StringUtil.RTrim( Ddo_contagemitemparecer_usuariocod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Filterisrange", StringUtil.BoolToStr( Ddo_contagemitemparecer_usuariocod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Includedatalist", StringUtil.BoolToStr( Ddo_contagemitemparecer_usuariocod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Sortasc", StringUtil.RTrim( Ddo_contagemitemparecer_usuariocod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Sortdsc", StringUtil.RTrim( Ddo_contagemitemparecer_usuariocod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Cleanfilter", StringUtil.RTrim( Ddo_contagemitemparecer_usuariocod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemitemparecer_usuariocod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Rangefilterto", StringUtil.RTrim( Ddo_contagemitemparecer_usuariocod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Searchbuttontext", StringUtil.RTrim( Ddo_contagemitemparecer_usuariocod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Caption", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoacod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Tooltip", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoacod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Cls", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoacod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Filteredtext_set", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoacod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoacod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoacod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoacod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Includesortasc", StringUtil.BoolToStr( Ddo_contagemitemparecer_usuariopessoacod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemitemparecer_usuariopessoacod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Sortedstatus", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoacod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Includefilter", StringUtil.BoolToStr( Ddo_contagemitemparecer_usuariopessoacod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Filtertype", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoacod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Filterisrange", StringUtil.BoolToStr( Ddo_contagemitemparecer_usuariopessoacod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Includedatalist", StringUtil.BoolToStr( Ddo_contagemitemparecer_usuariopessoacod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Sortasc", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoacod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Sortdsc", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoacod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Cleanfilter", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoacod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoacod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Rangefilterto", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoacod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Searchbuttontext", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoacod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Caption", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoanom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Tooltip", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoanom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Cls", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoanom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Filteredtext_set", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoanom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoanom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoanom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoanom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Includesortasc", StringUtil.BoolToStr( Ddo_contagemitemparecer_usuariopessoanom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemitemparecer_usuariopessoanom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Sortedstatus", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoanom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Includefilter", StringUtil.BoolToStr( Ddo_contagemitemparecer_usuariopessoanom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Filtertype", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoanom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Filterisrange", StringUtil.BoolToStr( Ddo_contagemitemparecer_usuariopessoanom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Includedatalist", StringUtil.BoolToStr( Ddo_contagemitemparecer_usuariopessoanom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Datalisttype", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoanom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Datalistproc", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoanom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemitemparecer_usuariopessoanom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Sortasc", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoanom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Sortdsc", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoanom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Loadingdata", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoanom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Cleanfilter", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoanom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Noresultsfound", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoanom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Searchbuttontext", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoanom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_contagemitemparecer_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_contagemitemparecer_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemitemparecer_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_LANCAMENTO_Activeeventkey", StringUtil.RTrim( Ddo_contagemitem_lancamento_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_LANCAMENTO_Filteredtext_get", StringUtil.RTrim( Ddo_contagemitem_lancamento_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_LANCAMENTO_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemitem_lancamento_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_COMENTARIO_Activeeventkey", StringUtil.RTrim( Ddo_contagemitemparecer_comentario_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_COMENTARIO_Filteredtext_get", StringUtil.RTrim( Ddo_contagemitemparecer_comentario_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_COMENTARIO_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemitemparecer_comentario_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_DATA_Activeeventkey", StringUtil.RTrim( Ddo_contagemitemparecer_data_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_DATA_Filteredtext_get", StringUtil.RTrim( Ddo_contagemitemparecer_data_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_DATA_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemitemparecer_data_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Activeeventkey", StringUtil.RTrim( Ddo_contagemitemparecer_usuariocod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Filteredtext_get", StringUtil.RTrim( Ddo_contagemitemparecer_usuariocod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemitemparecer_usuariocod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Activeeventkey", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoacod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Filteredtext_get", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoacod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoacod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Activeeventkey", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoanom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Filteredtext_get", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoanom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemitemparecer_usuariopessoanom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Activeeventkey", StringUtil.RTrim( Ddo_managefilters_Activeeventkey));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE5V2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT5V2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwcontagemitemparecer.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWContagemItemParecer" ;
      }

      public override String GetPgmdesc( )
      {
         return " Contagem Item Parecer" ;
      }

      protected void WB5V0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_5V2( true) ;
         }
         else
         {
            wb_table1_2_5V2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_5V2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_91_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(106, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_91_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV25DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(107, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,107);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavManagefiltersexecutionstep_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV92ManageFiltersExecutionStep), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV92ManageFiltersExecutionStep), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavManagefiltersexecutionstep_Jsonclick, 0, "Attribute", "", "", "", edtavManagefiltersexecutionstep_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemItemParecer.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemitemparecer_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV58TFContagemItemParecer_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV58TFContagemItemParecer_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemitemparecer_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemitemparecer_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemItemParecer.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemitemparecer_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV59TFContagemItemParecer_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV59TFContagemItemParecer_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemitemparecer_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemitemparecer_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemItemParecer.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemitem_lancamento_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV62TFContagemItem_Lancamento), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV62TFContagemItem_Lancamento), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemitem_lancamento_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemitem_lancamento_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemItemParecer.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemitem_lancamento_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV63TFContagemItem_Lancamento_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV63TFContagemItem_Lancamento_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,112);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemitem_lancamento_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemitem_lancamento_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemItemParecer.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_91_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfcontagemitemparecer_comentario_Internalname, AV66TFContagemItemParecer_Comentario, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,113);\"", 0, edtavTfcontagemitemparecer_comentario_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWContagemItemParecer.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_91_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfcontagemitemparecer_comentario_sel_Internalname, AV67TFContagemItemParecer_Comentario_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,114);\"", 0, edtavTfcontagemitemparecer_comentario_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWContagemItemParecer.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_91_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemitemparecer_data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemitemparecer_data_Internalname, context.localUtil.TToC( AV70TFContagemItemParecer_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV70TFContagemItemParecer_Data, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemitemparecer_data_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemitemparecer_data_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemItemParecer.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemitemparecer_data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemitemparecer_data_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemItemParecer.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_91_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemitemparecer_data_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemitemparecer_data_to_Internalname, context.localUtil.TToC( AV71TFContagemItemParecer_Data_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV71TFContagemItemParecer_Data_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemitemparecer_data_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemitemparecer_data_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemItemParecer.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemitemparecer_data_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemitemparecer_data_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemItemParecer.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contagemitemparecer_dataauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_91_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemitemparecer_dataauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemitemparecer_dataauxdate_Internalname, context.localUtil.Format(AV72DDO_ContagemItemParecer_DataAuxDate, "99/99/99"), context.localUtil.Format( AV72DDO_ContagemItemParecer_DataAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,118);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemitemparecer_dataauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemItemParecer.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemitemparecer_dataauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemItemParecer.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_91_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemitemparecer_dataauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemitemparecer_dataauxdateto_Internalname, context.localUtil.Format(AV73DDO_ContagemItemParecer_DataAuxDateTo, "99/99/99"), context.localUtil.Format( AV73DDO_ContagemItemParecer_DataAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,119);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemitemparecer_dataauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemItemParecer.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemitemparecer_dataauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemItemParecer.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemitemparecer_usuariocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV76TFContagemItemParecer_UsuarioCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV76TFContagemItemParecer_UsuarioCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,120);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemitemparecer_usuariocod_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemitemparecer_usuariocod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemItemParecer.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemitemparecer_usuariocod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV77TFContagemItemParecer_UsuarioCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV77TFContagemItemParecer_UsuarioCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,121);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemitemparecer_usuariocod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemitemparecer_usuariocod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemItemParecer.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemitemparecer_usuariopessoacod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV80TFContagemItemParecer_UsuarioPessoaCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV80TFContagemItemParecer_UsuarioPessoaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,122);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemitemparecer_usuariopessoacod_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemitemparecer_usuariopessoacod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemItemParecer.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemitemparecer_usuariopessoacod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV81TFContagemItemParecer_UsuarioPessoaCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV81TFContagemItemParecer_UsuarioPessoaCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,123);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemitemparecer_usuariopessoacod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemitemparecer_usuariopessoacod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemItemParecer.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemitemparecer_usuariopessoanom_Internalname, StringUtil.RTrim( AV84TFContagemItemParecer_UsuarioPessoaNom), StringUtil.RTrim( context.localUtil.Format( AV84TFContagemItemParecer_UsuarioPessoaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,124);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemitemparecer_usuariopessoanom_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontagemitemparecer_usuariopessoanom_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContagemItemParecer.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemitemparecer_usuariopessoanom_sel_Internalname, StringUtil.RTrim( AV85TFContagemItemParecer_UsuarioPessoaNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV85TFContagemItemParecer_UsuarioPessoaNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,125);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemitemparecer_usuariopessoanom_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontagemitemparecer_usuariopessoanom_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContagemItemParecer.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMITEMPARECER_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'" + sGXsfl_91_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemitemparecer_codigotitlecontrolidtoreplace_Internalname, AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,127);\"", 0, edtavDdo_contagemitemparecer_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContagemItemParecer.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMITEM_LANCAMENTOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 129,'',false,'" + sGXsfl_91_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemitem_lancamentotitlecontrolidtoreplace_Internalname, AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,129);\"", 0, edtavDdo_contagemitem_lancamentotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContagemItemParecer.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMITEMPARECER_COMENTARIOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'" + sGXsfl_91_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemitemparecer_comentariotitlecontrolidtoreplace_Internalname, AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,131);\"", 0, edtavDdo_contagemitemparecer_comentariotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContagemItemParecer.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMITEMPARECER_DATAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'" + sGXsfl_91_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemitemparecer_datatitlecontrolidtoreplace_Internalname, AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,133);\"", 0, edtavDdo_contagemitemparecer_datatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContagemItemParecer.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMITEMPARECER_USUARIOCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'" + sGXsfl_91_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemitemparecer_usuariocodtitlecontrolidtoreplace_Internalname, AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,135);\"", 0, edtavDdo_contagemitemparecer_usuariocodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContagemItemParecer.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMITEMPARECER_USUARIOPESSOACODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 137,'',false,'" + sGXsfl_91_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemitemparecer_usuariopessoacodtitlecontrolidtoreplace_Internalname, AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,137);\"", 0, edtavDdo_contagemitemparecer_usuariopessoacodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContagemItemParecer.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 139,'',false,'" + sGXsfl_91_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemitemparecer_usuariopessoanomtitlecontrolidtoreplace_Internalname, AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,139);\"", 0, edtavDdo_contagemitemparecer_usuariopessoanomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContagemItemParecer.htm");
         }
         wbLoad = true;
      }

      protected void START5V2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Contagem Item Parecer", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP5V0( ) ;
      }

      protected void WS5V2( )
      {
         START5V2( ) ;
         EVT5V2( ) ;
      }

      protected void EVT5V2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MANAGEFILTERS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E115V2 */
                              E115V2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E125V2 */
                              E125V2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMITEMPARECER_CODIGO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E135V2 */
                              E135V2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMITEM_LANCAMENTO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E145V2 */
                              E145V2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMITEMPARECER_COMENTARIO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E155V2 */
                              E155V2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMITEMPARECER_DATA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E165V2 */
                              E165V2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMITEMPARECER_USUARIOCOD.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E175V2 */
                              E175V2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E185V2 */
                              E185V2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E195V2 */
                              E195V2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E205V2 */
                              E205V2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E215V2 */
                              E215V2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E225V2 */
                              E225V2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E235V2 */
                              E235V2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E245V2 */
                              E245V2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E255V2 */
                              E255V2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E265V2 */
                              E265V2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E275V2 */
                              E275V2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E285V2 */
                              E285V2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E295V2 */
                              E295V2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_91_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_91_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_91_idx), 4, 0)), 4, "0");
                              SubsflControlProps_912( ) ;
                              AV52Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV52Update)) ? AV128Update_GXI : context.convertURL( context.PathToRelativeUrl( AV52Update))));
                              AV53Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV53Delete)) ? AV129Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV53Delete))));
                              AV91Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV91Display)) ? AV130Display_GXI : context.convertURL( context.PathToRelativeUrl( AV91Display))));
                              A243ContagemItemParecer_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemItemParecer_Codigo_Internalname), ",", "."));
                              A224ContagemItem_Lancamento = (int)(context.localUtil.CToN( cgiGet( edtContagemItem_Lancamento_Internalname), ",", "."));
                              A244ContagemItemParecer_Comentario = cgiGet( edtContagemItemParecer_Comentario_Internalname);
                              A245ContagemItemParecer_Data = context.localUtil.CToT( cgiGet( edtContagemItemParecer_Data_Internalname), 0);
                              A246ContagemItemParecer_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( edtContagemItemParecer_UsuarioCod_Internalname), ",", "."));
                              A247ContagemItemParecer_UsuarioPessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContagemItemParecer_UsuarioPessoaCod_Internalname), ",", "."));
                              n247ContagemItemParecer_UsuarioPessoaCod = false;
                              A248ContagemItemParecer_UsuarioPessoaNom = StringUtil.Upper( cgiGet( edtContagemItemParecer_UsuarioPessoaNom_Internalname));
                              n248ContagemItemParecer_UsuarioPessoaNom = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E305V2 */
                                    E305V2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E315V2 */
                                    E315V2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E325V2 */
                                    E325V2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemitemparecer_comentario1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMITEMPARECER_COMENTARIO1"), AV18ContagemItemParecer_Comentario1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemitemparecer_usuariopessoanom1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMITEMPARECER_USUARIOPESSOANOM1"), AV19ContagemItemParecer_UsuarioPessoaNom1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemitemparecer_comentario2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMITEMPARECER_COMENTARIO2"), AV23ContagemItemParecer_Comentario2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemitemparecer_usuariopessoanom2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMITEMPARECER_USUARIOPESSOANOM2"), AV24ContagemItemParecer_UsuarioPessoaNom2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV26DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV27DynamicFiltersOperator3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemitemparecer_comentario3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMITEMPARECER_COMENTARIO3"), AV28ContagemItemParecer_Comentario3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemitemparecer_usuariopessoanom3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMITEMPARECER_USUARIOPESSOANOM3"), AV29ContagemItemParecer_UsuarioPessoaNom3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV25DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemitemparecer_codigo Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMITEMPARECER_CODIGO"), ",", ".") != Convert.ToDecimal( AV58TFContagemItemParecer_Codigo )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemitemparecer_codigo_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMITEMPARECER_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV59TFContagemItemParecer_Codigo_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemitem_lancamento Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMITEM_LANCAMENTO"), ",", ".") != Convert.ToDecimal( AV62TFContagemItem_Lancamento )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemitem_lancamento_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMITEM_LANCAMENTO_TO"), ",", ".") != Convert.ToDecimal( AV63TFContagemItem_Lancamento_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemitemparecer_comentario Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMITEMPARECER_COMENTARIO"), AV66TFContagemItemParecer_Comentario) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemitemparecer_comentario_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMITEMPARECER_COMENTARIO_SEL"), AV67TFContagemItemParecer_Comentario_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemitemparecer_data Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMITEMPARECER_DATA"), 0) != AV70TFContagemItemParecer_Data )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemitemparecer_data_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMITEMPARECER_DATA_TO"), 0) != AV71TFContagemItemParecer_Data_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemitemparecer_usuariocod Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMITEMPARECER_USUARIOCOD"), ",", ".") != Convert.ToDecimal( AV76TFContagemItemParecer_UsuarioCod )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemitemparecer_usuariocod_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMITEMPARECER_USUARIOCOD_TO"), ",", ".") != Convert.ToDecimal( AV77TFContagemItemParecer_UsuarioCod_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemitemparecer_usuariopessoacod Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD"), ",", ".") != Convert.ToDecimal( AV80TFContagemItemParecer_UsuarioPessoaCod )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemitemparecer_usuariopessoacod_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD_TO"), ",", ".") != Convert.ToDecimal( AV81TFContagemItemParecer_UsuarioPessoaCod_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemitemparecer_usuariopessoanom Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM"), AV84TFContagemItemParecer_UsuarioPessoaNom) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemitemparecer_usuariopessoanom_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM_SEL"), AV85TFContagemItemParecer_UsuarioPessoaNom_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE5V2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA5V2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTAGEMITEMPARECER_COMENTARIO", "Coment�rios", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMITEMPARECER_USUARIOPESSOANOM", "do Usu�rio", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTAGEMITEMPARECER_COMENTARIO", "Coment�rios", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMITEMPARECER_USUARIOPESSOANOM", "do Usu�rio", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            cmbavDynamicfiltersoperator2.addItem("2", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("3", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTAGEMITEMPARECER_COMENTARIO", "Coment�rios", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMITEMPARECER_USUARIOPESSOANOM", "do Usu�rio", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV26DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV26DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            cmbavDynamicfiltersoperator3.addItem("2", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("3", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV27DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_912( ) ;
         while ( nGXsfl_91_idx <= nRC_GXsfl_91 )
         {
            sendrow_912( ) ;
            nGXsfl_91_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_91_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_91_idx+1));
            sGXsfl_91_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_91_idx), 4, 0)), 4, "0");
            SubsflControlProps_912( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       short AV17DynamicFiltersOperator1 ,
                                       String AV18ContagemItemParecer_Comentario1 ,
                                       String AV19ContagemItemParecer_UsuarioPessoaNom1 ,
                                       String AV21DynamicFiltersSelector2 ,
                                       short AV22DynamicFiltersOperator2 ,
                                       String AV23ContagemItemParecer_Comentario2 ,
                                       String AV24ContagemItemParecer_UsuarioPessoaNom2 ,
                                       String AV26DynamicFiltersSelector3 ,
                                       short AV27DynamicFiltersOperator3 ,
                                       String AV28ContagemItemParecer_Comentario3 ,
                                       String AV29ContagemItemParecer_UsuarioPessoaNom3 ,
                                       bool AV20DynamicFiltersEnabled2 ,
                                       bool AV25DynamicFiltersEnabled3 ,
                                       int AV58TFContagemItemParecer_Codigo ,
                                       int AV59TFContagemItemParecer_Codigo_To ,
                                       int AV62TFContagemItem_Lancamento ,
                                       int AV63TFContagemItem_Lancamento_To ,
                                       String AV66TFContagemItemParecer_Comentario ,
                                       String AV67TFContagemItemParecer_Comentario_Sel ,
                                       DateTime AV70TFContagemItemParecer_Data ,
                                       DateTime AV71TFContagemItemParecer_Data_To ,
                                       int AV76TFContagemItemParecer_UsuarioCod ,
                                       int AV77TFContagemItemParecer_UsuarioCod_To ,
                                       int AV80TFContagemItemParecer_UsuarioPessoaCod ,
                                       int AV81TFContagemItemParecer_UsuarioPessoaCod_To ,
                                       String AV84TFContagemItemParecer_UsuarioPessoaNom ,
                                       String AV85TFContagemItemParecer_UsuarioPessoaNom_Sel ,
                                       short AV92ManageFiltersExecutionStep ,
                                       String AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace ,
                                       String AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace ,
                                       String AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace ,
                                       String AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace ,
                                       String AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace ,
                                       String AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace ,
                                       String AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV131Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV31DynamicFiltersIgnoreFirst ,
                                       bool AV30DynamicFiltersRemoving ,
                                       int A243ContagemItemParecer_Codigo ,
                                       int A224ContagemItem_Lancamento ,
                                       int A246ContagemItemParecer_UsuarioCod )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF5V2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEMPARECER_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A243ContagemItemParecer_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMITEMPARECER_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A243ContagemItemParecer_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEMPARECER_COMENTARIO", GetSecureSignedToken( "", A244ContagemItemParecer_Comentario));
         GxWebStd.gx_hidden_field( context, "CONTAGEMITEMPARECER_COMENTARIO", A244ContagemItemParecer_Comentario);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEMPARECER_DATA", GetSecureSignedToken( "", context.localUtil.Format( A245ContagemItemParecer_Data, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMITEMPARECER_DATA", context.localUtil.TToC( A245ContagemItemParecer_Data, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEMPARECER_USUARIOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A246ContagemItemParecer_UsuarioCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMITEMPARECER_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A246ContagemItemParecer_UsuarioCod), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV26DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV26DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV27DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF5V2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV131Pgmname = "WWContagemItemParecer";
         context.Gx_err = 0;
      }

      protected void RF5V2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 91;
         /* Execute user event: E315V2 */
         E315V2 ();
         nGXsfl_91_idx = 1;
         sGXsfl_91_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_91_idx), 4, 0)), 4, "0");
         SubsflControlProps_912( ) ;
         nGXsfl_91_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_912( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV100WWContagemItemParecerDS_1_Dynamicfiltersselector1 ,
                                                 AV101WWContagemItemParecerDS_2_Dynamicfiltersoperator1 ,
                                                 AV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 ,
                                                 AV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 ,
                                                 AV104WWContagemItemParecerDS_5_Dynamicfiltersenabled2 ,
                                                 AV105WWContagemItemParecerDS_6_Dynamicfiltersselector2 ,
                                                 AV106WWContagemItemParecerDS_7_Dynamicfiltersoperator2 ,
                                                 AV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 ,
                                                 AV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 ,
                                                 AV109WWContagemItemParecerDS_10_Dynamicfiltersenabled3 ,
                                                 AV110WWContagemItemParecerDS_11_Dynamicfiltersselector3 ,
                                                 AV111WWContagemItemParecerDS_12_Dynamicfiltersoperator3 ,
                                                 AV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 ,
                                                 AV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 ,
                                                 AV114WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo ,
                                                 AV115WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to ,
                                                 AV116WWContagemItemParecerDS_17_Tfcontagemitem_lancamento ,
                                                 AV117WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to ,
                                                 AV119WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel ,
                                                 AV118WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario ,
                                                 AV120WWContagemItemParecerDS_21_Tfcontagemitemparecer_data ,
                                                 AV121WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to ,
                                                 AV122WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod ,
                                                 AV123WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to ,
                                                 AV124WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod ,
                                                 AV125WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to ,
                                                 AV127WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel ,
                                                 AV126WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom ,
                                                 A244ContagemItemParecer_Comentario ,
                                                 A248ContagemItemParecer_UsuarioPessoaNom ,
                                                 A243ContagemItemParecer_Codigo ,
                                                 A224ContagemItem_Lancamento ,
                                                 A245ContagemItemParecer_Data ,
                                                 A246ContagemItemParecer_UsuarioCod ,
                                                 A247ContagemItemParecer_UsuarioPessoaCod ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 = StringUtil.Concat( StringUtil.RTrim( AV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1), "%", "");
            lV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 = StringUtil.Concat( StringUtil.RTrim( AV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1), "%", "");
            lV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1), 100, "%");
            lV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1), 100, "%");
            lV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 = StringUtil.Concat( StringUtil.RTrim( AV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2), "%", "");
            lV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 = StringUtil.Concat( StringUtil.RTrim( AV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2), "%", "");
            lV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2), 100, "%");
            lV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2), 100, "%");
            lV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 = StringUtil.Concat( StringUtil.RTrim( AV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3), "%", "");
            lV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 = StringUtil.Concat( StringUtil.RTrim( AV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3), "%", "");
            lV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3), 100, "%");
            lV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3), 100, "%");
            lV118WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario = StringUtil.Concat( StringUtil.RTrim( AV118WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario), "%", "");
            lV126WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom = StringUtil.PadR( StringUtil.RTrim( AV126WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom), 100, "%");
            /* Using cursor H005V2 */
            pr_default.execute(0, new Object[] {lV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1, lV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1, lV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1, lV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1, lV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2, lV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2, lV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2, lV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2, lV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3, lV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3, lV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3, lV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3, AV114WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo, AV115WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to, AV116WWContagemItemParecerDS_17_Tfcontagemitem_lancamento, AV117WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to, lV118WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario, AV119WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel, AV120WWContagemItemParecerDS_21_Tfcontagemitemparecer_data, AV121WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to, AV122WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod, AV123WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to, AV124WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod, AV125WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to, lV126WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom, AV127WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_91_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A248ContagemItemParecer_UsuarioPessoaNom = H005V2_A248ContagemItemParecer_UsuarioPessoaNom[0];
               n248ContagemItemParecer_UsuarioPessoaNom = H005V2_n248ContagemItemParecer_UsuarioPessoaNom[0];
               A247ContagemItemParecer_UsuarioPessoaCod = H005V2_A247ContagemItemParecer_UsuarioPessoaCod[0];
               n247ContagemItemParecer_UsuarioPessoaCod = H005V2_n247ContagemItemParecer_UsuarioPessoaCod[0];
               A246ContagemItemParecer_UsuarioCod = H005V2_A246ContagemItemParecer_UsuarioCod[0];
               A245ContagemItemParecer_Data = H005V2_A245ContagemItemParecer_Data[0];
               A244ContagemItemParecer_Comentario = H005V2_A244ContagemItemParecer_Comentario[0];
               A224ContagemItem_Lancamento = H005V2_A224ContagemItem_Lancamento[0];
               A243ContagemItemParecer_Codigo = H005V2_A243ContagemItemParecer_Codigo[0];
               A247ContagemItemParecer_UsuarioPessoaCod = H005V2_A247ContagemItemParecer_UsuarioPessoaCod[0];
               n247ContagemItemParecer_UsuarioPessoaCod = H005V2_n247ContagemItemParecer_UsuarioPessoaCod[0];
               A248ContagemItemParecer_UsuarioPessoaNom = H005V2_A248ContagemItemParecer_UsuarioPessoaNom[0];
               n248ContagemItemParecer_UsuarioPessoaNom = H005V2_n248ContagemItemParecer_UsuarioPessoaNom[0];
               /* Execute user event: E325V2 */
               E325V2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 91;
            WB5V0( ) ;
         }
         nGXsfl_91_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV100WWContagemItemParecerDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV101WWContagemItemParecerDS_2_Dynamicfiltersoperator1 = AV17DynamicFiltersOperator1;
         AV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 = AV18ContagemItemParecer_Comentario1;
         AV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 = AV19ContagemItemParecer_UsuarioPessoaNom1;
         AV104WWContagemItemParecerDS_5_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV105WWContagemItemParecerDS_6_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV106WWContagemItemParecerDS_7_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 = AV23ContagemItemParecer_Comentario2;
         AV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 = AV24ContagemItemParecer_UsuarioPessoaNom2;
         AV109WWContagemItemParecerDS_10_Dynamicfiltersenabled3 = AV25DynamicFiltersEnabled3;
         AV110WWContagemItemParecerDS_11_Dynamicfiltersselector3 = AV26DynamicFiltersSelector3;
         AV111WWContagemItemParecerDS_12_Dynamicfiltersoperator3 = AV27DynamicFiltersOperator3;
         AV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 = AV28ContagemItemParecer_Comentario3;
         AV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 = AV29ContagemItemParecer_UsuarioPessoaNom3;
         AV114WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo = AV58TFContagemItemParecer_Codigo;
         AV115WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to = AV59TFContagemItemParecer_Codigo_To;
         AV116WWContagemItemParecerDS_17_Tfcontagemitem_lancamento = AV62TFContagemItem_Lancamento;
         AV117WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to = AV63TFContagemItem_Lancamento_To;
         AV118WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario = AV66TFContagemItemParecer_Comentario;
         AV119WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel = AV67TFContagemItemParecer_Comentario_Sel;
         AV120WWContagemItemParecerDS_21_Tfcontagemitemparecer_data = AV70TFContagemItemParecer_Data;
         AV121WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to = AV71TFContagemItemParecer_Data_To;
         AV122WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod = AV76TFContagemItemParecer_UsuarioCod;
         AV123WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to = AV77TFContagemItemParecer_UsuarioCod_To;
         AV124WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod = AV80TFContagemItemParecer_UsuarioPessoaCod;
         AV125WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to = AV81TFContagemItemParecer_UsuarioPessoaCod_To;
         AV126WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom = AV84TFContagemItemParecer_UsuarioPessoaNom;
         AV127WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel = AV85TFContagemItemParecer_UsuarioPessoaNom_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV100WWContagemItemParecerDS_1_Dynamicfiltersselector1 ,
                                              AV101WWContagemItemParecerDS_2_Dynamicfiltersoperator1 ,
                                              AV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 ,
                                              AV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 ,
                                              AV104WWContagemItemParecerDS_5_Dynamicfiltersenabled2 ,
                                              AV105WWContagemItemParecerDS_6_Dynamicfiltersselector2 ,
                                              AV106WWContagemItemParecerDS_7_Dynamicfiltersoperator2 ,
                                              AV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 ,
                                              AV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 ,
                                              AV109WWContagemItemParecerDS_10_Dynamicfiltersenabled3 ,
                                              AV110WWContagemItemParecerDS_11_Dynamicfiltersselector3 ,
                                              AV111WWContagemItemParecerDS_12_Dynamicfiltersoperator3 ,
                                              AV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 ,
                                              AV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 ,
                                              AV114WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo ,
                                              AV115WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to ,
                                              AV116WWContagemItemParecerDS_17_Tfcontagemitem_lancamento ,
                                              AV117WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to ,
                                              AV119WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel ,
                                              AV118WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario ,
                                              AV120WWContagemItemParecerDS_21_Tfcontagemitemparecer_data ,
                                              AV121WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to ,
                                              AV122WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod ,
                                              AV123WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to ,
                                              AV124WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod ,
                                              AV125WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to ,
                                              AV127WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel ,
                                              AV126WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom ,
                                              A244ContagemItemParecer_Comentario ,
                                              A248ContagemItemParecer_UsuarioPessoaNom ,
                                              A243ContagemItemParecer_Codigo ,
                                              A224ContagemItem_Lancamento ,
                                              A245ContagemItemParecer_Data ,
                                              A246ContagemItemParecer_UsuarioCod ,
                                              A247ContagemItemParecer_UsuarioPessoaCod ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 = StringUtil.Concat( StringUtil.RTrim( AV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1), "%", "");
         lV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 = StringUtil.Concat( StringUtil.RTrim( AV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1), "%", "");
         lV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1), 100, "%");
         lV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1), 100, "%");
         lV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 = StringUtil.Concat( StringUtil.RTrim( AV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2), "%", "");
         lV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 = StringUtil.Concat( StringUtil.RTrim( AV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2), "%", "");
         lV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2), 100, "%");
         lV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2), 100, "%");
         lV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 = StringUtil.Concat( StringUtil.RTrim( AV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3), "%", "");
         lV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 = StringUtil.Concat( StringUtil.RTrim( AV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3), "%", "");
         lV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3), 100, "%");
         lV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3), 100, "%");
         lV118WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario = StringUtil.Concat( StringUtil.RTrim( AV118WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario), "%", "");
         lV126WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom = StringUtil.PadR( StringUtil.RTrim( AV126WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom), 100, "%");
         /* Using cursor H005V3 */
         pr_default.execute(1, new Object[] {lV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1, lV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1, lV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1, lV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1, lV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2, lV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2, lV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2, lV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2, lV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3, lV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3, lV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3, lV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3, AV114WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo, AV115WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to, AV116WWContagemItemParecerDS_17_Tfcontagemitem_lancamento, AV117WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to, lV118WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario, AV119WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel, AV120WWContagemItemParecerDS_21_Tfcontagemitemparecer_data, AV121WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to, AV122WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod, AV123WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to, AV124WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod, AV125WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to, lV126WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom, AV127WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel});
         GRID_nRecordCount = H005V3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV100WWContagemItemParecerDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV101WWContagemItemParecerDS_2_Dynamicfiltersoperator1 = AV17DynamicFiltersOperator1;
         AV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 = AV18ContagemItemParecer_Comentario1;
         AV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 = AV19ContagemItemParecer_UsuarioPessoaNom1;
         AV104WWContagemItemParecerDS_5_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV105WWContagemItemParecerDS_6_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV106WWContagemItemParecerDS_7_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 = AV23ContagemItemParecer_Comentario2;
         AV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 = AV24ContagemItemParecer_UsuarioPessoaNom2;
         AV109WWContagemItemParecerDS_10_Dynamicfiltersenabled3 = AV25DynamicFiltersEnabled3;
         AV110WWContagemItemParecerDS_11_Dynamicfiltersselector3 = AV26DynamicFiltersSelector3;
         AV111WWContagemItemParecerDS_12_Dynamicfiltersoperator3 = AV27DynamicFiltersOperator3;
         AV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 = AV28ContagemItemParecer_Comentario3;
         AV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 = AV29ContagemItemParecer_UsuarioPessoaNom3;
         AV114WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo = AV58TFContagemItemParecer_Codigo;
         AV115WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to = AV59TFContagemItemParecer_Codigo_To;
         AV116WWContagemItemParecerDS_17_Tfcontagemitem_lancamento = AV62TFContagemItem_Lancamento;
         AV117WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to = AV63TFContagemItem_Lancamento_To;
         AV118WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario = AV66TFContagemItemParecer_Comentario;
         AV119WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel = AV67TFContagemItemParecer_Comentario_Sel;
         AV120WWContagemItemParecerDS_21_Tfcontagemitemparecer_data = AV70TFContagemItemParecer_Data;
         AV121WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to = AV71TFContagemItemParecer_Data_To;
         AV122WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod = AV76TFContagemItemParecer_UsuarioCod;
         AV123WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to = AV77TFContagemItemParecer_UsuarioCod_To;
         AV124WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod = AV80TFContagemItemParecer_UsuarioPessoaCod;
         AV125WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to = AV81TFContagemItemParecer_UsuarioPessoaCod_To;
         AV126WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom = AV84TFContagemItemParecer_UsuarioPessoaNom;
         AV127WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel = AV85TFContagemItemParecer_UsuarioPessoaNom_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemItemParecer_Comentario1, AV19ContagemItemParecer_UsuarioPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemItemParecer_Comentario2, AV24ContagemItemParecer_UsuarioPessoaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28ContagemItemParecer_Comentario3, AV29ContagemItemParecer_UsuarioPessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV58TFContagemItemParecer_Codigo, AV59TFContagemItemParecer_Codigo_To, AV62TFContagemItem_Lancamento, AV63TFContagemItem_Lancamento_To, AV66TFContagemItemParecer_Comentario, AV67TFContagemItemParecer_Comentario_Sel, AV70TFContagemItemParecer_Data, AV71TFContagemItemParecer_Data_To, AV76TFContagemItemParecer_UsuarioCod, AV77TFContagemItemParecer_UsuarioCod_To, AV80TFContagemItemParecer_UsuarioPessoaCod, AV81TFContagemItemParecer_UsuarioPessoaCod_To, AV84TFContagemItemParecer_UsuarioPessoaNom, AV85TFContagemItemParecer_UsuarioPessoaNom_Sel, AV92ManageFiltersExecutionStep, AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace, AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace, AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace, AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace, AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace, AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace, AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace, AV6WWPContext, AV131Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A243ContagemItemParecer_Codigo, A224ContagemItem_Lancamento, A246ContagemItemParecer_UsuarioCod) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV100WWContagemItemParecerDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV101WWContagemItemParecerDS_2_Dynamicfiltersoperator1 = AV17DynamicFiltersOperator1;
         AV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 = AV18ContagemItemParecer_Comentario1;
         AV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 = AV19ContagemItemParecer_UsuarioPessoaNom1;
         AV104WWContagemItemParecerDS_5_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV105WWContagemItemParecerDS_6_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV106WWContagemItemParecerDS_7_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 = AV23ContagemItemParecer_Comentario2;
         AV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 = AV24ContagemItemParecer_UsuarioPessoaNom2;
         AV109WWContagemItemParecerDS_10_Dynamicfiltersenabled3 = AV25DynamicFiltersEnabled3;
         AV110WWContagemItemParecerDS_11_Dynamicfiltersselector3 = AV26DynamicFiltersSelector3;
         AV111WWContagemItemParecerDS_12_Dynamicfiltersoperator3 = AV27DynamicFiltersOperator3;
         AV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 = AV28ContagemItemParecer_Comentario3;
         AV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 = AV29ContagemItemParecer_UsuarioPessoaNom3;
         AV114WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo = AV58TFContagemItemParecer_Codigo;
         AV115WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to = AV59TFContagemItemParecer_Codigo_To;
         AV116WWContagemItemParecerDS_17_Tfcontagemitem_lancamento = AV62TFContagemItem_Lancamento;
         AV117WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to = AV63TFContagemItem_Lancamento_To;
         AV118WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario = AV66TFContagemItemParecer_Comentario;
         AV119WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel = AV67TFContagemItemParecer_Comentario_Sel;
         AV120WWContagemItemParecerDS_21_Tfcontagemitemparecer_data = AV70TFContagemItemParecer_Data;
         AV121WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to = AV71TFContagemItemParecer_Data_To;
         AV122WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod = AV76TFContagemItemParecer_UsuarioCod;
         AV123WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to = AV77TFContagemItemParecer_UsuarioCod_To;
         AV124WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod = AV80TFContagemItemParecer_UsuarioPessoaCod;
         AV125WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to = AV81TFContagemItemParecer_UsuarioPessoaCod_To;
         AV126WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom = AV84TFContagemItemParecer_UsuarioPessoaNom;
         AV127WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel = AV85TFContagemItemParecer_UsuarioPessoaNom_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemItemParecer_Comentario1, AV19ContagemItemParecer_UsuarioPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemItemParecer_Comentario2, AV24ContagemItemParecer_UsuarioPessoaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28ContagemItemParecer_Comentario3, AV29ContagemItemParecer_UsuarioPessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV58TFContagemItemParecer_Codigo, AV59TFContagemItemParecer_Codigo_To, AV62TFContagemItem_Lancamento, AV63TFContagemItem_Lancamento_To, AV66TFContagemItemParecer_Comentario, AV67TFContagemItemParecer_Comentario_Sel, AV70TFContagemItemParecer_Data, AV71TFContagemItemParecer_Data_To, AV76TFContagemItemParecer_UsuarioCod, AV77TFContagemItemParecer_UsuarioCod_To, AV80TFContagemItemParecer_UsuarioPessoaCod, AV81TFContagemItemParecer_UsuarioPessoaCod_To, AV84TFContagemItemParecer_UsuarioPessoaNom, AV85TFContagemItemParecer_UsuarioPessoaNom_Sel, AV92ManageFiltersExecutionStep, AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace, AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace, AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace, AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace, AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace, AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace, AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace, AV6WWPContext, AV131Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A243ContagemItemParecer_Codigo, A224ContagemItem_Lancamento, A246ContagemItemParecer_UsuarioCod) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV100WWContagemItemParecerDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV101WWContagemItemParecerDS_2_Dynamicfiltersoperator1 = AV17DynamicFiltersOperator1;
         AV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 = AV18ContagemItemParecer_Comentario1;
         AV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 = AV19ContagemItemParecer_UsuarioPessoaNom1;
         AV104WWContagemItemParecerDS_5_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV105WWContagemItemParecerDS_6_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV106WWContagemItemParecerDS_7_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 = AV23ContagemItemParecer_Comentario2;
         AV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 = AV24ContagemItemParecer_UsuarioPessoaNom2;
         AV109WWContagemItemParecerDS_10_Dynamicfiltersenabled3 = AV25DynamicFiltersEnabled3;
         AV110WWContagemItemParecerDS_11_Dynamicfiltersselector3 = AV26DynamicFiltersSelector3;
         AV111WWContagemItemParecerDS_12_Dynamicfiltersoperator3 = AV27DynamicFiltersOperator3;
         AV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 = AV28ContagemItemParecer_Comentario3;
         AV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 = AV29ContagemItemParecer_UsuarioPessoaNom3;
         AV114WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo = AV58TFContagemItemParecer_Codigo;
         AV115WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to = AV59TFContagemItemParecer_Codigo_To;
         AV116WWContagemItemParecerDS_17_Tfcontagemitem_lancamento = AV62TFContagemItem_Lancamento;
         AV117WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to = AV63TFContagemItem_Lancamento_To;
         AV118WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario = AV66TFContagemItemParecer_Comentario;
         AV119WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel = AV67TFContagemItemParecer_Comentario_Sel;
         AV120WWContagemItemParecerDS_21_Tfcontagemitemparecer_data = AV70TFContagemItemParecer_Data;
         AV121WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to = AV71TFContagemItemParecer_Data_To;
         AV122WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod = AV76TFContagemItemParecer_UsuarioCod;
         AV123WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to = AV77TFContagemItemParecer_UsuarioCod_To;
         AV124WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod = AV80TFContagemItemParecer_UsuarioPessoaCod;
         AV125WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to = AV81TFContagemItemParecer_UsuarioPessoaCod_To;
         AV126WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom = AV84TFContagemItemParecer_UsuarioPessoaNom;
         AV127WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel = AV85TFContagemItemParecer_UsuarioPessoaNom_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemItemParecer_Comentario1, AV19ContagemItemParecer_UsuarioPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemItemParecer_Comentario2, AV24ContagemItemParecer_UsuarioPessoaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28ContagemItemParecer_Comentario3, AV29ContagemItemParecer_UsuarioPessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV58TFContagemItemParecer_Codigo, AV59TFContagemItemParecer_Codigo_To, AV62TFContagemItem_Lancamento, AV63TFContagemItem_Lancamento_To, AV66TFContagemItemParecer_Comentario, AV67TFContagemItemParecer_Comentario_Sel, AV70TFContagemItemParecer_Data, AV71TFContagemItemParecer_Data_To, AV76TFContagemItemParecer_UsuarioCod, AV77TFContagemItemParecer_UsuarioCod_To, AV80TFContagemItemParecer_UsuarioPessoaCod, AV81TFContagemItemParecer_UsuarioPessoaCod_To, AV84TFContagemItemParecer_UsuarioPessoaNom, AV85TFContagemItemParecer_UsuarioPessoaNom_Sel, AV92ManageFiltersExecutionStep, AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace, AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace, AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace, AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace, AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace, AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace, AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace, AV6WWPContext, AV131Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A243ContagemItemParecer_Codigo, A224ContagemItem_Lancamento, A246ContagemItemParecer_UsuarioCod) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV100WWContagemItemParecerDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV101WWContagemItemParecerDS_2_Dynamicfiltersoperator1 = AV17DynamicFiltersOperator1;
         AV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 = AV18ContagemItemParecer_Comentario1;
         AV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 = AV19ContagemItemParecer_UsuarioPessoaNom1;
         AV104WWContagemItemParecerDS_5_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV105WWContagemItemParecerDS_6_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV106WWContagemItemParecerDS_7_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 = AV23ContagemItemParecer_Comentario2;
         AV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 = AV24ContagemItemParecer_UsuarioPessoaNom2;
         AV109WWContagemItemParecerDS_10_Dynamicfiltersenabled3 = AV25DynamicFiltersEnabled3;
         AV110WWContagemItemParecerDS_11_Dynamicfiltersselector3 = AV26DynamicFiltersSelector3;
         AV111WWContagemItemParecerDS_12_Dynamicfiltersoperator3 = AV27DynamicFiltersOperator3;
         AV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 = AV28ContagemItemParecer_Comentario3;
         AV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 = AV29ContagemItemParecer_UsuarioPessoaNom3;
         AV114WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo = AV58TFContagemItemParecer_Codigo;
         AV115WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to = AV59TFContagemItemParecer_Codigo_To;
         AV116WWContagemItemParecerDS_17_Tfcontagemitem_lancamento = AV62TFContagemItem_Lancamento;
         AV117WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to = AV63TFContagemItem_Lancamento_To;
         AV118WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario = AV66TFContagemItemParecer_Comentario;
         AV119WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel = AV67TFContagemItemParecer_Comentario_Sel;
         AV120WWContagemItemParecerDS_21_Tfcontagemitemparecer_data = AV70TFContagemItemParecer_Data;
         AV121WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to = AV71TFContagemItemParecer_Data_To;
         AV122WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod = AV76TFContagemItemParecer_UsuarioCod;
         AV123WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to = AV77TFContagemItemParecer_UsuarioCod_To;
         AV124WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod = AV80TFContagemItemParecer_UsuarioPessoaCod;
         AV125WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to = AV81TFContagemItemParecer_UsuarioPessoaCod_To;
         AV126WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom = AV84TFContagemItemParecer_UsuarioPessoaNom;
         AV127WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel = AV85TFContagemItemParecer_UsuarioPessoaNom_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemItemParecer_Comentario1, AV19ContagemItemParecer_UsuarioPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemItemParecer_Comentario2, AV24ContagemItemParecer_UsuarioPessoaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28ContagemItemParecer_Comentario3, AV29ContagemItemParecer_UsuarioPessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV58TFContagemItemParecer_Codigo, AV59TFContagemItemParecer_Codigo_To, AV62TFContagemItem_Lancamento, AV63TFContagemItem_Lancamento_To, AV66TFContagemItemParecer_Comentario, AV67TFContagemItemParecer_Comentario_Sel, AV70TFContagemItemParecer_Data, AV71TFContagemItemParecer_Data_To, AV76TFContagemItemParecer_UsuarioCod, AV77TFContagemItemParecer_UsuarioCod_To, AV80TFContagemItemParecer_UsuarioPessoaCod, AV81TFContagemItemParecer_UsuarioPessoaCod_To, AV84TFContagemItemParecer_UsuarioPessoaNom, AV85TFContagemItemParecer_UsuarioPessoaNom_Sel, AV92ManageFiltersExecutionStep, AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace, AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace, AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace, AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace, AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace, AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace, AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace, AV6WWPContext, AV131Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A243ContagemItemParecer_Codigo, A224ContagemItem_Lancamento, A246ContagemItemParecer_UsuarioCod) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV100WWContagemItemParecerDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV101WWContagemItemParecerDS_2_Dynamicfiltersoperator1 = AV17DynamicFiltersOperator1;
         AV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 = AV18ContagemItemParecer_Comentario1;
         AV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 = AV19ContagemItemParecer_UsuarioPessoaNom1;
         AV104WWContagemItemParecerDS_5_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV105WWContagemItemParecerDS_6_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV106WWContagemItemParecerDS_7_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 = AV23ContagemItemParecer_Comentario2;
         AV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 = AV24ContagemItemParecer_UsuarioPessoaNom2;
         AV109WWContagemItemParecerDS_10_Dynamicfiltersenabled3 = AV25DynamicFiltersEnabled3;
         AV110WWContagemItemParecerDS_11_Dynamicfiltersselector3 = AV26DynamicFiltersSelector3;
         AV111WWContagemItemParecerDS_12_Dynamicfiltersoperator3 = AV27DynamicFiltersOperator3;
         AV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 = AV28ContagemItemParecer_Comentario3;
         AV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 = AV29ContagemItemParecer_UsuarioPessoaNom3;
         AV114WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo = AV58TFContagemItemParecer_Codigo;
         AV115WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to = AV59TFContagemItemParecer_Codigo_To;
         AV116WWContagemItemParecerDS_17_Tfcontagemitem_lancamento = AV62TFContagemItem_Lancamento;
         AV117WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to = AV63TFContagemItem_Lancamento_To;
         AV118WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario = AV66TFContagemItemParecer_Comentario;
         AV119WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel = AV67TFContagemItemParecer_Comentario_Sel;
         AV120WWContagemItemParecerDS_21_Tfcontagemitemparecer_data = AV70TFContagemItemParecer_Data;
         AV121WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to = AV71TFContagemItemParecer_Data_To;
         AV122WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod = AV76TFContagemItemParecer_UsuarioCod;
         AV123WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to = AV77TFContagemItemParecer_UsuarioCod_To;
         AV124WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod = AV80TFContagemItemParecer_UsuarioPessoaCod;
         AV125WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to = AV81TFContagemItemParecer_UsuarioPessoaCod_To;
         AV126WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom = AV84TFContagemItemParecer_UsuarioPessoaNom;
         AV127WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel = AV85TFContagemItemParecer_UsuarioPessoaNom_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemItemParecer_Comentario1, AV19ContagemItemParecer_UsuarioPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemItemParecer_Comentario2, AV24ContagemItemParecer_UsuarioPessoaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28ContagemItemParecer_Comentario3, AV29ContagemItemParecer_UsuarioPessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV58TFContagemItemParecer_Codigo, AV59TFContagemItemParecer_Codigo_To, AV62TFContagemItem_Lancamento, AV63TFContagemItem_Lancamento_To, AV66TFContagemItemParecer_Comentario, AV67TFContagemItemParecer_Comentario_Sel, AV70TFContagemItemParecer_Data, AV71TFContagemItemParecer_Data_To, AV76TFContagemItemParecer_UsuarioCod, AV77TFContagemItemParecer_UsuarioCod_To, AV80TFContagemItemParecer_UsuarioPessoaCod, AV81TFContagemItemParecer_UsuarioPessoaCod_To, AV84TFContagemItemParecer_UsuarioPessoaNom, AV85TFContagemItemParecer_UsuarioPessoaNom_Sel, AV92ManageFiltersExecutionStep, AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace, AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace, AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace, AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace, AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace, AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace, AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace, AV6WWPContext, AV131Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A243ContagemItemParecer_Codigo, A224ContagemItem_Lancamento, A246ContagemItemParecer_UsuarioCod) ;
         }
         return (int)(0) ;
      }

      protected void STRUP5V0( )
      {
         /* Before Start, stand alone formulas. */
         AV131Pgmname = "WWContagemItemParecer";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E305V2 */
         E305V2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vMANAGEFILTERSDATA"), AV96ManageFiltersData);
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV87DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMITEMPARECER_CODIGOTITLEFILTERDATA"), AV57ContagemItemParecer_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMITEM_LANCAMENTOTITLEFILTERDATA"), AV61ContagemItem_LancamentoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMITEMPARECER_COMENTARIOTITLEFILTERDATA"), AV65ContagemItemParecer_ComentarioTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMITEMPARECER_DATATITLEFILTERDATA"), AV69ContagemItemParecer_DataTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMITEMPARECER_USUARIOCODTITLEFILTERDATA"), AV75ContagemItemParecer_UsuarioCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMITEMPARECER_USUARIOPESSOACODTITLEFILTERDATA"), AV79ContagemItemParecer_UsuarioPessoaCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMITEMPARECER_USUARIOPESSOANOMTITLEFILTERDATA"), AV83ContagemItemParecer_UsuarioPessoaNomTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            AV18ContagemItemParecer_Comentario1 = cgiGet( edtavContagemitemparecer_comentario1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemItemParecer_Comentario1", AV18ContagemItemParecer_Comentario1);
            AV19ContagemItemParecer_UsuarioPessoaNom1 = StringUtil.Upper( cgiGet( edtavContagemitemparecer_usuariopessoanom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemItemParecer_UsuarioPessoaNom1", AV19ContagemItemParecer_UsuarioPessoaNom1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV21DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            AV23ContagemItemParecer_Comentario2 = cgiGet( edtavContagemitemparecer_comentario2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemItemParecer_Comentario2", AV23ContagemItemParecer_Comentario2);
            AV24ContagemItemParecer_UsuarioPessoaNom2 = StringUtil.Upper( cgiGet( edtavContagemitemparecer_usuariopessoanom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemItemParecer_UsuarioPessoaNom2", AV24ContagemItemParecer_UsuarioPessoaNom2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV26DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV27DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
            AV28ContagemItemParecer_Comentario3 = cgiGet( edtavContagemitemparecer_comentario3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ContagemItemParecer_Comentario3", AV28ContagemItemParecer_Comentario3);
            AV29ContagemItemParecer_UsuarioPessoaNom3 = StringUtil.Upper( cgiGet( edtavContagemitemparecer_usuariopessoanom3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemItemParecer_UsuarioPessoaNom3", AV29ContagemItemParecer_UsuarioPessoaNom3);
            AV20DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
            AV25DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vMANAGEFILTERSEXECUTIONSTEP");
               GX_FocusControl = edtavManagefiltersexecutionstep_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV92ManageFiltersExecutionStep = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV92ManageFiltersExecutionStep), 1, 0));
            }
            else
            {
               AV92ManageFiltersExecutionStep = (short)(context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV92ManageFiltersExecutionStep), 1, 0));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemitemparecer_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemitemparecer_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMITEMPARECER_CODIGO");
               GX_FocusControl = edtavTfcontagemitemparecer_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV58TFContagemItemParecer_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFContagemItemParecer_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFContagemItemParecer_Codigo), 6, 0)));
            }
            else
            {
               AV58TFContagemItemParecer_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfcontagemitemparecer_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFContagemItemParecer_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFContagemItemParecer_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemitemparecer_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemitemparecer_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMITEMPARECER_CODIGO_TO");
               GX_FocusControl = edtavTfcontagemitemparecer_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV59TFContagemItemParecer_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContagemItemParecer_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFContagemItemParecer_Codigo_To), 6, 0)));
            }
            else
            {
               AV59TFContagemItemParecer_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontagemitemparecer_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContagemItemParecer_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFContagemItemParecer_Codigo_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemitem_lancamento_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemitem_lancamento_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMITEM_LANCAMENTO");
               GX_FocusControl = edtavTfcontagemitem_lancamento_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV62TFContagemItem_Lancamento = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFContagemItem_Lancamento), 6, 0)));
            }
            else
            {
               AV62TFContagemItem_Lancamento = (int)(context.localUtil.CToN( cgiGet( edtavTfcontagemitem_lancamento_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFContagemItem_Lancamento), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemitem_lancamento_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemitem_lancamento_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMITEM_LANCAMENTO_TO");
               GX_FocusControl = edtavTfcontagemitem_lancamento_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV63TFContagemItem_Lancamento_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContagemItem_Lancamento_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63TFContagemItem_Lancamento_To), 6, 0)));
            }
            else
            {
               AV63TFContagemItem_Lancamento_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontagemitem_lancamento_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContagemItem_Lancamento_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63TFContagemItem_Lancamento_To), 6, 0)));
            }
            AV66TFContagemItemParecer_Comentario = cgiGet( edtavTfcontagemitemparecer_comentario_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContagemItemParecer_Comentario", AV66TFContagemItemParecer_Comentario);
            AV67TFContagemItemParecer_Comentario_Sel = cgiGet( edtavTfcontagemitemparecer_comentario_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContagemItemParecer_Comentario_Sel", AV67TFContagemItemParecer_Comentario_Sel);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemitemparecer_data_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFContagem Item Parecer_Data"}), 1, "vTFCONTAGEMITEMPARECER_DATA");
               GX_FocusControl = edtavTfcontagemitemparecer_data_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV70TFContagemItemParecer_Data = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContagemItemParecer_Data", context.localUtil.TToC( AV70TFContagemItemParecer_Data, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV70TFContagemItemParecer_Data = context.localUtil.CToT( cgiGet( edtavTfcontagemitemparecer_data_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContagemItemParecer_Data", context.localUtil.TToC( AV70TFContagemItemParecer_Data, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemitemparecer_data_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFContagem Item Parecer_Data_To"}), 1, "vTFCONTAGEMITEMPARECER_DATA_TO");
               GX_FocusControl = edtavTfcontagemitemparecer_data_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV71TFContagemItemParecer_Data_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContagemItemParecer_Data_To", context.localUtil.TToC( AV71TFContagemItemParecer_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV71TFContagemItemParecer_Data_To = context.localUtil.CToT( cgiGet( edtavTfcontagemitemparecer_data_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContagemItemParecer_Data_To", context.localUtil.TToC( AV71TFContagemItemParecer_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemitemparecer_dataauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Item Parecer_Data Aux Date"}), 1, "vDDO_CONTAGEMITEMPARECER_DATAAUXDATE");
               GX_FocusControl = edtavDdo_contagemitemparecer_dataauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV72DDO_ContagemItemParecer_DataAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72DDO_ContagemItemParecer_DataAuxDate", context.localUtil.Format(AV72DDO_ContagemItemParecer_DataAuxDate, "99/99/99"));
            }
            else
            {
               AV72DDO_ContagemItemParecer_DataAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemitemparecer_dataauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72DDO_ContagemItemParecer_DataAuxDate", context.localUtil.Format(AV72DDO_ContagemItemParecer_DataAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemitemparecer_dataauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Item Parecer_Data Aux Date To"}), 1, "vDDO_CONTAGEMITEMPARECER_DATAAUXDATETO");
               GX_FocusControl = edtavDdo_contagemitemparecer_dataauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV73DDO_ContagemItemParecer_DataAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73DDO_ContagemItemParecer_DataAuxDateTo", context.localUtil.Format(AV73DDO_ContagemItemParecer_DataAuxDateTo, "99/99/99"));
            }
            else
            {
               AV73DDO_ContagemItemParecer_DataAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemitemparecer_dataauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73DDO_ContagemItemParecer_DataAuxDateTo", context.localUtil.Format(AV73DDO_ContagemItemParecer_DataAuxDateTo, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemitemparecer_usuariocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemitemparecer_usuariocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMITEMPARECER_USUARIOCOD");
               GX_FocusControl = edtavTfcontagemitemparecer_usuariocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV76TFContagemItemParecer_UsuarioCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContagemItemParecer_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76TFContagemItemParecer_UsuarioCod), 6, 0)));
            }
            else
            {
               AV76TFContagemItemParecer_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( edtavTfcontagemitemparecer_usuariocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContagemItemParecer_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76TFContagemItemParecer_UsuarioCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemitemparecer_usuariocod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemitemparecer_usuariocod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMITEMPARECER_USUARIOCOD_TO");
               GX_FocusControl = edtavTfcontagemitemparecer_usuariocod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV77TFContagemItemParecer_UsuarioCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFContagemItemParecer_UsuarioCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77TFContagemItemParecer_UsuarioCod_To), 6, 0)));
            }
            else
            {
               AV77TFContagemItemParecer_UsuarioCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontagemitemparecer_usuariocod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFContagemItemParecer_UsuarioCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77TFContagemItemParecer_UsuarioCod_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemitemparecer_usuariopessoacod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemitemparecer_usuariopessoacod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD");
               GX_FocusControl = edtavTfcontagemitemparecer_usuariopessoacod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV80TFContagemItemParecer_UsuarioPessoaCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContagemItemParecer_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV80TFContagemItemParecer_UsuarioPessoaCod), 6, 0)));
            }
            else
            {
               AV80TFContagemItemParecer_UsuarioPessoaCod = (int)(context.localUtil.CToN( cgiGet( edtavTfcontagemitemparecer_usuariopessoacod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContagemItemParecer_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV80TFContagemItemParecer_UsuarioPessoaCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemitemparecer_usuariopessoacod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemitemparecer_usuariopessoacod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD_TO");
               GX_FocusControl = edtavTfcontagemitemparecer_usuariopessoacod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV81TFContagemItemParecer_UsuarioPessoaCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContagemItemParecer_UsuarioPessoaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81TFContagemItemParecer_UsuarioPessoaCod_To), 6, 0)));
            }
            else
            {
               AV81TFContagemItemParecer_UsuarioPessoaCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontagemitemparecer_usuariopessoacod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContagemItemParecer_UsuarioPessoaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81TFContagemItemParecer_UsuarioPessoaCod_To), 6, 0)));
            }
            AV84TFContagemItemParecer_UsuarioPessoaNom = StringUtil.Upper( cgiGet( edtavTfcontagemitemparecer_usuariopessoanom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContagemItemParecer_UsuarioPessoaNom", AV84TFContagemItemParecer_UsuarioPessoaNom);
            AV85TFContagemItemParecer_UsuarioPessoaNom_Sel = StringUtil.Upper( cgiGet( edtavTfcontagemitemparecer_usuariopessoanom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFContagemItemParecer_UsuarioPessoaNom_Sel", AV85TFContagemItemParecer_UsuarioPessoaNom_Sel);
            AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_contagemitemparecer_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace", AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace);
            AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace = cgiGet( edtavDdo_contagemitem_lancamentotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace", AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace);
            AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace = cgiGet( edtavDdo_contagemitemparecer_comentariotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace", AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace);
            AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace = cgiGet( edtavDdo_contagemitemparecer_datatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace", AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace);
            AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace = cgiGet( edtavDdo_contagemitemparecer_usuariocodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace", AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace);
            AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace = cgiGet( edtavDdo_contagemitemparecer_usuariopessoacodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace", AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace);
            AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace = cgiGet( edtavDdo_contagemitemparecer_usuariopessoanomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace", AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_91 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_91"), ",", "."));
            AV89GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV90GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_managefilters_Icon = cgiGet( "DDO_MANAGEFILTERS_Icon");
            Ddo_managefilters_Caption = cgiGet( "DDO_MANAGEFILTERS_Caption");
            Ddo_managefilters_Tooltip = cgiGet( "DDO_MANAGEFILTERS_Tooltip");
            Ddo_managefilters_Cls = cgiGet( "DDO_MANAGEFILTERS_Cls");
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contagemitemparecer_codigo_Caption = cgiGet( "DDO_CONTAGEMITEMPARECER_CODIGO_Caption");
            Ddo_contagemitemparecer_codigo_Tooltip = cgiGet( "DDO_CONTAGEMITEMPARECER_CODIGO_Tooltip");
            Ddo_contagemitemparecer_codigo_Cls = cgiGet( "DDO_CONTAGEMITEMPARECER_CODIGO_Cls");
            Ddo_contagemitemparecer_codigo_Filteredtext_set = cgiGet( "DDO_CONTAGEMITEMPARECER_CODIGO_Filteredtext_set");
            Ddo_contagemitemparecer_codigo_Filteredtextto_set = cgiGet( "DDO_CONTAGEMITEMPARECER_CODIGO_Filteredtextto_set");
            Ddo_contagemitemparecer_codigo_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMITEMPARECER_CODIGO_Dropdownoptionstype");
            Ddo_contagemitemparecer_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMITEMPARECER_CODIGO_Titlecontrolidtoreplace");
            Ddo_contagemitemparecer_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_CODIGO_Includesortasc"));
            Ddo_contagemitemparecer_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_CODIGO_Includesortdsc"));
            Ddo_contagemitemparecer_codigo_Sortedstatus = cgiGet( "DDO_CONTAGEMITEMPARECER_CODIGO_Sortedstatus");
            Ddo_contagemitemparecer_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_CODIGO_Includefilter"));
            Ddo_contagemitemparecer_codigo_Filtertype = cgiGet( "DDO_CONTAGEMITEMPARECER_CODIGO_Filtertype");
            Ddo_contagemitemparecer_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_CODIGO_Filterisrange"));
            Ddo_contagemitemparecer_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_CODIGO_Includedatalist"));
            Ddo_contagemitemparecer_codigo_Sortasc = cgiGet( "DDO_CONTAGEMITEMPARECER_CODIGO_Sortasc");
            Ddo_contagemitemparecer_codigo_Sortdsc = cgiGet( "DDO_CONTAGEMITEMPARECER_CODIGO_Sortdsc");
            Ddo_contagemitemparecer_codigo_Cleanfilter = cgiGet( "DDO_CONTAGEMITEMPARECER_CODIGO_Cleanfilter");
            Ddo_contagemitemparecer_codigo_Rangefilterfrom = cgiGet( "DDO_CONTAGEMITEMPARECER_CODIGO_Rangefilterfrom");
            Ddo_contagemitemparecer_codigo_Rangefilterto = cgiGet( "DDO_CONTAGEMITEMPARECER_CODIGO_Rangefilterto");
            Ddo_contagemitemparecer_codigo_Searchbuttontext = cgiGet( "DDO_CONTAGEMITEMPARECER_CODIGO_Searchbuttontext");
            Ddo_contagemitem_lancamento_Caption = cgiGet( "DDO_CONTAGEMITEM_LANCAMENTO_Caption");
            Ddo_contagemitem_lancamento_Tooltip = cgiGet( "DDO_CONTAGEMITEM_LANCAMENTO_Tooltip");
            Ddo_contagemitem_lancamento_Cls = cgiGet( "DDO_CONTAGEMITEM_LANCAMENTO_Cls");
            Ddo_contagemitem_lancamento_Filteredtext_set = cgiGet( "DDO_CONTAGEMITEM_LANCAMENTO_Filteredtext_set");
            Ddo_contagemitem_lancamento_Filteredtextto_set = cgiGet( "DDO_CONTAGEMITEM_LANCAMENTO_Filteredtextto_set");
            Ddo_contagemitem_lancamento_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMITEM_LANCAMENTO_Dropdownoptionstype");
            Ddo_contagemitem_lancamento_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMITEM_LANCAMENTO_Titlecontrolidtoreplace");
            Ddo_contagemitem_lancamento_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEM_LANCAMENTO_Includesortasc"));
            Ddo_contagemitem_lancamento_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEM_LANCAMENTO_Includesortdsc"));
            Ddo_contagemitem_lancamento_Sortedstatus = cgiGet( "DDO_CONTAGEMITEM_LANCAMENTO_Sortedstatus");
            Ddo_contagemitem_lancamento_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEM_LANCAMENTO_Includefilter"));
            Ddo_contagemitem_lancamento_Filtertype = cgiGet( "DDO_CONTAGEMITEM_LANCAMENTO_Filtertype");
            Ddo_contagemitem_lancamento_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEM_LANCAMENTO_Filterisrange"));
            Ddo_contagemitem_lancamento_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEM_LANCAMENTO_Includedatalist"));
            Ddo_contagemitem_lancamento_Sortasc = cgiGet( "DDO_CONTAGEMITEM_LANCAMENTO_Sortasc");
            Ddo_contagemitem_lancamento_Sortdsc = cgiGet( "DDO_CONTAGEMITEM_LANCAMENTO_Sortdsc");
            Ddo_contagemitem_lancamento_Cleanfilter = cgiGet( "DDO_CONTAGEMITEM_LANCAMENTO_Cleanfilter");
            Ddo_contagemitem_lancamento_Rangefilterfrom = cgiGet( "DDO_CONTAGEMITEM_LANCAMENTO_Rangefilterfrom");
            Ddo_contagemitem_lancamento_Rangefilterto = cgiGet( "DDO_CONTAGEMITEM_LANCAMENTO_Rangefilterto");
            Ddo_contagemitem_lancamento_Searchbuttontext = cgiGet( "DDO_CONTAGEMITEM_LANCAMENTO_Searchbuttontext");
            Ddo_contagemitemparecer_comentario_Caption = cgiGet( "DDO_CONTAGEMITEMPARECER_COMENTARIO_Caption");
            Ddo_contagemitemparecer_comentario_Tooltip = cgiGet( "DDO_CONTAGEMITEMPARECER_COMENTARIO_Tooltip");
            Ddo_contagemitemparecer_comentario_Cls = cgiGet( "DDO_CONTAGEMITEMPARECER_COMENTARIO_Cls");
            Ddo_contagemitemparecer_comentario_Filteredtext_set = cgiGet( "DDO_CONTAGEMITEMPARECER_COMENTARIO_Filteredtext_set");
            Ddo_contagemitemparecer_comentario_Selectedvalue_set = cgiGet( "DDO_CONTAGEMITEMPARECER_COMENTARIO_Selectedvalue_set");
            Ddo_contagemitemparecer_comentario_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMITEMPARECER_COMENTARIO_Dropdownoptionstype");
            Ddo_contagemitemparecer_comentario_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMITEMPARECER_COMENTARIO_Titlecontrolidtoreplace");
            Ddo_contagemitemparecer_comentario_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_COMENTARIO_Includesortasc"));
            Ddo_contagemitemparecer_comentario_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_COMENTARIO_Includesortdsc"));
            Ddo_contagemitemparecer_comentario_Sortedstatus = cgiGet( "DDO_CONTAGEMITEMPARECER_COMENTARIO_Sortedstatus");
            Ddo_contagemitemparecer_comentario_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_COMENTARIO_Includefilter"));
            Ddo_contagemitemparecer_comentario_Filtertype = cgiGet( "DDO_CONTAGEMITEMPARECER_COMENTARIO_Filtertype");
            Ddo_contagemitemparecer_comentario_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_COMENTARIO_Filterisrange"));
            Ddo_contagemitemparecer_comentario_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_COMENTARIO_Includedatalist"));
            Ddo_contagemitemparecer_comentario_Datalisttype = cgiGet( "DDO_CONTAGEMITEMPARECER_COMENTARIO_Datalisttype");
            Ddo_contagemitemparecer_comentario_Datalistproc = cgiGet( "DDO_CONTAGEMITEMPARECER_COMENTARIO_Datalistproc");
            Ddo_contagemitemparecer_comentario_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTAGEMITEMPARECER_COMENTARIO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemitemparecer_comentario_Sortasc = cgiGet( "DDO_CONTAGEMITEMPARECER_COMENTARIO_Sortasc");
            Ddo_contagemitemparecer_comentario_Sortdsc = cgiGet( "DDO_CONTAGEMITEMPARECER_COMENTARIO_Sortdsc");
            Ddo_contagemitemparecer_comentario_Loadingdata = cgiGet( "DDO_CONTAGEMITEMPARECER_COMENTARIO_Loadingdata");
            Ddo_contagemitemparecer_comentario_Cleanfilter = cgiGet( "DDO_CONTAGEMITEMPARECER_COMENTARIO_Cleanfilter");
            Ddo_contagemitemparecer_comentario_Noresultsfound = cgiGet( "DDO_CONTAGEMITEMPARECER_COMENTARIO_Noresultsfound");
            Ddo_contagemitemparecer_comentario_Searchbuttontext = cgiGet( "DDO_CONTAGEMITEMPARECER_COMENTARIO_Searchbuttontext");
            Ddo_contagemitemparecer_data_Caption = cgiGet( "DDO_CONTAGEMITEMPARECER_DATA_Caption");
            Ddo_contagemitemparecer_data_Tooltip = cgiGet( "DDO_CONTAGEMITEMPARECER_DATA_Tooltip");
            Ddo_contagemitemparecer_data_Cls = cgiGet( "DDO_CONTAGEMITEMPARECER_DATA_Cls");
            Ddo_contagemitemparecer_data_Filteredtext_set = cgiGet( "DDO_CONTAGEMITEMPARECER_DATA_Filteredtext_set");
            Ddo_contagemitemparecer_data_Filteredtextto_set = cgiGet( "DDO_CONTAGEMITEMPARECER_DATA_Filteredtextto_set");
            Ddo_contagemitemparecer_data_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMITEMPARECER_DATA_Dropdownoptionstype");
            Ddo_contagemitemparecer_data_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMITEMPARECER_DATA_Titlecontrolidtoreplace");
            Ddo_contagemitemparecer_data_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_DATA_Includesortasc"));
            Ddo_contagemitemparecer_data_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_DATA_Includesortdsc"));
            Ddo_contagemitemparecer_data_Sortedstatus = cgiGet( "DDO_CONTAGEMITEMPARECER_DATA_Sortedstatus");
            Ddo_contagemitemparecer_data_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_DATA_Includefilter"));
            Ddo_contagemitemparecer_data_Filtertype = cgiGet( "DDO_CONTAGEMITEMPARECER_DATA_Filtertype");
            Ddo_contagemitemparecer_data_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_DATA_Filterisrange"));
            Ddo_contagemitemparecer_data_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_DATA_Includedatalist"));
            Ddo_contagemitemparecer_data_Sortasc = cgiGet( "DDO_CONTAGEMITEMPARECER_DATA_Sortasc");
            Ddo_contagemitemparecer_data_Sortdsc = cgiGet( "DDO_CONTAGEMITEMPARECER_DATA_Sortdsc");
            Ddo_contagemitemparecer_data_Cleanfilter = cgiGet( "DDO_CONTAGEMITEMPARECER_DATA_Cleanfilter");
            Ddo_contagemitemparecer_data_Rangefilterfrom = cgiGet( "DDO_CONTAGEMITEMPARECER_DATA_Rangefilterfrom");
            Ddo_contagemitemparecer_data_Rangefilterto = cgiGet( "DDO_CONTAGEMITEMPARECER_DATA_Rangefilterto");
            Ddo_contagemitemparecer_data_Searchbuttontext = cgiGet( "DDO_CONTAGEMITEMPARECER_DATA_Searchbuttontext");
            Ddo_contagemitemparecer_usuariocod_Caption = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Caption");
            Ddo_contagemitemparecer_usuariocod_Tooltip = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Tooltip");
            Ddo_contagemitemparecer_usuariocod_Cls = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Cls");
            Ddo_contagemitemparecer_usuariocod_Filteredtext_set = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Filteredtext_set");
            Ddo_contagemitemparecer_usuariocod_Filteredtextto_set = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Filteredtextto_set");
            Ddo_contagemitemparecer_usuariocod_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Dropdownoptionstype");
            Ddo_contagemitemparecer_usuariocod_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Titlecontrolidtoreplace");
            Ddo_contagemitemparecer_usuariocod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Includesortasc"));
            Ddo_contagemitemparecer_usuariocod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Includesortdsc"));
            Ddo_contagemitemparecer_usuariocod_Sortedstatus = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Sortedstatus");
            Ddo_contagemitemparecer_usuariocod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Includefilter"));
            Ddo_contagemitemparecer_usuariocod_Filtertype = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Filtertype");
            Ddo_contagemitemparecer_usuariocod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Filterisrange"));
            Ddo_contagemitemparecer_usuariocod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Includedatalist"));
            Ddo_contagemitemparecer_usuariocod_Sortasc = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Sortasc");
            Ddo_contagemitemparecer_usuariocod_Sortdsc = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Sortdsc");
            Ddo_contagemitemparecer_usuariocod_Cleanfilter = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Cleanfilter");
            Ddo_contagemitemparecer_usuariocod_Rangefilterfrom = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Rangefilterfrom");
            Ddo_contagemitemparecer_usuariocod_Rangefilterto = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Rangefilterto");
            Ddo_contagemitemparecer_usuariocod_Searchbuttontext = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Searchbuttontext");
            Ddo_contagemitemparecer_usuariopessoacod_Caption = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Caption");
            Ddo_contagemitemparecer_usuariopessoacod_Tooltip = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Tooltip");
            Ddo_contagemitemparecer_usuariopessoacod_Cls = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Cls");
            Ddo_contagemitemparecer_usuariopessoacod_Filteredtext_set = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Filteredtext_set");
            Ddo_contagemitemparecer_usuariopessoacod_Filteredtextto_set = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Filteredtextto_set");
            Ddo_contagemitemparecer_usuariopessoacod_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Dropdownoptionstype");
            Ddo_contagemitemparecer_usuariopessoacod_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Titlecontrolidtoreplace");
            Ddo_contagemitemparecer_usuariopessoacod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Includesortasc"));
            Ddo_contagemitemparecer_usuariopessoacod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Includesortdsc"));
            Ddo_contagemitemparecer_usuariopessoacod_Sortedstatus = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Sortedstatus");
            Ddo_contagemitemparecer_usuariopessoacod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Includefilter"));
            Ddo_contagemitemparecer_usuariopessoacod_Filtertype = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Filtertype");
            Ddo_contagemitemparecer_usuariopessoacod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Filterisrange"));
            Ddo_contagemitemparecer_usuariopessoacod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Includedatalist"));
            Ddo_contagemitemparecer_usuariopessoacod_Sortasc = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Sortasc");
            Ddo_contagemitemparecer_usuariopessoacod_Sortdsc = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Sortdsc");
            Ddo_contagemitemparecer_usuariopessoacod_Cleanfilter = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Cleanfilter");
            Ddo_contagemitemparecer_usuariopessoacod_Rangefilterfrom = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Rangefilterfrom");
            Ddo_contagemitemparecer_usuariopessoacod_Rangefilterto = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Rangefilterto");
            Ddo_contagemitemparecer_usuariopessoacod_Searchbuttontext = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Searchbuttontext");
            Ddo_contagemitemparecer_usuariopessoanom_Caption = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Caption");
            Ddo_contagemitemparecer_usuariopessoanom_Tooltip = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Tooltip");
            Ddo_contagemitemparecer_usuariopessoanom_Cls = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Cls");
            Ddo_contagemitemparecer_usuariopessoanom_Filteredtext_set = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Filteredtext_set");
            Ddo_contagemitemparecer_usuariopessoanom_Selectedvalue_set = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Selectedvalue_set");
            Ddo_contagemitemparecer_usuariopessoanom_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Dropdownoptionstype");
            Ddo_contagemitemparecer_usuariopessoanom_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Titlecontrolidtoreplace");
            Ddo_contagemitemparecer_usuariopessoanom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Includesortasc"));
            Ddo_contagemitemparecer_usuariopessoanom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Includesortdsc"));
            Ddo_contagemitemparecer_usuariopessoanom_Sortedstatus = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Sortedstatus");
            Ddo_contagemitemparecer_usuariopessoanom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Includefilter"));
            Ddo_contagemitemparecer_usuariopessoanom_Filtertype = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Filtertype");
            Ddo_contagemitemparecer_usuariopessoanom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Filterisrange"));
            Ddo_contagemitemparecer_usuariopessoanom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Includedatalist"));
            Ddo_contagemitemparecer_usuariopessoanom_Datalisttype = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Datalisttype");
            Ddo_contagemitemparecer_usuariopessoanom_Datalistproc = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Datalistproc");
            Ddo_contagemitemparecer_usuariopessoanom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemitemparecer_usuariopessoanom_Sortasc = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Sortasc");
            Ddo_contagemitemparecer_usuariopessoanom_Sortdsc = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Sortdsc");
            Ddo_contagemitemparecer_usuariopessoanom_Loadingdata = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Loadingdata");
            Ddo_contagemitemparecer_usuariopessoanom_Cleanfilter = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Cleanfilter");
            Ddo_contagemitemparecer_usuariopessoanom_Noresultsfound = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Noresultsfound");
            Ddo_contagemitemparecer_usuariopessoanom_Searchbuttontext = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contagemitemparecer_codigo_Activeeventkey = cgiGet( "DDO_CONTAGEMITEMPARECER_CODIGO_Activeeventkey");
            Ddo_contagemitemparecer_codigo_Filteredtext_get = cgiGet( "DDO_CONTAGEMITEMPARECER_CODIGO_Filteredtext_get");
            Ddo_contagemitemparecer_codigo_Filteredtextto_get = cgiGet( "DDO_CONTAGEMITEMPARECER_CODIGO_Filteredtextto_get");
            Ddo_contagemitem_lancamento_Activeeventkey = cgiGet( "DDO_CONTAGEMITEM_LANCAMENTO_Activeeventkey");
            Ddo_contagemitem_lancamento_Filteredtext_get = cgiGet( "DDO_CONTAGEMITEM_LANCAMENTO_Filteredtext_get");
            Ddo_contagemitem_lancamento_Filteredtextto_get = cgiGet( "DDO_CONTAGEMITEM_LANCAMENTO_Filteredtextto_get");
            Ddo_contagemitemparecer_comentario_Activeeventkey = cgiGet( "DDO_CONTAGEMITEMPARECER_COMENTARIO_Activeeventkey");
            Ddo_contagemitemparecer_comentario_Filteredtext_get = cgiGet( "DDO_CONTAGEMITEMPARECER_COMENTARIO_Filteredtext_get");
            Ddo_contagemitemparecer_comentario_Selectedvalue_get = cgiGet( "DDO_CONTAGEMITEMPARECER_COMENTARIO_Selectedvalue_get");
            Ddo_contagemitemparecer_data_Activeeventkey = cgiGet( "DDO_CONTAGEMITEMPARECER_DATA_Activeeventkey");
            Ddo_contagemitemparecer_data_Filteredtext_get = cgiGet( "DDO_CONTAGEMITEMPARECER_DATA_Filteredtext_get");
            Ddo_contagemitemparecer_data_Filteredtextto_get = cgiGet( "DDO_CONTAGEMITEMPARECER_DATA_Filteredtextto_get");
            Ddo_contagemitemparecer_usuariocod_Activeeventkey = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Activeeventkey");
            Ddo_contagemitemparecer_usuariocod_Filteredtext_get = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Filteredtext_get");
            Ddo_contagemitemparecer_usuariocod_Filteredtextto_get = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOCOD_Filteredtextto_get");
            Ddo_contagemitemparecer_usuariopessoacod_Activeeventkey = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Activeeventkey");
            Ddo_contagemitemparecer_usuariopessoacod_Filteredtext_get = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Filteredtext_get");
            Ddo_contagemitemparecer_usuariopessoacod_Filteredtextto_get = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD_Filteredtextto_get");
            Ddo_contagemitemparecer_usuariopessoanom_Activeeventkey = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Activeeventkey");
            Ddo_contagemitemparecer_usuariopessoanom_Filteredtext_get = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Filteredtext_get");
            Ddo_contagemitemparecer_usuariopessoanom_Selectedvalue_get = cgiGet( "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM_Selectedvalue_get");
            Ddo_managefilters_Activeeventkey = cgiGet( "DDO_MANAGEFILTERS_Activeeventkey");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMITEMPARECER_COMENTARIO1"), AV18ContagemItemParecer_Comentario1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMITEMPARECER_USUARIOPESSOANOM1"), AV19ContagemItemParecer_UsuarioPessoaNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMITEMPARECER_COMENTARIO2"), AV23ContagemItemParecer_Comentario2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMITEMPARECER_USUARIOPESSOANOM2"), AV24ContagemItemParecer_UsuarioPessoaNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV26DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV27DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMITEMPARECER_COMENTARIO3"), AV28ContagemItemParecer_Comentario3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMITEMPARECER_USUARIOPESSOANOM3"), AV29ContagemItemParecer_UsuarioPessoaNom3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV25DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMITEMPARECER_CODIGO"), ",", ".") != Convert.ToDecimal( AV58TFContagemItemParecer_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMITEMPARECER_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV59TFContagemItemParecer_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMITEM_LANCAMENTO"), ",", ".") != Convert.ToDecimal( AV62TFContagemItem_Lancamento )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMITEM_LANCAMENTO_TO"), ",", ".") != Convert.ToDecimal( AV63TFContagemItem_Lancamento_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMITEMPARECER_COMENTARIO"), AV66TFContagemItemParecer_Comentario) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMITEMPARECER_COMENTARIO_SEL"), AV67TFContagemItemParecer_Comentario_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMITEMPARECER_DATA"), 0) != AV70TFContagemItemParecer_Data )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMITEMPARECER_DATA_TO"), 0) != AV71TFContagemItemParecer_Data_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMITEMPARECER_USUARIOCOD"), ",", ".") != Convert.ToDecimal( AV76TFContagemItemParecer_UsuarioCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMITEMPARECER_USUARIOCOD_TO"), ",", ".") != Convert.ToDecimal( AV77TFContagemItemParecer_UsuarioCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD"), ",", ".") != Convert.ToDecimal( AV80TFContagemItemParecer_UsuarioPessoaCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD_TO"), ",", ".") != Convert.ToDecimal( AV81TFContagemItemParecer_UsuarioPessoaCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM"), AV84TFContagemItemParecer_UsuarioPessoaNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM_SEL"), AV85TFContagemItemParecer_UsuarioPessoaNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E305V2 */
         E305V2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E305V2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         if ( StringUtil.StrCmp(AV7HTTPRequest.Method, "GET") == 0 )
         {
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            edtavManagefiltersexecutionstep_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavManagefiltersexecutionstep_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavManagefiltersexecutionstep_Visible), 5, 0)));
         }
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV16DynamicFiltersSelector1 = "CONTAGEMITEMPARECER_COMENTARIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector2 = "CONTAGEMITEMPARECER_COMENTARIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersSelector3 = "CONTAGEMITEMPARECER_COMENTARIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfcontagemitemparecer_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemitemparecer_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemitemparecer_codigo_Visible), 5, 0)));
         edtavTfcontagemitemparecer_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemitemparecer_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemitemparecer_codigo_to_Visible), 5, 0)));
         edtavTfcontagemitem_lancamento_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemitem_lancamento_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemitem_lancamento_Visible), 5, 0)));
         edtavTfcontagemitem_lancamento_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemitem_lancamento_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemitem_lancamento_to_Visible), 5, 0)));
         edtavTfcontagemitemparecer_comentario_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemitemparecer_comentario_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemitemparecer_comentario_Visible), 5, 0)));
         edtavTfcontagemitemparecer_comentario_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemitemparecer_comentario_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemitemparecer_comentario_sel_Visible), 5, 0)));
         edtavTfcontagemitemparecer_data_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemitemparecer_data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemitemparecer_data_Visible), 5, 0)));
         edtavTfcontagemitemparecer_data_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemitemparecer_data_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemitemparecer_data_to_Visible), 5, 0)));
         edtavTfcontagemitemparecer_usuariocod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemitemparecer_usuariocod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemitemparecer_usuariocod_Visible), 5, 0)));
         edtavTfcontagemitemparecer_usuariocod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemitemparecer_usuariocod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemitemparecer_usuariocod_to_Visible), 5, 0)));
         edtavTfcontagemitemparecer_usuariopessoacod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemitemparecer_usuariopessoacod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemitemparecer_usuariopessoacod_Visible), 5, 0)));
         edtavTfcontagemitemparecer_usuariopessoacod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemitemparecer_usuariopessoacod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemitemparecer_usuariopessoacod_to_Visible), 5, 0)));
         edtavTfcontagemitemparecer_usuariopessoanom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemitemparecer_usuariopessoanom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemitemparecer_usuariopessoanom_Visible), 5, 0)));
         edtavTfcontagemitemparecer_usuariopessoanom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemitemparecer_usuariopessoanom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemitemparecer_usuariopessoanom_sel_Visible), 5, 0)));
         Ddo_contagemitemparecer_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemItemParecer_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_codigo_Internalname, "TitleControlIdToReplace", Ddo_contagemitemparecer_codigo_Titlecontrolidtoreplace);
         AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace = Ddo_contagemitemparecer_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace", AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace);
         edtavDdo_contagemitemparecer_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemitemparecer_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemitemparecer_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemitem_lancamento_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemItem_Lancamento";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_lancamento_Internalname, "TitleControlIdToReplace", Ddo_contagemitem_lancamento_Titlecontrolidtoreplace);
         AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace = Ddo_contagemitem_lancamento_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace", AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace);
         edtavDdo_contagemitem_lancamentotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemitem_lancamentotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemitem_lancamentotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemitemparecer_comentario_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemItemParecer_Comentario";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_comentario_Internalname, "TitleControlIdToReplace", Ddo_contagemitemparecer_comentario_Titlecontrolidtoreplace);
         AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace = Ddo_contagemitemparecer_comentario_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace", AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace);
         edtavDdo_contagemitemparecer_comentariotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemitemparecer_comentariotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemitemparecer_comentariotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemitemparecer_data_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemItemParecer_Data";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_data_Internalname, "TitleControlIdToReplace", Ddo_contagemitemparecer_data_Titlecontrolidtoreplace);
         AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace = Ddo_contagemitemparecer_data_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace", AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace);
         edtavDdo_contagemitemparecer_datatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemitemparecer_datatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemitemparecer_datatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemitemparecer_usuariocod_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemItemParecer_UsuarioCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_usuariocod_Internalname, "TitleControlIdToReplace", Ddo_contagemitemparecer_usuariocod_Titlecontrolidtoreplace);
         AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace = Ddo_contagemitemparecer_usuariocod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace", AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace);
         edtavDdo_contagemitemparecer_usuariocodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemitemparecer_usuariocodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemitemparecer_usuariocodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemitemparecer_usuariopessoacod_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemItemParecer_UsuarioPessoaCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_usuariopessoacod_Internalname, "TitleControlIdToReplace", Ddo_contagemitemparecer_usuariopessoacod_Titlecontrolidtoreplace);
         AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace = Ddo_contagemitemparecer_usuariopessoacod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace", AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace);
         edtavDdo_contagemitemparecer_usuariopessoacodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemitemparecer_usuariopessoacodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemitemparecer_usuariopessoacodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemitemparecer_usuariopessoanom_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemItemParecer_UsuarioPessoaNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_usuariopessoanom_Internalname, "TitleControlIdToReplace", Ddo_contagemitemparecer_usuariopessoanom_Titlecontrolidtoreplace);
         AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace = Ddo_contagemitemparecer_usuariopessoanom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace", AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace);
         edtavDdo_contagemitemparecer_usuariopessoanomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemitemparecer_usuariopessoanomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemitemparecer_usuariopessoanomtitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Contagem Item Parecer";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Sequencial Parecer", 0);
         cmbavOrderedby.addItem("2", "Item do Lan�amento", 0);
         cmbavOrderedby.addItem("3", "Coment�rios", 0);
         cmbavOrderedby.addItem("4", "do Parecer", 0);
         cmbavOrderedby.addItem("5", "do Usu�rio", 0);
         cmbavOrderedby.addItem("6", "Pessoa ", 0);
         cmbavOrderedby.addItem("7", "do Usu�rio", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         Ddo_managefilters_Icon = context.convertURL( (String)(context.GetImagePath( "5efb9e2b-46db-43f4-8f74-dd3f5818d30e", "", context.GetTheme( ))));
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "Icon", Ddo_managefilters_Icon);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV87DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV87DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E315V2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV57ContagemItemParecer_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV61ContagemItem_LancamentoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV65ContagemItemParecer_ComentarioTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV69ContagemItemParecer_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV75ContagemItemParecer_UsuarioCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV79ContagemItemParecer_UsuarioPessoaCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV83ContagemItemParecer_UsuarioPessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV92ManageFiltersExecutionStep == 1 )
         {
            AV92ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV92ManageFiltersExecutionStep), 1, 0));
         }
         else if ( AV92ManageFiltersExecutionStep == 2 )
         {
            AV92ManageFiltersExecutionStep = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV92ManageFiltersExecutionStep), 1, 0));
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMITEMPARECER_COMENTARIO") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "Cont�m", 0);
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMITEMPARECER_COMENTARIO") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
               cmbavDynamicfiltersoperator2.addItem("2", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("3", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
               cmbavDynamicfiltersoperator2.addItem("2", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("3", "Cont�m", 0);
            }
            if ( AV25DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMITEMPARECER_COMENTARIO") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("3", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("3", "Cont�m", 0);
               }
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContagemItemParecer_Codigo_Titleformat = 2;
         edtContagemItemParecer_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Sequencial Parecer", AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItemParecer_Codigo_Internalname, "Title", edtContagemItemParecer_Codigo_Title);
         edtContagemItem_Lancamento_Titleformat = 2;
         edtContagemItem_Lancamento_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Item do Lan�amento", AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_Lancamento_Internalname, "Title", edtContagemItem_Lancamento_Title);
         edtContagemItemParecer_Comentario_Titleformat = 2;
         edtContagemItemParecer_Comentario_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Coment�rios", AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItemParecer_Comentario_Internalname, "Title", edtContagemItemParecer_Comentario_Title);
         edtContagemItemParecer_Data_Titleformat = 2;
         edtContagemItemParecer_Data_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "do Parecer", AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItemParecer_Data_Internalname, "Title", edtContagemItemParecer_Data_Title);
         edtContagemItemParecer_UsuarioCod_Titleformat = 2;
         edtContagemItemParecer_UsuarioCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "do Usu�rio", AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItemParecer_UsuarioCod_Internalname, "Title", edtContagemItemParecer_UsuarioCod_Title);
         edtContagemItemParecer_UsuarioPessoaCod_Titleformat = 2;
         edtContagemItemParecer_UsuarioPessoaCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Pessoa ", AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItemParecer_UsuarioPessoaCod_Internalname, "Title", edtContagemItemParecer_UsuarioPessoaCod_Title);
         edtContagemItemParecer_UsuarioPessoaNom_Titleformat = 2;
         edtContagemItemParecer_UsuarioPessoaNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "do Usu�rio", AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItemParecer_UsuarioPessoaNom_Internalname, "Title", edtContagemItemParecer_UsuarioPessoaNom_Title);
         AV89GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV89GridCurrentPage), 10, 0)));
         AV90GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV90GridPageCount), 10, 0)));
         AV100WWContagemItemParecerDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV101WWContagemItemParecerDS_2_Dynamicfiltersoperator1 = AV17DynamicFiltersOperator1;
         AV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 = AV18ContagemItemParecer_Comentario1;
         AV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 = AV19ContagemItemParecer_UsuarioPessoaNom1;
         AV104WWContagemItemParecerDS_5_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV105WWContagemItemParecerDS_6_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV106WWContagemItemParecerDS_7_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 = AV23ContagemItemParecer_Comentario2;
         AV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 = AV24ContagemItemParecer_UsuarioPessoaNom2;
         AV109WWContagemItemParecerDS_10_Dynamicfiltersenabled3 = AV25DynamicFiltersEnabled3;
         AV110WWContagemItemParecerDS_11_Dynamicfiltersselector3 = AV26DynamicFiltersSelector3;
         AV111WWContagemItemParecerDS_12_Dynamicfiltersoperator3 = AV27DynamicFiltersOperator3;
         AV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 = AV28ContagemItemParecer_Comentario3;
         AV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 = AV29ContagemItemParecer_UsuarioPessoaNom3;
         AV114WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo = AV58TFContagemItemParecer_Codigo;
         AV115WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to = AV59TFContagemItemParecer_Codigo_To;
         AV116WWContagemItemParecerDS_17_Tfcontagemitem_lancamento = AV62TFContagemItem_Lancamento;
         AV117WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to = AV63TFContagemItem_Lancamento_To;
         AV118WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario = AV66TFContagemItemParecer_Comentario;
         AV119WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel = AV67TFContagemItemParecer_Comentario_Sel;
         AV120WWContagemItemParecerDS_21_Tfcontagemitemparecer_data = AV70TFContagemItemParecer_Data;
         AV121WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to = AV71TFContagemItemParecer_Data_To;
         AV122WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod = AV76TFContagemItemParecer_UsuarioCod;
         AV123WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to = AV77TFContagemItemParecer_UsuarioCod_To;
         AV124WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod = AV80TFContagemItemParecer_UsuarioPessoaCod;
         AV125WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to = AV81TFContagemItemParecer_UsuarioPessoaCod_To;
         AV126WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom = AV84TFContagemItemParecer_UsuarioPessoaNom;
         AV127WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel = AV85TFContagemItemParecer_UsuarioPessoaNom_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV57ContagemItemParecer_CodigoTitleFilterData", AV57ContagemItemParecer_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV61ContagemItem_LancamentoTitleFilterData", AV61ContagemItem_LancamentoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV65ContagemItemParecer_ComentarioTitleFilterData", AV65ContagemItemParecer_ComentarioTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV69ContagemItemParecer_DataTitleFilterData", AV69ContagemItemParecer_DataTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV75ContagemItemParecer_UsuarioCodTitleFilterData", AV75ContagemItemParecer_UsuarioCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV79ContagemItemParecer_UsuarioPessoaCodTitleFilterData", AV79ContagemItemParecer_UsuarioPessoaCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV83ContagemItemParecer_UsuarioPessoaNomTitleFilterData", AV83ContagemItemParecer_UsuarioPessoaNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV96ManageFiltersData", AV96ManageFiltersData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E125V2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV88PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV88PageToGo) ;
         }
      }

      protected void E135V2( )
      {
         /* Ddo_contagemitemparecer_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemitemparecer_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemitemparecer_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_codigo_Internalname, "SortedStatus", Ddo_contagemitemparecer_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemitemparecer_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemitemparecer_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_codigo_Internalname, "SortedStatus", Ddo_contagemitemparecer_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemitemparecer_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV58TFContagemItemParecer_Codigo = (int)(NumberUtil.Val( Ddo_contagemitemparecer_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFContagemItemParecer_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFContagemItemParecer_Codigo), 6, 0)));
            AV59TFContagemItemParecer_Codigo_To = (int)(NumberUtil.Val( Ddo_contagemitemparecer_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContagemItemParecer_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFContagemItemParecer_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E145V2( )
      {
         /* Ddo_contagemitem_lancamento_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemitem_lancamento_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemitem_lancamento_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_lancamento_Internalname, "SortedStatus", Ddo_contagemitem_lancamento_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemitem_lancamento_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemitem_lancamento_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_lancamento_Internalname, "SortedStatus", Ddo_contagemitem_lancamento_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemitem_lancamento_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV62TFContagemItem_Lancamento = (int)(NumberUtil.Val( Ddo_contagemitem_lancamento_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFContagemItem_Lancamento), 6, 0)));
            AV63TFContagemItem_Lancamento_To = (int)(NumberUtil.Val( Ddo_contagemitem_lancamento_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContagemItem_Lancamento_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63TFContagemItem_Lancamento_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E155V2( )
      {
         /* Ddo_contagemitemparecer_comentario_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemitemparecer_comentario_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemitemparecer_comentario_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_comentario_Internalname, "SortedStatus", Ddo_contagemitemparecer_comentario_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemitemparecer_comentario_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemitemparecer_comentario_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_comentario_Internalname, "SortedStatus", Ddo_contagemitemparecer_comentario_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemitemparecer_comentario_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV66TFContagemItemParecer_Comentario = Ddo_contagemitemparecer_comentario_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContagemItemParecer_Comentario", AV66TFContagemItemParecer_Comentario);
            AV67TFContagemItemParecer_Comentario_Sel = Ddo_contagemitemparecer_comentario_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContagemItemParecer_Comentario_Sel", AV67TFContagemItemParecer_Comentario_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E165V2( )
      {
         /* Ddo_contagemitemparecer_data_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemitemparecer_data_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemitemparecer_data_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_data_Internalname, "SortedStatus", Ddo_contagemitemparecer_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemitemparecer_data_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemitemparecer_data_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_data_Internalname, "SortedStatus", Ddo_contagemitemparecer_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemitemparecer_data_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV70TFContagemItemParecer_Data = context.localUtil.CToT( Ddo_contagemitemparecer_data_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContagemItemParecer_Data", context.localUtil.TToC( AV70TFContagemItemParecer_Data, 8, 5, 0, 3, "/", ":", " "));
            AV71TFContagemItemParecer_Data_To = context.localUtil.CToT( Ddo_contagemitemparecer_data_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContagemItemParecer_Data_To", context.localUtil.TToC( AV71TFContagemItemParecer_Data_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV71TFContagemItemParecer_Data_To) )
            {
               AV71TFContagemItemParecer_Data_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV71TFContagemItemParecer_Data_To)), (short)(DateTimeUtil.Month( AV71TFContagemItemParecer_Data_To)), (short)(DateTimeUtil.Day( AV71TFContagemItemParecer_Data_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContagemItemParecer_Data_To", context.localUtil.TToC( AV71TFContagemItemParecer_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E175V2( )
      {
         /* Ddo_contagemitemparecer_usuariocod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemitemparecer_usuariocod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemitemparecer_usuariocod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_usuariocod_Internalname, "SortedStatus", Ddo_contagemitemparecer_usuariocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemitemparecer_usuariocod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemitemparecer_usuariocod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_usuariocod_Internalname, "SortedStatus", Ddo_contagemitemparecer_usuariocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemitemparecer_usuariocod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV76TFContagemItemParecer_UsuarioCod = (int)(NumberUtil.Val( Ddo_contagemitemparecer_usuariocod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContagemItemParecer_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76TFContagemItemParecer_UsuarioCod), 6, 0)));
            AV77TFContagemItemParecer_UsuarioCod_To = (int)(NumberUtil.Val( Ddo_contagemitemparecer_usuariocod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFContagemItemParecer_UsuarioCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77TFContagemItemParecer_UsuarioCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E185V2( )
      {
         /* Ddo_contagemitemparecer_usuariopessoacod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemitemparecer_usuariopessoacod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemitemparecer_usuariopessoacod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_usuariopessoacod_Internalname, "SortedStatus", Ddo_contagemitemparecer_usuariopessoacod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemitemparecer_usuariopessoacod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemitemparecer_usuariopessoacod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_usuariopessoacod_Internalname, "SortedStatus", Ddo_contagemitemparecer_usuariopessoacod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemitemparecer_usuariopessoacod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV80TFContagemItemParecer_UsuarioPessoaCod = (int)(NumberUtil.Val( Ddo_contagemitemparecer_usuariopessoacod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContagemItemParecer_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV80TFContagemItemParecer_UsuarioPessoaCod), 6, 0)));
            AV81TFContagemItemParecer_UsuarioPessoaCod_To = (int)(NumberUtil.Val( Ddo_contagemitemparecer_usuariopessoacod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContagemItemParecer_UsuarioPessoaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81TFContagemItemParecer_UsuarioPessoaCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E195V2( )
      {
         /* Ddo_contagemitemparecer_usuariopessoanom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemitemparecer_usuariopessoanom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemitemparecer_usuariopessoanom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_usuariopessoanom_Internalname, "SortedStatus", Ddo_contagemitemparecer_usuariopessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemitemparecer_usuariopessoanom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemitemparecer_usuariopessoanom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_usuariopessoanom_Internalname, "SortedStatus", Ddo_contagemitemparecer_usuariopessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemitemparecer_usuariopessoanom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV84TFContagemItemParecer_UsuarioPessoaNom = Ddo_contagemitemparecer_usuariopessoanom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContagemItemParecer_UsuarioPessoaNom", AV84TFContagemItemParecer_UsuarioPessoaNom);
            AV85TFContagemItemParecer_UsuarioPessoaNom_Sel = Ddo_contagemitemparecer_usuariopessoanom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFContagemItemParecer_UsuarioPessoaNom_Sel", AV85TFContagemItemParecer_UsuarioPessoaNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E325V2( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( AV6WWPContext.gxTpr_Update )
         {
            edtavUpdate_Link = formatLink("contagemitemparecer.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A243ContagemItemParecer_Codigo) + "," + UrlEncode("" +A224ContagemItem_Lancamento);
            AV52Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV52Update);
            AV128Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV52Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV52Update);
            AV128Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( AV6WWPContext.gxTpr_Delete )
         {
            edtavDelete_Link = formatLink("contagemitemparecer.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A243ContagemItemParecer_Codigo) + "," + UrlEncode("" +A224ContagemItem_Lancamento);
            AV53Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV53Delete);
            AV129Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV53Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV53Delete);
            AV129Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtavDisplay_Tooltiptext = "Mostrar";
         if ( AV6WWPContext.gxTpr_Display )
         {
            edtavDisplay_Link = formatLink("viewcontagemitemparecer.aspx") + "?" + UrlEncode("" +A243ContagemItemParecer_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
            AV91Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV91Display);
            AV130Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
            edtavDisplay_Enabled = 1;
         }
         else
         {
            edtavDisplay_Link = "";
            AV91Display = context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV91Display);
            AV130Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( )));
            edtavDisplay_Enabled = 0;
         }
         edtContagemItemParecer_Comentario_Link = formatLink("viewcontagemitemparecer.aspx") + "?" + UrlEncode("" +A243ContagemItemParecer_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtContagemItemParecer_UsuarioPessoaNom_Link = formatLink("viewusuario.aspx") + "?" + UrlEncode("" +A246ContagemItemParecer_UsuarioCod) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 91;
         }
         sendrow_912( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_91_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(91, GridRow);
         }
      }

      protected void E205V2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E255V2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV20DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemItemParecer_Comentario1, AV19ContagemItemParecer_UsuarioPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemItemParecer_Comentario2, AV24ContagemItemParecer_UsuarioPessoaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28ContagemItemParecer_Comentario3, AV29ContagemItemParecer_UsuarioPessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV58TFContagemItemParecer_Codigo, AV59TFContagemItemParecer_Codigo_To, AV62TFContagemItem_Lancamento, AV63TFContagemItem_Lancamento_To, AV66TFContagemItemParecer_Comentario, AV67TFContagemItemParecer_Comentario_Sel, AV70TFContagemItemParecer_Data, AV71TFContagemItemParecer_Data_To, AV76TFContagemItemParecer_UsuarioCod, AV77TFContagemItemParecer_UsuarioCod_To, AV80TFContagemItemParecer_UsuarioPessoaCod, AV81TFContagemItemParecer_UsuarioPessoaCod_To, AV84TFContagemItemParecer_UsuarioPessoaNom, AV85TFContagemItemParecer_UsuarioPessoaNom_Sel, AV92ManageFiltersExecutionStep, AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace, AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace, AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace, AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace, AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace, AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace, AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace, AV6WWPContext, AV131Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A243ContagemItemParecer_Codigo, A224ContagemItem_Lancamento, A246ContagemItemParecer_UsuarioCod) ;
      }

      protected void E215V2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV31DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV31DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemItemParecer_Comentario1, AV19ContagemItemParecer_UsuarioPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemItemParecer_Comentario2, AV24ContagemItemParecer_UsuarioPessoaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28ContagemItemParecer_Comentario3, AV29ContagemItemParecer_UsuarioPessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV58TFContagemItemParecer_Codigo, AV59TFContagemItemParecer_Codigo_To, AV62TFContagemItem_Lancamento, AV63TFContagemItem_Lancamento_To, AV66TFContagemItemParecer_Comentario, AV67TFContagemItemParecer_Comentario_Sel, AV70TFContagemItemParecer_Data, AV71TFContagemItemParecer_Data_To, AV76TFContagemItemParecer_UsuarioCod, AV77TFContagemItemParecer_UsuarioCod_To, AV80TFContagemItemParecer_UsuarioPessoaCod, AV81TFContagemItemParecer_UsuarioPessoaCod_To, AV84TFContagemItemParecer_UsuarioPessoaNom, AV85TFContagemItemParecer_UsuarioPessoaNom_Sel, AV92ManageFiltersExecutionStep, AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace, AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace, AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace, AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace, AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace, AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace, AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace, AV6WWPContext, AV131Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A243ContagemItemParecer_Codigo, A224ContagemItem_Lancamento, A246ContagemItemParecer_UsuarioCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E265V2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV17DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E275V2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV25DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemItemParecer_Comentario1, AV19ContagemItemParecer_UsuarioPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemItemParecer_Comentario2, AV24ContagemItemParecer_UsuarioPessoaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28ContagemItemParecer_Comentario3, AV29ContagemItemParecer_UsuarioPessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV58TFContagemItemParecer_Codigo, AV59TFContagemItemParecer_Codigo_To, AV62TFContagemItem_Lancamento, AV63TFContagemItem_Lancamento_To, AV66TFContagemItemParecer_Comentario, AV67TFContagemItemParecer_Comentario_Sel, AV70TFContagemItemParecer_Data, AV71TFContagemItemParecer_Data_To, AV76TFContagemItemParecer_UsuarioCod, AV77TFContagemItemParecer_UsuarioCod_To, AV80TFContagemItemParecer_UsuarioPessoaCod, AV81TFContagemItemParecer_UsuarioPessoaCod_To, AV84TFContagemItemParecer_UsuarioPessoaNom, AV85TFContagemItemParecer_UsuarioPessoaNom_Sel, AV92ManageFiltersExecutionStep, AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace, AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace, AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace, AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace, AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace, AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace, AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace, AV6WWPContext, AV131Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A243ContagemItemParecer_Codigo, A224ContagemItem_Lancamento, A246ContagemItemParecer_UsuarioCod) ;
      }

      protected void E225V2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemItemParecer_Comentario1, AV19ContagemItemParecer_UsuarioPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemItemParecer_Comentario2, AV24ContagemItemParecer_UsuarioPessoaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28ContagemItemParecer_Comentario3, AV29ContagemItemParecer_UsuarioPessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV58TFContagemItemParecer_Codigo, AV59TFContagemItemParecer_Codigo_To, AV62TFContagemItem_Lancamento, AV63TFContagemItem_Lancamento_To, AV66TFContagemItemParecer_Comentario, AV67TFContagemItemParecer_Comentario_Sel, AV70TFContagemItemParecer_Data, AV71TFContagemItemParecer_Data_To, AV76TFContagemItemParecer_UsuarioCod, AV77TFContagemItemParecer_UsuarioCod_To, AV80TFContagemItemParecer_UsuarioPessoaCod, AV81TFContagemItemParecer_UsuarioPessoaCod_To, AV84TFContagemItemParecer_UsuarioPessoaNom, AV85TFContagemItemParecer_UsuarioPessoaNom_Sel, AV92ManageFiltersExecutionStep, AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace, AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace, AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace, AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace, AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace, AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace, AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace, AV6WWPContext, AV131Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A243ContagemItemParecer_Codigo, A224ContagemItem_Lancamento, A246ContagemItemParecer_UsuarioCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E285V2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E235V2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV25DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemItemParecer_Comentario1, AV19ContagemItemParecer_UsuarioPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemItemParecer_Comentario2, AV24ContagemItemParecer_UsuarioPessoaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28ContagemItemParecer_Comentario3, AV29ContagemItemParecer_UsuarioPessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV58TFContagemItemParecer_Codigo, AV59TFContagemItemParecer_Codigo_To, AV62TFContagemItem_Lancamento, AV63TFContagemItem_Lancamento_To, AV66TFContagemItemParecer_Comentario, AV67TFContagemItemParecer_Comentario_Sel, AV70TFContagemItemParecer_Data, AV71TFContagemItemParecer_Data_To, AV76TFContagemItemParecer_UsuarioCod, AV77TFContagemItemParecer_UsuarioCod_To, AV80TFContagemItemParecer_UsuarioPessoaCod, AV81TFContagemItemParecer_UsuarioPessoaCod_To, AV84TFContagemItemParecer_UsuarioPessoaNom, AV85TFContagemItemParecer_UsuarioPessoaNom_Sel, AV92ManageFiltersExecutionStep, AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace, AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace, AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace, AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace, AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace, AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace, AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace, AV6WWPContext, AV131Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A243ContagemItemParecer_Codigo, A224ContagemItem_Lancamento, A246ContagemItemParecer_UsuarioCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E295V2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV27DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E115V2( )
      {
         /* Ddo_managefilters_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Clean#>") == 0 )
         {
            /* Execute user subroutine: 'CLEANFILTERS' */
            S242 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Save#>") == 0 )
         {
            /* Execute user subroutine: 'SAVEGRIDSTATE' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            context.PopUp(formatLink("wwpbaseobjects.savefilteras.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWContagemItemParecerFilters")) + "," + UrlEncode(StringUtil.RTrim(AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3"))), new Object[] {});
            AV92ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV92ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Manage#>") == 0 )
         {
            context.PopUp(formatLink("wwpbaseobjects.managefilters.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWContagemItemParecerFilters")), new Object[] {});
            AV92ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV92ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else
         {
            GXt_char2 = AV93ManageFiltersXml;
            new wwpbaseobjects.getfilterbyname(context ).execute(  "WWContagemItemParecerFilters",  Ddo_managefilters_Activeeventkey, out  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "ActiveEventKey", Ddo_managefilters_Activeeventkey);
            AV93ManageFiltersXml = GXt_char2;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV93ManageFiltersXml)) )
            {
               GX_msglist.addItem("O filtro selecionado n�o existe mais.");
            }
            else
            {
               /* Execute user subroutine: 'CLEANFILTERS' */
               S242 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               new wwpbaseobjects.savegridstate(context ).execute(  AV131Pgmname+"GridState",  AV93ManageFiltersXml) ;
               AV10GridState.FromXml(AV93ManageFiltersXml, "");
               AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
               S172 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
               S252 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
               S232 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               subgrid_firstpage( ) ;
               context.DoAjaxRefresh();
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E245V2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("contagemitemparecer.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S202( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contagemitemparecer_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_codigo_Internalname, "SortedStatus", Ddo_contagemitemparecer_codigo_Sortedstatus);
         Ddo_contagemitem_lancamento_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_lancamento_Internalname, "SortedStatus", Ddo_contagemitem_lancamento_Sortedstatus);
         Ddo_contagemitemparecer_comentario_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_comentario_Internalname, "SortedStatus", Ddo_contagemitemparecer_comentario_Sortedstatus);
         Ddo_contagemitemparecer_data_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_data_Internalname, "SortedStatus", Ddo_contagemitemparecer_data_Sortedstatus);
         Ddo_contagemitemparecer_usuariocod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_usuariocod_Internalname, "SortedStatus", Ddo_contagemitemparecer_usuariocod_Sortedstatus);
         Ddo_contagemitemparecer_usuariopessoacod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_usuariopessoacod_Internalname, "SortedStatus", Ddo_contagemitemparecer_usuariopessoacod_Sortedstatus);
         Ddo_contagemitemparecer_usuariopessoanom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_usuariopessoanom_Internalname, "SortedStatus", Ddo_contagemitemparecer_usuariopessoanom_Sortedstatus);
      }

      protected void S172( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_contagemitemparecer_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_codigo_Internalname, "SortedStatus", Ddo_contagemitemparecer_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_contagemitem_lancamento_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_lancamento_Internalname, "SortedStatus", Ddo_contagemitem_lancamento_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contagemitemparecer_comentario_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_comentario_Internalname, "SortedStatus", Ddo_contagemitemparecer_comentario_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_contagemitemparecer_data_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_data_Internalname, "SortedStatus", Ddo_contagemitemparecer_data_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_contagemitemparecer_usuariocod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_usuariocod_Internalname, "SortedStatus", Ddo_contagemitemparecer_usuariocod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_contagemitemparecer_usuariopessoacod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_usuariopessoacod_Internalname, "SortedStatus", Ddo_contagemitemparecer_usuariopessoacod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_contagemitemparecer_usuariopessoanom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_usuariopessoanom_Internalname, "SortedStatus", Ddo_contagemitemparecer_usuariopessoanom_Sortedstatus);
         }
      }

      protected void S182( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         if ( ! ( AV6WWPContext.gxTpr_Insert ) )
         {
            imgInsert_Link = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Link", imgInsert_Link);
            imgInsert_Bitmap = context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgInsert_Bitmap)));
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavContagemitemparecer_comentario1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitemparecer_comentario1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitemparecer_comentario1_Visible), 5, 0)));
         edtavContagemitemparecer_usuariopessoanom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitemparecer_usuariopessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitemparecer_usuariopessoanom1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMITEMPARECER_COMENTARIO") == 0 )
         {
            edtavContagemitemparecer_comentario1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitemparecer_comentario1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitemparecer_comentario1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 )
         {
            edtavContagemitemparecer_usuariopessoanom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitemparecer_usuariopessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitemparecer_usuariopessoanom1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavContagemitemparecer_comentario2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitemparecer_comentario2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitemparecer_comentario2_Visible), 5, 0)));
         edtavContagemitemparecer_usuariopessoanom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitemparecer_usuariopessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitemparecer_usuariopessoanom2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMITEMPARECER_COMENTARIO") == 0 )
         {
            edtavContagemitemparecer_comentario2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitemparecer_comentario2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitemparecer_comentario2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 )
         {
            edtavContagemitemparecer_usuariopessoanom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitemparecer_usuariopessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitemparecer_usuariopessoanom2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S142( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavContagemitemparecer_comentario3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitemparecer_comentario3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitemparecer_comentario3_Visible), 5, 0)));
         edtavContagemitemparecer_usuariopessoanom3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitemparecer_usuariopessoanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitemparecer_usuariopessoanom3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMITEMPARECER_COMENTARIO") == 0 )
         {
            edtavContagemitemparecer_comentario3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitemparecer_comentario3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitemparecer_comentario3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 )
         {
            edtavContagemitemparecer_usuariopessoanom3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitemparecer_usuariopessoanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitemparecer_usuariopessoanom3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S222( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         AV21DynamicFiltersSelector2 = "CONTAGEMITEMPARECER_COMENTARIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         AV23ContagemItemParecer_Comentario2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemItemParecer_Comentario2", AV23ContagemItemParecer_Comentario2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
         AV26DynamicFiltersSelector3 = "CONTAGEMITEMPARECER_COMENTARIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
         AV27DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
         AV28ContagemItemParecer_Comentario3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ContagemItemParecer_Comentario3", AV28ContagemItemParecer_Comentario3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S112( )
      {
         /* 'LOADSAVEDFILTERS' Routine */
         AV96ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV97ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV97ManageFiltersDataItem.gxTpr_Title = "Limpar filtros";
         AV97ManageFiltersDataItem.gxTpr_Eventkey = "<#Clean#>";
         AV97ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV97ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "63d2ae92-4e43-4a70-af61-0943e39ea422", "", context.GetTheme( ))));
         AV97ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
         AV96ManageFiltersData.Add(AV97ManageFiltersDataItem, 0);
         AV97ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV97ManageFiltersDataItem.gxTpr_Title = "Salvar filtro como...";
         AV97ManageFiltersDataItem.gxTpr_Eventkey = "<#Save#>";
         AV97ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV97ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "6eee63e8-73c7-4738-beee-f98e3a8d2841", "", context.GetTheme( ))));
         AV96ManageFiltersData.Add(AV97ManageFiltersDataItem, 0);
         AV97ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV97ManageFiltersDataItem.gxTpr_Isdivider = true;
         AV96ManageFiltersData.Add(AV97ManageFiltersDataItem, 0);
         AV94ManageFiltersItems.FromXml(new wwpbaseobjects.loadmanagefiltersstate(context).executeUdp(  "WWContagemItemParecerFilters"), "");
         AV132GXV1 = 1;
         while ( AV132GXV1 <= AV94ManageFiltersItems.Count )
         {
            AV95ManageFiltersItem = ((wwpbaseobjects.SdtGridStateCollection_Item)AV94ManageFiltersItems.Item(AV132GXV1));
            AV97ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV97ManageFiltersDataItem.gxTpr_Title = AV95ManageFiltersItem.gxTpr_Title;
            AV97ManageFiltersDataItem.gxTpr_Eventkey = AV95ManageFiltersItem.gxTpr_Title;
            AV97ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV97ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
            AV96ManageFiltersData.Add(AV97ManageFiltersDataItem, 0);
            if ( AV96ManageFiltersData.Count == 13 )
            {
               if (true) break;
            }
            AV132GXV1 = (int)(AV132GXV1+1);
         }
         if ( AV96ManageFiltersData.Count > 3 )
         {
            AV97ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV97ManageFiltersDataItem.gxTpr_Isdivider = true;
            AV96ManageFiltersData.Add(AV97ManageFiltersDataItem, 0);
            AV97ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV97ManageFiltersDataItem.gxTpr_Title = "Gerenciar filtros";
            AV97ManageFiltersDataItem.gxTpr_Eventkey = "<#Manage#>";
            AV97ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV97ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "653f6166-5d82-407a-af84-19e0dde65efd", "", context.GetTheme( ))));
            AV97ManageFiltersDataItem.gxTpr_Jsonclickevent = "";
            AV96ManageFiltersData.Add(AV97ManageFiltersDataItem, 0);
         }
      }

      protected void S242( )
      {
         /* 'CLEANFILTERS' Routine */
         AV58TFContagemItemParecer_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFContagemItemParecer_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFContagemItemParecer_Codigo), 6, 0)));
         Ddo_contagemitemparecer_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_codigo_Internalname, "FilteredText_set", Ddo_contagemitemparecer_codigo_Filteredtext_set);
         AV59TFContagemItemParecer_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContagemItemParecer_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFContagemItemParecer_Codigo_To), 6, 0)));
         Ddo_contagemitemparecer_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_codigo_Internalname, "FilteredTextTo_set", Ddo_contagemitemparecer_codigo_Filteredtextto_set);
         AV62TFContagemItem_Lancamento = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFContagemItem_Lancamento), 6, 0)));
         Ddo_contagemitem_lancamento_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_lancamento_Internalname, "FilteredText_set", Ddo_contagemitem_lancamento_Filteredtext_set);
         AV63TFContagemItem_Lancamento_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContagemItem_Lancamento_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63TFContagemItem_Lancamento_To), 6, 0)));
         Ddo_contagemitem_lancamento_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_lancamento_Internalname, "FilteredTextTo_set", Ddo_contagemitem_lancamento_Filteredtextto_set);
         AV66TFContagemItemParecer_Comentario = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContagemItemParecer_Comentario", AV66TFContagemItemParecer_Comentario);
         Ddo_contagemitemparecer_comentario_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_comentario_Internalname, "FilteredText_set", Ddo_contagemitemparecer_comentario_Filteredtext_set);
         AV67TFContagemItemParecer_Comentario_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContagemItemParecer_Comentario_Sel", AV67TFContagemItemParecer_Comentario_Sel);
         Ddo_contagemitemparecer_comentario_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_comentario_Internalname, "SelectedValue_set", Ddo_contagemitemparecer_comentario_Selectedvalue_set);
         AV70TFContagemItemParecer_Data = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContagemItemParecer_Data", context.localUtil.TToC( AV70TFContagemItemParecer_Data, 8, 5, 0, 3, "/", ":", " "));
         Ddo_contagemitemparecer_data_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_data_Internalname, "FilteredText_set", Ddo_contagemitemparecer_data_Filteredtext_set);
         AV71TFContagemItemParecer_Data_To = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContagemItemParecer_Data_To", context.localUtil.TToC( AV71TFContagemItemParecer_Data_To, 8, 5, 0, 3, "/", ":", " "));
         Ddo_contagemitemparecer_data_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_data_Internalname, "FilteredTextTo_set", Ddo_contagemitemparecer_data_Filteredtextto_set);
         AV76TFContagemItemParecer_UsuarioCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContagemItemParecer_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76TFContagemItemParecer_UsuarioCod), 6, 0)));
         Ddo_contagemitemparecer_usuariocod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_usuariocod_Internalname, "FilteredText_set", Ddo_contagemitemparecer_usuariocod_Filteredtext_set);
         AV77TFContagemItemParecer_UsuarioCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFContagemItemParecer_UsuarioCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77TFContagemItemParecer_UsuarioCod_To), 6, 0)));
         Ddo_contagemitemparecer_usuariocod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_usuariocod_Internalname, "FilteredTextTo_set", Ddo_contagemitemparecer_usuariocod_Filteredtextto_set);
         AV80TFContagemItemParecer_UsuarioPessoaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContagemItemParecer_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV80TFContagemItemParecer_UsuarioPessoaCod), 6, 0)));
         Ddo_contagemitemparecer_usuariopessoacod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_usuariopessoacod_Internalname, "FilteredText_set", Ddo_contagemitemparecer_usuariopessoacod_Filteredtext_set);
         AV81TFContagemItemParecer_UsuarioPessoaCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContagemItemParecer_UsuarioPessoaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81TFContagemItemParecer_UsuarioPessoaCod_To), 6, 0)));
         Ddo_contagemitemparecer_usuariopessoacod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_usuariopessoacod_Internalname, "FilteredTextTo_set", Ddo_contagemitemparecer_usuariopessoacod_Filteredtextto_set);
         AV84TFContagemItemParecer_UsuarioPessoaNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContagemItemParecer_UsuarioPessoaNom", AV84TFContagemItemParecer_UsuarioPessoaNom);
         Ddo_contagemitemparecer_usuariopessoanom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_usuariopessoanom_Internalname, "FilteredText_set", Ddo_contagemitemparecer_usuariopessoanom_Filteredtext_set);
         AV85TFContagemItemParecer_UsuarioPessoaNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFContagemItemParecer_UsuarioPessoaNom_Sel", AV85TFContagemItemParecer_UsuarioPessoaNom_Sel);
         Ddo_contagemitemparecer_usuariopessoanom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_usuariopessoanom_Internalname, "SelectedValue_set", Ddo_contagemitemparecer_usuariopessoanom_Selectedvalue_set);
         AV16DynamicFiltersSelector1 = "CONTAGEMITEMPARECER_COMENTARIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV17DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         AV18ContagemItemParecer_Comentario1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemItemParecer_Comentario1", AV18ContagemItemParecer_Comentario1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S162( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV51Session.Get(AV131Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV131Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV51Session.Get(AV131Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S252 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S252( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV133GXV2 = 1;
         while ( AV133GXV2 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV133GXV2));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMITEMPARECER_CODIGO") == 0 )
            {
               AV58TFContagemItemParecer_Codigo = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFContagemItemParecer_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFContagemItemParecer_Codigo), 6, 0)));
               AV59TFContagemItemParecer_Codigo_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContagemItemParecer_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFContagemItemParecer_Codigo_To), 6, 0)));
               if ( ! (0==AV58TFContagemItemParecer_Codigo) )
               {
                  Ddo_contagemitemparecer_codigo_Filteredtext_set = StringUtil.Str( (decimal)(AV58TFContagemItemParecer_Codigo), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_codigo_Internalname, "FilteredText_set", Ddo_contagemitemparecer_codigo_Filteredtext_set);
               }
               if ( ! (0==AV59TFContagemItemParecer_Codigo_To) )
               {
                  Ddo_contagemitemparecer_codigo_Filteredtextto_set = StringUtil.Str( (decimal)(AV59TFContagemItemParecer_Codigo_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_codigo_Internalname, "FilteredTextTo_set", Ddo_contagemitemparecer_codigo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMITEM_LANCAMENTO") == 0 )
            {
               AV62TFContagemItem_Lancamento = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFContagemItem_Lancamento), 6, 0)));
               AV63TFContagemItem_Lancamento_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContagemItem_Lancamento_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63TFContagemItem_Lancamento_To), 6, 0)));
               if ( ! (0==AV62TFContagemItem_Lancamento) )
               {
                  Ddo_contagemitem_lancamento_Filteredtext_set = StringUtil.Str( (decimal)(AV62TFContagemItem_Lancamento), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_lancamento_Internalname, "FilteredText_set", Ddo_contagemitem_lancamento_Filteredtext_set);
               }
               if ( ! (0==AV63TFContagemItem_Lancamento_To) )
               {
                  Ddo_contagemitem_lancamento_Filteredtextto_set = StringUtil.Str( (decimal)(AV63TFContagemItem_Lancamento_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_lancamento_Internalname, "FilteredTextTo_set", Ddo_contagemitem_lancamento_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMITEMPARECER_COMENTARIO") == 0 )
            {
               AV66TFContagemItemParecer_Comentario = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContagemItemParecer_Comentario", AV66TFContagemItemParecer_Comentario);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFContagemItemParecer_Comentario)) )
               {
                  Ddo_contagemitemparecer_comentario_Filteredtext_set = AV66TFContagemItemParecer_Comentario;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_comentario_Internalname, "FilteredText_set", Ddo_contagemitemparecer_comentario_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMITEMPARECER_COMENTARIO_SEL") == 0 )
            {
               AV67TFContagemItemParecer_Comentario_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContagemItemParecer_Comentario_Sel", AV67TFContagemItemParecer_Comentario_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67TFContagemItemParecer_Comentario_Sel)) )
               {
                  Ddo_contagemitemparecer_comentario_Selectedvalue_set = AV67TFContagemItemParecer_Comentario_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_comentario_Internalname, "SelectedValue_set", Ddo_contagemitemparecer_comentario_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMITEMPARECER_DATA") == 0 )
            {
               AV70TFContagemItemParecer_Data = context.localUtil.CToT( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContagemItemParecer_Data", context.localUtil.TToC( AV70TFContagemItemParecer_Data, 8, 5, 0, 3, "/", ":", " "));
               AV71TFContagemItemParecer_Data_To = context.localUtil.CToT( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContagemItemParecer_Data_To", context.localUtil.TToC( AV71TFContagemItemParecer_Data_To, 8, 5, 0, 3, "/", ":", " "));
               if ( ! (DateTime.MinValue==AV70TFContagemItemParecer_Data) )
               {
                  AV72DDO_ContagemItemParecer_DataAuxDate = DateTimeUtil.ResetTime(AV70TFContagemItemParecer_Data);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72DDO_ContagemItemParecer_DataAuxDate", context.localUtil.Format(AV72DDO_ContagemItemParecer_DataAuxDate, "99/99/99"));
                  Ddo_contagemitemparecer_data_Filteredtext_set = context.localUtil.DToC( AV72DDO_ContagemItemParecer_DataAuxDate, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_data_Internalname, "FilteredText_set", Ddo_contagemitemparecer_data_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV71TFContagemItemParecer_Data_To) )
               {
                  AV73DDO_ContagemItemParecer_DataAuxDateTo = DateTimeUtil.ResetTime(AV71TFContagemItemParecer_Data_To);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73DDO_ContagemItemParecer_DataAuxDateTo", context.localUtil.Format(AV73DDO_ContagemItemParecer_DataAuxDateTo, "99/99/99"));
                  Ddo_contagemitemparecer_data_Filteredtextto_set = context.localUtil.DToC( AV73DDO_ContagemItemParecer_DataAuxDateTo, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_data_Internalname, "FilteredTextTo_set", Ddo_contagemitemparecer_data_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMITEMPARECER_USUARIOCOD") == 0 )
            {
               AV76TFContagemItemParecer_UsuarioCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContagemItemParecer_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76TFContagemItemParecer_UsuarioCod), 6, 0)));
               AV77TFContagemItemParecer_UsuarioCod_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFContagemItemParecer_UsuarioCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77TFContagemItemParecer_UsuarioCod_To), 6, 0)));
               if ( ! (0==AV76TFContagemItemParecer_UsuarioCod) )
               {
                  Ddo_contagemitemparecer_usuariocod_Filteredtext_set = StringUtil.Str( (decimal)(AV76TFContagemItemParecer_UsuarioCod), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_usuariocod_Internalname, "FilteredText_set", Ddo_contagemitemparecer_usuariocod_Filteredtext_set);
               }
               if ( ! (0==AV77TFContagemItemParecer_UsuarioCod_To) )
               {
                  Ddo_contagemitemparecer_usuariocod_Filteredtextto_set = StringUtil.Str( (decimal)(AV77TFContagemItemParecer_UsuarioCod_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_usuariocod_Internalname, "FilteredTextTo_set", Ddo_contagemitemparecer_usuariocod_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMITEMPARECER_USUARIOPESSOACOD") == 0 )
            {
               AV80TFContagemItemParecer_UsuarioPessoaCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContagemItemParecer_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV80TFContagemItemParecer_UsuarioPessoaCod), 6, 0)));
               AV81TFContagemItemParecer_UsuarioPessoaCod_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContagemItemParecer_UsuarioPessoaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81TFContagemItemParecer_UsuarioPessoaCod_To), 6, 0)));
               if ( ! (0==AV80TFContagemItemParecer_UsuarioPessoaCod) )
               {
                  Ddo_contagemitemparecer_usuariopessoacod_Filteredtext_set = StringUtil.Str( (decimal)(AV80TFContagemItemParecer_UsuarioPessoaCod), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_usuariopessoacod_Internalname, "FilteredText_set", Ddo_contagemitemparecer_usuariopessoacod_Filteredtext_set);
               }
               if ( ! (0==AV81TFContagemItemParecer_UsuarioPessoaCod_To) )
               {
                  Ddo_contagemitemparecer_usuariopessoacod_Filteredtextto_set = StringUtil.Str( (decimal)(AV81TFContagemItemParecer_UsuarioPessoaCod_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_usuariopessoacod_Internalname, "FilteredTextTo_set", Ddo_contagemitemparecer_usuariopessoacod_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 )
            {
               AV84TFContagemItemParecer_UsuarioPessoaNom = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContagemItemParecer_UsuarioPessoaNom", AV84TFContagemItemParecer_UsuarioPessoaNom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84TFContagemItemParecer_UsuarioPessoaNom)) )
               {
                  Ddo_contagemitemparecer_usuariopessoanom_Filteredtext_set = AV84TFContagemItemParecer_UsuarioPessoaNom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_usuariopessoanom_Internalname, "FilteredText_set", Ddo_contagemitemparecer_usuariopessoanom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMITEMPARECER_USUARIOPESSOANOM_SEL") == 0 )
            {
               AV85TFContagemItemParecer_UsuarioPessoaNom_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFContagemItemParecer_UsuarioPessoaNom_Sel", AV85TFContagemItemParecer_UsuarioPessoaNom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85TFContagemItemParecer_UsuarioPessoaNom_Sel)) )
               {
                  Ddo_contagemitemparecer_usuariopessoanom_Selectedvalue_set = AV85TFContagemItemParecer_UsuarioPessoaNom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitemparecer_usuariopessoanom_Internalname, "SelectedValue_set", Ddo_contagemitemparecer_usuariopessoanom_Selectedvalue_set);
               }
            }
            AV133GXV2 = (int)(AV133GXV2+1);
         }
      }

      protected void S232( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMITEMPARECER_COMENTARIO") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV18ContagemItemParecer_Comentario1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemItemParecer_Comentario1", AV18ContagemItemParecer_Comentario1);
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV19ContagemItemParecer_UsuarioPessoaNom1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemItemParecer_UsuarioPessoaNom1", AV19ContagemItemParecer_UsuarioPessoaNom1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV20DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV21DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMITEMPARECER_COMENTARIO") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV23ContagemItemParecer_Comentario2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemItemParecer_Comentario2", AV23ContagemItemParecer_Comentario2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV24ContagemItemParecer_UsuarioPessoaNom2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemItemParecer_UsuarioPessoaNom2", AV24ContagemItemParecer_UsuarioPessoaNom2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S132 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV25DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV26DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMITEMPARECER_COMENTARIO") == 0 )
                  {
                     AV27DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
                     AV28ContagemItemParecer_Comentario3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ContagemItemParecer_Comentario3", AV28ContagemItemParecer_Comentario3);
                  }
                  else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 )
                  {
                     AV27DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
                     AV29ContagemItemParecer_UsuarioPessoaNom3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemItemParecer_UsuarioPessoaNom3", AV29ContagemItemParecer_UsuarioPessoaNom3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S142 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV30DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S192( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV51Session.Get(AV131Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV58TFContagemItemParecer_Codigo) && (0==AV59TFContagemItemParecer_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMITEMPARECER_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV58TFContagemItemParecer_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV59TFContagemItemParecer_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV62TFContagemItem_Lancamento) && (0==AV63TFContagemItem_Lancamento_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMITEM_LANCAMENTO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV62TFContagemItem_Lancamento), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV63TFContagemItem_Lancamento_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFContagemItemParecer_Comentario)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMITEMPARECER_COMENTARIO";
            AV11GridStateFilterValue.gxTpr_Value = AV66TFContagemItemParecer_Comentario;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67TFContagemItemParecer_Comentario_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMITEMPARECER_COMENTARIO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV67TFContagemItemParecer_Comentario_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV70TFContagemItemParecer_Data) && (DateTime.MinValue==AV71TFContagemItemParecer_Data_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMITEMPARECER_DATA";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV70TFContagemItemParecer_Data, 8, 5, 0, 3, "/", ":", " ");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV71TFContagemItemParecer_Data_To, 8, 5, 0, 3, "/", ":", " ");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV76TFContagemItemParecer_UsuarioCod) && (0==AV77TFContagemItemParecer_UsuarioCod_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMITEMPARECER_USUARIOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV76TFContagemItemParecer_UsuarioCod), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV77TFContagemItemParecer_UsuarioCod_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV80TFContagemItemParecer_UsuarioPessoaCod) && (0==AV81TFContagemItemParecer_UsuarioPessoaCod_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMITEMPARECER_USUARIOPESSOACOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV80TFContagemItemParecer_UsuarioPessoaCod), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV81TFContagemItemParecer_UsuarioPessoaCod_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84TFContagemItemParecer_UsuarioPessoaNom)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMITEMPARECER_USUARIOPESSOANOM";
            AV11GridStateFilterValue.gxTpr_Value = AV84TFContagemItemParecer_UsuarioPessoaNom;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85TFContagemItemParecer_UsuarioPessoaNom_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMITEMPARECER_USUARIOPESSOANOM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV85TFContagemItemParecer_UsuarioPessoaNom_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV131Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S212( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV31DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMITEMPARECER_COMENTARIO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18ContagemItemParecer_Comentario1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV18ContagemItemParecer_Comentario1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19ContagemItemParecer_UsuarioPessoaNom1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV19ContagemItemParecer_UsuarioPessoaNom1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMITEMPARECER_COMENTARIO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23ContagemItemParecer_Comentario2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV23ContagemItemParecer_Comentario2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV24ContagemItemParecer_UsuarioPessoaNom2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV24ContagemItemParecer_UsuarioPessoaNom2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV25DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV26DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMITEMPARECER_COMENTARIO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV28ContagemItemParecer_Comentario3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV28ContagemItemParecer_Comentario3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV27DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV29ContagemItemParecer_UsuarioPessoaNom3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV29ContagemItemParecer_UsuarioPessoaNom3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV27DynamicFiltersOperator3;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S152( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV131Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContagemItemParecer";
         AV51Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_5V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_5V2( true) ;
         }
         else
         {
            wb_table2_8_5V2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_5V2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_85_5V2( true) ;
         }
         else
         {
            wb_table3_85_5V2( false) ;
         }
         return  ;
      }

      protected void wb_table3_85_5V2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_5V2e( true) ;
         }
         else
         {
            wb_table1_2_5V2e( false) ;
         }
      }

      protected void wb_table3_85_5V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_88_5V2( true) ;
         }
         else
         {
            wb_table4_88_5V2( false) ;
         }
         return  ;
      }

      protected void wb_table4_88_5V2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_85_5V2e( true) ;
         }
         else
         {
            wb_table3_85_5V2e( false) ;
         }
      }

      protected void wb_table4_88_5V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"91\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemItemParecer_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemItemParecer_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemItemParecer_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemItem_Lancamento_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemItem_Lancamento_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemItem_Lancamento_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemItemParecer_Comentario_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemItemParecer_Comentario_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemItemParecer_Comentario_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemItemParecer_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemItemParecer_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemItemParecer_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemItemParecer_UsuarioCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemItemParecer_UsuarioCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemItemParecer_UsuarioCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemItemParecer_UsuarioPessoaCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemItemParecer_UsuarioPessoaCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemItemParecer_UsuarioPessoaCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemItemParecer_UsuarioPessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemItemParecer_UsuarioPessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemItemParecer_UsuarioPessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV52Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV53Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV91Display));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A243ContagemItemParecer_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemItemParecer_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemItemParecer_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A224ContagemItem_Lancamento), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemItem_Lancamento_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemItem_Lancamento_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A244ContagemItemParecer_Comentario);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemItemParecer_Comentario_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemItemParecer_Comentario_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContagemItemParecer_Comentario_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A245ContagemItemParecer_Data, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemItemParecer_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemItemParecer_Data_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A246ContagemItemParecer_UsuarioCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemItemParecer_UsuarioCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemItemParecer_UsuarioCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A247ContagemItemParecer_UsuarioPessoaCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemItemParecer_UsuarioPessoaCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemItemParecer_UsuarioPessoaCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A248ContagemItemParecer_UsuarioPessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemItemParecer_UsuarioPessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemItemParecer_UsuarioPessoaNom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContagemItemParecer_UsuarioPessoaNom_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 91 )
         {
            wbEnd = 0;
            nRC_GXsfl_91 = (short)(nGXsfl_91_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_88_5V2e( true) ;
         }
         else
         {
            wb_table4_88_5V2e( false) ;
         }
      }

      protected void wb_table2_8_5V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_11_5V2( true) ;
         }
         else
         {
            wb_table5_11_5V2( false) ;
         }
         return  ;
      }

      protected void wb_table5_11_5V2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_23_5V2( true) ;
         }
         else
         {
            wb_table6_23_5V2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_5V2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_5V2e( true) ;
         }
         else
         {
            wb_table2_8_5V2e( false) ;
         }
      }

      protected void wb_table6_23_5V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MANAGEFILTERSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_5V2( true) ;
         }
         else
         {
            wb_table7_28_5V2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_5V2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_5V2e( true) ;
         }
         else
         {
            wb_table6_23_5V2e( false) ;
         }
      }

      protected void wb_table7_28_5V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_91_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWContagemItemParecer.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_37_5V2( true) ;
         }
         else
         {
            wb_table8_37_5V2( false) ;
         }
         return  ;
      }

      protected void wb_table8_37_5V2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContagemItemParecer.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_91_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", "", true, "HLP_WWContagemItemParecer.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_55_5V2( true) ;
         }
         else
         {
            wb_table9_55_5V2( false) ;
         }
         return  ;
      }

      protected void wb_table9_55_5V2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContagemItemParecer.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_91_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV26DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", "", true, "HLP_WWContagemItemParecer.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_73_5V2( true) ;
         }
         else
         {
            wb_table10_73_5V2( false) ;
         }
         return  ;
      }

      protected void wb_table10_73_5V2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_5V2e( true) ;
         }
         else
         {
            wb_table7_28_5V2e( false) ;
         }
      }

      protected void wb_table10_73_5V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_91_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", "", true, "HLP_WWContagemItemParecer.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_91_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavContagemitemparecer_comentario3_Internalname, AV28ContagemItemParecer_Comentario3, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,78);\"", 0, edtavContagemitemparecer_comentario3_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "5000", -1, "", "", -1, true, "", "HLP_WWContagemItemParecer.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemitemparecer_usuariopessoanom3_Internalname, StringUtil.RTrim( AV29ContagemItemParecer_UsuarioPessoaNom3), StringUtil.RTrim( context.localUtil.Format( AV29ContagemItemParecer_UsuarioPessoaNom3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,79);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemitemparecer_usuariopessoanom3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemitemparecer_usuariopessoanom3_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_73_5V2e( true) ;
         }
         else
         {
            wb_table10_73_5V2e( false) ;
         }
      }

      protected void wb_table9_55_5V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_91_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "", true, "HLP_WWContagemItemParecer.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'" + sGXsfl_91_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavContagemitemparecer_comentario2_Internalname, AV23ContagemItemParecer_Comentario2, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", 0, edtavContagemitemparecer_comentario2_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "5000", -1, "", "", -1, true, "", "HLP_WWContagemItemParecer.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemitemparecer_usuariopessoanom2_Internalname, StringUtil.RTrim( AV24ContagemItemParecer_UsuarioPessoaNom2), StringUtil.RTrim( context.localUtil.Format( AV24ContagemItemParecer_UsuarioPessoaNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemitemparecer_usuariopessoanom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemitemparecer_usuariopessoanom2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_55_5V2e( true) ;
         }
         else
         {
            wb_table9_55_5V2e( false) ;
         }
      }

      protected void wb_table8_37_5V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_91_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_WWContagemItemParecer.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_91_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavContagemitemparecer_comentario1_Internalname, AV18ContagemItemParecer_Comentario1, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", 0, edtavContagemitemparecer_comentario1_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "5000", -1, "", "", -1, true, "", "HLP_WWContagemItemParecer.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemitemparecer_usuariopessoanom1_Internalname, StringUtil.RTrim( AV19ContagemItemParecer_UsuarioPessoaNom1), StringUtil.RTrim( context.localUtil.Format( AV19ContagemItemParecer_UsuarioPessoaNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemitemparecer_usuariopessoanom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemitemparecer_usuariopessoanom1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_37_5V2e( true) ;
         }
         else
         {
            wb_table8_37_5V2e( false) ;
         }
      }

      protected void wb_table5_11_5V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemitemparecertitle_Internalname, "Contagem Item Parecer", "", "", lblContagemitemparecertitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, imgInsert_Bitmap, "", "", "", context.GetTheme( ), 1, imgInsert_Enabled, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_91_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWContagemItemParecer.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_11_5V2e( true) ;
         }
         else
         {
            wb_table5_11_5V2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA5V2( ) ;
         WS5V2( ) ;
         WE5V2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299345130");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwcontagemitemparecer.js", "?20205299345131");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_912( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_91_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_91_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_91_idx;
         edtContagemItemParecer_Codigo_Internalname = "CONTAGEMITEMPARECER_CODIGO_"+sGXsfl_91_idx;
         edtContagemItem_Lancamento_Internalname = "CONTAGEMITEM_LANCAMENTO_"+sGXsfl_91_idx;
         edtContagemItemParecer_Comentario_Internalname = "CONTAGEMITEMPARECER_COMENTARIO_"+sGXsfl_91_idx;
         edtContagemItemParecer_Data_Internalname = "CONTAGEMITEMPARECER_DATA_"+sGXsfl_91_idx;
         edtContagemItemParecer_UsuarioCod_Internalname = "CONTAGEMITEMPARECER_USUARIOCOD_"+sGXsfl_91_idx;
         edtContagemItemParecer_UsuarioPessoaCod_Internalname = "CONTAGEMITEMPARECER_USUARIOPESSOACOD_"+sGXsfl_91_idx;
         edtContagemItemParecer_UsuarioPessoaNom_Internalname = "CONTAGEMITEMPARECER_USUARIOPESSOANOM_"+sGXsfl_91_idx;
      }

      protected void SubsflControlProps_fel_912( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_91_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_91_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_91_fel_idx;
         edtContagemItemParecer_Codigo_Internalname = "CONTAGEMITEMPARECER_CODIGO_"+sGXsfl_91_fel_idx;
         edtContagemItem_Lancamento_Internalname = "CONTAGEMITEM_LANCAMENTO_"+sGXsfl_91_fel_idx;
         edtContagemItemParecer_Comentario_Internalname = "CONTAGEMITEMPARECER_COMENTARIO_"+sGXsfl_91_fel_idx;
         edtContagemItemParecer_Data_Internalname = "CONTAGEMITEMPARECER_DATA_"+sGXsfl_91_fel_idx;
         edtContagemItemParecer_UsuarioCod_Internalname = "CONTAGEMITEMPARECER_USUARIOCOD_"+sGXsfl_91_fel_idx;
         edtContagemItemParecer_UsuarioPessoaCod_Internalname = "CONTAGEMITEMPARECER_USUARIOPESSOACOD_"+sGXsfl_91_fel_idx;
         edtContagemItemParecer_UsuarioPessoaNom_Internalname = "CONTAGEMITEMPARECER_USUARIOPESSOANOM_"+sGXsfl_91_fel_idx;
      }

      protected void sendrow_912( )
      {
         SubsflControlProps_912( ) ;
         WB5V0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_91_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_91_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_91_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV52Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV52Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV128Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV52Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV52Update)) ? AV128Update_GXI : context.PathToRelativeUrl( AV52Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV52Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV53Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV53Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV129Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV53Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV53Delete)) ? AV129Delete_GXI : context.PathToRelativeUrl( AV53Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV53Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV91Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV91Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV130Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV91Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV91Display)) ? AV130Display_GXI : context.PathToRelativeUrl( AV91Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDisplay_Enabled,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV91Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItemParecer_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A243ContagemItemParecer_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A243ContagemItemParecer_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItemParecer_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)91,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_Lancamento_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A224ContagemItem_Lancamento), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A224ContagemItem_Lancamento), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_Lancamento_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)91,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItemParecer_Comentario_Internalname,(String)A244ContagemItemParecer_Comentario,(String)A244ContagemItemParecer_Comentario,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtContagemItemParecer_Comentario_Link,(String)"",(String)"",(String)"",(String)edtContagemItemParecer_Comentario_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)5000,(short)0,(short)0,(short)91,(short)1,(short)0,(short)-1,(bool)true,(String)"Comentario",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItemParecer_Data_Internalname,context.localUtil.TToC( A245ContagemItemParecer_Data, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A245ContagemItemParecer_Data, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItemParecer_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)91,(short)1,(short)-1,(short)0,(bool)true,(String)"DataHora",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItemParecer_UsuarioCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A246ContagemItemParecer_UsuarioCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A246ContagemItemParecer_UsuarioCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItemParecer_UsuarioCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)91,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItemParecer_UsuarioPessoaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A247ContagemItemParecer_UsuarioPessoaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A247ContagemItemParecer_UsuarioPessoaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItemParecer_UsuarioPessoaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)91,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItemParecer_UsuarioPessoaNom_Internalname,StringUtil.RTrim( A248ContagemItemParecer_UsuarioPessoaNom),StringUtil.RTrim( context.localUtil.Format( A248ContagemItemParecer_UsuarioPessoaNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtContagemItemParecer_UsuarioPessoaNom_Link,(String)"",(String)"",(String)"",(String)edtContagemItemParecer_UsuarioPessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)91,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEMPARECER_CODIGO"+"_"+sGXsfl_91_idx, GetSecureSignedToken( sGXsfl_91_idx, context.localUtil.Format( (decimal)(A243ContagemItemParecer_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEMPARECER_COMENTARIO"+"_"+sGXsfl_91_idx, GetSecureSignedToken( sGXsfl_91_idx, A244ContagemItemParecer_Comentario));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEMPARECER_DATA"+"_"+sGXsfl_91_idx, GetSecureSignedToken( sGXsfl_91_idx, context.localUtil.Format( A245ContagemItemParecer_Data, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEMPARECER_USUARIOCOD"+"_"+sGXsfl_91_idx, GetSecureSignedToken( sGXsfl_91_idx, context.localUtil.Format( (decimal)(A246ContagemItemParecer_UsuarioCod), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_91_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_91_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_91_idx+1));
            sGXsfl_91_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_91_idx), 4, 0)), 4, "0");
            SubsflControlProps_912( ) ;
         }
         /* End function sendrow_912 */
      }

      protected void init_default_properties( )
      {
         lblContagemitemparecertitle_Internalname = "CONTAGEMITEMPARECERTITLE";
         imgInsert_Internalname = "INSERT";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         tblTableactions_Internalname = "TABLEACTIONS";
         Ddo_managefilters_Internalname = "DDO_MANAGEFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavContagemitemparecer_comentario1_Internalname = "vCONTAGEMITEMPARECER_COMENTARIO1";
         edtavContagemitemparecer_usuariopessoanom1_Internalname = "vCONTAGEMITEMPARECER_USUARIOPESSOANOM1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavContagemitemparecer_comentario2_Internalname = "vCONTAGEMITEMPARECER_COMENTARIO2";
         edtavContagemitemparecer_usuariopessoanom2_Internalname = "vCONTAGEMITEMPARECER_USUARIOPESSOANOM2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavContagemitemparecer_comentario3_Internalname = "vCONTAGEMITEMPARECER_COMENTARIO3";
         edtavContagemitemparecer_usuariopessoanom3_Internalname = "vCONTAGEMITEMPARECER_USUARIOPESSOANOM3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtContagemItemParecer_Codigo_Internalname = "CONTAGEMITEMPARECER_CODIGO";
         edtContagemItem_Lancamento_Internalname = "CONTAGEMITEM_LANCAMENTO";
         edtContagemItemParecer_Comentario_Internalname = "CONTAGEMITEMPARECER_COMENTARIO";
         edtContagemItemParecer_Data_Internalname = "CONTAGEMITEMPARECER_DATA";
         edtContagemItemParecer_UsuarioCod_Internalname = "CONTAGEMITEMPARECER_USUARIOCOD";
         edtContagemItemParecer_UsuarioPessoaCod_Internalname = "CONTAGEMITEMPARECER_USUARIOPESSOACOD";
         edtContagemItemParecer_UsuarioPessoaNom_Internalname = "CONTAGEMITEMPARECER_USUARIOPESSOANOM";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavManagefiltersexecutionstep_Internalname = "vMANAGEFILTERSEXECUTIONSTEP";
         edtavTfcontagemitemparecer_codigo_Internalname = "vTFCONTAGEMITEMPARECER_CODIGO";
         edtavTfcontagemitemparecer_codigo_to_Internalname = "vTFCONTAGEMITEMPARECER_CODIGO_TO";
         edtavTfcontagemitem_lancamento_Internalname = "vTFCONTAGEMITEM_LANCAMENTO";
         edtavTfcontagemitem_lancamento_to_Internalname = "vTFCONTAGEMITEM_LANCAMENTO_TO";
         edtavTfcontagemitemparecer_comentario_Internalname = "vTFCONTAGEMITEMPARECER_COMENTARIO";
         edtavTfcontagemitemparecer_comentario_sel_Internalname = "vTFCONTAGEMITEMPARECER_COMENTARIO_SEL";
         edtavTfcontagemitemparecer_data_Internalname = "vTFCONTAGEMITEMPARECER_DATA";
         edtavTfcontagemitemparecer_data_to_Internalname = "vTFCONTAGEMITEMPARECER_DATA_TO";
         edtavDdo_contagemitemparecer_dataauxdate_Internalname = "vDDO_CONTAGEMITEMPARECER_DATAAUXDATE";
         edtavDdo_contagemitemparecer_dataauxdateto_Internalname = "vDDO_CONTAGEMITEMPARECER_DATAAUXDATETO";
         divDdo_contagemitemparecer_dataauxdates_Internalname = "DDO_CONTAGEMITEMPARECER_DATAAUXDATES";
         edtavTfcontagemitemparecer_usuariocod_Internalname = "vTFCONTAGEMITEMPARECER_USUARIOCOD";
         edtavTfcontagemitemparecer_usuariocod_to_Internalname = "vTFCONTAGEMITEMPARECER_USUARIOCOD_TO";
         edtavTfcontagemitemparecer_usuariopessoacod_Internalname = "vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD";
         edtavTfcontagemitemparecer_usuariopessoacod_to_Internalname = "vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD_TO";
         edtavTfcontagemitemparecer_usuariopessoanom_Internalname = "vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM";
         edtavTfcontagemitemparecer_usuariopessoanom_sel_Internalname = "vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM_SEL";
         Ddo_contagemitemparecer_codigo_Internalname = "DDO_CONTAGEMITEMPARECER_CODIGO";
         edtavDdo_contagemitemparecer_codigotitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMITEMPARECER_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_contagemitem_lancamento_Internalname = "DDO_CONTAGEMITEM_LANCAMENTO";
         edtavDdo_contagemitem_lancamentotitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMITEM_LANCAMENTOTITLECONTROLIDTOREPLACE";
         Ddo_contagemitemparecer_comentario_Internalname = "DDO_CONTAGEMITEMPARECER_COMENTARIO";
         edtavDdo_contagemitemparecer_comentariotitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMITEMPARECER_COMENTARIOTITLECONTROLIDTOREPLACE";
         Ddo_contagemitemparecer_data_Internalname = "DDO_CONTAGEMITEMPARECER_DATA";
         edtavDdo_contagemitemparecer_datatitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMITEMPARECER_DATATITLECONTROLIDTOREPLACE";
         Ddo_contagemitemparecer_usuariocod_Internalname = "DDO_CONTAGEMITEMPARECER_USUARIOCOD";
         edtavDdo_contagemitemparecer_usuariocodtitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMITEMPARECER_USUARIOCODTITLECONTROLIDTOREPLACE";
         Ddo_contagemitemparecer_usuariopessoacod_Internalname = "DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD";
         edtavDdo_contagemitemparecer_usuariopessoacodtitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMITEMPARECER_USUARIOPESSOACODTITLECONTROLIDTOREPLACE";
         Ddo_contagemitemparecer_usuariopessoanom_Internalname = "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM";
         edtavDdo_contagemitemparecer_usuariopessoanomtitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMITEMPARECER_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContagemItemParecer_UsuarioPessoaNom_Jsonclick = "";
         edtContagemItemParecer_UsuarioPessoaCod_Jsonclick = "";
         edtContagemItemParecer_UsuarioCod_Jsonclick = "";
         edtContagemItemParecer_Data_Jsonclick = "";
         edtContagemItemParecer_Comentario_Jsonclick = "";
         edtContagemItem_Lancamento_Jsonclick = "";
         edtContagemItemParecer_Codigo_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         imgInsert_Enabled = 1;
         imgInsert_Bitmap = (String)(context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )));
         edtavContagemitemparecer_usuariopessoanom1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavContagemitemparecer_usuariopessoanom2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavContagemitemparecer_usuariopessoanom3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtContagemItemParecer_UsuarioPessoaNom_Link = "";
         edtContagemItemParecer_Comentario_Link = "";
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDisplay_Enabled = 1;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         edtContagemItemParecer_UsuarioPessoaNom_Titleformat = 0;
         edtContagemItemParecer_UsuarioPessoaCod_Titleformat = 0;
         edtContagemItemParecer_UsuarioCod_Titleformat = 0;
         edtContagemItemParecer_Data_Titleformat = 0;
         edtContagemItemParecer_Comentario_Titleformat = 0;
         edtContagemItem_Lancamento_Titleformat = 0;
         edtContagemItemParecer_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavContagemitemparecer_usuariopessoanom3_Visible = 1;
         edtavContagemitemparecer_comentario3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavContagemitemparecer_usuariopessoanom2_Visible = 1;
         edtavContagemitemparecer_comentario2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavContagemitemparecer_usuariopessoanom1_Visible = 1;
         edtavContagemitemparecer_comentario1_Visible = 1;
         edtContagemItemParecer_UsuarioPessoaNom_Title = "do Usu�rio";
         edtContagemItemParecer_UsuarioPessoaCod_Title = "Pessoa ";
         edtContagemItemParecer_UsuarioCod_Title = "do Usu�rio";
         edtContagemItemParecer_Data_Title = "do Parecer";
         edtContagemItemParecer_Comentario_Title = "Coment�rios";
         edtContagemItem_Lancamento_Title = "Item do Lan�amento";
         edtContagemItemParecer_Codigo_Title = "Sequencial Parecer";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_contagemitemparecer_usuariopessoanomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemitemparecer_usuariopessoacodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemitemparecer_usuariocodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemitemparecer_datatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemitemparecer_comentariotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemitem_lancamentotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemitemparecer_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfcontagemitemparecer_usuariopessoanom_sel_Jsonclick = "";
         edtavTfcontagemitemparecer_usuariopessoanom_sel_Visible = 1;
         edtavTfcontagemitemparecer_usuariopessoanom_Jsonclick = "";
         edtavTfcontagemitemparecer_usuariopessoanom_Visible = 1;
         edtavTfcontagemitemparecer_usuariopessoacod_to_Jsonclick = "";
         edtavTfcontagemitemparecer_usuariopessoacod_to_Visible = 1;
         edtavTfcontagemitemparecer_usuariopessoacod_Jsonclick = "";
         edtavTfcontagemitemparecer_usuariopessoacod_Visible = 1;
         edtavTfcontagemitemparecer_usuariocod_to_Jsonclick = "";
         edtavTfcontagemitemparecer_usuariocod_to_Visible = 1;
         edtavTfcontagemitemparecer_usuariocod_Jsonclick = "";
         edtavTfcontagemitemparecer_usuariocod_Visible = 1;
         edtavDdo_contagemitemparecer_dataauxdateto_Jsonclick = "";
         edtavDdo_contagemitemparecer_dataauxdate_Jsonclick = "";
         edtavTfcontagemitemparecer_data_to_Jsonclick = "";
         edtavTfcontagemitemparecer_data_to_Visible = 1;
         edtavTfcontagemitemparecer_data_Jsonclick = "";
         edtavTfcontagemitemparecer_data_Visible = 1;
         edtavTfcontagemitemparecer_comentario_sel_Visible = 1;
         edtavTfcontagemitemparecer_comentario_Visible = 1;
         edtavTfcontagemitem_lancamento_to_Jsonclick = "";
         edtavTfcontagemitem_lancamento_to_Visible = 1;
         edtavTfcontagemitem_lancamento_Jsonclick = "";
         edtavTfcontagemitem_lancamento_Visible = 1;
         edtavTfcontagemitemparecer_codigo_to_Jsonclick = "";
         edtavTfcontagemitemparecer_codigo_to_Visible = 1;
         edtavTfcontagemitemparecer_codigo_Jsonclick = "";
         edtavTfcontagemitemparecer_codigo_Visible = 1;
         edtavManagefiltersexecutionstep_Jsonclick = "";
         edtavManagefiltersexecutionstep_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_contagemitemparecer_usuariopessoanom_Searchbuttontext = "Pesquisar";
         Ddo_contagemitemparecer_usuariopessoanom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemitemparecer_usuariopessoanom_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemitemparecer_usuariopessoanom_Loadingdata = "Carregando dados...";
         Ddo_contagemitemparecer_usuariopessoanom_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemitemparecer_usuariopessoanom_Sortasc = "Ordenar de A � Z";
         Ddo_contagemitemparecer_usuariopessoanom_Datalistupdateminimumcharacters = 0;
         Ddo_contagemitemparecer_usuariopessoanom_Datalistproc = "GetWWContagemItemParecerFilterData";
         Ddo_contagemitemparecer_usuariopessoanom_Datalisttype = "Dynamic";
         Ddo_contagemitemparecer_usuariopessoanom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemitemparecer_usuariopessoanom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contagemitemparecer_usuariopessoanom_Filtertype = "Character";
         Ddo_contagemitemparecer_usuariopessoanom_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemitemparecer_usuariopessoanom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemitemparecer_usuariopessoanom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemitemparecer_usuariopessoanom_Titlecontrolidtoreplace = "";
         Ddo_contagemitemparecer_usuariopessoanom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemitemparecer_usuariopessoanom_Cls = "ColumnSettings";
         Ddo_contagemitemparecer_usuariopessoanom_Tooltip = "Op��es";
         Ddo_contagemitemparecer_usuariopessoanom_Caption = "";
         Ddo_contagemitemparecer_usuariopessoacod_Searchbuttontext = "Pesquisar";
         Ddo_contagemitemparecer_usuariopessoacod_Rangefilterto = "At�";
         Ddo_contagemitemparecer_usuariopessoacod_Rangefilterfrom = "Desde";
         Ddo_contagemitemparecer_usuariopessoacod_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemitemparecer_usuariopessoacod_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemitemparecer_usuariopessoacod_Sortasc = "Ordenar de A � Z";
         Ddo_contagemitemparecer_usuariopessoacod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemitemparecer_usuariopessoacod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemitemparecer_usuariopessoacod_Filtertype = "Numeric";
         Ddo_contagemitemparecer_usuariopessoacod_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemitemparecer_usuariopessoacod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemitemparecer_usuariopessoacod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemitemparecer_usuariopessoacod_Titlecontrolidtoreplace = "";
         Ddo_contagemitemparecer_usuariopessoacod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemitemparecer_usuariopessoacod_Cls = "ColumnSettings";
         Ddo_contagemitemparecer_usuariopessoacod_Tooltip = "Op��es";
         Ddo_contagemitemparecer_usuariopessoacod_Caption = "";
         Ddo_contagemitemparecer_usuariocod_Searchbuttontext = "Pesquisar";
         Ddo_contagemitemparecer_usuariocod_Rangefilterto = "At�";
         Ddo_contagemitemparecer_usuariocod_Rangefilterfrom = "Desde";
         Ddo_contagemitemparecer_usuariocod_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemitemparecer_usuariocod_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemitemparecer_usuariocod_Sortasc = "Ordenar de A � Z";
         Ddo_contagemitemparecer_usuariocod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemitemparecer_usuariocod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemitemparecer_usuariocod_Filtertype = "Numeric";
         Ddo_contagemitemparecer_usuariocod_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemitemparecer_usuariocod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemitemparecer_usuariocod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemitemparecer_usuariocod_Titlecontrolidtoreplace = "";
         Ddo_contagemitemparecer_usuariocod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemitemparecer_usuariocod_Cls = "ColumnSettings";
         Ddo_contagemitemparecer_usuariocod_Tooltip = "Op��es";
         Ddo_contagemitemparecer_usuariocod_Caption = "";
         Ddo_contagemitemparecer_data_Searchbuttontext = "Pesquisar";
         Ddo_contagemitemparecer_data_Rangefilterto = "At�";
         Ddo_contagemitemparecer_data_Rangefilterfrom = "Desde";
         Ddo_contagemitemparecer_data_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemitemparecer_data_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemitemparecer_data_Sortasc = "Ordenar de A � Z";
         Ddo_contagemitemparecer_data_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemitemparecer_data_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemitemparecer_data_Filtertype = "Date";
         Ddo_contagemitemparecer_data_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemitemparecer_data_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemitemparecer_data_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemitemparecer_data_Titlecontrolidtoreplace = "";
         Ddo_contagemitemparecer_data_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemitemparecer_data_Cls = "ColumnSettings";
         Ddo_contagemitemparecer_data_Tooltip = "Op��es";
         Ddo_contagemitemparecer_data_Caption = "";
         Ddo_contagemitemparecer_comentario_Searchbuttontext = "Pesquisar";
         Ddo_contagemitemparecer_comentario_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemitemparecer_comentario_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemitemparecer_comentario_Loadingdata = "Carregando dados...";
         Ddo_contagemitemparecer_comentario_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemitemparecer_comentario_Sortasc = "Ordenar de A � Z";
         Ddo_contagemitemparecer_comentario_Datalistupdateminimumcharacters = 0;
         Ddo_contagemitemparecer_comentario_Datalistproc = "GetWWContagemItemParecerFilterData";
         Ddo_contagemitemparecer_comentario_Datalisttype = "Dynamic";
         Ddo_contagemitemparecer_comentario_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemitemparecer_comentario_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contagemitemparecer_comentario_Filtertype = "Character";
         Ddo_contagemitemparecer_comentario_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemitemparecer_comentario_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemitemparecer_comentario_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemitemparecer_comentario_Titlecontrolidtoreplace = "";
         Ddo_contagemitemparecer_comentario_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemitemparecer_comentario_Cls = "ColumnSettings";
         Ddo_contagemitemparecer_comentario_Tooltip = "Op��es";
         Ddo_contagemitemparecer_comentario_Caption = "";
         Ddo_contagemitem_lancamento_Searchbuttontext = "Pesquisar";
         Ddo_contagemitem_lancamento_Rangefilterto = "At�";
         Ddo_contagemitem_lancamento_Rangefilterfrom = "Desde";
         Ddo_contagemitem_lancamento_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemitem_lancamento_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemitem_lancamento_Sortasc = "Ordenar de A � Z";
         Ddo_contagemitem_lancamento_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemitem_lancamento_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemitem_lancamento_Filtertype = "Numeric";
         Ddo_contagemitem_lancamento_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemitem_lancamento_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemitem_lancamento_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemitem_lancamento_Titlecontrolidtoreplace = "";
         Ddo_contagemitem_lancamento_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemitem_lancamento_Cls = "ColumnSettings";
         Ddo_contagemitem_lancamento_Tooltip = "Op��es";
         Ddo_contagemitem_lancamento_Caption = "";
         Ddo_contagemitemparecer_codigo_Searchbuttontext = "Pesquisar";
         Ddo_contagemitemparecer_codigo_Rangefilterto = "At�";
         Ddo_contagemitemparecer_codigo_Rangefilterfrom = "Desde";
         Ddo_contagemitemparecer_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemitemparecer_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemitemparecer_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_contagemitemparecer_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemitemparecer_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemitemparecer_codigo_Filtertype = "Numeric";
         Ddo_contagemitemparecer_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemitemparecer_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemitemparecer_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemitemparecer_codigo_Titlecontrolidtoreplace = "";
         Ddo_contagemitemparecer_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemitemparecer_codigo_Cls = "ColumnSettings";
         Ddo_contagemitemparecer_codigo_Tooltip = "Op��es";
         Ddo_contagemitemparecer_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Ddo_managefilters_Cls = "ManageFilters";
         Ddo_managefilters_Tooltip = "Gerenciar filtros";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Contagem Item Parecer";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A243ContagemItemParecer_Codigo',fld:'CONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A246ContagemItemParecer_UsuarioCod',fld:'CONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV92ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_LANCAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_COMENTARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemItemParecer_Comentario1',fld:'vCONTAGEMITEMPARECER_COMENTARIO1',pic:'',nv:''},{av:'AV19ContagemItemParecer_UsuarioPessoaNom1',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemItemParecer_Comentario2',fld:'vCONTAGEMITEMPARECER_COMENTARIO2',pic:'',nv:''},{av:'AV24ContagemItemParecer_UsuarioPessoaNom2',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemItemParecer_Comentario3',fld:'vCONTAGEMITEMPARECER_COMENTARIO3',pic:'',nv:''},{av:'AV29ContagemItemParecer_UsuarioPessoaNom3',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV58TFContagemItemParecer_Codigo',fld:'vTFCONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFContagemItemParecer_Codigo_To',fld:'vTFCONTAGEMITEMPARECER_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV62TFContagemItem_Lancamento',fld:'vTFCONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'AV63TFContagemItem_Lancamento_To',fld:'vTFCONTAGEMITEM_LANCAMENTO_TO',pic:'ZZZZZ9',nv:0},{av:'AV66TFContagemItemParecer_Comentario',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO',pic:'',nv:''},{av:'AV67TFContagemItemParecer_Comentario_Sel',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO_SEL',pic:'',nv:''},{av:'AV70TFContagemItemParecer_Data',fld:'vTFCONTAGEMITEMPARECER_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContagemItemParecer_Data_To',fld:'vTFCONTAGEMITEMPARECER_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV76TFContagemItemParecer_UsuarioCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV77TFContagemItemParecer_UsuarioCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV80TFContagemItemParecer_UsuarioPessoaCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV81TFContagemItemParecer_UsuarioPessoaCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV84TFContagemItemParecer_UsuarioPessoaNom',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV85TFContagemItemParecer_UsuarioPessoaNom_Sel',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV131Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV57ContagemItemParecer_CodigoTitleFilterData',fld:'vCONTAGEMITEMPARECER_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV61ContagemItem_LancamentoTitleFilterData',fld:'vCONTAGEMITEM_LANCAMENTOTITLEFILTERDATA',pic:'',nv:null},{av:'AV65ContagemItemParecer_ComentarioTitleFilterData',fld:'vCONTAGEMITEMPARECER_COMENTARIOTITLEFILTERDATA',pic:'',nv:null},{av:'AV69ContagemItemParecer_DataTitleFilterData',fld:'vCONTAGEMITEMPARECER_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV75ContagemItemParecer_UsuarioCodTitleFilterData',fld:'vCONTAGEMITEMPARECER_USUARIOCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV79ContagemItemParecer_UsuarioPessoaCodTitleFilterData',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOACODTITLEFILTERDATA',pic:'',nv:null},{av:'AV83ContagemItemParecer_UsuarioPessoaNomTitleFilterData',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV92ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtContagemItemParecer_Codigo_Titleformat',ctrl:'CONTAGEMITEMPARECER_CODIGO',prop:'Titleformat'},{av:'edtContagemItemParecer_Codigo_Title',ctrl:'CONTAGEMITEMPARECER_CODIGO',prop:'Title'},{av:'edtContagemItem_Lancamento_Titleformat',ctrl:'CONTAGEMITEM_LANCAMENTO',prop:'Titleformat'},{av:'edtContagemItem_Lancamento_Title',ctrl:'CONTAGEMITEM_LANCAMENTO',prop:'Title'},{av:'edtContagemItemParecer_Comentario_Titleformat',ctrl:'CONTAGEMITEMPARECER_COMENTARIO',prop:'Titleformat'},{av:'edtContagemItemParecer_Comentario_Title',ctrl:'CONTAGEMITEMPARECER_COMENTARIO',prop:'Title'},{av:'edtContagemItemParecer_Data_Titleformat',ctrl:'CONTAGEMITEMPARECER_DATA',prop:'Titleformat'},{av:'edtContagemItemParecer_Data_Title',ctrl:'CONTAGEMITEMPARECER_DATA',prop:'Title'},{av:'edtContagemItemParecer_UsuarioCod_Titleformat',ctrl:'CONTAGEMITEMPARECER_USUARIOCOD',prop:'Titleformat'},{av:'edtContagemItemParecer_UsuarioCod_Title',ctrl:'CONTAGEMITEMPARECER_USUARIOCOD',prop:'Title'},{av:'edtContagemItemParecer_UsuarioPessoaCod_Titleformat',ctrl:'CONTAGEMITEMPARECER_USUARIOPESSOACOD',prop:'Titleformat'},{av:'edtContagemItemParecer_UsuarioPessoaCod_Title',ctrl:'CONTAGEMITEMPARECER_USUARIOPESSOACOD',prop:'Title'},{av:'edtContagemItemParecer_UsuarioPessoaNom_Titleformat',ctrl:'CONTAGEMITEMPARECER_USUARIOPESSOANOM',prop:'Titleformat'},{av:'edtContagemItemParecer_UsuarioPessoaNom_Title',ctrl:'CONTAGEMITEMPARECER_USUARIOPESSOANOM',prop:'Title'},{av:'AV89GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV90GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{av:'AV96ManageFiltersData',fld:'vMANAGEFILTERSDATA',pic:'',nv:null},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E125V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemItemParecer_Comentario1',fld:'vCONTAGEMITEMPARECER_COMENTARIO1',pic:'',nv:''},{av:'AV19ContagemItemParecer_UsuarioPessoaNom1',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemItemParecer_Comentario2',fld:'vCONTAGEMITEMPARECER_COMENTARIO2',pic:'',nv:''},{av:'AV24ContagemItemParecer_UsuarioPessoaNom2',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemItemParecer_Comentario3',fld:'vCONTAGEMITEMPARECER_COMENTARIO3',pic:'',nv:''},{av:'AV29ContagemItemParecer_UsuarioPessoaNom3',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58TFContagemItemParecer_Codigo',fld:'vTFCONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFContagemItemParecer_Codigo_To',fld:'vTFCONTAGEMITEMPARECER_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV62TFContagemItem_Lancamento',fld:'vTFCONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'AV63TFContagemItem_Lancamento_To',fld:'vTFCONTAGEMITEM_LANCAMENTO_TO',pic:'ZZZZZ9',nv:0},{av:'AV66TFContagemItemParecer_Comentario',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO',pic:'',nv:''},{av:'AV67TFContagemItemParecer_Comentario_Sel',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO_SEL',pic:'',nv:''},{av:'AV70TFContagemItemParecer_Data',fld:'vTFCONTAGEMITEMPARECER_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContagemItemParecer_Data_To',fld:'vTFCONTAGEMITEMPARECER_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV76TFContagemItemParecer_UsuarioCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV77TFContagemItemParecer_UsuarioCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV80TFContagemItemParecer_UsuarioPessoaCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV81TFContagemItemParecer_UsuarioPessoaCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV84TFContagemItemParecer_UsuarioPessoaNom',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV85TFContagemItemParecer_UsuarioPessoaNom_Sel',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV92ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_LANCAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_COMENTARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV131Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A243ContagemItemParecer_Codigo',fld:'CONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A246ContagemItemParecer_UsuarioCod',fld:'CONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTAGEMITEMPARECER_CODIGO.ONOPTIONCLICKED","{handler:'E135V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemItemParecer_Comentario1',fld:'vCONTAGEMITEMPARECER_COMENTARIO1',pic:'',nv:''},{av:'AV19ContagemItemParecer_UsuarioPessoaNom1',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemItemParecer_Comentario2',fld:'vCONTAGEMITEMPARECER_COMENTARIO2',pic:'',nv:''},{av:'AV24ContagemItemParecer_UsuarioPessoaNom2',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemItemParecer_Comentario3',fld:'vCONTAGEMITEMPARECER_COMENTARIO3',pic:'',nv:''},{av:'AV29ContagemItemParecer_UsuarioPessoaNom3',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58TFContagemItemParecer_Codigo',fld:'vTFCONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFContagemItemParecer_Codigo_To',fld:'vTFCONTAGEMITEMPARECER_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV62TFContagemItem_Lancamento',fld:'vTFCONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'AV63TFContagemItem_Lancamento_To',fld:'vTFCONTAGEMITEM_LANCAMENTO_TO',pic:'ZZZZZ9',nv:0},{av:'AV66TFContagemItemParecer_Comentario',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO',pic:'',nv:''},{av:'AV67TFContagemItemParecer_Comentario_Sel',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO_SEL',pic:'',nv:''},{av:'AV70TFContagemItemParecer_Data',fld:'vTFCONTAGEMITEMPARECER_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContagemItemParecer_Data_To',fld:'vTFCONTAGEMITEMPARECER_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV76TFContagemItemParecer_UsuarioCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV77TFContagemItemParecer_UsuarioCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV80TFContagemItemParecer_UsuarioPessoaCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV81TFContagemItemParecer_UsuarioPessoaCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV84TFContagemItemParecer_UsuarioPessoaNom',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV85TFContagemItemParecer_UsuarioPessoaNom_Sel',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV92ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_LANCAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_COMENTARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV131Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A243ContagemItemParecer_Codigo',fld:'CONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A246ContagemItemParecer_UsuarioCod',fld:'CONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contagemitemparecer_codigo_Activeeventkey',ctrl:'DDO_CONTAGEMITEMPARECER_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_contagemitemparecer_codigo_Filteredtext_get',ctrl:'DDO_CONTAGEMITEMPARECER_CODIGO',prop:'FilteredText_get'},{av:'Ddo_contagemitemparecer_codigo_Filteredtextto_get',ctrl:'DDO_CONTAGEMITEMPARECER_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemitemparecer_codigo_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_CODIGO',prop:'SortedStatus'},{av:'AV58TFContagemItemParecer_Codigo',fld:'vTFCONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFContagemItemParecer_Codigo_To',fld:'vTFCONTAGEMITEMPARECER_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemitem_lancamento_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_LANCAMENTO',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_comentario_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_COMENTARIO',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_data_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_DATA',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_usuariocod_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOCOD',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_usuariopessoacod_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_usuariopessoanom_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMITEM_LANCAMENTO.ONOPTIONCLICKED","{handler:'E145V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemItemParecer_Comentario1',fld:'vCONTAGEMITEMPARECER_COMENTARIO1',pic:'',nv:''},{av:'AV19ContagemItemParecer_UsuarioPessoaNom1',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemItemParecer_Comentario2',fld:'vCONTAGEMITEMPARECER_COMENTARIO2',pic:'',nv:''},{av:'AV24ContagemItemParecer_UsuarioPessoaNom2',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemItemParecer_Comentario3',fld:'vCONTAGEMITEMPARECER_COMENTARIO3',pic:'',nv:''},{av:'AV29ContagemItemParecer_UsuarioPessoaNom3',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58TFContagemItemParecer_Codigo',fld:'vTFCONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFContagemItemParecer_Codigo_To',fld:'vTFCONTAGEMITEMPARECER_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV62TFContagemItem_Lancamento',fld:'vTFCONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'AV63TFContagemItem_Lancamento_To',fld:'vTFCONTAGEMITEM_LANCAMENTO_TO',pic:'ZZZZZ9',nv:0},{av:'AV66TFContagemItemParecer_Comentario',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO',pic:'',nv:''},{av:'AV67TFContagemItemParecer_Comentario_Sel',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO_SEL',pic:'',nv:''},{av:'AV70TFContagemItemParecer_Data',fld:'vTFCONTAGEMITEMPARECER_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContagemItemParecer_Data_To',fld:'vTFCONTAGEMITEMPARECER_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV76TFContagemItemParecer_UsuarioCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV77TFContagemItemParecer_UsuarioCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV80TFContagemItemParecer_UsuarioPessoaCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV81TFContagemItemParecer_UsuarioPessoaCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV84TFContagemItemParecer_UsuarioPessoaNom',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV85TFContagemItemParecer_UsuarioPessoaNom_Sel',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV92ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_LANCAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_COMENTARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV131Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A243ContagemItemParecer_Codigo',fld:'CONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A246ContagemItemParecer_UsuarioCod',fld:'CONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contagemitem_lancamento_Activeeventkey',ctrl:'DDO_CONTAGEMITEM_LANCAMENTO',prop:'ActiveEventKey'},{av:'Ddo_contagemitem_lancamento_Filteredtext_get',ctrl:'DDO_CONTAGEMITEM_LANCAMENTO',prop:'FilteredText_get'},{av:'Ddo_contagemitem_lancamento_Filteredtextto_get',ctrl:'DDO_CONTAGEMITEM_LANCAMENTO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemitem_lancamento_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_LANCAMENTO',prop:'SortedStatus'},{av:'AV62TFContagemItem_Lancamento',fld:'vTFCONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'AV63TFContagemItem_Lancamento_To',fld:'vTFCONTAGEMITEM_LANCAMENTO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemitemparecer_codigo_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_CODIGO',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_comentario_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_COMENTARIO',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_data_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_DATA',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_usuariocod_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOCOD',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_usuariopessoacod_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_usuariopessoanom_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMITEMPARECER_COMENTARIO.ONOPTIONCLICKED","{handler:'E155V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemItemParecer_Comentario1',fld:'vCONTAGEMITEMPARECER_COMENTARIO1',pic:'',nv:''},{av:'AV19ContagemItemParecer_UsuarioPessoaNom1',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemItemParecer_Comentario2',fld:'vCONTAGEMITEMPARECER_COMENTARIO2',pic:'',nv:''},{av:'AV24ContagemItemParecer_UsuarioPessoaNom2',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemItemParecer_Comentario3',fld:'vCONTAGEMITEMPARECER_COMENTARIO3',pic:'',nv:''},{av:'AV29ContagemItemParecer_UsuarioPessoaNom3',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58TFContagemItemParecer_Codigo',fld:'vTFCONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFContagemItemParecer_Codigo_To',fld:'vTFCONTAGEMITEMPARECER_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV62TFContagemItem_Lancamento',fld:'vTFCONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'AV63TFContagemItem_Lancamento_To',fld:'vTFCONTAGEMITEM_LANCAMENTO_TO',pic:'ZZZZZ9',nv:0},{av:'AV66TFContagemItemParecer_Comentario',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO',pic:'',nv:''},{av:'AV67TFContagemItemParecer_Comentario_Sel',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO_SEL',pic:'',nv:''},{av:'AV70TFContagemItemParecer_Data',fld:'vTFCONTAGEMITEMPARECER_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContagemItemParecer_Data_To',fld:'vTFCONTAGEMITEMPARECER_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV76TFContagemItemParecer_UsuarioCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV77TFContagemItemParecer_UsuarioCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV80TFContagemItemParecer_UsuarioPessoaCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV81TFContagemItemParecer_UsuarioPessoaCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV84TFContagemItemParecer_UsuarioPessoaNom',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV85TFContagemItemParecer_UsuarioPessoaNom_Sel',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV92ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_LANCAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_COMENTARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV131Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A243ContagemItemParecer_Codigo',fld:'CONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A246ContagemItemParecer_UsuarioCod',fld:'CONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contagemitemparecer_comentario_Activeeventkey',ctrl:'DDO_CONTAGEMITEMPARECER_COMENTARIO',prop:'ActiveEventKey'},{av:'Ddo_contagemitemparecer_comentario_Filteredtext_get',ctrl:'DDO_CONTAGEMITEMPARECER_COMENTARIO',prop:'FilteredText_get'},{av:'Ddo_contagemitemparecer_comentario_Selectedvalue_get',ctrl:'DDO_CONTAGEMITEMPARECER_COMENTARIO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemitemparecer_comentario_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_COMENTARIO',prop:'SortedStatus'},{av:'AV66TFContagemItemParecer_Comentario',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO',pic:'',nv:''},{av:'AV67TFContagemItemParecer_Comentario_Sel',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO_SEL',pic:'',nv:''},{av:'Ddo_contagemitemparecer_codigo_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_CODIGO',prop:'SortedStatus'},{av:'Ddo_contagemitem_lancamento_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_LANCAMENTO',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_data_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_DATA',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_usuariocod_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOCOD',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_usuariopessoacod_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_usuariopessoanom_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMITEMPARECER_DATA.ONOPTIONCLICKED","{handler:'E165V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemItemParecer_Comentario1',fld:'vCONTAGEMITEMPARECER_COMENTARIO1',pic:'',nv:''},{av:'AV19ContagemItemParecer_UsuarioPessoaNom1',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemItemParecer_Comentario2',fld:'vCONTAGEMITEMPARECER_COMENTARIO2',pic:'',nv:''},{av:'AV24ContagemItemParecer_UsuarioPessoaNom2',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemItemParecer_Comentario3',fld:'vCONTAGEMITEMPARECER_COMENTARIO3',pic:'',nv:''},{av:'AV29ContagemItemParecer_UsuarioPessoaNom3',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58TFContagemItemParecer_Codigo',fld:'vTFCONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFContagemItemParecer_Codigo_To',fld:'vTFCONTAGEMITEMPARECER_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV62TFContagemItem_Lancamento',fld:'vTFCONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'AV63TFContagemItem_Lancamento_To',fld:'vTFCONTAGEMITEM_LANCAMENTO_TO',pic:'ZZZZZ9',nv:0},{av:'AV66TFContagemItemParecer_Comentario',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO',pic:'',nv:''},{av:'AV67TFContagemItemParecer_Comentario_Sel',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO_SEL',pic:'',nv:''},{av:'AV70TFContagemItemParecer_Data',fld:'vTFCONTAGEMITEMPARECER_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContagemItemParecer_Data_To',fld:'vTFCONTAGEMITEMPARECER_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV76TFContagemItemParecer_UsuarioCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV77TFContagemItemParecer_UsuarioCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV80TFContagemItemParecer_UsuarioPessoaCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV81TFContagemItemParecer_UsuarioPessoaCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV84TFContagemItemParecer_UsuarioPessoaNom',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV85TFContagemItemParecer_UsuarioPessoaNom_Sel',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV92ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_LANCAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_COMENTARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV131Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A243ContagemItemParecer_Codigo',fld:'CONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A246ContagemItemParecer_UsuarioCod',fld:'CONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contagemitemparecer_data_Activeeventkey',ctrl:'DDO_CONTAGEMITEMPARECER_DATA',prop:'ActiveEventKey'},{av:'Ddo_contagemitemparecer_data_Filteredtext_get',ctrl:'DDO_CONTAGEMITEMPARECER_DATA',prop:'FilteredText_get'},{av:'Ddo_contagemitemparecer_data_Filteredtextto_get',ctrl:'DDO_CONTAGEMITEMPARECER_DATA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemitemparecer_data_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_DATA',prop:'SortedStatus'},{av:'AV70TFContagemItemParecer_Data',fld:'vTFCONTAGEMITEMPARECER_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContagemItemParecer_Data_To',fld:'vTFCONTAGEMITEMPARECER_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_contagemitemparecer_codigo_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_CODIGO',prop:'SortedStatus'},{av:'Ddo_contagemitem_lancamento_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_LANCAMENTO',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_comentario_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_COMENTARIO',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_usuariocod_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOCOD',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_usuariopessoacod_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_usuariopessoanom_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMITEMPARECER_USUARIOCOD.ONOPTIONCLICKED","{handler:'E175V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemItemParecer_Comentario1',fld:'vCONTAGEMITEMPARECER_COMENTARIO1',pic:'',nv:''},{av:'AV19ContagemItemParecer_UsuarioPessoaNom1',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemItemParecer_Comentario2',fld:'vCONTAGEMITEMPARECER_COMENTARIO2',pic:'',nv:''},{av:'AV24ContagemItemParecer_UsuarioPessoaNom2',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemItemParecer_Comentario3',fld:'vCONTAGEMITEMPARECER_COMENTARIO3',pic:'',nv:''},{av:'AV29ContagemItemParecer_UsuarioPessoaNom3',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58TFContagemItemParecer_Codigo',fld:'vTFCONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFContagemItemParecer_Codigo_To',fld:'vTFCONTAGEMITEMPARECER_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV62TFContagemItem_Lancamento',fld:'vTFCONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'AV63TFContagemItem_Lancamento_To',fld:'vTFCONTAGEMITEM_LANCAMENTO_TO',pic:'ZZZZZ9',nv:0},{av:'AV66TFContagemItemParecer_Comentario',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO',pic:'',nv:''},{av:'AV67TFContagemItemParecer_Comentario_Sel',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO_SEL',pic:'',nv:''},{av:'AV70TFContagemItemParecer_Data',fld:'vTFCONTAGEMITEMPARECER_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContagemItemParecer_Data_To',fld:'vTFCONTAGEMITEMPARECER_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV76TFContagemItemParecer_UsuarioCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV77TFContagemItemParecer_UsuarioCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV80TFContagemItemParecer_UsuarioPessoaCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV81TFContagemItemParecer_UsuarioPessoaCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV84TFContagemItemParecer_UsuarioPessoaNom',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV85TFContagemItemParecer_UsuarioPessoaNom_Sel',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV92ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_LANCAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_COMENTARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV131Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A243ContagemItemParecer_Codigo',fld:'CONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A246ContagemItemParecer_UsuarioCod',fld:'CONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contagemitemparecer_usuariocod_Activeeventkey',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOCOD',prop:'ActiveEventKey'},{av:'Ddo_contagemitemparecer_usuariocod_Filteredtext_get',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOCOD',prop:'FilteredText_get'},{av:'Ddo_contagemitemparecer_usuariocod_Filteredtextto_get',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemitemparecer_usuariocod_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOCOD',prop:'SortedStatus'},{av:'AV76TFContagemItemParecer_UsuarioCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV77TFContagemItemParecer_UsuarioCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemitemparecer_codigo_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_CODIGO',prop:'SortedStatus'},{av:'Ddo_contagemitem_lancamento_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_LANCAMENTO',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_comentario_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_COMENTARIO',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_data_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_DATA',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_usuariopessoacod_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_usuariopessoanom_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD.ONOPTIONCLICKED","{handler:'E185V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemItemParecer_Comentario1',fld:'vCONTAGEMITEMPARECER_COMENTARIO1',pic:'',nv:''},{av:'AV19ContagemItemParecer_UsuarioPessoaNom1',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemItemParecer_Comentario2',fld:'vCONTAGEMITEMPARECER_COMENTARIO2',pic:'',nv:''},{av:'AV24ContagemItemParecer_UsuarioPessoaNom2',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemItemParecer_Comentario3',fld:'vCONTAGEMITEMPARECER_COMENTARIO3',pic:'',nv:''},{av:'AV29ContagemItemParecer_UsuarioPessoaNom3',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58TFContagemItemParecer_Codigo',fld:'vTFCONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFContagemItemParecer_Codigo_To',fld:'vTFCONTAGEMITEMPARECER_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV62TFContagemItem_Lancamento',fld:'vTFCONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'AV63TFContagemItem_Lancamento_To',fld:'vTFCONTAGEMITEM_LANCAMENTO_TO',pic:'ZZZZZ9',nv:0},{av:'AV66TFContagemItemParecer_Comentario',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO',pic:'',nv:''},{av:'AV67TFContagemItemParecer_Comentario_Sel',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO_SEL',pic:'',nv:''},{av:'AV70TFContagemItemParecer_Data',fld:'vTFCONTAGEMITEMPARECER_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContagemItemParecer_Data_To',fld:'vTFCONTAGEMITEMPARECER_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV76TFContagemItemParecer_UsuarioCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV77TFContagemItemParecer_UsuarioCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV80TFContagemItemParecer_UsuarioPessoaCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV81TFContagemItemParecer_UsuarioPessoaCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV84TFContagemItemParecer_UsuarioPessoaNom',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV85TFContagemItemParecer_UsuarioPessoaNom_Sel',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV92ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_LANCAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_COMENTARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV131Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A243ContagemItemParecer_Codigo',fld:'CONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A246ContagemItemParecer_UsuarioCod',fld:'CONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contagemitemparecer_usuariopessoacod_Activeeventkey',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD',prop:'ActiveEventKey'},{av:'Ddo_contagemitemparecer_usuariopessoacod_Filteredtext_get',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD',prop:'FilteredText_get'},{av:'Ddo_contagemitemparecer_usuariopessoacod_Filteredtextto_get',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemitemparecer_usuariopessoacod_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD',prop:'SortedStatus'},{av:'AV80TFContagemItemParecer_UsuarioPessoaCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV81TFContagemItemParecer_UsuarioPessoaCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemitemparecer_codigo_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_CODIGO',prop:'SortedStatus'},{av:'Ddo_contagemitem_lancamento_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_LANCAMENTO',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_comentario_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_COMENTARIO',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_data_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_DATA',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_usuariocod_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOCOD',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_usuariopessoanom_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM.ONOPTIONCLICKED","{handler:'E195V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemItemParecer_Comentario1',fld:'vCONTAGEMITEMPARECER_COMENTARIO1',pic:'',nv:''},{av:'AV19ContagemItemParecer_UsuarioPessoaNom1',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemItemParecer_Comentario2',fld:'vCONTAGEMITEMPARECER_COMENTARIO2',pic:'',nv:''},{av:'AV24ContagemItemParecer_UsuarioPessoaNom2',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemItemParecer_Comentario3',fld:'vCONTAGEMITEMPARECER_COMENTARIO3',pic:'',nv:''},{av:'AV29ContagemItemParecer_UsuarioPessoaNom3',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58TFContagemItemParecer_Codigo',fld:'vTFCONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFContagemItemParecer_Codigo_To',fld:'vTFCONTAGEMITEMPARECER_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV62TFContagemItem_Lancamento',fld:'vTFCONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'AV63TFContagemItem_Lancamento_To',fld:'vTFCONTAGEMITEM_LANCAMENTO_TO',pic:'ZZZZZ9',nv:0},{av:'AV66TFContagemItemParecer_Comentario',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO',pic:'',nv:''},{av:'AV67TFContagemItemParecer_Comentario_Sel',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO_SEL',pic:'',nv:''},{av:'AV70TFContagemItemParecer_Data',fld:'vTFCONTAGEMITEMPARECER_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContagemItemParecer_Data_To',fld:'vTFCONTAGEMITEMPARECER_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV76TFContagemItemParecer_UsuarioCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV77TFContagemItemParecer_UsuarioCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV80TFContagemItemParecer_UsuarioPessoaCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV81TFContagemItemParecer_UsuarioPessoaCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV84TFContagemItemParecer_UsuarioPessoaNom',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV85TFContagemItemParecer_UsuarioPessoaNom_Sel',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV92ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_LANCAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_COMENTARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV131Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A243ContagemItemParecer_Codigo',fld:'CONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A246ContagemItemParecer_UsuarioCod',fld:'CONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contagemitemparecer_usuariopessoanom_Activeeventkey',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM',prop:'ActiveEventKey'},{av:'Ddo_contagemitemparecer_usuariopessoanom_Filteredtext_get',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM',prop:'FilteredText_get'},{av:'Ddo_contagemitemparecer_usuariopessoanom_Selectedvalue_get',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemitemparecer_usuariopessoanom_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM',prop:'SortedStatus'},{av:'AV84TFContagemItemParecer_UsuarioPessoaNom',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV85TFContagemItemParecer_UsuarioPessoaNom_Sel',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_contagemitemparecer_codigo_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_CODIGO',prop:'SortedStatus'},{av:'Ddo_contagemitem_lancamento_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_LANCAMENTO',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_comentario_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_COMENTARIO',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_data_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_DATA',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_usuariocod_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOCOD',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_usuariopessoacod_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E325V2',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A243ContagemItemParecer_Codigo',fld:'CONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A246ContagemItemParecer_UsuarioCod',fld:'CONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV52Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV53Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV91Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Enabled',ctrl:'vDISPLAY',prop:'Enabled'},{av:'edtContagemItemParecer_Comentario_Link',ctrl:'CONTAGEMITEMPARECER_COMENTARIO',prop:'Link'},{av:'edtContagemItemParecer_UsuarioPessoaNom_Link',ctrl:'CONTAGEMITEMPARECER_USUARIOPESSOANOM',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E205V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemItemParecer_Comentario1',fld:'vCONTAGEMITEMPARECER_COMENTARIO1',pic:'',nv:''},{av:'AV19ContagemItemParecer_UsuarioPessoaNom1',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemItemParecer_Comentario2',fld:'vCONTAGEMITEMPARECER_COMENTARIO2',pic:'',nv:''},{av:'AV24ContagemItemParecer_UsuarioPessoaNom2',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemItemParecer_Comentario3',fld:'vCONTAGEMITEMPARECER_COMENTARIO3',pic:'',nv:''},{av:'AV29ContagemItemParecer_UsuarioPessoaNom3',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58TFContagemItemParecer_Codigo',fld:'vTFCONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFContagemItemParecer_Codigo_To',fld:'vTFCONTAGEMITEMPARECER_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV62TFContagemItem_Lancamento',fld:'vTFCONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'AV63TFContagemItem_Lancamento_To',fld:'vTFCONTAGEMITEM_LANCAMENTO_TO',pic:'ZZZZZ9',nv:0},{av:'AV66TFContagemItemParecer_Comentario',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO',pic:'',nv:''},{av:'AV67TFContagemItemParecer_Comentario_Sel',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO_SEL',pic:'',nv:''},{av:'AV70TFContagemItemParecer_Data',fld:'vTFCONTAGEMITEMPARECER_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContagemItemParecer_Data_To',fld:'vTFCONTAGEMITEMPARECER_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV76TFContagemItemParecer_UsuarioCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV77TFContagemItemParecer_UsuarioCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV80TFContagemItemParecer_UsuarioPessoaCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV81TFContagemItemParecer_UsuarioPessoaCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV84TFContagemItemParecer_UsuarioPessoaNom',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV85TFContagemItemParecer_UsuarioPessoaNom_Sel',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV92ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_LANCAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_COMENTARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV131Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A243ContagemItemParecer_Codigo',fld:'CONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A246ContagemItemParecer_UsuarioCod',fld:'CONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E255V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemItemParecer_Comentario1',fld:'vCONTAGEMITEMPARECER_COMENTARIO1',pic:'',nv:''},{av:'AV19ContagemItemParecer_UsuarioPessoaNom1',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemItemParecer_Comentario2',fld:'vCONTAGEMITEMPARECER_COMENTARIO2',pic:'',nv:''},{av:'AV24ContagemItemParecer_UsuarioPessoaNom2',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemItemParecer_Comentario3',fld:'vCONTAGEMITEMPARECER_COMENTARIO3',pic:'',nv:''},{av:'AV29ContagemItemParecer_UsuarioPessoaNom3',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58TFContagemItemParecer_Codigo',fld:'vTFCONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFContagemItemParecer_Codigo_To',fld:'vTFCONTAGEMITEMPARECER_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV62TFContagemItem_Lancamento',fld:'vTFCONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'AV63TFContagemItem_Lancamento_To',fld:'vTFCONTAGEMITEM_LANCAMENTO_TO',pic:'ZZZZZ9',nv:0},{av:'AV66TFContagemItemParecer_Comentario',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO',pic:'',nv:''},{av:'AV67TFContagemItemParecer_Comentario_Sel',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO_SEL',pic:'',nv:''},{av:'AV70TFContagemItemParecer_Data',fld:'vTFCONTAGEMITEMPARECER_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContagemItemParecer_Data_To',fld:'vTFCONTAGEMITEMPARECER_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV76TFContagemItemParecer_UsuarioCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV77TFContagemItemParecer_UsuarioCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV80TFContagemItemParecer_UsuarioPessoaCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV81TFContagemItemParecer_UsuarioPessoaCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV84TFContagemItemParecer_UsuarioPessoaNom',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV85TFContagemItemParecer_UsuarioPessoaNom_Sel',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV92ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_LANCAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_COMENTARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV131Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A243ContagemItemParecer_Codigo',fld:'CONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A246ContagemItemParecer_UsuarioCod',fld:'CONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E215V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemItemParecer_Comentario1',fld:'vCONTAGEMITEMPARECER_COMENTARIO1',pic:'',nv:''},{av:'AV19ContagemItemParecer_UsuarioPessoaNom1',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemItemParecer_Comentario2',fld:'vCONTAGEMITEMPARECER_COMENTARIO2',pic:'',nv:''},{av:'AV24ContagemItemParecer_UsuarioPessoaNom2',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemItemParecer_Comentario3',fld:'vCONTAGEMITEMPARECER_COMENTARIO3',pic:'',nv:''},{av:'AV29ContagemItemParecer_UsuarioPessoaNom3',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58TFContagemItemParecer_Codigo',fld:'vTFCONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFContagemItemParecer_Codigo_To',fld:'vTFCONTAGEMITEMPARECER_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV62TFContagemItem_Lancamento',fld:'vTFCONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'AV63TFContagemItem_Lancamento_To',fld:'vTFCONTAGEMITEM_LANCAMENTO_TO',pic:'ZZZZZ9',nv:0},{av:'AV66TFContagemItemParecer_Comentario',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO',pic:'',nv:''},{av:'AV67TFContagemItemParecer_Comentario_Sel',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO_SEL',pic:'',nv:''},{av:'AV70TFContagemItemParecer_Data',fld:'vTFCONTAGEMITEMPARECER_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContagemItemParecer_Data_To',fld:'vTFCONTAGEMITEMPARECER_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV76TFContagemItemParecer_UsuarioCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV77TFContagemItemParecer_UsuarioCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV80TFContagemItemParecer_UsuarioPessoaCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV81TFContagemItemParecer_UsuarioPessoaCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV84TFContagemItemParecer_UsuarioPessoaNom',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV85TFContagemItemParecer_UsuarioPessoaNom_Sel',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV92ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_LANCAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_COMENTARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV131Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A243ContagemItemParecer_Codigo',fld:'CONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A246ContagemItemParecer_UsuarioCod',fld:'CONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemItemParecer_Comentario2',fld:'vCONTAGEMITEMPARECER_COMENTARIO2',pic:'',nv:''},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemItemParecer_Comentario3',fld:'vCONTAGEMITEMPARECER_COMENTARIO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemItemParecer_Comentario1',fld:'vCONTAGEMITEMPARECER_COMENTARIO1',pic:'',nv:''},{av:'AV19ContagemItemParecer_UsuarioPessoaNom1',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24ContagemItemParecer_UsuarioPessoaNom2',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV29ContagemItemParecer_UsuarioPessoaNom3',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'edtavContagemitemparecer_comentario2_Visible',ctrl:'vCONTAGEMITEMPARECER_COMENTARIO2',prop:'Visible'},{av:'edtavContagemitemparecer_usuariopessoanom2_Visible',ctrl:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagemitemparecer_comentario3_Visible',ctrl:'vCONTAGEMITEMPARECER_COMENTARIO3',prop:'Visible'},{av:'edtavContagemitemparecer_usuariopessoanom3_Visible',ctrl:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContagemitemparecer_comentario1_Visible',ctrl:'vCONTAGEMITEMPARECER_COMENTARIO1',prop:'Visible'},{av:'edtavContagemitemparecer_usuariopessoanom1_Visible',ctrl:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E265V2',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavContagemitemparecer_comentario1_Visible',ctrl:'vCONTAGEMITEMPARECER_COMENTARIO1',prop:'Visible'},{av:'edtavContagemitemparecer_usuariopessoanom1_Visible',ctrl:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E275V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemItemParecer_Comentario1',fld:'vCONTAGEMITEMPARECER_COMENTARIO1',pic:'',nv:''},{av:'AV19ContagemItemParecer_UsuarioPessoaNom1',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemItemParecer_Comentario2',fld:'vCONTAGEMITEMPARECER_COMENTARIO2',pic:'',nv:''},{av:'AV24ContagemItemParecer_UsuarioPessoaNom2',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemItemParecer_Comentario3',fld:'vCONTAGEMITEMPARECER_COMENTARIO3',pic:'',nv:''},{av:'AV29ContagemItemParecer_UsuarioPessoaNom3',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58TFContagemItemParecer_Codigo',fld:'vTFCONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFContagemItemParecer_Codigo_To',fld:'vTFCONTAGEMITEMPARECER_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV62TFContagemItem_Lancamento',fld:'vTFCONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'AV63TFContagemItem_Lancamento_To',fld:'vTFCONTAGEMITEM_LANCAMENTO_TO',pic:'ZZZZZ9',nv:0},{av:'AV66TFContagemItemParecer_Comentario',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO',pic:'',nv:''},{av:'AV67TFContagemItemParecer_Comentario_Sel',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO_SEL',pic:'',nv:''},{av:'AV70TFContagemItemParecer_Data',fld:'vTFCONTAGEMITEMPARECER_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContagemItemParecer_Data_To',fld:'vTFCONTAGEMITEMPARECER_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV76TFContagemItemParecer_UsuarioCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV77TFContagemItemParecer_UsuarioCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV80TFContagemItemParecer_UsuarioPessoaCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV81TFContagemItemParecer_UsuarioPessoaCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV84TFContagemItemParecer_UsuarioPessoaNom',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV85TFContagemItemParecer_UsuarioPessoaNom_Sel',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV92ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_LANCAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_COMENTARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV131Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A243ContagemItemParecer_Codigo',fld:'CONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A246ContagemItemParecer_UsuarioCod',fld:'CONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E225V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemItemParecer_Comentario1',fld:'vCONTAGEMITEMPARECER_COMENTARIO1',pic:'',nv:''},{av:'AV19ContagemItemParecer_UsuarioPessoaNom1',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemItemParecer_Comentario2',fld:'vCONTAGEMITEMPARECER_COMENTARIO2',pic:'',nv:''},{av:'AV24ContagemItemParecer_UsuarioPessoaNom2',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemItemParecer_Comentario3',fld:'vCONTAGEMITEMPARECER_COMENTARIO3',pic:'',nv:''},{av:'AV29ContagemItemParecer_UsuarioPessoaNom3',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58TFContagemItemParecer_Codigo',fld:'vTFCONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFContagemItemParecer_Codigo_To',fld:'vTFCONTAGEMITEMPARECER_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV62TFContagemItem_Lancamento',fld:'vTFCONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'AV63TFContagemItem_Lancamento_To',fld:'vTFCONTAGEMITEM_LANCAMENTO_TO',pic:'ZZZZZ9',nv:0},{av:'AV66TFContagemItemParecer_Comentario',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO',pic:'',nv:''},{av:'AV67TFContagemItemParecer_Comentario_Sel',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO_SEL',pic:'',nv:''},{av:'AV70TFContagemItemParecer_Data',fld:'vTFCONTAGEMITEMPARECER_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContagemItemParecer_Data_To',fld:'vTFCONTAGEMITEMPARECER_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV76TFContagemItemParecer_UsuarioCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV77TFContagemItemParecer_UsuarioCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV80TFContagemItemParecer_UsuarioPessoaCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV81TFContagemItemParecer_UsuarioPessoaCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV84TFContagemItemParecer_UsuarioPessoaNom',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV85TFContagemItemParecer_UsuarioPessoaNom_Sel',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV92ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_LANCAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_COMENTARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV131Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A243ContagemItemParecer_Codigo',fld:'CONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A246ContagemItemParecer_UsuarioCod',fld:'CONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemItemParecer_Comentario2',fld:'vCONTAGEMITEMPARECER_COMENTARIO2',pic:'',nv:''},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemItemParecer_Comentario3',fld:'vCONTAGEMITEMPARECER_COMENTARIO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemItemParecer_Comentario1',fld:'vCONTAGEMITEMPARECER_COMENTARIO1',pic:'',nv:''},{av:'AV19ContagemItemParecer_UsuarioPessoaNom1',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24ContagemItemParecer_UsuarioPessoaNom2',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV29ContagemItemParecer_UsuarioPessoaNom3',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'edtavContagemitemparecer_comentario2_Visible',ctrl:'vCONTAGEMITEMPARECER_COMENTARIO2',prop:'Visible'},{av:'edtavContagemitemparecer_usuariopessoanom2_Visible',ctrl:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagemitemparecer_comentario3_Visible',ctrl:'vCONTAGEMITEMPARECER_COMENTARIO3',prop:'Visible'},{av:'edtavContagemitemparecer_usuariopessoanom3_Visible',ctrl:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContagemitemparecer_comentario1_Visible',ctrl:'vCONTAGEMITEMPARECER_COMENTARIO1',prop:'Visible'},{av:'edtavContagemitemparecer_usuariopessoanom1_Visible',ctrl:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E285V2',iparms:[{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavContagemitemparecer_comentario2_Visible',ctrl:'vCONTAGEMITEMPARECER_COMENTARIO2',prop:'Visible'},{av:'edtavContagemitemparecer_usuariopessoanom2_Visible',ctrl:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E235V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemItemParecer_Comentario1',fld:'vCONTAGEMITEMPARECER_COMENTARIO1',pic:'',nv:''},{av:'AV19ContagemItemParecer_UsuarioPessoaNom1',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemItemParecer_Comentario2',fld:'vCONTAGEMITEMPARECER_COMENTARIO2',pic:'',nv:''},{av:'AV24ContagemItemParecer_UsuarioPessoaNom2',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemItemParecer_Comentario3',fld:'vCONTAGEMITEMPARECER_COMENTARIO3',pic:'',nv:''},{av:'AV29ContagemItemParecer_UsuarioPessoaNom3',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58TFContagemItemParecer_Codigo',fld:'vTFCONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFContagemItemParecer_Codigo_To',fld:'vTFCONTAGEMITEMPARECER_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV62TFContagemItem_Lancamento',fld:'vTFCONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'AV63TFContagemItem_Lancamento_To',fld:'vTFCONTAGEMITEM_LANCAMENTO_TO',pic:'ZZZZZ9',nv:0},{av:'AV66TFContagemItemParecer_Comentario',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO',pic:'',nv:''},{av:'AV67TFContagemItemParecer_Comentario_Sel',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO_SEL',pic:'',nv:''},{av:'AV70TFContagemItemParecer_Data',fld:'vTFCONTAGEMITEMPARECER_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContagemItemParecer_Data_To',fld:'vTFCONTAGEMITEMPARECER_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV76TFContagemItemParecer_UsuarioCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV77TFContagemItemParecer_UsuarioCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV80TFContagemItemParecer_UsuarioPessoaCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV81TFContagemItemParecer_UsuarioPessoaCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV84TFContagemItemParecer_UsuarioPessoaNom',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV85TFContagemItemParecer_UsuarioPessoaNom_Sel',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV92ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_LANCAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_COMENTARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV131Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A243ContagemItemParecer_Codigo',fld:'CONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A246ContagemItemParecer_UsuarioCod',fld:'CONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemItemParecer_Comentario2',fld:'vCONTAGEMITEMPARECER_COMENTARIO2',pic:'',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemItemParecer_Comentario3',fld:'vCONTAGEMITEMPARECER_COMENTARIO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemItemParecer_Comentario1',fld:'vCONTAGEMITEMPARECER_COMENTARIO1',pic:'',nv:''},{av:'AV19ContagemItemParecer_UsuarioPessoaNom1',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24ContagemItemParecer_UsuarioPessoaNom2',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV29ContagemItemParecer_UsuarioPessoaNom3',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'edtavContagemitemparecer_comentario2_Visible',ctrl:'vCONTAGEMITEMPARECER_COMENTARIO2',prop:'Visible'},{av:'edtavContagemitemparecer_usuariopessoanom2_Visible',ctrl:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagemitemparecer_comentario3_Visible',ctrl:'vCONTAGEMITEMPARECER_COMENTARIO3',prop:'Visible'},{av:'edtavContagemitemparecer_usuariopessoanom3_Visible',ctrl:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContagemitemparecer_comentario1_Visible',ctrl:'vCONTAGEMITEMPARECER_COMENTARIO1',prop:'Visible'},{av:'edtavContagemitemparecer_usuariopessoanom1_Visible',ctrl:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E295V2',iparms:[{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtavContagemitemparecer_comentario3_Visible',ctrl:'vCONTAGEMITEMPARECER_COMENTARIO3',prop:'Visible'},{av:'edtavContagemitemparecer_usuariopessoanom3_Visible',ctrl:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("DDO_MANAGEFILTERS.ONOPTIONCLICKED","{handler:'E115V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemItemParecer_Comentario1',fld:'vCONTAGEMITEMPARECER_COMENTARIO1',pic:'',nv:''},{av:'AV19ContagemItemParecer_UsuarioPessoaNom1',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemItemParecer_Comentario2',fld:'vCONTAGEMITEMPARECER_COMENTARIO2',pic:'',nv:''},{av:'AV24ContagemItemParecer_UsuarioPessoaNom2',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemItemParecer_Comentario3',fld:'vCONTAGEMITEMPARECER_COMENTARIO3',pic:'',nv:''},{av:'AV29ContagemItemParecer_UsuarioPessoaNom3',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58TFContagemItemParecer_Codigo',fld:'vTFCONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFContagemItemParecer_Codigo_To',fld:'vTFCONTAGEMITEMPARECER_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV62TFContagemItem_Lancamento',fld:'vTFCONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'AV63TFContagemItem_Lancamento_To',fld:'vTFCONTAGEMITEM_LANCAMENTO_TO',pic:'ZZZZZ9',nv:0},{av:'AV66TFContagemItemParecer_Comentario',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO',pic:'',nv:''},{av:'AV67TFContagemItemParecer_Comentario_Sel',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO_SEL',pic:'',nv:''},{av:'AV70TFContagemItemParecer_Data',fld:'vTFCONTAGEMITEMPARECER_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContagemItemParecer_Data_To',fld:'vTFCONTAGEMITEMPARECER_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV76TFContagemItemParecer_UsuarioCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV77TFContagemItemParecer_UsuarioCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV80TFContagemItemParecer_UsuarioPessoaCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV81TFContagemItemParecer_UsuarioPessoaCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV84TFContagemItemParecer_UsuarioPessoaNom',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV85TFContagemItemParecer_UsuarioPessoaNom_Sel',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV92ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_LANCAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_COMENTARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEMPARECER_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV131Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A243ContagemItemParecer_Codigo',fld:'CONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A246ContagemItemParecer_UsuarioCod',fld:'CONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_managefilters_Activeeventkey',ctrl:'DDO_MANAGEFILTERS',prop:'ActiveEventKey'}],oparms:[{av:'AV92ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV58TFContagemItemParecer_Codigo',fld:'vTFCONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemitemparecer_codigo_Filteredtext_set',ctrl:'DDO_CONTAGEMITEMPARECER_CODIGO',prop:'FilteredText_set'},{av:'AV59TFContagemItemParecer_Codigo_To',fld:'vTFCONTAGEMITEMPARECER_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemitemparecer_codigo_Filteredtextto_set',ctrl:'DDO_CONTAGEMITEMPARECER_CODIGO',prop:'FilteredTextTo_set'},{av:'AV62TFContagemItem_Lancamento',fld:'vTFCONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemitem_lancamento_Filteredtext_set',ctrl:'DDO_CONTAGEMITEM_LANCAMENTO',prop:'FilteredText_set'},{av:'AV63TFContagemItem_Lancamento_To',fld:'vTFCONTAGEMITEM_LANCAMENTO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemitem_lancamento_Filteredtextto_set',ctrl:'DDO_CONTAGEMITEM_LANCAMENTO',prop:'FilteredTextTo_set'},{av:'AV66TFContagemItemParecer_Comentario',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO',pic:'',nv:''},{av:'Ddo_contagemitemparecer_comentario_Filteredtext_set',ctrl:'DDO_CONTAGEMITEMPARECER_COMENTARIO',prop:'FilteredText_set'},{av:'AV67TFContagemItemParecer_Comentario_Sel',fld:'vTFCONTAGEMITEMPARECER_COMENTARIO_SEL',pic:'',nv:''},{av:'Ddo_contagemitemparecer_comentario_Selectedvalue_set',ctrl:'DDO_CONTAGEMITEMPARECER_COMENTARIO',prop:'SelectedValue_set'},{av:'AV70TFContagemItemParecer_Data',fld:'vTFCONTAGEMITEMPARECER_DATA',pic:'99/99/99 99:99',nv:''},{av:'Ddo_contagemitemparecer_data_Filteredtext_set',ctrl:'DDO_CONTAGEMITEMPARECER_DATA',prop:'FilteredText_set'},{av:'AV71TFContagemItemParecer_Data_To',fld:'vTFCONTAGEMITEMPARECER_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_contagemitemparecer_data_Filteredtextto_set',ctrl:'DDO_CONTAGEMITEMPARECER_DATA',prop:'FilteredTextTo_set'},{av:'AV76TFContagemItemParecer_UsuarioCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemitemparecer_usuariocod_Filteredtext_set',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOCOD',prop:'FilteredText_set'},{av:'AV77TFContagemItemParecer_UsuarioCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemitemparecer_usuariocod_Filteredtextto_set',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOCOD',prop:'FilteredTextTo_set'},{av:'AV80TFContagemItemParecer_UsuarioPessoaCod',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemitemparecer_usuariopessoacod_Filteredtext_set',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD',prop:'FilteredText_set'},{av:'AV81TFContagemItemParecer_UsuarioPessoaCod_To',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemitemparecer_usuariopessoacod_Filteredtextto_set',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD',prop:'FilteredTextTo_set'},{av:'AV84TFContagemItemParecer_UsuarioPessoaNom',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'Ddo_contagemitemparecer_usuariopessoanom_Filteredtext_set',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM',prop:'FilteredText_set'},{av:'AV85TFContagemItemParecer_UsuarioPessoaNom_Sel',fld:'vTFCONTAGEMITEMPARECER_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_contagemitemparecer_usuariopessoanom_Selectedvalue_set',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM',prop:'SelectedValue_set'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemItemParecer_Comentario1',fld:'vCONTAGEMITEMPARECER_COMENTARIO1',pic:'',nv:''},{av:'Ddo_contagemitemparecer_usuariopessoanom_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_usuariopessoacod_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOPESSOACOD',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_usuariocod_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_USUARIOCOD',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_data_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_DATA',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_comentario_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_COMENTARIO',prop:'SortedStatus'},{av:'Ddo_contagemitem_lancamento_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_LANCAMENTO',prop:'SortedStatus'},{av:'Ddo_contagemitemparecer_codigo_Sortedstatus',ctrl:'DDO_CONTAGEMITEMPARECER_CODIGO',prop:'SortedStatus'},{av:'AV72DDO_ContagemItemParecer_DataAuxDate',fld:'vDDO_CONTAGEMITEMPARECER_DATAAUXDATE',pic:'',nv:''},{av:'AV73DDO_ContagemItemParecer_DataAuxDateTo',fld:'vDDO_CONTAGEMITEMPARECER_DATAAUXDATETO',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV19ContagemItemParecer_UsuarioPessoaNom1',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemItemParecer_Comentario2',fld:'vCONTAGEMITEMPARECER_COMENTARIO2',pic:'',nv:''},{av:'AV24ContagemItemParecer_UsuarioPessoaNom2',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemItemParecer_Comentario3',fld:'vCONTAGEMITEMPARECER_COMENTARIO3',pic:'',nv:''},{av:'AV29ContagemItemParecer_UsuarioPessoaNom3',fld:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'edtavContagemitemparecer_comentario1_Visible',ctrl:'vCONTAGEMITEMPARECER_COMENTARIO1',prop:'Visible'},{av:'edtavContagemitemparecer_usuariopessoanom1_Visible',ctrl:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'edtavContagemitemparecer_comentario2_Visible',ctrl:'vCONTAGEMITEMPARECER_COMENTARIO2',prop:'Visible'},{av:'edtavContagemitemparecer_usuariopessoanom2_Visible',ctrl:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagemitemparecer_comentario3_Visible',ctrl:'vCONTAGEMITEMPARECER_COMENTARIO3',prop:'Visible'},{av:'edtavContagemitemparecer_usuariopessoanom3_Visible',ctrl:'vCONTAGEMITEMPARECER_USUARIOPESSOANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E245V2',iparms:[{av:'A243ContagemItemParecer_Codigo',fld:'CONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contagemitemparecer_codigo_Activeeventkey = "";
         Ddo_contagemitemparecer_codigo_Filteredtext_get = "";
         Ddo_contagemitemparecer_codigo_Filteredtextto_get = "";
         Ddo_contagemitem_lancamento_Activeeventkey = "";
         Ddo_contagemitem_lancamento_Filteredtext_get = "";
         Ddo_contagemitem_lancamento_Filteredtextto_get = "";
         Ddo_contagemitemparecer_comentario_Activeeventkey = "";
         Ddo_contagemitemparecer_comentario_Filteredtext_get = "";
         Ddo_contagemitemparecer_comentario_Selectedvalue_get = "";
         Ddo_contagemitemparecer_data_Activeeventkey = "";
         Ddo_contagemitemparecer_data_Filteredtext_get = "";
         Ddo_contagemitemparecer_data_Filteredtextto_get = "";
         Ddo_contagemitemparecer_usuariocod_Activeeventkey = "";
         Ddo_contagemitemparecer_usuariocod_Filteredtext_get = "";
         Ddo_contagemitemparecer_usuariocod_Filteredtextto_get = "";
         Ddo_contagemitemparecer_usuariopessoacod_Activeeventkey = "";
         Ddo_contagemitemparecer_usuariopessoacod_Filteredtext_get = "";
         Ddo_contagemitemparecer_usuariopessoacod_Filteredtextto_get = "";
         Ddo_contagemitemparecer_usuariopessoanom_Activeeventkey = "";
         Ddo_contagemitemparecer_usuariopessoanom_Filteredtext_get = "";
         Ddo_contagemitemparecer_usuariopessoanom_Selectedvalue_get = "";
         Ddo_managefilters_Activeeventkey = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV16DynamicFiltersSelector1 = "";
         AV18ContagemItemParecer_Comentario1 = "";
         AV19ContagemItemParecer_UsuarioPessoaNom1 = "";
         AV21DynamicFiltersSelector2 = "";
         AV23ContagemItemParecer_Comentario2 = "";
         AV24ContagemItemParecer_UsuarioPessoaNom2 = "";
         AV26DynamicFiltersSelector3 = "";
         AV28ContagemItemParecer_Comentario3 = "";
         AV29ContagemItemParecer_UsuarioPessoaNom3 = "";
         AV66TFContagemItemParecer_Comentario = "";
         AV67TFContagemItemParecer_Comentario_Sel = "";
         AV70TFContagemItemParecer_Data = (DateTime)(DateTime.MinValue);
         AV71TFContagemItemParecer_Data_To = (DateTime)(DateTime.MinValue);
         AV84TFContagemItemParecer_UsuarioPessoaNom = "";
         AV85TFContagemItemParecer_UsuarioPessoaNom_Sel = "";
         AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace = "";
         AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace = "";
         AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace = "";
         AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace = "";
         AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace = "";
         AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace = "";
         AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV131Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV96ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV87DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV57ContagemItemParecer_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV61ContagemItem_LancamentoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV65ContagemItemParecer_ComentarioTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV69ContagemItemParecer_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV75ContagemItemParecer_UsuarioCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV79ContagemItemParecer_UsuarioPessoaCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV83ContagemItemParecer_UsuarioPessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_managefilters_Icon = "";
         Ddo_managefilters_Caption = "";
         Ddo_contagemitemparecer_codigo_Filteredtext_set = "";
         Ddo_contagemitemparecer_codigo_Filteredtextto_set = "";
         Ddo_contagemitemparecer_codigo_Sortedstatus = "";
         Ddo_contagemitem_lancamento_Filteredtext_set = "";
         Ddo_contagemitem_lancamento_Filteredtextto_set = "";
         Ddo_contagemitem_lancamento_Sortedstatus = "";
         Ddo_contagemitemparecer_comentario_Filteredtext_set = "";
         Ddo_contagemitemparecer_comentario_Selectedvalue_set = "";
         Ddo_contagemitemparecer_comentario_Sortedstatus = "";
         Ddo_contagemitemparecer_data_Filteredtext_set = "";
         Ddo_contagemitemparecer_data_Filteredtextto_set = "";
         Ddo_contagemitemparecer_data_Sortedstatus = "";
         Ddo_contagemitemparecer_usuariocod_Filteredtext_set = "";
         Ddo_contagemitemparecer_usuariocod_Filteredtextto_set = "";
         Ddo_contagemitemparecer_usuariocod_Sortedstatus = "";
         Ddo_contagemitemparecer_usuariopessoacod_Filteredtext_set = "";
         Ddo_contagemitemparecer_usuariopessoacod_Filteredtextto_set = "";
         Ddo_contagemitemparecer_usuariopessoacod_Sortedstatus = "";
         Ddo_contagemitemparecer_usuariopessoanom_Filteredtext_set = "";
         Ddo_contagemitemparecer_usuariopessoanom_Selectedvalue_set = "";
         Ddo_contagemitemparecer_usuariopessoanom_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV72DDO_ContagemItemParecer_DataAuxDate = DateTime.MinValue;
         AV73DDO_ContagemItemParecer_DataAuxDateTo = DateTime.MinValue;
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV52Update = "";
         AV128Update_GXI = "";
         AV53Delete = "";
         AV129Delete_GXI = "";
         AV91Display = "";
         AV130Display_GXI = "";
         A244ContagemItemParecer_Comentario = "";
         A245ContagemItemParecer_Data = (DateTime)(DateTime.MinValue);
         A248ContagemItemParecer_UsuarioPessoaNom = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 = "";
         lV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 = "";
         lV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 = "";
         lV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 = "";
         lV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 = "";
         lV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 = "";
         lV118WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario = "";
         lV126WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom = "";
         AV100WWContagemItemParecerDS_1_Dynamicfiltersselector1 = "";
         AV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 = "";
         AV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 = "";
         AV105WWContagemItemParecerDS_6_Dynamicfiltersselector2 = "";
         AV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 = "";
         AV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 = "";
         AV110WWContagemItemParecerDS_11_Dynamicfiltersselector3 = "";
         AV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 = "";
         AV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 = "";
         AV119WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel = "";
         AV118WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario = "";
         AV120WWContagemItemParecerDS_21_Tfcontagemitemparecer_data = (DateTime)(DateTime.MinValue);
         AV121WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to = (DateTime)(DateTime.MinValue);
         AV127WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel = "";
         AV126WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom = "";
         H005V2_A248ContagemItemParecer_UsuarioPessoaNom = new String[] {""} ;
         H005V2_n248ContagemItemParecer_UsuarioPessoaNom = new bool[] {false} ;
         H005V2_A247ContagemItemParecer_UsuarioPessoaCod = new int[1] ;
         H005V2_n247ContagemItemParecer_UsuarioPessoaCod = new bool[] {false} ;
         H005V2_A246ContagemItemParecer_UsuarioCod = new int[1] ;
         H005V2_A245ContagemItemParecer_Data = new DateTime[] {DateTime.MinValue} ;
         H005V2_A244ContagemItemParecer_Comentario = new String[] {""} ;
         H005V2_A224ContagemItem_Lancamento = new int[1] ;
         H005V2_A243ContagemItemParecer_Codigo = new int[1] ;
         H005V3_AGRID_nRecordCount = new long[1] ;
         AV7HTTPRequest = new GxHttpRequest( context);
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         AV93ManageFiltersXml = "";
         GXt_char2 = "";
         imgInsert_Link = "";
         AV97ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV94ManageFiltersItems = new GxObjectCollection( context, "GridStateCollection.Item", "", "wwpbaseobjects.SdtGridStateCollection_Item", "GeneXus.Programs");
         AV95ManageFiltersItem = new wwpbaseobjects.SdtGridStateCollection_Item(context);
         AV51Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblContagemitemparecertitle_Jsonclick = "";
         imgInsert_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwcontagemitemparecer__default(),
            new Object[][] {
                new Object[] {
               H005V2_A248ContagemItemParecer_UsuarioPessoaNom, H005V2_n248ContagemItemParecer_UsuarioPessoaNom, H005V2_A247ContagemItemParecer_UsuarioPessoaCod, H005V2_n247ContagemItemParecer_UsuarioPessoaCod, H005V2_A246ContagemItemParecer_UsuarioCod, H005V2_A245ContagemItemParecer_Data, H005V2_A244ContagemItemParecer_Comentario, H005V2_A224ContagemItem_Lancamento, H005V2_A243ContagemItemParecer_Codigo
               }
               , new Object[] {
               H005V3_AGRID_nRecordCount
               }
            }
         );
         AV131Pgmname = "WWContagemItemParecer";
         /* GeneXus formulas. */
         AV131Pgmname = "WWContagemItemParecer";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_91 ;
      private short nGXsfl_91_idx=1 ;
      private short AV13OrderedBy ;
      private short AV17DynamicFiltersOperator1 ;
      private short AV22DynamicFiltersOperator2 ;
      private short AV27DynamicFiltersOperator3 ;
      private short AV92ManageFiltersExecutionStep ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_91_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV101WWContagemItemParecerDS_2_Dynamicfiltersoperator1 ;
      private short AV106WWContagemItemParecerDS_7_Dynamicfiltersoperator2 ;
      private short AV111WWContagemItemParecerDS_12_Dynamicfiltersoperator3 ;
      private short edtContagemItemParecer_Codigo_Titleformat ;
      private short edtContagemItem_Lancamento_Titleformat ;
      private short edtContagemItemParecer_Comentario_Titleformat ;
      private short edtContagemItemParecer_Data_Titleformat ;
      private short edtContagemItemParecer_UsuarioCod_Titleformat ;
      private short edtContagemItemParecer_UsuarioPessoaCod_Titleformat ;
      private short edtContagemItemParecer_UsuarioPessoaNom_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV58TFContagemItemParecer_Codigo ;
      private int AV59TFContagemItemParecer_Codigo_To ;
      private int AV62TFContagemItem_Lancamento ;
      private int AV63TFContagemItem_Lancamento_To ;
      private int AV76TFContagemItemParecer_UsuarioCod ;
      private int AV77TFContagemItemParecer_UsuarioCod_To ;
      private int AV80TFContagemItemParecer_UsuarioPessoaCod ;
      private int AV81TFContagemItemParecer_UsuarioPessoaCod_To ;
      private int A243ContagemItemParecer_Codigo ;
      private int A224ContagemItem_Lancamento ;
      private int A246ContagemItemParecer_UsuarioCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contagemitemparecer_comentario_Datalistupdateminimumcharacters ;
      private int Ddo_contagemitemparecer_usuariopessoanom_Datalistupdateminimumcharacters ;
      private int edtavManagefiltersexecutionstep_Visible ;
      private int edtavTfcontagemitemparecer_codigo_Visible ;
      private int edtavTfcontagemitemparecer_codigo_to_Visible ;
      private int edtavTfcontagemitem_lancamento_Visible ;
      private int edtavTfcontagemitem_lancamento_to_Visible ;
      private int edtavTfcontagemitemparecer_comentario_Visible ;
      private int edtavTfcontagemitemparecer_comentario_sel_Visible ;
      private int edtavTfcontagemitemparecer_data_Visible ;
      private int edtavTfcontagemitemparecer_data_to_Visible ;
      private int edtavTfcontagemitemparecer_usuariocod_Visible ;
      private int edtavTfcontagemitemparecer_usuariocod_to_Visible ;
      private int edtavTfcontagemitemparecer_usuariopessoacod_Visible ;
      private int edtavTfcontagemitemparecer_usuariopessoacod_to_Visible ;
      private int edtavTfcontagemitemparecer_usuariopessoanom_Visible ;
      private int edtavTfcontagemitemparecer_usuariopessoanom_sel_Visible ;
      private int edtavDdo_contagemitemparecer_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemitem_lancamentotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemitemparecer_comentariotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemitemparecer_datatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemitemparecer_usuariocodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemitemparecer_usuariopessoacodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemitemparecer_usuariopessoanomtitlecontrolidtoreplace_Visible ;
      private int A247ContagemItemParecer_UsuarioPessoaCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV114WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo ;
      private int AV115WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to ;
      private int AV116WWContagemItemParecerDS_17_Tfcontagemitem_lancamento ;
      private int AV117WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to ;
      private int AV122WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod ;
      private int AV123WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to ;
      private int AV124WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod ;
      private int AV125WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to ;
      private int edtavOrdereddsc_Visible ;
      private int AV88PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int edtavDisplay_Enabled ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int imgInsert_Enabled ;
      private int edtavContagemitemparecer_comentario1_Visible ;
      private int edtavContagemitemparecer_usuariopessoanom1_Visible ;
      private int edtavContagemitemparecer_comentario2_Visible ;
      private int edtavContagemitemparecer_usuariopessoanom2_Visible ;
      private int edtavContagemitemparecer_comentario3_Visible ;
      private int edtavContagemitemparecer_usuariopessoanom3_Visible ;
      private int AV132GXV1 ;
      private int AV133GXV2 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV89GridCurrentPage ;
      private long AV90GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contagemitemparecer_codigo_Activeeventkey ;
      private String Ddo_contagemitemparecer_codigo_Filteredtext_get ;
      private String Ddo_contagemitemparecer_codigo_Filteredtextto_get ;
      private String Ddo_contagemitem_lancamento_Activeeventkey ;
      private String Ddo_contagemitem_lancamento_Filteredtext_get ;
      private String Ddo_contagemitem_lancamento_Filteredtextto_get ;
      private String Ddo_contagemitemparecer_comentario_Activeeventkey ;
      private String Ddo_contagemitemparecer_comentario_Filteredtext_get ;
      private String Ddo_contagemitemparecer_comentario_Selectedvalue_get ;
      private String Ddo_contagemitemparecer_data_Activeeventkey ;
      private String Ddo_contagemitemparecer_data_Filteredtext_get ;
      private String Ddo_contagemitemparecer_data_Filteredtextto_get ;
      private String Ddo_contagemitemparecer_usuariocod_Activeeventkey ;
      private String Ddo_contagemitemparecer_usuariocod_Filteredtext_get ;
      private String Ddo_contagemitemparecer_usuariocod_Filteredtextto_get ;
      private String Ddo_contagemitemparecer_usuariopessoacod_Activeeventkey ;
      private String Ddo_contagemitemparecer_usuariopessoacod_Filteredtext_get ;
      private String Ddo_contagemitemparecer_usuariopessoacod_Filteredtextto_get ;
      private String Ddo_contagemitemparecer_usuariopessoanom_Activeeventkey ;
      private String Ddo_contagemitemparecer_usuariopessoanom_Filteredtext_get ;
      private String Ddo_contagemitemparecer_usuariopessoanom_Selectedvalue_get ;
      private String Ddo_managefilters_Activeeventkey ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_91_idx="0001" ;
      private String AV19ContagemItemParecer_UsuarioPessoaNom1 ;
      private String AV24ContagemItemParecer_UsuarioPessoaNom2 ;
      private String AV29ContagemItemParecer_UsuarioPessoaNom3 ;
      private String AV84TFContagemItemParecer_UsuarioPessoaNom ;
      private String AV85TFContagemItemParecer_UsuarioPessoaNom_Sel ;
      private String AV131Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_managefilters_Icon ;
      private String Ddo_managefilters_Caption ;
      private String Ddo_managefilters_Tooltip ;
      private String Ddo_managefilters_Cls ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contagemitemparecer_codigo_Caption ;
      private String Ddo_contagemitemparecer_codigo_Tooltip ;
      private String Ddo_contagemitemparecer_codigo_Cls ;
      private String Ddo_contagemitemparecer_codigo_Filteredtext_set ;
      private String Ddo_contagemitemparecer_codigo_Filteredtextto_set ;
      private String Ddo_contagemitemparecer_codigo_Dropdownoptionstype ;
      private String Ddo_contagemitemparecer_codigo_Titlecontrolidtoreplace ;
      private String Ddo_contagemitemparecer_codigo_Sortedstatus ;
      private String Ddo_contagemitemparecer_codigo_Filtertype ;
      private String Ddo_contagemitemparecer_codigo_Sortasc ;
      private String Ddo_contagemitemparecer_codigo_Sortdsc ;
      private String Ddo_contagemitemparecer_codigo_Cleanfilter ;
      private String Ddo_contagemitemparecer_codigo_Rangefilterfrom ;
      private String Ddo_contagemitemparecer_codigo_Rangefilterto ;
      private String Ddo_contagemitemparecer_codigo_Searchbuttontext ;
      private String Ddo_contagemitem_lancamento_Caption ;
      private String Ddo_contagemitem_lancamento_Tooltip ;
      private String Ddo_contagemitem_lancamento_Cls ;
      private String Ddo_contagemitem_lancamento_Filteredtext_set ;
      private String Ddo_contagemitem_lancamento_Filteredtextto_set ;
      private String Ddo_contagemitem_lancamento_Dropdownoptionstype ;
      private String Ddo_contagemitem_lancamento_Titlecontrolidtoreplace ;
      private String Ddo_contagemitem_lancamento_Sortedstatus ;
      private String Ddo_contagemitem_lancamento_Filtertype ;
      private String Ddo_contagemitem_lancamento_Sortasc ;
      private String Ddo_contagemitem_lancamento_Sortdsc ;
      private String Ddo_contagemitem_lancamento_Cleanfilter ;
      private String Ddo_contagemitem_lancamento_Rangefilterfrom ;
      private String Ddo_contagemitem_lancamento_Rangefilterto ;
      private String Ddo_contagemitem_lancamento_Searchbuttontext ;
      private String Ddo_contagemitemparecer_comentario_Caption ;
      private String Ddo_contagemitemparecer_comentario_Tooltip ;
      private String Ddo_contagemitemparecer_comentario_Cls ;
      private String Ddo_contagemitemparecer_comentario_Filteredtext_set ;
      private String Ddo_contagemitemparecer_comentario_Selectedvalue_set ;
      private String Ddo_contagemitemparecer_comentario_Dropdownoptionstype ;
      private String Ddo_contagemitemparecer_comentario_Titlecontrolidtoreplace ;
      private String Ddo_contagemitemparecer_comentario_Sortedstatus ;
      private String Ddo_contagemitemparecer_comentario_Filtertype ;
      private String Ddo_contagemitemparecer_comentario_Datalisttype ;
      private String Ddo_contagemitemparecer_comentario_Datalistproc ;
      private String Ddo_contagemitemparecer_comentario_Sortasc ;
      private String Ddo_contagemitemparecer_comentario_Sortdsc ;
      private String Ddo_contagemitemparecer_comentario_Loadingdata ;
      private String Ddo_contagemitemparecer_comentario_Cleanfilter ;
      private String Ddo_contagemitemparecer_comentario_Noresultsfound ;
      private String Ddo_contagemitemparecer_comentario_Searchbuttontext ;
      private String Ddo_contagemitemparecer_data_Caption ;
      private String Ddo_contagemitemparecer_data_Tooltip ;
      private String Ddo_contagemitemparecer_data_Cls ;
      private String Ddo_contagemitemparecer_data_Filteredtext_set ;
      private String Ddo_contagemitemparecer_data_Filteredtextto_set ;
      private String Ddo_contagemitemparecer_data_Dropdownoptionstype ;
      private String Ddo_contagemitemparecer_data_Titlecontrolidtoreplace ;
      private String Ddo_contagemitemparecer_data_Sortedstatus ;
      private String Ddo_contagemitemparecer_data_Filtertype ;
      private String Ddo_contagemitemparecer_data_Sortasc ;
      private String Ddo_contagemitemparecer_data_Sortdsc ;
      private String Ddo_contagemitemparecer_data_Cleanfilter ;
      private String Ddo_contagemitemparecer_data_Rangefilterfrom ;
      private String Ddo_contagemitemparecer_data_Rangefilterto ;
      private String Ddo_contagemitemparecer_data_Searchbuttontext ;
      private String Ddo_contagemitemparecer_usuariocod_Caption ;
      private String Ddo_contagemitemparecer_usuariocod_Tooltip ;
      private String Ddo_contagemitemparecer_usuariocod_Cls ;
      private String Ddo_contagemitemparecer_usuariocod_Filteredtext_set ;
      private String Ddo_contagemitemparecer_usuariocod_Filteredtextto_set ;
      private String Ddo_contagemitemparecer_usuariocod_Dropdownoptionstype ;
      private String Ddo_contagemitemparecer_usuariocod_Titlecontrolidtoreplace ;
      private String Ddo_contagemitemparecer_usuariocod_Sortedstatus ;
      private String Ddo_contagemitemparecer_usuariocod_Filtertype ;
      private String Ddo_contagemitemparecer_usuariocod_Sortasc ;
      private String Ddo_contagemitemparecer_usuariocod_Sortdsc ;
      private String Ddo_contagemitemparecer_usuariocod_Cleanfilter ;
      private String Ddo_contagemitemparecer_usuariocod_Rangefilterfrom ;
      private String Ddo_contagemitemparecer_usuariocod_Rangefilterto ;
      private String Ddo_contagemitemparecer_usuariocod_Searchbuttontext ;
      private String Ddo_contagemitemparecer_usuariopessoacod_Caption ;
      private String Ddo_contagemitemparecer_usuariopessoacod_Tooltip ;
      private String Ddo_contagemitemparecer_usuariopessoacod_Cls ;
      private String Ddo_contagemitemparecer_usuariopessoacod_Filteredtext_set ;
      private String Ddo_contagemitemparecer_usuariopessoacod_Filteredtextto_set ;
      private String Ddo_contagemitemparecer_usuariopessoacod_Dropdownoptionstype ;
      private String Ddo_contagemitemparecer_usuariopessoacod_Titlecontrolidtoreplace ;
      private String Ddo_contagemitemparecer_usuariopessoacod_Sortedstatus ;
      private String Ddo_contagemitemparecer_usuariopessoacod_Filtertype ;
      private String Ddo_contagemitemparecer_usuariopessoacod_Sortasc ;
      private String Ddo_contagemitemparecer_usuariopessoacod_Sortdsc ;
      private String Ddo_contagemitemparecer_usuariopessoacod_Cleanfilter ;
      private String Ddo_contagemitemparecer_usuariopessoacod_Rangefilterfrom ;
      private String Ddo_contagemitemparecer_usuariopessoacod_Rangefilterto ;
      private String Ddo_contagemitemparecer_usuariopessoacod_Searchbuttontext ;
      private String Ddo_contagemitemparecer_usuariopessoanom_Caption ;
      private String Ddo_contagemitemparecer_usuariopessoanom_Tooltip ;
      private String Ddo_contagemitemparecer_usuariopessoanom_Cls ;
      private String Ddo_contagemitemparecer_usuariopessoanom_Filteredtext_set ;
      private String Ddo_contagemitemparecer_usuariopessoanom_Selectedvalue_set ;
      private String Ddo_contagemitemparecer_usuariopessoanom_Dropdownoptionstype ;
      private String Ddo_contagemitemparecer_usuariopessoanom_Titlecontrolidtoreplace ;
      private String Ddo_contagemitemparecer_usuariopessoanom_Sortedstatus ;
      private String Ddo_contagemitemparecer_usuariopessoanom_Filtertype ;
      private String Ddo_contagemitemparecer_usuariopessoanom_Datalisttype ;
      private String Ddo_contagemitemparecer_usuariopessoanom_Datalistproc ;
      private String Ddo_contagemitemparecer_usuariopessoanom_Sortasc ;
      private String Ddo_contagemitemparecer_usuariopessoanom_Sortdsc ;
      private String Ddo_contagemitemparecer_usuariopessoanom_Loadingdata ;
      private String Ddo_contagemitemparecer_usuariopessoanom_Cleanfilter ;
      private String Ddo_contagemitemparecer_usuariopessoanom_Noresultsfound ;
      private String Ddo_contagemitemparecer_usuariopessoanom_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavManagefiltersexecutionstep_Internalname ;
      private String edtavManagefiltersexecutionstep_Jsonclick ;
      private String edtavTfcontagemitemparecer_codigo_Internalname ;
      private String edtavTfcontagemitemparecer_codigo_Jsonclick ;
      private String edtavTfcontagemitemparecer_codigo_to_Internalname ;
      private String edtavTfcontagemitemparecer_codigo_to_Jsonclick ;
      private String edtavTfcontagemitem_lancamento_Internalname ;
      private String edtavTfcontagemitem_lancamento_Jsonclick ;
      private String edtavTfcontagemitem_lancamento_to_Internalname ;
      private String edtavTfcontagemitem_lancamento_to_Jsonclick ;
      private String edtavTfcontagemitemparecer_comentario_Internalname ;
      private String edtavTfcontagemitemparecer_comentario_sel_Internalname ;
      private String edtavTfcontagemitemparecer_data_Internalname ;
      private String edtavTfcontagemitemparecer_data_Jsonclick ;
      private String edtavTfcontagemitemparecer_data_to_Internalname ;
      private String edtavTfcontagemitemparecer_data_to_Jsonclick ;
      private String divDdo_contagemitemparecer_dataauxdates_Internalname ;
      private String edtavDdo_contagemitemparecer_dataauxdate_Internalname ;
      private String edtavDdo_contagemitemparecer_dataauxdate_Jsonclick ;
      private String edtavDdo_contagemitemparecer_dataauxdateto_Internalname ;
      private String edtavDdo_contagemitemparecer_dataauxdateto_Jsonclick ;
      private String edtavTfcontagemitemparecer_usuariocod_Internalname ;
      private String edtavTfcontagemitemparecer_usuariocod_Jsonclick ;
      private String edtavTfcontagemitemparecer_usuariocod_to_Internalname ;
      private String edtavTfcontagemitemparecer_usuariocod_to_Jsonclick ;
      private String edtavTfcontagemitemparecer_usuariopessoacod_Internalname ;
      private String edtavTfcontagemitemparecer_usuariopessoacod_Jsonclick ;
      private String edtavTfcontagemitemparecer_usuariopessoacod_to_Internalname ;
      private String edtavTfcontagemitemparecer_usuariopessoacod_to_Jsonclick ;
      private String edtavTfcontagemitemparecer_usuariopessoanom_Internalname ;
      private String edtavTfcontagemitemparecer_usuariopessoanom_Jsonclick ;
      private String edtavTfcontagemitemparecer_usuariopessoanom_sel_Internalname ;
      private String edtavTfcontagemitemparecer_usuariopessoanom_sel_Jsonclick ;
      private String edtavDdo_contagemitemparecer_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemitem_lancamentotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemitemparecer_comentariotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemitemparecer_datatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemitemparecer_usuariocodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemitemparecer_usuariopessoacodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemitemparecer_usuariopessoanomtitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtContagemItemParecer_Codigo_Internalname ;
      private String edtContagemItem_Lancamento_Internalname ;
      private String edtContagemItemParecer_Comentario_Internalname ;
      private String edtContagemItemParecer_Data_Internalname ;
      private String edtContagemItemParecer_UsuarioCod_Internalname ;
      private String edtContagemItemParecer_UsuarioPessoaCod_Internalname ;
      private String A248ContagemItemParecer_UsuarioPessoaNom ;
      private String edtContagemItemParecer_UsuarioPessoaNom_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 ;
      private String lV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 ;
      private String lV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 ;
      private String lV126WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom ;
      private String AV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 ;
      private String AV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 ;
      private String AV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 ;
      private String AV127WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel ;
      private String AV126WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavContagemitemparecer_comentario1_Internalname ;
      private String edtavContagemitemparecer_usuariopessoanom1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavContagemitemparecer_comentario2_Internalname ;
      private String edtavContagemitemparecer_usuariopessoanom2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavContagemitemparecer_comentario3_Internalname ;
      private String edtavContagemitemparecer_usuariopessoanom3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contagemitemparecer_codigo_Internalname ;
      private String Ddo_contagemitem_lancamento_Internalname ;
      private String Ddo_contagemitemparecer_comentario_Internalname ;
      private String Ddo_contagemitemparecer_data_Internalname ;
      private String Ddo_contagemitemparecer_usuariocod_Internalname ;
      private String Ddo_contagemitemparecer_usuariopessoacod_Internalname ;
      private String Ddo_contagemitemparecer_usuariopessoanom_Internalname ;
      private String Ddo_managefilters_Internalname ;
      private String edtContagemItemParecer_Codigo_Title ;
      private String edtContagemItem_Lancamento_Title ;
      private String edtContagemItemParecer_Comentario_Title ;
      private String edtContagemItemParecer_Data_Title ;
      private String edtContagemItemParecer_UsuarioCod_Title ;
      private String edtContagemItemParecer_UsuarioPessoaCod_Title ;
      private String edtContagemItemParecer_UsuarioPessoaNom_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtContagemItemParecer_Comentario_Link ;
      private String edtContagemItemParecer_UsuarioPessoaNom_Link ;
      private String GXt_char2 ;
      private String imgInsert_Link ;
      private String imgInsert_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavContagemitemparecer_usuariopessoanom3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavContagemitemparecer_usuariopessoanom2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavContagemitemparecer_usuariopessoanom1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String lblContagemitemparecertitle_Internalname ;
      private String lblContagemitemparecertitle_Jsonclick ;
      private String imgInsert_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String sGXsfl_91_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContagemItemParecer_Codigo_Jsonclick ;
      private String edtContagemItem_Lancamento_Jsonclick ;
      private String edtContagemItemParecer_Comentario_Jsonclick ;
      private String edtContagemItemParecer_Data_Jsonclick ;
      private String edtContagemItemParecer_UsuarioCod_Jsonclick ;
      private String edtContagemItemParecer_UsuarioPessoaCod_Jsonclick ;
      private String edtContagemItemParecer_UsuarioPessoaNom_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV70TFContagemItemParecer_Data ;
      private DateTime AV71TFContagemItemParecer_Data_To ;
      private DateTime A245ContagemItemParecer_Data ;
      private DateTime AV120WWContagemItemParecerDS_21_Tfcontagemitemparecer_data ;
      private DateTime AV121WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to ;
      private DateTime AV72DDO_ContagemItemParecer_DataAuxDate ;
      private DateTime AV73DDO_ContagemItemParecer_DataAuxDateTo ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV20DynamicFiltersEnabled2 ;
      private bool AV25DynamicFiltersEnabled3 ;
      private bool AV31DynamicFiltersIgnoreFirst ;
      private bool AV30DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contagemitemparecer_codigo_Includesortasc ;
      private bool Ddo_contagemitemparecer_codigo_Includesortdsc ;
      private bool Ddo_contagemitemparecer_codigo_Includefilter ;
      private bool Ddo_contagemitemparecer_codigo_Filterisrange ;
      private bool Ddo_contagemitemparecer_codigo_Includedatalist ;
      private bool Ddo_contagemitem_lancamento_Includesortasc ;
      private bool Ddo_contagemitem_lancamento_Includesortdsc ;
      private bool Ddo_contagemitem_lancamento_Includefilter ;
      private bool Ddo_contagemitem_lancamento_Filterisrange ;
      private bool Ddo_contagemitem_lancamento_Includedatalist ;
      private bool Ddo_contagemitemparecer_comentario_Includesortasc ;
      private bool Ddo_contagemitemparecer_comentario_Includesortdsc ;
      private bool Ddo_contagemitemparecer_comentario_Includefilter ;
      private bool Ddo_contagemitemparecer_comentario_Filterisrange ;
      private bool Ddo_contagemitemparecer_comentario_Includedatalist ;
      private bool Ddo_contagemitemparecer_data_Includesortasc ;
      private bool Ddo_contagemitemparecer_data_Includesortdsc ;
      private bool Ddo_contagemitemparecer_data_Includefilter ;
      private bool Ddo_contagemitemparecer_data_Filterisrange ;
      private bool Ddo_contagemitemparecer_data_Includedatalist ;
      private bool Ddo_contagemitemparecer_usuariocod_Includesortasc ;
      private bool Ddo_contagemitemparecer_usuariocod_Includesortdsc ;
      private bool Ddo_contagemitemparecer_usuariocod_Includefilter ;
      private bool Ddo_contagemitemparecer_usuariocod_Filterisrange ;
      private bool Ddo_contagemitemparecer_usuariocod_Includedatalist ;
      private bool Ddo_contagemitemparecer_usuariopessoacod_Includesortasc ;
      private bool Ddo_contagemitemparecer_usuariopessoacod_Includesortdsc ;
      private bool Ddo_contagemitemparecer_usuariopessoacod_Includefilter ;
      private bool Ddo_contagemitemparecer_usuariopessoacod_Filterisrange ;
      private bool Ddo_contagemitemparecer_usuariopessoacod_Includedatalist ;
      private bool Ddo_contagemitemparecer_usuariopessoanom_Includesortasc ;
      private bool Ddo_contagemitemparecer_usuariopessoanom_Includesortdsc ;
      private bool Ddo_contagemitemparecer_usuariopessoanom_Includefilter ;
      private bool Ddo_contagemitemparecer_usuariopessoanom_Filterisrange ;
      private bool Ddo_contagemitemparecer_usuariopessoanom_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n247ContagemItemParecer_UsuarioPessoaCod ;
      private bool n248ContagemItemParecer_UsuarioPessoaNom ;
      private bool AV104WWContagemItemParecerDS_5_Dynamicfiltersenabled2 ;
      private bool AV109WWContagemItemParecerDS_10_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV52Update_IsBlob ;
      private bool AV53Delete_IsBlob ;
      private bool AV91Display_IsBlob ;
      private String AV18ContagemItemParecer_Comentario1 ;
      private String AV23ContagemItemParecer_Comentario2 ;
      private String AV28ContagemItemParecer_Comentario3 ;
      private String A244ContagemItemParecer_Comentario ;
      private String lV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 ;
      private String lV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 ;
      private String lV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 ;
      private String AV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 ;
      private String AV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 ;
      private String AV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 ;
      private String AV93ManageFiltersXml ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV21DynamicFiltersSelector2 ;
      private String AV26DynamicFiltersSelector3 ;
      private String AV66TFContagemItemParecer_Comentario ;
      private String AV67TFContagemItemParecer_Comentario_Sel ;
      private String AV60ddo_ContagemItemParecer_CodigoTitleControlIdToReplace ;
      private String AV64ddo_ContagemItem_LancamentoTitleControlIdToReplace ;
      private String AV68ddo_ContagemItemParecer_ComentarioTitleControlIdToReplace ;
      private String AV74ddo_ContagemItemParecer_DataTitleControlIdToReplace ;
      private String AV78ddo_ContagemItemParecer_UsuarioCodTitleControlIdToReplace ;
      private String AV82ddo_ContagemItemParecer_UsuarioPessoaCodTitleControlIdToReplace ;
      private String AV86ddo_ContagemItemParecer_UsuarioPessoaNomTitleControlIdToReplace ;
      private String AV128Update_GXI ;
      private String AV129Delete_GXI ;
      private String AV130Display_GXI ;
      private String lV118WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario ;
      private String AV100WWContagemItemParecerDS_1_Dynamicfiltersselector1 ;
      private String AV105WWContagemItemParecerDS_6_Dynamicfiltersselector2 ;
      private String AV110WWContagemItemParecerDS_11_Dynamicfiltersselector3 ;
      private String AV119WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel ;
      private String AV118WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario ;
      private String AV52Update ;
      private String AV53Delete ;
      private String AV91Display ;
      private String imgInsert_Bitmap ;
      private IGxSession AV51Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H005V2_A248ContagemItemParecer_UsuarioPessoaNom ;
      private bool[] H005V2_n248ContagemItemParecer_UsuarioPessoaNom ;
      private int[] H005V2_A247ContagemItemParecer_UsuarioPessoaCod ;
      private bool[] H005V2_n247ContagemItemParecer_UsuarioPessoaCod ;
      private int[] H005V2_A246ContagemItemParecer_UsuarioCod ;
      private DateTime[] H005V2_A245ContagemItemParecer_Data ;
      private String[] H005V2_A244ContagemItemParecer_Comentario ;
      private int[] H005V2_A224ContagemItem_Lancamento ;
      private int[] H005V2_A243ContagemItemParecer_Codigo ;
      private long[] H005V3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV96ManageFiltersData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV57ContagemItemParecer_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV61ContagemItem_LancamentoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV65ContagemItemParecer_ComentarioTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV69ContagemItemParecer_DataTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV75ContagemItemParecer_UsuarioCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV79ContagemItemParecer_UsuarioPessoaCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV83ContagemItemParecer_UsuarioPessoaNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtGridStateCollection_Item ))]
      private IGxCollection AV94ManageFiltersItems ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item AV97ManageFiltersDataItem ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV87DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
      private wwpbaseobjects.SdtGridStateCollection_Item AV95ManageFiltersItem ;
   }

   public class wwcontagemitemparecer__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H005V2( IGxContext context ,
                                             String AV100WWContagemItemParecerDS_1_Dynamicfiltersselector1 ,
                                             short AV101WWContagemItemParecerDS_2_Dynamicfiltersoperator1 ,
                                             String AV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 ,
                                             String AV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 ,
                                             bool AV104WWContagemItemParecerDS_5_Dynamicfiltersenabled2 ,
                                             String AV105WWContagemItemParecerDS_6_Dynamicfiltersselector2 ,
                                             short AV106WWContagemItemParecerDS_7_Dynamicfiltersoperator2 ,
                                             String AV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 ,
                                             String AV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 ,
                                             bool AV109WWContagemItemParecerDS_10_Dynamicfiltersenabled3 ,
                                             String AV110WWContagemItemParecerDS_11_Dynamicfiltersselector3 ,
                                             short AV111WWContagemItemParecerDS_12_Dynamicfiltersoperator3 ,
                                             String AV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 ,
                                             String AV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 ,
                                             int AV114WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo ,
                                             int AV115WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to ,
                                             int AV116WWContagemItemParecerDS_17_Tfcontagemitem_lancamento ,
                                             int AV117WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to ,
                                             String AV119WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel ,
                                             String AV118WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario ,
                                             DateTime AV120WWContagemItemParecerDS_21_Tfcontagemitemparecer_data ,
                                             DateTime AV121WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to ,
                                             int AV122WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod ,
                                             int AV123WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to ,
                                             int AV124WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod ,
                                             int AV125WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to ,
                                             String AV127WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel ,
                                             String AV126WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom ,
                                             String A244ContagemItemParecer_Comentario ,
                                             String A248ContagemItemParecer_UsuarioPessoaNom ,
                                             int A243ContagemItemParecer_Codigo ,
                                             int A224ContagemItem_Lancamento ,
                                             DateTime A245ContagemItemParecer_Data ,
                                             int A246ContagemItemParecer_UsuarioCod ,
                                             int A247ContagemItemParecer_UsuarioPessoaCod ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [31] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T3.[Pessoa_Nome] AS ContagemItemParecer_UsuarioPessoaNom, T2.[Usuario_PessoaCod] AS ContagemItemParecer_UsuarioPessoaCod, T1.[ContagemItemParecer_UsuarioCod] AS ContagemItemParecer_UsuarioCod, T1.[ContagemItemParecer_Data], T1.[ContagemItemParecer_Comentario], T1.[ContagemItem_Lancamento], T1.[ContagemItemParecer_Codigo]";
         sFromString = " FROM (([ContagemItemParecer] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemItemParecer_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV100WWContagemItemParecerDS_1_Dynamicfiltersselector1, "CONTAGEMITEMPARECER_COMENTARIO") == 0 ) && ( ( AV101WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 0 ) || ( AV101WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] like @lV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] like @lV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV100WWContagemItemParecerDS_1_Dynamicfiltersselector1, "CONTAGEMITEMPARECER_COMENTARIO") == 0 ) && ( ( AV101WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 1 ) || ( AV101WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] like '%' + @lV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] like '%' + @lV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV100WWContagemItemParecerDS_1_Dynamicfiltersselector1, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 ) && ( ( AV101WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 0 ) || ( AV101WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV100WWContagemItemParecerDS_1_Dynamicfiltersselector1, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 ) && ( ( AV101WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 1 ) || ( AV101WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV104WWContagemItemParecerDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV105WWContagemItemParecerDS_6_Dynamicfiltersselector2, "CONTAGEMITEMPARECER_COMENTARIO") == 0 ) && ( ( AV106WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 0 ) || ( AV106WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] like @lV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] like @lV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV104WWContagemItemParecerDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV105WWContagemItemParecerDS_6_Dynamicfiltersselector2, "CONTAGEMITEMPARECER_COMENTARIO") == 0 ) && ( ( AV106WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 1 ) || ( AV106WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] like '%' + @lV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] like '%' + @lV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV104WWContagemItemParecerDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV105WWContagemItemParecerDS_6_Dynamicfiltersselector2, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 ) && ( ( AV106WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 0 ) || ( AV106WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV104WWContagemItemParecerDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV105WWContagemItemParecerDS_6_Dynamicfiltersselector2, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 ) && ( ( AV106WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 1 ) || ( AV106WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV109WWContagemItemParecerDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV110WWContagemItemParecerDS_11_Dynamicfiltersselector3, "CONTAGEMITEMPARECER_COMENTARIO") == 0 ) && ( ( AV111WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 0 ) || ( AV111WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] like @lV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] like @lV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV109WWContagemItemParecerDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV110WWContagemItemParecerDS_11_Dynamicfiltersselector3, "CONTAGEMITEMPARECER_COMENTARIO") == 0 ) && ( ( AV111WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 1 ) || ( AV111WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] like '%' + @lV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] like '%' + @lV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV109WWContagemItemParecerDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV110WWContagemItemParecerDS_11_Dynamicfiltersselector3, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 ) && ( ( AV111WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 0 ) || ( AV111WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV109WWContagemItemParecerDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV110WWContagemItemParecerDS_11_Dynamicfiltersselector3, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 ) && ( ( AV111WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 1 ) || ( AV111WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! (0==AV114WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Codigo] >= @AV114WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Codigo] >= @AV114WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! (0==AV115WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Codigo] <= @AV115WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Codigo] <= @AV115WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! (0==AV116WWContagemItemParecerDS_17_Tfcontagemitem_lancamento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItem_Lancamento] >= @AV116WWContagemItemParecerDS_17_Tfcontagemitem_lancamento)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItem_Lancamento] >= @AV116WWContagemItemParecerDS_17_Tfcontagemitem_lancamento)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! (0==AV117WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItem_Lancamento] <= @AV117WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItem_Lancamento] <= @AV117WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV119WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] like @lV118WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] like @lV118WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV119WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] = @AV119WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] = @AV119WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! (DateTime.MinValue==AV120WWContagemItemParecerDS_21_Tfcontagemitemparecer_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Data] >= @AV120WWContagemItemParecerDS_21_Tfcontagemitemparecer_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Data] >= @AV120WWContagemItemParecerDS_21_Tfcontagemitemparecer_data)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! (DateTime.MinValue==AV121WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Data] <= @AV121WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Data] <= @AV121WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( ! (0==AV122WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_UsuarioCod] >= @AV122WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_UsuarioCod] >= @AV122WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ! (0==AV123WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_UsuarioCod] <= @AV123WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_UsuarioCod] <= @AV123WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( ! (0==AV124WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] >= @AV124WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_PessoaCod] >= @AV124WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod)";
            }
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( ! (0==AV125WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] <= @AV125WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_PessoaCod] <= @AV125WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to)";
            }
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV127WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV126WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV126WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV126WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom)";
            }
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV127WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV127WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV127WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel)";
            }
         }
         else
         {
            GXv_int3[25] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemItemParecer_Codigo]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemItemParecer_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemItem_Lancamento]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemItem_Lancamento] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemItemParecer_Comentario]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemItemParecer_Comentario] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemItemParecer_Data]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemItemParecer_Data] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemItemParecer_UsuarioCod]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemItemParecer_UsuarioCod] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Usuario_PessoaCod]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Usuario_PessoaCod] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Pessoa_Nome] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemItemParecer_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_H005V3( IGxContext context ,
                                             String AV100WWContagemItemParecerDS_1_Dynamicfiltersselector1 ,
                                             short AV101WWContagemItemParecerDS_2_Dynamicfiltersoperator1 ,
                                             String AV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 ,
                                             String AV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 ,
                                             bool AV104WWContagemItemParecerDS_5_Dynamicfiltersenabled2 ,
                                             String AV105WWContagemItemParecerDS_6_Dynamicfiltersselector2 ,
                                             short AV106WWContagemItemParecerDS_7_Dynamicfiltersoperator2 ,
                                             String AV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 ,
                                             String AV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 ,
                                             bool AV109WWContagemItemParecerDS_10_Dynamicfiltersenabled3 ,
                                             String AV110WWContagemItemParecerDS_11_Dynamicfiltersselector3 ,
                                             short AV111WWContagemItemParecerDS_12_Dynamicfiltersoperator3 ,
                                             String AV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 ,
                                             String AV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 ,
                                             int AV114WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo ,
                                             int AV115WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to ,
                                             int AV116WWContagemItemParecerDS_17_Tfcontagemitem_lancamento ,
                                             int AV117WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to ,
                                             String AV119WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel ,
                                             String AV118WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario ,
                                             DateTime AV120WWContagemItemParecerDS_21_Tfcontagemitemparecer_data ,
                                             DateTime AV121WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to ,
                                             int AV122WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod ,
                                             int AV123WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to ,
                                             int AV124WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod ,
                                             int AV125WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to ,
                                             String AV127WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel ,
                                             String AV126WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom ,
                                             String A244ContagemItemParecer_Comentario ,
                                             String A248ContagemItemParecer_UsuarioPessoaNom ,
                                             int A243ContagemItemParecer_Codigo ,
                                             int A224ContagemItem_Lancamento ,
                                             DateTime A245ContagemItemParecer_Data ,
                                             int A246ContagemItemParecer_UsuarioCod ,
                                             int A247ContagemItemParecer_UsuarioPessoaCod ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [26] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (([ContagemItemParecer] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemItemParecer_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV100WWContagemItemParecerDS_1_Dynamicfiltersselector1, "CONTAGEMITEMPARECER_COMENTARIO") == 0 ) && ( ( AV101WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 0 ) || ( AV101WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] like @lV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] like @lV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV100WWContagemItemParecerDS_1_Dynamicfiltersselector1, "CONTAGEMITEMPARECER_COMENTARIO") == 0 ) && ( ( AV101WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 1 ) || ( AV101WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] like '%' + @lV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] like '%' + @lV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV100WWContagemItemParecerDS_1_Dynamicfiltersselector1, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 ) && ( ( AV101WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 0 ) || ( AV101WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV100WWContagemItemParecerDS_1_Dynamicfiltersselector1, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 ) && ( ( AV101WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 1 ) || ( AV101WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV104WWContagemItemParecerDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV105WWContagemItemParecerDS_6_Dynamicfiltersselector2, "CONTAGEMITEMPARECER_COMENTARIO") == 0 ) && ( ( AV106WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 0 ) || ( AV106WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] like @lV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] like @lV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV104WWContagemItemParecerDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV105WWContagemItemParecerDS_6_Dynamicfiltersselector2, "CONTAGEMITEMPARECER_COMENTARIO") == 0 ) && ( ( AV106WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 1 ) || ( AV106WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] like '%' + @lV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] like '%' + @lV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV104WWContagemItemParecerDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV105WWContagemItemParecerDS_6_Dynamicfiltersselector2, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 ) && ( ( AV106WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 0 ) || ( AV106WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV104WWContagemItemParecerDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV105WWContagemItemParecerDS_6_Dynamicfiltersselector2, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 ) && ( ( AV106WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 1 ) || ( AV106WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV109WWContagemItemParecerDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV110WWContagemItemParecerDS_11_Dynamicfiltersselector3, "CONTAGEMITEMPARECER_COMENTARIO") == 0 ) && ( ( AV111WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 0 ) || ( AV111WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] like @lV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] like @lV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV109WWContagemItemParecerDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV110WWContagemItemParecerDS_11_Dynamicfiltersselector3, "CONTAGEMITEMPARECER_COMENTARIO") == 0 ) && ( ( AV111WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 1 ) || ( AV111WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] like '%' + @lV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] like '%' + @lV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV109WWContagemItemParecerDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV110WWContagemItemParecerDS_11_Dynamicfiltersselector3, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 ) && ( ( AV111WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 0 ) || ( AV111WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( AV109WWContagemItemParecerDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV110WWContagemItemParecerDS_11_Dynamicfiltersselector3, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 ) && ( ( AV111WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 1 ) || ( AV111WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ! (0==AV114WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Codigo] >= @AV114WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Codigo] >= @AV114WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ! (0==AV115WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Codigo] <= @AV115WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Codigo] <= @AV115WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( ! (0==AV116WWContagemItemParecerDS_17_Tfcontagemitem_lancamento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItem_Lancamento] >= @AV116WWContagemItemParecerDS_17_Tfcontagemitem_lancamento)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItem_Lancamento] >= @AV116WWContagemItemParecerDS_17_Tfcontagemitem_lancamento)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! (0==AV117WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItem_Lancamento] <= @AV117WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItem_Lancamento] <= @AV117WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV119WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] like @lV118WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] like @lV118WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV119WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] = @AV119WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] = @AV119WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( ! (DateTime.MinValue==AV120WWContagemItemParecerDS_21_Tfcontagemitemparecer_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Data] >= @AV120WWContagemItemParecerDS_21_Tfcontagemitemparecer_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Data] >= @AV120WWContagemItemParecerDS_21_Tfcontagemitemparecer_data)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! (DateTime.MinValue==AV121WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Data] <= @AV121WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Data] <= @AV121WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( ! (0==AV122WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_UsuarioCod] >= @AV122WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_UsuarioCod] >= @AV122WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod)";
            }
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( ! (0==AV123WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_UsuarioCod] <= @AV123WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_UsuarioCod] <= @AV123WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to)";
            }
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( ! (0==AV124WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] >= @AV124WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_PessoaCod] >= @AV124WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod)";
            }
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( ! (0==AV125WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] <= @AV125WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_PessoaCod] <= @AV125WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to)";
            }
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV127WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV126WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV126WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV126WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom)";
            }
         }
         else
         {
            GXv_int5[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV127WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV127WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV127WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel)";
            }
         }
         else
         {
            GXv_int5[25] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H005V2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (DateTime)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (short)dynConstraints[35] , (bool)dynConstraints[36] );
               case 1 :
                     return conditional_H005V3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (DateTime)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (short)dynConstraints[35] , (bool)dynConstraints[36] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH005V2 ;
          prmH005V2 = new Object[] {
          new Object[] {"@lV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@lV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@lV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@lV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@lV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@lV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@lV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV114WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV115WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV116WWContagemItemParecerDS_17_Tfcontagemitem_lancamento",SqlDbType.Int,6,0} ,
          new Object[] {"@AV117WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV118WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV119WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV120WWContagemItemParecerDS_21_Tfcontagemitemparecer_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV121WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV122WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV123WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV124WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV125WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV126WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV127WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH005V3 ;
          prmH005V3 = new Object[] {
          new Object[] {"@lV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@lV102WWContagemItemParecerDS_3_Contagemitemparecer_comentario1",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@lV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV103WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@lV107WWContagemItemParecerDS_8_Contagemitemparecer_comentario2",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@lV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV108WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@lV112WWContagemItemParecerDS_13_Contagemitemparecer_comentario3",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@lV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV113WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV114WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV115WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV116WWContagemItemParecerDS_17_Tfcontagemitem_lancamento",SqlDbType.Int,6,0} ,
          new Object[] {"@AV117WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV118WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV119WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV120WWContagemItemParecerDS_21_Tfcontagemitemparecer_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV121WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV122WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV123WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV124WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV125WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV126WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV127WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel",SqlDbType.Char,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H005V2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH005V2,11,0,true,false )
             ,new CursorDef("H005V3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH005V3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(4) ;
                ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[49]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[50]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[51]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[52]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[54]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[59]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[60]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[61]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                return;
       }
    }

 }

}
