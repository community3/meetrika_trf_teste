/*
               File: type_SdtGAMExampleSDTApplicationData
        Description: GAMExampleSDTApplicationData
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:43.88
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "GAMExampleSDTApplicationData" )]
   [XmlType(TypeName =  "GAMExampleSDTApplicationData" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtGAMExampleSDTApplicationData : GxUserType
   {
      public SdtGAMExampleSDTApplicationData( )
      {
         /* Constructor for serialization */
         gxTv_SdtGAMExampleSDTApplicationData_Application = "";
         gxTv_SdtGAMExampleSDTApplicationData_Operation = "";
      }

      public SdtGAMExampleSDTApplicationData( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtGAMExampleSDTApplicationData deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtGAMExampleSDTApplicationData)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtGAMExampleSDTApplicationData obj ;
         obj = this;
         obj.gxTpr_Application = deserialized.gxTpr_Application;
         obj.gxTpr_Operation = deserialized.gxTpr_Operation;
         obj.gxTpr_Other = deserialized.gxTpr_Other;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Application") )
               {
                  gxTv_SdtGAMExampleSDTApplicationData_Application = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Operation") )
               {
                  gxTv_SdtGAMExampleSDTApplicationData_Operation = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Other") )
               {
                  gxTv_SdtGAMExampleSDTApplicationData_Other = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "GAMExampleSDTApplicationData";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Application", StringUtil.RTrim( gxTv_SdtGAMExampleSDTApplicationData_Application));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Operation", StringUtil.RTrim( gxTv_SdtGAMExampleSDTApplicationData_Operation));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Other", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtGAMExampleSDTApplicationData_Other), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Application", gxTv_SdtGAMExampleSDTApplicationData_Application, false);
         AddObjectProperty("Operation", gxTv_SdtGAMExampleSDTApplicationData_Operation, false);
         AddObjectProperty("Other", gxTv_SdtGAMExampleSDTApplicationData_Other, false);
         return  ;
      }

      [  SoapElement( ElementName = "Application" )]
      [  XmlElement( ElementName = "Application"   )]
      public String gxTpr_Application
      {
         get {
            return gxTv_SdtGAMExampleSDTApplicationData_Application ;
         }

         set {
            gxTv_SdtGAMExampleSDTApplicationData_Application = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Operation" )]
      [  XmlElement( ElementName = "Operation"   )]
      public String gxTpr_Operation
      {
         get {
            return gxTv_SdtGAMExampleSDTApplicationData_Operation ;
         }

         set {
            gxTv_SdtGAMExampleSDTApplicationData_Operation = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Other" )]
      [  XmlElement( ElementName = "Other"   )]
      public short gxTpr_Other
      {
         get {
            return gxTv_SdtGAMExampleSDTApplicationData_Other ;
         }

         set {
            gxTv_SdtGAMExampleSDTApplicationData_Other = (short)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtGAMExampleSDTApplicationData_Application = "";
         gxTv_SdtGAMExampleSDTApplicationData_Operation = "";
         sTagName = "";
         return  ;
      }

      protected short gxTv_SdtGAMExampleSDTApplicationData_Other ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtGAMExampleSDTApplicationData_Application ;
      protected String gxTv_SdtGAMExampleSDTApplicationData_Operation ;
      protected String sTagName ;
   }

   [DataContract(Name = @"GAMExampleSDTApplicationData", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtGAMExampleSDTApplicationData_RESTInterface : GxGenericCollectionItem<SdtGAMExampleSDTApplicationData>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtGAMExampleSDTApplicationData_RESTInterface( ) : base()
      {
      }

      public SdtGAMExampleSDTApplicationData_RESTInterface( SdtGAMExampleSDTApplicationData psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Application" , Order = 0 )]
      public String gxTpr_Application
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Application) ;
         }

         set {
            sdt.gxTpr_Application = (String)(value);
         }

      }

      [DataMember( Name = "Operation" , Order = 1 )]
      public String gxTpr_Operation
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Operation) ;
         }

         set {
            sdt.gxTpr_Operation = (String)(value);
         }

      }

      [DataMember( Name = "Other" , Order = 2 )]
      public Nullable<short> gxTpr_Other
      {
         get {
            return sdt.gxTpr_Other ;
         }

         set {
            sdt.gxTpr_Other = (short)(value.HasValue ? value.Value : 0);
         }

      }

      public SdtGAMExampleSDTApplicationData sdt
      {
         get {
            return (SdtGAMExampleSDTApplicationData)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtGAMExampleSDTApplicationData() ;
         }
      }

   }

}
