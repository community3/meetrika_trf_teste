/*
               File: WP_DemandasVinculadas
        Description: Criar demandas Vinculadas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/29/2020 21:28:44.6
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_demandasvinculadas : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_demandasvinculadas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_demandasvinculadas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavContratada_codigo = new GXCombobox();
         dynavContrato_codigo = new GXCombobox();
         cmbavContratoservicos_codigo = new GXCombobox();
         cmbavIsmodificaragrupador = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATADA_CODIGO") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV20Context);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTRATADA_CODIGOTO2( AV20Context) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATO_CODIGO") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV20Context);
               AV21Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Contratada_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTRATO_CODIGOTO2( AV20Context, AV21Contratada_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PATO2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTTO2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206292128449");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_demandasvinculadas.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_boolean_hidden_field( context, "vCHECKREQUIREDFIELDSRESULT", AV7CheckRequiredFieldsResult);
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATOSERVICOS_ATIVO", A638ContratoServicos_Ativo);
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_IDENTIFICACAO", A2107Servico_Identificacao);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTEXT", AV20Context);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTEXT", AV20Context);
         }
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GxWebStd.gx_hidden_field( context, "INNEWWINDOW1_Target", StringUtil.RTrim( Innewwindow1_Target));
         GxWebStd.gx_hidden_field( context, "DVELOP_CONFIRMPANEL_Width", StringUtil.RTrim( Dvelop_confirmpanel_Width));
         GxWebStd.gx_hidden_field( context, "DVELOP_CONFIRMPANEL_Height", StringUtil.RTrim( Dvelop_confirmpanel_Height));
         GxWebStd.gx_hidden_field( context, "DVELOP_CONFIRMPANEL_Title", StringUtil.RTrim( Dvelop_confirmpanel_Title));
         GxWebStd.gx_hidden_field( context, "DVELOP_CONFIRMPANEL_Confirmationtext", StringUtil.RTrim( Dvelop_confirmpanel_Confirmationtext));
         GxWebStd.gx_hidden_field( context, "DVELOP_CONFIRMPANEL_Yesbuttoncaption", StringUtil.RTrim( Dvelop_confirmpanel_Yesbuttoncaption));
         GxWebStd.gx_hidden_field( context, "DVELOP_CONFIRMPANEL_Cancelbuttoncaption", StringUtil.RTrim( Dvelop_confirmpanel_Cancelbuttoncaption));
         GxWebStd.gx_hidden_field( context, "DVELOP_CONFIRMPANEL_Confirmtype", StringUtil.RTrim( Dvelop_confirmpanel_Confirmtype));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WETO2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTTO2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_demandasvinculadas.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_DemandasVinculadas" ;
      }

      public override String GetPgmdesc( )
      {
         return "Criar demandas Vinculadas" ;
      }

      protected void WBTO0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_TO2( true) ;
         }
         else
         {
            wb_table1_2_TO2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_TO2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTTO2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Criar demandas Vinculadas", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPTO0( ) ;
      }

      protected void WSTO2( )
      {
         STARTTO2( ) ;
         EVTTO2( ) ;
      }

      protected void EVTTO2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DVELOP_CONFIRMPANEL.CLOSE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11TO2 */
                              E11TO2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12TO2 */
                              E12TO2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E13TO2 */
                                    E13TO2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14TO2 */
                              E14TO2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTRATO_CODIGO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15TO2 */
                              E15TO2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTRATADA_CODIGO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16TO2 */
                              E16TO2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17TO2 */
                              E17TO2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WETO2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PATO2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavContratada_codigo.Name = "vCONTRATADA_CODIGO";
            dynavContratada_codigo.WebTags = "";
            dynavContrato_codigo.Name = "vCONTRATO_CODIGO";
            dynavContrato_codigo.WebTags = "";
            cmbavContratoservicos_codigo.Name = "vCONTRATOSERVICOS_CODIGO";
            cmbavContratoservicos_codigo.WebTags = "";
            cmbavContratoservicos_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "Selecione", 0);
            if ( cmbavContratoservicos_codigo.ItemCount > 0 )
            {
               AV24ContratoServicos_Codigo = (int)(NumberUtil.Val( cmbavContratoservicos_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24ContratoServicos_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24ContratoServicos_Codigo), 6, 0)));
            }
            cmbavIsmodificaragrupador.Name = "vISMODIFICARAGRUPADOR";
            cmbavIsmodificaragrupador.WebTags = "";
            cmbavIsmodificaragrupador.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 1, 0)), "Selecione", 0);
            if ( cmbavIsmodificaragrupador.ItemCount > 0 )
            {
               AV47IsModificarAgrupador = (short)(NumberUtil.Val( cmbavIsmodificaragrupador.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV47IsModificarAgrupador), 1, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47IsModificarAgrupador", StringUtil.Str( (decimal)(AV47IsModificarAgrupador), 1, 0));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavContratada_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         GXVvCONTRATO_CODIGO_htmlTO2( AV20Context, AV21Contratada_Codigo) ;
         GXVvCONTRATO_CODIGO_htmlTO2( AV20Context, AV21Contratada_Codigo) ;
         /* End function dynload_actions */
      }

      protected void GXDLVvCONTRATADA_CODIGOTO2( wwpbaseobjects.SdtWWPContext AV20Context )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATADA_CODIGO_dataTO2( AV20Context) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATADA_CODIGO_htmlTO2( wwpbaseobjects.SdtWWPContext AV20Context )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATADA_CODIGO_dataTO2( AV20Context) ;
         gxdynajaxindex = 1;
         dynavContratada_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratada_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratada_codigo.ItemCount > 0 )
         {
            AV21Contratada_Codigo = (int)(NumberUtil.Val( dynavContratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21Contratada_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Contratada_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATADA_CODIGO_dataTO2( wwpbaseobjects.SdtWWPContext AV20Context )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00TO2 */
         pr_default.execute(0, new Object[] {AV20Context.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00TO2_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00TO2_A438Contratada_Sigla[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvCONTRATO_CODIGOTO2( wwpbaseobjects.SdtWWPContext AV20Context ,
                                               int AV21Contratada_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATO_CODIGO_dataTO2( AV20Context, AV21Contratada_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATO_CODIGO_htmlTO2( wwpbaseobjects.SdtWWPContext AV20Context ,
                                                  int AV21Contratada_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATO_CODIGO_dataTO2( AV20Context, AV21Contratada_Codigo) ;
         gxdynajaxindex = 1;
         dynavContrato_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContrato_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContrato_codigo.ItemCount > 0 )
         {
            AV23Contrato_Codigo = (int)(NumberUtil.Val( dynavContrato_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV23Contrato_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Contrato_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATO_CODIGO_dataTO2( wwpbaseobjects.SdtWWPContext AV20Context ,
                                                    int AV21Contratada_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00TO3 */
         pr_default.execute(1, new Object[] {AV20Context.gxTpr_Areatrabalho_codigo, AV21Contratada_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00TO3_A74Contrato_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00TO3_A77Contrato_Numero[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavContratada_codigo.ItemCount > 0 )
         {
            AV21Contratada_Codigo = (int)(NumberUtil.Val( dynavContratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21Contratada_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Contratada_Codigo), 6, 0)));
         }
         if ( dynavContrato_codigo.ItemCount > 0 )
         {
            AV23Contrato_Codigo = (int)(NumberUtil.Val( dynavContrato_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV23Contrato_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Contrato_Codigo), 6, 0)));
         }
         if ( cmbavContratoservicos_codigo.ItemCount > 0 )
         {
            AV24ContratoServicos_Codigo = (int)(NumberUtil.Val( cmbavContratoservicos_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24ContratoServicos_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24ContratoServicos_Codigo), 6, 0)));
         }
         if ( cmbavIsmodificaragrupador.ItemCount > 0 )
         {
            AV47IsModificarAgrupador = (short)(NumberUtil.Val( cmbavIsmodificaragrupador.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV47IsModificarAgrupador), 1, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47IsModificarAgrupador", StringUtil.Str( (decimal)(AV47IsModificarAgrupador), 1, 0));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFTO2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFTO2( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E14TO2 */
         E14TO2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E17TO2 */
            E17TO2 ();
            WBTO0( ) ;
         }
      }

      protected void STRUPTO0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12TO2 */
         E12TO2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvCONTRATADA_CODIGO_htmlTO2( AV20Context) ;
         GXVvCONTRATO_CODIGO_htmlTO2( AV20Context, AV21Contratada_Codigo) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            dynavContratada_codigo.CurrentValue = cgiGet( dynavContratada_codigo_Internalname);
            AV21Contratada_Codigo = (int)(NumberUtil.Val( cgiGet( dynavContratada_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Contratada_Codigo), 6, 0)));
            dynavContrato_codigo.CurrentValue = cgiGet( dynavContrato_codigo_Internalname);
            AV23Contrato_Codigo = (int)(NumberUtil.Val( cgiGet( dynavContrato_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Contrato_Codigo), 6, 0)));
            cmbavContratoservicos_codigo.CurrentValue = cgiGet( cmbavContratoservicos_codigo_Internalname);
            AV24ContratoServicos_Codigo = (int)(NumberUtil.Val( cgiGet( cmbavContratoservicos_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24ContratoServicos_Codigo), 6, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_dataentrega_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data para Entrega"}), 1, "vCONTAGEMRESULTADO_DATAENTREGA");
               GX_FocusControl = edtavContagemresultado_dataentrega_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13ContagemResultado_DataEntrega = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContagemResultado_DataEntrega", context.localUtil.Format(AV13ContagemResultado_DataEntrega, "99/99/99"));
            }
            else
            {
               AV13ContagemResultado_DataEntrega = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_dataentrega_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContagemResultado_DataEntrega", context.localUtil.Format(AV13ContagemResultado_DataEntrega, "99/99/99"));
            }
            AV45ContagemResultado_Agrupador = StringUtil.Upper( cgiGet( edtavContagemresultado_agrupador_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ContagemResultado_Agrupador", AV45ContagemResultado_Agrupador);
            cmbavIsmodificaragrupador.CurrentValue = cgiGet( cmbavIsmodificaragrupador_Internalname);
            AV47IsModificarAgrupador = (short)(NumberUtil.Val( cgiGet( cmbavIsmodificaragrupador_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47IsModificarAgrupador", StringUtil.Str( (decimal)(AV47IsModificarAgrupador), 1, 0));
            /* Read saved values. */
            Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
            Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
            Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
            Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
            Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
            Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
            Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
            Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
            Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
            Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
            Innewwindow1_Target = cgiGet( "INNEWWINDOW1_Target");
            Dvelop_confirmpanel_Width = cgiGet( "DVELOP_CONFIRMPANEL_Width");
            Dvelop_confirmpanel_Height = cgiGet( "DVELOP_CONFIRMPANEL_Height");
            Dvelop_confirmpanel_Title = cgiGet( "DVELOP_CONFIRMPANEL_Title");
            Dvelop_confirmpanel_Confirmationtext = cgiGet( "DVELOP_CONFIRMPANEL_Confirmationtext");
            Dvelop_confirmpanel_Yesbuttoncaption = cgiGet( "DVELOP_CONFIRMPANEL_Yesbuttoncaption");
            Dvelop_confirmpanel_Cancelbuttoncaption = cgiGet( "DVELOP_CONFIRMPANEL_Cancelbuttoncaption");
            Dvelop_confirmpanel_Confirmtype = cgiGet( "DVELOP_CONFIRMPANEL_Confirmtype");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXVvCONTRATADA_CODIGO_htmlTO2( AV20Context) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E12TO2 */
         E12TO2 ();
         if (returnInSub) return;
      }

      protected void E12TO2( )
      {
         /* Start Routine */
         AV8clientId = AV42webnotification.gxTpr_Clientid;
         Form.Meta.addItem("Versao", "1.1 - Data: 29/06/2020 20:13", 0) ;
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV20Context) ;
         AV9CodigosOS.FromXml(AV43WebSession.Get("SdtCodigos"), "Collection");
         cmbavIsmodificaragrupador.removeAllItems();
         cmbavIsmodificaragrupador.addItem("0", "Selecione", 0);
         cmbavIsmodificaragrupador.addItem("1", "Sim", 0);
         cmbavIsmodificaragrupador.addItem("2", "N�o", 0);
         AV47IsModificarAgrupador = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47IsModificarAgrupador", StringUtil.Str( (decimal)(AV47IsModificarAgrupador), 1, 0));
         /* Execute user subroutine: 'ATTRIBUTESSECURITYCODE' */
         S112 ();
         if (returnInSub) return;
      }

      public void GXEnter( )
      {
         /* Execute user event: E13TO2 */
         E13TO2 ();
         if (returnInSub) return;
      }

      protected void E13TO2( )
      {
         /* Enter Routine */
         /* Execute user subroutine: 'CHECKREQUIREDFIELDS' */
         S122 ();
         if (returnInSub) return;
         if ( AV7CheckRequiredFieldsResult )
         {
            /* Execute user subroutine: 'EXECULTA.REGRAS' */
            S132 ();
            if (returnInSub) return;
         }
      }

      protected void S122( )
      {
         /* 'CHECKREQUIREDFIELDS' Routine */
         AV7CheckRequiredFieldsResult = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7CheckRequiredFieldsResult", AV7CheckRequiredFieldsResult);
         if ( (0==AV21Contratada_Codigo) )
         {
            GX_msglist.addItem("Contratada � obrigat�rio.");
            AV7CheckRequiredFieldsResult = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7CheckRequiredFieldsResult", AV7CheckRequiredFieldsResult);
         }
         if ( (0==AV23Contrato_Codigo) )
         {
            GX_msglist.addItem("Contrato � obrigat�rio.");
            AV7CheckRequiredFieldsResult = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7CheckRequiredFieldsResult", AV7CheckRequiredFieldsResult);
         }
         if ( (0==AV24ContratoServicos_Codigo) )
         {
            GX_msglist.addItem("Servi�o � obrigat�rio.");
            AV7CheckRequiredFieldsResult = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7CheckRequiredFieldsResult", AV7CheckRequiredFieldsResult);
         }
         if ( (DateTime.MinValue==AV13ContagemResultado_DataEntrega) )
         {
            GX_msglist.addItem("Prazo � obrigat�rio.");
            AV7CheckRequiredFieldsResult = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7CheckRequiredFieldsResult", AV7CheckRequiredFieldsResult);
         }
         if ( ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45ContagemResultado_Agrupador)) ) && (0==AV47IsModificarAgrupador) )
         {
            GX_msglist.addItem("Modificar o Agrupador das demandas de Origem? � obrigat�rio.");
            AV7CheckRequiredFieldsResult = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7CheckRequiredFieldsResult", AV7CheckRequiredFieldsResult);
         }
      }

      protected void S112( )
      {
         /* 'ATTRIBUTESSECURITYCODE' Routine */
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV45ContagemResultado_Agrupador)) )
         {
            tblTable2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTable2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTable2_Visible), 5, 0)));
         }
         else
         {
            tblTable2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTable2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTable2_Visible), 5, 0)));
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45ContagemResultado_Agrupador)) )
         {
            cellIsmodificaragrupador_cell_Class = "RequiredDataContentCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellIsmodificaragrupador_cell_Internalname, "Class", cellIsmodificaragrupador_cell_Class);
         }
         else
         {
            cellIsmodificaragrupador_cell_Class = "DataContentCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellIsmodificaragrupador_cell_Internalname, "Class", cellIsmodificaragrupador_cell_Class);
         }
      }

      protected void E14TO2( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV20Context) ;
      }

      protected void E15TO2( )
      {
         /* Contrato_codigo_Click Routine */
         /* Execute user subroutine: 'CARREGA.CONTRATO.SERVICOS' */
         S142 ();
         if (returnInSub) return;
         cmbavContratoservicos_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24ContratoServicos_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicos_codigo_Internalname, "Values", cmbavContratoservicos_codigo.ToJavascriptSource());
      }

      protected void E16TO2( )
      {
         /* Contratada_codigo_Click Routine */
         AV45ContagemResultado_Agrupador = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ContagemResultado_Agrupador", AV45ContagemResultado_Agrupador);
         /* Execute user subroutine: 'CARREGA.CONTRATO.SERVICOS' */
         S142 ();
         if (returnInSub) return;
         cmbavContratoservicos_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24ContratoServicos_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicos_codigo_Internalname, "Values", cmbavContratoservicos_codigo.ToJavascriptSource());
      }

      protected void E11TO2( )
      {
         /* Dvelop_confirmpanel_Close Routine */
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void S142( )
      {
         /* 'CARREGA.CONTRATO.SERVICOS' Routine */
         cmbavContratoservicos_codigo.removeAllItems();
         cmbavContratoservicos_codigo.addItem("0", "Selecione", 0);
         /* Using cursor H00TO4 */
         pr_default.execute(2, new Object[] {AV23Contrato_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A155Servico_Codigo = H00TO4_A155Servico_Codigo[0];
            A638ContratoServicos_Ativo = H00TO4_A638ContratoServicos_Ativo[0];
            A74Contrato_Codigo = H00TO4_A74Contrato_Codigo[0];
            A160ContratoServicos_Codigo = H00TO4_A160ContratoServicos_Codigo[0];
            A608Servico_Nome = H00TO4_A608Servico_Nome[0];
            A605Servico_Sigla = H00TO4_A605Servico_Sigla[0];
            A608Servico_Nome = H00TO4_A608Servico_Nome[0];
            A605Servico_Sigla = H00TO4_A605Servico_Sigla[0];
            A2107Servico_Identificacao = StringUtil.Concat( StringUtil.Trim( A605Servico_Sigla), StringUtil.Trim( A608Servico_Nome), " - ");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2107Servico_Identificacao", A2107Servico_Identificacao);
            cmbavContratoservicos_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)), A2107Servico_Identificacao, 0);
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void S132( )
      {
         /* 'EXECULTA.REGRAS' Routine */
         /* Execute user subroutine: 'ATTRIBUTESSECURITYCODE' */
         S112 ();
         if (returnInSub) return;
         Innewwindow1_Target = formatLink("arel_demandasvinculadas.aspx") + "?" + UrlEncode("" +AV21Contratada_Codigo) + "," + UrlEncode("" +AV23Contrato_Codigo) + "," + UrlEncode("" +AV24ContratoServicos_Codigo) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV13ContagemResultado_DataEntrega)) + "," + UrlEncode(StringUtil.RTrim(AV45ContagemResultado_Agrupador)) + "," + UrlEncode("" +AV47IsModificarAgrupador);
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Innewwindow1_Internalname, "Target", Innewwindow1_Target);
         this.executeUsercontrolMethod("", false, "INNEWWINDOW1Container", "OpenWindow", "", new Object[] {});
         Dvelop_confirmpanel_Title = "Aten��o!";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvelop_confirmpanel_Internalname, "Title", Dvelop_confirmpanel_Title);
         Dvelop_confirmpanel_Confirmationtext = "Gera��o das Demandandas vinculadas conclu�da. Verifique o relat�rio gerado!";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvelop_confirmpanel_Internalname, "ConfirmationText", Dvelop_confirmpanel_Confirmationtext);
         Dvelop_confirmpanel_Yesbuttoncaption = "Confirmar";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvelop_confirmpanel_Internalname, "YesButtonCaption", Dvelop_confirmpanel_Yesbuttoncaption);
         Dvelop_confirmpanel_Confirmtype = "YES";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvelop_confirmpanel_Internalname, "ConfirmType", Dvelop_confirmpanel_Confirmtype);
         this.executeUsercontrolMethod("", false, "DVELOP_CONFIRMPANELContainer", "Confirm", "", new Object[] {});
      }

      protected void nextLoad( )
      {
      }

      protected void E17TO2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_TO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_TO2( true) ;
         }
         else
         {
            wb_table2_5_TO2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_TO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            wb_table3_52_TO2( true) ;
         }
         else
         {
            wb_table3_52_TO2( false) ;
         }
         return  ;
      }

      protected void wb_table3_52_TO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_60_TO2( true) ;
         }
         else
         {
            wb_table4_60_TO2( false) ;
         }
         return  ;
      }

      protected void wb_table4_60_TO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVELOP_CONFIRMPANELContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_TO2e( true) ;
         }
         else
         {
            wb_table1_2_TO2e( false) ;
         }
      }

      protected void wb_table4_60_TO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableinnewwindow_Internalname, tblTableinnewwindow_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"INNEWWINDOW1Container"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_60_TO2e( true) ;
         }
         else
         {
            wb_table4_60_TO2e( false) ;
         }
      }

      protected void wb_table3_52_TO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenter_Internalname, "", "Confirmar", bttBtnenter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_DemandasVinculadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_DemandasVinculadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_52_TO2e( true) ;
         }
         else
         {
            wb_table3_52_TO2e( false) ;
         }
      }

      protected void wb_table2_5_TO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table5_13_TO2( true) ;
         }
         else
         {
            wb_table5_13_TO2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_TO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_TO2e( true) ;
         }
         else
         {
            wb_table2_5_TO2e( false) ;
         }
      }

      protected void wb_table5_13_TO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(450), 10, 0)) + "px" + ";";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(800), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_16_TO2( true) ;
         }
         else
         {
            wb_table6_16_TO2( false) ;
         }
         return  ;
      }

      protected void wb_table6_16_TO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_44_TO2( true) ;
         }
         else
         {
            wb_table7_44_TO2( false) ;
         }
         return  ;
      }

      protected void wb_table7_44_TO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_TO2e( true) ;
         }
         else
         {
            wb_table5_13_TO2e( false) ;
         }
      }

      protected void wb_table7_44_TO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTable2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockismodificaragrupador_Internalname, "Modificar o Agrupador das demandas de Origem?", "", "", lblTextblockismodificaragrupador_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_DemandasVinculadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellIsmodificaragrupador_cell_Internalname+"\"  class='"+cellIsmodificaragrupador_cell_Class+"'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavIsmodificaragrupador, cmbavIsmodificaragrupador_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV47IsModificarAgrupador), 1, 0)), 1, cmbavIsmodificaragrupador_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_WP_DemandasVinculadas.htm");
            cmbavIsmodificaragrupador.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV47IsModificarAgrupador), 1, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavIsmodificaragrupador_Internalname, "Values", (String)(cmbavIsmodificaragrupador.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_44_TO2e( true) ;
         }
         else
         {
            wb_table7_44_TO2e( false) ;
         }
      }

      protected void wb_table6_16_TO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable_Internalname, tblTable_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_codigo_Internalname, "Contratada", "", "", lblTextblockcontratada_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_DemandasVinculadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratada_codigo, dynavContratada_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21Contratada_Codigo), 6, 0)), 1, dynavContratada_codigo_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTRATADA_CODIGO.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "", true, "HLP_WP_DemandasVinculadas.htm");
            dynavContratada_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21Contratada_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo_Internalname, "Values", (String)(dynavContratada_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_codigo_Internalname, "Contrato", "", "", lblTextblockcontrato_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_DemandasVinculadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContrato_codigo, dynavContrato_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV23Contrato_Codigo), 6, 0)), 1, dynavContrato_codigo_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTRATO_CODIGO.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,26);\"", "", true, "HLP_WP_DemandasVinculadas.htm");
            dynavContrato_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23Contrato_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContrato_codigo_Internalname, "Values", (String)(dynavContrato_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_codigo_Internalname, "Servi�o", "", "", lblTextblockcontratoservicos_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_DemandasVinculadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContratoservicos_codigo, cmbavContratoservicos_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24ContratoServicos_Codigo), 6, 0)), 1, cmbavContratoservicos_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_WP_DemandasVinculadas.htm");
            cmbavContratoservicos_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24ContratoServicos_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicos_codigo_Internalname, "Values", (String)(cmbavContratoservicos_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_dataentrega_Internalname, "Prazo", "", "", lblTextblockcontagemresultado_dataentrega_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_DemandasVinculadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_dataentrega_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_dataentrega_Internalname, context.localUtil.Format(AV13ContagemResultado_DataEntrega, "99/99/99"), context.localUtil.Format( AV13ContagemResultado_DataEntrega, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_dataentrega_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_DemandasVinculadas.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_dataentrega_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_DemandasVinculadas.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_agrupador_Internalname, "Agrupador", "", "", lblTextblockcontagemresultado_agrupador_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_DemandasVinculadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_agrupador_Internalname, StringUtil.RTrim( AV45ContagemResultado_Agrupador), StringUtil.RTrim( context.localUtil.Format( AV45ContagemResultado_Agrupador, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_agrupador_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_WP_DemandasVinculadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_16_TO2e( true) ;
         }
         else
         {
            wb_table6_16_TO2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PATO2( ) ;
         WSTO2( ) ;
         WETO2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202062921284488");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_demandasvinculadas.js", "?202062921284488");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontratada_codigo_Internalname = "TEXTBLOCKCONTRATADA_CODIGO";
         dynavContratada_codigo_Internalname = "vCONTRATADA_CODIGO";
         lblTextblockcontrato_codigo_Internalname = "TEXTBLOCKCONTRATO_CODIGO";
         dynavContrato_codigo_Internalname = "vCONTRATO_CODIGO";
         lblTextblockcontratoservicos_codigo_Internalname = "TEXTBLOCKCONTRATOSERVICOS_CODIGO";
         cmbavContratoservicos_codigo_Internalname = "vCONTRATOSERVICOS_CODIGO";
         lblTextblockcontagemresultado_dataentrega_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_DATAENTREGA";
         edtavContagemresultado_dataentrega_Internalname = "vCONTAGEMRESULTADO_DATAENTREGA";
         lblTextblockcontagemresultado_agrupador_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_AGRUPADOR";
         edtavContagemresultado_agrupador_Internalname = "vCONTAGEMRESULTADO_AGRUPADOR";
         tblTable_Internalname = "TABLE";
         lblTextblockismodificaragrupador_Internalname = "TEXTBLOCKISMODIFICARAGRUPADOR";
         cmbavIsmodificaragrupador_Internalname = "vISMODIFICARAGRUPADOR";
         cellIsmodificaragrupador_cell_Internalname = "ISMODIFICARAGRUPADOR_CELL";
         tblTable2_Internalname = "TABLE2";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtnenter_Internalname = "BTNENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         Innewwindow1_Internalname = "INNEWWINDOW1";
         tblTableinnewwindow_Internalname = "TABLEINNEWWINDOW";
         Dvelop_confirmpanel_Internalname = "DVELOP_CONFIRMPANEL";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavContagemresultado_agrupador_Jsonclick = "";
         edtavContagemresultado_dataentrega_Jsonclick = "";
         cmbavContratoservicos_codigo_Jsonclick = "";
         dynavContrato_codigo_Jsonclick = "";
         dynavContratada_codigo_Jsonclick = "";
         cmbavIsmodificaragrupador_Jsonclick = "";
         cellIsmodificaragrupador_cell_Class = "";
         tblTable2_Visible = 1;
         Dvelop_confirmpanel_Confirmtype = "";
         Dvelop_confirmpanel_Cancelbuttoncaption = "Fechar";
         Dvelop_confirmpanel_Yesbuttoncaption = "Confirmar";
         Dvelop_confirmpanel_Confirmationtext = "Gera��o das Demandandas vinculadas conclu�da. Verifique o relat�rio geardo!";
         Dvelop_confirmpanel_Title = "Aten��o!";
         Dvelop_confirmpanel_Height = "200px";
         Dvelop_confirmpanel_Width = "250px";
         Innewwindow1_Target = "";
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Informa��es";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Criar demandas Vinculadas";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public void Validv_Contratada_codigo( wwpbaseobjects.SdtWWPContext GX_Parm1 ,
                                            GXCombobox dynGX_Parm2 ,
                                            GXCombobox dynGX_Parm3 )
      {
         AV20Context = GX_Parm1;
         dynavContratada_codigo = dynGX_Parm2;
         AV21Contratada_Codigo = (int)(NumberUtil.Val( dynavContratada_codigo.CurrentValue, "."));
         dynavContrato_codigo = dynGX_Parm3;
         AV23Contrato_Codigo = (int)(NumberUtil.Val( dynavContrato_codigo.CurrentValue, "."));
         GXVvCONTRATO_CODIGO_htmlTO2( AV20Context, AV21Contratada_Codigo) ;
         dynload_actions( ) ;
         if ( dynavContrato_codigo.ItemCount > 0 )
         {
            AV23Contrato_Codigo = (int)(NumberUtil.Val( dynavContrato_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV23Contrato_Codigo), 6, 0))), "."));
         }
         dynavContrato_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23Contrato_Codigo), 6, 0));
         isValidOutput.Add(dynavContrato_codigo);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E13TO2',iparms:[{av:'AV7CheckRequiredFieldsResult',fld:'vCHECKREQUIREDFIELDSRESULT',pic:'',nv:false},{av:'AV21Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV23Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13ContagemResultado_DataEntrega',fld:'vCONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'AV45ContagemResultado_Agrupador',fld:'vCONTAGEMRESULTADO_AGRUPADOR',pic:'@!',nv:''},{av:'AV47IsModificarAgrupador',fld:'vISMODIFICARAGRUPADOR',pic:'9',nv:0}],oparms:[{av:'AV7CheckRequiredFieldsResult',fld:'vCHECKREQUIREDFIELDSRESULT',pic:'',nv:false},{av:'Innewwindow1_Target',ctrl:'INNEWWINDOW1',prop:'Target'},{av:'Dvelop_confirmpanel_Title',ctrl:'DVELOP_CONFIRMPANEL',prop:'Title'},{av:'Dvelop_confirmpanel_Confirmationtext',ctrl:'DVELOP_CONFIRMPANEL',prop:'ConfirmationText'},{av:'Dvelop_confirmpanel_Yesbuttoncaption',ctrl:'DVELOP_CONFIRMPANEL',prop:'YesButtonCaption'},{av:'Dvelop_confirmpanel_Confirmtype',ctrl:'DVELOP_CONFIRMPANEL',prop:'ConfirmType'},{av:'tblTable2_Visible',ctrl:'TABLE2',prop:'Visible'},{av:'cellIsmodificaragrupador_cell_Class',ctrl:'ISMODIFICARAGRUPADOR_CELL',prop:'Class'}]}");
         setEventMetadata("VCONTRATO_CODIGO.CLICK","{handler:'E15TO2',iparms:[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV23Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',nv:false},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A2107Servico_Identificacao',fld:'SERVICO_IDENTIFICACAO',pic:'',nv:''}],oparms:[{av:'AV24ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VCONTRATADA_CODIGO.CLICK","{handler:'E16TO2',iparms:[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV23Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',nv:false},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A2107Servico_Identificacao',fld:'SERVICO_IDENTIFICACAO',pic:'',nv:''}],oparms:[{av:'AV45ContagemResultado_Agrupador',fld:'vCONTAGEMRESULTADO_AGRUPADOR',pic:'@!',nv:''},{av:'AV24ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("DVELOP_CONFIRMPANEL.CLOSE","{handler:'E11TO2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV20Context = new wwpbaseobjects.SdtWWPContext(context);
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A2107Servico_Identificacao = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00TO2_A74Contrato_Codigo = new int[1] ;
         H00TO2_A39Contratada_Codigo = new int[1] ;
         H00TO2_A438Contratada_Sigla = new String[] {""} ;
         H00TO2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00TO2_A43Contratada_Ativo = new bool[] {false} ;
         H00TO2_A92Contrato_Ativo = new bool[] {false} ;
         H00TO3_A74Contrato_Codigo = new int[1] ;
         H00TO3_A77Contrato_Numero = new String[] {""} ;
         H00TO3_A75Contrato_AreaTrabalhoCod = new int[1] ;
         H00TO3_A92Contrato_Ativo = new bool[] {false} ;
         H00TO3_A39Contratada_Codigo = new int[1] ;
         AV13ContagemResultado_DataEntrega = DateTime.MinValue;
         AV45ContagemResultado_Agrupador = "";
         AV8clientId = "";
         AV42webnotification = new SdtWebNotification(context);
         AV9CodigosOS = new GxSimpleCollection();
         AV43WebSession = context.GetSession();
         H00TO4_A155Servico_Codigo = new int[1] ;
         H00TO4_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00TO4_A74Contrato_Codigo = new int[1] ;
         H00TO4_A160ContratoServicos_Codigo = new int[1] ;
         H00TO4_A608Servico_Nome = new String[] {""} ;
         H00TO4_A605Servico_Sigla = new String[] {""} ;
         A608Servico_Nome = "";
         A605Servico_Sigla = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtnenter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         lblTextblockismodificaragrupador_Jsonclick = "";
         lblTextblockcontratada_codigo_Jsonclick = "";
         lblTextblockcontrato_codigo_Jsonclick = "";
         lblTextblockcontratoservicos_codigo_Jsonclick = "";
         lblTextblockcontagemresultado_dataentrega_Jsonclick = "";
         lblTextblockcontagemresultado_agrupador_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_demandasvinculadas__default(),
            new Object[][] {
                new Object[] {
               H00TO2_A74Contrato_Codigo, H00TO2_A39Contratada_Codigo, H00TO2_A438Contratada_Sigla, H00TO2_A52Contratada_AreaTrabalhoCod, H00TO2_A43Contratada_Ativo, H00TO2_A92Contrato_Ativo
               }
               , new Object[] {
               H00TO3_A74Contrato_Codigo, H00TO3_A77Contrato_Numero, H00TO3_A75Contrato_AreaTrabalhoCod, H00TO3_A92Contrato_Ativo, H00TO3_A39Contratada_Codigo
               }
               , new Object[] {
               H00TO4_A155Servico_Codigo, H00TO4_A638ContratoServicos_Ativo, H00TO4_A74Contrato_Codigo, H00TO4_A160ContratoServicos_Codigo, H00TO4_A608Servico_Nome, H00TO4_A605Servico_Sigla
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV47IsModificarAgrupador ;
      private short nGXWrapped ;
      private short wbTemp ;
      private int AV21Contratada_Codigo ;
      private int A74Contrato_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int AV24ContratoServicos_Codigo ;
      private int gxdynajaxindex ;
      private int AV23Contrato_Codigo ;
      private int tblTable2_Visible ;
      private int A155Servico_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String Innewwindow1_Target ;
      private String Dvelop_confirmpanel_Width ;
      private String Dvelop_confirmpanel_Height ;
      private String Dvelop_confirmpanel_Title ;
      private String Dvelop_confirmpanel_Confirmationtext ;
      private String Dvelop_confirmpanel_Yesbuttoncaption ;
      private String Dvelop_confirmpanel_Cancelbuttoncaption ;
      private String Dvelop_confirmpanel_Confirmtype ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String dynavContratada_codigo_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String dynavContrato_codigo_Internalname ;
      private String cmbavContratoservicos_codigo_Internalname ;
      private String edtavContagemresultado_dataentrega_Internalname ;
      private String AV45ContagemResultado_Agrupador ;
      private String edtavContagemresultado_agrupador_Internalname ;
      private String cmbavIsmodificaragrupador_Internalname ;
      private String AV8clientId ;
      private String tblTable2_Internalname ;
      private String cellIsmodificaragrupador_cell_Class ;
      private String cellIsmodificaragrupador_cell_Internalname ;
      private String A608Servico_Nome ;
      private String A605Servico_Sigla ;
      private String Innewwindow1_Internalname ;
      private String Dvelop_confirmpanel_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableinnewwindow_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtnenter_Internalname ;
      private String bttBtnenter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockismodificaragrupador_Internalname ;
      private String lblTextblockismodificaragrupador_Jsonclick ;
      private String cmbavIsmodificaragrupador_Jsonclick ;
      private String tblTable_Internalname ;
      private String lblTextblockcontratada_codigo_Internalname ;
      private String lblTextblockcontratada_codigo_Jsonclick ;
      private String dynavContratada_codigo_Jsonclick ;
      private String lblTextblockcontrato_codigo_Internalname ;
      private String lblTextblockcontrato_codigo_Jsonclick ;
      private String dynavContrato_codigo_Jsonclick ;
      private String lblTextblockcontratoservicos_codigo_Internalname ;
      private String lblTextblockcontratoservicos_codigo_Jsonclick ;
      private String cmbavContratoservicos_codigo_Jsonclick ;
      private String lblTextblockcontagemresultado_dataentrega_Internalname ;
      private String lblTextblockcontagemresultado_dataentrega_Jsonclick ;
      private String edtavContagemresultado_dataentrega_Jsonclick ;
      private String lblTextblockcontagemresultado_agrupador_Internalname ;
      private String lblTextblockcontagemresultado_agrupador_Jsonclick ;
      private String edtavContagemresultado_agrupador_Jsonclick ;
      private String Dvpanel_tableattributes_Internalname ;
      private DateTime AV13ContagemResultado_DataEntrega ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool AV7CheckRequiredFieldsResult ;
      private bool A638ContratoServicos_Ativo ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private String A2107Servico_Identificacao ;
      private IGxSession AV43WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavContratada_codigo ;
      private GXCombobox dynavContrato_codigo ;
      private GXCombobox cmbavContratoservicos_codigo ;
      private GXCombobox cmbavIsmodificaragrupador ;
      private IDataStoreProvider pr_default ;
      private int[] H00TO2_A74Contrato_Codigo ;
      private int[] H00TO2_A39Contratada_Codigo ;
      private String[] H00TO2_A438Contratada_Sigla ;
      private int[] H00TO2_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00TO2_A43Contratada_Ativo ;
      private bool[] H00TO2_A92Contrato_Ativo ;
      private int[] H00TO3_A74Contrato_Codigo ;
      private String[] H00TO3_A77Contrato_Numero ;
      private int[] H00TO3_A75Contrato_AreaTrabalhoCod ;
      private bool[] H00TO3_A92Contrato_Ativo ;
      private int[] H00TO3_A39Contratada_Codigo ;
      private int[] H00TO4_A155Servico_Codigo ;
      private bool[] H00TO4_A638ContratoServicos_Ativo ;
      private int[] H00TO4_A74Contrato_Codigo ;
      private int[] H00TO4_A160ContratoServicos_Codigo ;
      private String[] H00TO4_A608Servico_Nome ;
      private String[] H00TO4_A605Servico_Sigla ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV9CodigosOS ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV20Context ;
      private SdtWebNotification AV42webnotification ;
   }

   public class wp_demandasvinculadas__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00TO2 ;
          prmH00TO2 = new Object[] {
          new Object[] {"@AV20Cont_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00TO3 ;
          prmH00TO3 = new Object[] {
          new Object[] {"@AV20Cont_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00TO4 ;
          prmH00TO4 = new Object[] {
          new Object[] {"@AV23Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00TO2", "SELECT T1.[Contrato_Codigo], T1.[Contratada_Codigo], T2.[Contratada_Sigla], T2.[Contratada_AreaTrabalhoCod], T2.[Contratada_Ativo], T1.[Contrato_Ativo] FROM ([Contrato] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[Contratada_Codigo]) WHERE (T2.[Contratada_AreaTrabalhoCod] = @AV20Cont_1Areatrabalho_codigo) AND (T2.[Contratada_Ativo] = 1) AND (T1.[Contrato_Ativo] = 1) ORDER BY T2.[Contratada_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TO2,0,0,true,false )
             ,new CursorDef("H00TO3", "SELECT [Contrato_Codigo], [Contrato_Numero], [Contrato_AreaTrabalhoCod], [Contrato_Ativo], [Contratada_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE ([Contrato_AreaTrabalhoCod] = @AV20Cont_1Areatrabalho_codigo) AND ([Contrato_Ativo] = 1) AND ([Contratada_Codigo] = @AV21Contratada_Codigo) ORDER BY [Contrato_Numero] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TO3,0,0,true,false )
             ,new CursorDef("H00TO4", "SELECT T1.[Servico_Codigo], T1.[ContratoServicos_Ativo], T1.[Contrato_Codigo], T1.[ContratoServicos_Codigo], T2.[Servico_Nome], T2.[Servico_Sigla] FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) WHERE (T1.[Contrato_Codigo] = @AV23Contrato_Codigo) AND (T1.[ContratoServicos_Ativo] = 1) ORDER BY T1.[Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TO4,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.getBool(5) ;
                ((bool[]) buf[5])[0] = rslt.getBool(6) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 15) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
