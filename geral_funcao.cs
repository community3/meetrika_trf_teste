/*
               File: Geral_Funcao
        Description: Funcao
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:29:55.33
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class geral_funcao : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vFUNCAO_UOCOD") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLVvFUNCAO_UOCOD2784( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_18") == 0 )
         {
            A619GrupoFuncao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A619GrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A619GrupoFuncao_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_18( A619GrupoFuncao_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_19") == 0 )
         {
            A635Funcao_UOCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n635Funcao_UOCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A635Funcao_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A635Funcao_UOCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_19( A635Funcao_UOCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Funcao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Funcao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Funcao_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFUNCAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Funcao_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynGrupoFuncao_Codigo.Name = "GRUPOFUNCAO_CODIGO";
         dynGrupoFuncao_Codigo.WebTags = "";
         dynGrupoFuncao_Codigo.removeAllItems();
         /* Using cursor T00276 */
         pr_default.execute(4);
         while ( (pr_default.getStatus(4) != 101) )
         {
            dynGrupoFuncao_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T00276_A619GrupoFuncao_Codigo[0]), 6, 0)), T00276_A620GrupoFuncao_Nome[0], 0);
            pr_default.readNext(4);
         }
         pr_default.close(4);
         if ( dynGrupoFuncao_Codigo.ItemCount > 0 )
         {
            A619GrupoFuncao_Codigo = (int)(NumberUtil.Val( dynGrupoFuncao_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A619GrupoFuncao_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A619GrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A619GrupoFuncao_Codigo), 6, 0)));
         }
         dynavFuncao_uocod.Name = "vFUNCAO_UOCOD";
         dynavFuncao_uocod.WebTags = "";
         chkFuncao_Ativo.Name = "FUNCAO_ATIVO";
         chkFuncao_Ativo.WebTags = "";
         chkFuncao_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkFuncao_Ativo_Internalname, "TitleCaption", chkFuncao_Ativo.Caption);
         chkFuncao_Ativo.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Funcao", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = dynGrupoFuncao_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public geral_funcao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public geral_funcao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Funcao_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Funcao_Codigo = aP1_Funcao_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynGrupoFuncao_Codigo = new GXCombobox();
         dynavFuncao_uocod = new GXCombobox();
         chkFuncao_Ativo = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynGrupoFuncao_Codigo.ItemCount > 0 )
         {
            A619GrupoFuncao_Codigo = (int)(NumberUtil.Val( dynGrupoFuncao_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A619GrupoFuncao_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A619GrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A619GrupoFuncao_Codigo), 6, 0)));
         }
         if ( dynavFuncao_uocod.ItemCount > 0 )
         {
            AV14Funcao_UOCod = (int)(NumberUtil.Val( dynavFuncao_uocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14Funcao_UOCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Funcao_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Funcao_UOCod), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2784( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2784e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncao_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A621Funcao_Codigo), 6, 0, ",", "")), ((edtFuncao_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A621Funcao_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A621Funcao_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncao_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtFuncao_Codigo_Visible, edtFuncao_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Geral_Funcao.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2784( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2784( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2784e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_41_2784( true) ;
         }
         return  ;
      }

      protected void wb_table3_41_2784e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2784e( true) ;
         }
         else
         {
            wb_table1_2_2784e( false) ;
         }
      }

      protected void wb_table3_41_2784( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Geral_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Geral_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Geral_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_41_2784e( true) ;
         }
         else
         {
            wb_table3_41_2784e( false) ;
         }
      }

      protected void wb_table2_5_2784( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_2784( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_2784e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2784e( true) ;
         }
         else
         {
            wb_table2_5_2784e( false) ;
         }
      }

      protected void wb_table4_13_2784( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockgrupofuncao_codigo_Internalname, "Grupo de Fun��o", "", "", lblTextblockgrupofuncao_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Geral_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynGrupoFuncao_Codigo, dynGrupoFuncao_Codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A619GrupoFuncao_Codigo), 6, 0)), 1, dynGrupoFuncao_Codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynGrupoFuncao_Codigo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_Geral_Funcao.htm");
            dynGrupoFuncao_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A619GrupoFuncao_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynGrupoFuncao_Codigo_Internalname, "Values", (String)(dynGrupoFuncao_Codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncao_uocod_Internalname, "Unidade Organizacional", "", "", lblTextblockfuncao_uocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Geral_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_23_2784( true) ;
         }
         return  ;
      }

      protected void wb_table5_23_2784e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncao_nome_Internalname, "Nome", "", "", lblTextblockfuncao_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Geral_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFuncao_Nome_Internalname, A622Funcao_Nome, StringUtil.RTrim( context.localUtil.Format( A622Funcao_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncao_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtFuncao_Nome_Enabled, 0, "text", "", 380, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncao_ativo_Internalname, "Ativa?", "", "", lblTextblockfuncao_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockfuncao_ativo_Visible, 1, 0, "HLP_Geral_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkFuncao_Ativo_Internalname, StringUtil.BoolToStr( A630Funcao_Ativo), "", "", chkFuncao_Ativo.Visible, chkFuncao_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(38, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_2784e( true) ;
         }
         else
         {
            wb_table4_13_2784e( false) ;
         }
      }

      protected void wb_table5_23_2784( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedfuncao_uocod_Internalname, tblTablemergedfuncao_uocod_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavFuncao_uocod, dynavFuncao_uocod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14Funcao_UOCod), 6, 0)), 1, dynavFuncao_uocod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynavFuncao_uocod.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,26);\"", "", true, "HLP_Geral_Funcao.htm");
            dynavFuncao_uocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14Funcao_UOCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavFuncao_uocod_Internalname, "Values", (String)(dynavFuncao_uocod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgSelectuo_Internalname, context.GetImagePath( "f5b04895-0024-488b-8e3b-b687ca4598ee", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgSelectuo_Visible, 1, "", "Selecionar Unidade Organizacional", 0, 0, 0, "px", 0, "px", 0, 0, 7, imgSelectuo_Jsonclick, "'"+""+"'"+",false,"+"'"+"e112784_client"+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Geral_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_23_2784e( true) ;
         }
         else
         {
            wb_table5_23_2784e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12272 */
         E12272 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               dynGrupoFuncao_Codigo.CurrentValue = cgiGet( dynGrupoFuncao_Codigo_Internalname);
               A619GrupoFuncao_Codigo = (int)(NumberUtil.Val( cgiGet( dynGrupoFuncao_Codigo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A619GrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A619GrupoFuncao_Codigo), 6, 0)));
               dynavFuncao_uocod.CurrentValue = cgiGet( dynavFuncao_uocod_Internalname);
               AV14Funcao_UOCod = (int)(NumberUtil.Val( cgiGet( dynavFuncao_uocod_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Funcao_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Funcao_UOCod), 6, 0)));
               A622Funcao_Nome = StringUtil.Upper( cgiGet( edtFuncao_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A622Funcao_Nome", A622Funcao_Nome);
               A630Funcao_Ativo = StringUtil.StrToBool( cgiGet( chkFuncao_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A630Funcao_Ativo", A630Funcao_Ativo);
               A621Funcao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncao_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A621Funcao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A621Funcao_Codigo), 6, 0)));
               /* Read saved values. */
               Z621Funcao_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z621Funcao_Codigo"), ",", "."));
               Z622Funcao_Nome = cgiGet( "Z622Funcao_Nome");
               Z623Funcao_GTO = StringUtil.StrToBool( cgiGet( "Z623Funcao_GTO"));
               Z630Funcao_Ativo = StringUtil.StrToBool( cgiGet( "Z630Funcao_Ativo"));
               Z619GrupoFuncao_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z619GrupoFuncao_Codigo"), ",", "."));
               Z635Funcao_UOCod = (int)(context.localUtil.CToN( cgiGet( "Z635Funcao_UOCod"), ",", "."));
               n635Funcao_UOCod = ((0==A635Funcao_UOCod) ? true : false);
               A623Funcao_GTO = StringUtil.StrToBool( cgiGet( "Z623Funcao_GTO"));
               A635Funcao_UOCod = (int)(context.localUtil.CToN( cgiGet( "Z635Funcao_UOCod"), ",", "."));
               n635Funcao_UOCod = false;
               n635Funcao_UOCod = ((0==A635Funcao_UOCod) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N619GrupoFuncao_Codigo = (int)(context.localUtil.CToN( cgiGet( "N619GrupoFuncao_Codigo"), ",", "."));
               N635Funcao_UOCod = (int)(context.localUtil.CToN( cgiGet( "N635Funcao_UOCod"), ",", "."));
               n635Funcao_UOCod = ((0==A635Funcao_UOCod) ? true : false);
               AV7Funcao_Codigo = (int)(context.localUtil.CToN( cgiGet( "vFUNCAO_CODIGO"), ",", "."));
               AV11Insert_GrupoFuncao_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_GRUPOFUNCAO_CODIGO"), ",", "."));
               AV13Insert_Funcao_UOCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_FUNCAO_UOCOD"), ",", "."));
               A635Funcao_UOCod = (int)(context.localUtil.CToN( cgiGet( "FUNCAO_UOCOD"), ",", "."));
               n635Funcao_UOCod = ((0==A635Funcao_UOCod) ? true : false);
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A623Funcao_GTO = StringUtil.StrToBool( cgiGet( "FUNCAO_GTO"));
               A636Funcao_UONom = cgiGet( "FUNCAO_UONOM");
               n636Funcao_UONom = false;
               AV15Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Geral_Funcao";
               A621Funcao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncao_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A621Funcao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A621Funcao_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A621Funcao_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A623Funcao_GTO);
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A621Funcao_Codigo != Z621Funcao_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("geral_funcao:[SecurityCheckFailed value for]"+"Funcao_Codigo:"+context.localUtil.Format( (decimal)(A621Funcao_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("geral_funcao:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("geral_funcao:[SecurityCheckFailed value for]"+"Funcao_GTO:"+StringUtil.BoolToStr( A623Funcao_GTO));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A621Funcao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A621Funcao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A621Funcao_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode84 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode84;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound84 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_270( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "FUNCAO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtFuncao_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12272 */
                           E12272 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13272 */
                           E13272 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E13272 */
            E13272 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2784( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes2784( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavFuncao_uocod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavFuncao_uocod.Enabled), 5, 0)));
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_270( )
      {
         BeforeValidate2784( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2784( ) ;
            }
            else
            {
               CheckExtendedTable2784( ) ;
               CloseExtendedTableCursors2784( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption270( )
      {
      }

      protected void E12272( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV15Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV16GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GXV1), 8, 0)));
            while ( AV16GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV16GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "GrupoFuncao_Codigo") == 0 )
               {
                  AV11Insert_GrupoFuncao_Codigo = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_GrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_GrupoFuncao_Codigo), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Funcao_UOCod") == 0 )
               {
                  AV13Insert_Funcao_UOCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Insert_Funcao_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Insert_Funcao_UOCod), 6, 0)));
               }
               AV16GXV1 = (int)(AV16GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GXV1), 8, 0)));
            }
         }
         edtFuncao_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncao_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncao_Codigo_Visible), 5, 0)));
         AV14Funcao_UOCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Funcao_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Funcao_UOCod), 6, 0)));
      }

      protected void E13272( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwgeral_funcao.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM2784( short GX_JID )
      {
         if ( ( GX_JID == 17 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z622Funcao_Nome = T00273_A622Funcao_Nome[0];
               Z623Funcao_GTO = T00273_A623Funcao_GTO[0];
               Z630Funcao_Ativo = T00273_A630Funcao_Ativo[0];
               Z619GrupoFuncao_Codigo = T00273_A619GrupoFuncao_Codigo[0];
               Z635Funcao_UOCod = T00273_A635Funcao_UOCod[0];
            }
            else
            {
               Z622Funcao_Nome = A622Funcao_Nome;
               Z623Funcao_GTO = A623Funcao_GTO;
               Z630Funcao_Ativo = A630Funcao_Ativo;
               Z619GrupoFuncao_Codigo = A619GrupoFuncao_Codigo;
               Z635Funcao_UOCod = A635Funcao_UOCod;
            }
         }
         if ( GX_JID == -17 )
         {
            Z621Funcao_Codigo = A621Funcao_Codigo;
            Z622Funcao_Nome = A622Funcao_Nome;
            Z623Funcao_GTO = A623Funcao_GTO;
            Z630Funcao_Ativo = A630Funcao_Ativo;
            Z619GrupoFuncao_Codigo = A619GrupoFuncao_Codigo;
            Z635Funcao_UOCod = A635Funcao_UOCod;
            Z636Funcao_UONom = A636Funcao_UONom;
         }
      }

      protected void standaloneNotModal( )
      {
         GXVvFUNCAO_UOCOD_html2784( ) ;
         edtFuncao_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncao_Codigo_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         AV15Pgmname = "Geral_Funcao";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Pgmname", AV15Pgmname);
         edtFuncao_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncao_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7Funcao_Codigo) )
         {
            A621Funcao_Codigo = AV7Funcao_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A621Funcao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A621Funcao_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_GrupoFuncao_Codigo) )
         {
            dynGrupoFuncao_Codigo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynGrupoFuncao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynGrupoFuncao_Codigo.Enabled), 5, 0)));
         }
         else
         {
            dynGrupoFuncao_Codigo.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynGrupoFuncao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynGrupoFuncao_Codigo.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         chkFuncao_Ativo.Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkFuncao_Ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkFuncao_Ativo.Visible), 5, 0)));
         lblTextblockfuncao_ativo_Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockfuncao_ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockfuncao_ativo_Visible), 5, 0)));
         imgSelectuo_Visible = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgSelectuo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgSelectuo_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_Funcao_UOCod) )
         {
            A635Funcao_UOCod = AV13Insert_Funcao_UOCod;
            n635Funcao_UOCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A635Funcao_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A635Funcao_UOCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_GrupoFuncao_Codigo) )
         {
            A619GrupoFuncao_Codigo = AV11Insert_GrupoFuncao_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A619GrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A619GrupoFuncao_Codigo), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A630Funcao_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A630Funcao_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A630Funcao_Ativo", A630Funcao_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T00275 */
            pr_default.execute(3, new Object[] {n635Funcao_UOCod, A635Funcao_UOCod});
            A636Funcao_UONom = T00275_A636Funcao_UONom[0];
            n636Funcao_UONom = T00275_n636Funcao_UONom[0];
            pr_default.close(3);
         }
      }

      protected void Load2784( )
      {
         /* Using cursor T00277 */
         pr_default.execute(5, new Object[] {A621Funcao_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound84 = 1;
            A622Funcao_Nome = T00277_A622Funcao_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A622Funcao_Nome", A622Funcao_Nome);
            A623Funcao_GTO = T00277_A623Funcao_GTO[0];
            A636Funcao_UONom = T00277_A636Funcao_UONom[0];
            n636Funcao_UONom = T00277_n636Funcao_UONom[0];
            A630Funcao_Ativo = T00277_A630Funcao_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A630Funcao_Ativo", A630Funcao_Ativo);
            A619GrupoFuncao_Codigo = T00277_A619GrupoFuncao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A619GrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A619GrupoFuncao_Codigo), 6, 0)));
            A635Funcao_UOCod = T00277_A635Funcao_UOCod[0];
            n635Funcao_UOCod = T00277_n635Funcao_UOCod[0];
            ZM2784( -17) ;
         }
         pr_default.close(5);
         OnLoadActions2784( ) ;
      }

      protected void OnLoadActions2784( )
      {
         if ( (0==AV14Funcao_UOCod) && ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV14Funcao_UOCod = A635Funcao_UOCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Funcao_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Funcao_UOCod), 6, 0)));
         }
      }

      protected void CheckExtendedTable2784( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         if ( (0==AV14Funcao_UOCod) && ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV14Funcao_UOCod = A635Funcao_UOCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Funcao_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Funcao_UOCod), 6, 0)));
         }
         /* Using cursor T00274 */
         pr_default.execute(2, new Object[] {A619GrupoFuncao_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Grupo de Func�es'.", "ForeignKeyNotFound", 1, "GRUPOFUNCAO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynGrupoFuncao_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
         /* Using cursor T00275 */
         pr_default.execute(3, new Object[] {n635Funcao_UOCod, A635Funcao_UOCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A635Funcao_UOCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Funcao Unidade Organizacional'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A636Funcao_UONom = T00275_A636Funcao_UONom[0];
         n636Funcao_UONom = T00275_n636Funcao_UONom[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors2784( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_18( int A619GrupoFuncao_Codigo )
      {
         /* Using cursor T00278 */
         pr_default.execute(6, new Object[] {A619GrupoFuncao_Codigo});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Grupo de Func�es'.", "ForeignKeyNotFound", 1, "GRUPOFUNCAO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynGrupoFuncao_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void gxLoad_19( int A635Funcao_UOCod )
      {
         /* Using cursor T00279 */
         pr_default.execute(7, new Object[] {n635Funcao_UOCod, A635Funcao_UOCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            if ( ! ( (0==A635Funcao_UOCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Funcao Unidade Organizacional'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A636Funcao_UONom = T00279_A636Funcao_UONom[0];
         n636Funcao_UONom = T00279_n636Funcao_UONom[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A636Funcao_UONom))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void GetKey2784( )
      {
         /* Using cursor T002710 */
         pr_default.execute(8, new Object[] {A621Funcao_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound84 = 1;
         }
         else
         {
            RcdFound84 = 0;
         }
         pr_default.close(8);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00273 */
         pr_default.execute(1, new Object[] {A621Funcao_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2784( 17) ;
            RcdFound84 = 1;
            A621Funcao_Codigo = T00273_A621Funcao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A621Funcao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A621Funcao_Codigo), 6, 0)));
            A622Funcao_Nome = T00273_A622Funcao_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A622Funcao_Nome", A622Funcao_Nome);
            A623Funcao_GTO = T00273_A623Funcao_GTO[0];
            A630Funcao_Ativo = T00273_A630Funcao_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A630Funcao_Ativo", A630Funcao_Ativo);
            A619GrupoFuncao_Codigo = T00273_A619GrupoFuncao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A619GrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A619GrupoFuncao_Codigo), 6, 0)));
            A635Funcao_UOCod = T00273_A635Funcao_UOCod[0];
            n635Funcao_UOCod = T00273_n635Funcao_UOCod[0];
            Z621Funcao_Codigo = A621Funcao_Codigo;
            sMode84 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load2784( ) ;
            if ( AnyError == 1 )
            {
               RcdFound84 = 0;
               InitializeNonKey2784( ) ;
            }
            Gx_mode = sMode84;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound84 = 0;
            InitializeNonKey2784( ) ;
            sMode84 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode84;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2784( ) ;
         if ( RcdFound84 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound84 = 0;
         /* Using cursor T002711 */
         pr_default.execute(9, new Object[] {A621Funcao_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T002711_A621Funcao_Codigo[0] < A621Funcao_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T002711_A621Funcao_Codigo[0] > A621Funcao_Codigo ) ) )
            {
               A621Funcao_Codigo = T002711_A621Funcao_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A621Funcao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A621Funcao_Codigo), 6, 0)));
               RcdFound84 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void move_previous( )
      {
         RcdFound84 = 0;
         /* Using cursor T002712 */
         pr_default.execute(10, new Object[] {A621Funcao_Codigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            while ( (pr_default.getStatus(10) != 101) && ( ( T002712_A621Funcao_Codigo[0] > A621Funcao_Codigo ) ) )
            {
               pr_default.readNext(10);
            }
            if ( (pr_default.getStatus(10) != 101) && ( ( T002712_A621Funcao_Codigo[0] < A621Funcao_Codigo ) ) )
            {
               A621Funcao_Codigo = T002712_A621Funcao_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A621Funcao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A621Funcao_Codigo), 6, 0)));
               RcdFound84 = 1;
            }
         }
         pr_default.close(10);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2784( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = dynGrupoFuncao_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2784( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound84 == 1 )
            {
               if ( A621Funcao_Codigo != Z621Funcao_Codigo )
               {
                  A621Funcao_Codigo = Z621Funcao_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A621Funcao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A621Funcao_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "FUNCAO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtFuncao_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = dynGrupoFuncao_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update2784( ) ;
                  GX_FocusControl = dynGrupoFuncao_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A621Funcao_Codigo != Z621Funcao_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = dynGrupoFuncao_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2784( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "FUNCAO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtFuncao_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = dynGrupoFuncao_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2784( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A621Funcao_Codigo != Z621Funcao_Codigo )
         {
            A621Funcao_Codigo = Z621Funcao_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A621Funcao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A621Funcao_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "FUNCAO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtFuncao_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = dynGrupoFuncao_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency2784( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00272 */
            pr_default.execute(0, new Object[] {A621Funcao_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Geral_Funcao"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z622Funcao_Nome, T00272_A622Funcao_Nome[0]) != 0 ) || ( Z623Funcao_GTO != T00272_A623Funcao_GTO[0] ) || ( Z630Funcao_Ativo != T00272_A630Funcao_Ativo[0] ) || ( Z619GrupoFuncao_Codigo != T00272_A619GrupoFuncao_Codigo[0] ) || ( Z635Funcao_UOCod != T00272_A635Funcao_UOCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z622Funcao_Nome, T00272_A622Funcao_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("geral_funcao:[seudo value changed for attri]"+"Funcao_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z622Funcao_Nome);
                  GXUtil.WriteLogRaw("Current: ",T00272_A622Funcao_Nome[0]);
               }
               if ( Z623Funcao_GTO != T00272_A623Funcao_GTO[0] )
               {
                  GXUtil.WriteLog("geral_funcao:[seudo value changed for attri]"+"Funcao_GTO");
                  GXUtil.WriteLogRaw("Old: ",Z623Funcao_GTO);
                  GXUtil.WriteLogRaw("Current: ",T00272_A623Funcao_GTO[0]);
               }
               if ( Z630Funcao_Ativo != T00272_A630Funcao_Ativo[0] )
               {
                  GXUtil.WriteLog("geral_funcao:[seudo value changed for attri]"+"Funcao_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z630Funcao_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T00272_A630Funcao_Ativo[0]);
               }
               if ( Z619GrupoFuncao_Codigo != T00272_A619GrupoFuncao_Codigo[0] )
               {
                  GXUtil.WriteLog("geral_funcao:[seudo value changed for attri]"+"GrupoFuncao_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z619GrupoFuncao_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T00272_A619GrupoFuncao_Codigo[0]);
               }
               if ( Z635Funcao_UOCod != T00272_A635Funcao_UOCod[0] )
               {
                  GXUtil.WriteLog("geral_funcao:[seudo value changed for attri]"+"Funcao_UOCod");
                  GXUtil.WriteLogRaw("Old: ",Z635Funcao_UOCod);
                  GXUtil.WriteLogRaw("Current: ",T00272_A635Funcao_UOCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Geral_Funcao"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2784( )
      {
         BeforeValidate2784( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2784( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2784( 0) ;
            CheckOptimisticConcurrency2784( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2784( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2784( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002713 */
                     pr_default.execute(11, new Object[] {A622Funcao_Nome, A623Funcao_GTO, A630Funcao_Ativo, A619GrupoFuncao_Codigo, n635Funcao_UOCod, A635Funcao_UOCod});
                     A621Funcao_Codigo = T002713_A621Funcao_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A621Funcao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A621Funcao_Codigo), 6, 0)));
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("Geral_Funcao") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption270( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2784( ) ;
            }
            EndLevel2784( ) ;
         }
         CloseExtendedTableCursors2784( ) ;
      }

      protected void Update2784( )
      {
         BeforeValidate2784( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2784( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2784( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2784( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2784( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002714 */
                     pr_default.execute(12, new Object[] {A622Funcao_Nome, A623Funcao_GTO, A630Funcao_Ativo, A619GrupoFuncao_Codigo, n635Funcao_UOCod, A635Funcao_UOCod, A621Funcao_Codigo});
                     pr_default.close(12);
                     dsDefault.SmartCacheProvider.SetUpdated("Geral_Funcao") ;
                     if ( (pr_default.getStatus(12) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Geral_Funcao"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2784( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2784( ) ;
         }
         CloseExtendedTableCursors2784( ) ;
      }

      protected void DeferredUpdate2784( )
      {
      }

      protected void delete( )
      {
         BeforeValidate2784( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2784( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2784( ) ;
            AfterConfirm2784( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2784( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002715 */
                  pr_default.execute(13, new Object[] {A621Funcao_Codigo});
                  pr_default.close(13);
                  dsDefault.SmartCacheProvider.SetUpdated("Geral_Funcao") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode84 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel2784( ) ;
         Gx_mode = sMode84;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls2784( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T002716 */
            pr_default.execute(14, new Object[] {n635Funcao_UOCod, A635Funcao_UOCod});
            A636Funcao_UONom = T002716_A636Funcao_UONom[0];
            n636Funcao_UONom = T002716_n636Funcao_UONom[0];
            pr_default.close(14);
            if ( (0==AV14Funcao_UOCod) && ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV14Funcao_UOCod = A635Funcao_UOCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Funcao_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Funcao_UOCod), 6, 0)));
            }
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T002717 */
            pr_default.execute(15, new Object[] {A621Funcao_Codigo});
            if ( (pr_default.getStatus(15) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {" T237"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(15);
         }
      }

      protected void EndLevel2784( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2784( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(14);
            context.CommitDataStores( "Geral_Funcao");
            if ( AnyError == 0 )
            {
               ConfirmValues270( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(14);
            context.RollbackDataStores( "Geral_Funcao");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2784( )
      {
         /* Scan By routine */
         /* Using cursor T002718 */
         pr_default.execute(16);
         RcdFound84 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound84 = 1;
            A621Funcao_Codigo = T002718_A621Funcao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A621Funcao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A621Funcao_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2784( )
      {
         /* Scan next routine */
         pr_default.readNext(16);
         RcdFound84 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound84 = 1;
            A621Funcao_Codigo = T002718_A621Funcao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A621Funcao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A621Funcao_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd2784( )
      {
         pr_default.close(16);
      }

      protected void AfterConfirm2784( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2784( )
      {
         /* Before Insert Rules */
         A635Funcao_UOCod = AV14Funcao_UOCod;
         n635Funcao_UOCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A635Funcao_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A635Funcao_UOCod), 6, 0)));
      }

      protected void BeforeUpdate2784( )
      {
         /* Before Update Rules */
         A635Funcao_UOCod = AV14Funcao_UOCod;
         n635Funcao_UOCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A635Funcao_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A635Funcao_UOCod), 6, 0)));
      }

      protected void BeforeDelete2784( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2784( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2784( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2784( )
      {
         dynGrupoFuncao_Codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynGrupoFuncao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynGrupoFuncao_Codigo.Enabled), 5, 0)));
         dynavFuncao_uocod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavFuncao_uocod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavFuncao_uocod.Enabled), 5, 0)));
         edtFuncao_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncao_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncao_Nome_Enabled), 5, 0)));
         chkFuncao_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkFuncao_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkFuncao_Ativo.Enabled), 5, 0)));
         edtFuncao_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncao_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues270( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299295675");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("geral_funcao.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Funcao_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z621Funcao_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z621Funcao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z622Funcao_Nome", Z622Funcao_Nome);
         GxWebStd.gx_boolean_hidden_field( context, "Z623Funcao_GTO", Z623Funcao_GTO);
         GxWebStd.gx_boolean_hidden_field( context, "Z630Funcao_Ativo", Z630Funcao_Ativo);
         GxWebStd.gx_hidden_field( context, "Z619GrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z619GrupoFuncao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z635Funcao_UOCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z635Funcao_UOCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N619GrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A619GrupoFuncao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N635Funcao_UOCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A635Funcao_UOCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vFUNCAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Funcao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_GRUPOFUNCAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_GrupoFuncao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_FUNCAO_UOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Insert_Funcao_UOCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAO_UOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A635Funcao_UOCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "FUNCAO_GTO", A623Funcao_GTO);
         GxWebStd.gx_hidden_field( context, "FUNCAO_UONOM", StringUtil.RTrim( A636Funcao_UONom));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV15Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vFUNCAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Funcao_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Geral_Funcao";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A621Funcao_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A623Funcao_GTO);
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("geral_funcao:[SendSecurityCheck value for]"+"Funcao_Codigo:"+context.localUtil.Format( (decimal)(A621Funcao_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("geral_funcao:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("geral_funcao:[SendSecurityCheck value for]"+"Funcao_GTO:"+StringUtil.BoolToStr( A623Funcao_GTO));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("geral_funcao.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Funcao_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Geral_Funcao" ;
      }

      public override String GetPgmdesc( )
      {
         return "Funcao" ;
      }

      protected void InitializeNonKey2784( )
      {
         A619GrupoFuncao_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A619GrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A619GrupoFuncao_Codigo), 6, 0)));
         A635Funcao_UOCod = 0;
         n635Funcao_UOCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A635Funcao_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A635Funcao_UOCod), 6, 0)));
         A622Funcao_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A622Funcao_Nome", A622Funcao_Nome);
         A623Funcao_GTO = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A623Funcao_GTO", A623Funcao_GTO);
         A636Funcao_UONom = "";
         n636Funcao_UONom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A636Funcao_UONom", A636Funcao_UONom);
         A630Funcao_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A630Funcao_Ativo", A630Funcao_Ativo);
         Z622Funcao_Nome = "";
         Z623Funcao_GTO = false;
         Z630Funcao_Ativo = false;
         Z619GrupoFuncao_Codigo = 0;
         Z635Funcao_UOCod = 0;
      }

      protected void InitAll2784( )
      {
         A621Funcao_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A621Funcao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A621Funcao_Codigo), 6, 0)));
         InitializeNonKey2784( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A630Funcao_Ativo = i630Funcao_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A630Funcao_Ativo", A630Funcao_Ativo);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020529929575");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("geral_funcao.js", "?2020529929575");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockgrupofuncao_codigo_Internalname = "TEXTBLOCKGRUPOFUNCAO_CODIGO";
         dynGrupoFuncao_Codigo_Internalname = "GRUPOFUNCAO_CODIGO";
         lblTextblockfuncao_uocod_Internalname = "TEXTBLOCKFUNCAO_UOCOD";
         dynavFuncao_uocod_Internalname = "vFUNCAO_UOCOD";
         imgSelectuo_Internalname = "SELECTUO";
         tblTablemergedfuncao_uocod_Internalname = "TABLEMERGEDFUNCAO_UOCOD";
         lblTextblockfuncao_nome_Internalname = "TEXTBLOCKFUNCAO_NOME";
         edtFuncao_Nome_Internalname = "FUNCAO_NOME";
         lblTextblockfuncao_ativo_Internalname = "TEXTBLOCKFUNCAO_ATIVO";
         chkFuncao_Ativo_Internalname = "FUNCAO_ATIVO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtFuncao_Codigo_Internalname = "FUNCAO_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Fun��o";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Funcao";
         imgSelectuo_Visible = 1;
         dynavFuncao_uocod_Jsonclick = "";
         dynavFuncao_uocod.Enabled = 1;
         chkFuncao_Ativo.Enabled = 1;
         chkFuncao_Ativo.Visible = 1;
         lblTextblockfuncao_ativo_Visible = 1;
         edtFuncao_Nome_Jsonclick = "";
         edtFuncao_Nome_Enabled = 1;
         dynGrupoFuncao_Codigo_Jsonclick = "";
         dynGrupoFuncao_Codigo.Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtFuncao_Codigo_Jsonclick = "";
         edtFuncao_Codigo_Enabled = 0;
         edtFuncao_Codigo_Visible = 1;
         chkFuncao_Ativo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAGRUPOFUNCAO_CODIGO271( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAGRUPOFUNCAO_CODIGO_data271( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAGRUPOFUNCAO_CODIGO_html271( )
      {
         int gxdynajaxvalue ;
         GXDLAGRUPOFUNCAO_CODIGO_data271( ) ;
         gxdynajaxindex = 1;
         dynGrupoFuncao_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynGrupoFuncao_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAGRUPOFUNCAO_CODIGO_data271( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor T002719 */
         pr_default.execute(17);
         while ( (pr_default.getStatus(17) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T002719_A619GrupoFuncao_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(T002719_A620GrupoFuncao_Nome[0]);
            pr_default.readNext(17);
         }
         pr_default.close(17);
      }

      protected void GXDLVvFUNCAO_UOCOD2784( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvFUNCAO_UOCOD_data2784( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvFUNCAO_UOCOD_html2784( )
      {
         int gxdynajaxvalue ;
         GXDLVvFUNCAO_UOCOD_data2784( ) ;
         gxdynajaxindex = 1;
         dynavFuncao_uocod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavFuncao_uocod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLVvFUNCAO_UOCOD_data2784( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor T002720 */
         pr_default.execute(18);
         while ( (pr_default.getStatus(18) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T002720_A611UnidadeOrganizacional_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T002720_A612UnidadeOrganizacional_Nome[0]));
            pr_default.readNext(18);
         }
         pr_default.close(18);
      }

      public void Valid_Grupofuncao_codigo( GXCombobox dynGX_Parm1 )
      {
         dynGrupoFuncao_Codigo = dynGX_Parm1;
         A619GrupoFuncao_Codigo = (int)(NumberUtil.Val( dynGrupoFuncao_Codigo.CurrentValue, "."));
         /* Using cursor T002721 */
         pr_default.execute(19, new Object[] {A619GrupoFuncao_Codigo});
         if ( (pr_default.getStatus(19) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Grupo de Func�es'.", "ForeignKeyNotFound", 1, "GRUPOFUNCAO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynGrupoFuncao_Codigo_Internalname;
         }
         pr_default.close(19);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Funcao_Codigo',fld:'vFUNCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E13272',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("'DOSELECTUO'","{handler:'E112784',iparms:[{av:'AV14Funcao_UOCod',fld:'vFUNCAO_UOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV14Funcao_UOCod',fld:'vFUNCAO_UOCOD',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(19);
         pr_default.close(14);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z622Funcao_Nome = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         T00276_A619GrupoFuncao_Codigo = new int[1] ;
         T00276_A620GrupoFuncao_Nome = new String[] {""} ;
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockgrupofuncao_codigo_Jsonclick = "";
         lblTextblockfuncao_uocod_Jsonclick = "";
         lblTextblockfuncao_nome_Jsonclick = "";
         A622Funcao_Nome = "";
         lblTextblockfuncao_ativo_Jsonclick = "";
         imgSelectuo_Jsonclick = "";
         A636Funcao_UONom = "";
         AV15Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode84 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z636Funcao_UONom = "";
         T00275_A636Funcao_UONom = new String[] {""} ;
         T00275_n636Funcao_UONom = new bool[] {false} ;
         T00277_A621Funcao_Codigo = new int[1] ;
         T00277_A622Funcao_Nome = new String[] {""} ;
         T00277_A623Funcao_GTO = new bool[] {false} ;
         T00277_A636Funcao_UONom = new String[] {""} ;
         T00277_n636Funcao_UONom = new bool[] {false} ;
         T00277_A630Funcao_Ativo = new bool[] {false} ;
         T00277_A619GrupoFuncao_Codigo = new int[1] ;
         T00277_A635Funcao_UOCod = new int[1] ;
         T00277_n635Funcao_UOCod = new bool[] {false} ;
         T00274_A619GrupoFuncao_Codigo = new int[1] ;
         T00278_A619GrupoFuncao_Codigo = new int[1] ;
         T00279_A636Funcao_UONom = new String[] {""} ;
         T00279_n636Funcao_UONom = new bool[] {false} ;
         T002710_A621Funcao_Codigo = new int[1] ;
         T00273_A621Funcao_Codigo = new int[1] ;
         T00273_A622Funcao_Nome = new String[] {""} ;
         T00273_A623Funcao_GTO = new bool[] {false} ;
         T00273_A630Funcao_Ativo = new bool[] {false} ;
         T00273_A619GrupoFuncao_Codigo = new int[1] ;
         T00273_A635Funcao_UOCod = new int[1] ;
         T00273_n635Funcao_UOCod = new bool[] {false} ;
         T002711_A621Funcao_Codigo = new int[1] ;
         T002712_A621Funcao_Codigo = new int[1] ;
         T00272_A621Funcao_Codigo = new int[1] ;
         T00272_A622Funcao_Nome = new String[] {""} ;
         T00272_A623Funcao_GTO = new bool[] {false} ;
         T00272_A630Funcao_Ativo = new bool[] {false} ;
         T00272_A619GrupoFuncao_Codigo = new int[1] ;
         T00272_A635Funcao_UOCod = new int[1] ;
         T00272_n635Funcao_UOCod = new bool[] {false} ;
         T002713_A621Funcao_Codigo = new int[1] ;
         T002716_A636Funcao_UONom = new String[] {""} ;
         T002716_n636Funcao_UONom = new bool[] {false} ;
         T002717_A2175Equipe_Codigo = new int[1] ;
         T002717_A1Usuario_Codigo = new int[1] ;
         T002718_A621Funcao_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T002719_A619GrupoFuncao_Codigo = new int[1] ;
         T002719_A620GrupoFuncao_Nome = new String[] {""} ;
         T002720_A611UnidadeOrganizacional_Codigo = new int[1] ;
         T002720_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         T002720_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         T002721_A619GrupoFuncao_Codigo = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.geral_funcao__default(),
            new Object[][] {
                new Object[] {
               T00272_A621Funcao_Codigo, T00272_A622Funcao_Nome, T00272_A623Funcao_GTO, T00272_A630Funcao_Ativo, T00272_A619GrupoFuncao_Codigo, T00272_A635Funcao_UOCod, T00272_n635Funcao_UOCod
               }
               , new Object[] {
               T00273_A621Funcao_Codigo, T00273_A622Funcao_Nome, T00273_A623Funcao_GTO, T00273_A630Funcao_Ativo, T00273_A619GrupoFuncao_Codigo, T00273_A635Funcao_UOCod, T00273_n635Funcao_UOCod
               }
               , new Object[] {
               T00274_A619GrupoFuncao_Codigo
               }
               , new Object[] {
               T00275_A636Funcao_UONom, T00275_n636Funcao_UONom
               }
               , new Object[] {
               T00276_A619GrupoFuncao_Codigo, T00276_A620GrupoFuncao_Nome
               }
               , new Object[] {
               T00277_A621Funcao_Codigo, T00277_A622Funcao_Nome, T00277_A623Funcao_GTO, T00277_A636Funcao_UONom, T00277_n636Funcao_UONom, T00277_A630Funcao_Ativo, T00277_A619GrupoFuncao_Codigo, T00277_A635Funcao_UOCod, T00277_n635Funcao_UOCod
               }
               , new Object[] {
               T00278_A619GrupoFuncao_Codigo
               }
               , new Object[] {
               T00279_A636Funcao_UONom, T00279_n636Funcao_UONom
               }
               , new Object[] {
               T002710_A621Funcao_Codigo
               }
               , new Object[] {
               T002711_A621Funcao_Codigo
               }
               , new Object[] {
               T002712_A621Funcao_Codigo
               }
               , new Object[] {
               T002713_A621Funcao_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002716_A636Funcao_UONom, T002716_n636Funcao_UONom
               }
               , new Object[] {
               T002717_A2175Equipe_Codigo, T002717_A1Usuario_Codigo
               }
               , new Object[] {
               T002718_A621Funcao_Codigo
               }
               , new Object[] {
               T002719_A619GrupoFuncao_Codigo, T002719_A620GrupoFuncao_Nome
               }
               , new Object[] {
               T002720_A611UnidadeOrganizacional_Codigo, T002720_A612UnidadeOrganizacional_Nome, T002720_A629UnidadeOrganizacional_Ativo
               }
               , new Object[] {
               T002721_A619GrupoFuncao_Codigo
               }
            }
         );
         Z630Funcao_Ativo = true;
         A630Funcao_Ativo = true;
         i630Funcao_Ativo = true;
         AV15Pgmname = "Geral_Funcao";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short RcdFound84 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7Funcao_Codigo ;
      private int Z621Funcao_Codigo ;
      private int Z619GrupoFuncao_Codigo ;
      private int Z635Funcao_UOCod ;
      private int N619GrupoFuncao_Codigo ;
      private int N635Funcao_UOCod ;
      private int A619GrupoFuncao_Codigo ;
      private int A635Funcao_UOCod ;
      private int AV7Funcao_Codigo ;
      private int trnEnded ;
      private int AV14Funcao_UOCod ;
      private int A621Funcao_Codigo ;
      private int edtFuncao_Codigo_Enabled ;
      private int edtFuncao_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtFuncao_Nome_Enabled ;
      private int lblTextblockfuncao_ativo_Visible ;
      private int imgSelectuo_Visible ;
      private int AV11Insert_GrupoFuncao_Codigo ;
      private int AV13Insert_Funcao_UOCod ;
      private int AV16GXV1 ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String chkFuncao_Ativo_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String dynGrupoFuncao_Codigo_Internalname ;
      private String edtFuncao_Codigo_Internalname ;
      private String edtFuncao_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockgrupofuncao_codigo_Internalname ;
      private String lblTextblockgrupofuncao_codigo_Jsonclick ;
      private String dynGrupoFuncao_Codigo_Jsonclick ;
      private String lblTextblockfuncao_uocod_Internalname ;
      private String lblTextblockfuncao_uocod_Jsonclick ;
      private String lblTextblockfuncao_nome_Internalname ;
      private String lblTextblockfuncao_nome_Jsonclick ;
      private String edtFuncao_Nome_Internalname ;
      private String edtFuncao_Nome_Jsonclick ;
      private String lblTextblockfuncao_ativo_Internalname ;
      private String lblTextblockfuncao_ativo_Jsonclick ;
      private String tblTablemergedfuncao_uocod_Internalname ;
      private String dynavFuncao_uocod_Internalname ;
      private String dynavFuncao_uocod_Jsonclick ;
      private String imgSelectuo_Internalname ;
      private String imgSelectuo_Jsonclick ;
      private String A636Funcao_UONom ;
      private String AV15Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode84 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z636Funcao_UONom ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private bool Z623Funcao_GTO ;
      private bool Z630Funcao_Ativo ;
      private bool entryPointCalled ;
      private bool n635Funcao_UOCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A630Funcao_Ativo ;
      private bool A623Funcao_GTO ;
      private bool n636Funcao_UONom ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool i630Funcao_Ativo ;
      private String Z622Funcao_Nome ;
      private String A622Funcao_Nome ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IDataStoreProvider pr_default ;
      private int[] T00276_A619GrupoFuncao_Codigo ;
      private String[] T00276_A620GrupoFuncao_Nome ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynGrupoFuncao_Codigo ;
      private GXCombobox dynavFuncao_uocod ;
      private GXCheckbox chkFuncao_Ativo ;
      private String[] T00275_A636Funcao_UONom ;
      private bool[] T00275_n636Funcao_UONom ;
      private int[] T00277_A621Funcao_Codigo ;
      private String[] T00277_A622Funcao_Nome ;
      private bool[] T00277_A623Funcao_GTO ;
      private String[] T00277_A636Funcao_UONom ;
      private bool[] T00277_n636Funcao_UONom ;
      private bool[] T00277_A630Funcao_Ativo ;
      private int[] T00277_A619GrupoFuncao_Codigo ;
      private int[] T00277_A635Funcao_UOCod ;
      private bool[] T00277_n635Funcao_UOCod ;
      private int[] T00274_A619GrupoFuncao_Codigo ;
      private int[] T00278_A619GrupoFuncao_Codigo ;
      private String[] T00279_A636Funcao_UONom ;
      private bool[] T00279_n636Funcao_UONom ;
      private int[] T002710_A621Funcao_Codigo ;
      private int[] T00273_A621Funcao_Codigo ;
      private String[] T00273_A622Funcao_Nome ;
      private bool[] T00273_A623Funcao_GTO ;
      private bool[] T00273_A630Funcao_Ativo ;
      private int[] T00273_A619GrupoFuncao_Codigo ;
      private int[] T00273_A635Funcao_UOCod ;
      private bool[] T00273_n635Funcao_UOCod ;
      private int[] T002711_A621Funcao_Codigo ;
      private int[] T002712_A621Funcao_Codigo ;
      private int[] T00272_A621Funcao_Codigo ;
      private String[] T00272_A622Funcao_Nome ;
      private bool[] T00272_A623Funcao_GTO ;
      private bool[] T00272_A630Funcao_Ativo ;
      private int[] T00272_A619GrupoFuncao_Codigo ;
      private int[] T00272_A635Funcao_UOCod ;
      private bool[] T00272_n635Funcao_UOCod ;
      private int[] T002713_A621Funcao_Codigo ;
      private String[] T002716_A636Funcao_UONom ;
      private bool[] T002716_n636Funcao_UONom ;
      private int[] T002717_A2175Equipe_Codigo ;
      private int[] T002717_A1Usuario_Codigo ;
      private int[] T002718_A621Funcao_Codigo ;
      private int[] T002719_A619GrupoFuncao_Codigo ;
      private String[] T002719_A620GrupoFuncao_Nome ;
      private int[] T002720_A611UnidadeOrganizacional_Codigo ;
      private String[] T002720_A612UnidadeOrganizacional_Nome ;
      private bool[] T002720_A629UnidadeOrganizacional_Ativo ;
      private int[] T002721_A619GrupoFuncao_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class geral_funcao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00276 ;
          prmT00276 = new Object[] {
          } ;
          Object[] prmT00277 ;
          prmT00277 = new Object[] {
          new Object[] {"@Funcao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00274 ;
          prmT00274 = new Object[] {
          new Object[] {"@GrupoFuncao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00275 ;
          prmT00275 = new Object[] {
          new Object[] {"@Funcao_UOCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00278 ;
          prmT00278 = new Object[] {
          new Object[] {"@GrupoFuncao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00279 ;
          prmT00279 = new Object[] {
          new Object[] {"@Funcao_UOCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002710 ;
          prmT002710 = new Object[] {
          new Object[] {"@Funcao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00273 ;
          prmT00273 = new Object[] {
          new Object[] {"@Funcao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002711 ;
          prmT002711 = new Object[] {
          new Object[] {"@Funcao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002712 ;
          prmT002712 = new Object[] {
          new Object[] {"@Funcao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00272 ;
          prmT00272 = new Object[] {
          new Object[] {"@Funcao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002713 ;
          prmT002713 = new Object[] {
          new Object[] {"@Funcao_Nome",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Funcao_GTO",SqlDbType.Bit,4,0} ,
          new Object[] {"@Funcao_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@GrupoFuncao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Funcao_UOCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002714 ;
          prmT002714 = new Object[] {
          new Object[] {"@Funcao_Nome",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Funcao_GTO",SqlDbType.Bit,4,0} ,
          new Object[] {"@Funcao_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@GrupoFuncao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Funcao_UOCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Funcao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002715 ;
          prmT002715 = new Object[] {
          new Object[] {"@Funcao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002716 ;
          prmT002716 = new Object[] {
          new Object[] {"@Funcao_UOCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002717 ;
          prmT002717 = new Object[] {
          new Object[] {"@Funcao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002718 ;
          prmT002718 = new Object[] {
          } ;
          Object[] prmT002719 ;
          prmT002719 = new Object[] {
          } ;
          Object[] prmT002720 ;
          prmT002720 = new Object[] {
          } ;
          Object[] prmT002721 ;
          prmT002721 = new Object[] {
          new Object[] {"@GrupoFuncao_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00272", "SELECT [Funcao_Codigo], [Funcao_Nome], [Funcao_GTO], [Funcao_Ativo], [GrupoFuncao_Codigo], [Funcao_UOCod] AS Funcao_UOCod FROM [Geral_Funcao] WITH (UPDLOCK) WHERE [Funcao_Codigo] = @Funcao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00272,1,0,true,false )
             ,new CursorDef("T00273", "SELECT [Funcao_Codigo], [Funcao_Nome], [Funcao_GTO], [Funcao_Ativo], [GrupoFuncao_Codigo], [Funcao_UOCod] AS Funcao_UOCod FROM [Geral_Funcao] WITH (NOLOCK) WHERE [Funcao_Codigo] = @Funcao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00273,1,0,true,false )
             ,new CursorDef("T00274", "SELECT [GrupoFuncao_Codigo] FROM [Geral_Grupo_Funcao] WITH (NOLOCK) WHERE [GrupoFuncao_Codigo] = @GrupoFuncao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00274,1,0,true,false )
             ,new CursorDef("T00275", "SELECT [UnidadeOrganizacional_Nome] AS Funcao_UONom FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Codigo] = @Funcao_UOCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00275,1,0,true,false )
             ,new CursorDef("T00276", "SELECT [GrupoFuncao_Codigo], [GrupoFuncao_Nome] FROM [Geral_Grupo_Funcao] WITH (NOLOCK) ORDER BY [GrupoFuncao_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT00276,0,0,true,false )
             ,new CursorDef("T00277", "SELECT TM1.[Funcao_Codigo], TM1.[Funcao_Nome], TM1.[Funcao_GTO], T2.[UnidadeOrganizacional_Nome] AS Funcao_UONom, TM1.[Funcao_Ativo], TM1.[GrupoFuncao_Codigo], TM1.[Funcao_UOCod] AS Funcao_UOCod FROM ([Geral_Funcao] TM1 WITH (NOLOCK) LEFT JOIN [Geral_UnidadeOrganizacional] T2 WITH (NOLOCK) ON T2.[UnidadeOrganizacional_Codigo] = TM1.[Funcao_UOCod]) WHERE TM1.[Funcao_Codigo] = @Funcao_Codigo ORDER BY TM1.[Funcao_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00277,100,0,true,false )
             ,new CursorDef("T00278", "SELECT [GrupoFuncao_Codigo] FROM [Geral_Grupo_Funcao] WITH (NOLOCK) WHERE [GrupoFuncao_Codigo] = @GrupoFuncao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00278,1,0,true,false )
             ,new CursorDef("T00279", "SELECT [UnidadeOrganizacional_Nome] AS Funcao_UONom FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Codigo] = @Funcao_UOCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00279,1,0,true,false )
             ,new CursorDef("T002710", "SELECT [Funcao_Codigo] FROM [Geral_Funcao] WITH (NOLOCK) WHERE [Funcao_Codigo] = @Funcao_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002710,1,0,true,false )
             ,new CursorDef("T002711", "SELECT TOP 1 [Funcao_Codigo] FROM [Geral_Funcao] WITH (NOLOCK) WHERE ( [Funcao_Codigo] > @Funcao_Codigo) ORDER BY [Funcao_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002711,1,0,true,true )
             ,new CursorDef("T002712", "SELECT TOP 1 [Funcao_Codigo] FROM [Geral_Funcao] WITH (NOLOCK) WHERE ( [Funcao_Codigo] < @Funcao_Codigo) ORDER BY [Funcao_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002712,1,0,true,true )
             ,new CursorDef("T002713", "INSERT INTO [Geral_Funcao]([Funcao_Nome], [Funcao_GTO], [Funcao_Ativo], [GrupoFuncao_Codigo], [Funcao_UOCod]) VALUES(@Funcao_Nome, @Funcao_GTO, @Funcao_Ativo, @GrupoFuncao_Codigo, @Funcao_UOCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT002713)
             ,new CursorDef("T002714", "UPDATE [Geral_Funcao] SET [Funcao_Nome]=@Funcao_Nome, [Funcao_GTO]=@Funcao_GTO, [Funcao_Ativo]=@Funcao_Ativo, [GrupoFuncao_Codigo]=@GrupoFuncao_Codigo, [Funcao_UOCod]=@Funcao_UOCod  WHERE [Funcao_Codigo] = @Funcao_Codigo", GxErrorMask.GX_NOMASK,prmT002714)
             ,new CursorDef("T002715", "DELETE FROM [Geral_Funcao]  WHERE [Funcao_Codigo] = @Funcao_Codigo", GxErrorMask.GX_NOMASK,prmT002715)
             ,new CursorDef("T002716", "SELECT [UnidadeOrganizacional_Nome] AS Funcao_UONom FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Codigo] = @Funcao_UOCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002716,1,0,true,false )
             ,new CursorDef("T002717", "SELECT TOP 1 [Equipe_Codigo], [Usuario_Codigo] FROM [EquipeUsuario] WITH (NOLOCK) WHERE [Funcao_Codigo] = @Funcao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002717,1,0,true,true )
             ,new CursorDef("T002718", "SELECT [Funcao_Codigo] FROM [Geral_Funcao] WITH (NOLOCK) ORDER BY [Funcao_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002718,100,0,true,false )
             ,new CursorDef("T002719", "SELECT [GrupoFuncao_Codigo], [GrupoFuncao_Nome] FROM [Geral_Grupo_Funcao] WITH (NOLOCK) ORDER BY [GrupoFuncao_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002719,0,0,true,false )
             ,new CursorDef("T002720", "SELECT [UnidadeOrganizacional_Codigo], [UnidadeOrganizacional_Nome], [UnidadeOrganizacional_Ativo] FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Ativo] = 1 ORDER BY [UnidadeOrganizacional_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002720,0,0,true,false )
             ,new CursorDef("T002721", "SELECT [GrupoFuncao_Codigo] FROM [Geral_Grupo_Funcao] WITH (NOLOCK) WHERE [GrupoFuncao_Codigo] = @GrupoFuncao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002721,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                stmt.SetParameter(3, (bool)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[5]);
                }
                return;
             case 12 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                stmt.SetParameter(3, (bool)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[5]);
                }
                stmt.SetParameter(6, (int)parms[6]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
