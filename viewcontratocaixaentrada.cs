/*
               File: ViewContratoCaixaEntrada
        Description: View Contrato Caixa Entrada
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:3:35.33
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class viewcontratocaixaentrada : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public viewcontratocaixaentrada( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public viewcontratocaixaentrada( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contrato_Codigo ,
                           int aP1_ContratoCaixaEntrada_Codigo ,
                           String aP2_TabCode )
      {
         this.AV9Contrato_Codigo = aP0_Contrato_Codigo;
         this.AV12ContratoCaixaEntrada_Codigo = aP1_ContratoCaixaEntrada_Codigo;
         this.AV7TabCode = aP2_TabCode;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbContratoCaixaEntrada_GestorTodasEnviadas = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV9Contrato_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Contrato_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9Contrato_Codigo), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV12ContratoCaixaEntrada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ContratoCaixaEntrada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12ContratoCaixaEntrada_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOCAIXAENTRADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV12ContratoCaixaEntrada_Codigo), "ZZZZZ9")));
                  AV7TabCode = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7TabCode", AV7TabCode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PASI2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTSI2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020429033537");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("viewcontratocaixaentrada.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo) + "," + UrlEncode("" +AV12ContratoCaixaEntrada_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV7TabCode))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATOCAIXAENTRADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12ContratoCaixaEntrada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vTABCODE", StringUtil.RTrim( AV7TabCode));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS", GetSecureSignedToken( "", A2097ContratoCaixaEntrada_GestorTodasEnviadas));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9Contrato_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOCAIXAENTRADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV12ContratoCaixaEntrada_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9Contrato_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOCAIXAENTRADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV12ContratoCaixaEntrada_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         if ( ! ( WebComp_Tabbedview == null ) )
         {
            WebComp_Tabbedview.componentjscripts();
         }
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WESI2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTSI2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("viewcontratocaixaentrada.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo) + "," + UrlEncode("" +AV12ContratoCaixaEntrada_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV7TabCode)) ;
      }

      public override String GetPgmname( )
      {
         return "ViewContratoCaixaEntrada" ;
      }

      public override String GetPgmdesc( )
      {
         return "View Contrato Caixa Entrada" ;
      }

      protected void WBSI0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_SI2( true) ;
         }
         else
         {
            wb_table1_2_SI2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_SI2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTSI2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "View Contrato Caixa Entrada", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPSI0( ) ;
      }

      protected void WSSI2( )
      {
         STARTSI2( ) ;
         EVTSI2( ) ;
      }

      protected void EVTSI2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11SI2 */
                              E11SI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12SI2 */
                              E12SI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     else if ( StringUtil.StrCmp(sEvtType, "W") == 0 )
                     {
                        sEvtType = StringUtil.Left( sEvt, 4);
                        sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                        if ( nCmpId == 16 )
                        {
                           OldTabbedview = cgiGet( "W0016");
                           if ( ( StringUtil.Len( OldTabbedview) == 0 ) || ( StringUtil.StrCmp(OldTabbedview, WebComp_Tabbedview_Component) != 0 ) )
                           {
                              WebComp_Tabbedview = getWebComponent(GetType(), "GeneXus.Programs", OldTabbedview, new Object[] {context} );
                              WebComp_Tabbedview.ComponentInit();
                              WebComp_Tabbedview.Name = "OldTabbedview";
                              WebComp_Tabbedview_Component = OldTabbedview;
                           }
                           if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
                           {
                              WebComp_Tabbedview.componentprocess("W0016", "", sEvt);
                           }
                           WebComp_Tabbedview_Component = OldTabbedview;
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WESI2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PASI2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbContratoCaixaEntrada_GestorTodasEnviadas.Name = "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS";
            cmbContratoCaixaEntrada_GestorTodasEnviadas.WebTags = "";
            cmbContratoCaixaEntrada_GestorTodasEnviadas.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            cmbContratoCaixaEntrada_GestorTodasEnviadas.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            if ( cmbContratoCaixaEntrada_GestorTodasEnviadas.ItemCount > 0 )
            {
               A2097ContratoCaixaEntrada_GestorTodasEnviadas = StringUtil.StrToBool( cmbContratoCaixaEntrada_GestorTodasEnviadas.getValidValue(StringUtil.BoolToStr( A2097ContratoCaixaEntrada_GestorTodasEnviadas)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2097ContratoCaixaEntrada_GestorTodasEnviadas", A2097ContratoCaixaEntrada_GestorTodasEnviadas);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS", GetSecureSignedToken( "", A2097ContratoCaixaEntrada_GestorTodasEnviadas));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbContratoCaixaEntrada_GestorTodasEnviadas.ItemCount > 0 )
         {
            A2097ContratoCaixaEntrada_GestorTodasEnviadas = StringUtil.StrToBool( cmbContratoCaixaEntrada_GestorTodasEnviadas.getValidValue(StringUtil.BoolToStr( A2097ContratoCaixaEntrada_GestorTodasEnviadas)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2097ContratoCaixaEntrada_GestorTodasEnviadas", A2097ContratoCaixaEntrada_GestorTodasEnviadas);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS", GetSecureSignedToken( "", A2097ContratoCaixaEntrada_GestorTodasEnviadas));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFSI2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFSI2( )
      {
         initialize_formulas( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( 1 != 0 )
            {
               if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
               {
                  WebComp_Tabbedview.componentstart();
               }
            }
         }
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00SI2 */
            pr_default.execute(0, new Object[] {AV12ContratoCaixaEntrada_Codigo, AV9Contrato_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A2098ContratoCaixaEntrada_Codigo = H00SI2_A2098ContratoCaixaEntrada_Codigo[0];
               A74Contrato_Codigo = H00SI2_A74Contrato_Codigo[0];
               A2097ContratoCaixaEntrada_GestorTodasEnviadas = H00SI2_A2097ContratoCaixaEntrada_GestorTodasEnviadas[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2097ContratoCaixaEntrada_GestorTodasEnviadas", A2097ContratoCaixaEntrada_GestorTodasEnviadas);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS", GetSecureSignedToken( "", A2097ContratoCaixaEntrada_GestorTodasEnviadas));
               /* Execute user event: E12SI2 */
               E12SI2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBSI0( ) ;
         }
      }

      protected void STRUPSI0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11SI2 */
         E11SI2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbContratoCaixaEntrada_GestorTodasEnviadas.CurrentValue = cgiGet( cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname);
            A2097ContratoCaixaEntrada_GestorTodasEnviadas = StringUtil.StrToBool( cgiGet( cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2097ContratoCaixaEntrada_GestorTodasEnviadas", A2097ContratoCaixaEntrada_GestorTodasEnviadas);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS", GetSecureSignedToken( "", A2097ContratoCaixaEntrada_GestorTodasEnviadas));
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11SI2 */
         E11SI2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11SI2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         lblWorkwithlink_Link = formatLink("wwcontratocaixaentrada.aspx") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblWorkwithlink_Internalname, "Link", lblWorkwithlink_Link);
         AV15GXLvl9 = 0;
         /* Using cursor H00SI3 */
         pr_default.execute(1, new Object[] {AV12ContratoCaixaEntrada_Codigo, AV9Contrato_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A2098ContratoCaixaEntrada_Codigo = H00SI3_A2098ContratoCaixaEntrada_Codigo[0];
            A74Contrato_Codigo = H00SI3_A74Contrato_Codigo[0];
            A77Contrato_Numero = H00SI3_A77Contrato_Numero[0];
            A77Contrato_Numero = H00SI3_A77Contrato_Numero[0];
            AV15GXLvl9 = 1;
            Form.Caption = "Contrato: "+A77Contrato_Numero;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
            AV8Exists = true;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
         if ( AV15GXLvl9 == 0 )
         {
            Form.Caption = "Registro n�o encontrado ";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
            AV8Exists = false;
         }
         if ( AV8Exists )
         {
            /* Execute user subroutine: 'LOADTABS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            /* Object Property */
            if ( StringUtil.StrCmp(StringUtil.Lower( WebComp_Tabbedview_Component), StringUtil.Lower( "WWPBaseObjects.WWPTabbedView")) != 0 )
            {
               WebComp_Tabbedview = getWebComponent(GetType(), "GeneXus.Programs", "wwpbaseobjects.wwptabbedview", new Object[] {context} );
               WebComp_Tabbedview.ComponentInit();
               WebComp_Tabbedview.Name = "WWPBaseObjects.WWPTabbedView";
               WebComp_Tabbedview_Component = "WWPBaseObjects.WWPTabbedView";
            }
            if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
            {
               WebComp_Tabbedview.setjustcreated();
               WebComp_Tabbedview.componentprepare(new Object[] {(String)"W0016",(String)"",(IGxCollection)AV10Tabs,(String)AV7TabCode});
               WebComp_Tabbedview.componentbind(new Object[] {(String)"",(String)""});
            }
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12SI2( )
      {
         /* Load Routine */
      }

      protected void S112( )
      {
         /* 'LOADTABS' Routine */
         AV10Tabs = new GxObjectCollection( context, "WWPTabOptions.TabOptionsItem", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem", "GeneXus.Programs");
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "General";
         AV11Tab.gxTpr_Description = "General";
         AV11Tab.gxTpr_Webcomponent = formatLink("contratocaixaentradageneral.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo) + "," + UrlEncode("" +AV12ContratoCaixaEntrada_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontratocaixaentrada.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo) + "," + UrlEncode("" +AV12ContratoCaixaEntrada_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 0;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
      }

      protected void wb_table1_2_SI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableContentNoMargin", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TextBlockTitleCell'>") ;
            wb_table2_5_SI2( true) ;
         }
         else
         {
            wb_table2_5_SI2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_SI2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\" class='TableTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblWorkwithlink_Internalname, " Configura��es da Caixa de Entrada", lblWorkwithlink_Link, "", lblWorkwithlink_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockLink", 0, "", 1, 1, 0, "HLP_ViewContratoCaixaEntrada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               GxWebStd.gx_hidden_field( context, "W0016"+"", StringUtil.RTrim( WebComp_Tabbedview_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpW0016"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
               {
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldTabbedview), StringUtil.Lower( WebComp_Tabbedview_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0016"+"");
                  }
                  WebComp_Tabbedview.componentdraw();
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldTabbedview), StringUtil.Lower( WebComp_Tabbedview_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspEndCmp();
                  }
               }
               context.WriteHtmlText( "</div>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_SI2e( true) ;
         }
         else
         {
            wb_table1_2_SI2e( false) ;
         }
      }

      protected void wb_table2_5_SI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedviewtitle_Internalname, tblTablemergedviewtitle_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblViewtitle_Internalname, "Configura��es da Caixa de Entrada :: ", "", "", lblViewtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_ViewContratoCaixaEntrada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoCaixaEntrada_GestorTodasEnviadas, cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname, StringUtil.BoolToStr( A2097ContratoCaixaEntrada_GestorTodasEnviadas), 1, cmbContratoCaixaEntrada_GestorTodasEnviadas_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "AttributeTitleWWP", "", "", "", true, "HLP_ViewContratoCaixaEntrada.htm");
            cmbContratoCaixaEntrada_GestorTodasEnviadas.CurrentValue = StringUtil.BoolToStr( A2097ContratoCaixaEntrada_GestorTodasEnviadas);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname, "Values", (String)(cmbContratoCaixaEntrada_GestorTodasEnviadas.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_SI2e( true) ;
         }
         else
         {
            wb_table2_5_SI2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV9Contrato_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Contrato_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9Contrato_Codigo), "ZZZZZ9")));
         AV12ContratoCaixaEntrada_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ContratoCaixaEntrada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12ContratoCaixaEntrada_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOCAIXAENTRADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV12ContratoCaixaEntrada_Codigo), "ZZZZZ9")));
         AV7TabCode = (String)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7TabCode", AV7TabCode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PASI2( ) ;
         WSSI2( ) ;
         WESI2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         if ( ! ( WebComp_Tabbedview == null ) )
         {
            if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
            {
               WebComp_Tabbedview.componentthemes();
            }
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020429033554");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("viewcontratocaixaentrada.js", "?2020429033554");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblViewtitle_Internalname = "VIEWTITLE";
         cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname = "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS";
         tblTablemergedviewtitle_Internalname = "TABLEMERGEDVIEWTITLE";
         lblWorkwithlink_Internalname = "WORKWITHLINK";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbContratoCaixaEntrada_GestorTodasEnviadas_Jsonclick = "";
         lblWorkwithlink_Link = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "View Contrato Caixa Entrada";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV7TabCode = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         OldTabbedview = "";
         WebComp_Tabbedview_Component = "";
         scmdbuf = "";
         H00SI2_A2098ContratoCaixaEntrada_Codigo = new int[1] ;
         H00SI2_A74Contrato_Codigo = new int[1] ;
         H00SI2_A2097ContratoCaixaEntrada_GestorTodasEnviadas = new bool[] {false} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         H00SI3_A2098ContratoCaixaEntrada_Codigo = new int[1] ;
         H00SI3_A74Contrato_Codigo = new int[1] ;
         H00SI3_A77Contrato_Numero = new String[] {""} ;
         A77Contrato_Numero = "";
         AV10Tabs = new GxObjectCollection( context, "WWPTabOptions.TabOptionsItem", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem", "GeneXus.Programs");
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         sStyleString = "";
         lblWorkwithlink_Jsonclick = "";
         lblViewtitle_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.viewcontratocaixaentrada__default(),
            new Object[][] {
                new Object[] {
               H00SI2_A2098ContratoCaixaEntrada_Codigo, H00SI2_A74Contrato_Codigo, H00SI2_A2097ContratoCaixaEntrada_GestorTodasEnviadas
               }
               , new Object[] {
               H00SI3_A2098ContratoCaixaEntrada_Codigo, H00SI3_A74Contrato_Codigo, H00SI3_A77Contrato_Numero
               }
            }
         );
         WebComp_Tabbedview = new GeneXus.Http.GXNullWebComponent();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV15GXLvl9 ;
      private short nGXWrapped ;
      private int AV9Contrato_Codigo ;
      private int AV12ContratoCaixaEntrada_Codigo ;
      private int wcpOAV9Contrato_Codigo ;
      private int wcpOAV12ContratoCaixaEntrada_Codigo ;
      private int A2098ContratoCaixaEntrada_Codigo ;
      private int A74Contrato_Codigo ;
      private int idxLst ;
      private String AV7TabCode ;
      private String wcpOAV7TabCode ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String OldTabbedview ;
      private String WebComp_Tabbedview_Component ;
      private String scmdbuf ;
      private String cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname ;
      private String lblWorkwithlink_Link ;
      private String lblWorkwithlink_Internalname ;
      private String A77Contrato_Numero ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblWorkwithlink_Jsonclick ;
      private String tblTablemergedviewtitle_Internalname ;
      private String lblViewtitle_Internalname ;
      private String lblViewtitle_Jsonclick ;
      private String cmbContratoCaixaEntrada_GestorTodasEnviadas_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A2097ContratoCaixaEntrada_GestorTodasEnviadas ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool AV8Exists ;
      private GXWebComponent WebComp_Tabbedview ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContratoCaixaEntrada_GestorTodasEnviadas ;
      private IDataStoreProvider pr_default ;
      private int[] H00SI2_A2098ContratoCaixaEntrada_Codigo ;
      private int[] H00SI2_A74Contrato_Codigo ;
      private bool[] H00SI2_A2097ContratoCaixaEntrada_GestorTodasEnviadas ;
      private int[] H00SI3_A2098ContratoCaixaEntrada_Codigo ;
      private int[] H00SI3_A74Contrato_Codigo ;
      private String[] H00SI3_A77Contrato_Numero ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem ))]
      private IGxCollection AV10Tabs ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem AV11Tab ;
   }

   public class viewcontratocaixaentrada__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00SI2 ;
          prmH00SI2 = new Object[] {
          new Object[] {"@AV12ContratoCaixaEntrada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00SI3 ;
          prmH00SI3 = new Object[] {
          new Object[] {"@AV12ContratoCaixaEntrada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00SI2", "SELECT [ContratoCaixaEntrada_Codigo], [Contrato_Codigo], [ContratoCaixaEntrada_GestorTodasEnviadas] FROM [ContratoCaixaEntrada] WITH (NOLOCK) WHERE [ContratoCaixaEntrada_Codigo] = @AV12ContratoCaixaEntrada_Codigo and [Contrato_Codigo] = @AV9Contrato_Codigo ORDER BY [ContratoCaixaEntrada_Codigo], [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SI2,1,0,true,true )
             ,new CursorDef("H00SI3", "SELECT T1.[ContratoCaixaEntrada_Codigo], T1.[Contrato_Codigo], T2.[Contrato_Numero] FROM ([ContratoCaixaEntrada] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T1.[ContratoCaixaEntrada_Codigo] = @AV12ContratoCaixaEntrada_Codigo and T1.[Contrato_Codigo] = @AV9Contrato_Codigo) AND (T1.[Contrato_Codigo] = @AV9Contrato_Codigo and T1.[ContratoCaixaEntrada_Codigo] = @AV12ContratoCaixaEntrada_Codigo) ORDER BY T1.[ContratoCaixaEntrada_Codigo], T1.[Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SI3,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
