/*
               File: GetWWFuncaoUsuarioFilterData
        Description: Get WWFuncao Usuario Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:53:15.49
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwfuncaousuariofilterdata : GXProcedure
   {
      public getwwfuncaousuariofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwfuncaousuariofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
         return AV25OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwfuncaousuariofilterdata objgetwwfuncaousuariofilterdata;
         objgetwwfuncaousuariofilterdata = new getwwfuncaousuariofilterdata();
         objgetwwfuncaousuariofilterdata.AV16DDOName = aP0_DDOName;
         objgetwwfuncaousuariofilterdata.AV14SearchTxt = aP1_SearchTxt;
         objgetwwfuncaousuariofilterdata.AV15SearchTxtTo = aP2_SearchTxtTo;
         objgetwwfuncaousuariofilterdata.AV20OptionsJson = "" ;
         objgetwwfuncaousuariofilterdata.AV23OptionsDescJson = "" ;
         objgetwwfuncaousuariofilterdata.AV25OptionIndexesJson = "" ;
         objgetwwfuncaousuariofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwfuncaousuariofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwfuncaousuariofilterdata);
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwfuncaousuariofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV19Options = (IGxCollection)(new GxSimpleCollection());
         AV22OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV24OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_FUNCAOUSUARIO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAOUSUARIO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV20OptionsJson = AV19Options.ToJSonString(false);
         AV23OptionsDescJson = AV22OptionsDesc.ToJSonString(false);
         AV25OptionIndexesJson = AV24OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get("WWFuncaoUsuarioGridState"), "") == 0 )
         {
            AV29GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWFuncaoUsuarioGridState"), "");
         }
         else
         {
            AV29GridState.FromXml(AV27Session.Get("WWFuncaoUsuarioGridState"), "");
         }
         AV40GXV1 = 1;
         while ( AV40GXV1 <= AV29GridState.gxTpr_Filtervalues.Count )
         {
            AV30GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV29GridState.gxTpr_Filtervalues.Item(AV40GXV1));
            if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFFUNCAOUSUARIO_NOME") == 0 )
            {
               AV10TFFuncaoUsuario_Nome = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFFUNCAOUSUARIO_NOME_SEL") == 0 )
            {
               AV11TFFuncaoUsuario_Nome_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFFUNCAOUSUARIO_PF") == 0 )
            {
               AV12TFFuncaoUsuario_PF = NumberUtil.Val( AV30GridStateFilterValue.gxTpr_Value, ".");
               AV13TFFuncaoUsuario_PF_To = NumberUtil.Val( AV30GridStateFilterValue.gxTpr_Valueto, ".");
            }
            AV40GXV1 = (int)(AV40GXV1+1);
         }
         if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(1));
            AV32DynamicFiltersSelector1 = AV31GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "FUNCAOUSUARIO_NOME") == 0 )
            {
               AV33DynamicFiltersOperator1 = AV31GridStateDynamicFilter.gxTpr_Operator;
               AV34FuncaoUsuario_Nome1 = AV31GridStateDynamicFilter.gxTpr_Value;
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADFUNCAOUSUARIO_NOMEOPTIONS' Routine */
         AV10TFFuncaoUsuario_Nome = AV14SearchTxt;
         AV11TFFuncaoUsuario_Nome_Sel = "";
         AV42WWFuncaoUsuarioDS_1_Dynamicfiltersselector1 = AV32DynamicFiltersSelector1;
         AV43WWFuncaoUsuarioDS_2_Dynamicfiltersoperator1 = AV33DynamicFiltersOperator1;
         AV44WWFuncaoUsuarioDS_3_Funcaousuario_nome1 = AV34FuncaoUsuario_Nome1;
         AV45WWFuncaoUsuarioDS_4_Tffuncaousuario_nome = AV10TFFuncaoUsuario_Nome;
         AV46WWFuncaoUsuarioDS_5_Tffuncaousuario_nome_sel = AV11TFFuncaoUsuario_Nome_Sel;
         AV47WWFuncaoUsuarioDS_6_Tffuncaousuario_pf = AV12TFFuncaoUsuario_PF;
         AV48WWFuncaoUsuarioDS_7_Tffuncaousuario_pf_to = AV13TFFuncaoUsuario_PF_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV42WWFuncaoUsuarioDS_1_Dynamicfiltersselector1 ,
                                              AV43WWFuncaoUsuarioDS_2_Dynamicfiltersoperator1 ,
                                              AV44WWFuncaoUsuarioDS_3_Funcaousuario_nome1 ,
                                              AV46WWFuncaoUsuarioDS_5_Tffuncaousuario_nome_sel ,
                                              AV45WWFuncaoUsuarioDS_4_Tffuncaousuario_nome ,
                                              A162FuncaoUsuario_Nome ,
                                              AV47WWFuncaoUsuarioDS_6_Tffuncaousuario_pf ,
                                              A396FuncaoUsuario_PF ,
                                              AV48WWFuncaoUsuarioDS_7_Tffuncaousuario_pf_to },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL
                                              }
         });
         lV44WWFuncaoUsuarioDS_3_Funcaousuario_nome1 = StringUtil.Concat( StringUtil.RTrim( AV44WWFuncaoUsuarioDS_3_Funcaousuario_nome1), "%", "");
         lV44WWFuncaoUsuarioDS_3_Funcaousuario_nome1 = StringUtil.Concat( StringUtil.RTrim( AV44WWFuncaoUsuarioDS_3_Funcaousuario_nome1), "%", "");
         lV45WWFuncaoUsuarioDS_4_Tffuncaousuario_nome = StringUtil.Concat( StringUtil.RTrim( AV45WWFuncaoUsuarioDS_4_Tffuncaousuario_nome), "%", "");
         /* Using cursor P00GB2 */
         pr_default.execute(0, new Object[] {lV44WWFuncaoUsuarioDS_3_Funcaousuario_nome1, lV44WWFuncaoUsuarioDS_3_Funcaousuario_nome1, lV45WWFuncaoUsuarioDS_4_Tffuncaousuario_nome, AV46WWFuncaoUsuarioDS_5_Tffuncaousuario_nome_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKGB2 = false;
            A162FuncaoUsuario_Nome = P00GB2_A162FuncaoUsuario_Nome[0];
            A161FuncaoUsuario_Codigo = P00GB2_A161FuncaoUsuario_Codigo[0];
            GXt_int1 = (short)(A396FuncaoUsuario_PF);
            new prc_fupf(context ).execute( ref  A161FuncaoUsuario_Codigo, ref  GXt_int1) ;
            A396FuncaoUsuario_PF = (decimal)(GXt_int1);
            if ( (Convert.ToDecimal(0)==AV47WWFuncaoUsuarioDS_6_Tffuncaousuario_pf) || ( ( A396FuncaoUsuario_PF >= AV47WWFuncaoUsuarioDS_6_Tffuncaousuario_pf ) ) )
            {
               if ( (Convert.ToDecimal(0)==AV48WWFuncaoUsuarioDS_7_Tffuncaousuario_pf_to) || ( ( A396FuncaoUsuario_PF <= AV48WWFuncaoUsuarioDS_7_Tffuncaousuario_pf_to ) ) )
               {
                  AV26count = 0;
                  while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00GB2_A162FuncaoUsuario_Nome[0], A162FuncaoUsuario_Nome) == 0 ) )
                  {
                     BRKGB2 = false;
                     A161FuncaoUsuario_Codigo = P00GB2_A161FuncaoUsuario_Codigo[0];
                     AV26count = (long)(AV26count+1);
                     BRKGB2 = true;
                     pr_default.readNext(0);
                  }
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A162FuncaoUsuario_Nome)) )
                  {
                     AV18Option = A162FuncaoUsuario_Nome;
                     AV19Options.Add(AV18Option, 0);
                     AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
                  }
                  if ( AV19Options.Count == 50 )
                  {
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
               }
            }
            if ( ! BRKGB2 )
            {
               BRKGB2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV19Options = new GxSimpleCollection();
         AV22OptionsDesc = new GxSimpleCollection();
         AV24OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV27Session = context.GetSession();
         AV29GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV30GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFFuncaoUsuario_Nome = "";
         AV11TFFuncaoUsuario_Nome_Sel = "";
         AV31GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV32DynamicFiltersSelector1 = "";
         AV34FuncaoUsuario_Nome1 = "";
         AV42WWFuncaoUsuarioDS_1_Dynamicfiltersselector1 = "";
         AV44WWFuncaoUsuarioDS_3_Funcaousuario_nome1 = "";
         AV45WWFuncaoUsuarioDS_4_Tffuncaousuario_nome = "";
         AV46WWFuncaoUsuarioDS_5_Tffuncaousuario_nome_sel = "";
         scmdbuf = "";
         lV44WWFuncaoUsuarioDS_3_Funcaousuario_nome1 = "";
         lV45WWFuncaoUsuarioDS_4_Tffuncaousuario_nome = "";
         A162FuncaoUsuario_Nome = "";
         P00GB2_A162FuncaoUsuario_Nome = new String[] {""} ;
         P00GB2_A161FuncaoUsuario_Codigo = new int[1] ;
         AV18Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwfuncaousuariofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00GB2_A162FuncaoUsuario_Nome, P00GB2_A161FuncaoUsuario_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV33DynamicFiltersOperator1 ;
      private short AV43WWFuncaoUsuarioDS_2_Dynamicfiltersoperator1 ;
      private short GXt_int1 ;
      private int AV40GXV1 ;
      private int A161FuncaoUsuario_Codigo ;
      private long AV26count ;
      private decimal AV12TFFuncaoUsuario_PF ;
      private decimal AV13TFFuncaoUsuario_PF_To ;
      private decimal AV47WWFuncaoUsuarioDS_6_Tffuncaousuario_pf ;
      private decimal AV48WWFuncaoUsuarioDS_7_Tffuncaousuario_pf_to ;
      private decimal A396FuncaoUsuario_PF ;
      private String scmdbuf ;
      private bool returnInSub ;
      private bool BRKGB2 ;
      private String AV25OptionIndexesJson ;
      private String AV20OptionsJson ;
      private String AV23OptionsDescJson ;
      private String AV16DDOName ;
      private String AV14SearchTxt ;
      private String AV15SearchTxtTo ;
      private String AV10TFFuncaoUsuario_Nome ;
      private String AV11TFFuncaoUsuario_Nome_Sel ;
      private String AV32DynamicFiltersSelector1 ;
      private String AV34FuncaoUsuario_Nome1 ;
      private String AV42WWFuncaoUsuarioDS_1_Dynamicfiltersselector1 ;
      private String AV44WWFuncaoUsuarioDS_3_Funcaousuario_nome1 ;
      private String AV45WWFuncaoUsuarioDS_4_Tffuncaousuario_nome ;
      private String AV46WWFuncaoUsuarioDS_5_Tffuncaousuario_nome_sel ;
      private String lV44WWFuncaoUsuarioDS_3_Funcaousuario_nome1 ;
      private String lV45WWFuncaoUsuarioDS_4_Tffuncaousuario_nome ;
      private String A162FuncaoUsuario_Nome ;
      private String AV18Option ;
      private IGxSession AV27Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00GB2_A162FuncaoUsuario_Nome ;
      private int[] P00GB2_A161FuncaoUsuario_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV19Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV29GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV30GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV31GridStateDynamicFilter ;
   }

   public class getwwfuncaousuariofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00GB2( IGxContext context ,
                                             String AV42WWFuncaoUsuarioDS_1_Dynamicfiltersselector1 ,
                                             short AV43WWFuncaoUsuarioDS_2_Dynamicfiltersoperator1 ,
                                             String AV44WWFuncaoUsuarioDS_3_Funcaousuario_nome1 ,
                                             String AV46WWFuncaoUsuarioDS_5_Tffuncaousuario_nome_sel ,
                                             String AV45WWFuncaoUsuarioDS_4_Tffuncaousuario_nome ,
                                             String A162FuncaoUsuario_Nome ,
                                             decimal AV47WWFuncaoUsuarioDS_6_Tffuncaousuario_pf ,
                                             decimal A396FuncaoUsuario_PF ,
                                             decimal AV48WWFuncaoUsuarioDS_7_Tffuncaousuario_pf_to )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [4] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT [FuncaoUsuario_Nome], [FuncaoUsuario_Codigo] FROM [ModuloFuncoes] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV42WWFuncaoUsuarioDS_1_Dynamicfiltersselector1, "FUNCAOUSUARIO_NOME") == 0 ) && ( AV43WWFuncaoUsuarioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44WWFuncaoUsuarioDS_3_Funcaousuario_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoUsuario_Nome] like @lV44WWFuncaoUsuarioDS_3_Funcaousuario_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoUsuario_Nome] like @lV44WWFuncaoUsuarioDS_3_Funcaousuario_nome1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV42WWFuncaoUsuarioDS_1_Dynamicfiltersselector1, "FUNCAOUSUARIO_NOME") == 0 ) && ( AV43WWFuncaoUsuarioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44WWFuncaoUsuarioDS_3_Funcaousuario_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoUsuario_Nome] like '%' + @lV44WWFuncaoUsuarioDS_3_Funcaousuario_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoUsuario_Nome] like '%' + @lV44WWFuncaoUsuarioDS_3_Funcaousuario_nome1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV46WWFuncaoUsuarioDS_5_Tffuncaousuario_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45WWFuncaoUsuarioDS_4_Tffuncaousuario_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoUsuario_Nome] like @lV45WWFuncaoUsuarioDS_4_Tffuncaousuario_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoUsuario_Nome] like @lV45WWFuncaoUsuarioDS_4_Tffuncaousuario_nome)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46WWFuncaoUsuarioDS_5_Tffuncaousuario_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoUsuario_Nome] = @AV46WWFuncaoUsuarioDS_5_Tffuncaousuario_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoUsuario_Nome] = @AV46WWFuncaoUsuarioDS_5_Tffuncaousuario_nome_sel)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [FuncaoUsuario_Nome]";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00GB2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (decimal)dynConstraints[6] , (decimal)dynConstraints[7] , (decimal)dynConstraints[8] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00GB2 ;
          prmP00GB2 = new Object[] {
          new Object[] {"@lV44WWFuncaoUsuarioDS_3_Funcaousuario_nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV44WWFuncaoUsuarioDS_3_Funcaousuario_nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV45WWFuncaoUsuarioDS_4_Tffuncaousuario_nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV46WWFuncaoUsuarioDS_5_Tffuncaousuario_nome_sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00GB2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00GB2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwfuncaousuariofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwfuncaousuariofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwfuncaousuariofilterdata") )
          {
             return  ;
          }
          getwwfuncaousuariofilterdata worker = new getwwfuncaousuariofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
