/*
               File: PRC_CarregaSDTBaseline
        Description: Carrega SDTBaseline
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/3/2020 18:50:11.47
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aprc_carregasdtbaseline : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV63PraLinha = (short)(NumberUtil.Val( gxfirstwebparm, "."));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV9Arquivo = GetNextPar( );
                  AV8Aba = GetNextPar( );
                  AV90ColNomen = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV91ColTipon = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV92ColDERn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV93ColRLRn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV94ColObservn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV82FileName = GetNextPar( );
                  AV97Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV98Sistema_Sigla = GetNextPar( );
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public aprc_carregasdtbaseline( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public aprc_carregasdtbaseline( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( short aP0_PraLinha ,
                           String aP1_Arquivo ,
                           String aP2_Aba ,
                           short aP3_ColNomen ,
                           short aP4_ColTipon ,
                           short aP5_ColDERn ,
                           short aP6_ColRLRn ,
                           short aP7_ColObservn ,
                           String aP8_FileName ,
                           int aP9_Sistema_Codigo ,
                           String aP10_Sistema_Sigla )
      {
         this.AV63PraLinha = aP0_PraLinha;
         this.AV9Arquivo = aP1_Arquivo;
         this.AV8Aba = aP2_Aba;
         this.AV90ColNomen = aP3_ColNomen;
         this.AV91ColTipon = aP4_ColTipon;
         this.AV92ColDERn = aP5_ColDERn;
         this.AV93ColRLRn = aP6_ColRLRn;
         this.AV94ColObservn = aP7_ColObservn;
         this.AV82FileName = aP8_FileName;
         this.AV97Sistema_Codigo = aP9_Sistema_Codigo;
         this.AV98Sistema_Sigla = aP10_Sistema_Sigla;
         initialize();
         executePrivate();
      }

      public void executeSubmit( short aP0_PraLinha ,
                                 String aP1_Arquivo ,
                                 String aP2_Aba ,
                                 short aP3_ColNomen ,
                                 short aP4_ColTipon ,
                                 short aP5_ColDERn ,
                                 short aP6_ColRLRn ,
                                 short aP7_ColObservn ,
                                 String aP8_FileName ,
                                 int aP9_Sistema_Codigo ,
                                 String aP10_Sistema_Sigla )
      {
         aprc_carregasdtbaseline objaprc_carregasdtbaseline;
         objaprc_carregasdtbaseline = new aprc_carregasdtbaseline();
         objaprc_carregasdtbaseline.AV63PraLinha = aP0_PraLinha;
         objaprc_carregasdtbaseline.AV9Arquivo = aP1_Arquivo;
         objaprc_carregasdtbaseline.AV8Aba = aP2_Aba;
         objaprc_carregasdtbaseline.AV90ColNomen = aP3_ColNomen;
         objaprc_carregasdtbaseline.AV91ColTipon = aP4_ColTipon;
         objaprc_carregasdtbaseline.AV92ColDERn = aP5_ColDERn;
         objaprc_carregasdtbaseline.AV93ColRLRn = aP6_ColRLRn;
         objaprc_carregasdtbaseline.AV94ColObservn = aP7_ColObservn;
         objaprc_carregasdtbaseline.AV82FileName = aP8_FileName;
         objaprc_carregasdtbaseline.AV97Sistema_Codigo = aP9_Sistema_Codigo;
         objaprc_carregasdtbaseline.AV98Sistema_Sigla = aP10_Sistema_Sigla;
         objaprc_carregasdtbaseline.context.SetSubmitInitialConfig(context);
         objaprc_carregasdtbaseline.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaprc_carregasdtbaseline);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aprc_carregasdtbaseline)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 6;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 256, 16834, 11909, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*6));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            /* Execute user subroutine: 'OPENFILE' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            AV57NomeArq = "Arquivo: " + AV82FileName;
            AV57NomeArq = AV57NomeArq + (String.IsNullOrEmpty(StringUtil.RTrim( AV8Aba)) ? "" : " (aba "+AV8Aba+")");
            AV53Ln = AV63PraLinha;
            AV86TipoExterna = "EESECE";
            AV96TxtLinha = "Linha ";
            while ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40ExcelDocument.get_Cells(AV53Ln, AV90ColNomen, 1, 1).Text)) )
            {
               if ( ( AV53Ln /  ( decimal )( 75 ) == Convert.ToDecimal( NumberUtil.Int( (long)(AV53Ln/ (decimal)(75))) )) )
               {
                  AV38ErrCod = AV40ExcelDocument.Close();
                  /* Execute user subroutine: 'OPENFILE' */
                  S121 ();
                  if ( returnInSub )
                  {
                     this.cleanup();
                     if (true) return;
                  }
               }
               AV51Linha = AV96TxtLinha + StringUtil.Trim( StringUtil.Str( (decimal)(AV53Ln), 4, 0)) + " - ";
               AV96TxtLinha = "      ";
               AV38ErrCod = 0;
               AV65SDT_Funcao.gxTpr_Tipo = StringUtil.Upper( StringUtil.Trim( AV40ExcelDocument.get_Cells(AV53Ln, AV91ColTipon, 1, 1).Text));
               /* Execute user subroutine: 'LERNOMEFUNCAO' */
               S111 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
               GXt_char1 = AV95NomeLimpo;
               new prc_padronizastring(context ).execute(  AV65SDT_Funcao.gxTpr_Nome, out  GXt_char1) ;
               AV95NomeLimpo = GXt_char1;
               if ( StringUtil.StringSearch( AV86TipoExterna, AV65SDT_Funcao.gxTpr_Tipo, 1) > 0 )
               {
                  /* Using cursor P008C2 */
                  pr_default.execute(0, new Object[] {AV97Sistema_Codigo});
                  while ( (pr_default.getStatus(0) != 101) )
                  {
                     A166FuncaoAPF_Nome = P008C2_A166FuncaoAPF_Nome[0];
                     A360FuncaoAPF_SistemaCod = P008C2_A360FuncaoAPF_SistemaCod[0];
                     n360FuncaoAPF_SistemaCod = P008C2_n360FuncaoAPF_SistemaCod[0];
                     A165FuncaoAPF_Codigo = P008C2_A165FuncaoAPF_Codigo[0];
                     if ( StringUtil.StrCmp(new prc_padronizastring(context).executeUdp(  A166FuncaoAPF_Nome), AV95NomeLimpo) == 0 )
                     {
                        AV51Linha = AV51Linha + "Fun��o de TRN " + AV65SDT_Funcao.gxTpr_Nome + " j� cadastrada e ser� atualizada.";
                        AV65SDT_Funcao.gxTpr_Codigo = A165FuncaoAPF_Codigo;
                     }
                     pr_default.readNext(0);
                  }
                  pr_default.close(0);
               }
               else
               {
                  /* Using cursor P008C3 */
                  pr_default.execute(1, new Object[] {AV97Sistema_Codigo});
                  while ( (pr_default.getStatus(1) != 101) )
                  {
                     A369FuncaoDados_Nome = P008C3_A369FuncaoDados_Nome[0];
                     A370FuncaoDados_SistemaCod = P008C3_A370FuncaoDados_SistemaCod[0];
                     A368FuncaoDados_Codigo = P008C3_A368FuncaoDados_Codigo[0];
                     if ( StringUtil.StrCmp(new prc_padronizastring(context).executeUdp(  A369FuncaoDados_Nome), AV95NomeLimpo) == 0 )
                     {
                        AV51Linha = AV51Linha + "Fun��o de Dados " + AV65SDT_Funcao.gxTpr_Nome + " j� cadastrada e ser� atualizada.";
                        AV65SDT_Funcao.gxTpr_Codigo = A368FuncaoDados_Codigo;
                     }
                     pr_default.readNext(1);
                  }
                  pr_default.close(1);
               }
               if ( AV38ErrCod == 0 )
               {
                  /* Execute user subroutine: 'CARREGASDT' */
                  S131 ();
                  if ( returnInSub )
                  {
                     this.cleanup();
                     if (true) return;
                  }
                  AV66SDT_Baseline.Add(AV65SDT_Funcao, 0);
                  AV65SDT_Funcao = new SdtSDT_Baselines_Funcao(context);
               }
               else
               {
                  AV39Erros = (short)(AV39Erros+1);
               }
               H8C0( false, 18) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV51Linha, "")), 17, Gx_line+0, 799, Gx_line+15, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+18);
               AV50Lidas = (short)(AV50Lidas+1);
               AV53Ln = (short)(AV53Ln+1);
            }
            AV74Totais = StringUtil.Trim( StringUtil.Str( (decimal)(AV50Lidas), 4, 0)) + " linhas processadas" + ((0==AV39Erros) ? "." : "  -  "+StringUtil.Trim( StringUtil.Str( (decimal)(AV39Erros), 4, 0))+" linhas N�O ser�o importadas.");
            H8C0( false, 35) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV74Totais, "")), 25, Gx_line+17, 755, Gx_line+35, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+35);
            AV40ExcelDocument.Close();
            AV78WebSession.Set("SDTBaseline", AV66SDT_Baseline.ToXml(false, true, "SDT_Baselines", "GxEv3Up14_MeetrikaVs3"));
            context.nUserReturn = 1;
            this.cleanup();
            if (true) return;
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H8C0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LERNOMEFUNCAO' Routine */
         AV85Nome = StringUtil.Trim( AV40ExcelDocument.get_Cells(AV53Ln, AV90ColNomen, 1, 1).Text);
         AV85Nome = StringUtil.Trim( AV85Nome);
         AV85Nome = StringUtil.StringReplace( AV85Nome, "'", "");
         AV85Nome = StringUtil.StringReplace( AV85Nome, "\"", "");
         AV85Nome = StringUtil.StringReplace( AV85Nome, "�", "");
         AV65SDT_Funcao.gxTpr_Nome = AV85Nome;
      }

      protected void S121( )
      {
         /* 'OPENFILE' Routine */
         AV38ErrCod = AV40ExcelDocument.Open(AV9Arquivo);
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV8Aba)) )
         {
            AV38ErrCod = AV40ExcelDocument.SelectSheet(AV8Aba);
         }
      }

      protected void S131( )
      {
         /* 'CARREGASDT' Routine */
         AV65SDT_Funcao.gxTpr_Observacao = AV40ExcelDocument.get_Cells(AV53Ln, AV94ColObservn, 1, 1).Text;
         AV65SDT_Funcao.gxTpr_Der = (short)(AV40ExcelDocument.get_Cells(AV53Ln, AV92ColDERn, 1, 1).Number);
         if ( (0==AV65SDT_Funcao.gxTpr_Der) )
         {
            AV65SDT_Funcao.gxTpr_Der = (short)(NumberUtil.Val( AV40ExcelDocument.get_Cells(AV53Ln, AV92ColDERn, 1, 1).Value, "."));
         }
         AV65SDT_Funcao.gxTpr_Rlr = (short)(AV40ExcelDocument.get_Cells(AV53Ln, AV93ColRLRn, 1, 1).Number);
         if ( (0==AV65SDT_Funcao.gxTpr_Rlr) )
         {
            AV65SDT_Funcao.gxTpr_Rlr = (short)(NumberUtil.Val( AV40ExcelDocument.get_Cells(AV53Ln, AV93ColRLRn, 1, 1).Value, "."));
         }
         if ( StringUtil.StringSearch( AV51Linha, "cadastrada", 1) == 0 )
         {
            AV51Linha = AV51Linha + "Fun��o de " + ((StringUtil.StringSearch( AV86TipoExterna, AV65SDT_Funcao.gxTpr_Tipo, 1)>0) ? "TRN " : "Dados ") + AV65SDT_Funcao.gxTpr_Nome + ", Tipo " + AV65SDT_Funcao.gxTpr_Tipo + ", DER: " + StringUtil.Trim( StringUtil.Str( (decimal)(AV65SDT_Funcao.gxTpr_Der), 4, 0)) + ", RLR: " + StringUtil.Trim( StringUtil.Str( (decimal)(AV65SDT_Funcao.gxTpr_Rlr), 4, 0));
         }
      }

      protected void H8C0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(Gx_page), "ZZZZZ9")), 708, Gx_line+0, 747, Gx_line+15, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("{{Pages}}", 767, Gx_line+0, 816, Gx_line+14, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("de", 750, Gx_line+0, 764, Gx_line+14, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("P�gina", 675, Gx_line+0, 710, Gx_line+14, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+15);
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(context.localUtil.Format( Gx_date, "99/99/99"), 675, Gx_line+9, 724, Gx_line+24, 2+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( Gx_time, "")), 733, Gx_line+9, 826, Gx_line+24, 0+256, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV98Sistema_Sigla, "@!")), 326, Gx_line+28, 483, Gx_line+46, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV57NomeArq, "")), 8, Gx_line+50, 808, Gx_line+68, 1, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 14, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("An�lise de Baseline para importar", 258, Gx_line+0, 552, Gx_line+25, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+83);
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV57NomeArq = "";
         AV86TipoExterna = "";
         AV96TxtLinha = "";
         AV40ExcelDocument = new ExcelDocumentI();
         AV51Linha = "";
         AV65SDT_Funcao = new SdtSDT_Baselines_Funcao(context);
         AV95NomeLimpo = "";
         GXt_char1 = "";
         scmdbuf = "";
         P008C2_A166FuncaoAPF_Nome = new String[] {""} ;
         P008C2_A360FuncaoAPF_SistemaCod = new int[1] ;
         P008C2_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         P008C2_A165FuncaoAPF_Codigo = new int[1] ;
         A166FuncaoAPF_Nome = "";
         P008C3_A369FuncaoDados_Nome = new String[] {""} ;
         P008C3_A370FuncaoDados_SistemaCod = new int[1] ;
         P008C3_A368FuncaoDados_Codigo = new int[1] ;
         A369FuncaoDados_Nome = "";
         AV66SDT_Baseline = new GxObjectCollection( context, "SDT_Baselines.Funcao", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Baselines_Funcao", "GeneXus.Programs");
         AV74Totais = "";
         AV78WebSession = context.GetSession();
         AV85Nome = "";
         Gx_date = DateTime.MinValue;
         Gx_time = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.aprc_carregasdtbaseline__default(),
            new Object[][] {
                new Object[] {
               P008C2_A166FuncaoAPF_Nome, P008C2_A360FuncaoAPF_SistemaCod, P008C2_n360FuncaoAPF_SistemaCod, P008C2_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               P008C3_A369FuncaoDados_Nome, P008C3_A370FuncaoDados_SistemaCod, P008C3_A368FuncaoDados_Codigo
               }
            }
         );
         Gx_time = context.localUtil.Time( );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_line = 0;
         Gx_time = context.localUtil.Time( );
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short AV63PraLinha ;
      private short AV90ColNomen ;
      private short AV91ColTipon ;
      private short AV92ColDERn ;
      private short AV93ColRLRn ;
      private short AV94ColObservn ;
      private short GxWebError ;
      private short AV53Ln ;
      private short AV38ErrCod ;
      private short AV39Erros ;
      private short AV50Lidas ;
      private int AV97Sistema_Codigo ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int A360FuncaoAPF_SistemaCod ;
      private int A165FuncaoAPF_Codigo ;
      private int A370FuncaoDados_SistemaCod ;
      private int A368FuncaoDados_Codigo ;
      private int Gx_OldLine ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV9Arquivo ;
      private String AV8Aba ;
      private String AV82FileName ;
      private String AV98Sistema_Sigla ;
      private String AV57NomeArq ;
      private String AV86TipoExterna ;
      private String AV96TxtLinha ;
      private String AV51Linha ;
      private String AV95NomeLimpo ;
      private String GXt_char1 ;
      private String scmdbuf ;
      private String AV74Totais ;
      private String Gx_time ;
      private DateTime Gx_date ;
      private bool entryPointCalled ;
      private bool returnInSub ;
      private bool n360FuncaoAPF_SistemaCod ;
      private String A166FuncaoAPF_Nome ;
      private String A369FuncaoDados_Nome ;
      private String AV85Nome ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P008C2_A166FuncaoAPF_Nome ;
      private int[] P008C2_A360FuncaoAPF_SistemaCod ;
      private bool[] P008C2_n360FuncaoAPF_SistemaCod ;
      private int[] P008C2_A165FuncaoAPF_Codigo ;
      private String[] P008C3_A369FuncaoDados_Nome ;
      private int[] P008C3_A370FuncaoDados_SistemaCod ;
      private int[] P008C3_A368FuncaoDados_Codigo ;
      private IGxSession AV78WebSession ;
      private ExcelDocumentI AV40ExcelDocument ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Baselines_Funcao ))]
      private IGxCollection AV66SDT_Baseline ;
      private SdtSDT_Baselines_Funcao AV65SDT_Funcao ;
   }

   public class aprc_carregasdtbaseline__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP008C2 ;
          prmP008C2 = new Object[] {
          new Object[] {"@AV97Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008C3 ;
          prmP008C3 = new Object[] {
          new Object[] {"@AV97Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P008C2", "SELECT [FuncaoAPF_Nome], [FuncaoAPF_SistemaCod], [FuncaoAPF_Codigo] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_SistemaCod] = @AV97Sistema_Codigo ORDER BY [FuncaoAPF_SistemaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008C2,100,0,true,false )
             ,new CursorDef("P008C3", "SELECT [FuncaoDados_Nome], [FuncaoDados_SistemaCod], [FuncaoDados_Codigo] FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_SistemaCod] = @AV97Sistema_Codigo ORDER BY [FuncaoDados_SistemaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008C3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
