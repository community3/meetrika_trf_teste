/*
               File: PromptServicoGrupo
        Description: Selecione Servico Grupo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:15:12.79
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptservicogrupo : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptservicogrupo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptservicogrupo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutServicoGrupo_Codigo ,
                           ref String aP1_InOutServicoGrupo_Descricao )
      {
         this.AV7InOutServicoGrupo_Codigo = aP0_InOutServicoGrupo_Codigo;
         this.AV8InOutServicoGrupo_Descricao = aP1_InOutServicoGrupo_Descricao;
         executePrivate();
         aP0_InOutServicoGrupo_Codigo=this.AV7InOutServicoGrupo_Codigo;
         aP1_InOutServicoGrupo_Descricao=this.AV8InOutServicoGrupo_Descricao;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         chkavServicogrupo_ativo = new GXCheckbox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkServicoGrupo_Ativo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_84 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_84_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_84_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17ServicoGrupo_Descricao1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ServicoGrupo_Descricao1", AV17ServicoGrupo_Descricao1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
               AV21ServicoGrupo_Descricao2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ServicoGrupo_Descricao2", AV21ServicoGrupo_Descricao2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
               AV25ServicoGrupo_Descricao3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ServicoGrupo_Descricao3", AV25ServicoGrupo_Descricao3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV32TFServicoGrupo_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFServicoGrupo_Descricao", AV32TFServicoGrupo_Descricao);
               AV33TFServicoGrupo_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFServicoGrupo_Descricao_Sel", AV33TFServicoGrupo_Descricao_Sel);
               AV36TFServicoGrupo_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFServicoGrupo_Ativo_Sel", StringUtil.Str( (decimal)(AV36TFServicoGrupo_Ativo_Sel), 1, 0));
               AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace", AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace);
               AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace", AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace);
               AV30ServicoGrupo_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ServicoGrupo_Ativo", AV30ServicoGrupo_Ativo);
               AV45Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ServicoGrupo_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ServicoGrupo_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ServicoGrupo_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV32TFServicoGrupo_Descricao, AV33TFServicoGrupo_Descricao_Sel, AV36TFServicoGrupo_Ativo_Sel, AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace, AV30ServicoGrupo_Ativo, AV45Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutServicoGrupo_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutServicoGrupo_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutServicoGrupo_Descricao = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutServicoGrupo_Descricao", AV8InOutServicoGrupo_Descricao);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA6T2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV45Pgmname = "PromptServicoGrupo";
               context.Gx_err = 0;
               WS6T2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE6T2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282315130");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptservicogrupo.aspx") + "?" + UrlEncode("" +AV7InOutServicoGrupo_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV8InOutServicoGrupo_Descricao))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICOGRUPO_DESCRICAO1", AV17ServicoGrupo_Descricao1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICOGRUPO_DESCRICAO2", AV21ServicoGrupo_Descricao2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICOGRUPO_DESCRICAO3", AV25ServicoGrupo_Descricao3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICOGRUPO_DESCRICAO", AV32TFServicoGrupo_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICOGRUPO_DESCRICAO_SEL", AV33TFServicoGrupo_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICOGRUPO_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFServicoGrupo_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_84", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_84), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV41GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV38DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV38DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSERVICOGRUPO_DESCRICAOTITLEFILTERDATA", AV31ServicoGrupo_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSERVICOGRUPO_DESCRICAOTITLEFILTERDATA", AV31ServicoGrupo_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSERVICOGRUPO_ATIVOTITLEFILTERDATA", AV35ServicoGrupo_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSERVICOGRUPO_ATIVOTITLEFILTERDATA", AV35ServicoGrupo_AtivoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV45Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTSERVICOGRUPO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutServicoGrupo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTSERVICOGRUPO_DESCRICAO", AV8InOutServicoGrupo_Descricao);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Caption", StringUtil.RTrim( Ddo_servicogrupo_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_servicogrupo_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Cls", StringUtil.RTrim( Ddo_servicogrupo_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_servicogrupo_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_servicogrupo_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_servicogrupo_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servicogrupo_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_servicogrupo_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_servicogrupo_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_servicogrupo_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_servicogrupo_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_servicogrupo_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_servicogrupo_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_servicogrupo_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_servicogrupo_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Datalistfixedvalues", StringUtil.RTrim( Ddo_servicogrupo_descricao_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_servicogrupo_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_servicogrupo_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_servicogrupo_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_servicogrupo_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_servicogrupo_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_servicogrupo_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Rangefilterfrom", StringUtil.RTrim( Ddo_servicogrupo_descricao_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Rangefilterto", StringUtil.RTrim( Ddo_servicogrupo_descricao_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_servicogrupo_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_servicogrupo_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_ATIVO_Caption", StringUtil.RTrim( Ddo_servicogrupo_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_ATIVO_Tooltip", StringUtil.RTrim( Ddo_servicogrupo_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_ATIVO_Cls", StringUtil.RTrim( Ddo_servicogrupo_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_servicogrupo_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_servicogrupo_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servicogrupo_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_servicogrupo_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_servicogrupo_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_servicogrupo_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_servicogrupo_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_ATIVO_Filterisrange", StringUtil.BoolToStr( Ddo_servicogrupo_ativo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_servicogrupo_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_servicogrupo_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_servicogrupo_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_ATIVO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_servicogrupo_ativo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_ATIVO_Sortasc", StringUtil.RTrim( Ddo_servicogrupo_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_servicogrupo_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_ATIVO_Loadingdata", StringUtil.RTrim( Ddo_servicogrupo_ativo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_servicogrupo_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_ATIVO_Rangefilterfrom", StringUtil.RTrim( Ddo_servicogrupo_ativo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_ATIVO_Rangefilterto", StringUtil.RTrim( Ddo_servicogrupo_ativo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_ATIVO_Noresultsfound", StringUtil.RTrim( Ddo_servicogrupo_ativo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_servicogrupo_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_servicogrupo_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_servicogrupo_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_servicogrupo_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_servicogrupo_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_servicogrupo_ativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm6T2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptServicoGrupo" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Servico Grupo" ;
      }

      protected void WB6T0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_6T2( true) ;
         }
         else
         {
            wb_table1_2_6T2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_6T2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(93, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,93);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(94, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,94);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicogrupo_descricao_Internalname, AV32TFServicoGrupo_Descricao, StringUtil.RTrim( context.localUtil.Format( AV32TFServicoGrupo_Descricao, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,95);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicogrupo_descricao_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicogrupo_descricao_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptServicoGrupo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicogrupo_descricao_sel_Internalname, AV33TFServicoGrupo_Descricao_Sel, StringUtil.RTrim( context.localUtil.Format( AV33TFServicoGrupo_Descricao_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,96);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicogrupo_descricao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicogrupo_descricao_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptServicoGrupo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicogrupo_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFServicoGrupo_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV36TFServicoGrupo_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicogrupo_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicogrupo_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptServicoGrupo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SERVICOGRUPO_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Internalname, AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,99);\"", 0, edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptServicoGrupo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SERVICOGRUPO_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servicogrupo_ativotitlecontrolidtoreplace_Internalname, AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,101);\"", 0, edtavDdo_servicogrupo_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptServicoGrupo.htm");
         }
         wbLoad = true;
      }

      protected void START6T2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Servico Grupo", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP6T0( ) ;
      }

      protected void WS6T2( )
      {
         START6T2( ) ;
         EVT6T2( ) ;
      }

      protected void EVT6T2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E116T2 */
                           E116T2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICOGRUPO_DESCRICAO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E126T2 */
                           E126T2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICOGRUPO_ATIVO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E136T2 */
                           E136T2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E146T2 */
                           E146T2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E156T2 */
                           E156T2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E166T2 */
                           E166T2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E176T2 */
                           E176T2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E186T2 */
                           E186T2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E196T2 */
                           E196T2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E206T2 */
                           E206T2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E216T2 */
                           E216T2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E226T2 */
                           E226T2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E236T2 */
                           E236T2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_84_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
                           SubsflControlProps_842( ) ;
                           AV28Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV44Select_GXI : context.convertURL( context.PathToRelativeUrl( AV28Select))));
                           A157ServicoGrupo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServicoGrupo_Codigo_Internalname), ",", "."));
                           A158ServicoGrupo_Descricao = cgiGet( edtServicoGrupo_Descricao_Internalname);
                           A159ServicoGrupo_Ativo = StringUtil.StrToBool( cgiGet( chkServicoGrupo_Ativo_Internalname));
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E246T2 */
                                 E246T2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E256T2 */
                                 E256T2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E266T2 */
                                 E266T2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Servicogrupo_descricao1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICOGRUPO_DESCRICAO1"), AV17ServicoGrupo_Descricao1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Servicogrupo_descricao2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICOGRUPO_DESCRICAO2"), AV21ServicoGrupo_Descricao2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Servicogrupo_descricao3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICOGRUPO_DESCRICAO3"), AV25ServicoGrupo_Descricao3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfservicogrupo_descricao Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOGRUPO_DESCRICAO"), AV32TFServicoGrupo_Descricao) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfservicogrupo_descricao_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOGRUPO_DESCRICAO_SEL"), AV33TFServicoGrupo_Descricao_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfservicogrupo_ativo_sel Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSERVICOGRUPO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV36TFServicoGrupo_Ativo_Sel )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E276T2 */
                                       E276T2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE6T2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm6T2( ) ;
            }
         }
      }

      protected void PA6T2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            chkavServicogrupo_ativo.Name = "vSERVICOGRUPO_ATIVO";
            chkavServicogrupo_ativo.WebTags = "";
            chkavServicogrupo_ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavServicogrupo_ativo_Internalname, "TitleCaption", chkavServicogrupo_ativo.Caption);
            chkavServicogrupo_ativo.CheckedValue = "false";
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("SERVICOGRUPO_DESCRICAO", "Nome", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("SERVICOGRUPO_DESCRICAO", "Nome", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("SERVICOGRUPO_DESCRICAO", "Nome", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            }
            GXCCtl = "SERVICOGRUPO_ATIVO_" + sGXsfl_84_idx;
            chkServicoGrupo_Ativo.Name = GXCCtl;
            chkServicoGrupo_Ativo.WebTags = "";
            chkServicoGrupo_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkServicoGrupo_Ativo_Internalname, "TitleCaption", chkServicoGrupo_Ativo.Caption);
            chkServicoGrupo_Ativo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_842( ) ;
         while ( nGXsfl_84_idx <= nRC_GXsfl_84 )
         {
            sendrow_842( ) ;
            nGXsfl_84_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_84_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_84_idx+1));
            sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
            SubsflControlProps_842( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17ServicoGrupo_Descricao1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV20DynamicFiltersOperator2 ,
                                       String AV21ServicoGrupo_Descricao2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       short AV24DynamicFiltersOperator3 ,
                                       String AV25ServicoGrupo_Descricao3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       String AV32TFServicoGrupo_Descricao ,
                                       String AV33TFServicoGrupo_Descricao_Sel ,
                                       short AV36TFServicoGrupo_Ativo_Sel ,
                                       String AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace ,
                                       String AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace ,
                                       bool AV30ServicoGrupo_Ativo ,
                                       String AV45Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF6T2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_SERVICOGRUPO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SERVICOGRUPO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A157ServicoGrupo_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_SERVICOGRUPO_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A158ServicoGrupo_Descricao, ""))));
         GxWebStd.gx_hidden_field( context, "SERVICOGRUPO_DESCRICAO", A158ServicoGrupo_Descricao);
         GxWebStd.gx_hidden_field( context, "gxhash_SERVICOGRUPO_ATIVO", GetSecureSignedToken( "", A159ServicoGrupo_Ativo));
         GxWebStd.gx_hidden_field( context, "SERVICOGRUPO_ATIVO", StringUtil.BoolToStr( A159ServicoGrupo_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF6T2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV45Pgmname = "PromptServicoGrupo";
         context.Gx_err = 0;
      }

      protected void RF6T2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 84;
         /* Execute user event: E256T2 */
         E256T2 ();
         nGXsfl_84_idx = 1;
         sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
         SubsflControlProps_842( ) ;
         nGXsfl_84_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_842( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16DynamicFiltersOperator1 ,
                                                 AV17ServicoGrupo_Descricao1 ,
                                                 AV18DynamicFiltersEnabled2 ,
                                                 AV19DynamicFiltersSelector2 ,
                                                 AV20DynamicFiltersOperator2 ,
                                                 AV21ServicoGrupo_Descricao2 ,
                                                 AV22DynamicFiltersEnabled3 ,
                                                 AV23DynamicFiltersSelector3 ,
                                                 AV24DynamicFiltersOperator3 ,
                                                 AV25ServicoGrupo_Descricao3 ,
                                                 AV33TFServicoGrupo_Descricao_Sel ,
                                                 AV32TFServicoGrupo_Descricao ,
                                                 AV36TFServicoGrupo_Ativo_Sel ,
                                                 A158ServicoGrupo_Descricao ,
                                                 A159ServicoGrupo_Ativo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV17ServicoGrupo_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV17ServicoGrupo_Descricao1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ServicoGrupo_Descricao1", AV17ServicoGrupo_Descricao1);
            lV17ServicoGrupo_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV17ServicoGrupo_Descricao1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ServicoGrupo_Descricao1", AV17ServicoGrupo_Descricao1);
            lV21ServicoGrupo_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV21ServicoGrupo_Descricao2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ServicoGrupo_Descricao2", AV21ServicoGrupo_Descricao2);
            lV21ServicoGrupo_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV21ServicoGrupo_Descricao2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ServicoGrupo_Descricao2", AV21ServicoGrupo_Descricao2);
            lV25ServicoGrupo_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV25ServicoGrupo_Descricao3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ServicoGrupo_Descricao3", AV25ServicoGrupo_Descricao3);
            lV25ServicoGrupo_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV25ServicoGrupo_Descricao3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ServicoGrupo_Descricao3", AV25ServicoGrupo_Descricao3);
            lV32TFServicoGrupo_Descricao = StringUtil.Concat( StringUtil.RTrim( AV32TFServicoGrupo_Descricao), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFServicoGrupo_Descricao", AV32TFServicoGrupo_Descricao);
            /* Using cursor H006T2 */
            pr_default.execute(0, new Object[] {lV17ServicoGrupo_Descricao1, lV17ServicoGrupo_Descricao1, lV21ServicoGrupo_Descricao2, lV21ServicoGrupo_Descricao2, lV25ServicoGrupo_Descricao3, lV25ServicoGrupo_Descricao3, lV32TFServicoGrupo_Descricao, AV33TFServicoGrupo_Descricao_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_84_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A159ServicoGrupo_Ativo = H006T2_A159ServicoGrupo_Ativo[0];
               A158ServicoGrupo_Descricao = H006T2_A158ServicoGrupo_Descricao[0];
               A157ServicoGrupo_Codigo = H006T2_A157ServicoGrupo_Codigo[0];
               /* Execute user event: E266T2 */
               E266T2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 84;
            WB6T0( ) ;
         }
         nGXsfl_84_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV16DynamicFiltersOperator1 ,
                                              AV17ServicoGrupo_Descricao1 ,
                                              AV18DynamicFiltersEnabled2 ,
                                              AV19DynamicFiltersSelector2 ,
                                              AV20DynamicFiltersOperator2 ,
                                              AV21ServicoGrupo_Descricao2 ,
                                              AV22DynamicFiltersEnabled3 ,
                                              AV23DynamicFiltersSelector3 ,
                                              AV24DynamicFiltersOperator3 ,
                                              AV25ServicoGrupo_Descricao3 ,
                                              AV33TFServicoGrupo_Descricao_Sel ,
                                              AV32TFServicoGrupo_Descricao ,
                                              AV36TFServicoGrupo_Ativo_Sel ,
                                              A158ServicoGrupo_Descricao ,
                                              A159ServicoGrupo_Ativo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV17ServicoGrupo_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV17ServicoGrupo_Descricao1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ServicoGrupo_Descricao1", AV17ServicoGrupo_Descricao1);
         lV17ServicoGrupo_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV17ServicoGrupo_Descricao1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ServicoGrupo_Descricao1", AV17ServicoGrupo_Descricao1);
         lV21ServicoGrupo_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV21ServicoGrupo_Descricao2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ServicoGrupo_Descricao2", AV21ServicoGrupo_Descricao2);
         lV21ServicoGrupo_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV21ServicoGrupo_Descricao2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ServicoGrupo_Descricao2", AV21ServicoGrupo_Descricao2);
         lV25ServicoGrupo_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV25ServicoGrupo_Descricao3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ServicoGrupo_Descricao3", AV25ServicoGrupo_Descricao3);
         lV25ServicoGrupo_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV25ServicoGrupo_Descricao3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ServicoGrupo_Descricao3", AV25ServicoGrupo_Descricao3);
         lV32TFServicoGrupo_Descricao = StringUtil.Concat( StringUtil.RTrim( AV32TFServicoGrupo_Descricao), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFServicoGrupo_Descricao", AV32TFServicoGrupo_Descricao);
         /* Using cursor H006T3 */
         pr_default.execute(1, new Object[] {lV17ServicoGrupo_Descricao1, lV17ServicoGrupo_Descricao1, lV21ServicoGrupo_Descricao2, lV21ServicoGrupo_Descricao2, lV25ServicoGrupo_Descricao3, lV25ServicoGrupo_Descricao3, lV32TFServicoGrupo_Descricao, AV33TFServicoGrupo_Descricao_Sel});
         GRID_nRecordCount = H006T3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ServicoGrupo_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ServicoGrupo_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ServicoGrupo_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV32TFServicoGrupo_Descricao, AV33TFServicoGrupo_Descricao_Sel, AV36TFServicoGrupo_Ativo_Sel, AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace, AV30ServicoGrupo_Ativo, AV45Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ServicoGrupo_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ServicoGrupo_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ServicoGrupo_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV32TFServicoGrupo_Descricao, AV33TFServicoGrupo_Descricao_Sel, AV36TFServicoGrupo_Ativo_Sel, AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace, AV30ServicoGrupo_Ativo, AV45Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ServicoGrupo_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ServicoGrupo_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ServicoGrupo_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV32TFServicoGrupo_Descricao, AV33TFServicoGrupo_Descricao_Sel, AV36TFServicoGrupo_Ativo_Sel, AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace, AV30ServicoGrupo_Ativo, AV45Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ServicoGrupo_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ServicoGrupo_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ServicoGrupo_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV32TFServicoGrupo_Descricao, AV33TFServicoGrupo_Descricao_Sel, AV36TFServicoGrupo_Ativo_Sel, AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace, AV30ServicoGrupo_Ativo, AV45Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ServicoGrupo_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ServicoGrupo_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ServicoGrupo_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV32TFServicoGrupo_Descricao, AV33TFServicoGrupo_Descricao_Sel, AV36TFServicoGrupo_Ativo_Sel, AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace, AV30ServicoGrupo_Ativo, AV45Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUP6T0( )
      {
         /* Before Start, stand alone formulas. */
         AV45Pgmname = "PromptServicoGrupo";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E246T2 */
         E246T2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV38DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vSERVICOGRUPO_DESCRICAOTITLEFILTERDATA"), AV31ServicoGrupo_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSERVICOGRUPO_ATIVOTITLEFILTERDATA"), AV35ServicoGrupo_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            AV30ServicoGrupo_Ativo = StringUtil.StrToBool( cgiGet( chkavServicogrupo_ativo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ServicoGrupo_Ativo", AV30ServicoGrupo_Ativo);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17ServicoGrupo_Descricao1 = cgiGet( edtavServicogrupo_descricao1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ServicoGrupo_Descricao1", AV17ServicoGrupo_Descricao1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            AV21ServicoGrupo_Descricao2 = cgiGet( edtavServicogrupo_descricao2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ServicoGrupo_Descricao2", AV21ServicoGrupo_Descricao2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            AV25ServicoGrupo_Descricao3 = cgiGet( edtavServicogrupo_descricao3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ServicoGrupo_Descricao3", AV25ServicoGrupo_Descricao3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            AV32TFServicoGrupo_Descricao = cgiGet( edtavTfservicogrupo_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFServicoGrupo_Descricao", AV32TFServicoGrupo_Descricao);
            AV33TFServicoGrupo_Descricao_Sel = cgiGet( edtavTfservicogrupo_descricao_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFServicoGrupo_Descricao_Sel", AV33TFServicoGrupo_Descricao_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfservicogrupo_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfservicogrupo_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSERVICOGRUPO_ATIVO_SEL");
               GX_FocusControl = edtavTfservicogrupo_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36TFServicoGrupo_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFServicoGrupo_Ativo_Sel", StringUtil.Str( (decimal)(AV36TFServicoGrupo_Ativo_Sel), 1, 0));
            }
            else
            {
               AV36TFServicoGrupo_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfservicogrupo_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFServicoGrupo_Ativo_Sel", StringUtil.Str( (decimal)(AV36TFServicoGrupo_Ativo_Sel), 1, 0));
            }
            AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace", AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace);
            AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_servicogrupo_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace", AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_84 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_84"), ",", "."));
            AV40GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV41GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_servicogrupo_descricao_Caption = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Caption");
            Ddo_servicogrupo_descricao_Tooltip = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Tooltip");
            Ddo_servicogrupo_descricao_Cls = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Cls");
            Ddo_servicogrupo_descricao_Filteredtext_set = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Filteredtext_set");
            Ddo_servicogrupo_descricao_Selectedvalue_set = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Selectedvalue_set");
            Ddo_servicogrupo_descricao_Dropdownoptionstype = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Dropdownoptionstype");
            Ddo_servicogrupo_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_servicogrupo_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Includesortasc"));
            Ddo_servicogrupo_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Includesortdsc"));
            Ddo_servicogrupo_descricao_Sortedstatus = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Sortedstatus");
            Ddo_servicogrupo_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Includefilter"));
            Ddo_servicogrupo_descricao_Filtertype = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Filtertype");
            Ddo_servicogrupo_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Filterisrange"));
            Ddo_servicogrupo_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Includedatalist"));
            Ddo_servicogrupo_descricao_Datalisttype = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Datalisttype");
            Ddo_servicogrupo_descricao_Datalistfixedvalues = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Datalistfixedvalues");
            Ddo_servicogrupo_descricao_Datalistproc = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Datalistproc");
            Ddo_servicogrupo_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_servicogrupo_descricao_Sortasc = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Sortasc");
            Ddo_servicogrupo_descricao_Sortdsc = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Sortdsc");
            Ddo_servicogrupo_descricao_Loadingdata = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Loadingdata");
            Ddo_servicogrupo_descricao_Cleanfilter = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Cleanfilter");
            Ddo_servicogrupo_descricao_Rangefilterfrom = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Rangefilterfrom");
            Ddo_servicogrupo_descricao_Rangefilterto = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Rangefilterto");
            Ddo_servicogrupo_descricao_Noresultsfound = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Noresultsfound");
            Ddo_servicogrupo_descricao_Searchbuttontext = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Searchbuttontext");
            Ddo_servicogrupo_ativo_Caption = cgiGet( "DDO_SERVICOGRUPO_ATIVO_Caption");
            Ddo_servicogrupo_ativo_Tooltip = cgiGet( "DDO_SERVICOGRUPO_ATIVO_Tooltip");
            Ddo_servicogrupo_ativo_Cls = cgiGet( "DDO_SERVICOGRUPO_ATIVO_Cls");
            Ddo_servicogrupo_ativo_Selectedvalue_set = cgiGet( "DDO_SERVICOGRUPO_ATIVO_Selectedvalue_set");
            Ddo_servicogrupo_ativo_Dropdownoptionstype = cgiGet( "DDO_SERVICOGRUPO_ATIVO_Dropdownoptionstype");
            Ddo_servicogrupo_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_SERVICOGRUPO_ATIVO_Titlecontrolidtoreplace");
            Ddo_servicogrupo_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SERVICOGRUPO_ATIVO_Includesortasc"));
            Ddo_servicogrupo_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SERVICOGRUPO_ATIVO_Includesortdsc"));
            Ddo_servicogrupo_ativo_Sortedstatus = cgiGet( "DDO_SERVICOGRUPO_ATIVO_Sortedstatus");
            Ddo_servicogrupo_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SERVICOGRUPO_ATIVO_Includefilter"));
            Ddo_servicogrupo_ativo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SERVICOGRUPO_ATIVO_Filterisrange"));
            Ddo_servicogrupo_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SERVICOGRUPO_ATIVO_Includedatalist"));
            Ddo_servicogrupo_ativo_Datalisttype = cgiGet( "DDO_SERVICOGRUPO_ATIVO_Datalisttype");
            Ddo_servicogrupo_ativo_Datalistfixedvalues = cgiGet( "DDO_SERVICOGRUPO_ATIVO_Datalistfixedvalues");
            Ddo_servicogrupo_ativo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SERVICOGRUPO_ATIVO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_servicogrupo_ativo_Sortasc = cgiGet( "DDO_SERVICOGRUPO_ATIVO_Sortasc");
            Ddo_servicogrupo_ativo_Sortdsc = cgiGet( "DDO_SERVICOGRUPO_ATIVO_Sortdsc");
            Ddo_servicogrupo_ativo_Loadingdata = cgiGet( "DDO_SERVICOGRUPO_ATIVO_Loadingdata");
            Ddo_servicogrupo_ativo_Cleanfilter = cgiGet( "DDO_SERVICOGRUPO_ATIVO_Cleanfilter");
            Ddo_servicogrupo_ativo_Rangefilterfrom = cgiGet( "DDO_SERVICOGRUPO_ATIVO_Rangefilterfrom");
            Ddo_servicogrupo_ativo_Rangefilterto = cgiGet( "DDO_SERVICOGRUPO_ATIVO_Rangefilterto");
            Ddo_servicogrupo_ativo_Noresultsfound = cgiGet( "DDO_SERVICOGRUPO_ATIVO_Noresultsfound");
            Ddo_servicogrupo_ativo_Searchbuttontext = cgiGet( "DDO_SERVICOGRUPO_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_servicogrupo_descricao_Activeeventkey = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Activeeventkey");
            Ddo_servicogrupo_descricao_Filteredtext_get = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Filteredtext_get");
            Ddo_servicogrupo_descricao_Selectedvalue_get = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Selectedvalue_get");
            Ddo_servicogrupo_ativo_Activeeventkey = cgiGet( "DDO_SERVICOGRUPO_ATIVO_Activeeventkey");
            Ddo_servicogrupo_ativo_Selectedvalue_get = cgiGet( "DDO_SERVICOGRUPO_ATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICOGRUPO_DESCRICAO1"), AV17ServicoGrupo_Descricao1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICOGRUPO_DESCRICAO2"), AV21ServicoGrupo_Descricao2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICOGRUPO_DESCRICAO3"), AV25ServicoGrupo_Descricao3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOGRUPO_DESCRICAO"), AV32TFServicoGrupo_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOGRUPO_DESCRICAO_SEL"), AV33TFServicoGrupo_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSERVICOGRUPO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV36TFServicoGrupo_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E246T2 */
         E246T2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E246T2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "SERVICOGRUPO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "SERVICOGRUPO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "SERVICOGRUPO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfservicogrupo_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservicogrupo_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicogrupo_descricao_Visible), 5, 0)));
         edtavTfservicogrupo_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservicogrupo_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicogrupo_descricao_sel_Visible), 5, 0)));
         edtavTfservicogrupo_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservicogrupo_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicogrupo_ativo_sel_Visible), 5, 0)));
         Ddo_servicogrupo_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_ServicoGrupo_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicogrupo_descricao_Internalname, "TitleControlIdToReplace", Ddo_servicogrupo_descricao_Titlecontrolidtoreplace);
         AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace = Ddo_servicogrupo_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace", AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace);
         edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_servicogrupo_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_ServicoGrupo_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicogrupo_ativo_Internalname, "TitleControlIdToReplace", Ddo_servicogrupo_ativo_Titlecontrolidtoreplace);
         AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace = Ddo_servicogrupo_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace", AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace);
         edtavDdo_servicogrupo_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_servicogrupo_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servicogrupo_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Grupo de /servi�o";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome", 0);
         cmbavOrderedby.addItem("2", "Ativo?", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV38DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV38DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E256T2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV31ServicoGrupo_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV35ServicoGrupo_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtServicoGrupo_Descricao_Titleformat = 2;
         edtServicoGrupo_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoGrupo_Descricao_Internalname, "Title", edtServicoGrupo_Descricao_Title);
         chkServicoGrupo_Ativo_Titleformat = 2;
         chkServicoGrupo_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo?", AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkServicoGrupo_Ativo_Internalname, "Title", chkServicoGrupo_Ativo.Title.Text);
         AV40GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40GridCurrentPage), 10, 0)));
         AV41GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV31ServicoGrupo_DescricaoTitleFilterData", AV31ServicoGrupo_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35ServicoGrupo_AtivoTitleFilterData", AV35ServicoGrupo_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E116T2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV39PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV39PageToGo) ;
         }
      }

      protected void E126T2( )
      {
         /* Ddo_servicogrupo_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servicogrupo_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicogrupo_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicogrupo_descricao_Internalname, "SortedStatus", Ddo_servicogrupo_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servicogrupo_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicogrupo_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicogrupo_descricao_Internalname, "SortedStatus", Ddo_servicogrupo_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servicogrupo_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV32TFServicoGrupo_Descricao = Ddo_servicogrupo_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFServicoGrupo_Descricao", AV32TFServicoGrupo_Descricao);
            AV33TFServicoGrupo_Descricao_Sel = Ddo_servicogrupo_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFServicoGrupo_Descricao_Sel", AV33TFServicoGrupo_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E136T2( )
      {
         /* Ddo_servicogrupo_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servicogrupo_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicogrupo_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicogrupo_ativo_Internalname, "SortedStatus", Ddo_servicogrupo_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servicogrupo_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicogrupo_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicogrupo_ativo_Internalname, "SortedStatus", Ddo_servicogrupo_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servicogrupo_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV36TFServicoGrupo_Ativo_Sel = (short)(NumberUtil.Val( Ddo_servicogrupo_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFServicoGrupo_Ativo_Sel", StringUtil.Str( (decimal)(AV36TFServicoGrupo_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E266T2( )
      {
         /* Grid_Load Routine */
         AV28Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV28Select);
         AV44Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 84;
         }
         sendrow_842( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_84_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(84, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E276T2 */
         E276T2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E276T2( )
      {
         /* Enter Routine */
         AV7InOutServicoGrupo_Codigo = A157ServicoGrupo_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutServicoGrupo_Codigo), 6, 0)));
         AV8InOutServicoGrupo_Descricao = A158ServicoGrupo_Descricao;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutServicoGrupo_Descricao", AV8InOutServicoGrupo_Descricao);
         context.setWebReturnParms(new Object[] {(int)AV7InOutServicoGrupo_Codigo,(String)AV8InOutServicoGrupo_Descricao});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E146T2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E196T2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E156T2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ServicoGrupo_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ServicoGrupo_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ServicoGrupo_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV32TFServicoGrupo_Descricao, AV33TFServicoGrupo_Descricao_Sel, AV36TFServicoGrupo_Ativo_Sel, AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace, AV30ServicoGrupo_Ativo, AV45Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E206T2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E216T2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E166T2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ServicoGrupo_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ServicoGrupo_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ServicoGrupo_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV32TFServicoGrupo_Descricao, AV33TFServicoGrupo_Descricao_Sel, AV36TFServicoGrupo_Ativo_Sel, AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace, AV30ServicoGrupo_Ativo, AV45Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E226T2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E176T2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ServicoGrupo_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ServicoGrupo_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ServicoGrupo_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV32TFServicoGrupo_Descricao, AV33TFServicoGrupo_Descricao_Sel, AV36TFServicoGrupo_Ativo_Sel, AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace, AV30ServicoGrupo_Ativo, AV45Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E236T2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E186T2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_servicogrupo_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicogrupo_descricao_Internalname, "SortedStatus", Ddo_servicogrupo_descricao_Sortedstatus);
         Ddo_servicogrupo_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicogrupo_ativo_Internalname, "SortedStatus", Ddo_servicogrupo_ativo_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_servicogrupo_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicogrupo_descricao_Internalname, "SortedStatus", Ddo_servicogrupo_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_servicogrupo_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicogrupo_ativo_Internalname, "SortedStatus", Ddo_servicogrupo_ativo_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavServicogrupo_descricao1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicogrupo_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicogrupo_descricao1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICOGRUPO_DESCRICAO") == 0 )
         {
            edtavServicogrupo_descricao1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicogrupo_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicogrupo_descricao1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavServicogrupo_descricao2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicogrupo_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicogrupo_descricao2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SERVICOGRUPO_DESCRICAO") == 0 )
         {
            edtavServicogrupo_descricao2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicogrupo_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicogrupo_descricao2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavServicogrupo_descricao3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicogrupo_descricao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicogrupo_descricao3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SERVICOGRUPO_DESCRICAO") == 0 )
         {
            edtavServicogrupo_descricao3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicogrupo_descricao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicogrupo_descricao3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "SERVICOGRUPO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         AV21ServicoGrupo_Descricao2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ServicoGrupo_Descricao2", AV21ServicoGrupo_Descricao2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "SERVICOGRUPO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         AV25ServicoGrupo_Descricao3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ServicoGrupo_Descricao3", AV25ServicoGrupo_Descricao3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV30ServicoGrupo_Ativo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ServicoGrupo_Ativo", AV30ServicoGrupo_Ativo);
         AV32TFServicoGrupo_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFServicoGrupo_Descricao", AV32TFServicoGrupo_Descricao);
         Ddo_servicogrupo_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicogrupo_descricao_Internalname, "FilteredText_set", Ddo_servicogrupo_descricao_Filteredtext_set);
         AV33TFServicoGrupo_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFServicoGrupo_Descricao_Sel", AV33TFServicoGrupo_Descricao_Sel);
         Ddo_servicogrupo_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicogrupo_descricao_Internalname, "SelectedValue_set", Ddo_servicogrupo_descricao_Selectedvalue_set);
         AV36TFServicoGrupo_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFServicoGrupo_Ativo_Sel", StringUtil.Str( (decimal)(AV36TFServicoGrupo_Ativo_Sel), 1, 0));
         Ddo_servicogrupo_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicogrupo_ativo_Internalname, "SelectedValue_set", Ddo_servicogrupo_ativo_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "SERVICOGRUPO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17ServicoGrupo_Descricao1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ServicoGrupo_Descricao1", AV17ServicoGrupo_Descricao1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICOGRUPO_DESCRICAO") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17ServicoGrupo_Descricao1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ServicoGrupo_Descricao1", AV17ServicoGrupo_Descricao1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SERVICOGRUPO_DESCRICAO") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV21ServicoGrupo_Descricao2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ServicoGrupo_Descricao2", AV21ServicoGrupo_Descricao2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SERVICOGRUPO_DESCRICAO") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV25ServicoGrupo_Descricao3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ServicoGrupo_Descricao3", AV25ServicoGrupo_Descricao3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (false==AV30ServicoGrupo_Ativo) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "SERVICOGRUPO_ATIVO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.BoolToStr( AV30ServicoGrupo_Ativo);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TFServicoGrupo_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICOGRUPO_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV32TFServicoGrupo_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFServicoGrupo_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICOGRUPO_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV33TFServicoGrupo_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV36TFServicoGrupo_Ativo_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICOGRUPO_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV36TFServicoGrupo_Ativo_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV45Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICOGRUPO_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ServicoGrupo_Descricao1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17ServicoGrupo_Descricao1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SERVICOGRUPO_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21ServicoGrupo_Descricao2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21ServicoGrupo_Descricao2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SERVICOGRUPO_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ServicoGrupo_Descricao3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25ServicoGrupo_Descricao3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_6T2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_6T2( true) ;
         }
         else
         {
            wb_table2_5_6T2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_6T2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_78_6T2( true) ;
         }
         else
         {
            wb_table3_78_6T2( false) ;
         }
         return  ;
      }

      protected void wb_table3_78_6T2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_6T2e( true) ;
         }
         else
         {
            wb_table1_2_6T2e( false) ;
         }
      }

      protected void wb_table3_78_6T2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_81_6T2( true) ;
         }
         else
         {
            wb_table4_81_6T2( false) ;
         }
         return  ;
      }

      protected void wb_table4_81_6T2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_78_6T2e( true) ;
         }
         else
         {
            wb_table3_78_6T2e( false) ;
         }
      }

      protected void wb_table4_81_6T2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"84\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServicoGrupo_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtServicoGrupo_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServicoGrupo_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkServicoGrupo_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkServicoGrupo_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkServicoGrupo_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A157ServicoGrupo_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A158ServicoGrupo_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServicoGrupo_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServicoGrupo_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A159ServicoGrupo_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkServicoGrupo_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkServicoGrupo_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 84 )
         {
            wbEnd = 0;
            nRC_GXsfl_84 = (short)(nGXsfl_84_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_81_6T2e( true) ;
         }
         else
         {
            wb_table4_81_6T2e( false) ;
         }
      }

      protected void wb_table2_5_6T2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptServicoGrupo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptServicoGrupo.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptServicoGrupo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_6T2( true) ;
         }
         else
         {
            wb_table5_14_6T2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_6T2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_6T2e( true) ;
         }
         else
         {
            wb_table2_5_6T2e( false) ;
         }
      }

      protected void wb_table5_14_6T2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptServicoGrupo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextservicogrupo_ativo_Internalname, "Ativo?", "", "", lblFiltertextservicogrupo_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptServicoGrupo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavServicogrupo_ativo_Internalname, StringUtil.BoolToStr( AV30ServicoGrupo_Ativo), "", "", 1, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(21, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_23_6T2( true) ;
         }
         else
         {
            wb_table6_23_6T2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_6T2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptServicoGrupo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_6T2e( true) ;
         }
         else
         {
            wb_table5_14_6T2e( false) ;
         }
      }

      protected void wb_table6_23_6T2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptServicoGrupo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_PromptServicoGrupo.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptServicoGrupo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_32_6T2( true) ;
         }
         else
         {
            wb_table7_32_6T2( false) ;
         }
         return  ;
      }

      protected void wb_table7_32_6T2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptServicoGrupo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptServicoGrupo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptServicoGrupo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", "", true, "HLP_PromptServicoGrupo.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptServicoGrupo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_49_6T2( true) ;
         }
         else
         {
            wb_table8_49_6T2( false) ;
         }
         return  ;
      }

      protected void wb_table8_49_6T2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptServicoGrupo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptServicoGrupo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptServicoGrupo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", "", true, "HLP_PromptServicoGrupo.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptServicoGrupo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_66_6T2( true) ;
         }
         else
         {
            wb_table9_66_6T2( false) ;
         }
         return  ;
      }

      protected void wb_table9_66_6T2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptServicoGrupo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_6T2e( true) ;
         }
         else
         {
            wb_table6_23_6T2e( false) ;
         }
      }

      protected void wb_table9_66_6T2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", "", true, "HLP_PromptServicoGrupo.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServicogrupo_descricao3_Internalname, AV25ServicoGrupo_Descricao3, StringUtil.RTrim( context.localUtil.Format( AV25ServicoGrupo_Descricao3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,71);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServicogrupo_descricao3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServicogrupo_descricao3_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptServicoGrupo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_66_6T2e( true) ;
         }
         else
         {
            wb_table9_66_6T2e( false) ;
         }
      }

      protected void wb_table8_49_6T2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", "", true, "HLP_PromptServicoGrupo.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServicogrupo_descricao2_Internalname, AV21ServicoGrupo_Descricao2, StringUtil.RTrim( context.localUtil.Format( AV21ServicoGrupo_Descricao2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServicogrupo_descricao2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServicogrupo_descricao2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptServicoGrupo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_49_6T2e( true) ;
         }
         else
         {
            wb_table8_49_6T2e( false) ;
         }
      }

      protected void wb_table7_32_6T2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,35);\"", "", true, "HLP_PromptServicoGrupo.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServicogrupo_descricao1_Internalname, AV17ServicoGrupo_Descricao1, StringUtil.RTrim( context.localUtil.Format( AV17ServicoGrupo_Descricao1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServicogrupo_descricao1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServicogrupo_descricao1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptServicoGrupo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_32_6T2e( true) ;
         }
         else
         {
            wb_table7_32_6T2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutServicoGrupo_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutServicoGrupo_Codigo), 6, 0)));
         AV8InOutServicoGrupo_Descricao = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutServicoGrupo_Descricao", AV8InOutServicoGrupo_Descricao);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA6T2( ) ;
         WS6T2( ) ;
         WE6T2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042823151652");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptservicogrupo.js", "?202042823151652");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_842( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_84_idx;
         edtServicoGrupo_Codigo_Internalname = "SERVICOGRUPO_CODIGO_"+sGXsfl_84_idx;
         edtServicoGrupo_Descricao_Internalname = "SERVICOGRUPO_DESCRICAO_"+sGXsfl_84_idx;
         chkServicoGrupo_Ativo_Internalname = "SERVICOGRUPO_ATIVO_"+sGXsfl_84_idx;
      }

      protected void SubsflControlProps_fel_842( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_84_fel_idx;
         edtServicoGrupo_Codigo_Internalname = "SERVICOGRUPO_CODIGO_"+sGXsfl_84_fel_idx;
         edtServicoGrupo_Descricao_Internalname = "SERVICOGRUPO_DESCRICAO_"+sGXsfl_84_fel_idx;
         chkServicoGrupo_Ativo_Internalname = "SERVICOGRUPO_ATIVO_"+sGXsfl_84_fel_idx;
      }

      protected void sendrow_842( )
      {
         SubsflControlProps_842( ) ;
         WB6T0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_84_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_84_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_84_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 85,'',false,'',84)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV28Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV44Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV44Select_GXI : context.PathToRelativeUrl( AV28Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_84_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV28Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoGrupo_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A157ServicoGrupo_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoGrupo_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoGrupo_Descricao_Internalname,(String)A158ServicoGrupo_Descricao,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoGrupo_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkServicoGrupo_Ativo_Internalname,StringUtil.BoolToStr( A159ServicoGrupo_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, "gxhash_SERVICOGRUPO_CODIGO"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sGXsfl_84_idx, context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_SERVICOGRUPO_DESCRICAO"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sGXsfl_84_idx, StringUtil.RTrim( context.localUtil.Format( A158ServicoGrupo_Descricao, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_SERVICOGRUPO_ATIVO"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sGXsfl_84_idx, A159ServicoGrupo_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_84_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_84_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_84_idx+1));
            sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
            SubsflControlProps_842( ) ;
         }
         /* End function sendrow_842 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextservicogrupo_ativo_Internalname = "FILTERTEXTSERVICOGRUPO_ATIVO";
         chkavServicogrupo_ativo_Internalname = "vSERVICOGRUPO_ATIVO";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavServicogrupo_descricao1_Internalname = "vSERVICOGRUPO_DESCRICAO1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavServicogrupo_descricao2_Internalname = "vSERVICOGRUPO_DESCRICAO2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavServicogrupo_descricao3_Internalname = "vSERVICOGRUPO_DESCRICAO3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtServicoGrupo_Codigo_Internalname = "SERVICOGRUPO_CODIGO";
         edtServicoGrupo_Descricao_Internalname = "SERVICOGRUPO_DESCRICAO";
         chkServicoGrupo_Ativo_Internalname = "SERVICOGRUPO_ATIVO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfservicogrupo_descricao_Internalname = "vTFSERVICOGRUPO_DESCRICAO";
         edtavTfservicogrupo_descricao_sel_Internalname = "vTFSERVICOGRUPO_DESCRICAO_SEL";
         edtavTfservicogrupo_ativo_sel_Internalname = "vTFSERVICOGRUPO_ATIVO_SEL";
         Ddo_servicogrupo_descricao_Internalname = "DDO_SERVICOGRUPO_DESCRICAO";
         edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Internalname = "vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_servicogrupo_ativo_Internalname = "DDO_SERVICOGRUPO_ATIVO";
         edtavDdo_servicogrupo_ativotitlecontrolidtoreplace_Internalname = "vDDO_SERVICOGRUPO_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtServicoGrupo_Descricao_Jsonclick = "";
         edtServicoGrupo_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavServicogrupo_descricao1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavServicogrupo_descricao2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavServicogrupo_descricao3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         chkServicoGrupo_Ativo_Titleformat = 0;
         edtServicoGrupo_Descricao_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavServicogrupo_descricao3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavServicogrupo_descricao2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavServicogrupo_descricao1_Visible = 1;
         chkServicoGrupo_Ativo.Title.Text = "Ativo?";
         edtServicoGrupo_Descricao_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkServicoGrupo_Ativo.Caption = "";
         chkavServicogrupo_ativo.Caption = "";
         edtavDdo_servicogrupo_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavTfservicogrupo_ativo_sel_Jsonclick = "";
         edtavTfservicogrupo_ativo_sel_Visible = 1;
         edtavTfservicogrupo_descricao_sel_Jsonclick = "";
         edtavTfservicogrupo_descricao_sel_Visible = 1;
         edtavTfservicogrupo_descricao_Jsonclick = "";
         edtavTfservicogrupo_descricao_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_servicogrupo_ativo_Searchbuttontext = "Pesquisar";
         Ddo_servicogrupo_ativo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_servicogrupo_ativo_Rangefilterto = "At�";
         Ddo_servicogrupo_ativo_Rangefilterfrom = "Desde";
         Ddo_servicogrupo_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_servicogrupo_ativo_Loadingdata = "Carregando dados...";
         Ddo_servicogrupo_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_servicogrupo_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_servicogrupo_ativo_Datalistupdateminimumcharacters = 0;
         Ddo_servicogrupo_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_servicogrupo_ativo_Datalisttype = "FixedValues";
         Ddo_servicogrupo_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_servicogrupo_ativo_Filterisrange = Convert.ToBoolean( 0);
         Ddo_servicogrupo_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_servicogrupo_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_servicogrupo_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_servicogrupo_ativo_Titlecontrolidtoreplace = "";
         Ddo_servicogrupo_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servicogrupo_ativo_Cls = "ColumnSettings";
         Ddo_servicogrupo_ativo_Tooltip = "Op��es";
         Ddo_servicogrupo_ativo_Caption = "";
         Ddo_servicogrupo_descricao_Searchbuttontext = "Pesquisar";
         Ddo_servicogrupo_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_servicogrupo_descricao_Rangefilterto = "At�";
         Ddo_servicogrupo_descricao_Rangefilterfrom = "Desde";
         Ddo_servicogrupo_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_servicogrupo_descricao_Loadingdata = "Carregando dados...";
         Ddo_servicogrupo_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_servicogrupo_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_servicogrupo_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_servicogrupo_descricao_Datalistproc = "GetPromptServicoGrupoFilterData";
         Ddo_servicogrupo_descricao_Datalistfixedvalues = "";
         Ddo_servicogrupo_descricao_Datalisttype = "Dynamic";
         Ddo_servicogrupo_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_servicogrupo_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_servicogrupo_descricao_Filtertype = "Character";
         Ddo_servicogrupo_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_servicogrupo_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_servicogrupo_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_servicogrupo_descricao_Titlecontrolidtoreplace = "";
         Ddo_servicogrupo_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servicogrupo_descricao_Cls = "ColumnSettings";
         Ddo_servicogrupo_descricao_Tooltip = "Op��es";
         Ddo_servicogrupo_descricao_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Servico Grupo";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV30ServicoGrupo_Ativo',fld:'vSERVICOGRUPO_ATIVO',pic:'',nv:false},{av:'AV32TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV33TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV36TFServicoGrupo_Ativo_Sel',fld:'vTFSERVICOGRUPO_ATIVO_SEL',pic:'9',nv:0},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ServicoGrupo_Descricao1',fld:'vSERVICOGRUPO_DESCRICAO1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ServicoGrupo_Descricao2',fld:'vSERVICOGRUPO_DESCRICAO2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ServicoGrupo_Descricao3',fld:'vSERVICOGRUPO_DESCRICAO3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0}],oparms:[{av:'AV31ServicoGrupo_DescricaoTitleFilterData',fld:'vSERVICOGRUPO_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV35ServicoGrupo_AtivoTitleFilterData',fld:'vSERVICOGRUPO_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'edtServicoGrupo_Descricao_Titleformat',ctrl:'SERVICOGRUPO_DESCRICAO',prop:'Titleformat'},{av:'edtServicoGrupo_Descricao_Title',ctrl:'SERVICOGRUPO_DESCRICAO',prop:'Title'},{av:'chkServicoGrupo_Ativo_Titleformat',ctrl:'SERVICOGRUPO_ATIVO',prop:'Titleformat'},{av:'chkServicoGrupo_Ativo.Title.Text',ctrl:'SERVICOGRUPO_ATIVO',prop:'Title'},{av:'AV40GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV41GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E116T2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoGrupo_Descricao1',fld:'vSERVICOGRUPO_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoGrupo_Descricao2',fld:'vSERVICOGRUPO_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoGrupo_Descricao3',fld:'vSERVICOGRUPO_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV33TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV36TFServicoGrupo_Ativo_Sel',fld:'vTFSERVICOGRUPO_ATIVO_SEL',pic:'9',nv:0},{av:'AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ServicoGrupo_Ativo',fld:'vSERVICOGRUPO_ATIVO',pic:'',nv:false},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_SERVICOGRUPO_DESCRICAO.ONOPTIONCLICKED","{handler:'E126T2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoGrupo_Descricao1',fld:'vSERVICOGRUPO_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoGrupo_Descricao2',fld:'vSERVICOGRUPO_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoGrupo_Descricao3',fld:'vSERVICOGRUPO_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV33TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV36TFServicoGrupo_Ativo_Sel',fld:'vTFSERVICOGRUPO_ATIVO_SEL',pic:'9',nv:0},{av:'AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ServicoGrupo_Ativo',fld:'vSERVICOGRUPO_ATIVO',pic:'',nv:false},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_servicogrupo_descricao_Activeeventkey',ctrl:'DDO_SERVICOGRUPO_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_servicogrupo_descricao_Filteredtext_get',ctrl:'DDO_SERVICOGRUPO_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_servicogrupo_descricao_Selectedvalue_get',ctrl:'DDO_SERVICOGRUPO_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_servicogrupo_descricao_Sortedstatus',ctrl:'DDO_SERVICOGRUPO_DESCRICAO',prop:'SortedStatus'},{av:'AV32TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV33TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_servicogrupo_ativo_Sortedstatus',ctrl:'DDO_SERVICOGRUPO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SERVICOGRUPO_ATIVO.ONOPTIONCLICKED","{handler:'E136T2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoGrupo_Descricao1',fld:'vSERVICOGRUPO_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoGrupo_Descricao2',fld:'vSERVICOGRUPO_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoGrupo_Descricao3',fld:'vSERVICOGRUPO_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV33TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV36TFServicoGrupo_Ativo_Sel',fld:'vTFSERVICOGRUPO_ATIVO_SEL',pic:'9',nv:0},{av:'AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ServicoGrupo_Ativo',fld:'vSERVICOGRUPO_ATIVO',pic:'',nv:false},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_servicogrupo_ativo_Activeeventkey',ctrl:'DDO_SERVICOGRUPO_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_servicogrupo_ativo_Selectedvalue_get',ctrl:'DDO_SERVICOGRUPO_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_servicogrupo_ativo_Sortedstatus',ctrl:'DDO_SERVICOGRUPO_ATIVO',prop:'SortedStatus'},{av:'AV36TFServicoGrupo_Ativo_Sel',fld:'vTFSERVICOGRUPO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_servicogrupo_descricao_Sortedstatus',ctrl:'DDO_SERVICOGRUPO_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E266T2',iparms:[],oparms:[{av:'AV28Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E276T2',iparms:[{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',hsh:true,nv:''}],oparms:[{av:'AV7InOutServicoGrupo_Codigo',fld:'vINOUTSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutServicoGrupo_Descricao',fld:'vINOUTSERVICOGRUPO_DESCRICAO',pic:'',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E146T2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoGrupo_Descricao1',fld:'vSERVICOGRUPO_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoGrupo_Descricao2',fld:'vSERVICOGRUPO_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoGrupo_Descricao3',fld:'vSERVICOGRUPO_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV33TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV36TFServicoGrupo_Ativo_Sel',fld:'vTFSERVICOGRUPO_ATIVO_SEL',pic:'9',nv:0},{av:'AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ServicoGrupo_Ativo',fld:'vSERVICOGRUPO_ATIVO',pic:'',nv:false},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E196T2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E156T2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoGrupo_Descricao1',fld:'vSERVICOGRUPO_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoGrupo_Descricao2',fld:'vSERVICOGRUPO_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoGrupo_Descricao3',fld:'vSERVICOGRUPO_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV33TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV36TFServicoGrupo_Ativo_Sel',fld:'vTFSERVICOGRUPO_ATIVO_SEL',pic:'9',nv:0},{av:'AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ServicoGrupo_Ativo',fld:'vSERVICOGRUPO_ATIVO',pic:'',nv:false},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoGrupo_Descricao2',fld:'vSERVICOGRUPO_DESCRICAO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoGrupo_Descricao3',fld:'vSERVICOGRUPO_DESCRICAO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoGrupo_Descricao1',fld:'vSERVICOGRUPO_DESCRICAO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavServicogrupo_descricao2_Visible',ctrl:'vSERVICOGRUPO_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavServicogrupo_descricao3_Visible',ctrl:'vSERVICOGRUPO_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavServicogrupo_descricao1_Visible',ctrl:'vSERVICOGRUPO_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E206T2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavServicogrupo_descricao1_Visible',ctrl:'vSERVICOGRUPO_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E216T2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E166T2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoGrupo_Descricao1',fld:'vSERVICOGRUPO_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoGrupo_Descricao2',fld:'vSERVICOGRUPO_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoGrupo_Descricao3',fld:'vSERVICOGRUPO_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV33TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV36TFServicoGrupo_Ativo_Sel',fld:'vTFSERVICOGRUPO_ATIVO_SEL',pic:'9',nv:0},{av:'AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ServicoGrupo_Ativo',fld:'vSERVICOGRUPO_ATIVO',pic:'',nv:false},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoGrupo_Descricao2',fld:'vSERVICOGRUPO_DESCRICAO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoGrupo_Descricao3',fld:'vSERVICOGRUPO_DESCRICAO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoGrupo_Descricao1',fld:'vSERVICOGRUPO_DESCRICAO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavServicogrupo_descricao2_Visible',ctrl:'vSERVICOGRUPO_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavServicogrupo_descricao3_Visible',ctrl:'vSERVICOGRUPO_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavServicogrupo_descricao1_Visible',ctrl:'vSERVICOGRUPO_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E226T2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavServicogrupo_descricao2_Visible',ctrl:'vSERVICOGRUPO_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E176T2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoGrupo_Descricao1',fld:'vSERVICOGRUPO_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoGrupo_Descricao2',fld:'vSERVICOGRUPO_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoGrupo_Descricao3',fld:'vSERVICOGRUPO_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV33TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV36TFServicoGrupo_Ativo_Sel',fld:'vTFSERVICOGRUPO_ATIVO_SEL',pic:'9',nv:0},{av:'AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ServicoGrupo_Ativo',fld:'vSERVICOGRUPO_ATIVO',pic:'',nv:false},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoGrupo_Descricao2',fld:'vSERVICOGRUPO_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoGrupo_Descricao3',fld:'vSERVICOGRUPO_DESCRICAO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoGrupo_Descricao1',fld:'vSERVICOGRUPO_DESCRICAO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavServicogrupo_descricao2_Visible',ctrl:'vSERVICOGRUPO_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavServicogrupo_descricao3_Visible',ctrl:'vSERVICOGRUPO_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavServicogrupo_descricao1_Visible',ctrl:'vSERVICOGRUPO_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E236T2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavServicogrupo_descricao3_Visible',ctrl:'vSERVICOGRUPO_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E186T2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoGrupo_Descricao1',fld:'vSERVICOGRUPO_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoGrupo_Descricao2',fld:'vSERVICOGRUPO_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoGrupo_Descricao3',fld:'vSERVICOGRUPO_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV33TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV36TFServicoGrupo_Ativo_Sel',fld:'vTFSERVICOGRUPO_ATIVO_SEL',pic:'9',nv:0},{av:'AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ServicoGrupo_Ativo',fld:'vSERVICOGRUPO_ATIVO',pic:'',nv:false},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV30ServicoGrupo_Ativo',fld:'vSERVICOGRUPO_ATIVO',pic:'',nv:false},{av:'AV32TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'Ddo_servicogrupo_descricao_Filteredtext_set',ctrl:'DDO_SERVICOGRUPO_DESCRICAO',prop:'FilteredText_set'},{av:'AV33TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_servicogrupo_descricao_Selectedvalue_set',ctrl:'DDO_SERVICOGRUPO_DESCRICAO',prop:'SelectedValue_set'},{av:'AV36TFServicoGrupo_Ativo_Sel',fld:'vTFSERVICOGRUPO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_servicogrupo_ativo_Selectedvalue_set',ctrl:'DDO_SERVICOGRUPO_ATIVO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoGrupo_Descricao1',fld:'vSERVICOGRUPO_DESCRICAO1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavServicogrupo_descricao1_Visible',ctrl:'vSERVICOGRUPO_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoGrupo_Descricao2',fld:'vSERVICOGRUPO_DESCRICAO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoGrupo_Descricao3',fld:'vSERVICOGRUPO_DESCRICAO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavServicogrupo_descricao2_Visible',ctrl:'vSERVICOGRUPO_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavServicogrupo_descricao3_Visible',ctrl:'vSERVICOGRUPO_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutServicoGrupo_Descricao = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_servicogrupo_descricao_Activeeventkey = "";
         Ddo_servicogrupo_descricao_Filteredtext_get = "";
         Ddo_servicogrupo_descricao_Selectedvalue_get = "";
         Ddo_servicogrupo_ativo_Activeeventkey = "";
         Ddo_servicogrupo_ativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17ServicoGrupo_Descricao1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV21ServicoGrupo_Descricao2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV25ServicoGrupo_Descricao3 = "";
         AV32TFServicoGrupo_Descricao = "";
         AV33TFServicoGrupo_Descricao_Sel = "";
         AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace = "";
         AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace = "";
         AV30ServicoGrupo_Ativo = true;
         AV45Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV38DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV31ServicoGrupo_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV35ServicoGrupo_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_servicogrupo_descricao_Filteredtext_set = "";
         Ddo_servicogrupo_descricao_Selectedvalue_set = "";
         Ddo_servicogrupo_descricao_Sortedstatus = "";
         Ddo_servicogrupo_ativo_Selectedvalue_set = "";
         Ddo_servicogrupo_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Select = "";
         AV44Select_GXI = "";
         A158ServicoGrupo_Descricao = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV17ServicoGrupo_Descricao1 = "";
         lV21ServicoGrupo_Descricao2 = "";
         lV25ServicoGrupo_Descricao3 = "";
         lV32TFServicoGrupo_Descricao = "";
         H006T2_A159ServicoGrupo_Ativo = new bool[] {false} ;
         H006T2_A158ServicoGrupo_Descricao = new String[] {""} ;
         H006T2_A157ServicoGrupo_Codigo = new int[1] ;
         H006T3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblFiltertextservicogrupo_ativo_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptservicogrupo__default(),
            new Object[][] {
                new Object[] {
               H006T2_A159ServicoGrupo_Ativo, H006T2_A158ServicoGrupo_Descricao, H006T2_A157ServicoGrupo_Codigo
               }
               , new Object[] {
               H006T3_AGRID_nRecordCount
               }
            }
         );
         AV45Pgmname = "PromptServicoGrupo";
         /* GeneXus formulas. */
         AV45Pgmname = "PromptServicoGrupo";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_84 ;
      private short nGXsfl_84_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV20DynamicFiltersOperator2 ;
      private short AV24DynamicFiltersOperator3 ;
      private short AV36TFServicoGrupo_Ativo_Sel ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_84_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtServicoGrupo_Descricao_Titleformat ;
      private short chkServicoGrupo_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutServicoGrupo_Codigo ;
      private int wcpOAV7InOutServicoGrupo_Codigo ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_servicogrupo_descricao_Datalistupdateminimumcharacters ;
      private int Ddo_servicogrupo_ativo_Datalistupdateminimumcharacters ;
      private int edtavTfservicogrupo_descricao_Visible ;
      private int edtavTfservicogrupo_descricao_sel_Visible ;
      private int edtavTfservicogrupo_ativo_sel_Visible ;
      private int edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_servicogrupo_ativotitlecontrolidtoreplace_Visible ;
      private int A157ServicoGrupo_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV39PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavServicogrupo_descricao1_Visible ;
      private int edtavServicogrupo_descricao2_Visible ;
      private int edtavServicogrupo_descricao3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV40GridCurrentPage ;
      private long AV41GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_servicogrupo_descricao_Activeeventkey ;
      private String Ddo_servicogrupo_descricao_Filteredtext_get ;
      private String Ddo_servicogrupo_descricao_Selectedvalue_get ;
      private String Ddo_servicogrupo_ativo_Activeeventkey ;
      private String Ddo_servicogrupo_ativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_84_idx="0001" ;
      private String AV45Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_servicogrupo_descricao_Caption ;
      private String Ddo_servicogrupo_descricao_Tooltip ;
      private String Ddo_servicogrupo_descricao_Cls ;
      private String Ddo_servicogrupo_descricao_Filteredtext_set ;
      private String Ddo_servicogrupo_descricao_Selectedvalue_set ;
      private String Ddo_servicogrupo_descricao_Dropdownoptionstype ;
      private String Ddo_servicogrupo_descricao_Titlecontrolidtoreplace ;
      private String Ddo_servicogrupo_descricao_Sortedstatus ;
      private String Ddo_servicogrupo_descricao_Filtertype ;
      private String Ddo_servicogrupo_descricao_Datalisttype ;
      private String Ddo_servicogrupo_descricao_Datalistfixedvalues ;
      private String Ddo_servicogrupo_descricao_Datalistproc ;
      private String Ddo_servicogrupo_descricao_Sortasc ;
      private String Ddo_servicogrupo_descricao_Sortdsc ;
      private String Ddo_servicogrupo_descricao_Loadingdata ;
      private String Ddo_servicogrupo_descricao_Cleanfilter ;
      private String Ddo_servicogrupo_descricao_Rangefilterfrom ;
      private String Ddo_servicogrupo_descricao_Rangefilterto ;
      private String Ddo_servicogrupo_descricao_Noresultsfound ;
      private String Ddo_servicogrupo_descricao_Searchbuttontext ;
      private String Ddo_servicogrupo_ativo_Caption ;
      private String Ddo_servicogrupo_ativo_Tooltip ;
      private String Ddo_servicogrupo_ativo_Cls ;
      private String Ddo_servicogrupo_ativo_Selectedvalue_set ;
      private String Ddo_servicogrupo_ativo_Dropdownoptionstype ;
      private String Ddo_servicogrupo_ativo_Titlecontrolidtoreplace ;
      private String Ddo_servicogrupo_ativo_Sortedstatus ;
      private String Ddo_servicogrupo_ativo_Datalisttype ;
      private String Ddo_servicogrupo_ativo_Datalistfixedvalues ;
      private String Ddo_servicogrupo_ativo_Sortasc ;
      private String Ddo_servicogrupo_ativo_Sortdsc ;
      private String Ddo_servicogrupo_ativo_Loadingdata ;
      private String Ddo_servicogrupo_ativo_Cleanfilter ;
      private String Ddo_servicogrupo_ativo_Rangefilterfrom ;
      private String Ddo_servicogrupo_ativo_Rangefilterto ;
      private String Ddo_servicogrupo_ativo_Noresultsfound ;
      private String Ddo_servicogrupo_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfservicogrupo_descricao_Internalname ;
      private String edtavTfservicogrupo_descricao_Jsonclick ;
      private String edtavTfservicogrupo_descricao_sel_Internalname ;
      private String edtavTfservicogrupo_descricao_sel_Jsonclick ;
      private String edtavTfservicogrupo_ativo_sel_Internalname ;
      private String edtavTfservicogrupo_ativo_sel_Jsonclick ;
      private String edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_servicogrupo_ativotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtServicoGrupo_Codigo_Internalname ;
      private String edtServicoGrupo_Descricao_Internalname ;
      private String chkServicoGrupo_Ativo_Internalname ;
      private String chkavServicogrupo_ativo_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavServicogrupo_descricao1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavServicogrupo_descricao2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavServicogrupo_descricao3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_servicogrupo_descricao_Internalname ;
      private String Ddo_servicogrupo_ativo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtServicoGrupo_Descricao_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String lblFiltertextservicogrupo_ativo_Internalname ;
      private String lblFiltertextservicogrupo_ativo_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavServicogrupo_descricao3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavServicogrupo_descricao2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavServicogrupo_descricao1_Jsonclick ;
      private String sGXsfl_84_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtServicoGrupo_Codigo_Jsonclick ;
      private String edtServicoGrupo_Descricao_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV30ServicoGrupo_Ativo ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_servicogrupo_descricao_Includesortasc ;
      private bool Ddo_servicogrupo_descricao_Includesortdsc ;
      private bool Ddo_servicogrupo_descricao_Includefilter ;
      private bool Ddo_servicogrupo_descricao_Filterisrange ;
      private bool Ddo_servicogrupo_descricao_Includedatalist ;
      private bool Ddo_servicogrupo_ativo_Includesortasc ;
      private bool Ddo_servicogrupo_ativo_Includesortdsc ;
      private bool Ddo_servicogrupo_ativo_Includefilter ;
      private bool Ddo_servicogrupo_ativo_Filterisrange ;
      private bool Ddo_servicogrupo_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool A159ServicoGrupo_Ativo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Select_IsBlob ;
      private String AV8InOutServicoGrupo_Descricao ;
      private String wcpOAV8InOutServicoGrupo_Descricao ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV17ServicoGrupo_Descricao1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV21ServicoGrupo_Descricao2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV25ServicoGrupo_Descricao3 ;
      private String AV32TFServicoGrupo_Descricao ;
      private String AV33TFServicoGrupo_Descricao_Sel ;
      private String AV34ddo_ServicoGrupo_DescricaoTitleControlIdToReplace ;
      private String AV37ddo_ServicoGrupo_AtivoTitleControlIdToReplace ;
      private String AV44Select_GXI ;
      private String A158ServicoGrupo_Descricao ;
      private String lV17ServicoGrupo_Descricao1 ;
      private String lV21ServicoGrupo_Descricao2 ;
      private String lV25ServicoGrupo_Descricao3 ;
      private String lV32TFServicoGrupo_Descricao ;
      private String AV28Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutServicoGrupo_Codigo ;
      private String aP1_InOutServicoGrupo_Descricao ;
      private GXCombobox cmbavOrderedby ;
      private GXCheckbox chkavServicogrupo_ativo ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkServicoGrupo_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private bool[] H006T2_A159ServicoGrupo_Ativo ;
      private String[] H006T2_A158ServicoGrupo_Descricao ;
      private int[] H006T2_A157ServicoGrupo_Codigo ;
      private long[] H006T3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV31ServicoGrupo_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV35ServicoGrupo_AtivoTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV38DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptservicogrupo__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H006T2( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV17ServicoGrupo_Descricao1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             short AV20DynamicFiltersOperator2 ,
                                             String AV21ServicoGrupo_Descricao2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             short AV24DynamicFiltersOperator3 ,
                                             String AV25ServicoGrupo_Descricao3 ,
                                             String AV33TFServicoGrupo_Descricao_Sel ,
                                             String AV32TFServicoGrupo_Descricao ,
                                             short AV36TFServicoGrupo_Ativo_Sel ,
                                             String A158ServicoGrupo_Descricao ,
                                             bool A159ServicoGrupo_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [13] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [ServicoGrupo_Ativo], [ServicoGrupo_Descricao], [ServicoGrupo_Codigo]";
         sFromString = " FROM [ServicoGrupo] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([ServicoGrupo_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICOGRUPO_DESCRICAO") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ServicoGrupo_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Descricao] like '%' + @lV17ServicoGrupo_Descricao1 + '%')";
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICOGRUPO_DESCRICAO") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ServicoGrupo_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Descricao] like '%' + @lV17ServicoGrupo_Descricao1 + '%')";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SERVICOGRUPO_DESCRICAO") == 0 ) && ( AV20DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21ServicoGrupo_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Descricao] like '%' + @lV21ServicoGrupo_Descricao2 + '%')";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SERVICOGRUPO_DESCRICAO") == 0 ) && ( AV20DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21ServicoGrupo_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Descricao] like '%' + @lV21ServicoGrupo_Descricao2 + '%')";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SERVICOGRUPO_DESCRICAO") == 0 ) && ( AV24DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ServicoGrupo_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Descricao] like '%' + @lV25ServicoGrupo_Descricao3 + '%')";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SERVICOGRUPO_DESCRICAO") == 0 ) && ( AV24DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ServicoGrupo_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Descricao] like '%' + @lV25ServicoGrupo_Descricao3 + '%')";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV33TFServicoGrupo_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TFServicoGrupo_Descricao)) ) )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Descricao] like @lV32TFServicoGrupo_Descricao)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFServicoGrupo_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Descricao] = @AV33TFServicoGrupo_Descricao_Sel)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV36TFServicoGrupo_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Ativo] = 1)";
         }
         if ( AV36TFServicoGrupo_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Ativo] = 0)";
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ServicoGrupo_Descricao]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ServicoGrupo_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ServicoGrupo_Ativo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ServicoGrupo_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [ServicoGrupo_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H006T3( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV17ServicoGrupo_Descricao1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             short AV20DynamicFiltersOperator2 ,
                                             String AV21ServicoGrupo_Descricao2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             short AV24DynamicFiltersOperator3 ,
                                             String AV25ServicoGrupo_Descricao3 ,
                                             String AV33TFServicoGrupo_Descricao_Sel ,
                                             String AV32TFServicoGrupo_Descricao ,
                                             short AV36TFServicoGrupo_Ativo_Sel ,
                                             String A158ServicoGrupo_Descricao ,
                                             bool A159ServicoGrupo_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [8] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [ServicoGrupo] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([ServicoGrupo_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICOGRUPO_DESCRICAO") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ServicoGrupo_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Descricao] like '%' + @lV17ServicoGrupo_Descricao1 + '%')";
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICOGRUPO_DESCRICAO") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ServicoGrupo_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Descricao] like '%' + @lV17ServicoGrupo_Descricao1 + '%')";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SERVICOGRUPO_DESCRICAO") == 0 ) && ( AV20DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21ServicoGrupo_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Descricao] like '%' + @lV21ServicoGrupo_Descricao2 + '%')";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SERVICOGRUPO_DESCRICAO") == 0 ) && ( AV20DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21ServicoGrupo_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Descricao] like '%' + @lV21ServicoGrupo_Descricao2 + '%')";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SERVICOGRUPO_DESCRICAO") == 0 ) && ( AV24DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ServicoGrupo_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Descricao] like '%' + @lV25ServicoGrupo_Descricao3 + '%')";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SERVICOGRUPO_DESCRICAO") == 0 ) && ( AV24DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ServicoGrupo_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Descricao] like '%' + @lV25ServicoGrupo_Descricao3 + '%')";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV33TFServicoGrupo_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TFServicoGrupo_Descricao)) ) )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Descricao] like @lV32TFServicoGrupo_Descricao)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFServicoGrupo_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Descricao] = @AV33TFServicoGrupo_Descricao_Sel)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV36TFServicoGrupo_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Ativo] = 1)";
         }
         if ( AV36TFServicoGrupo_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H006T2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] , (short)dynConstraints[16] , (bool)dynConstraints[17] );
               case 1 :
                     return conditional_H006T3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] , (short)dynConstraints[16] , (bool)dynConstraints[17] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH006T2 ;
          prmH006T2 = new Object[] {
          new Object[] {"@lV17ServicoGrupo_Descricao1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV17ServicoGrupo_Descricao1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV21ServicoGrupo_Descricao2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV21ServicoGrupo_Descricao2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV25ServicoGrupo_Descricao3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV25ServicoGrupo_Descricao3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV32TFServicoGrupo_Descricao",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV33TFServicoGrupo_Descricao_Sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH006T3 ;
          prmH006T3 = new Object[] {
          new Object[] {"@lV17ServicoGrupo_Descricao1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV17ServicoGrupo_Descricao1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV21ServicoGrupo_Descricao2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV21ServicoGrupo_Descricao2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV25ServicoGrupo_Descricao3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV25ServicoGrupo_Descricao3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV32TFServicoGrupo_Descricao",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV33TFServicoGrupo_Descricao_Sel",SqlDbType.VarChar,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H006T2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006T2,11,0,true,false )
             ,new CursorDef("H006T3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006T3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                return;
       }
    }

 }

}
