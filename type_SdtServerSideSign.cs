/*
               File: type_SdtServerSideSign
        Description: ServerSideSign
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:30:8.44
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtServerSideSign : GxUserType, IGxExternalObject
   {
      public SdtServerSideSign( )
      {
         initialize();
      }

      public SdtServerSideSign( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String signdocument( String gxTp_tipoFirma ,
                                  String gxTp_signerInfoPath ,
                                  String gxTp_signerInfoPass ,
                                  String gxTp_signerAlias ,
                                  String gxTp_pathToFile ,
                                  String gxTp_pathSigned )
      {
         String returnsigndocument ;
         if ( ServerSideSign_externalReference == null )
         {
            ServerSideSign_externalReference = new digitalSign.ServerSideSign();
         }
         returnsigndocument = "";
         returnsigndocument = (String)(ServerSideSign_externalReference.signDocument(gxTp_tipoFirma, gxTp_signerInfoPath, gxTp_signerInfoPass, gxTp_signerAlias, gxTp_pathToFile, gxTp_pathSigned));
         return returnsigndocument ;
      }

      public String verifydigitalsignature( String gxTp_tipoFirma ,
                                            String gxTp_certificateAuthorityPath ,
                                            String gxTp_certificateAuthorityRevocationsPath ,
                                            String gxTp_certificateAuthorityOcspService ,
                                            String gxTp_signerCertificateFromCAPath ,
                                            String gxTp_pathToFile ,
                                            String gxTp_pathToSign ,
                                            bool gxTp_checkNonRepudiation ,
                                            DateTime gxTp_fecha )
      {
         String returnverifydigitalsignature ;
         if ( ServerSideSign_externalReference == null )
         {
            ServerSideSign_externalReference = new digitalSign.ServerSideSign();
         }
         returnverifydigitalsignature = "";
         returnverifydigitalsignature = (String)(ServerSideSign_externalReference.verifyDigitalSignature(gxTp_tipoFirma, gxTp_certificateAuthorityPath, gxTp_certificateAuthorityRevocationsPath, gxTp_certificateAuthorityOcspService, gxTp_signerCertificateFromCAPath, gxTp_pathToFile, gxTp_pathToSign, gxTp_checkNonRepudiation, gxTp_fecha));
         return returnverifydigitalsignature ;
      }

      public String signdocumentencodedin64( String gxTp_tipoFirma ,
                                             String gxTp_signerInfoPath ,
                                             String gxTp_signerInfoPass ,
                                             String gxTp_signerAlias ,
                                             String gxTp_document64Str ,
                                             out String gxTp_signature64Str )
      {
         String returnsigndocumentencodedin64 ;
         gxTp_signature64Str = "";
         if ( ServerSideSign_externalReference == null )
         {
            ServerSideSign_externalReference = new digitalSign.ServerSideSign();
         }
         returnsigndocumentencodedin64 = "";
         returnsigndocumentencodedin64 = (String)(ServerSideSign_externalReference.signDocumentEncodedIn64(gxTp_tipoFirma, gxTp_signerInfoPath, gxTp_signerInfoPass, gxTp_signerAlias, gxTp_document64Str, out gxTp_signature64Str));
         return returnsigndocumentencodedin64 ;
      }

      public String verifydigitalsignatureencodedin64( String gxTp_tipoFirma ,
                                                       String gxTp_certificateAuthorityPath ,
                                                       String gxTp_certificateAuthorityRevocationsPath ,
                                                       String gxTp_certificateAuthorityOcspService ,
                                                       String gxTp_signerCertificateFromCAPath ,
                                                       String gxTp_document64Str ,
                                                       String gxTp_signature64Str ,
                                                       bool gxTp_checkNonRepudiation ,
                                                       DateTime gxTp_fecha )
      {
         String returnverifydigitalsignatureencodedin64 ;
         if ( ServerSideSign_externalReference == null )
         {
            ServerSideSign_externalReference = new digitalSign.ServerSideSign();
         }
         returnverifydigitalsignatureencodedin64 = "";
         returnverifydigitalsignatureencodedin64 = (String)(ServerSideSign_externalReference.verifyDigitalSignatureEncodedIn64(gxTp_tipoFirma, gxTp_certificateAuthorityPath, gxTp_certificateAuthorityRevocationsPath, gxTp_certificateAuthorityOcspService, gxTp_signerCertificateFromCAPath, gxTp_document64Str, gxTp_signature64Str, gxTp_checkNonRepudiation, gxTp_fecha));
         return returnverifydigitalsignatureencodedin64 ;
      }

      public String encodebase64( String gxTp_textToEncode )
      {
         String returnencodebase64 ;
         if ( ServerSideSign_externalReference == null )
         {
            ServerSideSign_externalReference = new digitalSign.ServerSideSign();
         }
         returnencodebase64 = "";
         returnencodebase64 = (String)(ServerSideSign_externalReference.EncodeBase64(gxTp_textToEncode));
         return returnencodebase64 ;
      }

      public String decodebase64( String gxTp_textToDecode )
      {
         String returndecodebase64 ;
         if ( ServerSideSign_externalReference == null )
         {
            ServerSideSign_externalReference = new digitalSign.ServerSideSign();
         }
         returndecodebase64 = "";
         returndecodebase64 = (String)(ServerSideSign_externalReference.DecodeBase64(gxTp_textToDecode));
         return returndecodebase64 ;
      }

      public void getsignerinformation( String gxTp_tipoFirma ,
                                        String gxTp_pathToFile )
      {
         if ( ServerSideSign_externalReference == null )
         {
            ServerSideSign_externalReference = new digitalSign.ServerSideSign();
         }
         ServerSideSign_externalReference.getSignerInformation(gxTp_tipoFirma, gxTp_pathToFile);
         return  ;
      }

      public void getsignerinformationencodedin64( String gxTp_tipoFirma ,
                                                   String gxTp_document64Str )
      {
         if ( ServerSideSign_externalReference == null )
         {
            ServerSideSign_externalReference = new digitalSign.ServerSideSign();
         }
         ServerSideSign_externalReference.getSignerInformationEncodedIn64(gxTp_tipoFirma, gxTp_document64Str);
         return  ;
      }

      public void getcertificateinformation( String gxTp_certificate )
      {
         if ( ServerSideSign_externalReference == null )
         {
            ServerSideSign_externalReference = new digitalSign.ServerSideSign();
         }
         ServerSideSign_externalReference.getSignerInformation(gxTp_certificate);
         return  ;
      }

      public bool gxTpr_Includexmlsignercertificate
      {
         get {
            if ( ServerSideSign_externalReference == null )
            {
               ServerSideSign_externalReference = new digitalSign.ServerSideSign();
            }
            return ServerSideSign_externalReference.IncludeXMLSignerCertificate ;
         }

         set {
            if ( ServerSideSign_externalReference == null )
            {
               ServerSideSign_externalReference = new digitalSign.ServerSideSign();
            }
            ServerSideSign_externalReference.IncludeXMLSignerCertificate = value;
         }

      }

      public int gxTpr_Pdfcertificationlevel
      {
         get {
            if ( ServerSideSign_externalReference == null )
            {
               ServerSideSign_externalReference = new digitalSign.ServerSideSign();
            }
            return ServerSideSign_externalReference.PdfCertificationLevel ;
         }

         set {
            if ( ServerSideSign_externalReference == null )
            {
               ServerSideSign_externalReference = new digitalSign.ServerSideSign();
            }
            ServerSideSign_externalReference.PdfCertificationLevel = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( ServerSideSign_externalReference == null )
            {
               ServerSideSign_externalReference = new digitalSign.ServerSideSign();
            }
            return ServerSideSign_externalReference ;
         }

         set {
            ServerSideSign_externalReference = (digitalSign.ServerSideSign)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected digitalSign.ServerSideSign ServerSideSign_externalReference=null ;
   }

}
