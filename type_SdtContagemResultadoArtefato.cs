/*
               File: type_SdtContagemResultadoArtefato
        Description: Artefatos das OS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:17:51.45
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ContagemResultadoArtefato" )]
   [XmlType(TypeName =  "ContagemResultadoArtefato" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtContagemResultadoArtefato : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContagemResultadoArtefato( )
      {
         /* Constructor for serialization */
         gxTv_SdtContagemResultadoArtefato_Mode = "";
      }

      public SdtContagemResultadoArtefato( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV1769ContagemResultadoArtefato_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV1769ContagemResultadoArtefato_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"ContagemResultadoArtefato_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "ContagemResultadoArtefato");
         metadata.Set("BT", "ContagemResultadoArtefato");
         metadata.Set("PK", "[ \"ContagemResultadoArtefato_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"ContagemResultadoArtefato_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Anexo_Codigo\" ],\"FKMap\":[ \"ContagemResultadoArtefato_AnxCod-Anexo_Codigo\" ] },{ \"FK\":[ \"Artefatos_Codigo\" ],\"FKMap\":[ \"ContagemResultadoArtefato_ArtefatoCod-Artefatos_Codigo\" ] },{ \"FK\":[ \"ContagemResultadoEvidencia_Codigo\" ],\"FKMap\":[ \"ContagemResultadoArtefato_EvdCod-ContagemResultadoEvidencia_Codigo\" ] },{ \"FK\":[ \"ContagemResultado_Codigo\" ],\"FKMap\":[ \"ContagemResultadoArtefato_OSCod-ContagemResultado_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoartefato_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoartefato_oscod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoartefato_artefatocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoartefato_evdcod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoartefato_anxcod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoartefato_evdcod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoartefato_anxcod_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtContagemResultadoArtefato deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtContagemResultadoArtefato)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtContagemResultadoArtefato obj ;
         obj = this;
         obj.gxTpr_Contagemresultadoartefato_codigo = deserialized.gxTpr_Contagemresultadoartefato_codigo;
         obj.gxTpr_Contagemresultadoartefato_oscod = deserialized.gxTpr_Contagemresultadoartefato_oscod;
         obj.gxTpr_Contagemresultadoartefato_artefatocod = deserialized.gxTpr_Contagemresultadoartefato_artefatocod;
         obj.gxTpr_Contagemresultadoartefato_evdcod = deserialized.gxTpr_Contagemresultadoartefato_evdcod;
         obj.gxTpr_Contagemresultadoartefato_anxcod = deserialized.gxTpr_Contagemresultadoartefato_anxcod;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Contagemresultadoartefato_codigo_Z = deserialized.gxTpr_Contagemresultadoartefato_codigo_Z;
         obj.gxTpr_Contagemresultadoartefato_oscod_Z = deserialized.gxTpr_Contagemresultadoartefato_oscod_Z;
         obj.gxTpr_Contagemresultadoartefato_artefatocod_Z = deserialized.gxTpr_Contagemresultadoartefato_artefatocod_Z;
         obj.gxTpr_Contagemresultadoartefato_evdcod_Z = deserialized.gxTpr_Contagemresultadoartefato_evdcod_Z;
         obj.gxTpr_Contagemresultadoartefato_anxcod_Z = deserialized.gxTpr_Contagemresultadoartefato_anxcod_Z;
         obj.gxTpr_Contagemresultadoartefato_evdcod_N = deserialized.gxTpr_Contagemresultadoartefato_evdcod_N;
         obj.gxTpr_Contagemresultadoartefato_anxcod_N = deserialized.gxTpr_Contagemresultadoartefato_anxcod_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoArtefato_Codigo") )
               {
                  gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoArtefato_OSCod") )
               {
                  gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_oscod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoArtefato_ArtefatoCod") )
               {
                  gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_artefatocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoArtefato_EvdCod") )
               {
                  gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoArtefato_AnxCod") )
               {
                  gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtContagemResultadoArtefato_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtContagemResultadoArtefato_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoArtefato_Codigo_Z") )
               {
                  gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoArtefato_OSCod_Z") )
               {
                  gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_oscod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoArtefato_ArtefatoCod_Z") )
               {
                  gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_artefatocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoArtefato_EvdCod_Z") )
               {
                  gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoArtefato_AnxCod_Z") )
               {
                  gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoArtefato_EvdCod_N") )
               {
                  gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoArtefato_AnxCod_N") )
               {
                  gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ContagemResultadoArtefato";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContagemResultadoArtefato_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultadoArtefato_OSCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_oscod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultadoArtefato_ArtefatoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_artefatocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultadoArtefato_EvdCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultadoArtefato_AnxCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtContagemResultadoArtefato_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoArtefato_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoArtefato_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoArtefato_OSCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_oscod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoArtefato_ArtefatoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_artefatocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoArtefato_EvdCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoArtefato_AnxCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoArtefato_EvdCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoArtefato_AnxCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContagemResultadoArtefato_Codigo", gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_codigo, false);
         AddObjectProperty("ContagemResultadoArtefato_OSCod", gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_oscod, false);
         AddObjectProperty("ContagemResultadoArtefato_ArtefatoCod", gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_artefatocod, false);
         AddObjectProperty("ContagemResultadoArtefato_EvdCod", gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod, false);
         AddObjectProperty("ContagemResultadoArtefato_AnxCod", gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtContagemResultadoArtefato_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtContagemResultadoArtefato_Initialized, false);
            AddObjectProperty("ContagemResultadoArtefato_Codigo_Z", gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_codigo_Z, false);
            AddObjectProperty("ContagemResultadoArtefato_OSCod_Z", gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_oscod_Z, false);
            AddObjectProperty("ContagemResultadoArtefato_ArtefatoCod_Z", gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_artefatocod_Z, false);
            AddObjectProperty("ContagemResultadoArtefato_EvdCod_Z", gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod_Z, false);
            AddObjectProperty("ContagemResultadoArtefato_AnxCod_Z", gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod_Z, false);
            AddObjectProperty("ContagemResultadoArtefato_EvdCod_N", gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod_N, false);
            AddObjectProperty("ContagemResultadoArtefato_AnxCod_N", gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ContagemResultadoArtefato_Codigo" )]
      [  XmlElement( ElementName = "ContagemResultadoArtefato_Codigo"   )]
      public int gxTpr_Contagemresultadoartefato_codigo
      {
         get {
            return gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_codigo ;
         }

         set {
            if ( gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_codigo != value )
            {
               gxTv_SdtContagemResultadoArtefato_Mode = "INS";
               this.gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_codigo_Z_SetNull( );
               this.gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_oscod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_artefatocod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod_Z_SetNull( );
            }
            gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoArtefato_OSCod" )]
      [  XmlElement( ElementName = "ContagemResultadoArtefato_OSCod"   )]
      public int gxTpr_Contagemresultadoartefato_oscod
      {
         get {
            return gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_oscod ;
         }

         set {
            gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_oscod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoArtefato_ArtefatoCod" )]
      [  XmlElement( ElementName = "ContagemResultadoArtefato_ArtefatoCod"   )]
      public int gxTpr_Contagemresultadoartefato_artefatocod
      {
         get {
            return gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_artefatocod ;
         }

         set {
            gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_artefatocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoArtefato_EvdCod" )]
      [  XmlElement( ElementName = "ContagemResultadoArtefato_EvdCod"   )]
      public int gxTpr_Contagemresultadoartefato_evdcod
      {
         get {
            return gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod ;
         }

         set {
            gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod_N = 0;
            gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod_SetNull( )
      {
         gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod_N = 1;
         gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoArtefato_AnxCod" )]
      [  XmlElement( ElementName = "ContagemResultadoArtefato_AnxCod"   )]
      public int gxTpr_Contagemresultadoartefato_anxcod
      {
         get {
            return gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod ;
         }

         set {
            gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod_N = 0;
            gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod_SetNull( )
      {
         gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod_N = 1;
         gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtContagemResultadoArtefato_Mode ;
         }

         set {
            gxTv_SdtContagemResultadoArtefato_Mode = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoArtefato_Mode_SetNull( )
      {
         gxTv_SdtContagemResultadoArtefato_Mode = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoArtefato_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtContagemResultadoArtefato_Initialized ;
         }

         set {
            gxTv_SdtContagemResultadoArtefato_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoArtefato_Initialized_SetNull( )
      {
         gxTv_SdtContagemResultadoArtefato_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoArtefato_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoArtefato_Codigo_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoArtefato_Codigo_Z"   )]
      public int gxTpr_Contagemresultadoartefato_codigo_Z
      {
         get {
            return gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_codigo_Z ;
         }

         set {
            gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_codigo_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoArtefato_OSCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoArtefato_OSCod_Z"   )]
      public int gxTpr_Contagemresultadoartefato_oscod_Z
      {
         get {
            return gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_oscod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_oscod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_oscod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_oscod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_oscod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoArtefato_ArtefatoCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoArtefato_ArtefatoCod_Z"   )]
      public int gxTpr_Contagemresultadoartefato_artefatocod_Z
      {
         get {
            return gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_artefatocod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_artefatocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_artefatocod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_artefatocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_artefatocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoArtefato_EvdCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoArtefato_EvdCod_Z"   )]
      public int gxTpr_Contagemresultadoartefato_evdcod_Z
      {
         get {
            return gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoArtefato_AnxCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoArtefato_AnxCod_Z"   )]
      public int gxTpr_Contagemresultadoartefato_anxcod_Z
      {
         get {
            return gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoArtefato_EvdCod_N" )]
      [  XmlElement( ElementName = "ContagemResultadoArtefato_EvdCod_N"   )]
      public short gxTpr_Contagemresultadoartefato_evdcod_N
      {
         get {
            return gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod_N ;
         }

         set {
            gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod_N_SetNull( )
      {
         gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoArtefato_AnxCod_N" )]
      [  XmlElement( ElementName = "ContagemResultadoArtefato_AnxCod_N"   )]
      public short gxTpr_Contagemresultadoartefato_anxcod_N
      {
         get {
            return gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod_N ;
         }

         set {
            gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod_N_SetNull( )
      {
         gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtContagemResultadoArtefato_Mode = "";
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "contagemresultadoartefato", "GeneXus.Programs.contagemresultadoartefato_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtContagemResultadoArtefato_Initialized ;
      private short gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod_N ;
      private short gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_codigo ;
      private int gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_oscod ;
      private int gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_artefatocod ;
      private int gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod ;
      private int gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod ;
      private int gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_codigo_Z ;
      private int gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_oscod_Z ;
      private int gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_artefatocod_Z ;
      private int gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod_Z ;
      private int gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_anxcod_Z ;
      private String gxTv_SdtContagemResultadoArtefato_Mode ;
      private String sTagName ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ContagemResultadoArtefato", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtContagemResultadoArtefato_RESTInterface : GxGenericCollectionItem<SdtContagemResultadoArtefato>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContagemResultadoArtefato_RESTInterface( ) : base()
      {
      }

      public SdtContagemResultadoArtefato_RESTInterface( SdtContagemResultadoArtefato psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContagemResultadoArtefato_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadoartefato_codigo
      {
         get {
            return sdt.gxTpr_Contagemresultadoartefato_codigo ;
         }

         set {
            sdt.gxTpr_Contagemresultadoartefato_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoArtefato_OSCod" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadoartefato_oscod
      {
         get {
            return sdt.gxTpr_Contagemresultadoartefato_oscod ;
         }

         set {
            sdt.gxTpr_Contagemresultadoartefato_oscod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoArtefato_ArtefatoCod" , Order = 2 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadoartefato_artefatocod
      {
         get {
            return sdt.gxTpr_Contagemresultadoartefato_artefatocod ;
         }

         set {
            sdt.gxTpr_Contagemresultadoartefato_artefatocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoArtefato_EvdCod" , Order = 3 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadoartefato_evdcod
      {
         get {
            return sdt.gxTpr_Contagemresultadoartefato_evdcod ;
         }

         set {
            sdt.gxTpr_Contagemresultadoartefato_evdcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoArtefato_AnxCod" , Order = 4 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadoartefato_anxcod
      {
         get {
            return sdt.gxTpr_Contagemresultadoartefato_anxcod ;
         }

         set {
            sdt.gxTpr_Contagemresultadoartefato_anxcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      public SdtContagemResultadoArtefato sdt
      {
         get {
            return (SdtContagemResultadoArtefato)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtContagemResultadoArtefato() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 14 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
