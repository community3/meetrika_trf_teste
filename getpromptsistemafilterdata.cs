/*
               File: GetPromptSistemaFilterData
        Description: Get Prompt Sistema Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:53:13.65
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptsistemafilterdata : GXProcedure
   {
      public getpromptsistemafilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptsistemafilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
         return AV29OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptsistemafilterdata objgetpromptsistemafilterdata;
         objgetpromptsistemafilterdata = new getpromptsistemafilterdata();
         objgetpromptsistemafilterdata.AV20DDOName = aP0_DDOName;
         objgetpromptsistemafilterdata.AV18SearchTxt = aP1_SearchTxt;
         objgetpromptsistemafilterdata.AV19SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptsistemafilterdata.AV24OptionsJson = "" ;
         objgetpromptsistemafilterdata.AV27OptionsDescJson = "" ;
         objgetpromptsistemafilterdata.AV29OptionIndexesJson = "" ;
         objgetpromptsistemafilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptsistemafilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptsistemafilterdata);
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptsistemafilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV23Options = (IGxCollection)(new GxSimpleCollection());
         AV26OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV28OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_SISTEMA_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADSISTEMA_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_SISTEMA_SIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADSISTEMA_SIGLAOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_AMBIENTETECNOLOGICO_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADAMBIENTETECNOLOGICO_DESCRICAOOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_METODOLOGIA_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADMETODOLOGIA_DESCRICAOOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV24OptionsJson = AV23Options.ToJSonString(false);
         AV27OptionsDescJson = AV26OptionsDesc.ToJSonString(false);
         AV29OptionIndexesJson = AV28OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV31Session.Get("PromptSistemaGridState"), "") == 0 )
         {
            AV33GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptSistemaGridState"), "");
         }
         else
         {
            AV33GridState.FromXml(AV31Session.Get("PromptSistemaGridState"), "");
         }
         AV42GXV1 = 1;
         while ( AV42GXV1 <= AV33GridState.gxTpr_Filtervalues.Count )
         {
            AV34GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV33GridState.gxTpr_Filtervalues.Item(AV42GXV1));
            if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "SISTEMA_AREATRABALHOCOD") == 0 )
            {
               AV36Sistema_AreaTrabalhoCod = (int)(NumberUtil.Val( AV34GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "SISTEMA_ATIVO") == 0 )
            {
               AV37Sistema_Ativo = BooleanUtil.Val( AV34GridStateFilterValue.gxTpr_Value);
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFSISTEMA_NOME") == 0 )
            {
               AV10TFSistema_Nome = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFSISTEMA_NOME_SEL") == 0 )
            {
               AV11TFSistema_Nome_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFSISTEMA_SIGLA") == 0 )
            {
               AV12TFSistema_Sigla = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFSISTEMA_SIGLA_SEL") == 0 )
            {
               AV13TFSistema_Sigla_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_DESCRICAO") == 0 )
            {
               AV14TFAmbienteTecnologico_Descricao = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_DESCRICAO_SEL") == 0 )
            {
               AV15TFAmbienteTecnologico_Descricao_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFMETODOLOGIA_DESCRICAO") == 0 )
            {
               AV16TFMetodologia_Descricao = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFMETODOLOGIA_DESCRICAO_SEL") == 0 )
            {
               AV17TFMetodologia_Descricao_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            AV42GXV1 = (int)(AV42GXV1+1);
         }
         if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(1));
            AV38DynamicFiltersSelector1 = AV35GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "SISTEMA_NOME") == 0 )
            {
               AV39Sistema_Nome1 = AV35GridStateDynamicFilter.gxTpr_Value;
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADSISTEMA_NOMEOPTIONS' Routine */
         AV10TFSistema_Nome = AV18SearchTxt;
         AV11TFSistema_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV36Sistema_AreaTrabalhoCod ,
                                              AV38DynamicFiltersSelector1 ,
                                              AV39Sistema_Nome1 ,
                                              AV11TFSistema_Nome_Sel ,
                                              AV10TFSistema_Nome ,
                                              AV13TFSistema_Sigla_Sel ,
                                              AV12TFSistema_Sigla ,
                                              AV15TFAmbienteTecnologico_Descricao_Sel ,
                                              AV14TFAmbienteTecnologico_Descricao ,
                                              AV17TFMetodologia_Descricao_Sel ,
                                              AV16TFMetodologia_Descricao ,
                                              A135Sistema_AreaTrabalhoCod ,
                                              A416Sistema_Nome ,
                                              A129Sistema_Sigla ,
                                              A352AmbienteTecnologico_Descricao ,
                                              A138Metodologia_Descricao ,
                                              A130Sistema_Ativo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV39Sistema_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV39Sistema_Nome1), "%", "");
         lV10TFSistema_Nome = StringUtil.Concat( StringUtil.RTrim( AV10TFSistema_Nome), "%", "");
         lV12TFSistema_Sigla = StringUtil.PadR( StringUtil.RTrim( AV12TFSistema_Sigla), 25, "%");
         lV14TFAmbienteTecnologico_Descricao = StringUtil.Concat( StringUtil.RTrim( AV14TFAmbienteTecnologico_Descricao), "%", "");
         lV16TFMetodologia_Descricao = StringUtil.Concat( StringUtil.RTrim( AV16TFMetodologia_Descricao), "%", "");
         /* Using cursor P00FS2 */
         pr_default.execute(0, new Object[] {AV36Sistema_AreaTrabalhoCod, lV39Sistema_Nome1, lV10TFSistema_Nome, AV11TFSistema_Nome_Sel, lV12TFSistema_Sigla, AV13TFSistema_Sigla_Sel, lV14TFAmbienteTecnologico_Descricao, AV15TFAmbienteTecnologico_Descricao_Sel, lV16TFMetodologia_Descricao, AV17TFMetodologia_Descricao_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKFS2 = false;
            A137Metodologia_Codigo = P00FS2_A137Metodologia_Codigo[0];
            n137Metodologia_Codigo = P00FS2_n137Metodologia_Codigo[0];
            A351AmbienteTecnologico_Codigo = P00FS2_A351AmbienteTecnologico_Codigo[0];
            n351AmbienteTecnologico_Codigo = P00FS2_n351AmbienteTecnologico_Codigo[0];
            A130Sistema_Ativo = P00FS2_A130Sistema_Ativo[0];
            A416Sistema_Nome = P00FS2_A416Sistema_Nome[0];
            A138Metodologia_Descricao = P00FS2_A138Metodologia_Descricao[0];
            A352AmbienteTecnologico_Descricao = P00FS2_A352AmbienteTecnologico_Descricao[0];
            A129Sistema_Sigla = P00FS2_A129Sistema_Sigla[0];
            A135Sistema_AreaTrabalhoCod = P00FS2_A135Sistema_AreaTrabalhoCod[0];
            A127Sistema_Codigo = P00FS2_A127Sistema_Codigo[0];
            A138Metodologia_Descricao = P00FS2_A138Metodologia_Descricao[0];
            A352AmbienteTecnologico_Descricao = P00FS2_A352AmbienteTecnologico_Descricao[0];
            AV30count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00FS2_A416Sistema_Nome[0], A416Sistema_Nome) == 0 ) )
            {
               BRKFS2 = false;
               A127Sistema_Codigo = P00FS2_A127Sistema_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKFS2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A416Sistema_Nome)) )
            {
               AV22Option = A416Sistema_Nome;
               AV25OptionDesc = StringUtil.Trim( StringUtil.RTrim( context.localUtil.Format( A416Sistema_Nome, "@!")));
               AV23Options.Add(AV22Option, 0);
               AV26OptionsDesc.Add(AV25OptionDesc, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKFS2 )
            {
               BRKFS2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADSISTEMA_SIGLAOPTIONS' Routine */
         AV12TFSistema_Sigla = AV18SearchTxt;
         AV13TFSistema_Sigla_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV36Sistema_AreaTrabalhoCod ,
                                              AV38DynamicFiltersSelector1 ,
                                              AV39Sistema_Nome1 ,
                                              AV11TFSistema_Nome_Sel ,
                                              AV10TFSistema_Nome ,
                                              AV13TFSistema_Sigla_Sel ,
                                              AV12TFSistema_Sigla ,
                                              AV15TFAmbienteTecnologico_Descricao_Sel ,
                                              AV14TFAmbienteTecnologico_Descricao ,
                                              AV17TFMetodologia_Descricao_Sel ,
                                              AV16TFMetodologia_Descricao ,
                                              A135Sistema_AreaTrabalhoCod ,
                                              A416Sistema_Nome ,
                                              A129Sistema_Sigla ,
                                              A352AmbienteTecnologico_Descricao ,
                                              A138Metodologia_Descricao ,
                                              A130Sistema_Ativo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV39Sistema_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV39Sistema_Nome1), "%", "");
         lV10TFSistema_Nome = StringUtil.Concat( StringUtil.RTrim( AV10TFSistema_Nome), "%", "");
         lV12TFSistema_Sigla = StringUtil.PadR( StringUtil.RTrim( AV12TFSistema_Sigla), 25, "%");
         lV14TFAmbienteTecnologico_Descricao = StringUtil.Concat( StringUtil.RTrim( AV14TFAmbienteTecnologico_Descricao), "%", "");
         lV16TFMetodologia_Descricao = StringUtil.Concat( StringUtil.RTrim( AV16TFMetodologia_Descricao), "%", "");
         /* Using cursor P00FS3 */
         pr_default.execute(1, new Object[] {AV36Sistema_AreaTrabalhoCod, lV39Sistema_Nome1, lV10TFSistema_Nome, AV11TFSistema_Nome_Sel, lV12TFSistema_Sigla, AV13TFSistema_Sigla_Sel, lV14TFAmbienteTecnologico_Descricao, AV15TFAmbienteTecnologico_Descricao_Sel, lV16TFMetodologia_Descricao, AV17TFMetodologia_Descricao_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKFS4 = false;
            A137Metodologia_Codigo = P00FS3_A137Metodologia_Codigo[0];
            n137Metodologia_Codigo = P00FS3_n137Metodologia_Codigo[0];
            A351AmbienteTecnologico_Codigo = P00FS3_A351AmbienteTecnologico_Codigo[0];
            n351AmbienteTecnologico_Codigo = P00FS3_n351AmbienteTecnologico_Codigo[0];
            A129Sistema_Sigla = P00FS3_A129Sistema_Sigla[0];
            A138Metodologia_Descricao = P00FS3_A138Metodologia_Descricao[0];
            A352AmbienteTecnologico_Descricao = P00FS3_A352AmbienteTecnologico_Descricao[0];
            A416Sistema_Nome = P00FS3_A416Sistema_Nome[0];
            A130Sistema_Ativo = P00FS3_A130Sistema_Ativo[0];
            A135Sistema_AreaTrabalhoCod = P00FS3_A135Sistema_AreaTrabalhoCod[0];
            A127Sistema_Codigo = P00FS3_A127Sistema_Codigo[0];
            A138Metodologia_Descricao = P00FS3_A138Metodologia_Descricao[0];
            A352AmbienteTecnologico_Descricao = P00FS3_A352AmbienteTecnologico_Descricao[0];
            AV30count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00FS3_A129Sistema_Sigla[0], A129Sistema_Sigla) == 0 ) )
            {
               BRKFS4 = false;
               A127Sistema_Codigo = P00FS3_A127Sistema_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKFS4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A129Sistema_Sigla)) )
            {
               AV22Option = A129Sistema_Sigla;
               AV25OptionDesc = StringUtil.Trim( StringUtil.RTrim( context.localUtil.Format( A129Sistema_Sigla, "@!")));
               AV23Options.Add(AV22Option, 0);
               AV26OptionsDesc.Add(AV25OptionDesc, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKFS4 )
            {
               BRKFS4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADAMBIENTETECNOLOGICO_DESCRICAOOPTIONS' Routine */
         AV14TFAmbienteTecnologico_Descricao = AV18SearchTxt;
         AV15TFAmbienteTecnologico_Descricao_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV36Sistema_AreaTrabalhoCod ,
                                              AV38DynamicFiltersSelector1 ,
                                              AV39Sistema_Nome1 ,
                                              AV11TFSistema_Nome_Sel ,
                                              AV10TFSistema_Nome ,
                                              AV13TFSistema_Sigla_Sel ,
                                              AV12TFSistema_Sigla ,
                                              AV15TFAmbienteTecnologico_Descricao_Sel ,
                                              AV14TFAmbienteTecnologico_Descricao ,
                                              AV17TFMetodologia_Descricao_Sel ,
                                              AV16TFMetodologia_Descricao ,
                                              A135Sistema_AreaTrabalhoCod ,
                                              A416Sistema_Nome ,
                                              A129Sistema_Sigla ,
                                              A352AmbienteTecnologico_Descricao ,
                                              A138Metodologia_Descricao ,
                                              A130Sistema_Ativo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV39Sistema_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV39Sistema_Nome1), "%", "");
         lV10TFSistema_Nome = StringUtil.Concat( StringUtil.RTrim( AV10TFSistema_Nome), "%", "");
         lV12TFSistema_Sigla = StringUtil.PadR( StringUtil.RTrim( AV12TFSistema_Sigla), 25, "%");
         lV14TFAmbienteTecnologico_Descricao = StringUtil.Concat( StringUtil.RTrim( AV14TFAmbienteTecnologico_Descricao), "%", "");
         lV16TFMetodologia_Descricao = StringUtil.Concat( StringUtil.RTrim( AV16TFMetodologia_Descricao), "%", "");
         /* Using cursor P00FS4 */
         pr_default.execute(2, new Object[] {AV36Sistema_AreaTrabalhoCod, lV39Sistema_Nome1, lV10TFSistema_Nome, AV11TFSistema_Nome_Sel, lV12TFSistema_Sigla, AV13TFSistema_Sigla_Sel, lV14TFAmbienteTecnologico_Descricao, AV15TFAmbienteTecnologico_Descricao_Sel, lV16TFMetodologia_Descricao, AV17TFMetodologia_Descricao_Sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKFS6 = false;
            A137Metodologia_Codigo = P00FS4_A137Metodologia_Codigo[0];
            n137Metodologia_Codigo = P00FS4_n137Metodologia_Codigo[0];
            A351AmbienteTecnologico_Codigo = P00FS4_A351AmbienteTecnologico_Codigo[0];
            n351AmbienteTecnologico_Codigo = P00FS4_n351AmbienteTecnologico_Codigo[0];
            A138Metodologia_Descricao = P00FS4_A138Metodologia_Descricao[0];
            A352AmbienteTecnologico_Descricao = P00FS4_A352AmbienteTecnologico_Descricao[0];
            A129Sistema_Sigla = P00FS4_A129Sistema_Sigla[0];
            A416Sistema_Nome = P00FS4_A416Sistema_Nome[0];
            A130Sistema_Ativo = P00FS4_A130Sistema_Ativo[0];
            A135Sistema_AreaTrabalhoCod = P00FS4_A135Sistema_AreaTrabalhoCod[0];
            A127Sistema_Codigo = P00FS4_A127Sistema_Codigo[0];
            A138Metodologia_Descricao = P00FS4_A138Metodologia_Descricao[0];
            A352AmbienteTecnologico_Descricao = P00FS4_A352AmbienteTecnologico_Descricao[0];
            AV30count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( P00FS4_A351AmbienteTecnologico_Codigo[0] == A351AmbienteTecnologico_Codigo ) )
            {
               BRKFS6 = false;
               A127Sistema_Codigo = P00FS4_A127Sistema_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKFS6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A352AmbienteTecnologico_Descricao)) )
            {
               AV22Option = A352AmbienteTecnologico_Descricao;
               AV21InsertIndex = 1;
               while ( ( AV21InsertIndex <= AV23Options.Count ) && ( StringUtil.StrCmp(((String)AV23Options.Item(AV21InsertIndex)), AV22Option) < 0 ) )
               {
                  AV21InsertIndex = (int)(AV21InsertIndex+1);
               }
               AV23Options.Add(AV22Option, AV21InsertIndex);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), AV21InsertIndex);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKFS6 )
            {
               BRKFS6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADMETODOLOGIA_DESCRICAOOPTIONS' Routine */
         AV16TFMetodologia_Descricao = AV18SearchTxt;
         AV17TFMetodologia_Descricao_Sel = "";
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV36Sistema_AreaTrabalhoCod ,
                                              AV38DynamicFiltersSelector1 ,
                                              AV39Sistema_Nome1 ,
                                              AV11TFSistema_Nome_Sel ,
                                              AV10TFSistema_Nome ,
                                              AV13TFSistema_Sigla_Sel ,
                                              AV12TFSistema_Sigla ,
                                              AV15TFAmbienteTecnologico_Descricao_Sel ,
                                              AV14TFAmbienteTecnologico_Descricao ,
                                              AV17TFMetodologia_Descricao_Sel ,
                                              AV16TFMetodologia_Descricao ,
                                              A135Sistema_AreaTrabalhoCod ,
                                              A416Sistema_Nome ,
                                              A129Sistema_Sigla ,
                                              A352AmbienteTecnologico_Descricao ,
                                              A138Metodologia_Descricao ,
                                              A130Sistema_Ativo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV39Sistema_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV39Sistema_Nome1), "%", "");
         lV10TFSistema_Nome = StringUtil.Concat( StringUtil.RTrim( AV10TFSistema_Nome), "%", "");
         lV12TFSistema_Sigla = StringUtil.PadR( StringUtil.RTrim( AV12TFSistema_Sigla), 25, "%");
         lV14TFAmbienteTecnologico_Descricao = StringUtil.Concat( StringUtil.RTrim( AV14TFAmbienteTecnologico_Descricao), "%", "");
         lV16TFMetodologia_Descricao = StringUtil.Concat( StringUtil.RTrim( AV16TFMetodologia_Descricao), "%", "");
         /* Using cursor P00FS5 */
         pr_default.execute(3, new Object[] {AV36Sistema_AreaTrabalhoCod, lV39Sistema_Nome1, lV10TFSistema_Nome, AV11TFSistema_Nome_Sel, lV12TFSistema_Sigla, AV13TFSistema_Sigla_Sel, lV14TFAmbienteTecnologico_Descricao, AV15TFAmbienteTecnologico_Descricao_Sel, lV16TFMetodologia_Descricao, AV17TFMetodologia_Descricao_Sel});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKFS8 = false;
            A351AmbienteTecnologico_Codigo = P00FS5_A351AmbienteTecnologico_Codigo[0];
            n351AmbienteTecnologico_Codigo = P00FS5_n351AmbienteTecnologico_Codigo[0];
            A137Metodologia_Codigo = P00FS5_A137Metodologia_Codigo[0];
            n137Metodologia_Codigo = P00FS5_n137Metodologia_Codigo[0];
            A138Metodologia_Descricao = P00FS5_A138Metodologia_Descricao[0];
            A352AmbienteTecnologico_Descricao = P00FS5_A352AmbienteTecnologico_Descricao[0];
            A129Sistema_Sigla = P00FS5_A129Sistema_Sigla[0];
            A416Sistema_Nome = P00FS5_A416Sistema_Nome[0];
            A130Sistema_Ativo = P00FS5_A130Sistema_Ativo[0];
            A135Sistema_AreaTrabalhoCod = P00FS5_A135Sistema_AreaTrabalhoCod[0];
            A127Sistema_Codigo = P00FS5_A127Sistema_Codigo[0];
            A352AmbienteTecnologico_Descricao = P00FS5_A352AmbienteTecnologico_Descricao[0];
            A138Metodologia_Descricao = P00FS5_A138Metodologia_Descricao[0];
            AV30count = 0;
            while ( (pr_default.getStatus(3) != 101) && ( P00FS5_A137Metodologia_Codigo[0] == A137Metodologia_Codigo ) )
            {
               BRKFS8 = false;
               A127Sistema_Codigo = P00FS5_A127Sistema_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKFS8 = true;
               pr_default.readNext(3);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A138Metodologia_Descricao)) )
            {
               AV22Option = A138Metodologia_Descricao;
               AV21InsertIndex = 1;
               while ( ( AV21InsertIndex <= AV23Options.Count ) && ( StringUtil.StrCmp(((String)AV23Options.Item(AV21InsertIndex)), AV22Option) < 0 ) )
               {
                  AV21InsertIndex = (int)(AV21InsertIndex+1);
               }
               AV23Options.Add(AV22Option, AV21InsertIndex);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), AV21InsertIndex);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKFS8 )
            {
               BRKFS8 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV23Options = new GxSimpleCollection();
         AV26OptionsDesc = new GxSimpleCollection();
         AV28OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV31Session = context.GetSession();
         AV33GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV34GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV37Sistema_Ativo = true;
         AV10TFSistema_Nome = "";
         AV11TFSistema_Nome_Sel = "";
         AV12TFSistema_Sigla = "";
         AV13TFSistema_Sigla_Sel = "";
         AV14TFAmbienteTecnologico_Descricao = "";
         AV15TFAmbienteTecnologico_Descricao_Sel = "";
         AV16TFMetodologia_Descricao = "";
         AV17TFMetodologia_Descricao_Sel = "";
         AV35GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV38DynamicFiltersSelector1 = "";
         AV39Sistema_Nome1 = "";
         scmdbuf = "";
         lV39Sistema_Nome1 = "";
         lV10TFSistema_Nome = "";
         lV12TFSistema_Sigla = "";
         lV14TFAmbienteTecnologico_Descricao = "";
         lV16TFMetodologia_Descricao = "";
         A416Sistema_Nome = "";
         A129Sistema_Sigla = "";
         A352AmbienteTecnologico_Descricao = "";
         A138Metodologia_Descricao = "";
         P00FS2_A137Metodologia_Codigo = new int[1] ;
         P00FS2_n137Metodologia_Codigo = new bool[] {false} ;
         P00FS2_A351AmbienteTecnologico_Codigo = new int[1] ;
         P00FS2_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         P00FS2_A130Sistema_Ativo = new bool[] {false} ;
         P00FS2_A416Sistema_Nome = new String[] {""} ;
         P00FS2_A138Metodologia_Descricao = new String[] {""} ;
         P00FS2_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         P00FS2_A129Sistema_Sigla = new String[] {""} ;
         P00FS2_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00FS2_A127Sistema_Codigo = new int[1] ;
         AV22Option = "";
         AV25OptionDesc = "";
         P00FS3_A137Metodologia_Codigo = new int[1] ;
         P00FS3_n137Metodologia_Codigo = new bool[] {false} ;
         P00FS3_A351AmbienteTecnologico_Codigo = new int[1] ;
         P00FS3_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         P00FS3_A129Sistema_Sigla = new String[] {""} ;
         P00FS3_A138Metodologia_Descricao = new String[] {""} ;
         P00FS3_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         P00FS3_A416Sistema_Nome = new String[] {""} ;
         P00FS3_A130Sistema_Ativo = new bool[] {false} ;
         P00FS3_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00FS3_A127Sistema_Codigo = new int[1] ;
         P00FS4_A137Metodologia_Codigo = new int[1] ;
         P00FS4_n137Metodologia_Codigo = new bool[] {false} ;
         P00FS4_A351AmbienteTecnologico_Codigo = new int[1] ;
         P00FS4_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         P00FS4_A138Metodologia_Descricao = new String[] {""} ;
         P00FS4_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         P00FS4_A129Sistema_Sigla = new String[] {""} ;
         P00FS4_A416Sistema_Nome = new String[] {""} ;
         P00FS4_A130Sistema_Ativo = new bool[] {false} ;
         P00FS4_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00FS4_A127Sistema_Codigo = new int[1] ;
         P00FS5_A351AmbienteTecnologico_Codigo = new int[1] ;
         P00FS5_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         P00FS5_A137Metodologia_Codigo = new int[1] ;
         P00FS5_n137Metodologia_Codigo = new bool[] {false} ;
         P00FS5_A138Metodologia_Descricao = new String[] {""} ;
         P00FS5_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         P00FS5_A129Sistema_Sigla = new String[] {""} ;
         P00FS5_A416Sistema_Nome = new String[] {""} ;
         P00FS5_A130Sistema_Ativo = new bool[] {false} ;
         P00FS5_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00FS5_A127Sistema_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptsistemafilterdata__default(),
            new Object[][] {
                new Object[] {
               P00FS2_A137Metodologia_Codigo, P00FS2_n137Metodologia_Codigo, P00FS2_A351AmbienteTecnologico_Codigo, P00FS2_n351AmbienteTecnologico_Codigo, P00FS2_A130Sistema_Ativo, P00FS2_A416Sistema_Nome, P00FS2_A138Metodologia_Descricao, P00FS2_A352AmbienteTecnologico_Descricao, P00FS2_A129Sistema_Sigla, P00FS2_A135Sistema_AreaTrabalhoCod,
               P00FS2_A127Sistema_Codigo
               }
               , new Object[] {
               P00FS3_A137Metodologia_Codigo, P00FS3_n137Metodologia_Codigo, P00FS3_A351AmbienteTecnologico_Codigo, P00FS3_n351AmbienteTecnologico_Codigo, P00FS3_A129Sistema_Sigla, P00FS3_A138Metodologia_Descricao, P00FS3_A352AmbienteTecnologico_Descricao, P00FS3_A416Sistema_Nome, P00FS3_A130Sistema_Ativo, P00FS3_A135Sistema_AreaTrabalhoCod,
               P00FS3_A127Sistema_Codigo
               }
               , new Object[] {
               P00FS4_A137Metodologia_Codigo, P00FS4_n137Metodologia_Codigo, P00FS4_A351AmbienteTecnologico_Codigo, P00FS4_n351AmbienteTecnologico_Codigo, P00FS4_A138Metodologia_Descricao, P00FS4_A352AmbienteTecnologico_Descricao, P00FS4_A129Sistema_Sigla, P00FS4_A416Sistema_Nome, P00FS4_A130Sistema_Ativo, P00FS4_A135Sistema_AreaTrabalhoCod,
               P00FS4_A127Sistema_Codigo
               }
               , new Object[] {
               P00FS5_A351AmbienteTecnologico_Codigo, P00FS5_n351AmbienteTecnologico_Codigo, P00FS5_A137Metodologia_Codigo, P00FS5_n137Metodologia_Codigo, P00FS5_A138Metodologia_Descricao, P00FS5_A352AmbienteTecnologico_Descricao, P00FS5_A129Sistema_Sigla, P00FS5_A416Sistema_Nome, P00FS5_A130Sistema_Ativo, P00FS5_A135Sistema_AreaTrabalhoCod,
               P00FS5_A127Sistema_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV42GXV1 ;
      private int AV36Sistema_AreaTrabalhoCod ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A137Metodologia_Codigo ;
      private int A351AmbienteTecnologico_Codigo ;
      private int A127Sistema_Codigo ;
      private int AV21InsertIndex ;
      private long AV30count ;
      private String AV12TFSistema_Sigla ;
      private String AV13TFSistema_Sigla_Sel ;
      private String scmdbuf ;
      private String lV12TFSistema_Sigla ;
      private String A129Sistema_Sigla ;
      private bool returnInSub ;
      private bool AV37Sistema_Ativo ;
      private bool A130Sistema_Ativo ;
      private bool BRKFS2 ;
      private bool n137Metodologia_Codigo ;
      private bool n351AmbienteTecnologico_Codigo ;
      private bool BRKFS4 ;
      private bool BRKFS6 ;
      private bool BRKFS8 ;
      private String AV29OptionIndexesJson ;
      private String AV24OptionsJson ;
      private String AV27OptionsDescJson ;
      private String AV20DDOName ;
      private String AV18SearchTxt ;
      private String AV19SearchTxtTo ;
      private String AV10TFSistema_Nome ;
      private String AV11TFSistema_Nome_Sel ;
      private String AV14TFAmbienteTecnologico_Descricao ;
      private String AV15TFAmbienteTecnologico_Descricao_Sel ;
      private String AV16TFMetodologia_Descricao ;
      private String AV17TFMetodologia_Descricao_Sel ;
      private String AV38DynamicFiltersSelector1 ;
      private String AV39Sistema_Nome1 ;
      private String lV39Sistema_Nome1 ;
      private String lV10TFSistema_Nome ;
      private String lV14TFAmbienteTecnologico_Descricao ;
      private String lV16TFMetodologia_Descricao ;
      private String A416Sistema_Nome ;
      private String A352AmbienteTecnologico_Descricao ;
      private String A138Metodologia_Descricao ;
      private String AV22Option ;
      private String AV25OptionDesc ;
      private IGxSession AV31Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00FS2_A137Metodologia_Codigo ;
      private bool[] P00FS2_n137Metodologia_Codigo ;
      private int[] P00FS2_A351AmbienteTecnologico_Codigo ;
      private bool[] P00FS2_n351AmbienteTecnologico_Codigo ;
      private bool[] P00FS2_A130Sistema_Ativo ;
      private String[] P00FS2_A416Sistema_Nome ;
      private String[] P00FS2_A138Metodologia_Descricao ;
      private String[] P00FS2_A352AmbienteTecnologico_Descricao ;
      private String[] P00FS2_A129Sistema_Sigla ;
      private int[] P00FS2_A135Sistema_AreaTrabalhoCod ;
      private int[] P00FS2_A127Sistema_Codigo ;
      private int[] P00FS3_A137Metodologia_Codigo ;
      private bool[] P00FS3_n137Metodologia_Codigo ;
      private int[] P00FS3_A351AmbienteTecnologico_Codigo ;
      private bool[] P00FS3_n351AmbienteTecnologico_Codigo ;
      private String[] P00FS3_A129Sistema_Sigla ;
      private String[] P00FS3_A138Metodologia_Descricao ;
      private String[] P00FS3_A352AmbienteTecnologico_Descricao ;
      private String[] P00FS3_A416Sistema_Nome ;
      private bool[] P00FS3_A130Sistema_Ativo ;
      private int[] P00FS3_A135Sistema_AreaTrabalhoCod ;
      private int[] P00FS3_A127Sistema_Codigo ;
      private int[] P00FS4_A137Metodologia_Codigo ;
      private bool[] P00FS4_n137Metodologia_Codigo ;
      private int[] P00FS4_A351AmbienteTecnologico_Codigo ;
      private bool[] P00FS4_n351AmbienteTecnologico_Codigo ;
      private String[] P00FS4_A138Metodologia_Descricao ;
      private String[] P00FS4_A352AmbienteTecnologico_Descricao ;
      private String[] P00FS4_A129Sistema_Sigla ;
      private String[] P00FS4_A416Sistema_Nome ;
      private bool[] P00FS4_A130Sistema_Ativo ;
      private int[] P00FS4_A135Sistema_AreaTrabalhoCod ;
      private int[] P00FS4_A127Sistema_Codigo ;
      private int[] P00FS5_A351AmbienteTecnologico_Codigo ;
      private bool[] P00FS5_n351AmbienteTecnologico_Codigo ;
      private int[] P00FS5_A137Metodologia_Codigo ;
      private bool[] P00FS5_n137Metodologia_Codigo ;
      private String[] P00FS5_A138Metodologia_Descricao ;
      private String[] P00FS5_A352AmbienteTecnologico_Descricao ;
      private String[] P00FS5_A129Sistema_Sigla ;
      private String[] P00FS5_A416Sistema_Nome ;
      private bool[] P00FS5_A130Sistema_Ativo ;
      private int[] P00FS5_A135Sistema_AreaTrabalhoCod ;
      private int[] P00FS5_A127Sistema_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV33GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV34GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV35GridStateDynamicFilter ;
   }

   public class getpromptsistemafilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00FS2( IGxContext context ,
                                             int AV36Sistema_AreaTrabalhoCod ,
                                             String AV38DynamicFiltersSelector1 ,
                                             String AV39Sistema_Nome1 ,
                                             String AV11TFSistema_Nome_Sel ,
                                             String AV10TFSistema_Nome ,
                                             String AV13TFSistema_Sigla_Sel ,
                                             String AV12TFSistema_Sigla ,
                                             String AV15TFAmbienteTecnologico_Descricao_Sel ,
                                             String AV14TFAmbienteTecnologico_Descricao ,
                                             String AV17TFMetodologia_Descricao_Sel ,
                                             String AV16TFMetodologia_Descricao ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             String A416Sistema_Nome ,
                                             String A129Sistema_Sigla ,
                                             String A352AmbienteTecnologico_Descricao ,
                                             String A138Metodologia_Descricao ,
                                             bool A130Sistema_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [10] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Metodologia_Codigo], T1.[AmbienteTecnologico_Codigo], T1.[Sistema_Ativo], T1.[Sistema_Nome], T2.[Metodologia_Descricao], T3.[AmbienteTecnologico_Descricao], T1.[Sistema_Sigla], T1.[Sistema_AreaTrabalhoCod], T1.[Sistema_Codigo] FROM (([Sistema] T1 WITH (NOLOCK) LEFT JOIN [Metodologia] T2 WITH (NOLOCK) ON T2.[Metodologia_Codigo] = T1.[Metodologia_Codigo]) LEFT JOIN [AmbienteTecnologico] T3 WITH (NOLOCK) ON T3.[AmbienteTecnologico_Codigo] = T1.[AmbienteTecnologico_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Sistema_Ativo] = 1)";
         if ( ! (0==AV36Sistema_AreaTrabalhoCod) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_AreaTrabalhoCod] = @AV36Sistema_AreaTrabalhoCod)";
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "SISTEMA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39Sistema_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like '%' + @lV39Sistema_Nome1 + '%')";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFSistema_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFSistema_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like @lV10TFSistema_Nome)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFSistema_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] = @AV11TFSistema_Nome_Sel)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFSistema_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFSistema_Sigla)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like @lV12TFSistema_Sigla)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFSistema_Sigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] = @AV13TFSistema_Sigla_Sel)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFAmbienteTecnologico_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFAmbienteTecnologico_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AmbienteTecnologico_Descricao] like @lV14TFAmbienteTecnologico_Descricao)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFAmbienteTecnologico_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[AmbienteTecnologico_Descricao] = @AV15TFAmbienteTecnologico_Descricao_Sel)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFMetodologia_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFMetodologia_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV16TFMetodologia_Descricao)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFMetodologia_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] = @AV17TFMetodologia_Descricao_Sel)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Sistema_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00FS3( IGxContext context ,
                                             int AV36Sistema_AreaTrabalhoCod ,
                                             String AV38DynamicFiltersSelector1 ,
                                             String AV39Sistema_Nome1 ,
                                             String AV11TFSistema_Nome_Sel ,
                                             String AV10TFSistema_Nome ,
                                             String AV13TFSistema_Sigla_Sel ,
                                             String AV12TFSistema_Sigla ,
                                             String AV15TFAmbienteTecnologico_Descricao_Sel ,
                                             String AV14TFAmbienteTecnologico_Descricao ,
                                             String AV17TFMetodologia_Descricao_Sel ,
                                             String AV16TFMetodologia_Descricao ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             String A416Sistema_Nome ,
                                             String A129Sistema_Sigla ,
                                             String A352AmbienteTecnologico_Descricao ,
                                             String A138Metodologia_Descricao ,
                                             bool A130Sistema_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [10] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Metodologia_Codigo], T1.[AmbienteTecnologico_Codigo], T1.[Sistema_Sigla], T2.[Metodologia_Descricao], T3.[AmbienteTecnologico_Descricao], T1.[Sistema_Nome], T1.[Sistema_Ativo], T1.[Sistema_AreaTrabalhoCod], T1.[Sistema_Codigo] FROM (([Sistema] T1 WITH (NOLOCK) LEFT JOIN [Metodologia] T2 WITH (NOLOCK) ON T2.[Metodologia_Codigo] = T1.[Metodologia_Codigo]) LEFT JOIN [AmbienteTecnologico] T3 WITH (NOLOCK) ON T3.[AmbienteTecnologico_Codigo] = T1.[AmbienteTecnologico_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Sistema_Ativo] = 1)";
         if ( ! (0==AV36Sistema_AreaTrabalhoCod) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_AreaTrabalhoCod] = @AV36Sistema_AreaTrabalhoCod)";
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "SISTEMA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39Sistema_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like '%' + @lV39Sistema_Nome1 + '%')";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFSistema_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFSistema_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like @lV10TFSistema_Nome)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFSistema_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] = @AV11TFSistema_Nome_Sel)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFSistema_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFSistema_Sigla)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like @lV12TFSistema_Sigla)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFSistema_Sigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] = @AV13TFSistema_Sigla_Sel)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFAmbienteTecnologico_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFAmbienteTecnologico_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AmbienteTecnologico_Descricao] like @lV14TFAmbienteTecnologico_Descricao)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFAmbienteTecnologico_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[AmbienteTecnologico_Descricao] = @AV15TFAmbienteTecnologico_Descricao_Sel)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFMetodologia_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFMetodologia_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV16TFMetodologia_Descricao)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFMetodologia_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] = @AV17TFMetodologia_Descricao_Sel)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Sistema_Sigla]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00FS4( IGxContext context ,
                                             int AV36Sistema_AreaTrabalhoCod ,
                                             String AV38DynamicFiltersSelector1 ,
                                             String AV39Sistema_Nome1 ,
                                             String AV11TFSistema_Nome_Sel ,
                                             String AV10TFSistema_Nome ,
                                             String AV13TFSistema_Sigla_Sel ,
                                             String AV12TFSistema_Sigla ,
                                             String AV15TFAmbienteTecnologico_Descricao_Sel ,
                                             String AV14TFAmbienteTecnologico_Descricao ,
                                             String AV17TFMetodologia_Descricao_Sel ,
                                             String AV16TFMetodologia_Descricao ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             String A416Sistema_Nome ,
                                             String A129Sistema_Sigla ,
                                             String A352AmbienteTecnologico_Descricao ,
                                             String A138Metodologia_Descricao ,
                                             bool A130Sistema_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [10] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[Metodologia_Codigo], T1.[AmbienteTecnologico_Codigo], T2.[Metodologia_Descricao], T3.[AmbienteTecnologico_Descricao], T1.[Sistema_Sigla], T1.[Sistema_Nome], T1.[Sistema_Ativo], T1.[Sistema_AreaTrabalhoCod], T1.[Sistema_Codigo] FROM (([Sistema] T1 WITH (NOLOCK) LEFT JOIN [Metodologia] T2 WITH (NOLOCK) ON T2.[Metodologia_Codigo] = T1.[Metodologia_Codigo]) LEFT JOIN [AmbienteTecnologico] T3 WITH (NOLOCK) ON T3.[AmbienteTecnologico_Codigo] = T1.[AmbienteTecnologico_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Sistema_Ativo] = 1)";
         if ( ! (0==AV36Sistema_AreaTrabalhoCod) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_AreaTrabalhoCod] = @AV36Sistema_AreaTrabalhoCod)";
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "SISTEMA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39Sistema_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like '%' + @lV39Sistema_Nome1 + '%')";
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFSistema_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFSistema_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like @lV10TFSistema_Nome)";
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFSistema_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] = @AV11TFSistema_Nome_Sel)";
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFSistema_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFSistema_Sigla)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like @lV12TFSistema_Sigla)";
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFSistema_Sigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] = @AV13TFSistema_Sigla_Sel)";
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFAmbienteTecnologico_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFAmbienteTecnologico_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AmbienteTecnologico_Descricao] like @lV14TFAmbienteTecnologico_Descricao)";
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFAmbienteTecnologico_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[AmbienteTecnologico_Descricao] = @AV15TFAmbienteTecnologico_Descricao_Sel)";
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFMetodologia_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFMetodologia_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV16TFMetodologia_Descricao)";
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFMetodologia_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] = @AV17TFMetodologia_Descricao_Sel)";
         }
         else
         {
            GXv_int5[9] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[AmbienteTecnologico_Codigo]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00FS5( IGxContext context ,
                                             int AV36Sistema_AreaTrabalhoCod ,
                                             String AV38DynamicFiltersSelector1 ,
                                             String AV39Sistema_Nome1 ,
                                             String AV11TFSistema_Nome_Sel ,
                                             String AV10TFSistema_Nome ,
                                             String AV13TFSistema_Sigla_Sel ,
                                             String AV12TFSistema_Sigla ,
                                             String AV15TFAmbienteTecnologico_Descricao_Sel ,
                                             String AV14TFAmbienteTecnologico_Descricao ,
                                             String AV17TFMetodologia_Descricao_Sel ,
                                             String AV16TFMetodologia_Descricao ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             String A416Sistema_Nome ,
                                             String A129Sistema_Sigla ,
                                             String A352AmbienteTecnologico_Descricao ,
                                             String A138Metodologia_Descricao ,
                                             bool A130Sistema_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [10] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[AmbienteTecnologico_Codigo], T1.[Metodologia_Codigo], T3.[Metodologia_Descricao], T2.[AmbienteTecnologico_Descricao], T1.[Sistema_Sigla], T1.[Sistema_Nome], T1.[Sistema_Ativo], T1.[Sistema_AreaTrabalhoCod], T1.[Sistema_Codigo] FROM (([Sistema] T1 WITH (NOLOCK) LEFT JOIN [AmbienteTecnologico] T2 WITH (NOLOCK) ON T2.[AmbienteTecnologico_Codigo] = T1.[AmbienteTecnologico_Codigo]) LEFT JOIN [Metodologia] T3 WITH (NOLOCK) ON T3.[Metodologia_Codigo] = T1.[Metodologia_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Sistema_Ativo] = 1)";
         if ( ! (0==AV36Sistema_AreaTrabalhoCod) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_AreaTrabalhoCod] = @AV36Sistema_AreaTrabalhoCod)";
         }
         else
         {
            GXv_int7[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "SISTEMA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39Sistema_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like '%' + @lV39Sistema_Nome1 + '%')";
         }
         else
         {
            GXv_int7[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFSistema_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFSistema_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like @lV10TFSistema_Nome)";
         }
         else
         {
            GXv_int7[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFSistema_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] = @AV11TFSistema_Nome_Sel)";
         }
         else
         {
            GXv_int7[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFSistema_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFSistema_Sigla)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like @lV12TFSistema_Sigla)";
         }
         else
         {
            GXv_int7[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFSistema_Sigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] = @AV13TFSistema_Sigla_Sel)";
         }
         else
         {
            GXv_int7[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFAmbienteTecnologico_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFAmbienteTecnologico_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[AmbienteTecnologico_Descricao] like @lV14TFAmbienteTecnologico_Descricao)";
         }
         else
         {
            GXv_int7[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFAmbienteTecnologico_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[AmbienteTecnologico_Descricao] = @AV15TFAmbienteTecnologico_Descricao_Sel)";
         }
         else
         {
            GXv_int7[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFMetodologia_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFMetodologia_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Metodologia_Descricao] like @lV16TFMetodologia_Descricao)";
         }
         else
         {
            GXv_int7[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFMetodologia_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Metodologia_Descricao] = @AV17TFMetodologia_Descricao_Sel)";
         }
         else
         {
            GXv_int7[9] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Metodologia_Codigo]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00FS2(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (bool)dynConstraints[16] );
               case 1 :
                     return conditional_P00FS3(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (bool)dynConstraints[16] );
               case 2 :
                     return conditional_P00FS4(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (bool)dynConstraints[16] );
               case 3 :
                     return conditional_P00FS5(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (bool)dynConstraints[16] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00FS2 ;
          prmP00FS2 = new Object[] {
          new Object[] {"@AV36Sistema_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV39Sistema_Nome1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV10TFSistema_Nome",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV11TFSistema_Nome_Sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV12TFSistema_Sigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV13TFSistema_Sigla_Sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV14TFAmbienteTecnologico_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV15TFAmbienteTecnologico_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV16TFMetodologia_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV17TFMetodologia_Descricao_Sel",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP00FS3 ;
          prmP00FS3 = new Object[] {
          new Object[] {"@AV36Sistema_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV39Sistema_Nome1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV10TFSistema_Nome",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV11TFSistema_Nome_Sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV12TFSistema_Sigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV13TFSistema_Sigla_Sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV14TFAmbienteTecnologico_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV15TFAmbienteTecnologico_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV16TFMetodologia_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV17TFMetodologia_Descricao_Sel",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP00FS4 ;
          prmP00FS4 = new Object[] {
          new Object[] {"@AV36Sistema_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV39Sistema_Nome1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV10TFSistema_Nome",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV11TFSistema_Nome_Sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV12TFSistema_Sigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV13TFSistema_Sigla_Sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV14TFAmbienteTecnologico_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV15TFAmbienteTecnologico_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV16TFMetodologia_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV17TFMetodologia_Descricao_Sel",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP00FS5 ;
          prmP00FS5 = new Object[] {
          new Object[] {"@AV36Sistema_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV39Sistema_Nome1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV10TFSistema_Nome",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV11TFSistema_Nome_Sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV12TFSistema_Sigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV13TFSistema_Sigla_Sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV14TFAmbienteTecnologico_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV15TFAmbienteTecnologico_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV16TFMetodologia_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV17TFMetodologia_Descricao_Sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00FS2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00FS2,100,0,true,false )
             ,new CursorDef("P00FS3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00FS3,100,0,true,false )
             ,new CursorDef("P00FS4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00FS4,100,0,true,false )
             ,new CursorDef("P00FS5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00FS5,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((bool[]) buf[4])[0] = rslt.getBool(3) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((String[]) buf[8])[0] = rslt.getString(7, 25) ;
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 25) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.getBool(7) ;
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 25) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.getBool(7) ;
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 25) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.getBool(7) ;
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptsistemafilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptsistemafilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptsistemafilterdata") )
          {
             return  ;
          }
          getpromptsistemafilterdata worker = new getpromptsistemafilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
