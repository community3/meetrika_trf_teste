/*
               File: ContagemResultadoNotificacao_BC
        Description: Notifica��es da Contagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:17:44.65
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadonotificacao_bc : GXHttpHandler, IGxSilentTrn
   {
      public contagemresultadonotificacao_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemresultadonotificacao_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow3O167( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey3O167( ) ;
         standaloneModal( ) ;
         AddRow3O167( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E113O2 */
            E113O2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z1412ContagemResultadoNotificacao_Codigo = A1412ContagemResultadoNotificacao_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_3O0( )
      {
         BeforeValidate3O167( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls3O167( ) ;
            }
            else
            {
               CheckExtendedTable3O167( ) ;
               if ( AnyError == 0 )
               {
                  ZM3O167( 8) ;
                  ZM3O167( 9) ;
                  ZM3O167( 10) ;
                  ZM3O167( 11) ;
                  ZM3O167( 12) ;
                  ZM3O167( 13) ;
               }
               CloseExtendedTableCursors3O167( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            /* Save parent mode. */
            sMode167 = Gx_mode;
            CONFIRM_3O168( ) ;
            if ( AnyError == 0 )
            {
               CONFIRM_3O169( ) ;
               if ( AnyError == 0 )
               {
                  /* Restore parent mode. */
                  Gx_mode = sMode167;
                  IsConfirmed = 1;
               }
            }
            /* Restore parent mode. */
            Gx_mode = sMode167;
         }
      }

      protected void CONFIRM_3O169( )
      {
         nGXsfl_169_idx = 0;
         while ( nGXsfl_169_idx < bcContagemResultadoNotificacao.gxTpr_Demanda.Count )
         {
            ReadRow3O169( ) ;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( Gx_mode)) )
            {
               if ( RcdFound169 == 0 )
               {
                  Gx_mode = "INS";
               }
               else
               {
                  Gx_mode = "UPD";
               }
            }
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) || ( nIsMod_169 != 0 ) )
            {
               GetKey3O169( ) ;
               if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 ) )
               {
                  if ( RcdFound169 == 0 )
                  {
                     Gx_mode = "INS";
                     BeforeValidate3O169( ) ;
                     if ( AnyError == 0 )
                     {
                        CheckExtendedTable3O169( ) ;
                        if ( AnyError == 0 )
                        {
                           ZM3O169( 18) ;
                           ZM3O169( 19) ;
                           ZM3O169( 20) ;
                           ZM3O169( 22) ;
                        }
                        CloseExtendedTableCursors3O169( ) ;
                        if ( AnyError == 0 )
                        {
                           IsConfirmed = 1;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                     AnyError = 1;
                  }
               }
               else
               {
                  if ( RcdFound169 != 0 )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                     {
                        Gx_mode = "DLT";
                        getByPrimaryKey3O169( ) ;
                        Load3O169( ) ;
                        BeforeValidate3O169( ) ;
                        if ( AnyError == 0 )
                        {
                           OnDeleteControls3O169( ) ;
                        }
                     }
                     else
                     {
                        if ( nIsMod_169 != 0 )
                        {
                           Gx_mode = "UPD";
                           BeforeValidate3O169( ) ;
                           if ( AnyError == 0 )
                           {
                              CheckExtendedTable3O169( ) ;
                              if ( AnyError == 0 )
                              {
                                 ZM3O169( 18) ;
                                 ZM3O169( 19) ;
                                 ZM3O169( 20) ;
                                 ZM3O169( 22) ;
                              }
                              CloseExtendedTableCursors3O169( ) ;
                              if ( AnyError == 0 )
                              {
                                 IsConfirmed = 1;
                              }
                           }
                        }
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
               VarsToRow169( ((SdtContagemResultadoNotificacao_Demanda)bcContagemResultadoNotificacao.gxTpr_Demanda.Item(nGXsfl_169_idx))) ;
            }
         }
         /* Start of After( level) rules */
         /* Using cursor BC003O8 */
         pr_default.execute(5, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            A1963ContagemResultadoNotificacao_Demandas = BC003O8_A1963ContagemResultadoNotificacao_Demandas[0];
            n1963ContagemResultadoNotificacao_Demandas = BC003O8_n1963ContagemResultadoNotificacao_Demandas[0];
         }
         else
         {
            A1963ContagemResultadoNotificacao_Demandas = 0;
            n1963ContagemResultadoNotificacao_Demandas = false;
         }
         /* End of After( level) rules */
      }

      protected void CONFIRM_3O168( )
      {
         s1962ContagemResultadoNotificacao_Destinatarios = O1962ContagemResultadoNotificacao_Destinatarios;
         n1962ContagemResultadoNotificacao_Destinatarios = false;
         nGXsfl_168_idx = 0;
         while ( nGXsfl_168_idx < bcContagemResultadoNotificacao.gxTpr_Destinatario.Count )
         {
            ReadRow3O168( ) ;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( Gx_mode)) )
            {
               if ( RcdFound168 == 0 )
               {
                  Gx_mode = "INS";
               }
               else
               {
                  Gx_mode = "UPD";
               }
            }
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) || ( nIsMod_168 != 0 ) )
            {
               GetKey3O168( ) ;
               if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 ) )
               {
                  if ( RcdFound168 == 0 )
                  {
                     Gx_mode = "INS";
                     BeforeValidate3O168( ) ;
                     if ( AnyError == 0 )
                     {
                        CheckExtendedTable3O168( ) ;
                        if ( AnyError == 0 )
                        {
                           ZM3O168( 15) ;
                           ZM3O168( 16) ;
                        }
                        CloseExtendedTableCursors3O168( ) ;
                        if ( AnyError == 0 )
                        {
                           IsConfirmed = 1;
                        }
                        O1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
                        n1962ContagemResultadoNotificacao_Destinatarios = false;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                     AnyError = 1;
                  }
               }
               else
               {
                  if ( RcdFound168 != 0 )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                     {
                        Gx_mode = "DLT";
                        getByPrimaryKey3O168( ) ;
                        Load3O168( ) ;
                        BeforeValidate3O168( ) ;
                        if ( AnyError == 0 )
                        {
                           OnDeleteControls3O168( ) ;
                           O1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
                           n1962ContagemResultadoNotificacao_Destinatarios = false;
                        }
                     }
                     else
                     {
                        if ( nIsMod_168 != 0 )
                        {
                           Gx_mode = "UPD";
                           BeforeValidate3O168( ) ;
                           if ( AnyError == 0 )
                           {
                              CheckExtendedTable3O168( ) ;
                              if ( AnyError == 0 )
                              {
                                 ZM3O168( 15) ;
                                 ZM3O168( 16) ;
                              }
                              CloseExtendedTableCursors3O168( ) ;
                              if ( AnyError == 0 )
                              {
                                 IsConfirmed = 1;
                              }
                              O1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
                              n1962ContagemResultadoNotificacao_Destinatarios = false;
                           }
                        }
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
               VarsToRow168( ((SdtContagemResultadoNotificacao_Destinatario)bcContagemResultadoNotificacao.gxTpr_Destinatario.Item(nGXsfl_168_idx))) ;
            }
         }
         O1962ContagemResultadoNotificacao_Destinatarios = s1962ContagemResultadoNotificacao_Destinatarios;
         n1962ContagemResultadoNotificacao_Destinatarios = false;
         /* Start of After( level) rules */
         /* End of After( level) rules */
      }

      protected void E123O2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV16Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV17GXV1 = 1;
            while ( AV17GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV17GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "ContagemResultadoNotificacao_AreaTrabalhoCod") == 0 )
               {
                  AV15Insert_ContagemResultadoNotificacao_AreaTrabalhoCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "ContagemResultadoNotificacao_UsuCod") == 0 )
               {
                  AV11Insert_ContagemResultadoNotificacao_UsuCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "ContagemResultadoNotificacao_ReenvioCod") == 0 )
               {
                  AV14Insert_ContagemResultadoNotificacao_ReenvioCod = (long)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               AV17GXV1 = (int)(AV17GXV1+1);
            }
         }
      }

      protected void E113O2( )
      {
         /* After Trn Routine */
      }

      protected void ZM3O167( short GX_JID )
      {
         if ( ( GX_JID == 7 ) || ( GX_JID == 0 ) )
         {
            Z1416ContagemResultadoNotificacao_DataHora = A1416ContagemResultadoNotificacao_DataHora;
            Z1418ContagemResultadoNotificacao_CorpoEmail = A1418ContagemResultadoNotificacao_CorpoEmail;
            Z1420ContagemResultadoNotificacao_Midia = A1420ContagemResultadoNotificacao_Midia;
            Z1956ContagemResultadoNotificacao_Host = A1956ContagemResultadoNotificacao_Host;
            Z1957ContagemResultadoNotificacao_User = A1957ContagemResultadoNotificacao_User;
            Z1958ContagemResultadoNotificacao_Port = A1958ContagemResultadoNotificacao_Port;
            Z1959ContagemResultadoNotificacao_Aut = A1959ContagemResultadoNotificacao_Aut;
            Z1960ContagemResultadoNotificacao_Sec = A1960ContagemResultadoNotificacao_Sec;
            Z1961ContagemResultadoNotificacao_Logged = A1961ContagemResultadoNotificacao_Logged;
            Z1413ContagemResultadoNotificacao_UsuCod = A1413ContagemResultadoNotificacao_UsuCod;
            Z1966ContagemResultadoNotificacao_AreaTrabalhoCod = A1966ContagemResultadoNotificacao_AreaTrabalhoCod;
            Z1965ContagemResultadoNotificacao_ReenvioCod = A1965ContagemResultadoNotificacao_ReenvioCod;
            Z1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
            Z1963ContagemResultadoNotificacao_Demandas = A1963ContagemResultadoNotificacao_Demandas;
            Z1414ContagemResultadoNotificacao_DestCod = A1414ContagemResultadoNotificacao_DestCod;
            Z1426ContagemResultadoNotificacao_DestPesCod = A1426ContagemResultadoNotificacao_DestPesCod;
            Z1421ContagemResultadoNotificacao_DestNome = A1421ContagemResultadoNotificacao_DestNome;
            Z1419ContagemResultadoNotificacao_DestEmail = A1419ContagemResultadoNotificacao_DestEmail;
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            Z457ContagemResultado_Demanda = A457ContagemResultado_Demanda;
            Z493ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            Z601ContagemResultado_Servico = A601ContagemResultado_Servico;
            Z801ContagemResultado_ServicoSigla = A801ContagemResultado_ServicoSigla;
            Z1227ContagemResultado_PrazoInicialDias = A1227ContagemResultado_PrazoInicialDias;
         }
         if ( ( GX_JID == 8 ) || ( GX_JID == 0 ) )
         {
            Z1427ContagemResultadoNotificacao_UsuPesCod = A1427ContagemResultadoNotificacao_UsuPesCod;
            Z1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
            Z1963ContagemResultadoNotificacao_Demandas = A1963ContagemResultadoNotificacao_Demandas;
            Z1414ContagemResultadoNotificacao_DestCod = A1414ContagemResultadoNotificacao_DestCod;
            Z1426ContagemResultadoNotificacao_DestPesCod = A1426ContagemResultadoNotificacao_DestPesCod;
            Z1421ContagemResultadoNotificacao_DestNome = A1421ContagemResultadoNotificacao_DestNome;
            Z1419ContagemResultadoNotificacao_DestEmail = A1419ContagemResultadoNotificacao_DestEmail;
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            Z457ContagemResultado_Demanda = A457ContagemResultado_Demanda;
            Z493ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            Z601ContagemResultado_Servico = A601ContagemResultado_Servico;
            Z801ContagemResultado_ServicoSigla = A801ContagemResultado_ServicoSigla;
            Z1227ContagemResultado_PrazoInicialDias = A1227ContagemResultado_PrazoInicialDias;
         }
         if ( ( GX_JID == 9 ) || ( GX_JID == 0 ) )
         {
            Z1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
            Z1963ContagemResultadoNotificacao_Demandas = A1963ContagemResultadoNotificacao_Demandas;
            Z1414ContagemResultadoNotificacao_DestCod = A1414ContagemResultadoNotificacao_DestCod;
            Z1426ContagemResultadoNotificacao_DestPesCod = A1426ContagemResultadoNotificacao_DestPesCod;
            Z1421ContagemResultadoNotificacao_DestNome = A1421ContagemResultadoNotificacao_DestNome;
            Z1419ContagemResultadoNotificacao_DestEmail = A1419ContagemResultadoNotificacao_DestEmail;
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            Z457ContagemResultado_Demanda = A457ContagemResultado_Demanda;
            Z493ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            Z601ContagemResultado_Servico = A601ContagemResultado_Servico;
            Z801ContagemResultado_ServicoSigla = A801ContagemResultado_ServicoSigla;
            Z1227ContagemResultado_PrazoInicialDias = A1227ContagemResultado_PrazoInicialDias;
         }
         if ( ( GX_JID == 10 ) || ( GX_JID == 0 ) )
         {
            Z1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
            Z1963ContagemResultadoNotificacao_Demandas = A1963ContagemResultadoNotificacao_Demandas;
            Z1414ContagemResultadoNotificacao_DestCod = A1414ContagemResultadoNotificacao_DestCod;
            Z1426ContagemResultadoNotificacao_DestPesCod = A1426ContagemResultadoNotificacao_DestPesCod;
            Z1421ContagemResultadoNotificacao_DestNome = A1421ContagemResultadoNotificacao_DestNome;
            Z1419ContagemResultadoNotificacao_DestEmail = A1419ContagemResultadoNotificacao_DestEmail;
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            Z457ContagemResultado_Demanda = A457ContagemResultado_Demanda;
            Z493ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            Z601ContagemResultado_Servico = A601ContagemResultado_Servico;
            Z801ContagemResultado_ServicoSigla = A801ContagemResultado_ServicoSigla;
            Z1227ContagemResultado_PrazoInicialDias = A1227ContagemResultado_PrazoInicialDias;
         }
         if ( ( GX_JID == 11 ) || ( GX_JID == 0 ) )
         {
            Z1422ContagemResultadoNotificacao_UsuNom = A1422ContagemResultadoNotificacao_UsuNom;
            Z1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
            Z1963ContagemResultadoNotificacao_Demandas = A1963ContagemResultadoNotificacao_Demandas;
            Z1414ContagemResultadoNotificacao_DestCod = A1414ContagemResultadoNotificacao_DestCod;
            Z1426ContagemResultadoNotificacao_DestPesCod = A1426ContagemResultadoNotificacao_DestPesCod;
            Z1421ContagemResultadoNotificacao_DestNome = A1421ContagemResultadoNotificacao_DestNome;
            Z1419ContagemResultadoNotificacao_DestEmail = A1419ContagemResultadoNotificacao_DestEmail;
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            Z457ContagemResultado_Demanda = A457ContagemResultado_Demanda;
            Z493ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            Z601ContagemResultado_Servico = A601ContagemResultado_Servico;
            Z801ContagemResultado_ServicoSigla = A801ContagemResultado_ServicoSigla;
            Z1227ContagemResultado_PrazoInicialDias = A1227ContagemResultado_PrazoInicialDias;
         }
         if ( ( GX_JID == 12 ) || ( GX_JID == 0 ) )
         {
            Z1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
            Z1963ContagemResultadoNotificacao_Demandas = A1963ContagemResultadoNotificacao_Demandas;
            Z1414ContagemResultadoNotificacao_DestCod = A1414ContagemResultadoNotificacao_DestCod;
            Z1426ContagemResultadoNotificacao_DestPesCod = A1426ContagemResultadoNotificacao_DestPesCod;
            Z1421ContagemResultadoNotificacao_DestNome = A1421ContagemResultadoNotificacao_DestNome;
            Z1419ContagemResultadoNotificacao_DestEmail = A1419ContagemResultadoNotificacao_DestEmail;
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            Z457ContagemResultado_Demanda = A457ContagemResultado_Demanda;
            Z493ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            Z601ContagemResultado_Servico = A601ContagemResultado_Servico;
            Z801ContagemResultado_ServicoSigla = A801ContagemResultado_ServicoSigla;
            Z1227ContagemResultado_PrazoInicialDias = A1227ContagemResultado_PrazoInicialDias;
         }
         if ( ( GX_JID == 13 ) || ( GX_JID == 0 ) )
         {
            Z1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
            Z1963ContagemResultadoNotificacao_Demandas = A1963ContagemResultadoNotificacao_Demandas;
            Z1414ContagemResultadoNotificacao_DestCod = A1414ContagemResultadoNotificacao_DestCod;
            Z1426ContagemResultadoNotificacao_DestPesCod = A1426ContagemResultadoNotificacao_DestPesCod;
            Z1421ContagemResultadoNotificacao_DestNome = A1421ContagemResultadoNotificacao_DestNome;
            Z1419ContagemResultadoNotificacao_DestEmail = A1419ContagemResultadoNotificacao_DestEmail;
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            Z457ContagemResultado_Demanda = A457ContagemResultado_Demanda;
            Z493ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            Z601ContagemResultado_Servico = A601ContagemResultado_Servico;
            Z801ContagemResultado_ServicoSigla = A801ContagemResultado_ServicoSigla;
            Z1227ContagemResultado_PrazoInicialDias = A1227ContagemResultado_PrazoInicialDias;
         }
         if ( GX_JID == -7 )
         {
            Z1412ContagemResultadoNotificacao_Codigo = A1412ContagemResultadoNotificacao_Codigo;
            Z1416ContagemResultadoNotificacao_DataHora = A1416ContagemResultadoNotificacao_DataHora;
            Z1417ContagemResultadoNotificacao_Assunto = A1417ContagemResultadoNotificacao_Assunto;
            Z1418ContagemResultadoNotificacao_CorpoEmail = A1418ContagemResultadoNotificacao_CorpoEmail;
            Z1420ContagemResultadoNotificacao_Midia = A1420ContagemResultadoNotificacao_Midia;
            Z1415ContagemResultadoNotificacao_Observacao = A1415ContagemResultadoNotificacao_Observacao;
            Z1956ContagemResultadoNotificacao_Host = A1956ContagemResultadoNotificacao_Host;
            Z1957ContagemResultadoNotificacao_User = A1957ContagemResultadoNotificacao_User;
            Z1958ContagemResultadoNotificacao_Port = A1958ContagemResultadoNotificacao_Port;
            Z1959ContagemResultadoNotificacao_Aut = A1959ContagemResultadoNotificacao_Aut;
            Z1960ContagemResultadoNotificacao_Sec = A1960ContagemResultadoNotificacao_Sec;
            Z1961ContagemResultadoNotificacao_Logged = A1961ContagemResultadoNotificacao_Logged;
            Z1413ContagemResultadoNotificacao_UsuCod = A1413ContagemResultadoNotificacao_UsuCod;
            Z1966ContagemResultadoNotificacao_AreaTrabalhoCod = A1966ContagemResultadoNotificacao_AreaTrabalhoCod;
            Z1965ContagemResultadoNotificacao_ReenvioCod = A1965ContagemResultadoNotificacao_ReenvioCod;
            Z1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
            Z1963ContagemResultadoNotificacao_Demandas = A1963ContagemResultadoNotificacao_Demandas;
            Z1427ContagemResultadoNotificacao_UsuPesCod = A1427ContagemResultadoNotificacao_UsuPesCod;
            Z1422ContagemResultadoNotificacao_UsuNom = A1422ContagemResultadoNotificacao_UsuNom;
         }
      }

      protected void standaloneNotModal( )
      {
         AV16Pgmname = "ContagemResultadoNotificacao_BC";
      }

      protected void standaloneModal( )
      {
      }

      protected void Load3O167( )
      {
         /* Using cursor BC003O23 */
         pr_default.execute(17, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
         if ( (pr_default.getStatus(17) != 101) )
         {
            RcdFound167 = 1;
            A1416ContagemResultadoNotificacao_DataHora = BC003O23_A1416ContagemResultadoNotificacao_DataHora[0];
            n1416ContagemResultadoNotificacao_DataHora = BC003O23_n1416ContagemResultadoNotificacao_DataHora[0];
            A1422ContagemResultadoNotificacao_UsuNom = BC003O23_A1422ContagemResultadoNotificacao_UsuNom[0];
            n1422ContagemResultadoNotificacao_UsuNom = BC003O23_n1422ContagemResultadoNotificacao_UsuNom[0];
            A1417ContagemResultadoNotificacao_Assunto = BC003O23_A1417ContagemResultadoNotificacao_Assunto[0];
            n1417ContagemResultadoNotificacao_Assunto = BC003O23_n1417ContagemResultadoNotificacao_Assunto[0];
            A1418ContagemResultadoNotificacao_CorpoEmail = BC003O23_A1418ContagemResultadoNotificacao_CorpoEmail[0];
            n1418ContagemResultadoNotificacao_CorpoEmail = BC003O23_n1418ContagemResultadoNotificacao_CorpoEmail[0];
            A1420ContagemResultadoNotificacao_Midia = BC003O23_A1420ContagemResultadoNotificacao_Midia[0];
            n1420ContagemResultadoNotificacao_Midia = BC003O23_n1420ContagemResultadoNotificacao_Midia[0];
            A1415ContagemResultadoNotificacao_Observacao = BC003O23_A1415ContagemResultadoNotificacao_Observacao[0];
            n1415ContagemResultadoNotificacao_Observacao = BC003O23_n1415ContagemResultadoNotificacao_Observacao[0];
            A1956ContagemResultadoNotificacao_Host = BC003O23_A1956ContagemResultadoNotificacao_Host[0];
            n1956ContagemResultadoNotificacao_Host = BC003O23_n1956ContagemResultadoNotificacao_Host[0];
            A1957ContagemResultadoNotificacao_User = BC003O23_A1957ContagemResultadoNotificacao_User[0];
            n1957ContagemResultadoNotificacao_User = BC003O23_n1957ContagemResultadoNotificacao_User[0];
            A1958ContagemResultadoNotificacao_Port = BC003O23_A1958ContagemResultadoNotificacao_Port[0];
            n1958ContagemResultadoNotificacao_Port = BC003O23_n1958ContagemResultadoNotificacao_Port[0];
            A1959ContagemResultadoNotificacao_Aut = BC003O23_A1959ContagemResultadoNotificacao_Aut[0];
            n1959ContagemResultadoNotificacao_Aut = BC003O23_n1959ContagemResultadoNotificacao_Aut[0];
            A1960ContagemResultadoNotificacao_Sec = BC003O23_A1960ContagemResultadoNotificacao_Sec[0];
            n1960ContagemResultadoNotificacao_Sec = BC003O23_n1960ContagemResultadoNotificacao_Sec[0];
            A1961ContagemResultadoNotificacao_Logged = BC003O23_A1961ContagemResultadoNotificacao_Logged[0];
            n1961ContagemResultadoNotificacao_Logged = BC003O23_n1961ContagemResultadoNotificacao_Logged[0];
            A1413ContagemResultadoNotificacao_UsuCod = BC003O23_A1413ContagemResultadoNotificacao_UsuCod[0];
            A1966ContagemResultadoNotificacao_AreaTrabalhoCod = BC003O23_A1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
            n1966ContagemResultadoNotificacao_AreaTrabalhoCod = BC003O23_n1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
            A1965ContagemResultadoNotificacao_ReenvioCod = BC003O23_A1965ContagemResultadoNotificacao_ReenvioCod[0];
            n1965ContagemResultadoNotificacao_ReenvioCod = BC003O23_n1965ContagemResultadoNotificacao_ReenvioCod[0];
            A1427ContagemResultadoNotificacao_UsuPesCod = BC003O23_A1427ContagemResultadoNotificacao_UsuPesCod[0];
            n1427ContagemResultadoNotificacao_UsuPesCod = BC003O23_n1427ContagemResultadoNotificacao_UsuPesCod[0];
            A1962ContagemResultadoNotificacao_Destinatarios = BC003O23_A1962ContagemResultadoNotificacao_Destinatarios[0];
            n1962ContagemResultadoNotificacao_Destinatarios = BC003O23_n1962ContagemResultadoNotificacao_Destinatarios[0];
            A1963ContagemResultadoNotificacao_Demandas = BC003O23_A1963ContagemResultadoNotificacao_Demandas[0];
            n1963ContagemResultadoNotificacao_Demandas = BC003O23_n1963ContagemResultadoNotificacao_Demandas[0];
            ZM3O167( -7) ;
         }
         pr_default.close(17);
         OnLoadActions3O167( ) ;
      }

      protected void OnLoadActions3O167( )
      {
         O1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
         n1962ContagemResultadoNotificacao_Destinatarios = false;
      }

      protected void CheckExtendedTable3O167( )
      {
         standaloneModal( ) ;
         /* Using cursor BC003O20 */
         pr_default.execute(16, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
         if ( (pr_default.getStatus(16) != 101) )
         {
            A1962ContagemResultadoNotificacao_Destinatarios = BC003O20_A1962ContagemResultadoNotificacao_Destinatarios[0];
            n1962ContagemResultadoNotificacao_Destinatarios = BC003O20_n1962ContagemResultadoNotificacao_Destinatarios[0];
         }
         else
         {
            A1962ContagemResultadoNotificacao_Destinatarios = 0;
            n1962ContagemResultadoNotificacao_Destinatarios = false;
         }
         pr_default.close(16);
         /* Using cursor BC003O8 */
         pr_default.execute(5, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            A1963ContagemResultadoNotificacao_Demandas = BC003O8_A1963ContagemResultadoNotificacao_Demandas[0];
            n1963ContagemResultadoNotificacao_Demandas = BC003O8_n1963ContagemResultadoNotificacao_Demandas[0];
         }
         else
         {
            A1963ContagemResultadoNotificacao_Demandas = 0;
            n1963ContagemResultadoNotificacao_Demandas = false;
         }
         pr_default.close(5);
         if ( ! ( (DateTime.MinValue==A1416ContagemResultadoNotificacao_DataHora) || ( A1416ContagemResultadoNotificacao_DataHora >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data e Hora da Notifica��o fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC003O16 */
         pr_default.execute(13, new Object[] {n1966ContagemResultadoNotificacao_AreaTrabalhoCod, A1966ContagemResultadoNotificacao_AreaTrabalhoCod});
         if ( (pr_default.getStatus(13) == 101) )
         {
            if ( ! ( (0==A1966ContagemResultadoNotificacao_AreaTrabalhoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado Notificacao_AreaTrabalhoCod'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADONOTIFICACAO_AREATRABALHOCOD");
               AnyError = 1;
            }
         }
         pr_default.close(13);
         /* Using cursor BC003O15 */
         pr_default.execute(12, new Object[] {A1413ContagemResultadoNotificacao_UsuCod});
         if ( (pr_default.getStatus(12) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usu�rio Respons�vel pela Gera��o da Notifica��o'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADONOTIFICACAO_USUCOD");
            AnyError = 1;
         }
         A1427ContagemResultadoNotificacao_UsuPesCod = BC003O15_A1427ContagemResultadoNotificacao_UsuPesCod[0];
         n1427ContagemResultadoNotificacao_UsuPesCod = BC003O15_n1427ContagemResultadoNotificacao_UsuPesCod[0];
         pr_default.close(12);
         /* Using cursor BC003O18 */
         pr_default.execute(15, new Object[] {n1427ContagemResultadoNotificacao_UsuPesCod, A1427ContagemResultadoNotificacao_UsuPesCod});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1422ContagemResultadoNotificacao_UsuNom = BC003O18_A1422ContagemResultadoNotificacao_UsuNom[0];
         n1422ContagemResultadoNotificacao_UsuNom = BC003O18_n1422ContagemResultadoNotificacao_UsuNom[0];
         pr_default.close(15);
         if ( ! ( ( StringUtil.StrCmp(A1420ContagemResultadoNotificacao_Midia, "EML") == 0 ) || ( StringUtil.StrCmp(A1420ContagemResultadoNotificacao_Midia, "SMS") == 0 ) || ( StringUtil.StrCmp(A1420ContagemResultadoNotificacao_Midia, "TEL") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A1420ContagemResultadoNotificacao_Midia)) ) )
         {
            GX_msglist.addItem("Campo M�dia da Notifica��o fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC003O17 */
         pr_default.execute(14, new Object[] {n1965ContagemResultadoNotificacao_ReenvioCod, A1965ContagemResultadoNotificacao_ReenvioCod});
         if ( (pr_default.getStatus(14) == 101) )
         {
            if ( ! ( (0==A1965ContagemResultadoNotificacao_ReenvioCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado Notificacao_ReenvioCod'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADONOTIFICACAO_REENVIOCOD");
               AnyError = 1;
            }
         }
         pr_default.close(14);
      }

      protected void CloseExtendedTableCursors3O167( )
      {
         pr_default.close(16);
         pr_default.close(5);
         pr_default.close(13);
         pr_default.close(12);
         pr_default.close(15);
         pr_default.close(14);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey3O167( )
      {
         /* Using cursor BC003O24 */
         pr_default.execute(18, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
         if ( (pr_default.getStatus(18) != 101) )
         {
            RcdFound167 = 1;
         }
         else
         {
            RcdFound167 = 0;
         }
         pr_default.close(18);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC003O14 */
         pr_default.execute(11, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            ZM3O167( 7) ;
            RcdFound167 = 1;
            A1412ContagemResultadoNotificacao_Codigo = BC003O14_A1412ContagemResultadoNotificacao_Codigo[0];
            A1416ContagemResultadoNotificacao_DataHora = BC003O14_A1416ContagemResultadoNotificacao_DataHora[0];
            n1416ContagemResultadoNotificacao_DataHora = BC003O14_n1416ContagemResultadoNotificacao_DataHora[0];
            A1417ContagemResultadoNotificacao_Assunto = BC003O14_A1417ContagemResultadoNotificacao_Assunto[0];
            n1417ContagemResultadoNotificacao_Assunto = BC003O14_n1417ContagemResultadoNotificacao_Assunto[0];
            A1418ContagemResultadoNotificacao_CorpoEmail = BC003O14_A1418ContagemResultadoNotificacao_CorpoEmail[0];
            n1418ContagemResultadoNotificacao_CorpoEmail = BC003O14_n1418ContagemResultadoNotificacao_CorpoEmail[0];
            A1420ContagemResultadoNotificacao_Midia = BC003O14_A1420ContagemResultadoNotificacao_Midia[0];
            n1420ContagemResultadoNotificacao_Midia = BC003O14_n1420ContagemResultadoNotificacao_Midia[0];
            A1415ContagemResultadoNotificacao_Observacao = BC003O14_A1415ContagemResultadoNotificacao_Observacao[0];
            n1415ContagemResultadoNotificacao_Observacao = BC003O14_n1415ContagemResultadoNotificacao_Observacao[0];
            A1956ContagemResultadoNotificacao_Host = BC003O14_A1956ContagemResultadoNotificacao_Host[0];
            n1956ContagemResultadoNotificacao_Host = BC003O14_n1956ContagemResultadoNotificacao_Host[0];
            A1957ContagemResultadoNotificacao_User = BC003O14_A1957ContagemResultadoNotificacao_User[0];
            n1957ContagemResultadoNotificacao_User = BC003O14_n1957ContagemResultadoNotificacao_User[0];
            A1958ContagemResultadoNotificacao_Port = BC003O14_A1958ContagemResultadoNotificacao_Port[0];
            n1958ContagemResultadoNotificacao_Port = BC003O14_n1958ContagemResultadoNotificacao_Port[0];
            A1959ContagemResultadoNotificacao_Aut = BC003O14_A1959ContagemResultadoNotificacao_Aut[0];
            n1959ContagemResultadoNotificacao_Aut = BC003O14_n1959ContagemResultadoNotificacao_Aut[0];
            A1960ContagemResultadoNotificacao_Sec = BC003O14_A1960ContagemResultadoNotificacao_Sec[0];
            n1960ContagemResultadoNotificacao_Sec = BC003O14_n1960ContagemResultadoNotificacao_Sec[0];
            A1961ContagemResultadoNotificacao_Logged = BC003O14_A1961ContagemResultadoNotificacao_Logged[0];
            n1961ContagemResultadoNotificacao_Logged = BC003O14_n1961ContagemResultadoNotificacao_Logged[0];
            A1413ContagemResultadoNotificacao_UsuCod = BC003O14_A1413ContagemResultadoNotificacao_UsuCod[0];
            A1966ContagemResultadoNotificacao_AreaTrabalhoCod = BC003O14_A1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
            n1966ContagemResultadoNotificacao_AreaTrabalhoCod = BC003O14_n1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
            A1965ContagemResultadoNotificacao_ReenvioCod = BC003O14_A1965ContagemResultadoNotificacao_ReenvioCod[0];
            n1965ContagemResultadoNotificacao_ReenvioCod = BC003O14_n1965ContagemResultadoNotificacao_ReenvioCod[0];
            Z1412ContagemResultadoNotificacao_Codigo = A1412ContagemResultadoNotificacao_Codigo;
            sMode167 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load3O167( ) ;
            if ( AnyError == 1 )
            {
               RcdFound167 = 0;
               InitializeNonKey3O167( ) ;
            }
            Gx_mode = sMode167;
         }
         else
         {
            RcdFound167 = 0;
            InitializeNonKey3O167( ) ;
            sMode167 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode167;
         }
         pr_default.close(11);
      }

      protected void getEqualNoModal( )
      {
         GetKey3O167( ) ;
         if ( RcdFound167 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_3O0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency3O167( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC003O13 */
            pr_default.execute(10, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
            if ( (pr_default.getStatus(10) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoNotificacao"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(10) == 101) || ( Z1416ContagemResultadoNotificacao_DataHora != BC003O13_A1416ContagemResultadoNotificacao_DataHora[0] ) || ( StringUtil.StrCmp(Z1418ContagemResultadoNotificacao_CorpoEmail, BC003O13_A1418ContagemResultadoNotificacao_CorpoEmail[0]) != 0 ) || ( StringUtil.StrCmp(Z1420ContagemResultadoNotificacao_Midia, BC003O13_A1420ContagemResultadoNotificacao_Midia[0]) != 0 ) || ( StringUtil.StrCmp(Z1956ContagemResultadoNotificacao_Host, BC003O13_A1956ContagemResultadoNotificacao_Host[0]) != 0 ) || ( StringUtil.StrCmp(Z1957ContagemResultadoNotificacao_User, BC003O13_A1957ContagemResultadoNotificacao_User[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1958ContagemResultadoNotificacao_Port != BC003O13_A1958ContagemResultadoNotificacao_Port[0] ) || ( Z1959ContagemResultadoNotificacao_Aut != BC003O13_A1959ContagemResultadoNotificacao_Aut[0] ) || ( Z1960ContagemResultadoNotificacao_Sec != BC003O13_A1960ContagemResultadoNotificacao_Sec[0] ) || ( Z1961ContagemResultadoNotificacao_Logged != BC003O13_A1961ContagemResultadoNotificacao_Logged[0] ) || ( Z1413ContagemResultadoNotificacao_UsuCod != BC003O13_A1413ContagemResultadoNotificacao_UsuCod[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1966ContagemResultadoNotificacao_AreaTrabalhoCod != BC003O13_A1966ContagemResultadoNotificacao_AreaTrabalhoCod[0] ) || ( Z1965ContagemResultadoNotificacao_ReenvioCod != BC003O13_A1965ContagemResultadoNotificacao_ReenvioCod[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultadoNotificacao"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3O167( )
      {
         BeforeValidate3O167( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3O167( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3O167( 0) ;
            CheckOptimisticConcurrency3O167( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3O167( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3O167( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC003O25 */
                     pr_default.execute(19, new Object[] {n1416ContagemResultadoNotificacao_DataHora, A1416ContagemResultadoNotificacao_DataHora, n1417ContagemResultadoNotificacao_Assunto, A1417ContagemResultadoNotificacao_Assunto, n1418ContagemResultadoNotificacao_CorpoEmail, A1418ContagemResultadoNotificacao_CorpoEmail, n1420ContagemResultadoNotificacao_Midia, A1420ContagemResultadoNotificacao_Midia, n1415ContagemResultadoNotificacao_Observacao, A1415ContagemResultadoNotificacao_Observacao, n1956ContagemResultadoNotificacao_Host, A1956ContagemResultadoNotificacao_Host, n1957ContagemResultadoNotificacao_User, A1957ContagemResultadoNotificacao_User, n1958ContagemResultadoNotificacao_Port, A1958ContagemResultadoNotificacao_Port, n1959ContagemResultadoNotificacao_Aut, A1959ContagemResultadoNotificacao_Aut, n1960ContagemResultadoNotificacao_Sec, A1960ContagemResultadoNotificacao_Sec, n1961ContagemResultadoNotificacao_Logged, A1961ContagemResultadoNotificacao_Logged, A1413ContagemResultadoNotificacao_UsuCod, n1966ContagemResultadoNotificacao_AreaTrabalhoCod, A1966ContagemResultadoNotificacao_AreaTrabalhoCod, n1965ContagemResultadoNotificacao_ReenvioCod, A1965ContagemResultadoNotificacao_ReenvioCod});
                     A1412ContagemResultadoNotificacao_Codigo = BC003O25_A1412ContagemResultadoNotificacao_Codigo[0];
                     pr_default.close(19);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoNotificacao") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel3O167( ) ;
                           if ( AnyError == 0 )
                           {
                              /* Save values for previous() function. */
                              GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3O167( ) ;
            }
            EndLevel3O167( ) ;
         }
         CloseExtendedTableCursors3O167( ) ;
      }

      protected void Update3O167( )
      {
         BeforeValidate3O167( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3O167( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3O167( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3O167( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3O167( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC003O26 */
                     pr_default.execute(20, new Object[] {n1416ContagemResultadoNotificacao_DataHora, A1416ContagemResultadoNotificacao_DataHora, n1417ContagemResultadoNotificacao_Assunto, A1417ContagemResultadoNotificacao_Assunto, n1418ContagemResultadoNotificacao_CorpoEmail, A1418ContagemResultadoNotificacao_CorpoEmail, n1420ContagemResultadoNotificacao_Midia, A1420ContagemResultadoNotificacao_Midia, n1415ContagemResultadoNotificacao_Observacao, A1415ContagemResultadoNotificacao_Observacao, n1956ContagemResultadoNotificacao_Host, A1956ContagemResultadoNotificacao_Host, n1957ContagemResultadoNotificacao_User, A1957ContagemResultadoNotificacao_User, n1958ContagemResultadoNotificacao_Port, A1958ContagemResultadoNotificacao_Port, n1959ContagemResultadoNotificacao_Aut, A1959ContagemResultadoNotificacao_Aut, n1960ContagemResultadoNotificacao_Sec, A1960ContagemResultadoNotificacao_Sec, n1961ContagemResultadoNotificacao_Logged, A1961ContagemResultadoNotificacao_Logged, A1413ContagemResultadoNotificacao_UsuCod, n1966ContagemResultadoNotificacao_AreaTrabalhoCod, A1966ContagemResultadoNotificacao_AreaTrabalhoCod, n1965ContagemResultadoNotificacao_ReenvioCod, A1965ContagemResultadoNotificacao_ReenvioCod, A1412ContagemResultadoNotificacao_Codigo});
                     pr_default.close(20);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoNotificacao") ;
                     if ( (pr_default.getStatus(20) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoNotificacao"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3O167( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel3O167( ) ;
                           if ( AnyError == 0 )
                           {
                              getByPrimaryKey( ) ;
                              GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3O167( ) ;
         }
         CloseExtendedTableCursors3O167( ) ;
      }

      protected void DeferredUpdate3O167( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate3O167( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3O167( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3O167( ) ;
            AfterConfirm3O167( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3O167( ) ;
               if ( AnyError == 0 )
               {
                  ScanKeyStart3O169( ) ;
                  while ( RcdFound169 != 0 )
                  {
                     getByPrimaryKey3O169( ) ;
                     Delete3O169( ) ;
                     ScanKeyNext3O169( ) ;
                  }
                  ScanKeyEnd3O169( ) ;
                  A1962ContagemResultadoNotificacao_Destinatarios = O1962ContagemResultadoNotificacao_Destinatarios;
                  n1962ContagemResultadoNotificacao_Destinatarios = false;
                  ScanKeyStart3O168( ) ;
                  while ( RcdFound168 != 0 )
                  {
                     getByPrimaryKey3O168( ) ;
                     Delete3O168( ) ;
                     ScanKeyNext3O168( ) ;
                     O1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
                     n1962ContagemResultadoNotificacao_Destinatarios = false;
                  }
                  ScanKeyEnd3O168( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC003O27 */
                     pr_default.execute(21, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
                     pr_default.close(21);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoNotificacao") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( delete) rules */
                        /* End of After( delete) rules */
                        if ( AnyError == 0 )
                        {
                           GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
         sMode167 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel3O167( ) ;
         Gx_mode = sMode167;
      }

      protected void OnDeleteControls3O167( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC003O29 */
            pr_default.execute(22, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
            if ( (pr_default.getStatus(22) != 101) )
            {
               A1962ContagemResultadoNotificacao_Destinatarios = BC003O29_A1962ContagemResultadoNotificacao_Destinatarios[0];
               n1962ContagemResultadoNotificacao_Destinatarios = BC003O29_n1962ContagemResultadoNotificacao_Destinatarios[0];
            }
            else
            {
               A1962ContagemResultadoNotificacao_Destinatarios = 0;
               n1962ContagemResultadoNotificacao_Destinatarios = false;
            }
            pr_default.close(22);
            /* Using cursor BC003O31 */
            pr_default.execute(23, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
            if ( (pr_default.getStatus(23) != 101) )
            {
               A1963ContagemResultadoNotificacao_Demandas = BC003O31_A1963ContagemResultadoNotificacao_Demandas[0];
               n1963ContagemResultadoNotificacao_Demandas = BC003O31_n1963ContagemResultadoNotificacao_Demandas[0];
            }
            else
            {
               A1963ContagemResultadoNotificacao_Demandas = 0;
               n1963ContagemResultadoNotificacao_Demandas = false;
            }
            pr_default.close(23);
            /* Using cursor BC003O32 */
            pr_default.execute(24, new Object[] {A1413ContagemResultadoNotificacao_UsuCod});
            A1427ContagemResultadoNotificacao_UsuPesCod = BC003O32_A1427ContagemResultadoNotificacao_UsuPesCod[0];
            n1427ContagemResultadoNotificacao_UsuPesCod = BC003O32_n1427ContagemResultadoNotificacao_UsuPesCod[0];
            pr_default.close(24);
            /* Using cursor BC003O33 */
            pr_default.execute(25, new Object[] {n1427ContagemResultadoNotificacao_UsuPesCod, A1427ContagemResultadoNotificacao_UsuPesCod});
            A1422ContagemResultadoNotificacao_UsuNom = BC003O33_A1422ContagemResultadoNotificacao_UsuNom[0];
            n1422ContagemResultadoNotificacao_UsuNom = BC003O33_n1422ContagemResultadoNotificacao_UsuNom[0];
            pr_default.close(25);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor BC003O34 */
            pr_default.execute(26, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
            if ( (pr_default.getStatus(26) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Notifica��es da Contagem"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(26);
         }
      }

      protected void ProcessNestedLevel3O168( )
      {
         s1962ContagemResultadoNotificacao_Destinatarios = O1962ContagemResultadoNotificacao_Destinatarios;
         n1962ContagemResultadoNotificacao_Destinatarios = false;
         nGXsfl_168_idx = 0;
         while ( nGXsfl_168_idx < bcContagemResultadoNotificacao.gxTpr_Destinatario.Count )
         {
            ReadRow3O168( ) ;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( Gx_mode)) )
            {
               if ( RcdFound168 == 0 )
               {
                  Gx_mode = "INS";
               }
               else
               {
                  Gx_mode = "UPD";
               }
            }
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) || ( nIsMod_168 != 0 ) )
            {
               standaloneNotModal3O168( ) ;
               if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
               {
                  Gx_mode = "INS";
                  Insert3O168( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                  {
                     Gx_mode = "DLT";
                     Delete3O168( ) ;
                  }
                  else
                  {
                     Gx_mode = "UPD";
                     Update3O168( ) ;
                  }
               }
               O1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
               n1962ContagemResultadoNotificacao_Destinatarios = false;
            }
            KeyVarsToRow168( ((SdtContagemResultadoNotificacao_Destinatario)bcContagemResultadoNotificacao.gxTpr_Destinatario.Item(nGXsfl_168_idx))) ;
         }
         if ( AnyError == 0 )
         {
            /* Batch update SDT rows */
            nGXsfl_168_idx = 0;
            while ( nGXsfl_168_idx < bcContagemResultadoNotificacao.gxTpr_Destinatario.Count )
            {
               ReadRow3O168( ) ;
               if ( String.IsNullOrEmpty(StringUtil.RTrim( Gx_mode)) )
               {
                  if ( RcdFound168 == 0 )
                  {
                     Gx_mode = "INS";
                  }
                  else
                  {
                     Gx_mode = "UPD";
                  }
               }
               /* Update SDT row */
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  bcContagemResultadoNotificacao.gxTpr_Destinatario.RemoveElement(nGXsfl_168_idx);
                  nGXsfl_168_idx = (short)(nGXsfl_168_idx-1);
               }
               else
               {
                  Gx_mode = "UPD";
                  getByPrimaryKey3O168( ) ;
                  VarsToRow168( ((SdtContagemResultadoNotificacao_Destinatario)bcContagemResultadoNotificacao.gxTpr_Destinatario.Item(nGXsfl_168_idx))) ;
               }
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
         InitAll3O168( ) ;
         if ( AnyError != 0 )
         {
            O1962ContagemResultadoNotificacao_Destinatarios = s1962ContagemResultadoNotificacao_Destinatarios;
            n1962ContagemResultadoNotificacao_Destinatarios = false;
         }
         nRcdExists_168 = 0;
         nIsMod_168 = 0;
         Gxremove168 = 0;
      }

      protected void ProcessNestedLevel3O169( )
      {
         nGXsfl_169_idx = 0;
         while ( nGXsfl_169_idx < bcContagemResultadoNotificacao.gxTpr_Demanda.Count )
         {
            ReadRow3O169( ) ;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( Gx_mode)) )
            {
               if ( RcdFound169 == 0 )
               {
                  Gx_mode = "INS";
               }
               else
               {
                  Gx_mode = "UPD";
               }
            }
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) || ( nIsMod_169 != 0 ) )
            {
               standaloneNotModal3O169( ) ;
               if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
               {
                  Gx_mode = "INS";
                  Insert3O169( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                  {
                     Gx_mode = "DLT";
                     Delete3O169( ) ;
                  }
                  else
                  {
                     Gx_mode = "UPD";
                     Update3O169( ) ;
                  }
               }
            }
            KeyVarsToRow169( ((SdtContagemResultadoNotificacao_Demanda)bcContagemResultadoNotificacao.gxTpr_Demanda.Item(nGXsfl_169_idx))) ;
         }
         if ( AnyError == 0 )
         {
            /* Batch update SDT rows */
            nGXsfl_169_idx = 0;
            while ( nGXsfl_169_idx < bcContagemResultadoNotificacao.gxTpr_Demanda.Count )
            {
               ReadRow3O169( ) ;
               if ( String.IsNullOrEmpty(StringUtil.RTrim( Gx_mode)) )
               {
                  if ( RcdFound169 == 0 )
                  {
                     Gx_mode = "INS";
                  }
                  else
                  {
                     Gx_mode = "UPD";
                  }
               }
               /* Update SDT row */
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  bcContagemResultadoNotificacao.gxTpr_Demanda.RemoveElement(nGXsfl_169_idx);
                  nGXsfl_169_idx = (short)(nGXsfl_169_idx-1);
               }
               else
               {
                  Gx_mode = "UPD";
                  getByPrimaryKey3O169( ) ;
                  VarsToRow169( ((SdtContagemResultadoNotificacao_Demanda)bcContagemResultadoNotificacao.gxTpr_Demanda.Item(nGXsfl_169_idx))) ;
               }
            }
         }
         /* Start of After( level) rules */
         /* Using cursor BC003O31 */
         pr_default.execute(23, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
         if ( (pr_default.getStatus(23) != 101) )
         {
            A1963ContagemResultadoNotificacao_Demandas = BC003O31_A1963ContagemResultadoNotificacao_Demandas[0];
            n1963ContagemResultadoNotificacao_Demandas = BC003O31_n1963ContagemResultadoNotificacao_Demandas[0];
         }
         else
         {
            A1963ContagemResultadoNotificacao_Demandas = 0;
            n1963ContagemResultadoNotificacao_Demandas = false;
         }
         /* End of After( level) rules */
         InitAll3O169( ) ;
         if ( AnyError != 0 )
         {
         }
         nRcdExists_169 = 0;
         nIsMod_169 = 0;
         Gxremove169 = 0;
      }

      protected void ProcessLevel3O167( )
      {
         /* Save parent mode. */
         sMode167 = Gx_mode;
         ProcessNestedLevel3O168( ) ;
         ProcessNestedLevel3O169( ) ;
         if ( AnyError != 0 )
         {
            O1962ContagemResultadoNotificacao_Destinatarios = s1962ContagemResultadoNotificacao_Destinatarios;
            n1962ContagemResultadoNotificacao_Destinatarios = false;
         }
         /* Restore parent mode. */
         Gx_mode = sMode167;
         /* ' Update level parameters */
      }

      protected void EndLevel3O167( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(10);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3O167( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart3O167( )
      {
         /* Scan By routine */
         /* Using cursor BC003O37 */
         pr_default.execute(27, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
         RcdFound167 = 0;
         if ( (pr_default.getStatus(27) != 101) )
         {
            RcdFound167 = 1;
            A1412ContagemResultadoNotificacao_Codigo = BC003O37_A1412ContagemResultadoNotificacao_Codigo[0];
            A1416ContagemResultadoNotificacao_DataHora = BC003O37_A1416ContagemResultadoNotificacao_DataHora[0];
            n1416ContagemResultadoNotificacao_DataHora = BC003O37_n1416ContagemResultadoNotificacao_DataHora[0];
            A1422ContagemResultadoNotificacao_UsuNom = BC003O37_A1422ContagemResultadoNotificacao_UsuNom[0];
            n1422ContagemResultadoNotificacao_UsuNom = BC003O37_n1422ContagemResultadoNotificacao_UsuNom[0];
            A1417ContagemResultadoNotificacao_Assunto = BC003O37_A1417ContagemResultadoNotificacao_Assunto[0];
            n1417ContagemResultadoNotificacao_Assunto = BC003O37_n1417ContagemResultadoNotificacao_Assunto[0];
            A1418ContagemResultadoNotificacao_CorpoEmail = BC003O37_A1418ContagemResultadoNotificacao_CorpoEmail[0];
            n1418ContagemResultadoNotificacao_CorpoEmail = BC003O37_n1418ContagemResultadoNotificacao_CorpoEmail[0];
            A1420ContagemResultadoNotificacao_Midia = BC003O37_A1420ContagemResultadoNotificacao_Midia[0];
            n1420ContagemResultadoNotificacao_Midia = BC003O37_n1420ContagemResultadoNotificacao_Midia[0];
            A1415ContagemResultadoNotificacao_Observacao = BC003O37_A1415ContagemResultadoNotificacao_Observacao[0];
            n1415ContagemResultadoNotificacao_Observacao = BC003O37_n1415ContagemResultadoNotificacao_Observacao[0];
            A1956ContagemResultadoNotificacao_Host = BC003O37_A1956ContagemResultadoNotificacao_Host[0];
            n1956ContagemResultadoNotificacao_Host = BC003O37_n1956ContagemResultadoNotificacao_Host[0];
            A1957ContagemResultadoNotificacao_User = BC003O37_A1957ContagemResultadoNotificacao_User[0];
            n1957ContagemResultadoNotificacao_User = BC003O37_n1957ContagemResultadoNotificacao_User[0];
            A1958ContagemResultadoNotificacao_Port = BC003O37_A1958ContagemResultadoNotificacao_Port[0];
            n1958ContagemResultadoNotificacao_Port = BC003O37_n1958ContagemResultadoNotificacao_Port[0];
            A1959ContagemResultadoNotificacao_Aut = BC003O37_A1959ContagemResultadoNotificacao_Aut[0];
            n1959ContagemResultadoNotificacao_Aut = BC003O37_n1959ContagemResultadoNotificacao_Aut[0];
            A1960ContagemResultadoNotificacao_Sec = BC003O37_A1960ContagemResultadoNotificacao_Sec[0];
            n1960ContagemResultadoNotificacao_Sec = BC003O37_n1960ContagemResultadoNotificacao_Sec[0];
            A1961ContagemResultadoNotificacao_Logged = BC003O37_A1961ContagemResultadoNotificacao_Logged[0];
            n1961ContagemResultadoNotificacao_Logged = BC003O37_n1961ContagemResultadoNotificacao_Logged[0];
            A1413ContagemResultadoNotificacao_UsuCod = BC003O37_A1413ContagemResultadoNotificacao_UsuCod[0];
            A1966ContagemResultadoNotificacao_AreaTrabalhoCod = BC003O37_A1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
            n1966ContagemResultadoNotificacao_AreaTrabalhoCod = BC003O37_n1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
            A1965ContagemResultadoNotificacao_ReenvioCod = BC003O37_A1965ContagemResultadoNotificacao_ReenvioCod[0];
            n1965ContagemResultadoNotificacao_ReenvioCod = BC003O37_n1965ContagemResultadoNotificacao_ReenvioCod[0];
            A1427ContagemResultadoNotificacao_UsuPesCod = BC003O37_A1427ContagemResultadoNotificacao_UsuPesCod[0];
            n1427ContagemResultadoNotificacao_UsuPesCod = BC003O37_n1427ContagemResultadoNotificacao_UsuPesCod[0];
            A1962ContagemResultadoNotificacao_Destinatarios = BC003O37_A1962ContagemResultadoNotificacao_Destinatarios[0];
            n1962ContagemResultadoNotificacao_Destinatarios = BC003O37_n1962ContagemResultadoNotificacao_Destinatarios[0];
            A1963ContagemResultadoNotificacao_Demandas = BC003O37_A1963ContagemResultadoNotificacao_Demandas[0];
            n1963ContagemResultadoNotificacao_Demandas = BC003O37_n1963ContagemResultadoNotificacao_Demandas[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext3O167( )
      {
         /* Scan next routine */
         pr_default.readNext(27);
         RcdFound167 = 0;
         ScanKeyLoad3O167( ) ;
      }

      protected void ScanKeyLoad3O167( )
      {
         sMode167 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(27) != 101) )
         {
            RcdFound167 = 1;
            A1412ContagemResultadoNotificacao_Codigo = BC003O37_A1412ContagemResultadoNotificacao_Codigo[0];
            A1416ContagemResultadoNotificacao_DataHora = BC003O37_A1416ContagemResultadoNotificacao_DataHora[0];
            n1416ContagemResultadoNotificacao_DataHora = BC003O37_n1416ContagemResultadoNotificacao_DataHora[0];
            A1422ContagemResultadoNotificacao_UsuNom = BC003O37_A1422ContagemResultadoNotificacao_UsuNom[0];
            n1422ContagemResultadoNotificacao_UsuNom = BC003O37_n1422ContagemResultadoNotificacao_UsuNom[0];
            A1417ContagemResultadoNotificacao_Assunto = BC003O37_A1417ContagemResultadoNotificacao_Assunto[0];
            n1417ContagemResultadoNotificacao_Assunto = BC003O37_n1417ContagemResultadoNotificacao_Assunto[0];
            A1418ContagemResultadoNotificacao_CorpoEmail = BC003O37_A1418ContagemResultadoNotificacao_CorpoEmail[0];
            n1418ContagemResultadoNotificacao_CorpoEmail = BC003O37_n1418ContagemResultadoNotificacao_CorpoEmail[0];
            A1420ContagemResultadoNotificacao_Midia = BC003O37_A1420ContagemResultadoNotificacao_Midia[0];
            n1420ContagemResultadoNotificacao_Midia = BC003O37_n1420ContagemResultadoNotificacao_Midia[0];
            A1415ContagemResultadoNotificacao_Observacao = BC003O37_A1415ContagemResultadoNotificacao_Observacao[0];
            n1415ContagemResultadoNotificacao_Observacao = BC003O37_n1415ContagemResultadoNotificacao_Observacao[0];
            A1956ContagemResultadoNotificacao_Host = BC003O37_A1956ContagemResultadoNotificacao_Host[0];
            n1956ContagemResultadoNotificacao_Host = BC003O37_n1956ContagemResultadoNotificacao_Host[0];
            A1957ContagemResultadoNotificacao_User = BC003O37_A1957ContagemResultadoNotificacao_User[0];
            n1957ContagemResultadoNotificacao_User = BC003O37_n1957ContagemResultadoNotificacao_User[0];
            A1958ContagemResultadoNotificacao_Port = BC003O37_A1958ContagemResultadoNotificacao_Port[0];
            n1958ContagemResultadoNotificacao_Port = BC003O37_n1958ContagemResultadoNotificacao_Port[0];
            A1959ContagemResultadoNotificacao_Aut = BC003O37_A1959ContagemResultadoNotificacao_Aut[0];
            n1959ContagemResultadoNotificacao_Aut = BC003O37_n1959ContagemResultadoNotificacao_Aut[0];
            A1960ContagemResultadoNotificacao_Sec = BC003O37_A1960ContagemResultadoNotificacao_Sec[0];
            n1960ContagemResultadoNotificacao_Sec = BC003O37_n1960ContagemResultadoNotificacao_Sec[0];
            A1961ContagemResultadoNotificacao_Logged = BC003O37_A1961ContagemResultadoNotificacao_Logged[0];
            n1961ContagemResultadoNotificacao_Logged = BC003O37_n1961ContagemResultadoNotificacao_Logged[0];
            A1413ContagemResultadoNotificacao_UsuCod = BC003O37_A1413ContagemResultadoNotificacao_UsuCod[0];
            A1966ContagemResultadoNotificacao_AreaTrabalhoCod = BC003O37_A1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
            n1966ContagemResultadoNotificacao_AreaTrabalhoCod = BC003O37_n1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
            A1965ContagemResultadoNotificacao_ReenvioCod = BC003O37_A1965ContagemResultadoNotificacao_ReenvioCod[0];
            n1965ContagemResultadoNotificacao_ReenvioCod = BC003O37_n1965ContagemResultadoNotificacao_ReenvioCod[0];
            A1427ContagemResultadoNotificacao_UsuPesCod = BC003O37_A1427ContagemResultadoNotificacao_UsuPesCod[0];
            n1427ContagemResultadoNotificacao_UsuPesCod = BC003O37_n1427ContagemResultadoNotificacao_UsuPesCod[0];
            A1962ContagemResultadoNotificacao_Destinatarios = BC003O37_A1962ContagemResultadoNotificacao_Destinatarios[0];
            n1962ContagemResultadoNotificacao_Destinatarios = BC003O37_n1962ContagemResultadoNotificacao_Destinatarios[0];
            A1963ContagemResultadoNotificacao_Demandas = BC003O37_A1963ContagemResultadoNotificacao_Demandas[0];
            n1963ContagemResultadoNotificacao_Demandas = BC003O37_n1963ContagemResultadoNotificacao_Demandas[0];
         }
         Gx_mode = sMode167;
      }

      protected void ScanKeyEnd3O167( )
      {
         pr_default.close(27);
      }

      protected void AfterConfirm3O167( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3O167( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3O167( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3O167( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3O167( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3O167( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3O167( )
      {
      }

      protected void ZM3O168( short GX_JID )
      {
         if ( ( GX_JID == 14 ) || ( GX_JID == 0 ) )
         {
            Z1419ContagemResultadoNotificacao_DestEmail = A1419ContagemResultadoNotificacao_DestEmail;
            Z1416ContagemResultadoNotificacao_DataHora = A1416ContagemResultadoNotificacao_DataHora;
            Z1966ContagemResultadoNotificacao_AreaTrabalhoCod = A1966ContagemResultadoNotificacao_AreaTrabalhoCod;
            Z1413ContagemResultadoNotificacao_UsuCod = A1413ContagemResultadoNotificacao_UsuCod;
            Z1427ContagemResultadoNotificacao_UsuPesCod = A1427ContagemResultadoNotificacao_UsuPesCod;
            Z1422ContagemResultadoNotificacao_UsuNom = A1422ContagemResultadoNotificacao_UsuNom;
            Z1418ContagemResultadoNotificacao_CorpoEmail = A1418ContagemResultadoNotificacao_CorpoEmail;
            Z1420ContagemResultadoNotificacao_Midia = A1420ContagemResultadoNotificacao_Midia;
            Z1956ContagemResultadoNotificacao_Host = A1956ContagemResultadoNotificacao_Host;
            Z1957ContagemResultadoNotificacao_User = A1957ContagemResultadoNotificacao_User;
            Z1958ContagemResultadoNotificacao_Port = A1958ContagemResultadoNotificacao_Port;
            Z1959ContagemResultadoNotificacao_Aut = A1959ContagemResultadoNotificacao_Aut;
            Z1960ContagemResultadoNotificacao_Sec = A1960ContagemResultadoNotificacao_Sec;
            Z1961ContagemResultadoNotificacao_Logged = A1961ContagemResultadoNotificacao_Logged;
            Z1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
            Z1963ContagemResultadoNotificacao_Demandas = A1963ContagemResultadoNotificacao_Demandas;
            Z1965ContagemResultadoNotificacao_ReenvioCod = A1965ContagemResultadoNotificacao_ReenvioCod;
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            Z457ContagemResultado_Demanda = A457ContagemResultado_Demanda;
            Z493ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            Z601ContagemResultado_Servico = A601ContagemResultado_Servico;
            Z801ContagemResultado_ServicoSigla = A801ContagemResultado_ServicoSigla;
            Z1227ContagemResultado_PrazoInicialDias = A1227ContagemResultado_PrazoInicialDias;
         }
         if ( ( GX_JID == 15 ) || ( GX_JID == 0 ) )
         {
            Z1426ContagemResultadoNotificacao_DestPesCod = A1426ContagemResultadoNotificacao_DestPesCod;
            Z1416ContagemResultadoNotificacao_DataHora = A1416ContagemResultadoNotificacao_DataHora;
            Z1966ContagemResultadoNotificacao_AreaTrabalhoCod = A1966ContagemResultadoNotificacao_AreaTrabalhoCod;
            Z1413ContagemResultadoNotificacao_UsuCod = A1413ContagemResultadoNotificacao_UsuCod;
            Z1427ContagemResultadoNotificacao_UsuPesCod = A1427ContagemResultadoNotificacao_UsuPesCod;
            Z1422ContagemResultadoNotificacao_UsuNom = A1422ContagemResultadoNotificacao_UsuNom;
            Z1418ContagemResultadoNotificacao_CorpoEmail = A1418ContagemResultadoNotificacao_CorpoEmail;
            Z1420ContagemResultadoNotificacao_Midia = A1420ContagemResultadoNotificacao_Midia;
            Z1956ContagemResultadoNotificacao_Host = A1956ContagemResultadoNotificacao_Host;
            Z1957ContagemResultadoNotificacao_User = A1957ContagemResultadoNotificacao_User;
            Z1958ContagemResultadoNotificacao_Port = A1958ContagemResultadoNotificacao_Port;
            Z1959ContagemResultadoNotificacao_Aut = A1959ContagemResultadoNotificacao_Aut;
            Z1960ContagemResultadoNotificacao_Sec = A1960ContagemResultadoNotificacao_Sec;
            Z1961ContagemResultadoNotificacao_Logged = A1961ContagemResultadoNotificacao_Logged;
            Z1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
            Z1963ContagemResultadoNotificacao_Demandas = A1963ContagemResultadoNotificacao_Demandas;
            Z1965ContagemResultadoNotificacao_ReenvioCod = A1965ContagemResultadoNotificacao_ReenvioCod;
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            Z457ContagemResultado_Demanda = A457ContagemResultado_Demanda;
            Z493ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            Z601ContagemResultado_Servico = A601ContagemResultado_Servico;
            Z801ContagemResultado_ServicoSigla = A801ContagemResultado_ServicoSigla;
            Z1227ContagemResultado_PrazoInicialDias = A1227ContagemResultado_PrazoInicialDias;
         }
         if ( ( GX_JID == 16 ) || ( GX_JID == 0 ) )
         {
            Z1421ContagemResultadoNotificacao_DestNome = A1421ContagemResultadoNotificacao_DestNome;
            Z1416ContagemResultadoNotificacao_DataHora = A1416ContagemResultadoNotificacao_DataHora;
            Z1966ContagemResultadoNotificacao_AreaTrabalhoCod = A1966ContagemResultadoNotificacao_AreaTrabalhoCod;
            Z1413ContagemResultadoNotificacao_UsuCod = A1413ContagemResultadoNotificacao_UsuCod;
            Z1427ContagemResultadoNotificacao_UsuPesCod = A1427ContagemResultadoNotificacao_UsuPesCod;
            Z1422ContagemResultadoNotificacao_UsuNom = A1422ContagemResultadoNotificacao_UsuNom;
            Z1418ContagemResultadoNotificacao_CorpoEmail = A1418ContagemResultadoNotificacao_CorpoEmail;
            Z1420ContagemResultadoNotificacao_Midia = A1420ContagemResultadoNotificacao_Midia;
            Z1956ContagemResultadoNotificacao_Host = A1956ContagemResultadoNotificacao_Host;
            Z1957ContagemResultadoNotificacao_User = A1957ContagemResultadoNotificacao_User;
            Z1958ContagemResultadoNotificacao_Port = A1958ContagemResultadoNotificacao_Port;
            Z1959ContagemResultadoNotificacao_Aut = A1959ContagemResultadoNotificacao_Aut;
            Z1960ContagemResultadoNotificacao_Sec = A1960ContagemResultadoNotificacao_Sec;
            Z1961ContagemResultadoNotificacao_Logged = A1961ContagemResultadoNotificacao_Logged;
            Z1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
            Z1963ContagemResultadoNotificacao_Demandas = A1963ContagemResultadoNotificacao_Demandas;
            Z1965ContagemResultadoNotificacao_ReenvioCod = A1965ContagemResultadoNotificacao_ReenvioCod;
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            Z457ContagemResultado_Demanda = A457ContagemResultado_Demanda;
            Z493ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            Z601ContagemResultado_Servico = A601ContagemResultado_Servico;
            Z801ContagemResultado_ServicoSigla = A801ContagemResultado_ServicoSigla;
            Z1227ContagemResultado_PrazoInicialDias = A1227ContagemResultado_PrazoInicialDias;
         }
         if ( GX_JID == -14 )
         {
            Z1412ContagemResultadoNotificacao_Codigo = A1412ContagemResultadoNotificacao_Codigo;
            Z1419ContagemResultadoNotificacao_DestEmail = A1419ContagemResultadoNotificacao_DestEmail;
            Z1414ContagemResultadoNotificacao_DestCod = A1414ContagemResultadoNotificacao_DestCod;
            Z1426ContagemResultadoNotificacao_DestPesCod = A1426ContagemResultadoNotificacao_DestPesCod;
            Z1421ContagemResultadoNotificacao_DestNome = A1421ContagemResultadoNotificacao_DestNome;
         }
      }

      protected void standaloneNotModal3O168( )
      {
      }

      protected void standaloneModal3O168( )
      {
      }

      protected void Load3O168( )
      {
         /* Using cursor BC003O38 */
         pr_default.execute(28, new Object[] {A1412ContagemResultadoNotificacao_Codigo, A1414ContagemResultadoNotificacao_DestCod});
         if ( (pr_default.getStatus(28) != 101) )
         {
            RcdFound168 = 1;
            A1421ContagemResultadoNotificacao_DestNome = BC003O38_A1421ContagemResultadoNotificacao_DestNome[0];
            n1421ContagemResultadoNotificacao_DestNome = BC003O38_n1421ContagemResultadoNotificacao_DestNome[0];
            A1419ContagemResultadoNotificacao_DestEmail = BC003O38_A1419ContagemResultadoNotificacao_DestEmail[0];
            n1419ContagemResultadoNotificacao_DestEmail = BC003O38_n1419ContagemResultadoNotificacao_DestEmail[0];
            A1426ContagemResultadoNotificacao_DestPesCod = BC003O38_A1426ContagemResultadoNotificacao_DestPesCod[0];
            n1426ContagemResultadoNotificacao_DestPesCod = BC003O38_n1426ContagemResultadoNotificacao_DestPesCod[0];
            ZM3O168( -14) ;
         }
         pr_default.close(28);
         OnLoadActions3O168( ) ;
      }

      protected void OnLoadActions3O168( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A1962ContagemResultadoNotificacao_Destinatarios = (int)(O1962ContagemResultadoNotificacao_Destinatarios+1);
            n1962ContagemResultadoNotificacao_Destinatarios = false;
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
            {
               A1962ContagemResultadoNotificacao_Destinatarios = O1962ContagemResultadoNotificacao_Destinatarios;
               n1962ContagemResultadoNotificacao_Destinatarios = false;
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  )
               {
                  A1962ContagemResultadoNotificacao_Destinatarios = (int)(O1962ContagemResultadoNotificacao_Destinatarios-1);
                  n1962ContagemResultadoNotificacao_Destinatarios = false;
               }
            }
         }
      }

      protected void CheckExtendedTable3O168( )
      {
         Gx_BScreen = 1;
         standaloneModal3O168( ) ;
         Gx_BScreen = 0;
         /* Using cursor BC003O11 */
         pr_default.execute(8, new Object[] {A1414ContagemResultadoNotificacao_DestCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Destinat�rio da Notifica��o da Contagem'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADONOTIFICACAO_DESTCOD");
            AnyError = 1;
         }
         A1426ContagemResultadoNotificacao_DestPesCod = BC003O11_A1426ContagemResultadoNotificacao_DestPesCod[0];
         n1426ContagemResultadoNotificacao_DestPesCod = BC003O11_n1426ContagemResultadoNotificacao_DestPesCod[0];
         pr_default.close(8);
         /* Using cursor BC003O12 */
         pr_default.execute(9, new Object[] {n1426ContagemResultadoNotificacao_DestPesCod, A1426ContagemResultadoNotificacao_DestPesCod});
         if ( (pr_default.getStatus(9) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1421ContagemResultadoNotificacao_DestNome = BC003O12_A1421ContagemResultadoNotificacao_DestNome[0];
         n1421ContagemResultadoNotificacao_DestNome = BC003O12_n1421ContagemResultadoNotificacao_DestNome[0];
         pr_default.close(9);
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A1962ContagemResultadoNotificacao_Destinatarios = (int)(O1962ContagemResultadoNotificacao_Destinatarios+1);
            n1962ContagemResultadoNotificacao_Destinatarios = false;
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
            {
               A1962ContagemResultadoNotificacao_Destinatarios = O1962ContagemResultadoNotificacao_Destinatarios;
               n1962ContagemResultadoNotificacao_Destinatarios = false;
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  )
               {
                  A1962ContagemResultadoNotificacao_Destinatarios = (int)(O1962ContagemResultadoNotificacao_Destinatarios-1);
                  n1962ContagemResultadoNotificacao_Destinatarios = false;
               }
            }
         }
         if ( ! ( GxRegex.IsMatch(A1419ContagemResultadoNotificacao_DestEmail,"^((\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*)|(\\s*))$") || String.IsNullOrEmpty(StringUtil.RTrim( A1419ContagemResultadoNotificacao_DestEmail)) ) )
         {
            GX_msglist.addItem("O valor de Destinat�rio Email n�o coincide com o padr�o especificado", "OutOfRange", 1, "");
            AnyError = 1;
         }
      }

      protected void CloseExtendedTableCursors3O168( )
      {
         pr_default.close(8);
         pr_default.close(9);
      }

      protected void enableDisable3O168( )
      {
      }

      protected void GetKey3O168( )
      {
         /* Using cursor BC003O39 */
         pr_default.execute(29, new Object[] {A1412ContagemResultadoNotificacao_Codigo, A1414ContagemResultadoNotificacao_DestCod});
         if ( (pr_default.getStatus(29) != 101) )
         {
            RcdFound168 = 1;
         }
         else
         {
            RcdFound168 = 0;
         }
         pr_default.close(29);
      }

      protected void getByPrimaryKey3O168( )
      {
         /* Using cursor BC003O10 */
         pr_default.execute(7, new Object[] {A1412ContagemResultadoNotificacao_Codigo, A1414ContagemResultadoNotificacao_DestCod});
         if ( (pr_default.getStatus(7) != 101) )
         {
            ZM3O168( 14) ;
            RcdFound168 = 1;
            InitializeNonKey3O168( ) ;
            A1419ContagemResultadoNotificacao_DestEmail = BC003O10_A1419ContagemResultadoNotificacao_DestEmail[0];
            n1419ContagemResultadoNotificacao_DestEmail = BC003O10_n1419ContagemResultadoNotificacao_DestEmail[0];
            A1414ContagemResultadoNotificacao_DestCod = BC003O10_A1414ContagemResultadoNotificacao_DestCod[0];
            Z1412ContagemResultadoNotificacao_Codigo = A1412ContagemResultadoNotificacao_Codigo;
            Z1414ContagemResultadoNotificacao_DestCod = A1414ContagemResultadoNotificacao_DestCod;
            sMode168 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal3O168( ) ;
            Load3O168( ) ;
            Gx_mode = sMode168;
         }
         else
         {
            RcdFound168 = 0;
            InitializeNonKey3O168( ) ;
            sMode168 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal3O168( ) ;
            Gx_mode = sMode168;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            DisableAttributes3O168( ) ;
         }
         pr_default.close(7);
      }

      protected void CheckOptimisticConcurrency3O168( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC003O9 */
            pr_default.execute(6, new Object[] {A1412ContagemResultadoNotificacao_Codigo, A1414ContagemResultadoNotificacao_DestCod});
            if ( (pr_default.getStatus(6) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoNotificacaoDestinatario"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(6) == 101) || ( StringUtil.StrCmp(Z1419ContagemResultadoNotificacao_DestEmail, BC003O9_A1419ContagemResultadoNotificacao_DestEmail[0]) != 0 ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultadoNotificacaoDestinatario"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3O168( )
      {
         BeforeValidate3O168( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3O168( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3O168( 0) ;
            CheckOptimisticConcurrency3O168( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3O168( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3O168( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC003O40 */
                     pr_default.execute(30, new Object[] {A1412ContagemResultadoNotificacao_Codigo, n1419ContagemResultadoNotificacao_DestEmail, A1419ContagemResultadoNotificacao_DestEmail, A1414ContagemResultadoNotificacao_DestCod});
                     pr_default.close(30);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoNotificacaoDestinatario") ;
                     if ( (pr_default.getStatus(30) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3O168( ) ;
            }
            EndLevel3O168( ) ;
         }
         CloseExtendedTableCursors3O168( ) ;
      }

      protected void Update3O168( )
      {
         BeforeValidate3O168( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3O168( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3O168( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3O168( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3O168( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC003O41 */
                     pr_default.execute(31, new Object[] {n1419ContagemResultadoNotificacao_DestEmail, A1419ContagemResultadoNotificacao_DestEmail, A1412ContagemResultadoNotificacao_Codigo, A1414ContagemResultadoNotificacao_DestCod});
                     pr_default.close(31);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoNotificacaoDestinatario") ;
                     if ( (pr_default.getStatus(31) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoNotificacaoDestinatario"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3O168( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey3O168( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3O168( ) ;
         }
         CloseExtendedTableCursors3O168( ) ;
      }

      protected void DeferredUpdate3O168( )
      {
      }

      protected void Delete3O168( )
      {
         Gx_mode = "DLT";
         BeforeValidate3O168( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3O168( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3O168( ) ;
            AfterConfirm3O168( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3O168( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC003O42 */
                  pr_default.execute(32, new Object[] {A1412ContagemResultadoNotificacao_Codigo, A1414ContagemResultadoNotificacao_DestCod});
                  pr_default.close(32);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoNotificacaoDestinatario") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode168 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel3O168( ) ;
         Gx_mode = sMode168;
      }

      protected void OnDeleteControls3O168( )
      {
         standaloneModal3O168( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC003O43 */
            pr_default.execute(33, new Object[] {A1414ContagemResultadoNotificacao_DestCod});
            A1426ContagemResultadoNotificacao_DestPesCod = BC003O43_A1426ContagemResultadoNotificacao_DestPesCod[0];
            n1426ContagemResultadoNotificacao_DestPesCod = BC003O43_n1426ContagemResultadoNotificacao_DestPesCod[0];
            pr_default.close(33);
            /* Using cursor BC003O44 */
            pr_default.execute(34, new Object[] {n1426ContagemResultadoNotificacao_DestPesCod, A1426ContagemResultadoNotificacao_DestPesCod});
            A1421ContagemResultadoNotificacao_DestNome = BC003O44_A1421ContagemResultadoNotificacao_DestNome[0];
            n1421ContagemResultadoNotificacao_DestNome = BC003O44_n1421ContagemResultadoNotificacao_DestNome[0];
            pr_default.close(34);
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               A1962ContagemResultadoNotificacao_Destinatarios = (int)(O1962ContagemResultadoNotificacao_Destinatarios+1);
               n1962ContagemResultadoNotificacao_Destinatarios = false;
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
               {
                  A1962ContagemResultadoNotificacao_Destinatarios = O1962ContagemResultadoNotificacao_Destinatarios;
                  n1962ContagemResultadoNotificacao_Destinatarios = false;
               }
               else
               {
                  if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  )
                  {
                     A1962ContagemResultadoNotificacao_Destinatarios = (int)(O1962ContagemResultadoNotificacao_Destinatarios-1);
                     n1962ContagemResultadoNotificacao_Destinatarios = false;
                  }
               }
            }
         }
      }

      protected void EndLevel3O168( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(6);
         }
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart3O168( )
      {
         /* Scan By routine */
         /* Using cursor BC003O45 */
         pr_default.execute(35, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
         RcdFound168 = 0;
         if ( (pr_default.getStatus(35) != 101) )
         {
            RcdFound168 = 1;
            A1421ContagemResultadoNotificacao_DestNome = BC003O45_A1421ContagemResultadoNotificacao_DestNome[0];
            n1421ContagemResultadoNotificacao_DestNome = BC003O45_n1421ContagemResultadoNotificacao_DestNome[0];
            A1419ContagemResultadoNotificacao_DestEmail = BC003O45_A1419ContagemResultadoNotificacao_DestEmail[0];
            n1419ContagemResultadoNotificacao_DestEmail = BC003O45_n1419ContagemResultadoNotificacao_DestEmail[0];
            A1414ContagemResultadoNotificacao_DestCod = BC003O45_A1414ContagemResultadoNotificacao_DestCod[0];
            A1426ContagemResultadoNotificacao_DestPesCod = BC003O45_A1426ContagemResultadoNotificacao_DestPesCod[0];
            n1426ContagemResultadoNotificacao_DestPesCod = BC003O45_n1426ContagemResultadoNotificacao_DestPesCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext3O168( )
      {
         /* Scan next routine */
         pr_default.readNext(35);
         RcdFound168 = 0;
         ScanKeyLoad3O168( ) ;
      }

      protected void ScanKeyLoad3O168( )
      {
         sMode168 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(35) != 101) )
         {
            RcdFound168 = 1;
            A1421ContagemResultadoNotificacao_DestNome = BC003O45_A1421ContagemResultadoNotificacao_DestNome[0];
            n1421ContagemResultadoNotificacao_DestNome = BC003O45_n1421ContagemResultadoNotificacao_DestNome[0];
            A1419ContagemResultadoNotificacao_DestEmail = BC003O45_A1419ContagemResultadoNotificacao_DestEmail[0];
            n1419ContagemResultadoNotificacao_DestEmail = BC003O45_n1419ContagemResultadoNotificacao_DestEmail[0];
            A1414ContagemResultadoNotificacao_DestCod = BC003O45_A1414ContagemResultadoNotificacao_DestCod[0];
            A1426ContagemResultadoNotificacao_DestPesCod = BC003O45_A1426ContagemResultadoNotificacao_DestPesCod[0];
            n1426ContagemResultadoNotificacao_DestPesCod = BC003O45_n1426ContagemResultadoNotificacao_DestPesCod[0];
         }
         Gx_mode = sMode168;
      }

      protected void ScanKeyEnd3O168( )
      {
         pr_default.close(35);
      }

      protected void AfterConfirm3O168( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3O168( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3O168( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3O168( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3O168( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3O168( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3O168( )
      {
      }

      protected void ZM3O169( short GX_JID )
      {
         if ( ( GX_JID == 17 ) || ( GX_JID == 0 ) )
         {
            Z1416ContagemResultadoNotificacao_DataHora = A1416ContagemResultadoNotificacao_DataHora;
            Z1966ContagemResultadoNotificacao_AreaTrabalhoCod = A1966ContagemResultadoNotificacao_AreaTrabalhoCod;
            Z1413ContagemResultadoNotificacao_UsuCod = A1413ContagemResultadoNotificacao_UsuCod;
            Z1427ContagemResultadoNotificacao_UsuPesCod = A1427ContagemResultadoNotificacao_UsuPesCod;
            Z1422ContagemResultadoNotificacao_UsuNom = A1422ContagemResultadoNotificacao_UsuNom;
            Z1418ContagemResultadoNotificacao_CorpoEmail = A1418ContagemResultadoNotificacao_CorpoEmail;
            Z1420ContagemResultadoNotificacao_Midia = A1420ContagemResultadoNotificacao_Midia;
            Z1956ContagemResultadoNotificacao_Host = A1956ContagemResultadoNotificacao_Host;
            Z1957ContagemResultadoNotificacao_User = A1957ContagemResultadoNotificacao_User;
            Z1958ContagemResultadoNotificacao_Port = A1958ContagemResultadoNotificacao_Port;
            Z1959ContagemResultadoNotificacao_Aut = A1959ContagemResultadoNotificacao_Aut;
            Z1960ContagemResultadoNotificacao_Sec = A1960ContagemResultadoNotificacao_Sec;
            Z1961ContagemResultadoNotificacao_Logged = A1961ContagemResultadoNotificacao_Logged;
            Z1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
            Z1963ContagemResultadoNotificacao_Demandas = A1963ContagemResultadoNotificacao_Demandas;
            Z1965ContagemResultadoNotificacao_ReenvioCod = A1965ContagemResultadoNotificacao_ReenvioCod;
            Z1414ContagemResultadoNotificacao_DestCod = A1414ContagemResultadoNotificacao_DestCod;
            Z1426ContagemResultadoNotificacao_DestPesCod = A1426ContagemResultadoNotificacao_DestPesCod;
            Z1421ContagemResultadoNotificacao_DestNome = A1421ContagemResultadoNotificacao_DestNome;
            Z1419ContagemResultadoNotificacao_DestEmail = A1419ContagemResultadoNotificacao_DestEmail;
         }
         if ( ( GX_JID == 18 ) || ( GX_JID == 0 ) )
         {
            Z1553ContagemResultado_CntSrvCod = A1553ContagemResultado_CntSrvCod;
            Z457ContagemResultado_Demanda = A457ContagemResultado_Demanda;
            Z493ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            Z1227ContagemResultado_PrazoInicialDias = A1227ContagemResultado_PrazoInicialDias;
            Z1416ContagemResultadoNotificacao_DataHora = A1416ContagemResultadoNotificacao_DataHora;
            Z1966ContagemResultadoNotificacao_AreaTrabalhoCod = A1966ContagemResultadoNotificacao_AreaTrabalhoCod;
            Z1413ContagemResultadoNotificacao_UsuCod = A1413ContagemResultadoNotificacao_UsuCod;
            Z1427ContagemResultadoNotificacao_UsuPesCod = A1427ContagemResultadoNotificacao_UsuPesCod;
            Z1422ContagemResultadoNotificacao_UsuNom = A1422ContagemResultadoNotificacao_UsuNom;
            Z1418ContagemResultadoNotificacao_CorpoEmail = A1418ContagemResultadoNotificacao_CorpoEmail;
            Z1420ContagemResultadoNotificacao_Midia = A1420ContagemResultadoNotificacao_Midia;
            Z1956ContagemResultadoNotificacao_Host = A1956ContagemResultadoNotificacao_Host;
            Z1957ContagemResultadoNotificacao_User = A1957ContagemResultadoNotificacao_User;
            Z1958ContagemResultadoNotificacao_Port = A1958ContagemResultadoNotificacao_Port;
            Z1959ContagemResultadoNotificacao_Aut = A1959ContagemResultadoNotificacao_Aut;
            Z1960ContagemResultadoNotificacao_Sec = A1960ContagemResultadoNotificacao_Sec;
            Z1961ContagemResultadoNotificacao_Logged = A1961ContagemResultadoNotificacao_Logged;
            Z1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
            Z1963ContagemResultadoNotificacao_Demandas = A1963ContagemResultadoNotificacao_Demandas;
            Z1965ContagemResultadoNotificacao_ReenvioCod = A1965ContagemResultadoNotificacao_ReenvioCod;
            Z1414ContagemResultadoNotificacao_DestCod = A1414ContagemResultadoNotificacao_DestCod;
            Z1426ContagemResultadoNotificacao_DestPesCod = A1426ContagemResultadoNotificacao_DestPesCod;
            Z1421ContagemResultadoNotificacao_DestNome = A1421ContagemResultadoNotificacao_DestNome;
            Z1419ContagemResultadoNotificacao_DestEmail = A1419ContagemResultadoNotificacao_DestEmail;
         }
         if ( ( GX_JID == 19 ) || ( GX_JID == 0 ) )
         {
            Z601ContagemResultado_Servico = A601ContagemResultado_Servico;
            Z1416ContagemResultadoNotificacao_DataHora = A1416ContagemResultadoNotificacao_DataHora;
            Z1966ContagemResultadoNotificacao_AreaTrabalhoCod = A1966ContagemResultadoNotificacao_AreaTrabalhoCod;
            Z1413ContagemResultadoNotificacao_UsuCod = A1413ContagemResultadoNotificacao_UsuCod;
            Z1427ContagemResultadoNotificacao_UsuPesCod = A1427ContagemResultadoNotificacao_UsuPesCod;
            Z1422ContagemResultadoNotificacao_UsuNom = A1422ContagemResultadoNotificacao_UsuNom;
            Z1418ContagemResultadoNotificacao_CorpoEmail = A1418ContagemResultadoNotificacao_CorpoEmail;
            Z1420ContagemResultadoNotificacao_Midia = A1420ContagemResultadoNotificacao_Midia;
            Z1956ContagemResultadoNotificacao_Host = A1956ContagemResultadoNotificacao_Host;
            Z1957ContagemResultadoNotificacao_User = A1957ContagemResultadoNotificacao_User;
            Z1958ContagemResultadoNotificacao_Port = A1958ContagemResultadoNotificacao_Port;
            Z1959ContagemResultadoNotificacao_Aut = A1959ContagemResultadoNotificacao_Aut;
            Z1960ContagemResultadoNotificacao_Sec = A1960ContagemResultadoNotificacao_Sec;
            Z1961ContagemResultadoNotificacao_Logged = A1961ContagemResultadoNotificacao_Logged;
            Z1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
            Z1963ContagemResultadoNotificacao_Demandas = A1963ContagemResultadoNotificacao_Demandas;
            Z1965ContagemResultadoNotificacao_ReenvioCod = A1965ContagemResultadoNotificacao_ReenvioCod;
            Z1414ContagemResultadoNotificacao_DestCod = A1414ContagemResultadoNotificacao_DestCod;
            Z1426ContagemResultadoNotificacao_DestPesCod = A1426ContagemResultadoNotificacao_DestPesCod;
            Z1421ContagemResultadoNotificacao_DestNome = A1421ContagemResultadoNotificacao_DestNome;
            Z1419ContagemResultadoNotificacao_DestEmail = A1419ContagemResultadoNotificacao_DestEmail;
         }
         if ( ( GX_JID == 20 ) || ( GX_JID == 0 ) )
         {
            Z801ContagemResultado_ServicoSigla = A801ContagemResultado_ServicoSigla;
            Z1416ContagemResultadoNotificacao_DataHora = A1416ContagemResultadoNotificacao_DataHora;
            Z1966ContagemResultadoNotificacao_AreaTrabalhoCod = A1966ContagemResultadoNotificacao_AreaTrabalhoCod;
            Z1413ContagemResultadoNotificacao_UsuCod = A1413ContagemResultadoNotificacao_UsuCod;
            Z1427ContagemResultadoNotificacao_UsuPesCod = A1427ContagemResultadoNotificacao_UsuPesCod;
            Z1422ContagemResultadoNotificacao_UsuNom = A1422ContagemResultadoNotificacao_UsuNom;
            Z1418ContagemResultadoNotificacao_CorpoEmail = A1418ContagemResultadoNotificacao_CorpoEmail;
            Z1420ContagemResultadoNotificacao_Midia = A1420ContagemResultadoNotificacao_Midia;
            Z1956ContagemResultadoNotificacao_Host = A1956ContagemResultadoNotificacao_Host;
            Z1957ContagemResultadoNotificacao_User = A1957ContagemResultadoNotificacao_User;
            Z1958ContagemResultadoNotificacao_Port = A1958ContagemResultadoNotificacao_Port;
            Z1959ContagemResultadoNotificacao_Aut = A1959ContagemResultadoNotificacao_Aut;
            Z1960ContagemResultadoNotificacao_Sec = A1960ContagemResultadoNotificacao_Sec;
            Z1961ContagemResultadoNotificacao_Logged = A1961ContagemResultadoNotificacao_Logged;
            Z1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
            Z1963ContagemResultadoNotificacao_Demandas = A1963ContagemResultadoNotificacao_Demandas;
            Z1965ContagemResultadoNotificacao_ReenvioCod = A1965ContagemResultadoNotificacao_ReenvioCod;
            Z1414ContagemResultadoNotificacao_DestCod = A1414ContagemResultadoNotificacao_DestCod;
            Z1426ContagemResultadoNotificacao_DestPesCod = A1426ContagemResultadoNotificacao_DestPesCod;
            Z1421ContagemResultadoNotificacao_DestNome = A1421ContagemResultadoNotificacao_DestNome;
            Z1419ContagemResultadoNotificacao_DestEmail = A1419ContagemResultadoNotificacao_DestEmail;
         }
         if ( ( GX_JID == 22 ) || ( GX_JID == 0 ) )
         {
            Z1416ContagemResultadoNotificacao_DataHora = A1416ContagemResultadoNotificacao_DataHora;
            Z1966ContagemResultadoNotificacao_AreaTrabalhoCod = A1966ContagemResultadoNotificacao_AreaTrabalhoCod;
            Z1413ContagemResultadoNotificacao_UsuCod = A1413ContagemResultadoNotificacao_UsuCod;
            Z1427ContagemResultadoNotificacao_UsuPesCod = A1427ContagemResultadoNotificacao_UsuPesCod;
            Z1422ContagemResultadoNotificacao_UsuNom = A1422ContagemResultadoNotificacao_UsuNom;
            Z1418ContagemResultadoNotificacao_CorpoEmail = A1418ContagemResultadoNotificacao_CorpoEmail;
            Z1420ContagemResultadoNotificacao_Midia = A1420ContagemResultadoNotificacao_Midia;
            Z1956ContagemResultadoNotificacao_Host = A1956ContagemResultadoNotificacao_Host;
            Z1957ContagemResultadoNotificacao_User = A1957ContagemResultadoNotificacao_User;
            Z1958ContagemResultadoNotificacao_Port = A1958ContagemResultadoNotificacao_Port;
            Z1959ContagemResultadoNotificacao_Aut = A1959ContagemResultadoNotificacao_Aut;
            Z1960ContagemResultadoNotificacao_Sec = A1960ContagemResultadoNotificacao_Sec;
            Z1961ContagemResultadoNotificacao_Logged = A1961ContagemResultadoNotificacao_Logged;
            Z1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
            Z1963ContagemResultadoNotificacao_Demandas = A1963ContagemResultadoNotificacao_Demandas;
            Z1965ContagemResultadoNotificacao_ReenvioCod = A1965ContagemResultadoNotificacao_ReenvioCod;
            Z1414ContagemResultadoNotificacao_DestCod = A1414ContagemResultadoNotificacao_DestCod;
            Z1426ContagemResultadoNotificacao_DestPesCod = A1426ContagemResultadoNotificacao_DestPesCod;
            Z1421ContagemResultadoNotificacao_DestNome = A1421ContagemResultadoNotificacao_DestNome;
            Z1419ContagemResultadoNotificacao_DestEmail = A1419ContagemResultadoNotificacao_DestEmail;
         }
         if ( GX_JID == -17 )
         {
            Z1412ContagemResultadoNotificacao_Codigo = A1412ContagemResultadoNotificacao_Codigo;
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            Z1553ContagemResultado_CntSrvCod = A1553ContagemResultado_CntSrvCod;
            Z457ContagemResultado_Demanda = A457ContagemResultado_Demanda;
            Z493ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            Z1227ContagemResultado_PrazoInicialDias = A1227ContagemResultado_PrazoInicialDias;
            Z601ContagemResultado_Servico = A601ContagemResultado_Servico;
            Z801ContagemResultado_ServicoSigla = A801ContagemResultado_ServicoSigla;
         }
      }

      protected void standaloneNotModal3O169( )
      {
      }

      protected void standaloneModal3O169( )
      {
      }

      protected void Load3O169( )
      {
         /* Using cursor BC003O46 */
         pr_default.execute(36, new Object[] {A1412ContagemResultadoNotificacao_Codigo, A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(36) != 101) )
         {
            RcdFound169 = 1;
            A1553ContagemResultado_CntSrvCod = BC003O46_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = BC003O46_n1553ContagemResultado_CntSrvCod[0];
            A457ContagemResultado_Demanda = BC003O46_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = BC003O46_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = BC003O46_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = BC003O46_n493ContagemResultado_DemandaFM[0];
            A801ContagemResultado_ServicoSigla = BC003O46_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = BC003O46_n801ContagemResultado_ServicoSigla[0];
            A1227ContagemResultado_PrazoInicialDias = BC003O46_A1227ContagemResultado_PrazoInicialDias[0];
            n1227ContagemResultado_PrazoInicialDias = BC003O46_n1227ContagemResultado_PrazoInicialDias[0];
            A601ContagemResultado_Servico = BC003O46_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = BC003O46_n601ContagemResultado_Servico[0];
            ZM3O169( -17) ;
         }
         pr_default.close(36);
         OnLoadActions3O169( ) ;
      }

      protected void OnLoadActions3O169( )
      {
      }

      protected void CheckExtendedTable3O169( )
      {
         Gx_BScreen = 1;
         standaloneModal3O169( ) ;
         Gx_BScreen = 0;
         /* Using cursor BC003O4 */
         pr_default.execute(2, new Object[] {A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Resultado das Contagens'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CODIGO");
            AnyError = 1;
         }
         A1553ContagemResultado_CntSrvCod = BC003O4_A1553ContagemResultado_CntSrvCod[0];
         n1553ContagemResultado_CntSrvCod = BC003O4_n1553ContagemResultado_CntSrvCod[0];
         A457ContagemResultado_Demanda = BC003O4_A457ContagemResultado_Demanda[0];
         n457ContagemResultado_Demanda = BC003O4_n457ContagemResultado_Demanda[0];
         A493ContagemResultado_DemandaFM = BC003O4_A493ContagemResultado_DemandaFM[0];
         n493ContagemResultado_DemandaFM = BC003O4_n493ContagemResultado_DemandaFM[0];
         A1227ContagemResultado_PrazoInicialDias = BC003O4_A1227ContagemResultado_PrazoInicialDias[0];
         n1227ContagemResultado_PrazoInicialDias = BC003O4_n1227ContagemResultado_PrazoInicialDias[0];
         pr_default.close(2);
         /* Using cursor BC003O5 */
         pr_default.execute(3, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A1553ContagemResultado_CntSrvCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A601ContagemResultado_Servico = BC003O5_A601ContagemResultado_Servico[0];
         n601ContagemResultado_Servico = BC003O5_n601ContagemResultado_Servico[0];
         pr_default.close(3);
         /* Using cursor BC003O6 */
         pr_default.execute(4, new Object[] {n601ContagemResultado_Servico, A601ContagemResultado_Servico});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A601ContagemResultado_Servico) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A801ContagemResultado_ServicoSigla = BC003O6_A801ContagemResultado_ServicoSigla[0];
         n801ContagemResultado_ServicoSigla = BC003O6_n801ContagemResultado_ServicoSigla[0];
         pr_default.close(4);
      }

      protected void CloseExtendedTableCursors3O169( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(4);
      }

      protected void enableDisable3O169( )
      {
      }

      protected void GetKey3O169( )
      {
         /* Using cursor BC003O47 */
         pr_default.execute(37, new Object[] {A1412ContagemResultadoNotificacao_Codigo, A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(37) != 101) )
         {
            RcdFound169 = 1;
         }
         else
         {
            RcdFound169 = 0;
         }
         pr_default.close(37);
      }

      protected void getByPrimaryKey3O169( )
      {
         /* Using cursor BC003O3 */
         pr_default.execute(1, new Object[] {A1412ContagemResultadoNotificacao_Codigo, A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3O169( 17) ;
            RcdFound169 = 1;
            InitializeNonKey3O169( ) ;
            A456ContagemResultado_Codigo = BC003O3_A456ContagemResultado_Codigo[0];
            Z1412ContagemResultadoNotificacao_Codigo = A1412ContagemResultadoNotificacao_Codigo;
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            sMode169 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal3O169( ) ;
            Load3O169( ) ;
            Gx_mode = sMode169;
         }
         else
         {
            RcdFound169 = 0;
            InitializeNonKey3O169( ) ;
            sMode169 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal3O169( ) ;
            Gx_mode = sMode169;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            DisableAttributes3O169( ) ;
         }
         pr_default.close(1);
      }

      protected void CheckOptimisticConcurrency3O169( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC003O2 */
            pr_default.execute(0, new Object[] {A1412ContagemResultadoNotificacao_Codigo, A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoNotificacaoDemanda"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultadoNotificacaoDemanda"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3O169( )
      {
         BeforeValidate3O169( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3O169( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3O169( 0) ;
            CheckOptimisticConcurrency3O169( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3O169( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3O169( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC003O48 */
                     pr_default.execute(38, new Object[] {A1412ContagemResultadoNotificacao_Codigo, A456ContagemResultado_Codigo});
                     pr_default.close(38);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoNotificacaoDemanda") ;
                     if ( (pr_default.getStatus(38) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3O169( ) ;
            }
            EndLevel3O169( ) ;
         }
         CloseExtendedTableCursors3O169( ) ;
      }

      protected void Update3O169( )
      {
         BeforeValidate3O169( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3O169( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3O169( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3O169( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3O169( ) ;
                  if ( AnyError == 0 )
                  {
                     /* No attributes to update on table [ContagemResultadoNotificacaoDemanda] */
                     DeferredUpdate3O169( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey3O169( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3O169( ) ;
         }
         CloseExtendedTableCursors3O169( ) ;
      }

      protected void DeferredUpdate3O169( )
      {
      }

      protected void Delete3O169( )
      {
         Gx_mode = "DLT";
         BeforeValidate3O169( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3O169( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3O169( ) ;
            AfterConfirm3O169( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3O169( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC003O49 */
                  pr_default.execute(39, new Object[] {A1412ContagemResultadoNotificacao_Codigo, A456ContagemResultado_Codigo});
                  pr_default.close(39);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoNotificacaoDemanda") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode169 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel3O169( ) ;
         Gx_mode = sMode169;
      }

      protected void OnDeleteControls3O169( )
      {
         standaloneModal3O169( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC003O50 */
            pr_default.execute(40, new Object[] {A456ContagemResultado_Codigo});
            A1553ContagemResultado_CntSrvCod = BC003O50_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = BC003O50_n1553ContagemResultado_CntSrvCod[0];
            A457ContagemResultado_Demanda = BC003O50_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = BC003O50_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = BC003O50_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = BC003O50_n493ContagemResultado_DemandaFM[0];
            A1227ContagemResultado_PrazoInicialDias = BC003O50_A1227ContagemResultado_PrazoInicialDias[0];
            n1227ContagemResultado_PrazoInicialDias = BC003O50_n1227ContagemResultado_PrazoInicialDias[0];
            pr_default.close(40);
            /* Using cursor BC003O51 */
            pr_default.execute(41, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
            A601ContagemResultado_Servico = BC003O51_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = BC003O51_n601ContagemResultado_Servico[0];
            pr_default.close(41);
            /* Using cursor BC003O52 */
            pr_default.execute(42, new Object[] {n601ContagemResultado_Servico, A601ContagemResultado_Servico});
            A801ContagemResultado_ServicoSigla = BC003O52_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = BC003O52_n801ContagemResultado_ServicoSigla[0];
            pr_default.close(42);
         }
      }

      protected void EndLevel3O169( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart3O169( )
      {
         /* Scan By routine */
         /* Using cursor BC003O53 */
         pr_default.execute(43, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
         RcdFound169 = 0;
         if ( (pr_default.getStatus(43) != 101) )
         {
            RcdFound169 = 1;
            A1553ContagemResultado_CntSrvCod = BC003O53_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = BC003O53_n1553ContagemResultado_CntSrvCod[0];
            A457ContagemResultado_Demanda = BC003O53_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = BC003O53_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = BC003O53_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = BC003O53_n493ContagemResultado_DemandaFM[0];
            A801ContagemResultado_ServicoSigla = BC003O53_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = BC003O53_n801ContagemResultado_ServicoSigla[0];
            A1227ContagemResultado_PrazoInicialDias = BC003O53_A1227ContagemResultado_PrazoInicialDias[0];
            n1227ContagemResultado_PrazoInicialDias = BC003O53_n1227ContagemResultado_PrazoInicialDias[0];
            A456ContagemResultado_Codigo = BC003O53_A456ContagemResultado_Codigo[0];
            A601ContagemResultado_Servico = BC003O53_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = BC003O53_n601ContagemResultado_Servico[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext3O169( )
      {
         /* Scan next routine */
         pr_default.readNext(43);
         RcdFound169 = 0;
         ScanKeyLoad3O169( ) ;
      }

      protected void ScanKeyLoad3O169( )
      {
         sMode169 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(43) != 101) )
         {
            RcdFound169 = 1;
            A1553ContagemResultado_CntSrvCod = BC003O53_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = BC003O53_n1553ContagemResultado_CntSrvCod[0];
            A457ContagemResultado_Demanda = BC003O53_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = BC003O53_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = BC003O53_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = BC003O53_n493ContagemResultado_DemandaFM[0];
            A801ContagemResultado_ServicoSigla = BC003O53_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = BC003O53_n801ContagemResultado_ServicoSigla[0];
            A1227ContagemResultado_PrazoInicialDias = BC003O53_A1227ContagemResultado_PrazoInicialDias[0];
            n1227ContagemResultado_PrazoInicialDias = BC003O53_n1227ContagemResultado_PrazoInicialDias[0];
            A456ContagemResultado_Codigo = BC003O53_A456ContagemResultado_Codigo[0];
            A601ContagemResultado_Servico = BC003O53_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = BC003O53_n601ContagemResultado_Servico[0];
         }
         Gx_mode = sMode169;
      }

      protected void ScanKeyEnd3O169( )
      {
         pr_default.close(43);
      }

      protected void AfterConfirm3O169( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3O169( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3O169( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3O169( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3O169( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3O169( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3O169( )
      {
      }

      protected void AddRow3O167( )
      {
         VarsToRow167( bcContagemResultadoNotificacao) ;
      }

      protected void ReadRow3O167( )
      {
         RowToVars167( bcContagemResultadoNotificacao, 1) ;
      }

      protected void AddRow3O168( )
      {
         SdtContagemResultadoNotificacao_Destinatario obj168 ;
         obj168 = new SdtContagemResultadoNotificacao_Destinatario(context);
         VarsToRow168( obj168) ;
         bcContagemResultadoNotificacao.gxTpr_Destinatario.Add(obj168, 0);
         obj168.gxTpr_Mode = "UPD";
         obj168.gxTpr_Modified = 0;
      }

      protected void ReadRow3O168( )
      {
         nGXsfl_168_idx = (short)(nGXsfl_168_idx+1);
         RowToVars168( ((SdtContagemResultadoNotificacao_Destinatario)bcContagemResultadoNotificacao.gxTpr_Destinatario.Item(nGXsfl_168_idx)), 1) ;
      }

      protected void AddRow3O169( )
      {
         SdtContagemResultadoNotificacao_Demanda obj169 ;
         obj169 = new SdtContagemResultadoNotificacao_Demanda(context);
         VarsToRow169( obj169) ;
         bcContagemResultadoNotificacao.gxTpr_Demanda.Add(obj169, 0);
         obj169.gxTpr_Mode = "UPD";
         obj169.gxTpr_Modified = 0;
      }

      protected void ReadRow3O169( )
      {
         nGXsfl_169_idx = (short)(nGXsfl_169_idx+1);
         RowToVars169( ((SdtContagemResultadoNotificacao_Demanda)bcContagemResultadoNotificacao.gxTpr_Demanda.Item(nGXsfl_169_idx)), 1) ;
      }

      protected void InitializeNonKey3O167( )
      {
         A1416ContagemResultadoNotificacao_DataHora = (DateTime)(DateTime.MinValue);
         n1416ContagemResultadoNotificacao_DataHora = false;
         A1966ContagemResultadoNotificacao_AreaTrabalhoCod = 0;
         n1966ContagemResultadoNotificacao_AreaTrabalhoCod = false;
         A1413ContagemResultadoNotificacao_UsuCod = 0;
         A1427ContagemResultadoNotificacao_UsuPesCod = 0;
         n1427ContagemResultadoNotificacao_UsuPesCod = false;
         A1422ContagemResultadoNotificacao_UsuNom = "";
         n1422ContagemResultadoNotificacao_UsuNom = false;
         A1417ContagemResultadoNotificacao_Assunto = "";
         n1417ContagemResultadoNotificacao_Assunto = false;
         A1418ContagemResultadoNotificacao_CorpoEmail = "";
         n1418ContagemResultadoNotificacao_CorpoEmail = false;
         A1420ContagemResultadoNotificacao_Midia = "";
         n1420ContagemResultadoNotificacao_Midia = false;
         A1415ContagemResultadoNotificacao_Observacao = "";
         n1415ContagemResultadoNotificacao_Observacao = false;
         A1956ContagemResultadoNotificacao_Host = "";
         n1956ContagemResultadoNotificacao_Host = false;
         A1957ContagemResultadoNotificacao_User = "";
         n1957ContagemResultadoNotificacao_User = false;
         A1958ContagemResultadoNotificacao_Port = 0;
         n1958ContagemResultadoNotificacao_Port = false;
         A1959ContagemResultadoNotificacao_Aut = false;
         n1959ContagemResultadoNotificacao_Aut = false;
         A1960ContagemResultadoNotificacao_Sec = 0;
         n1960ContagemResultadoNotificacao_Sec = false;
         A1961ContagemResultadoNotificacao_Logged = 0;
         n1961ContagemResultadoNotificacao_Logged = false;
         A1962ContagemResultadoNotificacao_Destinatarios = 0;
         n1962ContagemResultadoNotificacao_Destinatarios = false;
         A1963ContagemResultadoNotificacao_Demandas = 0;
         n1963ContagemResultadoNotificacao_Demandas = false;
         A1965ContagemResultadoNotificacao_ReenvioCod = 0;
         n1965ContagemResultadoNotificacao_ReenvioCod = false;
         O1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
         n1962ContagemResultadoNotificacao_Destinatarios = false;
         Z1416ContagemResultadoNotificacao_DataHora = (DateTime)(DateTime.MinValue);
         Z1418ContagemResultadoNotificacao_CorpoEmail = "";
         Z1420ContagemResultadoNotificacao_Midia = "";
         Z1956ContagemResultadoNotificacao_Host = "";
         Z1957ContagemResultadoNotificacao_User = "";
         Z1958ContagemResultadoNotificacao_Port = 0;
         Z1959ContagemResultadoNotificacao_Aut = false;
         Z1960ContagemResultadoNotificacao_Sec = 0;
         Z1961ContagemResultadoNotificacao_Logged = 0;
         Z1413ContagemResultadoNotificacao_UsuCod = 0;
         Z1966ContagemResultadoNotificacao_AreaTrabalhoCod = 0;
         Z1965ContagemResultadoNotificacao_ReenvioCod = 0;
      }

      protected void InitAll3O167( )
      {
         A1412ContagemResultadoNotificacao_Codigo = 0;
         InitializeNonKey3O167( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void InitializeNonKey3O168( )
      {
         A1426ContagemResultadoNotificacao_DestPesCod = 0;
         n1426ContagemResultadoNotificacao_DestPesCod = false;
         A1421ContagemResultadoNotificacao_DestNome = "";
         n1421ContagemResultadoNotificacao_DestNome = false;
         A1419ContagemResultadoNotificacao_DestEmail = "";
         n1419ContagemResultadoNotificacao_DestEmail = false;
         Z1419ContagemResultadoNotificacao_DestEmail = "";
      }

      protected void InitAll3O168( )
      {
         A1414ContagemResultadoNotificacao_DestCod = 0;
         InitializeNonKey3O168( ) ;
      }

      protected void StandaloneModalInsert3O168( )
      {
      }

      protected void InitializeNonKey3O169( )
      {
         A1553ContagemResultado_CntSrvCod = 0;
         n1553ContagemResultado_CntSrvCod = false;
         A457ContagemResultado_Demanda = "";
         n457ContagemResultado_Demanda = false;
         A493ContagemResultado_DemandaFM = "";
         n493ContagemResultado_DemandaFM = false;
         A601ContagemResultado_Servico = 0;
         n601ContagemResultado_Servico = false;
         A801ContagemResultado_ServicoSigla = "";
         n801ContagemResultado_ServicoSigla = false;
         A1227ContagemResultado_PrazoInicialDias = 0;
         n1227ContagemResultado_PrazoInicialDias = false;
      }

      protected void InitAll3O169( )
      {
         A456ContagemResultado_Codigo = 0;
         InitializeNonKey3O169( ) ;
      }

      protected void StandaloneModalInsert3O169( )
      {
      }

      public void VarsToRow167( SdtContagemResultadoNotificacao obj167 )
      {
         obj167.gxTpr_Mode = Gx_mode;
         obj167.gxTpr_Contagemresultadonotificacao_datahora = A1416ContagemResultadoNotificacao_DataHora;
         obj167.gxTpr_Contagemresultadonotificacao_areatrabalhocod = A1966ContagemResultadoNotificacao_AreaTrabalhoCod;
         obj167.gxTpr_Contagemresultadonotificacao_usucod = A1413ContagemResultadoNotificacao_UsuCod;
         obj167.gxTpr_Contagemresultadonotificacao_usupescod = A1427ContagemResultadoNotificacao_UsuPesCod;
         obj167.gxTpr_Contagemresultadonotificacao_usunom = A1422ContagemResultadoNotificacao_UsuNom;
         obj167.gxTpr_Contagemresultadonotificacao_assunto = A1417ContagemResultadoNotificacao_Assunto;
         obj167.gxTpr_Contagemresultadonotificacao_corpoemail = A1418ContagemResultadoNotificacao_CorpoEmail;
         obj167.gxTpr_Contagemresultadonotificacao_midia = A1420ContagemResultadoNotificacao_Midia;
         obj167.gxTpr_Contagemresultadonotificacao_observacao = A1415ContagemResultadoNotificacao_Observacao;
         obj167.gxTpr_Contagemresultadonotificacao_host = A1956ContagemResultadoNotificacao_Host;
         obj167.gxTpr_Contagemresultadonotificacao_user = A1957ContagemResultadoNotificacao_User;
         obj167.gxTpr_Contagemresultadonotificacao_port = A1958ContagemResultadoNotificacao_Port;
         obj167.gxTpr_Contagemresultadonotificacao_aut = A1959ContagemResultadoNotificacao_Aut;
         obj167.gxTpr_Contagemresultadonotificacao_sec = A1960ContagemResultadoNotificacao_Sec;
         obj167.gxTpr_Contagemresultadonotificacao_logged = A1961ContagemResultadoNotificacao_Logged;
         obj167.gxTpr_Contagemresultadonotificacao_destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
         obj167.gxTpr_Contagemresultadonotificacao_demandas = A1963ContagemResultadoNotificacao_Demandas;
         obj167.gxTpr_Contagemresultadonotificacao_reenviocod = A1965ContagemResultadoNotificacao_ReenvioCod;
         obj167.gxTpr_Contagemresultadonotificacao_codigo = A1412ContagemResultadoNotificacao_Codigo;
         obj167.gxTpr_Contagemresultadonotificacao_codigo_Z = Z1412ContagemResultadoNotificacao_Codigo;
         obj167.gxTpr_Contagemresultadonotificacao_datahora_Z = Z1416ContagemResultadoNotificacao_DataHora;
         obj167.gxTpr_Contagemresultadonotificacao_areatrabalhocod_Z = Z1966ContagemResultadoNotificacao_AreaTrabalhoCod;
         obj167.gxTpr_Contagemresultadonotificacao_usucod_Z = Z1413ContagemResultadoNotificacao_UsuCod;
         obj167.gxTpr_Contagemresultadonotificacao_usupescod_Z = Z1427ContagemResultadoNotificacao_UsuPesCod;
         obj167.gxTpr_Contagemresultadonotificacao_usunom_Z = Z1422ContagemResultadoNotificacao_UsuNom;
         obj167.gxTpr_Contagemresultadonotificacao_corpoemail_Z = Z1418ContagemResultadoNotificacao_CorpoEmail;
         obj167.gxTpr_Contagemresultadonotificacao_midia_Z = Z1420ContagemResultadoNotificacao_Midia;
         obj167.gxTpr_Contagemresultadonotificacao_host_Z = Z1956ContagemResultadoNotificacao_Host;
         obj167.gxTpr_Contagemresultadonotificacao_user_Z = Z1957ContagemResultadoNotificacao_User;
         obj167.gxTpr_Contagemresultadonotificacao_port_Z = Z1958ContagemResultadoNotificacao_Port;
         obj167.gxTpr_Contagemresultadonotificacao_aut_Z = Z1959ContagemResultadoNotificacao_Aut;
         obj167.gxTpr_Contagemresultadonotificacao_sec_Z = Z1960ContagemResultadoNotificacao_Sec;
         obj167.gxTpr_Contagemresultadonotificacao_logged_Z = Z1961ContagemResultadoNotificacao_Logged;
         obj167.gxTpr_Contagemresultadonotificacao_destinatarios_Z = Z1962ContagemResultadoNotificacao_Destinatarios;
         obj167.gxTpr_Contagemresultadonotificacao_demandas_Z = Z1963ContagemResultadoNotificacao_Demandas;
         obj167.gxTpr_Contagemresultadonotificacao_reenviocod_Z = Z1965ContagemResultadoNotificacao_ReenvioCod;
         obj167.gxTpr_Contagemresultadonotificacao_datahora_N = (short)(Convert.ToInt16(n1416ContagemResultadoNotificacao_DataHora));
         obj167.gxTpr_Contagemresultadonotificacao_areatrabalhocod_N = (short)(Convert.ToInt16(n1966ContagemResultadoNotificacao_AreaTrabalhoCod));
         obj167.gxTpr_Contagemresultadonotificacao_usupescod_N = (short)(Convert.ToInt16(n1427ContagemResultadoNotificacao_UsuPesCod));
         obj167.gxTpr_Contagemresultadonotificacao_usunom_N = (short)(Convert.ToInt16(n1422ContagemResultadoNotificacao_UsuNom));
         obj167.gxTpr_Contagemresultadonotificacao_assunto_N = (short)(Convert.ToInt16(n1417ContagemResultadoNotificacao_Assunto));
         obj167.gxTpr_Contagemresultadonotificacao_corpoemail_N = (short)(Convert.ToInt16(n1418ContagemResultadoNotificacao_CorpoEmail));
         obj167.gxTpr_Contagemresultadonotificacao_midia_N = (short)(Convert.ToInt16(n1420ContagemResultadoNotificacao_Midia));
         obj167.gxTpr_Contagemresultadonotificacao_observacao_N = (short)(Convert.ToInt16(n1415ContagemResultadoNotificacao_Observacao));
         obj167.gxTpr_Contagemresultadonotificacao_host_N = (short)(Convert.ToInt16(n1956ContagemResultadoNotificacao_Host));
         obj167.gxTpr_Contagemresultadonotificacao_user_N = (short)(Convert.ToInt16(n1957ContagemResultadoNotificacao_User));
         obj167.gxTpr_Contagemresultadonotificacao_port_N = (short)(Convert.ToInt16(n1958ContagemResultadoNotificacao_Port));
         obj167.gxTpr_Contagemresultadonotificacao_aut_N = (short)(Convert.ToInt16(n1959ContagemResultadoNotificacao_Aut));
         obj167.gxTpr_Contagemresultadonotificacao_sec_N = (short)(Convert.ToInt16(n1960ContagemResultadoNotificacao_Sec));
         obj167.gxTpr_Contagemresultadonotificacao_logged_N = (short)(Convert.ToInt16(n1961ContagemResultadoNotificacao_Logged));
         obj167.gxTpr_Contagemresultadonotificacao_destinatarios_N = (short)(Convert.ToInt16(n1962ContagemResultadoNotificacao_Destinatarios));
         obj167.gxTpr_Contagemresultadonotificacao_demandas_N = (short)(Convert.ToInt16(n1963ContagemResultadoNotificacao_Demandas));
         obj167.gxTpr_Contagemresultadonotificacao_reenviocod_N = (short)(Convert.ToInt16(n1965ContagemResultadoNotificacao_ReenvioCod));
         obj167.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow167( SdtContagemResultadoNotificacao obj167 )
      {
         obj167.gxTpr_Contagemresultadonotificacao_codigo = A1412ContagemResultadoNotificacao_Codigo;
         return  ;
      }

      public void RowToVars167( SdtContagemResultadoNotificacao obj167 ,
                                int forceLoad )
      {
         Gx_mode = obj167.gxTpr_Mode;
         A1416ContagemResultadoNotificacao_DataHora = obj167.gxTpr_Contagemresultadonotificacao_datahora;
         n1416ContagemResultadoNotificacao_DataHora = false;
         A1966ContagemResultadoNotificacao_AreaTrabalhoCod = obj167.gxTpr_Contagemresultadonotificacao_areatrabalhocod;
         n1966ContagemResultadoNotificacao_AreaTrabalhoCod = false;
         A1413ContagemResultadoNotificacao_UsuCod = obj167.gxTpr_Contagemresultadonotificacao_usucod;
         A1427ContagemResultadoNotificacao_UsuPesCod = obj167.gxTpr_Contagemresultadonotificacao_usupescod;
         n1427ContagemResultadoNotificacao_UsuPesCod = false;
         A1422ContagemResultadoNotificacao_UsuNom = obj167.gxTpr_Contagemresultadonotificacao_usunom;
         n1422ContagemResultadoNotificacao_UsuNom = false;
         A1417ContagemResultadoNotificacao_Assunto = obj167.gxTpr_Contagemresultadonotificacao_assunto;
         n1417ContagemResultadoNotificacao_Assunto = false;
         A1418ContagemResultadoNotificacao_CorpoEmail = obj167.gxTpr_Contagemresultadonotificacao_corpoemail;
         n1418ContagemResultadoNotificacao_CorpoEmail = false;
         A1420ContagemResultadoNotificacao_Midia = obj167.gxTpr_Contagemresultadonotificacao_midia;
         n1420ContagemResultadoNotificacao_Midia = false;
         A1415ContagemResultadoNotificacao_Observacao = obj167.gxTpr_Contagemresultadonotificacao_observacao;
         n1415ContagemResultadoNotificacao_Observacao = false;
         A1956ContagemResultadoNotificacao_Host = obj167.gxTpr_Contagemresultadonotificacao_host;
         n1956ContagemResultadoNotificacao_Host = false;
         A1957ContagemResultadoNotificacao_User = obj167.gxTpr_Contagemresultadonotificacao_user;
         n1957ContagemResultadoNotificacao_User = false;
         A1958ContagemResultadoNotificacao_Port = obj167.gxTpr_Contagemresultadonotificacao_port;
         n1958ContagemResultadoNotificacao_Port = false;
         A1959ContagemResultadoNotificacao_Aut = obj167.gxTpr_Contagemresultadonotificacao_aut;
         n1959ContagemResultadoNotificacao_Aut = false;
         A1960ContagemResultadoNotificacao_Sec = obj167.gxTpr_Contagemresultadonotificacao_sec;
         n1960ContagemResultadoNotificacao_Sec = false;
         A1961ContagemResultadoNotificacao_Logged = obj167.gxTpr_Contagemresultadonotificacao_logged;
         n1961ContagemResultadoNotificacao_Logged = false;
         A1962ContagemResultadoNotificacao_Destinatarios = obj167.gxTpr_Contagemresultadonotificacao_destinatarios;
         n1962ContagemResultadoNotificacao_Destinatarios = false;
         A1963ContagemResultadoNotificacao_Demandas = obj167.gxTpr_Contagemresultadonotificacao_demandas;
         n1963ContagemResultadoNotificacao_Demandas = false;
         A1965ContagemResultadoNotificacao_ReenvioCod = obj167.gxTpr_Contagemresultadonotificacao_reenviocod;
         n1965ContagemResultadoNotificacao_ReenvioCod = false;
         A1412ContagemResultadoNotificacao_Codigo = obj167.gxTpr_Contagemresultadonotificacao_codigo;
         Z1412ContagemResultadoNotificacao_Codigo = obj167.gxTpr_Contagemresultadonotificacao_codigo_Z;
         Z1416ContagemResultadoNotificacao_DataHora = obj167.gxTpr_Contagemresultadonotificacao_datahora_Z;
         Z1966ContagemResultadoNotificacao_AreaTrabalhoCod = obj167.gxTpr_Contagemresultadonotificacao_areatrabalhocod_Z;
         Z1413ContagemResultadoNotificacao_UsuCod = obj167.gxTpr_Contagemresultadonotificacao_usucod_Z;
         Z1427ContagemResultadoNotificacao_UsuPesCod = obj167.gxTpr_Contagemresultadonotificacao_usupescod_Z;
         Z1422ContagemResultadoNotificacao_UsuNom = obj167.gxTpr_Contagemresultadonotificacao_usunom_Z;
         Z1418ContagemResultadoNotificacao_CorpoEmail = obj167.gxTpr_Contagemresultadonotificacao_corpoemail_Z;
         Z1420ContagemResultadoNotificacao_Midia = obj167.gxTpr_Contagemresultadonotificacao_midia_Z;
         Z1956ContagemResultadoNotificacao_Host = obj167.gxTpr_Contagemresultadonotificacao_host_Z;
         Z1957ContagemResultadoNotificacao_User = obj167.gxTpr_Contagemresultadonotificacao_user_Z;
         Z1958ContagemResultadoNotificacao_Port = obj167.gxTpr_Contagemresultadonotificacao_port_Z;
         Z1959ContagemResultadoNotificacao_Aut = obj167.gxTpr_Contagemresultadonotificacao_aut_Z;
         Z1960ContagemResultadoNotificacao_Sec = obj167.gxTpr_Contagemresultadonotificacao_sec_Z;
         Z1961ContagemResultadoNotificacao_Logged = obj167.gxTpr_Contagemresultadonotificacao_logged_Z;
         Z1962ContagemResultadoNotificacao_Destinatarios = obj167.gxTpr_Contagemresultadonotificacao_destinatarios_Z;
         O1962ContagemResultadoNotificacao_Destinatarios = obj167.gxTpr_Contagemresultadonotificacao_destinatarios_Z;
         Z1963ContagemResultadoNotificacao_Demandas = obj167.gxTpr_Contagemresultadonotificacao_demandas_Z;
         Z1965ContagemResultadoNotificacao_ReenvioCod = obj167.gxTpr_Contagemresultadonotificacao_reenviocod_Z;
         n1416ContagemResultadoNotificacao_DataHora = (bool)(Convert.ToBoolean(obj167.gxTpr_Contagemresultadonotificacao_datahora_N));
         n1966ContagemResultadoNotificacao_AreaTrabalhoCod = (bool)(Convert.ToBoolean(obj167.gxTpr_Contagemresultadonotificacao_areatrabalhocod_N));
         n1427ContagemResultadoNotificacao_UsuPesCod = (bool)(Convert.ToBoolean(obj167.gxTpr_Contagemresultadonotificacao_usupescod_N));
         n1422ContagemResultadoNotificacao_UsuNom = (bool)(Convert.ToBoolean(obj167.gxTpr_Contagemresultadonotificacao_usunom_N));
         n1417ContagemResultadoNotificacao_Assunto = (bool)(Convert.ToBoolean(obj167.gxTpr_Contagemresultadonotificacao_assunto_N));
         n1418ContagemResultadoNotificacao_CorpoEmail = (bool)(Convert.ToBoolean(obj167.gxTpr_Contagemresultadonotificacao_corpoemail_N));
         n1420ContagemResultadoNotificacao_Midia = (bool)(Convert.ToBoolean(obj167.gxTpr_Contagemresultadonotificacao_midia_N));
         n1415ContagemResultadoNotificacao_Observacao = (bool)(Convert.ToBoolean(obj167.gxTpr_Contagemresultadonotificacao_observacao_N));
         n1956ContagemResultadoNotificacao_Host = (bool)(Convert.ToBoolean(obj167.gxTpr_Contagemresultadonotificacao_host_N));
         n1957ContagemResultadoNotificacao_User = (bool)(Convert.ToBoolean(obj167.gxTpr_Contagemresultadonotificacao_user_N));
         n1958ContagemResultadoNotificacao_Port = (bool)(Convert.ToBoolean(obj167.gxTpr_Contagemresultadonotificacao_port_N));
         n1959ContagemResultadoNotificacao_Aut = (bool)(Convert.ToBoolean(obj167.gxTpr_Contagemresultadonotificacao_aut_N));
         n1960ContagemResultadoNotificacao_Sec = (bool)(Convert.ToBoolean(obj167.gxTpr_Contagemresultadonotificacao_sec_N));
         n1961ContagemResultadoNotificacao_Logged = (bool)(Convert.ToBoolean(obj167.gxTpr_Contagemresultadonotificacao_logged_N));
         n1962ContagemResultadoNotificacao_Destinatarios = (bool)(Convert.ToBoolean(obj167.gxTpr_Contagemresultadonotificacao_destinatarios_N));
         n1963ContagemResultadoNotificacao_Demandas = (bool)(Convert.ToBoolean(obj167.gxTpr_Contagemresultadonotificacao_demandas_N));
         n1965ContagemResultadoNotificacao_ReenvioCod = (bool)(Convert.ToBoolean(obj167.gxTpr_Contagemresultadonotificacao_reenviocod_N));
         Gx_mode = obj167.gxTpr_Mode;
         return  ;
      }

      public void VarsToRow168( SdtContagemResultadoNotificacao_Destinatario obj168 )
      {
         obj168.gxTpr_Mode = Gx_mode;
         obj168.gxTpr_Contagemresultadonotificacao_destpescod = A1426ContagemResultadoNotificacao_DestPesCod;
         obj168.gxTpr_Contagemresultadonotificacao_destnome = A1421ContagemResultadoNotificacao_DestNome;
         obj168.gxTpr_Contagemresultadonotificacao_destemail = A1419ContagemResultadoNotificacao_DestEmail;
         obj168.gxTpr_Contagemresultadonotificacao_destcod = A1414ContagemResultadoNotificacao_DestCod;
         obj168.gxTpr_Contagemresultadonotificacao_destcod_Z = Z1414ContagemResultadoNotificacao_DestCod;
         obj168.gxTpr_Contagemresultadonotificacao_destpescod_Z = Z1426ContagemResultadoNotificacao_DestPesCod;
         obj168.gxTpr_Contagemresultadonotificacao_destnome_Z = Z1421ContagemResultadoNotificacao_DestNome;
         obj168.gxTpr_Contagemresultadonotificacao_destemail_Z = Z1419ContagemResultadoNotificacao_DestEmail;
         obj168.gxTpr_Contagemresultadonotificacao_destpescod_N = (short)(Convert.ToInt16(n1426ContagemResultadoNotificacao_DestPesCod));
         obj168.gxTpr_Contagemresultadonotificacao_destnome_N = (short)(Convert.ToInt16(n1421ContagemResultadoNotificacao_DestNome));
         obj168.gxTpr_Contagemresultadonotificacao_destemail_N = (short)(Convert.ToInt16(n1419ContagemResultadoNotificacao_DestEmail));
         obj168.gxTpr_Modified = nIsMod_168;
         return  ;
      }

      public void KeyVarsToRow168( SdtContagemResultadoNotificacao_Destinatario obj168 )
      {
         obj168.gxTpr_Contagemresultadonotificacao_destcod = A1414ContagemResultadoNotificacao_DestCod;
         return  ;
      }

      public void RowToVars168( SdtContagemResultadoNotificacao_Destinatario obj168 ,
                                int forceLoad )
      {
         Gx_mode = obj168.gxTpr_Mode;
         A1426ContagemResultadoNotificacao_DestPesCod = obj168.gxTpr_Contagemresultadonotificacao_destpescod;
         n1426ContagemResultadoNotificacao_DestPesCod = false;
         A1421ContagemResultadoNotificacao_DestNome = obj168.gxTpr_Contagemresultadonotificacao_destnome;
         n1421ContagemResultadoNotificacao_DestNome = false;
         A1419ContagemResultadoNotificacao_DestEmail = obj168.gxTpr_Contagemresultadonotificacao_destemail;
         n1419ContagemResultadoNotificacao_DestEmail = false;
         A1414ContagemResultadoNotificacao_DestCod = obj168.gxTpr_Contagemresultadonotificacao_destcod;
         Z1414ContagemResultadoNotificacao_DestCod = obj168.gxTpr_Contagemresultadonotificacao_destcod_Z;
         Z1426ContagemResultadoNotificacao_DestPesCod = obj168.gxTpr_Contagemresultadonotificacao_destpescod_Z;
         Z1421ContagemResultadoNotificacao_DestNome = obj168.gxTpr_Contagemresultadonotificacao_destnome_Z;
         Z1419ContagemResultadoNotificacao_DestEmail = obj168.gxTpr_Contagemresultadonotificacao_destemail_Z;
         n1426ContagemResultadoNotificacao_DestPesCod = (bool)(Convert.ToBoolean(obj168.gxTpr_Contagemresultadonotificacao_destpescod_N));
         n1421ContagemResultadoNotificacao_DestNome = (bool)(Convert.ToBoolean(obj168.gxTpr_Contagemresultadonotificacao_destnome_N));
         n1419ContagemResultadoNotificacao_DestEmail = (bool)(Convert.ToBoolean(obj168.gxTpr_Contagemresultadonotificacao_destemail_N));
         nIsMod_168 = obj168.gxTpr_Modified;
         return  ;
      }

      public void VarsToRow169( SdtContagemResultadoNotificacao_Demanda obj169 )
      {
         obj169.gxTpr_Mode = Gx_mode;
         obj169.gxTpr_Contagemresultado_demanda = A457ContagemResultado_Demanda;
         obj169.gxTpr_Contagemresultado_demandafm = A493ContagemResultado_DemandaFM;
         obj169.gxTpr_Contagemresultado_servico = A601ContagemResultado_Servico;
         obj169.gxTpr_Contagemresultado_servicosigla = A801ContagemResultado_ServicoSigla;
         obj169.gxTpr_Contagemresultado_prazoinicialdias = A1227ContagemResultado_PrazoInicialDias;
         obj169.gxTpr_Contagemresultado_codigo = A456ContagemResultado_Codigo;
         obj169.gxTpr_Contagemresultado_codigo_Z = Z456ContagemResultado_Codigo;
         obj169.gxTpr_Contagemresultado_demanda_Z = Z457ContagemResultado_Demanda;
         obj169.gxTpr_Contagemresultado_demandafm_Z = Z493ContagemResultado_DemandaFM;
         obj169.gxTpr_Contagemresultado_servico_Z = Z601ContagemResultado_Servico;
         obj169.gxTpr_Contagemresultado_servicosigla_Z = Z801ContagemResultado_ServicoSigla;
         obj169.gxTpr_Contagemresultado_prazoinicialdias_Z = Z1227ContagemResultado_PrazoInicialDias;
         obj169.gxTpr_Contagemresultado_demanda_N = (short)(Convert.ToInt16(n457ContagemResultado_Demanda));
         obj169.gxTpr_Contagemresultado_demandafm_N = (short)(Convert.ToInt16(n493ContagemResultado_DemandaFM));
         obj169.gxTpr_Contagemresultado_servico_N = (short)(Convert.ToInt16(n601ContagemResultado_Servico));
         obj169.gxTpr_Contagemresultado_servicosigla_N = (short)(Convert.ToInt16(n801ContagemResultado_ServicoSigla));
         obj169.gxTpr_Contagemresultado_prazoinicialdias_N = (short)(Convert.ToInt16(n1227ContagemResultado_PrazoInicialDias));
         obj169.gxTpr_Modified = nIsMod_169;
         return  ;
      }

      public void KeyVarsToRow169( SdtContagemResultadoNotificacao_Demanda obj169 )
      {
         obj169.gxTpr_Contagemresultado_codigo = A456ContagemResultado_Codigo;
         return  ;
      }

      public void RowToVars169( SdtContagemResultadoNotificacao_Demanda obj169 ,
                                int forceLoad )
      {
         Gx_mode = obj169.gxTpr_Mode;
         A457ContagemResultado_Demanda = obj169.gxTpr_Contagemresultado_demanda;
         n457ContagemResultado_Demanda = false;
         A493ContagemResultado_DemandaFM = obj169.gxTpr_Contagemresultado_demandafm;
         n493ContagemResultado_DemandaFM = false;
         A601ContagemResultado_Servico = obj169.gxTpr_Contagemresultado_servico;
         n601ContagemResultado_Servico = false;
         A801ContagemResultado_ServicoSigla = obj169.gxTpr_Contagemresultado_servicosigla;
         n801ContagemResultado_ServicoSigla = false;
         A1227ContagemResultado_PrazoInicialDias = obj169.gxTpr_Contagemresultado_prazoinicialdias;
         n1227ContagemResultado_PrazoInicialDias = false;
         A456ContagemResultado_Codigo = obj169.gxTpr_Contagemresultado_codigo;
         Z456ContagemResultado_Codigo = obj169.gxTpr_Contagemresultado_codigo_Z;
         Z457ContagemResultado_Demanda = obj169.gxTpr_Contagemresultado_demanda_Z;
         Z493ContagemResultado_DemandaFM = obj169.gxTpr_Contagemresultado_demandafm_Z;
         Z601ContagemResultado_Servico = obj169.gxTpr_Contagemresultado_servico_Z;
         Z801ContagemResultado_ServicoSigla = obj169.gxTpr_Contagemresultado_servicosigla_Z;
         Z1227ContagemResultado_PrazoInicialDias = obj169.gxTpr_Contagemresultado_prazoinicialdias_Z;
         n457ContagemResultado_Demanda = (bool)(Convert.ToBoolean(obj169.gxTpr_Contagemresultado_demanda_N));
         n493ContagemResultado_DemandaFM = (bool)(Convert.ToBoolean(obj169.gxTpr_Contagemresultado_demandafm_N));
         n601ContagemResultado_Servico = (bool)(Convert.ToBoolean(obj169.gxTpr_Contagemresultado_servico_N));
         n801ContagemResultado_ServicoSigla = (bool)(Convert.ToBoolean(obj169.gxTpr_Contagemresultado_servicosigla_N));
         n1227ContagemResultado_PrazoInicialDias = (bool)(Convert.ToBoolean(obj169.gxTpr_Contagemresultado_prazoinicialdias_N));
         nIsMod_169 = obj169.gxTpr_Modified;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A1412ContagemResultadoNotificacao_Codigo = (long)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey3O167( ) ;
         ScanKeyStart3O167( ) ;
         if ( RcdFound167 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1412ContagemResultadoNotificacao_Codigo = A1412ContagemResultadoNotificacao_Codigo;
         }
         ZM3O167( -7) ;
         OnLoadActions3O167( ) ;
         AddRow3O167( ) ;
         bcContagemResultadoNotificacao.gxTpr_Destinatario.ClearCollection();
         if ( RcdFound167 == 1 )
         {
            ScanKeyStart3O168( ) ;
            nGXsfl_168_idx = 1;
            while ( RcdFound168 != 0 )
            {
               Z1412ContagemResultadoNotificacao_Codigo = A1412ContagemResultadoNotificacao_Codigo;
               Z1414ContagemResultadoNotificacao_DestCod = A1414ContagemResultadoNotificacao_DestCod;
               ZM3O168( -14) ;
               OnLoadActions3O168( ) ;
               nRcdExists_168 = 1;
               nIsMod_168 = 0;
               AddRow3O168( ) ;
               nGXsfl_168_idx = (short)(nGXsfl_168_idx+1);
               ScanKeyNext3O168( ) ;
            }
            ScanKeyEnd3O168( ) ;
         }
         bcContagemResultadoNotificacao.gxTpr_Demanda.ClearCollection();
         if ( RcdFound167 == 1 )
         {
            ScanKeyStart3O169( ) ;
            nGXsfl_169_idx = 1;
            while ( RcdFound169 != 0 )
            {
               Z1412ContagemResultadoNotificacao_Codigo = A1412ContagemResultadoNotificacao_Codigo;
               Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
               ZM3O169( -17) ;
               OnLoadActions3O169( ) ;
               nRcdExists_169 = 1;
               nIsMod_169 = 0;
               AddRow3O169( ) ;
               nGXsfl_169_idx = (short)(nGXsfl_169_idx+1);
               ScanKeyNext3O169( ) ;
            }
            ScanKeyEnd3O169( ) ;
         }
         ScanKeyEnd3O167( ) ;
         if ( RcdFound167 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars167( bcContagemResultadoNotificacao, 0) ;
         ScanKeyStart3O167( ) ;
         if ( RcdFound167 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1412ContagemResultadoNotificacao_Codigo = A1412ContagemResultadoNotificacao_Codigo;
         }
         ZM3O167( -7) ;
         OnLoadActions3O167( ) ;
         AddRow3O167( ) ;
         bcContagemResultadoNotificacao.gxTpr_Destinatario.ClearCollection();
         if ( RcdFound167 == 1 )
         {
            ScanKeyStart3O168( ) ;
            nGXsfl_168_idx = 1;
            while ( RcdFound168 != 0 )
            {
               Z1412ContagemResultadoNotificacao_Codigo = A1412ContagemResultadoNotificacao_Codigo;
               Z1414ContagemResultadoNotificacao_DestCod = A1414ContagemResultadoNotificacao_DestCod;
               ZM3O168( -14) ;
               OnLoadActions3O168( ) ;
               nRcdExists_168 = 1;
               nIsMod_168 = 0;
               AddRow3O168( ) ;
               nGXsfl_168_idx = (short)(nGXsfl_168_idx+1);
               ScanKeyNext3O168( ) ;
            }
            ScanKeyEnd3O168( ) ;
         }
         bcContagemResultadoNotificacao.gxTpr_Demanda.ClearCollection();
         if ( RcdFound167 == 1 )
         {
            ScanKeyStart3O169( ) ;
            nGXsfl_169_idx = 1;
            while ( RcdFound169 != 0 )
            {
               Z1412ContagemResultadoNotificacao_Codigo = A1412ContagemResultadoNotificacao_Codigo;
               Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
               ZM3O169( -17) ;
               OnLoadActions3O169( ) ;
               nRcdExists_169 = 1;
               nIsMod_169 = 0;
               AddRow3O169( ) ;
               nGXsfl_169_idx = (short)(nGXsfl_169_idx+1);
               ScanKeyNext3O169( ) ;
            }
            ScanKeyEnd3O169( ) ;
         }
         ScanKeyEnd3O167( ) ;
         if ( RcdFound167 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars167( bcContagemResultadoNotificacao, 0) ;
         nKeyPressed = 1;
         GetKey3O167( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            A1962ContagemResultadoNotificacao_Destinatarios = O1962ContagemResultadoNotificacao_Destinatarios;
            n1962ContagemResultadoNotificacao_Destinatarios = false;
            Insert3O167( ) ;
         }
         else
         {
            if ( RcdFound167 == 1 )
            {
               if ( A1412ContagemResultadoNotificacao_Codigo != Z1412ContagemResultadoNotificacao_Codigo )
               {
                  A1412ContagemResultadoNotificacao_Codigo = Z1412ContagemResultadoNotificacao_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  A1962ContagemResultadoNotificacao_Destinatarios = O1962ContagemResultadoNotificacao_Destinatarios;
                  n1962ContagemResultadoNotificacao_Destinatarios = false;
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  A1962ContagemResultadoNotificacao_Destinatarios = O1962ContagemResultadoNotificacao_Destinatarios;
                  n1962ContagemResultadoNotificacao_Destinatarios = false;
                  Update3O167( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A1412ContagemResultadoNotificacao_Codigo != Z1412ContagemResultadoNotificacao_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        A1962ContagemResultadoNotificacao_Destinatarios = O1962ContagemResultadoNotificacao_Destinatarios;
                        n1962ContagemResultadoNotificacao_Destinatarios = false;
                        Insert3O167( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        A1962ContagemResultadoNotificacao_Destinatarios = O1962ContagemResultadoNotificacao_Destinatarios;
                        n1962ContagemResultadoNotificacao_Destinatarios = false;
                        Insert3O167( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow167( bcContagemResultadoNotificacao) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars167( bcContagemResultadoNotificacao, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey3O167( ) ;
         if ( RcdFound167 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A1412ContagemResultadoNotificacao_Codigo != Z1412ContagemResultadoNotificacao_Codigo )
            {
               A1412ContagemResultadoNotificacao_Codigo = Z1412ContagemResultadoNotificacao_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A1412ContagemResultadoNotificacao_Codigo != Z1412ContagemResultadoNotificacao_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(11);
         pr_default.close(7);
         pr_default.close(1);
         pr_default.close(24);
         pr_default.close(25);
         pr_default.close(22);
         pr_default.close(23);
         pr_default.close(33);
         pr_default.close(34);
         pr_default.close(40);
         pr_default.close(41);
         pr_default.close(42);
         context.RollbackDataStores( "ContagemResultadoNotificacao_BC");
         VarsToRow167( bcContagemResultadoNotificacao) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcContagemResultadoNotificacao.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcContagemResultadoNotificacao.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcContagemResultadoNotificacao )
         {
            bcContagemResultadoNotificacao = (SdtContagemResultadoNotificacao)(sdt);
            if ( StringUtil.StrCmp(bcContagemResultadoNotificacao.gxTpr_Mode, "") == 0 )
            {
               bcContagemResultadoNotificacao.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow167( bcContagemResultadoNotificacao) ;
            }
            else
            {
               RowToVars167( bcContagemResultadoNotificacao, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcContagemResultadoNotificacao.gxTpr_Mode, "") == 0 )
            {
               bcContagemResultadoNotificacao.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars167( bcContagemResultadoNotificacao, 1) ;
         return  ;
      }

      public SdtContagemResultadoNotificacao ContagemResultadoNotificacao_BC
      {
         get {
            return bcContagemResultadoNotificacao ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(40);
         pr_default.close(41);
         pr_default.close(42);
         pr_default.close(7);
         pr_default.close(33);
         pr_default.close(34);
         pr_default.close(11);
         pr_default.close(24);
         pr_default.close(25);
         pr_default.close(22);
         pr_default.close(23);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         sMode167 = "";
         BC003O8_A1963ContagemResultadoNotificacao_Demandas = new int[1] ;
         BC003O8_n1963ContagemResultadoNotificacao_Demandas = new bool[] {false} ;
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV16Pgmname = "";
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z1416ContagemResultadoNotificacao_DataHora = (DateTime)(DateTime.MinValue);
         A1416ContagemResultadoNotificacao_DataHora = (DateTime)(DateTime.MinValue);
         Z1418ContagemResultadoNotificacao_CorpoEmail = "";
         A1418ContagemResultadoNotificacao_CorpoEmail = "";
         Z1420ContagemResultadoNotificacao_Midia = "";
         A1420ContagemResultadoNotificacao_Midia = "";
         Z1956ContagemResultadoNotificacao_Host = "";
         A1956ContagemResultadoNotificacao_Host = "";
         Z1957ContagemResultadoNotificacao_User = "";
         A1957ContagemResultadoNotificacao_User = "";
         Z1421ContagemResultadoNotificacao_DestNome = "";
         A1421ContagemResultadoNotificacao_DestNome = "";
         Z1419ContagemResultadoNotificacao_DestEmail = "";
         A1419ContagemResultadoNotificacao_DestEmail = "";
         Z457ContagemResultado_Demanda = "";
         A457ContagemResultado_Demanda = "";
         Z493ContagemResultado_DemandaFM = "";
         A493ContagemResultado_DemandaFM = "";
         Z801ContagemResultado_ServicoSigla = "";
         A801ContagemResultado_ServicoSigla = "";
         Z1422ContagemResultadoNotificacao_UsuNom = "";
         A1422ContagemResultadoNotificacao_UsuNom = "";
         Z1417ContagemResultadoNotificacao_Assunto = "";
         A1417ContagemResultadoNotificacao_Assunto = "";
         Z1415ContagemResultadoNotificacao_Observacao = "";
         A1415ContagemResultadoNotificacao_Observacao = "";
         BC003O23_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         BC003O23_A1416ContagemResultadoNotificacao_DataHora = new DateTime[] {DateTime.MinValue} ;
         BC003O23_n1416ContagemResultadoNotificacao_DataHora = new bool[] {false} ;
         BC003O23_A1422ContagemResultadoNotificacao_UsuNom = new String[] {""} ;
         BC003O23_n1422ContagemResultadoNotificacao_UsuNom = new bool[] {false} ;
         BC003O23_A1417ContagemResultadoNotificacao_Assunto = new String[] {""} ;
         BC003O23_n1417ContagemResultadoNotificacao_Assunto = new bool[] {false} ;
         BC003O23_A1418ContagemResultadoNotificacao_CorpoEmail = new String[] {""} ;
         BC003O23_n1418ContagemResultadoNotificacao_CorpoEmail = new bool[] {false} ;
         BC003O23_A1420ContagemResultadoNotificacao_Midia = new String[] {""} ;
         BC003O23_n1420ContagemResultadoNotificacao_Midia = new bool[] {false} ;
         BC003O23_A1415ContagemResultadoNotificacao_Observacao = new String[] {""} ;
         BC003O23_n1415ContagemResultadoNotificacao_Observacao = new bool[] {false} ;
         BC003O23_A1956ContagemResultadoNotificacao_Host = new String[] {""} ;
         BC003O23_n1956ContagemResultadoNotificacao_Host = new bool[] {false} ;
         BC003O23_A1957ContagemResultadoNotificacao_User = new String[] {""} ;
         BC003O23_n1957ContagemResultadoNotificacao_User = new bool[] {false} ;
         BC003O23_A1958ContagemResultadoNotificacao_Port = new short[1] ;
         BC003O23_n1958ContagemResultadoNotificacao_Port = new bool[] {false} ;
         BC003O23_A1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         BC003O23_n1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         BC003O23_A1960ContagemResultadoNotificacao_Sec = new short[1] ;
         BC003O23_n1960ContagemResultadoNotificacao_Sec = new bool[] {false} ;
         BC003O23_A1961ContagemResultadoNotificacao_Logged = new short[1] ;
         BC003O23_n1961ContagemResultadoNotificacao_Logged = new bool[] {false} ;
         BC003O23_A1413ContagemResultadoNotificacao_UsuCod = new int[1] ;
         BC003O23_A1966ContagemResultadoNotificacao_AreaTrabalhoCod = new int[1] ;
         BC003O23_n1966ContagemResultadoNotificacao_AreaTrabalhoCod = new bool[] {false} ;
         BC003O23_A1965ContagemResultadoNotificacao_ReenvioCod = new long[1] ;
         BC003O23_n1965ContagemResultadoNotificacao_ReenvioCod = new bool[] {false} ;
         BC003O23_A1427ContagemResultadoNotificacao_UsuPesCod = new int[1] ;
         BC003O23_n1427ContagemResultadoNotificacao_UsuPesCod = new bool[] {false} ;
         BC003O23_A1962ContagemResultadoNotificacao_Destinatarios = new int[1] ;
         BC003O23_n1962ContagemResultadoNotificacao_Destinatarios = new bool[] {false} ;
         BC003O23_A1963ContagemResultadoNotificacao_Demandas = new int[1] ;
         BC003O23_n1963ContagemResultadoNotificacao_Demandas = new bool[] {false} ;
         BC003O20_A1962ContagemResultadoNotificacao_Destinatarios = new int[1] ;
         BC003O20_n1962ContagemResultadoNotificacao_Destinatarios = new bool[] {false} ;
         BC003O16_A1966ContagemResultadoNotificacao_AreaTrabalhoCod = new int[1] ;
         BC003O16_n1966ContagemResultadoNotificacao_AreaTrabalhoCod = new bool[] {false} ;
         BC003O15_A1427ContagemResultadoNotificacao_UsuPesCod = new int[1] ;
         BC003O15_n1427ContagemResultadoNotificacao_UsuPesCod = new bool[] {false} ;
         BC003O18_A1422ContagemResultadoNotificacao_UsuNom = new String[] {""} ;
         BC003O18_n1422ContagemResultadoNotificacao_UsuNom = new bool[] {false} ;
         BC003O17_A1965ContagemResultadoNotificacao_ReenvioCod = new long[1] ;
         BC003O17_n1965ContagemResultadoNotificacao_ReenvioCod = new bool[] {false} ;
         BC003O24_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         BC003O14_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         BC003O14_A1416ContagemResultadoNotificacao_DataHora = new DateTime[] {DateTime.MinValue} ;
         BC003O14_n1416ContagemResultadoNotificacao_DataHora = new bool[] {false} ;
         BC003O14_A1417ContagemResultadoNotificacao_Assunto = new String[] {""} ;
         BC003O14_n1417ContagemResultadoNotificacao_Assunto = new bool[] {false} ;
         BC003O14_A1418ContagemResultadoNotificacao_CorpoEmail = new String[] {""} ;
         BC003O14_n1418ContagemResultadoNotificacao_CorpoEmail = new bool[] {false} ;
         BC003O14_A1420ContagemResultadoNotificacao_Midia = new String[] {""} ;
         BC003O14_n1420ContagemResultadoNotificacao_Midia = new bool[] {false} ;
         BC003O14_A1415ContagemResultadoNotificacao_Observacao = new String[] {""} ;
         BC003O14_n1415ContagemResultadoNotificacao_Observacao = new bool[] {false} ;
         BC003O14_A1956ContagemResultadoNotificacao_Host = new String[] {""} ;
         BC003O14_n1956ContagemResultadoNotificacao_Host = new bool[] {false} ;
         BC003O14_A1957ContagemResultadoNotificacao_User = new String[] {""} ;
         BC003O14_n1957ContagemResultadoNotificacao_User = new bool[] {false} ;
         BC003O14_A1958ContagemResultadoNotificacao_Port = new short[1] ;
         BC003O14_n1958ContagemResultadoNotificacao_Port = new bool[] {false} ;
         BC003O14_A1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         BC003O14_n1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         BC003O14_A1960ContagemResultadoNotificacao_Sec = new short[1] ;
         BC003O14_n1960ContagemResultadoNotificacao_Sec = new bool[] {false} ;
         BC003O14_A1961ContagemResultadoNotificacao_Logged = new short[1] ;
         BC003O14_n1961ContagemResultadoNotificacao_Logged = new bool[] {false} ;
         BC003O14_A1413ContagemResultadoNotificacao_UsuCod = new int[1] ;
         BC003O14_A1966ContagemResultadoNotificacao_AreaTrabalhoCod = new int[1] ;
         BC003O14_n1966ContagemResultadoNotificacao_AreaTrabalhoCod = new bool[] {false} ;
         BC003O14_A1965ContagemResultadoNotificacao_ReenvioCod = new long[1] ;
         BC003O14_n1965ContagemResultadoNotificacao_ReenvioCod = new bool[] {false} ;
         BC003O13_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         BC003O13_A1416ContagemResultadoNotificacao_DataHora = new DateTime[] {DateTime.MinValue} ;
         BC003O13_n1416ContagemResultadoNotificacao_DataHora = new bool[] {false} ;
         BC003O13_A1417ContagemResultadoNotificacao_Assunto = new String[] {""} ;
         BC003O13_n1417ContagemResultadoNotificacao_Assunto = new bool[] {false} ;
         BC003O13_A1418ContagemResultadoNotificacao_CorpoEmail = new String[] {""} ;
         BC003O13_n1418ContagemResultadoNotificacao_CorpoEmail = new bool[] {false} ;
         BC003O13_A1420ContagemResultadoNotificacao_Midia = new String[] {""} ;
         BC003O13_n1420ContagemResultadoNotificacao_Midia = new bool[] {false} ;
         BC003O13_A1415ContagemResultadoNotificacao_Observacao = new String[] {""} ;
         BC003O13_n1415ContagemResultadoNotificacao_Observacao = new bool[] {false} ;
         BC003O13_A1956ContagemResultadoNotificacao_Host = new String[] {""} ;
         BC003O13_n1956ContagemResultadoNotificacao_Host = new bool[] {false} ;
         BC003O13_A1957ContagemResultadoNotificacao_User = new String[] {""} ;
         BC003O13_n1957ContagemResultadoNotificacao_User = new bool[] {false} ;
         BC003O13_A1958ContagemResultadoNotificacao_Port = new short[1] ;
         BC003O13_n1958ContagemResultadoNotificacao_Port = new bool[] {false} ;
         BC003O13_A1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         BC003O13_n1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         BC003O13_A1960ContagemResultadoNotificacao_Sec = new short[1] ;
         BC003O13_n1960ContagemResultadoNotificacao_Sec = new bool[] {false} ;
         BC003O13_A1961ContagemResultadoNotificacao_Logged = new short[1] ;
         BC003O13_n1961ContagemResultadoNotificacao_Logged = new bool[] {false} ;
         BC003O13_A1413ContagemResultadoNotificacao_UsuCod = new int[1] ;
         BC003O13_A1966ContagemResultadoNotificacao_AreaTrabalhoCod = new int[1] ;
         BC003O13_n1966ContagemResultadoNotificacao_AreaTrabalhoCod = new bool[] {false} ;
         BC003O13_A1965ContagemResultadoNotificacao_ReenvioCod = new long[1] ;
         BC003O13_n1965ContagemResultadoNotificacao_ReenvioCod = new bool[] {false} ;
         BC003O25_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         BC003O29_A1962ContagemResultadoNotificacao_Destinatarios = new int[1] ;
         BC003O29_n1962ContagemResultadoNotificacao_Destinatarios = new bool[] {false} ;
         BC003O31_A1963ContagemResultadoNotificacao_Demandas = new int[1] ;
         BC003O31_n1963ContagemResultadoNotificacao_Demandas = new bool[] {false} ;
         BC003O32_A1427ContagemResultadoNotificacao_UsuPesCod = new int[1] ;
         BC003O32_n1427ContagemResultadoNotificacao_UsuPesCod = new bool[] {false} ;
         BC003O33_A1422ContagemResultadoNotificacao_UsuNom = new String[] {""} ;
         BC003O33_n1422ContagemResultadoNotificacao_UsuNom = new bool[] {false} ;
         BC003O34_A1965ContagemResultadoNotificacao_ReenvioCod = new long[1] ;
         BC003O34_n1965ContagemResultadoNotificacao_ReenvioCod = new bool[] {false} ;
         BC003O37_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         BC003O37_A1416ContagemResultadoNotificacao_DataHora = new DateTime[] {DateTime.MinValue} ;
         BC003O37_n1416ContagemResultadoNotificacao_DataHora = new bool[] {false} ;
         BC003O37_A1422ContagemResultadoNotificacao_UsuNom = new String[] {""} ;
         BC003O37_n1422ContagemResultadoNotificacao_UsuNom = new bool[] {false} ;
         BC003O37_A1417ContagemResultadoNotificacao_Assunto = new String[] {""} ;
         BC003O37_n1417ContagemResultadoNotificacao_Assunto = new bool[] {false} ;
         BC003O37_A1418ContagemResultadoNotificacao_CorpoEmail = new String[] {""} ;
         BC003O37_n1418ContagemResultadoNotificacao_CorpoEmail = new bool[] {false} ;
         BC003O37_A1420ContagemResultadoNotificacao_Midia = new String[] {""} ;
         BC003O37_n1420ContagemResultadoNotificacao_Midia = new bool[] {false} ;
         BC003O37_A1415ContagemResultadoNotificacao_Observacao = new String[] {""} ;
         BC003O37_n1415ContagemResultadoNotificacao_Observacao = new bool[] {false} ;
         BC003O37_A1956ContagemResultadoNotificacao_Host = new String[] {""} ;
         BC003O37_n1956ContagemResultadoNotificacao_Host = new bool[] {false} ;
         BC003O37_A1957ContagemResultadoNotificacao_User = new String[] {""} ;
         BC003O37_n1957ContagemResultadoNotificacao_User = new bool[] {false} ;
         BC003O37_A1958ContagemResultadoNotificacao_Port = new short[1] ;
         BC003O37_n1958ContagemResultadoNotificacao_Port = new bool[] {false} ;
         BC003O37_A1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         BC003O37_n1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         BC003O37_A1960ContagemResultadoNotificacao_Sec = new short[1] ;
         BC003O37_n1960ContagemResultadoNotificacao_Sec = new bool[] {false} ;
         BC003O37_A1961ContagemResultadoNotificacao_Logged = new short[1] ;
         BC003O37_n1961ContagemResultadoNotificacao_Logged = new bool[] {false} ;
         BC003O37_A1413ContagemResultadoNotificacao_UsuCod = new int[1] ;
         BC003O37_A1966ContagemResultadoNotificacao_AreaTrabalhoCod = new int[1] ;
         BC003O37_n1966ContagemResultadoNotificacao_AreaTrabalhoCod = new bool[] {false} ;
         BC003O37_A1965ContagemResultadoNotificacao_ReenvioCod = new long[1] ;
         BC003O37_n1965ContagemResultadoNotificacao_ReenvioCod = new bool[] {false} ;
         BC003O37_A1427ContagemResultadoNotificacao_UsuPesCod = new int[1] ;
         BC003O37_n1427ContagemResultadoNotificacao_UsuPesCod = new bool[] {false} ;
         BC003O37_A1962ContagemResultadoNotificacao_Destinatarios = new int[1] ;
         BC003O37_n1962ContagemResultadoNotificacao_Destinatarios = new bool[] {false} ;
         BC003O37_A1963ContagemResultadoNotificacao_Demandas = new int[1] ;
         BC003O37_n1963ContagemResultadoNotificacao_Demandas = new bool[] {false} ;
         BC003O38_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         BC003O38_A1421ContagemResultadoNotificacao_DestNome = new String[] {""} ;
         BC003O38_n1421ContagemResultadoNotificacao_DestNome = new bool[] {false} ;
         BC003O38_A1419ContagemResultadoNotificacao_DestEmail = new String[] {""} ;
         BC003O38_n1419ContagemResultadoNotificacao_DestEmail = new bool[] {false} ;
         BC003O38_A1414ContagemResultadoNotificacao_DestCod = new int[1] ;
         BC003O38_A1426ContagemResultadoNotificacao_DestPesCod = new int[1] ;
         BC003O38_n1426ContagemResultadoNotificacao_DestPesCod = new bool[] {false} ;
         BC003O11_A1426ContagemResultadoNotificacao_DestPesCod = new int[1] ;
         BC003O11_n1426ContagemResultadoNotificacao_DestPesCod = new bool[] {false} ;
         BC003O12_A1421ContagemResultadoNotificacao_DestNome = new String[] {""} ;
         BC003O12_n1421ContagemResultadoNotificacao_DestNome = new bool[] {false} ;
         BC003O39_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         BC003O39_A1414ContagemResultadoNotificacao_DestCod = new int[1] ;
         BC003O10_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         BC003O10_A1419ContagemResultadoNotificacao_DestEmail = new String[] {""} ;
         BC003O10_n1419ContagemResultadoNotificacao_DestEmail = new bool[] {false} ;
         BC003O10_A1414ContagemResultadoNotificacao_DestCod = new int[1] ;
         sMode168 = "";
         BC003O9_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         BC003O9_A1419ContagemResultadoNotificacao_DestEmail = new String[] {""} ;
         BC003O9_n1419ContagemResultadoNotificacao_DestEmail = new bool[] {false} ;
         BC003O9_A1414ContagemResultadoNotificacao_DestCod = new int[1] ;
         BC003O43_A1426ContagemResultadoNotificacao_DestPesCod = new int[1] ;
         BC003O43_n1426ContagemResultadoNotificacao_DestPesCod = new bool[] {false} ;
         BC003O44_A1421ContagemResultadoNotificacao_DestNome = new String[] {""} ;
         BC003O44_n1421ContagemResultadoNotificacao_DestNome = new bool[] {false} ;
         BC003O45_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         BC003O45_A1421ContagemResultadoNotificacao_DestNome = new String[] {""} ;
         BC003O45_n1421ContagemResultadoNotificacao_DestNome = new bool[] {false} ;
         BC003O45_A1419ContagemResultadoNotificacao_DestEmail = new String[] {""} ;
         BC003O45_n1419ContagemResultadoNotificacao_DestEmail = new bool[] {false} ;
         BC003O45_A1414ContagemResultadoNotificacao_DestCod = new int[1] ;
         BC003O45_A1426ContagemResultadoNotificacao_DestPesCod = new int[1] ;
         BC003O45_n1426ContagemResultadoNotificacao_DestPesCod = new bool[] {false} ;
         BC003O46_A1553ContagemResultado_CntSrvCod = new int[1] ;
         BC003O46_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         BC003O46_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         BC003O46_A457ContagemResultado_Demanda = new String[] {""} ;
         BC003O46_n457ContagemResultado_Demanda = new bool[] {false} ;
         BC003O46_A493ContagemResultado_DemandaFM = new String[] {""} ;
         BC003O46_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         BC003O46_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         BC003O46_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         BC003O46_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         BC003O46_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         BC003O46_A456ContagemResultado_Codigo = new int[1] ;
         BC003O46_A601ContagemResultado_Servico = new int[1] ;
         BC003O46_n601ContagemResultado_Servico = new bool[] {false} ;
         BC003O4_A1553ContagemResultado_CntSrvCod = new int[1] ;
         BC003O4_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         BC003O4_A457ContagemResultado_Demanda = new String[] {""} ;
         BC003O4_n457ContagemResultado_Demanda = new bool[] {false} ;
         BC003O4_A493ContagemResultado_DemandaFM = new String[] {""} ;
         BC003O4_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         BC003O4_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         BC003O4_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         BC003O5_A601ContagemResultado_Servico = new int[1] ;
         BC003O5_n601ContagemResultado_Servico = new bool[] {false} ;
         BC003O6_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         BC003O6_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         BC003O47_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         BC003O47_A456ContagemResultado_Codigo = new int[1] ;
         BC003O3_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         BC003O3_A456ContagemResultado_Codigo = new int[1] ;
         sMode169 = "";
         BC003O2_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         BC003O2_A456ContagemResultado_Codigo = new int[1] ;
         BC003O50_A1553ContagemResultado_CntSrvCod = new int[1] ;
         BC003O50_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         BC003O50_A457ContagemResultado_Demanda = new String[] {""} ;
         BC003O50_n457ContagemResultado_Demanda = new bool[] {false} ;
         BC003O50_A493ContagemResultado_DemandaFM = new String[] {""} ;
         BC003O50_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         BC003O50_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         BC003O50_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         BC003O51_A601ContagemResultado_Servico = new int[1] ;
         BC003O51_n601ContagemResultado_Servico = new bool[] {false} ;
         BC003O52_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         BC003O52_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         BC003O53_A1553ContagemResultado_CntSrvCod = new int[1] ;
         BC003O53_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         BC003O53_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         BC003O53_A457ContagemResultado_Demanda = new String[] {""} ;
         BC003O53_n457ContagemResultado_Demanda = new bool[] {false} ;
         BC003O53_A493ContagemResultado_DemandaFM = new String[] {""} ;
         BC003O53_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         BC003O53_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         BC003O53_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         BC003O53_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         BC003O53_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         BC003O53_A456ContagemResultado_Codigo = new int[1] ;
         BC003O53_A601ContagemResultado_Servico = new int[1] ;
         BC003O53_n601ContagemResultado_Servico = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadonotificacao_bc__default(),
            new Object[][] {
                new Object[] {
               BC003O2_A1412ContagemResultadoNotificacao_Codigo, BC003O2_A456ContagemResultado_Codigo
               }
               , new Object[] {
               BC003O3_A1412ContagemResultadoNotificacao_Codigo, BC003O3_A456ContagemResultado_Codigo
               }
               , new Object[] {
               BC003O4_A1553ContagemResultado_CntSrvCod, BC003O4_n1553ContagemResultado_CntSrvCod, BC003O4_A457ContagemResultado_Demanda, BC003O4_n457ContagemResultado_Demanda, BC003O4_A493ContagemResultado_DemandaFM, BC003O4_n493ContagemResultado_DemandaFM, BC003O4_A1227ContagemResultado_PrazoInicialDias, BC003O4_n1227ContagemResultado_PrazoInicialDias
               }
               , new Object[] {
               BC003O5_A601ContagemResultado_Servico, BC003O5_n601ContagemResultado_Servico
               }
               , new Object[] {
               BC003O6_A801ContagemResultado_ServicoSigla, BC003O6_n801ContagemResultado_ServicoSigla
               }
               , new Object[] {
               BC003O8_A1963ContagemResultadoNotificacao_Demandas, BC003O8_n1963ContagemResultadoNotificacao_Demandas
               }
               , new Object[] {
               BC003O9_A1412ContagemResultadoNotificacao_Codigo, BC003O9_A1419ContagemResultadoNotificacao_DestEmail, BC003O9_n1419ContagemResultadoNotificacao_DestEmail, BC003O9_A1414ContagemResultadoNotificacao_DestCod
               }
               , new Object[] {
               BC003O10_A1412ContagemResultadoNotificacao_Codigo, BC003O10_A1419ContagemResultadoNotificacao_DestEmail, BC003O10_n1419ContagemResultadoNotificacao_DestEmail, BC003O10_A1414ContagemResultadoNotificacao_DestCod
               }
               , new Object[] {
               BC003O11_A1426ContagemResultadoNotificacao_DestPesCod, BC003O11_n1426ContagemResultadoNotificacao_DestPesCod
               }
               , new Object[] {
               BC003O12_A1421ContagemResultadoNotificacao_DestNome, BC003O12_n1421ContagemResultadoNotificacao_DestNome
               }
               , new Object[] {
               BC003O13_A1412ContagemResultadoNotificacao_Codigo, BC003O13_A1416ContagemResultadoNotificacao_DataHora, BC003O13_n1416ContagemResultadoNotificacao_DataHora, BC003O13_A1417ContagemResultadoNotificacao_Assunto, BC003O13_n1417ContagemResultadoNotificacao_Assunto, BC003O13_A1418ContagemResultadoNotificacao_CorpoEmail, BC003O13_n1418ContagemResultadoNotificacao_CorpoEmail, BC003O13_A1420ContagemResultadoNotificacao_Midia, BC003O13_n1420ContagemResultadoNotificacao_Midia, BC003O13_A1415ContagemResultadoNotificacao_Observacao,
               BC003O13_n1415ContagemResultadoNotificacao_Observacao, BC003O13_A1956ContagemResultadoNotificacao_Host, BC003O13_n1956ContagemResultadoNotificacao_Host, BC003O13_A1957ContagemResultadoNotificacao_User, BC003O13_n1957ContagemResultadoNotificacao_User, BC003O13_A1958ContagemResultadoNotificacao_Port, BC003O13_n1958ContagemResultadoNotificacao_Port, BC003O13_A1959ContagemResultadoNotificacao_Aut, BC003O13_n1959ContagemResultadoNotificacao_Aut, BC003O13_A1960ContagemResultadoNotificacao_Sec,
               BC003O13_n1960ContagemResultadoNotificacao_Sec, BC003O13_A1961ContagemResultadoNotificacao_Logged, BC003O13_n1961ContagemResultadoNotificacao_Logged, BC003O13_A1413ContagemResultadoNotificacao_UsuCod, BC003O13_A1966ContagemResultadoNotificacao_AreaTrabalhoCod, BC003O13_n1966ContagemResultadoNotificacao_AreaTrabalhoCod, BC003O13_A1965ContagemResultadoNotificacao_ReenvioCod, BC003O13_n1965ContagemResultadoNotificacao_ReenvioCod
               }
               , new Object[] {
               BC003O14_A1412ContagemResultadoNotificacao_Codigo, BC003O14_A1416ContagemResultadoNotificacao_DataHora, BC003O14_n1416ContagemResultadoNotificacao_DataHora, BC003O14_A1417ContagemResultadoNotificacao_Assunto, BC003O14_n1417ContagemResultadoNotificacao_Assunto, BC003O14_A1418ContagemResultadoNotificacao_CorpoEmail, BC003O14_n1418ContagemResultadoNotificacao_CorpoEmail, BC003O14_A1420ContagemResultadoNotificacao_Midia, BC003O14_n1420ContagemResultadoNotificacao_Midia, BC003O14_A1415ContagemResultadoNotificacao_Observacao,
               BC003O14_n1415ContagemResultadoNotificacao_Observacao, BC003O14_A1956ContagemResultadoNotificacao_Host, BC003O14_n1956ContagemResultadoNotificacao_Host, BC003O14_A1957ContagemResultadoNotificacao_User, BC003O14_n1957ContagemResultadoNotificacao_User, BC003O14_A1958ContagemResultadoNotificacao_Port, BC003O14_n1958ContagemResultadoNotificacao_Port, BC003O14_A1959ContagemResultadoNotificacao_Aut, BC003O14_n1959ContagemResultadoNotificacao_Aut, BC003O14_A1960ContagemResultadoNotificacao_Sec,
               BC003O14_n1960ContagemResultadoNotificacao_Sec, BC003O14_A1961ContagemResultadoNotificacao_Logged, BC003O14_n1961ContagemResultadoNotificacao_Logged, BC003O14_A1413ContagemResultadoNotificacao_UsuCod, BC003O14_A1966ContagemResultadoNotificacao_AreaTrabalhoCod, BC003O14_n1966ContagemResultadoNotificacao_AreaTrabalhoCod, BC003O14_A1965ContagemResultadoNotificacao_ReenvioCod, BC003O14_n1965ContagemResultadoNotificacao_ReenvioCod
               }
               , new Object[] {
               BC003O15_A1427ContagemResultadoNotificacao_UsuPesCod, BC003O15_n1427ContagemResultadoNotificacao_UsuPesCod
               }
               , new Object[] {
               BC003O16_A1966ContagemResultadoNotificacao_AreaTrabalhoCod
               }
               , new Object[] {
               BC003O17_A1965ContagemResultadoNotificacao_ReenvioCod
               }
               , new Object[] {
               BC003O18_A1422ContagemResultadoNotificacao_UsuNom, BC003O18_n1422ContagemResultadoNotificacao_UsuNom
               }
               , new Object[] {
               BC003O20_A1962ContagemResultadoNotificacao_Destinatarios, BC003O20_n1962ContagemResultadoNotificacao_Destinatarios
               }
               , new Object[] {
               BC003O23_A1412ContagemResultadoNotificacao_Codigo, BC003O23_A1416ContagemResultadoNotificacao_DataHora, BC003O23_n1416ContagemResultadoNotificacao_DataHora, BC003O23_A1422ContagemResultadoNotificacao_UsuNom, BC003O23_n1422ContagemResultadoNotificacao_UsuNom, BC003O23_A1417ContagemResultadoNotificacao_Assunto, BC003O23_n1417ContagemResultadoNotificacao_Assunto, BC003O23_A1418ContagemResultadoNotificacao_CorpoEmail, BC003O23_n1418ContagemResultadoNotificacao_CorpoEmail, BC003O23_A1420ContagemResultadoNotificacao_Midia,
               BC003O23_n1420ContagemResultadoNotificacao_Midia, BC003O23_A1415ContagemResultadoNotificacao_Observacao, BC003O23_n1415ContagemResultadoNotificacao_Observacao, BC003O23_A1956ContagemResultadoNotificacao_Host, BC003O23_n1956ContagemResultadoNotificacao_Host, BC003O23_A1957ContagemResultadoNotificacao_User, BC003O23_n1957ContagemResultadoNotificacao_User, BC003O23_A1958ContagemResultadoNotificacao_Port, BC003O23_n1958ContagemResultadoNotificacao_Port, BC003O23_A1959ContagemResultadoNotificacao_Aut,
               BC003O23_n1959ContagemResultadoNotificacao_Aut, BC003O23_A1960ContagemResultadoNotificacao_Sec, BC003O23_n1960ContagemResultadoNotificacao_Sec, BC003O23_A1961ContagemResultadoNotificacao_Logged, BC003O23_n1961ContagemResultadoNotificacao_Logged, BC003O23_A1413ContagemResultadoNotificacao_UsuCod, BC003O23_A1966ContagemResultadoNotificacao_AreaTrabalhoCod, BC003O23_n1966ContagemResultadoNotificacao_AreaTrabalhoCod, BC003O23_A1965ContagemResultadoNotificacao_ReenvioCod, BC003O23_n1965ContagemResultadoNotificacao_ReenvioCod,
               BC003O23_A1427ContagemResultadoNotificacao_UsuPesCod, BC003O23_n1427ContagemResultadoNotificacao_UsuPesCod, BC003O23_A1962ContagemResultadoNotificacao_Destinatarios, BC003O23_n1962ContagemResultadoNotificacao_Destinatarios, BC003O23_A1963ContagemResultadoNotificacao_Demandas, BC003O23_n1963ContagemResultadoNotificacao_Demandas
               }
               , new Object[] {
               BC003O24_A1412ContagemResultadoNotificacao_Codigo
               }
               , new Object[] {
               BC003O25_A1412ContagemResultadoNotificacao_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC003O29_A1962ContagemResultadoNotificacao_Destinatarios, BC003O29_n1962ContagemResultadoNotificacao_Destinatarios
               }
               , new Object[] {
               BC003O31_A1963ContagemResultadoNotificacao_Demandas, BC003O31_n1963ContagemResultadoNotificacao_Demandas
               }
               , new Object[] {
               BC003O32_A1427ContagemResultadoNotificacao_UsuPesCod, BC003O32_n1427ContagemResultadoNotificacao_UsuPesCod
               }
               , new Object[] {
               BC003O33_A1422ContagemResultadoNotificacao_UsuNom, BC003O33_n1422ContagemResultadoNotificacao_UsuNom
               }
               , new Object[] {
               BC003O34_A1965ContagemResultadoNotificacao_ReenvioCod
               }
               , new Object[] {
               BC003O37_A1412ContagemResultadoNotificacao_Codigo, BC003O37_A1416ContagemResultadoNotificacao_DataHora, BC003O37_n1416ContagemResultadoNotificacao_DataHora, BC003O37_A1422ContagemResultadoNotificacao_UsuNom, BC003O37_n1422ContagemResultadoNotificacao_UsuNom, BC003O37_A1417ContagemResultadoNotificacao_Assunto, BC003O37_n1417ContagemResultadoNotificacao_Assunto, BC003O37_A1418ContagemResultadoNotificacao_CorpoEmail, BC003O37_n1418ContagemResultadoNotificacao_CorpoEmail, BC003O37_A1420ContagemResultadoNotificacao_Midia,
               BC003O37_n1420ContagemResultadoNotificacao_Midia, BC003O37_A1415ContagemResultadoNotificacao_Observacao, BC003O37_n1415ContagemResultadoNotificacao_Observacao, BC003O37_A1956ContagemResultadoNotificacao_Host, BC003O37_n1956ContagemResultadoNotificacao_Host, BC003O37_A1957ContagemResultadoNotificacao_User, BC003O37_n1957ContagemResultadoNotificacao_User, BC003O37_A1958ContagemResultadoNotificacao_Port, BC003O37_n1958ContagemResultadoNotificacao_Port, BC003O37_A1959ContagemResultadoNotificacao_Aut,
               BC003O37_n1959ContagemResultadoNotificacao_Aut, BC003O37_A1960ContagemResultadoNotificacao_Sec, BC003O37_n1960ContagemResultadoNotificacao_Sec, BC003O37_A1961ContagemResultadoNotificacao_Logged, BC003O37_n1961ContagemResultadoNotificacao_Logged, BC003O37_A1413ContagemResultadoNotificacao_UsuCod, BC003O37_A1966ContagemResultadoNotificacao_AreaTrabalhoCod, BC003O37_n1966ContagemResultadoNotificacao_AreaTrabalhoCod, BC003O37_A1965ContagemResultadoNotificacao_ReenvioCod, BC003O37_n1965ContagemResultadoNotificacao_ReenvioCod,
               BC003O37_A1427ContagemResultadoNotificacao_UsuPesCod, BC003O37_n1427ContagemResultadoNotificacao_UsuPesCod, BC003O37_A1962ContagemResultadoNotificacao_Destinatarios, BC003O37_n1962ContagemResultadoNotificacao_Destinatarios, BC003O37_A1963ContagemResultadoNotificacao_Demandas, BC003O37_n1963ContagemResultadoNotificacao_Demandas
               }
               , new Object[] {
               BC003O38_A1412ContagemResultadoNotificacao_Codigo, BC003O38_A1421ContagemResultadoNotificacao_DestNome, BC003O38_n1421ContagemResultadoNotificacao_DestNome, BC003O38_A1419ContagemResultadoNotificacao_DestEmail, BC003O38_n1419ContagemResultadoNotificacao_DestEmail, BC003O38_A1414ContagemResultadoNotificacao_DestCod, BC003O38_A1426ContagemResultadoNotificacao_DestPesCod, BC003O38_n1426ContagemResultadoNotificacao_DestPesCod
               }
               , new Object[] {
               BC003O39_A1412ContagemResultadoNotificacao_Codigo, BC003O39_A1414ContagemResultadoNotificacao_DestCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC003O43_A1426ContagemResultadoNotificacao_DestPesCod, BC003O43_n1426ContagemResultadoNotificacao_DestPesCod
               }
               , new Object[] {
               BC003O44_A1421ContagemResultadoNotificacao_DestNome, BC003O44_n1421ContagemResultadoNotificacao_DestNome
               }
               , new Object[] {
               BC003O45_A1412ContagemResultadoNotificacao_Codigo, BC003O45_A1421ContagemResultadoNotificacao_DestNome, BC003O45_n1421ContagemResultadoNotificacao_DestNome, BC003O45_A1419ContagemResultadoNotificacao_DestEmail, BC003O45_n1419ContagemResultadoNotificacao_DestEmail, BC003O45_A1414ContagemResultadoNotificacao_DestCod, BC003O45_A1426ContagemResultadoNotificacao_DestPesCod, BC003O45_n1426ContagemResultadoNotificacao_DestPesCod
               }
               , new Object[] {
               BC003O46_A1553ContagemResultado_CntSrvCod, BC003O46_n1553ContagemResultado_CntSrvCod, BC003O46_A1412ContagemResultadoNotificacao_Codigo, BC003O46_A457ContagemResultado_Demanda, BC003O46_n457ContagemResultado_Demanda, BC003O46_A493ContagemResultado_DemandaFM, BC003O46_n493ContagemResultado_DemandaFM, BC003O46_A801ContagemResultado_ServicoSigla, BC003O46_n801ContagemResultado_ServicoSigla, BC003O46_A1227ContagemResultado_PrazoInicialDias,
               BC003O46_n1227ContagemResultado_PrazoInicialDias, BC003O46_A456ContagemResultado_Codigo, BC003O46_A601ContagemResultado_Servico, BC003O46_n601ContagemResultado_Servico
               }
               , new Object[] {
               BC003O47_A1412ContagemResultadoNotificacao_Codigo, BC003O47_A456ContagemResultado_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC003O50_A1553ContagemResultado_CntSrvCod, BC003O50_n1553ContagemResultado_CntSrvCod, BC003O50_A457ContagemResultado_Demanda, BC003O50_n457ContagemResultado_Demanda, BC003O50_A493ContagemResultado_DemandaFM, BC003O50_n493ContagemResultado_DemandaFM, BC003O50_A1227ContagemResultado_PrazoInicialDias, BC003O50_n1227ContagemResultado_PrazoInicialDias
               }
               , new Object[] {
               BC003O51_A601ContagemResultado_Servico, BC003O51_n601ContagemResultado_Servico
               }
               , new Object[] {
               BC003O52_A801ContagemResultado_ServicoSigla, BC003O52_n801ContagemResultado_ServicoSigla
               }
               , new Object[] {
               BC003O53_A1553ContagemResultado_CntSrvCod, BC003O53_n1553ContagemResultado_CntSrvCod, BC003O53_A1412ContagemResultadoNotificacao_Codigo, BC003O53_A457ContagemResultado_Demanda, BC003O53_n457ContagemResultado_Demanda, BC003O53_A493ContagemResultado_DemandaFM, BC003O53_n493ContagemResultado_DemandaFM, BC003O53_A801ContagemResultado_ServicoSigla, BC003O53_n801ContagemResultado_ServicoSigla, BC003O53_A1227ContagemResultado_PrazoInicialDias,
               BC003O53_n1227ContagemResultado_PrazoInicialDias, BC003O53_A456ContagemResultado_Codigo, BC003O53_A601ContagemResultado_Servico, BC003O53_n601ContagemResultado_Servico
               }
            }
         );
         AV16Pgmname = "ContagemResultadoNotificacao_BC";
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E123O2 */
         E123O2 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short nGXsfl_169_idx=1 ;
      private short nIsMod_169 ;
      private short RcdFound169 ;
      private short nGXsfl_168_idx=1 ;
      private short nIsMod_168 ;
      private short RcdFound168 ;
      private short GX_JID ;
      private short Z1958ContagemResultadoNotificacao_Port ;
      private short A1958ContagemResultadoNotificacao_Port ;
      private short Z1960ContagemResultadoNotificacao_Sec ;
      private short A1960ContagemResultadoNotificacao_Sec ;
      private short Z1961ContagemResultadoNotificacao_Logged ;
      private short A1961ContagemResultadoNotificacao_Logged ;
      private short Z1227ContagemResultado_PrazoInicialDias ;
      private short A1227ContagemResultado_PrazoInicialDias ;
      private short RcdFound167 ;
      private short nRcdExists_168 ;
      private short Gxremove168 ;
      private short nRcdExists_169 ;
      private short Gxremove169 ;
      private short Gx_BScreen ;
      private int trnEnded ;
      private int A1963ContagemResultadoNotificacao_Demandas ;
      private int s1962ContagemResultadoNotificacao_Destinatarios ;
      private int O1962ContagemResultadoNotificacao_Destinatarios ;
      private int A1962ContagemResultadoNotificacao_Destinatarios ;
      private int AV17GXV1 ;
      private int AV15Insert_ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private int AV11Insert_ContagemResultadoNotificacao_UsuCod ;
      private int Z1413ContagemResultadoNotificacao_UsuCod ;
      private int A1413ContagemResultadoNotificacao_UsuCod ;
      private int Z1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private int A1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private int Z1962ContagemResultadoNotificacao_Destinatarios ;
      private int Z1963ContagemResultadoNotificacao_Demandas ;
      private int Z1414ContagemResultadoNotificacao_DestCod ;
      private int A1414ContagemResultadoNotificacao_DestCod ;
      private int Z1426ContagemResultadoNotificacao_DestPesCod ;
      private int A1426ContagemResultadoNotificacao_DestPesCod ;
      private int Z456ContagemResultado_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int Z601ContagemResultado_Servico ;
      private int A601ContagemResultado_Servico ;
      private int Z1427ContagemResultadoNotificacao_UsuPesCod ;
      private int A1427ContagemResultadoNotificacao_UsuPesCod ;
      private int Z1553ContagemResultado_CntSrvCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private long Z1412ContagemResultadoNotificacao_Codigo ;
      private long A1412ContagemResultadoNotificacao_Codigo ;
      private long AV14Insert_ContagemResultadoNotificacao_ReenvioCod ;
      private long Z1965ContagemResultadoNotificacao_ReenvioCod ;
      private long A1965ContagemResultadoNotificacao_ReenvioCod ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode167 ;
      private String AV16Pgmname ;
      private String Z1420ContagemResultadoNotificacao_Midia ;
      private String A1420ContagemResultadoNotificacao_Midia ;
      private String Z1956ContagemResultadoNotificacao_Host ;
      private String A1956ContagemResultadoNotificacao_Host ;
      private String Z1957ContagemResultadoNotificacao_User ;
      private String A1957ContagemResultadoNotificacao_User ;
      private String Z1421ContagemResultadoNotificacao_DestNome ;
      private String A1421ContagemResultadoNotificacao_DestNome ;
      private String Z801ContagemResultado_ServicoSigla ;
      private String A801ContagemResultado_ServicoSigla ;
      private String Z1422ContagemResultadoNotificacao_UsuNom ;
      private String A1422ContagemResultadoNotificacao_UsuNom ;
      private String sMode168 ;
      private String sMode169 ;
      private DateTime Z1416ContagemResultadoNotificacao_DataHora ;
      private DateTime A1416ContagemResultadoNotificacao_DataHora ;
      private bool n1963ContagemResultadoNotificacao_Demandas ;
      private bool n1962ContagemResultadoNotificacao_Destinatarios ;
      private bool Z1959ContagemResultadoNotificacao_Aut ;
      private bool A1959ContagemResultadoNotificacao_Aut ;
      private bool n1416ContagemResultadoNotificacao_DataHora ;
      private bool n1422ContagemResultadoNotificacao_UsuNom ;
      private bool n1417ContagemResultadoNotificacao_Assunto ;
      private bool n1418ContagemResultadoNotificacao_CorpoEmail ;
      private bool n1420ContagemResultadoNotificacao_Midia ;
      private bool n1415ContagemResultadoNotificacao_Observacao ;
      private bool n1956ContagemResultadoNotificacao_Host ;
      private bool n1957ContagemResultadoNotificacao_User ;
      private bool n1958ContagemResultadoNotificacao_Port ;
      private bool n1959ContagemResultadoNotificacao_Aut ;
      private bool n1960ContagemResultadoNotificacao_Sec ;
      private bool n1961ContagemResultadoNotificacao_Logged ;
      private bool n1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private bool n1965ContagemResultadoNotificacao_ReenvioCod ;
      private bool n1427ContagemResultadoNotificacao_UsuPesCod ;
      private bool Gx_longc ;
      private bool n1421ContagemResultadoNotificacao_DestNome ;
      private bool n1419ContagemResultadoNotificacao_DestEmail ;
      private bool n1426ContagemResultadoNotificacao_DestPesCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n457ContagemResultado_Demanda ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n1227ContagemResultado_PrazoInicialDias ;
      private bool n601ContagemResultado_Servico ;
      private String Z1417ContagemResultadoNotificacao_Assunto ;
      private String A1417ContagemResultadoNotificacao_Assunto ;
      private String Z1415ContagemResultadoNotificacao_Observacao ;
      private String A1415ContagemResultadoNotificacao_Observacao ;
      private String Z1418ContagemResultadoNotificacao_CorpoEmail ;
      private String A1418ContagemResultadoNotificacao_CorpoEmail ;
      private String Z1419ContagemResultadoNotificacao_DestEmail ;
      private String A1419ContagemResultadoNotificacao_DestEmail ;
      private String Z457ContagemResultado_Demanda ;
      private String A457ContagemResultado_Demanda ;
      private String Z493ContagemResultado_DemandaFM ;
      private String A493ContagemResultado_DemandaFM ;
      private IGxSession AV10WebSession ;
      private SdtContagemResultadoNotificacao bcContagemResultadoNotificacao ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC003O8_A1963ContagemResultadoNotificacao_Demandas ;
      private bool[] BC003O8_n1963ContagemResultadoNotificacao_Demandas ;
      private long[] BC003O23_A1412ContagemResultadoNotificacao_Codigo ;
      private DateTime[] BC003O23_A1416ContagemResultadoNotificacao_DataHora ;
      private bool[] BC003O23_n1416ContagemResultadoNotificacao_DataHora ;
      private String[] BC003O23_A1422ContagemResultadoNotificacao_UsuNom ;
      private bool[] BC003O23_n1422ContagemResultadoNotificacao_UsuNom ;
      private String[] BC003O23_A1417ContagemResultadoNotificacao_Assunto ;
      private bool[] BC003O23_n1417ContagemResultadoNotificacao_Assunto ;
      private String[] BC003O23_A1418ContagemResultadoNotificacao_CorpoEmail ;
      private bool[] BC003O23_n1418ContagemResultadoNotificacao_CorpoEmail ;
      private String[] BC003O23_A1420ContagemResultadoNotificacao_Midia ;
      private bool[] BC003O23_n1420ContagemResultadoNotificacao_Midia ;
      private String[] BC003O23_A1415ContagemResultadoNotificacao_Observacao ;
      private bool[] BC003O23_n1415ContagemResultadoNotificacao_Observacao ;
      private String[] BC003O23_A1956ContagemResultadoNotificacao_Host ;
      private bool[] BC003O23_n1956ContagemResultadoNotificacao_Host ;
      private String[] BC003O23_A1957ContagemResultadoNotificacao_User ;
      private bool[] BC003O23_n1957ContagemResultadoNotificacao_User ;
      private short[] BC003O23_A1958ContagemResultadoNotificacao_Port ;
      private bool[] BC003O23_n1958ContagemResultadoNotificacao_Port ;
      private bool[] BC003O23_A1959ContagemResultadoNotificacao_Aut ;
      private bool[] BC003O23_n1959ContagemResultadoNotificacao_Aut ;
      private short[] BC003O23_A1960ContagemResultadoNotificacao_Sec ;
      private bool[] BC003O23_n1960ContagemResultadoNotificacao_Sec ;
      private short[] BC003O23_A1961ContagemResultadoNotificacao_Logged ;
      private bool[] BC003O23_n1961ContagemResultadoNotificacao_Logged ;
      private int[] BC003O23_A1413ContagemResultadoNotificacao_UsuCod ;
      private int[] BC003O23_A1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private bool[] BC003O23_n1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private long[] BC003O23_A1965ContagemResultadoNotificacao_ReenvioCod ;
      private bool[] BC003O23_n1965ContagemResultadoNotificacao_ReenvioCod ;
      private int[] BC003O23_A1427ContagemResultadoNotificacao_UsuPesCod ;
      private bool[] BC003O23_n1427ContagemResultadoNotificacao_UsuPesCod ;
      private int[] BC003O23_A1962ContagemResultadoNotificacao_Destinatarios ;
      private bool[] BC003O23_n1962ContagemResultadoNotificacao_Destinatarios ;
      private int[] BC003O23_A1963ContagemResultadoNotificacao_Demandas ;
      private bool[] BC003O23_n1963ContagemResultadoNotificacao_Demandas ;
      private int[] BC003O20_A1962ContagemResultadoNotificacao_Destinatarios ;
      private bool[] BC003O20_n1962ContagemResultadoNotificacao_Destinatarios ;
      private int[] BC003O16_A1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private bool[] BC003O16_n1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private int[] BC003O15_A1427ContagemResultadoNotificacao_UsuPesCod ;
      private bool[] BC003O15_n1427ContagemResultadoNotificacao_UsuPesCod ;
      private String[] BC003O18_A1422ContagemResultadoNotificacao_UsuNom ;
      private bool[] BC003O18_n1422ContagemResultadoNotificacao_UsuNom ;
      private long[] BC003O17_A1965ContagemResultadoNotificacao_ReenvioCod ;
      private bool[] BC003O17_n1965ContagemResultadoNotificacao_ReenvioCod ;
      private long[] BC003O24_A1412ContagemResultadoNotificacao_Codigo ;
      private long[] BC003O14_A1412ContagemResultadoNotificacao_Codigo ;
      private DateTime[] BC003O14_A1416ContagemResultadoNotificacao_DataHora ;
      private bool[] BC003O14_n1416ContagemResultadoNotificacao_DataHora ;
      private String[] BC003O14_A1417ContagemResultadoNotificacao_Assunto ;
      private bool[] BC003O14_n1417ContagemResultadoNotificacao_Assunto ;
      private String[] BC003O14_A1418ContagemResultadoNotificacao_CorpoEmail ;
      private bool[] BC003O14_n1418ContagemResultadoNotificacao_CorpoEmail ;
      private String[] BC003O14_A1420ContagemResultadoNotificacao_Midia ;
      private bool[] BC003O14_n1420ContagemResultadoNotificacao_Midia ;
      private String[] BC003O14_A1415ContagemResultadoNotificacao_Observacao ;
      private bool[] BC003O14_n1415ContagemResultadoNotificacao_Observacao ;
      private String[] BC003O14_A1956ContagemResultadoNotificacao_Host ;
      private bool[] BC003O14_n1956ContagemResultadoNotificacao_Host ;
      private String[] BC003O14_A1957ContagemResultadoNotificacao_User ;
      private bool[] BC003O14_n1957ContagemResultadoNotificacao_User ;
      private short[] BC003O14_A1958ContagemResultadoNotificacao_Port ;
      private bool[] BC003O14_n1958ContagemResultadoNotificacao_Port ;
      private bool[] BC003O14_A1959ContagemResultadoNotificacao_Aut ;
      private bool[] BC003O14_n1959ContagemResultadoNotificacao_Aut ;
      private short[] BC003O14_A1960ContagemResultadoNotificacao_Sec ;
      private bool[] BC003O14_n1960ContagemResultadoNotificacao_Sec ;
      private short[] BC003O14_A1961ContagemResultadoNotificacao_Logged ;
      private bool[] BC003O14_n1961ContagemResultadoNotificacao_Logged ;
      private int[] BC003O14_A1413ContagemResultadoNotificacao_UsuCod ;
      private int[] BC003O14_A1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private bool[] BC003O14_n1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private long[] BC003O14_A1965ContagemResultadoNotificacao_ReenvioCod ;
      private bool[] BC003O14_n1965ContagemResultadoNotificacao_ReenvioCod ;
      private long[] BC003O13_A1412ContagemResultadoNotificacao_Codigo ;
      private DateTime[] BC003O13_A1416ContagemResultadoNotificacao_DataHora ;
      private bool[] BC003O13_n1416ContagemResultadoNotificacao_DataHora ;
      private String[] BC003O13_A1417ContagemResultadoNotificacao_Assunto ;
      private bool[] BC003O13_n1417ContagemResultadoNotificacao_Assunto ;
      private String[] BC003O13_A1418ContagemResultadoNotificacao_CorpoEmail ;
      private bool[] BC003O13_n1418ContagemResultadoNotificacao_CorpoEmail ;
      private String[] BC003O13_A1420ContagemResultadoNotificacao_Midia ;
      private bool[] BC003O13_n1420ContagemResultadoNotificacao_Midia ;
      private String[] BC003O13_A1415ContagemResultadoNotificacao_Observacao ;
      private bool[] BC003O13_n1415ContagemResultadoNotificacao_Observacao ;
      private String[] BC003O13_A1956ContagemResultadoNotificacao_Host ;
      private bool[] BC003O13_n1956ContagemResultadoNotificacao_Host ;
      private String[] BC003O13_A1957ContagemResultadoNotificacao_User ;
      private bool[] BC003O13_n1957ContagemResultadoNotificacao_User ;
      private short[] BC003O13_A1958ContagemResultadoNotificacao_Port ;
      private bool[] BC003O13_n1958ContagemResultadoNotificacao_Port ;
      private bool[] BC003O13_A1959ContagemResultadoNotificacao_Aut ;
      private bool[] BC003O13_n1959ContagemResultadoNotificacao_Aut ;
      private short[] BC003O13_A1960ContagemResultadoNotificacao_Sec ;
      private bool[] BC003O13_n1960ContagemResultadoNotificacao_Sec ;
      private short[] BC003O13_A1961ContagemResultadoNotificacao_Logged ;
      private bool[] BC003O13_n1961ContagemResultadoNotificacao_Logged ;
      private int[] BC003O13_A1413ContagemResultadoNotificacao_UsuCod ;
      private int[] BC003O13_A1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private bool[] BC003O13_n1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private long[] BC003O13_A1965ContagemResultadoNotificacao_ReenvioCod ;
      private bool[] BC003O13_n1965ContagemResultadoNotificacao_ReenvioCod ;
      private long[] BC003O25_A1412ContagemResultadoNotificacao_Codigo ;
      private int[] BC003O29_A1962ContagemResultadoNotificacao_Destinatarios ;
      private bool[] BC003O29_n1962ContagemResultadoNotificacao_Destinatarios ;
      private int[] BC003O31_A1963ContagemResultadoNotificacao_Demandas ;
      private bool[] BC003O31_n1963ContagemResultadoNotificacao_Demandas ;
      private int[] BC003O32_A1427ContagemResultadoNotificacao_UsuPesCod ;
      private bool[] BC003O32_n1427ContagemResultadoNotificacao_UsuPesCod ;
      private String[] BC003O33_A1422ContagemResultadoNotificacao_UsuNom ;
      private bool[] BC003O33_n1422ContagemResultadoNotificacao_UsuNom ;
      private long[] BC003O34_A1965ContagemResultadoNotificacao_ReenvioCod ;
      private bool[] BC003O34_n1965ContagemResultadoNotificacao_ReenvioCod ;
      private long[] BC003O37_A1412ContagemResultadoNotificacao_Codigo ;
      private DateTime[] BC003O37_A1416ContagemResultadoNotificacao_DataHora ;
      private bool[] BC003O37_n1416ContagemResultadoNotificacao_DataHora ;
      private String[] BC003O37_A1422ContagemResultadoNotificacao_UsuNom ;
      private bool[] BC003O37_n1422ContagemResultadoNotificacao_UsuNom ;
      private String[] BC003O37_A1417ContagemResultadoNotificacao_Assunto ;
      private bool[] BC003O37_n1417ContagemResultadoNotificacao_Assunto ;
      private String[] BC003O37_A1418ContagemResultadoNotificacao_CorpoEmail ;
      private bool[] BC003O37_n1418ContagemResultadoNotificacao_CorpoEmail ;
      private String[] BC003O37_A1420ContagemResultadoNotificacao_Midia ;
      private bool[] BC003O37_n1420ContagemResultadoNotificacao_Midia ;
      private String[] BC003O37_A1415ContagemResultadoNotificacao_Observacao ;
      private bool[] BC003O37_n1415ContagemResultadoNotificacao_Observacao ;
      private String[] BC003O37_A1956ContagemResultadoNotificacao_Host ;
      private bool[] BC003O37_n1956ContagemResultadoNotificacao_Host ;
      private String[] BC003O37_A1957ContagemResultadoNotificacao_User ;
      private bool[] BC003O37_n1957ContagemResultadoNotificacao_User ;
      private short[] BC003O37_A1958ContagemResultadoNotificacao_Port ;
      private bool[] BC003O37_n1958ContagemResultadoNotificacao_Port ;
      private bool[] BC003O37_A1959ContagemResultadoNotificacao_Aut ;
      private bool[] BC003O37_n1959ContagemResultadoNotificacao_Aut ;
      private short[] BC003O37_A1960ContagemResultadoNotificacao_Sec ;
      private bool[] BC003O37_n1960ContagemResultadoNotificacao_Sec ;
      private short[] BC003O37_A1961ContagemResultadoNotificacao_Logged ;
      private bool[] BC003O37_n1961ContagemResultadoNotificacao_Logged ;
      private int[] BC003O37_A1413ContagemResultadoNotificacao_UsuCod ;
      private int[] BC003O37_A1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private bool[] BC003O37_n1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private long[] BC003O37_A1965ContagemResultadoNotificacao_ReenvioCod ;
      private bool[] BC003O37_n1965ContagemResultadoNotificacao_ReenvioCod ;
      private int[] BC003O37_A1427ContagemResultadoNotificacao_UsuPesCod ;
      private bool[] BC003O37_n1427ContagemResultadoNotificacao_UsuPesCod ;
      private int[] BC003O37_A1962ContagemResultadoNotificacao_Destinatarios ;
      private bool[] BC003O37_n1962ContagemResultadoNotificacao_Destinatarios ;
      private int[] BC003O37_A1963ContagemResultadoNotificacao_Demandas ;
      private bool[] BC003O37_n1963ContagemResultadoNotificacao_Demandas ;
      private long[] BC003O38_A1412ContagemResultadoNotificacao_Codigo ;
      private String[] BC003O38_A1421ContagemResultadoNotificacao_DestNome ;
      private bool[] BC003O38_n1421ContagemResultadoNotificacao_DestNome ;
      private String[] BC003O38_A1419ContagemResultadoNotificacao_DestEmail ;
      private bool[] BC003O38_n1419ContagemResultadoNotificacao_DestEmail ;
      private int[] BC003O38_A1414ContagemResultadoNotificacao_DestCod ;
      private int[] BC003O38_A1426ContagemResultadoNotificacao_DestPesCod ;
      private bool[] BC003O38_n1426ContagemResultadoNotificacao_DestPesCod ;
      private int[] BC003O11_A1426ContagemResultadoNotificacao_DestPesCod ;
      private bool[] BC003O11_n1426ContagemResultadoNotificacao_DestPesCod ;
      private String[] BC003O12_A1421ContagemResultadoNotificacao_DestNome ;
      private bool[] BC003O12_n1421ContagemResultadoNotificacao_DestNome ;
      private long[] BC003O39_A1412ContagemResultadoNotificacao_Codigo ;
      private int[] BC003O39_A1414ContagemResultadoNotificacao_DestCod ;
      private long[] BC003O10_A1412ContagemResultadoNotificacao_Codigo ;
      private String[] BC003O10_A1419ContagemResultadoNotificacao_DestEmail ;
      private bool[] BC003O10_n1419ContagemResultadoNotificacao_DestEmail ;
      private int[] BC003O10_A1414ContagemResultadoNotificacao_DestCod ;
      private long[] BC003O9_A1412ContagemResultadoNotificacao_Codigo ;
      private String[] BC003O9_A1419ContagemResultadoNotificacao_DestEmail ;
      private bool[] BC003O9_n1419ContagemResultadoNotificacao_DestEmail ;
      private int[] BC003O9_A1414ContagemResultadoNotificacao_DestCod ;
      private int[] BC003O43_A1426ContagemResultadoNotificacao_DestPesCod ;
      private bool[] BC003O43_n1426ContagemResultadoNotificacao_DestPesCod ;
      private String[] BC003O44_A1421ContagemResultadoNotificacao_DestNome ;
      private bool[] BC003O44_n1421ContagemResultadoNotificacao_DestNome ;
      private long[] BC003O45_A1412ContagemResultadoNotificacao_Codigo ;
      private String[] BC003O45_A1421ContagemResultadoNotificacao_DestNome ;
      private bool[] BC003O45_n1421ContagemResultadoNotificacao_DestNome ;
      private String[] BC003O45_A1419ContagemResultadoNotificacao_DestEmail ;
      private bool[] BC003O45_n1419ContagemResultadoNotificacao_DestEmail ;
      private int[] BC003O45_A1414ContagemResultadoNotificacao_DestCod ;
      private int[] BC003O45_A1426ContagemResultadoNotificacao_DestPesCod ;
      private bool[] BC003O45_n1426ContagemResultadoNotificacao_DestPesCod ;
      private int[] BC003O46_A1553ContagemResultado_CntSrvCod ;
      private bool[] BC003O46_n1553ContagemResultado_CntSrvCod ;
      private long[] BC003O46_A1412ContagemResultadoNotificacao_Codigo ;
      private String[] BC003O46_A457ContagemResultado_Demanda ;
      private bool[] BC003O46_n457ContagemResultado_Demanda ;
      private String[] BC003O46_A493ContagemResultado_DemandaFM ;
      private bool[] BC003O46_n493ContagemResultado_DemandaFM ;
      private String[] BC003O46_A801ContagemResultado_ServicoSigla ;
      private bool[] BC003O46_n801ContagemResultado_ServicoSigla ;
      private short[] BC003O46_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] BC003O46_n1227ContagemResultado_PrazoInicialDias ;
      private int[] BC003O46_A456ContagemResultado_Codigo ;
      private int[] BC003O46_A601ContagemResultado_Servico ;
      private bool[] BC003O46_n601ContagemResultado_Servico ;
      private int[] BC003O4_A1553ContagemResultado_CntSrvCod ;
      private bool[] BC003O4_n1553ContagemResultado_CntSrvCod ;
      private String[] BC003O4_A457ContagemResultado_Demanda ;
      private bool[] BC003O4_n457ContagemResultado_Demanda ;
      private String[] BC003O4_A493ContagemResultado_DemandaFM ;
      private bool[] BC003O4_n493ContagemResultado_DemandaFM ;
      private short[] BC003O4_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] BC003O4_n1227ContagemResultado_PrazoInicialDias ;
      private int[] BC003O5_A601ContagemResultado_Servico ;
      private bool[] BC003O5_n601ContagemResultado_Servico ;
      private String[] BC003O6_A801ContagemResultado_ServicoSigla ;
      private bool[] BC003O6_n801ContagemResultado_ServicoSigla ;
      private long[] BC003O47_A1412ContagemResultadoNotificacao_Codigo ;
      private int[] BC003O47_A456ContagemResultado_Codigo ;
      private long[] BC003O3_A1412ContagemResultadoNotificacao_Codigo ;
      private int[] BC003O3_A456ContagemResultado_Codigo ;
      private long[] BC003O2_A1412ContagemResultadoNotificacao_Codigo ;
      private int[] BC003O2_A456ContagemResultado_Codigo ;
      private int[] BC003O50_A1553ContagemResultado_CntSrvCod ;
      private bool[] BC003O50_n1553ContagemResultado_CntSrvCod ;
      private String[] BC003O50_A457ContagemResultado_Demanda ;
      private bool[] BC003O50_n457ContagemResultado_Demanda ;
      private String[] BC003O50_A493ContagemResultado_DemandaFM ;
      private bool[] BC003O50_n493ContagemResultado_DemandaFM ;
      private short[] BC003O50_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] BC003O50_n1227ContagemResultado_PrazoInicialDias ;
      private int[] BC003O51_A601ContagemResultado_Servico ;
      private bool[] BC003O51_n601ContagemResultado_Servico ;
      private String[] BC003O52_A801ContagemResultado_ServicoSigla ;
      private bool[] BC003O52_n801ContagemResultado_ServicoSigla ;
      private int[] BC003O53_A1553ContagemResultado_CntSrvCod ;
      private bool[] BC003O53_n1553ContagemResultado_CntSrvCod ;
      private long[] BC003O53_A1412ContagemResultadoNotificacao_Codigo ;
      private String[] BC003O53_A457ContagemResultado_Demanda ;
      private bool[] BC003O53_n457ContagemResultado_Demanda ;
      private String[] BC003O53_A493ContagemResultado_DemandaFM ;
      private bool[] BC003O53_n493ContagemResultado_DemandaFM ;
      private String[] BC003O53_A801ContagemResultado_ServicoSigla ;
      private bool[] BC003O53_n801ContagemResultado_ServicoSigla ;
      private short[] BC003O53_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] BC003O53_n1227ContagemResultado_PrazoInicialDias ;
      private int[] BC003O53_A456ContagemResultado_Codigo ;
      private int[] BC003O53_A601ContagemResultado_Servico ;
      private bool[] BC003O53_n601ContagemResultado_Servico ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class contagemresultadonotificacao_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new UpdateCursor(def[20])
         ,new UpdateCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new UpdateCursor(def[30])
         ,new UpdateCursor(def[31])
         ,new UpdateCursor(def[32])
         ,new ForEachCursor(def[33])
         ,new ForEachCursor(def[34])
         ,new ForEachCursor(def[35])
         ,new ForEachCursor(def[36])
         ,new ForEachCursor(def[37])
         ,new UpdateCursor(def[38])
         ,new UpdateCursor(def[39])
         ,new ForEachCursor(def[40])
         ,new ForEachCursor(def[41])
         ,new ForEachCursor(def[42])
         ,new ForEachCursor(def[43])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC003O23 ;
          prmBC003O23 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          String cmdBufferBC003O23 ;
          cmdBufferBC003O23=" SELECT TM1.[ContagemResultadoNotificacao_Codigo], TM1.[ContagemResultadoNotificacao_DataHora], T5.[Pessoa_Nome] AS ContagemResultadoNotificacao_UsuNom, TM1.[ContagemResultadoNotificacao_Assunto], TM1.[ContagemResultadoNotificacao_CorpoEmail], TM1.[ContagemResultadoNotificacao_Midia], TM1.[ContagemResultadoNotificacao_Observacao], TM1.[ContagemResultadoNotificacao_Host], TM1.[ContagemResultadoNotificacao_User], TM1.[ContagemResultadoNotificacao_Port], TM1.[ContagemResultadoNotificacao_Aut], TM1.[ContagemResultadoNotificacao_Sec], TM1.[ContagemResultadoNotificacao_Logged], TM1.[ContagemResultadoNotificacao_UsuCod] AS ContagemResultadoNotificacao_UsuCod, TM1.[ContagemResultadoNotificacao_AreaTrabalhoCod] AS ContagemResultadoNotificacao_AreaTrabalhoCod, TM1.[ContagemResultadoNotificacao_ReenvioCod] AS ContagemResultadoNotificacao_ReenvioCod, T4.[Usuario_PessoaCod] AS ContagemResultadoNotificacao_UsuPesCod, COALESCE( T2.[ContagemResultadoNotificacao_Destinatarios], 0) AS ContagemResultadoNotificacao_Destinatarios, COALESCE( T3.[ContagemResultadoNotificacao_Demandas], 0) AS ContagemResultadoNotificacao_Demandas FROM (((([ContagemResultadoNotificacao] TM1 WITH (NOLOCK) LEFT JOIN (SELECT COUNT(*) AS ContagemResultadoNotificacao_Destinatarios, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDestinatario] WITH (NOLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T2 ON T2.[ContagemResultadoNotificacao_Codigo] = TM1.[ContagemResultadoNotificacao_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContagemResultadoNotificacao_Demandas, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDemanda] WITH (NOLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T3 ON T3.[ContagemResultadoNotificacao_Codigo] = TM1.[ContagemResultadoNotificacao_Codigo]) "
          + " INNER JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = TM1.[ContagemResultadoNotificacao_UsuCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod]) WHERE TM1.[ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo ORDER BY TM1.[ContagemResultadoNotificacao_Codigo]  OPTION (FAST 100)" ;
          Object[] prmBC003O20 ;
          prmBC003O20 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmBC003O8 ;
          prmBC003O8 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmBC003O16 ;
          prmBC003O16 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003O15 ;
          prmBC003O15 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_UsuCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003O18 ;
          prmBC003O18 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_UsuPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003O17 ;
          prmBC003O17 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_ReenvioCod",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmBC003O24 ;
          prmBC003O24 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmBC003O14 ;
          prmBC003O14 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmBC003O13 ;
          prmBC003O13 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmBC003O25 ;
          prmBC003O25 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoNotificacao_Assunto",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@ContagemResultadoNotificacao_CorpoEmail",SqlDbType.VarChar,8000,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Midia",SqlDbType.Char,3,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Host",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultadoNotificacao_User",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Port",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Aut",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Sec",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Logged",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoNotificacao_UsuCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoNotificacao_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoNotificacao_ReenvioCod",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmBC003O26 ;
          prmBC003O26 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoNotificacao_Assunto",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@ContagemResultadoNotificacao_CorpoEmail",SqlDbType.VarChar,8000,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Midia",SqlDbType.Char,3,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Host",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultadoNotificacao_User",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Port",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Aut",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Sec",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Logged",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoNotificacao_UsuCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoNotificacao_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoNotificacao_ReenvioCod",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmBC003O27 ;
          prmBC003O27 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmBC003O29 ;
          prmBC003O29 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmBC003O32 ;
          prmBC003O32 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_UsuCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003O33 ;
          prmBC003O33 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_UsuPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003O34 ;
          prmBC003O34 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmBC003O31 ;
          prmBC003O31 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmBC003O37 ;
          prmBC003O37 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          String cmdBufferBC003O37 ;
          cmdBufferBC003O37=" SELECT TM1.[ContagemResultadoNotificacao_Codigo], TM1.[ContagemResultadoNotificacao_DataHora], T5.[Pessoa_Nome] AS ContagemResultadoNotificacao_UsuNom, TM1.[ContagemResultadoNotificacao_Assunto], TM1.[ContagemResultadoNotificacao_CorpoEmail], TM1.[ContagemResultadoNotificacao_Midia], TM1.[ContagemResultadoNotificacao_Observacao], TM1.[ContagemResultadoNotificacao_Host], TM1.[ContagemResultadoNotificacao_User], TM1.[ContagemResultadoNotificacao_Port], TM1.[ContagemResultadoNotificacao_Aut], TM1.[ContagemResultadoNotificacao_Sec], TM1.[ContagemResultadoNotificacao_Logged], TM1.[ContagemResultadoNotificacao_UsuCod] AS ContagemResultadoNotificacao_UsuCod, TM1.[ContagemResultadoNotificacao_AreaTrabalhoCod] AS ContagemResultadoNotificacao_AreaTrabalhoCod, TM1.[ContagemResultadoNotificacao_ReenvioCod] AS ContagemResultadoNotificacao_ReenvioCod, T4.[Usuario_PessoaCod] AS ContagemResultadoNotificacao_UsuPesCod, COALESCE( T2.[ContagemResultadoNotificacao_Destinatarios], 0) AS ContagemResultadoNotificacao_Destinatarios, COALESCE( T3.[ContagemResultadoNotificacao_Demandas], 0) AS ContagemResultadoNotificacao_Demandas FROM (((([ContagemResultadoNotificacao] TM1 WITH (NOLOCK) LEFT JOIN (SELECT COUNT(*) AS ContagemResultadoNotificacao_Destinatarios, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDestinatario] WITH (NOLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T2 ON T2.[ContagemResultadoNotificacao_Codigo] = TM1.[ContagemResultadoNotificacao_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContagemResultadoNotificacao_Demandas, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDemanda] WITH (NOLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T3 ON T3.[ContagemResultadoNotificacao_Codigo] = TM1.[ContagemResultadoNotificacao_Codigo]) "
          + " INNER JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = TM1.[ContagemResultadoNotificacao_UsuCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod]) WHERE TM1.[ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo ORDER BY TM1.[ContagemResultadoNotificacao_Codigo]  OPTION (FAST 100)" ;
          Object[] prmBC003O38 ;
          prmBC003O38 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultadoNotificacao_DestCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003O11 ;
          prmBC003O11 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_DestCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003O12 ;
          prmBC003O12 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_DestPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003O39 ;
          prmBC003O39 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultadoNotificacao_DestCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003O10 ;
          prmBC003O10 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultadoNotificacao_DestCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003O9 ;
          prmBC003O9 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultadoNotificacao_DestCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003O40 ;
          prmBC003O40 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultadoNotificacao_DestEmail",SqlDbType.VarChar,100,0} ,
          new Object[] {"@ContagemResultadoNotificacao_DestCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003O41 ;
          prmBC003O41 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_DestEmail",SqlDbType.VarChar,100,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultadoNotificacao_DestCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003O42 ;
          prmBC003O42 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultadoNotificacao_DestCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003O43 ;
          prmBC003O43 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_DestCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003O44 ;
          prmBC003O44 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_DestPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003O45 ;
          prmBC003O45 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmBC003O46 ;
          prmBC003O46 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003O4 ;
          prmBC003O4 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003O5 ;
          prmBC003O5 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003O6 ;
          prmBC003O6 = new Object[] {
          new Object[] {"@ContagemResultado_Servico",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003O47 ;
          prmBC003O47 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003O3 ;
          prmBC003O3 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003O2 ;
          prmBC003O2 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003O48 ;
          prmBC003O48 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003O49 ;
          prmBC003O49 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003O50 ;
          prmBC003O50 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003O51 ;
          prmBC003O51 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003O52 ;
          prmBC003O52 = new Object[] {
          new Object[] {"@ContagemResultado_Servico",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003O53 ;
          prmBC003O53 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC003O2", "SELECT [ContagemResultadoNotificacao_Codigo], [ContagemResultado_Codigo] FROM [ContagemResultadoNotificacaoDemanda] WITH (UPDLOCK) WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo AND [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O2,1,0,true,false )
             ,new CursorDef("BC003O3", "SELECT [ContagemResultadoNotificacao_Codigo], [ContagemResultado_Codigo] FROM [ContagemResultadoNotificacaoDemanda] WITH (NOLOCK) WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo AND [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O3,1,0,true,false )
             ,new CursorDef("BC003O4", "SELECT [ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, [ContagemResultado_Demanda], [ContagemResultado_DemandaFM], [ContagemResultado_PrazoInicialDias] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O4,1,0,true,false )
             ,new CursorDef("BC003O5", "SELECT [Servico_Codigo] AS ContagemResultado_Servico FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContagemResultado_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O5,1,0,true,false )
             ,new CursorDef("BC003O6", "SELECT [Servico_Sigla] AS ContagemResultado_ServicoSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContagemResultado_Servico ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O6,1,0,true,false )
             ,new CursorDef("BC003O8", "SELECT COALESCE( T1.[ContagemResultadoNotificacao_Demandas], 0) AS ContagemResultadoNotificacao_Demandas FROM (SELECT COUNT(*) AS ContagemResultadoNotificacao_Demandas, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDemanda] WITH (UPDLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T1 WHERE T1.[ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O8,1,0,true,false )
             ,new CursorDef("BC003O9", "SELECT [ContagemResultadoNotificacao_Codigo], [ContagemResultadoNotificacao_DestEmail], [ContagemResultadoNotificacao_DestCod] AS ContagemResultadoNotificacao_D FROM [ContagemResultadoNotificacaoDestinatario] WITH (UPDLOCK) WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo AND [ContagemResultadoNotificacao_DestCod] = @ContagemResultadoNotificacao_DestCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O9,1,0,true,false )
             ,new CursorDef("BC003O10", "SELECT [ContagemResultadoNotificacao_Codigo], [ContagemResultadoNotificacao_DestEmail], [ContagemResultadoNotificacao_DestCod] AS ContagemResultadoNotificacao_D FROM [ContagemResultadoNotificacaoDestinatario] WITH (NOLOCK) WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo AND [ContagemResultadoNotificacao_DestCod] = @ContagemResultadoNotificacao_DestCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O10,1,0,true,false )
             ,new CursorDef("BC003O11", "SELECT [Usuario_PessoaCod] AS ContagemResultadoNotificacao_D FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoNotificacao_DestCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O11,1,0,true,false )
             ,new CursorDef("BC003O12", "SELECT [Pessoa_Nome] AS ContagemResultadoNotificacao_D FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoNotificacao_DestPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O12,1,0,true,false )
             ,new CursorDef("BC003O13", "SELECT [ContagemResultadoNotificacao_Codigo], [ContagemResultadoNotificacao_DataHora], [ContagemResultadoNotificacao_Assunto], [ContagemResultadoNotificacao_CorpoEmail], [ContagemResultadoNotificacao_Midia], [ContagemResultadoNotificacao_Observacao], [ContagemResultadoNotificacao_Host], [ContagemResultadoNotificacao_User], [ContagemResultadoNotificacao_Port], [ContagemResultadoNotificacao_Aut], [ContagemResultadoNotificacao_Sec], [ContagemResultadoNotificacao_Logged], [ContagemResultadoNotificacao_UsuCod] AS ContagemResultadoNotificacao_UsuCod, [ContagemResultadoNotificacao_AreaTrabalhoCod] AS ContagemResultadoNotificacao_AreaTrabalhoCod, [ContagemResultadoNotificacao_ReenvioCod] AS ContagemResultadoNotificacao_ReenvioCod FROM [ContagemResultadoNotificacao] WITH (UPDLOCK) WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O13,1,0,true,false )
             ,new CursorDef("BC003O14", "SELECT [ContagemResultadoNotificacao_Codigo], [ContagemResultadoNotificacao_DataHora], [ContagemResultadoNotificacao_Assunto], [ContagemResultadoNotificacao_CorpoEmail], [ContagemResultadoNotificacao_Midia], [ContagemResultadoNotificacao_Observacao], [ContagemResultadoNotificacao_Host], [ContagemResultadoNotificacao_User], [ContagemResultadoNotificacao_Port], [ContagemResultadoNotificacao_Aut], [ContagemResultadoNotificacao_Sec], [ContagemResultadoNotificacao_Logged], [ContagemResultadoNotificacao_UsuCod] AS ContagemResultadoNotificacao_UsuCod, [ContagemResultadoNotificacao_AreaTrabalhoCod] AS ContagemResultadoNotificacao_AreaTrabalhoCod, [ContagemResultadoNotificacao_ReenvioCod] AS ContagemResultadoNotificacao_ReenvioCod FROM [ContagemResultadoNotificacao] WITH (NOLOCK) WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O14,1,0,true,false )
             ,new CursorDef("BC003O15", "SELECT [Usuario_PessoaCod] AS ContagemResultadoNotificacao_UsuPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoNotificacao_UsuCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O15,1,0,true,false )
             ,new CursorDef("BC003O16", "SELECT [AreaTrabalho_Codigo] AS ContagemResultadoNotificacao_AreaTrabalhoCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @ContagemResultadoNotificacao_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O16,1,0,true,false )
             ,new CursorDef("BC003O17", "SELECT [ContagemResultadoNotificacao_Codigo] AS ContagemResultadoNotificacao_ReenvioCod FROM [ContagemResultadoNotificacao] WITH (NOLOCK) WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_ReenvioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O17,1,0,true,false )
             ,new CursorDef("BC003O18", "SELECT [Pessoa_Nome] AS ContagemResultadoNotificacao_UsuNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoNotificacao_UsuPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O18,1,0,true,false )
             ,new CursorDef("BC003O20", "SELECT COALESCE( T1.[ContagemResultadoNotificacao_Destinatarios], 0) AS ContagemResultadoNotificacao_Destinatarios FROM (SELECT COUNT(*) AS ContagemResultadoNotificacao_Destinatarios, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDestinatario] WITH (UPDLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T1 WHERE T1.[ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O20,1,0,true,false )
             ,new CursorDef("BC003O23", cmdBufferBC003O23,true, GxErrorMask.GX_NOMASK, false, this,prmBC003O23,100,0,true,false )
             ,new CursorDef("BC003O24", "SELECT [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacao] WITH (NOLOCK) WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O24,1,0,true,false )
             ,new CursorDef("BC003O25", "INSERT INTO [ContagemResultadoNotificacao]([ContagemResultadoNotificacao_DataHora], [ContagemResultadoNotificacao_Assunto], [ContagemResultadoNotificacao_CorpoEmail], [ContagemResultadoNotificacao_Midia], [ContagemResultadoNotificacao_Observacao], [ContagemResultadoNotificacao_Host], [ContagemResultadoNotificacao_User], [ContagemResultadoNotificacao_Port], [ContagemResultadoNotificacao_Aut], [ContagemResultadoNotificacao_Sec], [ContagemResultadoNotificacao_Logged], [ContagemResultadoNotificacao_UsuCod], [ContagemResultadoNotificacao_AreaTrabalhoCod], [ContagemResultadoNotificacao_ReenvioCod]) VALUES(@ContagemResultadoNotificacao_DataHora, @ContagemResultadoNotificacao_Assunto, @ContagemResultadoNotificacao_CorpoEmail, @ContagemResultadoNotificacao_Midia, @ContagemResultadoNotificacao_Observacao, @ContagemResultadoNotificacao_Host, @ContagemResultadoNotificacao_User, @ContagemResultadoNotificacao_Port, @ContagemResultadoNotificacao_Aut, @ContagemResultadoNotificacao_Sec, @ContagemResultadoNotificacao_Logged, @ContagemResultadoNotificacao_UsuCod, @ContagemResultadoNotificacao_AreaTrabalhoCod, @ContagemResultadoNotificacao_ReenvioCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC003O25)
             ,new CursorDef("BC003O26", "UPDATE [ContagemResultadoNotificacao] SET [ContagemResultadoNotificacao_DataHora]=@ContagemResultadoNotificacao_DataHora, [ContagemResultadoNotificacao_Assunto]=@ContagemResultadoNotificacao_Assunto, [ContagemResultadoNotificacao_CorpoEmail]=@ContagemResultadoNotificacao_CorpoEmail, [ContagemResultadoNotificacao_Midia]=@ContagemResultadoNotificacao_Midia, [ContagemResultadoNotificacao_Observacao]=@ContagemResultadoNotificacao_Observacao, [ContagemResultadoNotificacao_Host]=@ContagemResultadoNotificacao_Host, [ContagemResultadoNotificacao_User]=@ContagemResultadoNotificacao_User, [ContagemResultadoNotificacao_Port]=@ContagemResultadoNotificacao_Port, [ContagemResultadoNotificacao_Aut]=@ContagemResultadoNotificacao_Aut, [ContagemResultadoNotificacao_Sec]=@ContagemResultadoNotificacao_Sec, [ContagemResultadoNotificacao_Logged]=@ContagemResultadoNotificacao_Logged, [ContagemResultadoNotificacao_UsuCod]=@ContagemResultadoNotificacao_UsuCod, [ContagemResultadoNotificacao_AreaTrabalhoCod]=@ContagemResultadoNotificacao_AreaTrabalhoCod, [ContagemResultadoNotificacao_ReenvioCod]=@ContagemResultadoNotificacao_ReenvioCod  WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo", GxErrorMask.GX_NOMASK,prmBC003O26)
             ,new CursorDef("BC003O27", "DELETE FROM [ContagemResultadoNotificacao]  WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo", GxErrorMask.GX_NOMASK,prmBC003O27)
             ,new CursorDef("BC003O29", "SELECT COALESCE( T1.[ContagemResultadoNotificacao_Destinatarios], 0) AS ContagemResultadoNotificacao_Destinatarios FROM (SELECT COUNT(*) AS ContagemResultadoNotificacao_Destinatarios, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDestinatario] WITH (UPDLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T1 WHERE T1.[ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O29,1,0,true,false )
             ,new CursorDef("BC003O31", "SELECT COALESCE( T1.[ContagemResultadoNotificacao_Demandas], 0) AS ContagemResultadoNotificacao_Demandas FROM (SELECT COUNT(*) AS ContagemResultadoNotificacao_Demandas, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDemanda] WITH (UPDLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T1 WHERE T1.[ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O31,1,0,true,false )
             ,new CursorDef("BC003O32", "SELECT [Usuario_PessoaCod] AS ContagemResultadoNotificacao_UsuPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoNotificacao_UsuCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O32,1,0,true,false )
             ,new CursorDef("BC003O33", "SELECT [Pessoa_Nome] AS ContagemResultadoNotificacao_UsuNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoNotificacao_UsuPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O33,1,0,true,false )
             ,new CursorDef("BC003O34", "SELECT TOP 1 [ContagemResultadoNotificacao_Codigo] AS ContagemResultadoNotificacao_ReenvioCod FROM [ContagemResultadoNotificacao] WITH (NOLOCK) WHERE [ContagemResultadoNotificacao_ReenvioCod] = @ContagemResultadoNotificacao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O34,1,0,true,true )
             ,new CursorDef("BC003O37", cmdBufferBC003O37,true, GxErrorMask.GX_NOMASK, false, this,prmBC003O37,100,0,true,false )
             ,new CursorDef("BC003O38", "SELECT T1.[ContagemResultadoNotificacao_Codigo], T3.[Pessoa_Nome] AS ContagemResultadoNotificacao_D, T1.[ContagemResultadoNotificacao_DestEmail], T1.[ContagemResultadoNotificacao_DestCod] AS ContagemResultadoNotificacao_D, T2.[Usuario_PessoaCod] AS ContagemResultadoNotificacao_D FROM (([ContagemResultadoNotificacaoDestinatario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultadoNotificacao_DestCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE T1.[ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo and T1.[ContagemResultadoNotificacao_DestCod] = @ContagemResultadoNotificacao_DestCod ORDER BY T1.[ContagemResultadoNotificacao_Codigo], T1.[ContagemResultadoNotificacao_DestCod]  OPTION (FAST 11)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O38,11,0,true,false )
             ,new CursorDef("BC003O39", "SELECT [ContagemResultadoNotificacao_Codigo], [ContagemResultadoNotificacao_DestCod] AS ContagemResultadoNotificacao_D FROM [ContagemResultadoNotificacaoDestinatario] WITH (NOLOCK) WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo AND [ContagemResultadoNotificacao_DestCod] = @ContagemResultadoNotificacao_DestCod  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O39,1,0,true,false )
             ,new CursorDef("BC003O40", "INSERT INTO [ContagemResultadoNotificacaoDestinatario]([ContagemResultadoNotificacao_Codigo], [ContagemResultadoNotificacao_DestEmail], [ContagemResultadoNotificacao_DestCod]) VALUES(@ContagemResultadoNotificacao_Codigo, @ContagemResultadoNotificacao_DestEmail, @ContagemResultadoNotificacao_DestCod)", GxErrorMask.GX_NOMASK,prmBC003O40)
             ,new CursorDef("BC003O41", "UPDATE [ContagemResultadoNotificacaoDestinatario] SET [ContagemResultadoNotificacao_DestEmail]=@ContagemResultadoNotificacao_DestEmail  WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo AND [ContagemResultadoNotificacao_DestCod] = @ContagemResultadoNotificacao_DestCod", GxErrorMask.GX_NOMASK,prmBC003O41)
             ,new CursorDef("BC003O42", "DELETE FROM [ContagemResultadoNotificacaoDestinatario]  WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo AND [ContagemResultadoNotificacao_DestCod] = @ContagemResultadoNotificacao_DestCod", GxErrorMask.GX_NOMASK,prmBC003O42)
             ,new CursorDef("BC003O43", "SELECT [Usuario_PessoaCod] AS ContagemResultadoNotificacao_D FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoNotificacao_DestCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O43,1,0,true,false )
             ,new CursorDef("BC003O44", "SELECT [Pessoa_Nome] AS ContagemResultadoNotificacao_D FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoNotificacao_DestPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O44,1,0,true,false )
             ,new CursorDef("BC003O45", "SELECT T1.[ContagemResultadoNotificacao_Codigo], T3.[Pessoa_Nome] AS ContagemResultadoNotificacao_D, T1.[ContagemResultadoNotificacao_DestEmail], T1.[ContagemResultadoNotificacao_DestCod] AS ContagemResultadoNotificacao_D, T2.[Usuario_PessoaCod] AS ContagemResultadoNotificacao_D FROM (([ContagemResultadoNotificacaoDestinatario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultadoNotificacao_DestCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE T1.[ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo ORDER BY T1.[ContagemResultadoNotificacao_Codigo], T1.[ContagemResultadoNotificacao_DestCod]  OPTION (FAST 11)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O45,11,0,true,false )
             ,new CursorDef("BC003O46", "SELECT T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultadoNotificacao_Codigo], T2.[ContagemResultado_Demanda], T2.[ContagemResultado_DemandaFM], T4.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T2.[ContagemResultado_PrazoInicialDias], T1.[ContagemResultado_Codigo], T3.[Servico_Codigo] AS ContagemResultado_Servico FROM ((([ContagemResultadoNotificacaoDemanda] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T3.[Servico_Codigo]) WHERE T1.[ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo and T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY T1.[ContagemResultadoNotificacao_Codigo], T1.[ContagemResultado_Codigo]  OPTION (FAST 11)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O46,11,0,true,false )
             ,new CursorDef("BC003O47", "SELECT [ContagemResultadoNotificacao_Codigo], [ContagemResultado_Codigo] FROM [ContagemResultadoNotificacaoDemanda] WITH (NOLOCK) WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo AND [ContagemResultado_Codigo] = @ContagemResultado_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O47,1,0,true,false )
             ,new CursorDef("BC003O48", "INSERT INTO [ContagemResultadoNotificacaoDemanda]([ContagemResultadoNotificacao_Codigo], [ContagemResultado_Codigo]) VALUES(@ContagemResultadoNotificacao_Codigo, @ContagemResultado_Codigo)", GxErrorMask.GX_NOMASK,prmBC003O48)
             ,new CursorDef("BC003O49", "DELETE FROM [ContagemResultadoNotificacaoDemanda]  WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo AND [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK,prmBC003O49)
             ,new CursorDef("BC003O50", "SELECT [ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, [ContagemResultado_Demanda], [ContagemResultado_DemandaFM], [ContagemResultado_PrazoInicialDias] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O50,1,0,true,false )
             ,new CursorDef("BC003O51", "SELECT [Servico_Codigo] AS ContagemResultado_Servico FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContagemResultado_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O51,1,0,true,false )
             ,new CursorDef("BC003O52", "SELECT [Servico_Sigla] AS ContagemResultado_ServicoSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContagemResultado_Servico ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O52,1,0,true,false )
             ,new CursorDef("BC003O53", "SELECT T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultadoNotificacao_Codigo], T2.[ContagemResultado_Demanda], T2.[ContagemResultado_DemandaFM], T4.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T2.[ContagemResultado_PrazoInicialDias], T1.[ContagemResultado_Codigo], T3.[Servico_Codigo] AS ContagemResultado_Servico FROM ((([ContagemResultadoNotificacaoDemanda] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T3.[Servico_Codigo]) WHERE T1.[ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo ORDER BY T1.[ContagemResultadoNotificacao_Codigo], T1.[ContagemResultado_Codigo]  OPTION (FAST 11)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003O53,11,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((short[]) buf[6])[0] = rslt.getShort(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 7 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 3) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((short[]) buf[15])[0] = rslt.getShort(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((bool[]) buf[17])[0] = rslt.getBool(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((short[]) buf[19])[0] = rslt.getShort(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((short[]) buf[21])[0] = rslt.getShort(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((int[]) buf[23])[0] = rslt.getInt(13) ;
                ((int[]) buf[24])[0] = rslt.getInt(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((long[]) buf[26])[0] = rslt.getLong(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                return;
             case 11 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 3) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((short[]) buf[15])[0] = rslt.getShort(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((bool[]) buf[17])[0] = rslt.getBool(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((short[]) buf[19])[0] = rslt.getShort(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((short[]) buf[21])[0] = rslt.getShort(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((int[]) buf[23])[0] = rslt.getInt(13) ;
                ((int[]) buf[24])[0] = rslt.getInt(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((long[]) buf[26])[0] = rslt.getLong(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 17 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 3) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((short[]) buf[17])[0] = rslt.getShort(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((bool[]) buf[19])[0] = rslt.getBool(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((short[]) buf[21])[0] = rslt.getShort(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((short[]) buf[23])[0] = rslt.getShort(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((int[]) buf[25])[0] = rslt.getInt(14) ;
                ((int[]) buf[26])[0] = rslt.getInt(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((long[]) buf[28])[0] = rslt.getLong(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((int[]) buf[30])[0] = rslt.getInt(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((int[]) buf[32])[0] = rslt.getInt(18) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((int[]) buf[34])[0] = rslt.getInt(19) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(19);
                return;
             case 18 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 19 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 25 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 26 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 27 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 3) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((short[]) buf[17])[0] = rslt.getShort(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((bool[]) buf[19])[0] = rslt.getBool(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((short[]) buf[21])[0] = rslt.getShort(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((short[]) buf[23])[0] = rslt.getShort(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((int[]) buf[25])[0] = rslt.getInt(14) ;
                ((int[]) buf[26])[0] = rslt.getInt(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((long[]) buf[28])[0] = rslt.getLong(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((int[]) buf[30])[0] = rslt.getInt(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((int[]) buf[32])[0] = rslt.getInt(18) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((int[]) buf[34])[0] = rslt.getInt(19) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(19);
                return;
             case 28 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 29 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 33 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 34 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 35 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 36 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((long[]) buf[2])[0] = rslt.getLong(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((short[]) buf[9])[0] = rslt.getShort(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                return;
             case 37 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 40 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((short[]) buf[6])[0] = rslt.getShort(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
             case 41 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 42 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 43 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((long[]) buf[2])[0] = rslt.getLong(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((short[]) buf[9])[0] = rslt.getShort(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (long)parms[1]);
                }
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 16 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 19 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 8 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(8, (short)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 9 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(9, (bool)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 10 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(10, (short)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 11 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(11, (short)parms[21]);
                }
                stmt.SetParameter(12, (int)parms[22]);
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 13 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(13, (int)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 14 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(14, (long)parms[26]);
                }
                return;
             case 20 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 8 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(8, (short)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 9 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(9, (bool)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 10 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(10, (short)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 11 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(11, (short)parms[21]);
                }
                stmt.SetParameter(12, (int)parms[22]);
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 13 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(13, (int)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 14 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(14, (long)parms[26]);
                }
                stmt.SetParameter(15, (long)parms[27]);
                return;
             case 21 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 23 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 24 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 25 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 26 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 27 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 28 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 29 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                stmt.SetParameter(1, (long)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 31 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (long)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 32 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 33 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 34 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 35 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 36 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 37 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 38 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 39 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 40 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 41 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 42 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 43 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
       }
    }

 }

}
