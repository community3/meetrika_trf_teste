/*
               File: PRC_SrvHmlSemCnf
        Description: Servi�o homologa sem confer�ncia
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:35.41
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_srvhmlsemcnf : GXProcedure
   {
      public prc_srvhmlsemcnf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_srvhmlsemcnf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Contratada_Codigo ,
                           ref int aP1_Servico_Codigo ,
                           out bool aP2_Flag )
      {
         this.A39Contratada_Codigo = aP0_Contratada_Codigo;
         this.A155Servico_Codigo = aP1_Servico_Codigo;
         this.AV8Flag = false ;
         initialize();
         executePrivate();
         aP0_Contratada_Codigo=this.A39Contratada_Codigo;
         aP1_Servico_Codigo=this.A155Servico_Codigo;
         aP2_Flag=this.AV8Flag;
      }

      public bool executeUdp( ref int aP0_Contratada_Codigo ,
                              ref int aP1_Servico_Codigo )
      {
         this.A39Contratada_Codigo = aP0_Contratada_Codigo;
         this.A155Servico_Codigo = aP1_Servico_Codigo;
         this.AV8Flag = false ;
         initialize();
         executePrivate();
         aP0_Contratada_Codigo=this.A39Contratada_Codigo;
         aP1_Servico_Codigo=this.A155Servico_Codigo;
         aP2_Flag=this.AV8Flag;
         return AV8Flag ;
      }

      public void executeSubmit( ref int aP0_Contratada_Codigo ,
                                 ref int aP1_Servico_Codigo ,
                                 out bool aP2_Flag )
      {
         prc_srvhmlsemcnf objprc_srvhmlsemcnf;
         objprc_srvhmlsemcnf = new prc_srvhmlsemcnf();
         objprc_srvhmlsemcnf.A39Contratada_Codigo = aP0_Contratada_Codigo;
         objprc_srvhmlsemcnf.A155Servico_Codigo = aP1_Servico_Codigo;
         objprc_srvhmlsemcnf.AV8Flag = false ;
         objprc_srvhmlsemcnf.context.SetSubmitInitialConfig(context);
         objprc_srvhmlsemcnf.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_srvhmlsemcnf);
         aP0_Contratada_Codigo=this.A39Contratada_Codigo;
         aP1_Servico_Codigo=this.A155Servico_Codigo;
         aP2_Flag=this.AV8Flag;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_srvhmlsemcnf)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00672 */
         pr_default.execute(0, new Object[] {A39Contratada_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A74Contrato_Codigo = P00672_A74Contrato_Codigo[0];
            /* Using cursor P00673 */
            pr_default.execute(1, new Object[] {A74Contrato_Codigo, A155Servico_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A888ContratoServicos_HmlSemCnf = P00673_A888ContratoServicos_HmlSemCnf[0];
               n888ContratoServicos_HmlSemCnf = P00673_n888ContratoServicos_HmlSemCnf[0];
               A160ContratoServicos_Codigo = P00673_A160ContratoServicos_Codigo[0];
               AV8Flag = A888ContratoServicos_HmlSemCnf;
               pr_default.readNext(1);
            }
            pr_default.close(1);
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00672_A39Contratada_Codigo = new int[1] ;
         P00672_A74Contrato_Codigo = new int[1] ;
         P00673_A74Contrato_Codigo = new int[1] ;
         P00673_A155Servico_Codigo = new int[1] ;
         P00673_A888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         P00673_n888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         P00673_A160ContratoServicos_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_srvhmlsemcnf__default(),
            new Object[][] {
                new Object[] {
               P00672_A39Contratada_Codigo, P00672_A74Contrato_Codigo
               }
               , new Object[] {
               P00673_A74Contrato_Codigo, P00673_A155Servico_Codigo, P00673_A888ContratoServicos_HmlSemCnf, P00673_n888ContratoServicos_HmlSemCnf, P00673_A160ContratoServicos_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A39Contratada_Codigo ;
      private int A155Servico_Codigo ;
      private int A74Contrato_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private String scmdbuf ;
      private bool AV8Flag ;
      private bool A888ContratoServicos_HmlSemCnf ;
      private bool n888ContratoServicos_HmlSemCnf ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Contratada_Codigo ;
      private int aP1_Servico_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00672_A39Contratada_Codigo ;
      private int[] P00672_A74Contrato_Codigo ;
      private int[] P00673_A74Contrato_Codigo ;
      private int[] P00673_A155Servico_Codigo ;
      private bool[] P00673_A888ContratoServicos_HmlSemCnf ;
      private bool[] P00673_n888ContratoServicos_HmlSemCnf ;
      private int[] P00673_A160ContratoServicos_Codigo ;
      private bool aP2_Flag ;
   }

   public class prc_srvhmlsemcnf__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00672 ;
          prmP00672 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00673 ;
          prmP00673 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00672", "SELECT [Contratada_Codigo], [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ORDER BY [Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00672,100,0,true,false )
             ,new CursorDef("P00673", "SELECT [Contrato_Codigo], [Servico_Codigo], [ContratoServicos_HmlSemCnf], [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE ([Contrato_Codigo] = @Contrato_Codigo) AND ([Servico_Codigo] = @Servico_Codigo) ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00673,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
