/*
               File: WP_SntDmn
        Description: Sintpetico de Demandas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:24:5.34
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_sntdmn : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_sntdmn( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_sntdmn( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavContratada = new GXCombobox();
         cmbavStatus = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxSuggest"+"_"+"vAGRUPADOR") == 0 )
            {
               AV8AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8AreaTrabalho_Codigo), 6, 0)));
               A1046ContagemResultado_Agrupador = GetNextPar( );
               n1046ContagemResultado_Agrupador = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1046ContagemResultado_Agrupador", A1046ContagemResultado_Agrupador);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXSGVvAGRUPADORJW0( AV8AreaTrabalho_Codigo, A1046ContagemResultado_Agrupador) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATADA") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV7WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTRATADAJW2( AV7WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_48 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_48_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_48_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A52Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n52Contratada_AreaTrabalhoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
               A490ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n490ContagemResultado_ContratadaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A490ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0)));
               A484ContagemResultado_StatusDmn = GetNextPar( );
               n484ContagemResultado_StatusDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV7WWPContext);
               A1046ContagemResultado_Agrupador = GetNextPar( );
               n1046ContagemResultado_Agrupador = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1046ContagemResultado_Agrupador", A1046ContagemResultado_Agrupador);
               AV9Agrupador = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Agrupador", AV9Agrupador);
               AV17Contratada = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contratada), 6, 0)));
               A471ContagemResultado_DataDmn = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A471ContagemResultado_DataDmn", context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"));
               AV12InicioDmn = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12InicioDmn", context.localUtil.Format(AV12InicioDmn, "99/99/99"));
               AV13FimDmn = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13FimDmn", context.localUtil.Format(AV13FimDmn, "99/99/99"));
               A803ContagemResultado_ContratadaSigla = GetNextPar( );
               n803ContagemResultado_ContratadaSigla = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A803ContagemResultado_ContratadaSigla", A803ContagemResultado_ContratadaSigla);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, A52Contratada_AreaTrabalhoCod, A490ContagemResultado_ContratadaCod, A484ContagemResultado_StatusDmn, AV7WWPContext, A1046ContagemResultado_Agrupador, AV9Agrupador, AV17Contratada, A471ContagemResultado_DataDmn, AV12InicioDmn, AV13FimDmn, A803ContagemResultado_ContratadaSigla) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAJW2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTJW2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020621624540");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_sntdmn.aspx") +"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_48", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_48), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_STATUSDMN", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_AGRUPADOR", StringUtil.RTrim( A1046ContagemResultado_Agrupador));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATADMN", context.localUtil.DToC( A471ContagemResultado_DataDmn, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADASIGLA", StringUtil.RTrim( A803ContagemResultado_ContratadaSigla));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV7WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV7WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "</form>") ;
         }
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEJW2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTJW2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_sntdmn.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_SntDmn" ;
      }

      public override String GetPgmdesc( )
      {
         return "Sintpetico de Demandas" ;
      }

      protected void WBJW0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_JW2( true) ;
         }
         else
         {
            wb_table1_2_JW2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_JW2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTJW2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Sintpetico de Demandas", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPJW0( ) ;
      }

      protected void WSJW2( )
      {
         STARTJW2( ) ;
         EVTJW2( ) ;
      }

      protected void EVTJW2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11JW2 */
                              E11JW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOATUALIZAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12JW2 */
                              E12JW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_48_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_48_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_48_idx), 4, 0)), 4, "0");
                              SubsflControlProps_482( ) ;
                              AV16Sigla = StringUtil.Upper( cgiGet( edtavSigla_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSigla_Internalname, AV16Sigla);
                              cmbavStatus.Name = cmbavStatus_Internalname;
                              cmbavStatus.CurrentValue = cgiGet( cmbavStatus_Internalname);
                              AV5Status = cgiGet( cmbavStatus_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavStatus_Internalname, AV5Status);
                              AV6Qtde = (short)(context.localUtil.CToN( cgiGet( edtavQtde_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtde_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Qtde), 4, 0)));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13JW2 */
                                    E13JW2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14JW2 */
                                    E14JW2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEJW2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAJW2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavContratada.Name = "vCONTRATADA";
            dynavContratada.WebTags = "";
            GXCCtl = "vSTATUS_" + sGXsfl_48_idx;
            cmbavStatus.Name = GXCCtl;
            cmbavStatus.WebTags = "";
            cmbavStatus.addItem("B", "Stand by", 0);
            cmbavStatus.addItem("S", "Solicitada", 0);
            cmbavStatus.addItem("E", "Em An�lise", 0);
            cmbavStatus.addItem("A", "Em execu��o", 0);
            cmbavStatus.addItem("R", "Resolvida", 0);
            cmbavStatus.addItem("C", "Conferida", 0);
            cmbavStatus.addItem("D", "Retornada", 0);
            cmbavStatus.addItem("H", "Homologada", 0);
            cmbavStatus.addItem("O", "Aceite", 0);
            cmbavStatus.addItem("P", "A Pagar", 0);
            cmbavStatus.addItem("L", "Liquidada", 0);
            cmbavStatus.addItem("X", "Cancelada", 0);
            cmbavStatus.addItem("N", "N�o Faturada", 0);
            cmbavStatus.addItem("J", "Planejamento", 0);
            cmbavStatus.addItem("I", "An�lise Planejamento", 0);
            cmbavStatus.addItem("T", "Validacao T�cnica", 0);
            cmbavStatus.addItem("Q", "Validacao Qualidade", 0);
            cmbavStatus.addItem("G", "Em Homologa��o", 0);
            cmbavStatus.addItem("M", "Valida��o Mensura��o", 0);
            cmbavStatus.addItem("U", "Rascunho", 0);
            if ( cmbavStatus.ItemCount > 0 )
            {
               AV5Status = cmbavStatus.getValidValue(AV5Status);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavStatus_Internalname, AV5Status);
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavContratada_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXSGVvAGRUPADORJW0( int AV8AreaTrabalho_Codigo ,
                                         String A1046ContagemResultado_Agrupador )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXSGVvAGRUPADOR_dataJW0( AV8AreaTrabalho_Codigo, A1046ContagemResultado_Agrupador) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXSGVvAGRUPADOR_dataJW0( int AV8AreaTrabalho_Codigo ,
                                              String A1046ContagemResultado_Agrupador )
      {
         l1046ContagemResultado_Agrupador = StringUtil.PadR( StringUtil.RTrim( A1046ContagemResultado_Agrupador), 15, "%");
         n1046ContagemResultado_Agrupador = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1046ContagemResultado_Agrupador", A1046ContagemResultado_Agrupador);
         /* Using cursor H00JW2 */
         pr_default.execute(0, new Object[] {l1046ContagemResultado_Agrupador, AV8AreaTrabalho_Codigo});
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.RTrim( H00JW2_A1046ContagemResultado_Agrupador[0]));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00JW2_A1046ContagemResultado_Agrupador[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvCONTRATADAJW2( wwpbaseobjects.SdtWWPContext AV7WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATADA_dataJW2( AV7WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATADA_htmlJW2( wwpbaseobjects.SdtWWPContext AV7WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATADA_dataJW2( AV7WWPContext) ;
         gxdynajaxindex = 1;
         dynavContratada.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratada.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratada.ItemCount > 0 )
         {
            AV17Contratada = (int)(NumberUtil.Val( dynavContratada.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17Contratada), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contratada), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATADA_dataJW2( wwpbaseobjects.SdtWWPContext AV7WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Todas)");
         /* Using cursor H00JW3 */
         pr_default.execute(1, new Object[] {AV7WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00JW3_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00JW3_A438Contratada_Sigla[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_482( ) ;
         while ( nGXsfl_48_idx <= nRC_GXsfl_48 )
         {
            sendrow_482( ) ;
            nGXsfl_48_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_48_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_48_idx+1));
            sGXsfl_48_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_48_idx), 4, 0)), 4, "0");
            SubsflControlProps_482( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       int A52Contratada_AreaTrabalhoCod ,
                                       int A490ContagemResultado_ContratadaCod ,
                                       String A484ContagemResultado_StatusDmn ,
                                       wwpbaseobjects.SdtWWPContext AV7WWPContext ,
                                       String A1046ContagemResultado_Agrupador ,
                                       String AV9Agrupador ,
                                       int AV17Contratada ,
                                       DateTime A471ContagemResultado_DataDmn ,
                                       DateTime AV12InicioDmn ,
                                       DateTime AV13FimDmn ,
                                       String A803ContagemResultado_ContratadaSigla )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFJW2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavContratada.ItemCount > 0 )
         {
            AV17Contratada = (int)(NumberUtil.Val( dynavContratada.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17Contratada), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contratada), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFJW2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavSigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSigla_Enabled), 5, 0)));
         cmbavStatus.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatus_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavStatus.Enabled), 5, 0)));
         edtavQtde_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtde_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtde_Enabled), 5, 0)));
      }

      protected void RFJW2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 48;
         nGXsfl_48_idx = 1;
         sGXsfl_48_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_48_idx), 4, 0)), 4, "0");
         SubsflControlProps_482( ) ;
         nGXsfl_48_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_482( ) ;
            /* Execute user event: E14JW2 */
            E14JW2 ();
            if ( ( GRID_nCurrentRecord > 0 ) && ( GRID_nGridOutOfScope == 0 ) && ( nGXsfl_48_idx == 1 ) )
            {
               GRID_nCurrentRecord = 0;
               GRID_nGridOutOfScope = 1;
               subgrid_firstpage( ) ;
               /* Execute user event: E14JW2 */
               E14JW2 ();
            }
            wbEnd = 48;
            WBJW0( ) ;
         }
         nGXsfl_48_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A52Contratada_AreaTrabalhoCod, A490ContagemResultado_ContratadaCod, A484ContagemResultado_StatusDmn, AV7WWPContext, A1046ContagemResultado_Agrupador, AV9Agrupador, AV17Contratada, A471ContagemResultado_DataDmn, AV12InicioDmn, AV13FimDmn, A803ContagemResultado_ContratadaSigla) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         if ( GRID_nEOF == 0 )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A52Contratada_AreaTrabalhoCod, A490ContagemResultado_ContratadaCod, A484ContagemResultado_StatusDmn, AV7WWPContext, A1046ContagemResultado_Agrupador, AV9Agrupador, AV17Contratada, A471ContagemResultado_DataDmn, AV12InicioDmn, AV13FimDmn, A803ContagemResultado_ContratadaSigla) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A52Contratada_AreaTrabalhoCod, A490ContagemResultado_ContratadaCod, A484ContagemResultado_StatusDmn, AV7WWPContext, A1046ContagemResultado_Agrupador, AV9Agrupador, AV17Contratada, A471ContagemResultado_DataDmn, AV12InicioDmn, AV13FimDmn, A803ContagemResultado_ContratadaSigla) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         subGrid_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A52Contratada_AreaTrabalhoCod, A490ContagemResultado_ContratadaCod, A484ContagemResultado_StatusDmn, AV7WWPContext, A1046ContagemResultado_Agrupador, AV9Agrupador, AV17Contratada, A471ContagemResultado_DataDmn, AV12InicioDmn, AV13FimDmn, A803ContagemResultado_ContratadaSigla) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A52Contratada_AreaTrabalhoCod, A490ContagemResultado_ContratadaCod, A484ContagemResultado_StatusDmn, AV7WWPContext, A1046ContagemResultado_Agrupador, AV9Agrupador, AV17Contratada, A471ContagemResultado_DataDmn, AV12InicioDmn, AV13FimDmn, A803ContagemResultado_ContratadaSigla) ;
         }
         return (int)(0) ;
      }

      protected void STRUPJW0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavSigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSigla_Enabled), 5, 0)));
         cmbavStatus.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatus_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavStatus.Enabled), 5, 0)));
         edtavQtde_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtde_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtde_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E13JW2 */
         E13JW2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvCONTRATADA_htmlJW2( AV7WWPContext) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            dynavContratada.Name = dynavContratada_Internalname;
            dynavContratada.CurrentValue = cgiGet( dynavContratada_Internalname);
            AV17Contratada = (int)(NumberUtil.Val( cgiGet( dynavContratada_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contratada), 6, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavIniciodmn_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Inicio Dmn"}), 1, "vINICIODMN");
               GX_FocusControl = edtavIniciodmn_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV12InicioDmn = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12InicioDmn", context.localUtil.Format(AV12InicioDmn, "99/99/99"));
            }
            else
            {
               AV12InicioDmn = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavIniciodmn_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12InicioDmn", context.localUtil.Format(AV12InicioDmn, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavFimdmn_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Fim Dmn"}), 1, "vFIMDMN");
               GX_FocusControl = edtavFimdmn_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13FimDmn = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13FimDmn", context.localUtil.Format(AV13FimDmn, "99/99/99"));
            }
            else
            {
               AV13FimDmn = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavFimdmn_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13FimDmn", context.localUtil.Format(AV13FimDmn, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavIniciocnt_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Inicio Cnt"}), 1, "vINICIOCNT");
               GX_FocusControl = edtavIniciocnt_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV14InicioCnt = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14InicioCnt", context.localUtil.Format(AV14InicioCnt, "99/99/99"));
            }
            else
            {
               AV14InicioCnt = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavIniciocnt_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14InicioCnt", context.localUtil.Format(AV14InicioCnt, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavFimcnt_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Fim Cnt"}), 1, "vFIMCNT");
               GX_FocusControl = edtavFimcnt_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV15FimCnt = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15FimCnt", context.localUtil.Format(AV15FimCnt, "99/99/99"));
            }
            else
            {
               AV15FimCnt = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavFimcnt_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15FimCnt", context.localUtil.Format(AV15FimCnt, "99/99/99"));
            }
            AV9Agrupador = StringUtil.Upper( cgiGet( edtavAgrupador_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Agrupador", AV9Agrupador);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavAreatrabalho_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavAreatrabalho_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vAREATRABALHO_CODIGO");
               GX_FocusControl = edtavAreatrabalho_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV8AreaTrabalho_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8AreaTrabalho_Codigo), 6, 0)));
            }
            else
            {
               AV8AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavAreatrabalho_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8AreaTrabalho_Codigo), 6, 0)));
            }
            /* Read saved values. */
            nRC_GXsfl_48 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_48"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E13JW2 */
         E13JW2 ();
         if (returnInSub) return;
      }

      protected void E13JW2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV7WWPContext) ;
         subGrid_Rows = 0;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         AV8AreaTrabalho_Codigo = AV7WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8AreaTrabalho_Codigo), 6, 0)));
         subgrid_gotopage( 1) ;
         AV13FimDmn = DateTimeUtil.ServerDate( context, "DEFAULT");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13FimDmn", context.localUtil.Format(AV13FimDmn, "99/99/99"));
         AV13FimDmn = DateTimeUtil.DateEndOfMonth( AV13FimDmn);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13FimDmn", context.localUtil.Format(AV13FimDmn, "99/99/99"));
         AV12InicioDmn = AV13FimDmn;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12InicioDmn", context.localUtil.Format(AV12InicioDmn, "99/99/99"));
         GXt_dtime1 = DateTimeUtil.ResetTime( AV12InicioDmn ) ;
         AV12InicioDmn = DateTimeUtil.ResetTime(DateTimeUtil.TAdd( GXt_dtime1, 86400*(-DateTimeUtil.Day( AV13FimDmn))));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12InicioDmn", context.localUtil.Format(AV12InicioDmn, "99/99/99"));
         GXt_dtime1 = DateTimeUtil.ResetTime( AV12InicioDmn ) ;
         AV12InicioDmn = DateTimeUtil.ResetTime(DateTimeUtil.TAdd( GXt_dtime1, 86400*(1)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12InicioDmn", context.localUtil.Format(AV12InicioDmn, "99/99/99"));
      }

      private void E14JW2( )
      {
         /* Grid_Load Routine */
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV9Agrupador ,
                                              AV17Contratada ,
                                              AV12InicioDmn ,
                                              AV13FimDmn ,
                                              A1046ContagemResultado_Agrupador ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A471ContagemResultado_DataDmn ,
                                              AV7WWPContext.gxTpr_Areatrabalho_codigo ,
                                              A52Contratada_AreaTrabalhoCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00JW4 */
         pr_default.execute(2, new Object[] {AV7WWPContext.gxTpr_Areatrabalho_codigo, AV9Agrupador, AV17Contratada, AV12InicioDmn, AV13FimDmn});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKJW3 = false;
            A52Contratada_AreaTrabalhoCod = H00JW4_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00JW4_n52Contratada_AreaTrabalhoCod[0];
            A484ContagemResultado_StatusDmn = H00JW4_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00JW4_n484ContagemResultado_StatusDmn[0];
            A490ContagemResultado_ContratadaCod = H00JW4_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00JW4_n490ContagemResultado_ContratadaCod[0];
            A471ContagemResultado_DataDmn = H00JW4_A471ContagemResultado_DataDmn[0];
            A1046ContagemResultado_Agrupador = H00JW4_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = H00JW4_n1046ContagemResultado_Agrupador[0];
            A803ContagemResultado_ContratadaSigla = H00JW4_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = H00JW4_n803ContagemResultado_ContratadaSigla[0];
            A52Contratada_AreaTrabalhoCod = H00JW4_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00JW4_n52Contratada_AreaTrabalhoCod[0];
            A803ContagemResultado_ContratadaSigla = H00JW4_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = H00JW4_n803ContagemResultado_ContratadaSigla[0];
            AV6Qtde = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtde_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Qtde), 4, 0)));
            AV5Status = A484ContagemResultado_StatusDmn;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavStatus_Internalname, AV5Status);
            AV16Sigla = A803ContagemResultado_ContratadaSigla;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSigla_Internalname, AV16Sigla);
            while ( (pr_default.getStatus(2) != 101) && ( H00JW4_A52Contratada_AreaTrabalhoCod[0] == A52Contratada_AreaTrabalhoCod ) && ( H00JW4_A490ContagemResultado_ContratadaCod[0] == A490ContagemResultado_ContratadaCod ) && ( StringUtil.StrCmp(H00JW4_A484ContagemResultado_StatusDmn[0], A484ContagemResultado_StatusDmn) == 0 ) )
            {
               BRKJW3 = false;
               A471ContagemResultado_DataDmn = H00JW4_A471ContagemResultado_DataDmn[0];
               A1046ContagemResultado_Agrupador = H00JW4_A1046ContagemResultado_Agrupador[0];
               n1046ContagemResultado_Agrupador = H00JW4_n1046ContagemResultado_Agrupador[0];
               AV6Qtde = (short)(AV6Qtde+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtde_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Qtde), 4, 0)));
               BRKJW3 = true;
               pr_default.readNext(2);
            }
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 48;
            }
            if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
            {
               sendrow_482( ) ;
               GRID_nEOF = 1;
               GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
               if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
               {
                  GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
               }
            }
            if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
            {
               GRID_nEOF = 0;
               GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            }
            GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
            if ( isFullAjaxMode( ) && ( nGXsfl_48_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(48, GridRow);
            }
            if ( ! BRKJW3 )
            {
               BRKJW3 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
         cmbavStatus.CurrentValue = StringUtil.RTrim( AV5Status);
      }

      protected void E11JW2( )
      {
         /* 'DoCleanFilters' Routine */
         AV9Agrupador = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Agrupador", AV9Agrupador);
         AV13FimDmn = DateTimeUtil.ServerDate( context, "DEFAULT");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13FimDmn", context.localUtil.Format(AV13FimDmn, "99/99/99"));
         AV13FimDmn = DateTimeUtil.DateEndOfMonth( AV13FimDmn);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13FimDmn", context.localUtil.Format(AV13FimDmn, "99/99/99"));
         AV12InicioDmn = AV13FimDmn;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12InicioDmn", context.localUtil.Format(AV12InicioDmn, "99/99/99"));
         GXt_dtime1 = DateTimeUtil.ResetTime( AV12InicioDmn ) ;
         AV12InicioDmn = DateTimeUtil.ResetTime(DateTimeUtil.TAdd( GXt_dtime1, 86400*(-DateTimeUtil.Day( AV13FimDmn))));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12InicioDmn", context.localUtil.Format(AV12InicioDmn, "99/99/99"));
         GXt_dtime1 = DateTimeUtil.ResetTime( AV12InicioDmn ) ;
         AV12InicioDmn = DateTimeUtil.ResetTime(DateTimeUtil.TAdd( GXt_dtime1, 86400*(1)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12InicioDmn", context.localUtil.Format(AV12InicioDmn, "99/99/99"));
      }

      protected void E12JW2( )
      {
         /* 'DoAtualizar' Routine */
         context.DoAjaxRefresh();
      }

      protected void wb_table1_2_JW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktitle_Internalname, "Sint�tico de Demandas", "", "", lblTextblocktitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_SntDmn.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_11_JW2( true) ;
         }
         else
         {
            wb_table2_11_JW2( false) ;
         }
         return  ;
      }

      protected void wb_table2_11_JW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_48_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAreatrabalho_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8AreaTrabalho_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV8AreaTrabalho_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAreatrabalho_codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_SntDmn.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_JW2e( true) ;
         }
         else
         {
            wb_table1_2_JW2e( false) ;
         }
      }

      protected void wb_table2_11_JW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table3_14_JW2( true) ;
         }
         else
         {
            wb_table3_14_JW2( false) ;
         }
         return  ;
      }

      protected void wb_table3_14_JW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_45_JW2( true) ;
         }
         else
         {
            wb_table4_45_JW2( false) ;
         }
         return  ;
      }

      protected void wb_table4_45_JW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_11_JW2e( true) ;
         }
         else
         {
            wb_table2_11_JW2e( false) ;
         }
      }

      protected void wb_table4_45_JW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"48\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Contratada") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Status") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Qtde") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV16Sigla));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavSigla_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV5Status));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavStatus.Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6Qtde), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavQtde_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 48 )
         {
            wbEnd = 0;
            nRC_GXsfl_48 = (short)(nGXsfl_48_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_45_JW2e( true) ;
         }
         else
         {
            wb_table4_45_JW2e( false) ;
         }
      }

      protected void wb_table3_14_JW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_17_JW2( true) ;
         }
         else
         {
            wb_table5_17_JW2( false) ;
         }
         return  ;
      }

      protected void wb_table5_17_JW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_14_JW2e( true) ;
         }
         else
         {
            wb_table3_14_JW2e( false) ;
         }
      }

      protected void wb_table5_17_JW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_Internalname, "Contratada", "", "", lblTextblockcontratada_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_SntDmn.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'" + sGXsfl_48_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratada, dynavContratada_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17Contratada), 6, 0)), 1, dynavContratada_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,22);\"", "", true, "HLP_WP_SntDmn.htm");
            dynavContratada.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17Contratada), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_Internalname, "Values", (String)(dynavContratada.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockiniciodmn_Internalname, "Demandas", "", "", lblTextblockiniciodmn_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_SntDmn.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'" + sGXsfl_48_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavIniciodmn_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavIniciodmn_Internalname, context.localUtil.Format(AV12InicioDmn, "99/99/99"), context.localUtil.Format( AV12InicioDmn, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,26);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavIniciodmn_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_WP_SntDmn.htm");
            GxWebStd.gx_bitmap( context, edtavIniciodmn_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_SntDmn.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'" + sGXsfl_48_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavFimdmn_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavFimdmn_Internalname, context.localUtil.Format(AV13FimDmn, "99/99/99"), context.localUtil.Format( AV13FimDmn, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFimdmn_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_WP_SntDmn.htm");
            GxWebStd.gx_bitmap( context, edtavFimdmn_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_SntDmn.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockiniciocnt_Internalname, "Demandas", "", "", lblTextblockiniciocnt_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_SntDmn.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'" + sGXsfl_48_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavIniciocnt_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavIniciocnt_Internalname, context.localUtil.Format(AV14InicioCnt, "99/99/99"), context.localUtil.Format( AV14InicioCnt, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,32);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavIniciocnt_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_WP_SntDmn.htm");
            GxWebStd.gx_bitmap( context, edtavIniciocnt_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_SntDmn.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_48_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavFimcnt_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavFimcnt_Internalname, context.localUtil.Format(AV15FimCnt, "99/99/99"), context.localUtil.Format( AV15FimCnt, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFimcnt_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_WP_SntDmn.htm");
            GxWebStd.gx_bitmap( context, edtavFimcnt_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_SntDmn.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockagrupador_Internalname, "Agrupador", "", "", lblTextblockagrupador_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_SntDmn.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_48_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAgrupador_Internalname, StringUtil.RTrim( AV9Agrupador), StringUtil.RTrim( context.localUtil.Format( AV9Agrupador, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAgrupador_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_SntDmn.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_SntDmn.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnatualizar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(48), 2, 0)+","+"null"+");", "Procurar", bttBtnatualizar_Jsonclick, 5, "Procurar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOATUALIZAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_SntDmn.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_17_JW2e( true) ;
         }
         else
         {
            wb_table5_17_JW2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAJW2( ) ;
         WSJW2( ) ;
         WEJW2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020621624582");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
            context.AddJavascriptSource("wp_sntdmn.js", "?2020621624582");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_482( )
      {
         edtavSigla_Internalname = "vSIGLA_"+sGXsfl_48_idx;
         cmbavStatus_Internalname = "vSTATUS_"+sGXsfl_48_idx;
         edtavQtde_Internalname = "vQTDE_"+sGXsfl_48_idx;
      }

      protected void SubsflControlProps_fel_482( )
      {
         edtavSigla_Internalname = "vSIGLA_"+sGXsfl_48_fel_idx;
         cmbavStatus_Internalname = "vSTATUS_"+sGXsfl_48_fel_idx;
         edtavQtde_Internalname = "vQTDE_"+sGXsfl_48_fel_idx;
      }

      protected void sendrow_482( )
      {
         SubsflControlProps_482( ) ;
         WBJW0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_48_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_48_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_48_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavSigla_Internalname,StringUtil.RTrim( AV16Sigla),StringUtil.RTrim( context.localUtil.Format( AV16Sigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavSigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavSigla_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)48,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_48_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "vSTATUS_" + sGXsfl_48_idx;
               cmbavStatus.Name = GXCCtl;
               cmbavStatus.WebTags = "";
               cmbavStatus.addItem("B", "Stand by", 0);
               cmbavStatus.addItem("S", "Solicitada", 0);
               cmbavStatus.addItem("E", "Em An�lise", 0);
               cmbavStatus.addItem("A", "Em execu��o", 0);
               cmbavStatus.addItem("R", "Resolvida", 0);
               cmbavStatus.addItem("C", "Conferida", 0);
               cmbavStatus.addItem("D", "Retornada", 0);
               cmbavStatus.addItem("H", "Homologada", 0);
               cmbavStatus.addItem("O", "Aceite", 0);
               cmbavStatus.addItem("P", "A Pagar", 0);
               cmbavStatus.addItem("L", "Liquidada", 0);
               cmbavStatus.addItem("X", "Cancelada", 0);
               cmbavStatus.addItem("N", "N�o Faturada", 0);
               cmbavStatus.addItem("J", "Planejamento", 0);
               cmbavStatus.addItem("I", "An�lise Planejamento", 0);
               cmbavStatus.addItem("T", "Validacao T�cnica", 0);
               cmbavStatus.addItem("Q", "Validacao Qualidade", 0);
               cmbavStatus.addItem("G", "Em Homologa��o", 0);
               cmbavStatus.addItem("M", "Valida��o Mensura��o", 0);
               cmbavStatus.addItem("U", "Rascunho", 0);
               if ( cmbavStatus.ItemCount > 0 )
               {
                  AV5Status = cmbavStatus.getValidValue(AV5Status);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavStatus_Internalname, AV5Status);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavStatus,(String)cmbavStatus_Internalname,StringUtil.RTrim( AV5Status),(short)1,(String)cmbavStatus_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,cmbavStatus.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbavStatus.CurrentValue = StringUtil.RTrim( AV5Status);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatus_Internalname, "Values", (String)(cmbavStatus.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavQtde_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6Qtde), 4, 0, ",", "")),((edtavQtde_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV6Qtde), "ZZZ9")) : context.localUtil.Format( (decimal)(AV6Qtde), "ZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavQtde_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavQtde_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)48,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GridContainer.AddRow(GridRow);
            nGXsfl_48_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_48_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_48_idx+1));
            sGXsfl_48_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_48_idx), 4, 0)), 4, "0");
            SubsflControlProps_482( ) ;
         }
         /* End function sendrow_482 */
      }

      protected void init_default_properties( )
      {
         lblTextblocktitle_Internalname = "TEXTBLOCKTITLE";
         lblTextblockcontratada_Internalname = "TEXTBLOCKCONTRATADA";
         dynavContratada_Internalname = "vCONTRATADA";
         lblTextblockiniciodmn_Internalname = "TEXTBLOCKINICIODMN";
         edtavIniciodmn_Internalname = "vINICIODMN";
         edtavFimdmn_Internalname = "vFIMDMN";
         lblTextblockiniciocnt_Internalname = "TEXTBLOCKINICIOCNT";
         edtavIniciocnt_Internalname = "vINICIOCNT";
         edtavFimcnt_Internalname = "vFIMCNT";
         lblTextblockagrupador_Internalname = "TEXTBLOCKAGRUPADOR";
         edtavAgrupador_Internalname = "vAGRUPADOR";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         bttBtnatualizar_Internalname = "BTNATUALIZAR";
         tblUnnamedtable2_Internalname = "UNNAMEDTABLE2";
         tblTablefilters_Internalname = "TABLEFILTERS";
         edtavSigla_Internalname = "vSIGLA";
         cmbavStatus_Internalname = "vSTATUS";
         edtavQtde_Internalname = "vQTDE";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavAreatrabalho_codigo_Internalname = "vAREATRABALHO_CODIGO";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavQtde_Jsonclick = "";
         cmbavStatus_Jsonclick = "";
         edtavSigla_Jsonclick = "";
         edtavAgrupador_Jsonclick = "";
         edtavFimcnt_Jsonclick = "";
         edtavIniciocnt_Jsonclick = "";
         edtavFimdmn_Jsonclick = "";
         edtavIniciodmn_Jsonclick = "";
         dynavContratada_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavQtde_Enabled = 0;
         cmbavStatus.Enabled = 0;
         edtavSigla_Enabled = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavAreatrabalho_codigo_Jsonclick = "";
         subGrid_Backcolorstyle = 3;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Sintpetico de Demandas";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV7WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1046ContagemResultado_Agrupador',fld:'CONTAGEMRESULTADO_AGRUPADOR',pic:'@!',nv:''},{av:'AV9Agrupador',fld:'vAGRUPADOR',pic:'@!',nv:''},{av:'AV17Contratada',fld:'vCONTRATADA',pic:'ZZZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV12InicioDmn',fld:'vINICIODMN',pic:'',nv:''},{av:'AV13FimDmn',fld:'vFIMDMN',pic:'',nv:''},{av:'A803ContagemResultado_ContratadaSigla',fld:'CONTAGEMRESULTADO_CONTRATADASIGLA',pic:'@!',nv:''}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E14JW2',iparms:[{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV7WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1046ContagemResultado_Agrupador',fld:'CONTAGEMRESULTADO_AGRUPADOR',pic:'@!',nv:''},{av:'AV9Agrupador',fld:'vAGRUPADOR',pic:'@!',nv:''},{av:'AV17Contratada',fld:'vCONTRATADA',pic:'ZZZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV12InicioDmn',fld:'vINICIODMN',pic:'',nv:''},{av:'AV13FimDmn',fld:'vFIMDMN',pic:'',nv:''},{av:'A803ContagemResultado_ContratadaSigla',fld:'CONTAGEMRESULTADO_CONTRATADASIGLA',pic:'@!',nv:''}],oparms:[{av:'AV6Qtde',fld:'vQTDE',pic:'ZZZ9',nv:0},{av:'AV5Status',fld:'vSTATUS',pic:'',nv:''},{av:'AV16Sigla',fld:'vSIGLA',pic:'@!',nv:''}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E11JW2',iparms:[],oparms:[{av:'AV9Agrupador',fld:'vAGRUPADOR',pic:'@!',nv:''},{av:'AV13FimDmn',fld:'vFIMDMN',pic:'',nv:''},{av:'AV12InicioDmn',fld:'vINICIODMN',pic:'',nv:''}]}");
         setEventMetadata("'DOATUALIZAR'","{handler:'E12JW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV7WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1046ContagemResultado_Agrupador',fld:'CONTAGEMRESULTADO_AGRUPADOR',pic:'@!',nv:''},{av:'AV9Agrupador',fld:'vAGRUPADOR',pic:'@!',nv:''},{av:'AV17Contratada',fld:'vCONTRATADA',pic:'ZZZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV12InicioDmn',fld:'vINICIODMN',pic:'',nv:''},{av:'AV13FimDmn',fld:'vFIMDMN',pic:'',nv:''},{av:'A803ContagemResultado_ContratadaSigla',fld:'CONTAGEMRESULTADO_CONTRATADASIGLA',pic:'@!',nv:''}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         A1046ContagemResultado_Agrupador = "";
         AV7WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         A484ContagemResultado_StatusDmn = "";
         AV9Agrupador = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         AV12InicioDmn = DateTime.MinValue;
         AV13FimDmn = DateTime.MinValue;
         A803ContagemResultado_ContratadaSigla = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV16Sigla = "";
         AV5Status = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         l1046ContagemResultado_Agrupador = "";
         H00JW2_A1046ContagemResultado_Agrupador = new String[] {""} ;
         H00JW2_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         H00JW3_A39Contratada_Codigo = new int[1] ;
         H00JW3_A438Contratada_Sigla = new String[] {""} ;
         H00JW3_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00JW3_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         GridContainer = new GXWebGrid( context);
         AV14InicioCnt = DateTime.MinValue;
         AV15FimCnt = DateTime.MinValue;
         H00JW4_A456ContagemResultado_Codigo = new int[1] ;
         H00JW4_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00JW4_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00JW4_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00JW4_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00JW4_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00JW4_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00JW4_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00JW4_A1046ContagemResultado_Agrupador = new String[] {""} ;
         H00JW4_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         H00JW4_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         H00JW4_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         GridRow = new GXWebRow();
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         sStyleString = "";
         lblTextblocktitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblTextblockcontratada_Jsonclick = "";
         lblTextblockiniciodmn_Jsonclick = "";
         lblTextblockiniciocnt_Jsonclick = "";
         lblTextblockagrupador_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         bttBtnatualizar_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_sntdmn__default(),
            new Object[][] {
                new Object[] {
               H00JW2_A1046ContagemResultado_Agrupador, H00JW2_n1046ContagemResultado_Agrupador
               }
               , new Object[] {
               H00JW3_A39Contratada_Codigo, H00JW3_A438Contratada_Sigla, H00JW3_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               H00JW4_A456ContagemResultado_Codigo, H00JW4_A52Contratada_AreaTrabalhoCod, H00JW4_n52Contratada_AreaTrabalhoCod, H00JW4_A484ContagemResultado_StatusDmn, H00JW4_n484ContagemResultado_StatusDmn, H00JW4_A490ContagemResultado_ContratadaCod, H00JW4_n490ContagemResultado_ContratadaCod, H00JW4_A471ContagemResultado_DataDmn, H00JW4_A1046ContagemResultado_Agrupador, H00JW4_n1046ContagemResultado_Agrupador,
               H00JW4_A803ContagemResultado_ContratadaSigla, H00JW4_n803ContagemResultado_ContratadaSigla
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavSigla_Enabled = 0;
         cmbavStatus.Enabled = 0;
         edtavQtde_Enabled = 0;
      }

      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_48 ;
      private short nGXsfl_48_idx=1 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short nGXWrapped ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short AV6Qtde ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_48_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short subGrid_Backstyle ;
      private int AV8AreaTrabalho_Codigo ;
      private int subGrid_Rows ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int AV17Contratada ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int edtavSigla_Enabled ;
      private int edtavQtde_Enabled ;
      private int GRID_nGridOutOfScope ;
      private int AV7WWPContext_gxTpr_Areatrabalho_codigo ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String A1046ContagemResultado_Agrupador ;
      private String sGXsfl_48_idx="0001" ;
      private String A484ContagemResultado_StatusDmn ;
      private String AV9Agrupador ;
      private String A803ContagemResultado_ContratadaSigla ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV16Sigla ;
      private String edtavSigla_Internalname ;
      private String cmbavStatus_Internalname ;
      private String AV5Status ;
      private String edtavQtde_Internalname ;
      private String GXCCtl ;
      private String dynavContratada_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String l1046ContagemResultado_Agrupador ;
      private String edtavIniciodmn_Internalname ;
      private String edtavFimdmn_Internalname ;
      private String edtavIniciocnt_Internalname ;
      private String edtavFimcnt_Internalname ;
      private String edtavAgrupador_Internalname ;
      private String edtavAreatrabalho_codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblTextblocktitle_Internalname ;
      private String lblTextblocktitle_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String edtavAreatrabalho_codigo_Jsonclick ;
      private String tblTableheader_Internalname ;
      private String tblUnnamedtable1_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablefilters_Internalname ;
      private String tblUnnamedtable2_Internalname ;
      private String lblTextblockcontratada_Internalname ;
      private String lblTextblockcontratada_Jsonclick ;
      private String dynavContratada_Jsonclick ;
      private String lblTextblockiniciodmn_Internalname ;
      private String lblTextblockiniciodmn_Jsonclick ;
      private String edtavIniciodmn_Jsonclick ;
      private String edtavFimdmn_Jsonclick ;
      private String lblTextblockiniciocnt_Internalname ;
      private String lblTextblockiniciocnt_Jsonclick ;
      private String edtavIniciocnt_Jsonclick ;
      private String edtavFimcnt_Jsonclick ;
      private String lblTextblockagrupador_Internalname ;
      private String lblTextblockagrupador_Jsonclick ;
      private String edtavAgrupador_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String bttBtnatualizar_Internalname ;
      private String bttBtnatualizar_Jsonclick ;
      private String sGXsfl_48_fel_idx="0001" ;
      private String ROClassString ;
      private String edtavSigla_Jsonclick ;
      private String cmbavStatus_Jsonclick ;
      private String edtavQtde_Jsonclick ;
      private DateTime GXt_dtime1 ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime AV12InicioDmn ;
      private DateTime AV13FimDmn ;
      private DateTime AV14InicioCnt ;
      private DateTime AV15FimCnt ;
      private bool entryPointCalled ;
      private bool n1046ContagemResultado_Agrupador ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n803ContagemResultado_ContratadaSigla ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool BRKJW3 ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavContratada ;
      private GXCombobox cmbavStatus ;
      private IDataStoreProvider pr_default ;
      private String[] H00JW2_A1046ContagemResultado_Agrupador ;
      private bool[] H00JW2_n1046ContagemResultado_Agrupador ;
      private int[] H00JW3_A39Contratada_Codigo ;
      private String[] H00JW3_A438Contratada_Sigla ;
      private int[] H00JW3_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00JW3_n52Contratada_AreaTrabalhoCod ;
      private int[] H00JW4_A456ContagemResultado_Codigo ;
      private int[] H00JW4_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00JW4_n52Contratada_AreaTrabalhoCod ;
      private String[] H00JW4_A484ContagemResultado_StatusDmn ;
      private bool[] H00JW4_n484ContagemResultado_StatusDmn ;
      private int[] H00JW4_A490ContagemResultado_ContratadaCod ;
      private bool[] H00JW4_n490ContagemResultado_ContratadaCod ;
      private DateTime[] H00JW4_A471ContagemResultado_DataDmn ;
      private String[] H00JW4_A1046ContagemResultado_Agrupador ;
      private bool[] H00JW4_n1046ContagemResultado_Agrupador ;
      private String[] H00JW4_A803ContagemResultado_ContratadaSigla ;
      private bool[] H00JW4_n803ContagemResultado_ContratadaSigla ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV7WWPContext ;
   }

   public class wp_sntdmn__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00JW4( IGxContext context ,
                                             String AV9Agrupador ,
                                             int AV17Contratada ,
                                             DateTime AV12InicioDmn ,
                                             DateTime AV13FimDmn ,
                                             String A1046ContagemResultado_Agrupador ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             int AV7WWPContext_gxTpr_Areatrabalho_codigo ,
                                             int A52Contratada_AreaTrabalhoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [5] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_Codigo], T2.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_Agrupador], T2.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (T2.[Contratada_AreaTrabalhoCod] = @AV7WWPCo_1Areatrabalho_codigo)";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV9Agrupador)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV9Agrupador)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! (0==AV17Contratada) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV17Contratada)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! (DateTime.MinValue==AV12InicioDmn) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV12InicioDmn)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! (DateTime.MinValue==AV13FimDmn) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV13FimDmn)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_StatusDmn]";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 2 :
                     return conditional_H00JW4(context, (String)dynConstraints[0] , (int)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (String)dynConstraints[4] , (int)dynConstraints[5] , (DateTime)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00JW2 ;
          prmH00JW2 = new Object[] {
          new Object[] {"@l1046ContagemResultado_Agrupador",SqlDbType.Char,15,0} ,
          new Object[] {"@AV8AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00JW3 ;
          prmH00JW3 = new Object[] {
          new Object[] {"@AV7WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00JW4 ;
          prmH00JW4 = new Object[] {
          new Object[] {"@AV7WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9Agrupador",SqlDbType.Char,15,0} ,
          new Object[] {"@AV17Contratada",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12InicioDmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV13FimDmn",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00JW2", "SELECT DISTINCT TOP 5 T1.[ContagemResultado_Agrupador] FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaOrigemCod]) WHERE (UPPER(T1.[ContagemResultado_Agrupador]) like UPPER(@l1046ContagemResultado_Agrupador)) AND (T2.[Contratada_AreaTrabalhoCod] = @AV8AreaTrabalho_Codigo) ORDER BY T1.[ContagemResultado_Agrupador] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JW2,0,0,true,false )
             ,new CursorDef("H00JW3", "SELECT [Contratada_Codigo], [Contratada_Sigla], [Contratada_AreaTrabalhoCod] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_AreaTrabalhoCod] = @AV7WWPCo_1Areatrabalho_codigo ORDER BY [Contratada_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JW3,0,0,true,false )
             ,new CursorDef("H00JW4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JW4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(5) ;
                ((String[]) buf[8])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[9]);
                }
                return;
       }
    }

 }

}
