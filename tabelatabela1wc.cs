/*
               File: TabelaTabela1WC
        Description: Tabela Tabela1 WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/4/2020 8:38:6.45
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class tabelatabela1wc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public tabelatabela1wc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public tabelatabela1wc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Tabela_MelhoraCod )
      {
         this.AV7Tabela_MelhoraCod = aP0_Tabela_MelhoraCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkTabela_Ativo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7Tabela_MelhoraCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Tabela_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Tabela_MelhoraCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7Tabela_MelhoraCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_90 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_90_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_90_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
                  AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
                  AV16DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
                  AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
                  AV18Tabela_Nome1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Tabela_Nome1", AV18Tabela_Nome1);
                  AV19Tabela_SistemaDes1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Tabela_SistemaDes1", AV19Tabela_SistemaDes1);
                  AV20Tabela_ModuloDes1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20Tabela_ModuloDes1", AV20Tabela_ModuloDes1);
                  AV21Tabela_PaiNom1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21Tabela_PaiNom1", AV21Tabela_PaiNom1);
                  AV23DynamicFiltersSelector2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersSelector2", AV23DynamicFiltersSelector2);
                  AV24DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator2), 4, 0)));
                  AV25Tabela_Nome2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25Tabela_Nome2", AV25Tabela_Nome2);
                  AV26Tabela_SistemaDes2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Tabela_SistemaDes2", AV26Tabela_SistemaDes2);
                  AV27Tabela_ModuloDes2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27Tabela_ModuloDes2", AV27Tabela_ModuloDes2);
                  AV28Tabela_PaiNom2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Tabela_PaiNom2", AV28Tabela_PaiNom2);
                  AV30DynamicFiltersSelector3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersSelector3", AV30DynamicFiltersSelector3);
                  AV31DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator3), 4, 0)));
                  AV32Tabela_Nome3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32Tabela_Nome3", AV32Tabela_Nome3);
                  AV33Tabela_SistemaDes3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33Tabela_SistemaDes3", AV33Tabela_SistemaDes3);
                  AV34Tabela_ModuloDes3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34Tabela_ModuloDes3", AV34Tabela_ModuloDes3);
                  AV35Tabela_PaiNom3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35Tabela_PaiNom3", AV35Tabela_PaiNom3);
                  AV22DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersEnabled2", AV22DynamicFiltersEnabled2);
                  AV29DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersEnabled3", AV29DynamicFiltersEnabled3);
                  AV7Tabela_MelhoraCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Tabela_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Tabela_MelhoraCod), 6, 0)));
                  AV51Pgmname = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
                  AV37DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37DynamicFiltersIgnoreFirst", AV37DynamicFiltersIgnoreFirst);
                  AV36DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36DynamicFiltersRemoving", AV36DynamicFiltersRemoving);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
                  A172Tabela_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A190Tabela_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV39Tabela_ModuloCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39Tabela_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39Tabela_ModuloCod), 6, 0)));
                  A188Tabela_ModuloCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n188Tabela_ModuloCod = false;
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Tabela_Nome1, AV19Tabela_SistemaDes1, AV20Tabela_ModuloDes1, AV21Tabela_PaiNom1, AV23DynamicFiltersSelector2, AV24DynamicFiltersOperator2, AV25Tabela_Nome2, AV26Tabela_SistemaDes2, AV27Tabela_ModuloDes2, AV28Tabela_PaiNom2, AV30DynamicFiltersSelector3, AV31DynamicFiltersOperator3, AV32Tabela_Nome3, AV33Tabela_SistemaDes3, AV34Tabela_ModuloDes3, AV35Tabela_PaiNom3, AV22DynamicFiltersEnabled2, AV29DynamicFiltersEnabled3, AV7Tabela_MelhoraCod, AV51Pgmname, AV11GridState, AV37DynamicFiltersIgnoreFirst, AV36DynamicFiltersRemoving, AV6WWPContext, A172Tabela_Codigo, A190Tabela_SistemaCod, AV39Tabela_ModuloCod, A188Tabela_ModuloCod, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAPR2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV51Pgmname = "TabelaTabela1WC";
               context.Gx_err = 0;
               WSPR2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Tabela Tabela1 WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202054838697");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("tabelatabela1wc.aspx") + "?" + UrlEncode("" +AV7Tabela_MelhoraCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTABELA_NOME1", StringUtil.RTrim( AV18Tabela_Nome1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTABELA_SISTEMADES1", AV19Tabela_SistemaDes1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTABELA_MODULODES1", StringUtil.RTrim( AV20Tabela_ModuloDes1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTABELA_PAINOM1", StringUtil.RTrim( AV21Tabela_PaiNom1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2", AV23DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTABELA_NOME2", StringUtil.RTrim( AV25Tabela_Nome2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTABELA_SISTEMADES2", AV26Tabela_SistemaDes2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTABELA_MODULODES2", StringUtil.RTrim( AV27Tabela_ModuloDes2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTABELA_PAINOM2", StringUtil.RTrim( AV28Tabela_PaiNom2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3", AV30DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTABELA_NOME3", StringUtil.RTrim( AV32Tabela_Nome3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTABELA_SISTEMADES3", AV33Tabela_SistemaDes3);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTABELA_MODULODES3", StringUtil.RTrim( AV34Tabela_ModuloDes3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTABELA_PAINOM3", StringUtil.RTrim( AV35Tabela_PaiNom3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV22DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV29DynamicFiltersEnabled3));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_90", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_90), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45GridPageCount), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7Tabela_MelhoraCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7Tabela_MelhoraCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vTABELA_MELHORACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Tabela_MelhoraCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV51Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSIGNOREFIRST", AV37DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSREMOVING", AV36DynamicFiltersRemoving);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"vTABELA_MODULOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39Tabela_ModuloCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormPR2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("tabelatabela1wc.js", "?202054838751");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "TabelaTabela1WC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Tabela Tabela1 WC" ;
      }

      protected void WBPR0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "tabelatabela1wc.aspx");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
            }
            wb_table1_2_PR2( true) ;
         }
         else
         {
            wb_table1_2_PR2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_PR2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtTabela_MelhoraCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A746Tabela_MelhoraCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A746Tabela_MelhoraCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTabela_MelhoraCod_Jsonclick, 0, "Attribute", "", "", "", edtTabela_MelhoraCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_TabelaTabela1WC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'" + sPrefix + "',false,'" + sGXsfl_90_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(109, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,109);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'" + sPrefix + "',false,'" + sGXsfl_90_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV29DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(110, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,110);\"");
         }
         wbLoad = true;
      }

      protected void STARTPR2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Tabela Tabela1 WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPPR0( ) ;
            }
         }
      }

      protected void WSPR2( )
      {
         STARTPR2( ) ;
         EVTPR2( ) ;
      }

      protected void EVTPR2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPPR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPPR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11PR2 */
                                    E11PR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPPR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12PR2 */
                                    E12PR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPPR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13PR2 */
                                    E13PR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPPR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14PR2 */
                                    E14PR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPPR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15PR2 */
                                    E15PR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPPR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16PR2 */
                                    E16PR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPPR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E17PR2 */
                                    E17PR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPPR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E18PR2 */
                                    E18PR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPPR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E19PR2 */
                                    E19PR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPPR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E20PR2 */
                                    E20PR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPPR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E21PR2 */
                                    E21PR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPPR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPPR0( ) ;
                              }
                              nGXsfl_90_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_90_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_90_idx), 4, 0)), 4, "0");
                              SubsflControlProps_902( ) ;
                              AV38Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV38Update)) ? AV48Update_GXI : context.convertURL( context.PathToRelativeUrl( AV38Update))));
                              AV40Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV40Delete)) ? AV49Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV40Delete))));
                              AV41Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV41Display)) ? AV50Display_GXI : context.convertURL( context.PathToRelativeUrl( AV41Display))));
                              A172Tabela_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTabela_Codigo_Internalname), ",", "."));
                              A173Tabela_Nome = StringUtil.Upper( cgiGet( edtTabela_Nome_Internalname));
                              A175Tabela_Descricao = cgiGet( edtTabela_Descricao_Internalname);
                              n175Tabela_Descricao = false;
                              A190Tabela_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtTabela_SistemaCod_Internalname), ",", "."));
                              A191Tabela_SistemaDes = StringUtil.Upper( cgiGet( edtTabela_SistemaDes_Internalname));
                              n191Tabela_SistemaDes = false;
                              A188Tabela_ModuloCod = (int)(context.localUtil.CToN( cgiGet( edtTabela_ModuloCod_Internalname), ",", "."));
                              n188Tabela_ModuloCod = false;
                              A189Tabela_ModuloDes = StringUtil.Upper( cgiGet( edtTabela_ModuloDes_Internalname));
                              n189Tabela_ModuloDes = false;
                              A181Tabela_PaiCod = (int)(context.localUtil.CToN( cgiGet( edtTabela_PaiCod_Internalname), ",", "."));
                              n181Tabela_PaiCod = false;
                              A182Tabela_PaiNom = StringUtil.Upper( cgiGet( edtTabela_PaiNom_Internalname));
                              n182Tabela_PaiNom = false;
                              A174Tabela_Ativo = StringUtil.StrToBool( cgiGet( chkTabela_Ativo_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E22PR2 */
                                          E22PR2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E23PR2 */
                                          E23PR2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E24PR2 */
                                          E24PR2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tabela_nome1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_NOME1"), AV18Tabela_Nome1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tabela_sistemades1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_SISTEMADES1"), AV19Tabela_SistemaDes1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tabela_modulodes1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_MODULODES1"), AV20Tabela_ModuloDes1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tabela_painom1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_PAINOM1"), AV21Tabela_PaiNom1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV23DynamicFiltersSelector2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator2 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tabela_nome2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_NOME2"), AV25Tabela_Nome2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tabela_sistemades2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_SISTEMADES2"), AV26Tabela_SistemaDes2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tabela_modulodes2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_MODULODES2"), AV27Tabela_ModuloDes2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tabela_painom2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_PAINOM2"), AV28Tabela_PaiNom2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV30DynamicFiltersSelector3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV31DynamicFiltersOperator3 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tabela_nome3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_NOME3"), AV32Tabela_Nome3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tabela_sistemades3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_SISTEMADES3"), AV33Tabela_SistemaDes3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tabela_modulodes3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_MODULODES3"), AV34Tabela_ModuloDes3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tabela_painom3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_PAINOM3"), AV35Tabela_PaiNom3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV22DynamicFiltersEnabled2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV29DynamicFiltersEnabled3 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPPR0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEPR2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormPR2( ) ;
            }
         }
      }

      protected void PAPR2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("TABELA_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("TABELA_SISTEMADES", "Sistema", 0);
            cmbavDynamicfiltersselector1.addItem("TABELA_MODULODES", "Des", 0);
            cmbavDynamicfiltersselector1.addItem("TABELA_PAINOM", "Tabela", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("TABELA_NOME", "Nome", 0);
            cmbavDynamicfiltersselector2.addItem("TABELA_SISTEMADES", "Sistema", 0);
            cmbavDynamicfiltersselector2.addItem("TABELA_MODULODES", "Des", 0);
            cmbavDynamicfiltersselector2.addItem("TABELA_PAINOM", "Tabela", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV23DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersSelector2", AV23DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV24DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("TABELA_NOME", "Nome", 0);
            cmbavDynamicfiltersselector3.addItem("TABELA_SISTEMADES", "Sistema", 0);
            cmbavDynamicfiltersselector3.addItem("TABELA_MODULODES", "Des", 0);
            cmbavDynamicfiltersselector3.addItem("TABELA_PAINOM", "Tabela", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV30DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV30DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersSelector3", AV30DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV31DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator3), 4, 0)));
            }
            GXCCtl = "TABELA_ATIVO_" + sGXsfl_90_idx;
            chkTabela_Ativo.Name = GXCCtl;
            chkTabela_Ativo.WebTags = "";
            chkTabela_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkTabela_Ativo_Internalname, "TitleCaption", chkTabela_Ativo.Caption);
            chkTabela_Ativo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_902( ) ;
         while ( nGXsfl_90_idx <= nRC_GXsfl_90 )
         {
            sendrow_902( ) ;
            nGXsfl_90_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_90_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_90_idx+1));
            sGXsfl_90_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_90_idx), 4, 0)), 4, "0");
            SubsflControlProps_902( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       short AV17DynamicFiltersOperator1 ,
                                       String AV18Tabela_Nome1 ,
                                       String AV19Tabela_SistemaDes1 ,
                                       String AV20Tabela_ModuloDes1 ,
                                       String AV21Tabela_PaiNom1 ,
                                       String AV23DynamicFiltersSelector2 ,
                                       short AV24DynamicFiltersOperator2 ,
                                       String AV25Tabela_Nome2 ,
                                       String AV26Tabela_SistemaDes2 ,
                                       String AV27Tabela_ModuloDes2 ,
                                       String AV28Tabela_PaiNom2 ,
                                       String AV30DynamicFiltersSelector3 ,
                                       short AV31DynamicFiltersOperator3 ,
                                       String AV32Tabela_Nome3 ,
                                       String AV33Tabela_SistemaDes3 ,
                                       String AV34Tabela_ModuloDes3 ,
                                       String AV35Tabela_PaiNom3 ,
                                       bool AV22DynamicFiltersEnabled2 ,
                                       bool AV29DynamicFiltersEnabled3 ,
                                       int AV7Tabela_MelhoraCod ,
                                       String AV51Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       bool AV37DynamicFiltersIgnoreFirst ,
                                       bool AV36DynamicFiltersRemoving ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       int A172Tabela_Codigo ,
                                       int A190Tabela_SistemaCod ,
                                       int AV39Tabela_ModuloCod ,
                                       int A188Tabela_ModuloCod ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFPR2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A172Tabela_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"TABELA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A172Tabela_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A173Tabela_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"TABELA_NOME", StringUtil.RTrim( A173Tabela_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_DESCRICAO", GetSecureSignedToken( sPrefix, A175Tabela_Descricao));
         GxWebStd.gx_hidden_field( context, sPrefix+"TABELA_DESCRICAO", A175Tabela_Descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_MODULOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A188Tabela_ModuloCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"TABELA_MODULOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A188Tabela_ModuloCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_PAICOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A181Tabela_PaiCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"TABELA_PAICOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A181Tabela_PaiCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_ATIVO", GetSecureSignedToken( sPrefix, A174Tabela_Ativo));
         GxWebStd.gx_hidden_field( context, sPrefix+"TABELA_ATIVO", StringUtil.BoolToStr( A174Tabela_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV23DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersSelector2", AV23DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV24DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV30DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV30DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersSelector3", AV30DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV31DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFPR2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV51Pgmname = "TabelaTabela1WC";
         context.Gx_err = 0;
      }

      protected void RFPR2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 90;
         /* Execute user event: E23PR2 */
         E23PR2 ();
         nGXsfl_90_idx = 1;
         sGXsfl_90_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_90_idx), 4, 0)), 4, "0");
         SubsflControlProps_902( ) ;
         nGXsfl_90_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_902( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV17DynamicFiltersOperator1 ,
                                                 AV18Tabela_Nome1 ,
                                                 AV19Tabela_SistemaDes1 ,
                                                 AV20Tabela_ModuloDes1 ,
                                                 AV21Tabela_PaiNom1 ,
                                                 AV22DynamicFiltersEnabled2 ,
                                                 AV23DynamicFiltersSelector2 ,
                                                 AV24DynamicFiltersOperator2 ,
                                                 AV25Tabela_Nome2 ,
                                                 AV26Tabela_SistemaDes2 ,
                                                 AV27Tabela_ModuloDes2 ,
                                                 AV28Tabela_PaiNom2 ,
                                                 AV29DynamicFiltersEnabled3 ,
                                                 AV30DynamicFiltersSelector3 ,
                                                 AV31DynamicFiltersOperator3 ,
                                                 AV32Tabela_Nome3 ,
                                                 AV33Tabela_SistemaDes3 ,
                                                 AV34Tabela_ModuloDes3 ,
                                                 AV35Tabela_PaiNom3 ,
                                                 A173Tabela_Nome ,
                                                 A191Tabela_SistemaDes ,
                                                 A189Tabela_ModuloDes ,
                                                 A182Tabela_PaiNom ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc ,
                                                 A746Tabela_MelhoraCod ,
                                                 AV7Tabela_MelhoraCod },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                                 TypeConstants.BOOLEAN, TypeConstants.INT
                                                 }
            });
            lV18Tabela_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18Tabela_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Tabela_Nome1", AV18Tabela_Nome1);
            lV18Tabela_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18Tabela_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Tabela_Nome1", AV18Tabela_Nome1);
            lV19Tabela_SistemaDes1 = StringUtil.Concat( StringUtil.RTrim( AV19Tabela_SistemaDes1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Tabela_SistemaDes1", AV19Tabela_SistemaDes1);
            lV19Tabela_SistemaDes1 = StringUtil.Concat( StringUtil.RTrim( AV19Tabela_SistemaDes1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Tabela_SistemaDes1", AV19Tabela_SistemaDes1);
            lV20Tabela_ModuloDes1 = StringUtil.PadR( StringUtil.RTrim( AV20Tabela_ModuloDes1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20Tabela_ModuloDes1", AV20Tabela_ModuloDes1);
            lV20Tabela_ModuloDes1 = StringUtil.PadR( StringUtil.RTrim( AV20Tabela_ModuloDes1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20Tabela_ModuloDes1", AV20Tabela_ModuloDes1);
            lV21Tabela_PaiNom1 = StringUtil.PadR( StringUtil.RTrim( AV21Tabela_PaiNom1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21Tabela_PaiNom1", AV21Tabela_PaiNom1);
            lV21Tabela_PaiNom1 = StringUtil.PadR( StringUtil.RTrim( AV21Tabela_PaiNom1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21Tabela_PaiNom1", AV21Tabela_PaiNom1);
            lV25Tabela_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV25Tabela_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25Tabela_Nome2", AV25Tabela_Nome2);
            lV25Tabela_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV25Tabela_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25Tabela_Nome2", AV25Tabela_Nome2);
            lV26Tabela_SistemaDes2 = StringUtil.Concat( StringUtil.RTrim( AV26Tabela_SistemaDes2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Tabela_SistemaDes2", AV26Tabela_SistemaDes2);
            lV26Tabela_SistemaDes2 = StringUtil.Concat( StringUtil.RTrim( AV26Tabela_SistemaDes2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Tabela_SistemaDes2", AV26Tabela_SistemaDes2);
            lV27Tabela_ModuloDes2 = StringUtil.PadR( StringUtil.RTrim( AV27Tabela_ModuloDes2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27Tabela_ModuloDes2", AV27Tabela_ModuloDes2);
            lV27Tabela_ModuloDes2 = StringUtil.PadR( StringUtil.RTrim( AV27Tabela_ModuloDes2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27Tabela_ModuloDes2", AV27Tabela_ModuloDes2);
            lV28Tabela_PaiNom2 = StringUtil.PadR( StringUtil.RTrim( AV28Tabela_PaiNom2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Tabela_PaiNom2", AV28Tabela_PaiNom2);
            lV28Tabela_PaiNom2 = StringUtil.PadR( StringUtil.RTrim( AV28Tabela_PaiNom2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Tabela_PaiNom2", AV28Tabela_PaiNom2);
            lV32Tabela_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV32Tabela_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32Tabela_Nome3", AV32Tabela_Nome3);
            lV32Tabela_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV32Tabela_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32Tabela_Nome3", AV32Tabela_Nome3);
            lV33Tabela_SistemaDes3 = StringUtil.Concat( StringUtil.RTrim( AV33Tabela_SistemaDes3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33Tabela_SistemaDes3", AV33Tabela_SistemaDes3);
            lV33Tabela_SistemaDes3 = StringUtil.Concat( StringUtil.RTrim( AV33Tabela_SistemaDes3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33Tabela_SistemaDes3", AV33Tabela_SistemaDes3);
            lV34Tabela_ModuloDes3 = StringUtil.PadR( StringUtil.RTrim( AV34Tabela_ModuloDes3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34Tabela_ModuloDes3", AV34Tabela_ModuloDes3);
            lV34Tabela_ModuloDes3 = StringUtil.PadR( StringUtil.RTrim( AV34Tabela_ModuloDes3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34Tabela_ModuloDes3", AV34Tabela_ModuloDes3);
            lV35Tabela_PaiNom3 = StringUtil.PadR( StringUtil.RTrim( AV35Tabela_PaiNom3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35Tabela_PaiNom3", AV35Tabela_PaiNom3);
            lV35Tabela_PaiNom3 = StringUtil.PadR( StringUtil.RTrim( AV35Tabela_PaiNom3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35Tabela_PaiNom3", AV35Tabela_PaiNom3);
            /* Using cursor H00PR2 */
            pr_default.execute(0, new Object[] {AV7Tabela_MelhoraCod, lV18Tabela_Nome1, lV18Tabela_Nome1, lV19Tabela_SistemaDes1, lV19Tabela_SistemaDes1, lV20Tabela_ModuloDes1, lV20Tabela_ModuloDes1, lV21Tabela_PaiNom1, lV21Tabela_PaiNom1, lV25Tabela_Nome2, lV25Tabela_Nome2, lV26Tabela_SistemaDes2, lV26Tabela_SistemaDes2, lV27Tabela_ModuloDes2, lV27Tabela_ModuloDes2, lV28Tabela_PaiNom2, lV28Tabela_PaiNom2, lV32Tabela_Nome3, lV32Tabela_Nome3, lV33Tabela_SistemaDes3, lV33Tabela_SistemaDes3, lV34Tabela_ModuloDes3, lV34Tabela_ModuloDes3, lV35Tabela_PaiNom3, lV35Tabela_PaiNom3, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_90_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A746Tabela_MelhoraCod = H00PR2_A746Tabela_MelhoraCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A746Tabela_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A746Tabela_MelhoraCod), 6, 0)));
               n746Tabela_MelhoraCod = H00PR2_n746Tabela_MelhoraCod[0];
               A174Tabela_Ativo = H00PR2_A174Tabela_Ativo[0];
               A182Tabela_PaiNom = H00PR2_A182Tabela_PaiNom[0];
               n182Tabela_PaiNom = H00PR2_n182Tabela_PaiNom[0];
               A181Tabela_PaiCod = H00PR2_A181Tabela_PaiCod[0];
               n181Tabela_PaiCod = H00PR2_n181Tabela_PaiCod[0];
               A189Tabela_ModuloDes = H00PR2_A189Tabela_ModuloDes[0];
               n189Tabela_ModuloDes = H00PR2_n189Tabela_ModuloDes[0];
               A188Tabela_ModuloCod = H00PR2_A188Tabela_ModuloCod[0];
               n188Tabela_ModuloCod = H00PR2_n188Tabela_ModuloCod[0];
               A191Tabela_SistemaDes = H00PR2_A191Tabela_SistemaDes[0];
               n191Tabela_SistemaDes = H00PR2_n191Tabela_SistemaDes[0];
               A190Tabela_SistemaCod = H00PR2_A190Tabela_SistemaCod[0];
               A175Tabela_Descricao = H00PR2_A175Tabela_Descricao[0];
               n175Tabela_Descricao = H00PR2_n175Tabela_Descricao[0];
               A173Tabela_Nome = H00PR2_A173Tabela_Nome[0];
               A172Tabela_Codigo = H00PR2_A172Tabela_Codigo[0];
               A182Tabela_PaiNom = H00PR2_A182Tabela_PaiNom[0];
               n182Tabela_PaiNom = H00PR2_n182Tabela_PaiNom[0];
               A189Tabela_ModuloDes = H00PR2_A189Tabela_ModuloDes[0];
               n189Tabela_ModuloDes = H00PR2_n189Tabela_ModuloDes[0];
               A191Tabela_SistemaDes = H00PR2_A191Tabela_SistemaDes[0];
               n191Tabela_SistemaDes = H00PR2_n191Tabela_SistemaDes[0];
               /* Execute user event: E24PR2 */
               E24PR2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 90;
            WBPR0( ) ;
         }
         nGXsfl_90_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV16DynamicFiltersSelector1 ,
                                              AV17DynamicFiltersOperator1 ,
                                              AV18Tabela_Nome1 ,
                                              AV19Tabela_SistemaDes1 ,
                                              AV20Tabela_ModuloDes1 ,
                                              AV21Tabela_PaiNom1 ,
                                              AV22DynamicFiltersEnabled2 ,
                                              AV23DynamicFiltersSelector2 ,
                                              AV24DynamicFiltersOperator2 ,
                                              AV25Tabela_Nome2 ,
                                              AV26Tabela_SistemaDes2 ,
                                              AV27Tabela_ModuloDes2 ,
                                              AV28Tabela_PaiNom2 ,
                                              AV29DynamicFiltersEnabled3 ,
                                              AV30DynamicFiltersSelector3 ,
                                              AV31DynamicFiltersOperator3 ,
                                              AV32Tabela_Nome3 ,
                                              AV33Tabela_SistemaDes3 ,
                                              AV34Tabela_ModuloDes3 ,
                                              AV35Tabela_PaiNom3 ,
                                              A173Tabela_Nome ,
                                              A191Tabela_SistemaDes ,
                                              A189Tabela_ModuloDes ,
                                              A182Tabela_PaiNom ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc ,
                                              A746Tabela_MelhoraCod ,
                                              AV7Tabela_MelhoraCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV18Tabela_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18Tabela_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Tabela_Nome1", AV18Tabela_Nome1);
         lV18Tabela_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18Tabela_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Tabela_Nome1", AV18Tabela_Nome1);
         lV19Tabela_SistemaDes1 = StringUtil.Concat( StringUtil.RTrim( AV19Tabela_SistemaDes1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Tabela_SistemaDes1", AV19Tabela_SistemaDes1);
         lV19Tabela_SistemaDes1 = StringUtil.Concat( StringUtil.RTrim( AV19Tabela_SistemaDes1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Tabela_SistemaDes1", AV19Tabela_SistemaDes1);
         lV20Tabela_ModuloDes1 = StringUtil.PadR( StringUtil.RTrim( AV20Tabela_ModuloDes1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20Tabela_ModuloDes1", AV20Tabela_ModuloDes1);
         lV20Tabela_ModuloDes1 = StringUtil.PadR( StringUtil.RTrim( AV20Tabela_ModuloDes1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20Tabela_ModuloDes1", AV20Tabela_ModuloDes1);
         lV21Tabela_PaiNom1 = StringUtil.PadR( StringUtil.RTrim( AV21Tabela_PaiNom1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21Tabela_PaiNom1", AV21Tabela_PaiNom1);
         lV21Tabela_PaiNom1 = StringUtil.PadR( StringUtil.RTrim( AV21Tabela_PaiNom1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21Tabela_PaiNom1", AV21Tabela_PaiNom1);
         lV25Tabela_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV25Tabela_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25Tabela_Nome2", AV25Tabela_Nome2);
         lV25Tabela_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV25Tabela_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25Tabela_Nome2", AV25Tabela_Nome2);
         lV26Tabela_SistemaDes2 = StringUtil.Concat( StringUtil.RTrim( AV26Tabela_SistemaDes2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Tabela_SistemaDes2", AV26Tabela_SistemaDes2);
         lV26Tabela_SistemaDes2 = StringUtil.Concat( StringUtil.RTrim( AV26Tabela_SistemaDes2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Tabela_SistemaDes2", AV26Tabela_SistemaDes2);
         lV27Tabela_ModuloDes2 = StringUtil.PadR( StringUtil.RTrim( AV27Tabela_ModuloDes2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27Tabela_ModuloDes2", AV27Tabela_ModuloDes2);
         lV27Tabela_ModuloDes2 = StringUtil.PadR( StringUtil.RTrim( AV27Tabela_ModuloDes2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27Tabela_ModuloDes2", AV27Tabela_ModuloDes2);
         lV28Tabela_PaiNom2 = StringUtil.PadR( StringUtil.RTrim( AV28Tabela_PaiNom2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Tabela_PaiNom2", AV28Tabela_PaiNom2);
         lV28Tabela_PaiNom2 = StringUtil.PadR( StringUtil.RTrim( AV28Tabela_PaiNom2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Tabela_PaiNom2", AV28Tabela_PaiNom2);
         lV32Tabela_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV32Tabela_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32Tabela_Nome3", AV32Tabela_Nome3);
         lV32Tabela_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV32Tabela_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32Tabela_Nome3", AV32Tabela_Nome3);
         lV33Tabela_SistemaDes3 = StringUtil.Concat( StringUtil.RTrim( AV33Tabela_SistemaDes3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33Tabela_SistemaDes3", AV33Tabela_SistemaDes3);
         lV33Tabela_SistemaDes3 = StringUtil.Concat( StringUtil.RTrim( AV33Tabela_SistemaDes3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33Tabela_SistemaDes3", AV33Tabela_SistemaDes3);
         lV34Tabela_ModuloDes3 = StringUtil.PadR( StringUtil.RTrim( AV34Tabela_ModuloDes3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34Tabela_ModuloDes3", AV34Tabela_ModuloDes3);
         lV34Tabela_ModuloDes3 = StringUtil.PadR( StringUtil.RTrim( AV34Tabela_ModuloDes3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34Tabela_ModuloDes3", AV34Tabela_ModuloDes3);
         lV35Tabela_PaiNom3 = StringUtil.PadR( StringUtil.RTrim( AV35Tabela_PaiNom3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35Tabela_PaiNom3", AV35Tabela_PaiNom3);
         lV35Tabela_PaiNom3 = StringUtil.PadR( StringUtil.RTrim( AV35Tabela_PaiNom3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35Tabela_PaiNom3", AV35Tabela_PaiNom3);
         /* Using cursor H00PR3 */
         pr_default.execute(1, new Object[] {AV7Tabela_MelhoraCod, lV18Tabela_Nome1, lV18Tabela_Nome1, lV19Tabela_SistemaDes1, lV19Tabela_SistemaDes1, lV20Tabela_ModuloDes1, lV20Tabela_ModuloDes1, lV21Tabela_PaiNom1, lV21Tabela_PaiNom1, lV25Tabela_Nome2, lV25Tabela_Nome2, lV26Tabela_SistemaDes2, lV26Tabela_SistemaDes2, lV27Tabela_ModuloDes2, lV27Tabela_ModuloDes2, lV28Tabela_PaiNom2, lV28Tabela_PaiNom2, lV32Tabela_Nome3, lV32Tabela_Nome3, lV33Tabela_SistemaDes3, lV33Tabela_SistemaDes3, lV34Tabela_ModuloDes3, lV34Tabela_ModuloDes3, lV35Tabela_PaiNom3, lV35Tabela_PaiNom3});
         GRID_nRecordCount = H00PR3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Tabela_Nome1, AV19Tabela_SistemaDes1, AV20Tabela_ModuloDes1, AV21Tabela_PaiNom1, AV23DynamicFiltersSelector2, AV24DynamicFiltersOperator2, AV25Tabela_Nome2, AV26Tabela_SistemaDes2, AV27Tabela_ModuloDes2, AV28Tabela_PaiNom2, AV30DynamicFiltersSelector3, AV31DynamicFiltersOperator3, AV32Tabela_Nome3, AV33Tabela_SistemaDes3, AV34Tabela_ModuloDes3, AV35Tabela_PaiNom3, AV22DynamicFiltersEnabled2, AV29DynamicFiltersEnabled3, AV7Tabela_MelhoraCod, AV51Pgmname, AV11GridState, AV37DynamicFiltersIgnoreFirst, AV36DynamicFiltersRemoving, AV6WWPContext, A172Tabela_Codigo, A190Tabela_SistemaCod, AV39Tabela_ModuloCod, A188Tabela_ModuloCod, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Tabela_Nome1, AV19Tabela_SistemaDes1, AV20Tabela_ModuloDes1, AV21Tabela_PaiNom1, AV23DynamicFiltersSelector2, AV24DynamicFiltersOperator2, AV25Tabela_Nome2, AV26Tabela_SistemaDes2, AV27Tabela_ModuloDes2, AV28Tabela_PaiNom2, AV30DynamicFiltersSelector3, AV31DynamicFiltersOperator3, AV32Tabela_Nome3, AV33Tabela_SistemaDes3, AV34Tabela_ModuloDes3, AV35Tabela_PaiNom3, AV22DynamicFiltersEnabled2, AV29DynamicFiltersEnabled3, AV7Tabela_MelhoraCod, AV51Pgmname, AV11GridState, AV37DynamicFiltersIgnoreFirst, AV36DynamicFiltersRemoving, AV6WWPContext, A172Tabela_Codigo, A190Tabela_SistemaCod, AV39Tabela_ModuloCod, A188Tabela_ModuloCod, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Tabela_Nome1, AV19Tabela_SistemaDes1, AV20Tabela_ModuloDes1, AV21Tabela_PaiNom1, AV23DynamicFiltersSelector2, AV24DynamicFiltersOperator2, AV25Tabela_Nome2, AV26Tabela_SistemaDes2, AV27Tabela_ModuloDes2, AV28Tabela_PaiNom2, AV30DynamicFiltersSelector3, AV31DynamicFiltersOperator3, AV32Tabela_Nome3, AV33Tabela_SistemaDes3, AV34Tabela_ModuloDes3, AV35Tabela_PaiNom3, AV22DynamicFiltersEnabled2, AV29DynamicFiltersEnabled3, AV7Tabela_MelhoraCod, AV51Pgmname, AV11GridState, AV37DynamicFiltersIgnoreFirst, AV36DynamicFiltersRemoving, AV6WWPContext, A172Tabela_Codigo, A190Tabela_SistemaCod, AV39Tabela_ModuloCod, A188Tabela_ModuloCod, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Tabela_Nome1, AV19Tabela_SistemaDes1, AV20Tabela_ModuloDes1, AV21Tabela_PaiNom1, AV23DynamicFiltersSelector2, AV24DynamicFiltersOperator2, AV25Tabela_Nome2, AV26Tabela_SistemaDes2, AV27Tabela_ModuloDes2, AV28Tabela_PaiNom2, AV30DynamicFiltersSelector3, AV31DynamicFiltersOperator3, AV32Tabela_Nome3, AV33Tabela_SistemaDes3, AV34Tabela_ModuloDes3, AV35Tabela_PaiNom3, AV22DynamicFiltersEnabled2, AV29DynamicFiltersEnabled3, AV7Tabela_MelhoraCod, AV51Pgmname, AV11GridState, AV37DynamicFiltersIgnoreFirst, AV36DynamicFiltersRemoving, AV6WWPContext, A172Tabela_Codigo, A190Tabela_SistemaCod, AV39Tabela_ModuloCod, A188Tabela_ModuloCod, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Tabela_Nome1, AV19Tabela_SistemaDes1, AV20Tabela_ModuloDes1, AV21Tabela_PaiNom1, AV23DynamicFiltersSelector2, AV24DynamicFiltersOperator2, AV25Tabela_Nome2, AV26Tabela_SistemaDes2, AV27Tabela_ModuloDes2, AV28Tabela_PaiNom2, AV30DynamicFiltersSelector3, AV31DynamicFiltersOperator3, AV32Tabela_Nome3, AV33Tabela_SistemaDes3, AV34Tabela_ModuloDes3, AV35Tabela_PaiNom3, AV22DynamicFiltersEnabled2, AV29DynamicFiltersEnabled3, AV7Tabela_MelhoraCod, AV51Pgmname, AV11GridState, AV37DynamicFiltersIgnoreFirst, AV36DynamicFiltersRemoving, AV6WWPContext, A172Tabela_Codigo, A190Tabela_SistemaCod, AV39Tabela_ModuloCod, A188Tabela_ModuloCod, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPPR0( )
      {
         /* Before Start, stand alone formulas. */
         AV51Pgmname = "TabelaTabela1WC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E22PR2 */
         E22PR2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV14OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            AV18Tabela_Nome1 = StringUtil.Upper( cgiGet( edtavTabela_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Tabela_Nome1", AV18Tabela_Nome1);
            AV19Tabela_SistemaDes1 = StringUtil.Upper( cgiGet( edtavTabela_sistemades1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Tabela_SistemaDes1", AV19Tabela_SistemaDes1);
            AV20Tabela_ModuloDes1 = StringUtil.Upper( cgiGet( edtavTabela_modulodes1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20Tabela_ModuloDes1", AV20Tabela_ModuloDes1);
            AV21Tabela_PaiNom1 = StringUtil.Upper( cgiGet( edtavTabela_painom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21Tabela_PaiNom1", AV21Tabela_PaiNom1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV23DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersSelector2", AV23DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV24DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator2), 4, 0)));
            AV25Tabela_Nome2 = StringUtil.Upper( cgiGet( edtavTabela_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25Tabela_Nome2", AV25Tabela_Nome2);
            AV26Tabela_SistemaDes2 = StringUtil.Upper( cgiGet( edtavTabela_sistemades2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Tabela_SistemaDes2", AV26Tabela_SistemaDes2);
            AV27Tabela_ModuloDes2 = StringUtil.Upper( cgiGet( edtavTabela_modulodes2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27Tabela_ModuloDes2", AV27Tabela_ModuloDes2);
            AV28Tabela_PaiNom2 = StringUtil.Upper( cgiGet( edtavTabela_painom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Tabela_PaiNom2", AV28Tabela_PaiNom2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV30DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersSelector3", AV30DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV31DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator3), 4, 0)));
            AV32Tabela_Nome3 = StringUtil.Upper( cgiGet( edtavTabela_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32Tabela_Nome3", AV32Tabela_Nome3);
            AV33Tabela_SistemaDes3 = StringUtil.Upper( cgiGet( edtavTabela_sistemades3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33Tabela_SistemaDes3", AV33Tabela_SistemaDes3);
            AV34Tabela_ModuloDes3 = StringUtil.Upper( cgiGet( edtavTabela_modulodes3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34Tabela_ModuloDes3", AV34Tabela_ModuloDes3);
            AV35Tabela_PaiNom3 = StringUtil.Upper( cgiGet( edtavTabela_painom3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35Tabela_PaiNom3", AV35Tabela_PaiNom3);
            A746Tabela_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( edtTabela_MelhoraCod_Internalname), ",", "."));
            n746Tabela_MelhoraCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A746Tabela_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A746Tabela_MelhoraCod), 6, 0)));
            AV22DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersEnabled2", AV22DynamicFiltersEnabled2);
            AV29DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersEnabled3", AV29DynamicFiltersEnabled3);
            /* Read saved values. */
            nRC_GXsfl_90 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_90"), ",", "."));
            AV44GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV45GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7Tabela_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Tabela_MelhoraCod"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_NOME1"), AV18Tabela_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_SISTEMADES1"), AV19Tabela_SistemaDes1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_MODULODES1"), AV20Tabela_ModuloDes1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_PAINOM1"), AV21Tabela_PaiNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV23DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_NOME2"), AV25Tabela_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_SISTEMADES2"), AV26Tabela_SistemaDes2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_MODULODES2"), AV27Tabela_ModuloDes2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_PAINOM2"), AV28Tabela_PaiNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV30DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV31DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_NOME3"), AV32Tabela_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_SISTEMADES3"), AV33Tabela_SistemaDes3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_MODULODES3"), AV34Tabela_ModuloDes3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_PAINOM3"), AV35Tabela_PaiNom3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV22DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV29DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E22PR2 */
         E22PR2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22PR2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV16DynamicFiltersSelector1 = "TABELA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector2 = "TABELA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersSelector2", AV23DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersSelector3 = "TABELA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersSelector3", AV30DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtTabela_MelhoraCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtTabela_MelhoraCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTabela_MelhoraCod_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome", 0);
         cmbavOrderedby.addItem("2", "C�digo", 0);
         cmbavOrderedby.addItem("3", "Descri��o", 0);
         cmbavOrderedby.addItem("4", "Sistema", 0);
         cmbavOrderedby.addItem("5", "Sistema", 0);
         cmbavOrderedby.addItem("6", "Des", 0);
         cmbavOrderedby.addItem("7", "Tabela", 0);
         cmbavOrderedby.addItem("8", "Tabela", 0);
         cmbavOrderedby.addItem("9", "Ativo", 0);
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
      }

      protected void E23PR2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_SISTEMADES") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_MODULODES") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_PAINOM") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         if ( AV22DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_NOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_SISTEMADES") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_MODULODES") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_PAINOM") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            if ( AV29DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_NOME") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_SISTEMADES") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_MODULODES") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_PAINOM") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtTabela_Codigo_Titleformat = 2;
         edtTabela_Codigo_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV14OrderedBy==2) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "C�digo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtTabela_Codigo_Internalname, "Title", edtTabela_Codigo_Title);
         edtTabela_Nome_Titleformat = 2;
         edtTabela_Nome_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 1);' >%5</span>", ((AV14OrderedBy==1) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Nome", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtTabela_Nome_Internalname, "Title", edtTabela_Nome_Title);
         edtTabela_Descricao_Titleformat = 2;
         edtTabela_Descricao_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV14OrderedBy==3) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Descri��o", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtTabela_Descricao_Internalname, "Title", edtTabela_Descricao_Title);
         edtTabela_SistemaCod_Titleformat = 2;
         edtTabela_SistemaCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV14OrderedBy==4) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Sistema", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtTabela_SistemaCod_Internalname, "Title", edtTabela_SistemaCod_Title);
         edtTabela_SistemaDes_Titleformat = 2;
         edtTabela_SistemaDes_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 5);' >%5</span>", ((AV14OrderedBy==5) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Sistema", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtTabela_SistemaDes_Internalname, "Title", edtTabela_SistemaDes_Title);
         edtTabela_ModuloDes_Titleformat = 2;
         edtTabela_ModuloDes_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 6);' >%5</span>", ((AV14OrderedBy==6) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Des", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtTabela_ModuloDes_Internalname, "Title", edtTabela_ModuloDes_Title);
         edtTabela_PaiCod_Titleformat = 2;
         edtTabela_PaiCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 7);' >%5</span>", ((AV14OrderedBy==7) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Tabela", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtTabela_PaiCod_Internalname, "Title", edtTabela_PaiCod_Title);
         edtTabela_PaiNom_Titleformat = 2;
         edtTabela_PaiNom_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 8);' >%5</span>", ((AV14OrderedBy==8) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Tabela", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtTabela_PaiNom_Internalname, "Title", edtTabela_PaiNom_Title);
         chkTabela_Ativo_Titleformat = 2;
         chkTabela_Ativo.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 9);' >%5</span>", ((AV14OrderedBy==9) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Ativo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkTabela_Ativo_Internalname, "Title", chkTabela_Ativo.Title.Text);
         AV44GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44GridCurrentPage), 10, 0)));
         AV45GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6WWPContext", AV6WWPContext);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E11PR2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV43PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV43PageToGo) ;
         }
      }

      private void E24PR2( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( AV6WWPContext.gxTpr_Update )
         {
            edtavUpdate_Link = formatLink("tabela.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A172Tabela_Codigo) + "," + UrlEncode("" +A190Tabela_SistemaCod) + "," + UrlEncode("" +AV39Tabela_ModuloCod);
            AV38Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV38Update);
            AV48Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV38Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV38Update);
            AV48Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( AV6WWPContext.gxTpr_Delete )
         {
            edtavDelete_Link = formatLink("tabela.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A172Tabela_Codigo) + "," + UrlEncode("" +A190Tabela_SistemaCod) + "," + UrlEncode("" +AV39Tabela_ModuloCod);
            AV40Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV40Delete);
            AV49Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV40Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV40Delete);
            AV49Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtavDisplay_Tooltiptext = "Mostrar";
         if ( AV6WWPContext.gxTpr_Display )
         {
            edtavDisplay_Link = formatLink("viewtabela.aspx") + "?" + UrlEncode("" +A172Tabela_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
            AV41Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDisplay_Internalname, AV41Display);
            AV50Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
            edtavDisplay_Enabled = 1;
         }
         else
         {
            edtavDisplay_Link = "";
            AV41Display = context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDisplay_Internalname, AV41Display);
            AV50Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( )));
            edtavDisplay_Enabled = 0;
         }
         edtTabela_ModuloDes_Link = formatLink("viewmodulo.aspx") + "?" + UrlEncode("" +A188Tabela_ModuloCod) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 90;
         }
         sendrow_902( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_90_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(90, GridRow);
         }
      }

      protected void E12PR2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E17PR2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV22DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersEnabled2", AV22DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Tabela_Nome1, AV19Tabela_SistemaDes1, AV20Tabela_ModuloDes1, AV21Tabela_PaiNom1, AV23DynamicFiltersSelector2, AV24DynamicFiltersOperator2, AV25Tabela_Nome2, AV26Tabela_SistemaDes2, AV27Tabela_ModuloDes2, AV28Tabela_PaiNom2, AV30DynamicFiltersSelector3, AV31DynamicFiltersOperator3, AV32Tabela_Nome3, AV33Tabela_SistemaDes3, AV34Tabela_ModuloDes3, AV35Tabela_PaiNom3, AV22DynamicFiltersEnabled2, AV29DynamicFiltersEnabled3, AV7Tabela_MelhoraCod, AV51Pgmname, AV11GridState, AV37DynamicFiltersIgnoreFirst, AV36DynamicFiltersRemoving, AV6WWPContext, A172Tabela_Codigo, A190Tabela_SistemaCod, AV39Tabela_ModuloCod, A188Tabela_ModuloCod, sPrefix) ;
      }

      protected void E13PR2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV36DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36DynamicFiltersRemoving", AV36DynamicFiltersRemoving);
         AV37DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37DynamicFiltersIgnoreFirst", AV37DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV36DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36DynamicFiltersRemoving", AV36DynamicFiltersRemoving);
         AV37DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37DynamicFiltersIgnoreFirst", AV37DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Tabela_Nome1, AV19Tabela_SistemaDes1, AV20Tabela_ModuloDes1, AV21Tabela_PaiNom1, AV23DynamicFiltersSelector2, AV24DynamicFiltersOperator2, AV25Tabela_Nome2, AV26Tabela_SistemaDes2, AV27Tabela_ModuloDes2, AV28Tabela_PaiNom2, AV30DynamicFiltersSelector3, AV31DynamicFiltersOperator3, AV32Tabela_Nome3, AV33Tabela_SistemaDes3, AV34Tabela_ModuloDes3, AV35Tabela_PaiNom3, AV22DynamicFiltersEnabled2, AV29DynamicFiltersEnabled3, AV7Tabela_MelhoraCod, AV51Pgmname, AV11GridState, AV37DynamicFiltersIgnoreFirst, AV36DynamicFiltersRemoving, AV6WWPContext, A172Tabela_Codigo, A190Tabela_SistemaCod, AV39Tabela_ModuloCod, A188Tabela_ModuloCod, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV30DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E18PR2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV17DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E19PR2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV29DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersEnabled3", AV29DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Tabela_Nome1, AV19Tabela_SistemaDes1, AV20Tabela_ModuloDes1, AV21Tabela_PaiNom1, AV23DynamicFiltersSelector2, AV24DynamicFiltersOperator2, AV25Tabela_Nome2, AV26Tabela_SistemaDes2, AV27Tabela_ModuloDes2, AV28Tabela_PaiNom2, AV30DynamicFiltersSelector3, AV31DynamicFiltersOperator3, AV32Tabela_Nome3, AV33Tabela_SistemaDes3, AV34Tabela_ModuloDes3, AV35Tabela_PaiNom3, AV22DynamicFiltersEnabled2, AV29DynamicFiltersEnabled3, AV7Tabela_MelhoraCod, AV51Pgmname, AV11GridState, AV37DynamicFiltersIgnoreFirst, AV36DynamicFiltersRemoving, AV6WWPContext, A172Tabela_Codigo, A190Tabela_SistemaCod, AV39Tabela_ModuloCod, A188Tabela_ModuloCod, sPrefix) ;
      }

      protected void E14PR2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV36DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36DynamicFiltersRemoving", AV36DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersEnabled2", AV22DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV36DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36DynamicFiltersRemoving", AV36DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Tabela_Nome1, AV19Tabela_SistemaDes1, AV20Tabela_ModuloDes1, AV21Tabela_PaiNom1, AV23DynamicFiltersSelector2, AV24DynamicFiltersOperator2, AV25Tabela_Nome2, AV26Tabela_SistemaDes2, AV27Tabela_ModuloDes2, AV28Tabela_PaiNom2, AV30DynamicFiltersSelector3, AV31DynamicFiltersOperator3, AV32Tabela_Nome3, AV33Tabela_SistemaDes3, AV34Tabela_ModuloDes3, AV35Tabela_PaiNom3, AV22DynamicFiltersEnabled2, AV29DynamicFiltersEnabled3, AV7Tabela_MelhoraCod, AV51Pgmname, AV11GridState, AV37DynamicFiltersIgnoreFirst, AV36DynamicFiltersRemoving, AV6WWPContext, A172Tabela_Codigo, A190Tabela_SistemaCod, AV39Tabela_ModuloCod, A188Tabela_ModuloCod, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV30DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E20PR2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV24DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E15PR2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV36DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36DynamicFiltersRemoving", AV36DynamicFiltersRemoving);
         AV29DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersEnabled3", AV29DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV36DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36DynamicFiltersRemoving", AV36DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Tabela_Nome1, AV19Tabela_SistemaDes1, AV20Tabela_ModuloDes1, AV21Tabela_PaiNom1, AV23DynamicFiltersSelector2, AV24DynamicFiltersOperator2, AV25Tabela_Nome2, AV26Tabela_SistemaDes2, AV27Tabela_ModuloDes2, AV28Tabela_PaiNom2, AV30DynamicFiltersSelector3, AV31DynamicFiltersOperator3, AV32Tabela_Nome3, AV33Tabela_SistemaDes3, AV34Tabela_ModuloDes3, AV35Tabela_PaiNom3, AV22DynamicFiltersEnabled2, AV29DynamicFiltersEnabled3, AV7Tabela_MelhoraCod, AV51Pgmname, AV11GridState, AV37DynamicFiltersIgnoreFirst, AV36DynamicFiltersRemoving, AV6WWPContext, A172Tabela_Codigo, A190Tabela_SistemaCod, AV39Tabela_ModuloCod, A188Tabela_ModuloCod, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV30DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E21PR2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV31DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E16PR2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("tabela.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +0) + "," + UrlEncode("" +AV39Tabela_ModuloCod);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavTabela_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_nome1_Visible), 5, 0)));
         edtavTabela_sistemades1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_sistemades1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_sistemades1_Visible), 5, 0)));
         edtavTabela_modulodes1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_modulodes1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_modulodes1_Visible), 5, 0)));
         edtavTabela_painom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_painom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_painom1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_NOME") == 0 )
         {
            edtavTabela_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_SISTEMADES") == 0 )
         {
            edtavTabela_sistemades1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_sistemades1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_sistemades1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_MODULODES") == 0 )
         {
            edtavTabela_modulodes1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_modulodes1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_modulodes1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_PAINOM") == 0 )
         {
            edtavTabela_painom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_painom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_painom1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavTabela_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_nome2_Visible), 5, 0)));
         edtavTabela_sistemades2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_sistemades2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_sistemades2_Visible), 5, 0)));
         edtavTabela_modulodes2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_modulodes2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_modulodes2_Visible), 5, 0)));
         edtavTabela_painom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_painom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_painom2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_NOME") == 0 )
         {
            edtavTabela_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_SISTEMADES") == 0 )
         {
            edtavTabela_sistemades2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_sistemades2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_sistemades2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_MODULODES") == 0 )
         {
            edtavTabela_modulodes2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_modulodes2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_modulodes2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_PAINOM") == 0 )
         {
            edtavTabela_painom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_painom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_painom2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavTabela_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_nome3_Visible), 5, 0)));
         edtavTabela_sistemades3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_sistemades3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_sistemades3_Visible), 5, 0)));
         edtavTabela_modulodes3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_modulodes3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_modulodes3_Visible), 5, 0)));
         edtavTabela_painom3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_painom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_painom3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_NOME") == 0 )
         {
            edtavTabela_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_SISTEMADES") == 0 )
         {
            edtavTabela_sistemades3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_sistemades3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_sistemades3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_MODULODES") == 0 )
         {
            edtavTabela_modulodes3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_modulodes3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_modulodes3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_PAINOM") == 0 )
         {
            edtavTabela_painom3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_painom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_painom3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV22DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersEnabled2", AV22DynamicFiltersEnabled2);
         AV23DynamicFiltersSelector2 = "TABELA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersSelector2", AV23DynamicFiltersSelector2);
         AV24DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator2), 4, 0)));
         AV25Tabela_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25Tabela_Nome2", AV25Tabela_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersEnabled3", AV29DynamicFiltersEnabled3);
         AV30DynamicFiltersSelector3 = "TABELA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersSelector3", AV30DynamicFiltersSelector3);
         AV31DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator3), 4, 0)));
         AV32Tabela_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32Tabela_Nome3", AV32Tabela_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV42Session.Get(AV51Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV51Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV42Session.Get(AV51Pgmname+"GridState"), "");
         }
         AV14OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         AV15OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S192( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_NOME") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV18Tabela_Nome1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Tabela_Nome1", AV18Tabela_Nome1);
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_SISTEMADES") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV19Tabela_SistemaDes1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Tabela_SistemaDes1", AV19Tabela_SistemaDes1);
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_MODULODES") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV20Tabela_ModuloDes1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20Tabela_ModuloDes1", AV20Tabela_ModuloDes1);
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_PAINOM") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV21Tabela_PaiNom1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21Tabela_PaiNom1", AV21Tabela_PaiNom1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV22DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersEnabled2", AV22DynamicFiltersEnabled2);
               AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(2));
               AV23DynamicFiltersSelector2 = AV13GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersSelector2", AV23DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_NOME") == 0 )
               {
                  AV24DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator2), 4, 0)));
                  AV25Tabela_Nome2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25Tabela_Nome2", AV25Tabela_Nome2);
               }
               else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_SISTEMADES") == 0 )
               {
                  AV24DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator2), 4, 0)));
                  AV26Tabela_SistemaDes2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Tabela_SistemaDes2", AV26Tabela_SistemaDes2);
               }
               else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_MODULODES") == 0 )
               {
                  AV24DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator2), 4, 0)));
                  AV27Tabela_ModuloDes2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27Tabela_ModuloDes2", AV27Tabela_ModuloDes2);
               }
               else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_PAINOM") == 0 )
               {
                  AV24DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator2), 4, 0)));
                  AV28Tabela_PaiNom2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Tabela_PaiNom2", AV28Tabela_PaiNom2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV29DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersEnabled3", AV29DynamicFiltersEnabled3);
                  AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(3));
                  AV30DynamicFiltersSelector3 = AV13GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersSelector3", AV30DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_NOME") == 0 )
                  {
                     AV31DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator3), 4, 0)));
                     AV32Tabela_Nome3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32Tabela_Nome3", AV32Tabela_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_SISTEMADES") == 0 )
                  {
                     AV31DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator3), 4, 0)));
                     AV33Tabela_SistemaDes3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33Tabela_SistemaDes3", AV33Tabela_SistemaDes3);
                  }
                  else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_MODULODES") == 0 )
                  {
                     AV31DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator3), 4, 0)));
                     AV34Tabela_ModuloDes3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34Tabela_ModuloDes3", AV34Tabela_ModuloDes3);
                  }
                  else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_PAINOM") == 0 )
                  {
                     AV31DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator3), 4, 0)));
                     AV35Tabela_PaiNom3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35Tabela_PaiNom3", AV35Tabela_PaiNom3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV36DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV42Session.Get(AV51Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV51Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S172( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV37DynamicFiltersIgnoreFirst )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Tabela_Nome1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV18Tabela_Nome1;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_SISTEMADES") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Tabela_SistemaDes1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV19Tabela_SistemaDes1;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_MODULODES") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV20Tabela_ModuloDes1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV20Tabela_ModuloDes1;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_PAINOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21Tabela_PaiNom1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV21Tabela_PaiNom1;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            if ( AV36DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled2 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Tabela_Nome2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV25Tabela_Nome2;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_SISTEMADES") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV26Tabela_SistemaDes2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV26Tabela_SistemaDes2;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_MODULODES") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV27Tabela_ModuloDes2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV27Tabela_ModuloDes2;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_PAINOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Tabela_PaiNom2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV28Tabela_PaiNom2;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator2;
            }
            if ( AV36DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV29DynamicFiltersEnabled3 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV30DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV32Tabela_Nome3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV32Tabela_Nome3;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV31DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_SISTEMADES") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV33Tabela_SistemaDes3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV33Tabela_SistemaDes3;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV31DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_MODULODES") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV34Tabela_ModuloDes3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV34Tabela_ModuloDes3;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV31DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_PAINOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV35Tabela_PaiNom3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV35Tabela_PaiNom3;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV31DynamicFiltersOperator3;
            }
            if ( AV36DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV51Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "Tabela";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "Tabela_MelhoraCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Tabela_MelhoraCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV42Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_PR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_8_PR2( true) ;
         }
         else
         {
            wb_table2_8_PR2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_PR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_87_PR2( true) ;
         }
         else
         {
            wb_table3_87_PR2( false) ;
         }
         return  ;
      }

      protected void wb_table3_87_PR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_PR2e( true) ;
         }
         else
         {
            wb_table1_2_PR2e( false) ;
         }
      }

      protected void wb_table3_87_PR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"90\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTabela_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtTabela_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTabela_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTabela_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtTabela_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTabela_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTabela_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtTabela_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTabela_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTabela_SistemaCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtTabela_SistemaCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTabela_SistemaCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTabela_SistemaDes_Titleformat == 0 )
               {
                  context.SendWebValue( edtTabela_SistemaDes_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTabela_SistemaDes_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�d. M�dulo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTabela_ModuloDes_Titleformat == 0 )
               {
                  context.SendWebValue( edtTabela_ModuloDes_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTabela_ModuloDes_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTabela_PaiCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtTabela_PaiCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTabela_PaiCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTabela_PaiNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtTabela_PaiNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTabela_PaiNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkTabela_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkTabela_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkTabela_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV38Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV40Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV41Display));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A172Tabela_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTabela_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTabela_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A173Tabela_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTabela_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTabela_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A175Tabela_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTabela_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTabela_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A190Tabela_SistemaCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTabela_SistemaCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTabela_SistemaCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A191Tabela_SistemaDes);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTabela_SistemaDes_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTabela_SistemaDes_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A188Tabela_ModuloCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A189Tabela_ModuloDes));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTabela_ModuloDes_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTabela_ModuloDes_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtTabela_ModuloDes_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A181Tabela_PaiCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTabela_PaiCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTabela_PaiCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A182Tabela_PaiNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTabela_PaiNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTabela_PaiNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A174Tabela_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkTabela_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkTabela_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 90 )
         {
            wbEnd = 0;
            nRC_GXsfl_90 = (short)(nGXsfl_90_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_87_PR2e( true) ;
         }
         else
         {
            wb_table3_87_PR2e( false) ;
         }
      }

      protected void wb_table2_8_PR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Width100'>") ;
            wb_table4_11_PR2( true) ;
         }
         else
         {
            wb_table4_11_PR2( false) ;
         }
         return  ;
      }

      protected void wb_table4_11_PR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_TabelaTabela1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'" + sGXsfl_90_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_TabelaTabela1WC.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_TabelaTabela1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_21_PR2( true) ;
         }
         else
         {
            wb_table5_21_PR2( false) ;
         }
         return  ;
      }

      protected void wb_table5_21_PR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_PR2e( true) ;
         }
         else
         {
            wb_table2_8_PR2e( false) ;
         }
      }

      protected void wb_table5_21_PR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_24_PR2( true) ;
         }
         else
         {
            wb_table6_24_PR2( false) ;
         }
         return  ;
      }

      protected void wb_table6_24_PR2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_TabelaTabela1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_21_PR2e( true) ;
         }
         else
         {
            wb_table5_21_PR2e( false) ;
         }
      }

      protected void wb_table6_24_PR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_TabelaTabela1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'" + sPrefix + "',false,'" + sGXsfl_90_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,29);\"", "", true, "HLP_TabelaTabela1WC.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_TabelaTabela1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_33_PR2( true) ;
         }
         else
         {
            wb_table7_33_PR2( false) ;
         }
         return  ;
      }

      protected void wb_table7_33_PR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TabelaTabela1WC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TabelaTabela1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_TabelaTabela1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'" + sPrefix + "',false,'" + sGXsfl_90_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_TabelaTabela1WC.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_TabelaTabela1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_53_PR2( true) ;
         }
         else
         {
            wb_table8_53_PR2( false) ;
         }
         return  ;
      }

      protected void wb_table8_53_PR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TabelaTabela1WC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TabelaTabela1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_TabelaTabela1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'" + sPrefix + "',false,'" + sGXsfl_90_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV30DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", "", true, "HLP_TabelaTabela1WC.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV30DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_TabelaTabela1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_73_PR2( true) ;
         }
         else
         {
            wb_table9_73_PR2( false) ;
         }
         return  ;
      }

      protected void wb_table9_73_PR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TabelaTabela1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_24_PR2e( true) ;
         }
         else
         {
            wb_table6_24_PR2e( false) ;
         }
      }

      protected void wb_table9_73_PR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'" + sPrefix + "',false,'" + sGXsfl_90_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", "", true, "HLP_TabelaTabela1WC.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'" + sPrefix + "',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTabela_nome3_Internalname, StringUtil.RTrim( AV32Tabela_Nome3), StringUtil.RTrim( context.localUtil.Format( AV32Tabela_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,78);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTabela_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTabela_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_TabelaTabela1WC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'" + sPrefix + "',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTabela_sistemades3_Internalname, AV33Tabela_SistemaDes3, StringUtil.RTrim( context.localUtil.Format( AV33Tabela_SistemaDes3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,79);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTabela_sistemades3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTabela_sistemades3_Visible, 1, 0, "text", "", 300, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_TabelaTabela1WC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'" + sPrefix + "',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTabela_modulodes3_Internalname, StringUtil.RTrim( AV34Tabela_ModuloDes3), StringUtil.RTrim( context.localUtil.Format( AV34Tabela_ModuloDes3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,80);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTabela_modulodes3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTabela_modulodes3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_TabelaTabela1WC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'" + sPrefix + "',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTabela_painom3_Internalname, StringUtil.RTrim( AV35Tabela_PaiNom3), StringUtil.RTrim( context.localUtil.Format( AV35Tabela_PaiNom3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,81);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTabela_painom3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTabela_painom3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_TabelaTabela1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_73_PR2e( true) ;
         }
         else
         {
            wb_table9_73_PR2e( false) ;
         }
      }

      protected void wb_table8_53_PR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'" + sPrefix + "',false,'" + sGXsfl_90_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", "", true, "HLP_TabelaTabela1WC.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'" + sPrefix + "',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTabela_nome2_Internalname, StringUtil.RTrim( AV25Tabela_Nome2), StringUtil.RTrim( context.localUtil.Format( AV25Tabela_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,58);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTabela_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTabela_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_TabelaTabela1WC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'" + sPrefix + "',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTabela_sistemades2_Internalname, AV26Tabela_SistemaDes2, StringUtil.RTrim( context.localUtil.Format( AV26Tabela_SistemaDes2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,59);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTabela_sistemades2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTabela_sistemades2_Visible, 1, 0, "text", "", 300, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_TabelaTabela1WC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'" + sPrefix + "',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTabela_modulodes2_Internalname, StringUtil.RTrim( AV27Tabela_ModuloDes2), StringUtil.RTrim( context.localUtil.Format( AV27Tabela_ModuloDes2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,60);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTabela_modulodes2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTabela_modulodes2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_TabelaTabela1WC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'" + sPrefix + "',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTabela_painom2_Internalname, StringUtil.RTrim( AV28Tabela_PaiNom2), StringUtil.RTrim( context.localUtil.Format( AV28Tabela_PaiNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,61);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTabela_painom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTabela_painom2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_TabelaTabela1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_53_PR2e( true) ;
         }
         else
         {
            wb_table8_53_PR2e( false) ;
         }
      }

      protected void wb_table7_33_PR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'" + sPrefix + "',false,'" + sGXsfl_90_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", "", true, "HLP_TabelaTabela1WC.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'" + sPrefix + "',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTabela_nome1_Internalname, StringUtil.RTrim( AV18Tabela_Nome1), StringUtil.RTrim( context.localUtil.Format( AV18Tabela_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,38);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTabela_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTabela_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_TabelaTabela1WC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTabela_sistemades1_Internalname, AV19Tabela_SistemaDes1, StringUtil.RTrim( context.localUtil.Format( AV19Tabela_SistemaDes1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,39);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTabela_sistemades1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTabela_sistemades1_Visible, 1, 0, "text", "", 300, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_TabelaTabela1WC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'" + sPrefix + "',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTabela_modulodes1_Internalname, StringUtil.RTrim( AV20Tabela_ModuloDes1), StringUtil.RTrim( context.localUtil.Format( AV20Tabela_ModuloDes1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,40);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTabela_modulodes1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTabela_modulodes1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_TabelaTabela1WC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'" + sPrefix + "',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTabela_painom1_Internalname, StringUtil.RTrim( AV21Tabela_PaiNom1), StringUtil.RTrim( context.localUtil.Format( AV21Tabela_PaiNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,41);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTabela_painom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTabela_painom1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_TabelaTabela1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_33_PR2e( true) ;
         }
         else
         {
            wb_table7_33_PR2e( false) ;
         }
      }

      protected void wb_table4_11_PR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TabelaTabela1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_PR2e( true) ;
         }
         else
         {
            wb_table4_11_PR2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7Tabela_MelhoraCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Tabela_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Tabela_MelhoraCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAPR2( ) ;
         WSPR2( ) ;
         WEPR2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7Tabela_MelhoraCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAPR2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "tabelatabela1wc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAPR2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7Tabela_MelhoraCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Tabela_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Tabela_MelhoraCod), 6, 0)));
         }
         wcpOAV7Tabela_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Tabela_MelhoraCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7Tabela_MelhoraCod != wcpOAV7Tabela_MelhoraCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV7Tabela_MelhoraCod = AV7Tabela_MelhoraCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7Tabela_MelhoraCod = cgiGet( sPrefix+"AV7Tabela_MelhoraCod_CTRL");
         if ( StringUtil.Len( sCtrlAV7Tabela_MelhoraCod) > 0 )
         {
            AV7Tabela_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7Tabela_MelhoraCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Tabela_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Tabela_MelhoraCod), 6, 0)));
         }
         else
         {
            AV7Tabela_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7Tabela_MelhoraCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAPR2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSPR2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSPR2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7Tabela_MelhoraCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Tabela_MelhoraCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7Tabela_MelhoraCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7Tabela_MelhoraCod_CTRL", StringUtil.RTrim( sCtrlAV7Tabela_MelhoraCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEPR2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020548381246");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("tabelatabela1wc.js", "?2020548381247");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_902( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_90_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_90_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_90_idx;
         edtTabela_Codigo_Internalname = sPrefix+"TABELA_CODIGO_"+sGXsfl_90_idx;
         edtTabela_Nome_Internalname = sPrefix+"TABELA_NOME_"+sGXsfl_90_idx;
         edtTabela_Descricao_Internalname = sPrefix+"TABELA_DESCRICAO_"+sGXsfl_90_idx;
         edtTabela_SistemaCod_Internalname = sPrefix+"TABELA_SISTEMACOD_"+sGXsfl_90_idx;
         edtTabela_SistemaDes_Internalname = sPrefix+"TABELA_SISTEMADES_"+sGXsfl_90_idx;
         edtTabela_ModuloCod_Internalname = sPrefix+"TABELA_MODULOCOD_"+sGXsfl_90_idx;
         edtTabela_ModuloDes_Internalname = sPrefix+"TABELA_MODULODES_"+sGXsfl_90_idx;
         edtTabela_PaiCod_Internalname = sPrefix+"TABELA_PAICOD_"+sGXsfl_90_idx;
         edtTabela_PaiNom_Internalname = sPrefix+"TABELA_PAINOM_"+sGXsfl_90_idx;
         chkTabela_Ativo_Internalname = sPrefix+"TABELA_ATIVO_"+sGXsfl_90_idx;
      }

      protected void SubsflControlProps_fel_902( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_90_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_90_fel_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_90_fel_idx;
         edtTabela_Codigo_Internalname = sPrefix+"TABELA_CODIGO_"+sGXsfl_90_fel_idx;
         edtTabela_Nome_Internalname = sPrefix+"TABELA_NOME_"+sGXsfl_90_fel_idx;
         edtTabela_Descricao_Internalname = sPrefix+"TABELA_DESCRICAO_"+sGXsfl_90_fel_idx;
         edtTabela_SistemaCod_Internalname = sPrefix+"TABELA_SISTEMACOD_"+sGXsfl_90_fel_idx;
         edtTabela_SistemaDes_Internalname = sPrefix+"TABELA_SISTEMADES_"+sGXsfl_90_fel_idx;
         edtTabela_ModuloCod_Internalname = sPrefix+"TABELA_MODULOCOD_"+sGXsfl_90_fel_idx;
         edtTabela_ModuloDes_Internalname = sPrefix+"TABELA_MODULODES_"+sGXsfl_90_fel_idx;
         edtTabela_PaiCod_Internalname = sPrefix+"TABELA_PAICOD_"+sGXsfl_90_fel_idx;
         edtTabela_PaiNom_Internalname = sPrefix+"TABELA_PAINOM_"+sGXsfl_90_fel_idx;
         chkTabela_Ativo_Internalname = sPrefix+"TABELA_ATIVO_"+sGXsfl_90_fel_idx;
      }

      protected void sendrow_902( )
      {
         SubsflControlProps_902( ) ;
         WBPR0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_90_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_90_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_90_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV38Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV38Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV48Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV38Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV38Update)) ? AV48Update_GXI : context.PathToRelativeUrl( AV38Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV38Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV40Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV40Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV49Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV40Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV40Delete)) ? AV49Delete_GXI : context.PathToRelativeUrl( AV40Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV40Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV41Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV41Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV50Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV41Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV41Display)) ? AV50Display_GXI : context.PathToRelativeUrl( AV41Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDisplay_Enabled,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV41Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A172Tabela_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A172Tabela_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)90,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_Nome_Internalname,StringUtil.RTrim( A173Tabela_Nome),StringUtil.RTrim( context.localUtil.Format( A173Tabela_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)90,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_Descricao_Internalname,(String)A175Tabela_Descricao,(String)A175Tabela_Descricao,(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)90,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_SistemaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A190Tabela_SistemaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A190Tabela_SistemaCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_SistemaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)90,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_SistemaDes_Internalname,(String)A191Tabela_SistemaDes,StringUtil.RTrim( context.localUtil.Format( A191Tabela_SistemaDes, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_SistemaDes_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)90,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_ModuloCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A188Tabela_ModuloCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A188Tabela_ModuloCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_ModuloCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)90,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_ModuloDes_Internalname,StringUtil.RTrim( A189Tabela_ModuloDes),StringUtil.RTrim( context.localUtil.Format( A189Tabela_ModuloDes, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtTabela_ModuloDes_Link,(String)"",(String)"",(String)"",(String)edtTabela_ModuloDes_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)90,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_PaiCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A181Tabela_PaiCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A181Tabela_PaiCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_PaiCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)90,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_PaiNom_Internalname,StringUtil.RTrim( A182Tabela_PaiNom),StringUtil.RTrim( context.localUtil.Format( A182Tabela_PaiNom, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_PaiNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)90,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkTabela_Ativo_Internalname,StringUtil.BoolToStr( A174Tabela_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_CODIGO"+"_"+sGXsfl_90_idx, GetSecureSignedToken( sPrefix+sGXsfl_90_idx, context.localUtil.Format( (decimal)(A172Tabela_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_NOME"+"_"+sGXsfl_90_idx, GetSecureSignedToken( sPrefix+sGXsfl_90_idx, StringUtil.RTrim( context.localUtil.Format( A173Tabela_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_DESCRICAO"+"_"+sGXsfl_90_idx, GetSecureSignedToken( sPrefix+sGXsfl_90_idx, A175Tabela_Descricao));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_MODULOCOD"+"_"+sGXsfl_90_idx, GetSecureSignedToken( sPrefix+sGXsfl_90_idx, context.localUtil.Format( (decimal)(A188Tabela_ModuloCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_PAICOD"+"_"+sGXsfl_90_idx, GetSecureSignedToken( sPrefix+sGXsfl_90_idx, context.localUtil.Format( (decimal)(A181Tabela_PaiCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_ATIVO"+"_"+sGXsfl_90_idx, GetSecureSignedToken( sPrefix+sGXsfl_90_idx, A174Tabela_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_90_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_90_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_90_idx+1));
            sGXsfl_90_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_90_idx), 4, 0)), 4, "0");
            SubsflControlProps_902( ) ;
         }
         /* End function sendrow_902 */
      }

      protected void init_default_properties( )
      {
         imgInsert_Internalname = sPrefix+"INSERT";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         lblOrderedtext_Internalname = sPrefix+"ORDEREDTEXT";
         cmbavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR1";
         edtavTabela_nome1_Internalname = sPrefix+"vTABELA_NOME1";
         edtavTabela_sistemades1_Internalname = sPrefix+"vTABELA_SISTEMADES1";
         edtavTabela_modulodes1_Internalname = sPrefix+"vTABELA_MODULODES1";
         edtavTabela_painom1_Internalname = sPrefix+"vTABELA_PAINOM1";
         tblTablemergeddynamicfilters1_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = sPrefix+"ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = sPrefix+"REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = sPrefix+"DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR2";
         edtavTabela_nome2_Internalname = sPrefix+"vTABELA_NOME2";
         edtavTabela_sistemades2_Internalname = sPrefix+"vTABELA_SISTEMADES2";
         edtavTabela_modulodes2_Internalname = sPrefix+"vTABELA_MODULODES2";
         edtavTabela_painom2_Internalname = sPrefix+"vTABELA_PAINOM2";
         tblTablemergeddynamicfilters2_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = sPrefix+"ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = sPrefix+"REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = sPrefix+"DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR3";
         edtavTabela_nome3_Internalname = sPrefix+"vTABELA_NOME3";
         edtavTabela_sistemades3_Internalname = sPrefix+"vTABELA_SISTEMADES3";
         edtavTabela_modulodes3_Internalname = sPrefix+"vTABELA_MODULODES3";
         edtavTabela_painom3_Internalname = sPrefix+"vTABELA_PAINOM3";
         tblTablemergeddynamicfilters3_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = sPrefix+"REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = sPrefix+"JSDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtavDisplay_Internalname = sPrefix+"vDISPLAY";
         edtTabela_Codigo_Internalname = sPrefix+"TABELA_CODIGO";
         edtTabela_Nome_Internalname = sPrefix+"TABELA_NOME";
         edtTabela_Descricao_Internalname = sPrefix+"TABELA_DESCRICAO";
         edtTabela_SistemaCod_Internalname = sPrefix+"TABELA_SISTEMACOD";
         edtTabela_SistemaDes_Internalname = sPrefix+"TABELA_SISTEMADES";
         edtTabela_ModuloCod_Internalname = sPrefix+"TABELA_MODULOCOD";
         edtTabela_ModuloDes_Internalname = sPrefix+"TABELA_MODULODES";
         edtTabela_PaiCod_Internalname = sPrefix+"TABELA_PAICOD";
         edtTabela_PaiNom_Internalname = sPrefix+"TABELA_PAINOM";
         chkTabela_Ativo_Internalname = sPrefix+"TABELA_ATIVO";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtTabela_MelhoraCod_Internalname = sPrefix+"TABELA_MELHORACOD";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = sPrefix+"vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = sPrefix+"vDYNAMICFILTERSENABLED3";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtTabela_PaiNom_Jsonclick = "";
         edtTabela_PaiCod_Jsonclick = "";
         edtTabela_ModuloDes_Jsonclick = "";
         edtTabela_ModuloCod_Jsonclick = "";
         edtTabela_SistemaDes_Jsonclick = "";
         edtTabela_SistemaCod_Jsonclick = "";
         edtTabela_Descricao_Jsonclick = "";
         edtTabela_Nome_Jsonclick = "";
         edtTabela_Codigo_Jsonclick = "";
         edtavTabela_painom1_Jsonclick = "";
         edtavTabela_modulodes1_Jsonclick = "";
         edtavTabela_sistemades1_Jsonclick = "";
         edtavTabela_nome1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavTabela_painom2_Jsonclick = "";
         edtavTabela_modulodes2_Jsonclick = "";
         edtavTabela_sistemades2_Jsonclick = "";
         edtavTabela_nome2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavTabela_painom3_Jsonclick = "";
         edtavTabela_modulodes3_Jsonclick = "";
         edtavTabela_sistemades3_Jsonclick = "";
         edtavTabela_nome3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtTabela_ModuloDes_Link = "";
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDisplay_Enabled = 1;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         chkTabela_Ativo_Titleformat = 0;
         edtTabela_PaiNom_Titleformat = 0;
         edtTabela_PaiCod_Titleformat = 0;
         edtTabela_ModuloDes_Titleformat = 0;
         edtTabela_SistemaDes_Titleformat = 0;
         edtTabela_SistemaCod_Titleformat = 0;
         edtTabela_Descricao_Titleformat = 0;
         edtTabela_Nome_Titleformat = 0;
         edtTabela_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavTabela_painom3_Visible = 1;
         edtavTabela_modulodes3_Visible = 1;
         edtavTabela_sistemades3_Visible = 1;
         edtavTabela_nome3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavTabela_painom2_Visible = 1;
         edtavTabela_modulodes2_Visible = 1;
         edtavTabela_sistemades2_Visible = 1;
         edtavTabela_nome2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavTabela_painom1_Visible = 1;
         edtavTabela_modulodes1_Visible = 1;
         edtavTabela_sistemades1_Visible = 1;
         edtavTabela_nome1_Visible = 1;
         chkTabela_Ativo.Title.Text = "Ativo";
         edtTabela_PaiNom_Title = "Tabela";
         edtTabela_PaiCod_Title = "Tabela";
         edtTabela_ModuloDes_Title = "Des";
         edtTabela_SistemaDes_Title = "Sistema";
         edtTabela_SistemaCod_Title = "Sistema";
         edtTabela_Descricao_Title = "Descri��o";
         edtTabela_Nome_Title = "Nome";
         edtTabela_Codigo_Title = "C�digo";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkTabela_Ativo.Caption = "";
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtTabela_MelhoraCod_Jsonclick = "";
         edtTabela_MelhoraCod_Visible = 1;
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7Tabela_MelhoraCod',fld:'vTABELA_MELHORACOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV39Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'A188Tabela_ModuloCod',fld:'TABELA_MODULOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV29DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV30DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV51Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV37DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV18Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19Tabela_SistemaDes1',fld:'vTABELA_SISTEMADES1',pic:'@!',nv:''},{av:'AV20Tabela_ModuloDes1',fld:'vTABELA_MODULODES1',pic:'@!',nv:''},{av:'AV21Tabela_PaiNom1',fld:'vTABELA_PAINOM1',pic:'@!',nv:''},{av:'AV36DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV25Tabela_Nome2',fld:'vTABELA_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV26Tabela_SistemaDes2',fld:'vTABELA_SISTEMADES2',pic:'@!',nv:''},{av:'AV27Tabela_ModuloDes2',fld:'vTABELA_MODULODES2',pic:'@!',nv:''},{av:'AV28Tabela_PaiNom2',fld:'vTABELA_PAINOM2',pic:'@!',nv:''},{av:'AV32Tabela_Nome3',fld:'vTABELA_NOME3',pic:'@!',nv:''},{av:'AV31DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV33Tabela_SistemaDes3',fld:'vTABELA_SISTEMADES3',pic:'@!',nv:''},{av:'AV34Tabela_ModuloDes3',fld:'vTABELA_MODULODES3',pic:'@!',nv:''},{av:'AV35Tabela_PaiNom3',fld:'vTABELA_PAINOM3',pic:'@!',nv:''}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV24DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV31DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtTabela_Codigo_Titleformat',ctrl:'TABELA_CODIGO',prop:'Titleformat'},{av:'edtTabela_Codigo_Title',ctrl:'TABELA_CODIGO',prop:'Title'},{av:'edtTabela_Nome_Titleformat',ctrl:'TABELA_NOME',prop:'Titleformat'},{av:'edtTabela_Nome_Title',ctrl:'TABELA_NOME',prop:'Title'},{av:'edtTabela_Descricao_Titleformat',ctrl:'TABELA_DESCRICAO',prop:'Titleformat'},{av:'edtTabela_Descricao_Title',ctrl:'TABELA_DESCRICAO',prop:'Title'},{av:'edtTabela_SistemaCod_Titleformat',ctrl:'TABELA_SISTEMACOD',prop:'Titleformat'},{av:'edtTabela_SistemaCod_Title',ctrl:'TABELA_SISTEMACOD',prop:'Title'},{av:'edtTabela_SistemaDes_Titleformat',ctrl:'TABELA_SISTEMADES',prop:'Titleformat'},{av:'edtTabela_SistemaDes_Title',ctrl:'TABELA_SISTEMADES',prop:'Title'},{av:'edtTabela_ModuloDes_Titleformat',ctrl:'TABELA_MODULODES',prop:'Titleformat'},{av:'edtTabela_ModuloDes_Title',ctrl:'TABELA_MODULODES',prop:'Title'},{av:'edtTabela_PaiCod_Titleformat',ctrl:'TABELA_PAICOD',prop:'Titleformat'},{av:'edtTabela_PaiCod_Title',ctrl:'TABELA_PAICOD',prop:'Title'},{av:'edtTabela_PaiNom_Titleformat',ctrl:'TABELA_PAINOM',prop:'Titleformat'},{av:'edtTabela_PaiNom_Title',ctrl:'TABELA_PAINOM',prop:'Title'},{av:'chkTabela_Ativo_Titleformat',ctrl:'TABELA_ATIVO',prop:'Titleformat'},{av:'chkTabela_Ativo.Title.Text',ctrl:'TABELA_ATIVO',prop:'Title'},{av:'AV44GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV45GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11PR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV19Tabela_SistemaDes1',fld:'vTABELA_SISTEMADES1',pic:'@!',nv:''},{av:'AV20Tabela_ModuloDes1',fld:'vTABELA_MODULODES1',pic:'@!',nv:''},{av:'AV21Tabela_PaiNom1',fld:'vTABELA_PAINOM1',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV25Tabela_Nome2',fld:'vTABELA_NOME2',pic:'@!',nv:''},{av:'AV26Tabela_SistemaDes2',fld:'vTABELA_SISTEMADES2',pic:'@!',nv:''},{av:'AV27Tabela_ModuloDes2',fld:'vTABELA_MODULODES2',pic:'@!',nv:''},{av:'AV28Tabela_PaiNom2',fld:'vTABELA_PAINOM2',pic:'@!',nv:''},{av:'AV30DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV31DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV32Tabela_Nome3',fld:'vTABELA_NOME3',pic:'@!',nv:''},{av:'AV33Tabela_SistemaDes3',fld:'vTABELA_SISTEMADES3',pic:'@!',nv:''},{av:'AV34Tabela_ModuloDes3',fld:'vTABELA_MODULODES3',pic:'@!',nv:''},{av:'AV35Tabela_PaiNom3',fld:'vTABELA_PAINOM3',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV29DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Tabela_MelhoraCod',fld:'vTABELA_MELHORACOD',pic:'ZZZZZ9',nv:0},{av:'AV51Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV37DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV36DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV39Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'A188Tabela_ModuloCod',fld:'TABELA_MODULOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E24PR2',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV39Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'A188Tabela_ModuloCod',fld:'TABELA_MODULOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV38Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV40Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV41Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Enabled',ctrl:'vDISPLAY',prop:'Enabled'},{av:'edtTabela_ModuloDes_Link',ctrl:'TABELA_MODULODES',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E12PR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV19Tabela_SistemaDes1',fld:'vTABELA_SISTEMADES1',pic:'@!',nv:''},{av:'AV20Tabela_ModuloDes1',fld:'vTABELA_MODULODES1',pic:'@!',nv:''},{av:'AV21Tabela_PaiNom1',fld:'vTABELA_PAINOM1',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV25Tabela_Nome2',fld:'vTABELA_NOME2',pic:'@!',nv:''},{av:'AV26Tabela_SistemaDes2',fld:'vTABELA_SISTEMADES2',pic:'@!',nv:''},{av:'AV27Tabela_ModuloDes2',fld:'vTABELA_MODULODES2',pic:'@!',nv:''},{av:'AV28Tabela_PaiNom2',fld:'vTABELA_PAINOM2',pic:'@!',nv:''},{av:'AV30DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV31DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV32Tabela_Nome3',fld:'vTABELA_NOME3',pic:'@!',nv:''},{av:'AV33Tabela_SistemaDes3',fld:'vTABELA_SISTEMADES3',pic:'@!',nv:''},{av:'AV34Tabela_ModuloDes3',fld:'vTABELA_MODULODES3',pic:'@!',nv:''},{av:'AV35Tabela_PaiNom3',fld:'vTABELA_PAINOM3',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV29DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Tabela_MelhoraCod',fld:'vTABELA_MELHORACOD',pic:'ZZZZZ9',nv:0},{av:'AV51Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV37DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV36DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV39Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'A188Tabela_ModuloCod',fld:'TABELA_MODULOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E17PR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV19Tabela_SistemaDes1',fld:'vTABELA_SISTEMADES1',pic:'@!',nv:''},{av:'AV20Tabela_ModuloDes1',fld:'vTABELA_MODULODES1',pic:'@!',nv:''},{av:'AV21Tabela_PaiNom1',fld:'vTABELA_PAINOM1',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV25Tabela_Nome2',fld:'vTABELA_NOME2',pic:'@!',nv:''},{av:'AV26Tabela_SistemaDes2',fld:'vTABELA_SISTEMADES2',pic:'@!',nv:''},{av:'AV27Tabela_ModuloDes2',fld:'vTABELA_MODULODES2',pic:'@!',nv:''},{av:'AV28Tabela_PaiNom2',fld:'vTABELA_PAINOM2',pic:'@!',nv:''},{av:'AV30DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV31DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV32Tabela_Nome3',fld:'vTABELA_NOME3',pic:'@!',nv:''},{av:'AV33Tabela_SistemaDes3',fld:'vTABELA_SISTEMADES3',pic:'@!',nv:''},{av:'AV34Tabela_ModuloDes3',fld:'vTABELA_MODULODES3',pic:'@!',nv:''},{av:'AV35Tabela_PaiNom3',fld:'vTABELA_PAINOM3',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV29DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Tabela_MelhoraCod',fld:'vTABELA_MELHORACOD',pic:'ZZZZZ9',nv:0},{av:'AV51Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV37DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV36DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV39Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'A188Tabela_ModuloCod',fld:'TABELA_MODULOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E13PR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV19Tabela_SistemaDes1',fld:'vTABELA_SISTEMADES1',pic:'@!',nv:''},{av:'AV20Tabela_ModuloDes1',fld:'vTABELA_MODULODES1',pic:'@!',nv:''},{av:'AV21Tabela_PaiNom1',fld:'vTABELA_PAINOM1',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV25Tabela_Nome2',fld:'vTABELA_NOME2',pic:'@!',nv:''},{av:'AV26Tabela_SistemaDes2',fld:'vTABELA_SISTEMADES2',pic:'@!',nv:''},{av:'AV27Tabela_ModuloDes2',fld:'vTABELA_MODULODES2',pic:'@!',nv:''},{av:'AV28Tabela_PaiNom2',fld:'vTABELA_PAINOM2',pic:'@!',nv:''},{av:'AV30DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV31DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV32Tabela_Nome3',fld:'vTABELA_NOME3',pic:'@!',nv:''},{av:'AV33Tabela_SistemaDes3',fld:'vTABELA_SISTEMADES3',pic:'@!',nv:''},{av:'AV34Tabela_ModuloDes3',fld:'vTABELA_MODULODES3',pic:'@!',nv:''},{av:'AV35Tabela_PaiNom3',fld:'vTABELA_PAINOM3',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV29DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Tabela_MelhoraCod',fld:'vTABELA_MELHORACOD',pic:'ZZZZZ9',nv:0},{av:'AV51Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV37DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV36DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV39Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'A188Tabela_ModuloCod',fld:'TABELA_MODULOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV36DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV37DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV25Tabela_Nome2',fld:'vTABELA_NOME2',pic:'@!',nv:''},{av:'AV29DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV30DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV31DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV32Tabela_Nome3',fld:'vTABELA_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV19Tabela_SistemaDes1',fld:'vTABELA_SISTEMADES1',pic:'@!',nv:''},{av:'AV20Tabela_ModuloDes1',fld:'vTABELA_MODULODES1',pic:'@!',nv:''},{av:'AV21Tabela_PaiNom1',fld:'vTABELA_PAINOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV26Tabela_SistemaDes2',fld:'vTABELA_SISTEMADES2',pic:'@!',nv:''},{av:'AV27Tabela_ModuloDes2',fld:'vTABELA_MODULODES2',pic:'@!',nv:''},{av:'AV28Tabela_PaiNom2',fld:'vTABELA_PAINOM2',pic:'@!',nv:''},{av:'AV33Tabela_SistemaDes3',fld:'vTABELA_SISTEMADES3',pic:'@!',nv:''},{av:'AV34Tabela_ModuloDes3',fld:'vTABELA_MODULODES3',pic:'@!',nv:''},{av:'AV35Tabela_PaiNom3',fld:'vTABELA_PAINOM3',pic:'@!',nv:''},{av:'edtavTabela_nome2_Visible',ctrl:'vTABELA_NOME2',prop:'Visible'},{av:'edtavTabela_sistemades2_Visible',ctrl:'vTABELA_SISTEMADES2',prop:'Visible'},{av:'edtavTabela_modulodes2_Visible',ctrl:'vTABELA_MODULODES2',prop:'Visible'},{av:'edtavTabela_painom2_Visible',ctrl:'vTABELA_PAINOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavTabela_nome3_Visible',ctrl:'vTABELA_NOME3',prop:'Visible'},{av:'edtavTabela_sistemades3_Visible',ctrl:'vTABELA_SISTEMADES3',prop:'Visible'},{av:'edtavTabela_modulodes3_Visible',ctrl:'vTABELA_MODULODES3',prop:'Visible'},{av:'edtavTabela_painom3_Visible',ctrl:'vTABELA_PAINOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavTabela_nome1_Visible',ctrl:'vTABELA_NOME1',prop:'Visible'},{av:'edtavTabela_sistemades1_Visible',ctrl:'vTABELA_SISTEMADES1',prop:'Visible'},{av:'edtavTabela_modulodes1_Visible',ctrl:'vTABELA_MODULODES1',prop:'Visible'},{av:'edtavTabela_painom1_Visible',ctrl:'vTABELA_PAINOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E18PR2',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavTabela_nome1_Visible',ctrl:'vTABELA_NOME1',prop:'Visible'},{av:'edtavTabela_sistemades1_Visible',ctrl:'vTABELA_SISTEMADES1',prop:'Visible'},{av:'edtavTabela_modulodes1_Visible',ctrl:'vTABELA_MODULODES1',prop:'Visible'},{av:'edtavTabela_painom1_Visible',ctrl:'vTABELA_PAINOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E19PR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV19Tabela_SistemaDes1',fld:'vTABELA_SISTEMADES1',pic:'@!',nv:''},{av:'AV20Tabela_ModuloDes1',fld:'vTABELA_MODULODES1',pic:'@!',nv:''},{av:'AV21Tabela_PaiNom1',fld:'vTABELA_PAINOM1',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV25Tabela_Nome2',fld:'vTABELA_NOME2',pic:'@!',nv:''},{av:'AV26Tabela_SistemaDes2',fld:'vTABELA_SISTEMADES2',pic:'@!',nv:''},{av:'AV27Tabela_ModuloDes2',fld:'vTABELA_MODULODES2',pic:'@!',nv:''},{av:'AV28Tabela_PaiNom2',fld:'vTABELA_PAINOM2',pic:'@!',nv:''},{av:'AV30DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV31DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV32Tabela_Nome3',fld:'vTABELA_NOME3',pic:'@!',nv:''},{av:'AV33Tabela_SistemaDes3',fld:'vTABELA_SISTEMADES3',pic:'@!',nv:''},{av:'AV34Tabela_ModuloDes3',fld:'vTABELA_MODULODES3',pic:'@!',nv:''},{av:'AV35Tabela_PaiNom3',fld:'vTABELA_PAINOM3',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV29DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Tabela_MelhoraCod',fld:'vTABELA_MELHORACOD',pic:'ZZZZZ9',nv:0},{av:'AV51Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV37DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV36DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV39Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'A188Tabela_ModuloCod',fld:'TABELA_MODULOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV29DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E14PR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV19Tabela_SistemaDes1',fld:'vTABELA_SISTEMADES1',pic:'@!',nv:''},{av:'AV20Tabela_ModuloDes1',fld:'vTABELA_MODULODES1',pic:'@!',nv:''},{av:'AV21Tabela_PaiNom1',fld:'vTABELA_PAINOM1',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV25Tabela_Nome2',fld:'vTABELA_NOME2',pic:'@!',nv:''},{av:'AV26Tabela_SistemaDes2',fld:'vTABELA_SISTEMADES2',pic:'@!',nv:''},{av:'AV27Tabela_ModuloDes2',fld:'vTABELA_MODULODES2',pic:'@!',nv:''},{av:'AV28Tabela_PaiNom2',fld:'vTABELA_PAINOM2',pic:'@!',nv:''},{av:'AV30DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV31DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV32Tabela_Nome3',fld:'vTABELA_NOME3',pic:'@!',nv:''},{av:'AV33Tabela_SistemaDes3',fld:'vTABELA_SISTEMADES3',pic:'@!',nv:''},{av:'AV34Tabela_ModuloDes3',fld:'vTABELA_MODULODES3',pic:'@!',nv:''},{av:'AV35Tabela_PaiNom3',fld:'vTABELA_PAINOM3',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV29DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Tabela_MelhoraCod',fld:'vTABELA_MELHORACOD',pic:'ZZZZZ9',nv:0},{av:'AV51Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV37DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV36DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV39Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'A188Tabela_ModuloCod',fld:'TABELA_MODULOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV36DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV25Tabela_Nome2',fld:'vTABELA_NOME2',pic:'@!',nv:''},{av:'AV29DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV30DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV31DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV32Tabela_Nome3',fld:'vTABELA_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV19Tabela_SistemaDes1',fld:'vTABELA_SISTEMADES1',pic:'@!',nv:''},{av:'AV20Tabela_ModuloDes1',fld:'vTABELA_MODULODES1',pic:'@!',nv:''},{av:'AV21Tabela_PaiNom1',fld:'vTABELA_PAINOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV26Tabela_SistemaDes2',fld:'vTABELA_SISTEMADES2',pic:'@!',nv:''},{av:'AV27Tabela_ModuloDes2',fld:'vTABELA_MODULODES2',pic:'@!',nv:''},{av:'AV28Tabela_PaiNom2',fld:'vTABELA_PAINOM2',pic:'@!',nv:''},{av:'AV33Tabela_SistemaDes3',fld:'vTABELA_SISTEMADES3',pic:'@!',nv:''},{av:'AV34Tabela_ModuloDes3',fld:'vTABELA_MODULODES3',pic:'@!',nv:''},{av:'AV35Tabela_PaiNom3',fld:'vTABELA_PAINOM3',pic:'@!',nv:''},{av:'edtavTabela_nome2_Visible',ctrl:'vTABELA_NOME2',prop:'Visible'},{av:'edtavTabela_sistemades2_Visible',ctrl:'vTABELA_SISTEMADES2',prop:'Visible'},{av:'edtavTabela_modulodes2_Visible',ctrl:'vTABELA_MODULODES2',prop:'Visible'},{av:'edtavTabela_painom2_Visible',ctrl:'vTABELA_PAINOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavTabela_nome3_Visible',ctrl:'vTABELA_NOME3',prop:'Visible'},{av:'edtavTabela_sistemades3_Visible',ctrl:'vTABELA_SISTEMADES3',prop:'Visible'},{av:'edtavTabela_modulodes3_Visible',ctrl:'vTABELA_MODULODES3',prop:'Visible'},{av:'edtavTabela_painom3_Visible',ctrl:'vTABELA_PAINOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavTabela_nome1_Visible',ctrl:'vTABELA_NOME1',prop:'Visible'},{av:'edtavTabela_sistemades1_Visible',ctrl:'vTABELA_SISTEMADES1',prop:'Visible'},{av:'edtavTabela_modulodes1_Visible',ctrl:'vTABELA_MODULODES1',prop:'Visible'},{av:'edtavTabela_painom1_Visible',ctrl:'vTABELA_PAINOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E20PR2',iparms:[{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV24DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavTabela_nome2_Visible',ctrl:'vTABELA_NOME2',prop:'Visible'},{av:'edtavTabela_sistemades2_Visible',ctrl:'vTABELA_SISTEMADES2',prop:'Visible'},{av:'edtavTabela_modulodes2_Visible',ctrl:'vTABELA_MODULODES2',prop:'Visible'},{av:'edtavTabela_painom2_Visible',ctrl:'vTABELA_PAINOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E15PR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV19Tabela_SistemaDes1',fld:'vTABELA_SISTEMADES1',pic:'@!',nv:''},{av:'AV20Tabela_ModuloDes1',fld:'vTABELA_MODULODES1',pic:'@!',nv:''},{av:'AV21Tabela_PaiNom1',fld:'vTABELA_PAINOM1',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV25Tabela_Nome2',fld:'vTABELA_NOME2',pic:'@!',nv:''},{av:'AV26Tabela_SistemaDes2',fld:'vTABELA_SISTEMADES2',pic:'@!',nv:''},{av:'AV27Tabela_ModuloDes2',fld:'vTABELA_MODULODES2',pic:'@!',nv:''},{av:'AV28Tabela_PaiNom2',fld:'vTABELA_PAINOM2',pic:'@!',nv:''},{av:'AV30DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV31DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV32Tabela_Nome3',fld:'vTABELA_NOME3',pic:'@!',nv:''},{av:'AV33Tabela_SistemaDes3',fld:'vTABELA_SISTEMADES3',pic:'@!',nv:''},{av:'AV34Tabela_ModuloDes3',fld:'vTABELA_MODULODES3',pic:'@!',nv:''},{av:'AV35Tabela_PaiNom3',fld:'vTABELA_PAINOM3',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV29DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Tabela_MelhoraCod',fld:'vTABELA_MELHORACOD',pic:'ZZZZZ9',nv:0},{av:'AV51Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV37DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV36DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV39Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'A188Tabela_ModuloCod',fld:'TABELA_MODULOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV36DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV29DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV25Tabela_Nome2',fld:'vTABELA_NOME2',pic:'@!',nv:''},{av:'AV30DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV31DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV32Tabela_Nome3',fld:'vTABELA_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV19Tabela_SistemaDes1',fld:'vTABELA_SISTEMADES1',pic:'@!',nv:''},{av:'AV20Tabela_ModuloDes1',fld:'vTABELA_MODULODES1',pic:'@!',nv:''},{av:'AV21Tabela_PaiNom1',fld:'vTABELA_PAINOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV26Tabela_SistemaDes2',fld:'vTABELA_SISTEMADES2',pic:'@!',nv:''},{av:'AV27Tabela_ModuloDes2',fld:'vTABELA_MODULODES2',pic:'@!',nv:''},{av:'AV28Tabela_PaiNom2',fld:'vTABELA_PAINOM2',pic:'@!',nv:''},{av:'AV33Tabela_SistemaDes3',fld:'vTABELA_SISTEMADES3',pic:'@!',nv:''},{av:'AV34Tabela_ModuloDes3',fld:'vTABELA_MODULODES3',pic:'@!',nv:''},{av:'AV35Tabela_PaiNom3',fld:'vTABELA_PAINOM3',pic:'@!',nv:''},{av:'edtavTabela_nome2_Visible',ctrl:'vTABELA_NOME2',prop:'Visible'},{av:'edtavTabela_sistemades2_Visible',ctrl:'vTABELA_SISTEMADES2',prop:'Visible'},{av:'edtavTabela_modulodes2_Visible',ctrl:'vTABELA_MODULODES2',prop:'Visible'},{av:'edtavTabela_painom2_Visible',ctrl:'vTABELA_PAINOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavTabela_nome3_Visible',ctrl:'vTABELA_NOME3',prop:'Visible'},{av:'edtavTabela_sistemades3_Visible',ctrl:'vTABELA_SISTEMADES3',prop:'Visible'},{av:'edtavTabela_modulodes3_Visible',ctrl:'vTABELA_MODULODES3',prop:'Visible'},{av:'edtavTabela_painom3_Visible',ctrl:'vTABELA_PAINOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavTabela_nome1_Visible',ctrl:'vTABELA_NOME1',prop:'Visible'},{av:'edtavTabela_sistemades1_Visible',ctrl:'vTABELA_SISTEMADES1',prop:'Visible'},{av:'edtavTabela_modulodes1_Visible',ctrl:'vTABELA_MODULODES1',prop:'Visible'},{av:'edtavTabela_painom1_Visible',ctrl:'vTABELA_PAINOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E21PR2',iparms:[{av:'AV30DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'AV31DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtavTabela_nome3_Visible',ctrl:'vTABELA_NOME3',prop:'Visible'},{av:'edtavTabela_sistemades3_Visible',ctrl:'vTABELA_SISTEMADES3',prop:'Visible'},{av:'edtavTabela_modulodes3_Visible',ctrl:'vTABELA_MODULODES3',prop:'Visible'},{av:'edtavTabela_painom3_Visible',ctrl:'vTABELA_PAINOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E16PR2',iparms:[{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV39Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV39Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16DynamicFiltersSelector1 = "";
         AV18Tabela_Nome1 = "";
         AV19Tabela_SistemaDes1 = "";
         AV20Tabela_ModuloDes1 = "";
         AV21Tabela_PaiNom1 = "";
         AV23DynamicFiltersSelector2 = "";
         AV25Tabela_Nome2 = "";
         AV26Tabela_SistemaDes2 = "";
         AV27Tabela_ModuloDes2 = "";
         AV28Tabela_PaiNom2 = "";
         AV30DynamicFiltersSelector3 = "";
         AV32Tabela_Nome3 = "";
         AV33Tabela_SistemaDes3 = "";
         AV34Tabela_ModuloDes3 = "";
         AV35Tabela_PaiNom3 = "";
         AV51Pgmname = "";
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV38Update = "";
         AV48Update_GXI = "";
         AV40Delete = "";
         AV49Delete_GXI = "";
         AV41Display = "";
         AV50Display_GXI = "";
         A173Tabela_Nome = "";
         A175Tabela_Descricao = "";
         A191Tabela_SistemaDes = "";
         A189Tabela_ModuloDes = "";
         A182Tabela_PaiNom = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV18Tabela_Nome1 = "";
         lV19Tabela_SistemaDes1 = "";
         lV20Tabela_ModuloDes1 = "";
         lV21Tabela_PaiNom1 = "";
         lV25Tabela_Nome2 = "";
         lV26Tabela_SistemaDes2 = "";
         lV27Tabela_ModuloDes2 = "";
         lV28Tabela_PaiNom2 = "";
         lV32Tabela_Nome3 = "";
         lV33Tabela_SistemaDes3 = "";
         lV34Tabela_ModuloDes3 = "";
         lV35Tabela_PaiNom3 = "";
         H00PR2_A746Tabela_MelhoraCod = new int[1] ;
         H00PR2_n746Tabela_MelhoraCod = new bool[] {false} ;
         H00PR2_A174Tabela_Ativo = new bool[] {false} ;
         H00PR2_A182Tabela_PaiNom = new String[] {""} ;
         H00PR2_n182Tabela_PaiNom = new bool[] {false} ;
         H00PR2_A181Tabela_PaiCod = new int[1] ;
         H00PR2_n181Tabela_PaiCod = new bool[] {false} ;
         H00PR2_A189Tabela_ModuloDes = new String[] {""} ;
         H00PR2_n189Tabela_ModuloDes = new bool[] {false} ;
         H00PR2_A188Tabela_ModuloCod = new int[1] ;
         H00PR2_n188Tabela_ModuloCod = new bool[] {false} ;
         H00PR2_A191Tabela_SistemaDes = new String[] {""} ;
         H00PR2_n191Tabela_SistemaDes = new bool[] {false} ;
         H00PR2_A190Tabela_SistemaCod = new int[1] ;
         H00PR2_A175Tabela_Descricao = new String[] {""} ;
         H00PR2_n175Tabela_Descricao = new bool[] {false} ;
         H00PR2_A173Tabela_Nome = new String[] {""} ;
         H00PR2_A172Tabela_Codigo = new int[1] ;
         H00PR3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         GridRow = new GXWebRow();
         AV42Session = context.GetSession();
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7Tabela_MelhoraCod = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.tabelatabela1wc__default(),
            new Object[][] {
                new Object[] {
               H00PR2_A746Tabela_MelhoraCod, H00PR2_n746Tabela_MelhoraCod, H00PR2_A174Tabela_Ativo, H00PR2_A182Tabela_PaiNom, H00PR2_n182Tabela_PaiNom, H00PR2_A181Tabela_PaiCod, H00PR2_n181Tabela_PaiCod, H00PR2_A189Tabela_ModuloDes, H00PR2_n189Tabela_ModuloDes, H00PR2_A188Tabela_ModuloCod,
               H00PR2_n188Tabela_ModuloCod, H00PR2_A191Tabela_SistemaDes, H00PR2_n191Tabela_SistemaDes, H00PR2_A190Tabela_SistemaCod, H00PR2_A175Tabela_Descricao, H00PR2_n175Tabela_Descricao, H00PR2_A173Tabela_Nome, H00PR2_A172Tabela_Codigo
               }
               , new Object[] {
               H00PR3_AGRID_nRecordCount
               }
            }
         );
         AV51Pgmname = "TabelaTabela1WC";
         /* GeneXus formulas. */
         AV51Pgmname = "TabelaTabela1WC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_90 ;
      private short nGXsfl_90_idx=1 ;
      private short AV14OrderedBy ;
      private short AV17DynamicFiltersOperator1 ;
      private short AV24DynamicFiltersOperator2 ;
      private short AV31DynamicFiltersOperator3 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_90_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtTabela_Codigo_Titleformat ;
      private short edtTabela_Nome_Titleformat ;
      private short edtTabela_Descricao_Titleformat ;
      private short edtTabela_SistemaCod_Titleformat ;
      private short edtTabela_SistemaDes_Titleformat ;
      private short edtTabela_ModuloDes_Titleformat ;
      private short edtTabela_PaiCod_Titleformat ;
      private short edtTabela_PaiNom_Titleformat ;
      private short chkTabela_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7Tabela_MelhoraCod ;
      private int wcpOAV7Tabela_MelhoraCod ;
      private int subGrid_Rows ;
      private int A172Tabela_Codigo ;
      private int A190Tabela_SistemaCod ;
      private int AV39Tabela_ModuloCod ;
      private int A188Tabela_ModuloCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int A746Tabela_MelhoraCod ;
      private int edtTabela_MelhoraCod_Visible ;
      private int A181Tabela_PaiCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV43PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int edtavDisplay_Enabled ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavTabela_nome1_Visible ;
      private int edtavTabela_sistemades1_Visible ;
      private int edtavTabela_modulodes1_Visible ;
      private int edtavTabela_painom1_Visible ;
      private int edtavTabela_nome2_Visible ;
      private int edtavTabela_sistemades2_Visible ;
      private int edtavTabela_modulodes2_Visible ;
      private int edtavTabela_painom2_Visible ;
      private int edtavTabela_nome3_Visible ;
      private int edtavTabela_sistemades3_Visible ;
      private int edtavTabela_modulodes3_Visible ;
      private int edtavTabela_painom3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV44GridCurrentPage ;
      private long AV45GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_90_idx="0001" ;
      private String AV18Tabela_Nome1 ;
      private String AV20Tabela_ModuloDes1 ;
      private String AV21Tabela_PaiNom1 ;
      private String AV25Tabela_Nome2 ;
      private String AV27Tabela_ModuloDes2 ;
      private String AV28Tabela_PaiNom2 ;
      private String AV32Tabela_Nome3 ;
      private String AV34Tabela_ModuloDes3 ;
      private String AV35Tabela_PaiNom3 ;
      private String AV51Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String GX_FocusControl ;
      private String edtTabela_MelhoraCod_Internalname ;
      private String edtTabela_MelhoraCod_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavOrderedby_Internalname ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtTabela_Codigo_Internalname ;
      private String A173Tabela_Nome ;
      private String edtTabela_Nome_Internalname ;
      private String edtTabela_Descricao_Internalname ;
      private String edtTabela_SistemaCod_Internalname ;
      private String edtTabela_SistemaDes_Internalname ;
      private String edtTabela_ModuloCod_Internalname ;
      private String A189Tabela_ModuloDes ;
      private String edtTabela_ModuloDes_Internalname ;
      private String edtTabela_PaiCod_Internalname ;
      private String A182Tabela_PaiNom ;
      private String edtTabela_PaiNom_Internalname ;
      private String chkTabela_Ativo_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String lV18Tabela_Nome1 ;
      private String lV20Tabela_ModuloDes1 ;
      private String lV21Tabela_PaiNom1 ;
      private String lV25Tabela_Nome2 ;
      private String lV27Tabela_ModuloDes2 ;
      private String lV28Tabela_PaiNom2 ;
      private String lV32Tabela_Nome3 ;
      private String lV34Tabela_ModuloDes3 ;
      private String lV35Tabela_PaiNom3 ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavTabela_nome1_Internalname ;
      private String edtavTabela_sistemades1_Internalname ;
      private String edtavTabela_modulodes1_Internalname ;
      private String edtavTabela_painom1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavTabela_nome2_Internalname ;
      private String edtavTabela_sistemades2_Internalname ;
      private String edtavTabela_modulodes2_Internalname ;
      private String edtavTabela_painom2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavTabela_nome3_Internalname ;
      private String edtavTabela_sistemades3_Internalname ;
      private String edtavTabela_modulodes3_Internalname ;
      private String edtavTabela_painom3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String edtTabela_Codigo_Title ;
      private String edtTabela_Nome_Title ;
      private String edtTabela_Descricao_Title ;
      private String edtTabela_SistemaCod_Title ;
      private String edtTabela_SistemaDes_Title ;
      private String edtTabela_ModuloDes_Title ;
      private String edtTabela_PaiCod_Title ;
      private String edtTabela_PaiNom_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtTabela_ModuloDes_Link ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavTabela_nome3_Jsonclick ;
      private String edtavTabela_sistemades3_Jsonclick ;
      private String edtavTabela_modulodes3_Jsonclick ;
      private String edtavTabela_painom3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavTabela_nome2_Jsonclick ;
      private String edtavTabela_sistemades2_Jsonclick ;
      private String edtavTabela_modulodes2_Jsonclick ;
      private String edtavTabela_painom2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavTabela_nome1_Jsonclick ;
      private String edtavTabela_sistemades1_Jsonclick ;
      private String edtavTabela_modulodes1_Jsonclick ;
      private String edtavTabela_painom1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sCtrlAV7Tabela_MelhoraCod ;
      private String sGXsfl_90_fel_idx="0001" ;
      private String ROClassString ;
      private String edtTabela_Codigo_Jsonclick ;
      private String edtTabela_Nome_Jsonclick ;
      private String edtTabela_Descricao_Jsonclick ;
      private String edtTabela_SistemaCod_Jsonclick ;
      private String edtTabela_SistemaDes_Jsonclick ;
      private String edtTabela_ModuloCod_Jsonclick ;
      private String edtTabela_ModuloDes_Jsonclick ;
      private String edtTabela_PaiCod_Jsonclick ;
      private String edtTabela_PaiNom_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool AV22DynamicFiltersEnabled2 ;
      private bool AV29DynamicFiltersEnabled3 ;
      private bool AV37DynamicFiltersIgnoreFirst ;
      private bool AV36DynamicFiltersRemoving ;
      private bool n188Tabela_ModuloCod ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n175Tabela_Descricao ;
      private bool n191Tabela_SistemaDes ;
      private bool n189Tabela_ModuloDes ;
      private bool n181Tabela_PaiCod ;
      private bool n182Tabela_PaiNom ;
      private bool A174Tabela_Ativo ;
      private bool n746Tabela_MelhoraCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV38Update_IsBlob ;
      private bool AV40Delete_IsBlob ;
      private bool AV41Display_IsBlob ;
      private String A175Tabela_Descricao ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV19Tabela_SistemaDes1 ;
      private String AV23DynamicFiltersSelector2 ;
      private String AV26Tabela_SistemaDes2 ;
      private String AV30DynamicFiltersSelector3 ;
      private String AV33Tabela_SistemaDes3 ;
      private String AV48Update_GXI ;
      private String AV49Delete_GXI ;
      private String AV50Display_GXI ;
      private String A191Tabela_SistemaDes ;
      private String lV19Tabela_SistemaDes1 ;
      private String lV26Tabela_SistemaDes2 ;
      private String lV33Tabela_SistemaDes3 ;
      private String AV38Update ;
      private String AV40Delete ;
      private String AV41Display ;
      private IGxSession AV42Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkTabela_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00PR2_A746Tabela_MelhoraCod ;
      private bool[] H00PR2_n746Tabela_MelhoraCod ;
      private bool[] H00PR2_A174Tabela_Ativo ;
      private String[] H00PR2_A182Tabela_PaiNom ;
      private bool[] H00PR2_n182Tabela_PaiNom ;
      private int[] H00PR2_A181Tabela_PaiCod ;
      private bool[] H00PR2_n181Tabela_PaiCod ;
      private String[] H00PR2_A189Tabela_ModuloDes ;
      private bool[] H00PR2_n189Tabela_ModuloDes ;
      private int[] H00PR2_A188Tabela_ModuloCod ;
      private bool[] H00PR2_n188Tabela_ModuloCod ;
      private String[] H00PR2_A191Tabela_SistemaDes ;
      private bool[] H00PR2_n191Tabela_SistemaDes ;
      private int[] H00PR2_A190Tabela_SistemaCod ;
      private String[] H00PR2_A175Tabela_Descricao ;
      private bool[] H00PR2_n175Tabela_Descricao ;
      private String[] H00PR2_A173Tabela_Nome ;
      private int[] H00PR2_A172Tabela_Codigo ;
      private long[] H00PR3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
   }

   public class tabelatabela1wc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00PR2( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             String AV18Tabela_Nome1 ,
                                             String AV19Tabela_SistemaDes1 ,
                                             String AV20Tabela_ModuloDes1 ,
                                             String AV21Tabela_PaiNom1 ,
                                             bool AV22DynamicFiltersEnabled2 ,
                                             String AV23DynamicFiltersSelector2 ,
                                             short AV24DynamicFiltersOperator2 ,
                                             String AV25Tabela_Nome2 ,
                                             String AV26Tabela_SistemaDes2 ,
                                             String AV27Tabela_ModuloDes2 ,
                                             String AV28Tabela_PaiNom2 ,
                                             bool AV29DynamicFiltersEnabled3 ,
                                             String AV30DynamicFiltersSelector3 ,
                                             short AV31DynamicFiltersOperator3 ,
                                             String AV32Tabela_Nome3 ,
                                             String AV33Tabela_SistemaDes3 ,
                                             String AV34Tabela_ModuloDes3 ,
                                             String AV35Tabela_PaiNom3 ,
                                             String A173Tabela_Nome ,
                                             String A191Tabela_SistemaDes ,
                                             String A189Tabela_ModuloDes ,
                                             String A182Tabela_PaiNom ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A746Tabela_MelhoraCod ,
                                             int AV7Tabela_MelhoraCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [30] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Tabela_MelhoraCod], T1.[Tabela_Ativo], T2.[Tabela_Nome] AS Tabela_PaiNom, T1.[Tabela_PaiCod] AS Tabela_PaiCod, T3.[Modulo_Nome] AS Tabela_ModuloDes, T1.[Tabela_ModuloCod] AS Tabela_ModuloCod, T4.[Sistema_Nome] AS Tabela_SistemaDes, T1.[Tabela_SistemaCod] AS Tabela_SistemaCod, T1.[Tabela_Descricao], T1.[Tabela_Nome], T1.[Tabela_Codigo]";
         sFromString = " FROM ((([Tabela] T1 WITH (NOLOCK) LEFT JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = T1.[Tabela_PaiCod]) LEFT JOIN [Modulo] T3 WITH (NOLOCK) ON T3.[Modulo_Codigo] = T1.[Tabela_ModuloCod]) INNER JOIN [Sistema] T4 WITH (NOLOCK) ON T4.[Sistema_Codigo] = T1.[Tabela_SistemaCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[Tabela_MelhoraCod] = @AV7Tabela_MelhoraCod)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Tabela_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV18Tabela_Nome1)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Tabela_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] like '%' + @lV18Tabela_Nome1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_SISTEMADES") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Tabela_SistemaDes1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Sistema_Nome] like @lV19Tabela_SistemaDes1)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_SISTEMADES") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Tabela_SistemaDes1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Sistema_Nome] like '%' + @lV19Tabela_SistemaDes1)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_MODULODES") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20Tabela_ModuloDes1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Modulo_Nome] like @lV20Tabela_ModuloDes1)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_MODULODES") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20Tabela_ModuloDes1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Modulo_Nome] like '%' + @lV20Tabela_ModuloDes1)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_PAINOM") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21Tabela_PaiNom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV21Tabela_PaiNom1)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_PAINOM") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21Tabela_PaiNom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Tabela_Nome] like '%' + @lV21Tabela_PaiNom1)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV22DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_NOME") == 0 ) && ( AV24DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Tabela_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV25Tabela_Nome2)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV22DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_NOME") == 0 ) && ( AV24DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Tabela_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] like '%' + @lV25Tabela_Nome2)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV22DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_SISTEMADES") == 0 ) && ( AV24DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26Tabela_SistemaDes2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Sistema_Nome] like @lV26Tabela_SistemaDes2)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV22DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_SISTEMADES") == 0 ) && ( AV24DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26Tabela_SistemaDes2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Sistema_Nome] like '%' + @lV26Tabela_SistemaDes2)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( AV22DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_MODULODES") == 0 ) && ( AV24DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27Tabela_ModuloDes2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Modulo_Nome] like @lV27Tabela_ModuloDes2)";
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( AV22DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_MODULODES") == 0 ) && ( AV24DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27Tabela_ModuloDes2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Modulo_Nome] like '%' + @lV27Tabela_ModuloDes2)";
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( AV22DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_PAINOM") == 0 ) && ( AV24DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Tabela_PaiNom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV28Tabela_PaiNom2)";
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( AV22DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_PAINOM") == 0 ) && ( AV24DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Tabela_PaiNom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Tabela_Nome] like '%' + @lV28Tabela_PaiNom2)";
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( AV29DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_NOME") == 0 ) && ( AV31DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32Tabela_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV32Tabela_Nome3)";
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( AV29DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_NOME") == 0 ) && ( AV31DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32Tabela_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] like '%' + @lV32Tabela_Nome3)";
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( AV29DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_SISTEMADES") == 0 ) && ( AV31DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33Tabela_SistemaDes3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Sistema_Nome] like @lV33Tabela_SistemaDes3)";
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( AV29DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_SISTEMADES") == 0 ) && ( AV31DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33Tabela_SistemaDes3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Sistema_Nome] like '%' + @lV33Tabela_SistemaDes3)";
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( AV29DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_MODULODES") == 0 ) && ( AV31DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34Tabela_ModuloDes3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Modulo_Nome] like @lV34Tabela_ModuloDes3)";
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( AV29DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_MODULODES") == 0 ) && ( AV31DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34Tabela_ModuloDes3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Modulo_Nome] like '%' + @lV34Tabela_ModuloDes3)";
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( AV29DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_PAINOM") == 0 ) && ( AV31DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35Tabela_PaiNom3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV35Tabela_PaiNom3)";
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( AV29DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_PAINOM") == 0 ) && ( AV31DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35Tabela_PaiNom3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Tabela_Nome] like '%' + @lV35Tabela_PaiNom3)";
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_MelhoraCod], T1.[Tabela_Nome]";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_MelhoraCod] DESC, T1.[Tabela_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_MelhoraCod], T1.[Tabela_Codigo]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_MelhoraCod] DESC, T1.[Tabela_Codigo] DESC";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_MelhoraCod], T1.[Tabela_Descricao]";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_MelhoraCod] DESC, T1.[Tabela_Descricao] DESC";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_MelhoraCod], T1.[Tabela_SistemaCod]";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_MelhoraCod] DESC, T1.[Tabela_SistemaCod] DESC";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_MelhoraCod], T4.[Sistema_Nome]";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_MelhoraCod] DESC, T4.[Sistema_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 6 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_MelhoraCod], T3.[Modulo_Nome]";
         }
         else if ( ( AV14OrderedBy == 6 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_MelhoraCod] DESC, T3.[Modulo_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 7 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_MelhoraCod], T1.[Tabela_PaiCod]";
         }
         else if ( ( AV14OrderedBy == 7 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_MelhoraCod] DESC, T1.[Tabela_PaiCod] DESC";
         }
         else if ( ( AV14OrderedBy == 8 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_MelhoraCod], T2.[Tabela_Nome]";
         }
         else if ( ( AV14OrderedBy == 8 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_MelhoraCod] DESC, T2.[Tabela_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 9 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_MelhoraCod], T1.[Tabela_Ativo]";
         }
         else if ( ( AV14OrderedBy == 9 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_MelhoraCod] DESC, T1.[Tabela_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H00PR3( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             String AV18Tabela_Nome1 ,
                                             String AV19Tabela_SistemaDes1 ,
                                             String AV20Tabela_ModuloDes1 ,
                                             String AV21Tabela_PaiNom1 ,
                                             bool AV22DynamicFiltersEnabled2 ,
                                             String AV23DynamicFiltersSelector2 ,
                                             short AV24DynamicFiltersOperator2 ,
                                             String AV25Tabela_Nome2 ,
                                             String AV26Tabela_SistemaDes2 ,
                                             String AV27Tabela_ModuloDes2 ,
                                             String AV28Tabela_PaiNom2 ,
                                             bool AV29DynamicFiltersEnabled3 ,
                                             String AV30DynamicFiltersSelector3 ,
                                             short AV31DynamicFiltersOperator3 ,
                                             String AV32Tabela_Nome3 ,
                                             String AV33Tabela_SistemaDes3 ,
                                             String AV34Tabela_ModuloDes3 ,
                                             String AV35Tabela_PaiNom3 ,
                                             String A173Tabela_Nome ,
                                             String A191Tabela_SistemaDes ,
                                             String A189Tabela_ModuloDes ,
                                             String A182Tabela_PaiNom ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A746Tabela_MelhoraCod ,
                                             int AV7Tabela_MelhoraCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [25] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((([Tabela] T1 WITH (NOLOCK) LEFT JOIN [Tabela] T4 WITH (NOLOCK) ON T4.[Tabela_Codigo] = T1.[Tabela_PaiCod]) LEFT JOIN [Modulo] T2 WITH (NOLOCK) ON T2.[Modulo_Codigo] = T1.[Tabela_ModuloCod]) INNER JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T1.[Tabela_SistemaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Tabela_MelhoraCod] = @AV7Tabela_MelhoraCod)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Tabela_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV18Tabela_Nome1)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Tabela_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] like '%' + @lV18Tabela_Nome1)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_SISTEMADES") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Tabela_SistemaDes1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Sistema_Nome] like @lV19Tabela_SistemaDes1)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_SISTEMADES") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Tabela_SistemaDes1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Sistema_Nome] like '%' + @lV19Tabela_SistemaDes1)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_MODULODES") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20Tabela_ModuloDes1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Modulo_Nome] like @lV20Tabela_ModuloDes1)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_MODULODES") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20Tabela_ModuloDes1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Modulo_Nome] like '%' + @lV20Tabela_ModuloDes1)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_PAINOM") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21Tabela_PaiNom1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Tabela_Nome] like @lV21Tabela_PaiNom1)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_PAINOM") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21Tabela_PaiNom1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Tabela_Nome] like '%' + @lV21Tabela_PaiNom1)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV22DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_NOME") == 0 ) && ( AV24DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Tabela_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV25Tabela_Nome2)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV22DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_NOME") == 0 ) && ( AV24DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Tabela_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] like '%' + @lV25Tabela_Nome2)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV22DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_SISTEMADES") == 0 ) && ( AV24DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26Tabela_SistemaDes2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Sistema_Nome] like @lV26Tabela_SistemaDes2)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV22DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_SISTEMADES") == 0 ) && ( AV24DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26Tabela_SistemaDes2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Sistema_Nome] like '%' + @lV26Tabela_SistemaDes2)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( AV22DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_MODULODES") == 0 ) && ( AV24DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27Tabela_ModuloDes2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Modulo_Nome] like @lV27Tabela_ModuloDes2)";
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( AV22DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_MODULODES") == 0 ) && ( AV24DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27Tabela_ModuloDes2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Modulo_Nome] like '%' + @lV27Tabela_ModuloDes2)";
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( AV22DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_PAINOM") == 0 ) && ( AV24DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Tabela_PaiNom2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Tabela_Nome] like @lV28Tabela_PaiNom2)";
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( AV22DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "TABELA_PAINOM") == 0 ) && ( AV24DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Tabela_PaiNom2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Tabela_Nome] like '%' + @lV28Tabela_PaiNom2)";
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( AV29DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_NOME") == 0 ) && ( AV31DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32Tabela_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV32Tabela_Nome3)";
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( AV29DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_NOME") == 0 ) && ( AV31DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32Tabela_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] like '%' + @lV32Tabela_Nome3)";
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( AV29DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_SISTEMADES") == 0 ) && ( AV31DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33Tabela_SistemaDes3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Sistema_Nome] like @lV33Tabela_SistemaDes3)";
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( AV29DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_SISTEMADES") == 0 ) && ( AV31DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33Tabela_SistemaDes3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Sistema_Nome] like '%' + @lV33Tabela_SistemaDes3)";
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( AV29DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_MODULODES") == 0 ) && ( AV31DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34Tabela_ModuloDes3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Modulo_Nome] like @lV34Tabela_ModuloDes3)";
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( AV29DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_MODULODES") == 0 ) && ( AV31DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34Tabela_ModuloDes3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Modulo_Nome] like '%' + @lV34Tabela_ModuloDes3)";
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( AV29DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_PAINOM") == 0 ) && ( AV31DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35Tabela_PaiNom3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Tabela_Nome] like @lV35Tabela_PaiNom3)";
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( AV29DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "TABELA_PAINOM") == 0 ) && ( AV31DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35Tabela_PaiNom3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Tabela_Nome] like '%' + @lV35Tabela_PaiNom3)";
         }
         else
         {
            GXv_int3[24] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 6 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 6 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 7 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 7 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 8 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 8 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 9 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 9 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00PR2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (short)dynConstraints[24] , (bool)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] );
               case 1 :
                     return conditional_H00PR3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (short)dynConstraints[24] , (bool)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00PR2 ;
          prmH00PR2 = new Object[] {
          new Object[] {"@AV7Tabela_MelhoraCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18Tabela_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV18Tabela_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV19Tabela_SistemaDes1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV19Tabela_SistemaDes1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV20Tabela_ModuloDes1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV20Tabela_ModuloDes1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV21Tabela_PaiNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV21Tabela_PaiNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV25Tabela_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV25Tabela_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV26Tabela_SistemaDes2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV26Tabela_SistemaDes2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV27Tabela_ModuloDes2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV27Tabela_ModuloDes2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV28Tabela_PaiNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV28Tabela_PaiNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV32Tabela_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV32Tabela_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV33Tabela_SistemaDes3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV33Tabela_SistemaDes3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV34Tabela_ModuloDes3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV34Tabela_ModuloDes3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV35Tabela_PaiNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV35Tabela_PaiNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00PR3 ;
          prmH00PR3 = new Object[] {
          new Object[] {"@AV7Tabela_MelhoraCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18Tabela_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV18Tabela_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV19Tabela_SistemaDes1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV19Tabela_SistemaDes1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV20Tabela_ModuloDes1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV20Tabela_ModuloDes1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV21Tabela_PaiNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV21Tabela_PaiNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV25Tabela_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV25Tabela_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV26Tabela_SistemaDes2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV26Tabela_SistemaDes2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV27Tabela_ModuloDes2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV27Tabela_ModuloDes2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV28Tabela_PaiNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV28Tabela_PaiNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV32Tabela_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV32Tabela_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV33Tabela_SistemaDes3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV33Tabela_SistemaDes3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV34Tabela_ModuloDes3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV34Tabela_ModuloDes3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV35Tabela_PaiNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV35Tabela_PaiNom3",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00PR2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00PR2,11,0,true,false )
             ,new CursorDef("H00PR3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00PR3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((String[]) buf[14])[0] = rslt.getLongVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getString(10, 50) ;
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[56]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[59]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                return;
       }
    }

 }

}
