/*
               File: PRC_AutorizacaoConsumoAprovar
        Description: PRC_Autorizacao Consumo Aprovar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:58.22
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_autorizacaoconsumoaprovar : GXProcedure
   {
      public prc_autorizacaoconsumoaprovar( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_autorizacaoconsumoaprovar( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AutorizacaoConsumo_Codigo )
      {
         this.AV8AutorizacaoConsumo_Codigo = aP0_AutorizacaoConsumo_Codigo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_AutorizacaoConsumo_Codigo )
      {
         prc_autorizacaoconsumoaprovar objprc_autorizacaoconsumoaprovar;
         objprc_autorizacaoconsumoaprovar = new prc_autorizacaoconsumoaprovar();
         objprc_autorizacaoconsumoaprovar.AV8AutorizacaoConsumo_Codigo = aP0_AutorizacaoConsumo_Codigo;
         objprc_autorizacaoconsumoaprovar.context.SetSubmitInitialConfig(context);
         objprc_autorizacaoconsumoaprovar.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_autorizacaoconsumoaprovar);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_autorizacaoconsumoaprovar)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00D62 */
         pr_default.execute(0, new Object[] {AV8AutorizacaoConsumo_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1774AutorizacaoConsumo_Codigo = P00D62_A1774AutorizacaoConsumo_Codigo[0];
            A1787AutorizacaoConsumo_Status = P00D62_A1787AutorizacaoConsumo_Status[0];
            A1777AutorizacaoConsumo_UnidadeMedicaoCod = P00D62_A1777AutorizacaoConsumo_UnidadeMedicaoCod[0];
            A74Contrato_Codigo = P00D62_A74Contrato_Codigo[0];
            A1782AutorizacaoConsumo_Valor = P00D62_A1782AutorizacaoConsumo_Valor[0];
            A1787AutorizacaoConsumo_Status = "AUT";
            AV9AutorizacaoConsumo_UnidadeMedicaoCod = A1777AutorizacaoConsumo_UnidadeMedicaoCod;
            AV10Contrato_Codigo = A74Contrato_Codigo;
            AV12AutorizacaoConsumo_Valor = A1782AutorizacaoConsumo_Valor;
            /* Using cursor P00D63 */
            pr_default.execute(1, new Object[] {A1787AutorizacaoConsumo_Status, A1774AutorizacaoConsumo_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("AutorizacaoConsumo") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         /* Using cursor P00D64 */
         pr_default.execute(2, new Object[] {AV10Contrato_Codigo, AV9AutorizacaoConsumo_UnidadeMedicaoCod});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A1783SaldoContrato_UnidadeMedicao_Codigo = P00D64_A1783SaldoContrato_UnidadeMedicao_Codigo[0];
            A74Contrato_Codigo = P00D64_A74Contrato_Codigo[0];
            A1561SaldoContrato_Codigo = P00D64_A1561SaldoContrato_Codigo[0];
            AV11SaldoContrato_Codigo = A1561SaldoContrato_Codigo;
            pr_default.readNext(2);
         }
         pr_default.close(2);
         new prc_saldocontratoreservar(context ).execute(  AV11SaldoContrato_Codigo,  AV10Contrato_Codigo,  0,  0,  AV8AutorizacaoConsumo_Codigo,  AV12AutorizacaoConsumo_Valor) ;
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00D62_A1774AutorizacaoConsumo_Codigo = new int[1] ;
         P00D62_A1787AutorizacaoConsumo_Status = new String[] {""} ;
         P00D62_A1777AutorizacaoConsumo_UnidadeMedicaoCod = new int[1] ;
         P00D62_A74Contrato_Codigo = new int[1] ;
         P00D62_A1782AutorizacaoConsumo_Valor = new decimal[1] ;
         A1787AutorizacaoConsumo_Status = "";
         P00D64_A1783SaldoContrato_UnidadeMedicao_Codigo = new int[1] ;
         P00D64_A74Contrato_Codigo = new int[1] ;
         P00D64_A1561SaldoContrato_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_autorizacaoconsumoaprovar__default(),
            new Object[][] {
                new Object[] {
               P00D62_A1774AutorizacaoConsumo_Codigo, P00D62_A1787AutorizacaoConsumo_Status, P00D62_A1777AutorizacaoConsumo_UnidadeMedicaoCod, P00D62_A74Contrato_Codigo, P00D62_A1782AutorizacaoConsumo_Valor
               }
               , new Object[] {
               }
               , new Object[] {
               P00D64_A1783SaldoContrato_UnidadeMedicao_Codigo, P00D64_A74Contrato_Codigo, P00D64_A1561SaldoContrato_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8AutorizacaoConsumo_Codigo ;
      private int A1774AutorizacaoConsumo_Codigo ;
      private int A1777AutorizacaoConsumo_UnidadeMedicaoCod ;
      private int A74Contrato_Codigo ;
      private int AV9AutorizacaoConsumo_UnidadeMedicaoCod ;
      private int AV10Contrato_Codigo ;
      private int A1783SaldoContrato_UnidadeMedicao_Codigo ;
      private int A1561SaldoContrato_Codigo ;
      private int AV11SaldoContrato_Codigo ;
      private decimal A1782AutorizacaoConsumo_Valor ;
      private decimal AV12AutorizacaoConsumo_Valor ;
      private String scmdbuf ;
      private String A1787AutorizacaoConsumo_Status ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00D62_A1774AutorizacaoConsumo_Codigo ;
      private String[] P00D62_A1787AutorizacaoConsumo_Status ;
      private int[] P00D62_A1777AutorizacaoConsumo_UnidadeMedicaoCod ;
      private int[] P00D62_A74Contrato_Codigo ;
      private decimal[] P00D62_A1782AutorizacaoConsumo_Valor ;
      private int[] P00D64_A1783SaldoContrato_UnidadeMedicao_Codigo ;
      private int[] P00D64_A74Contrato_Codigo ;
      private int[] P00D64_A1561SaldoContrato_Codigo ;
   }

   public class prc_autorizacaoconsumoaprovar__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00D62 ;
          prmP00D62 = new Object[] {
          new Object[] {"@AV8AutorizacaoConsumo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00D63 ;
          prmP00D63 = new Object[] {
          new Object[] {"@AutorizacaoConsumo_Status",SqlDbType.Char,3,0} ,
          new Object[] {"@AutorizacaoConsumo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00D64 ;
          prmP00D64 = new Object[] {
          new Object[] {"@AV10Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9AutorizacaoConsumo_UnidadeMedicaoCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00D62", "SELECT [AutorizacaoConsumo_Codigo], [AutorizacaoConsumo_Status], [AutorizacaoConsumo_UnidadeMedicaoCod], [Contrato_Codigo], [AutorizacaoConsumo_Valor] FROM [AutorizacaoConsumo] WITH (UPDLOCK) WHERE [AutorizacaoConsumo_Codigo] = @AV8AutorizacaoConsumo_Codigo ORDER BY [AutorizacaoConsumo_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00D62,1,0,true,true )
             ,new CursorDef("P00D63", "UPDATE [AutorizacaoConsumo] SET [AutorizacaoConsumo_Status]=@AutorizacaoConsumo_Status  WHERE [AutorizacaoConsumo_Codigo] = @AutorizacaoConsumo_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00D63)
             ,new CursorDef("P00D64", "SELECT [SaldoContrato_UnidadeMedicao_Codigo], [Contrato_Codigo], [SaldoContrato_Codigo] FROM [SaldoContrato] WITH (NOLOCK) WHERE ([Contrato_Codigo] = @AV10Contrato_Codigo) AND ([SaldoContrato_UnidadeMedicao_Codigo] = @AV9AutorizacaoConsumo_UnidadeMedicaoCod) ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00D64,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
