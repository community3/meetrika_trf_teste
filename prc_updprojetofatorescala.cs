/*
               File: PRC_UpdProjetoFatorEscala
        Description: Upd Projeto Fator Escala
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/6/2020 23:50:38.93
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_updprojetofatorescala : GXProcedure
   {
      public prc_updprojetofatorescala( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_updprojetofatorescala( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_VariavelCocomo_AreaTrabalhoCod ,
                           ref int aP1_VariavelCocomo_ProjetoCod ,
                           ref String aP2_VariavelCocomo_Tipo )
      {
         this.A961VariavelCocomo_AreaTrabalhoCod = aP0_VariavelCocomo_AreaTrabalhoCod;
         this.A965VariavelCocomo_ProjetoCod = aP1_VariavelCocomo_ProjetoCod;
         this.A964VariavelCocomo_Tipo = aP2_VariavelCocomo_Tipo;
         initialize();
         executePrivate();
         aP0_VariavelCocomo_AreaTrabalhoCod=this.A961VariavelCocomo_AreaTrabalhoCod;
         aP1_VariavelCocomo_ProjetoCod=this.A965VariavelCocomo_ProjetoCod;
         aP2_VariavelCocomo_Tipo=this.A964VariavelCocomo_Tipo;
      }

      public String executeUdp( ref int aP0_VariavelCocomo_AreaTrabalhoCod ,
                                ref int aP1_VariavelCocomo_ProjetoCod )
      {
         this.A961VariavelCocomo_AreaTrabalhoCod = aP0_VariavelCocomo_AreaTrabalhoCod;
         this.A965VariavelCocomo_ProjetoCod = aP1_VariavelCocomo_ProjetoCod;
         this.A964VariavelCocomo_Tipo = aP2_VariavelCocomo_Tipo;
         initialize();
         executePrivate();
         aP0_VariavelCocomo_AreaTrabalhoCod=this.A961VariavelCocomo_AreaTrabalhoCod;
         aP1_VariavelCocomo_ProjetoCod=this.A965VariavelCocomo_ProjetoCod;
         aP2_VariavelCocomo_Tipo=this.A964VariavelCocomo_Tipo;
         return A964VariavelCocomo_Tipo ;
      }

      public void executeSubmit( ref int aP0_VariavelCocomo_AreaTrabalhoCod ,
                                 ref int aP1_VariavelCocomo_ProjetoCod ,
                                 ref String aP2_VariavelCocomo_Tipo )
      {
         prc_updprojetofatorescala objprc_updprojetofatorescala;
         objprc_updprojetofatorescala = new prc_updprojetofatorescala();
         objprc_updprojetofatorescala.A961VariavelCocomo_AreaTrabalhoCod = aP0_VariavelCocomo_AreaTrabalhoCod;
         objprc_updprojetofatorescala.A965VariavelCocomo_ProjetoCod = aP1_VariavelCocomo_ProjetoCod;
         objprc_updprojetofatorescala.A964VariavelCocomo_Tipo = aP2_VariavelCocomo_Tipo;
         objprc_updprojetofatorescala.context.SetSubmitInitialConfig(context);
         objprc_updprojetofatorescala.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_updprojetofatorescala);
         aP0_VariavelCocomo_AreaTrabalhoCod=this.A961VariavelCocomo_AreaTrabalhoCod;
         aP1_VariavelCocomo_ProjetoCod=this.A965VariavelCocomo_ProjetoCod;
         aP2_VariavelCocomo_Tipo=this.A964VariavelCocomo_Tipo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_updprojetofatorescala)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV11i = 1;
         AV30sdt_VariaveisCocomo.FromXml(AV29WebSession.Get("VariaveisCocomo"), "");
         AV29WebSession.Remove("VariaveisCocomo");
         AV33GXLvl6 = 0;
         /* Using cursor P006I2 */
         pr_default.execute(0, new Object[] {A961VariavelCocomo_AreaTrabalhoCod, A964VariavelCocomo_Tipo, n965VariavelCocomo_ProjetoCod, A965VariavelCocomo_ProjetoCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A969VariavelCocomo_Nominal = P006I2_A969VariavelCocomo_Nominal[0];
            n969VariavelCocomo_Nominal = P006I2_n969VariavelCocomo_Nominal[0];
            A968VariavelCocomo_Baixo = P006I2_A968VariavelCocomo_Baixo[0];
            n968VariavelCocomo_Baixo = P006I2_n968VariavelCocomo_Baixo[0];
            A967VariavelCocomo_MuitoBaixo = P006I2_A967VariavelCocomo_MuitoBaixo[0];
            n967VariavelCocomo_MuitoBaixo = P006I2_n967VariavelCocomo_MuitoBaixo[0];
            A986VariavelCocomo_ExtraBaixo = P006I2_A986VariavelCocomo_ExtraBaixo[0];
            n986VariavelCocomo_ExtraBaixo = P006I2_n986VariavelCocomo_ExtraBaixo[0];
            A970VariavelCocomo_Alto = P006I2_A970VariavelCocomo_Alto[0];
            n970VariavelCocomo_Alto = P006I2_n970VariavelCocomo_Alto[0];
            A971VariavelCocomo_MuitoAlto = P006I2_A971VariavelCocomo_MuitoAlto[0];
            n971VariavelCocomo_MuitoAlto = P006I2_n971VariavelCocomo_MuitoAlto[0];
            A972VariavelCocomo_ExtraAlto = P006I2_A972VariavelCocomo_ExtraAlto[0];
            n972VariavelCocomo_ExtraAlto = P006I2_n972VariavelCocomo_ExtraAlto[0];
            A973VariavelCocomo_Usado = P006I2_A973VariavelCocomo_Usado[0];
            n973VariavelCocomo_Usado = P006I2_n973VariavelCocomo_Usado[0];
            A962VariavelCocomo_Sigla = P006I2_A962VariavelCocomo_Sigla[0];
            A992VariavelCocomo_Sequencial = P006I2_A992VariavelCocomo_Sequencial[0];
            AV33GXLvl6 = 1;
            A969VariavelCocomo_Nominal = ((SdtSDT_VariaveisCocomo_Item)AV30sdt_VariaveisCocomo.Item(AV11i)).gxTpr_Variavelcocomo_nominal;
            n969VariavelCocomo_Nominal = false;
            A968VariavelCocomo_Baixo = ((SdtSDT_VariaveisCocomo_Item)AV30sdt_VariaveisCocomo.Item(AV11i)).gxTpr_Variavelcocomo_baixo;
            n968VariavelCocomo_Baixo = false;
            A967VariavelCocomo_MuitoBaixo = ((SdtSDT_VariaveisCocomo_Item)AV30sdt_VariaveisCocomo.Item(AV11i)).gxTpr_Variavelcocomo_muitobaixo;
            n967VariavelCocomo_MuitoBaixo = false;
            A986VariavelCocomo_ExtraBaixo = ((SdtSDT_VariaveisCocomo_Item)AV30sdt_VariaveisCocomo.Item(AV11i)).gxTpr_Variavelcocomo_extrabaixo;
            n986VariavelCocomo_ExtraBaixo = false;
            A970VariavelCocomo_Alto = ((SdtSDT_VariaveisCocomo_Item)AV30sdt_VariaveisCocomo.Item(AV11i)).gxTpr_Variavelcocomo_alto;
            n970VariavelCocomo_Alto = false;
            A971VariavelCocomo_MuitoAlto = ((SdtSDT_VariaveisCocomo_Item)AV30sdt_VariaveisCocomo.Item(AV11i)).gxTpr_Variavelcocomo_muitoalto;
            n971VariavelCocomo_MuitoAlto = false;
            A972VariavelCocomo_ExtraAlto = ((SdtSDT_VariaveisCocomo_Item)AV30sdt_VariaveisCocomo.Item(AV11i)).gxTpr_Variavelcocomo_extraalto;
            n972VariavelCocomo_ExtraAlto = false;
            A973VariavelCocomo_Usado = ((SdtSDT_VariaveisCocomo_Item)AV30sdt_VariaveisCocomo.Item(AV11i)).gxTpr_Variavelcocomo_usado;
            n973VariavelCocomo_Usado = false;
            AV11i = (short)(AV11i+1);
            /* Using cursor P006I3 */
            pr_default.execute(1, new Object[] {n969VariavelCocomo_Nominal, A969VariavelCocomo_Nominal, n968VariavelCocomo_Baixo, A968VariavelCocomo_Baixo, n967VariavelCocomo_MuitoBaixo, A967VariavelCocomo_MuitoBaixo, n986VariavelCocomo_ExtraBaixo, A986VariavelCocomo_ExtraBaixo, n970VariavelCocomo_Alto, A970VariavelCocomo_Alto, n971VariavelCocomo_MuitoAlto, A971VariavelCocomo_MuitoAlto, n972VariavelCocomo_ExtraAlto, A972VariavelCocomo_ExtraAlto, n973VariavelCocomo_Usado, A973VariavelCocomo_Usado, A961VariavelCocomo_AreaTrabalhoCod, A962VariavelCocomo_Sigla, A964VariavelCocomo_Tipo, A992VariavelCocomo_Sequencial});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("VariaveisCocomo") ;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( AV33GXLvl6 == 0 )
         {
            /* Using cursor P006I4 */
            pr_default.execute(2, new Object[] {A961VariavelCocomo_AreaTrabalhoCod, A964VariavelCocomo_Tipo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A973VariavelCocomo_Usado = P006I4_A973VariavelCocomo_Usado[0];
               n973VariavelCocomo_Usado = P006I4_n973VariavelCocomo_Usado[0];
               A972VariavelCocomo_ExtraAlto = P006I4_A972VariavelCocomo_ExtraAlto[0];
               n972VariavelCocomo_ExtraAlto = P006I4_n972VariavelCocomo_ExtraAlto[0];
               A971VariavelCocomo_MuitoAlto = P006I4_A971VariavelCocomo_MuitoAlto[0];
               n971VariavelCocomo_MuitoAlto = P006I4_n971VariavelCocomo_MuitoAlto[0];
               A970VariavelCocomo_Alto = P006I4_A970VariavelCocomo_Alto[0];
               n970VariavelCocomo_Alto = P006I4_n970VariavelCocomo_Alto[0];
               A969VariavelCocomo_Nominal = P006I4_A969VariavelCocomo_Nominal[0];
               n969VariavelCocomo_Nominal = P006I4_n969VariavelCocomo_Nominal[0];
               A968VariavelCocomo_Baixo = P006I4_A968VariavelCocomo_Baixo[0];
               n968VariavelCocomo_Baixo = P006I4_n968VariavelCocomo_Baixo[0];
               A967VariavelCocomo_MuitoBaixo = P006I4_A967VariavelCocomo_MuitoBaixo[0];
               n967VariavelCocomo_MuitoBaixo = P006I4_n967VariavelCocomo_MuitoBaixo[0];
               A986VariavelCocomo_ExtraBaixo = P006I4_A986VariavelCocomo_ExtraBaixo[0];
               n986VariavelCocomo_ExtraBaixo = P006I4_n986VariavelCocomo_ExtraBaixo[0];
               A966VariavelCocomo_Data = P006I4_A966VariavelCocomo_Data[0];
               n966VariavelCocomo_Data = P006I4_n966VariavelCocomo_Data[0];
               A992VariavelCocomo_Sequencial = P006I4_A992VariavelCocomo_Sequencial[0];
               A963VariavelCocomo_Nome = P006I4_A963VariavelCocomo_Nome[0];
               A962VariavelCocomo_Sigla = P006I4_A962VariavelCocomo_Sigla[0];
               /*
                  INSERT RECORD ON TABLE VariaveisCocomo

               */
               W992VariavelCocomo_Sequencial = A992VariavelCocomo_Sequencial;
               W966VariavelCocomo_Data = A966VariavelCocomo_Data;
               n966VariavelCocomo_Data = false;
               W969VariavelCocomo_Nominal = A969VariavelCocomo_Nominal;
               n969VariavelCocomo_Nominal = false;
               W968VariavelCocomo_Baixo = A968VariavelCocomo_Baixo;
               n968VariavelCocomo_Baixo = false;
               W967VariavelCocomo_MuitoBaixo = A967VariavelCocomo_MuitoBaixo;
               n967VariavelCocomo_MuitoBaixo = false;
               W986VariavelCocomo_ExtraBaixo = A986VariavelCocomo_ExtraBaixo;
               n986VariavelCocomo_ExtraBaixo = false;
               W970VariavelCocomo_Alto = A970VariavelCocomo_Alto;
               n970VariavelCocomo_Alto = false;
               W971VariavelCocomo_MuitoAlto = A971VariavelCocomo_MuitoAlto;
               n971VariavelCocomo_MuitoAlto = false;
               W972VariavelCocomo_ExtraAlto = A972VariavelCocomo_ExtraAlto;
               n972VariavelCocomo_ExtraAlto = false;
               W973VariavelCocomo_Usado = A973VariavelCocomo_Usado;
               n973VariavelCocomo_Usado = false;
               A992VariavelCocomo_Sequencial = (short)(A965VariavelCocomo_ProjetoCod);
               A966VariavelCocomo_Data = DateTimeUtil.ResetTime(DateTimeUtil.ServerNow( context, "DEFAULT"));
               n966VariavelCocomo_Data = false;
               A969VariavelCocomo_Nominal = ((SdtSDT_VariaveisCocomo_Item)AV30sdt_VariaveisCocomo.Item(AV11i)).gxTpr_Variavelcocomo_nominal;
               n969VariavelCocomo_Nominal = false;
               A968VariavelCocomo_Baixo = ((SdtSDT_VariaveisCocomo_Item)AV30sdt_VariaveisCocomo.Item(AV11i)).gxTpr_Variavelcocomo_baixo;
               n968VariavelCocomo_Baixo = false;
               A967VariavelCocomo_MuitoBaixo = ((SdtSDT_VariaveisCocomo_Item)AV30sdt_VariaveisCocomo.Item(AV11i)).gxTpr_Variavelcocomo_muitobaixo;
               n967VariavelCocomo_MuitoBaixo = false;
               A986VariavelCocomo_ExtraBaixo = ((SdtSDT_VariaveisCocomo_Item)AV30sdt_VariaveisCocomo.Item(AV11i)).gxTpr_Variavelcocomo_extrabaixo;
               n986VariavelCocomo_ExtraBaixo = false;
               A970VariavelCocomo_Alto = ((SdtSDT_VariaveisCocomo_Item)AV30sdt_VariaveisCocomo.Item(AV11i)).gxTpr_Variavelcocomo_alto;
               n970VariavelCocomo_Alto = false;
               A971VariavelCocomo_MuitoAlto = ((SdtSDT_VariaveisCocomo_Item)AV30sdt_VariaveisCocomo.Item(AV11i)).gxTpr_Variavelcocomo_muitoalto;
               n971VariavelCocomo_MuitoAlto = false;
               A972VariavelCocomo_ExtraAlto = ((SdtSDT_VariaveisCocomo_Item)AV30sdt_VariaveisCocomo.Item(AV11i)).gxTpr_Variavelcocomo_extraalto;
               n972VariavelCocomo_ExtraAlto = false;
               A973VariavelCocomo_Usado = ((SdtSDT_VariaveisCocomo_Item)AV30sdt_VariaveisCocomo.Item(AV11i)).gxTpr_Variavelcocomo_usado;
               n973VariavelCocomo_Usado = false;
               /* Using cursor P006I5 */
               pr_default.execute(3, new Object[] {A961VariavelCocomo_AreaTrabalhoCod, A962VariavelCocomo_Sigla, A964VariavelCocomo_Tipo, A992VariavelCocomo_Sequencial, A963VariavelCocomo_Nome, n965VariavelCocomo_ProjetoCod, A965VariavelCocomo_ProjetoCod, n966VariavelCocomo_Data, A966VariavelCocomo_Data, n986VariavelCocomo_ExtraBaixo, A986VariavelCocomo_ExtraBaixo, n967VariavelCocomo_MuitoBaixo, A967VariavelCocomo_MuitoBaixo, n968VariavelCocomo_Baixo, A968VariavelCocomo_Baixo, n969VariavelCocomo_Nominal, A969VariavelCocomo_Nominal, n970VariavelCocomo_Alto, A970VariavelCocomo_Alto, n971VariavelCocomo_MuitoAlto, A971VariavelCocomo_MuitoAlto, n972VariavelCocomo_ExtraAlto, A972VariavelCocomo_ExtraAlto, n973VariavelCocomo_Usado, A973VariavelCocomo_Usado});
               pr_default.close(3);
               dsDefault.SmartCacheProvider.SetUpdated("VariaveisCocomo") ;
               if ( (pr_default.getStatus(3) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               A992VariavelCocomo_Sequencial = W992VariavelCocomo_Sequencial;
               A966VariavelCocomo_Data = W966VariavelCocomo_Data;
               n966VariavelCocomo_Data = false;
               A969VariavelCocomo_Nominal = W969VariavelCocomo_Nominal;
               n969VariavelCocomo_Nominal = false;
               A968VariavelCocomo_Baixo = W968VariavelCocomo_Baixo;
               n968VariavelCocomo_Baixo = false;
               A967VariavelCocomo_MuitoBaixo = W967VariavelCocomo_MuitoBaixo;
               n967VariavelCocomo_MuitoBaixo = false;
               A986VariavelCocomo_ExtraBaixo = W986VariavelCocomo_ExtraBaixo;
               n986VariavelCocomo_ExtraBaixo = false;
               A970VariavelCocomo_Alto = W970VariavelCocomo_Alto;
               n970VariavelCocomo_Alto = false;
               A971VariavelCocomo_MuitoAlto = W971VariavelCocomo_MuitoAlto;
               n971VariavelCocomo_MuitoAlto = false;
               A972VariavelCocomo_ExtraAlto = W972VariavelCocomo_ExtraAlto;
               n972VariavelCocomo_ExtraAlto = false;
               A973VariavelCocomo_Usado = W973VariavelCocomo_Usado;
               n973VariavelCocomo_Usado = false;
               /* End Insert */
               AV11i = (short)(AV11i+1);
               pr_default.readNext(2);
            }
            pr_default.close(2);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_UpdProjetoFatorEscala");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV30sdt_VariaveisCocomo = new GxObjectCollection( context, "SDT_VariaveisCocomo.Item", "GxEv3Up14_MeetrikaVs3", "SdtSDT_VariaveisCocomo_Item", "GeneXus.Programs");
         AV29WebSession = context.GetSession();
         scmdbuf = "";
         P006I2_A961VariavelCocomo_AreaTrabalhoCod = new int[1] ;
         P006I2_A964VariavelCocomo_Tipo = new String[] {""} ;
         P006I2_A965VariavelCocomo_ProjetoCod = new int[1] ;
         P006I2_n965VariavelCocomo_ProjetoCod = new bool[] {false} ;
         P006I2_A969VariavelCocomo_Nominal = new decimal[1] ;
         P006I2_n969VariavelCocomo_Nominal = new bool[] {false} ;
         P006I2_A968VariavelCocomo_Baixo = new decimal[1] ;
         P006I2_n968VariavelCocomo_Baixo = new bool[] {false} ;
         P006I2_A967VariavelCocomo_MuitoBaixo = new decimal[1] ;
         P006I2_n967VariavelCocomo_MuitoBaixo = new bool[] {false} ;
         P006I2_A986VariavelCocomo_ExtraBaixo = new decimal[1] ;
         P006I2_n986VariavelCocomo_ExtraBaixo = new bool[] {false} ;
         P006I2_A970VariavelCocomo_Alto = new decimal[1] ;
         P006I2_n970VariavelCocomo_Alto = new bool[] {false} ;
         P006I2_A971VariavelCocomo_MuitoAlto = new decimal[1] ;
         P006I2_n971VariavelCocomo_MuitoAlto = new bool[] {false} ;
         P006I2_A972VariavelCocomo_ExtraAlto = new decimal[1] ;
         P006I2_n972VariavelCocomo_ExtraAlto = new bool[] {false} ;
         P006I2_A973VariavelCocomo_Usado = new decimal[1] ;
         P006I2_n973VariavelCocomo_Usado = new bool[] {false} ;
         P006I2_A962VariavelCocomo_Sigla = new String[] {""} ;
         P006I2_A992VariavelCocomo_Sequencial = new short[1] ;
         A962VariavelCocomo_Sigla = "";
         P006I4_A961VariavelCocomo_AreaTrabalhoCod = new int[1] ;
         P006I4_A964VariavelCocomo_Tipo = new String[] {""} ;
         P006I4_A973VariavelCocomo_Usado = new decimal[1] ;
         P006I4_n973VariavelCocomo_Usado = new bool[] {false} ;
         P006I4_A972VariavelCocomo_ExtraAlto = new decimal[1] ;
         P006I4_n972VariavelCocomo_ExtraAlto = new bool[] {false} ;
         P006I4_A971VariavelCocomo_MuitoAlto = new decimal[1] ;
         P006I4_n971VariavelCocomo_MuitoAlto = new bool[] {false} ;
         P006I4_A970VariavelCocomo_Alto = new decimal[1] ;
         P006I4_n970VariavelCocomo_Alto = new bool[] {false} ;
         P006I4_A969VariavelCocomo_Nominal = new decimal[1] ;
         P006I4_n969VariavelCocomo_Nominal = new bool[] {false} ;
         P006I4_A968VariavelCocomo_Baixo = new decimal[1] ;
         P006I4_n968VariavelCocomo_Baixo = new bool[] {false} ;
         P006I4_A967VariavelCocomo_MuitoBaixo = new decimal[1] ;
         P006I4_n967VariavelCocomo_MuitoBaixo = new bool[] {false} ;
         P006I4_A986VariavelCocomo_ExtraBaixo = new decimal[1] ;
         P006I4_n986VariavelCocomo_ExtraBaixo = new bool[] {false} ;
         P006I4_A966VariavelCocomo_Data = new DateTime[] {DateTime.MinValue} ;
         P006I4_n966VariavelCocomo_Data = new bool[] {false} ;
         P006I4_A992VariavelCocomo_Sequencial = new short[1] ;
         P006I4_A965VariavelCocomo_ProjetoCod = new int[1] ;
         P006I4_n965VariavelCocomo_ProjetoCod = new bool[] {false} ;
         P006I4_A963VariavelCocomo_Nome = new String[] {""} ;
         P006I4_A962VariavelCocomo_Sigla = new String[] {""} ;
         A966VariavelCocomo_Data = DateTime.MinValue;
         A963VariavelCocomo_Nome = "";
         W966VariavelCocomo_Data = DateTime.MinValue;
         Gx_emsg = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_updprojetofatorescala__default(),
            new Object[][] {
                new Object[] {
               P006I2_A961VariavelCocomo_AreaTrabalhoCod, P006I2_A964VariavelCocomo_Tipo, P006I2_A965VariavelCocomo_ProjetoCod, P006I2_n965VariavelCocomo_ProjetoCod, P006I2_A969VariavelCocomo_Nominal, P006I2_n969VariavelCocomo_Nominal, P006I2_A968VariavelCocomo_Baixo, P006I2_n968VariavelCocomo_Baixo, P006I2_A967VariavelCocomo_MuitoBaixo, P006I2_n967VariavelCocomo_MuitoBaixo,
               P006I2_A986VariavelCocomo_ExtraBaixo, P006I2_n986VariavelCocomo_ExtraBaixo, P006I2_A970VariavelCocomo_Alto, P006I2_n970VariavelCocomo_Alto, P006I2_A971VariavelCocomo_MuitoAlto, P006I2_n971VariavelCocomo_MuitoAlto, P006I2_A972VariavelCocomo_ExtraAlto, P006I2_n972VariavelCocomo_ExtraAlto, P006I2_A973VariavelCocomo_Usado, P006I2_n973VariavelCocomo_Usado,
               P006I2_A962VariavelCocomo_Sigla, P006I2_A992VariavelCocomo_Sequencial
               }
               , new Object[] {
               }
               , new Object[] {
               P006I4_A961VariavelCocomo_AreaTrabalhoCod, P006I4_A964VariavelCocomo_Tipo, P006I4_A973VariavelCocomo_Usado, P006I4_n973VariavelCocomo_Usado, P006I4_A972VariavelCocomo_ExtraAlto, P006I4_n972VariavelCocomo_ExtraAlto, P006I4_A971VariavelCocomo_MuitoAlto, P006I4_n971VariavelCocomo_MuitoAlto, P006I4_A970VariavelCocomo_Alto, P006I4_n970VariavelCocomo_Alto,
               P006I4_A969VariavelCocomo_Nominal, P006I4_n969VariavelCocomo_Nominal, P006I4_A968VariavelCocomo_Baixo, P006I4_n968VariavelCocomo_Baixo, P006I4_A967VariavelCocomo_MuitoBaixo, P006I4_n967VariavelCocomo_MuitoBaixo, P006I4_A986VariavelCocomo_ExtraBaixo, P006I4_n986VariavelCocomo_ExtraBaixo, P006I4_A966VariavelCocomo_Data, P006I4_n966VariavelCocomo_Data,
               P006I4_A992VariavelCocomo_Sequencial, P006I4_A965VariavelCocomo_ProjetoCod, P006I4_n965VariavelCocomo_ProjetoCod, P006I4_A963VariavelCocomo_Nome, P006I4_A962VariavelCocomo_Sigla
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV11i ;
      private short AV33GXLvl6 ;
      private short A992VariavelCocomo_Sequencial ;
      private short W992VariavelCocomo_Sequencial ;
      private int A961VariavelCocomo_AreaTrabalhoCod ;
      private int A965VariavelCocomo_ProjetoCod ;
      private int GX_INS120 ;
      private decimal A969VariavelCocomo_Nominal ;
      private decimal A968VariavelCocomo_Baixo ;
      private decimal A967VariavelCocomo_MuitoBaixo ;
      private decimal A986VariavelCocomo_ExtraBaixo ;
      private decimal A970VariavelCocomo_Alto ;
      private decimal A971VariavelCocomo_MuitoAlto ;
      private decimal A972VariavelCocomo_ExtraAlto ;
      private decimal A973VariavelCocomo_Usado ;
      private decimal W969VariavelCocomo_Nominal ;
      private decimal W968VariavelCocomo_Baixo ;
      private decimal W967VariavelCocomo_MuitoBaixo ;
      private decimal W986VariavelCocomo_ExtraBaixo ;
      private decimal W970VariavelCocomo_Alto ;
      private decimal W971VariavelCocomo_MuitoAlto ;
      private decimal W972VariavelCocomo_ExtraAlto ;
      private decimal W973VariavelCocomo_Usado ;
      private String A964VariavelCocomo_Tipo ;
      private String scmdbuf ;
      private String A962VariavelCocomo_Sigla ;
      private String A963VariavelCocomo_Nome ;
      private String Gx_emsg ;
      private DateTime A966VariavelCocomo_Data ;
      private DateTime W966VariavelCocomo_Data ;
      private bool n965VariavelCocomo_ProjetoCod ;
      private bool n969VariavelCocomo_Nominal ;
      private bool n968VariavelCocomo_Baixo ;
      private bool n967VariavelCocomo_MuitoBaixo ;
      private bool n986VariavelCocomo_ExtraBaixo ;
      private bool n970VariavelCocomo_Alto ;
      private bool n971VariavelCocomo_MuitoAlto ;
      private bool n972VariavelCocomo_ExtraAlto ;
      private bool n973VariavelCocomo_Usado ;
      private bool n966VariavelCocomo_Data ;
      private IGxSession AV29WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_VariavelCocomo_AreaTrabalhoCod ;
      private int aP1_VariavelCocomo_ProjetoCod ;
      private String aP2_VariavelCocomo_Tipo ;
      private IDataStoreProvider pr_default ;
      private int[] P006I2_A961VariavelCocomo_AreaTrabalhoCod ;
      private String[] P006I2_A964VariavelCocomo_Tipo ;
      private int[] P006I2_A965VariavelCocomo_ProjetoCod ;
      private bool[] P006I2_n965VariavelCocomo_ProjetoCod ;
      private decimal[] P006I2_A969VariavelCocomo_Nominal ;
      private bool[] P006I2_n969VariavelCocomo_Nominal ;
      private decimal[] P006I2_A968VariavelCocomo_Baixo ;
      private bool[] P006I2_n968VariavelCocomo_Baixo ;
      private decimal[] P006I2_A967VariavelCocomo_MuitoBaixo ;
      private bool[] P006I2_n967VariavelCocomo_MuitoBaixo ;
      private decimal[] P006I2_A986VariavelCocomo_ExtraBaixo ;
      private bool[] P006I2_n986VariavelCocomo_ExtraBaixo ;
      private decimal[] P006I2_A970VariavelCocomo_Alto ;
      private bool[] P006I2_n970VariavelCocomo_Alto ;
      private decimal[] P006I2_A971VariavelCocomo_MuitoAlto ;
      private bool[] P006I2_n971VariavelCocomo_MuitoAlto ;
      private decimal[] P006I2_A972VariavelCocomo_ExtraAlto ;
      private bool[] P006I2_n972VariavelCocomo_ExtraAlto ;
      private decimal[] P006I2_A973VariavelCocomo_Usado ;
      private bool[] P006I2_n973VariavelCocomo_Usado ;
      private String[] P006I2_A962VariavelCocomo_Sigla ;
      private short[] P006I2_A992VariavelCocomo_Sequencial ;
      private int[] P006I4_A961VariavelCocomo_AreaTrabalhoCod ;
      private String[] P006I4_A964VariavelCocomo_Tipo ;
      private decimal[] P006I4_A973VariavelCocomo_Usado ;
      private bool[] P006I4_n973VariavelCocomo_Usado ;
      private decimal[] P006I4_A972VariavelCocomo_ExtraAlto ;
      private bool[] P006I4_n972VariavelCocomo_ExtraAlto ;
      private decimal[] P006I4_A971VariavelCocomo_MuitoAlto ;
      private bool[] P006I4_n971VariavelCocomo_MuitoAlto ;
      private decimal[] P006I4_A970VariavelCocomo_Alto ;
      private bool[] P006I4_n970VariavelCocomo_Alto ;
      private decimal[] P006I4_A969VariavelCocomo_Nominal ;
      private bool[] P006I4_n969VariavelCocomo_Nominal ;
      private decimal[] P006I4_A968VariavelCocomo_Baixo ;
      private bool[] P006I4_n968VariavelCocomo_Baixo ;
      private decimal[] P006I4_A967VariavelCocomo_MuitoBaixo ;
      private bool[] P006I4_n967VariavelCocomo_MuitoBaixo ;
      private decimal[] P006I4_A986VariavelCocomo_ExtraBaixo ;
      private bool[] P006I4_n986VariavelCocomo_ExtraBaixo ;
      private DateTime[] P006I4_A966VariavelCocomo_Data ;
      private bool[] P006I4_n966VariavelCocomo_Data ;
      private short[] P006I4_A992VariavelCocomo_Sequencial ;
      private int[] P006I4_A965VariavelCocomo_ProjetoCod ;
      private bool[] P006I4_n965VariavelCocomo_ProjetoCod ;
      private String[] P006I4_A963VariavelCocomo_Nome ;
      private String[] P006I4_A962VariavelCocomo_Sigla ;
      [ObjectCollection(ItemType=typeof( SdtSDT_VariaveisCocomo_Item ))]
      private IGxCollection AV30sdt_VariaveisCocomo ;
   }

   public class prc_updprojetofatorescala__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new UpdateCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP006I2 ;
          prmP006I2 = new Object[] {
          new Object[] {"@VariavelCocomo_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@VariavelCocomo_Tipo",SqlDbType.Char,1,0} ,
          new Object[] {"@VariavelCocomo_ProjetoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006I3 ;
          prmP006I3 = new Object[] {
          new Object[] {"@VariavelCocomo_Nominal",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_Baixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_MuitoBaixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_ExtraBaixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_Alto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_MuitoAlto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_ExtraAlto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_Usado",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@VariavelCocomo_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@VariavelCocomo_Tipo",SqlDbType.Char,1,0} ,
          new Object[] {"@VariavelCocomo_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmP006I4 ;
          prmP006I4 = new Object[] {
          new Object[] {"@VariavelCocomo_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@VariavelCocomo_Tipo",SqlDbType.Char,1,0}
          } ;
          Object[] prmP006I5 ;
          prmP006I5 = new Object[] {
          new Object[] {"@VariavelCocomo_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@VariavelCocomo_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@VariavelCocomo_Tipo",SqlDbType.Char,1,0} ,
          new Object[] {"@VariavelCocomo_Sequencial",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@VariavelCocomo_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@VariavelCocomo_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@VariavelCocomo_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@VariavelCocomo_ExtraBaixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_MuitoBaixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_Baixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_Nominal",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_Alto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_MuitoAlto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_ExtraAlto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_Usado",SqlDbType.Decimal,12,2}
          } ;
          def= new CursorDef[] {
              new CursorDef("P006I2", "SELECT [VariavelCocomo_AreaTrabalhoCod], [VariavelCocomo_Tipo], [VariavelCocomo_ProjetoCod], [VariavelCocomo_Nominal], [VariavelCocomo_Baixo], [VariavelCocomo_MuitoBaixo], [VariavelCocomo_ExtraBaixo], [VariavelCocomo_Alto], [VariavelCocomo_MuitoAlto], [VariavelCocomo_ExtraAlto], [VariavelCocomo_Usado], [VariavelCocomo_Sigla], [VariavelCocomo_Sequencial] FROM [VariaveisCocomo] WITH (UPDLOCK) WHERE ([VariavelCocomo_AreaTrabalhoCod] = @VariavelCocomo_AreaTrabalhoCod) AND ([VariavelCocomo_Tipo] = @VariavelCocomo_Tipo) AND ([VariavelCocomo_ProjetoCod] = @VariavelCocomo_ProjetoCod) ORDER BY [VariavelCocomo_AreaTrabalhoCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006I2,1,0,true,false )
             ,new CursorDef("P006I3", "UPDATE [VariaveisCocomo] SET [VariavelCocomo_Nominal]=@VariavelCocomo_Nominal, [VariavelCocomo_Baixo]=@VariavelCocomo_Baixo, [VariavelCocomo_MuitoBaixo]=@VariavelCocomo_MuitoBaixo, [VariavelCocomo_ExtraBaixo]=@VariavelCocomo_ExtraBaixo, [VariavelCocomo_Alto]=@VariavelCocomo_Alto, [VariavelCocomo_MuitoAlto]=@VariavelCocomo_MuitoAlto, [VariavelCocomo_ExtraAlto]=@VariavelCocomo_ExtraAlto, [VariavelCocomo_Usado]=@VariavelCocomo_Usado  WHERE [VariavelCocomo_AreaTrabalhoCod] = @VariavelCocomo_AreaTrabalhoCod AND [VariavelCocomo_Sigla] = @VariavelCocomo_Sigla AND [VariavelCocomo_Tipo] = @VariavelCocomo_Tipo AND [VariavelCocomo_Sequencial] = @VariavelCocomo_Sequencial", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP006I3)
             ,new CursorDef("P006I4", "SELECT [VariavelCocomo_AreaTrabalhoCod], [VariavelCocomo_Tipo], [VariavelCocomo_Usado], [VariavelCocomo_ExtraAlto], [VariavelCocomo_MuitoAlto], [VariavelCocomo_Alto], [VariavelCocomo_Nominal], [VariavelCocomo_Baixo], [VariavelCocomo_MuitoBaixo], [VariavelCocomo_ExtraBaixo], [VariavelCocomo_Data], [VariavelCocomo_Sequencial], [VariavelCocomo_ProjetoCod], [VariavelCocomo_Nome], [VariavelCocomo_Sigla] FROM [VariaveisCocomo] WITH (NOLOCK) WHERE ([VariavelCocomo_AreaTrabalhoCod] = @VariavelCocomo_AreaTrabalhoCod) AND ([VariavelCocomo_Tipo] = @VariavelCocomo_Tipo) AND ([VariavelCocomo_ProjetoCod] IS NULL) ORDER BY [VariavelCocomo_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006I4,100,0,true,false )
             ,new CursorDef("P006I5", "INSERT INTO [VariaveisCocomo]([VariavelCocomo_AreaTrabalhoCod], [VariavelCocomo_Sigla], [VariavelCocomo_Tipo], [VariavelCocomo_Sequencial], [VariavelCocomo_Nome], [VariavelCocomo_ProjetoCod], [VariavelCocomo_Data], [VariavelCocomo_ExtraBaixo], [VariavelCocomo_MuitoBaixo], [VariavelCocomo_Baixo], [VariavelCocomo_Nominal], [VariavelCocomo_Alto], [VariavelCocomo_MuitoAlto], [VariavelCocomo_ExtraAlto], [VariavelCocomo_Usado]) VALUES(@VariavelCocomo_AreaTrabalhoCod, @VariavelCocomo_Sigla, @VariavelCocomo_Tipo, @VariavelCocomo_Sequencial, @VariavelCocomo_Nome, @VariavelCocomo_ProjetoCod, @VariavelCocomo_Data, @VariavelCocomo_ExtraBaixo, @VariavelCocomo_MuitoBaixo, @VariavelCocomo_Baixo, @VariavelCocomo_Nominal, @VariavelCocomo_Alto, @VariavelCocomo_MuitoAlto, @VariavelCocomo_ExtraAlto, @VariavelCocomo_Usado)", GxErrorMask.GX_NOMASK,prmP006I5)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((decimal[]) buf[14])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((decimal[]) buf[16])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((String[]) buf[20])[0] = rslt.getString(12, 15) ;
                ((short[]) buf[21])[0] = rslt.getShort(13) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((decimal[]) buf[14])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((decimal[]) buf[16])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[18])[0] = rslt.getGXDate(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((short[]) buf[20])[0] = rslt.getShort(12) ;
                ((int[]) buf[21])[0] = rslt.getInt(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getString(14, 50) ;
                ((String[]) buf[24])[0] = rslt.getString(15, 15) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[3]);
                }
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (decimal)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 8 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(8, (decimal)parms[15]);
                }
                stmt.SetParameter(9, (int)parms[16]);
                stmt.SetParameter(10, (String)parms[17]);
                stmt.SetParameter(11, (String)parms[18]);
                stmt.SetParameter(12, (short)parms[19]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                stmt.SetParameter(5, (String)parms[4]);
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 7 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(7, (DateTime)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 8 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(8, (decimal)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 9 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(9, (decimal)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 10 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(10, (decimal)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 11 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(11, (decimal)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 12 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(12, (decimal)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 13 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(13, (decimal)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 14 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(14, (decimal)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 15 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(15, (decimal)parms[24]);
                }
                return;
       }
    }

 }

}
