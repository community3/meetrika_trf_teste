/*
               File: Rel_ContagemGerencial
        Description: Relat�rio Gerencial
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:53.34
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class arel_contagemgerencial : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV109Data = context.localUtil.ParseDateParm( gxfirstwebparm);
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV47ContagemResultado_LoteAceite = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV236Filtro = GetNextPar( );
                  AV136Inicio = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV237Fim = context.localUtil.ParseDateParm( GetNextPar( ));
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public arel_contagemgerencial( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public arel_contagemgerencial( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( DateTime aP0_Data ,
                           int aP1_ContagemResultado_LoteAceite ,
                           String aP2_Filtro ,
                           DateTime aP3_Inicio ,
                           DateTime aP4_Fim )
      {
         this.AV109Data = aP0_Data;
         this.AV47ContagemResultado_LoteAceite = aP1_ContagemResultado_LoteAceite;
         this.AV236Filtro = aP2_Filtro;
         this.AV136Inicio = aP3_Inicio;
         this.AV237Fim = aP4_Fim;
         initialize();
         executePrivate();
      }

      public void executeSubmit( DateTime aP0_Data ,
                                 int aP1_ContagemResultado_LoteAceite ,
                                 String aP2_Filtro ,
                                 DateTime aP3_Inicio ,
                                 DateTime aP4_Fim )
      {
         arel_contagemgerencial objarel_contagemgerencial;
         objarel_contagemgerencial = new arel_contagemgerencial();
         objarel_contagemgerencial.AV109Data = aP0_Data;
         objarel_contagemgerencial.AV47ContagemResultado_LoteAceite = aP1_ContagemResultado_LoteAceite;
         objarel_contagemgerencial.AV236Filtro = aP2_Filtro;
         objarel_contagemgerencial.AV136Inicio = aP3_Inicio;
         objarel_contagemgerencial.AV237Fim = aP4_Fim;
         objarel_contagemgerencial.context.SetSubmitInitialConfig(context);
         objarel_contagemgerencial.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objarel_contagemgerencial);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((arel_contagemgerencial)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 4;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 2, 1, 12240, 15840, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*4));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV164WWPContext) ;
            AV221ContratanteArea = StringUtil.Trim( AV164WWPContext.gxTpr_Contratante_nomefantasia);
            GXt_boolean1 = AV142OSAutomatica;
            GXt_int2 = AV164WWPContext.gxTpr_Areatrabalho_codigo;
            new prc_osautomatica(context ).execute( ref  GXt_int2, out  GXt_boolean1) ;
            AV164WWPContext.gxTpr_Areatrabalho_codigo = GXt_int2;
            AV142OSAutomatica = GXt_boolean1;
            AV114DescricaoLength = 50;
            AV113DescricaoGlsLength = 120;
            AV219Hoje = DateTimeUtil.ServerNow( context, "DEFAULT");
            AV111DataInicio = AV136Inicio;
            AV110DataFim = AV237Fim;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV236Filtro)) )
            {
               AV235Periodo = "Per�odo";
            }
            else
            {
               AV235Periodo = AV236Filtro;
            }
            if ( (0==AV47ContagemResultado_LoteAceite) )
            {
               AV173PrdFtrIni = AV109Data;
               AV174PrdFtrFim = AV109Data;
               AV12Codigos.FromXml(AV163WebSession.Get("SdtCodigos"), "Collection");
               AV148Quantidade = (short)(AV12Codigos.Count);
               AV157SubTitulo = "(Demandas para Homologa��o)";
               AV11Codigo = (int)(AV12Codigos.GetNumeric(1));
               /* Using cursor P008W2 */
               pr_default.execute(0, new Object[] {AV11Codigo});
               while ( (pr_default.getStatus(0) != 101) )
               {
                  A146Modulo_Codigo = P008W2_A146Modulo_Codigo[0];
                  n146Modulo_Codigo = P008W2_n146Modulo_Codigo[0];
                  A127Sistema_Codigo = P008W2_A127Sistema_Codigo[0];
                  A135Sistema_AreaTrabalhoCod = P008W2_A135Sistema_AreaTrabalhoCod[0];
                  A1620ContagemResultado_CntSrvUndCnt = P008W2_A1620ContagemResultado_CntSrvUndCnt[0];
                  n1620ContagemResultado_CntSrvUndCnt = P008W2_n1620ContagemResultado_CntSrvUndCnt[0];
                  A29Contratante_Codigo = P008W2_A29Contratante_Codigo[0];
                  n29Contratante_Codigo = P008W2_n29Contratante_Codigo[0];
                  A456ContagemResultado_Codigo = P008W2_A456ContagemResultado_Codigo[0];
                  A40001ContagemResultado_ContratadaLogo_GXI = P008W2_A40001ContagemResultado_ContratadaLogo_GXI[0];
                  n40001ContagemResultado_ContratadaLogo_GXI = P008W2_n40001ContagemResultado_ContratadaLogo_GXI[0];
                  A490ContagemResultado_ContratadaCod = P008W2_A490ContagemResultado_ContratadaCod[0];
                  n490ContagemResultado_ContratadaCod = P008W2_n490ContagemResultado_ContratadaCod[0];
                  A601ContagemResultado_Servico = P008W2_A601ContagemResultado_Servico[0];
                  n601ContagemResultado_Servico = P008W2_n601ContagemResultado_Servico[0];
                  A1603ContagemResultado_CntCod = P008W2_A1603ContagemResultado_CntCod[0];
                  n1603ContagemResultado_CntCod = P008W2_n1603ContagemResultado_CntCod[0];
                  A1553ContagemResultado_CntSrvCod = P008W2_A1553ContagemResultado_CntSrvCod[0];
                  n1553ContagemResultado_CntSrvCod = P008W2_n1553ContagemResultado_CntSrvCod[0];
                  A1621ContagemResultado_CntSrvUndCntSgl = P008W2_A1621ContagemResultado_CntSrvUndCntSgl[0];
                  n1621ContagemResultado_CntSrvUndCntSgl = P008W2_n1621ContagemResultado_CntSrvUndCntSgl[0];
                  A803ContagemResultado_ContratadaSigla = P008W2_A803ContagemResultado_ContratadaSigla[0];
                  n803ContagemResultado_ContratadaSigla = P008W2_n803ContagemResultado_ContratadaSigla[0];
                  A2034Contratante_TtlRltGerencial = P008W2_A2034Contratante_TtlRltGerencial[0];
                  n2034Contratante_TtlRltGerencial = P008W2_n2034Contratante_TtlRltGerencial[0];
                  A1816ContagemResultado_ContratadaLogo = P008W2_A1816ContagemResultado_ContratadaLogo[0];
                  n1816ContagemResultado_ContratadaLogo = P008W2_n1816ContagemResultado_ContratadaLogo[0];
                  A127Sistema_Codigo = P008W2_A127Sistema_Codigo[0];
                  A135Sistema_AreaTrabalhoCod = P008W2_A135Sistema_AreaTrabalhoCod[0];
                  A29Contratante_Codigo = P008W2_A29Contratante_Codigo[0];
                  n29Contratante_Codigo = P008W2_n29Contratante_Codigo[0];
                  A2034Contratante_TtlRltGerencial = P008W2_A2034Contratante_TtlRltGerencial[0];
                  n2034Contratante_TtlRltGerencial = P008W2_n2034Contratante_TtlRltGerencial[0];
                  A40001ContagemResultado_ContratadaLogo_GXI = P008W2_A40001ContagemResultado_ContratadaLogo_GXI[0];
                  n40001ContagemResultado_ContratadaLogo_GXI = P008W2_n40001ContagemResultado_ContratadaLogo_GXI[0];
                  A803ContagemResultado_ContratadaSigla = P008W2_A803ContagemResultado_ContratadaSigla[0];
                  n803ContagemResultado_ContratadaSigla = P008W2_n803ContagemResultado_ContratadaSigla[0];
                  A1816ContagemResultado_ContratadaLogo = P008W2_A1816ContagemResultado_ContratadaLogo[0];
                  n1816ContagemResultado_ContratadaLogo = P008W2_n1816ContagemResultado_ContratadaLogo[0];
                  A1620ContagemResultado_CntSrvUndCnt = P008W2_A1620ContagemResultado_CntSrvUndCnt[0];
                  n1620ContagemResultado_CntSrvUndCnt = P008W2_n1620ContagemResultado_CntSrvUndCnt[0];
                  A601ContagemResultado_Servico = P008W2_A601ContagemResultado_Servico[0];
                  n601ContagemResultado_Servico = P008W2_n601ContagemResultado_Servico[0];
                  A1603ContagemResultado_CntCod = P008W2_A1603ContagemResultado_CntCod[0];
                  n1603ContagemResultado_CntCod = P008W2_n1603ContagemResultado_CntCod[0];
                  A1621ContagemResultado_CntSrvUndCntSgl = P008W2_A1621ContagemResultado_CntSrvUndCntSgl[0];
                  n1621ContagemResultado_CntSrvUndCntSgl = P008W2_n1621ContagemResultado_CntSrvUndCntSgl[0];
                  OV241Contratante_TtlRltGerencial = AV241Contratante_TtlRltGerencial;
                  AV105Contratada = A490ContagemResultado_ContratadaCod;
                  AV212Servico = A601ContagemResultado_Servico;
                  AV228Contrato_Codigo = A1603ContagemResultado_CntCod;
                  AV232ContratoServicos_Codigo = A1553ContagemResultado_CntSrvCod;
                  AV220UnidadeMedicao_Sigla = A1621ContagemResultado_CntSrvUndCntSgl;
                  AV233Contratada_Logo = A1816ContagemResultado_ContratadaLogo;
                  A40000Contratada_Logo_GXI = A40001ContagemResultado_ContratadaLogo_GXI;
                  AV234Contratada_Sigla = A803ContagemResultado_ContratadaSigla;
                  /* Execute user subroutine: 'DADOSDOCONTRATO' */
                  S271 ();
                  if ( returnInSub )
                  {
                     pr_default.close(0);
                     this.cleanup();
                     if (true) return;
                  }
                  AV241Contratante_TtlRltGerencial = A2034Contratante_TtlRltGerencial;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(0);
               pr_default.dynParam(1, new Object[]{ new Object[]{
                                                    A456ContagemResultado_Codigo ,
                                                    AV12Codigos ,
                                                    A597ContagemResultado_LoteAceiteCod ,
                                                    AV47ContagemResultado_LoteAceite ,
                                                    AV232ContratoServicos_Codigo ,
                                                    AV251ContratoServicosIndicador_Tipo ,
                                                    A1270ContratoServicosIndicador_CntSrvCod ,
                                                    A1308ContratoServicosIndicador_Tipo ,
                                                    A517ContagemResultado_Ultima },
                                                    new int[] {
                                                    TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                                    }
               });
               /* Using cursor P008W4 */
               pr_default.execute(1);
               if ( (pr_default.getStatus(1) != 101) )
               {
                  A40002ContagemResultado_DataUltCnt = P008W4_A40002ContagemResultado_DataUltCnt[0];
               }
               else
               {
                  A40002ContagemResultado_DataUltCnt = DateTime.MinValue;
               }
               pr_default.close(1);
               pr_default.dynParam(2, new Object[]{ new Object[]{
                                                    A456ContagemResultado_Codigo ,
                                                    AV12Codigos ,
                                                    A597ContagemResultado_LoteAceiteCod ,
                                                    AV47ContagemResultado_LoteAceite ,
                                                    AV232ContratoServicos_Codigo ,
                                                    AV251ContratoServicosIndicador_Tipo ,
                                                    A1270ContratoServicosIndicador_CntSrvCod ,
                                                    A1308ContratoServicosIndicador_Tipo },
                                                    new int[] {
                                                    TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN
                                                    }
               });
               /* Using cursor P008W6 */
               pr_default.execute(2);
               if ( (pr_default.getStatus(2) != 101) )
               {
                  A40003GXC4 = P008W6_A40003GXC4[0];
               }
               else
               {
                  A40003GXC4 = (DateTime)(DateTime.MinValue);
               }
               pr_default.close(2);
               pr_default.dynParam(3, new Object[]{ new Object[]{
                                                    A456ContagemResultado_Codigo ,
                                                    AV12Codigos ,
                                                    A597ContagemResultado_LoteAceiteCod ,
                                                    AV47ContagemResultado_LoteAceite ,
                                                    AV232ContratoServicos_Codigo ,
                                                    AV251ContratoServicosIndicador_Tipo ,
                                                    A1270ContratoServicosIndicador_CntSrvCod ,
                                                    A1308ContratoServicosIndicador_Tipo },
                                                    new int[] {
                                                    TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN
                                                    }
               });
               /* Using cursor P008W8 */
               pr_default.execute(3);
               if ( (pr_default.getStatus(3) != 101) )
               {
                  A40004GXC5 = P008W8_A40004GXC5[0];
               }
               else
               {
                  A40004GXC5 = DateTime.MinValue;
               }
               pr_default.close(3);
               pr_default.dynParam(4, new Object[]{ new Object[]{
                                                    A456ContagemResultado_Codigo ,
                                                    AV12Codigos ,
                                                    A597ContagemResultado_LoteAceiteCod ,
                                                    AV47ContagemResultado_LoteAceite ,
                                                    AV232ContratoServicos_Codigo ,
                                                    AV251ContratoServicosIndicador_Tipo ,
                                                    A1270ContratoServicosIndicador_CntSrvCod ,
                                                    A1308ContratoServicosIndicador_Tipo },
                                                    new int[] {
                                                    TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN
                                                    }
               });
               /* Using cursor P008W10 */
               pr_default.execute(4);
               if ( (pr_default.getStatus(4) != 101) )
               {
                  A40005GXC6 = P008W10_A40005GXC6[0];
               }
               else
               {
                  A40005GXC6 = (DateTime)(DateTime.MinValue);
               }
               pr_default.close(4);
               pr_default.dynParam(5, new Object[]{ new Object[]{
                                                    A456ContagemResultado_Codigo ,
                                                    AV12Codigos ,
                                                    A597ContagemResultado_LoteAceiteCod ,
                                                    AV47ContagemResultado_LoteAceite ,
                                                    AV232ContratoServicos_Codigo ,
                                                    AV251ContratoServicosIndicador_Tipo ,
                                                    A1270ContratoServicosIndicador_CntSrvCod ,
                                                    A1308ContratoServicosIndicador_Tipo ,
                                                    A517ContagemResultado_Ultima },
                                                    new int[] {
                                                    TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                                    }
               });
               /* Using cursor P008W12 */
               pr_default.execute(5);
               if ( (pr_default.getStatus(5) != 101) )
               {
                  A40006GXC7 = P008W12_A40006GXC7[0];
               }
               else
               {
                  A40006GXC7 = DateTime.MinValue;
               }
               pr_default.close(5);
               pr_default.dynParam(6, new Object[]{ new Object[]{
                                                    A456ContagemResultado_Codigo ,
                                                    AV12Codigos ,
                                                    A597ContagemResultado_LoteAceiteCod ,
                                                    AV47ContagemResultado_LoteAceite ,
                                                    AV232ContratoServicos_Codigo ,
                                                    AV251ContratoServicosIndicador_Tipo ,
                                                    A1270ContratoServicosIndicador_CntSrvCod ,
                                                    A1308ContratoServicosIndicador_Tipo },
                                                    new int[] {
                                                    TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN
                                                    }
               });
               /* Using cursor P008W14 */
               pr_default.execute(6);
               if ( (pr_default.getStatus(6) != 101) )
               {
                  A40007GXC8 = P008W14_A40007GXC8[0];
               }
               else
               {
                  A40007GXC8 = (DateTime)(DateTime.MinValue);
               }
               pr_default.close(6);
               pr_default.dynParam(7, new Object[]{ new Object[]{
                                                    A456ContagemResultado_Codigo ,
                                                    AV12Codigos ,
                                                    A597ContagemResultado_LoteAceiteCod ,
                                                    AV47ContagemResultado_LoteAceite ,
                                                    AV232ContratoServicos_Codigo ,
                                                    AV251ContratoServicosIndicador_Tipo ,
                                                    A1270ContratoServicosIndicador_CntSrvCod ,
                                                    A1308ContratoServicosIndicador_Tipo },
                                                    new int[] {
                                                    TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN
                                                    }
               });
               /* Using cursor P008W16 */
               pr_default.execute(7);
               if ( (pr_default.getStatus(7) != 101) )
               {
                  A40008GXC9 = P008W16_A40008GXC9[0];
               }
               else
               {
                  A40008GXC9 = DateTime.MinValue;
               }
               pr_default.close(7);
               pr_default.dynParam(8, new Object[]{ new Object[]{
                                                    A456ContagemResultado_Codigo ,
                                                    AV12Codigos ,
                                                    A597ContagemResultado_LoteAceiteCod ,
                                                    AV47ContagemResultado_LoteAceite ,
                                                    AV232ContratoServicos_Codigo ,
                                                    AV251ContratoServicosIndicador_Tipo ,
                                                    A1270ContratoServicosIndicador_CntSrvCod ,
                                                    A1308ContratoServicosIndicador_Tipo },
                                                    new int[] {
                                                    TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN
                                                    }
               });
               /* Using cursor P008W18 */
               pr_default.execute(8);
               if ( (pr_default.getStatus(8) != 101) )
               {
                  A40009GXC10 = P008W18_A40009GXC10[0];
               }
               else
               {
                  A40009GXC10 = (DateTime)(DateTime.MinValue);
               }
               pr_default.close(8);
               GXt_decimal3 = AV242Usado;
               new prc_usadodocontrato(context ).execute( ref  AV228Contrato_Codigo, out  GXt_decimal3) ;
               AV242Usado = GXt_decimal3;
               AV244Saldo = (decimal)(AV243Contratado-AV242Usado);
               if ( (DateTime.MinValue==AV111DataInicio) )
               {
                  if ( StringUtil.StrCmp(AV236Filtro, "Per�odo de entrega") == 0 )
                  {
                     AV111DataInicio = A40002ContagemResultado_DataUltCnt;
                  }
                  else if ( StringUtil.StrCmp(AV236Filtro, "Per�odo de data prevista") == 0 )
                  {
                     AV111DataInicio = DateTimeUtil.ResetTime(A40003GXC4);
                  }
                  else if ( StringUtil.StrCmp(AV236Filtro, "Per�odo de solicita��o") == 0 )
                  {
                     AV111DataInicio = A40004GXC5;
                  }
                  else if ( StringUtil.StrCmp(AV236Filtro, "Per�odo de homologa��o") == 0 )
                  {
                     AV111DataInicio = DateTimeUtil.ResetTime(A40005GXC6);
                  }
                  else if ( StringUtil.StrCmp(AV236Filtro, "Per�odo de homologa��o") == 0 )
                  {
                     AV111DataInicio = DateTimeUtil.ResetTime(A40005GXC6);
                  }
               }
               if ( (DateTime.MinValue==AV110DataFim) )
               {
                  if ( StringUtil.StrCmp(AV236Filtro, "Per�odo de entrega") == 0 )
                  {
                     AV110DataFim = A40006GXC7;
                  }
                  else if ( StringUtil.StrCmp(AV236Filtro, "Per�odo de data prevista") == 0 )
                  {
                     AV110DataFim = DateTimeUtil.ResetTime(A40007GXC8);
                  }
                  else if ( StringUtil.StrCmp(AV236Filtro, "Per�odo de solicita��o") == 0 )
                  {
                     AV110DataFim = A40008GXC9;
                  }
                  else if ( StringUtil.StrCmp(AV236Filtro, "Per�odo de homologa��o") == 0 )
                  {
                     AV110DataFim = DateTimeUtil.ResetTime(A40009GXC10);
                  }
                  else if ( StringUtil.StrCmp(AV236Filtro, "Per�odo de homologa��o") == 0 )
                  {
                     AV110DataFim = DateTimeUtil.ResetTime(A40009GXC10);
                  }
               }
               AV235Periodo = AV235Periodo + " " + context.localUtil.DToC( AV111DataInicio, 2, "/") + " - " + context.localUtil.DToC( AV110DataFim, 2, "/");
               /* Execute user subroutine: 'CNPJ' */
               S261 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
               /* Execute user subroutine: 'PRINTDATA' */
               S111 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
            }
            else
            {
               /* Using cursor P008W20 */
               pr_default.execute(9, new Object[] {AV47ContagemResultado_LoteAceite});
               while ( (pr_default.getStatus(9) != 101) )
               {
                  A1620ContagemResultado_CntSrvUndCnt = P008W20_A1620ContagemResultado_CntSrvUndCnt[0];
                  n1620ContagemResultado_CntSrvUndCnt = P008W20_n1620ContagemResultado_CntSrvUndCnt[0];
                  A456ContagemResultado_Codigo = P008W20_A456ContagemResultado_Codigo[0];
                  A597ContagemResultado_LoteAceiteCod = P008W20_A597ContagemResultado_LoteAceiteCod[0];
                  n597ContagemResultado_LoteAceiteCod = P008W20_n597ContagemResultado_LoteAceiteCod[0];
                  A40001ContagemResultado_ContratadaLogo_GXI = P008W20_A40001ContagemResultado_ContratadaLogo_GXI[0];
                  n40001ContagemResultado_ContratadaLogo_GXI = P008W20_n40001ContagemResultado_ContratadaLogo_GXI[0];
                  A490ContagemResultado_ContratadaCod = P008W20_A490ContagemResultado_ContratadaCod[0];
                  n490ContagemResultado_ContratadaCod = P008W20_n490ContagemResultado_ContratadaCod[0];
                  A601ContagemResultado_Servico = P008W20_A601ContagemResultado_Servico[0];
                  n601ContagemResultado_Servico = P008W20_n601ContagemResultado_Servico[0];
                  A1603ContagemResultado_CntCod = P008W20_A1603ContagemResultado_CntCod[0];
                  n1603ContagemResultado_CntCod = P008W20_n1603ContagemResultado_CntCod[0];
                  A1553ContagemResultado_CntSrvCod = P008W20_A1553ContagemResultado_CntSrvCod[0];
                  n1553ContagemResultado_CntSrvCod = P008W20_n1553ContagemResultado_CntSrvCod[0];
                  A1621ContagemResultado_CntSrvUndCntSgl = P008W20_A1621ContagemResultado_CntSrvUndCntSgl[0];
                  n1621ContagemResultado_CntSrvUndCntSgl = P008W20_n1621ContagemResultado_CntSrvUndCntSgl[0];
                  A803ContagemResultado_ContratadaSigla = P008W20_A803ContagemResultado_ContratadaSigla[0];
                  n803ContagemResultado_ContratadaSigla = P008W20_n803ContagemResultado_ContratadaSigla[0];
                  A566ContagemResultado_DataUltCnt = P008W20_A566ContagemResultado_DataUltCnt[0];
                  n566ContagemResultado_DataUltCnt = P008W20_n566ContagemResultado_DataUltCnt[0];
                  A1816ContagemResultado_ContratadaLogo = P008W20_A1816ContagemResultado_ContratadaLogo[0];
                  n1816ContagemResultado_ContratadaLogo = P008W20_n1816ContagemResultado_ContratadaLogo[0];
                  A566ContagemResultado_DataUltCnt = P008W20_A566ContagemResultado_DataUltCnt[0];
                  n566ContagemResultado_DataUltCnt = P008W20_n566ContagemResultado_DataUltCnt[0];
                  A40001ContagemResultado_ContratadaLogo_GXI = P008W20_A40001ContagemResultado_ContratadaLogo_GXI[0];
                  n40001ContagemResultado_ContratadaLogo_GXI = P008W20_n40001ContagemResultado_ContratadaLogo_GXI[0];
                  A803ContagemResultado_ContratadaSigla = P008W20_A803ContagemResultado_ContratadaSigla[0];
                  n803ContagemResultado_ContratadaSigla = P008W20_n803ContagemResultado_ContratadaSigla[0];
                  A1816ContagemResultado_ContratadaLogo = P008W20_A1816ContagemResultado_ContratadaLogo[0];
                  n1816ContagemResultado_ContratadaLogo = P008W20_n1816ContagemResultado_ContratadaLogo[0];
                  A1620ContagemResultado_CntSrvUndCnt = P008W20_A1620ContagemResultado_CntSrvUndCnt[0];
                  n1620ContagemResultado_CntSrvUndCnt = P008W20_n1620ContagemResultado_CntSrvUndCnt[0];
                  A601ContagemResultado_Servico = P008W20_A601ContagemResultado_Servico[0];
                  n601ContagemResultado_Servico = P008W20_n601ContagemResultado_Servico[0];
                  A1603ContagemResultado_CntCod = P008W20_A1603ContagemResultado_CntCod[0];
                  n1603ContagemResultado_CntCod = P008W20_n1603ContagemResultado_CntCod[0];
                  A1621ContagemResultado_CntSrvUndCntSgl = P008W20_A1621ContagemResultado_CntSrvUndCntSgl[0];
                  n1621ContagemResultado_CntSrvUndCntSgl = P008W20_n1621ContagemResultado_CntSrvUndCntSgl[0];
                  AV105Contratada = A490ContagemResultado_ContratadaCod;
                  AV212Servico = A601ContagemResultado_Servico;
                  AV173PrdFtrIni = A566ContagemResultado_DataUltCnt;
                  AV174PrdFtrFim = A566ContagemResultado_DataUltCnt;
                  AV228Contrato_Codigo = A1603ContagemResultado_CntCod;
                  AV232ContratoServicos_Codigo = A1553ContagemResultado_CntSrvCod;
                  AV220UnidadeMedicao_Sigla = A1621ContagemResultado_CntSrvUndCntSgl;
                  AV233Contratada_Logo = A1816ContagemResultado_ContratadaLogo;
                  A40000Contratada_Logo_GXI = A40001ContagemResultado_ContratadaLogo_GXI;
                  AV234Contratada_Sigla = A803ContagemResultado_ContratadaSigla;
                  /* Execute user subroutine: 'DADOSDOCONTRATO' */
                  S271 ();
                  if ( returnInSub )
                  {
                     pr_default.close(9);
                     this.cleanup();
                     if (true) return;
                  }
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(9);
               }
               pr_default.close(9);
               AV157SubTitulo = "";
               /* Using cursor P008W23 */
               pr_default.execute(10, new Object[] {AV47ContagemResultado_LoteAceite});
               while ( (pr_default.getStatus(10) != 101) )
               {
                  A563Lote_Nome = P008W23_A563Lote_Nome[0];
                  A562Lote_Numero = P008W23_A562Lote_Numero[0];
                  A2055Lote_CntUsado = P008W23_A2055Lote_CntUsado[0];
                  n2055Lote_CntUsado = P008W23_n2055Lote_CntUsado[0];
                  A2056Lote_CntSaldo = P008W23_A2056Lote_CntSaldo[0];
                  n2056Lote_CntSaldo = P008W23_n2056Lote_CntSaldo[0];
                  A596Lote_Codigo = P008W23_A596Lote_Codigo[0];
                  A567Lote_DataIni = P008W23_A567Lote_DataIni[0];
                  A568Lote_DataFim = P008W23_A568Lote_DataFim[0];
                  A567Lote_DataIni = P008W23_A567Lote_DataIni[0];
                  A568Lote_DataFim = P008W23_A568Lote_DataFim[0];
                  AV157SubTitulo = A563Lote_Nome;
                  AV108d = (decimal)(StringUtil.Len( A562Lote_Numero)-3);
                  AV138NumeroRelatorio = StringUtil.Substring( A562Lote_Numero, (int)(AV108d), 4);
                  AV108d = (decimal)(AV108d-2);
                  AV138NumeroRelatorio = StringUtil.Substring( A562Lote_Numero, (int)(AV108d), 2) + "/" + AV138NumeroRelatorio;
                  AV108d = (decimal)(AV108d-1);
                  AV138NumeroRelatorio = "Lote " + StringUtil.Substring( AV138NumeroRelatorio, 4, 4) + "/" + StringUtil.PadL( StringUtil.Substring( A562Lote_Numero, 1, (int)(AV108d)), 3, "0");
                  AV111DataInicio = A567Lote_DataIni;
                  AV110DataFim = A568Lote_DataFim;
                  AV242Usado = A2055Lote_CntUsado;
                  AV244Saldo = A2056Lote_CntSaldo;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(10);
               AV235Periodo = AV235Periodo + " " + context.localUtil.DToC( AV111DataInicio, 2, "/") + " - " + context.localUtil.DToC( AV110DataFim, 2, "/");
               /* Execute user subroutine: 'CNPJ' */
               S261 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
               /* Execute user subroutine: 'PRINTDATALOTE' */
               S241 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
            }
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H8W0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'PRINTDATA' Routine */
         pr_default.dynParam(11, new Object[]{ new Object[]{
                                              A456ContagemResultado_Codigo ,
                                              AV12Codigos },
                                              new int[] {
                                              TypeConstants.INT
                                              }
         });
         /* Using cursor P008W24 */
         pr_default.execute(11);
         while ( (pr_default.getStatus(11) != 101) )
         {
            A484ContagemResultado_StatusDmn = P008W24_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P008W24_n484ContagemResultado_StatusDmn[0];
            A1854ContagemResultado_VlrCnc = P008W24_A1854ContagemResultado_VlrCnc[0];
            n1854ContagemResultado_VlrCnc = P008W24_n1854ContagemResultado_VlrCnc[0];
            A512ContagemResultado_ValorPF = P008W24_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P008W24_n512ContagemResultado_ValorPF[0];
            A456ContagemResultado_Codigo = P008W24_A456ContagemResultado_Codigo[0];
            GXt_decimal3 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal3) ;
            A574ContagemResultado_PFFinal = GXt_decimal3;
            A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*A512ContagemResultado_ValorPF);
            AV71ContagemResultado_PFTotal = (decimal)(AV71ContagemResultado_PFTotal+A574ContagemResultado_PFFinal);
            if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 )
            {
               AV72ContagemResultado_PFTotalFaturar = (decimal)(AV72ContagemResultado_PFTotalFaturar+A1854ContagemResultado_VlrCnc);
            }
            else
            {
               AV72ContagemResultado_PFTotalFaturar = (decimal)(AV72ContagemResultado_PFTotalFaturar+A606ContagemResultado_ValorFinal);
            }
            pr_default.readNext(11);
         }
         pr_default.close(11);
         AV108d = (decimal)(DateTimeUtil.Day( AV111DataInicio)/ (decimal)(7));
         if ( ( DateTimeUtil.Day( AV111DataInicio) <= 7 ) && ( DateTimeUtil.Day( AV111DataInicio) > DateTimeUtil.Dow( AV111DataInicio) ) )
         {
            AV108d = (decimal)(AV108d+1);
         }
         if ( ( AV108d != Convert.ToDecimal( NumberUtil.Int( (long)(AV108d)) )) )
         {
            AV108d = (decimal)(NumberUtil.Int( (long)(AV108d))+1);
         }
         AV138NumeroRelatorio = "NR " + StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( AV111DataInicio)), 10, 0)) + "/" + StringUtil.PadL( StringUtil.Trim( StringUtil.Str( (decimal)(NumberUtil.Int( (long)(AV108d))), 10, 0)), 3, "0");
         AV245SaldoPrev = (decimal)(AV244Saldo-AV71ContagemResultado_PFTotal);
         H8W0( false, 218) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("consumidos/as:", 146, Gx_line+100, 246, Gx_line+118, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText("contratados/as:", 148, Gx_line+67, 246, Gx_line+85, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText("Saldo Atual:", 170, Gx_line+133, 246, Gx_line+151, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV242Usado, "ZZ,ZZZ,ZZ9.999")), 258, Gx_line+100, 361, Gx_line+118, 2+256, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV220UnidadeMedicao_Sigla, "@!")), 75, Gx_line+67, 133, Gx_line+85, 2, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV220UnidadeMedicao_Sigla, "@!")), 75, Gx_line+100, 133, Gx_line+118, 2, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV243Contratado, "ZZ,ZZZ,ZZ9.999")), 258, Gx_line+67, 361, Gx_line+85, 2+256, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV244Saldo, "ZZ,ZZZ,ZZ9.999")), 258, Gx_line+133, 361, Gx_line+151, 2+256, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV220UnidadeMedicao_Sigla, "@!")), 75, Gx_line+167, 133, Gx_line+185, 2, 0, 0, 0) ;
         getPrinter().GxDrawText("para Homologar:", 139, Gx_line+167, 246, Gx_line+185, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV71ContagemResultado_PFTotal, "ZZ,ZZZ,ZZ9.999")), 258, Gx_line+167, 361, Gx_line+185, 2+256, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV9Contrato_Numero, "")), 233, Gx_line+17, 359, Gx_line+35, 2+256, 0, 0, 0) ;
         getPrinter().GxDrawText("Contrato N�", 148, Gx_line+17, 220, Gx_line+35, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText("Saldo Previsto:", 150, Gx_line+200, 246, Gx_line+218, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV245SaldoPrev, "ZZ,ZZZ,ZZ9.999")), 258, Gx_line+200, 361, Gx_line+218, 2+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+218);
         /* Eject command */
         Gx_OldLine = Gx_line;
         Gx_line = (int)(P_lines+1);
         if ( AV142OSAutomatica )
         {
            pr_default.dynParam(12, new Object[]{ new Object[]{
                                                 A456ContagemResultado_Codigo ,
                                                 AV12Codigos },
                                                 new int[] {
                                                 TypeConstants.INT
                                                 }
            });
            /* Using cursor P008W25 */
            pr_default.execute(12);
            while ( (pr_default.getStatus(12) != 101) )
            {
               A456ContagemResultado_Codigo = P008W25_A456ContagemResultado_Codigo[0];
               A493ContagemResultado_DemandaFM = P008W25_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = P008W25_n493ContagemResultado_DemandaFM[0];
               AV211ContagemResultado_Codigo = A456ContagemResultado_Codigo;
               AV141Os = A493ContagemResultado_DemandaFM;
               /* Execute user subroutine: 'LINHASDAOS' */
               S128 ();
               if ( returnInSub )
               {
                  pr_default.close(12);
                  returnInSub = true;
                  if (true) return;
               }
               pr_default.readNext(12);
            }
            pr_default.close(12);
         }
         else
         {
            pr_default.dynParam(13, new Object[]{ new Object[]{
                                                 A456ContagemResultado_Codigo ,
                                                 AV12Codigos },
                                                 new int[] {
                                                 TypeConstants.INT
                                                 }
            });
            /* Using cursor P008W26 */
            pr_default.execute(13);
            while ( (pr_default.getStatus(13) != 101) )
            {
               A456ContagemResultado_Codigo = P008W26_A456ContagemResultado_Codigo[0];
               A493ContagemResultado_DemandaFM = P008W26_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = P008W26_n493ContagemResultado_DemandaFM[0];
               AV211ContagemResultado_Codigo = A456ContagemResultado_Codigo;
               AV46ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
               AV141Os = A493ContagemResultado_DemandaFM;
               AV229SemOSFM = String.IsNullOrEmpty(StringUtil.RTrim( AV141Os));
               /* Execute user subroutine: 'LINHASDAOS' */
               S128 ();
               if ( returnInSub )
               {
                  pr_default.close(13);
                  returnInSub = true;
                  if (true) return;
               }
               pr_default.readNext(13);
            }
            pr_default.close(13);
         }
         /* Execute user subroutine: 'PRINTFREQUENCIA' */
         S131 ();
         if (returnInSub) return;
         H8W0( false, 201) ;
         getPrinter().GxDrawRect(0, Gx_line+0, 1076, Gx_line+22, 1, 0, 0, 0, 1, 245, 245, 245, 0, 0, 0, 0, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(700, Gx_line+116, 967, Gx_line+116, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(275, Gx_line+116, 542, Gx_line+116, 1, 0, 0, 0, 0) ;
         getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV72ContagemResultado_PFTotalFaturar, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 925, Gx_line+1, 1067, Gx_line+19, 2, 0, 0, 0) ;
         getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Valor Total do Per�odo R$", 720, Gx_line+0, 907, Gx_line+19, 0+256, 0, 0, 0) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 9, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Preposto da Contratada", 341, Gx_line+116, 476, Gx_line+132, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText("CNPJ: ", 342, Gx_line+157, 382, Gx_line+173, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText("Gestor(a) Contratual", 775, Gx_line+116, 891, Gx_line+133, 0, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV225Preposto, "@!")), 278, Gx_line+129, 539, Gx_line+146, 1+256, 0, 0, 0) ;
         getPrinter().GxDrawText(AV164WWPContext.gxTpr_Contratada_pessoanom, 148, Gx_line+142, 670, Gx_line+159, 1, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV226CNPJ, "")), 380, Gx_line+157, 454, Gx_line+174, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+201);
         /* Eject command */
         Gx_OldLine = Gx_line;
         Gx_line = (int)(P_lines+1);
         if ( AV175TemIndicadorPntLote )
         {
            if ( AV165Previstas > 0 )
            {
               AV168PntLoteIndice = (decimal)((AV167Atrasadas/ (decimal)(AV165Previstas))*100);
               AV224PntLotePrcSemAtraso = (decimal)((AV166Cumpridas/ (decimal)(AV165Previstas))*100);
            }
            /* Execute user subroutine: 'GETSANCAOINDPNTLOTE' */
            S141 ();
            if (returnInSub) return;
            H8W0( false, 351) ;
            getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV172PntLoteIndicador, "")), 150, Gx_line+17, 880, Gx_line+35, 0+256, 0, 0, 1) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("-", 417, Gx_line+83, 425, Gx_line+97, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Previstas:", 158, Gx_line+167, 220, Gx_line+185, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Entregues sem atraso:", 158, Gx_line+183, 299, Gx_line+201, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Entregues com atraso:", 158, Gx_line+200, 299, Gx_line+218, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Indice do indicador:", 158, Gx_line+217, 281, Gx_line+235, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Valor Total do Per�odo         R$:", 158, Gx_line+300, 350, Gx_line+318, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Valor Liquido a ser faturado R$:", 158, Gx_line+333, 354, Gx_line+351, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV165Previstas), "ZZZ9")), 450, Gx_line+167, 480, Gx_line+185, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV166Cumpridas), "ZZZ9")), 450, Gx_line+183, 480, Gx_line+201, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV167Atrasadas), "ZZZ9")), 450, Gx_line+200, 480, Gx_line+218, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV168PntLoteIndice, "ZZ9.99 %")), 442, Gx_line+217, 501, Gx_line+235, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV170PntLoteSancao, "ZZZZZZZZ9.99")), 392, Gx_line+317, 481, Gx_line+335, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV171PntLoteLiquido, "ZZZ,ZZZ,ZZ9.99")), 383, Gx_line+333, 486, Gx_line+351, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Glosa                                          R$:", 158, Gx_line+317, 350, Gx_line+335, 0, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV149Reduz, "ZZ9.99 %")), 442, Gx_line+233, 501, Gx_line+251, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Valor Bruto apurado:", 158, Gx_line+100, 287, Gx_line+118, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Per�odo:", 158, Gx_line+83, 212, Gx_line+101, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV110DataFim, "99/99/99"), 433, Gx_line+83, 483, Gx_line+101, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV111DataInicio, "99/99/99"), 358, Gx_line+83, 408, Gx_line+101, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV223PntLoteUndBruto, "ZZ,ZZZ,ZZ9.999")), 375, Gx_line+100, 483, Gx_line+118, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV72ContagemResultado_PFTotalFaturar, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 342, Gx_line+300, 484, Gx_line+318, 2, 0, 0, 0) ;
            getPrinter().GxDrawText("San��o:", 158, Gx_line+233, 211, Gx_line+251, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV224PntLotePrcSemAtraso, "(ZZ9.99 %)")), 492, Gx_line+183, 566, Gx_line+201, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV220UnidadeMedicao_Sigla, "@!")), 492, Gx_line+100, 550, Gx_line+118, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 10, false, true, true, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Quantidade de Demandas:", 150, Gx_line+133, 320, Gx_line+150, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Base de C�lculo:", 150, Gx_line+50, 257, Gx_line+67, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Valor do Faturamento:", 150, Gx_line+267, 289, Gx_line+284, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Indicador", 75, Gx_line+17, 134, Gx_line+34, 0+256, 0, 0, 1) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+351);
         }
         /* Execute user subroutine: 'PRINTFAIXAS' */
         S151 ();
         if (returnInSub) return;
      }

      protected void S128( )
      {
         /* 'LINHASDAOS' Routine */
         AV161TotalPontos = 0;
         AV160TotalFinal = 0;
         AV215Linhas = 1;
         /* Using cursor P008W28 */
         pr_default.execute(14, new Object[] {AV211ContagemResultado_Codigo});
         while ( (pr_default.getStatus(14) != 101) )
         {
            A489ContagemResultado_SistemaCod = P008W28_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P008W28_n489ContagemResultado_SistemaCod[0];
            A1553ContagemResultado_CntSrvCod = P008W28_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P008W28_n1553ContagemResultado_CntSrvCod[0];
            A484ContagemResultado_StatusDmn = P008W28_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P008W28_n484ContagemResultado_StatusDmn[0];
            A1854ContagemResultado_VlrCnc = P008W28_A1854ContagemResultado_VlrCnc[0];
            n1854ContagemResultado_VlrCnc = P008W28_n1854ContagemResultado_VlrCnc[0];
            A494ContagemResultado_Descricao = P008W28_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P008W28_n494ContagemResultado_Descricao[0];
            A490ContagemResultado_ContratadaCod = P008W28_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P008W28_n490ContagemResultado_ContratadaCod[0];
            A601ContagemResultado_Servico = P008W28_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P008W28_n601ContagemResultado_Servico[0];
            A1237ContagemResultado_PrazoMaisDias = P008W28_A1237ContagemResultado_PrazoMaisDias[0];
            n1237ContagemResultado_PrazoMaisDias = P008W28_n1237ContagemResultado_PrazoMaisDias[0];
            A1227ContagemResultado_PrazoInicialDias = P008W28_A1227ContagemResultado_PrazoInicialDias[0];
            n1227ContagemResultado_PrazoInicialDias = P008W28_n1227ContagemResultado_PrazoInicialDias[0];
            A2017ContagemResultado_DataEntregaReal = P008W28_A2017ContagemResultado_DataEntregaReal[0];
            n2017ContagemResultado_DataEntregaReal = P008W28_n2017ContagemResultado_DataEntregaReal[0];
            A801ContagemResultado_ServicoSigla = P008W28_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P008W28_n801ContagemResultado_ServicoSigla[0];
            A509ContagemrResultado_SistemaSigla = P008W28_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P008W28_n509ContagemrResultado_SistemaSigla[0];
            A457ContagemResultado_Demanda = P008W28_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P008W28_n457ContagemResultado_Demanda[0];
            A2008ContagemResultado_CntSrvAls = P008W28_A2008ContagemResultado_CntSrvAls[0];
            n2008ContagemResultado_CntSrvAls = P008W28_n2008ContagemResultado_CntSrvAls[0];
            A471ContagemResultado_DataDmn = P008W28_A471ContagemResultado_DataDmn[0];
            A1051ContagemResultado_GlsValor = P008W28_A1051ContagemResultado_GlsValor[0];
            n1051ContagemResultado_GlsValor = P008W28_n1051ContagemResultado_GlsValor[0];
            A1050ContagemResultado_GlsDescricao = P008W28_A1050ContagemResultado_GlsDescricao[0];
            n1050ContagemResultado_GlsDescricao = P008W28_n1050ContagemResultado_GlsDescricao[0];
            A566ContagemResultado_DataUltCnt = P008W28_A566ContagemResultado_DataUltCnt[0];
            n566ContagemResultado_DataUltCnt = P008W28_n566ContagemResultado_DataUltCnt[0];
            A456ContagemResultado_Codigo = P008W28_A456ContagemResultado_Codigo[0];
            A512ContagemResultado_ValorPF = P008W28_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P008W28_n512ContagemResultado_ValorPF[0];
            A509ContagemrResultado_SistemaSigla = P008W28_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P008W28_n509ContagemrResultado_SistemaSigla[0];
            A601ContagemResultado_Servico = P008W28_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P008W28_n601ContagemResultado_Servico[0];
            A2008ContagemResultado_CntSrvAls = P008W28_A2008ContagemResultado_CntSrvAls[0];
            n2008ContagemResultado_CntSrvAls = P008W28_n2008ContagemResultado_CntSrvAls[0];
            A801ContagemResultado_ServicoSigla = P008W28_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P008W28_n801ContagemResultado_ServicoSigla[0];
            A566ContagemResultado_DataUltCnt = P008W28_A566ContagemResultado_DataUltCnt[0];
            n566ContagemResultado_DataUltCnt = P008W28_n566ContagemResultado_DataUltCnt[0];
            GXt_decimal3 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal3) ;
            A574ContagemResultado_PFFinal = GXt_decimal3;
            A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*A512ContagemResultado_ValorPF);
            if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 )
            {
               AV70ContagemResultado_PFFaturar = A1854ContagemResultado_VlrCnc;
            }
            else
            {
               AV70ContagemResultado_PFFaturar = A606ContagemResultado_ValorFinal;
            }
            AV213DataCnt = A566ContagemResultado_DataUltCnt;
            AV133i = 1;
            AV116DmnDescricao = A494ContagemResultado_Descricao;
            AV11Codigo = A456ContagemResultado_Codigo;
            AV105Contratada = A490ContagemResultado_ContratadaCod;
            AV212Servico = A601ContagemResultado_Servico;
            AV145Prazo = (short)(A1227ContagemResultado_PrazoInicialDias+A1237ContagemResultado_PrazoMaisDias);
            AV216GlsTxt = "";
            AV161TotalPontos = (decimal)(AV161TotalPontos+(A574ContagemResultado_PFFinal-AV128GlsPF));
            /* Execute user subroutine: 'LINHADADMNDESCRICAO' */
            S1610 ();
            if ( returnInSub )
            {
               pr_default.close(14);
               returnInSub = true;
               if (true) return;
            }
            /* Execute user subroutine: 'TEMINDPNT' */
            S1710 ();
            if ( returnInSub )
            {
               pr_default.close(14);
               returnInSub = true;
               if (true) return;
            }
            /* Execute user subroutine: 'TEMINDFRQ' */
            S1810 ();
            if ( returnInSub )
            {
               pr_default.close(14);
               returnInSub = true;
               if (true) return;
            }
            if ( A574ContagemResultado_PFFinal < 0.001m )
            {
               AV154strPFFinal = StringUtil.Str( A574ContagemResultado_PFFinal, 14, 4);
            }
            else
            {
               AV154strPFFinal = StringUtil.Str( A574ContagemResultado_PFFinal, 14, 3);
            }
            AV126Entregue = DateTimeUtil.ResetTime(A2017ContagemResultado_DataEntregaReal);
            if ( AV142OSAutomatica )
            {
               H8W0( false, 16) ;
               getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV112Descricao, "")), 575, Gx_line+0, 905, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!")), 58, Gx_line+0, 216, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A509ContagemrResultado_SistemaSigla, "@!")), 397, Gx_line+0, 571, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV141Os, "")), 0, Gx_line+0, 53, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV126Entregue, "99/99/99"), 217, Gx_line+0, 266, Gx_line+15, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV154strPFFinal, "")), 885, Gx_line+0, 959, Gx_line+15, 2+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A512ContagemResultado_ValorPF, "ZZ,ZZZ,ZZZ,ZZ9.999")), 970, Gx_line+0, 1005, Gx_line+15, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV70ContagemResultado_PFFaturar, "ZZ,ZZZ,ZZZ,ZZ9.999")), 1001, Gx_line+0, 1068, Gx_line+15, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A801ContagemResultado_ServicoSigla, "@!")), 275, Gx_line+0, 354, Gx_line+15, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+16);
            }
            else
            {
               H8W0( false, 16) ;
               getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV112Descricao, "")), 575, Gx_line+0, 905, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV141Os, "")), 0, Gx_line+0, 93, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A509ContagemrResultado_SistemaSigla, "@!")), 400, Gx_line+0, 574, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!")), 96, Gx_line+0, 346, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV126Entregue, "99/99/99"), 341, Gx_line+0, 390, Gx_line+15, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV154strPFFinal, "")), 885, Gx_line+0, 959, Gx_line+15, 2+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A512ContagemResultado_ValorPF, "ZZ,ZZZ,ZZZ,ZZ9.999")), 970, Gx_line+0, 1005, Gx_line+15, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV70ContagemResultado_PFFaturar, "ZZ,ZZZ,ZZZ,ZZ9.999")), 1001, Gx_line+0, 1068, Gx_line+15, 2, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+16);
            }
            /* Execute user subroutine: 'PRINTDESCRICAO' */
            S1910 ();
            if ( returnInSub )
            {
               pr_default.close(14);
               returnInSub = true;
               if (true) return;
            }
            AV108d = (decimal)(1);
            AV127GlsDescricao = "Data: " + context.localUtil.DToC( A471ContagemResultado_DataDmn, 2, "/") + " Servi�o: " + A2008ContagemResultado_CntSrvAls;
            /* Execute user subroutine: 'PRINTGLOSADESC' */
            S2010 ();
            if ( returnInSub )
            {
               pr_default.close(14);
               returnInSub = true;
               if (true) return;
            }
            AV161TotalPontos = (decimal)(AV161TotalPontos+A574ContagemResultado_PFFinal);
            AV160TotalFinal = (decimal)(AV160TotalFinal+AV70ContagemResultado_PFFaturar);
            if ( ( A1051ContagemResultado_GlsValor > Convert.ToDecimal( 0 )) )
            {
               AV129GlsValor = A1051ContagemResultado_GlsValor;
               AV160TotalFinal = (decimal)(AV160TotalFinal-AV129GlsValor);
               AV72ContagemResultado_PFTotalFaturar = (decimal)(AV72ContagemResultado_PFTotalFaturar-AV129GlsValor);
               AV217StrGlsValor = "";
               AV216GlsTxt = "";
               AV129GlsValor = (decimal)(AV129GlsValor*-1);
               AV217StrGlsValor = StringUtil.Str( AV129GlsValor, 14, 2);
               AV216GlsTxt = "Glosa";
               AV127GlsDescricao = A1050ContagemResultado_GlsDescricao;
               AV108d = (decimal)(1);
               /* Execute user subroutine: 'LINHADAGLSDESCRICAO' */
               S2110 ();
               if ( returnInSub )
               {
                  pr_default.close(14);
                  returnInSub = true;
                  if (true) return;
               }
               H8W0( false, 16) ;
               getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV112Descricao, "")), 133, Gx_line+0, 905, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV216GlsTxt, "")), 92, Gx_line+0, 145, Gx_line+15, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV217StrGlsValor, "")), 994, Gx_line+0, 1068, Gx_line+15, 2+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+16);
               /* Execute user subroutine: 'PRINTGLOSADESC' */
               S2010 ();
               if ( returnInSub )
               {
                  pr_default.close(14);
                  returnInSub = true;
                  if (true) return;
               }
               AV215Linhas = (short)(AV215Linhas+1);
            }
            if ( AV158TemIndicadorDePontoalidade )
            {
               AV153Solicitada = A471ContagemResultado_DataDmn;
               AV162ValorPF = A512ContagemResultado_ValorPF;
               /* Execute user subroutine: 'INDICADORPONTUALIDADE' */
               S2210 ();
               if ( returnInSub )
               {
                  pr_default.close(14);
                  returnInSub = true;
                  if (true) return;
               }
            }
            if ( AV175TemIndicadorPntLote )
            {
               if ( ( AV146Previsto >= AV173PrdFtrIni ) && ( AV146Previsto <= AV174PrdFtrFim ) )
               {
                  AV165Previstas = (short)(AV165Previstas+1);
                  if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 )
                  {
                     AV169PntLoteBaseCalculo = (decimal)(AV169PntLoteBaseCalculo+A1854ContagemResultado_VlrCnc);
                  }
                  else
                  {
                     AV169PntLoteBaseCalculo = (decimal)(AV169PntLoteBaseCalculo+A606ContagemResultado_ValorFinal);
                  }
                  AV223PntLoteUndBruto = (decimal)(AV223PntLoteUndBruto+A574ContagemResultado_PFFinal);
                  if ( AV126Entregue <= AV146Previsto )
                  {
                     AV166Cumpridas = (short)(AV166Cumpridas+1);
                  }
                  else
                  {
                     AV167Atrasadas = (short)(AV167Atrasadas+1);
                  }
               }
            }
            if ( AV247TemIndicadorDeFrequencia )
            {
               /* Execute user subroutine: 'INDICADORFREQUENCIA' */
               S2310 ();
               if ( returnInSub )
               {
                  pr_default.close(14);
                  returnInSub = true;
                  if (true) return;
               }
            }
            AV141Os = "";
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(14);
         if ( AV161TotalPontos < 0.001m )
         {
            AV155strPFTotal = StringUtil.Str( AV161TotalPontos, 14, 4);
         }
         else
         {
            AV155strPFTotal = StringUtil.Str( AV161TotalPontos, 14, 3);
         }
         if ( AV160TotalFinal < 0.001m )
         {
            AV159TotalFaturar = NumberUtil.Round( AV160TotalFinal, 4);
            AV156strPFTotalFaturar = StringUtil.Str( AV159TotalFaturar, 14, 4);
         }
         else if ( AV160TotalFinal < 0.01m )
         {
            AV159TotalFaturar = NumberUtil.Round( AV160TotalFinal, 3);
            AV156strPFTotalFaturar = StringUtil.Str( AV159TotalFaturar, 14, 3);
         }
         else
         {
            AV156strPFTotalFaturar = StringUtil.Str( AV160TotalFinal, 14, 2);
         }
         AV159TotalFaturar = NumberUtil.Round( AV160TotalFinal, 2);
         if ( ! AV229SemOSFM && ( AV215Linhas > 1 ) )
         {
            H8W0( false, 17) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV156strPFTotalFaturar, "")), 958, Gx_line+0, 1067, Gx_line+15, 2, 0, 0, 0) ;
            getPrinter().GxDrawText("Sub total:", 867, Gx_line+0, 915, Gx_line+14, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+17);
         }
      }

      protected void S241( )
      {
         /* 'PRINTDATALOTE' Routine */
         /* Using cursor P008W29 */
         pr_default.execute(15, new Object[] {AV47ContagemResultado_LoteAceite});
         while ( (pr_default.getStatus(15) != 101) )
         {
            A597ContagemResultado_LoteAceiteCod = P008W29_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P008W29_n597ContagemResultado_LoteAceiteCod[0];
            A484ContagemResultado_StatusDmn = P008W29_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P008W29_n484ContagemResultado_StatusDmn[0];
            A1854ContagemResultado_VlrCnc = P008W29_A1854ContagemResultado_VlrCnc[0];
            n1854ContagemResultado_VlrCnc = P008W29_n1854ContagemResultado_VlrCnc[0];
            A456ContagemResultado_Codigo = P008W29_A456ContagemResultado_Codigo[0];
            A512ContagemResultado_ValorPF = P008W29_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P008W29_n512ContagemResultado_ValorPF[0];
            GXt_decimal3 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal3) ;
            A574ContagemResultado_PFFinal = GXt_decimal3;
            A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*A512ContagemResultado_ValorPF);
            AV12Codigos.Add(A456ContagemResultado_Codigo, 0);
            if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 )
            {
               AV70ContagemResultado_PFFaturar = A1854ContagemResultado_VlrCnc;
            }
            else
            {
               AV70ContagemResultado_PFFaturar = A606ContagemResultado_ValorFinal;
            }
            AV71ContagemResultado_PFTotal = (decimal)(AV71ContagemResultado_PFTotal+A574ContagemResultado_PFFinal);
            AV72ContagemResultado_PFTotalFaturar = (decimal)(AV72ContagemResultado_PFTotalFaturar+AV70ContagemResultado_PFFaturar);
            AV148Quantidade = (short)(AV148Quantidade+1);
            pr_default.readNext(15);
         }
         pr_default.close(15);
         AV245SaldoPrev = (decimal)(AV244Saldo-AV71ContagemResultado_PFTotal);
         H8W0( false, 218) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV245SaldoPrev, "ZZ,ZZZ,ZZ9.999")), 258, Gx_line+200, 361, Gx_line+218, 2+256, 0, 0, 0) ;
         getPrinter().GxDrawText("Saldo Atual:", 170, Gx_line+200, 246, Gx_line+218, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText("Contrato N�", 150, Gx_line+17, 222, Gx_line+35, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV9Contrato_Numero, "")), 233, Gx_line+17, 359, Gx_line+35, 2+256, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV71ContagemResultado_PFTotal, "ZZ,ZZZ,ZZ9.999")), 258, Gx_line+167, 361, Gx_line+185, 2+256, 0, 0, 0) ;
         getPrinter().GxDrawText("usado no Lote:", 153, Gx_line+167, 246, Gx_line+185, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV220UnidadeMedicao_Sigla, "@!")), 83, Gx_line+167, 141, Gx_line+185, 2, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV244Saldo, "ZZ,ZZZ,ZZ9.999")), 258, Gx_line+133, 361, Gx_line+151, 2+256, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV243Contratado, "ZZ,ZZZ,ZZ9.999")), 258, Gx_line+67, 361, Gx_line+85, 2+256, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV220UnidadeMedicao_Sigla, "@!")), 83, Gx_line+100, 141, Gx_line+118, 2, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV220UnidadeMedicao_Sigla, "@!")), 83, Gx_line+67, 141, Gx_line+85, 2, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV242Usado, "ZZ,ZZZ,ZZ9.999")), 258, Gx_line+100, 361, Gx_line+118, 2+256, 0, 0, 0) ;
         getPrinter().GxDrawText("Saldo Contratado:", 132, Gx_line+133, 246, Gx_line+151, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText("contratados/as:", 148, Gx_line+67, 246, Gx_line+85, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText("consumidos/as:", 146, Gx_line+100, 246, Gx_line+118, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+218);
         /* Eject command */
         Gx_OldLine = Gx_line;
         Gx_line = (int)(P_lines+1);
         if ( AV142OSAutomatica )
         {
            /* Using cursor P008W30 */
            pr_default.execute(16, new Object[] {AV47ContagemResultado_LoteAceite});
            while ( (pr_default.getStatus(16) != 101) )
            {
               A597ContagemResultado_LoteAceiteCod = P008W30_A597ContagemResultado_LoteAceiteCod[0];
               n597ContagemResultado_LoteAceiteCod = P008W30_n597ContagemResultado_LoteAceiteCod[0];
               A456ContagemResultado_Codigo = P008W30_A456ContagemResultado_Codigo[0];
               A493ContagemResultado_DemandaFM = P008W30_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = P008W30_n493ContagemResultado_DemandaFM[0];
               AV211ContagemResultado_Codigo = A456ContagemResultado_Codigo;
               AV46ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
               AV141Os = A493ContagemResultado_DemandaFM;
               /* Execute user subroutine: 'LINHASDAOSLOTE' */
               S2512 ();
               if ( returnInSub )
               {
                  pr_default.close(16);
                  returnInSub = true;
                  if (true) return;
               }
               pr_default.readNext(16);
            }
            pr_default.close(16);
         }
         else
         {
            /* Using cursor P008W31 */
            pr_default.execute(17, new Object[] {AV47ContagemResultado_LoteAceite});
            while ( (pr_default.getStatus(17) != 101) )
            {
               A597ContagemResultado_LoteAceiteCod = P008W31_A597ContagemResultado_LoteAceiteCod[0];
               n597ContagemResultado_LoteAceiteCod = P008W31_n597ContagemResultado_LoteAceiteCod[0];
               A456ContagemResultado_Codigo = P008W31_A456ContagemResultado_Codigo[0];
               A493ContagemResultado_DemandaFM = P008W31_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = P008W31_n493ContagemResultado_DemandaFM[0];
               AV211ContagemResultado_Codigo = A456ContagemResultado_Codigo;
               AV46ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
               AV141Os = A493ContagemResultado_DemandaFM;
               AV229SemOSFM = String.IsNullOrEmpty(StringUtil.RTrim( AV141Os));
               /* Execute user subroutine: 'LINHASDAOSLOTE' */
               S2512 ();
               if ( returnInSub )
               {
                  pr_default.close(17);
                  returnInSub = true;
                  if (true) return;
               }
               pr_default.readNext(17);
            }
            pr_default.close(17);
         }
         /* Execute user subroutine: 'PRINTFREQUENCIA' */
         S131 ();
         if (returnInSub) return;
         H8W0( false, 201) ;
         getPrinter().GxDrawRect(0, Gx_line+0, 1076, Gx_line+22, 1, 0, 0, 0, 1, 245, 245, 245, 0, 0, 0, 0, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(700, Gx_line+116, 967, Gx_line+116, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(275, Gx_line+116, 542, Gx_line+116, 1, 0, 0, 0, 0) ;
         getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV72ContagemResultado_PFTotalFaturar, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 925, Gx_line+1, 1067, Gx_line+19, 2, 0, 0, 0) ;
         getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Valor Total do Per�odo R$", 720, Gx_line+0, 907, Gx_line+19, 0+256, 0, 0, 0) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 9, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Preposto da Contratada", 341, Gx_line+116, 476, Gx_line+132, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText("CNPJ: ", 342, Gx_line+157, 382, Gx_line+173, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText("Gestor(a) Contratual", 775, Gx_line+116, 891, Gx_line+133, 0, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV225Preposto, "@!")), 278, Gx_line+129, 539, Gx_line+146, 1+256, 0, 0, 0) ;
         getPrinter().GxDrawText(AV164WWPContext.gxTpr_Contratada_pessoanom, 148, Gx_line+142, 670, Gx_line+159, 1, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV226CNPJ, "")), 380, Gx_line+157, 454, Gx_line+174, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+201);
         /* Eject command */
         Gx_OldLine = Gx_line;
         Gx_line = (int)(P_lines+1);
         if ( AV175TemIndicadorPntLote )
         {
            if ( AV165Previstas > 0 )
            {
               AV168PntLoteIndice = (decimal)((AV167Atrasadas/ (decimal)(AV165Previstas))*100);
               AV224PntLotePrcSemAtraso = (decimal)((AV166Cumpridas/ (decimal)(AV165Previstas))*100);
            }
            /* Execute user subroutine: 'GETSANCAOINDPNTLOTE' */
            S141 ();
            if (returnInSub) return;
            H8W0( false, 351) ;
            getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV172PntLoteIndicador, "")), 150, Gx_line+17, 880, Gx_line+35, 0+256, 0, 0, 1) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("-", 417, Gx_line+83, 425, Gx_line+97, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Previstas:", 158, Gx_line+167, 220, Gx_line+185, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Entregues sem atraso:", 158, Gx_line+183, 299, Gx_line+201, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Entregues com atraso:", 158, Gx_line+200, 299, Gx_line+218, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Indice do indicador:", 158, Gx_line+217, 281, Gx_line+235, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Valor Total do Per�odo         R$:", 158, Gx_line+300, 350, Gx_line+318, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Valor Liquido a ser faturado R$:", 158, Gx_line+333, 354, Gx_line+351, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV165Previstas), "ZZZ9")), 450, Gx_line+167, 480, Gx_line+185, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV166Cumpridas), "ZZZ9")), 450, Gx_line+183, 480, Gx_line+201, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV167Atrasadas), "ZZZ9")), 450, Gx_line+200, 480, Gx_line+218, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV168PntLoteIndice, "ZZ9.99 %")), 442, Gx_line+217, 501, Gx_line+235, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV170PntLoteSancao, "ZZZZZZZZ9.99")), 392, Gx_line+317, 481, Gx_line+335, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV171PntLoteLiquido, "ZZZ,ZZZ,ZZ9.99")), 383, Gx_line+333, 486, Gx_line+351, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Glosa                                          R$:", 158, Gx_line+317, 350, Gx_line+335, 0, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV149Reduz, "ZZ9.99 %")), 442, Gx_line+233, 501, Gx_line+251, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Valor Bruto apurado:", 158, Gx_line+100, 287, Gx_line+118, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Per�odo:", 158, Gx_line+83, 212, Gx_line+101, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV110DataFim, "99/99/99"), 433, Gx_line+83, 483, Gx_line+101, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV111DataInicio, "99/99/99"), 358, Gx_line+83, 408, Gx_line+101, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV223PntLoteUndBruto, "ZZ,ZZZ,ZZ9.999")), 375, Gx_line+100, 483, Gx_line+118, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV72ContagemResultado_PFTotalFaturar, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 342, Gx_line+300, 484, Gx_line+318, 2, 0, 0, 0) ;
            getPrinter().GxDrawText("San��o:", 158, Gx_line+233, 211, Gx_line+251, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV224PntLotePrcSemAtraso, "(ZZ9.99 %)")), 492, Gx_line+183, 566, Gx_line+201, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV220UnidadeMedicao_Sigla, "@!")), 492, Gx_line+100, 550, Gx_line+118, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 10, false, true, true, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Quantidade de Demandas:", 150, Gx_line+133, 320, Gx_line+150, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Base de C�lculo:", 150, Gx_line+50, 257, Gx_line+67, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Valor do Faturamento:", 150, Gx_line+267, 289, Gx_line+284, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Indicador", 75, Gx_line+17, 134, Gx_line+34, 0+256, 0, 0, 1) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+351);
         }
         /* Execute user subroutine: 'PRINTFAIXAS' */
         S151 ();
         if (returnInSub) return;
      }

      protected void S2512( )
      {
         /* 'LINHASDAOSLOTE' Routine */
         AV161TotalPontos = 0;
         AV160TotalFinal = 0;
         AV215Linhas = 1;
         if ( AV142OSAutomatica )
         {
            /* Using cursor P008W33 */
            pr_default.execute(18, new Object[] {AV211ContagemResultado_Codigo});
            while ( (pr_default.getStatus(18) != 101) )
            {
               A489ContagemResultado_SistemaCod = P008W33_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = P008W33_n489ContagemResultado_SistemaCod[0];
               A1553ContagemResultado_CntSrvCod = P008W33_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P008W33_n1553ContagemResultado_CntSrvCod[0];
               A484ContagemResultado_StatusDmn = P008W33_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P008W33_n484ContagemResultado_StatusDmn[0];
               A1854ContagemResultado_VlrCnc = P008W33_A1854ContagemResultado_VlrCnc[0];
               n1854ContagemResultado_VlrCnc = P008W33_n1854ContagemResultado_VlrCnc[0];
               A494ContagemResultado_Descricao = P008W33_A494ContagemResultado_Descricao[0];
               n494ContagemResultado_Descricao = P008W33_n494ContagemResultado_Descricao[0];
               A490ContagemResultado_ContratadaCod = P008W33_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P008W33_n490ContagemResultado_ContratadaCod[0];
               A601ContagemResultado_Servico = P008W33_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P008W33_n601ContagemResultado_Servico[0];
               A1237ContagemResultado_PrazoMaisDias = P008W33_A1237ContagemResultado_PrazoMaisDias[0];
               n1237ContagemResultado_PrazoMaisDias = P008W33_n1237ContagemResultado_PrazoMaisDias[0];
               A1227ContagemResultado_PrazoInicialDias = P008W33_A1227ContagemResultado_PrazoInicialDias[0];
               n1227ContagemResultado_PrazoInicialDias = P008W33_n1227ContagemResultado_PrazoInicialDias[0];
               A2017ContagemResultado_DataEntregaReal = P008W33_A2017ContagemResultado_DataEntregaReal[0];
               n2017ContagemResultado_DataEntregaReal = P008W33_n2017ContagemResultado_DataEntregaReal[0];
               A801ContagemResultado_ServicoSigla = P008W33_A801ContagemResultado_ServicoSigla[0];
               n801ContagemResultado_ServicoSigla = P008W33_n801ContagemResultado_ServicoSigla[0];
               A509ContagemrResultado_SistemaSigla = P008W33_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = P008W33_n509ContagemrResultado_SistemaSigla[0];
               A457ContagemResultado_Demanda = P008W33_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = P008W33_n457ContagemResultado_Demanda[0];
               A2008ContagemResultado_CntSrvAls = P008W33_A2008ContagemResultado_CntSrvAls[0];
               n2008ContagemResultado_CntSrvAls = P008W33_n2008ContagemResultado_CntSrvAls[0];
               A471ContagemResultado_DataDmn = P008W33_A471ContagemResultado_DataDmn[0];
               A1051ContagemResultado_GlsValor = P008W33_A1051ContagemResultado_GlsValor[0];
               n1051ContagemResultado_GlsValor = P008W33_n1051ContagemResultado_GlsValor[0];
               A1050ContagemResultado_GlsDescricao = P008W33_A1050ContagemResultado_GlsDescricao[0];
               n1050ContagemResultado_GlsDescricao = P008W33_n1050ContagemResultado_GlsDescricao[0];
               A566ContagemResultado_DataUltCnt = P008W33_A566ContagemResultado_DataUltCnt[0];
               n566ContagemResultado_DataUltCnt = P008W33_n566ContagemResultado_DataUltCnt[0];
               A456ContagemResultado_Codigo = P008W33_A456ContagemResultado_Codigo[0];
               A512ContagemResultado_ValorPF = P008W33_A512ContagemResultado_ValorPF[0];
               n512ContagemResultado_ValorPF = P008W33_n512ContagemResultado_ValorPF[0];
               A509ContagemrResultado_SistemaSigla = P008W33_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = P008W33_n509ContagemrResultado_SistemaSigla[0];
               A601ContagemResultado_Servico = P008W33_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P008W33_n601ContagemResultado_Servico[0];
               A2008ContagemResultado_CntSrvAls = P008W33_A2008ContagemResultado_CntSrvAls[0];
               n2008ContagemResultado_CntSrvAls = P008W33_n2008ContagemResultado_CntSrvAls[0];
               A801ContagemResultado_ServicoSigla = P008W33_A801ContagemResultado_ServicoSigla[0];
               n801ContagemResultado_ServicoSigla = P008W33_n801ContagemResultado_ServicoSigla[0];
               A566ContagemResultado_DataUltCnt = P008W33_A566ContagemResultado_DataUltCnt[0];
               n566ContagemResultado_DataUltCnt = P008W33_n566ContagemResultado_DataUltCnt[0];
               GXt_decimal3 = A574ContagemResultado_PFFinal;
               new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal3) ;
               A574ContagemResultado_PFFinal = GXt_decimal3;
               A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*A512ContagemResultado_ValorPF);
               if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 )
               {
                  AV70ContagemResultado_PFFaturar = A1854ContagemResultado_VlrCnc;
               }
               else
               {
                  AV70ContagemResultado_PFFaturar = A606ContagemResultado_ValorFinal;
               }
               AV213DataCnt = A566ContagemResultado_DataUltCnt;
               AV133i = 1;
               AV116DmnDescricao = A494ContagemResultado_Descricao;
               AV11Codigo = A456ContagemResultado_Codigo;
               AV105Contratada = A490ContagemResultado_ContratadaCod;
               AV212Servico = A601ContagemResultado_Servico;
               AV145Prazo = (short)(A1227ContagemResultado_PrazoInicialDias+A1237ContagemResultado_PrazoMaisDias);
               AV216GlsTxt = "";
               AV161TotalPontos = (decimal)(AV161TotalPontos+(A574ContagemResultado_PFFinal-AV128GlsPF));
               /* Execute user subroutine: 'LINHADADMNDESCRICAO' */
               S1610 ();
               if ( returnInSub )
               {
                  pr_default.close(18);
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'TEMINDPNT' */
               S1710 ();
               if ( returnInSub )
               {
                  pr_default.close(18);
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'TEMINDFRQ' */
               S1810 ();
               if ( returnInSub )
               {
                  pr_default.close(18);
                  returnInSub = true;
                  if (true) return;
               }
               if ( A574ContagemResultado_PFFinal < 0.001m )
               {
                  AV154strPFFinal = StringUtil.Str( A574ContagemResultado_PFFinal, 14, 4);
               }
               else
               {
                  AV154strPFFinal = StringUtil.Str( A574ContagemResultado_PFFinal, 14, 3);
               }
               AV126Entregue = DateTimeUtil.ResetTime(A2017ContagemResultado_DataEntregaReal);
               H8W0( false, 16) ;
               getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV112Descricao, "")), 575, Gx_line+0, 905, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!")), 58, Gx_line+0, 216, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A509ContagemrResultado_SistemaSigla, "@!")), 397, Gx_line+0, 571, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV141Os, "")), 0, Gx_line+0, 53, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV126Entregue, "99/99/99"), 217, Gx_line+0, 266, Gx_line+15, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV154strPFFinal, "")), 885, Gx_line+0, 959, Gx_line+15, 2+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A512ContagemResultado_ValorPF, "ZZ,ZZZ,ZZZ,ZZ9.999")), 970, Gx_line+0, 1005, Gx_line+15, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV70ContagemResultado_PFFaturar, "ZZ,ZZZ,ZZZ,ZZ9.999")), 1001, Gx_line+0, 1068, Gx_line+15, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A801ContagemResultado_ServicoSigla, "@!")), 275, Gx_line+0, 354, Gx_line+15, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+16);
               /* Execute user subroutine: 'PRINTDESCRICAO' */
               S1910 ();
               if ( returnInSub )
               {
                  pr_default.close(18);
                  returnInSub = true;
                  if (true) return;
               }
               AV108d = (decimal)(1);
               AV127GlsDescricao = "Data: " + context.localUtil.DToC( A471ContagemResultado_DataDmn, 2, "/") + " Servi�o: " + A2008ContagemResultado_CntSrvAls;
               /* Execute user subroutine: 'PRINTGLOSADESC' */
               S2010 ();
               if ( returnInSub )
               {
                  pr_default.close(18);
                  returnInSub = true;
                  if (true) return;
               }
               AV161TotalPontos = (decimal)(AV161TotalPontos+A574ContagemResultado_PFFinal);
               AV160TotalFinal = (decimal)(AV160TotalFinal+AV70ContagemResultado_PFFaturar);
               if ( ( A1051ContagemResultado_GlsValor > Convert.ToDecimal( 0 )) )
               {
                  AV129GlsValor = A1051ContagemResultado_GlsValor;
                  AV160TotalFinal = (decimal)(AV160TotalFinal-AV129GlsValor);
                  AV72ContagemResultado_PFTotalFaturar = (decimal)(AV72ContagemResultado_PFTotalFaturar-AV129GlsValor);
                  AV217StrGlsValor = "";
                  AV216GlsTxt = "";
                  AV129GlsValor = (decimal)(AV129GlsValor*-1);
                  AV217StrGlsValor = StringUtil.Str( AV129GlsValor, 14, 2);
                  AV216GlsTxt = "Glosa";
                  AV127GlsDescricao = A1050ContagemResultado_GlsDescricao;
                  AV108d = (decimal)(1);
                  /* Execute user subroutine: 'LINHADAGLSDESCRICAO' */
                  S2110 ();
                  if ( returnInSub )
                  {
                     pr_default.close(18);
                     returnInSub = true;
                     if (true) return;
                  }
                  H8W0( false, 16) ;
                  getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV112Descricao, "")), 133, Gx_line+0, 905, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV216GlsTxt, "")), 92, Gx_line+0, 145, Gx_line+15, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV217StrGlsValor, "")), 994, Gx_line+0, 1068, Gx_line+15, 2+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+16);
                  /* Execute user subroutine: 'PRINTGLOSADESC' */
                  S2010 ();
                  if ( returnInSub )
                  {
                     pr_default.close(18);
                     returnInSub = true;
                     if (true) return;
                  }
                  AV215Linhas = (short)(AV215Linhas+1);
               }
               if ( AV158TemIndicadorDePontoalidade )
               {
                  AV153Solicitada = A471ContagemResultado_DataDmn;
                  AV162ValorPF = A512ContagemResultado_ValorPF;
                  /* Execute user subroutine: 'INDICADORPONTUALIDADE' */
                  S2210 ();
                  if ( returnInSub )
                  {
                     pr_default.close(18);
                     returnInSub = true;
                     if (true) return;
                  }
               }
               if ( AV175TemIndicadorPntLote )
               {
                  if ( ( AV146Previsto >= AV173PrdFtrIni ) && ( AV146Previsto <= AV174PrdFtrFim ) )
                  {
                     AV165Previstas = (short)(AV165Previstas+1);
                     if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 )
                     {
                        AV169PntLoteBaseCalculo = (decimal)(AV169PntLoteBaseCalculo+A1854ContagemResultado_VlrCnc);
                     }
                     else
                     {
                        AV169PntLoteBaseCalculo = (decimal)(AV169PntLoteBaseCalculo+A606ContagemResultado_ValorFinal);
                     }
                     AV223PntLoteUndBruto = (decimal)(AV223PntLoteUndBruto+A574ContagemResultado_PFFinal);
                     if ( AV126Entregue <= AV146Previsto )
                     {
                        AV166Cumpridas = (short)(AV166Cumpridas+1);
                     }
                     else
                     {
                        AV167Atrasadas = (short)(AV167Atrasadas+1);
                     }
                  }
               }
               if ( AV247TemIndicadorDeFrequencia )
               {
                  /* Execute user subroutine: 'INDICADORFREQUENCIA' */
                  S2310 ();
                  if ( returnInSub )
                  {
                     pr_default.close(18);
                     returnInSub = true;
                     if (true) return;
                  }
               }
               AV141Os = "";
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(18);
         }
         else
         {
            /* Using cursor P008W35 */
            pr_default.execute(19, new Object[] {AV46ContagemResultado_DemandaFM, AV47ContagemResultado_LoteAceite});
            while ( (pr_default.getStatus(19) != 101) )
            {
               A489ContagemResultado_SistemaCod = P008W35_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = P008W35_n489ContagemResultado_SistemaCod[0];
               A1553ContagemResultado_CntSrvCod = P008W35_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P008W35_n1553ContagemResultado_CntSrvCod[0];
               A493ContagemResultado_DemandaFM = P008W35_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = P008W35_n493ContagemResultado_DemandaFM[0];
               A597ContagemResultado_LoteAceiteCod = P008W35_A597ContagemResultado_LoteAceiteCod[0];
               n597ContagemResultado_LoteAceiteCod = P008W35_n597ContagemResultado_LoteAceiteCod[0];
               A484ContagemResultado_StatusDmn = P008W35_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P008W35_n484ContagemResultado_StatusDmn[0];
               A1854ContagemResultado_VlrCnc = P008W35_A1854ContagemResultado_VlrCnc[0];
               n1854ContagemResultado_VlrCnc = P008W35_n1854ContagemResultado_VlrCnc[0];
               A494ContagemResultado_Descricao = P008W35_A494ContagemResultado_Descricao[0];
               n494ContagemResultado_Descricao = P008W35_n494ContagemResultado_Descricao[0];
               A490ContagemResultado_ContratadaCod = P008W35_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P008W35_n490ContagemResultado_ContratadaCod[0];
               A601ContagemResultado_Servico = P008W35_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P008W35_n601ContagemResultado_Servico[0];
               A1237ContagemResultado_PrazoMaisDias = P008W35_A1237ContagemResultado_PrazoMaisDias[0];
               n1237ContagemResultado_PrazoMaisDias = P008W35_n1237ContagemResultado_PrazoMaisDias[0];
               A1227ContagemResultado_PrazoInicialDias = P008W35_A1227ContagemResultado_PrazoInicialDias[0];
               n1227ContagemResultado_PrazoInicialDias = P008W35_n1227ContagemResultado_PrazoInicialDias[0];
               A2017ContagemResultado_DataEntregaReal = P008W35_A2017ContagemResultado_DataEntregaReal[0];
               n2017ContagemResultado_DataEntregaReal = P008W35_n2017ContagemResultado_DataEntregaReal[0];
               A457ContagemResultado_Demanda = P008W35_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = P008W35_n457ContagemResultado_Demanda[0];
               A509ContagemrResultado_SistemaSigla = P008W35_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = P008W35_n509ContagemrResultado_SistemaSigla[0];
               A2008ContagemResultado_CntSrvAls = P008W35_A2008ContagemResultado_CntSrvAls[0];
               n2008ContagemResultado_CntSrvAls = P008W35_n2008ContagemResultado_CntSrvAls[0];
               A471ContagemResultado_DataDmn = P008W35_A471ContagemResultado_DataDmn[0];
               A1051ContagemResultado_GlsValor = P008W35_A1051ContagemResultado_GlsValor[0];
               n1051ContagemResultado_GlsValor = P008W35_n1051ContagemResultado_GlsValor[0];
               A1050ContagemResultado_GlsDescricao = P008W35_A1050ContagemResultado_GlsDescricao[0];
               n1050ContagemResultado_GlsDescricao = P008W35_n1050ContagemResultado_GlsDescricao[0];
               A566ContagemResultado_DataUltCnt = P008W35_A566ContagemResultado_DataUltCnt[0];
               n566ContagemResultado_DataUltCnt = P008W35_n566ContagemResultado_DataUltCnt[0];
               A456ContagemResultado_Codigo = P008W35_A456ContagemResultado_Codigo[0];
               A512ContagemResultado_ValorPF = P008W35_A512ContagemResultado_ValorPF[0];
               n512ContagemResultado_ValorPF = P008W35_n512ContagemResultado_ValorPF[0];
               A509ContagemrResultado_SistemaSigla = P008W35_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = P008W35_n509ContagemrResultado_SistemaSigla[0];
               A601ContagemResultado_Servico = P008W35_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P008W35_n601ContagemResultado_Servico[0];
               A2008ContagemResultado_CntSrvAls = P008W35_A2008ContagemResultado_CntSrvAls[0];
               n2008ContagemResultado_CntSrvAls = P008W35_n2008ContagemResultado_CntSrvAls[0];
               A566ContagemResultado_DataUltCnt = P008W35_A566ContagemResultado_DataUltCnt[0];
               n566ContagemResultado_DataUltCnt = P008W35_n566ContagemResultado_DataUltCnt[0];
               GXt_decimal3 = A574ContagemResultado_PFFinal;
               new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal3) ;
               A574ContagemResultado_PFFinal = GXt_decimal3;
               A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*A512ContagemResultado_ValorPF);
               if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 )
               {
                  AV70ContagemResultado_PFFaturar = A1854ContagemResultado_VlrCnc;
               }
               else
               {
                  AV70ContagemResultado_PFFaturar = A606ContagemResultado_ValorFinal;
               }
               AV213DataCnt = A566ContagemResultado_DataUltCnt;
               AV133i = 1;
               AV116DmnDescricao = A494ContagemResultado_Descricao;
               AV11Codigo = A456ContagemResultado_Codigo;
               AV105Contratada = A490ContagemResultado_ContratadaCod;
               AV212Servico = A601ContagemResultado_Servico;
               AV145Prazo = (short)(A1227ContagemResultado_PrazoInicialDias+A1237ContagemResultado_PrazoMaisDias);
               AV216GlsTxt = "";
               AV161TotalPontos = (decimal)(AV161TotalPontos+(A574ContagemResultado_PFFinal-AV128GlsPF));
               /* Execute user subroutine: 'LINHADADMNDESCRICAO' */
               S1610 ();
               if ( returnInSub )
               {
                  pr_default.close(19);
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'TEMINDPNT' */
               S1710 ();
               if ( returnInSub )
               {
                  pr_default.close(19);
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'TEMINDFRQ' */
               S1810 ();
               if ( returnInSub )
               {
                  pr_default.close(19);
                  returnInSub = true;
                  if (true) return;
               }
               if ( A574ContagemResultado_PFFinal < 0.001m )
               {
                  AV154strPFFinal = StringUtil.Str( A574ContagemResultado_PFFinal, 14, 4);
               }
               else
               {
                  AV154strPFFinal = StringUtil.Str( A574ContagemResultado_PFFinal, 14, 3);
               }
               AV126Entregue = DateTimeUtil.ResetTime(A2017ContagemResultado_DataEntregaReal);
               H8W0( false, 16) ;
               getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV112Descricao, "")), 575, Gx_line+0, 905, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV141Os, "")), 0, Gx_line+0, 93, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A509ContagemrResultado_SistemaSigla, "@!")), 400, Gx_line+0, 574, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!")), 96, Gx_line+0, 346, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV126Entregue, "99/99/99"), 341, Gx_line+0, 390, Gx_line+15, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV154strPFFinal, "")), 885, Gx_line+0, 959, Gx_line+15, 2+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A512ContagemResultado_ValorPF, "ZZ,ZZZ,ZZZ,ZZ9.999")), 970, Gx_line+0, 1005, Gx_line+15, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV70ContagemResultado_PFFaturar, "ZZ,ZZZ,ZZZ,ZZ9.999")), 1001, Gx_line+0, 1068, Gx_line+15, 2, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+16);
               /* Execute user subroutine: 'PRINTDESCRICAO' */
               S1910 ();
               if ( returnInSub )
               {
                  pr_default.close(19);
                  returnInSub = true;
                  if (true) return;
               }
               AV108d = (decimal)(1);
               AV127GlsDescricao = "Data: " + context.localUtil.DToC( A471ContagemResultado_DataDmn, 2, "/") + " Servi�o: " + A2008ContagemResultado_CntSrvAls;
               /* Execute user subroutine: 'PRINTGLOSADESC' */
               S2010 ();
               if ( returnInSub )
               {
                  pr_default.close(19);
                  returnInSub = true;
                  if (true) return;
               }
               AV161TotalPontos = (decimal)(AV161TotalPontos+A574ContagemResultado_PFFinal);
               AV160TotalFinal = (decimal)(AV160TotalFinal+AV70ContagemResultado_PFFaturar);
               if ( ( A1051ContagemResultado_GlsValor > Convert.ToDecimal( 0 )) )
               {
                  AV129GlsValor = A1051ContagemResultado_GlsValor;
                  AV160TotalFinal = (decimal)(AV160TotalFinal-AV129GlsValor);
                  AV72ContagemResultado_PFTotalFaturar = (decimal)(AV72ContagemResultado_PFTotalFaturar-AV129GlsValor);
                  AV217StrGlsValor = "";
                  AV216GlsTxt = "";
                  AV129GlsValor = (decimal)(AV129GlsValor*-1);
                  AV217StrGlsValor = StringUtil.Str( AV129GlsValor, 14, 2);
                  AV216GlsTxt = "Glosa";
                  AV127GlsDescricao = A1050ContagemResultado_GlsDescricao;
                  AV108d = (decimal)(1);
                  /* Execute user subroutine: 'LINHADAGLSDESCRICAO' */
                  S2110 ();
                  if ( returnInSub )
                  {
                     pr_default.close(19);
                     returnInSub = true;
                     if (true) return;
                  }
                  H8W0( false, 16) ;
                  getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV112Descricao, "")), 133, Gx_line+0, 905, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV216GlsTxt, "")), 92, Gx_line+0, 145, Gx_line+15, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV217StrGlsValor, "")), 994, Gx_line+0, 1068, Gx_line+15, 2+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+16);
                  /* Execute user subroutine: 'PRINTGLOSADESC' */
                  S2010 ();
                  if ( returnInSub )
                  {
                     pr_default.close(19);
                     returnInSub = true;
                     if (true) return;
                  }
                  AV215Linhas = (short)(AV215Linhas+1);
               }
               if ( AV158TemIndicadorDePontoalidade )
               {
                  AV153Solicitada = A471ContagemResultado_DataDmn;
                  AV162ValorPF = A512ContagemResultado_ValorPF;
                  /* Execute user subroutine: 'INDICADORPONTUALIDADE' */
                  S2210 ();
                  if ( returnInSub )
                  {
                     pr_default.close(19);
                     returnInSub = true;
                     if (true) return;
                  }
               }
               if ( AV175TemIndicadorPntLote )
               {
                  if ( ( AV146Previsto >= AV173PrdFtrIni ) && ( AV146Previsto <= AV174PrdFtrFim ) )
                  {
                     AV165Previstas = (short)(AV165Previstas+1);
                     if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 )
                     {
                        AV169PntLoteBaseCalculo = (decimal)(AV169PntLoteBaseCalculo+A1854ContagemResultado_VlrCnc);
                     }
                     else
                     {
                        AV169PntLoteBaseCalculo = (decimal)(AV169PntLoteBaseCalculo+A606ContagemResultado_ValorFinal);
                     }
                     AV223PntLoteUndBruto = (decimal)(AV223PntLoteUndBruto+A574ContagemResultado_PFFinal);
                     if ( AV126Entregue <= AV146Previsto )
                     {
                        AV166Cumpridas = (short)(AV166Cumpridas+1);
                     }
                     else
                     {
                        AV167Atrasadas = (short)(AV167Atrasadas+1);
                     }
                  }
               }
               if ( AV247TemIndicadorDeFrequencia )
               {
                  /* Execute user subroutine: 'INDICADORFREQUENCIA' */
                  S2310 ();
                  if ( returnInSub )
                  {
                     pr_default.close(19);
                     returnInSub = true;
                     if (true) return;
                  }
               }
               AV141Os = "";
               pr_default.readNext(19);
            }
            pr_default.close(19);
         }
         if ( AV161TotalPontos < 0.001m )
         {
            AV155strPFTotal = StringUtil.Str( AV161TotalPontos, 14, 4);
         }
         else
         {
            AV155strPFTotal = StringUtil.Str( AV161TotalPontos, 14, 3);
         }
         if ( AV160TotalFinal < 0.001m )
         {
            AV159TotalFaturar = NumberUtil.Round( AV160TotalFinal, 4);
            AV156strPFTotalFaturar = StringUtil.Str( AV159TotalFaturar, 14, 4);
         }
         else if ( AV160TotalFinal < 0.01m )
         {
            AV159TotalFaturar = NumberUtil.Round( AV160TotalFinal, 3);
            AV156strPFTotalFaturar = StringUtil.Str( AV159TotalFaturar, 14, 3);
         }
         else
         {
            AV156strPFTotalFaturar = StringUtil.Str( AV160TotalFinal, 14, 2);
         }
         AV159TotalFaturar = NumberUtil.Round( AV160TotalFinal, 2);
         if ( ( ! AV229SemOSFM && ( AV215Linhas > 1 ) ) || ( AV129GlsValor < Convert.ToDecimal( 0 )) )
         {
            H8W0( false, 17) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV156strPFTotalFaturar, "")), 958, Gx_line+0, 1067, Gx_line+15, 2, 0, 0, 0) ;
            getPrinter().GxDrawText("Sub total:", 867, Gx_line+0, 915, Gx_line+14, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+17);
         }
      }

      protected void S1910( )
      {
         /* 'PRINTDESCRICAO' Routine */
         while ( StringUtil.Len( AV116DmnDescricao) >= AV133i )
         {
            /* Execute user subroutine: 'LINHADADMNDESCRICAO' */
            S1610 ();
            if (returnInSub) return;
            H8W0( false, 16) ;
            getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV112Descricao, "")), 575, Gx_line+0, 905, Gx_line+16, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+16);
         }
         AV133i = 0;
      }

      protected void S2010( )
      {
         /* 'PRINTGLOSADESC' Routine */
         while ( (Convert.ToDecimal( StringUtil.Len( AV127GlsDescricao) ) >= AV108d ) )
         {
            /* Execute user subroutine: 'LINHADAGLSDESCRICAO' */
            S2110 ();
            if (returnInSub) return;
            H8W0( false, 16) ;
            getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV112Descricao, "")), 133, Gx_line+0, 905, Gx_line+16, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+16);
         }
      }

      protected void S1610( )
      {
         /* 'LINHADADMNDESCRICAO' Routine */
         AV143p = AV114DescricaoLength;
         AV112Descricao = StringUtil.Substring( AV116DmnDescricao, AV133i, AV143p);
         if ( ( ( AV133i < StringUtil.Len( AV116DmnDescricao) ) && ( StringUtil.StringSearch( " -+/*_.,:;\\()[]{}?%!$><", StringUtil.Substring( AV112Descricao, AV143p, 1), 1) == 0 ) ) || ( ( AV133i + AV143p <= StringUtil.Len( AV116DmnDescricao) ) && ( StringUtil.StringSearch( " -+/*_.,:;\\()[]{}?%!$><", StringUtil.Substring( AV112Descricao, AV143p+1, 1), 1) == 0 ) ) )
         {
            AV144p2 = AV114DescricaoLength;
            while ( AV144p2 >= 1 )
            {
               AV143p = (short)(StringUtil.StringSearch( " -+/*_.,:;\\()[]{}?%!$><", StringUtil.Substring( AV112Descricao, AV144p2, 1), 1));
               if ( AV143p > 0 )
               {
                  if ( AV143p == 1 )
                  {
                     AV143p = (short)(AV144p2-1);
                     AV112Descricao = StringUtil.Substring( AV116DmnDescricao, AV133i, AV143p);
                  }
                  else
                  {
                     AV143p = AV144p2;
                     AV112Descricao = StringUtil.Substring( AV116DmnDescricao, AV133i, AV143p);
                  }
                  AV133i = (short)(AV133i+AV144p2);
                  if (true) break;
               }
               else
               {
                  AV143p = AV144p2;
               }
               AV144p2 = (short)(AV144p2+-1);
            }
            if ( AV143p < 2 )
            {
               AV112Descricao = StringUtil.Substring( AV116DmnDescricao, AV133i, AV114DescricaoLength);
               AV133i = (short)(AV133i+AV114DescricaoLength);
            }
         }
         else
         {
            AV133i = (short)(AV133i+AV143p);
         }
      }

      protected void S2110( )
      {
         /* 'LINHADAGLSDESCRICAO' Routine */
         AV143p = AV113DescricaoGlsLength;
         AV112Descricao = StringUtil.Substring( AV127GlsDescricao, (int)(AV108d), AV143p);
         if ( ( ( AV108d < Convert.ToDecimal( StringUtil.Len( AV127GlsDescricao) )) && ( StringUtil.StringSearch( " -+/*_.,:;\\()[]{}?%!$><", StringUtil.Substring( AV112Descricao, AV143p, 1), 1) == 0 ) ) || ( ( AV108d + AV143p <= Convert.ToDecimal( StringUtil.Len( AV127GlsDescricao) )) && ( StringUtil.StringSearch( " -+/*_.,:;\\()[]{}?%!$><", StringUtil.Substring( AV112Descricao, AV143p+1, 1), 1) == 0 ) ) )
         {
            AV144p2 = AV113DescricaoGlsLength;
            while ( AV144p2 >= 1 )
            {
               AV143p = (short)(StringUtil.StringSearch( " -+/*_.,:;\\()[]{}?%!$><", StringUtil.Substring( AV112Descricao, AV144p2, 1), 1));
               if ( AV143p > 0 )
               {
                  if ( AV143p == 1 )
                  {
                     AV143p = (short)(AV144p2-1);
                     AV112Descricao = StringUtil.Substring( AV127GlsDescricao, (int)(AV108d), AV143p);
                  }
                  else
                  {
                     AV143p = AV144p2;
                     AV112Descricao = StringUtil.Substring( AV127GlsDescricao, (int)(AV108d), AV143p);
                  }
                  AV108d = (decimal)(AV108d+AV144p2);
                  if (true) break;
               }
               else
               {
                  AV143p = AV144p2;
               }
               AV144p2 = (short)(AV144p2+-1);
            }
            if ( AV143p < 2 )
            {
               AV112Descricao = StringUtil.Substring( AV127GlsDescricao, (int)(AV108d), AV113DescricaoGlsLength);
               AV108d = (decimal)(AV108d+AV113DescricaoGlsLength);
            }
         }
         else
         {
            AV108d = (decimal)(AV108d+AV143p);
         }
      }

      protected void S1710( )
      {
         /* 'TEMINDPNT' Routine */
         new prc_temindicadorpnt(context ).execute( ref  AV232ContratoServicos_Codigo, out  AV158TemIndicadorDePontoalidade, out  AV175TemIndicadorPntLote) ;
      }

      protected void S1810( )
      {
         /* 'TEMINDFRQ' Routine */
         new prc_temindicadorfrq(context ).execute( ref  AV232ContratoServicos_Codigo, out  AV247TemIndicadorDeFrequencia) ;
      }

      protected void S2210( )
      {
         /* 'INDICADORPONTUALIDADE' Routine */
         /* Using cursor P008W36 */
         pr_default.execute(20, new Object[] {AV11Codigo});
         while ( (pr_default.getStatus(20) != 101) )
         {
            A1404ContagemResultadoExecucao_OSCod = P008W36_A1404ContagemResultadoExecucao_OSCod[0];
            A1405ContagemResultadoExecucao_Codigo = P008W36_A1405ContagemResultadoExecucao_Codigo[0];
            A1406ContagemResultadoExecucao_Inicio = P008W36_A1406ContagemResultadoExecucao_Inicio[0];
            new prc_pontualidadedaos(context ).execute(  A1405ContagemResultadoExecucao_Codigo,  AV70ContagemResultado_PFFaturar, out  AV149Reduz, out  AV129GlsValor, out  AV128GlsPF, out  AV127GlsDescricao) ;
            AV160TotalFinal = (decimal)(AV160TotalFinal-(NumberUtil.Round( AV129GlsValor, 3)));
            AV72ContagemResultado_PFTotalFaturar = (decimal)(AV72ContagemResultado_PFTotalFaturar-(NumberUtil.Round( AV129GlsValor, 3)));
            AV217StrGlsValor = "";
            AV216GlsTxt = "";
            if ( ( AV129GlsValor > Convert.ToDecimal( 0 )) )
            {
               AV129GlsValor = (decimal)(AV129GlsValor*-1);
               AV217StrGlsValor = StringUtil.Str( AV129GlsValor, 14, 2);
               AV216GlsTxt = "Glosa";
               AV215Linhas = (short)(AV215Linhas+1);
            }
            AV108d = (decimal)(1);
            /* Execute user subroutine: 'LINHADAGLSDESCRICAO' */
            S2110 ();
            if ( returnInSub )
            {
               pr_default.close(20);
               returnInSub = true;
               if (true) return;
            }
            H8W0( false, 16) ;
            getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV112Descricao, "")), 133, Gx_line+0, 905, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV216GlsTxt, "")), 92, Gx_line+0, 145, Gx_line+15, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV217StrGlsValor, "")), 994, Gx_line+0, 1068, Gx_line+15, 2+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+16);
            /* Execute user subroutine: 'PRINTGLOSADESC' */
            S2010 ();
            if ( returnInSub )
            {
               pr_default.close(20);
               returnInSub = true;
               if (true) return;
            }
            pr_default.readNext(20);
         }
         pr_default.close(20);
      }

      protected void S2310( )
      {
         /* 'INDICADORFREQUENCIA' Routine */
         AV217StrGlsValor = "";
         AV216GlsTxt = "";
         GXt_int4 = AV246Count;
         new prc_countnaocnf(context ).execute(  AV250Indicador,  AV11Codigo,  AV254SemCodigos,  context.localUtil.CToD( "01/11/2018", 2), out  GXt_int4) ;
         AV246Count = GXt_int4;
         GXt_decimal3 = AV149Reduz;
         GXt_int2 = AV250Indicador;
         new prc_frequenciademandareduz(context ).execute( ref  AV232ContratoServicos_Codigo,  AV246Count,  AV135IndP, out  GXt_int2, out  AV248Indicador_Sigla, out  AV249Faixa, out  GXt_decimal3) ;
         AV250Indicador = (short)(Convert.ToInt16(GXt_int2));
         AV149Reduz = GXt_decimal3;
         AV127GlsDescricao = AV248Indicador_Sigla + ": " + StringUtil.Trim( StringUtil.Str( (decimal)(AV246Count), 4, 0)) + " recusa(s).";
         AV215Linhas = (short)(AV215Linhas+1);
         AV108d = (decimal)(1);
         /* Execute user subroutine: 'LINHADAGLSDESCRICAO' */
         S2110 ();
         if (returnInSub) return;
         H8W0( false, 16) ;
         getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV112Descricao, "")), 133, Gx_line+0, 905, Gx_line+16, 0, 0, 0, 0) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV216GlsTxt, "")), 92, Gx_line+0, 145, Gx_line+15, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV217StrGlsValor, "")), 994, Gx_line+0, 1068, Gx_line+15, 2+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+16);
         /* Execute user subroutine: 'PRINTGLOSADESC' */
         S2010 ();
         if (returnInSub) return;
      }

      protected void S141( )
      {
         /* 'GETSANCAOINDPNTLOTE' Routine */
         AV149Reduz = 0;
         GXt_decimal3 = AV149Reduz;
         new prc_pontualidadelotereduz(context ).execute( ref  AV232ContratoServicos_Codigo,  0,  (short)(AV168PntLoteIndice), out  GXt_decimal3) ;
         AV149Reduz = GXt_decimal3;
         AV171PntLoteLiquido = AV72ContagemResultado_PFTotalFaturar;
         AV169PntLoteBaseCalculo = AV72ContagemResultado_PFTotalFaturar;
         if ( ( AV149Reduz > Convert.ToDecimal( 0 )) )
         {
            AV170PntLoteSancao = (decimal)(AV169PntLoteBaseCalculo*(AV149Reduz/ (decimal)(100)));
            AV171PntLoteLiquido = (decimal)(AV171PntLoteLiquido-AV170PntLoteSancao);
         }
      }

      protected void S261( )
      {
         /* 'CNPJ' Routine */
         /* Using cursor P008W37 */
         pr_default.execute(21, new Object[] {AV105Contratada});
         while ( (pr_default.getStatus(21) != 101) )
         {
            A40Contratada_PessoaCod = P008W37_A40Contratada_PessoaCod[0];
            A503Pessoa_MunicipioCod = P008W37_A503Pessoa_MunicipioCod[0];
            n503Pessoa_MunicipioCod = P008W37_n503Pessoa_MunicipioCod[0];
            A52Contratada_AreaTrabalhoCod = P008W37_A52Contratada_AreaTrabalhoCod[0];
            A1595Contratada_AreaTrbSrvPdr = P008W37_A1595Contratada_AreaTrbSrvPdr[0];
            n1595Contratada_AreaTrbSrvPdr = P008W37_n1595Contratada_AreaTrbSrvPdr[0];
            A633Servico_UO = P008W37_A633Servico_UO[0];
            n633Servico_UO = P008W37_n633Servico_UO[0];
            A39Contratada_Codigo = P008W37_A39Contratada_Codigo[0];
            A42Contratada_PessoaCNPJ = P008W37_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P008W37_n42Contratada_PessoaCNPJ[0];
            A521Pessoa_CEP = P008W37_A521Pessoa_CEP[0];
            n521Pessoa_CEP = P008W37_n521Pessoa_CEP[0];
            A23Estado_UF = P008W37_A23Estado_UF[0];
            n23Estado_UF = P008W37_n23Estado_UF[0];
            A26Municipio_Nome = P008W37_A26Municipio_Nome[0];
            n26Municipio_Nome = P008W37_n26Municipio_Nome[0];
            A519Pessoa_Endereco = P008W37_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = P008W37_n519Pessoa_Endereco[0];
            A503Pessoa_MunicipioCod = P008W37_A503Pessoa_MunicipioCod[0];
            n503Pessoa_MunicipioCod = P008W37_n503Pessoa_MunicipioCod[0];
            A42Contratada_PessoaCNPJ = P008W37_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P008W37_n42Contratada_PessoaCNPJ[0];
            A521Pessoa_CEP = P008W37_A521Pessoa_CEP[0];
            n521Pessoa_CEP = P008W37_n521Pessoa_CEP[0];
            A519Pessoa_Endereco = P008W37_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = P008W37_n519Pessoa_Endereco[0];
            A26Municipio_Nome = P008W37_A26Municipio_Nome[0];
            n26Municipio_Nome = P008W37_n26Municipio_Nome[0];
            A1595Contratada_AreaTrbSrvPdr = P008W37_A1595Contratada_AreaTrbSrvPdr[0];
            n1595Contratada_AreaTrbSrvPdr = P008W37_n1595Contratada_AreaTrbSrvPdr[0];
            A633Servico_UO = P008W37_A633Servico_UO[0];
            n633Servico_UO = P008W37_n633Servico_UO[0];
            A23Estado_UF = P008W37_A23Estado_UF[0];
            n23Estado_UF = P008W37_n23Estado_UF[0];
            AV226CNPJ = A42Contratada_PessoaCNPJ;
            AV238Endereco = StringUtil.Trim( A519Pessoa_Endereco) + ", " + StringUtil.Trim( A26Municipio_Nome) + " - " + A23Estado_UF + " CEP " + A521Pessoa_CEP;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(21);
      }

      protected void S271( )
      {
         /* 'DADOSDOCONTRATO' Routine */
         /* Using cursor P008W38 */
         pr_default.execute(22, new Object[] {AV228Contrato_Codigo});
         while ( (pr_default.getStatus(22) != 101) )
         {
            A39Contratada_Codigo = P008W38_A39Contratada_Codigo[0];
            A52Contratada_AreaTrabalhoCod = P008W38_A52Contratada_AreaTrabalhoCod[0];
            A1013Contrato_PrepostoCod = P008W38_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = P008W38_n1013Contrato_PrepostoCod[0];
            A1016Contrato_PrepostoPesCod = P008W38_A1016Contrato_PrepostoPesCod[0];
            n1016Contrato_PrepostoPesCod = P008W38_n1016Contrato_PrepostoPesCod[0];
            A29Contratante_Codigo = P008W38_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P008W38_n29Contratante_Codigo[0];
            A74Contrato_Codigo = P008W38_A74Contrato_Codigo[0];
            A77Contrato_Numero = P008W38_A77Contrato_Numero[0];
            A1015Contrato_PrepostoNom = P008W38_A1015Contrato_PrepostoNom[0];
            n1015Contrato_PrepostoNom = P008W38_n1015Contrato_PrepostoNom[0];
            A81Contrato_Quantidade = P008W38_A81Contrato_Quantidade[0];
            A2034Contratante_TtlRltGerencial = P008W38_A2034Contratante_TtlRltGerencial[0];
            n2034Contratante_TtlRltGerencial = P008W38_n2034Contratante_TtlRltGerencial[0];
            A1354Contrato_PrdFtrCada = P008W38_A1354Contrato_PrdFtrCada[0];
            n1354Contrato_PrdFtrCada = P008W38_n1354Contrato_PrdFtrCada[0];
            A1357Contrato_PrdFtrIni = P008W38_A1357Contrato_PrdFtrIni[0];
            n1357Contrato_PrdFtrIni = P008W38_n1357Contrato_PrdFtrIni[0];
            A1358Contrato_PrdFtrFim = P008W38_A1358Contrato_PrdFtrFim[0];
            n1358Contrato_PrdFtrFim = P008W38_n1358Contrato_PrdFtrFim[0];
            A52Contratada_AreaTrabalhoCod = P008W38_A52Contratada_AreaTrabalhoCod[0];
            A29Contratante_Codigo = P008W38_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P008W38_n29Contratante_Codigo[0];
            A2034Contratante_TtlRltGerencial = P008W38_A2034Contratante_TtlRltGerencial[0];
            n2034Contratante_TtlRltGerencial = P008W38_n2034Contratante_TtlRltGerencial[0];
            A1016Contrato_PrepostoPesCod = P008W38_A1016Contrato_PrepostoPesCod[0];
            n1016Contrato_PrepostoPesCod = P008W38_n1016Contrato_PrepostoPesCod[0];
            A1015Contrato_PrepostoNom = P008W38_A1015Contrato_PrepostoNom[0];
            n1015Contrato_PrepostoNom = P008W38_n1015Contrato_PrepostoNom[0];
            OV241Contratante_TtlRltGerencial = AV241Contratante_TtlRltGerencial;
            AV9Contrato_Numero = A77Contrato_Numero;
            AV225Preposto = StringUtil.Upper( A1015Contrato_PrepostoNom);
            AV243Contratado = (decimal)(A81Contrato_Quantidade);
            AV241Contratante_TtlRltGerencial = A2034Contratante_TtlRltGerencial;
            if ( StringUtil.StrCmp(A1354Contrato_PrdFtrCada, "S") == 0 )
            {
               if ( DateTimeUtil.Dow( AV173PrdFtrIni) != 2 )
               {
                  while ( DateTimeUtil.Dow( AV173PrdFtrIni) != 2 )
                  {
                     GXt_dtime5 = DateTimeUtil.ResetTime( AV173PrdFtrIni ) ;
                     AV173PrdFtrIni = DateTimeUtil.ResetTime(DateTimeUtil.TAdd( GXt_dtime5, 86400*(-1)));
                  }
               }
               if ( DateTimeUtil.Dow( AV174PrdFtrFim) != 1 )
               {
                  while ( DateTimeUtil.Dow( AV174PrdFtrFim) != 1 )
                  {
                     GXt_dtime5 = DateTimeUtil.ResetTime( AV174PrdFtrFim ) ;
                     AV174PrdFtrFim = DateTimeUtil.ResetTime(DateTimeUtil.TAdd( GXt_dtime5, 86400*(1)));
                  }
               }
            }
            else if ( StringUtil.StrCmp(A1354Contrato_PrdFtrCada, "M") == 0 )
            {
               AV133i = (short)(1-DateTimeUtil.Day( AV173PrdFtrIni));
               GXt_dtime5 = DateTimeUtil.ResetTime( AV173PrdFtrIni ) ;
               AV173PrdFtrIni = DateTimeUtil.ResetTime(DateTimeUtil.TAdd( GXt_dtime5, 86400*(AV133i)));
               AV174PrdFtrFim = DateTimeUtil.DateEndOfMonth( AV174PrdFtrFim);
            }
            else if ( StringUtil.StrCmp(A1354Contrato_PrdFtrCada, "P") == 0 )
            {
               if ( DateTimeUtil.Day( AV173PrdFtrIni) != A1357Contrato_PrdFtrIni )
               {
                  while ( DateTimeUtil.Day( AV173PrdFtrIni) != A1357Contrato_PrdFtrIni )
                  {
                     GXt_dtime5 = DateTimeUtil.ResetTime( AV173PrdFtrIni ) ;
                     AV173PrdFtrIni = DateTimeUtil.ResetTime(DateTimeUtil.TAdd( GXt_dtime5, 86400*(-1)));
                  }
               }
               if ( DateTimeUtil.Day( AV174PrdFtrFim) != A1358Contrato_PrdFtrFim )
               {
                  while ( DateTimeUtil.Day( AV174PrdFtrFim) != A1358Contrato_PrdFtrFim )
                  {
                     GXt_dtime5 = DateTimeUtil.ResetTime( AV174PrdFtrFim ) ;
                     AV174PrdFtrFim = DateTimeUtil.ResetTime(DateTimeUtil.TAdd( GXt_dtime5, 86400*(1)));
                  }
               }
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(22);
      }

      protected void S131( )
      {
         /* 'PRINTFREQUENCIA' Routine */
         if ( AV247TemIndicadorDeFrequencia )
         {
            AV246Count = 0;
            AV129GlsValor = 0;
            GXt_int2 = AV250Indicador;
            GXt_char6 = "FP";
            new prc_getindicador(context ).execute( ref  AV232ContratoServicos_Codigo, ref  GXt_char6, out  AV248Indicador_Sigla, out  AV252CalculoSob, out  GXt_int2) ;
            AV250Indicador = (short)(GXt_int2);
            GXt_int4 = AV246Count;
            new prc_countnaocnf(context ).execute(  AV250Indicador,  0,  AV12Codigos,  context.localUtil.CToD( "01/11/2018", 2), out  GXt_int4) ;
            AV246Count = GXt_int4;
            AV148Quantidade = (short)(AV12Codigos.Count);
            if ( StringUtil.StrCmp(AV252CalculoSob, "D") == 0 )
            {
               GXt_decimal3 = AV149Reduz;
               GXt_int2 = AV250Indicador;
               new prc_frequenciademandareduz(context ).execute( ref  AV232ContratoServicos_Codigo,  AV246Count,  (decimal)(0), out  GXt_int2, out  AV248Indicador_Sigla, out  AV249Faixa, out  GXt_decimal3) ;
               AV250Indicador = (short)(Convert.ToInt16(GXt_int2));
               AV149Reduz = GXt_decimal3;
            }
            else
            {
               if ( AV148Quantidade > 0 )
               {
                  AV135IndP = (decimal)(AV246Count/ (decimal)(AV148Quantidade));
                  GXt_decimal3 = AV149Reduz;
                  GXt_int2 = AV250Indicador;
                  new prc_frequenciademandareduz(context ).execute( ref  AV232ContratoServicos_Codigo,  0,  AV135IndP, out  GXt_int2, out  AV248Indicador_Sigla, out  AV249Faixa, out  GXt_decimal3) ;
                  AV250Indicador = (short)(Convert.ToInt16(GXt_int2));
                  AV149Reduz = GXt_decimal3;
               }
            }
            if ( ( AV149Reduz > Convert.ToDecimal( 0 )) )
            {
               AV129GlsValor = (decimal)(AV70ContagemResultado_PFFaturar*(AV149Reduz/ (decimal)(100)));
               AV72ContagemResultado_PFTotalFaturar = (decimal)(AV72ContagemResultado_PFTotalFaturar-AV129GlsValor);
               AV160TotalFinal = (decimal)(AV160TotalFinal-(NumberUtil.Round( AV129GlsValor, 3)));
               AV72ContagemResultado_PFTotalFaturar = (decimal)(AV72ContagemResultado_PFTotalFaturar-(NumberUtil.Round( AV129GlsValor, 3)));
               AV129GlsValor = (decimal)(AV129GlsValor*-1);
            }
            AV217StrGlsValor = StringUtil.Str( AV129GlsValor, 14, 2);
            H8W0( false, 65) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 9, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV246Count), "ZZZ9")), 692, Gx_line+17, 726, Gx_line+34, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV249Faixa), "ZZZ9")), 667, Gx_line+33, 701, Gx_line+50, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV217StrGlsValor, "")), 978, Gx_line+33, 1067, Gx_line+50, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV149Reduz, "ZZ9.99 %")), 708, Gx_line+33, 776, Gx_line+50, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV148Quantidade), "ZZZ9")), 442, Gx_line+17, 484, Gx_line+34, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV135IndP, "ZZ9.99")), 175, Gx_line+17, 226, Gx_line+34, 2+256, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Quantidade de OS:", 308, Gx_line+17, 428, Gx_line+35, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Somat�ria de recusas:", 542, Gx_line+17, 684, Gx_line+35, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("IARC:", 142, Gx_line+17, 178, Gx_line+35, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Faixa de apura��o:", 542, Gx_line+33, 664, Gx_line+51, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Glosa R$:", 853, Gx_line+33, 915, Gx_line+51, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("%", 233, Gx_line+17, 247, Gx_line+35, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+65);
         }
      }

      protected void S151( )
      {
         /* 'PRINTFAIXAS' Routine */
         /* Eject command */
         Gx_OldLine = Gx_line;
         Gx_line = (int)(P_lines+1);
         if ( AV158TemIndicadorDePontoalidade )
         {
            AV251ContratoServicosIndicador_Tipo = "P";
            /* Execute user subroutine: 'PRINTFAIXA' */
            S281 ();
            if (returnInSub) return;
         }
         if ( AV175TemIndicadorPntLote )
         {
            AV251ContratoServicosIndicador_Tipo = "PL";
            /* Execute user subroutine: 'PRINTFAIXA' */
            S281 ();
            if (returnInSub) return;
         }
         if ( AV247TemIndicadorDeFrequencia )
         {
            AV251ContratoServicosIndicador_Tipo = "FP";
            /* Execute user subroutine: 'PRINTFAIXA' */
            S281 ();
            if (returnInSub) return;
         }
      }

      protected void S281( )
      {
         /* 'PRINTFAIXA' Routine */
         /* Using cursor P008W39 */
         pr_default.execute(23, new Object[] {AV232ContratoServicos_Codigo, AV251ContratoServicosIndicador_Tipo});
         while ( (pr_default.getStatus(23) != 101) )
         {
            A1270ContratoServicosIndicador_CntSrvCod = P008W39_A1270ContratoServicosIndicador_CntSrvCod[0];
            A1308ContratoServicosIndicador_Tipo = P008W39_A1308ContratoServicosIndicador_Tipo[0];
            n1308ContratoServicosIndicador_Tipo = P008W39_n1308ContratoServicosIndicador_Tipo[0];
            A1269ContratoServicosIndicador_Codigo = P008W39_A1269ContratoServicosIndicador_Codigo[0];
            A1274ContratoServicosIndicador_Indicador = P008W39_A1274ContratoServicosIndicador_Indicador[0];
            A1271ContratoServicosIndicador_Numero = P008W39_A1271ContratoServicosIndicador_Numero[0];
            AV172PntLoteIndicador = "N� " + StringUtil.Trim( StringUtil.Str( (decimal)(A1271ContratoServicosIndicador_Numero), 4, 0)) + " / " + A1274ContratoServicosIndicador_Indicador;
            H8W0( false, 84) ;
            getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Regras de Apura��o:", 142, Gx_line+67, 294, Gx_line+84, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV172PntLoteIndicador, "")), 142, Gx_line+33, 872, Gx_line+51, 0+256, 0, 0, 1) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 10, false, true, true, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Indicador", 67, Gx_line+33, 126, Gx_line+50, 0+256, 0, 0, 1) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+84);
            AV133i = 1;
            /* Using cursor P008W40 */
            pr_default.execute(24, new Object[] {A1269ContratoServicosIndicador_Codigo});
            while ( (pr_default.getStatus(24) != 101) )
            {
               A1303ContratoServicosIndicadorFaixa_Reduz = P008W40_A1303ContratoServicosIndicadorFaixa_Reduz[0];
               A1302ContratoServicosIndicadorFaixa_Ate = P008W40_A1302ContratoServicosIndicadorFaixa_Ate[0];
               A1301ContratoServicosIndicadorFaixa_Desde = P008W40_A1301ContratoServicosIndicadorFaixa_Desde[0];
               A1300ContratoServicosIndicadorFaixa_Numero = P008W40_A1300ContratoServicosIndicadorFaixa_Numero[0];
               A1299ContratoServicosIndicadorFaixa_Codigo = P008W40_A1299ContratoServicosIndicadorFaixa_Codigo[0];
               if ( AV133i == 1 )
               {
                  H8W0( false, 18) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), "ZZZ9")), 100, Gx_line+0, 130, Gx_line+18, 2+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A1301ContratoServicosIndicadorFaixa_Desde, "ZZ9.99")), 150, Gx_line+0, 195, Gx_line+18, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A1302ContratoServicosIndicadorFaixa_Ate, "ZZ9.99")), 217, Gx_line+0, 262, Gx_line+18, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A1303ContratoServicosIndicadorFaixa_Reduz, "ZZ9.99 %")), 267, Gx_line+0, 326, Gx_line+18, 2+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("at�", 196, Gx_line+0, 217, Gx_line+18, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+18);
               }
               else if ( AV133i == 2 )
               {
                  /* Noskip command */
                  Gx_line = Gx_OldLine;
                  H8W0( false, 18) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("at�", 469, Gx_line+0, 490, Gx_line+18, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A1303ContratoServicosIndicadorFaixa_Reduz, "ZZ9.99 %")), 542, Gx_line+0, 601, Gx_line+18, 2+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A1302ContratoServicosIndicadorFaixa_Ate, "ZZ9.99")), 492, Gx_line+0, 537, Gx_line+18, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A1301ContratoServicosIndicadorFaixa_Desde, "ZZ9.99")), 425, Gx_line+0, 470, Gx_line+18, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), "ZZZ9")), 375, Gx_line+0, 405, Gx_line+18, 2+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+18);
               }
               else if ( AV133i == 3 )
               {
                  /* Noskip command */
                  Gx_line = Gx_OldLine;
                  H8W0( false, 18) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("at�", 756, Gx_line+0, 777, Gx_line+18, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A1303ContratoServicosIndicadorFaixa_Reduz, "ZZ9.99 %")), 825, Gx_line+0, 884, Gx_line+18, 2+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A1302ContratoServicosIndicadorFaixa_Ate, "ZZ9.99")), 775, Gx_line+0, 820, Gx_line+18, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A1301ContratoServicosIndicadorFaixa_Desde, "ZZ9.99")), 708, Gx_line+0, 753, Gx_line+18, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), "ZZZ9")), 658, Gx_line+0, 688, Gx_line+18, 2+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+18);
               }
               else
               {
                  H8W0( false, 18) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), "ZZZ9")), 100, Gx_line+0, 130, Gx_line+18, 2+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A1301ContratoServicosIndicadorFaixa_Desde, "ZZ9.99")), 150, Gx_line+0, 195, Gx_line+18, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A1302ContratoServicosIndicadorFaixa_Ate, "ZZ9.99")), 217, Gx_line+0, 262, Gx_line+18, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A1303ContratoServicosIndicadorFaixa_Reduz, "ZZ9.99 %")), 267, Gx_line+0, 326, Gx_line+18, 2+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("at�", 196, Gx_line+0, 217, Gx_line+18, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+18);
                  AV133i = 1;
               }
               AV133i = (short)(AV133i+1);
               pr_default.readNext(24);
            }
            pr_default.close(24);
            pr_default.readNext(23);
         }
         pr_default.close(23);
      }

      protected void H8W0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxDrawLine(0, Gx_line+6, 1076, Gx_line+6, 1, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("P�gina", 933, Gx_line+10, 968, Gx_line+24, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("de", 1008, Gx_line+10, 1022, Gx_line+24, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("{{Pages}}", 1025, Gx_line+10, 1074, Gx_line+24, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(Gx_page), "ZZZZZ9")), 967, Gx_line+10, 1006, Gx_line+25, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(AV164WWPContext.gxTpr_Username, 0, Gx_line+10, 340, Gx_line+25, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV219Hoje, "99/99/99 99:99"), 508, Gx_line+10, 588, Gx_line+25, 1+256, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV138NumeroRelatorio, "")), 800, Gx_line+8, 926, Gx_line+26, 1+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+42);
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               if ( AV142OSAutomatica )
               {
                  if ( Gx_page == 1 )
                  {
                     getPrinter().GxDrawRect(0, Gx_line+133, 1073, Gx_line+154, 1, 0, 0, 0, 1, 245, 245, 245, 0, 0, 0, 0, 0, 0, 0, 0) ;
                     getPrinter().GxDrawRect(0, Gx_line+183, 1073, Gx_line+204, 1, 0, 0, 0, 1, 245, 245, 245, 0, 0, 0, 0, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Calibri", 9, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV148Quantidade), "ZZZ9")), 667, Gx_line+162, 717, Gx_line+178, 2, 0, 0, 0) ;
                     getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("SUM�RIO A FATURAR", 483, Gx_line+133, 622, Gx_line+152, 0+256, 0, 0, 0) ;
                     getPrinter().GxAttris("Calibri", 14, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV241Contratante_TtlRltGerencial, "")), 167, Gx_line+5, 942, Gx_line+30, 1, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Qtd de Demandas Executadas para o Faturamento:", 392, Gx_line+162, 649, Gx_line+176, 0, 0, 0, 0) ;
                     getPrinter().GxDrawBitMap(AV233Contratada_Logo, 0, Gx_line+17, 120, Gx_line+117) ;
                     getPrinter().GxDrawText("CNPJ:", 133, Gx_line+50, 165, Gx_line+64, 0+256, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV238Endereco, "")), 133, Gx_line+67, 408, Gx_line+117, 0+16, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV226CNPJ, "")), 167, Gx_line+50, 241, Gx_line+65, 0+256, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV138NumeroRelatorio, "")), 483, Gx_line+67, 609, Gx_line+85, 1+256, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV157SubTitulo, "")), 500, Gx_line+33, 595, Gx_line+51, 1+256, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV235Periodo, "")), 425, Gx_line+50, 676, Gx_line+68, 1+256, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV234Contratada_Sigla, "@!")), 133, Gx_line+33, 228, Gx_line+51, 0+256, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV221ContratanteArea, "")), 683, Gx_line+33, 950, Gx_line+51, 2, 0, 0, 1) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+210);
                  }
                  else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV251ContratoServicosIndicador_Tipo)) )
                  {
                     getPrinter().GxDrawRect(0, Gx_line+0, 1073, Gx_line+35, 1, 0, 0, 0, 1, 245, 245, 245, 0, 0, 0, 0, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV220UnidadeMedicao_Sigla, "@!")), 908, Gx_line+4, 966, Gx_line+19, 1, 0, 0, 0) ;
                     getPrinter().GxDrawText("Data de", 225, Gx_line+4, 270, Gx_line+18, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText("entrega", 225, Gx_line+17, 264, Gx_line+31, 0+256, 0, 0, 0) ;
                     getPrinter().GxDrawText("OS Refer�ncia", 58, Gx_line+17, 139, Gx_line+31, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText("Servi�o", 275, Gx_line+17, 360, Gx_line+31, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText("Sistema", 400, Gx_line+17, 461, Gx_line+31, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText("Valor", 975, Gx_line+4, 1005, Gx_line+18, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText("Valor", 1033, Gx_line+4, 1058, Gx_line+18, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText("Descri��o da OS", 575, Gx_line+17, 667, Gx_line+31, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText("Exec.", 925, Gx_line+17, 954, Gx_line+31, 0+256, 0, 0, 0) ;
                     getPrinter().GxDrawText("Unit�rio", 967, Gx_line+17, 1006, Gx_line+31, 0+256, 0, 0, 0) ;
                     getPrinter().GxDrawText("Total", 1033, Gx_line+17, 1059, Gx_line+31, 0+256, 0, 0, 0) ;
                     getPrinter().GxDrawText("N� OS", 2, Gx_line+17, 44, Gx_line+31, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+43);
                     if ( AV133i > AV114DescricaoLength * 1.1m )
                     {
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A509ContagemrResultado_SistemaSigla, "@!")), 400, Gx_line+0, 574, Gx_line+15, 0, 0, 0, 0) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV126Entregue, "99/99/99"), 217, Gx_line+0, 266, Gx_line+15, 0+256, 0, 0, 0) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A801ContagemResultado_ServicoSigla, "@!")), 275, Gx_line+0, 354, Gx_line+15, 0+256, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+15);
                        /* Noskip command */
                        Gx_line = Gx_OldLine;
                     }
                  }
               }
               else
               {
                  if ( Gx_page == 1 )
                  {
                     getPrinter().GxDrawRect(0, Gx_line+133, 1073, Gx_line+154, 1, 0, 0, 0, 1, 245, 245, 245, 0, 0, 0, 0, 0, 0, 0, 0) ;
                     getPrinter().GxDrawRect(0, Gx_line+183, 1073, Gx_line+204, 1, 0, 0, 0, 1, 245, 245, 245, 0, 0, 0, 0, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Calibri", 9, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV148Quantidade), "ZZZ9")), 667, Gx_line+162, 717, Gx_line+178, 2, 0, 0, 0) ;
                     getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("SUM�RIO A FATURAR", 483, Gx_line+133, 622, Gx_line+152, 0+256, 0, 0, 0) ;
                     getPrinter().GxAttris("Calibri", 14, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV241Contratante_TtlRltGerencial, "")), 158, Gx_line+6, 933, Gx_line+31, 1, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Qtd de Demandas Executadas para o Faturamento:", 392, Gx_line+162, 649, Gx_line+176, 0, 0, 0, 0) ;
                     getPrinter().GxDrawBitMap(AV233Contratada_Logo, 0, Gx_line+17, 120, Gx_line+117) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV226CNPJ, "")), 167, Gx_line+50, 241, Gx_line+65, 0+256, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV238Endereco, "")), 133, Gx_line+67, 408, Gx_line+117, 0+16, 0, 0, 0) ;
                     getPrinter().GxDrawText("CNPJ:", 133, Gx_line+50, 165, Gx_line+64, 0+256, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV157SubTitulo, "")), 500, Gx_line+33, 595, Gx_line+51, 1+256, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV138NumeroRelatorio, "")), 483, Gx_line+67, 609, Gx_line+85, 1+256, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV235Periodo, "")), 425, Gx_line+50, 676, Gx_line+68, 1+256, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV234Contratada_Sigla, "@!")), 133, Gx_line+33, 228, Gx_line+51, 0+256, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV221ContratanteArea, "")), 683, Gx_line+33, 950, Gx_line+51, 2, 0, 0, 1) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+210);
                  }
                  else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV251ContratoServicosIndicador_Tipo)) )
                  {
                     getPrinter().GxDrawRect(0, Gx_line+0, 1073, Gx_line+35, 1, 0, 0, 0, 1, 245, 245, 245, 0, 0, 0, 0, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Valor", 975, Gx_line+4, 1005, Gx_line+18, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV220UnidadeMedicao_Sigla, "@!")), 908, Gx_line+4, 966, Gx_line+19, 1, 0, 0, 0) ;
                     getPrinter().GxDrawText("Exec.", 925, Gx_line+17, 954, Gx_line+31, 0+256, 0, 0, 0) ;
                     getPrinter().GxDrawText("Data de", 350, Gx_line+4, 411, Gx_line+18, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText("entrega", 350, Gx_line+17, 389, Gx_line+31, 0+256, 0, 0, 0) ;
                     getPrinter().GxDrawText("OS Refer�ncia", 100, Gx_line+17, 183, Gx_line+31, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText("Sistema", 400, Gx_line+17, 461, Gx_line+31, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText("Valor", 1033, Gx_line+4, 1058, Gx_line+18, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText("Descri��o da OS", 575, Gx_line+17, 667, Gx_line+31, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText("Unit�rio", 967, Gx_line+17, 1006, Gx_line+31, 0+256, 0, 0, 0) ;
                     getPrinter().GxDrawText("Total", 1033, Gx_line+17, 1059, Gx_line+31, 0+256, 0, 0, 0) ;
                     getPrinter().GxDrawText("N� OS", 2, Gx_line+17, 44, Gx_line+31, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+43);
                  }
               }
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
         add_metrics2( ) ;
         add_metrics3( ) ;
         add_metrics4( ) ;
         add_metrics5( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Arial", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics2( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, true, 56, 14, 70, 118,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 18, 18, 22, 35, 35, 56, 42, 12, 21, 21, 25, 37, 18, 21, 18, 18, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 18, 18, 37, 37, 37, 35, 64, 42, 42, 45, 45, 42, 38, 49, 45, 18, 32, 42, 35, 53, 45, 49, 42, 49, 45, 42, 38, 45, 42, 61, 42, 42, 38, 18, 18, 18, 30, 35, 21, 35, 35, 32, 35, 35, 18, 35, 35, 14, 14, 32, 14, 52, 35, 35, 35, 35, 21, 32, 18, 35, 32, 45, 32, 32, 29, 21, 16, 21, 37, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 18, 21, 35, 35, 34, 35, 16, 35, 21, 46, 23, 35, 37, 21, 46, 35, 25, 35, 21, 20, 21, 35, 34, 21, 21, 20, 23, 35, 53, 53, 53, 38, 42, 42, 42, 42, 42, 42, 63, 45, 42, 42, 42, 42, 18, 18, 18, 18, 45, 45, 49, 49, 49, 49, 49, 37, 49, 45, 45, 45, 45, 42, 42, 38, 35, 35, 35, 35, 35, 35, 56, 32, 35, 35, 35, 35, 18, 18, 18, 18, 35, 35, 35, 35, 35, 35, 35, 35, 38, 35, 35, 35, 35, 32, 35, 32}) ;
      }

      protected void add_metrics3( )
      {
         getPrinter().setMetrics("Courier New", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics4( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics5( )
      {
         getPrinter().setMetrics("Calibri", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV164WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV221ContratanteArea = "";
         AV219Hoje = (DateTime)(DateTime.MinValue);
         AV111DataInicio = DateTime.MinValue;
         AV110DataFim = DateTime.MinValue;
         AV235Periodo = "";
         AV173PrdFtrIni = DateTime.MinValue;
         AV174PrdFtrFim = DateTime.MinValue;
         AV12Codigos = new GxSimpleCollection();
         AV163WebSession = context.GetSession();
         AV157SubTitulo = "";
         scmdbuf = "";
         P008W2_A146Modulo_Codigo = new int[1] ;
         P008W2_n146Modulo_Codigo = new bool[] {false} ;
         P008W2_A127Sistema_Codigo = new int[1] ;
         P008W2_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P008W2_A1620ContagemResultado_CntSrvUndCnt = new int[1] ;
         P008W2_n1620ContagemResultado_CntSrvUndCnt = new bool[] {false} ;
         P008W2_A29Contratante_Codigo = new int[1] ;
         P008W2_n29Contratante_Codigo = new bool[] {false} ;
         P008W2_A456ContagemResultado_Codigo = new int[1] ;
         P008W2_A40001ContagemResultado_ContratadaLogo_GXI = new String[] {""} ;
         P008W2_n40001ContagemResultado_ContratadaLogo_GXI = new bool[] {false} ;
         P008W2_A490ContagemResultado_ContratadaCod = new int[1] ;
         P008W2_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P008W2_A601ContagemResultado_Servico = new int[1] ;
         P008W2_n601ContagemResultado_Servico = new bool[] {false} ;
         P008W2_A1603ContagemResultado_CntCod = new int[1] ;
         P008W2_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P008W2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P008W2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P008W2_A1621ContagemResultado_CntSrvUndCntSgl = new String[] {""} ;
         P008W2_n1621ContagemResultado_CntSrvUndCntSgl = new bool[] {false} ;
         P008W2_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         P008W2_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         P008W2_A2034Contratante_TtlRltGerencial = new String[] {""} ;
         P008W2_n2034Contratante_TtlRltGerencial = new bool[] {false} ;
         P008W2_A1816ContagemResultado_ContratadaLogo = new String[] {""} ;
         P008W2_n1816ContagemResultado_ContratadaLogo = new bool[] {false} ;
         A40001ContagemResultado_ContratadaLogo_GXI = "";
         A1621ContagemResultado_CntSrvUndCntSgl = "";
         A803ContagemResultado_ContratadaSigla = "";
         A2034Contratante_TtlRltGerencial = "";
         A1816ContagemResultado_ContratadaLogo = "";
         OV241Contratante_TtlRltGerencial = "";
         AV241Contratante_TtlRltGerencial = "Relat�rio Gerencial";
         AV220UnidadeMedicao_Sigla = "";
         AV233Contratada_Logo = "";
         A40000Contratada_Logo_GXI = "";
         AV234Contratada_Sigla = "";
         AV251ContratoServicosIndicador_Tipo = "";
         A1308ContratoServicosIndicador_Tipo = "";
         P008W4_A40002ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         A40002ContagemResultado_DataUltCnt = DateTime.MinValue;
         P008W6_A40003GXC4 = new DateTime[] {DateTime.MinValue} ;
         A40003GXC4 = (DateTime)(DateTime.MinValue);
         P008W8_A40004GXC5 = new DateTime[] {DateTime.MinValue} ;
         A40004GXC5 = DateTime.MinValue;
         P008W10_A40005GXC6 = new DateTime[] {DateTime.MinValue} ;
         A40005GXC6 = (DateTime)(DateTime.MinValue);
         P008W12_A40006GXC7 = new DateTime[] {DateTime.MinValue} ;
         A40006GXC7 = DateTime.MinValue;
         P008W14_A40007GXC8 = new DateTime[] {DateTime.MinValue} ;
         A40007GXC8 = (DateTime)(DateTime.MinValue);
         P008W16_A40008GXC9 = new DateTime[] {DateTime.MinValue} ;
         A40008GXC9 = DateTime.MinValue;
         P008W18_A40009GXC10 = new DateTime[] {DateTime.MinValue} ;
         A40009GXC10 = (DateTime)(DateTime.MinValue);
         P008W20_A1620ContagemResultado_CntSrvUndCnt = new int[1] ;
         P008W20_n1620ContagemResultado_CntSrvUndCnt = new bool[] {false} ;
         P008W20_A456ContagemResultado_Codigo = new int[1] ;
         P008W20_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P008W20_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P008W20_A40001ContagemResultado_ContratadaLogo_GXI = new String[] {""} ;
         P008W20_n40001ContagemResultado_ContratadaLogo_GXI = new bool[] {false} ;
         P008W20_A490ContagemResultado_ContratadaCod = new int[1] ;
         P008W20_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P008W20_A601ContagemResultado_Servico = new int[1] ;
         P008W20_n601ContagemResultado_Servico = new bool[] {false} ;
         P008W20_A1603ContagemResultado_CntCod = new int[1] ;
         P008W20_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P008W20_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P008W20_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P008W20_A1621ContagemResultado_CntSrvUndCntSgl = new String[] {""} ;
         P008W20_n1621ContagemResultado_CntSrvUndCntSgl = new bool[] {false} ;
         P008W20_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         P008W20_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         P008W20_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P008W20_n566ContagemResultado_DataUltCnt = new bool[] {false} ;
         P008W20_A1816ContagemResultado_ContratadaLogo = new String[] {""} ;
         P008W20_n1816ContagemResultado_ContratadaLogo = new bool[] {false} ;
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         P008W23_A563Lote_Nome = new String[] {""} ;
         P008W23_A562Lote_Numero = new String[] {""} ;
         P008W23_A2055Lote_CntUsado = new decimal[1] ;
         P008W23_n2055Lote_CntUsado = new bool[] {false} ;
         P008W23_A2056Lote_CntSaldo = new decimal[1] ;
         P008W23_n2056Lote_CntSaldo = new bool[] {false} ;
         P008W23_A596Lote_Codigo = new int[1] ;
         P008W23_A567Lote_DataIni = new DateTime[] {DateTime.MinValue} ;
         P008W23_A568Lote_DataFim = new DateTime[] {DateTime.MinValue} ;
         A563Lote_Nome = "";
         A562Lote_Numero = "";
         A567Lote_DataIni = DateTime.MinValue;
         A568Lote_DataFim = DateTime.MinValue;
         AV138NumeroRelatorio = "";
         P008W24_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P008W24_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P008W24_A1854ContagemResultado_VlrCnc = new decimal[1] ;
         P008W24_n1854ContagemResultado_VlrCnc = new bool[] {false} ;
         P008W24_A512ContagemResultado_ValorPF = new decimal[1] ;
         P008W24_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P008W24_A456ContagemResultado_Codigo = new int[1] ;
         A484ContagemResultado_StatusDmn = "";
         AV9Contrato_Numero = "";
         P008W25_A456ContagemResultado_Codigo = new int[1] ;
         P008W25_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P008W25_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         A493ContagemResultado_DemandaFM = "";
         AV141Os = "";
         P008W26_A456ContagemResultado_Codigo = new int[1] ;
         P008W26_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P008W26_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         AV46ContagemResultado_DemandaFM = "";
         AV225Preposto = "";
         AV226CNPJ = "";
         AV172PntLoteIndicador = "";
         P008W28_A489ContagemResultado_SistemaCod = new int[1] ;
         P008W28_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P008W28_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P008W28_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P008W28_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P008W28_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P008W28_A1854ContagemResultado_VlrCnc = new decimal[1] ;
         P008W28_n1854ContagemResultado_VlrCnc = new bool[] {false} ;
         P008W28_A494ContagemResultado_Descricao = new String[] {""} ;
         P008W28_n494ContagemResultado_Descricao = new bool[] {false} ;
         P008W28_A490ContagemResultado_ContratadaCod = new int[1] ;
         P008W28_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P008W28_A601ContagemResultado_Servico = new int[1] ;
         P008W28_n601ContagemResultado_Servico = new bool[] {false} ;
         P008W28_A1237ContagemResultado_PrazoMaisDias = new short[1] ;
         P008W28_n1237ContagemResultado_PrazoMaisDias = new bool[] {false} ;
         P008W28_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         P008W28_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         P008W28_A2017ContagemResultado_DataEntregaReal = new DateTime[] {DateTime.MinValue} ;
         P008W28_n2017ContagemResultado_DataEntregaReal = new bool[] {false} ;
         P008W28_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P008W28_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P008W28_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P008W28_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P008W28_A457ContagemResultado_Demanda = new String[] {""} ;
         P008W28_n457ContagemResultado_Demanda = new bool[] {false} ;
         P008W28_A2008ContagemResultado_CntSrvAls = new String[] {""} ;
         P008W28_n2008ContagemResultado_CntSrvAls = new bool[] {false} ;
         P008W28_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P008W28_A1051ContagemResultado_GlsValor = new decimal[1] ;
         P008W28_n1051ContagemResultado_GlsValor = new bool[] {false} ;
         P008W28_A1050ContagemResultado_GlsDescricao = new String[] {""} ;
         P008W28_n1050ContagemResultado_GlsDescricao = new bool[] {false} ;
         P008W28_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P008W28_n566ContagemResultado_DataUltCnt = new bool[] {false} ;
         P008W28_A456ContagemResultado_Codigo = new int[1] ;
         P008W28_A512ContagemResultado_ValorPF = new decimal[1] ;
         P008W28_n512ContagemResultado_ValorPF = new bool[] {false} ;
         A494ContagemResultado_Descricao = "";
         A2017ContagemResultado_DataEntregaReal = (DateTime)(DateTime.MinValue);
         A801ContagemResultado_ServicoSigla = "";
         A509ContagemrResultado_SistemaSigla = "";
         A457ContagemResultado_Demanda = "";
         A2008ContagemResultado_CntSrvAls = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A1050ContagemResultado_GlsDescricao = "";
         AV213DataCnt = DateTime.MinValue;
         AV116DmnDescricao = "";
         AV216GlsTxt = "";
         AV154strPFFinal = "";
         AV126Entregue = DateTime.MinValue;
         AV112Descricao = "";
         AV127GlsDescricao = "";
         AV217StrGlsValor = "";
         AV153Solicitada = DateTime.MinValue;
         AV146Previsto = DateTime.MinValue;
         AV155strPFTotal = "";
         AV156strPFTotalFaturar = "";
         P008W29_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P008W29_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P008W29_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P008W29_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P008W29_A1854ContagemResultado_VlrCnc = new decimal[1] ;
         P008W29_n1854ContagemResultado_VlrCnc = new bool[] {false} ;
         P008W29_A456ContagemResultado_Codigo = new int[1] ;
         P008W29_A512ContagemResultado_ValorPF = new decimal[1] ;
         P008W29_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P008W30_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P008W30_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P008W30_A456ContagemResultado_Codigo = new int[1] ;
         P008W30_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P008W30_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P008W31_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P008W31_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P008W31_A456ContagemResultado_Codigo = new int[1] ;
         P008W31_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P008W31_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P008W33_A489ContagemResultado_SistemaCod = new int[1] ;
         P008W33_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P008W33_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P008W33_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P008W33_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P008W33_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P008W33_A1854ContagemResultado_VlrCnc = new decimal[1] ;
         P008W33_n1854ContagemResultado_VlrCnc = new bool[] {false} ;
         P008W33_A494ContagemResultado_Descricao = new String[] {""} ;
         P008W33_n494ContagemResultado_Descricao = new bool[] {false} ;
         P008W33_A490ContagemResultado_ContratadaCod = new int[1] ;
         P008W33_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P008W33_A601ContagemResultado_Servico = new int[1] ;
         P008W33_n601ContagemResultado_Servico = new bool[] {false} ;
         P008W33_A1237ContagemResultado_PrazoMaisDias = new short[1] ;
         P008W33_n1237ContagemResultado_PrazoMaisDias = new bool[] {false} ;
         P008W33_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         P008W33_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         P008W33_A2017ContagemResultado_DataEntregaReal = new DateTime[] {DateTime.MinValue} ;
         P008W33_n2017ContagemResultado_DataEntregaReal = new bool[] {false} ;
         P008W33_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P008W33_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P008W33_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P008W33_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P008W33_A457ContagemResultado_Demanda = new String[] {""} ;
         P008W33_n457ContagemResultado_Demanda = new bool[] {false} ;
         P008W33_A2008ContagemResultado_CntSrvAls = new String[] {""} ;
         P008W33_n2008ContagemResultado_CntSrvAls = new bool[] {false} ;
         P008W33_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P008W33_A1051ContagemResultado_GlsValor = new decimal[1] ;
         P008W33_n1051ContagemResultado_GlsValor = new bool[] {false} ;
         P008W33_A1050ContagemResultado_GlsDescricao = new String[] {""} ;
         P008W33_n1050ContagemResultado_GlsDescricao = new bool[] {false} ;
         P008W33_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P008W33_n566ContagemResultado_DataUltCnt = new bool[] {false} ;
         P008W33_A456ContagemResultado_Codigo = new int[1] ;
         P008W33_A512ContagemResultado_ValorPF = new decimal[1] ;
         P008W33_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P008W35_A489ContagemResultado_SistemaCod = new int[1] ;
         P008W35_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P008W35_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P008W35_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P008W35_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P008W35_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P008W35_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P008W35_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P008W35_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P008W35_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P008W35_A1854ContagemResultado_VlrCnc = new decimal[1] ;
         P008W35_n1854ContagemResultado_VlrCnc = new bool[] {false} ;
         P008W35_A494ContagemResultado_Descricao = new String[] {""} ;
         P008W35_n494ContagemResultado_Descricao = new bool[] {false} ;
         P008W35_A490ContagemResultado_ContratadaCod = new int[1] ;
         P008W35_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P008W35_A601ContagemResultado_Servico = new int[1] ;
         P008W35_n601ContagemResultado_Servico = new bool[] {false} ;
         P008W35_A1237ContagemResultado_PrazoMaisDias = new short[1] ;
         P008W35_n1237ContagemResultado_PrazoMaisDias = new bool[] {false} ;
         P008W35_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         P008W35_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         P008W35_A2017ContagemResultado_DataEntregaReal = new DateTime[] {DateTime.MinValue} ;
         P008W35_n2017ContagemResultado_DataEntregaReal = new bool[] {false} ;
         P008W35_A457ContagemResultado_Demanda = new String[] {""} ;
         P008W35_n457ContagemResultado_Demanda = new bool[] {false} ;
         P008W35_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P008W35_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P008W35_A2008ContagemResultado_CntSrvAls = new String[] {""} ;
         P008W35_n2008ContagemResultado_CntSrvAls = new bool[] {false} ;
         P008W35_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P008W35_A1051ContagemResultado_GlsValor = new decimal[1] ;
         P008W35_n1051ContagemResultado_GlsValor = new bool[] {false} ;
         P008W35_A1050ContagemResultado_GlsDescricao = new String[] {""} ;
         P008W35_n1050ContagemResultado_GlsDescricao = new bool[] {false} ;
         P008W35_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P008W35_n566ContagemResultado_DataUltCnt = new bool[] {false} ;
         P008W35_A456ContagemResultado_Codigo = new int[1] ;
         P008W35_A512ContagemResultado_ValorPF = new decimal[1] ;
         P008W35_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P008W36_A1404ContagemResultadoExecucao_OSCod = new int[1] ;
         P008W36_A1405ContagemResultadoExecucao_Codigo = new int[1] ;
         P008W36_A1406ContagemResultadoExecucao_Inicio = new DateTime[] {DateTime.MinValue} ;
         A1406ContagemResultadoExecucao_Inicio = (DateTime)(DateTime.MinValue);
         AV254SemCodigos = new GxSimpleCollection();
         AV248Indicador_Sigla = "";
         P008W37_A40Contratada_PessoaCod = new int[1] ;
         P008W37_A503Pessoa_MunicipioCod = new int[1] ;
         P008W37_n503Pessoa_MunicipioCod = new bool[] {false} ;
         P008W37_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P008W37_A1595Contratada_AreaTrbSrvPdr = new int[1] ;
         P008W37_n1595Contratada_AreaTrbSrvPdr = new bool[] {false} ;
         P008W37_A633Servico_UO = new int[1] ;
         P008W37_n633Servico_UO = new bool[] {false} ;
         P008W37_A39Contratada_Codigo = new int[1] ;
         P008W37_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P008W37_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P008W37_A521Pessoa_CEP = new String[] {""} ;
         P008W37_n521Pessoa_CEP = new bool[] {false} ;
         P008W37_A23Estado_UF = new String[] {""} ;
         P008W37_n23Estado_UF = new bool[] {false} ;
         P008W37_A26Municipio_Nome = new String[] {""} ;
         P008W37_n26Municipio_Nome = new bool[] {false} ;
         P008W37_A519Pessoa_Endereco = new String[] {""} ;
         P008W37_n519Pessoa_Endereco = new bool[] {false} ;
         A42Contratada_PessoaCNPJ = "";
         A521Pessoa_CEP = "";
         A23Estado_UF = "";
         A26Municipio_Nome = "";
         A519Pessoa_Endereco = "";
         AV238Endereco = "";
         P008W38_A39Contratada_Codigo = new int[1] ;
         P008W38_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P008W38_A1013Contrato_PrepostoCod = new int[1] ;
         P008W38_n1013Contrato_PrepostoCod = new bool[] {false} ;
         P008W38_A1016Contrato_PrepostoPesCod = new int[1] ;
         P008W38_n1016Contrato_PrepostoPesCod = new bool[] {false} ;
         P008W38_A29Contratante_Codigo = new int[1] ;
         P008W38_n29Contratante_Codigo = new bool[] {false} ;
         P008W38_A74Contrato_Codigo = new int[1] ;
         P008W38_A77Contrato_Numero = new String[] {""} ;
         P008W38_A1015Contrato_PrepostoNom = new String[] {""} ;
         P008W38_n1015Contrato_PrepostoNom = new bool[] {false} ;
         P008W38_A81Contrato_Quantidade = new int[1] ;
         P008W38_A2034Contratante_TtlRltGerencial = new String[] {""} ;
         P008W38_n2034Contratante_TtlRltGerencial = new bool[] {false} ;
         P008W38_A1354Contrato_PrdFtrCada = new String[] {""} ;
         P008W38_n1354Contrato_PrdFtrCada = new bool[] {false} ;
         P008W38_A1357Contrato_PrdFtrIni = new short[1] ;
         P008W38_n1357Contrato_PrdFtrIni = new bool[] {false} ;
         P008W38_A1358Contrato_PrdFtrFim = new short[1] ;
         P008W38_n1358Contrato_PrdFtrFim = new bool[] {false} ;
         A77Contrato_Numero = "";
         A1015Contrato_PrepostoNom = "";
         A1354Contrato_PrdFtrCada = "";
         GXt_dtime5 = (DateTime)(DateTime.MinValue);
         GXt_char6 = "";
         AV252CalculoSob = "";
         P008W39_A1270ContratoServicosIndicador_CntSrvCod = new int[1] ;
         P008W39_A1308ContratoServicosIndicador_Tipo = new String[] {""} ;
         P008W39_n1308ContratoServicosIndicador_Tipo = new bool[] {false} ;
         P008W39_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         P008W39_A1274ContratoServicosIndicador_Indicador = new String[] {""} ;
         P008W39_A1271ContratoServicosIndicador_Numero = new short[1] ;
         A1274ContratoServicosIndicador_Indicador = "";
         P008W40_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         P008W40_A1303ContratoServicosIndicadorFaixa_Reduz = new decimal[1] ;
         P008W40_A1302ContratoServicosIndicadorFaixa_Ate = new decimal[1] ;
         P008W40_A1301ContratoServicosIndicadorFaixa_Desde = new decimal[1] ;
         P008W40_A1300ContratoServicosIndicadorFaixa_Numero = new short[1] ;
         P008W40_A1299ContratoServicosIndicadorFaixa_Codigo = new int[1] ;
         AV233Contratada_Logo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.arel_contagemgerencial__default(),
            new Object[][] {
                new Object[] {
               P008W2_A146Modulo_Codigo, P008W2_n146Modulo_Codigo, P008W2_A127Sistema_Codigo, P008W2_A135Sistema_AreaTrabalhoCod, P008W2_A1620ContagemResultado_CntSrvUndCnt, P008W2_n1620ContagemResultado_CntSrvUndCnt, P008W2_A29Contratante_Codigo, P008W2_n29Contratante_Codigo, P008W2_A456ContagemResultado_Codigo, P008W2_A40001ContagemResultado_ContratadaLogo_GXI,
               P008W2_n40001ContagemResultado_ContratadaLogo_GXI, P008W2_A490ContagemResultado_ContratadaCod, P008W2_n490ContagemResultado_ContratadaCod, P008W2_A601ContagemResultado_Servico, P008W2_n601ContagemResultado_Servico, P008W2_A1603ContagemResultado_CntCod, P008W2_n1603ContagemResultado_CntCod, P008W2_A1553ContagemResultado_CntSrvCod, P008W2_n1553ContagemResultado_CntSrvCod, P008W2_A1621ContagemResultado_CntSrvUndCntSgl,
               P008W2_n1621ContagemResultado_CntSrvUndCntSgl, P008W2_A803ContagemResultado_ContratadaSigla, P008W2_n803ContagemResultado_ContratadaSigla, P008W2_A2034Contratante_TtlRltGerencial, P008W2_n2034Contratante_TtlRltGerencial, P008W2_A1816ContagemResultado_ContratadaLogo, P008W2_n1816ContagemResultado_ContratadaLogo
               }
               , new Object[] {
               P008W4_A40002ContagemResultado_DataUltCnt
               }
               , new Object[] {
               P008W6_A40003GXC4
               }
               , new Object[] {
               P008W8_A40004GXC5
               }
               , new Object[] {
               P008W10_A40005GXC6
               }
               , new Object[] {
               P008W12_A40006GXC7
               }
               , new Object[] {
               P008W14_A40007GXC8
               }
               , new Object[] {
               P008W16_A40008GXC9
               }
               , new Object[] {
               P008W18_A40009GXC10
               }
               , new Object[] {
               P008W20_A1620ContagemResultado_CntSrvUndCnt, P008W20_n1620ContagemResultado_CntSrvUndCnt, P008W20_A456ContagemResultado_Codigo, P008W20_A597ContagemResultado_LoteAceiteCod, P008W20_n597ContagemResultado_LoteAceiteCod, P008W20_A40001ContagemResultado_ContratadaLogo_GXI, P008W20_n40001ContagemResultado_ContratadaLogo_GXI, P008W20_A490ContagemResultado_ContratadaCod, P008W20_n490ContagemResultado_ContratadaCod, P008W20_A601ContagemResultado_Servico,
               P008W20_n601ContagemResultado_Servico, P008W20_A1603ContagemResultado_CntCod, P008W20_n1603ContagemResultado_CntCod, P008W20_A1553ContagemResultado_CntSrvCod, P008W20_n1553ContagemResultado_CntSrvCod, P008W20_A1621ContagemResultado_CntSrvUndCntSgl, P008W20_n1621ContagemResultado_CntSrvUndCntSgl, P008W20_A803ContagemResultado_ContratadaSigla, P008W20_n803ContagemResultado_ContratadaSigla, P008W20_A566ContagemResultado_DataUltCnt,
               P008W20_n566ContagemResultado_DataUltCnt, P008W20_A1816ContagemResultado_ContratadaLogo, P008W20_n1816ContagemResultado_ContratadaLogo
               }
               , new Object[] {
               P008W23_A563Lote_Nome, P008W23_A562Lote_Numero, P008W23_A2055Lote_CntUsado, P008W23_n2055Lote_CntUsado, P008W23_A2056Lote_CntSaldo, P008W23_n2056Lote_CntSaldo, P008W23_A596Lote_Codigo, P008W23_A567Lote_DataIni, P008W23_A568Lote_DataFim
               }
               , new Object[] {
               P008W24_A484ContagemResultado_StatusDmn, P008W24_n484ContagemResultado_StatusDmn, P008W24_A1854ContagemResultado_VlrCnc, P008W24_n1854ContagemResultado_VlrCnc, P008W24_A512ContagemResultado_ValorPF, P008W24_n512ContagemResultado_ValorPF, P008W24_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P008W25_A456ContagemResultado_Codigo, P008W25_A493ContagemResultado_DemandaFM, P008W25_n493ContagemResultado_DemandaFM
               }
               , new Object[] {
               P008W26_A456ContagemResultado_Codigo, P008W26_A493ContagemResultado_DemandaFM, P008W26_n493ContagemResultado_DemandaFM
               }
               , new Object[] {
               P008W28_A489ContagemResultado_SistemaCod, P008W28_n489ContagemResultado_SistemaCod, P008W28_A1553ContagemResultado_CntSrvCod, P008W28_n1553ContagemResultado_CntSrvCod, P008W28_A484ContagemResultado_StatusDmn, P008W28_n484ContagemResultado_StatusDmn, P008W28_A1854ContagemResultado_VlrCnc, P008W28_n1854ContagemResultado_VlrCnc, P008W28_A494ContagemResultado_Descricao, P008W28_n494ContagemResultado_Descricao,
               P008W28_A490ContagemResultado_ContratadaCod, P008W28_n490ContagemResultado_ContratadaCod, P008W28_A601ContagemResultado_Servico, P008W28_n601ContagemResultado_Servico, P008W28_A1237ContagemResultado_PrazoMaisDias, P008W28_n1237ContagemResultado_PrazoMaisDias, P008W28_A1227ContagemResultado_PrazoInicialDias, P008W28_n1227ContagemResultado_PrazoInicialDias, P008W28_A2017ContagemResultado_DataEntregaReal, P008W28_n2017ContagemResultado_DataEntregaReal,
               P008W28_A801ContagemResultado_ServicoSigla, P008W28_n801ContagemResultado_ServicoSigla, P008W28_A509ContagemrResultado_SistemaSigla, P008W28_n509ContagemrResultado_SistemaSigla, P008W28_A457ContagemResultado_Demanda, P008W28_n457ContagemResultado_Demanda, P008W28_A2008ContagemResultado_CntSrvAls, P008W28_n2008ContagemResultado_CntSrvAls, P008W28_A471ContagemResultado_DataDmn, P008W28_A1051ContagemResultado_GlsValor,
               P008W28_n1051ContagemResultado_GlsValor, P008W28_A1050ContagemResultado_GlsDescricao, P008W28_n1050ContagemResultado_GlsDescricao, P008W28_A566ContagemResultado_DataUltCnt, P008W28_n566ContagemResultado_DataUltCnt, P008W28_A456ContagemResultado_Codigo, P008W28_A512ContagemResultado_ValorPF, P008W28_n512ContagemResultado_ValorPF
               }
               , new Object[] {
               P008W29_A597ContagemResultado_LoteAceiteCod, P008W29_n597ContagemResultado_LoteAceiteCod, P008W29_A484ContagemResultado_StatusDmn, P008W29_n484ContagemResultado_StatusDmn, P008W29_A1854ContagemResultado_VlrCnc, P008W29_n1854ContagemResultado_VlrCnc, P008W29_A456ContagemResultado_Codigo, P008W29_A512ContagemResultado_ValorPF, P008W29_n512ContagemResultado_ValorPF
               }
               , new Object[] {
               P008W30_A597ContagemResultado_LoteAceiteCod, P008W30_n597ContagemResultado_LoteAceiteCod, P008W30_A456ContagemResultado_Codigo, P008W30_A493ContagemResultado_DemandaFM, P008W30_n493ContagemResultado_DemandaFM
               }
               , new Object[] {
               P008W31_A597ContagemResultado_LoteAceiteCod, P008W31_n597ContagemResultado_LoteAceiteCod, P008W31_A456ContagemResultado_Codigo, P008W31_A493ContagemResultado_DemandaFM, P008W31_n493ContagemResultado_DemandaFM
               }
               , new Object[] {
               P008W33_A489ContagemResultado_SistemaCod, P008W33_n489ContagemResultado_SistemaCod, P008W33_A1553ContagemResultado_CntSrvCod, P008W33_n1553ContagemResultado_CntSrvCod, P008W33_A484ContagemResultado_StatusDmn, P008W33_n484ContagemResultado_StatusDmn, P008W33_A1854ContagemResultado_VlrCnc, P008W33_n1854ContagemResultado_VlrCnc, P008W33_A494ContagemResultado_Descricao, P008W33_n494ContagemResultado_Descricao,
               P008W33_A490ContagemResultado_ContratadaCod, P008W33_n490ContagemResultado_ContratadaCod, P008W33_A601ContagemResultado_Servico, P008W33_n601ContagemResultado_Servico, P008W33_A1237ContagemResultado_PrazoMaisDias, P008W33_n1237ContagemResultado_PrazoMaisDias, P008W33_A1227ContagemResultado_PrazoInicialDias, P008W33_n1227ContagemResultado_PrazoInicialDias, P008W33_A2017ContagemResultado_DataEntregaReal, P008W33_n2017ContagemResultado_DataEntregaReal,
               P008W33_A801ContagemResultado_ServicoSigla, P008W33_n801ContagemResultado_ServicoSigla, P008W33_A509ContagemrResultado_SistemaSigla, P008W33_n509ContagemrResultado_SistemaSigla, P008W33_A457ContagemResultado_Demanda, P008W33_n457ContagemResultado_Demanda, P008W33_A2008ContagemResultado_CntSrvAls, P008W33_n2008ContagemResultado_CntSrvAls, P008W33_A471ContagemResultado_DataDmn, P008W33_A1051ContagemResultado_GlsValor,
               P008W33_n1051ContagemResultado_GlsValor, P008W33_A1050ContagemResultado_GlsDescricao, P008W33_n1050ContagemResultado_GlsDescricao, P008W33_A566ContagemResultado_DataUltCnt, P008W33_n566ContagemResultado_DataUltCnt, P008W33_A456ContagemResultado_Codigo, P008W33_A512ContagemResultado_ValorPF, P008W33_n512ContagemResultado_ValorPF
               }
               , new Object[] {
               P008W35_A489ContagemResultado_SistemaCod, P008W35_n489ContagemResultado_SistemaCod, P008W35_A1553ContagemResultado_CntSrvCod, P008W35_n1553ContagemResultado_CntSrvCod, P008W35_A493ContagemResultado_DemandaFM, P008W35_n493ContagemResultado_DemandaFM, P008W35_A597ContagemResultado_LoteAceiteCod, P008W35_n597ContagemResultado_LoteAceiteCod, P008W35_A484ContagemResultado_StatusDmn, P008W35_n484ContagemResultado_StatusDmn,
               P008W35_A1854ContagemResultado_VlrCnc, P008W35_n1854ContagemResultado_VlrCnc, P008W35_A494ContagemResultado_Descricao, P008W35_n494ContagemResultado_Descricao, P008W35_A490ContagemResultado_ContratadaCod, P008W35_n490ContagemResultado_ContratadaCod, P008W35_A601ContagemResultado_Servico, P008W35_n601ContagemResultado_Servico, P008W35_A1237ContagemResultado_PrazoMaisDias, P008W35_n1237ContagemResultado_PrazoMaisDias,
               P008W35_A1227ContagemResultado_PrazoInicialDias, P008W35_n1227ContagemResultado_PrazoInicialDias, P008W35_A2017ContagemResultado_DataEntregaReal, P008W35_n2017ContagemResultado_DataEntregaReal, P008W35_A457ContagemResultado_Demanda, P008W35_n457ContagemResultado_Demanda, P008W35_A509ContagemrResultado_SistemaSigla, P008W35_n509ContagemrResultado_SistemaSigla, P008W35_A2008ContagemResultado_CntSrvAls, P008W35_n2008ContagemResultado_CntSrvAls,
               P008W35_A471ContagemResultado_DataDmn, P008W35_A1051ContagemResultado_GlsValor, P008W35_n1051ContagemResultado_GlsValor, P008W35_A1050ContagemResultado_GlsDescricao, P008W35_n1050ContagemResultado_GlsDescricao, P008W35_A566ContagemResultado_DataUltCnt, P008W35_n566ContagemResultado_DataUltCnt, P008W35_A456ContagemResultado_Codigo, P008W35_A512ContagemResultado_ValorPF, P008W35_n512ContagemResultado_ValorPF
               }
               , new Object[] {
               P008W36_A1404ContagemResultadoExecucao_OSCod, P008W36_A1405ContagemResultadoExecucao_Codigo, P008W36_A1406ContagemResultadoExecucao_Inicio
               }
               , new Object[] {
               P008W37_A40Contratada_PessoaCod, P008W37_A503Pessoa_MunicipioCod, P008W37_n503Pessoa_MunicipioCod, P008W37_A52Contratada_AreaTrabalhoCod, P008W37_A1595Contratada_AreaTrbSrvPdr, P008W37_n1595Contratada_AreaTrbSrvPdr, P008W37_A633Servico_UO, P008W37_n633Servico_UO, P008W37_A39Contratada_Codigo, P008W37_A42Contratada_PessoaCNPJ,
               P008W37_n42Contratada_PessoaCNPJ, P008W37_A521Pessoa_CEP, P008W37_n521Pessoa_CEP, P008W37_A23Estado_UF, P008W37_n23Estado_UF, P008W37_A26Municipio_Nome, P008W37_n26Municipio_Nome, P008W37_A519Pessoa_Endereco, P008W37_n519Pessoa_Endereco
               }
               , new Object[] {
               P008W38_A39Contratada_Codigo, P008W38_A52Contratada_AreaTrabalhoCod, P008W38_A1013Contrato_PrepostoCod, P008W38_n1013Contrato_PrepostoCod, P008W38_A1016Contrato_PrepostoPesCod, P008W38_n1016Contrato_PrepostoPesCod, P008W38_A29Contratante_Codigo, P008W38_n29Contratante_Codigo, P008W38_A74Contrato_Codigo, P008W38_A77Contrato_Numero,
               P008W38_A1015Contrato_PrepostoNom, P008W38_n1015Contrato_PrepostoNom, P008W38_A81Contrato_Quantidade, P008W38_A2034Contratante_TtlRltGerencial, P008W38_n2034Contratante_TtlRltGerencial, P008W38_A1354Contrato_PrdFtrCada, P008W38_n1354Contrato_PrdFtrCada, P008W38_A1357Contrato_PrdFtrIni, P008W38_n1357Contrato_PrdFtrIni, P008W38_A1358Contrato_PrdFtrFim,
               P008W38_n1358Contrato_PrdFtrFim
               }
               , new Object[] {
               P008W39_A1270ContratoServicosIndicador_CntSrvCod, P008W39_A1308ContratoServicosIndicador_Tipo, P008W39_n1308ContratoServicosIndicador_Tipo, P008W39_A1269ContratoServicosIndicador_Codigo, P008W39_A1274ContratoServicosIndicador_Indicador, P008W39_A1271ContratoServicosIndicador_Numero
               }
               , new Object[] {
               P008W40_A1269ContratoServicosIndicador_Codigo, P008W40_A1303ContratoServicosIndicadorFaixa_Reduz, P008W40_A1302ContratoServicosIndicadorFaixa_Ate, P008W40_A1301ContratoServicosIndicadorFaixa_Desde, P008W40_A1300ContratoServicosIndicadorFaixa_Numero, P008W40_A1299ContratoServicosIndicadorFaixa_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         Gx_line = 0;
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short GxWebError ;
      private short AV114DescricaoLength ;
      private short AV113DescricaoGlsLength ;
      private short AV148Quantidade ;
      private short AV165Previstas ;
      private short AV167Atrasadas ;
      private short AV166Cumpridas ;
      private short AV215Linhas ;
      private short A1237ContagemResultado_PrazoMaisDias ;
      private short A1227ContagemResultado_PrazoInicialDias ;
      private short AV133i ;
      private short AV145Prazo ;
      private short AV143p ;
      private short AV144p2 ;
      private short AV246Count ;
      private short AV250Indicador ;
      private short AV249Faixa ;
      private short A1357Contrato_PrdFtrIni ;
      private short A1358Contrato_PrdFtrFim ;
      private short GXt_int4 ;
      private short A1271ContratoServicosIndicador_Numero ;
      private short A1300ContratoServicosIndicadorFaixa_Numero ;
      private int AV47ContagemResultado_LoteAceite ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int AV11Codigo ;
      private int A146Modulo_Codigo ;
      private int A127Sistema_Codigo ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A1620ContagemResultado_CntSrvUndCnt ;
      private int A29Contratante_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A601ContagemResultado_Servico ;
      private int A1603ContagemResultado_CntCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int AV105Contratada ;
      private int AV212Servico ;
      private int AV228Contrato_Codigo ;
      private int AV232ContratoServicos_Codigo ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A1270ContratoServicosIndicador_CntSrvCod ;
      private int A596Lote_Codigo ;
      private int Gx_OldLine ;
      private int AV211ContagemResultado_Codigo ;
      private int A489ContagemResultado_SistemaCod ;
      private int A1404ContagemResultadoExecucao_OSCod ;
      private int A1405ContagemResultadoExecucao_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int A503Pessoa_MunicipioCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A1595Contratada_AreaTrbSrvPdr ;
      private int A633Servico_UO ;
      private int A39Contratada_Codigo ;
      private int A1013Contrato_PrepostoCod ;
      private int A1016Contrato_PrepostoPesCod ;
      private int A74Contrato_Codigo ;
      private int A81Contrato_Quantidade ;
      private int GXt_int2 ;
      private int A1269ContratoServicosIndicador_Codigo ;
      private int A1299ContratoServicosIndicadorFaixa_Codigo ;
      private decimal AV242Usado ;
      private decimal AV244Saldo ;
      private decimal AV243Contratado ;
      private decimal A2055Lote_CntUsado ;
      private decimal A2056Lote_CntSaldo ;
      private decimal AV108d ;
      private decimal A1854ContagemResultado_VlrCnc ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal A606ContagemResultado_ValorFinal ;
      private decimal AV71ContagemResultado_PFTotal ;
      private decimal AV72ContagemResultado_PFTotalFaturar ;
      private decimal AV245SaldoPrev ;
      private decimal AV168PntLoteIndice ;
      private decimal AV224PntLotePrcSemAtraso ;
      private decimal AV170PntLoteSancao ;
      private decimal AV171PntLoteLiquido ;
      private decimal AV149Reduz ;
      private decimal AV223PntLoteUndBruto ;
      private decimal AV161TotalPontos ;
      private decimal AV160TotalFinal ;
      private decimal A1051ContagemResultado_GlsValor ;
      private decimal AV70ContagemResultado_PFFaturar ;
      private decimal AV128GlsPF ;
      private decimal AV129GlsValor ;
      private decimal AV162ValorPF ;
      private decimal AV169PntLoteBaseCalculo ;
      private decimal AV159TotalFaturar ;
      private decimal AV135IndP ;
      private decimal GXt_decimal3 ;
      private decimal A1303ContratoServicosIndicadorFaixa_Reduz ;
      private decimal A1302ContratoServicosIndicadorFaixa_Ate ;
      private decimal A1301ContratoServicosIndicadorFaixa_Desde ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV236Filtro ;
      private String AV221ContratanteArea ;
      private String AV235Periodo ;
      private String AV157SubTitulo ;
      private String scmdbuf ;
      private String A1621ContagemResultado_CntSrvUndCntSgl ;
      private String A803ContagemResultado_ContratadaSigla ;
      private String AV220UnidadeMedicao_Sigla ;
      private String AV234Contratada_Sigla ;
      private String AV251ContratoServicosIndicador_Tipo ;
      private String A1308ContratoServicosIndicador_Tipo ;
      private String A563Lote_Nome ;
      private String A562Lote_Numero ;
      private String AV138NumeroRelatorio ;
      private String A484ContagemResultado_StatusDmn ;
      private String AV9Contrato_Numero ;
      private String AV141Os ;
      private String AV225Preposto ;
      private String AV226CNPJ ;
      private String AV172PntLoteIndicador ;
      private String A801ContagemResultado_ServicoSigla ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String A2008ContagemResultado_CntSrvAls ;
      private String AV216GlsTxt ;
      private String AV154strPFFinal ;
      private String AV112Descricao ;
      private String AV217StrGlsValor ;
      private String AV155strPFTotal ;
      private String AV156strPFTotalFaturar ;
      private String AV248Indicador_Sigla ;
      private String A521Pessoa_CEP ;
      private String A23Estado_UF ;
      private String A26Municipio_Nome ;
      private String AV238Endereco ;
      private String A77Contrato_Numero ;
      private String A1015Contrato_PrepostoNom ;
      private String A1354Contrato_PrdFtrCada ;
      private String GXt_char6 ;
      private String AV252CalculoSob ;
      private DateTime AV219Hoje ;
      private DateTime A40003GXC4 ;
      private DateTime A40005GXC6 ;
      private DateTime A40007GXC8 ;
      private DateTime A40009GXC10 ;
      private DateTime A2017ContagemResultado_DataEntregaReal ;
      private DateTime A1406ContagemResultadoExecucao_Inicio ;
      private DateTime GXt_dtime5 ;
      private DateTime AV109Data ;
      private DateTime AV136Inicio ;
      private DateTime AV237Fim ;
      private DateTime AV111DataInicio ;
      private DateTime AV110DataFim ;
      private DateTime AV173PrdFtrIni ;
      private DateTime AV174PrdFtrFim ;
      private DateTime A40002ContagemResultado_DataUltCnt ;
      private DateTime A40004GXC5 ;
      private DateTime A40006GXC7 ;
      private DateTime A40008GXC9 ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private DateTime A567Lote_DataIni ;
      private DateTime A568Lote_DataFim ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime AV213DataCnt ;
      private DateTime AV126Entregue ;
      private DateTime AV153Solicitada ;
      private DateTime AV146Previsto ;
      private bool entryPointCalled ;
      private bool AV142OSAutomatica ;
      private bool GXt_boolean1 ;
      private bool n146Modulo_Codigo ;
      private bool n1620ContagemResultado_CntSrvUndCnt ;
      private bool n29Contratante_Codigo ;
      private bool n40001ContagemResultado_ContratadaLogo_GXI ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1621ContagemResultado_CntSrvUndCntSgl ;
      private bool n803ContagemResultado_ContratadaSigla ;
      private bool n2034Contratante_TtlRltGerencial ;
      private bool n1816ContagemResultado_ContratadaLogo ;
      private bool returnInSub ;
      private bool A517ContagemResultado_Ultima ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n566ContagemResultado_DataUltCnt ;
      private bool n2055Lote_CntUsado ;
      private bool n2056Lote_CntSaldo ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n1854ContagemResultado_VlrCnc ;
      private bool n512ContagemResultado_ValorPF ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool AV229SemOSFM ;
      private bool AV175TemIndicadorPntLote ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n494ContagemResultado_Descricao ;
      private bool n1237ContagemResultado_PrazoMaisDias ;
      private bool n1227ContagemResultado_PrazoInicialDias ;
      private bool n2017ContagemResultado_DataEntregaReal ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n457ContagemResultado_Demanda ;
      private bool n2008ContagemResultado_CntSrvAls ;
      private bool n1051ContagemResultado_GlsValor ;
      private bool n1050ContagemResultado_GlsDescricao ;
      private bool AV158TemIndicadorDePontoalidade ;
      private bool AV247TemIndicadorDeFrequencia ;
      private bool n503Pessoa_MunicipioCod ;
      private bool n1595Contratada_AreaTrbSrvPdr ;
      private bool n633Servico_UO ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n521Pessoa_CEP ;
      private bool n23Estado_UF ;
      private bool n26Municipio_Nome ;
      private bool n519Pessoa_Endereco ;
      private bool n1013Contrato_PrepostoCod ;
      private bool n1016Contrato_PrepostoPesCod ;
      private bool n1015Contrato_PrepostoNom ;
      private bool n1354Contrato_PrdFtrCada ;
      private bool n1357Contrato_PrdFtrIni ;
      private bool n1358Contrato_PrdFtrFim ;
      private bool n1308ContratoServicosIndicador_Tipo ;
      private String A1050ContagemResultado_GlsDescricao ;
      private String AV127GlsDescricao ;
      private String A1274ContratoServicosIndicador_Indicador ;
      private String A40001ContagemResultado_ContratadaLogo_GXI ;
      private String A2034Contratante_TtlRltGerencial ;
      private String OV241Contratante_TtlRltGerencial ;
      private String AV241Contratante_TtlRltGerencial ;
      private String A40000Contratada_Logo_GXI ;
      private String A493ContagemResultado_DemandaFM ;
      private String AV46ContagemResultado_DemandaFM ;
      private String A494ContagemResultado_Descricao ;
      private String A457ContagemResultado_Demanda ;
      private String AV116DmnDescricao ;
      private String A42Contratada_PessoaCNPJ ;
      private String A519Pessoa_Endereco ;
      private String A1816ContagemResultado_ContratadaLogo ;
      private String AV233Contratada_Logo ;
      private String Contratada_logo ;
      private IGxSession AV163WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P008W2_A146Modulo_Codigo ;
      private bool[] P008W2_n146Modulo_Codigo ;
      private int[] P008W2_A127Sistema_Codigo ;
      private int[] P008W2_A135Sistema_AreaTrabalhoCod ;
      private int[] P008W2_A1620ContagemResultado_CntSrvUndCnt ;
      private bool[] P008W2_n1620ContagemResultado_CntSrvUndCnt ;
      private int[] P008W2_A29Contratante_Codigo ;
      private bool[] P008W2_n29Contratante_Codigo ;
      private int[] P008W2_A456ContagemResultado_Codigo ;
      private String[] P008W2_A40001ContagemResultado_ContratadaLogo_GXI ;
      private bool[] P008W2_n40001ContagemResultado_ContratadaLogo_GXI ;
      private int[] P008W2_A490ContagemResultado_ContratadaCod ;
      private bool[] P008W2_n490ContagemResultado_ContratadaCod ;
      private int[] P008W2_A601ContagemResultado_Servico ;
      private bool[] P008W2_n601ContagemResultado_Servico ;
      private int[] P008W2_A1603ContagemResultado_CntCod ;
      private bool[] P008W2_n1603ContagemResultado_CntCod ;
      private int[] P008W2_A1553ContagemResultado_CntSrvCod ;
      private bool[] P008W2_n1553ContagemResultado_CntSrvCod ;
      private String[] P008W2_A1621ContagemResultado_CntSrvUndCntSgl ;
      private bool[] P008W2_n1621ContagemResultado_CntSrvUndCntSgl ;
      private String[] P008W2_A803ContagemResultado_ContratadaSigla ;
      private bool[] P008W2_n803ContagemResultado_ContratadaSigla ;
      private String[] P008W2_A2034Contratante_TtlRltGerencial ;
      private bool[] P008W2_n2034Contratante_TtlRltGerencial ;
      private String[] P008W2_A1816ContagemResultado_ContratadaLogo ;
      private bool[] P008W2_n1816ContagemResultado_ContratadaLogo ;
      private DateTime[] P008W4_A40002ContagemResultado_DataUltCnt ;
      private DateTime[] P008W6_A40003GXC4 ;
      private DateTime[] P008W8_A40004GXC5 ;
      private DateTime[] P008W10_A40005GXC6 ;
      private DateTime[] P008W12_A40006GXC7 ;
      private DateTime[] P008W14_A40007GXC8 ;
      private DateTime[] P008W16_A40008GXC9 ;
      private DateTime[] P008W18_A40009GXC10 ;
      private int[] P008W20_A1620ContagemResultado_CntSrvUndCnt ;
      private bool[] P008W20_n1620ContagemResultado_CntSrvUndCnt ;
      private int[] P008W20_A456ContagemResultado_Codigo ;
      private int[] P008W20_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P008W20_n597ContagemResultado_LoteAceiteCod ;
      private String[] P008W20_A40001ContagemResultado_ContratadaLogo_GXI ;
      private bool[] P008W20_n40001ContagemResultado_ContratadaLogo_GXI ;
      private int[] P008W20_A490ContagemResultado_ContratadaCod ;
      private bool[] P008W20_n490ContagemResultado_ContratadaCod ;
      private int[] P008W20_A601ContagemResultado_Servico ;
      private bool[] P008W20_n601ContagemResultado_Servico ;
      private int[] P008W20_A1603ContagemResultado_CntCod ;
      private bool[] P008W20_n1603ContagemResultado_CntCod ;
      private int[] P008W20_A1553ContagemResultado_CntSrvCod ;
      private bool[] P008W20_n1553ContagemResultado_CntSrvCod ;
      private String[] P008W20_A1621ContagemResultado_CntSrvUndCntSgl ;
      private bool[] P008W20_n1621ContagemResultado_CntSrvUndCntSgl ;
      private String[] P008W20_A803ContagemResultado_ContratadaSigla ;
      private bool[] P008W20_n803ContagemResultado_ContratadaSigla ;
      private DateTime[] P008W20_A566ContagemResultado_DataUltCnt ;
      private bool[] P008W20_n566ContagemResultado_DataUltCnt ;
      private String[] P008W20_A1816ContagemResultado_ContratadaLogo ;
      private bool[] P008W20_n1816ContagemResultado_ContratadaLogo ;
      private String[] P008W23_A563Lote_Nome ;
      private String[] P008W23_A562Lote_Numero ;
      private decimal[] P008W23_A2055Lote_CntUsado ;
      private bool[] P008W23_n2055Lote_CntUsado ;
      private decimal[] P008W23_A2056Lote_CntSaldo ;
      private bool[] P008W23_n2056Lote_CntSaldo ;
      private int[] P008W23_A596Lote_Codigo ;
      private DateTime[] P008W23_A567Lote_DataIni ;
      private DateTime[] P008W23_A568Lote_DataFim ;
      private String[] P008W24_A484ContagemResultado_StatusDmn ;
      private bool[] P008W24_n484ContagemResultado_StatusDmn ;
      private decimal[] P008W24_A1854ContagemResultado_VlrCnc ;
      private bool[] P008W24_n1854ContagemResultado_VlrCnc ;
      private decimal[] P008W24_A512ContagemResultado_ValorPF ;
      private bool[] P008W24_n512ContagemResultado_ValorPF ;
      private int[] P008W24_A456ContagemResultado_Codigo ;
      private int[] P008W25_A456ContagemResultado_Codigo ;
      private String[] P008W25_A493ContagemResultado_DemandaFM ;
      private bool[] P008W25_n493ContagemResultado_DemandaFM ;
      private int[] P008W26_A456ContagemResultado_Codigo ;
      private String[] P008W26_A493ContagemResultado_DemandaFM ;
      private bool[] P008W26_n493ContagemResultado_DemandaFM ;
      private int[] P008W28_A489ContagemResultado_SistemaCod ;
      private bool[] P008W28_n489ContagemResultado_SistemaCod ;
      private int[] P008W28_A1553ContagemResultado_CntSrvCod ;
      private bool[] P008W28_n1553ContagemResultado_CntSrvCod ;
      private String[] P008W28_A484ContagemResultado_StatusDmn ;
      private bool[] P008W28_n484ContagemResultado_StatusDmn ;
      private decimal[] P008W28_A1854ContagemResultado_VlrCnc ;
      private bool[] P008W28_n1854ContagemResultado_VlrCnc ;
      private String[] P008W28_A494ContagemResultado_Descricao ;
      private bool[] P008W28_n494ContagemResultado_Descricao ;
      private int[] P008W28_A490ContagemResultado_ContratadaCod ;
      private bool[] P008W28_n490ContagemResultado_ContratadaCod ;
      private int[] P008W28_A601ContagemResultado_Servico ;
      private bool[] P008W28_n601ContagemResultado_Servico ;
      private short[] P008W28_A1237ContagemResultado_PrazoMaisDias ;
      private bool[] P008W28_n1237ContagemResultado_PrazoMaisDias ;
      private short[] P008W28_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] P008W28_n1227ContagemResultado_PrazoInicialDias ;
      private DateTime[] P008W28_A2017ContagemResultado_DataEntregaReal ;
      private bool[] P008W28_n2017ContagemResultado_DataEntregaReal ;
      private String[] P008W28_A801ContagemResultado_ServicoSigla ;
      private bool[] P008W28_n801ContagemResultado_ServicoSigla ;
      private String[] P008W28_A509ContagemrResultado_SistemaSigla ;
      private bool[] P008W28_n509ContagemrResultado_SistemaSigla ;
      private String[] P008W28_A457ContagemResultado_Demanda ;
      private bool[] P008W28_n457ContagemResultado_Demanda ;
      private String[] P008W28_A2008ContagemResultado_CntSrvAls ;
      private bool[] P008W28_n2008ContagemResultado_CntSrvAls ;
      private DateTime[] P008W28_A471ContagemResultado_DataDmn ;
      private decimal[] P008W28_A1051ContagemResultado_GlsValor ;
      private bool[] P008W28_n1051ContagemResultado_GlsValor ;
      private String[] P008W28_A1050ContagemResultado_GlsDescricao ;
      private bool[] P008W28_n1050ContagemResultado_GlsDescricao ;
      private DateTime[] P008W28_A566ContagemResultado_DataUltCnt ;
      private bool[] P008W28_n566ContagemResultado_DataUltCnt ;
      private int[] P008W28_A456ContagemResultado_Codigo ;
      private decimal[] P008W28_A512ContagemResultado_ValorPF ;
      private bool[] P008W28_n512ContagemResultado_ValorPF ;
      private int[] P008W29_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P008W29_n597ContagemResultado_LoteAceiteCod ;
      private String[] P008W29_A484ContagemResultado_StatusDmn ;
      private bool[] P008W29_n484ContagemResultado_StatusDmn ;
      private decimal[] P008W29_A1854ContagemResultado_VlrCnc ;
      private bool[] P008W29_n1854ContagemResultado_VlrCnc ;
      private int[] P008W29_A456ContagemResultado_Codigo ;
      private decimal[] P008W29_A512ContagemResultado_ValorPF ;
      private bool[] P008W29_n512ContagemResultado_ValorPF ;
      private int[] P008W30_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P008W30_n597ContagemResultado_LoteAceiteCod ;
      private int[] P008W30_A456ContagemResultado_Codigo ;
      private String[] P008W30_A493ContagemResultado_DemandaFM ;
      private bool[] P008W30_n493ContagemResultado_DemandaFM ;
      private int[] P008W31_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P008W31_n597ContagemResultado_LoteAceiteCod ;
      private int[] P008W31_A456ContagemResultado_Codigo ;
      private String[] P008W31_A493ContagemResultado_DemandaFM ;
      private bool[] P008W31_n493ContagemResultado_DemandaFM ;
      private int[] P008W33_A489ContagemResultado_SistemaCod ;
      private bool[] P008W33_n489ContagemResultado_SistemaCod ;
      private int[] P008W33_A1553ContagemResultado_CntSrvCod ;
      private bool[] P008W33_n1553ContagemResultado_CntSrvCod ;
      private String[] P008W33_A484ContagemResultado_StatusDmn ;
      private bool[] P008W33_n484ContagemResultado_StatusDmn ;
      private decimal[] P008W33_A1854ContagemResultado_VlrCnc ;
      private bool[] P008W33_n1854ContagemResultado_VlrCnc ;
      private String[] P008W33_A494ContagemResultado_Descricao ;
      private bool[] P008W33_n494ContagemResultado_Descricao ;
      private int[] P008W33_A490ContagemResultado_ContratadaCod ;
      private bool[] P008W33_n490ContagemResultado_ContratadaCod ;
      private int[] P008W33_A601ContagemResultado_Servico ;
      private bool[] P008W33_n601ContagemResultado_Servico ;
      private short[] P008W33_A1237ContagemResultado_PrazoMaisDias ;
      private bool[] P008W33_n1237ContagemResultado_PrazoMaisDias ;
      private short[] P008W33_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] P008W33_n1227ContagemResultado_PrazoInicialDias ;
      private DateTime[] P008W33_A2017ContagemResultado_DataEntregaReal ;
      private bool[] P008W33_n2017ContagemResultado_DataEntregaReal ;
      private String[] P008W33_A801ContagemResultado_ServicoSigla ;
      private bool[] P008W33_n801ContagemResultado_ServicoSigla ;
      private String[] P008W33_A509ContagemrResultado_SistemaSigla ;
      private bool[] P008W33_n509ContagemrResultado_SistemaSigla ;
      private String[] P008W33_A457ContagemResultado_Demanda ;
      private bool[] P008W33_n457ContagemResultado_Demanda ;
      private String[] P008W33_A2008ContagemResultado_CntSrvAls ;
      private bool[] P008W33_n2008ContagemResultado_CntSrvAls ;
      private DateTime[] P008W33_A471ContagemResultado_DataDmn ;
      private decimal[] P008W33_A1051ContagemResultado_GlsValor ;
      private bool[] P008W33_n1051ContagemResultado_GlsValor ;
      private String[] P008W33_A1050ContagemResultado_GlsDescricao ;
      private bool[] P008W33_n1050ContagemResultado_GlsDescricao ;
      private DateTime[] P008W33_A566ContagemResultado_DataUltCnt ;
      private bool[] P008W33_n566ContagemResultado_DataUltCnt ;
      private int[] P008W33_A456ContagemResultado_Codigo ;
      private decimal[] P008W33_A512ContagemResultado_ValorPF ;
      private bool[] P008W33_n512ContagemResultado_ValorPF ;
      private int[] P008W35_A489ContagemResultado_SistemaCod ;
      private bool[] P008W35_n489ContagemResultado_SistemaCod ;
      private int[] P008W35_A1553ContagemResultado_CntSrvCod ;
      private bool[] P008W35_n1553ContagemResultado_CntSrvCod ;
      private String[] P008W35_A493ContagemResultado_DemandaFM ;
      private bool[] P008W35_n493ContagemResultado_DemandaFM ;
      private int[] P008W35_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P008W35_n597ContagemResultado_LoteAceiteCod ;
      private String[] P008W35_A484ContagemResultado_StatusDmn ;
      private bool[] P008W35_n484ContagemResultado_StatusDmn ;
      private decimal[] P008W35_A1854ContagemResultado_VlrCnc ;
      private bool[] P008W35_n1854ContagemResultado_VlrCnc ;
      private String[] P008W35_A494ContagemResultado_Descricao ;
      private bool[] P008W35_n494ContagemResultado_Descricao ;
      private int[] P008W35_A490ContagemResultado_ContratadaCod ;
      private bool[] P008W35_n490ContagemResultado_ContratadaCod ;
      private int[] P008W35_A601ContagemResultado_Servico ;
      private bool[] P008W35_n601ContagemResultado_Servico ;
      private short[] P008W35_A1237ContagemResultado_PrazoMaisDias ;
      private bool[] P008W35_n1237ContagemResultado_PrazoMaisDias ;
      private short[] P008W35_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] P008W35_n1227ContagemResultado_PrazoInicialDias ;
      private DateTime[] P008W35_A2017ContagemResultado_DataEntregaReal ;
      private bool[] P008W35_n2017ContagemResultado_DataEntregaReal ;
      private String[] P008W35_A457ContagemResultado_Demanda ;
      private bool[] P008W35_n457ContagemResultado_Demanda ;
      private String[] P008W35_A509ContagemrResultado_SistemaSigla ;
      private bool[] P008W35_n509ContagemrResultado_SistemaSigla ;
      private String[] P008W35_A2008ContagemResultado_CntSrvAls ;
      private bool[] P008W35_n2008ContagemResultado_CntSrvAls ;
      private DateTime[] P008W35_A471ContagemResultado_DataDmn ;
      private decimal[] P008W35_A1051ContagemResultado_GlsValor ;
      private bool[] P008W35_n1051ContagemResultado_GlsValor ;
      private String[] P008W35_A1050ContagemResultado_GlsDescricao ;
      private bool[] P008W35_n1050ContagemResultado_GlsDescricao ;
      private DateTime[] P008W35_A566ContagemResultado_DataUltCnt ;
      private bool[] P008W35_n566ContagemResultado_DataUltCnt ;
      private int[] P008W35_A456ContagemResultado_Codigo ;
      private decimal[] P008W35_A512ContagemResultado_ValorPF ;
      private bool[] P008W35_n512ContagemResultado_ValorPF ;
      private int[] P008W36_A1404ContagemResultadoExecucao_OSCod ;
      private int[] P008W36_A1405ContagemResultadoExecucao_Codigo ;
      private DateTime[] P008W36_A1406ContagemResultadoExecucao_Inicio ;
      private int[] P008W37_A40Contratada_PessoaCod ;
      private int[] P008W37_A503Pessoa_MunicipioCod ;
      private bool[] P008W37_n503Pessoa_MunicipioCod ;
      private int[] P008W37_A52Contratada_AreaTrabalhoCod ;
      private int[] P008W37_A1595Contratada_AreaTrbSrvPdr ;
      private bool[] P008W37_n1595Contratada_AreaTrbSrvPdr ;
      private int[] P008W37_A633Servico_UO ;
      private bool[] P008W37_n633Servico_UO ;
      private int[] P008W37_A39Contratada_Codigo ;
      private String[] P008W37_A42Contratada_PessoaCNPJ ;
      private bool[] P008W37_n42Contratada_PessoaCNPJ ;
      private String[] P008W37_A521Pessoa_CEP ;
      private bool[] P008W37_n521Pessoa_CEP ;
      private String[] P008W37_A23Estado_UF ;
      private bool[] P008W37_n23Estado_UF ;
      private String[] P008W37_A26Municipio_Nome ;
      private bool[] P008W37_n26Municipio_Nome ;
      private String[] P008W37_A519Pessoa_Endereco ;
      private bool[] P008W37_n519Pessoa_Endereco ;
      private int[] P008W38_A39Contratada_Codigo ;
      private int[] P008W38_A52Contratada_AreaTrabalhoCod ;
      private int[] P008W38_A1013Contrato_PrepostoCod ;
      private bool[] P008W38_n1013Contrato_PrepostoCod ;
      private int[] P008W38_A1016Contrato_PrepostoPesCod ;
      private bool[] P008W38_n1016Contrato_PrepostoPesCod ;
      private int[] P008W38_A29Contratante_Codigo ;
      private bool[] P008W38_n29Contratante_Codigo ;
      private int[] P008W38_A74Contrato_Codigo ;
      private String[] P008W38_A77Contrato_Numero ;
      private String[] P008W38_A1015Contrato_PrepostoNom ;
      private bool[] P008W38_n1015Contrato_PrepostoNom ;
      private int[] P008W38_A81Contrato_Quantidade ;
      private String[] P008W38_A2034Contratante_TtlRltGerencial ;
      private bool[] P008W38_n2034Contratante_TtlRltGerencial ;
      private String[] P008W38_A1354Contrato_PrdFtrCada ;
      private bool[] P008W38_n1354Contrato_PrdFtrCada ;
      private short[] P008W38_A1357Contrato_PrdFtrIni ;
      private bool[] P008W38_n1357Contrato_PrdFtrIni ;
      private short[] P008W38_A1358Contrato_PrdFtrFim ;
      private bool[] P008W38_n1358Contrato_PrdFtrFim ;
      private int[] P008W39_A1270ContratoServicosIndicador_CntSrvCod ;
      private String[] P008W39_A1308ContratoServicosIndicador_Tipo ;
      private bool[] P008W39_n1308ContratoServicosIndicador_Tipo ;
      private int[] P008W39_A1269ContratoServicosIndicador_Codigo ;
      private String[] P008W39_A1274ContratoServicosIndicador_Indicador ;
      private short[] P008W39_A1271ContratoServicosIndicador_Numero ;
      private int[] P008W40_A1269ContratoServicosIndicador_Codigo ;
      private decimal[] P008W40_A1303ContratoServicosIndicadorFaixa_Reduz ;
      private decimal[] P008W40_A1302ContratoServicosIndicadorFaixa_Ate ;
      private decimal[] P008W40_A1301ContratoServicosIndicadorFaixa_Desde ;
      private short[] P008W40_A1300ContratoServicosIndicadorFaixa_Numero ;
      private int[] P008W40_A1299ContratoServicosIndicadorFaixa_Codigo ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV12Codigos ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV254SemCodigos ;
      private wwpbaseobjects.SdtWWPContext AV164WWPContext ;
   }

   public class arel_contagemgerencial__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P008W4( IGxContext context ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV12Codigos ,
                                             int A597ContagemResultado_LoteAceiteCod ,
                                             int AV47ContagemResultado_LoteAceite ,
                                             int AV232ContratoServicos_Codigo ,
                                             String AV251ContratoServicosIndicador_Tipo ,
                                             int A1270ContratoServicosIndicador_CntSrvCod ,
                                             String A1308ContratoServicosIndicador_Tipo ,
                                             bool A517ContagemResultado_Ultima )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object7 ;
         GXv_Object7 = new Object [2] ;
         String sRef1 ;
         sRef1 = (String)(new GxDbmsUtils( new GxSqlServer()).ValueList(AV12Codigos, "[ContagemResultado_Codigo]"+" IN (", ")"));
         scmdbuf = "SELECT COALESCE( T1.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS GXC3 FROM (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE ";
         scmdbuf = scmdbuf + sRef1;
         scmdbuf = scmdbuf + " and [ContagemResultado_Ultima] = 1 ) T1";
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + "";
         GXv_Object7[0] = scmdbuf;
         return GXv_Object7 ;
      }

      protected Object[] conditional_P008W6( IGxContext context ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV12Codigos ,
                                             int A597ContagemResultado_LoteAceiteCod ,
                                             int AV47ContagemResultado_LoteAceite ,
                                             int AV232ContratoServicos_Codigo ,
                                             String AV251ContratoServicosIndicador_Tipo ,
                                             int A1270ContratoServicosIndicador_CntSrvCod ,
                                             String A1308ContratoServicosIndicador_Tipo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object9 ;
         GXv_Object9 = new Object [2] ;
         String sRef1 ;
         sRef1 = (String)(new GxDbmsUtils( new GxSqlServer()).ValueList(AV12Codigos, "[ContagemResultado_Codigo]"+" IN (", ")"));
         scmdbuf = "SELECT COALESCE( T1.[GXC4], convert( DATETIME, '17530101', 112 )) AS GXC4 FROM (SELECT MIN([ContagemResultado_DataPrevista]) AS GXC4 FROM [ContagemResultado] WITH (NOLOCK) WHERE ";
         scmdbuf = scmdbuf + sRef1;
         scmdbuf = scmdbuf + " ) T1";
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + "";
         GXv_Object9[0] = scmdbuf;
         return GXv_Object9 ;
      }

      protected Object[] conditional_P008W8( IGxContext context ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV12Codigos ,
                                             int A597ContagemResultado_LoteAceiteCod ,
                                             int AV47ContagemResultado_LoteAceite ,
                                             int AV232ContratoServicos_Codigo ,
                                             String AV251ContratoServicosIndicador_Tipo ,
                                             int A1270ContratoServicosIndicador_CntSrvCod ,
                                             String A1308ContratoServicosIndicador_Tipo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object11 ;
         GXv_Object11 = new Object [2] ;
         String sRef1 ;
         sRef1 = (String)(new GxDbmsUtils( new GxSqlServer()).ValueList(AV12Codigos, "[ContagemResultado_Codigo]"+" IN (", ")"));
         scmdbuf = "SELECT COALESCE( T1.[GXC5], convert( DATETIME, '17530101', 112 )) AS GXC5 FROM (SELECT MIN([ContagemResultado_DataDmn]) AS GXC5 FROM [ContagemResultado] WITH (NOLOCK) WHERE ";
         scmdbuf = scmdbuf + sRef1;
         scmdbuf = scmdbuf + " ) T1";
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + "";
         GXv_Object11[0] = scmdbuf;
         return GXv_Object11 ;
      }

      protected Object[] conditional_P008W10( IGxContext context ,
                                              int A456ContagemResultado_Codigo ,
                                              IGxCollection AV12Codigos ,
                                              int A597ContagemResultado_LoteAceiteCod ,
                                              int AV47ContagemResultado_LoteAceite ,
                                              int AV232ContratoServicos_Codigo ,
                                              String AV251ContratoServicosIndicador_Tipo ,
                                              int A1270ContratoServicosIndicador_CntSrvCod ,
                                              String A1308ContratoServicosIndicador_Tipo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object13 ;
         GXv_Object13 = new Object [2] ;
         String sRef1 ;
         sRef1 = (String)(new GxDbmsUtils( new GxSqlServer()).ValueList(AV12Codigos, "[ContagemResultado_Codigo]"+" IN (", ")"));
         scmdbuf = "SELECT COALESCE( T1.[GXC6], convert( DATETIME, '17530101', 112 )) AS GXC6 FROM (SELECT MIN([ContagemResultado_DataHomologacao]) AS GXC6 FROM [ContagemResultado] WITH (NOLOCK) WHERE ";
         scmdbuf = scmdbuf + sRef1;
         scmdbuf = scmdbuf + " ) T1";
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + "";
         GXv_Object13[0] = scmdbuf;
         return GXv_Object13 ;
      }

      protected Object[] conditional_P008W12( IGxContext context ,
                                              int A456ContagemResultado_Codigo ,
                                              IGxCollection AV12Codigos ,
                                              int A597ContagemResultado_LoteAceiteCod ,
                                              int AV47ContagemResultado_LoteAceite ,
                                              int AV232ContratoServicos_Codigo ,
                                              String AV251ContratoServicosIndicador_Tipo ,
                                              int A1270ContratoServicosIndicador_CntSrvCod ,
                                              String A1308ContratoServicosIndicador_Tipo ,
                                              bool A517ContagemResultado_Ultima )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object15 ;
         GXv_Object15 = new Object [2] ;
         String sRef1 ;
         sRef1 = (String)(new GxDbmsUtils( new GxSqlServer()).ValueList(AV12Codigos, "[ContagemResultado_Codigo]"+" IN (", ")"));
         scmdbuf = "SELECT COALESCE( T1.[GXC7], convert( DATETIME, '17530101', 112 )) AS GXC7 FROM (SELECT MAX([ContagemResultado_DataCnt]) AS GXC7 FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE ";
         scmdbuf = scmdbuf + sRef1;
         scmdbuf = scmdbuf + " and [ContagemResultado_Ultima] = 1 ) T1";
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + "";
         GXv_Object15[0] = scmdbuf;
         return GXv_Object15 ;
      }

      protected Object[] conditional_P008W14( IGxContext context ,
                                              int A456ContagemResultado_Codigo ,
                                              IGxCollection AV12Codigos ,
                                              int A597ContagemResultado_LoteAceiteCod ,
                                              int AV47ContagemResultado_LoteAceite ,
                                              int AV232ContratoServicos_Codigo ,
                                              String AV251ContratoServicosIndicador_Tipo ,
                                              int A1270ContratoServicosIndicador_CntSrvCod ,
                                              String A1308ContratoServicosIndicador_Tipo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object17 ;
         GXv_Object17 = new Object [2] ;
         String sRef1 ;
         sRef1 = (String)(new GxDbmsUtils( new GxSqlServer()).ValueList(AV12Codigos, "[ContagemResultado_Codigo]"+" IN (", ")"));
         scmdbuf = "SELECT COALESCE( T1.[GXC8], convert( DATETIME, '17530101', 112 )) AS GXC8 FROM (SELECT MAX([ContagemResultado_DataPrevista]) AS GXC8 FROM [ContagemResultado] WITH (NOLOCK) WHERE ";
         scmdbuf = scmdbuf + sRef1;
         scmdbuf = scmdbuf + " ) T1";
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + "";
         GXv_Object17[0] = scmdbuf;
         return GXv_Object17 ;
      }

      protected Object[] conditional_P008W16( IGxContext context ,
                                              int A456ContagemResultado_Codigo ,
                                              IGxCollection AV12Codigos ,
                                              int A597ContagemResultado_LoteAceiteCod ,
                                              int AV47ContagemResultado_LoteAceite ,
                                              int AV232ContratoServicos_Codigo ,
                                              String AV251ContratoServicosIndicador_Tipo ,
                                              int A1270ContratoServicosIndicador_CntSrvCod ,
                                              String A1308ContratoServicosIndicador_Tipo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object19 ;
         GXv_Object19 = new Object [2] ;
         String sRef1 ;
         sRef1 = (String)(new GxDbmsUtils( new GxSqlServer()).ValueList(AV12Codigos, "[ContagemResultado_Codigo]"+" IN (", ")"));
         scmdbuf = "SELECT COALESCE( T1.[GXC9], convert( DATETIME, '17530101', 112 )) AS GXC9 FROM (SELECT MAX([ContagemResultado_DataDmn]) AS GXC9 FROM [ContagemResultado] WITH (NOLOCK) WHERE ";
         scmdbuf = scmdbuf + sRef1;
         scmdbuf = scmdbuf + " ) T1";
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + "";
         GXv_Object19[0] = scmdbuf;
         return GXv_Object19 ;
      }

      protected Object[] conditional_P008W18( IGxContext context ,
                                              int A456ContagemResultado_Codigo ,
                                              IGxCollection AV12Codigos ,
                                              int A597ContagemResultado_LoteAceiteCod ,
                                              int AV47ContagemResultado_LoteAceite ,
                                              int AV232ContratoServicos_Codigo ,
                                              String AV251ContratoServicosIndicador_Tipo ,
                                              int A1270ContratoServicosIndicador_CntSrvCod ,
                                              String A1308ContratoServicosIndicador_Tipo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object21 ;
         GXv_Object21 = new Object [2] ;
         String sRef1 ;
         sRef1 = (String)(new GxDbmsUtils( new GxSqlServer()).ValueList(AV12Codigos, "[ContagemResultado_Codigo]"+" IN (", ")"));
         scmdbuf = "SELECT COALESCE( T1.[GXC10], convert( DATETIME, '17530101', 112 )) AS GXC10 FROM (SELECT MAX([ContagemResultado_DataHomologacao]) AS GXC10 FROM [ContagemResultado] WITH (NOLOCK) WHERE ";
         scmdbuf = scmdbuf + sRef1;
         scmdbuf = scmdbuf + " ) T1";
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + "";
         GXv_Object21[0] = scmdbuf;
         return GXv_Object21 ;
      }

      protected Object[] conditional_P008W24( IGxContext context ,
                                              int A456ContagemResultado_Codigo ,
                                              IGxCollection AV12Codigos )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object23 ;
         GXv_Object23 = new Object [2] ;
         scmdbuf = "SELECT [ContagemResultado_StatusDmn], [ContagemResultado_VlrCnc], [ContagemResultado_ValorPF], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV12Codigos, "[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ContagemResultado_Codigo]";
         GXv_Object23[0] = scmdbuf;
         return GXv_Object23 ;
      }

      protected Object[] conditional_P008W25( IGxContext context ,
                                              int A456ContagemResultado_Codigo ,
                                              IGxCollection AV12Codigos )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object25 ;
         GXv_Object25 = new Object [2] ;
         scmdbuf = "SELECT [ContagemResultado_Codigo], [ContagemResultado_DemandaFM] FROM [ContagemResultado] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV12Codigos, "[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ContagemResultado_DemandaFM]";
         GXv_Object25[0] = scmdbuf;
         return GXv_Object25 ;
      }

      protected Object[] conditional_P008W26( IGxContext context ,
                                              int A456ContagemResultado_Codigo ,
                                              IGxCollection AV12Codigos )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object27 ;
         GXv_Object27 = new Object [2] ;
         scmdbuf = "SELECT [ContagemResultado_Codigo], [ContagemResultado_DemandaFM] FROM [ContagemResultado] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV12Codigos, "[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ContagemResultado_DemandaFM]";
         GXv_Object27[0] = scmdbuf;
         return GXv_Object27 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_P008W4(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (String)dynConstraints[7] , (bool)dynConstraints[8] );
               case 2 :
                     return conditional_P008W6(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (String)dynConstraints[7] );
               case 3 :
                     return conditional_P008W8(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (String)dynConstraints[7] );
               case 4 :
                     return conditional_P008W10(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (String)dynConstraints[7] );
               case 5 :
                     return conditional_P008W12(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (String)dynConstraints[7] , (bool)dynConstraints[8] );
               case 6 :
                     return conditional_P008W14(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (String)dynConstraints[7] );
               case 7 :
                     return conditional_P008W16(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (String)dynConstraints[7] );
               case 8 :
                     return conditional_P008W18(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (String)dynConstraints[7] );
               case 11 :
                     return conditional_P008W24(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
               case 12 :
                     return conditional_P008W25(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
               case 13 :
                     return conditional_P008W26(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP008W2 ;
          prmP008W2 = new Object[] {
          new Object[] {"@AV11Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008W20 ;
          prmP008W20 = new Object[] {
          new Object[] {"@AV47ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008W23 ;
          prmP008W23 = new Object[] {
          new Object[] {"@AV47ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008W28 ;
          prmP008W28 = new Object[] {
          new Object[] {"@AV211ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008W29 ;
          prmP008W29 = new Object[] {
          new Object[] {"@AV47ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008W30 ;
          prmP008W30 = new Object[] {
          new Object[] {"@AV47ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008W31 ;
          prmP008W31 = new Object[] {
          new Object[] {"@AV47ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008W33 ;
          prmP008W33 = new Object[] {
          new Object[] {"@AV211ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008W35 ;
          prmP008W35 = new Object[] {
          new Object[] {"@AV46ContagemResultado_DemandaFM",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV47ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008W36 ;
          prmP008W36 = new Object[] {
          new Object[] {"@AV11Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008W37 ;
          prmP008W37 = new Object[] {
          new Object[] {"@AV105Contratada",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008W38 ;
          prmP008W38 = new Object[] {
          new Object[] {"@AV228Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008W39 ;
          prmP008W39 = new Object[] {
          new Object[] {"@AV232ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV251ContratoServicosIndicador_Tipo",SqlDbType.Char,2,0}
          } ;
          Object[] prmP008W40 ;
          prmP008W40 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008W4 ;
          prmP008W4 = new Object[] {
          } ;
          Object[] prmP008W6 ;
          prmP008W6 = new Object[] {
          } ;
          Object[] prmP008W8 ;
          prmP008W8 = new Object[] {
          } ;
          Object[] prmP008W10 ;
          prmP008W10 = new Object[] {
          } ;
          Object[] prmP008W12 ;
          prmP008W12 = new Object[] {
          } ;
          Object[] prmP008W14 ;
          prmP008W14 = new Object[] {
          } ;
          Object[] prmP008W16 ;
          prmP008W16 = new Object[] {
          } ;
          Object[] prmP008W18 ;
          prmP008W18 = new Object[] {
          } ;
          Object[] prmP008W24 ;
          prmP008W24 = new Object[] {
          } ;
          Object[] prmP008W25 ;
          prmP008W25 = new Object[] {
          } ;
          Object[] prmP008W26 ;
          prmP008W26 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("P008W2", "SELECT TOP 1 T1.[Modulo_Codigo], T2.[Sistema_Codigo], T3.[Sistema_AreaTrabalhoCod] AS Sistema_AreaTrabalhoCod, T7.[ContratoServicos_UnidadeContratada] AS ContagemResultado_CntSrvUndCnt, T4.[Contratante_Codigo], T1.[ContagemResultado_Codigo], T6.[Contratada_Logo_GXI] AS ContagemResultado_ContratadaLogo_GXI, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T7.[Servico_Codigo] AS ContagemResultado_Servico, T7.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T8.[UnidadeMedicao_Sigla] AS ContagemResultado_CntSrvUndCntSgl, T6.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T5.[Contratante_TtlRltGerencial], T6.[Contratada_Logo] AS ContagemResultado_ContratadaLogo FROM ((((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Modulo] T2 WITH (NOLOCK) ON T2.[Modulo_Codigo] = T1.[Modulo_Codigo]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T2.[Sistema_Codigo]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T3.[Sistema_AreaTrabalhoCod]) LEFT JOIN [Contratante] T5 WITH (NOLOCK) ON T5.[Contratante_Codigo] = T4.[Contratante_Codigo]) LEFT JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [ContratoServicos] T7 WITH (NOLOCK) ON T7.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [UnidadeMedicao] T8 WITH (NOLOCK) ON T8.[UnidadeMedicao_Codigo] = T7.[ContratoServicos_UnidadeContratada]) WHERE T1.[ContagemResultado_Codigo] = @AV11Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008W2,1,0,true,true )
             ,new CursorDef("P008W4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008W4,1,0,true,true )
             ,new CursorDef("P008W6", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008W6,1,0,true,true )
             ,new CursorDef("P008W8", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008W8,1,0,true,true )
             ,new CursorDef("P008W10", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008W10,1,0,true,true )
             ,new CursorDef("P008W12", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008W12,1,0,true,true )
             ,new CursorDef("P008W14", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008W14,1,0,true,true )
             ,new CursorDef("P008W16", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008W16,1,0,true,true )
             ,new CursorDef("P008W18", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008W18,1,0,true,true )
             ,new CursorDef("P008W20", "SELECT TOP 1 T4.[ContratoServicos_UnidadeContratada] AS ContagemResultado_CntSrvUndCnt, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_LoteAceiteCod], T3.[Contratada_Logo_GXI] AS ContagemResultado_ContratadaLogo_GXI, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T4.[Servico_Codigo] AS ContagemResultado_Servico, T4.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T5.[UnidadeMedicao_Sigla] AS ContagemResultado_CntSrvUndCntSgl, T3.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, COALESCE( T2.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, T3.[Contratada_Logo] AS ContagemResultado_ContratadaLogo FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [UnidadeMedicao] T5 WITH (NOLOCK) ON T5.[UnidadeMedicao_Codigo] = T4.[ContratoServicos_UnidadeContratada]) WHERE T1.[ContagemResultado_LoteAceiteCod] = @AV47ContagemResultado_LoteAceite ORDER BY T1.[ContagemResultado_LoteAceiteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008W20,1,0,true,true )
             ,new CursorDef("P008W23", "SELECT TOP 1 T1.[Lote_Nome], T1.[Lote_Numero], T1.[Lote_CntUsado], T1.[Lote_CntSaldo], T1.[Lote_Codigo], COALESCE( T2.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) AS Lote_DataIni, COALESCE( T2.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) AS Lote_DataFim FROM ([Lote] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN(COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataIni, T3.[ContagemResultado_LoteAceiteCod], MAX(COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataFim FROM ([ContagemResultado] T3 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T4 ON T4.[ContagemResultado_Codigo] = T3.[ContagemResultado_Codigo]) GROUP BY T3.[ContagemResultado_LoteAceiteCod] ) T2 ON T2.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo]) WHERE T1.[Lote_Codigo] = @AV47ContagemResultado_LoteAceite ORDER BY T1.[Lote_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008W23,1,0,false,true )
             ,new CursorDef("P008W24", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008W24,100,0,true,false )
             ,new CursorDef("P008W25", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008W25,100,0,true,false )
             ,new CursorDef("P008W26", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008W26,100,0,true,false )
             ,new CursorDef("P008W28", "SELECT T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_VlrCnc], T1.[ContagemResultado_Descricao], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T3.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_PrazoMaisDias], T1.[ContagemResultado_PrazoInicialDias], T1.[ContagemResultado_DataEntregaReal], T4.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_Demanda], T3.[ContratoServicos_Alias] AS ContagemResultado_CntSrvAls, T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_GlsValor], T1.[ContagemResultado_GlsDescricao], COALESCE( T5.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ValorPF] FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T3.[Servico_Codigo]) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T5 ON T5.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @AV211ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008W28,1,0,true,true )
             ,new CursorDef("P008W29", "SELECT [ContagemResultado_LoteAceiteCod], [ContagemResultado_StatusDmn], [ContagemResultado_VlrCnc], [ContagemResultado_Codigo], [ContagemResultado_ValorPF] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_LoteAceiteCod] = @AV47ContagemResultado_LoteAceite ORDER BY [ContagemResultado_LoteAceiteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008W29,100,0,true,false )
             ,new CursorDef("P008W30", "SELECT [ContagemResultado_LoteAceiteCod], [ContagemResultado_Codigo], [ContagemResultado_DemandaFM] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_LoteAceiteCod] = @AV47ContagemResultado_LoteAceite ORDER BY [ContagemResultado_LoteAceiteCod], [ContagemResultado_DemandaFM] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008W30,100,0,true,false )
             ,new CursorDef("P008W31", "SELECT [ContagemResultado_LoteAceiteCod], [ContagemResultado_Codigo], [ContagemResultado_DemandaFM] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_LoteAceiteCod] = @AV47ContagemResultado_LoteAceite ORDER BY [ContagemResultado_LoteAceiteCod], [ContagemResultado_DemandaFM] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008W31,100,0,true,false )
             ,new CursorDef("P008W33", "SELECT T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_VlrCnc], T1.[ContagemResultado_Descricao], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T3.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_PrazoMaisDias], T1.[ContagemResultado_PrazoInicialDias], T1.[ContagemResultado_DataEntregaReal], T4.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_Demanda], T3.[ContratoServicos_Alias] AS ContagemResultado_CntSrvAls, T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_GlsValor], T1.[ContagemResultado_GlsDescricao], COALESCE( T5.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ValorPF] FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T3.[Servico_Codigo]) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T5 ON T5.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @AV211ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008W33,1,0,true,true )
             ,new CursorDef("P008W35", "SELECT T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_VlrCnc], T1.[ContagemResultado_Descricao], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T3.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_PrazoMaisDias], T1.[ContagemResultado_PrazoInicialDias], T1.[ContagemResultado_DataEntregaReal], T1.[ContagemResultado_Demanda], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T3.[ContratoServicos_Alias] AS ContagemResultado_CntSrvAls, T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_GlsValor], T1.[ContagemResultado_GlsDescricao], COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ValorPF] FROM ((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T4 ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE (T1.[ContagemResultado_DemandaFM] = @AV46ContagemResultado_DemandaFM) AND (T1.[ContagemResultado_LoteAceiteCod] = @AV47ContagemResultado_LoteAceite) ORDER BY T1.[ContagemResultado_DemandaFM] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008W35,100,0,true,false )
             ,new CursorDef("P008W36", "SELECT [ContagemResultadoExecucao_OSCod], [ContagemResultadoExecucao_Codigo], [ContagemResultadoExecucao_Inicio] FROM [ContagemResultadoExecucao] WITH (NOLOCK) WHERE [ContagemResultadoExecucao_OSCod] = @AV11Codigo ORDER BY [ContagemResultadoExecucao_OSCod], [ContagemResultadoExecucao_Inicio] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008W36,100,0,true,false )
             ,new CursorDef("P008W37", "SELECT TOP 1 T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T2.[Pessoa_MunicipioCod] AS Pessoa_MunicipioCod, T1.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T4.[AreaTrabalho_ServicoPadrao] AS Contratada_AreaTrbSrvPdr, T5.[Servico_UO] AS Servico_UO, T1.[Contratada_Codigo], T2.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T2.[Pessoa_CEP], T6.[Estado_UF], T3.[Municipio_Nome], T2.[Pessoa_Endereco] FROM ((((([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T2.[Pessoa_MunicipioCod]) INNER JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T1.[Contratada_AreaTrabalhoCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[AreaTrabalho_ServicoPadrao]) LEFT JOIN [Geral_UnidadeOrganizacional] T6 WITH (NOLOCK) ON T6.[UnidadeOrganizacional_Codigo] = T5.[Servico_UO]) WHERE T1.[Contratada_Codigo] = @AV105Contratada ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008W37,1,0,false,true )
             ,new CursorDef("P008W38", "SELECT TOP 1 T1.[Contratada_Codigo], T2.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T1.[Contrato_PrepostoCod] AS Contrato_PrepostoCod, T5.[Usuario_PessoaCod] AS Contrato_PrepostoPesCod, T3.[Contratante_Codigo], T1.[Contrato_Codigo], T1.[Contrato_Numero], T6.[Pessoa_Nome] AS Contrato_PrepostoNom, T1.[Contrato_Quantidade], T4.[Contratante_TtlRltGerencial], T1.[Contrato_PrdFtrCada], T1.[Contrato_PrdFtrIni], T1.[Contrato_PrdFtrFim] FROM ((((([Contrato] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[Contratada_Codigo]) INNER JOIN [AreaTrabalho] T3 WITH (NOLOCK) ON T3.[AreaTrabalho_Codigo] = T2.[Contratada_AreaTrabalhoCod]) LEFT JOIN [Contratante] T4 WITH (NOLOCK) ON T4.[Contratante_Codigo] = T3.[Contratante_Codigo]) LEFT JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = T1.[Contrato_PrepostoCod]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) WHERE T1.[Contrato_Codigo] = @AV228Contrato_Codigo ORDER BY T1.[Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008W38,1,0,false,true )
             ,new CursorDef("P008W39", "SELECT [ContratoServicosIndicador_CntSrvCod], [ContratoServicosIndicador_Tipo], [ContratoServicosIndicador_Codigo], [ContratoServicosIndicador_Indicador], [ContratoServicosIndicador_Numero] FROM [ContratoServicosIndicador] WITH (NOLOCK) WHERE [ContratoServicosIndicador_CntSrvCod] = @AV232ContratoServicos_Codigo and [ContratoServicosIndicador_Tipo] = @AV251ContratoServicosIndicador_Tipo ORDER BY [ContratoServicosIndicador_CntSrvCod], [ContratoServicosIndicador_Tipo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008W39,100,0,true,false )
             ,new CursorDef("P008W40", "SELECT [ContratoServicosIndicador_Codigo], [ContratoServicosIndicadorFaixa_Reduz], [ContratoServicosIndicadorFaixa_Ate], [ContratoServicosIndicadorFaixa_Desde], [ContratoServicosIndicadorFaixa_Numero], [ContratoServicosIndicadorFaixa_Codigo] FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK) WHERE [ContratoServicosIndicador_Codigo] = @ContratoServicosIndicador_Codigo ORDER BY [ContratoServicosIndicador_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008W40,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((String[]) buf[9])[0] = rslt.getMultimediaUri(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getString(12, 15) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((String[]) buf[21])[0] = rslt.getString(13, 15) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getVarchar(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((String[]) buf[25])[0] = rslt.getMultimediaFile(15, rslt.getVarchar(7)) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                return;
             case 1 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                return;
             case 2 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDateTime(1) ;
                return;
             case 3 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                return;
             case 4 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDateTime(1) ;
                return;
             case 5 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                return;
             case 6 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDateTime(1) ;
                return;
             case 7 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                return;
             case 8 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDateTime(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getMultimediaUri(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getString(10, 15) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[19])[0] = rslt.getGXDate(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((String[]) buf[21])[0] = rslt.getMultimediaFile(12, rslt.getVarchar(4)) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(6) ;
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(7) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((short[]) buf[14])[0] = rslt.getShort(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((short[]) buf[16])[0] = rslt.getShort(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[18])[0] = rslt.getGXDateTime(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((String[]) buf[20])[0] = rslt.getString(11, 15) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((String[]) buf[22])[0] = rslt.getString(12, 25) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((String[]) buf[24])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(13);
                ((String[]) buf[26])[0] = rslt.getString(14, 15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(14);
                ((DateTime[]) buf[28])[0] = rslt.getGXDate(15) ;
                ((decimal[]) buf[29])[0] = rslt.getDecimal(16) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(16);
                ((String[]) buf[31])[0] = rslt.getLongVarchar(17) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(17);
                ((DateTime[]) buf[33])[0] = rslt.getGXDate(18) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(18);
                ((int[]) buf[35])[0] = rslt.getInt(19) ;
                ((decimal[]) buf[36])[0] = rslt.getDecimal(20) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(20);
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((decimal[]) buf[7])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((short[]) buf[14])[0] = rslt.getShort(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((short[]) buf[16])[0] = rslt.getShort(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[18])[0] = rslt.getGXDateTime(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((String[]) buf[20])[0] = rslt.getString(11, 15) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((String[]) buf[22])[0] = rslt.getString(12, 25) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((String[]) buf[24])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(13);
                ((String[]) buf[26])[0] = rslt.getString(14, 15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(14);
                ((DateTime[]) buf[28])[0] = rslt.getGXDate(15) ;
                ((decimal[]) buf[29])[0] = rslt.getDecimal(16) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(16);
                ((String[]) buf[31])[0] = rslt.getLongVarchar(17) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(17);
                ((DateTime[]) buf[33])[0] = rslt.getGXDate(18) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(18);
                ((int[]) buf[35])[0] = rslt.getInt(19) ;
                ((decimal[]) buf[36])[0] = rslt.getDecimal(20) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(20);
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((short[]) buf[18])[0] = rslt.getShort(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((short[]) buf[20])[0] = rslt.getShort(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[22])[0] = rslt.getGXDateTime(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((String[]) buf[24])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(13);
                ((String[]) buf[26])[0] = rslt.getString(14, 25) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(14);
                ((String[]) buf[28])[0] = rslt.getString(15, 15) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[30])[0] = rslt.getGXDate(16) ;
                ((decimal[]) buf[31])[0] = rslt.getDecimal(17) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(17);
                ((String[]) buf[33])[0] = rslt.getLongVarchar(18) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(18);
                ((DateTime[]) buf[35])[0] = rslt.getGXDate(19) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(19);
                ((int[]) buf[37])[0] = rslt.getInt(20) ;
                ((decimal[]) buf[38])[0] = rslt.getDecimal(21) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(21);
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 10) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getString(9, 2) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getString(10, 50) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((String[]) buf[9])[0] = rslt.getString(7, 20) ;
                ((String[]) buf[10])[0] = rslt.getString(8, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((String[]) buf[13])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((String[]) buf[15])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((short[]) buf[17])[0] = rslt.getShort(12) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((short[]) buf[19])[0] = rslt.getShort(13) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 23 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 24 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
