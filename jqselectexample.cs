/*
               File: jqSelectExample
        Description: jq Select Example
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:56:27.50
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class jqselectexample : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public jqselectexample( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public jqselectexample( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAPH2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WSPH2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEPH2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( "jq Select Example") ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042823562751");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery-1.4.2.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.core.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.widget.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.mouse.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.selectable.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.corner.js", "");
         context.AddJavascriptSource("jQueryUI/jqSelectRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("jqselectexample.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vJQSELECTDATA", AV5jqSelectData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vJQSELECTDATA", AV5jqSelectData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vJQSELECTITEM", AV6jqSelectItem);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vJQSELECTITEM", AV6jqSelectItem);
         }
         GxWebStd.gx_hidden_field( context, "vGXV1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV10GXV1), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "JQSELECT1_Width", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqselect1_Width), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQSELECT1_Height", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqselect1_Height), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQSELECT1_Rounding", StringUtil.RTrim( Jqselect1_Rounding));
         GxWebStd.gx_hidden_field( context, "JQSELECT1_Theme", StringUtil.RTrim( Jqselect1_Theme));
         GxWebStd.gx_hidden_field( context, "JQSELECT1_Selectedicon", StringUtil.RTrim( Jqselect1_Selectedicon));
         GxWebStd.gx_hidden_field( context, "JQSELECT1_Unselectedicon", StringUtil.RTrim( Jqselect1_Unselectedicon));
         GxWebStd.gx_hidden_field( context, "JQSELECT1_Selectedcolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqselect1_Selectedcolor), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQSELECT1_Selectingcolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqselect1_Selectingcolor), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQSELECT1_Itemwidth", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqselect1_Itemwidth), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQSELECT1_Itemheight", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqselect1_Itemheight), 9, 0, ".", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormPH2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "jqSelectExample" ;
      }

      public override String GetPgmdesc( )
      {
         return "jq Select Example" ;
      }

      protected void WBPH0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            wb_table1_2_PH2( true) ;
         }
         else
         {
            wb_table1_2_PH2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_PH2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTPH2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "jq Select Example", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPPH0( ) ;
      }

      protected void WSPH2( )
      {
         STARTPH2( ) ;
         EVTPH2( ) ;
      }

      protected void EVTPH2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11PH2 */
                           E11PH2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12PH2 */
                           E12PH2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                              }
                              dynload_actions( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEPH2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormPH2( ) ;
            }
         }
      }

      protected void PAPH2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFPH2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFPH2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E12PH2 */
            E12PH2 ();
            WBPH0( ) ;
         }
      }

      protected void STRUPPH0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11PH2 */
         E11PH2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vJQSELECTDATA"), AV5jqSelectData);
            ajax_req_read_hidden_sdt(cgiGet( "vJQSELECTITEM"), AV6jqSelectItem);
            /* Read variables values. */
            /* Read saved values. */
            AV10GXV1 = (int)(context.localUtil.CToN( cgiGet( "vGXV1"), ",", "."));
            Jqselect1_Width = (int)(context.localUtil.CToN( cgiGet( "JQSELECT1_Width"), ",", "."));
            Jqselect1_Height = (int)(context.localUtil.CToN( cgiGet( "JQSELECT1_Height"), ",", "."));
            Jqselect1_Rounding = cgiGet( "JQSELECT1_Rounding");
            Jqselect1_Theme = cgiGet( "JQSELECT1_Theme");
            Jqselect1_Selectedicon = cgiGet( "JQSELECT1_Selectedicon");
            Jqselect1_Unselectedicon = cgiGet( "JQSELECT1_Unselectedicon");
            Jqselect1_Selectedcolor = (int)(context.localUtil.CToN( cgiGet( "JQSELECT1_Selectedcolor"), ",", "."));
            Jqselect1_Selectingcolor = (int)(context.localUtil.CToN( cgiGet( "JQSELECT1_Selectingcolor"), ",", "."));
            Jqselect1_Itemwidth = (int)(context.localUtil.CToN( cgiGet( "JQSELECT1_Itemwidth"), ",", "."));
            Jqselect1_Itemheight = (int)(context.localUtil.CToN( cgiGet( "JQSELECT1_Itemheight"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void S112( )
      {
         /* 'JQUISELECTEXAMPLE' Routine */
         AV6jqSelectItem = new SdtjqSelectData_Item(context);
         AV6jqSelectItem.gxTpr_Id = "1";
         AV6jqSelectItem.gxTpr_Descr = "Lorem Ipsum Dolor Et";
         AV6jqSelectItem.gxTpr_Selected = false;
         AV5jqSelectData.Add(AV6jqSelectItem, 0);
         AV6jqSelectItem = new SdtjqSelectData_Item(context);
         AV6jqSelectItem.gxTpr_Id = "2";
         AV6jqSelectItem.gxTpr_Descr = "Lorem Ipsum Dolor Et";
         AV6jqSelectItem.gxTpr_Selected = true;
         AV5jqSelectData.Add(AV6jqSelectItem, 0);
      }

      protected void GXStart( )
      {
         /* Execute user event: E11PH2 */
         E11PH2 ();
         if (returnInSub) return;
      }

      protected void E11PH2( )
      {
         /* Start Routine */
         GXt_objcol_SdtjqSelectData_Item1 = AV5jqSelectData;
         new jqselectsample(context ).execute( out  GXt_objcol_SdtjqSelectData_Item1) ;
         AV5jqSelectData = GXt_objcol_SdtjqSelectData_Item1;
      }

      protected void nextLoad( )
      {
      }

      protected void E12PH2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_PH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:400px")+"\">") ;
            wb_table2_8_PH2( true) ;
         }
         else
         {
            wb_table2_8_PH2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_PH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_PH2e( true) ;
         }
         else
         {
            wb_table1_2_PH2e( false) ;
         }
      }

      protected void wb_table2_8_PH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"JQSELECT1Container"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton2_Internalname, "", "Show Select", bttButton2_Jsonclick, 7, "Show Select", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"e13ph1_client"+"'", TempTags, "", 2, "HLP_jqSelectExample.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_PH2e( true) ;
         }
         else
         {
            wb_table2_8_PH2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAPH2( ) ;
         WSPH2( ) ;
         WEPH2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("jQueryUI/css/demos.css", "?1349420");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042823562767");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("jqselectexample.js", "?202042823562767");
         context.AddJavascriptSource("Shared/jquery/jquery-1.4.2.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.core.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.widget.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.mouse.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.selectable.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.corner.js", "");
         context.AddJavascriptSource("jQueryUI/jqSelectRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         Jqselect1_Internalname = "JQSELECT1";
         bttButton2_Internalname = "BUTTON2";
         tblTable2_Internalname = "TABLE2";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Jqselect1_Itemheight = 30;
         Jqselect1_Itemwidth = 240;
         Jqselect1_Selectingcolor = (int)(0x6B8E23);
         Jqselect1_Selectedcolor = (int)(0xB8860B);
         Jqselect1_Unselectedicon = "ui-icon-eject";
         Jqselect1_Selectedicon = "ui-icon-power";
         Jqselect1_Theme = "pepper-grinder";
         Jqselect1_Rounding = "bevelfold bl";
         Jqselect1_Height = 400;
         Jqselect1_Width = 300;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'SHOWSELECT'","{handler:'E13PH1',iparms:[{av:'AV5jqSelectData',fld:'vJQSELECTDATA',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV5jqSelectData = new GxObjectCollection( context, "jqSelectData.Item", "GxEv3Up14_MeetrikaVs3", "SdtjqSelectData_Item", "GeneXus.Programs");
         AV6jqSelectItem = new SdtjqSelectData_Item(context);
         GXKey = "";
         GX_FocusControl = "";
         sPrefix = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXt_objcol_SdtjqSelectData_Item1 = new GxObjectCollection( context, "jqSelectData.Item", "GxEv3Up14_MeetrikaVs3", "SdtjqSelectData_Item", "GeneXus.Programs");
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttButton2_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int AV10GXV1 ;
      private int Jqselect1_Width ;
      private int Jqselect1_Height ;
      private int Jqselect1_Selectedcolor ;
      private int Jqselect1_Selectingcolor ;
      private int Jqselect1_Itemwidth ;
      private int Jqselect1_Itemheight ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Jqselect1_Rounding ;
      private String Jqselect1_Theme ;
      private String Jqselect1_Selectedicon ;
      private String Jqselect1_Unselectedicon ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTable2_Internalname ;
      private String TempTags ;
      private String bttButton2_Internalname ;
      private String bttButton2_Jsonclick ;
      private String Jqselect1_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtjqSelectData_Item ))]
      private IGxCollection AV5jqSelectData ;
      [ObjectCollection(ItemType=typeof( SdtjqSelectData_Item ))]
      private IGxCollection GXt_objcol_SdtjqSelectData_Item1 ;
      private SdtjqSelectData_Item AV6jqSelectItem ;
   }

}
