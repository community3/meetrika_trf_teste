/*
               File: StatusContagem
        Description: StatusContagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/15/2020 20:54:39.11
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gxdomainstatuscontagem
   {
      private static Hashtable domain = new Hashtable();
      static gxdomainstatuscontagem ()
      {
         domain[(short)1] = "Inicial";
         domain[(short)2] = "Auditoria";
         domain[(short)3] = "Negociac�o";
         domain[(short)4] = "N�o Aprovada";
         domain[(short)5] = "Entregue";
         domain[(short)6] = "Pend�ncias";
         domain[(short)7] = "Diverg�ncia";
         domain[(short)8] = "Aprovada";
      }

      public static string getDescription( IGxContext context ,
                                           short key )
      {
         return (string)domain[key] ;
      }

      public static GxSimpleCollection getValues( )
      {
         GxSimpleCollection value = new GxSimpleCollection();
         ArrayList aKeys = new ArrayList(domain.Keys);
         aKeys.Sort();
         foreach (short key in aKeys)
         {
            value.Add(key);
         }
         return value;
      }

   }

}
