/*
               File: type_SdtTileSDT
        Description: TileSDT
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:30:8.84
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "TileSDT" )]
   [XmlType(TypeName =  "TileSDT" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtTileSDT : GxUserType
   {
      public SdtTileSDT( )
      {
         /* Constructor for serialization */
         gxTv_SdtTileSDT_Description = "";
         gxTv_SdtTileSDT_Image = "";
         gxTv_SdtTileSDT_Image_gxi = "";
      }

      public SdtTileSDT( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtTileSDT deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtTileSDT)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtTileSDT obj ;
         obj = this;
         obj.gxTpr_Description = deserialized.gxTpr_Description;
         obj.gxTpr_Image = deserialized.gxTpr_Image;
         obj.gxTpr_Image_gxi = deserialized.gxTpr_Image_gxi;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Description") )
               {
                  gxTv_SdtTileSDT_Description = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Image") )
               {
                  gxTv_SdtTileSDT_Image = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Image_GXI") )
               {
                  gxTv_SdtTileSDT_Image_gxi = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "TileSDT";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Description", StringUtil.RTrim( gxTv_SdtTileSDT_Description));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Image", StringUtil.RTrim( gxTv_SdtTileSDT_Image));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Image_GXI", StringUtil.RTrim( gxTv_SdtTileSDT_Image_gxi));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Description", gxTv_SdtTileSDT_Description, false);
         AddObjectProperty("Image", gxTv_SdtTileSDT_Image, false);
         AddObjectProperty("Image_GXI", gxTv_SdtTileSDT_Image_gxi, false);
         return  ;
      }

      [  SoapElement( ElementName = "Description" )]
      [  XmlElement( ElementName = "Description"   )]
      public String gxTpr_Description
      {
         get {
            return gxTv_SdtTileSDT_Description ;
         }

         set {
            gxTv_SdtTileSDT_Description = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Image" )]
      [  XmlElement( ElementName = "Image"   )]
      [GxUpload()]
      public String gxTpr_Image
      {
         get {
            return gxTv_SdtTileSDT_Image ;
         }

         set {
            gxTv_SdtTileSDT_Image = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Image_GXI" )]
      [  XmlElement( ElementName = "Image_GXI"   )]
      public String gxTpr_Image_gxi
      {
         get {
            return gxTv_SdtTileSDT_Image_gxi ;
         }

         set {
            gxTv_SdtTileSDT_Image_gxi = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtTileSDT_Description = "";
         gxTv_SdtTileSDT_Image = "";
         gxTv_SdtTileSDT_Image_gxi = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String sTagName ;
      protected String gxTv_SdtTileSDT_Description ;
      protected String gxTv_SdtTileSDT_Image_gxi ;
      protected String gxTv_SdtTileSDT_Image ;
   }

   [DataContract(Name = @"TileSDT", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtTileSDT_RESTInterface : GxGenericCollectionItem<SdtTileSDT>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtTileSDT_RESTInterface( ) : base()
      {
      }

      public SdtTileSDT_RESTInterface( SdtTileSDT psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Description" , Order = 0 )]
      public String gxTpr_Description
      {
         get {
            return sdt.gxTpr_Description ;
         }

         set {
            sdt.gxTpr_Description = (String)(value);
         }

      }

      [DataMember( Name = "Image" , Order = 1 )]
      [GxUpload()]
      public String gxTpr_Image
      {
         get {
            return (!String.IsNullOrEmpty(StringUtil.RTrim( sdt.gxTpr_Image)) ? PathUtil.RelativePath( sdt.gxTpr_Image) : StringUtil.RTrim( sdt.gxTpr_Image_gxi)) ;
         }

         set {
            sdt.gxTpr_Image = (String)(value);
         }

      }

      public SdtTileSDT sdt
      {
         get {
            return (SdtTileSDT)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtTileSDT() ;
         }
      }

   }

}
