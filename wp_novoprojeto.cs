/*
               File: WP_NovoProjeto
        Description: OS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:43:5.99
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_novoprojeto : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_novoprojeto( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_novoprojeto( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Codigo ,
                           String aP1_Demanda )
      {
         this.AV17Codigo = aP0_Codigo;
         this.AV18Demanda = aP1_Demanda;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavProjeto_codigo = new GXCombobox();
         dynavTipoprojeto_codigo = new GXCombobox();
         dynavProjeto_servicocod = new GXCombobox();
         dynavProjeto_gerentecod = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vPROJETO_CODIGO") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvPROJETO_CODIGOLC2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vPROJETO_SERVICOCOD") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvPROJETO_SERVICOCODLC2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vPROJETO_GERENTECOD") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvPROJETO_GERENTECODLC2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV17Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV18Demanda = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Demanda", AV18Demanda);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vDEMANDA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV18Demanda, "@!"))));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PALC2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTLC2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020529943610");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/TabsPanel/BootstrapTabsPanelRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_novoprojeto.aspx") + "?" + UrlEncode("" +AV17Codigo) + "," + UrlEncode(StringUtil.RTrim(AV18Demanda))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vPROJETO_INTRODUCAO", AV9Projeto_Introducao);
         GxWebStd.gx_hidden_field( context, "vPROJETO_ESCOPO", AV8Projeto_Escopo);
         GxWebStd.gx_hidden_field( context, "vCODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PROJETO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A648Projeto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PROJETO_SIGLA", StringUtil.RTrim( A650Projeto_Sigla));
         GxWebStd.gx_hidden_field( context, "PROJETO_NOME", StringUtil.RTrim( A649Projeto_Nome));
         GxWebStd.gx_hidden_field( context, "PROJETO_PREVISAO", context.localUtil.DToC( A1541Projeto_Previsao, 0, "/"));
         GxWebStd.gx_hidden_field( context, "PROJETO_GERENTECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1542Projeto_GerenteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PROJETO_SERVICOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1543Projeto_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PROJETO_INTRODUCAO", A1540Projeto_Introducao);
         GxWebStd.gx_hidden_field( context, "PROJETO_ESCOPO", A653Projeto_Escopo);
         GxWebStd.gx_hidden_field( context, "vDEMANDA", AV18Demanda);
         GxWebStd.gx_hidden_field( context, "gxhash_vDEMANDA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV18Demanda, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vDEMANDA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV18Demanda, "@!"))));
         GxWebStd.gx_hidden_field( context, "PROJETO_INTRODUCAO_Enabled", StringUtil.BoolToStr( Projeto_introducao_Enabled));
         GxWebStd.gx_hidden_field( context, "PROJETO_ESCOPO_Enabled", StringUtil.BoolToStr( Projeto_escopo_Enabled));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TAB_Width", StringUtil.RTrim( Gxuitabspanel_tab_Width));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TAB_Cls", StringUtil.RTrim( Gxuitabspanel_tab_Cls));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TAB_Autowidth", StringUtil.BoolToStr( Gxuitabspanel_tab_Autowidth));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TAB_Autoheight", StringUtil.BoolToStr( Gxuitabspanel_tab_Autoheight));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TAB_Autoscroll", StringUtil.BoolToStr( Gxuitabspanel_tab_Autoscroll));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TAB_Designtimetabs", StringUtil.RTrim( Gxuitabspanel_tab_Designtimetabs));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WELC2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTLC2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_novoprojeto.aspx") + "?" + UrlEncode("" +AV17Codigo) + "," + UrlEncode(StringUtil.RTrim(AV18Demanda)) ;
      }

      public override String GetPgmname( )
      {
         return "WP_NovoProjeto" ;
      }

      public override String GetPgmdesc( )
      {
         return "OS" ;
      }

      protected void WBLC0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_LC2( true) ;
         }
         else
         {
            wb_table1_2_LC2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_LC2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTLC2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "OS", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPLC0( ) ;
      }

      protected void WSLC2( )
      {
         STARTLC2( ) ;
         EVTLC2( ) ;
      }

      protected void EVTLC2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11LC2 */
                              E11LC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E12LC2 */
                                    E12LC2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13LC2 */
                              E13LC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VPROJETO_CODIGO.ISVALID") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14LC2 */
                              E14LC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15LC2 */
                              E15LC2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WELC2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PALC2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavProjeto_codigo.Name = "vPROJETO_CODIGO";
            dynavProjeto_codigo.WebTags = "";
            dynavTipoprojeto_codigo.Name = "vTIPOPROJETO_CODIGO";
            dynavTipoprojeto_codigo.WebTags = "";
            dynavTipoprojeto_codigo.removeAllItems();
            /* Using cursor H00LC2 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               dynavTipoprojeto_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(H00LC2_A979TipoProjeto_Codigo[0]), 6, 0)), H00LC2_A980TipoProjeto_Nome[0], 0);
               pr_default.readNext(0);
            }
            pr_default.close(0);
            if ( dynavTipoprojeto_codigo.ItemCount > 0 )
            {
               AV19TipoProjeto_Codigo = (int)(NumberUtil.Val( dynavTipoprojeto_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV19TipoProjeto_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19TipoProjeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19TipoProjeto_Codigo), 6, 0)));
            }
            dynavProjeto_servicocod.Name = "vPROJETO_SERVICOCOD";
            dynavProjeto_servicocod.WebTags = "";
            dynavProjeto_gerentecod.Name = "vPROJETO_GERENTECOD";
            dynavProjeto_gerentecod.WebTags = "";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavProjeto_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvTIPOPROJETO_CODIGOLC1( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvTIPOPROJETO_CODIGO_dataLC1( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvTIPOPROJETO_CODIGO_htmlLC1( )
      {
         int gxdynajaxvalue ;
         GXDLVvTIPOPROJETO_CODIGO_dataLC1( ) ;
         gxdynajaxindex = 1;
         dynavTipoprojeto_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavTipoprojeto_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavTipoprojeto_codigo.ItemCount > 0 )
         {
            AV19TipoProjeto_Codigo = (int)(NumberUtil.Val( dynavTipoprojeto_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV19TipoProjeto_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19TipoProjeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19TipoProjeto_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvTIPOPROJETO_CODIGO_dataLC1( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H00LC3 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00LC3_A979TipoProjeto_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00LC3_A980TipoProjeto_Nome[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXDLVvPROJETO_CODIGOLC2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvPROJETO_CODIGO_dataLC2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvPROJETO_CODIGO_htmlLC2( )
      {
         int gxdynajaxvalue ;
         GXDLVvPROJETO_CODIGO_dataLC2( ) ;
         gxdynajaxindex = 1;
         dynavProjeto_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavProjeto_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavProjeto_codigo.ItemCount > 0 )
         {
            AV15Projeto_Codigo = (int)(NumberUtil.Val( dynavProjeto_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15Projeto_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Projeto_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvPROJETO_CODIGO_dataLC2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Novo Projeto");
         /* Using cursor H00LC4 */
         pr_default.execute(2);
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00LC4_A648Projeto_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00LC4_A650Projeto_Sigla[0]));
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void GXDLVvPROJETO_SERVICOCODLC2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvPROJETO_SERVICOCOD_dataLC2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvPROJETO_SERVICOCOD_htmlLC2( )
      {
         int gxdynajaxvalue ;
         GXDLVvPROJETO_SERVICOCOD_dataLC2( ) ;
         gxdynajaxindex = 1;
         dynavProjeto_servicocod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavProjeto_servicocod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavProjeto_servicocod.ItemCount > 0 )
         {
            AV14Projeto_ServicoCod = (int)(NumberUtil.Val( dynavProjeto_servicocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14Projeto_ServicoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Projeto_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Projeto_ServicoCod), 6, 0)));
         }
      }

      protected void GXDLVvPROJETO_SERVICOCOD_dataLC2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00LC5 */
         pr_default.execute(3);
         while ( (pr_default.getStatus(3) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00LC5_A827ContratoServicos_ServicoCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00LC5_A826ContratoServicos_ServicoSigla[0]));
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void GXDLVvPROJETO_GERENTECODLC2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvPROJETO_GERENTECOD_dataLC2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvPROJETO_GERENTECOD_htmlLC2( )
      {
         int gxdynajaxvalue ;
         GXDLVvPROJETO_GERENTECOD_dataLC2( ) ;
         gxdynajaxindex = 1;
         dynavProjeto_gerentecod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavProjeto_gerentecod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavProjeto_gerentecod.ItemCount > 0 )
         {
            AV13Projeto_GerenteCod = (int)(NumberUtil.Val( dynavProjeto_gerentecod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13Projeto_GerenteCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Projeto_GerenteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Projeto_GerenteCod), 6, 0)));
         }
      }

      protected void GXDLVvPROJETO_GERENTECOD_dataLC2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00LC6 */
         pr_default.execute(4);
         while ( (pr_default.getStatus(4) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00LC6_A69ContratadaUsuario_UsuarioCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00LC6_A71ContratadaUsuario_UsuarioPessoaNom[0]));
            pr_default.readNext(4);
         }
         pr_default.close(4);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavProjeto_codigo.ItemCount > 0 )
         {
            AV15Projeto_Codigo = (int)(NumberUtil.Val( dynavProjeto_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15Projeto_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Projeto_Codigo), 6, 0)));
         }
         if ( dynavTipoprojeto_codigo.ItemCount > 0 )
         {
            AV19TipoProjeto_Codigo = (int)(NumberUtil.Val( dynavTipoprojeto_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV19TipoProjeto_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19TipoProjeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19TipoProjeto_Codigo), 6, 0)));
         }
         if ( dynavProjeto_servicocod.ItemCount > 0 )
         {
            AV14Projeto_ServicoCod = (int)(NumberUtil.Val( dynavProjeto_servicocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14Projeto_ServicoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Projeto_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Projeto_ServicoCod), 6, 0)));
         }
         if ( dynavProjeto_gerentecod.ItemCount > 0 )
         {
            AV13Projeto_GerenteCod = (int)(NumberUtil.Val( dynavProjeto_gerentecod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13Projeto_GerenteCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Projeto_GerenteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Projeto_GerenteCod), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFLC2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFLC2( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E13LC2 */
         E13LC2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E15LC2 */
            E15LC2 ();
            WBLC0( ) ;
         }
      }

      protected void STRUPLC0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         GXVvPROJETO_CODIGO_htmlLC2( ) ;
         GXVvPROJETO_SERVICOCOD_htmlLC2( ) ;
         GXVvPROJETO_GERENTECOD_htmlLC2( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11LC2 */
         E11LC2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            dynavProjeto_codigo.CurrentValue = cgiGet( dynavProjeto_codigo_Internalname);
            AV15Projeto_Codigo = (int)(NumberUtil.Val( cgiGet( dynavProjeto_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Projeto_Codigo), 6, 0)));
            dynavTipoprojeto_codigo.CurrentValue = cgiGet( dynavTipoprojeto_codigo_Internalname);
            AV19TipoProjeto_Codigo = (int)(NumberUtil.Val( cgiGet( dynavTipoprojeto_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19TipoProjeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19TipoProjeto_Codigo), 6, 0)));
            AV11Projeto_Nome = StringUtil.Upper( cgiGet( edtavProjeto_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Projeto_Nome", AV11Projeto_Nome);
            AV10Projeto_Sigla = StringUtil.Upper( cgiGet( edtavProjeto_sigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Projeto_Sigla", AV10Projeto_Sigla);
            dynavProjeto_servicocod.CurrentValue = cgiGet( dynavProjeto_servicocod_Internalname);
            AV14Projeto_ServicoCod = (int)(NumberUtil.Val( cgiGet( dynavProjeto_servicocod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Projeto_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Projeto_ServicoCod), 6, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavProjeto_previsao_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Projeto_Previsao"}), 1, "vPROJETO_PREVISAO");
               GX_FocusControl = edtavProjeto_previsao_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV12Projeto_Previsao = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Projeto_Previsao", context.localUtil.Format(AV12Projeto_Previsao, "99/99/99"));
            }
            else
            {
               AV12Projeto_Previsao = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavProjeto_previsao_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Projeto_Previsao", context.localUtil.Format(AV12Projeto_Previsao, "99/99/99"));
            }
            dynavProjeto_gerentecod.CurrentValue = cgiGet( dynavProjeto_gerentecod_Internalname);
            AV13Projeto_GerenteCod = (int)(NumberUtil.Val( cgiGet( dynavProjeto_gerentecod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Projeto_GerenteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Projeto_GerenteCod), 6, 0)));
            /* Read saved values. */
            AV9Projeto_Introducao = cgiGet( "vPROJETO_INTRODUCAO");
            AV8Projeto_Escopo = cgiGet( "vPROJETO_ESCOPO");
            Projeto_introducao_Enabled = StringUtil.StrToBool( cgiGet( "PROJETO_INTRODUCAO_Enabled"));
            Projeto_escopo_Enabled = StringUtil.StrToBool( cgiGet( "PROJETO_ESCOPO_Enabled"));
            Gxuitabspanel_tab_Width = cgiGet( "GXUITABSPANEL_TAB_Width");
            Gxuitabspanel_tab_Cls = cgiGet( "GXUITABSPANEL_TAB_Cls");
            Gxuitabspanel_tab_Autowidth = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TAB_Autowidth"));
            Gxuitabspanel_tab_Autoheight = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TAB_Autoheight"));
            Gxuitabspanel_tab_Autoscroll = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TAB_Autoscroll"));
            Gxuitabspanel_tab_Designtimetabs = cgiGet( "GXUITABSPANEL_TAB_Designtimetabs");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXVvPROJETO_CODIGO_htmlLC2( ) ;
            GXVvPROJETO_SERVICOCOD_htmlLC2( ) ;
            GXVvPROJETO_GERENTECOD_htmlLC2( ) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11LC2 */
         E11LC2 ();
         if (returnInSub) return;
      }

      protected void E11LC2( )
      {
         /* Start Routine */
         Form.Caption = "OS "+AV18Demanda;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script language=\"javascript\" type=\"text/javascript\"> document.getElementById(\"GXUITABSPANEL_TABContainer\").setAttribute(\"class\",\"gx_usercontrol tab-container\");</script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
      }

      public void GXEnter( )
      {
         /* Execute user event: E12LC2 */
         E12LC2 ();
         if (returnInSub) return;
      }

      protected void E12LC2( )
      {
         /* Enter Routine */
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV10Projeto_Sigla)) )
         {
            GX_msglist.addItem("Campo Sigla � obrigat�rio!");
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11Projeto_Nome)) )
         {
            GX_msglist.addItem("Campo Nome � obrigat�rio!");
         }
         else if ( (0==AV14Projeto_ServicoCod) )
         {
            GX_msglist.addItem("Campo Servico � obrigat�rio!");
         }
         else if ( (DateTime.MinValue==AV12Projeto_Previsao) )
         {
            GX_msglist.addItem("Campo Previs�o � obrigat�rio!");
         }
         else
         {
            AV16Projeto = new SdtProjeto(context);
            AV16Projeto.gxTpr_Projeto_nome = AV11Projeto_Nome;
            AV16Projeto.gxTpr_Projeto_sigla = AV10Projeto_Sigla;
            AV16Projeto.gxTpr_Projeto_servicocod = AV14Projeto_ServicoCod;
            AV16Projeto.gxTpr_Projeto_previsao = AV12Projeto_Previsao;
            AV16Projeto.gxTpr_Projeto_introducao = AV9Projeto_Introducao;
            AV16Projeto.gxTpr_Projeto_escopo = AV8Projeto_Escopo;
            AV16Projeto.gxTv_SdtProjeto_Projeto_tipoprojetocod_SetNull();
            AV16Projeto.gxTpr_Projeto_tecnicacontagem = "2";
            AV16Projeto.gxTpr_Projeto_status = "A";
            AV16Projeto.Save();
            context.CommitDataStores( "WP_NovoProjeto");
            new prc_associarprojetocomos(context ).execute(  AV16Projeto.gxTpr_Projeto_codigo, ref  AV17Codigo) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Codigo), 6, 0)));
         }
      }

      protected void S112( )
      {
         /* 'SHOW MESSAGES' Routine */
         AV22GXV1 = 1;
         while ( AV22GXV1 <= AV6Messages.Count )
         {
            AV5Message = ((SdtMessages_Message)AV6Messages.Item(AV22GXV1));
            GX_msglist.addItem(AV5Message.gxTpr_Description);
            AV22GXV1 = (int)(AV22GXV1+1);
         }
      }

      protected void E13LC2( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV7Context) ;
      }

      protected void E14LC2( )
      {
         /* Projeto_codigo_Isvalid Routine */
         if ( (0==AV15Projeto_Codigo) )
         {
            AV10Projeto_Sigla = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Projeto_Sigla", AV10Projeto_Sigla);
            AV11Projeto_Nome = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Projeto_Nome", AV11Projeto_Nome);
            AV12Projeto_Previsao = DateTime.MinValue;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Projeto_Previsao", context.localUtil.Format(AV12Projeto_Previsao, "99/99/99"));
            AV13Projeto_GerenteCod = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Projeto_GerenteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Projeto_GerenteCod), 6, 0)));
            AV14Projeto_ServicoCod = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Projeto_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Projeto_ServicoCod), 6, 0)));
            AV9Projeto_Introducao = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Projeto_Introducao", AV9Projeto_Introducao);
            AV8Projeto_Escopo = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Projeto_Escopo", AV8Projeto_Escopo);
         }
         else
         {
            /* Using cursor H00LC7 */
            pr_default.execute(5, new Object[] {AV15Projeto_Codigo});
            while ( (pr_default.getStatus(5) != 101) )
            {
               A648Projeto_Codigo = H00LC7_A648Projeto_Codigo[0];
               A650Projeto_Sigla = H00LC7_A650Projeto_Sigla[0];
               A649Projeto_Nome = H00LC7_A649Projeto_Nome[0];
               A1541Projeto_Previsao = H00LC7_A1541Projeto_Previsao[0];
               n1541Projeto_Previsao = H00LC7_n1541Projeto_Previsao[0];
               A1542Projeto_GerenteCod = H00LC7_A1542Projeto_GerenteCod[0];
               n1542Projeto_GerenteCod = H00LC7_n1542Projeto_GerenteCod[0];
               A1543Projeto_ServicoCod = H00LC7_A1543Projeto_ServicoCod[0];
               n1543Projeto_ServicoCod = H00LC7_n1543Projeto_ServicoCod[0];
               A1540Projeto_Introducao = H00LC7_A1540Projeto_Introducao[0];
               n1540Projeto_Introducao = H00LC7_n1540Projeto_Introducao[0];
               A653Projeto_Escopo = H00LC7_A653Projeto_Escopo[0];
               AV10Projeto_Sigla = A650Projeto_Sigla;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Projeto_Sigla", AV10Projeto_Sigla);
               AV11Projeto_Nome = A649Projeto_Nome;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Projeto_Nome", AV11Projeto_Nome);
               AV12Projeto_Previsao = A1541Projeto_Previsao;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Projeto_Previsao", context.localUtil.Format(AV12Projeto_Previsao, "99/99/99"));
               AV13Projeto_GerenteCod = A1542Projeto_GerenteCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Projeto_GerenteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Projeto_GerenteCod), 6, 0)));
               AV14Projeto_ServicoCod = A1543Projeto_ServicoCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Projeto_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Projeto_ServicoCod), 6, 0)));
               AV9Projeto_Introducao = A1540Projeto_Introducao;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Projeto_Introducao", AV9Projeto_Introducao);
               AV8Projeto_Escopo = A653Projeto_Escopo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Projeto_Escopo", AV8Projeto_Escopo);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(5);
         }
         edtavProjeto_sigla_Enabled = ((0==AV15Projeto_Codigo) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_sigla_Enabled), 5, 0)));
         edtavProjeto_nome_Enabled = ((0==AV15Projeto_Codigo) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_nome_Enabled), 5, 0)));
         edtavProjeto_previsao_Enabled = ((0==AV15Projeto_Codigo) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_previsao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_previsao_Enabled), 5, 0)));
         dynavProjeto_gerentecod.Enabled = ((0==AV15Projeto_Codigo) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavProjeto_gerentecod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavProjeto_gerentecod.Enabled), 5, 0)));
         dynavProjeto_servicocod.Enabled = ((0==AV15Projeto_Codigo) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavProjeto_servicocod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavProjeto_servicocod.Enabled), 5, 0)));
         Projeto_introducao_Enabled = (0==AV15Projeto_Codigo);
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Projeto_introducao_Internalname, "Enabled", StringUtil.BoolToStr( Projeto_introducao_Enabled));
         Projeto_escopo_Enabled = (0==AV15Projeto_Codigo);
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Projeto_escopo_Internalname, "Enabled", StringUtil.BoolToStr( Projeto_escopo_Enabled));
         dynavProjeto_gerentecod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13Projeto_GerenteCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavProjeto_gerentecod_Internalname, "Values", dynavProjeto_gerentecod.ToJavascriptSource());
         dynavProjeto_servicocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14Projeto_ServicoCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavProjeto_servicocod_Internalname, "Values", dynavProjeto_servicocod.ToJavascriptSource());
      }

      protected void nextLoad( )
      {
      }

      protected void E15LC2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_LC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(670), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table2_5_LC2( true) ;
         }
         else
         {
            wb_table2_5_LC2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_LC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:bottom")+"\">") ;
            wb_table3_64_LC2( true) ;
         }
         else
         {
            wb_table3_64_LC2( false) ;
         }
         return  ;
      }

      protected void wb_table3_64_LC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_NovoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_LC2e( true) ;
         }
         else
         {
            wb_table1_2_LC2e( false) ;
         }
      }

      protected void wb_table3_64_LC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenter_Internalname, "", "Confirmar", bttBtnenter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_NovoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_NovoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_64_LC2e( true) ;
         }
         else
         {
            wb_table3_64_LC2e( false) ;
         }
      }

      protected void wb_table2_5_LC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GXUITABSPANEL_TABContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABContainer"+"TitleDados"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Dados") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABContainer"+"Dados"+"\" style=\"display:none;\">") ;
            wb_table4_14_LC2( true) ;
         }
         else
         {
            wb_table4_14_LC2( false) ;
         }
         return  ;
      }

      protected void wb_table4_14_LC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABContainer"+"TitleIntroducao"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Introdu��o") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABContainer"+"Introducao"+"\" style=\"display:none;\">") ;
            wb_table5_52_LC2( true) ;
         }
         else
         {
            wb_table5_52_LC2( false) ;
         }
         return  ;
      }

      protected void wb_table5_52_LC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABContainer"+"TitleEscopo"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Escopo") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABContainer"+"Escopo"+"\" style=\"display:none;\">") ;
            wb_table6_58_LC2( true) ;
         }
         else
         {
            wb_table6_58_LC2( false) ;
         }
         return  ;
      }

      protected void wb_table6_58_LC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_LC2e( true) ;
         }
         else
         {
            wb_table2_5_LC2e( false) ;
         }
      }

      protected void wb_table6_58_LC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='DataContentCell'>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"PROJETO_ESCOPOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_58_LC2e( true) ;
         }
         else
         {
            wb_table6_58_LC2e( false) ;
         }
      }

      protected void wb_table5_52_LC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='DataContentCell'>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"PROJETO_INTRODUCAOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_52_LC2e( true) ;
         }
         else
         {
            wb_table5_52_LC2e( false) ;
         }
      }

      protected void wb_table4_14_LC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable3_Internalname, tblUnnamedtable3_Internalname, "", "TableMain", 0, "", "", 10, 10, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_codigo_Internalname, "Projeto", "", "", lblTextblockprojeto_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavProjeto_codigo, dynavProjeto_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV15Projeto_Codigo), 6, 0)), 1, dynavProjeto_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "", true, "HLP_WP_NovoProjeto.htm");
            dynavProjeto_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15Projeto_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavProjeto_codigo_Internalname, "Values", (String)(dynavProjeto_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktipoprojeto_codigo_Internalname, "Tipo", "", "", lblTextblocktipoprojeto_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavTipoprojeto_codigo, dynavTipoprojeto_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV19TipoProjeto_Codigo), 6, 0)), 1, dynavTipoprojeto_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_WP_NovoProjeto.htm");
            dynavTipoprojeto_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV19TipoProjeto_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipoprojeto_codigo_Internalname, "Values", (String)(dynavTipoprojeto_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_nome_Internalname, "Nome", "", "", lblTextblockprojeto_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavProjeto_nome_Internalname, StringUtil.RTrim( AV11Projeto_Nome), StringUtil.RTrim( context.localUtil.Format( AV11Projeto_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,29);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavProjeto_nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavProjeto_nome_Enabled, 1, "text", "", 100, "%", 1, "row", 50, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_WP_NovoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_sigla_Internalname, "Sigla", "", "", lblTextblockprojeto_sigla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavProjeto_sigla_Internalname, StringUtil.RTrim( AV10Projeto_Sigla), StringUtil.RTrim( context.localUtil.Format( AV10Projeto_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavProjeto_sigla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavProjeto_sigla_Enabled, 1, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_NovoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_servicocod_Internalname, "Servi�o", "", "", lblTextblockprojeto_servicocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavProjeto_servicocod, dynavProjeto_servicocod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14Projeto_ServicoCod), 6, 0)), 1, dynavProjeto_servicocod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynavProjeto_servicocod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "", true, "HLP_WP_NovoProjeto.htm");
            dynavProjeto_servicocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14Projeto_ServicoCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavProjeto_servicocod_Internalname, "Values", (String)(dynavProjeto_servicocod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_previsao_Internalname, "Previs�o", "", "", lblTextblockprojeto_previsao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavProjeto_previsao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavProjeto_previsao_Internalname, context.localUtil.Format(AV12Projeto_Previsao, "99/99/99"), context.localUtil.Format( AV12Projeto_Previsao, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavProjeto_previsao_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtavProjeto_previsao_Enabled, 1, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_NovoProjeto.htm");
            GxWebStd.gx_bitmap( context, edtavProjeto_previsao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavProjeto_previsao_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_NovoProjeto.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_gerentecod_Internalname, "Gerente", "", "", lblTextblockprojeto_gerentecod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavProjeto_gerentecod, dynavProjeto_gerentecod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13Projeto_GerenteCod), 6, 0)), 1, dynavProjeto_gerentecod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynavProjeto_gerentecod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_WP_NovoProjeto.htm");
            dynavProjeto_gerentecod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13Projeto_GerenteCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavProjeto_gerentecod_Internalname, "Values", (String)(dynavProjeto_gerentecod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_14_LC2e( true) ;
         }
         else
         {
            wb_table4_14_LC2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV17Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Codigo), 6, 0)));
         AV18Demanda = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Demanda", AV18Demanda);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vDEMANDA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV18Demanda, "@!"))));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PALC2( ) ;
         WSLC2( ) ;
         WELC2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020529943688");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_novoprojeto.js", "?2020529943688");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/TabsPanel/BootstrapTabsPanelRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockprojeto_codigo_Internalname = "TEXTBLOCKPROJETO_CODIGO";
         dynavProjeto_codigo_Internalname = "vPROJETO_CODIGO";
         lblTextblocktipoprojeto_codigo_Internalname = "TEXTBLOCKTIPOPROJETO_CODIGO";
         dynavTipoprojeto_codigo_Internalname = "vTIPOPROJETO_CODIGO";
         lblTextblockprojeto_nome_Internalname = "TEXTBLOCKPROJETO_NOME";
         edtavProjeto_nome_Internalname = "vPROJETO_NOME";
         lblTextblockprojeto_sigla_Internalname = "TEXTBLOCKPROJETO_SIGLA";
         edtavProjeto_sigla_Internalname = "vPROJETO_SIGLA";
         lblTextblockprojeto_servicocod_Internalname = "TEXTBLOCKPROJETO_SERVICOCOD";
         dynavProjeto_servicocod_Internalname = "vPROJETO_SERVICOCOD";
         lblTextblockprojeto_previsao_Internalname = "TEXTBLOCKPROJETO_PREVISAO";
         edtavProjeto_previsao_Internalname = "vPROJETO_PREVISAO";
         lblTextblockprojeto_gerentecod_Internalname = "TEXTBLOCKPROJETO_GERENTECOD";
         dynavProjeto_gerentecod_Internalname = "vPROJETO_GERENTECOD";
         tblUnnamedtable3_Internalname = "UNNAMEDTABLE3";
         Projeto_introducao_Internalname = "PROJETO_INTRODUCAO";
         tblUnnamedtable2_Internalname = "UNNAMEDTABLE2";
         Projeto_escopo_Internalname = "PROJETO_ESCOPO";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         Gxuitabspanel_tab_Internalname = "GXUITABSPANEL_TAB";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtnenter_Internalname = "BTNENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblTbjava_Internalname = "TBJAVA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         dynavProjeto_gerentecod_Jsonclick = "";
         edtavProjeto_previsao_Jsonclick = "";
         dynavProjeto_servicocod_Jsonclick = "";
         edtavProjeto_sigla_Jsonclick = "";
         edtavProjeto_nome_Jsonclick = "";
         dynavTipoprojeto_codigo_Jsonclick = "";
         dynavProjeto_codigo_Jsonclick = "";
         Projeto_introducao_Enabled = Convert.ToBoolean( 1);
         Projeto_escopo_Enabled = Convert.ToBoolean( 1);
         lblTbjava_Visible = 1;
         dynavProjeto_servicocod.Enabled = 1;
         dynavProjeto_gerentecod.Enabled = 1;
         edtavProjeto_previsao_Enabled = 1;
         edtavProjeto_nome_Enabled = 1;
         edtavProjeto_sigla_Enabled = 1;
         lblTbjava_Caption = "tbJava";
         Gxuitabspanel_tab_Designtimetabs = "[{\"id\":\"Dados\"},{\"id\":\"Introducao\"},{\"id\":\"Escopo\"}]";
         Gxuitabspanel_tab_Autoscroll = Convert.ToBoolean( -1);
         Gxuitabspanel_tab_Autoheight = Convert.ToBoolean( -1);
         Gxuitabspanel_tab_Autowidth = Convert.ToBoolean( 0);
         Gxuitabspanel_tab_Cls = "GXUI-DVelop-Tabs";
         Gxuitabspanel_tab_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "OS";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E12LC2',iparms:[{av:'AV10Projeto_Sigla',fld:'vPROJETO_SIGLA',pic:'@!',nv:''},{av:'AV11Projeto_Nome',fld:'vPROJETO_NOME',pic:'@!',nv:''},{av:'AV14Projeto_ServicoCod',fld:'vPROJETO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV12Projeto_Previsao',fld:'vPROJETO_PREVISAO',pic:'',nv:''},{av:'AV9Projeto_Introducao',fld:'vPROJETO_INTRODUCAO',pic:'',nv:''},{av:'AV8Projeto_Escopo',fld:'vPROJETO_ESCOPO',pic:'',nv:''},{av:'AV17Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV17Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VPROJETO_CODIGO.ISVALID","{handler:'E14LC2',iparms:[{av:'AV15Projeto_Codigo',fld:'vPROJETO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A650Projeto_Sigla',fld:'PROJETO_SIGLA',pic:'@!',nv:''},{av:'A649Projeto_Nome',fld:'PROJETO_NOME',pic:'@!',nv:''},{av:'A1541Projeto_Previsao',fld:'PROJETO_PREVISAO',pic:'',nv:''},{av:'A1542Projeto_GerenteCod',fld:'PROJETO_GERENTECOD',pic:'ZZZZZ9',nv:0},{av:'A1543Projeto_ServicoCod',fld:'PROJETO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'A1540Projeto_Introducao',fld:'PROJETO_INTRODUCAO',pic:'',nv:''},{av:'A653Projeto_Escopo',fld:'PROJETO_ESCOPO',pic:'',nv:''}],oparms:[{av:'AV10Projeto_Sigla',fld:'vPROJETO_SIGLA',pic:'@!',nv:''},{av:'AV11Projeto_Nome',fld:'vPROJETO_NOME',pic:'@!',nv:''},{av:'AV12Projeto_Previsao',fld:'vPROJETO_PREVISAO',pic:'',nv:''},{av:'AV13Projeto_GerenteCod',fld:'vPROJETO_GERENTECOD',pic:'ZZZZZ9',nv:0},{av:'AV14Projeto_ServicoCod',fld:'vPROJETO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV9Projeto_Introducao',fld:'vPROJETO_INTRODUCAO',pic:'',nv:''},{av:'AV8Projeto_Escopo',fld:'vPROJETO_ESCOPO',pic:'',nv:''},{av:'edtavProjeto_sigla_Enabled',ctrl:'vPROJETO_SIGLA',prop:'Enabled'},{av:'edtavProjeto_nome_Enabled',ctrl:'vPROJETO_NOME',prop:'Enabled'},{av:'edtavProjeto_previsao_Enabled',ctrl:'vPROJETO_PREVISAO',prop:'Enabled'},{av:'dynavProjeto_gerentecod'},{av:'dynavProjeto_servicocod'},{av:'Projeto_introducao_Enabled',ctrl:'vPROJETO_INTRODUCAO',prop:'Enabled'},{av:'Projeto_escopo_Enabled',ctrl:'vPROJETO_ESCOPO',prop:'Enabled'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV18Demanda = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV9Projeto_Introducao = "";
         AV8Projeto_Escopo = "";
         A650Projeto_Sigla = "";
         A649Projeto_Nome = "";
         A1541Projeto_Previsao = DateTime.MinValue;
         A1540Projeto_Introducao = "";
         A653Projeto_Escopo = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00LC2_A979TipoProjeto_Codigo = new int[1] ;
         H00LC2_A980TipoProjeto_Nome = new String[] {""} ;
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         H00LC3_A979TipoProjeto_Codigo = new int[1] ;
         H00LC3_A980TipoProjeto_Nome = new String[] {""} ;
         H00LC4_A648Projeto_Codigo = new int[1] ;
         H00LC4_A650Projeto_Sigla = new String[] {""} ;
         H00LC5_A155Servico_Codigo = new int[1] ;
         H00LC5_A160ContratoServicos_Codigo = new int[1] ;
         H00LC5_A827ContratoServicos_ServicoCod = new int[1] ;
         H00LC5_A826ContratoServicos_ServicoSigla = new String[] {""} ;
         H00LC6_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00LC6_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00LC6_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00LC6_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00LC6_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00LC6_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         AV11Projeto_Nome = "";
         AV10Projeto_Sigla = "";
         AV12Projeto_Previsao = DateTime.MinValue;
         AV16Projeto = new SdtProjeto(context);
         AV6Messages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV5Message = new SdtMessages_Message(context);
         AV7Context = new wwpbaseobjects.SdtWWPContext(context);
         H00LC7_A648Projeto_Codigo = new int[1] ;
         H00LC7_A650Projeto_Sigla = new String[] {""} ;
         H00LC7_A649Projeto_Nome = new String[] {""} ;
         H00LC7_A1541Projeto_Previsao = new DateTime[] {DateTime.MinValue} ;
         H00LC7_n1541Projeto_Previsao = new bool[] {false} ;
         H00LC7_A1542Projeto_GerenteCod = new int[1] ;
         H00LC7_n1542Projeto_GerenteCod = new bool[] {false} ;
         H00LC7_A1543Projeto_ServicoCod = new int[1] ;
         H00LC7_n1543Projeto_ServicoCod = new bool[] {false} ;
         H00LC7_A1540Projeto_Introducao = new String[] {""} ;
         H00LC7_n1540Projeto_Introducao = new bool[] {false} ;
         H00LC7_A653Projeto_Escopo = new String[] {""} ;
         sStyleString = "";
         lblTbjava_Jsonclick = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtnenter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         lblTextblockprojeto_codigo_Jsonclick = "";
         lblTextblocktipoprojeto_codigo_Jsonclick = "";
         lblTextblockprojeto_nome_Jsonclick = "";
         lblTextblockprojeto_sigla_Jsonclick = "";
         lblTextblockprojeto_servicocod_Jsonclick = "";
         lblTextblockprojeto_previsao_Jsonclick = "";
         lblTextblockprojeto_gerentecod_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_novoprojeto__default(),
            new Object[][] {
                new Object[] {
               H00LC2_A979TipoProjeto_Codigo, H00LC2_A980TipoProjeto_Nome
               }
               , new Object[] {
               H00LC3_A979TipoProjeto_Codigo, H00LC3_A980TipoProjeto_Nome
               }
               , new Object[] {
               H00LC4_A648Projeto_Codigo, H00LC4_A650Projeto_Sigla
               }
               , new Object[] {
               H00LC5_A155Servico_Codigo, H00LC5_A160ContratoServicos_Codigo, H00LC5_A827ContratoServicos_ServicoCod, H00LC5_A826ContratoServicos_ServicoSigla
               }
               , new Object[] {
               H00LC6_A70ContratadaUsuario_UsuarioPessoaCod, H00LC6_n70ContratadaUsuario_UsuarioPessoaCod, H00LC6_A66ContratadaUsuario_ContratadaCod, H00LC6_A69ContratadaUsuario_UsuarioCod, H00LC6_A71ContratadaUsuario_UsuarioPessoaNom, H00LC6_n71ContratadaUsuario_UsuarioPessoaNom
               }
               , new Object[] {
               H00LC7_A648Projeto_Codigo, H00LC7_A650Projeto_Sigla, H00LC7_A649Projeto_Nome, H00LC7_A1541Projeto_Previsao, H00LC7_n1541Projeto_Previsao, H00LC7_A1542Projeto_GerenteCod, H00LC7_n1542Projeto_GerenteCod, H00LC7_A1543Projeto_ServicoCod, H00LC7_n1543Projeto_ServicoCod, H00LC7_A1540Projeto_Introducao,
               H00LC7_n1540Projeto_Introducao, H00LC7_A653Projeto_Escopo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int AV17Codigo ;
      private int wcpOAV17Codigo ;
      private int A648Projeto_Codigo ;
      private int A1542Projeto_GerenteCod ;
      private int A1543Projeto_ServicoCod ;
      private int AV19TipoProjeto_Codigo ;
      private int gxdynajaxindex ;
      private int AV15Projeto_Codigo ;
      private int AV14Projeto_ServicoCod ;
      private int AV13Projeto_GerenteCod ;
      private int lblTbjava_Visible ;
      private int AV22GXV1 ;
      private int edtavProjeto_sigla_Enabled ;
      private int edtavProjeto_nome_Enabled ;
      private int edtavProjeto_previsao_Enabled ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A650Projeto_Sigla ;
      private String A649Projeto_Nome ;
      private String Gxuitabspanel_tab_Width ;
      private String Gxuitabspanel_tab_Cls ;
      private String Gxuitabspanel_tab_Designtimetabs ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String dynavProjeto_codigo_Internalname ;
      private String gxwrpcisep ;
      private String dynavTipoprojeto_codigo_Internalname ;
      private String AV11Projeto_Nome ;
      private String edtavProjeto_nome_Internalname ;
      private String AV10Projeto_Sigla ;
      private String edtavProjeto_sigla_Internalname ;
      private String dynavProjeto_servicocod_Internalname ;
      private String edtavProjeto_previsao_Internalname ;
      private String dynavProjeto_gerentecod_Internalname ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String Projeto_introducao_Internalname ;
      private String Projeto_escopo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblTbjava_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtnenter_Internalname ;
      private String bttBtnenter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblUnnamedtable1_Internalname ;
      private String tblUnnamedtable2_Internalname ;
      private String tblUnnamedtable3_Internalname ;
      private String lblTextblockprojeto_codigo_Internalname ;
      private String lblTextblockprojeto_codigo_Jsonclick ;
      private String dynavProjeto_codigo_Jsonclick ;
      private String lblTextblocktipoprojeto_codigo_Internalname ;
      private String lblTextblocktipoprojeto_codigo_Jsonclick ;
      private String dynavTipoprojeto_codigo_Jsonclick ;
      private String lblTextblockprojeto_nome_Internalname ;
      private String lblTextblockprojeto_nome_Jsonclick ;
      private String edtavProjeto_nome_Jsonclick ;
      private String lblTextblockprojeto_sigla_Internalname ;
      private String lblTextblockprojeto_sigla_Jsonclick ;
      private String edtavProjeto_sigla_Jsonclick ;
      private String lblTextblockprojeto_servicocod_Internalname ;
      private String lblTextblockprojeto_servicocod_Jsonclick ;
      private String dynavProjeto_servicocod_Jsonclick ;
      private String lblTextblockprojeto_previsao_Internalname ;
      private String lblTextblockprojeto_previsao_Jsonclick ;
      private String edtavProjeto_previsao_Jsonclick ;
      private String lblTextblockprojeto_gerentecod_Internalname ;
      private String lblTextblockprojeto_gerentecod_Jsonclick ;
      private String dynavProjeto_gerentecod_Jsonclick ;
      private String Gxuitabspanel_tab_Internalname ;
      private DateTime A1541Projeto_Previsao ;
      private DateTime AV12Projeto_Previsao ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool Projeto_introducao_Enabled ;
      private bool Projeto_escopo_Enabled ;
      private bool Gxuitabspanel_tab_Autowidth ;
      private bool Gxuitabspanel_tab_Autoheight ;
      private bool Gxuitabspanel_tab_Autoscroll ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n1541Projeto_Previsao ;
      private bool n1542Projeto_GerenteCod ;
      private bool n1543Projeto_ServicoCod ;
      private bool n1540Projeto_Introducao ;
      private String AV9Projeto_Introducao ;
      private String AV8Projeto_Escopo ;
      private String A1540Projeto_Introducao ;
      private String A653Projeto_Escopo ;
      private String AV18Demanda ;
      private String wcpOAV18Demanda ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavProjeto_codigo ;
      private GXCombobox dynavTipoprojeto_codigo ;
      private GXCombobox dynavProjeto_servicocod ;
      private GXCombobox dynavProjeto_gerentecod ;
      private IDataStoreProvider pr_default ;
      private int[] H00LC2_A979TipoProjeto_Codigo ;
      private String[] H00LC2_A980TipoProjeto_Nome ;
      private int[] H00LC3_A979TipoProjeto_Codigo ;
      private String[] H00LC3_A980TipoProjeto_Nome ;
      private int[] H00LC4_A648Projeto_Codigo ;
      private String[] H00LC4_A650Projeto_Sigla ;
      private int[] H00LC5_A155Servico_Codigo ;
      private int[] H00LC5_A160ContratoServicos_Codigo ;
      private int[] H00LC5_A827ContratoServicos_ServicoCod ;
      private String[] H00LC5_A826ContratoServicos_ServicoSigla ;
      private int[] H00LC6_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00LC6_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H00LC6_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00LC6_A69ContratadaUsuario_UsuarioCod ;
      private String[] H00LC6_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00LC6_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] H00LC7_A648Projeto_Codigo ;
      private String[] H00LC7_A650Projeto_Sigla ;
      private String[] H00LC7_A649Projeto_Nome ;
      private DateTime[] H00LC7_A1541Projeto_Previsao ;
      private bool[] H00LC7_n1541Projeto_Previsao ;
      private int[] H00LC7_A1542Projeto_GerenteCod ;
      private bool[] H00LC7_n1542Projeto_GerenteCod ;
      private int[] H00LC7_A1543Projeto_ServicoCod ;
      private bool[] H00LC7_n1543Projeto_ServicoCod ;
      private String[] H00LC7_A1540Projeto_Introducao ;
      private bool[] H00LC7_n1540Projeto_Introducao ;
      private String[] H00LC7_A653Projeto_Escopo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV6Messages ;
      private GXWebForm Form ;
      private SdtProjeto AV16Projeto ;
      private SdtMessages_Message AV5Message ;
      private wwpbaseobjects.SdtWWPContext AV7Context ;
   }

   public class wp_novoprojeto__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00LC2 ;
          prmH00LC2 = new Object[] {
          } ;
          Object[] prmH00LC3 ;
          prmH00LC3 = new Object[] {
          } ;
          Object[] prmH00LC4 ;
          prmH00LC4 = new Object[] {
          } ;
          Object[] prmH00LC5 ;
          prmH00LC5 = new Object[] {
          } ;
          Object[] prmH00LC6 ;
          prmH00LC6 = new Object[] {
          } ;
          Object[] prmH00LC7 ;
          prmH00LC7 = new Object[] {
          new Object[] {"@AV15Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00LC2", "SELECT [TipoProjeto_Codigo], [TipoProjeto_Nome] FROM [TipoProjeto] WITH (NOLOCK) ORDER BY [TipoProjeto_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00LC2,0,0,true,false )
             ,new CursorDef("H00LC3", "SELECT [TipoProjeto_Codigo], [TipoProjeto_Nome] FROM [TipoProjeto] WITH (NOLOCK) ORDER BY [TipoProjeto_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00LC3,0,0,true,false )
             ,new CursorDef("H00LC4", "SELECT [Projeto_Codigo], [Projeto_Sigla] FROM [Projeto] WITH (NOLOCK) ORDER BY [Projeto_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00LC4,0,0,true,false )
             ,new CursorDef("H00LC5", "SELECT T2.[Servico_Codigo], T1.[ContratoServicos_Codigo], T1.[Servico_Codigo] AS ContratoServicos_ServicoCod, T2.[Servico_Sigla] AS ContratoServicos_ServicoSigla FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) ORDER BY [ContratoServicos_ServicoSigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00LC5,0,0,true,false )
             ,new CursorDef("H00LC6", "SELECT T3.[Pessoa_Codigo] AS ContratadaUsuario_UsuarioPessoaCod, T1.[ContratadaUsuario_ContratadaCod], T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPessoaNom FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) ORDER BY T3.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00LC6,0,0,true,false )
             ,new CursorDef("H00LC7", "SELECT TOP 1 [Projeto_Codigo], [Projeto_Sigla], [Projeto_Nome], [Projeto_Previsao], [Projeto_GerenteCod], [Projeto_ServicoCod], [Projeto_Introducao], [Projeto_Escopo] FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_Codigo] = @AV15Projeto_Codigo ORDER BY [Projeto_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00LC7,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 15) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getLongVarchar(8) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
