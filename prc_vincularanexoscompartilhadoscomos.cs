/*
               File: PRC_VincularAnexosCompartilhadosComOS
        Description: Vincular Anexos Compartilhados Com OS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:17.47
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_vincularanexoscompartilhadoscomos : GXProcedure
   {
      public prc_vincularanexoscompartilhadoscomos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_vincularanexoscompartilhadoscomos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         initialize();
         executePrivate();
      }

      public void executeSubmit( )
      {
         prc_vincularanexoscompartilhadoscomos objprc_vincularanexoscompartilhadoscomos;
         objprc_vincularanexoscompartilhadoscomos = new prc_vincularanexoscompartilhadoscomos();
         objprc_vincularanexoscompartilhadoscomos.context.SetSubmitInitialConfig(context);
         objprc_vincularanexoscompartilhadoscomos.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_vincularanexoscompartilhadoscomos);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_vincularanexoscompartilhadoscomos)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV12Codigos.FromXml(AV9WebSession.Get("Codigos"), "Collection");
         AV13Anexos.FromXml(AV9WebSession.Get("CodigosAnexos"), "Collection");
         AV16GXV1 = 1;
         while ( AV16GXV1 <= AV12Codigos.Count )
         {
            AV10ContagemResultado_Codigo = (int)(AV12Codigos.GetNumeric(AV16GXV1));
            AV11i = 1;
            while ( AV11i <= AV13Anexos.Count )
            {
               /*
                  INSERT RECORD ON TABLE AnexosDe

               */
               A1106Anexo_Codigo = (int)(AV13Anexos.GetNumeric(AV11i));
               A1109AnexoDe_Id = AV10ContagemResultado_Codigo;
               A1110AnexoDe_Tabela = 1;
               /* Using cursor P00AB2 */
               pr_default.execute(0, new Object[] {A1106Anexo_Codigo, A1109AnexoDe_Id, A1110AnexoDe_Tabela});
               pr_default.close(0);
               dsDefault.SmartCacheProvider.SetUpdated("AnexosDe") ;
               if ( (pr_default.getStatus(0) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               /* End Insert */
               AV11i = (short)(AV11i+1);
            }
            AV16GXV1 = (int)(AV16GXV1+1);
         }
         AV9WebSession.Remove("CodigosAnexos");
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_VincularAnexosCompartilhadosComOS");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV12Codigos = new GxSimpleCollection();
         AV9WebSession = context.GetSession();
         AV13Anexos = new GxSimpleCollection();
         Gx_emsg = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_vincularanexoscompartilhadoscomos__default(),
            new Object[][] {
                new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV11i ;
      private int AV16GXV1 ;
      private int AV10ContagemResultado_Codigo ;
      private int GX_INS134 ;
      private int A1106Anexo_Codigo ;
      private int A1109AnexoDe_Id ;
      private int A1110AnexoDe_Tabela ;
      private String Gx_emsg ;
      private IGxSession AV9WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV12Codigos ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV13Anexos ;
   }

   public class prc_vincularanexoscompartilhadoscomos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new UpdateCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00AB2 ;
          prmP00AB2 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AnexoDe_Id",SqlDbType.Int,6,0} ,
          new Object[] {"@AnexoDe_Tabela",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00AB2", "INSERT INTO [AnexosDe]([Anexo_Codigo], [AnexoDe_Id], [AnexoDe_Tabela]) VALUES(@Anexo_Codigo, @AnexoDe_Id, @AnexoDe_Tabela)", GxErrorMask.GX_NOMASK,prmP00AB2)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
       }
    }

 }

}
