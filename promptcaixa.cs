/*
               File: PromptCaixa
        Description: Selecione Fluxo de Caixa
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:40:33.18
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptcaixa : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptcaixa( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptcaixa( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutCaixa_Codigo ,
                           ref String aP1_InOutCaixa_Documento )
      {
         this.AV7InOutCaixa_Codigo = aP0_InOutCaixa_Codigo;
         this.AV8InOutCaixa_Documento = aP1_InOutCaixa_Documento;
         executePrivate();
         aP0_InOutCaixa_Codigo=this.AV7InOutCaixa_Codigo;
         aP1_InOutCaixa_Documento=this.AV8InOutCaixa_Documento;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxSuggest"+"_"+"vTFCAIXA_TIPODECONTACOD") == 0 )
            {
               A870TipodeConta_Codigo = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXSGVvTFCAIXA_TIPODECONTACODFQ0( A870TipodeConta_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxSuggest"+"_"+"vTFCAIXA_TIPODECONTACOD_SEL") == 0 )
            {
               A870TipodeConta_Codigo = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXSGVvTFCAIXA_TIPODECONTACOD_SELFQ0( A870TipodeConta_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxSuggest"+"_"+"CAIXA_TIPODECONTACOD") == 0 )
            {
               A870TipodeConta_Codigo = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXSGACAIXA_TIPODECONTACODFQ0( A870TipodeConta_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_80 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_80_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_80_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Caixa_Documento1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Caixa_Documento1", AV17Caixa_Documento1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
               AV21Caixa_Documento2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Caixa_Documento2", AV21Caixa_Documento2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
               AV25Caixa_Documento3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Caixa_Documento3", AV25Caixa_Documento3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV31TFCaixa_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFCaixa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFCaixa_Codigo), 6, 0)));
               AV32TFCaixa_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFCaixa_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFCaixa_Codigo_To), 6, 0)));
               AV35TFCaixa_Documento = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFCaixa_Documento", AV35TFCaixa_Documento);
               AV36TFCaixa_Documento_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFCaixa_Documento_Sel", AV36TFCaixa_Documento_Sel);
               AV39TFCaixa_Emissao = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFCaixa_Emissao", context.localUtil.Format(AV39TFCaixa_Emissao, "99/99/99"));
               AV40TFCaixa_Emissao_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFCaixa_Emissao_To", context.localUtil.Format(AV40TFCaixa_Emissao_To, "99/99/99"));
               AV45TFCaixa_Vencimento = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFCaixa_Vencimento", context.localUtil.Format(AV45TFCaixa_Vencimento, "99/99/99"));
               AV46TFCaixa_Vencimento_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFCaixa_Vencimento_To", context.localUtil.Format(AV46TFCaixa_Vencimento_To, "99/99/99"));
               AV51TFCaixa_TipoDeContaCod = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFCaixa_TipoDeContaCod", AV51TFCaixa_TipoDeContaCod);
               AV52TFCaixa_TipoDeContaCod_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFCaixa_TipoDeContaCod_Sel", AV52TFCaixa_TipoDeContaCod_Sel);
               AV55TFCaixa_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFCaixa_Descricao", AV55TFCaixa_Descricao);
               AV56TFCaixa_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFCaixa_Descricao_Sel", AV56TFCaixa_Descricao_Sel);
               AV59TFCaixa_Valor = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFCaixa_Valor", StringUtil.LTrim( StringUtil.Str( AV59TFCaixa_Valor, 18, 5)));
               AV60TFCaixa_Valor_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFCaixa_Valor_To", StringUtil.LTrim( StringUtil.Str( AV60TFCaixa_Valor_To, 18, 5)));
               AV33ddo_Caixa_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_Caixa_CodigoTitleControlIdToReplace", AV33ddo_Caixa_CodigoTitleControlIdToReplace);
               AV37ddo_Caixa_DocumentoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_Caixa_DocumentoTitleControlIdToReplace", AV37ddo_Caixa_DocumentoTitleControlIdToReplace);
               AV43ddo_Caixa_EmissaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_Caixa_EmissaoTitleControlIdToReplace", AV43ddo_Caixa_EmissaoTitleControlIdToReplace);
               AV49ddo_Caixa_VencimentoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_Caixa_VencimentoTitleControlIdToReplace", AV49ddo_Caixa_VencimentoTitleControlIdToReplace);
               AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace", AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace);
               AV57ddo_Caixa_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_Caixa_DescricaoTitleControlIdToReplace", AV57ddo_Caixa_DescricaoTitleControlIdToReplace);
               AV61ddo_Caixa_ValorTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_Caixa_ValorTitleControlIdToReplace", AV61ddo_Caixa_ValorTitleControlIdToReplace);
               AV69Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Caixa_Documento1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Caixa_Documento2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Caixa_Documento3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFCaixa_Codigo, AV32TFCaixa_Codigo_To, AV35TFCaixa_Documento, AV36TFCaixa_Documento_Sel, AV39TFCaixa_Emissao, AV40TFCaixa_Emissao_To, AV45TFCaixa_Vencimento, AV46TFCaixa_Vencimento_To, AV51TFCaixa_TipoDeContaCod, AV52TFCaixa_TipoDeContaCod_Sel, AV55TFCaixa_Descricao, AV56TFCaixa_Descricao_Sel, AV59TFCaixa_Valor, AV60TFCaixa_Valor_To, AV33ddo_Caixa_CodigoTitleControlIdToReplace, AV37ddo_Caixa_DocumentoTitleControlIdToReplace, AV43ddo_Caixa_EmissaoTitleControlIdToReplace, AV49ddo_Caixa_VencimentoTitleControlIdToReplace, AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace, AV57ddo_Caixa_DescricaoTitleControlIdToReplace, AV61ddo_Caixa_ValorTitleControlIdToReplace, AV69Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutCaixa_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutCaixa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutCaixa_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutCaixa_Documento = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutCaixa_Documento", AV8InOutCaixa_Documento);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAFQ2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV69Pgmname = "PromptCaixa";
               context.Gx_err = 0;
               WSFQ2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEFQ2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042823403365");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptcaixa.aspx") + "?" + UrlEncode("" +AV7InOutCaixa_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV8InOutCaixa_Documento))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCAIXA_DOCUMENTO1", AV17Caixa_Documento1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCAIXA_DOCUMENTO2", AV21Caixa_Documento2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCAIXA_DOCUMENTO3", AV25Caixa_Documento3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCAIXA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31TFCaixa_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCAIXA_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFCaixa_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCAIXA_DOCUMENTO", AV35TFCaixa_Documento);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCAIXA_DOCUMENTO_SEL", AV36TFCaixa_Documento_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCAIXA_EMISSAO", context.localUtil.Format(AV39TFCaixa_Emissao, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCAIXA_EMISSAO_TO", context.localUtil.Format(AV40TFCaixa_Emissao_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCAIXA_VENCIMENTO", context.localUtil.Format(AV45TFCaixa_Vencimento, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCAIXA_VENCIMENTO_TO", context.localUtil.Format(AV46TFCaixa_Vencimento_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCAIXA_TIPODECONTACOD", StringUtil.RTrim( AV51TFCaixa_TipoDeContaCod));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCAIXA_TIPODECONTACOD_SEL", StringUtil.RTrim( AV52TFCaixa_TipoDeContaCod_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCAIXA_DESCRICAO", AV55TFCaixa_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCAIXA_DESCRICAO_SEL", AV56TFCaixa_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCAIXA_VALOR", StringUtil.LTrim( StringUtil.NToC( AV59TFCaixa_Valor, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCAIXA_VALOR_TO", StringUtil.LTrim( StringUtil.NToC( AV60TFCaixa_Valor_To, 18, 5, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_80", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_80), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV64GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV65GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV62DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV62DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCAIXA_CODIGOTITLEFILTERDATA", AV30Caixa_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCAIXA_CODIGOTITLEFILTERDATA", AV30Caixa_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCAIXA_DOCUMENTOTITLEFILTERDATA", AV34Caixa_DocumentoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCAIXA_DOCUMENTOTITLEFILTERDATA", AV34Caixa_DocumentoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCAIXA_EMISSAOTITLEFILTERDATA", AV38Caixa_EmissaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCAIXA_EMISSAOTITLEFILTERDATA", AV38Caixa_EmissaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCAIXA_VENCIMENTOTITLEFILTERDATA", AV44Caixa_VencimentoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCAIXA_VENCIMENTOTITLEFILTERDATA", AV44Caixa_VencimentoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCAIXA_TIPODECONTACODTITLEFILTERDATA", AV50Caixa_TipoDeContaCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCAIXA_TIPODECONTACODTITLEFILTERDATA", AV50Caixa_TipoDeContaCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCAIXA_DESCRICAOTITLEFILTERDATA", AV54Caixa_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCAIXA_DESCRICAOTITLEFILTERDATA", AV54Caixa_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCAIXA_VALORTITLEFILTERDATA", AV58Caixa_ValorTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCAIXA_VALORTITLEFILTERDATA", AV58Caixa_ValorTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV69Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTCAIXA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutCaixa_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTCAIXA_DOCUMENTO", AV8InOutCaixa_Documento);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_CODIGO_Caption", StringUtil.RTrim( Ddo_caixa_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_CODIGO_Tooltip", StringUtil.RTrim( Ddo_caixa_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_CODIGO_Cls", StringUtil.RTrim( Ddo_caixa_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_caixa_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_caixa_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_caixa_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_caixa_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_caixa_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_caixa_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_caixa_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_caixa_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_CODIGO_Filtertype", StringUtil.RTrim( Ddo_caixa_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_caixa_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_caixa_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_CODIGO_Datalistfixedvalues", StringUtil.RTrim( Ddo_caixa_codigo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_CODIGO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_caixa_codigo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_CODIGO_Sortasc", StringUtil.RTrim( Ddo_caixa_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_caixa_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_CODIGO_Loadingdata", StringUtil.RTrim( Ddo_caixa_codigo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_caixa_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_caixa_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_caixa_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_CODIGO_Noresultsfound", StringUtil.RTrim( Ddo_caixa_codigo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_caixa_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DOCUMENTO_Caption", StringUtil.RTrim( Ddo_caixa_documento_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DOCUMENTO_Tooltip", StringUtil.RTrim( Ddo_caixa_documento_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DOCUMENTO_Cls", StringUtil.RTrim( Ddo_caixa_documento_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DOCUMENTO_Filteredtext_set", StringUtil.RTrim( Ddo_caixa_documento_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DOCUMENTO_Selectedvalue_set", StringUtil.RTrim( Ddo_caixa_documento_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DOCUMENTO_Dropdownoptionstype", StringUtil.RTrim( Ddo_caixa_documento_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DOCUMENTO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_caixa_documento_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DOCUMENTO_Includesortasc", StringUtil.BoolToStr( Ddo_caixa_documento_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DOCUMENTO_Includesortdsc", StringUtil.BoolToStr( Ddo_caixa_documento_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DOCUMENTO_Sortedstatus", StringUtil.RTrim( Ddo_caixa_documento_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DOCUMENTO_Includefilter", StringUtil.BoolToStr( Ddo_caixa_documento_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DOCUMENTO_Filtertype", StringUtil.RTrim( Ddo_caixa_documento_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DOCUMENTO_Filterisrange", StringUtil.BoolToStr( Ddo_caixa_documento_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DOCUMENTO_Includedatalist", StringUtil.BoolToStr( Ddo_caixa_documento_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DOCUMENTO_Datalisttype", StringUtil.RTrim( Ddo_caixa_documento_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DOCUMENTO_Datalistfixedvalues", StringUtil.RTrim( Ddo_caixa_documento_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DOCUMENTO_Datalistproc", StringUtil.RTrim( Ddo_caixa_documento_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DOCUMENTO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_caixa_documento_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DOCUMENTO_Sortasc", StringUtil.RTrim( Ddo_caixa_documento_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DOCUMENTO_Sortdsc", StringUtil.RTrim( Ddo_caixa_documento_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DOCUMENTO_Loadingdata", StringUtil.RTrim( Ddo_caixa_documento_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DOCUMENTO_Cleanfilter", StringUtil.RTrim( Ddo_caixa_documento_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DOCUMENTO_Rangefilterfrom", StringUtil.RTrim( Ddo_caixa_documento_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DOCUMENTO_Rangefilterto", StringUtil.RTrim( Ddo_caixa_documento_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DOCUMENTO_Noresultsfound", StringUtil.RTrim( Ddo_caixa_documento_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DOCUMENTO_Searchbuttontext", StringUtil.RTrim( Ddo_caixa_documento_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_EMISSAO_Caption", StringUtil.RTrim( Ddo_caixa_emissao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_EMISSAO_Tooltip", StringUtil.RTrim( Ddo_caixa_emissao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_EMISSAO_Cls", StringUtil.RTrim( Ddo_caixa_emissao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_EMISSAO_Filteredtext_set", StringUtil.RTrim( Ddo_caixa_emissao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_EMISSAO_Filteredtextto_set", StringUtil.RTrim( Ddo_caixa_emissao_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_EMISSAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_caixa_emissao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_EMISSAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_caixa_emissao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_EMISSAO_Includesortasc", StringUtil.BoolToStr( Ddo_caixa_emissao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_EMISSAO_Includesortdsc", StringUtil.BoolToStr( Ddo_caixa_emissao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_EMISSAO_Sortedstatus", StringUtil.RTrim( Ddo_caixa_emissao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_EMISSAO_Includefilter", StringUtil.BoolToStr( Ddo_caixa_emissao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_EMISSAO_Filtertype", StringUtil.RTrim( Ddo_caixa_emissao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_EMISSAO_Filterisrange", StringUtil.BoolToStr( Ddo_caixa_emissao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_EMISSAO_Includedatalist", StringUtil.BoolToStr( Ddo_caixa_emissao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_EMISSAO_Datalistfixedvalues", StringUtil.RTrim( Ddo_caixa_emissao_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_EMISSAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_caixa_emissao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_EMISSAO_Sortasc", StringUtil.RTrim( Ddo_caixa_emissao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_EMISSAO_Sortdsc", StringUtil.RTrim( Ddo_caixa_emissao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_EMISSAO_Loadingdata", StringUtil.RTrim( Ddo_caixa_emissao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_EMISSAO_Cleanfilter", StringUtil.RTrim( Ddo_caixa_emissao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_EMISSAO_Rangefilterfrom", StringUtil.RTrim( Ddo_caixa_emissao_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_EMISSAO_Rangefilterto", StringUtil.RTrim( Ddo_caixa_emissao_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_EMISSAO_Noresultsfound", StringUtil.RTrim( Ddo_caixa_emissao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_EMISSAO_Searchbuttontext", StringUtil.RTrim( Ddo_caixa_emissao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VENCIMENTO_Caption", StringUtil.RTrim( Ddo_caixa_vencimento_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VENCIMENTO_Tooltip", StringUtil.RTrim( Ddo_caixa_vencimento_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VENCIMENTO_Cls", StringUtil.RTrim( Ddo_caixa_vencimento_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VENCIMENTO_Filteredtext_set", StringUtil.RTrim( Ddo_caixa_vencimento_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VENCIMENTO_Filteredtextto_set", StringUtil.RTrim( Ddo_caixa_vencimento_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VENCIMENTO_Dropdownoptionstype", StringUtil.RTrim( Ddo_caixa_vencimento_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VENCIMENTO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_caixa_vencimento_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VENCIMENTO_Includesortasc", StringUtil.BoolToStr( Ddo_caixa_vencimento_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VENCIMENTO_Includesortdsc", StringUtil.BoolToStr( Ddo_caixa_vencimento_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VENCIMENTO_Sortedstatus", StringUtil.RTrim( Ddo_caixa_vencimento_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VENCIMENTO_Includefilter", StringUtil.BoolToStr( Ddo_caixa_vencimento_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VENCIMENTO_Filtertype", StringUtil.RTrim( Ddo_caixa_vencimento_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VENCIMENTO_Filterisrange", StringUtil.BoolToStr( Ddo_caixa_vencimento_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VENCIMENTO_Includedatalist", StringUtil.BoolToStr( Ddo_caixa_vencimento_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VENCIMENTO_Datalistfixedvalues", StringUtil.RTrim( Ddo_caixa_vencimento_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VENCIMENTO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_caixa_vencimento_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VENCIMENTO_Sortasc", StringUtil.RTrim( Ddo_caixa_vencimento_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VENCIMENTO_Sortdsc", StringUtil.RTrim( Ddo_caixa_vencimento_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VENCIMENTO_Loadingdata", StringUtil.RTrim( Ddo_caixa_vencimento_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VENCIMENTO_Cleanfilter", StringUtil.RTrim( Ddo_caixa_vencimento_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VENCIMENTO_Rangefilterfrom", StringUtil.RTrim( Ddo_caixa_vencimento_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VENCIMENTO_Rangefilterto", StringUtil.RTrim( Ddo_caixa_vencimento_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VENCIMENTO_Noresultsfound", StringUtil.RTrim( Ddo_caixa_vencimento_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VENCIMENTO_Searchbuttontext", StringUtil.RTrim( Ddo_caixa_vencimento_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_TIPODECONTACOD_Caption", StringUtil.RTrim( Ddo_caixa_tipodecontacod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_TIPODECONTACOD_Tooltip", StringUtil.RTrim( Ddo_caixa_tipodecontacod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_TIPODECONTACOD_Cls", StringUtil.RTrim( Ddo_caixa_tipodecontacod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_TIPODECONTACOD_Filteredtext_set", StringUtil.RTrim( Ddo_caixa_tipodecontacod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_TIPODECONTACOD_Selectedvalue_set", StringUtil.RTrim( Ddo_caixa_tipodecontacod_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_TIPODECONTACOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_caixa_tipodecontacod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_TIPODECONTACOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_caixa_tipodecontacod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_TIPODECONTACOD_Includesortasc", StringUtil.BoolToStr( Ddo_caixa_tipodecontacod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_TIPODECONTACOD_Includesortdsc", StringUtil.BoolToStr( Ddo_caixa_tipodecontacod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_TIPODECONTACOD_Sortedstatus", StringUtil.RTrim( Ddo_caixa_tipodecontacod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_TIPODECONTACOD_Includefilter", StringUtil.BoolToStr( Ddo_caixa_tipodecontacod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_TIPODECONTACOD_Filtertype", StringUtil.RTrim( Ddo_caixa_tipodecontacod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_TIPODECONTACOD_Filterisrange", StringUtil.BoolToStr( Ddo_caixa_tipodecontacod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_TIPODECONTACOD_Includedatalist", StringUtil.BoolToStr( Ddo_caixa_tipodecontacod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_TIPODECONTACOD_Datalisttype", StringUtil.RTrim( Ddo_caixa_tipodecontacod_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_TIPODECONTACOD_Datalistfixedvalues", StringUtil.RTrim( Ddo_caixa_tipodecontacod_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_TIPODECONTACOD_Datalistproc", StringUtil.RTrim( Ddo_caixa_tipodecontacod_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_TIPODECONTACOD_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_caixa_tipodecontacod_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_TIPODECONTACOD_Sortasc", StringUtil.RTrim( Ddo_caixa_tipodecontacod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_TIPODECONTACOD_Sortdsc", StringUtil.RTrim( Ddo_caixa_tipodecontacod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_TIPODECONTACOD_Loadingdata", StringUtil.RTrim( Ddo_caixa_tipodecontacod_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_TIPODECONTACOD_Cleanfilter", StringUtil.RTrim( Ddo_caixa_tipodecontacod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_TIPODECONTACOD_Rangefilterfrom", StringUtil.RTrim( Ddo_caixa_tipodecontacod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_TIPODECONTACOD_Rangefilterto", StringUtil.RTrim( Ddo_caixa_tipodecontacod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_TIPODECONTACOD_Noresultsfound", StringUtil.RTrim( Ddo_caixa_tipodecontacod_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_TIPODECONTACOD_Searchbuttontext", StringUtil.RTrim( Ddo_caixa_tipodecontacod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DESCRICAO_Caption", StringUtil.RTrim( Ddo_caixa_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_caixa_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DESCRICAO_Cls", StringUtil.RTrim( Ddo_caixa_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_caixa_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_caixa_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_caixa_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_caixa_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_caixa_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_caixa_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_caixa_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_caixa_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_caixa_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_caixa_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_caixa_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_caixa_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DESCRICAO_Datalistfixedvalues", StringUtil.RTrim( Ddo_caixa_descricao_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_caixa_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_caixa_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_caixa_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_caixa_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_caixa_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_caixa_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DESCRICAO_Rangefilterfrom", StringUtil.RTrim( Ddo_caixa_descricao_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DESCRICAO_Rangefilterto", StringUtil.RTrim( Ddo_caixa_descricao_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_caixa_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_caixa_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VALOR_Caption", StringUtil.RTrim( Ddo_caixa_valor_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VALOR_Tooltip", StringUtil.RTrim( Ddo_caixa_valor_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VALOR_Cls", StringUtil.RTrim( Ddo_caixa_valor_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VALOR_Filteredtext_set", StringUtil.RTrim( Ddo_caixa_valor_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VALOR_Filteredtextto_set", StringUtil.RTrim( Ddo_caixa_valor_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VALOR_Dropdownoptionstype", StringUtil.RTrim( Ddo_caixa_valor_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VALOR_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_caixa_valor_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VALOR_Includesortasc", StringUtil.BoolToStr( Ddo_caixa_valor_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VALOR_Includesortdsc", StringUtil.BoolToStr( Ddo_caixa_valor_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VALOR_Sortedstatus", StringUtil.RTrim( Ddo_caixa_valor_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VALOR_Includefilter", StringUtil.BoolToStr( Ddo_caixa_valor_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VALOR_Filtertype", StringUtil.RTrim( Ddo_caixa_valor_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VALOR_Filterisrange", StringUtil.BoolToStr( Ddo_caixa_valor_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VALOR_Includedatalist", StringUtil.BoolToStr( Ddo_caixa_valor_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VALOR_Datalistfixedvalues", StringUtil.RTrim( Ddo_caixa_valor_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VALOR_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_caixa_valor_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VALOR_Sortasc", StringUtil.RTrim( Ddo_caixa_valor_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VALOR_Sortdsc", StringUtil.RTrim( Ddo_caixa_valor_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VALOR_Loadingdata", StringUtil.RTrim( Ddo_caixa_valor_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VALOR_Cleanfilter", StringUtil.RTrim( Ddo_caixa_valor_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VALOR_Rangefilterfrom", StringUtil.RTrim( Ddo_caixa_valor_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VALOR_Rangefilterto", StringUtil.RTrim( Ddo_caixa_valor_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VALOR_Noresultsfound", StringUtil.RTrim( Ddo_caixa_valor_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VALOR_Searchbuttontext", StringUtil.RTrim( Ddo_caixa_valor_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_caixa_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_caixa_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_caixa_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DOCUMENTO_Activeeventkey", StringUtil.RTrim( Ddo_caixa_documento_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DOCUMENTO_Filteredtext_get", StringUtil.RTrim( Ddo_caixa_documento_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DOCUMENTO_Selectedvalue_get", StringUtil.RTrim( Ddo_caixa_documento_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_EMISSAO_Activeeventkey", StringUtil.RTrim( Ddo_caixa_emissao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_EMISSAO_Filteredtext_get", StringUtil.RTrim( Ddo_caixa_emissao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_EMISSAO_Filteredtextto_get", StringUtil.RTrim( Ddo_caixa_emissao_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VENCIMENTO_Activeeventkey", StringUtil.RTrim( Ddo_caixa_vencimento_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VENCIMENTO_Filteredtext_get", StringUtil.RTrim( Ddo_caixa_vencimento_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VENCIMENTO_Filteredtextto_get", StringUtil.RTrim( Ddo_caixa_vencimento_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_TIPODECONTACOD_Activeeventkey", StringUtil.RTrim( Ddo_caixa_tipodecontacod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_TIPODECONTACOD_Filteredtext_get", StringUtil.RTrim( Ddo_caixa_tipodecontacod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_TIPODECONTACOD_Selectedvalue_get", StringUtil.RTrim( Ddo_caixa_tipodecontacod_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_caixa_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_caixa_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_caixa_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VALOR_Activeeventkey", StringUtil.RTrim( Ddo_caixa_valor_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VALOR_Filteredtext_get", StringUtil.RTrim( Ddo_caixa_valor_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CAIXA_VALOR_Filteredtextto_get", StringUtil.RTrim( Ddo_caixa_valor_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormFQ2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptCaixa" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Fluxo de Caixa" ;
      }

      protected void WBFQ0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_FQ2( true) ;
         }
         else
         {
            wb_table1_2_FQ2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_FQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(93, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,93);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(94, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,94);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcaixa_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31TFCaixa_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV31TFCaixa_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,95);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcaixa_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcaixa_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptCaixa.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcaixa_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFCaixa_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV32TFCaixa_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,96);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcaixa_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcaixa_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptCaixa.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcaixa_documento_Internalname, AV35TFCaixa_Documento, StringUtil.RTrim( context.localUtil.Format( AV35TFCaixa_Documento, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcaixa_documento_Jsonclick, 0, "Attribute", "", "", "", edtavTfcaixa_documento_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptCaixa.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcaixa_documento_sel_Internalname, AV36TFCaixa_Documento_Sel, StringUtil.RTrim( context.localUtil.Format( AV36TFCaixa_Documento_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,98);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcaixa_documento_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcaixa_documento_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptCaixa.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_80_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcaixa_emissao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcaixa_emissao_Internalname, context.localUtil.Format(AV39TFCaixa_Emissao, "99/99/99"), context.localUtil.Format( AV39TFCaixa_Emissao, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcaixa_emissao_Jsonclick, 0, "Attribute", "", "", "", edtavTfcaixa_emissao_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptCaixa.htm");
            GxWebStd.gx_bitmap( context, edtavTfcaixa_emissao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcaixa_emissao_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptCaixa.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_80_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcaixa_emissao_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcaixa_emissao_to_Internalname, context.localUtil.Format(AV40TFCaixa_Emissao_To, "99/99/99"), context.localUtil.Format( AV40TFCaixa_Emissao_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,100);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcaixa_emissao_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcaixa_emissao_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptCaixa.htm");
            GxWebStd.gx_bitmap( context, edtavTfcaixa_emissao_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcaixa_emissao_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptCaixa.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_caixa_emissaoauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_80_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_caixa_emissaoauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_caixa_emissaoauxdate_Internalname, context.localUtil.Format(AV41DDO_Caixa_EmissaoAuxDate, "99/99/99"), context.localUtil.Format( AV41DDO_Caixa_EmissaoAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_caixa_emissaoauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptCaixa.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_caixa_emissaoauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptCaixa.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_80_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_caixa_emissaoauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_caixa_emissaoauxdateto_Internalname, context.localUtil.Format(AV42DDO_Caixa_EmissaoAuxDateTo, "99/99/99"), context.localUtil.Format( AV42DDO_Caixa_EmissaoAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_caixa_emissaoauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptCaixa.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_caixa_emissaoauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptCaixa.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_80_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcaixa_vencimento_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcaixa_vencimento_Internalname, context.localUtil.Format(AV45TFCaixa_Vencimento, "99/99/99"), context.localUtil.Format( AV45TFCaixa_Vencimento, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcaixa_vencimento_Jsonclick, 0, "Attribute", "", "", "", edtavTfcaixa_vencimento_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptCaixa.htm");
            GxWebStd.gx_bitmap( context, edtavTfcaixa_vencimento_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcaixa_vencimento_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptCaixa.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_80_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcaixa_vencimento_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcaixa_vencimento_to_Internalname, context.localUtil.Format(AV46TFCaixa_Vencimento_To, "99/99/99"), context.localUtil.Format( AV46TFCaixa_Vencimento_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcaixa_vencimento_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcaixa_vencimento_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptCaixa.htm");
            GxWebStd.gx_bitmap( context, edtavTfcaixa_vencimento_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcaixa_vencimento_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptCaixa.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_caixa_vencimentoauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_80_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_caixa_vencimentoauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_caixa_vencimentoauxdate_Internalname, context.localUtil.Format(AV47DDO_Caixa_VencimentoAuxDate, "99/99/99"), context.localUtil.Format( AV47DDO_Caixa_VencimentoAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_caixa_vencimentoauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptCaixa.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_caixa_vencimentoauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptCaixa.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_80_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_caixa_vencimentoauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_caixa_vencimentoauxdateto_Internalname, context.localUtil.Format(AV48DDO_Caixa_VencimentoAuxDateTo, "99/99/99"), context.localUtil.Format( AV48DDO_Caixa_VencimentoAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_caixa_vencimentoauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptCaixa.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_caixa_vencimentoauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptCaixa.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcaixa_tipodecontacod_Internalname, StringUtil.RTrim( AV51TFCaixa_TipoDeContaCod), StringUtil.RTrim( context.localUtil.Format( AV51TFCaixa_TipoDeContaCod, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcaixa_tipodecontacod_Jsonclick, 0, "Attribute", "", "", "", edtavTfcaixa_tipodecontacod_Visible, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_PromptCaixa.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcaixa_tipodecontacod_sel_Internalname, StringUtil.RTrim( AV52TFCaixa_TipoDeContaCod_Sel), StringUtil.RTrim( context.localUtil.Format( AV52TFCaixa_TipoDeContaCod_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcaixa_tipodecontacod_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcaixa_tipodecontacod_sel_Visible, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_PromptCaixa.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcaixa_descricao_Internalname, AV55TFCaixa_Descricao, StringUtil.RTrim( context.localUtil.Format( AV55TFCaixa_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcaixa_descricao_Jsonclick, 0, "Attribute", "", "", "", edtavTfcaixa_descricao_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptCaixa.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcaixa_descricao_sel_Internalname, AV56TFCaixa_Descricao_Sel, StringUtil.RTrim( context.localUtil.Format( AV56TFCaixa_Descricao_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,112);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcaixa_descricao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcaixa_descricao_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptCaixa.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcaixa_valor_Internalname, StringUtil.LTrim( StringUtil.NToC( AV59TFCaixa_Valor, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV59TFCaixa_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcaixa_valor_Jsonclick, 0, "Attribute", "", "", "", edtavTfcaixa_valor_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptCaixa.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcaixa_valor_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV60TFCaixa_Valor_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV60TFCaixa_Valor_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcaixa_valor_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcaixa_valor_to_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptCaixa.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CAIXA_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_caixa_codigotitlecontrolidtoreplace_Internalname, AV33ddo_Caixa_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,116);\"", 0, edtavDdo_caixa_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptCaixa.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CAIXA_DOCUMENTOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_caixa_documentotitlecontrolidtoreplace_Internalname, AV37ddo_Caixa_DocumentoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,118);\"", 0, edtavDdo_caixa_documentotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptCaixa.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CAIXA_EMISSAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_caixa_emissaotitlecontrolidtoreplace_Internalname, AV43ddo_Caixa_EmissaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,120);\"", 0, edtavDdo_caixa_emissaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptCaixa.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CAIXA_VENCIMENTOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_caixa_vencimentotitlecontrolidtoreplace_Internalname, AV49ddo_Caixa_VencimentoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,122);\"", 0, edtavDdo_caixa_vencimentotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptCaixa.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CAIXA_TIPODECONTACODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_caixa_tipodecontacodtitlecontrolidtoreplace_Internalname, AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,124);\"", 0, edtavDdo_caixa_tipodecontacodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptCaixa.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CAIXA_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_caixa_descricaotitlecontrolidtoreplace_Internalname, AV57ddo_Caixa_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,126);\"", 0, edtavDdo_caixa_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptCaixa.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CAIXA_VALORContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_caixa_valortitlecontrolidtoreplace_Internalname, AV61ddo_Caixa_ValorTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,128);\"", 0, edtavDdo_caixa_valortitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptCaixa.htm");
         }
         wbLoad = true;
      }

      protected void STARTFQ2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Fluxo de Caixa", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPFQ0( ) ;
      }

      protected void WSFQ2( )
      {
         STARTFQ2( ) ;
         EVTFQ2( ) ;
      }

      protected void EVTFQ2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11FQ2 */
                           E11FQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CAIXA_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12FQ2 */
                           E12FQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CAIXA_DOCUMENTO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13FQ2 */
                           E13FQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CAIXA_EMISSAO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14FQ2 */
                           E14FQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CAIXA_VENCIMENTO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15FQ2 */
                           E15FQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CAIXA_TIPODECONTACOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16FQ2 */
                           E16FQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CAIXA_DESCRICAO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17FQ2 */
                           E17FQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CAIXA_VALOR.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18FQ2 */
                           E18FQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19FQ2 */
                           E19FQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20FQ2 */
                           E20FQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21FQ2 */
                           E21FQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E22FQ2 */
                           E22FQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E23FQ2 */
                           E23FQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E24FQ2 */
                           E24FQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E25FQ2 */
                           E25FQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E26FQ2 */
                           E26FQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E27FQ2 */
                           E27FQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E28FQ2 */
                           E28FQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_80_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
                           SubsflControlProps_802( ) ;
                           AV28Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV68Select_GXI : context.convertURL( context.PathToRelativeUrl( AV28Select))));
                           A874Caixa_Codigo = (int)(context.localUtil.CToN( cgiGet( edtCaixa_Codigo_Internalname), ",", "."));
                           A875Caixa_Documento = StringUtil.Upper( cgiGet( edtCaixa_Documento_Internalname));
                           A876Caixa_Emissao = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtCaixa_Emissao_Internalname), 0));
                           n876Caixa_Emissao = false;
                           A877Caixa_Vencimento = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtCaixa_Vencimento_Internalname), 0));
                           n877Caixa_Vencimento = false;
                           A881Caixa_TipoDeContaCod = cgiGet( edtCaixa_TipoDeContaCod_Internalname);
                           A879Caixa_Descricao = StringUtil.Upper( cgiGet( edtCaixa_Descricao_Internalname));
                           n879Caixa_Descricao = false;
                           A880Caixa_Valor = context.localUtil.CToN( cgiGet( edtCaixa_Valor_Internalname), ",", ".");
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E29FQ2 */
                                 E29FQ2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E30FQ2 */
                                 E30FQ2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E31FQ2 */
                                 E31FQ2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Caixa_documento1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCAIXA_DOCUMENTO1"), AV17Caixa_Documento1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Caixa_documento2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCAIXA_DOCUMENTO2"), AV21Caixa_Documento2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Caixa_documento3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCAIXA_DOCUMENTO3"), AV25Caixa_Documento3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcaixa_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCAIXA_CODIGO"), ",", ".") != Convert.ToDecimal( AV31TFCaixa_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcaixa_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCAIXA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV32TFCaixa_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcaixa_documento Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCAIXA_DOCUMENTO"), AV35TFCaixa_Documento) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcaixa_documento_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCAIXA_DOCUMENTO_SEL"), AV36TFCaixa_Documento_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcaixa_emissao Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCAIXA_EMISSAO"), 0) != AV39TFCaixa_Emissao )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcaixa_emissao_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCAIXA_EMISSAO_TO"), 0) != AV40TFCaixa_Emissao_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcaixa_vencimento Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCAIXA_VENCIMENTO"), 0) != AV45TFCaixa_Vencimento )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcaixa_vencimento_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCAIXA_VENCIMENTO_TO"), 0) != AV46TFCaixa_Vencimento_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcaixa_tipodecontacod Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCAIXA_TIPODECONTACOD"), AV51TFCaixa_TipoDeContaCod) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcaixa_tipodecontacod_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCAIXA_TIPODECONTACOD_SEL"), AV52TFCaixa_TipoDeContaCod_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcaixa_descricao Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCAIXA_DESCRICAO"), AV55TFCaixa_Descricao) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcaixa_descricao_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCAIXA_DESCRICAO_SEL"), AV56TFCaixa_Descricao_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcaixa_valor Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCAIXA_VALOR"), ",", ".") != AV59TFCaixa_Valor )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcaixa_valor_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCAIXA_VALOR_TO"), ",", ".") != AV60TFCaixa_Valor_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E32FQ2 */
                                       E32FQ2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEFQ2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormFQ2( ) ;
            }
         }
      }

      protected void PAFQ2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CAIXA_DOCUMENTO", "Documento", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CAIXA_DOCUMENTO", "Documento", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CAIXA_DOCUMENTO", "Documento", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXSGVvTFCAIXA_TIPODECONTACODFQ0( String A870TipodeConta_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXSGVvTFCAIXA_TIPODECONTACOD_dataFQ0( A870TipodeConta_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXSGVvTFCAIXA_TIPODECONTACOD_dataFQ0( String A870TipodeConta_Codigo )
      {
         l870TipodeConta_Codigo = StringUtil.PadR( StringUtil.RTrim( A870TipodeConta_Codigo), 20, "%");
         /* Using cursor H00FQ2 */
         pr_default.execute(0, new Object[] {l870TipodeConta_Codigo});
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.RTrim( H00FQ2_A870TipodeConta_Codigo[0]));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00FQ2_A870TipodeConta_Codigo[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXSGVvTFCAIXA_TIPODECONTACOD_SELFQ0( String A870TipodeConta_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXSGVvTFCAIXA_TIPODECONTACOD_SEL_dataFQ0( A870TipodeConta_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXSGVvTFCAIXA_TIPODECONTACOD_SEL_dataFQ0( String A870TipodeConta_Codigo )
      {
         l870TipodeConta_Codigo = StringUtil.PadR( StringUtil.RTrim( A870TipodeConta_Codigo), 20, "%");
         /* Using cursor H00FQ3 */
         pr_default.execute(1, new Object[] {l870TipodeConta_Codigo});
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.RTrim( H00FQ3_A870TipodeConta_Codigo[0]));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00FQ3_A870TipodeConta_Codigo[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXSGACAIXA_TIPODECONTACODFQ0( String A870TipodeConta_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXSGACAIXA_TIPODECONTACOD_dataFQ0( A870TipodeConta_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXSGACAIXA_TIPODECONTACOD_dataFQ0( String A870TipodeConta_Codigo )
      {
         l870TipodeConta_Codigo = StringUtil.PadR( StringUtil.RTrim( A870TipodeConta_Codigo), 20, "%");
         /* Using cursor H00FQ4 */
         pr_default.execute(2, new Object[] {l870TipodeConta_Codigo});
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.RTrim( H00FQ4_A870TipodeConta_Codigo[0]));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00FQ4_A870TipodeConta_Codigo[0]));
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_802( ) ;
         while ( nGXsfl_80_idx <= nRC_GXsfl_80 )
         {
            sendrow_802( ) ;
            nGXsfl_80_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_80_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_80_idx+1));
            sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
            SubsflControlProps_802( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17Caixa_Documento1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV20DynamicFiltersOperator2 ,
                                       String AV21Caixa_Documento2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       short AV24DynamicFiltersOperator3 ,
                                       String AV25Caixa_Documento3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       int AV31TFCaixa_Codigo ,
                                       int AV32TFCaixa_Codigo_To ,
                                       String AV35TFCaixa_Documento ,
                                       String AV36TFCaixa_Documento_Sel ,
                                       DateTime AV39TFCaixa_Emissao ,
                                       DateTime AV40TFCaixa_Emissao_To ,
                                       DateTime AV45TFCaixa_Vencimento ,
                                       DateTime AV46TFCaixa_Vencimento_To ,
                                       String AV51TFCaixa_TipoDeContaCod ,
                                       String AV52TFCaixa_TipoDeContaCod_Sel ,
                                       String AV55TFCaixa_Descricao ,
                                       String AV56TFCaixa_Descricao_Sel ,
                                       decimal AV59TFCaixa_Valor ,
                                       decimal AV60TFCaixa_Valor_To ,
                                       String AV33ddo_Caixa_CodigoTitleControlIdToReplace ,
                                       String AV37ddo_Caixa_DocumentoTitleControlIdToReplace ,
                                       String AV43ddo_Caixa_EmissaoTitleControlIdToReplace ,
                                       String AV49ddo_Caixa_VencimentoTitleControlIdToReplace ,
                                       String AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace ,
                                       String AV57ddo_Caixa_DescricaoTitleControlIdToReplace ,
                                       String AV61ddo_Caixa_ValorTitleControlIdToReplace ,
                                       String AV69Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFFQ2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CAIXA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A874Caixa_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CAIXA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A874Caixa_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CAIXA_DOCUMENTO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A875Caixa_Documento, "@!"))));
         GxWebStd.gx_hidden_field( context, "CAIXA_DOCUMENTO", A875Caixa_Documento);
         GxWebStd.gx_hidden_field( context, "gxhash_CAIXA_EMISSAO", GetSecureSignedToken( "", A876Caixa_Emissao));
         GxWebStd.gx_hidden_field( context, "CAIXA_EMISSAO", context.localUtil.Format(A876Caixa_Emissao, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_CAIXA_VENCIMENTO", GetSecureSignedToken( "", A877Caixa_Vencimento));
         GxWebStd.gx_hidden_field( context, "CAIXA_VENCIMENTO", context.localUtil.Format(A877Caixa_Vencimento, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_CAIXA_TIPODECONTACOD", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A881Caixa_TipoDeContaCod, ""))));
         GxWebStd.gx_hidden_field( context, "CAIXA_TIPODECONTACOD", StringUtil.RTrim( A881Caixa_TipoDeContaCod));
         GxWebStd.gx_hidden_field( context, "gxhash_CAIXA_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A879Caixa_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, "CAIXA_DESCRICAO", A879Caixa_Descricao);
         GxWebStd.gx_hidden_field( context, "gxhash_CAIXA_VALOR", GetSecureSignedToken( "", context.localUtil.Format( A880Caixa_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "CAIXA_VALOR", StringUtil.LTrim( StringUtil.NToC( A880Caixa_Valor, 18, 5, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFFQ2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV69Pgmname = "PromptCaixa";
         context.Gx_err = 0;
      }

      protected void RFFQ2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 80;
         /* Execute user event: E30FQ2 */
         E30FQ2 ();
         nGXsfl_80_idx = 1;
         sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
         SubsflControlProps_802( ) ;
         nGXsfl_80_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_802( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(3, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16DynamicFiltersOperator1 ,
                                                 AV17Caixa_Documento1 ,
                                                 AV18DynamicFiltersEnabled2 ,
                                                 AV19DynamicFiltersSelector2 ,
                                                 AV20DynamicFiltersOperator2 ,
                                                 AV21Caixa_Documento2 ,
                                                 AV22DynamicFiltersEnabled3 ,
                                                 AV23DynamicFiltersSelector3 ,
                                                 AV24DynamicFiltersOperator3 ,
                                                 AV25Caixa_Documento3 ,
                                                 AV31TFCaixa_Codigo ,
                                                 AV32TFCaixa_Codigo_To ,
                                                 AV36TFCaixa_Documento_Sel ,
                                                 AV35TFCaixa_Documento ,
                                                 AV39TFCaixa_Emissao ,
                                                 AV40TFCaixa_Emissao_To ,
                                                 AV45TFCaixa_Vencimento ,
                                                 AV46TFCaixa_Vencimento_To ,
                                                 AV52TFCaixa_TipoDeContaCod_Sel ,
                                                 AV51TFCaixa_TipoDeContaCod ,
                                                 AV56TFCaixa_Descricao_Sel ,
                                                 AV55TFCaixa_Descricao ,
                                                 AV59TFCaixa_Valor ,
                                                 AV60TFCaixa_Valor_To ,
                                                 A875Caixa_Documento ,
                                                 A874Caixa_Codigo ,
                                                 A876Caixa_Emissao ,
                                                 A877Caixa_Vencimento ,
                                                 A881Caixa_TipoDeContaCod ,
                                                 A879Caixa_Descricao ,
                                                 A880Caixa_Valor ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE,
                                                 TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV17Caixa_Documento1 = StringUtil.Concat( StringUtil.RTrim( AV17Caixa_Documento1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Caixa_Documento1", AV17Caixa_Documento1);
            lV17Caixa_Documento1 = StringUtil.Concat( StringUtil.RTrim( AV17Caixa_Documento1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Caixa_Documento1", AV17Caixa_Documento1);
            lV21Caixa_Documento2 = StringUtil.Concat( StringUtil.RTrim( AV21Caixa_Documento2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Caixa_Documento2", AV21Caixa_Documento2);
            lV21Caixa_Documento2 = StringUtil.Concat( StringUtil.RTrim( AV21Caixa_Documento2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Caixa_Documento2", AV21Caixa_Documento2);
            lV25Caixa_Documento3 = StringUtil.Concat( StringUtil.RTrim( AV25Caixa_Documento3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Caixa_Documento3", AV25Caixa_Documento3);
            lV25Caixa_Documento3 = StringUtil.Concat( StringUtil.RTrim( AV25Caixa_Documento3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Caixa_Documento3", AV25Caixa_Documento3);
            lV35TFCaixa_Documento = StringUtil.Concat( StringUtil.RTrim( AV35TFCaixa_Documento), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFCaixa_Documento", AV35TFCaixa_Documento);
            lV51TFCaixa_TipoDeContaCod = StringUtil.PadR( StringUtil.RTrim( AV51TFCaixa_TipoDeContaCod), 20, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFCaixa_TipoDeContaCod", AV51TFCaixa_TipoDeContaCod);
            lV55TFCaixa_Descricao = StringUtil.Concat( StringUtil.RTrim( AV55TFCaixa_Descricao), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFCaixa_Descricao", AV55TFCaixa_Descricao);
            /* Using cursor H00FQ5 */
            pr_default.execute(3, new Object[] {lV17Caixa_Documento1, lV17Caixa_Documento1, lV21Caixa_Documento2, lV21Caixa_Documento2, lV25Caixa_Documento3, lV25Caixa_Documento3, AV31TFCaixa_Codigo, AV32TFCaixa_Codigo_To, lV35TFCaixa_Documento, AV36TFCaixa_Documento_Sel, AV39TFCaixa_Emissao, AV40TFCaixa_Emissao_To, AV45TFCaixa_Vencimento, AV46TFCaixa_Vencimento_To, lV51TFCaixa_TipoDeContaCod, AV52TFCaixa_TipoDeContaCod_Sel, lV55TFCaixa_Descricao, AV56TFCaixa_Descricao_Sel, AV59TFCaixa_Valor, AV60TFCaixa_Valor_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_80_idx = 1;
            while ( ( (pr_default.getStatus(3) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A880Caixa_Valor = H00FQ5_A880Caixa_Valor[0];
               A879Caixa_Descricao = H00FQ5_A879Caixa_Descricao[0];
               n879Caixa_Descricao = H00FQ5_n879Caixa_Descricao[0];
               A881Caixa_TipoDeContaCod = H00FQ5_A881Caixa_TipoDeContaCod[0];
               A877Caixa_Vencimento = H00FQ5_A877Caixa_Vencimento[0];
               n877Caixa_Vencimento = H00FQ5_n877Caixa_Vencimento[0];
               A876Caixa_Emissao = H00FQ5_A876Caixa_Emissao[0];
               n876Caixa_Emissao = H00FQ5_n876Caixa_Emissao[0];
               A875Caixa_Documento = H00FQ5_A875Caixa_Documento[0];
               A874Caixa_Codigo = H00FQ5_A874Caixa_Codigo[0];
               /* Execute user event: E31FQ2 */
               E31FQ2 ();
               pr_default.readNext(3);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(3) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(3);
            wbEnd = 80;
            WBFQ0( ) ;
         }
         nGXsfl_80_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(4, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV16DynamicFiltersOperator1 ,
                                              AV17Caixa_Documento1 ,
                                              AV18DynamicFiltersEnabled2 ,
                                              AV19DynamicFiltersSelector2 ,
                                              AV20DynamicFiltersOperator2 ,
                                              AV21Caixa_Documento2 ,
                                              AV22DynamicFiltersEnabled3 ,
                                              AV23DynamicFiltersSelector3 ,
                                              AV24DynamicFiltersOperator3 ,
                                              AV25Caixa_Documento3 ,
                                              AV31TFCaixa_Codigo ,
                                              AV32TFCaixa_Codigo_To ,
                                              AV36TFCaixa_Documento_Sel ,
                                              AV35TFCaixa_Documento ,
                                              AV39TFCaixa_Emissao ,
                                              AV40TFCaixa_Emissao_To ,
                                              AV45TFCaixa_Vencimento ,
                                              AV46TFCaixa_Vencimento_To ,
                                              AV52TFCaixa_TipoDeContaCod_Sel ,
                                              AV51TFCaixa_TipoDeContaCod ,
                                              AV56TFCaixa_Descricao_Sel ,
                                              AV55TFCaixa_Descricao ,
                                              AV59TFCaixa_Valor ,
                                              AV60TFCaixa_Valor_To ,
                                              A875Caixa_Documento ,
                                              A874Caixa_Codigo ,
                                              A876Caixa_Emissao ,
                                              A877Caixa_Vencimento ,
                                              A881Caixa_TipoDeContaCod ,
                                              A879Caixa_Descricao ,
                                              A880Caixa_Valor ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV17Caixa_Documento1 = StringUtil.Concat( StringUtil.RTrim( AV17Caixa_Documento1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Caixa_Documento1", AV17Caixa_Documento1);
         lV17Caixa_Documento1 = StringUtil.Concat( StringUtil.RTrim( AV17Caixa_Documento1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Caixa_Documento1", AV17Caixa_Documento1);
         lV21Caixa_Documento2 = StringUtil.Concat( StringUtil.RTrim( AV21Caixa_Documento2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Caixa_Documento2", AV21Caixa_Documento2);
         lV21Caixa_Documento2 = StringUtil.Concat( StringUtil.RTrim( AV21Caixa_Documento2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Caixa_Documento2", AV21Caixa_Documento2);
         lV25Caixa_Documento3 = StringUtil.Concat( StringUtil.RTrim( AV25Caixa_Documento3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Caixa_Documento3", AV25Caixa_Documento3);
         lV25Caixa_Documento3 = StringUtil.Concat( StringUtil.RTrim( AV25Caixa_Documento3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Caixa_Documento3", AV25Caixa_Documento3);
         lV35TFCaixa_Documento = StringUtil.Concat( StringUtil.RTrim( AV35TFCaixa_Documento), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFCaixa_Documento", AV35TFCaixa_Documento);
         lV51TFCaixa_TipoDeContaCod = StringUtil.PadR( StringUtil.RTrim( AV51TFCaixa_TipoDeContaCod), 20, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFCaixa_TipoDeContaCod", AV51TFCaixa_TipoDeContaCod);
         lV55TFCaixa_Descricao = StringUtil.Concat( StringUtil.RTrim( AV55TFCaixa_Descricao), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFCaixa_Descricao", AV55TFCaixa_Descricao);
         /* Using cursor H00FQ6 */
         pr_default.execute(4, new Object[] {lV17Caixa_Documento1, lV17Caixa_Documento1, lV21Caixa_Documento2, lV21Caixa_Documento2, lV25Caixa_Documento3, lV25Caixa_Documento3, AV31TFCaixa_Codigo, AV32TFCaixa_Codigo_To, lV35TFCaixa_Documento, AV36TFCaixa_Documento_Sel, AV39TFCaixa_Emissao, AV40TFCaixa_Emissao_To, AV45TFCaixa_Vencimento, AV46TFCaixa_Vencimento_To, lV51TFCaixa_TipoDeContaCod, AV52TFCaixa_TipoDeContaCod_Sel, lV55TFCaixa_Descricao, AV56TFCaixa_Descricao_Sel, AV59TFCaixa_Valor, AV60TFCaixa_Valor_To});
         GRID_nRecordCount = H00FQ6_AGRID_nRecordCount[0];
         pr_default.close(4);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Caixa_Documento1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Caixa_Documento2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Caixa_Documento3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFCaixa_Codigo, AV32TFCaixa_Codigo_To, AV35TFCaixa_Documento, AV36TFCaixa_Documento_Sel, AV39TFCaixa_Emissao, AV40TFCaixa_Emissao_To, AV45TFCaixa_Vencimento, AV46TFCaixa_Vencimento_To, AV51TFCaixa_TipoDeContaCod, AV52TFCaixa_TipoDeContaCod_Sel, AV55TFCaixa_Descricao, AV56TFCaixa_Descricao_Sel, AV59TFCaixa_Valor, AV60TFCaixa_Valor_To, AV33ddo_Caixa_CodigoTitleControlIdToReplace, AV37ddo_Caixa_DocumentoTitleControlIdToReplace, AV43ddo_Caixa_EmissaoTitleControlIdToReplace, AV49ddo_Caixa_VencimentoTitleControlIdToReplace, AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace, AV57ddo_Caixa_DescricaoTitleControlIdToReplace, AV61ddo_Caixa_ValorTitleControlIdToReplace, AV69Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Caixa_Documento1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Caixa_Documento2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Caixa_Documento3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFCaixa_Codigo, AV32TFCaixa_Codigo_To, AV35TFCaixa_Documento, AV36TFCaixa_Documento_Sel, AV39TFCaixa_Emissao, AV40TFCaixa_Emissao_To, AV45TFCaixa_Vencimento, AV46TFCaixa_Vencimento_To, AV51TFCaixa_TipoDeContaCod, AV52TFCaixa_TipoDeContaCod_Sel, AV55TFCaixa_Descricao, AV56TFCaixa_Descricao_Sel, AV59TFCaixa_Valor, AV60TFCaixa_Valor_To, AV33ddo_Caixa_CodigoTitleControlIdToReplace, AV37ddo_Caixa_DocumentoTitleControlIdToReplace, AV43ddo_Caixa_EmissaoTitleControlIdToReplace, AV49ddo_Caixa_VencimentoTitleControlIdToReplace, AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace, AV57ddo_Caixa_DescricaoTitleControlIdToReplace, AV61ddo_Caixa_ValorTitleControlIdToReplace, AV69Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Caixa_Documento1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Caixa_Documento2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Caixa_Documento3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFCaixa_Codigo, AV32TFCaixa_Codigo_To, AV35TFCaixa_Documento, AV36TFCaixa_Documento_Sel, AV39TFCaixa_Emissao, AV40TFCaixa_Emissao_To, AV45TFCaixa_Vencimento, AV46TFCaixa_Vencimento_To, AV51TFCaixa_TipoDeContaCod, AV52TFCaixa_TipoDeContaCod_Sel, AV55TFCaixa_Descricao, AV56TFCaixa_Descricao_Sel, AV59TFCaixa_Valor, AV60TFCaixa_Valor_To, AV33ddo_Caixa_CodigoTitleControlIdToReplace, AV37ddo_Caixa_DocumentoTitleControlIdToReplace, AV43ddo_Caixa_EmissaoTitleControlIdToReplace, AV49ddo_Caixa_VencimentoTitleControlIdToReplace, AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace, AV57ddo_Caixa_DescricaoTitleControlIdToReplace, AV61ddo_Caixa_ValorTitleControlIdToReplace, AV69Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Caixa_Documento1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Caixa_Documento2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Caixa_Documento3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFCaixa_Codigo, AV32TFCaixa_Codigo_To, AV35TFCaixa_Documento, AV36TFCaixa_Documento_Sel, AV39TFCaixa_Emissao, AV40TFCaixa_Emissao_To, AV45TFCaixa_Vencimento, AV46TFCaixa_Vencimento_To, AV51TFCaixa_TipoDeContaCod, AV52TFCaixa_TipoDeContaCod_Sel, AV55TFCaixa_Descricao, AV56TFCaixa_Descricao_Sel, AV59TFCaixa_Valor, AV60TFCaixa_Valor_To, AV33ddo_Caixa_CodigoTitleControlIdToReplace, AV37ddo_Caixa_DocumentoTitleControlIdToReplace, AV43ddo_Caixa_EmissaoTitleControlIdToReplace, AV49ddo_Caixa_VencimentoTitleControlIdToReplace, AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace, AV57ddo_Caixa_DescricaoTitleControlIdToReplace, AV61ddo_Caixa_ValorTitleControlIdToReplace, AV69Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Caixa_Documento1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Caixa_Documento2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Caixa_Documento3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFCaixa_Codigo, AV32TFCaixa_Codigo_To, AV35TFCaixa_Documento, AV36TFCaixa_Documento_Sel, AV39TFCaixa_Emissao, AV40TFCaixa_Emissao_To, AV45TFCaixa_Vencimento, AV46TFCaixa_Vencimento_To, AV51TFCaixa_TipoDeContaCod, AV52TFCaixa_TipoDeContaCod_Sel, AV55TFCaixa_Descricao, AV56TFCaixa_Descricao_Sel, AV59TFCaixa_Valor, AV60TFCaixa_Valor_To, AV33ddo_Caixa_CodigoTitleControlIdToReplace, AV37ddo_Caixa_DocumentoTitleControlIdToReplace, AV43ddo_Caixa_EmissaoTitleControlIdToReplace, AV49ddo_Caixa_VencimentoTitleControlIdToReplace, AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace, AV57ddo_Caixa_DescricaoTitleControlIdToReplace, AV61ddo_Caixa_ValorTitleControlIdToReplace, AV69Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUPFQ0( )
      {
         /* Before Start, stand alone formulas. */
         AV69Pgmname = "PromptCaixa";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E29FQ2 */
         E29FQ2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV62DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCAIXA_CODIGOTITLEFILTERDATA"), AV30Caixa_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCAIXA_DOCUMENTOTITLEFILTERDATA"), AV34Caixa_DocumentoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCAIXA_EMISSAOTITLEFILTERDATA"), AV38Caixa_EmissaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCAIXA_VENCIMENTOTITLEFILTERDATA"), AV44Caixa_VencimentoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCAIXA_TIPODECONTACODTITLEFILTERDATA"), AV50Caixa_TipoDeContaCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCAIXA_DESCRICAOTITLEFILTERDATA"), AV54Caixa_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCAIXA_VALORTITLEFILTERDATA"), AV58Caixa_ValorTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17Caixa_Documento1 = StringUtil.Upper( cgiGet( edtavCaixa_documento1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Caixa_Documento1", AV17Caixa_Documento1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            AV21Caixa_Documento2 = StringUtil.Upper( cgiGet( edtavCaixa_documento2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Caixa_Documento2", AV21Caixa_Documento2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            AV25Caixa_Documento3 = StringUtil.Upper( cgiGet( edtavCaixa_documento3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Caixa_Documento3", AV25Caixa_Documento3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcaixa_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcaixa_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCAIXA_CODIGO");
               GX_FocusControl = edtavTfcaixa_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV31TFCaixa_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFCaixa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFCaixa_Codigo), 6, 0)));
            }
            else
            {
               AV31TFCaixa_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfcaixa_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFCaixa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFCaixa_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcaixa_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcaixa_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCAIXA_CODIGO_TO");
               GX_FocusControl = edtavTfcaixa_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32TFCaixa_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFCaixa_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFCaixa_Codigo_To), 6, 0)));
            }
            else
            {
               AV32TFCaixa_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcaixa_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFCaixa_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFCaixa_Codigo_To), 6, 0)));
            }
            AV35TFCaixa_Documento = StringUtil.Upper( cgiGet( edtavTfcaixa_documento_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFCaixa_Documento", AV35TFCaixa_Documento);
            AV36TFCaixa_Documento_Sel = StringUtil.Upper( cgiGet( edtavTfcaixa_documento_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFCaixa_Documento_Sel", AV36TFCaixa_Documento_Sel);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcaixa_emissao_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFCaixa_Emissao"}), 1, "vTFCAIXA_EMISSAO");
               GX_FocusControl = edtavTfcaixa_emissao_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39TFCaixa_Emissao = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFCaixa_Emissao", context.localUtil.Format(AV39TFCaixa_Emissao, "99/99/99"));
            }
            else
            {
               AV39TFCaixa_Emissao = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcaixa_emissao_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFCaixa_Emissao", context.localUtil.Format(AV39TFCaixa_Emissao, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcaixa_emissao_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFCaixa_Emissao_To"}), 1, "vTFCAIXA_EMISSAO_TO");
               GX_FocusControl = edtavTfcaixa_emissao_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40TFCaixa_Emissao_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFCaixa_Emissao_To", context.localUtil.Format(AV40TFCaixa_Emissao_To, "99/99/99"));
            }
            else
            {
               AV40TFCaixa_Emissao_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcaixa_emissao_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFCaixa_Emissao_To", context.localUtil.Format(AV40TFCaixa_Emissao_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_caixa_emissaoauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Caixa_Emissao Aux Date"}), 1, "vDDO_CAIXA_EMISSAOAUXDATE");
               GX_FocusControl = edtavDdo_caixa_emissaoauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV41DDO_Caixa_EmissaoAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41DDO_Caixa_EmissaoAuxDate", context.localUtil.Format(AV41DDO_Caixa_EmissaoAuxDate, "99/99/99"));
            }
            else
            {
               AV41DDO_Caixa_EmissaoAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_caixa_emissaoauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41DDO_Caixa_EmissaoAuxDate", context.localUtil.Format(AV41DDO_Caixa_EmissaoAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_caixa_emissaoauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Caixa_Emissao Aux Date To"}), 1, "vDDO_CAIXA_EMISSAOAUXDATETO");
               GX_FocusControl = edtavDdo_caixa_emissaoauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV42DDO_Caixa_EmissaoAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42DDO_Caixa_EmissaoAuxDateTo", context.localUtil.Format(AV42DDO_Caixa_EmissaoAuxDateTo, "99/99/99"));
            }
            else
            {
               AV42DDO_Caixa_EmissaoAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_caixa_emissaoauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42DDO_Caixa_EmissaoAuxDateTo", context.localUtil.Format(AV42DDO_Caixa_EmissaoAuxDateTo, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcaixa_vencimento_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFCaixa_Vencimento"}), 1, "vTFCAIXA_VENCIMENTO");
               GX_FocusControl = edtavTfcaixa_vencimento_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV45TFCaixa_Vencimento = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFCaixa_Vencimento", context.localUtil.Format(AV45TFCaixa_Vencimento, "99/99/99"));
            }
            else
            {
               AV45TFCaixa_Vencimento = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcaixa_vencimento_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFCaixa_Vencimento", context.localUtil.Format(AV45TFCaixa_Vencimento, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcaixa_vencimento_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFCaixa_Vencimento_To"}), 1, "vTFCAIXA_VENCIMENTO_TO");
               GX_FocusControl = edtavTfcaixa_vencimento_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV46TFCaixa_Vencimento_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFCaixa_Vencimento_To", context.localUtil.Format(AV46TFCaixa_Vencimento_To, "99/99/99"));
            }
            else
            {
               AV46TFCaixa_Vencimento_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcaixa_vencimento_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFCaixa_Vencimento_To", context.localUtil.Format(AV46TFCaixa_Vencimento_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_caixa_vencimentoauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Caixa_Vencimento Aux Date"}), 1, "vDDO_CAIXA_VENCIMENTOAUXDATE");
               GX_FocusControl = edtavDdo_caixa_vencimentoauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV47DDO_Caixa_VencimentoAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47DDO_Caixa_VencimentoAuxDate", context.localUtil.Format(AV47DDO_Caixa_VencimentoAuxDate, "99/99/99"));
            }
            else
            {
               AV47DDO_Caixa_VencimentoAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_caixa_vencimentoauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47DDO_Caixa_VencimentoAuxDate", context.localUtil.Format(AV47DDO_Caixa_VencimentoAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_caixa_vencimentoauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Caixa_Vencimento Aux Date To"}), 1, "vDDO_CAIXA_VENCIMENTOAUXDATETO");
               GX_FocusControl = edtavDdo_caixa_vencimentoauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48DDO_Caixa_VencimentoAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48DDO_Caixa_VencimentoAuxDateTo", context.localUtil.Format(AV48DDO_Caixa_VencimentoAuxDateTo, "99/99/99"));
            }
            else
            {
               AV48DDO_Caixa_VencimentoAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_caixa_vencimentoauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48DDO_Caixa_VencimentoAuxDateTo", context.localUtil.Format(AV48DDO_Caixa_VencimentoAuxDateTo, "99/99/99"));
            }
            AV51TFCaixa_TipoDeContaCod = cgiGet( edtavTfcaixa_tipodecontacod_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFCaixa_TipoDeContaCod", AV51TFCaixa_TipoDeContaCod);
            AV52TFCaixa_TipoDeContaCod_Sel = cgiGet( edtavTfcaixa_tipodecontacod_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFCaixa_TipoDeContaCod_Sel", AV52TFCaixa_TipoDeContaCod_Sel);
            AV55TFCaixa_Descricao = StringUtil.Upper( cgiGet( edtavTfcaixa_descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFCaixa_Descricao", AV55TFCaixa_Descricao);
            AV56TFCaixa_Descricao_Sel = StringUtil.Upper( cgiGet( edtavTfcaixa_descricao_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFCaixa_Descricao_Sel", AV56TFCaixa_Descricao_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcaixa_valor_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcaixa_valor_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCAIXA_VALOR");
               GX_FocusControl = edtavTfcaixa_valor_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV59TFCaixa_Valor = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFCaixa_Valor", StringUtil.LTrim( StringUtil.Str( AV59TFCaixa_Valor, 18, 5)));
            }
            else
            {
               AV59TFCaixa_Valor = context.localUtil.CToN( cgiGet( edtavTfcaixa_valor_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFCaixa_Valor", StringUtil.LTrim( StringUtil.Str( AV59TFCaixa_Valor, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcaixa_valor_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcaixa_valor_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCAIXA_VALOR_TO");
               GX_FocusControl = edtavTfcaixa_valor_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV60TFCaixa_Valor_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFCaixa_Valor_To", StringUtil.LTrim( StringUtil.Str( AV60TFCaixa_Valor_To, 18, 5)));
            }
            else
            {
               AV60TFCaixa_Valor_To = context.localUtil.CToN( cgiGet( edtavTfcaixa_valor_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFCaixa_Valor_To", StringUtil.LTrim( StringUtil.Str( AV60TFCaixa_Valor_To, 18, 5)));
            }
            AV33ddo_Caixa_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_caixa_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_Caixa_CodigoTitleControlIdToReplace", AV33ddo_Caixa_CodigoTitleControlIdToReplace);
            AV37ddo_Caixa_DocumentoTitleControlIdToReplace = cgiGet( edtavDdo_caixa_documentotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_Caixa_DocumentoTitleControlIdToReplace", AV37ddo_Caixa_DocumentoTitleControlIdToReplace);
            AV43ddo_Caixa_EmissaoTitleControlIdToReplace = cgiGet( edtavDdo_caixa_emissaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_Caixa_EmissaoTitleControlIdToReplace", AV43ddo_Caixa_EmissaoTitleControlIdToReplace);
            AV49ddo_Caixa_VencimentoTitleControlIdToReplace = cgiGet( edtavDdo_caixa_vencimentotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_Caixa_VencimentoTitleControlIdToReplace", AV49ddo_Caixa_VencimentoTitleControlIdToReplace);
            AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace = cgiGet( edtavDdo_caixa_tipodecontacodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace", AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace);
            AV57ddo_Caixa_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_caixa_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_Caixa_DescricaoTitleControlIdToReplace", AV57ddo_Caixa_DescricaoTitleControlIdToReplace);
            AV61ddo_Caixa_ValorTitleControlIdToReplace = cgiGet( edtavDdo_caixa_valortitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_Caixa_ValorTitleControlIdToReplace", AV61ddo_Caixa_ValorTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_80 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_80"), ",", "."));
            AV64GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV65GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_caixa_codigo_Caption = cgiGet( "DDO_CAIXA_CODIGO_Caption");
            Ddo_caixa_codigo_Tooltip = cgiGet( "DDO_CAIXA_CODIGO_Tooltip");
            Ddo_caixa_codigo_Cls = cgiGet( "DDO_CAIXA_CODIGO_Cls");
            Ddo_caixa_codigo_Filteredtext_set = cgiGet( "DDO_CAIXA_CODIGO_Filteredtext_set");
            Ddo_caixa_codigo_Filteredtextto_set = cgiGet( "DDO_CAIXA_CODIGO_Filteredtextto_set");
            Ddo_caixa_codigo_Dropdownoptionstype = cgiGet( "DDO_CAIXA_CODIGO_Dropdownoptionstype");
            Ddo_caixa_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_CAIXA_CODIGO_Titlecontrolidtoreplace");
            Ddo_caixa_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_CODIGO_Includesortasc"));
            Ddo_caixa_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_CODIGO_Includesortdsc"));
            Ddo_caixa_codigo_Sortedstatus = cgiGet( "DDO_CAIXA_CODIGO_Sortedstatus");
            Ddo_caixa_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_CODIGO_Includefilter"));
            Ddo_caixa_codigo_Filtertype = cgiGet( "DDO_CAIXA_CODIGO_Filtertype");
            Ddo_caixa_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_CODIGO_Filterisrange"));
            Ddo_caixa_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_CODIGO_Includedatalist"));
            Ddo_caixa_codigo_Datalistfixedvalues = cgiGet( "DDO_CAIXA_CODIGO_Datalistfixedvalues");
            Ddo_caixa_codigo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CAIXA_CODIGO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_caixa_codigo_Sortasc = cgiGet( "DDO_CAIXA_CODIGO_Sortasc");
            Ddo_caixa_codigo_Sortdsc = cgiGet( "DDO_CAIXA_CODIGO_Sortdsc");
            Ddo_caixa_codigo_Loadingdata = cgiGet( "DDO_CAIXA_CODIGO_Loadingdata");
            Ddo_caixa_codigo_Cleanfilter = cgiGet( "DDO_CAIXA_CODIGO_Cleanfilter");
            Ddo_caixa_codigo_Rangefilterfrom = cgiGet( "DDO_CAIXA_CODIGO_Rangefilterfrom");
            Ddo_caixa_codigo_Rangefilterto = cgiGet( "DDO_CAIXA_CODIGO_Rangefilterto");
            Ddo_caixa_codigo_Noresultsfound = cgiGet( "DDO_CAIXA_CODIGO_Noresultsfound");
            Ddo_caixa_codigo_Searchbuttontext = cgiGet( "DDO_CAIXA_CODIGO_Searchbuttontext");
            Ddo_caixa_documento_Caption = cgiGet( "DDO_CAIXA_DOCUMENTO_Caption");
            Ddo_caixa_documento_Tooltip = cgiGet( "DDO_CAIXA_DOCUMENTO_Tooltip");
            Ddo_caixa_documento_Cls = cgiGet( "DDO_CAIXA_DOCUMENTO_Cls");
            Ddo_caixa_documento_Filteredtext_set = cgiGet( "DDO_CAIXA_DOCUMENTO_Filteredtext_set");
            Ddo_caixa_documento_Selectedvalue_set = cgiGet( "DDO_CAIXA_DOCUMENTO_Selectedvalue_set");
            Ddo_caixa_documento_Dropdownoptionstype = cgiGet( "DDO_CAIXA_DOCUMENTO_Dropdownoptionstype");
            Ddo_caixa_documento_Titlecontrolidtoreplace = cgiGet( "DDO_CAIXA_DOCUMENTO_Titlecontrolidtoreplace");
            Ddo_caixa_documento_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_DOCUMENTO_Includesortasc"));
            Ddo_caixa_documento_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_DOCUMENTO_Includesortdsc"));
            Ddo_caixa_documento_Sortedstatus = cgiGet( "DDO_CAIXA_DOCUMENTO_Sortedstatus");
            Ddo_caixa_documento_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_DOCUMENTO_Includefilter"));
            Ddo_caixa_documento_Filtertype = cgiGet( "DDO_CAIXA_DOCUMENTO_Filtertype");
            Ddo_caixa_documento_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_DOCUMENTO_Filterisrange"));
            Ddo_caixa_documento_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_DOCUMENTO_Includedatalist"));
            Ddo_caixa_documento_Datalisttype = cgiGet( "DDO_CAIXA_DOCUMENTO_Datalisttype");
            Ddo_caixa_documento_Datalistfixedvalues = cgiGet( "DDO_CAIXA_DOCUMENTO_Datalistfixedvalues");
            Ddo_caixa_documento_Datalistproc = cgiGet( "DDO_CAIXA_DOCUMENTO_Datalistproc");
            Ddo_caixa_documento_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CAIXA_DOCUMENTO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_caixa_documento_Sortasc = cgiGet( "DDO_CAIXA_DOCUMENTO_Sortasc");
            Ddo_caixa_documento_Sortdsc = cgiGet( "DDO_CAIXA_DOCUMENTO_Sortdsc");
            Ddo_caixa_documento_Loadingdata = cgiGet( "DDO_CAIXA_DOCUMENTO_Loadingdata");
            Ddo_caixa_documento_Cleanfilter = cgiGet( "DDO_CAIXA_DOCUMENTO_Cleanfilter");
            Ddo_caixa_documento_Rangefilterfrom = cgiGet( "DDO_CAIXA_DOCUMENTO_Rangefilterfrom");
            Ddo_caixa_documento_Rangefilterto = cgiGet( "DDO_CAIXA_DOCUMENTO_Rangefilterto");
            Ddo_caixa_documento_Noresultsfound = cgiGet( "DDO_CAIXA_DOCUMENTO_Noresultsfound");
            Ddo_caixa_documento_Searchbuttontext = cgiGet( "DDO_CAIXA_DOCUMENTO_Searchbuttontext");
            Ddo_caixa_emissao_Caption = cgiGet( "DDO_CAIXA_EMISSAO_Caption");
            Ddo_caixa_emissao_Tooltip = cgiGet( "DDO_CAIXA_EMISSAO_Tooltip");
            Ddo_caixa_emissao_Cls = cgiGet( "DDO_CAIXA_EMISSAO_Cls");
            Ddo_caixa_emissao_Filteredtext_set = cgiGet( "DDO_CAIXA_EMISSAO_Filteredtext_set");
            Ddo_caixa_emissao_Filteredtextto_set = cgiGet( "DDO_CAIXA_EMISSAO_Filteredtextto_set");
            Ddo_caixa_emissao_Dropdownoptionstype = cgiGet( "DDO_CAIXA_EMISSAO_Dropdownoptionstype");
            Ddo_caixa_emissao_Titlecontrolidtoreplace = cgiGet( "DDO_CAIXA_EMISSAO_Titlecontrolidtoreplace");
            Ddo_caixa_emissao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_EMISSAO_Includesortasc"));
            Ddo_caixa_emissao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_EMISSAO_Includesortdsc"));
            Ddo_caixa_emissao_Sortedstatus = cgiGet( "DDO_CAIXA_EMISSAO_Sortedstatus");
            Ddo_caixa_emissao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_EMISSAO_Includefilter"));
            Ddo_caixa_emissao_Filtertype = cgiGet( "DDO_CAIXA_EMISSAO_Filtertype");
            Ddo_caixa_emissao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_EMISSAO_Filterisrange"));
            Ddo_caixa_emissao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_EMISSAO_Includedatalist"));
            Ddo_caixa_emissao_Datalistfixedvalues = cgiGet( "DDO_CAIXA_EMISSAO_Datalistfixedvalues");
            Ddo_caixa_emissao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CAIXA_EMISSAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_caixa_emissao_Sortasc = cgiGet( "DDO_CAIXA_EMISSAO_Sortasc");
            Ddo_caixa_emissao_Sortdsc = cgiGet( "DDO_CAIXA_EMISSAO_Sortdsc");
            Ddo_caixa_emissao_Loadingdata = cgiGet( "DDO_CAIXA_EMISSAO_Loadingdata");
            Ddo_caixa_emissao_Cleanfilter = cgiGet( "DDO_CAIXA_EMISSAO_Cleanfilter");
            Ddo_caixa_emissao_Rangefilterfrom = cgiGet( "DDO_CAIXA_EMISSAO_Rangefilterfrom");
            Ddo_caixa_emissao_Rangefilterto = cgiGet( "DDO_CAIXA_EMISSAO_Rangefilterto");
            Ddo_caixa_emissao_Noresultsfound = cgiGet( "DDO_CAIXA_EMISSAO_Noresultsfound");
            Ddo_caixa_emissao_Searchbuttontext = cgiGet( "DDO_CAIXA_EMISSAO_Searchbuttontext");
            Ddo_caixa_vencimento_Caption = cgiGet( "DDO_CAIXA_VENCIMENTO_Caption");
            Ddo_caixa_vencimento_Tooltip = cgiGet( "DDO_CAIXA_VENCIMENTO_Tooltip");
            Ddo_caixa_vencimento_Cls = cgiGet( "DDO_CAIXA_VENCIMENTO_Cls");
            Ddo_caixa_vencimento_Filteredtext_set = cgiGet( "DDO_CAIXA_VENCIMENTO_Filteredtext_set");
            Ddo_caixa_vencimento_Filteredtextto_set = cgiGet( "DDO_CAIXA_VENCIMENTO_Filteredtextto_set");
            Ddo_caixa_vencimento_Dropdownoptionstype = cgiGet( "DDO_CAIXA_VENCIMENTO_Dropdownoptionstype");
            Ddo_caixa_vencimento_Titlecontrolidtoreplace = cgiGet( "DDO_CAIXA_VENCIMENTO_Titlecontrolidtoreplace");
            Ddo_caixa_vencimento_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_VENCIMENTO_Includesortasc"));
            Ddo_caixa_vencimento_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_VENCIMENTO_Includesortdsc"));
            Ddo_caixa_vencimento_Sortedstatus = cgiGet( "DDO_CAIXA_VENCIMENTO_Sortedstatus");
            Ddo_caixa_vencimento_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_VENCIMENTO_Includefilter"));
            Ddo_caixa_vencimento_Filtertype = cgiGet( "DDO_CAIXA_VENCIMENTO_Filtertype");
            Ddo_caixa_vencimento_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_VENCIMENTO_Filterisrange"));
            Ddo_caixa_vencimento_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_VENCIMENTO_Includedatalist"));
            Ddo_caixa_vencimento_Datalistfixedvalues = cgiGet( "DDO_CAIXA_VENCIMENTO_Datalistfixedvalues");
            Ddo_caixa_vencimento_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CAIXA_VENCIMENTO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_caixa_vencimento_Sortasc = cgiGet( "DDO_CAIXA_VENCIMENTO_Sortasc");
            Ddo_caixa_vencimento_Sortdsc = cgiGet( "DDO_CAIXA_VENCIMENTO_Sortdsc");
            Ddo_caixa_vencimento_Loadingdata = cgiGet( "DDO_CAIXA_VENCIMENTO_Loadingdata");
            Ddo_caixa_vencimento_Cleanfilter = cgiGet( "DDO_CAIXA_VENCIMENTO_Cleanfilter");
            Ddo_caixa_vencimento_Rangefilterfrom = cgiGet( "DDO_CAIXA_VENCIMENTO_Rangefilterfrom");
            Ddo_caixa_vencimento_Rangefilterto = cgiGet( "DDO_CAIXA_VENCIMENTO_Rangefilterto");
            Ddo_caixa_vencimento_Noresultsfound = cgiGet( "DDO_CAIXA_VENCIMENTO_Noresultsfound");
            Ddo_caixa_vencimento_Searchbuttontext = cgiGet( "DDO_CAIXA_VENCIMENTO_Searchbuttontext");
            Ddo_caixa_tipodecontacod_Caption = cgiGet( "DDO_CAIXA_TIPODECONTACOD_Caption");
            Ddo_caixa_tipodecontacod_Tooltip = cgiGet( "DDO_CAIXA_TIPODECONTACOD_Tooltip");
            Ddo_caixa_tipodecontacod_Cls = cgiGet( "DDO_CAIXA_TIPODECONTACOD_Cls");
            Ddo_caixa_tipodecontacod_Filteredtext_set = cgiGet( "DDO_CAIXA_TIPODECONTACOD_Filteredtext_set");
            Ddo_caixa_tipodecontacod_Selectedvalue_set = cgiGet( "DDO_CAIXA_TIPODECONTACOD_Selectedvalue_set");
            Ddo_caixa_tipodecontacod_Dropdownoptionstype = cgiGet( "DDO_CAIXA_TIPODECONTACOD_Dropdownoptionstype");
            Ddo_caixa_tipodecontacod_Titlecontrolidtoreplace = cgiGet( "DDO_CAIXA_TIPODECONTACOD_Titlecontrolidtoreplace");
            Ddo_caixa_tipodecontacod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_TIPODECONTACOD_Includesortasc"));
            Ddo_caixa_tipodecontacod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_TIPODECONTACOD_Includesortdsc"));
            Ddo_caixa_tipodecontacod_Sortedstatus = cgiGet( "DDO_CAIXA_TIPODECONTACOD_Sortedstatus");
            Ddo_caixa_tipodecontacod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_TIPODECONTACOD_Includefilter"));
            Ddo_caixa_tipodecontacod_Filtertype = cgiGet( "DDO_CAIXA_TIPODECONTACOD_Filtertype");
            Ddo_caixa_tipodecontacod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_TIPODECONTACOD_Filterisrange"));
            Ddo_caixa_tipodecontacod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_TIPODECONTACOD_Includedatalist"));
            Ddo_caixa_tipodecontacod_Datalisttype = cgiGet( "DDO_CAIXA_TIPODECONTACOD_Datalisttype");
            Ddo_caixa_tipodecontacod_Datalistfixedvalues = cgiGet( "DDO_CAIXA_TIPODECONTACOD_Datalistfixedvalues");
            Ddo_caixa_tipodecontacod_Datalistproc = cgiGet( "DDO_CAIXA_TIPODECONTACOD_Datalistproc");
            Ddo_caixa_tipodecontacod_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CAIXA_TIPODECONTACOD_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_caixa_tipodecontacod_Sortasc = cgiGet( "DDO_CAIXA_TIPODECONTACOD_Sortasc");
            Ddo_caixa_tipodecontacod_Sortdsc = cgiGet( "DDO_CAIXA_TIPODECONTACOD_Sortdsc");
            Ddo_caixa_tipodecontacod_Loadingdata = cgiGet( "DDO_CAIXA_TIPODECONTACOD_Loadingdata");
            Ddo_caixa_tipodecontacod_Cleanfilter = cgiGet( "DDO_CAIXA_TIPODECONTACOD_Cleanfilter");
            Ddo_caixa_tipodecontacod_Rangefilterfrom = cgiGet( "DDO_CAIXA_TIPODECONTACOD_Rangefilterfrom");
            Ddo_caixa_tipodecontacod_Rangefilterto = cgiGet( "DDO_CAIXA_TIPODECONTACOD_Rangefilterto");
            Ddo_caixa_tipodecontacod_Noresultsfound = cgiGet( "DDO_CAIXA_TIPODECONTACOD_Noresultsfound");
            Ddo_caixa_tipodecontacod_Searchbuttontext = cgiGet( "DDO_CAIXA_TIPODECONTACOD_Searchbuttontext");
            Ddo_caixa_descricao_Caption = cgiGet( "DDO_CAIXA_DESCRICAO_Caption");
            Ddo_caixa_descricao_Tooltip = cgiGet( "DDO_CAIXA_DESCRICAO_Tooltip");
            Ddo_caixa_descricao_Cls = cgiGet( "DDO_CAIXA_DESCRICAO_Cls");
            Ddo_caixa_descricao_Filteredtext_set = cgiGet( "DDO_CAIXA_DESCRICAO_Filteredtext_set");
            Ddo_caixa_descricao_Selectedvalue_set = cgiGet( "DDO_CAIXA_DESCRICAO_Selectedvalue_set");
            Ddo_caixa_descricao_Dropdownoptionstype = cgiGet( "DDO_CAIXA_DESCRICAO_Dropdownoptionstype");
            Ddo_caixa_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_CAIXA_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_caixa_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_DESCRICAO_Includesortasc"));
            Ddo_caixa_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_DESCRICAO_Includesortdsc"));
            Ddo_caixa_descricao_Sortedstatus = cgiGet( "DDO_CAIXA_DESCRICAO_Sortedstatus");
            Ddo_caixa_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_DESCRICAO_Includefilter"));
            Ddo_caixa_descricao_Filtertype = cgiGet( "DDO_CAIXA_DESCRICAO_Filtertype");
            Ddo_caixa_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_DESCRICAO_Filterisrange"));
            Ddo_caixa_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_DESCRICAO_Includedatalist"));
            Ddo_caixa_descricao_Datalisttype = cgiGet( "DDO_CAIXA_DESCRICAO_Datalisttype");
            Ddo_caixa_descricao_Datalistfixedvalues = cgiGet( "DDO_CAIXA_DESCRICAO_Datalistfixedvalues");
            Ddo_caixa_descricao_Datalistproc = cgiGet( "DDO_CAIXA_DESCRICAO_Datalistproc");
            Ddo_caixa_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CAIXA_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_caixa_descricao_Sortasc = cgiGet( "DDO_CAIXA_DESCRICAO_Sortasc");
            Ddo_caixa_descricao_Sortdsc = cgiGet( "DDO_CAIXA_DESCRICAO_Sortdsc");
            Ddo_caixa_descricao_Loadingdata = cgiGet( "DDO_CAIXA_DESCRICAO_Loadingdata");
            Ddo_caixa_descricao_Cleanfilter = cgiGet( "DDO_CAIXA_DESCRICAO_Cleanfilter");
            Ddo_caixa_descricao_Rangefilterfrom = cgiGet( "DDO_CAIXA_DESCRICAO_Rangefilterfrom");
            Ddo_caixa_descricao_Rangefilterto = cgiGet( "DDO_CAIXA_DESCRICAO_Rangefilterto");
            Ddo_caixa_descricao_Noresultsfound = cgiGet( "DDO_CAIXA_DESCRICAO_Noresultsfound");
            Ddo_caixa_descricao_Searchbuttontext = cgiGet( "DDO_CAIXA_DESCRICAO_Searchbuttontext");
            Ddo_caixa_valor_Caption = cgiGet( "DDO_CAIXA_VALOR_Caption");
            Ddo_caixa_valor_Tooltip = cgiGet( "DDO_CAIXA_VALOR_Tooltip");
            Ddo_caixa_valor_Cls = cgiGet( "DDO_CAIXA_VALOR_Cls");
            Ddo_caixa_valor_Filteredtext_set = cgiGet( "DDO_CAIXA_VALOR_Filteredtext_set");
            Ddo_caixa_valor_Filteredtextto_set = cgiGet( "DDO_CAIXA_VALOR_Filteredtextto_set");
            Ddo_caixa_valor_Dropdownoptionstype = cgiGet( "DDO_CAIXA_VALOR_Dropdownoptionstype");
            Ddo_caixa_valor_Titlecontrolidtoreplace = cgiGet( "DDO_CAIXA_VALOR_Titlecontrolidtoreplace");
            Ddo_caixa_valor_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_VALOR_Includesortasc"));
            Ddo_caixa_valor_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_VALOR_Includesortdsc"));
            Ddo_caixa_valor_Sortedstatus = cgiGet( "DDO_CAIXA_VALOR_Sortedstatus");
            Ddo_caixa_valor_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_VALOR_Includefilter"));
            Ddo_caixa_valor_Filtertype = cgiGet( "DDO_CAIXA_VALOR_Filtertype");
            Ddo_caixa_valor_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_VALOR_Filterisrange"));
            Ddo_caixa_valor_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CAIXA_VALOR_Includedatalist"));
            Ddo_caixa_valor_Datalistfixedvalues = cgiGet( "DDO_CAIXA_VALOR_Datalistfixedvalues");
            Ddo_caixa_valor_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CAIXA_VALOR_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_caixa_valor_Sortasc = cgiGet( "DDO_CAIXA_VALOR_Sortasc");
            Ddo_caixa_valor_Sortdsc = cgiGet( "DDO_CAIXA_VALOR_Sortdsc");
            Ddo_caixa_valor_Loadingdata = cgiGet( "DDO_CAIXA_VALOR_Loadingdata");
            Ddo_caixa_valor_Cleanfilter = cgiGet( "DDO_CAIXA_VALOR_Cleanfilter");
            Ddo_caixa_valor_Rangefilterfrom = cgiGet( "DDO_CAIXA_VALOR_Rangefilterfrom");
            Ddo_caixa_valor_Rangefilterto = cgiGet( "DDO_CAIXA_VALOR_Rangefilterto");
            Ddo_caixa_valor_Noresultsfound = cgiGet( "DDO_CAIXA_VALOR_Noresultsfound");
            Ddo_caixa_valor_Searchbuttontext = cgiGet( "DDO_CAIXA_VALOR_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_caixa_codigo_Activeeventkey = cgiGet( "DDO_CAIXA_CODIGO_Activeeventkey");
            Ddo_caixa_codigo_Filteredtext_get = cgiGet( "DDO_CAIXA_CODIGO_Filteredtext_get");
            Ddo_caixa_codigo_Filteredtextto_get = cgiGet( "DDO_CAIXA_CODIGO_Filteredtextto_get");
            Ddo_caixa_documento_Activeeventkey = cgiGet( "DDO_CAIXA_DOCUMENTO_Activeeventkey");
            Ddo_caixa_documento_Filteredtext_get = cgiGet( "DDO_CAIXA_DOCUMENTO_Filteredtext_get");
            Ddo_caixa_documento_Selectedvalue_get = cgiGet( "DDO_CAIXA_DOCUMENTO_Selectedvalue_get");
            Ddo_caixa_emissao_Activeeventkey = cgiGet( "DDO_CAIXA_EMISSAO_Activeeventkey");
            Ddo_caixa_emissao_Filteredtext_get = cgiGet( "DDO_CAIXA_EMISSAO_Filteredtext_get");
            Ddo_caixa_emissao_Filteredtextto_get = cgiGet( "DDO_CAIXA_EMISSAO_Filteredtextto_get");
            Ddo_caixa_vencimento_Activeeventkey = cgiGet( "DDO_CAIXA_VENCIMENTO_Activeeventkey");
            Ddo_caixa_vencimento_Filteredtext_get = cgiGet( "DDO_CAIXA_VENCIMENTO_Filteredtext_get");
            Ddo_caixa_vencimento_Filteredtextto_get = cgiGet( "DDO_CAIXA_VENCIMENTO_Filteredtextto_get");
            Ddo_caixa_tipodecontacod_Activeeventkey = cgiGet( "DDO_CAIXA_TIPODECONTACOD_Activeeventkey");
            Ddo_caixa_tipodecontacod_Filteredtext_get = cgiGet( "DDO_CAIXA_TIPODECONTACOD_Filteredtext_get");
            Ddo_caixa_tipodecontacod_Selectedvalue_get = cgiGet( "DDO_CAIXA_TIPODECONTACOD_Selectedvalue_get");
            Ddo_caixa_descricao_Activeeventkey = cgiGet( "DDO_CAIXA_DESCRICAO_Activeeventkey");
            Ddo_caixa_descricao_Filteredtext_get = cgiGet( "DDO_CAIXA_DESCRICAO_Filteredtext_get");
            Ddo_caixa_descricao_Selectedvalue_get = cgiGet( "DDO_CAIXA_DESCRICAO_Selectedvalue_get");
            Ddo_caixa_valor_Activeeventkey = cgiGet( "DDO_CAIXA_VALOR_Activeeventkey");
            Ddo_caixa_valor_Filteredtext_get = cgiGet( "DDO_CAIXA_VALOR_Filteredtext_get");
            Ddo_caixa_valor_Filteredtextto_get = cgiGet( "DDO_CAIXA_VALOR_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCAIXA_DOCUMENTO1"), AV17Caixa_Documento1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCAIXA_DOCUMENTO2"), AV21Caixa_Documento2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCAIXA_DOCUMENTO3"), AV25Caixa_Documento3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCAIXA_CODIGO"), ",", ".") != Convert.ToDecimal( AV31TFCaixa_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCAIXA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV32TFCaixa_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCAIXA_DOCUMENTO"), AV35TFCaixa_Documento) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCAIXA_DOCUMENTO_SEL"), AV36TFCaixa_Documento_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCAIXA_EMISSAO"), 0) != AV39TFCaixa_Emissao )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCAIXA_EMISSAO_TO"), 0) != AV40TFCaixa_Emissao_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCAIXA_VENCIMENTO"), 0) != AV45TFCaixa_Vencimento )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCAIXA_VENCIMENTO_TO"), 0) != AV46TFCaixa_Vencimento_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCAIXA_TIPODECONTACOD"), AV51TFCaixa_TipoDeContaCod) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCAIXA_TIPODECONTACOD_SEL"), AV52TFCaixa_TipoDeContaCod_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCAIXA_DESCRICAO"), AV55TFCaixa_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCAIXA_DESCRICAO_SEL"), AV56TFCaixa_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCAIXA_VALOR"), ",", ".") != AV59TFCaixa_Valor )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCAIXA_VALOR_TO"), ",", ".") != AV60TFCaixa_Valor_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E29FQ2 */
         E29FQ2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E29FQ2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "CAIXA_DOCUMENTO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "CAIXA_DOCUMENTO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "CAIXA_DOCUMENTO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfcaixa_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcaixa_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcaixa_codigo_Visible), 5, 0)));
         edtavTfcaixa_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcaixa_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcaixa_codigo_to_Visible), 5, 0)));
         edtavTfcaixa_documento_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcaixa_documento_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcaixa_documento_Visible), 5, 0)));
         edtavTfcaixa_documento_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcaixa_documento_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcaixa_documento_sel_Visible), 5, 0)));
         edtavTfcaixa_emissao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcaixa_emissao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcaixa_emissao_Visible), 5, 0)));
         edtavTfcaixa_emissao_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcaixa_emissao_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcaixa_emissao_to_Visible), 5, 0)));
         edtavTfcaixa_vencimento_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcaixa_vencimento_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcaixa_vencimento_Visible), 5, 0)));
         edtavTfcaixa_vencimento_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcaixa_vencimento_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcaixa_vencimento_to_Visible), 5, 0)));
         edtavTfcaixa_tipodecontacod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcaixa_tipodecontacod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcaixa_tipodecontacod_Visible), 5, 0)));
         edtavTfcaixa_tipodecontacod_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcaixa_tipodecontacod_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcaixa_tipodecontacod_sel_Visible), 5, 0)));
         edtavTfcaixa_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcaixa_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcaixa_descricao_Visible), 5, 0)));
         edtavTfcaixa_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcaixa_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcaixa_descricao_sel_Visible), 5, 0)));
         edtavTfcaixa_valor_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcaixa_valor_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcaixa_valor_Visible), 5, 0)));
         edtavTfcaixa_valor_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcaixa_valor_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcaixa_valor_to_Visible), 5, 0)));
         Ddo_caixa_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_Caixa_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_codigo_Internalname, "TitleControlIdToReplace", Ddo_caixa_codigo_Titlecontrolidtoreplace);
         AV33ddo_Caixa_CodigoTitleControlIdToReplace = Ddo_caixa_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_Caixa_CodigoTitleControlIdToReplace", AV33ddo_Caixa_CodigoTitleControlIdToReplace);
         edtavDdo_caixa_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_caixa_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_caixa_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_caixa_documento_Titlecontrolidtoreplace = subGrid_Internalname+"_Caixa_Documento";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_documento_Internalname, "TitleControlIdToReplace", Ddo_caixa_documento_Titlecontrolidtoreplace);
         AV37ddo_Caixa_DocumentoTitleControlIdToReplace = Ddo_caixa_documento_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_Caixa_DocumentoTitleControlIdToReplace", AV37ddo_Caixa_DocumentoTitleControlIdToReplace);
         edtavDdo_caixa_documentotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_caixa_documentotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_caixa_documentotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_caixa_emissao_Titlecontrolidtoreplace = subGrid_Internalname+"_Caixa_Emissao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_emissao_Internalname, "TitleControlIdToReplace", Ddo_caixa_emissao_Titlecontrolidtoreplace);
         AV43ddo_Caixa_EmissaoTitleControlIdToReplace = Ddo_caixa_emissao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_Caixa_EmissaoTitleControlIdToReplace", AV43ddo_Caixa_EmissaoTitleControlIdToReplace);
         edtavDdo_caixa_emissaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_caixa_emissaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_caixa_emissaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_caixa_vencimento_Titlecontrolidtoreplace = subGrid_Internalname+"_Caixa_Vencimento";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_vencimento_Internalname, "TitleControlIdToReplace", Ddo_caixa_vencimento_Titlecontrolidtoreplace);
         AV49ddo_Caixa_VencimentoTitleControlIdToReplace = Ddo_caixa_vencimento_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_Caixa_VencimentoTitleControlIdToReplace", AV49ddo_Caixa_VencimentoTitleControlIdToReplace);
         edtavDdo_caixa_vencimentotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_caixa_vencimentotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_caixa_vencimentotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_caixa_tipodecontacod_Titlecontrolidtoreplace = subGrid_Internalname+"_Caixa_TipoDeContaCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_tipodecontacod_Internalname, "TitleControlIdToReplace", Ddo_caixa_tipodecontacod_Titlecontrolidtoreplace);
         AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace = Ddo_caixa_tipodecontacod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace", AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace);
         edtavDdo_caixa_tipodecontacodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_caixa_tipodecontacodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_caixa_tipodecontacodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_caixa_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_Caixa_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_descricao_Internalname, "TitleControlIdToReplace", Ddo_caixa_descricao_Titlecontrolidtoreplace);
         AV57ddo_Caixa_DescricaoTitleControlIdToReplace = Ddo_caixa_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_Caixa_DescricaoTitleControlIdToReplace", AV57ddo_Caixa_DescricaoTitleControlIdToReplace);
         edtavDdo_caixa_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_caixa_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_caixa_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_caixa_valor_Titlecontrolidtoreplace = subGrid_Internalname+"_Caixa_Valor";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_valor_Internalname, "TitleControlIdToReplace", Ddo_caixa_valor_Titlecontrolidtoreplace);
         AV61ddo_Caixa_ValorTitleControlIdToReplace = Ddo_caixa_valor_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_Caixa_ValorTitleControlIdToReplace", AV61ddo_Caixa_ValorTitleControlIdToReplace);
         edtavDdo_caixa_valortitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_caixa_valortitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_caixa_valortitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Fluxo de Caixa";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Documento", 0);
         cmbavOrderedby.addItem("2", "Codigo", 0);
         cmbavOrderedby.addItem("3", "de Emiss�o", 0);
         cmbavOrderedby.addItem("4", "de Vencimento", 0);
         cmbavOrderedby.addItem("5", "Conta", 0);
         cmbavOrderedby.addItem("6", "Descri��o", 0);
         cmbavOrderedby.addItem("7", "Valor", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV62DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV62DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E30FQ2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV30Caixa_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34Caixa_DocumentoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38Caixa_EmissaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44Caixa_VencimentoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50Caixa_TipoDeContaCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54Caixa_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58Caixa_ValorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtCaixa_Codigo_Titleformat = 2;
         edtCaixa_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Codigo", AV33ddo_Caixa_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCaixa_Codigo_Internalname, "Title", edtCaixa_Codigo_Title);
         edtCaixa_Documento_Titleformat = 2;
         edtCaixa_Documento_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Documento", AV37ddo_Caixa_DocumentoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCaixa_Documento_Internalname, "Title", edtCaixa_Documento_Title);
         edtCaixa_Emissao_Titleformat = 2;
         edtCaixa_Emissao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "de Emiss�o", AV43ddo_Caixa_EmissaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCaixa_Emissao_Internalname, "Title", edtCaixa_Emissao_Title);
         edtCaixa_Vencimento_Titleformat = 2;
         edtCaixa_Vencimento_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "de Vencimento", AV49ddo_Caixa_VencimentoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCaixa_Vencimento_Internalname, "Title", edtCaixa_Vencimento_Title);
         edtCaixa_TipoDeContaCod_Titleformat = 2;
         edtCaixa_TipoDeContaCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Conta", AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCaixa_TipoDeContaCod_Internalname, "Title", edtCaixa_TipoDeContaCod_Title);
         edtCaixa_Descricao_Titleformat = 2;
         edtCaixa_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV57ddo_Caixa_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCaixa_Descricao_Internalname, "Title", edtCaixa_Descricao_Title);
         edtCaixa_Valor_Titleformat = 2;
         edtCaixa_Valor_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Valor", AV61ddo_Caixa_ValorTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCaixa_Valor_Internalname, "Title", edtCaixa_Valor_Title);
         AV64GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64GridCurrentPage), 10, 0)));
         AV65GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV30Caixa_CodigoTitleFilterData", AV30Caixa_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34Caixa_DocumentoTitleFilterData", AV34Caixa_DocumentoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV38Caixa_EmissaoTitleFilterData", AV38Caixa_EmissaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV44Caixa_VencimentoTitleFilterData", AV44Caixa_VencimentoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV50Caixa_TipoDeContaCodTitleFilterData", AV50Caixa_TipoDeContaCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV54Caixa_DescricaoTitleFilterData", AV54Caixa_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV58Caixa_ValorTitleFilterData", AV58Caixa_ValorTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11FQ2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV63PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV63PageToGo) ;
         }
      }

      protected void E12FQ2( )
      {
         /* Ddo_caixa_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_caixa_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_caixa_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_codigo_Internalname, "SortedStatus", Ddo_caixa_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_caixa_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_caixa_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_codigo_Internalname, "SortedStatus", Ddo_caixa_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_caixa_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV31TFCaixa_Codigo = (int)(NumberUtil.Val( Ddo_caixa_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFCaixa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFCaixa_Codigo), 6, 0)));
            AV32TFCaixa_Codigo_To = (int)(NumberUtil.Val( Ddo_caixa_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFCaixa_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFCaixa_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13FQ2( )
      {
         /* Ddo_caixa_documento_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_caixa_documento_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_caixa_documento_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_documento_Internalname, "SortedStatus", Ddo_caixa_documento_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_caixa_documento_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_caixa_documento_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_documento_Internalname, "SortedStatus", Ddo_caixa_documento_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_caixa_documento_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV35TFCaixa_Documento = Ddo_caixa_documento_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFCaixa_Documento", AV35TFCaixa_Documento);
            AV36TFCaixa_Documento_Sel = Ddo_caixa_documento_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFCaixa_Documento_Sel", AV36TFCaixa_Documento_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14FQ2( )
      {
         /* Ddo_caixa_emissao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_caixa_emissao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_caixa_emissao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_emissao_Internalname, "SortedStatus", Ddo_caixa_emissao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_caixa_emissao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_caixa_emissao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_emissao_Internalname, "SortedStatus", Ddo_caixa_emissao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_caixa_emissao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV39TFCaixa_Emissao = context.localUtil.CToD( Ddo_caixa_emissao_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFCaixa_Emissao", context.localUtil.Format(AV39TFCaixa_Emissao, "99/99/99"));
            AV40TFCaixa_Emissao_To = context.localUtil.CToD( Ddo_caixa_emissao_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFCaixa_Emissao_To", context.localUtil.Format(AV40TFCaixa_Emissao_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15FQ2( )
      {
         /* Ddo_caixa_vencimento_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_caixa_vencimento_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_caixa_vencimento_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_vencimento_Internalname, "SortedStatus", Ddo_caixa_vencimento_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_caixa_vencimento_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_caixa_vencimento_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_vencimento_Internalname, "SortedStatus", Ddo_caixa_vencimento_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_caixa_vencimento_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV45TFCaixa_Vencimento = context.localUtil.CToD( Ddo_caixa_vencimento_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFCaixa_Vencimento", context.localUtil.Format(AV45TFCaixa_Vencimento, "99/99/99"));
            AV46TFCaixa_Vencimento_To = context.localUtil.CToD( Ddo_caixa_vencimento_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFCaixa_Vencimento_To", context.localUtil.Format(AV46TFCaixa_Vencimento_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16FQ2( )
      {
         /* Ddo_caixa_tipodecontacod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_caixa_tipodecontacod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_caixa_tipodecontacod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_tipodecontacod_Internalname, "SortedStatus", Ddo_caixa_tipodecontacod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_caixa_tipodecontacod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_caixa_tipodecontacod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_tipodecontacod_Internalname, "SortedStatus", Ddo_caixa_tipodecontacod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_caixa_tipodecontacod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV51TFCaixa_TipoDeContaCod = Ddo_caixa_tipodecontacod_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFCaixa_TipoDeContaCod", AV51TFCaixa_TipoDeContaCod);
            AV52TFCaixa_TipoDeContaCod_Sel = Ddo_caixa_tipodecontacod_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFCaixa_TipoDeContaCod_Sel", AV52TFCaixa_TipoDeContaCod_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E17FQ2( )
      {
         /* Ddo_caixa_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_caixa_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_caixa_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_descricao_Internalname, "SortedStatus", Ddo_caixa_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_caixa_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_caixa_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_descricao_Internalname, "SortedStatus", Ddo_caixa_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_caixa_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV55TFCaixa_Descricao = Ddo_caixa_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFCaixa_Descricao", AV55TFCaixa_Descricao);
            AV56TFCaixa_Descricao_Sel = Ddo_caixa_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFCaixa_Descricao_Sel", AV56TFCaixa_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E18FQ2( )
      {
         /* Ddo_caixa_valor_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_caixa_valor_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_caixa_valor_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_valor_Internalname, "SortedStatus", Ddo_caixa_valor_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_caixa_valor_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_caixa_valor_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_valor_Internalname, "SortedStatus", Ddo_caixa_valor_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_caixa_valor_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV59TFCaixa_Valor = NumberUtil.Val( Ddo_caixa_valor_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFCaixa_Valor", StringUtil.LTrim( StringUtil.Str( AV59TFCaixa_Valor, 18, 5)));
            AV60TFCaixa_Valor_To = NumberUtil.Val( Ddo_caixa_valor_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFCaixa_Valor_To", StringUtil.LTrim( StringUtil.Str( AV60TFCaixa_Valor_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E31FQ2( )
      {
         /* Grid_Load Routine */
         AV28Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV28Select);
         AV68Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 80;
         }
         sendrow_802( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_80_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(80, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E32FQ2 */
         E32FQ2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E32FQ2( )
      {
         /* Enter Routine */
         AV7InOutCaixa_Codigo = A874Caixa_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutCaixa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutCaixa_Codigo), 6, 0)));
         AV8InOutCaixa_Documento = A875Caixa_Documento;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutCaixa_Documento", AV8InOutCaixa_Documento);
         context.setWebReturnParms(new Object[] {(int)AV7InOutCaixa_Codigo,(String)AV8InOutCaixa_Documento});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E19FQ2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E24FQ2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E20FQ2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Caixa_Documento1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Caixa_Documento2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Caixa_Documento3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFCaixa_Codigo, AV32TFCaixa_Codigo_To, AV35TFCaixa_Documento, AV36TFCaixa_Documento_Sel, AV39TFCaixa_Emissao, AV40TFCaixa_Emissao_To, AV45TFCaixa_Vencimento, AV46TFCaixa_Vencimento_To, AV51TFCaixa_TipoDeContaCod, AV52TFCaixa_TipoDeContaCod_Sel, AV55TFCaixa_Descricao, AV56TFCaixa_Descricao_Sel, AV59TFCaixa_Valor, AV60TFCaixa_Valor_To, AV33ddo_Caixa_CodigoTitleControlIdToReplace, AV37ddo_Caixa_DocumentoTitleControlIdToReplace, AV43ddo_Caixa_EmissaoTitleControlIdToReplace, AV49ddo_Caixa_VencimentoTitleControlIdToReplace, AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace, AV57ddo_Caixa_DescricaoTitleControlIdToReplace, AV61ddo_Caixa_ValorTitleControlIdToReplace, AV69Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E25FQ2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E26FQ2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E21FQ2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Caixa_Documento1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Caixa_Documento2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Caixa_Documento3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFCaixa_Codigo, AV32TFCaixa_Codigo_To, AV35TFCaixa_Documento, AV36TFCaixa_Documento_Sel, AV39TFCaixa_Emissao, AV40TFCaixa_Emissao_To, AV45TFCaixa_Vencimento, AV46TFCaixa_Vencimento_To, AV51TFCaixa_TipoDeContaCod, AV52TFCaixa_TipoDeContaCod_Sel, AV55TFCaixa_Descricao, AV56TFCaixa_Descricao_Sel, AV59TFCaixa_Valor, AV60TFCaixa_Valor_To, AV33ddo_Caixa_CodigoTitleControlIdToReplace, AV37ddo_Caixa_DocumentoTitleControlIdToReplace, AV43ddo_Caixa_EmissaoTitleControlIdToReplace, AV49ddo_Caixa_VencimentoTitleControlIdToReplace, AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace, AV57ddo_Caixa_DescricaoTitleControlIdToReplace, AV61ddo_Caixa_ValorTitleControlIdToReplace, AV69Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E27FQ2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22FQ2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Caixa_Documento1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Caixa_Documento2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Caixa_Documento3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFCaixa_Codigo, AV32TFCaixa_Codigo_To, AV35TFCaixa_Documento, AV36TFCaixa_Documento_Sel, AV39TFCaixa_Emissao, AV40TFCaixa_Emissao_To, AV45TFCaixa_Vencimento, AV46TFCaixa_Vencimento_To, AV51TFCaixa_TipoDeContaCod, AV52TFCaixa_TipoDeContaCod_Sel, AV55TFCaixa_Descricao, AV56TFCaixa_Descricao_Sel, AV59TFCaixa_Valor, AV60TFCaixa_Valor_To, AV33ddo_Caixa_CodigoTitleControlIdToReplace, AV37ddo_Caixa_DocumentoTitleControlIdToReplace, AV43ddo_Caixa_EmissaoTitleControlIdToReplace, AV49ddo_Caixa_VencimentoTitleControlIdToReplace, AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace, AV57ddo_Caixa_DescricaoTitleControlIdToReplace, AV61ddo_Caixa_ValorTitleControlIdToReplace, AV69Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E28FQ2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E23FQ2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_caixa_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_codigo_Internalname, "SortedStatus", Ddo_caixa_codigo_Sortedstatus);
         Ddo_caixa_documento_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_documento_Internalname, "SortedStatus", Ddo_caixa_documento_Sortedstatus);
         Ddo_caixa_emissao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_emissao_Internalname, "SortedStatus", Ddo_caixa_emissao_Sortedstatus);
         Ddo_caixa_vencimento_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_vencimento_Internalname, "SortedStatus", Ddo_caixa_vencimento_Sortedstatus);
         Ddo_caixa_tipodecontacod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_tipodecontacod_Internalname, "SortedStatus", Ddo_caixa_tipodecontacod_Sortedstatus);
         Ddo_caixa_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_descricao_Internalname, "SortedStatus", Ddo_caixa_descricao_Sortedstatus);
         Ddo_caixa_valor_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_valor_Internalname, "SortedStatus", Ddo_caixa_valor_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_caixa_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_codigo_Internalname, "SortedStatus", Ddo_caixa_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_caixa_documento_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_documento_Internalname, "SortedStatus", Ddo_caixa_documento_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_caixa_emissao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_emissao_Internalname, "SortedStatus", Ddo_caixa_emissao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_caixa_vencimento_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_vencimento_Internalname, "SortedStatus", Ddo_caixa_vencimento_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_caixa_tipodecontacod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_tipodecontacod_Internalname, "SortedStatus", Ddo_caixa_tipodecontacod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_caixa_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_descricao_Internalname, "SortedStatus", Ddo_caixa_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_caixa_valor_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_valor_Internalname, "SortedStatus", Ddo_caixa_valor_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavCaixa_documento1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCaixa_documento1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCaixa_documento1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CAIXA_DOCUMENTO") == 0 )
         {
            edtavCaixa_documento1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCaixa_documento1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCaixa_documento1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavCaixa_documento2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCaixa_documento2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCaixa_documento2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CAIXA_DOCUMENTO") == 0 )
         {
            edtavCaixa_documento2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCaixa_documento2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCaixa_documento2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavCaixa_documento3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCaixa_documento3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCaixa_documento3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CAIXA_DOCUMENTO") == 0 )
         {
            edtavCaixa_documento3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCaixa_documento3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCaixa_documento3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "CAIXA_DOCUMENTO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         AV21Caixa_Documento2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Caixa_Documento2", AV21Caixa_Documento2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "CAIXA_DOCUMENTO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         AV25Caixa_Documento3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Caixa_Documento3", AV25Caixa_Documento3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV31TFCaixa_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFCaixa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFCaixa_Codigo), 6, 0)));
         Ddo_caixa_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_codigo_Internalname, "FilteredText_set", Ddo_caixa_codigo_Filteredtext_set);
         AV32TFCaixa_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFCaixa_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFCaixa_Codigo_To), 6, 0)));
         Ddo_caixa_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_codigo_Internalname, "FilteredTextTo_set", Ddo_caixa_codigo_Filteredtextto_set);
         AV35TFCaixa_Documento = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFCaixa_Documento", AV35TFCaixa_Documento);
         Ddo_caixa_documento_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_documento_Internalname, "FilteredText_set", Ddo_caixa_documento_Filteredtext_set);
         AV36TFCaixa_Documento_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFCaixa_Documento_Sel", AV36TFCaixa_Documento_Sel);
         Ddo_caixa_documento_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_documento_Internalname, "SelectedValue_set", Ddo_caixa_documento_Selectedvalue_set);
         AV39TFCaixa_Emissao = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFCaixa_Emissao", context.localUtil.Format(AV39TFCaixa_Emissao, "99/99/99"));
         Ddo_caixa_emissao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_emissao_Internalname, "FilteredText_set", Ddo_caixa_emissao_Filteredtext_set);
         AV40TFCaixa_Emissao_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFCaixa_Emissao_To", context.localUtil.Format(AV40TFCaixa_Emissao_To, "99/99/99"));
         Ddo_caixa_emissao_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_emissao_Internalname, "FilteredTextTo_set", Ddo_caixa_emissao_Filteredtextto_set);
         AV45TFCaixa_Vencimento = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFCaixa_Vencimento", context.localUtil.Format(AV45TFCaixa_Vencimento, "99/99/99"));
         Ddo_caixa_vencimento_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_vencimento_Internalname, "FilteredText_set", Ddo_caixa_vencimento_Filteredtext_set);
         AV46TFCaixa_Vencimento_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFCaixa_Vencimento_To", context.localUtil.Format(AV46TFCaixa_Vencimento_To, "99/99/99"));
         Ddo_caixa_vencimento_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_vencimento_Internalname, "FilteredTextTo_set", Ddo_caixa_vencimento_Filteredtextto_set);
         AV51TFCaixa_TipoDeContaCod = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFCaixa_TipoDeContaCod", AV51TFCaixa_TipoDeContaCod);
         Ddo_caixa_tipodecontacod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_tipodecontacod_Internalname, "FilteredText_set", Ddo_caixa_tipodecontacod_Filteredtext_set);
         AV52TFCaixa_TipoDeContaCod_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFCaixa_TipoDeContaCod_Sel", AV52TFCaixa_TipoDeContaCod_Sel);
         Ddo_caixa_tipodecontacod_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_tipodecontacod_Internalname, "SelectedValue_set", Ddo_caixa_tipodecontacod_Selectedvalue_set);
         AV55TFCaixa_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFCaixa_Descricao", AV55TFCaixa_Descricao);
         Ddo_caixa_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_descricao_Internalname, "FilteredText_set", Ddo_caixa_descricao_Filteredtext_set);
         AV56TFCaixa_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFCaixa_Descricao_Sel", AV56TFCaixa_Descricao_Sel);
         Ddo_caixa_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_descricao_Internalname, "SelectedValue_set", Ddo_caixa_descricao_Selectedvalue_set);
         AV59TFCaixa_Valor = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFCaixa_Valor", StringUtil.LTrim( StringUtil.Str( AV59TFCaixa_Valor, 18, 5)));
         Ddo_caixa_valor_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_valor_Internalname, "FilteredText_set", Ddo_caixa_valor_Filteredtext_set);
         AV60TFCaixa_Valor_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFCaixa_Valor_To", StringUtil.LTrim( StringUtil.Str( AV60TFCaixa_Valor_To, 18, 5)));
         Ddo_caixa_valor_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_caixa_valor_Internalname, "FilteredTextTo_set", Ddo_caixa_valor_Filteredtextto_set);
         AV15DynamicFiltersSelector1 = "CAIXA_DOCUMENTO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17Caixa_Documento1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Caixa_Documento1", AV17Caixa_Documento1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CAIXA_DOCUMENTO") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Caixa_Documento1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Caixa_Documento1", AV17Caixa_Documento1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CAIXA_DOCUMENTO") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV21Caixa_Documento2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Caixa_Documento2", AV21Caixa_Documento2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CAIXA_DOCUMENTO") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV25Caixa_Documento3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Caixa_Documento3", AV25Caixa_Documento3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV31TFCaixa_Codigo) && (0==AV32TFCaixa_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCAIXA_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV31TFCaixa_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV32TFCaixa_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFCaixa_Documento)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCAIXA_DOCUMENTO";
            AV11GridStateFilterValue.gxTpr_Value = AV35TFCaixa_Documento;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFCaixa_Documento_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCAIXA_DOCUMENTO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV36TFCaixa_Documento_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV39TFCaixa_Emissao) && (DateTime.MinValue==AV40TFCaixa_Emissao_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCAIXA_EMISSAO";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV39TFCaixa_Emissao, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV40TFCaixa_Emissao_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV45TFCaixa_Vencimento) && (DateTime.MinValue==AV46TFCaixa_Vencimento_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCAIXA_VENCIMENTO";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV45TFCaixa_Vencimento, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV46TFCaixa_Vencimento_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51TFCaixa_TipoDeContaCod)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCAIXA_TIPODECONTACOD";
            AV11GridStateFilterValue.gxTpr_Value = AV51TFCaixa_TipoDeContaCod;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFCaixa_TipoDeContaCod_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCAIXA_TIPODECONTACOD_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV52TFCaixa_TipoDeContaCod_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55TFCaixa_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCAIXA_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV55TFCaixa_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFCaixa_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCAIXA_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV56TFCaixa_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV59TFCaixa_Valor) && (Convert.ToDecimal(0)==AV60TFCaixa_Valor_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCAIXA_VALOR";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV59TFCaixa_Valor, 18, 5);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV60TFCaixa_Valor_To, 18, 5);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV69Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CAIXA_DOCUMENTO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Caixa_Documento1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17Caixa_Documento1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CAIXA_DOCUMENTO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21Caixa_Documento2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21Caixa_Documento2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CAIXA_DOCUMENTO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Caixa_Documento3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25Caixa_Documento3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_FQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_FQ2( true) ;
         }
         else
         {
            wb_table2_5_FQ2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_FQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_74_FQ2( true) ;
         }
         else
         {
            wb_table3_74_FQ2( false) ;
         }
         return  ;
      }

      protected void wb_table3_74_FQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_FQ2e( true) ;
         }
         else
         {
            wb_table1_2_FQ2e( false) ;
         }
      }

      protected void wb_table3_74_FQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_77_FQ2( true) ;
         }
         else
         {
            wb_table4_77_FQ2( false) ;
         }
         return  ;
      }

      protected void wb_table4_77_FQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_74_FQ2e( true) ;
         }
         else
         {
            wb_table3_74_FQ2e( false) ;
         }
      }

      protected void wb_table4_77_FQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"80\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtCaixa_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtCaixa_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtCaixa_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtCaixa_Documento_Titleformat == 0 )
               {
                  context.SendWebValue( edtCaixa_Documento_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtCaixa_Documento_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtCaixa_Emissao_Titleformat == 0 )
               {
                  context.SendWebValue( edtCaixa_Emissao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtCaixa_Emissao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtCaixa_Vencimento_Titleformat == 0 )
               {
                  context.SendWebValue( edtCaixa_Vencimento_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtCaixa_Vencimento_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtCaixa_TipoDeContaCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtCaixa_TipoDeContaCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtCaixa_TipoDeContaCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtCaixa_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtCaixa_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtCaixa_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtCaixa_Valor_Titleformat == 0 )
               {
                  context.SendWebValue( edtCaixa_Valor_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtCaixa_Valor_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A874Caixa_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtCaixa_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCaixa_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A875Caixa_Documento);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtCaixa_Documento_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCaixa_Documento_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A876Caixa_Emissao, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtCaixa_Emissao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCaixa_Emissao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A877Caixa_Vencimento, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtCaixa_Vencimento_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCaixa_Vencimento_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A881Caixa_TipoDeContaCod));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtCaixa_TipoDeContaCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCaixa_TipoDeContaCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A879Caixa_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtCaixa_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCaixa_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A880Caixa_Valor, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtCaixa_Valor_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCaixa_Valor_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 80 )
         {
            wbEnd = 0;
            nRC_GXsfl_80 = (short)(nGXsfl_80_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_77_FQ2e( true) ;
         }
         else
         {
            wb_table4_77_FQ2e( false) ;
         }
      }

      protected void wb_table2_5_FQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptCaixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptCaixa.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptCaixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_FQ2( true) ;
         }
         else
         {
            wb_table5_14_FQ2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_FQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_FQ2e( true) ;
         }
         else
         {
            wb_table2_5_FQ2e( false) ;
         }
      }

      protected void wb_table5_14_FQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptCaixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_FQ2( true) ;
         }
         else
         {
            wb_table6_19_FQ2( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_FQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptCaixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_FQ2e( true) ;
         }
         else
         {
            wb_table5_14_FQ2e( false) ;
         }
      }

      protected void wb_table6_19_FQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptCaixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptCaixa.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptCaixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_FQ2( true) ;
         }
         else
         {
            wb_table7_28_FQ2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_FQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptCaixa.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptCaixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptCaixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "", true, "HLP_PromptCaixa.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptCaixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_45_FQ2( true) ;
         }
         else
         {
            wb_table8_45_FQ2( false) ;
         }
         return  ;
      }

      protected void wb_table8_45_FQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptCaixa.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptCaixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptCaixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "", true, "HLP_PromptCaixa.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptCaixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_62_FQ2( true) ;
         }
         else
         {
            wb_table9_62_FQ2( false) ;
         }
         return  ;
      }

      protected void wb_table9_62_FQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptCaixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_FQ2e( true) ;
         }
         else
         {
            wb_table6_19_FQ2e( false) ;
         }
      }

      protected void wb_table9_62_FQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", "", true, "HLP_PromptCaixa.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCaixa_documento3_Internalname, AV25Caixa_Documento3, StringUtil.RTrim( context.localUtil.Format( AV25Caixa_Documento3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,67);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCaixa_documento3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavCaixa_documento3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptCaixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_62_FQ2e( true) ;
         }
         else
         {
            wb_table9_62_FQ2e( false) ;
         }
      }

      protected void wb_table8_45_FQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "", true, "HLP_PromptCaixa.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCaixa_documento2_Internalname, AV21Caixa_Documento2, StringUtil.RTrim( context.localUtil.Format( AV21Caixa_Documento2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCaixa_documento2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavCaixa_documento2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptCaixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_45_FQ2e( true) ;
         }
         else
         {
            wb_table8_45_FQ2e( false) ;
         }
      }

      protected void wb_table7_28_FQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_PromptCaixa.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCaixa_documento1_Internalname, AV17Caixa_Documento1, StringUtil.RTrim( context.localUtil.Format( AV17Caixa_Documento1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCaixa_documento1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavCaixa_documento1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptCaixa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_FQ2e( true) ;
         }
         else
         {
            wb_table7_28_FQ2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutCaixa_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutCaixa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutCaixa_Codigo), 6, 0)));
         AV8InOutCaixa_Documento = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutCaixa_Documento", AV8InOutCaixa_Documento);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAFQ2( ) ;
         WSFQ2( ) ;
         WEFQ2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042823404596");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptcaixa.js", "?202042823404596");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_802( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_80_idx;
         edtCaixa_Codigo_Internalname = "CAIXA_CODIGO_"+sGXsfl_80_idx;
         edtCaixa_Documento_Internalname = "CAIXA_DOCUMENTO_"+sGXsfl_80_idx;
         edtCaixa_Emissao_Internalname = "CAIXA_EMISSAO_"+sGXsfl_80_idx;
         edtCaixa_Vencimento_Internalname = "CAIXA_VENCIMENTO_"+sGXsfl_80_idx;
         edtCaixa_TipoDeContaCod_Internalname = "CAIXA_TIPODECONTACOD_"+sGXsfl_80_idx;
         edtCaixa_Descricao_Internalname = "CAIXA_DESCRICAO_"+sGXsfl_80_idx;
         edtCaixa_Valor_Internalname = "CAIXA_VALOR_"+sGXsfl_80_idx;
      }

      protected void SubsflControlProps_fel_802( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_80_fel_idx;
         edtCaixa_Codigo_Internalname = "CAIXA_CODIGO_"+sGXsfl_80_fel_idx;
         edtCaixa_Documento_Internalname = "CAIXA_DOCUMENTO_"+sGXsfl_80_fel_idx;
         edtCaixa_Emissao_Internalname = "CAIXA_EMISSAO_"+sGXsfl_80_fel_idx;
         edtCaixa_Vencimento_Internalname = "CAIXA_VENCIMENTO_"+sGXsfl_80_fel_idx;
         edtCaixa_TipoDeContaCod_Internalname = "CAIXA_TIPODECONTACOD_"+sGXsfl_80_fel_idx;
         edtCaixa_Descricao_Internalname = "CAIXA_DESCRICAO_"+sGXsfl_80_fel_idx;
         edtCaixa_Valor_Internalname = "CAIXA_VALOR_"+sGXsfl_80_fel_idx;
      }

      protected void sendrow_802( )
      {
         SubsflControlProps_802( ) ;
         WBFQ0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_80_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_80_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_80_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 81,'',false,'',80)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV28Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV68Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV68Select_GXI : context.PathToRelativeUrl( AV28Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_80_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV28Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCaixa_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A874Caixa_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A874Caixa_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtCaixa_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCaixa_Documento_Internalname,(String)A875Caixa_Documento,StringUtil.RTrim( context.localUtil.Format( A875Caixa_Documento, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtCaixa_Documento_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCaixa_Emissao_Internalname,context.localUtil.Format(A876Caixa_Emissao, "99/99/99"),context.localUtil.Format( A876Caixa_Emissao, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtCaixa_Emissao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCaixa_Vencimento_Internalname,context.localUtil.Format(A877Caixa_Vencimento, "99/99/99"),context.localUtil.Format( A877Caixa_Vencimento, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtCaixa_Vencimento_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCaixa_TipoDeContaCod_Internalname,StringUtil.RTrim( A881Caixa_TipoDeContaCod),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtCaixa_TipoDeContaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)80,(short)1,(short)0,(short)0,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCaixa_Descricao_Internalname,(String)A879Caixa_Descricao,StringUtil.RTrim( context.localUtil.Format( A879Caixa_Descricao, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtCaixa_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCaixa_Valor_Internalname,StringUtil.LTrim( StringUtil.NToC( A880Caixa_Valor, 18, 5, ",", "")),context.localUtil.Format( A880Caixa_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtCaixa_Valor_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_CAIXA_CODIGO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( (decimal)(A874Caixa_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CAIXA_DOCUMENTO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, StringUtil.RTrim( context.localUtil.Format( A875Caixa_Documento, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_CAIXA_EMISSAO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, A876Caixa_Emissao));
            GxWebStd.gx_hidden_field( context, "gxhash_CAIXA_VENCIMENTO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, A877Caixa_Vencimento));
            GxWebStd.gx_hidden_field( context, "gxhash_CAIXA_TIPODECONTACOD"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, StringUtil.RTrim( context.localUtil.Format( A881Caixa_TipoDeContaCod, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CAIXA_DESCRICAO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, StringUtil.RTrim( context.localUtil.Format( A879Caixa_Descricao, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_CAIXA_VALOR"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( A880Caixa_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GridContainer.AddRow(GridRow);
            nGXsfl_80_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_80_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_80_idx+1));
            sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
            SubsflControlProps_802( ) ;
         }
         /* End function sendrow_802 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavCaixa_documento1_Internalname = "vCAIXA_DOCUMENTO1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavCaixa_documento2_Internalname = "vCAIXA_DOCUMENTO2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavCaixa_documento3_Internalname = "vCAIXA_DOCUMENTO3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtCaixa_Codigo_Internalname = "CAIXA_CODIGO";
         edtCaixa_Documento_Internalname = "CAIXA_DOCUMENTO";
         edtCaixa_Emissao_Internalname = "CAIXA_EMISSAO";
         edtCaixa_Vencimento_Internalname = "CAIXA_VENCIMENTO";
         edtCaixa_TipoDeContaCod_Internalname = "CAIXA_TIPODECONTACOD";
         edtCaixa_Descricao_Internalname = "CAIXA_DESCRICAO";
         edtCaixa_Valor_Internalname = "CAIXA_VALOR";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfcaixa_codigo_Internalname = "vTFCAIXA_CODIGO";
         edtavTfcaixa_codigo_to_Internalname = "vTFCAIXA_CODIGO_TO";
         edtavTfcaixa_documento_Internalname = "vTFCAIXA_DOCUMENTO";
         edtavTfcaixa_documento_sel_Internalname = "vTFCAIXA_DOCUMENTO_SEL";
         edtavTfcaixa_emissao_Internalname = "vTFCAIXA_EMISSAO";
         edtavTfcaixa_emissao_to_Internalname = "vTFCAIXA_EMISSAO_TO";
         edtavDdo_caixa_emissaoauxdate_Internalname = "vDDO_CAIXA_EMISSAOAUXDATE";
         edtavDdo_caixa_emissaoauxdateto_Internalname = "vDDO_CAIXA_EMISSAOAUXDATETO";
         divDdo_caixa_emissaoauxdates_Internalname = "DDO_CAIXA_EMISSAOAUXDATES";
         edtavTfcaixa_vencimento_Internalname = "vTFCAIXA_VENCIMENTO";
         edtavTfcaixa_vencimento_to_Internalname = "vTFCAIXA_VENCIMENTO_TO";
         edtavDdo_caixa_vencimentoauxdate_Internalname = "vDDO_CAIXA_VENCIMENTOAUXDATE";
         edtavDdo_caixa_vencimentoauxdateto_Internalname = "vDDO_CAIXA_VENCIMENTOAUXDATETO";
         divDdo_caixa_vencimentoauxdates_Internalname = "DDO_CAIXA_VENCIMENTOAUXDATES";
         edtavTfcaixa_tipodecontacod_Internalname = "vTFCAIXA_TIPODECONTACOD";
         edtavTfcaixa_tipodecontacod_sel_Internalname = "vTFCAIXA_TIPODECONTACOD_SEL";
         edtavTfcaixa_descricao_Internalname = "vTFCAIXA_DESCRICAO";
         edtavTfcaixa_descricao_sel_Internalname = "vTFCAIXA_DESCRICAO_SEL";
         edtavTfcaixa_valor_Internalname = "vTFCAIXA_VALOR";
         edtavTfcaixa_valor_to_Internalname = "vTFCAIXA_VALOR_TO";
         Ddo_caixa_codigo_Internalname = "DDO_CAIXA_CODIGO";
         edtavDdo_caixa_codigotitlecontrolidtoreplace_Internalname = "vDDO_CAIXA_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_caixa_documento_Internalname = "DDO_CAIXA_DOCUMENTO";
         edtavDdo_caixa_documentotitlecontrolidtoreplace_Internalname = "vDDO_CAIXA_DOCUMENTOTITLECONTROLIDTOREPLACE";
         Ddo_caixa_emissao_Internalname = "DDO_CAIXA_EMISSAO";
         edtavDdo_caixa_emissaotitlecontrolidtoreplace_Internalname = "vDDO_CAIXA_EMISSAOTITLECONTROLIDTOREPLACE";
         Ddo_caixa_vencimento_Internalname = "DDO_CAIXA_VENCIMENTO";
         edtavDdo_caixa_vencimentotitlecontrolidtoreplace_Internalname = "vDDO_CAIXA_VENCIMENTOTITLECONTROLIDTOREPLACE";
         Ddo_caixa_tipodecontacod_Internalname = "DDO_CAIXA_TIPODECONTACOD";
         edtavDdo_caixa_tipodecontacodtitlecontrolidtoreplace_Internalname = "vDDO_CAIXA_TIPODECONTACODTITLECONTROLIDTOREPLACE";
         Ddo_caixa_descricao_Internalname = "DDO_CAIXA_DESCRICAO";
         edtavDdo_caixa_descricaotitlecontrolidtoreplace_Internalname = "vDDO_CAIXA_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_caixa_valor_Internalname = "DDO_CAIXA_VALOR";
         edtavDdo_caixa_valortitlecontrolidtoreplace_Internalname = "vDDO_CAIXA_VALORTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtCaixa_Valor_Jsonclick = "";
         edtCaixa_Descricao_Jsonclick = "";
         edtCaixa_TipoDeContaCod_Jsonclick = "";
         edtCaixa_Vencimento_Jsonclick = "";
         edtCaixa_Emissao_Jsonclick = "";
         edtCaixa_Documento_Jsonclick = "";
         edtCaixa_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavCaixa_documento1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavCaixa_documento2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavCaixa_documento3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtCaixa_Valor_Titleformat = 0;
         edtCaixa_Descricao_Titleformat = 0;
         edtCaixa_TipoDeContaCod_Titleformat = 0;
         edtCaixa_Vencimento_Titleformat = 0;
         edtCaixa_Emissao_Titleformat = 0;
         edtCaixa_Documento_Titleformat = 0;
         edtCaixa_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavCaixa_documento3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavCaixa_documento2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavCaixa_documento1_Visible = 1;
         edtCaixa_Valor_Title = "Valor";
         edtCaixa_Descricao_Title = "Descri��o";
         edtCaixa_TipoDeContaCod_Title = "Conta";
         edtCaixa_Vencimento_Title = "de Vencimento";
         edtCaixa_Emissao_Title = "de Emiss�o";
         edtCaixa_Documento_Title = "Documento";
         edtCaixa_Codigo_Title = "Codigo";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_caixa_valortitlecontrolidtoreplace_Visible = 1;
         edtavDdo_caixa_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_caixa_tipodecontacodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_caixa_vencimentotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_caixa_emissaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_caixa_documentotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_caixa_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfcaixa_valor_to_Jsonclick = "";
         edtavTfcaixa_valor_to_Visible = 1;
         edtavTfcaixa_valor_Jsonclick = "";
         edtavTfcaixa_valor_Visible = 1;
         edtavTfcaixa_descricao_sel_Jsonclick = "";
         edtavTfcaixa_descricao_sel_Visible = 1;
         edtavTfcaixa_descricao_Jsonclick = "";
         edtavTfcaixa_descricao_Visible = 1;
         edtavTfcaixa_tipodecontacod_sel_Jsonclick = "";
         edtavTfcaixa_tipodecontacod_sel_Visible = 1;
         edtavTfcaixa_tipodecontacod_Jsonclick = "";
         edtavTfcaixa_tipodecontacod_Visible = 1;
         edtavDdo_caixa_vencimentoauxdateto_Jsonclick = "";
         edtavDdo_caixa_vencimentoauxdate_Jsonclick = "";
         edtavTfcaixa_vencimento_to_Jsonclick = "";
         edtavTfcaixa_vencimento_to_Visible = 1;
         edtavTfcaixa_vencimento_Jsonclick = "";
         edtavTfcaixa_vencimento_Visible = 1;
         edtavDdo_caixa_emissaoauxdateto_Jsonclick = "";
         edtavDdo_caixa_emissaoauxdate_Jsonclick = "";
         edtavTfcaixa_emissao_to_Jsonclick = "";
         edtavTfcaixa_emissao_to_Visible = 1;
         edtavTfcaixa_emissao_Jsonclick = "";
         edtavTfcaixa_emissao_Visible = 1;
         edtavTfcaixa_documento_sel_Jsonclick = "";
         edtavTfcaixa_documento_sel_Visible = 1;
         edtavTfcaixa_documento_Jsonclick = "";
         edtavTfcaixa_documento_Visible = 1;
         edtavTfcaixa_codigo_to_Jsonclick = "";
         edtavTfcaixa_codigo_to_Visible = 1;
         edtavTfcaixa_codigo_Jsonclick = "";
         edtavTfcaixa_codigo_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_caixa_valor_Searchbuttontext = "Pesquisar";
         Ddo_caixa_valor_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_caixa_valor_Rangefilterto = "At�";
         Ddo_caixa_valor_Rangefilterfrom = "Desde";
         Ddo_caixa_valor_Cleanfilter = "Limpar pesquisa";
         Ddo_caixa_valor_Loadingdata = "Carregando dados...";
         Ddo_caixa_valor_Sortdsc = "Ordenar de Z � A";
         Ddo_caixa_valor_Sortasc = "Ordenar de A � Z";
         Ddo_caixa_valor_Datalistupdateminimumcharacters = 0;
         Ddo_caixa_valor_Datalistfixedvalues = "";
         Ddo_caixa_valor_Includedatalist = Convert.ToBoolean( 0);
         Ddo_caixa_valor_Filterisrange = Convert.ToBoolean( -1);
         Ddo_caixa_valor_Filtertype = "Numeric";
         Ddo_caixa_valor_Includefilter = Convert.ToBoolean( -1);
         Ddo_caixa_valor_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_caixa_valor_Includesortasc = Convert.ToBoolean( -1);
         Ddo_caixa_valor_Titlecontrolidtoreplace = "";
         Ddo_caixa_valor_Dropdownoptionstype = "GridTitleSettings";
         Ddo_caixa_valor_Cls = "ColumnSettings";
         Ddo_caixa_valor_Tooltip = "Op��es";
         Ddo_caixa_valor_Caption = "";
         Ddo_caixa_descricao_Searchbuttontext = "Pesquisar";
         Ddo_caixa_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_caixa_descricao_Rangefilterto = "At�";
         Ddo_caixa_descricao_Rangefilterfrom = "Desde";
         Ddo_caixa_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_caixa_descricao_Loadingdata = "Carregando dados...";
         Ddo_caixa_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_caixa_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_caixa_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_caixa_descricao_Datalistproc = "GetPromptCaixaFilterData";
         Ddo_caixa_descricao_Datalistfixedvalues = "";
         Ddo_caixa_descricao_Datalisttype = "Dynamic";
         Ddo_caixa_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_caixa_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_caixa_descricao_Filtertype = "Character";
         Ddo_caixa_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_caixa_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_caixa_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_caixa_descricao_Titlecontrolidtoreplace = "";
         Ddo_caixa_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_caixa_descricao_Cls = "ColumnSettings";
         Ddo_caixa_descricao_Tooltip = "Op��es";
         Ddo_caixa_descricao_Caption = "";
         Ddo_caixa_tipodecontacod_Searchbuttontext = "Pesquisar";
         Ddo_caixa_tipodecontacod_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_caixa_tipodecontacod_Rangefilterto = "At�";
         Ddo_caixa_tipodecontacod_Rangefilterfrom = "Desde";
         Ddo_caixa_tipodecontacod_Cleanfilter = "Limpar pesquisa";
         Ddo_caixa_tipodecontacod_Loadingdata = "Carregando dados...";
         Ddo_caixa_tipodecontacod_Sortdsc = "Ordenar de Z � A";
         Ddo_caixa_tipodecontacod_Sortasc = "Ordenar de A � Z";
         Ddo_caixa_tipodecontacod_Datalistupdateminimumcharacters = 0;
         Ddo_caixa_tipodecontacod_Datalistproc = "GetPromptCaixaFilterData";
         Ddo_caixa_tipodecontacod_Datalistfixedvalues = "";
         Ddo_caixa_tipodecontacod_Datalisttype = "Dynamic";
         Ddo_caixa_tipodecontacod_Includedatalist = Convert.ToBoolean( -1);
         Ddo_caixa_tipodecontacod_Filterisrange = Convert.ToBoolean( 0);
         Ddo_caixa_tipodecontacod_Filtertype = "Character";
         Ddo_caixa_tipodecontacod_Includefilter = Convert.ToBoolean( -1);
         Ddo_caixa_tipodecontacod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_caixa_tipodecontacod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_caixa_tipodecontacod_Titlecontrolidtoreplace = "";
         Ddo_caixa_tipodecontacod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_caixa_tipodecontacod_Cls = "ColumnSettings";
         Ddo_caixa_tipodecontacod_Tooltip = "Op��es";
         Ddo_caixa_tipodecontacod_Caption = "";
         Ddo_caixa_vencimento_Searchbuttontext = "Pesquisar";
         Ddo_caixa_vencimento_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_caixa_vencimento_Rangefilterto = "At�";
         Ddo_caixa_vencimento_Rangefilterfrom = "Desde";
         Ddo_caixa_vencimento_Cleanfilter = "Limpar pesquisa";
         Ddo_caixa_vencimento_Loadingdata = "Carregando dados...";
         Ddo_caixa_vencimento_Sortdsc = "Ordenar de Z � A";
         Ddo_caixa_vencimento_Sortasc = "Ordenar de A � Z";
         Ddo_caixa_vencimento_Datalistupdateminimumcharacters = 0;
         Ddo_caixa_vencimento_Datalistfixedvalues = "";
         Ddo_caixa_vencimento_Includedatalist = Convert.ToBoolean( 0);
         Ddo_caixa_vencimento_Filterisrange = Convert.ToBoolean( -1);
         Ddo_caixa_vencimento_Filtertype = "Date";
         Ddo_caixa_vencimento_Includefilter = Convert.ToBoolean( -1);
         Ddo_caixa_vencimento_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_caixa_vencimento_Includesortasc = Convert.ToBoolean( -1);
         Ddo_caixa_vencimento_Titlecontrolidtoreplace = "";
         Ddo_caixa_vencimento_Dropdownoptionstype = "GridTitleSettings";
         Ddo_caixa_vencimento_Cls = "ColumnSettings";
         Ddo_caixa_vencimento_Tooltip = "Op��es";
         Ddo_caixa_vencimento_Caption = "";
         Ddo_caixa_emissao_Searchbuttontext = "Pesquisar";
         Ddo_caixa_emissao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_caixa_emissao_Rangefilterto = "At�";
         Ddo_caixa_emissao_Rangefilterfrom = "Desde";
         Ddo_caixa_emissao_Cleanfilter = "Limpar pesquisa";
         Ddo_caixa_emissao_Loadingdata = "Carregando dados...";
         Ddo_caixa_emissao_Sortdsc = "Ordenar de Z � A";
         Ddo_caixa_emissao_Sortasc = "Ordenar de A � Z";
         Ddo_caixa_emissao_Datalistupdateminimumcharacters = 0;
         Ddo_caixa_emissao_Datalistfixedvalues = "";
         Ddo_caixa_emissao_Includedatalist = Convert.ToBoolean( 0);
         Ddo_caixa_emissao_Filterisrange = Convert.ToBoolean( -1);
         Ddo_caixa_emissao_Filtertype = "Date";
         Ddo_caixa_emissao_Includefilter = Convert.ToBoolean( -1);
         Ddo_caixa_emissao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_caixa_emissao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_caixa_emissao_Titlecontrolidtoreplace = "";
         Ddo_caixa_emissao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_caixa_emissao_Cls = "ColumnSettings";
         Ddo_caixa_emissao_Tooltip = "Op��es";
         Ddo_caixa_emissao_Caption = "";
         Ddo_caixa_documento_Searchbuttontext = "Pesquisar";
         Ddo_caixa_documento_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_caixa_documento_Rangefilterto = "At�";
         Ddo_caixa_documento_Rangefilterfrom = "Desde";
         Ddo_caixa_documento_Cleanfilter = "Limpar pesquisa";
         Ddo_caixa_documento_Loadingdata = "Carregando dados...";
         Ddo_caixa_documento_Sortdsc = "Ordenar de Z � A";
         Ddo_caixa_documento_Sortasc = "Ordenar de A � Z";
         Ddo_caixa_documento_Datalistupdateminimumcharacters = 0;
         Ddo_caixa_documento_Datalistproc = "GetPromptCaixaFilterData";
         Ddo_caixa_documento_Datalistfixedvalues = "";
         Ddo_caixa_documento_Datalisttype = "Dynamic";
         Ddo_caixa_documento_Includedatalist = Convert.ToBoolean( -1);
         Ddo_caixa_documento_Filterisrange = Convert.ToBoolean( 0);
         Ddo_caixa_documento_Filtertype = "Character";
         Ddo_caixa_documento_Includefilter = Convert.ToBoolean( -1);
         Ddo_caixa_documento_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_caixa_documento_Includesortasc = Convert.ToBoolean( -1);
         Ddo_caixa_documento_Titlecontrolidtoreplace = "";
         Ddo_caixa_documento_Dropdownoptionstype = "GridTitleSettings";
         Ddo_caixa_documento_Cls = "ColumnSettings";
         Ddo_caixa_documento_Tooltip = "Op��es";
         Ddo_caixa_documento_Caption = "";
         Ddo_caixa_codigo_Searchbuttontext = "Pesquisar";
         Ddo_caixa_codigo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_caixa_codigo_Rangefilterto = "At�";
         Ddo_caixa_codigo_Rangefilterfrom = "Desde";
         Ddo_caixa_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_caixa_codigo_Loadingdata = "Carregando dados...";
         Ddo_caixa_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_caixa_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_caixa_codigo_Datalistupdateminimumcharacters = 0;
         Ddo_caixa_codigo_Datalistfixedvalues = "";
         Ddo_caixa_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_caixa_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_caixa_codigo_Filtertype = "Numeric";
         Ddo_caixa_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_caixa_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_caixa_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_caixa_codigo_Titlecontrolidtoreplace = "";
         Ddo_caixa_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_caixa_codigo_Cls = "ColumnSettings";
         Ddo_caixa_codigo_Tooltip = "Op��es";
         Ddo_caixa_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Fluxo de Caixa";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV33ddo_Caixa_CodigoTitleControlIdToReplace',fld:'vDDO_CAIXA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Caixa_DocumentoTitleControlIdToReplace',fld:'vDDO_CAIXA_DOCUMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Caixa_EmissaoTitleControlIdToReplace',fld:'vDDO_CAIXA_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Caixa_VencimentoTitleControlIdToReplace',fld:'vDDO_CAIXA_VENCIMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace',fld:'vDDO_CAIXA_TIPODECONTACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Caixa_DescricaoTitleControlIdToReplace',fld:'vDDO_CAIXA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Caixa_ValorTitleControlIdToReplace',fld:'vDDO_CAIXA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV31TFCaixa_Codigo',fld:'vTFCAIXA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFCaixa_Codigo_To',fld:'vTFCAIXA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFCaixa_Documento',fld:'vTFCAIXA_DOCUMENTO',pic:'@!',nv:''},{av:'AV36TFCaixa_Documento_Sel',fld:'vTFCAIXA_DOCUMENTO_SEL',pic:'@!',nv:''},{av:'AV39TFCaixa_Emissao',fld:'vTFCAIXA_EMISSAO',pic:'',nv:''},{av:'AV40TFCaixa_Emissao_To',fld:'vTFCAIXA_EMISSAO_TO',pic:'',nv:''},{av:'AV45TFCaixa_Vencimento',fld:'vTFCAIXA_VENCIMENTO',pic:'',nv:''},{av:'AV46TFCaixa_Vencimento_To',fld:'vTFCAIXA_VENCIMENTO_TO',pic:'',nv:''},{av:'AV51TFCaixa_TipoDeContaCod',fld:'vTFCAIXA_TIPODECONTACOD',pic:'',nv:''},{av:'AV52TFCaixa_TipoDeContaCod_Sel',fld:'vTFCAIXA_TIPODECONTACOD_SEL',pic:'',nv:''},{av:'AV55TFCaixa_Descricao',fld:'vTFCAIXA_DESCRICAO',pic:'@!',nv:''},{av:'AV56TFCaixa_Descricao_Sel',fld:'vTFCAIXA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV59TFCaixa_Valor',fld:'vTFCAIXA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV60TFCaixa_Valor_To',fld:'vTFCAIXA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Caixa_Documento1',fld:'vCAIXA_DOCUMENTO1',pic:'@!',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Caixa_Documento2',fld:'vCAIXA_DOCUMENTO2',pic:'@!',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25Caixa_Documento3',fld:'vCAIXA_DOCUMENTO3',pic:'@!',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0}],oparms:[{av:'AV30Caixa_CodigoTitleFilterData',fld:'vCAIXA_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV34Caixa_DocumentoTitleFilterData',fld:'vCAIXA_DOCUMENTOTITLEFILTERDATA',pic:'',nv:null},{av:'AV38Caixa_EmissaoTitleFilterData',fld:'vCAIXA_EMISSAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV44Caixa_VencimentoTitleFilterData',fld:'vCAIXA_VENCIMENTOTITLEFILTERDATA',pic:'',nv:null},{av:'AV50Caixa_TipoDeContaCodTitleFilterData',fld:'vCAIXA_TIPODECONTACODTITLEFILTERDATA',pic:'',nv:null},{av:'AV54Caixa_DescricaoTitleFilterData',fld:'vCAIXA_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV58Caixa_ValorTitleFilterData',fld:'vCAIXA_VALORTITLEFILTERDATA',pic:'',nv:null},{av:'edtCaixa_Codigo_Titleformat',ctrl:'CAIXA_CODIGO',prop:'Titleformat'},{av:'edtCaixa_Codigo_Title',ctrl:'CAIXA_CODIGO',prop:'Title'},{av:'edtCaixa_Documento_Titleformat',ctrl:'CAIXA_DOCUMENTO',prop:'Titleformat'},{av:'edtCaixa_Documento_Title',ctrl:'CAIXA_DOCUMENTO',prop:'Title'},{av:'edtCaixa_Emissao_Titleformat',ctrl:'CAIXA_EMISSAO',prop:'Titleformat'},{av:'edtCaixa_Emissao_Title',ctrl:'CAIXA_EMISSAO',prop:'Title'},{av:'edtCaixa_Vencimento_Titleformat',ctrl:'CAIXA_VENCIMENTO',prop:'Titleformat'},{av:'edtCaixa_Vencimento_Title',ctrl:'CAIXA_VENCIMENTO',prop:'Title'},{av:'edtCaixa_TipoDeContaCod_Titleformat',ctrl:'CAIXA_TIPODECONTACOD',prop:'Titleformat'},{av:'edtCaixa_TipoDeContaCod_Title',ctrl:'CAIXA_TIPODECONTACOD',prop:'Title'},{av:'edtCaixa_Descricao_Titleformat',ctrl:'CAIXA_DESCRICAO',prop:'Titleformat'},{av:'edtCaixa_Descricao_Title',ctrl:'CAIXA_DESCRICAO',prop:'Title'},{av:'edtCaixa_Valor_Titleformat',ctrl:'CAIXA_VALOR',prop:'Titleformat'},{av:'edtCaixa_Valor_Title',ctrl:'CAIXA_VALOR',prop:'Title'},{av:'AV64GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV65GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11FQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Caixa_Documento1',fld:'vCAIXA_DOCUMENTO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Caixa_Documento2',fld:'vCAIXA_DOCUMENTO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Caixa_Documento3',fld:'vCAIXA_DOCUMENTO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFCaixa_Codigo',fld:'vTFCAIXA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFCaixa_Codigo_To',fld:'vTFCAIXA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFCaixa_Documento',fld:'vTFCAIXA_DOCUMENTO',pic:'@!',nv:''},{av:'AV36TFCaixa_Documento_Sel',fld:'vTFCAIXA_DOCUMENTO_SEL',pic:'@!',nv:''},{av:'AV39TFCaixa_Emissao',fld:'vTFCAIXA_EMISSAO',pic:'',nv:''},{av:'AV40TFCaixa_Emissao_To',fld:'vTFCAIXA_EMISSAO_TO',pic:'',nv:''},{av:'AV45TFCaixa_Vencimento',fld:'vTFCAIXA_VENCIMENTO',pic:'',nv:''},{av:'AV46TFCaixa_Vencimento_To',fld:'vTFCAIXA_VENCIMENTO_TO',pic:'',nv:''},{av:'AV51TFCaixa_TipoDeContaCod',fld:'vTFCAIXA_TIPODECONTACOD',pic:'',nv:''},{av:'AV52TFCaixa_TipoDeContaCod_Sel',fld:'vTFCAIXA_TIPODECONTACOD_SEL',pic:'',nv:''},{av:'AV55TFCaixa_Descricao',fld:'vTFCAIXA_DESCRICAO',pic:'@!',nv:''},{av:'AV56TFCaixa_Descricao_Sel',fld:'vTFCAIXA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV59TFCaixa_Valor',fld:'vTFCAIXA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV60TFCaixa_Valor_To',fld:'vTFCAIXA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_Caixa_CodigoTitleControlIdToReplace',fld:'vDDO_CAIXA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Caixa_DocumentoTitleControlIdToReplace',fld:'vDDO_CAIXA_DOCUMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Caixa_EmissaoTitleControlIdToReplace',fld:'vDDO_CAIXA_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Caixa_VencimentoTitleControlIdToReplace',fld:'vDDO_CAIXA_VENCIMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace',fld:'vDDO_CAIXA_TIPODECONTACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Caixa_DescricaoTitleControlIdToReplace',fld:'vDDO_CAIXA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Caixa_ValorTitleControlIdToReplace',fld:'vDDO_CAIXA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CAIXA_CODIGO.ONOPTIONCLICKED","{handler:'E12FQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Caixa_Documento1',fld:'vCAIXA_DOCUMENTO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Caixa_Documento2',fld:'vCAIXA_DOCUMENTO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Caixa_Documento3',fld:'vCAIXA_DOCUMENTO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFCaixa_Codigo',fld:'vTFCAIXA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFCaixa_Codigo_To',fld:'vTFCAIXA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFCaixa_Documento',fld:'vTFCAIXA_DOCUMENTO',pic:'@!',nv:''},{av:'AV36TFCaixa_Documento_Sel',fld:'vTFCAIXA_DOCUMENTO_SEL',pic:'@!',nv:''},{av:'AV39TFCaixa_Emissao',fld:'vTFCAIXA_EMISSAO',pic:'',nv:''},{av:'AV40TFCaixa_Emissao_To',fld:'vTFCAIXA_EMISSAO_TO',pic:'',nv:''},{av:'AV45TFCaixa_Vencimento',fld:'vTFCAIXA_VENCIMENTO',pic:'',nv:''},{av:'AV46TFCaixa_Vencimento_To',fld:'vTFCAIXA_VENCIMENTO_TO',pic:'',nv:''},{av:'AV51TFCaixa_TipoDeContaCod',fld:'vTFCAIXA_TIPODECONTACOD',pic:'',nv:''},{av:'AV52TFCaixa_TipoDeContaCod_Sel',fld:'vTFCAIXA_TIPODECONTACOD_SEL',pic:'',nv:''},{av:'AV55TFCaixa_Descricao',fld:'vTFCAIXA_DESCRICAO',pic:'@!',nv:''},{av:'AV56TFCaixa_Descricao_Sel',fld:'vTFCAIXA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV59TFCaixa_Valor',fld:'vTFCAIXA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV60TFCaixa_Valor_To',fld:'vTFCAIXA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_Caixa_CodigoTitleControlIdToReplace',fld:'vDDO_CAIXA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Caixa_DocumentoTitleControlIdToReplace',fld:'vDDO_CAIXA_DOCUMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Caixa_EmissaoTitleControlIdToReplace',fld:'vDDO_CAIXA_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Caixa_VencimentoTitleControlIdToReplace',fld:'vDDO_CAIXA_VENCIMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace',fld:'vDDO_CAIXA_TIPODECONTACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Caixa_DescricaoTitleControlIdToReplace',fld:'vDDO_CAIXA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Caixa_ValorTitleControlIdToReplace',fld:'vDDO_CAIXA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_caixa_codigo_Activeeventkey',ctrl:'DDO_CAIXA_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_caixa_codigo_Filteredtext_get',ctrl:'DDO_CAIXA_CODIGO',prop:'FilteredText_get'},{av:'Ddo_caixa_codigo_Filteredtextto_get',ctrl:'DDO_CAIXA_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_caixa_codigo_Sortedstatus',ctrl:'DDO_CAIXA_CODIGO',prop:'SortedStatus'},{av:'AV31TFCaixa_Codigo',fld:'vTFCAIXA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFCaixa_Codigo_To',fld:'vTFCAIXA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_caixa_documento_Sortedstatus',ctrl:'DDO_CAIXA_DOCUMENTO',prop:'SortedStatus'},{av:'Ddo_caixa_emissao_Sortedstatus',ctrl:'DDO_CAIXA_EMISSAO',prop:'SortedStatus'},{av:'Ddo_caixa_vencimento_Sortedstatus',ctrl:'DDO_CAIXA_VENCIMENTO',prop:'SortedStatus'},{av:'Ddo_caixa_tipodecontacod_Sortedstatus',ctrl:'DDO_CAIXA_TIPODECONTACOD',prop:'SortedStatus'},{av:'Ddo_caixa_descricao_Sortedstatus',ctrl:'DDO_CAIXA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_caixa_valor_Sortedstatus',ctrl:'DDO_CAIXA_VALOR',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CAIXA_DOCUMENTO.ONOPTIONCLICKED","{handler:'E13FQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Caixa_Documento1',fld:'vCAIXA_DOCUMENTO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Caixa_Documento2',fld:'vCAIXA_DOCUMENTO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Caixa_Documento3',fld:'vCAIXA_DOCUMENTO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFCaixa_Codigo',fld:'vTFCAIXA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFCaixa_Codigo_To',fld:'vTFCAIXA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFCaixa_Documento',fld:'vTFCAIXA_DOCUMENTO',pic:'@!',nv:''},{av:'AV36TFCaixa_Documento_Sel',fld:'vTFCAIXA_DOCUMENTO_SEL',pic:'@!',nv:''},{av:'AV39TFCaixa_Emissao',fld:'vTFCAIXA_EMISSAO',pic:'',nv:''},{av:'AV40TFCaixa_Emissao_To',fld:'vTFCAIXA_EMISSAO_TO',pic:'',nv:''},{av:'AV45TFCaixa_Vencimento',fld:'vTFCAIXA_VENCIMENTO',pic:'',nv:''},{av:'AV46TFCaixa_Vencimento_To',fld:'vTFCAIXA_VENCIMENTO_TO',pic:'',nv:''},{av:'AV51TFCaixa_TipoDeContaCod',fld:'vTFCAIXA_TIPODECONTACOD',pic:'',nv:''},{av:'AV52TFCaixa_TipoDeContaCod_Sel',fld:'vTFCAIXA_TIPODECONTACOD_SEL',pic:'',nv:''},{av:'AV55TFCaixa_Descricao',fld:'vTFCAIXA_DESCRICAO',pic:'@!',nv:''},{av:'AV56TFCaixa_Descricao_Sel',fld:'vTFCAIXA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV59TFCaixa_Valor',fld:'vTFCAIXA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV60TFCaixa_Valor_To',fld:'vTFCAIXA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_Caixa_CodigoTitleControlIdToReplace',fld:'vDDO_CAIXA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Caixa_DocumentoTitleControlIdToReplace',fld:'vDDO_CAIXA_DOCUMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Caixa_EmissaoTitleControlIdToReplace',fld:'vDDO_CAIXA_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Caixa_VencimentoTitleControlIdToReplace',fld:'vDDO_CAIXA_VENCIMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace',fld:'vDDO_CAIXA_TIPODECONTACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Caixa_DescricaoTitleControlIdToReplace',fld:'vDDO_CAIXA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Caixa_ValorTitleControlIdToReplace',fld:'vDDO_CAIXA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_caixa_documento_Activeeventkey',ctrl:'DDO_CAIXA_DOCUMENTO',prop:'ActiveEventKey'},{av:'Ddo_caixa_documento_Filteredtext_get',ctrl:'DDO_CAIXA_DOCUMENTO',prop:'FilteredText_get'},{av:'Ddo_caixa_documento_Selectedvalue_get',ctrl:'DDO_CAIXA_DOCUMENTO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_caixa_documento_Sortedstatus',ctrl:'DDO_CAIXA_DOCUMENTO',prop:'SortedStatus'},{av:'AV35TFCaixa_Documento',fld:'vTFCAIXA_DOCUMENTO',pic:'@!',nv:''},{av:'AV36TFCaixa_Documento_Sel',fld:'vTFCAIXA_DOCUMENTO_SEL',pic:'@!',nv:''},{av:'Ddo_caixa_codigo_Sortedstatus',ctrl:'DDO_CAIXA_CODIGO',prop:'SortedStatus'},{av:'Ddo_caixa_emissao_Sortedstatus',ctrl:'DDO_CAIXA_EMISSAO',prop:'SortedStatus'},{av:'Ddo_caixa_vencimento_Sortedstatus',ctrl:'DDO_CAIXA_VENCIMENTO',prop:'SortedStatus'},{av:'Ddo_caixa_tipodecontacod_Sortedstatus',ctrl:'DDO_CAIXA_TIPODECONTACOD',prop:'SortedStatus'},{av:'Ddo_caixa_descricao_Sortedstatus',ctrl:'DDO_CAIXA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_caixa_valor_Sortedstatus',ctrl:'DDO_CAIXA_VALOR',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CAIXA_EMISSAO.ONOPTIONCLICKED","{handler:'E14FQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Caixa_Documento1',fld:'vCAIXA_DOCUMENTO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Caixa_Documento2',fld:'vCAIXA_DOCUMENTO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Caixa_Documento3',fld:'vCAIXA_DOCUMENTO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFCaixa_Codigo',fld:'vTFCAIXA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFCaixa_Codigo_To',fld:'vTFCAIXA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFCaixa_Documento',fld:'vTFCAIXA_DOCUMENTO',pic:'@!',nv:''},{av:'AV36TFCaixa_Documento_Sel',fld:'vTFCAIXA_DOCUMENTO_SEL',pic:'@!',nv:''},{av:'AV39TFCaixa_Emissao',fld:'vTFCAIXA_EMISSAO',pic:'',nv:''},{av:'AV40TFCaixa_Emissao_To',fld:'vTFCAIXA_EMISSAO_TO',pic:'',nv:''},{av:'AV45TFCaixa_Vencimento',fld:'vTFCAIXA_VENCIMENTO',pic:'',nv:''},{av:'AV46TFCaixa_Vencimento_To',fld:'vTFCAIXA_VENCIMENTO_TO',pic:'',nv:''},{av:'AV51TFCaixa_TipoDeContaCod',fld:'vTFCAIXA_TIPODECONTACOD',pic:'',nv:''},{av:'AV52TFCaixa_TipoDeContaCod_Sel',fld:'vTFCAIXA_TIPODECONTACOD_SEL',pic:'',nv:''},{av:'AV55TFCaixa_Descricao',fld:'vTFCAIXA_DESCRICAO',pic:'@!',nv:''},{av:'AV56TFCaixa_Descricao_Sel',fld:'vTFCAIXA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV59TFCaixa_Valor',fld:'vTFCAIXA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV60TFCaixa_Valor_To',fld:'vTFCAIXA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_Caixa_CodigoTitleControlIdToReplace',fld:'vDDO_CAIXA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Caixa_DocumentoTitleControlIdToReplace',fld:'vDDO_CAIXA_DOCUMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Caixa_EmissaoTitleControlIdToReplace',fld:'vDDO_CAIXA_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Caixa_VencimentoTitleControlIdToReplace',fld:'vDDO_CAIXA_VENCIMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace',fld:'vDDO_CAIXA_TIPODECONTACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Caixa_DescricaoTitleControlIdToReplace',fld:'vDDO_CAIXA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Caixa_ValorTitleControlIdToReplace',fld:'vDDO_CAIXA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_caixa_emissao_Activeeventkey',ctrl:'DDO_CAIXA_EMISSAO',prop:'ActiveEventKey'},{av:'Ddo_caixa_emissao_Filteredtext_get',ctrl:'DDO_CAIXA_EMISSAO',prop:'FilteredText_get'},{av:'Ddo_caixa_emissao_Filteredtextto_get',ctrl:'DDO_CAIXA_EMISSAO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_caixa_emissao_Sortedstatus',ctrl:'DDO_CAIXA_EMISSAO',prop:'SortedStatus'},{av:'AV39TFCaixa_Emissao',fld:'vTFCAIXA_EMISSAO',pic:'',nv:''},{av:'AV40TFCaixa_Emissao_To',fld:'vTFCAIXA_EMISSAO_TO',pic:'',nv:''},{av:'Ddo_caixa_codigo_Sortedstatus',ctrl:'DDO_CAIXA_CODIGO',prop:'SortedStatus'},{av:'Ddo_caixa_documento_Sortedstatus',ctrl:'DDO_CAIXA_DOCUMENTO',prop:'SortedStatus'},{av:'Ddo_caixa_vencimento_Sortedstatus',ctrl:'DDO_CAIXA_VENCIMENTO',prop:'SortedStatus'},{av:'Ddo_caixa_tipodecontacod_Sortedstatus',ctrl:'DDO_CAIXA_TIPODECONTACOD',prop:'SortedStatus'},{av:'Ddo_caixa_descricao_Sortedstatus',ctrl:'DDO_CAIXA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_caixa_valor_Sortedstatus',ctrl:'DDO_CAIXA_VALOR',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CAIXA_VENCIMENTO.ONOPTIONCLICKED","{handler:'E15FQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Caixa_Documento1',fld:'vCAIXA_DOCUMENTO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Caixa_Documento2',fld:'vCAIXA_DOCUMENTO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Caixa_Documento3',fld:'vCAIXA_DOCUMENTO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFCaixa_Codigo',fld:'vTFCAIXA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFCaixa_Codigo_To',fld:'vTFCAIXA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFCaixa_Documento',fld:'vTFCAIXA_DOCUMENTO',pic:'@!',nv:''},{av:'AV36TFCaixa_Documento_Sel',fld:'vTFCAIXA_DOCUMENTO_SEL',pic:'@!',nv:''},{av:'AV39TFCaixa_Emissao',fld:'vTFCAIXA_EMISSAO',pic:'',nv:''},{av:'AV40TFCaixa_Emissao_To',fld:'vTFCAIXA_EMISSAO_TO',pic:'',nv:''},{av:'AV45TFCaixa_Vencimento',fld:'vTFCAIXA_VENCIMENTO',pic:'',nv:''},{av:'AV46TFCaixa_Vencimento_To',fld:'vTFCAIXA_VENCIMENTO_TO',pic:'',nv:''},{av:'AV51TFCaixa_TipoDeContaCod',fld:'vTFCAIXA_TIPODECONTACOD',pic:'',nv:''},{av:'AV52TFCaixa_TipoDeContaCod_Sel',fld:'vTFCAIXA_TIPODECONTACOD_SEL',pic:'',nv:''},{av:'AV55TFCaixa_Descricao',fld:'vTFCAIXA_DESCRICAO',pic:'@!',nv:''},{av:'AV56TFCaixa_Descricao_Sel',fld:'vTFCAIXA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV59TFCaixa_Valor',fld:'vTFCAIXA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV60TFCaixa_Valor_To',fld:'vTFCAIXA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_Caixa_CodigoTitleControlIdToReplace',fld:'vDDO_CAIXA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Caixa_DocumentoTitleControlIdToReplace',fld:'vDDO_CAIXA_DOCUMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Caixa_EmissaoTitleControlIdToReplace',fld:'vDDO_CAIXA_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Caixa_VencimentoTitleControlIdToReplace',fld:'vDDO_CAIXA_VENCIMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace',fld:'vDDO_CAIXA_TIPODECONTACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Caixa_DescricaoTitleControlIdToReplace',fld:'vDDO_CAIXA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Caixa_ValorTitleControlIdToReplace',fld:'vDDO_CAIXA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_caixa_vencimento_Activeeventkey',ctrl:'DDO_CAIXA_VENCIMENTO',prop:'ActiveEventKey'},{av:'Ddo_caixa_vencimento_Filteredtext_get',ctrl:'DDO_CAIXA_VENCIMENTO',prop:'FilteredText_get'},{av:'Ddo_caixa_vencimento_Filteredtextto_get',ctrl:'DDO_CAIXA_VENCIMENTO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_caixa_vencimento_Sortedstatus',ctrl:'DDO_CAIXA_VENCIMENTO',prop:'SortedStatus'},{av:'AV45TFCaixa_Vencimento',fld:'vTFCAIXA_VENCIMENTO',pic:'',nv:''},{av:'AV46TFCaixa_Vencimento_To',fld:'vTFCAIXA_VENCIMENTO_TO',pic:'',nv:''},{av:'Ddo_caixa_codigo_Sortedstatus',ctrl:'DDO_CAIXA_CODIGO',prop:'SortedStatus'},{av:'Ddo_caixa_documento_Sortedstatus',ctrl:'DDO_CAIXA_DOCUMENTO',prop:'SortedStatus'},{av:'Ddo_caixa_emissao_Sortedstatus',ctrl:'DDO_CAIXA_EMISSAO',prop:'SortedStatus'},{av:'Ddo_caixa_tipodecontacod_Sortedstatus',ctrl:'DDO_CAIXA_TIPODECONTACOD',prop:'SortedStatus'},{av:'Ddo_caixa_descricao_Sortedstatus',ctrl:'DDO_CAIXA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_caixa_valor_Sortedstatus',ctrl:'DDO_CAIXA_VALOR',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CAIXA_TIPODECONTACOD.ONOPTIONCLICKED","{handler:'E16FQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Caixa_Documento1',fld:'vCAIXA_DOCUMENTO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Caixa_Documento2',fld:'vCAIXA_DOCUMENTO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Caixa_Documento3',fld:'vCAIXA_DOCUMENTO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFCaixa_Codigo',fld:'vTFCAIXA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFCaixa_Codigo_To',fld:'vTFCAIXA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFCaixa_Documento',fld:'vTFCAIXA_DOCUMENTO',pic:'@!',nv:''},{av:'AV36TFCaixa_Documento_Sel',fld:'vTFCAIXA_DOCUMENTO_SEL',pic:'@!',nv:''},{av:'AV39TFCaixa_Emissao',fld:'vTFCAIXA_EMISSAO',pic:'',nv:''},{av:'AV40TFCaixa_Emissao_To',fld:'vTFCAIXA_EMISSAO_TO',pic:'',nv:''},{av:'AV45TFCaixa_Vencimento',fld:'vTFCAIXA_VENCIMENTO',pic:'',nv:''},{av:'AV46TFCaixa_Vencimento_To',fld:'vTFCAIXA_VENCIMENTO_TO',pic:'',nv:''},{av:'AV51TFCaixa_TipoDeContaCod',fld:'vTFCAIXA_TIPODECONTACOD',pic:'',nv:''},{av:'AV52TFCaixa_TipoDeContaCod_Sel',fld:'vTFCAIXA_TIPODECONTACOD_SEL',pic:'',nv:''},{av:'AV55TFCaixa_Descricao',fld:'vTFCAIXA_DESCRICAO',pic:'@!',nv:''},{av:'AV56TFCaixa_Descricao_Sel',fld:'vTFCAIXA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV59TFCaixa_Valor',fld:'vTFCAIXA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV60TFCaixa_Valor_To',fld:'vTFCAIXA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_Caixa_CodigoTitleControlIdToReplace',fld:'vDDO_CAIXA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Caixa_DocumentoTitleControlIdToReplace',fld:'vDDO_CAIXA_DOCUMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Caixa_EmissaoTitleControlIdToReplace',fld:'vDDO_CAIXA_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Caixa_VencimentoTitleControlIdToReplace',fld:'vDDO_CAIXA_VENCIMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace',fld:'vDDO_CAIXA_TIPODECONTACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Caixa_DescricaoTitleControlIdToReplace',fld:'vDDO_CAIXA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Caixa_ValorTitleControlIdToReplace',fld:'vDDO_CAIXA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_caixa_tipodecontacod_Activeeventkey',ctrl:'DDO_CAIXA_TIPODECONTACOD',prop:'ActiveEventKey'},{av:'Ddo_caixa_tipodecontacod_Filteredtext_get',ctrl:'DDO_CAIXA_TIPODECONTACOD',prop:'FilteredText_get'},{av:'Ddo_caixa_tipodecontacod_Selectedvalue_get',ctrl:'DDO_CAIXA_TIPODECONTACOD',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_caixa_tipodecontacod_Sortedstatus',ctrl:'DDO_CAIXA_TIPODECONTACOD',prop:'SortedStatus'},{av:'AV51TFCaixa_TipoDeContaCod',fld:'vTFCAIXA_TIPODECONTACOD',pic:'',nv:''},{av:'AV52TFCaixa_TipoDeContaCod_Sel',fld:'vTFCAIXA_TIPODECONTACOD_SEL',pic:'',nv:''},{av:'Ddo_caixa_codigo_Sortedstatus',ctrl:'DDO_CAIXA_CODIGO',prop:'SortedStatus'},{av:'Ddo_caixa_documento_Sortedstatus',ctrl:'DDO_CAIXA_DOCUMENTO',prop:'SortedStatus'},{av:'Ddo_caixa_emissao_Sortedstatus',ctrl:'DDO_CAIXA_EMISSAO',prop:'SortedStatus'},{av:'Ddo_caixa_vencimento_Sortedstatus',ctrl:'DDO_CAIXA_VENCIMENTO',prop:'SortedStatus'},{av:'Ddo_caixa_descricao_Sortedstatus',ctrl:'DDO_CAIXA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_caixa_valor_Sortedstatus',ctrl:'DDO_CAIXA_VALOR',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CAIXA_DESCRICAO.ONOPTIONCLICKED","{handler:'E17FQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Caixa_Documento1',fld:'vCAIXA_DOCUMENTO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Caixa_Documento2',fld:'vCAIXA_DOCUMENTO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Caixa_Documento3',fld:'vCAIXA_DOCUMENTO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFCaixa_Codigo',fld:'vTFCAIXA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFCaixa_Codigo_To',fld:'vTFCAIXA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFCaixa_Documento',fld:'vTFCAIXA_DOCUMENTO',pic:'@!',nv:''},{av:'AV36TFCaixa_Documento_Sel',fld:'vTFCAIXA_DOCUMENTO_SEL',pic:'@!',nv:''},{av:'AV39TFCaixa_Emissao',fld:'vTFCAIXA_EMISSAO',pic:'',nv:''},{av:'AV40TFCaixa_Emissao_To',fld:'vTFCAIXA_EMISSAO_TO',pic:'',nv:''},{av:'AV45TFCaixa_Vencimento',fld:'vTFCAIXA_VENCIMENTO',pic:'',nv:''},{av:'AV46TFCaixa_Vencimento_To',fld:'vTFCAIXA_VENCIMENTO_TO',pic:'',nv:''},{av:'AV51TFCaixa_TipoDeContaCod',fld:'vTFCAIXA_TIPODECONTACOD',pic:'',nv:''},{av:'AV52TFCaixa_TipoDeContaCod_Sel',fld:'vTFCAIXA_TIPODECONTACOD_SEL',pic:'',nv:''},{av:'AV55TFCaixa_Descricao',fld:'vTFCAIXA_DESCRICAO',pic:'@!',nv:''},{av:'AV56TFCaixa_Descricao_Sel',fld:'vTFCAIXA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV59TFCaixa_Valor',fld:'vTFCAIXA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV60TFCaixa_Valor_To',fld:'vTFCAIXA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_Caixa_CodigoTitleControlIdToReplace',fld:'vDDO_CAIXA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Caixa_DocumentoTitleControlIdToReplace',fld:'vDDO_CAIXA_DOCUMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Caixa_EmissaoTitleControlIdToReplace',fld:'vDDO_CAIXA_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Caixa_VencimentoTitleControlIdToReplace',fld:'vDDO_CAIXA_VENCIMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace',fld:'vDDO_CAIXA_TIPODECONTACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Caixa_DescricaoTitleControlIdToReplace',fld:'vDDO_CAIXA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Caixa_ValorTitleControlIdToReplace',fld:'vDDO_CAIXA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_caixa_descricao_Activeeventkey',ctrl:'DDO_CAIXA_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_caixa_descricao_Filteredtext_get',ctrl:'DDO_CAIXA_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_caixa_descricao_Selectedvalue_get',ctrl:'DDO_CAIXA_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_caixa_descricao_Sortedstatus',ctrl:'DDO_CAIXA_DESCRICAO',prop:'SortedStatus'},{av:'AV55TFCaixa_Descricao',fld:'vTFCAIXA_DESCRICAO',pic:'@!',nv:''},{av:'AV56TFCaixa_Descricao_Sel',fld:'vTFCAIXA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_caixa_codigo_Sortedstatus',ctrl:'DDO_CAIXA_CODIGO',prop:'SortedStatus'},{av:'Ddo_caixa_documento_Sortedstatus',ctrl:'DDO_CAIXA_DOCUMENTO',prop:'SortedStatus'},{av:'Ddo_caixa_emissao_Sortedstatus',ctrl:'DDO_CAIXA_EMISSAO',prop:'SortedStatus'},{av:'Ddo_caixa_vencimento_Sortedstatus',ctrl:'DDO_CAIXA_VENCIMENTO',prop:'SortedStatus'},{av:'Ddo_caixa_tipodecontacod_Sortedstatus',ctrl:'DDO_CAIXA_TIPODECONTACOD',prop:'SortedStatus'},{av:'Ddo_caixa_valor_Sortedstatus',ctrl:'DDO_CAIXA_VALOR',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CAIXA_VALOR.ONOPTIONCLICKED","{handler:'E18FQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Caixa_Documento1',fld:'vCAIXA_DOCUMENTO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Caixa_Documento2',fld:'vCAIXA_DOCUMENTO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Caixa_Documento3',fld:'vCAIXA_DOCUMENTO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFCaixa_Codigo',fld:'vTFCAIXA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFCaixa_Codigo_To',fld:'vTFCAIXA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFCaixa_Documento',fld:'vTFCAIXA_DOCUMENTO',pic:'@!',nv:''},{av:'AV36TFCaixa_Documento_Sel',fld:'vTFCAIXA_DOCUMENTO_SEL',pic:'@!',nv:''},{av:'AV39TFCaixa_Emissao',fld:'vTFCAIXA_EMISSAO',pic:'',nv:''},{av:'AV40TFCaixa_Emissao_To',fld:'vTFCAIXA_EMISSAO_TO',pic:'',nv:''},{av:'AV45TFCaixa_Vencimento',fld:'vTFCAIXA_VENCIMENTO',pic:'',nv:''},{av:'AV46TFCaixa_Vencimento_To',fld:'vTFCAIXA_VENCIMENTO_TO',pic:'',nv:''},{av:'AV51TFCaixa_TipoDeContaCod',fld:'vTFCAIXA_TIPODECONTACOD',pic:'',nv:''},{av:'AV52TFCaixa_TipoDeContaCod_Sel',fld:'vTFCAIXA_TIPODECONTACOD_SEL',pic:'',nv:''},{av:'AV55TFCaixa_Descricao',fld:'vTFCAIXA_DESCRICAO',pic:'@!',nv:''},{av:'AV56TFCaixa_Descricao_Sel',fld:'vTFCAIXA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV59TFCaixa_Valor',fld:'vTFCAIXA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV60TFCaixa_Valor_To',fld:'vTFCAIXA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_Caixa_CodigoTitleControlIdToReplace',fld:'vDDO_CAIXA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Caixa_DocumentoTitleControlIdToReplace',fld:'vDDO_CAIXA_DOCUMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Caixa_EmissaoTitleControlIdToReplace',fld:'vDDO_CAIXA_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Caixa_VencimentoTitleControlIdToReplace',fld:'vDDO_CAIXA_VENCIMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace',fld:'vDDO_CAIXA_TIPODECONTACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Caixa_DescricaoTitleControlIdToReplace',fld:'vDDO_CAIXA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Caixa_ValorTitleControlIdToReplace',fld:'vDDO_CAIXA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_caixa_valor_Activeeventkey',ctrl:'DDO_CAIXA_VALOR',prop:'ActiveEventKey'},{av:'Ddo_caixa_valor_Filteredtext_get',ctrl:'DDO_CAIXA_VALOR',prop:'FilteredText_get'},{av:'Ddo_caixa_valor_Filteredtextto_get',ctrl:'DDO_CAIXA_VALOR',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_caixa_valor_Sortedstatus',ctrl:'DDO_CAIXA_VALOR',prop:'SortedStatus'},{av:'AV59TFCaixa_Valor',fld:'vTFCAIXA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV60TFCaixa_Valor_To',fld:'vTFCAIXA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_caixa_codigo_Sortedstatus',ctrl:'DDO_CAIXA_CODIGO',prop:'SortedStatus'},{av:'Ddo_caixa_documento_Sortedstatus',ctrl:'DDO_CAIXA_DOCUMENTO',prop:'SortedStatus'},{av:'Ddo_caixa_emissao_Sortedstatus',ctrl:'DDO_CAIXA_EMISSAO',prop:'SortedStatus'},{av:'Ddo_caixa_vencimento_Sortedstatus',ctrl:'DDO_CAIXA_VENCIMENTO',prop:'SortedStatus'},{av:'Ddo_caixa_tipodecontacod_Sortedstatus',ctrl:'DDO_CAIXA_TIPODECONTACOD',prop:'SortedStatus'},{av:'Ddo_caixa_descricao_Sortedstatus',ctrl:'DDO_CAIXA_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E31FQ2',iparms:[],oparms:[{av:'AV28Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E32FQ2',iparms:[{av:'A874Caixa_Codigo',fld:'CAIXA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A875Caixa_Documento',fld:'CAIXA_DOCUMENTO',pic:'@!',hsh:true,nv:''}],oparms:[{av:'AV7InOutCaixa_Codigo',fld:'vINOUTCAIXA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutCaixa_Documento',fld:'vINOUTCAIXA_DOCUMENTO',pic:'@!',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E19FQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Caixa_Documento1',fld:'vCAIXA_DOCUMENTO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Caixa_Documento2',fld:'vCAIXA_DOCUMENTO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Caixa_Documento3',fld:'vCAIXA_DOCUMENTO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFCaixa_Codigo',fld:'vTFCAIXA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFCaixa_Codigo_To',fld:'vTFCAIXA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFCaixa_Documento',fld:'vTFCAIXA_DOCUMENTO',pic:'@!',nv:''},{av:'AV36TFCaixa_Documento_Sel',fld:'vTFCAIXA_DOCUMENTO_SEL',pic:'@!',nv:''},{av:'AV39TFCaixa_Emissao',fld:'vTFCAIXA_EMISSAO',pic:'',nv:''},{av:'AV40TFCaixa_Emissao_To',fld:'vTFCAIXA_EMISSAO_TO',pic:'',nv:''},{av:'AV45TFCaixa_Vencimento',fld:'vTFCAIXA_VENCIMENTO',pic:'',nv:''},{av:'AV46TFCaixa_Vencimento_To',fld:'vTFCAIXA_VENCIMENTO_TO',pic:'',nv:''},{av:'AV51TFCaixa_TipoDeContaCod',fld:'vTFCAIXA_TIPODECONTACOD',pic:'',nv:''},{av:'AV52TFCaixa_TipoDeContaCod_Sel',fld:'vTFCAIXA_TIPODECONTACOD_SEL',pic:'',nv:''},{av:'AV55TFCaixa_Descricao',fld:'vTFCAIXA_DESCRICAO',pic:'@!',nv:''},{av:'AV56TFCaixa_Descricao_Sel',fld:'vTFCAIXA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV59TFCaixa_Valor',fld:'vTFCAIXA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV60TFCaixa_Valor_To',fld:'vTFCAIXA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_Caixa_CodigoTitleControlIdToReplace',fld:'vDDO_CAIXA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Caixa_DocumentoTitleControlIdToReplace',fld:'vDDO_CAIXA_DOCUMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Caixa_EmissaoTitleControlIdToReplace',fld:'vDDO_CAIXA_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Caixa_VencimentoTitleControlIdToReplace',fld:'vDDO_CAIXA_VENCIMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace',fld:'vDDO_CAIXA_TIPODECONTACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Caixa_DescricaoTitleControlIdToReplace',fld:'vDDO_CAIXA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Caixa_ValorTitleControlIdToReplace',fld:'vDDO_CAIXA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E24FQ2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E20FQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Caixa_Documento1',fld:'vCAIXA_DOCUMENTO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Caixa_Documento2',fld:'vCAIXA_DOCUMENTO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Caixa_Documento3',fld:'vCAIXA_DOCUMENTO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFCaixa_Codigo',fld:'vTFCAIXA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFCaixa_Codigo_To',fld:'vTFCAIXA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFCaixa_Documento',fld:'vTFCAIXA_DOCUMENTO',pic:'@!',nv:''},{av:'AV36TFCaixa_Documento_Sel',fld:'vTFCAIXA_DOCUMENTO_SEL',pic:'@!',nv:''},{av:'AV39TFCaixa_Emissao',fld:'vTFCAIXA_EMISSAO',pic:'',nv:''},{av:'AV40TFCaixa_Emissao_To',fld:'vTFCAIXA_EMISSAO_TO',pic:'',nv:''},{av:'AV45TFCaixa_Vencimento',fld:'vTFCAIXA_VENCIMENTO',pic:'',nv:''},{av:'AV46TFCaixa_Vencimento_To',fld:'vTFCAIXA_VENCIMENTO_TO',pic:'',nv:''},{av:'AV51TFCaixa_TipoDeContaCod',fld:'vTFCAIXA_TIPODECONTACOD',pic:'',nv:''},{av:'AV52TFCaixa_TipoDeContaCod_Sel',fld:'vTFCAIXA_TIPODECONTACOD_SEL',pic:'',nv:''},{av:'AV55TFCaixa_Descricao',fld:'vTFCAIXA_DESCRICAO',pic:'@!',nv:''},{av:'AV56TFCaixa_Descricao_Sel',fld:'vTFCAIXA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV59TFCaixa_Valor',fld:'vTFCAIXA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV60TFCaixa_Valor_To',fld:'vTFCAIXA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_Caixa_CodigoTitleControlIdToReplace',fld:'vDDO_CAIXA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Caixa_DocumentoTitleControlIdToReplace',fld:'vDDO_CAIXA_DOCUMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Caixa_EmissaoTitleControlIdToReplace',fld:'vDDO_CAIXA_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Caixa_VencimentoTitleControlIdToReplace',fld:'vDDO_CAIXA_VENCIMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace',fld:'vDDO_CAIXA_TIPODECONTACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Caixa_DescricaoTitleControlIdToReplace',fld:'vDDO_CAIXA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Caixa_ValorTitleControlIdToReplace',fld:'vDDO_CAIXA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Caixa_Documento2',fld:'vCAIXA_DOCUMENTO2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Caixa_Documento3',fld:'vCAIXA_DOCUMENTO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Caixa_Documento1',fld:'vCAIXA_DOCUMENTO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavCaixa_documento2_Visible',ctrl:'vCAIXA_DOCUMENTO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavCaixa_documento3_Visible',ctrl:'vCAIXA_DOCUMENTO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavCaixa_documento1_Visible',ctrl:'vCAIXA_DOCUMENTO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E25FQ2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavCaixa_documento1_Visible',ctrl:'vCAIXA_DOCUMENTO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E26FQ2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E21FQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Caixa_Documento1',fld:'vCAIXA_DOCUMENTO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Caixa_Documento2',fld:'vCAIXA_DOCUMENTO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Caixa_Documento3',fld:'vCAIXA_DOCUMENTO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFCaixa_Codigo',fld:'vTFCAIXA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFCaixa_Codigo_To',fld:'vTFCAIXA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFCaixa_Documento',fld:'vTFCAIXA_DOCUMENTO',pic:'@!',nv:''},{av:'AV36TFCaixa_Documento_Sel',fld:'vTFCAIXA_DOCUMENTO_SEL',pic:'@!',nv:''},{av:'AV39TFCaixa_Emissao',fld:'vTFCAIXA_EMISSAO',pic:'',nv:''},{av:'AV40TFCaixa_Emissao_To',fld:'vTFCAIXA_EMISSAO_TO',pic:'',nv:''},{av:'AV45TFCaixa_Vencimento',fld:'vTFCAIXA_VENCIMENTO',pic:'',nv:''},{av:'AV46TFCaixa_Vencimento_To',fld:'vTFCAIXA_VENCIMENTO_TO',pic:'',nv:''},{av:'AV51TFCaixa_TipoDeContaCod',fld:'vTFCAIXA_TIPODECONTACOD',pic:'',nv:''},{av:'AV52TFCaixa_TipoDeContaCod_Sel',fld:'vTFCAIXA_TIPODECONTACOD_SEL',pic:'',nv:''},{av:'AV55TFCaixa_Descricao',fld:'vTFCAIXA_DESCRICAO',pic:'@!',nv:''},{av:'AV56TFCaixa_Descricao_Sel',fld:'vTFCAIXA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV59TFCaixa_Valor',fld:'vTFCAIXA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV60TFCaixa_Valor_To',fld:'vTFCAIXA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_Caixa_CodigoTitleControlIdToReplace',fld:'vDDO_CAIXA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Caixa_DocumentoTitleControlIdToReplace',fld:'vDDO_CAIXA_DOCUMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Caixa_EmissaoTitleControlIdToReplace',fld:'vDDO_CAIXA_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Caixa_VencimentoTitleControlIdToReplace',fld:'vDDO_CAIXA_VENCIMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace',fld:'vDDO_CAIXA_TIPODECONTACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Caixa_DescricaoTitleControlIdToReplace',fld:'vDDO_CAIXA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Caixa_ValorTitleControlIdToReplace',fld:'vDDO_CAIXA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Caixa_Documento2',fld:'vCAIXA_DOCUMENTO2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Caixa_Documento3',fld:'vCAIXA_DOCUMENTO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Caixa_Documento1',fld:'vCAIXA_DOCUMENTO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavCaixa_documento2_Visible',ctrl:'vCAIXA_DOCUMENTO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavCaixa_documento3_Visible',ctrl:'vCAIXA_DOCUMENTO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavCaixa_documento1_Visible',ctrl:'vCAIXA_DOCUMENTO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E27FQ2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavCaixa_documento2_Visible',ctrl:'vCAIXA_DOCUMENTO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E22FQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Caixa_Documento1',fld:'vCAIXA_DOCUMENTO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Caixa_Documento2',fld:'vCAIXA_DOCUMENTO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Caixa_Documento3',fld:'vCAIXA_DOCUMENTO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFCaixa_Codigo',fld:'vTFCAIXA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFCaixa_Codigo_To',fld:'vTFCAIXA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFCaixa_Documento',fld:'vTFCAIXA_DOCUMENTO',pic:'@!',nv:''},{av:'AV36TFCaixa_Documento_Sel',fld:'vTFCAIXA_DOCUMENTO_SEL',pic:'@!',nv:''},{av:'AV39TFCaixa_Emissao',fld:'vTFCAIXA_EMISSAO',pic:'',nv:''},{av:'AV40TFCaixa_Emissao_To',fld:'vTFCAIXA_EMISSAO_TO',pic:'',nv:''},{av:'AV45TFCaixa_Vencimento',fld:'vTFCAIXA_VENCIMENTO',pic:'',nv:''},{av:'AV46TFCaixa_Vencimento_To',fld:'vTFCAIXA_VENCIMENTO_TO',pic:'',nv:''},{av:'AV51TFCaixa_TipoDeContaCod',fld:'vTFCAIXA_TIPODECONTACOD',pic:'',nv:''},{av:'AV52TFCaixa_TipoDeContaCod_Sel',fld:'vTFCAIXA_TIPODECONTACOD_SEL',pic:'',nv:''},{av:'AV55TFCaixa_Descricao',fld:'vTFCAIXA_DESCRICAO',pic:'@!',nv:''},{av:'AV56TFCaixa_Descricao_Sel',fld:'vTFCAIXA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV59TFCaixa_Valor',fld:'vTFCAIXA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV60TFCaixa_Valor_To',fld:'vTFCAIXA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_Caixa_CodigoTitleControlIdToReplace',fld:'vDDO_CAIXA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Caixa_DocumentoTitleControlIdToReplace',fld:'vDDO_CAIXA_DOCUMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Caixa_EmissaoTitleControlIdToReplace',fld:'vDDO_CAIXA_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Caixa_VencimentoTitleControlIdToReplace',fld:'vDDO_CAIXA_VENCIMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace',fld:'vDDO_CAIXA_TIPODECONTACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Caixa_DescricaoTitleControlIdToReplace',fld:'vDDO_CAIXA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Caixa_ValorTitleControlIdToReplace',fld:'vDDO_CAIXA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Caixa_Documento2',fld:'vCAIXA_DOCUMENTO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Caixa_Documento3',fld:'vCAIXA_DOCUMENTO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Caixa_Documento1',fld:'vCAIXA_DOCUMENTO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavCaixa_documento2_Visible',ctrl:'vCAIXA_DOCUMENTO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavCaixa_documento3_Visible',ctrl:'vCAIXA_DOCUMENTO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavCaixa_documento1_Visible',ctrl:'vCAIXA_DOCUMENTO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E28FQ2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavCaixa_documento3_Visible',ctrl:'vCAIXA_DOCUMENTO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E23FQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Caixa_Documento1',fld:'vCAIXA_DOCUMENTO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Caixa_Documento2',fld:'vCAIXA_DOCUMENTO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Caixa_Documento3',fld:'vCAIXA_DOCUMENTO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFCaixa_Codigo',fld:'vTFCAIXA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFCaixa_Codigo_To',fld:'vTFCAIXA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFCaixa_Documento',fld:'vTFCAIXA_DOCUMENTO',pic:'@!',nv:''},{av:'AV36TFCaixa_Documento_Sel',fld:'vTFCAIXA_DOCUMENTO_SEL',pic:'@!',nv:''},{av:'AV39TFCaixa_Emissao',fld:'vTFCAIXA_EMISSAO',pic:'',nv:''},{av:'AV40TFCaixa_Emissao_To',fld:'vTFCAIXA_EMISSAO_TO',pic:'',nv:''},{av:'AV45TFCaixa_Vencimento',fld:'vTFCAIXA_VENCIMENTO',pic:'',nv:''},{av:'AV46TFCaixa_Vencimento_To',fld:'vTFCAIXA_VENCIMENTO_TO',pic:'',nv:''},{av:'AV51TFCaixa_TipoDeContaCod',fld:'vTFCAIXA_TIPODECONTACOD',pic:'',nv:''},{av:'AV52TFCaixa_TipoDeContaCod_Sel',fld:'vTFCAIXA_TIPODECONTACOD_SEL',pic:'',nv:''},{av:'AV55TFCaixa_Descricao',fld:'vTFCAIXA_DESCRICAO',pic:'@!',nv:''},{av:'AV56TFCaixa_Descricao_Sel',fld:'vTFCAIXA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV59TFCaixa_Valor',fld:'vTFCAIXA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV60TFCaixa_Valor_To',fld:'vTFCAIXA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_Caixa_CodigoTitleControlIdToReplace',fld:'vDDO_CAIXA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Caixa_DocumentoTitleControlIdToReplace',fld:'vDDO_CAIXA_DOCUMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Caixa_EmissaoTitleControlIdToReplace',fld:'vDDO_CAIXA_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Caixa_VencimentoTitleControlIdToReplace',fld:'vDDO_CAIXA_VENCIMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace',fld:'vDDO_CAIXA_TIPODECONTACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Caixa_DescricaoTitleControlIdToReplace',fld:'vDDO_CAIXA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Caixa_ValorTitleControlIdToReplace',fld:'vDDO_CAIXA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV31TFCaixa_Codigo',fld:'vTFCAIXA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_caixa_codigo_Filteredtext_set',ctrl:'DDO_CAIXA_CODIGO',prop:'FilteredText_set'},{av:'AV32TFCaixa_Codigo_To',fld:'vTFCAIXA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_caixa_codigo_Filteredtextto_set',ctrl:'DDO_CAIXA_CODIGO',prop:'FilteredTextTo_set'},{av:'AV35TFCaixa_Documento',fld:'vTFCAIXA_DOCUMENTO',pic:'@!',nv:''},{av:'Ddo_caixa_documento_Filteredtext_set',ctrl:'DDO_CAIXA_DOCUMENTO',prop:'FilteredText_set'},{av:'AV36TFCaixa_Documento_Sel',fld:'vTFCAIXA_DOCUMENTO_SEL',pic:'@!',nv:''},{av:'Ddo_caixa_documento_Selectedvalue_set',ctrl:'DDO_CAIXA_DOCUMENTO',prop:'SelectedValue_set'},{av:'AV39TFCaixa_Emissao',fld:'vTFCAIXA_EMISSAO',pic:'',nv:''},{av:'Ddo_caixa_emissao_Filteredtext_set',ctrl:'DDO_CAIXA_EMISSAO',prop:'FilteredText_set'},{av:'AV40TFCaixa_Emissao_To',fld:'vTFCAIXA_EMISSAO_TO',pic:'',nv:''},{av:'Ddo_caixa_emissao_Filteredtextto_set',ctrl:'DDO_CAIXA_EMISSAO',prop:'FilteredTextTo_set'},{av:'AV45TFCaixa_Vencimento',fld:'vTFCAIXA_VENCIMENTO',pic:'',nv:''},{av:'Ddo_caixa_vencimento_Filteredtext_set',ctrl:'DDO_CAIXA_VENCIMENTO',prop:'FilteredText_set'},{av:'AV46TFCaixa_Vencimento_To',fld:'vTFCAIXA_VENCIMENTO_TO',pic:'',nv:''},{av:'Ddo_caixa_vencimento_Filteredtextto_set',ctrl:'DDO_CAIXA_VENCIMENTO',prop:'FilteredTextTo_set'},{av:'AV51TFCaixa_TipoDeContaCod',fld:'vTFCAIXA_TIPODECONTACOD',pic:'',nv:''},{av:'Ddo_caixa_tipodecontacod_Filteredtext_set',ctrl:'DDO_CAIXA_TIPODECONTACOD',prop:'FilteredText_set'},{av:'AV52TFCaixa_TipoDeContaCod_Sel',fld:'vTFCAIXA_TIPODECONTACOD_SEL',pic:'',nv:''},{av:'Ddo_caixa_tipodecontacod_Selectedvalue_set',ctrl:'DDO_CAIXA_TIPODECONTACOD',prop:'SelectedValue_set'},{av:'AV55TFCaixa_Descricao',fld:'vTFCAIXA_DESCRICAO',pic:'@!',nv:''},{av:'Ddo_caixa_descricao_Filteredtext_set',ctrl:'DDO_CAIXA_DESCRICAO',prop:'FilteredText_set'},{av:'AV56TFCaixa_Descricao_Sel',fld:'vTFCAIXA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_caixa_descricao_Selectedvalue_set',ctrl:'DDO_CAIXA_DESCRICAO',prop:'SelectedValue_set'},{av:'AV59TFCaixa_Valor',fld:'vTFCAIXA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_caixa_valor_Filteredtext_set',ctrl:'DDO_CAIXA_VALOR',prop:'FilteredText_set'},{av:'AV60TFCaixa_Valor_To',fld:'vTFCAIXA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_caixa_valor_Filteredtextto_set',ctrl:'DDO_CAIXA_VALOR',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Caixa_Documento1',fld:'vCAIXA_DOCUMENTO1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavCaixa_documento1_Visible',ctrl:'vCAIXA_DOCUMENTO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Caixa_Documento2',fld:'vCAIXA_DOCUMENTO2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Caixa_Documento3',fld:'vCAIXA_DOCUMENTO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavCaixa_documento2_Visible',ctrl:'vCAIXA_DOCUMENTO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavCaixa_documento3_Visible',ctrl:'vCAIXA_DOCUMENTO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutCaixa_Documento = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_caixa_codigo_Activeeventkey = "";
         Ddo_caixa_codigo_Filteredtext_get = "";
         Ddo_caixa_codigo_Filteredtextto_get = "";
         Ddo_caixa_documento_Activeeventkey = "";
         Ddo_caixa_documento_Filteredtext_get = "";
         Ddo_caixa_documento_Selectedvalue_get = "";
         Ddo_caixa_emissao_Activeeventkey = "";
         Ddo_caixa_emissao_Filteredtext_get = "";
         Ddo_caixa_emissao_Filteredtextto_get = "";
         Ddo_caixa_vencimento_Activeeventkey = "";
         Ddo_caixa_vencimento_Filteredtext_get = "";
         Ddo_caixa_vencimento_Filteredtextto_get = "";
         Ddo_caixa_tipodecontacod_Activeeventkey = "";
         Ddo_caixa_tipodecontacod_Filteredtext_get = "";
         Ddo_caixa_tipodecontacod_Selectedvalue_get = "";
         Ddo_caixa_descricao_Activeeventkey = "";
         Ddo_caixa_descricao_Filteredtext_get = "";
         Ddo_caixa_descricao_Selectedvalue_get = "";
         Ddo_caixa_valor_Activeeventkey = "";
         Ddo_caixa_valor_Filteredtext_get = "";
         Ddo_caixa_valor_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         A870TipodeConta_Codigo = "";
         AV15DynamicFiltersSelector1 = "";
         AV17Caixa_Documento1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV21Caixa_Documento2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV25Caixa_Documento3 = "";
         AV35TFCaixa_Documento = "";
         AV36TFCaixa_Documento_Sel = "";
         AV39TFCaixa_Emissao = DateTime.MinValue;
         AV40TFCaixa_Emissao_To = DateTime.MinValue;
         AV45TFCaixa_Vencimento = DateTime.MinValue;
         AV46TFCaixa_Vencimento_To = DateTime.MinValue;
         AV51TFCaixa_TipoDeContaCod = "";
         AV52TFCaixa_TipoDeContaCod_Sel = "";
         AV55TFCaixa_Descricao = "";
         AV56TFCaixa_Descricao_Sel = "";
         AV33ddo_Caixa_CodigoTitleControlIdToReplace = "";
         AV37ddo_Caixa_DocumentoTitleControlIdToReplace = "";
         AV43ddo_Caixa_EmissaoTitleControlIdToReplace = "";
         AV49ddo_Caixa_VencimentoTitleControlIdToReplace = "";
         AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace = "";
         AV57ddo_Caixa_DescricaoTitleControlIdToReplace = "";
         AV61ddo_Caixa_ValorTitleControlIdToReplace = "";
         AV69Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV62DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV30Caixa_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34Caixa_DocumentoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38Caixa_EmissaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44Caixa_VencimentoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50Caixa_TipoDeContaCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54Caixa_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58Caixa_ValorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_caixa_codigo_Filteredtext_set = "";
         Ddo_caixa_codigo_Filteredtextto_set = "";
         Ddo_caixa_codigo_Sortedstatus = "";
         Ddo_caixa_documento_Filteredtext_set = "";
         Ddo_caixa_documento_Selectedvalue_set = "";
         Ddo_caixa_documento_Sortedstatus = "";
         Ddo_caixa_emissao_Filteredtext_set = "";
         Ddo_caixa_emissao_Filteredtextto_set = "";
         Ddo_caixa_emissao_Sortedstatus = "";
         Ddo_caixa_vencimento_Filteredtext_set = "";
         Ddo_caixa_vencimento_Filteredtextto_set = "";
         Ddo_caixa_vencimento_Sortedstatus = "";
         Ddo_caixa_tipodecontacod_Filteredtext_set = "";
         Ddo_caixa_tipodecontacod_Selectedvalue_set = "";
         Ddo_caixa_tipodecontacod_Sortedstatus = "";
         Ddo_caixa_descricao_Filteredtext_set = "";
         Ddo_caixa_descricao_Selectedvalue_set = "";
         Ddo_caixa_descricao_Sortedstatus = "";
         Ddo_caixa_valor_Filteredtext_set = "";
         Ddo_caixa_valor_Filteredtextto_set = "";
         Ddo_caixa_valor_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV41DDO_Caixa_EmissaoAuxDate = DateTime.MinValue;
         AV42DDO_Caixa_EmissaoAuxDateTo = DateTime.MinValue;
         AV47DDO_Caixa_VencimentoAuxDate = DateTime.MinValue;
         AV48DDO_Caixa_VencimentoAuxDateTo = DateTime.MinValue;
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Select = "";
         AV68Select_GXI = "";
         A875Caixa_Documento = "";
         A876Caixa_Emissao = DateTime.MinValue;
         A877Caixa_Vencimento = DateTime.MinValue;
         A881Caixa_TipoDeContaCod = "";
         A879Caixa_Descricao = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         l870TipodeConta_Codigo = "";
         H00FQ2_A870TipodeConta_Codigo = new String[] {""} ;
         H00FQ3_A870TipodeConta_Codigo = new String[] {""} ;
         H00FQ4_A870TipodeConta_Codigo = new String[] {""} ;
         GridContainer = new GXWebGrid( context);
         lV17Caixa_Documento1 = "";
         lV21Caixa_Documento2 = "";
         lV25Caixa_Documento3 = "";
         lV35TFCaixa_Documento = "";
         lV51TFCaixa_TipoDeContaCod = "";
         lV55TFCaixa_Descricao = "";
         H00FQ5_A880Caixa_Valor = new decimal[1] ;
         H00FQ5_A879Caixa_Descricao = new String[] {""} ;
         H00FQ5_n879Caixa_Descricao = new bool[] {false} ;
         H00FQ5_A881Caixa_TipoDeContaCod = new String[] {""} ;
         H00FQ5_A877Caixa_Vencimento = new DateTime[] {DateTime.MinValue} ;
         H00FQ5_n877Caixa_Vencimento = new bool[] {false} ;
         H00FQ5_A876Caixa_Emissao = new DateTime[] {DateTime.MinValue} ;
         H00FQ5_n876Caixa_Emissao = new bool[] {false} ;
         H00FQ5_A875Caixa_Documento = new String[] {""} ;
         H00FQ5_A874Caixa_Codigo = new int[1] ;
         H00FQ6_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptcaixa__default(),
            new Object[][] {
                new Object[] {
               H00FQ2_A870TipodeConta_Codigo
               }
               , new Object[] {
               H00FQ3_A870TipodeConta_Codigo
               }
               , new Object[] {
               H00FQ4_A870TipodeConta_Codigo
               }
               , new Object[] {
               H00FQ5_A880Caixa_Valor, H00FQ5_A879Caixa_Descricao, H00FQ5_n879Caixa_Descricao, H00FQ5_A881Caixa_TipoDeContaCod, H00FQ5_A877Caixa_Vencimento, H00FQ5_n877Caixa_Vencimento, H00FQ5_A876Caixa_Emissao, H00FQ5_n876Caixa_Emissao, H00FQ5_A875Caixa_Documento, H00FQ5_A874Caixa_Codigo
               }
               , new Object[] {
               H00FQ6_AGRID_nRecordCount
               }
            }
         );
         AV69Pgmname = "PromptCaixa";
         /* GeneXus formulas. */
         AV69Pgmname = "PromptCaixa";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_80 ;
      private short nGXsfl_80_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV20DynamicFiltersOperator2 ;
      private short AV24DynamicFiltersOperator3 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_80_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtCaixa_Codigo_Titleformat ;
      private short edtCaixa_Documento_Titleformat ;
      private short edtCaixa_Emissao_Titleformat ;
      private short edtCaixa_Vencimento_Titleformat ;
      private short edtCaixa_TipoDeContaCod_Titleformat ;
      private short edtCaixa_Descricao_Titleformat ;
      private short edtCaixa_Valor_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutCaixa_Codigo ;
      private int wcpOAV7InOutCaixa_Codigo ;
      private int subGrid_Rows ;
      private int AV31TFCaixa_Codigo ;
      private int AV32TFCaixa_Codigo_To ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_caixa_codigo_Datalistupdateminimumcharacters ;
      private int Ddo_caixa_documento_Datalistupdateminimumcharacters ;
      private int Ddo_caixa_emissao_Datalistupdateminimumcharacters ;
      private int Ddo_caixa_vencimento_Datalistupdateminimumcharacters ;
      private int Ddo_caixa_tipodecontacod_Datalistupdateminimumcharacters ;
      private int Ddo_caixa_descricao_Datalistupdateminimumcharacters ;
      private int Ddo_caixa_valor_Datalistupdateminimumcharacters ;
      private int edtavTfcaixa_codigo_Visible ;
      private int edtavTfcaixa_codigo_to_Visible ;
      private int edtavTfcaixa_documento_Visible ;
      private int edtavTfcaixa_documento_sel_Visible ;
      private int edtavTfcaixa_emissao_Visible ;
      private int edtavTfcaixa_emissao_to_Visible ;
      private int edtavTfcaixa_vencimento_Visible ;
      private int edtavTfcaixa_vencimento_to_Visible ;
      private int edtavTfcaixa_tipodecontacod_Visible ;
      private int edtavTfcaixa_tipodecontacod_sel_Visible ;
      private int edtavTfcaixa_descricao_Visible ;
      private int edtavTfcaixa_descricao_sel_Visible ;
      private int edtavTfcaixa_valor_Visible ;
      private int edtavTfcaixa_valor_to_Visible ;
      private int edtavDdo_caixa_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_caixa_documentotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_caixa_emissaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_caixa_vencimentotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_caixa_tipodecontacodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_caixa_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_caixa_valortitlecontrolidtoreplace_Visible ;
      private int A874Caixa_Codigo ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV63PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavCaixa_documento1_Visible ;
      private int edtavCaixa_documento2_Visible ;
      private int edtavCaixa_documento3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV64GridCurrentPage ;
      private long AV65GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV59TFCaixa_Valor ;
      private decimal AV60TFCaixa_Valor_To ;
      private decimal A880Caixa_Valor ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_caixa_codigo_Activeeventkey ;
      private String Ddo_caixa_codigo_Filteredtext_get ;
      private String Ddo_caixa_codigo_Filteredtextto_get ;
      private String Ddo_caixa_documento_Activeeventkey ;
      private String Ddo_caixa_documento_Filteredtext_get ;
      private String Ddo_caixa_documento_Selectedvalue_get ;
      private String Ddo_caixa_emissao_Activeeventkey ;
      private String Ddo_caixa_emissao_Filteredtext_get ;
      private String Ddo_caixa_emissao_Filteredtextto_get ;
      private String Ddo_caixa_vencimento_Activeeventkey ;
      private String Ddo_caixa_vencimento_Filteredtext_get ;
      private String Ddo_caixa_vencimento_Filteredtextto_get ;
      private String Ddo_caixa_tipodecontacod_Activeeventkey ;
      private String Ddo_caixa_tipodecontacod_Filteredtext_get ;
      private String Ddo_caixa_tipodecontacod_Selectedvalue_get ;
      private String Ddo_caixa_descricao_Activeeventkey ;
      private String Ddo_caixa_descricao_Filteredtext_get ;
      private String Ddo_caixa_descricao_Selectedvalue_get ;
      private String Ddo_caixa_valor_Activeeventkey ;
      private String Ddo_caixa_valor_Filteredtext_get ;
      private String Ddo_caixa_valor_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String A870TipodeConta_Codigo ;
      private String sGXsfl_80_idx="0001" ;
      private String AV51TFCaixa_TipoDeContaCod ;
      private String AV52TFCaixa_TipoDeContaCod_Sel ;
      private String AV69Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_caixa_codigo_Caption ;
      private String Ddo_caixa_codigo_Tooltip ;
      private String Ddo_caixa_codigo_Cls ;
      private String Ddo_caixa_codigo_Filteredtext_set ;
      private String Ddo_caixa_codigo_Filteredtextto_set ;
      private String Ddo_caixa_codigo_Dropdownoptionstype ;
      private String Ddo_caixa_codigo_Titlecontrolidtoreplace ;
      private String Ddo_caixa_codigo_Sortedstatus ;
      private String Ddo_caixa_codigo_Filtertype ;
      private String Ddo_caixa_codigo_Datalistfixedvalues ;
      private String Ddo_caixa_codigo_Sortasc ;
      private String Ddo_caixa_codigo_Sortdsc ;
      private String Ddo_caixa_codigo_Loadingdata ;
      private String Ddo_caixa_codigo_Cleanfilter ;
      private String Ddo_caixa_codigo_Rangefilterfrom ;
      private String Ddo_caixa_codigo_Rangefilterto ;
      private String Ddo_caixa_codigo_Noresultsfound ;
      private String Ddo_caixa_codigo_Searchbuttontext ;
      private String Ddo_caixa_documento_Caption ;
      private String Ddo_caixa_documento_Tooltip ;
      private String Ddo_caixa_documento_Cls ;
      private String Ddo_caixa_documento_Filteredtext_set ;
      private String Ddo_caixa_documento_Selectedvalue_set ;
      private String Ddo_caixa_documento_Dropdownoptionstype ;
      private String Ddo_caixa_documento_Titlecontrolidtoreplace ;
      private String Ddo_caixa_documento_Sortedstatus ;
      private String Ddo_caixa_documento_Filtertype ;
      private String Ddo_caixa_documento_Datalisttype ;
      private String Ddo_caixa_documento_Datalistfixedvalues ;
      private String Ddo_caixa_documento_Datalistproc ;
      private String Ddo_caixa_documento_Sortasc ;
      private String Ddo_caixa_documento_Sortdsc ;
      private String Ddo_caixa_documento_Loadingdata ;
      private String Ddo_caixa_documento_Cleanfilter ;
      private String Ddo_caixa_documento_Rangefilterfrom ;
      private String Ddo_caixa_documento_Rangefilterto ;
      private String Ddo_caixa_documento_Noresultsfound ;
      private String Ddo_caixa_documento_Searchbuttontext ;
      private String Ddo_caixa_emissao_Caption ;
      private String Ddo_caixa_emissao_Tooltip ;
      private String Ddo_caixa_emissao_Cls ;
      private String Ddo_caixa_emissao_Filteredtext_set ;
      private String Ddo_caixa_emissao_Filteredtextto_set ;
      private String Ddo_caixa_emissao_Dropdownoptionstype ;
      private String Ddo_caixa_emissao_Titlecontrolidtoreplace ;
      private String Ddo_caixa_emissao_Sortedstatus ;
      private String Ddo_caixa_emissao_Filtertype ;
      private String Ddo_caixa_emissao_Datalistfixedvalues ;
      private String Ddo_caixa_emissao_Sortasc ;
      private String Ddo_caixa_emissao_Sortdsc ;
      private String Ddo_caixa_emissao_Loadingdata ;
      private String Ddo_caixa_emissao_Cleanfilter ;
      private String Ddo_caixa_emissao_Rangefilterfrom ;
      private String Ddo_caixa_emissao_Rangefilterto ;
      private String Ddo_caixa_emissao_Noresultsfound ;
      private String Ddo_caixa_emissao_Searchbuttontext ;
      private String Ddo_caixa_vencimento_Caption ;
      private String Ddo_caixa_vencimento_Tooltip ;
      private String Ddo_caixa_vencimento_Cls ;
      private String Ddo_caixa_vencimento_Filteredtext_set ;
      private String Ddo_caixa_vencimento_Filteredtextto_set ;
      private String Ddo_caixa_vencimento_Dropdownoptionstype ;
      private String Ddo_caixa_vencimento_Titlecontrolidtoreplace ;
      private String Ddo_caixa_vencimento_Sortedstatus ;
      private String Ddo_caixa_vencimento_Filtertype ;
      private String Ddo_caixa_vencimento_Datalistfixedvalues ;
      private String Ddo_caixa_vencimento_Sortasc ;
      private String Ddo_caixa_vencimento_Sortdsc ;
      private String Ddo_caixa_vencimento_Loadingdata ;
      private String Ddo_caixa_vencimento_Cleanfilter ;
      private String Ddo_caixa_vencimento_Rangefilterfrom ;
      private String Ddo_caixa_vencimento_Rangefilterto ;
      private String Ddo_caixa_vencimento_Noresultsfound ;
      private String Ddo_caixa_vencimento_Searchbuttontext ;
      private String Ddo_caixa_tipodecontacod_Caption ;
      private String Ddo_caixa_tipodecontacod_Tooltip ;
      private String Ddo_caixa_tipodecontacod_Cls ;
      private String Ddo_caixa_tipodecontacod_Filteredtext_set ;
      private String Ddo_caixa_tipodecontacod_Selectedvalue_set ;
      private String Ddo_caixa_tipodecontacod_Dropdownoptionstype ;
      private String Ddo_caixa_tipodecontacod_Titlecontrolidtoreplace ;
      private String Ddo_caixa_tipodecontacod_Sortedstatus ;
      private String Ddo_caixa_tipodecontacod_Filtertype ;
      private String Ddo_caixa_tipodecontacod_Datalisttype ;
      private String Ddo_caixa_tipodecontacod_Datalistfixedvalues ;
      private String Ddo_caixa_tipodecontacod_Datalistproc ;
      private String Ddo_caixa_tipodecontacod_Sortasc ;
      private String Ddo_caixa_tipodecontacod_Sortdsc ;
      private String Ddo_caixa_tipodecontacod_Loadingdata ;
      private String Ddo_caixa_tipodecontacod_Cleanfilter ;
      private String Ddo_caixa_tipodecontacod_Rangefilterfrom ;
      private String Ddo_caixa_tipodecontacod_Rangefilterto ;
      private String Ddo_caixa_tipodecontacod_Noresultsfound ;
      private String Ddo_caixa_tipodecontacod_Searchbuttontext ;
      private String Ddo_caixa_descricao_Caption ;
      private String Ddo_caixa_descricao_Tooltip ;
      private String Ddo_caixa_descricao_Cls ;
      private String Ddo_caixa_descricao_Filteredtext_set ;
      private String Ddo_caixa_descricao_Selectedvalue_set ;
      private String Ddo_caixa_descricao_Dropdownoptionstype ;
      private String Ddo_caixa_descricao_Titlecontrolidtoreplace ;
      private String Ddo_caixa_descricao_Sortedstatus ;
      private String Ddo_caixa_descricao_Filtertype ;
      private String Ddo_caixa_descricao_Datalisttype ;
      private String Ddo_caixa_descricao_Datalistfixedvalues ;
      private String Ddo_caixa_descricao_Datalistproc ;
      private String Ddo_caixa_descricao_Sortasc ;
      private String Ddo_caixa_descricao_Sortdsc ;
      private String Ddo_caixa_descricao_Loadingdata ;
      private String Ddo_caixa_descricao_Cleanfilter ;
      private String Ddo_caixa_descricao_Rangefilterfrom ;
      private String Ddo_caixa_descricao_Rangefilterto ;
      private String Ddo_caixa_descricao_Noresultsfound ;
      private String Ddo_caixa_descricao_Searchbuttontext ;
      private String Ddo_caixa_valor_Caption ;
      private String Ddo_caixa_valor_Tooltip ;
      private String Ddo_caixa_valor_Cls ;
      private String Ddo_caixa_valor_Filteredtext_set ;
      private String Ddo_caixa_valor_Filteredtextto_set ;
      private String Ddo_caixa_valor_Dropdownoptionstype ;
      private String Ddo_caixa_valor_Titlecontrolidtoreplace ;
      private String Ddo_caixa_valor_Sortedstatus ;
      private String Ddo_caixa_valor_Filtertype ;
      private String Ddo_caixa_valor_Datalistfixedvalues ;
      private String Ddo_caixa_valor_Sortasc ;
      private String Ddo_caixa_valor_Sortdsc ;
      private String Ddo_caixa_valor_Loadingdata ;
      private String Ddo_caixa_valor_Cleanfilter ;
      private String Ddo_caixa_valor_Rangefilterfrom ;
      private String Ddo_caixa_valor_Rangefilterto ;
      private String Ddo_caixa_valor_Noresultsfound ;
      private String Ddo_caixa_valor_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfcaixa_codigo_Internalname ;
      private String edtavTfcaixa_codigo_Jsonclick ;
      private String edtavTfcaixa_codigo_to_Internalname ;
      private String edtavTfcaixa_codigo_to_Jsonclick ;
      private String edtavTfcaixa_documento_Internalname ;
      private String edtavTfcaixa_documento_Jsonclick ;
      private String edtavTfcaixa_documento_sel_Internalname ;
      private String edtavTfcaixa_documento_sel_Jsonclick ;
      private String edtavTfcaixa_emissao_Internalname ;
      private String edtavTfcaixa_emissao_Jsonclick ;
      private String edtavTfcaixa_emissao_to_Internalname ;
      private String edtavTfcaixa_emissao_to_Jsonclick ;
      private String divDdo_caixa_emissaoauxdates_Internalname ;
      private String edtavDdo_caixa_emissaoauxdate_Internalname ;
      private String edtavDdo_caixa_emissaoauxdate_Jsonclick ;
      private String edtavDdo_caixa_emissaoauxdateto_Internalname ;
      private String edtavDdo_caixa_emissaoauxdateto_Jsonclick ;
      private String edtavTfcaixa_vencimento_Internalname ;
      private String edtavTfcaixa_vencimento_Jsonclick ;
      private String edtavTfcaixa_vencimento_to_Internalname ;
      private String edtavTfcaixa_vencimento_to_Jsonclick ;
      private String divDdo_caixa_vencimentoauxdates_Internalname ;
      private String edtavDdo_caixa_vencimentoauxdate_Internalname ;
      private String edtavDdo_caixa_vencimentoauxdate_Jsonclick ;
      private String edtavDdo_caixa_vencimentoauxdateto_Internalname ;
      private String edtavDdo_caixa_vencimentoauxdateto_Jsonclick ;
      private String edtavTfcaixa_tipodecontacod_Internalname ;
      private String edtavTfcaixa_tipodecontacod_Jsonclick ;
      private String edtavTfcaixa_tipodecontacod_sel_Internalname ;
      private String edtavTfcaixa_tipodecontacod_sel_Jsonclick ;
      private String edtavTfcaixa_descricao_Internalname ;
      private String edtavTfcaixa_descricao_Jsonclick ;
      private String edtavTfcaixa_descricao_sel_Internalname ;
      private String edtavTfcaixa_descricao_sel_Jsonclick ;
      private String edtavTfcaixa_valor_Internalname ;
      private String edtavTfcaixa_valor_Jsonclick ;
      private String edtavTfcaixa_valor_to_Internalname ;
      private String edtavTfcaixa_valor_to_Jsonclick ;
      private String edtavDdo_caixa_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_caixa_documentotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_caixa_emissaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_caixa_vencimentotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_caixa_tipodecontacodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_caixa_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_caixa_valortitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtCaixa_Codigo_Internalname ;
      private String edtCaixa_Documento_Internalname ;
      private String edtCaixa_Emissao_Internalname ;
      private String edtCaixa_Vencimento_Internalname ;
      private String A881Caixa_TipoDeContaCod ;
      private String edtCaixa_TipoDeContaCod_Internalname ;
      private String edtCaixa_Descricao_Internalname ;
      private String edtCaixa_Valor_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String l870TipodeConta_Codigo ;
      private String lV51TFCaixa_TipoDeContaCod ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavCaixa_documento1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavCaixa_documento2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavCaixa_documento3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_caixa_codigo_Internalname ;
      private String Ddo_caixa_documento_Internalname ;
      private String Ddo_caixa_emissao_Internalname ;
      private String Ddo_caixa_vencimento_Internalname ;
      private String Ddo_caixa_tipodecontacod_Internalname ;
      private String Ddo_caixa_descricao_Internalname ;
      private String Ddo_caixa_valor_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtCaixa_Codigo_Title ;
      private String edtCaixa_Documento_Title ;
      private String edtCaixa_Emissao_Title ;
      private String edtCaixa_Vencimento_Title ;
      private String edtCaixa_TipoDeContaCod_Title ;
      private String edtCaixa_Descricao_Title ;
      private String edtCaixa_Valor_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavCaixa_documento3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavCaixa_documento2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavCaixa_documento1_Jsonclick ;
      private String sGXsfl_80_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtCaixa_Codigo_Jsonclick ;
      private String edtCaixa_Documento_Jsonclick ;
      private String edtCaixa_Emissao_Jsonclick ;
      private String edtCaixa_Vencimento_Jsonclick ;
      private String edtCaixa_TipoDeContaCod_Jsonclick ;
      private String edtCaixa_Descricao_Jsonclick ;
      private String edtCaixa_Valor_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV39TFCaixa_Emissao ;
      private DateTime AV40TFCaixa_Emissao_To ;
      private DateTime AV45TFCaixa_Vencimento ;
      private DateTime AV46TFCaixa_Vencimento_To ;
      private DateTime AV41DDO_Caixa_EmissaoAuxDate ;
      private DateTime AV42DDO_Caixa_EmissaoAuxDateTo ;
      private DateTime AV47DDO_Caixa_VencimentoAuxDate ;
      private DateTime AV48DDO_Caixa_VencimentoAuxDateTo ;
      private DateTime A876Caixa_Emissao ;
      private DateTime A877Caixa_Vencimento ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_caixa_codigo_Includesortasc ;
      private bool Ddo_caixa_codigo_Includesortdsc ;
      private bool Ddo_caixa_codigo_Includefilter ;
      private bool Ddo_caixa_codigo_Filterisrange ;
      private bool Ddo_caixa_codigo_Includedatalist ;
      private bool Ddo_caixa_documento_Includesortasc ;
      private bool Ddo_caixa_documento_Includesortdsc ;
      private bool Ddo_caixa_documento_Includefilter ;
      private bool Ddo_caixa_documento_Filterisrange ;
      private bool Ddo_caixa_documento_Includedatalist ;
      private bool Ddo_caixa_emissao_Includesortasc ;
      private bool Ddo_caixa_emissao_Includesortdsc ;
      private bool Ddo_caixa_emissao_Includefilter ;
      private bool Ddo_caixa_emissao_Filterisrange ;
      private bool Ddo_caixa_emissao_Includedatalist ;
      private bool Ddo_caixa_vencimento_Includesortasc ;
      private bool Ddo_caixa_vencimento_Includesortdsc ;
      private bool Ddo_caixa_vencimento_Includefilter ;
      private bool Ddo_caixa_vencimento_Filterisrange ;
      private bool Ddo_caixa_vencimento_Includedatalist ;
      private bool Ddo_caixa_tipodecontacod_Includesortasc ;
      private bool Ddo_caixa_tipodecontacod_Includesortdsc ;
      private bool Ddo_caixa_tipodecontacod_Includefilter ;
      private bool Ddo_caixa_tipodecontacod_Filterisrange ;
      private bool Ddo_caixa_tipodecontacod_Includedatalist ;
      private bool Ddo_caixa_descricao_Includesortasc ;
      private bool Ddo_caixa_descricao_Includesortdsc ;
      private bool Ddo_caixa_descricao_Includefilter ;
      private bool Ddo_caixa_descricao_Filterisrange ;
      private bool Ddo_caixa_descricao_Includedatalist ;
      private bool Ddo_caixa_valor_Includesortasc ;
      private bool Ddo_caixa_valor_Includesortdsc ;
      private bool Ddo_caixa_valor_Includefilter ;
      private bool Ddo_caixa_valor_Filterisrange ;
      private bool Ddo_caixa_valor_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n876Caixa_Emissao ;
      private bool n877Caixa_Vencimento ;
      private bool n879Caixa_Descricao ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Select_IsBlob ;
      private String AV8InOutCaixa_Documento ;
      private String wcpOAV8InOutCaixa_Documento ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV17Caixa_Documento1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV21Caixa_Documento2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV25Caixa_Documento3 ;
      private String AV35TFCaixa_Documento ;
      private String AV36TFCaixa_Documento_Sel ;
      private String AV55TFCaixa_Descricao ;
      private String AV56TFCaixa_Descricao_Sel ;
      private String AV33ddo_Caixa_CodigoTitleControlIdToReplace ;
      private String AV37ddo_Caixa_DocumentoTitleControlIdToReplace ;
      private String AV43ddo_Caixa_EmissaoTitleControlIdToReplace ;
      private String AV49ddo_Caixa_VencimentoTitleControlIdToReplace ;
      private String AV53ddo_Caixa_TipoDeContaCodTitleControlIdToReplace ;
      private String AV57ddo_Caixa_DescricaoTitleControlIdToReplace ;
      private String AV61ddo_Caixa_ValorTitleControlIdToReplace ;
      private String AV68Select_GXI ;
      private String A875Caixa_Documento ;
      private String A879Caixa_Descricao ;
      private String lV17Caixa_Documento1 ;
      private String lV21Caixa_Documento2 ;
      private String lV25Caixa_Documento3 ;
      private String lV35TFCaixa_Documento ;
      private String lV55TFCaixa_Descricao ;
      private String AV28Select ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutCaixa_Codigo ;
      private String aP1_InOutCaixa_Documento ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H00FQ2_A870TipodeConta_Codigo ;
      private String[] H00FQ3_A870TipodeConta_Codigo ;
      private String[] H00FQ4_A870TipodeConta_Codigo ;
      private decimal[] H00FQ5_A880Caixa_Valor ;
      private String[] H00FQ5_A879Caixa_Descricao ;
      private bool[] H00FQ5_n879Caixa_Descricao ;
      private String[] H00FQ5_A881Caixa_TipoDeContaCod ;
      private DateTime[] H00FQ5_A877Caixa_Vencimento ;
      private bool[] H00FQ5_n877Caixa_Vencimento ;
      private DateTime[] H00FQ5_A876Caixa_Emissao ;
      private bool[] H00FQ5_n876Caixa_Emissao ;
      private String[] H00FQ5_A875Caixa_Documento ;
      private int[] H00FQ5_A874Caixa_Codigo ;
      private long[] H00FQ6_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV30Caixa_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV34Caixa_DocumentoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38Caixa_EmissaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV44Caixa_VencimentoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV50Caixa_TipoDeContaCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV54Caixa_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV58Caixa_ValorTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV62DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptcaixa__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00FQ5( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV17Caixa_Documento1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             short AV20DynamicFiltersOperator2 ,
                                             String AV21Caixa_Documento2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             short AV24DynamicFiltersOperator3 ,
                                             String AV25Caixa_Documento3 ,
                                             int AV31TFCaixa_Codigo ,
                                             int AV32TFCaixa_Codigo_To ,
                                             String AV36TFCaixa_Documento_Sel ,
                                             String AV35TFCaixa_Documento ,
                                             DateTime AV39TFCaixa_Emissao ,
                                             DateTime AV40TFCaixa_Emissao_To ,
                                             DateTime AV45TFCaixa_Vencimento ,
                                             DateTime AV46TFCaixa_Vencimento_To ,
                                             String AV52TFCaixa_TipoDeContaCod_Sel ,
                                             String AV51TFCaixa_TipoDeContaCod ,
                                             String AV56TFCaixa_Descricao_Sel ,
                                             String AV55TFCaixa_Descricao ,
                                             decimal AV59TFCaixa_Valor ,
                                             decimal AV60TFCaixa_Valor_To ,
                                             String A875Caixa_Documento ,
                                             int A874Caixa_Codigo ,
                                             DateTime A876Caixa_Emissao ,
                                             DateTime A877Caixa_Vencimento ,
                                             String A881Caixa_TipoDeContaCod ,
                                             String A879Caixa_Descricao ,
                                             decimal A880Caixa_Valor ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [25] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [Caixa_Valor], [Caixa_Descricao], [Caixa_TipoDeContaCod], [Caixa_Vencimento], [Caixa_Emissao], [Caixa_Documento], [Caixa_Codigo]";
         sFromString = " FROM [Caixa] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CAIXA_DOCUMENTO") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Caixa_Documento1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV17Caixa_Documento1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV17Caixa_Documento1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CAIXA_DOCUMENTO") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Caixa_Documento1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like '%' + @lV17Caixa_Documento1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like '%' + @lV17Caixa_Documento1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CAIXA_DOCUMENTO") == 0 ) && ( AV20DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21Caixa_Documento2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV21Caixa_Documento2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV21Caixa_Documento2)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CAIXA_DOCUMENTO") == 0 ) && ( AV20DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21Caixa_Documento2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like '%' + @lV21Caixa_Documento2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like '%' + @lV21Caixa_Documento2)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CAIXA_DOCUMENTO") == 0 ) && ( AV24DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Caixa_Documento3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV25Caixa_Documento3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV25Caixa_Documento3)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CAIXA_DOCUMENTO") == 0 ) && ( AV24DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Caixa_Documento3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like '%' + @lV25Caixa_Documento3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like '%' + @lV25Caixa_Documento3)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (0==AV31TFCaixa_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Codigo] >= @AV31TFCaixa_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Codigo] >= @AV31TFCaixa_Codigo)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! (0==AV32TFCaixa_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Codigo] <= @AV32TFCaixa_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Codigo] <= @AV32TFCaixa_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV36TFCaixa_Documento_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFCaixa_Documento)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV35TFCaixa_Documento)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV35TFCaixa_Documento)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFCaixa_Documento_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] = @AV36TFCaixa_Documento_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] = @AV36TFCaixa_Documento_Sel)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV39TFCaixa_Emissao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Emissao] >= @AV39TFCaixa_Emissao)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Emissao] >= @AV39TFCaixa_Emissao)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV40TFCaixa_Emissao_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Emissao] <= @AV40TFCaixa_Emissao_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Emissao] <= @AV40TFCaixa_Emissao_To)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV45TFCaixa_Vencimento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Vencimento] >= @AV45TFCaixa_Vencimento)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Vencimento] >= @AV45TFCaixa_Vencimento)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! (DateTime.MinValue==AV46TFCaixa_Vencimento_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Vencimento] <= @AV46TFCaixa_Vencimento_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Vencimento] <= @AV46TFCaixa_Vencimento_To)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV52TFCaixa_TipoDeContaCod_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51TFCaixa_TipoDeContaCod)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_TipoDeContaCod] like @lV51TFCaixa_TipoDeContaCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_TipoDeContaCod] like @lV51TFCaixa_TipoDeContaCod)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFCaixa_TipoDeContaCod_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_TipoDeContaCod] = @AV52TFCaixa_TipoDeContaCod_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_TipoDeContaCod] = @AV52TFCaixa_TipoDeContaCod_Sel)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV56TFCaixa_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55TFCaixa_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Descricao] like @lV55TFCaixa_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Descricao] like @lV55TFCaixa_Descricao)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFCaixa_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Descricao] = @AV56TFCaixa_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Descricao] = @AV56TFCaixa_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV59TFCaixa_Valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Valor] >= @AV59TFCaixa_Valor)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Valor] >= @AV59TFCaixa_Valor)";
            }
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV60TFCaixa_Valor_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Valor] <= @AV60TFCaixa_Valor_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Valor] <= @AV60TFCaixa_Valor_To)";
            }
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Caixa_Documento]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Caixa_Documento] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Caixa_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Caixa_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Caixa_Emissao]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Caixa_Emissao] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Caixa_Vencimento]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Caixa_Vencimento] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Caixa_TipoDeContaCod]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Caixa_TipoDeContaCod] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Caixa_Descricao]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Caixa_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Caixa_Valor]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Caixa_Valor] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [Caixa_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00FQ6( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV17Caixa_Documento1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             short AV20DynamicFiltersOperator2 ,
                                             String AV21Caixa_Documento2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             short AV24DynamicFiltersOperator3 ,
                                             String AV25Caixa_Documento3 ,
                                             int AV31TFCaixa_Codigo ,
                                             int AV32TFCaixa_Codigo_To ,
                                             String AV36TFCaixa_Documento_Sel ,
                                             String AV35TFCaixa_Documento ,
                                             DateTime AV39TFCaixa_Emissao ,
                                             DateTime AV40TFCaixa_Emissao_To ,
                                             DateTime AV45TFCaixa_Vencimento ,
                                             DateTime AV46TFCaixa_Vencimento_To ,
                                             String AV52TFCaixa_TipoDeContaCod_Sel ,
                                             String AV51TFCaixa_TipoDeContaCod ,
                                             String AV56TFCaixa_Descricao_Sel ,
                                             String AV55TFCaixa_Descricao ,
                                             decimal AV59TFCaixa_Valor ,
                                             decimal AV60TFCaixa_Valor_To ,
                                             String A875Caixa_Documento ,
                                             int A874Caixa_Codigo ,
                                             DateTime A876Caixa_Emissao ,
                                             DateTime A877Caixa_Vencimento ,
                                             String A881Caixa_TipoDeContaCod ,
                                             String A879Caixa_Descricao ,
                                             decimal A880Caixa_Valor ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [20] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [Caixa] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CAIXA_DOCUMENTO") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Caixa_Documento1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV17Caixa_Documento1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV17Caixa_Documento1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CAIXA_DOCUMENTO") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Caixa_Documento1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like '%' + @lV17Caixa_Documento1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like '%' + @lV17Caixa_Documento1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CAIXA_DOCUMENTO") == 0 ) && ( AV20DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21Caixa_Documento2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV21Caixa_Documento2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV21Caixa_Documento2)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CAIXA_DOCUMENTO") == 0 ) && ( AV20DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21Caixa_Documento2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like '%' + @lV21Caixa_Documento2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like '%' + @lV21Caixa_Documento2)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CAIXA_DOCUMENTO") == 0 ) && ( AV24DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Caixa_Documento3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV25Caixa_Documento3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV25Caixa_Documento3)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CAIXA_DOCUMENTO") == 0 ) && ( AV24DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Caixa_Documento3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like '%' + @lV25Caixa_Documento3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like '%' + @lV25Caixa_Documento3)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (0==AV31TFCaixa_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Codigo] >= @AV31TFCaixa_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Codigo] >= @AV31TFCaixa_Codigo)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! (0==AV32TFCaixa_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Codigo] <= @AV32TFCaixa_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Codigo] <= @AV32TFCaixa_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV36TFCaixa_Documento_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFCaixa_Documento)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV35TFCaixa_Documento)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV35TFCaixa_Documento)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFCaixa_Documento_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] = @AV36TFCaixa_Documento_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] = @AV36TFCaixa_Documento_Sel)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV39TFCaixa_Emissao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Emissao] >= @AV39TFCaixa_Emissao)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Emissao] >= @AV39TFCaixa_Emissao)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV40TFCaixa_Emissao_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Emissao] <= @AV40TFCaixa_Emissao_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Emissao] <= @AV40TFCaixa_Emissao_To)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV45TFCaixa_Vencimento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Vencimento] >= @AV45TFCaixa_Vencimento)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Vencimento] >= @AV45TFCaixa_Vencimento)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! (DateTime.MinValue==AV46TFCaixa_Vencimento_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Vencimento] <= @AV46TFCaixa_Vencimento_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Vencimento] <= @AV46TFCaixa_Vencimento_To)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV52TFCaixa_TipoDeContaCod_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51TFCaixa_TipoDeContaCod)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_TipoDeContaCod] like @lV51TFCaixa_TipoDeContaCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_TipoDeContaCod] like @lV51TFCaixa_TipoDeContaCod)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFCaixa_TipoDeContaCod_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_TipoDeContaCod] = @AV52TFCaixa_TipoDeContaCod_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_TipoDeContaCod] = @AV52TFCaixa_TipoDeContaCod_Sel)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV56TFCaixa_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55TFCaixa_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Descricao] like @lV55TFCaixa_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Descricao] like @lV55TFCaixa_Descricao)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFCaixa_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Descricao] = @AV56TFCaixa_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Descricao] = @AV56TFCaixa_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV59TFCaixa_Valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Valor] >= @AV59TFCaixa_Valor)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Valor] >= @AV59TFCaixa_Valor)";
            }
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV60TFCaixa_Valor_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Valor] <= @AV60TFCaixa_Valor_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Valor] <= @AV60TFCaixa_Valor_To)";
            }
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 3 :
                     return conditional_H00FQ5(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (decimal)dynConstraints[31] , (short)dynConstraints[32] , (bool)dynConstraints[33] );
               case 4 :
                     return conditional_H00FQ6(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (decimal)dynConstraints[31] , (short)dynConstraints[32] , (bool)dynConstraints[33] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00FQ2 ;
          prmH00FQ2 = new Object[] {
          new Object[] {"@l870TipodeConta_Codigo",SqlDbType.Char,20,0}
          } ;
          Object[] prmH00FQ3 ;
          prmH00FQ3 = new Object[] {
          new Object[] {"@l870TipodeConta_Codigo",SqlDbType.Char,20,0}
          } ;
          Object[] prmH00FQ4 ;
          prmH00FQ4 = new Object[] {
          new Object[] {"@l870TipodeConta_Codigo",SqlDbType.Char,20,0}
          } ;
          Object[] prmH00FQ5 ;
          prmH00FQ5 = new Object[] {
          new Object[] {"@lV17Caixa_Documento1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV17Caixa_Documento1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV21Caixa_Documento2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV21Caixa_Documento2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV25Caixa_Documento3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV25Caixa_Documento3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV31TFCaixa_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32TFCaixa_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV35TFCaixa_Documento",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV36TFCaixa_Documento_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV39TFCaixa_Emissao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV40TFCaixa_Emissao_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV45TFCaixa_Vencimento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV46TFCaixa_Vencimento_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV51TFCaixa_TipoDeContaCod",SqlDbType.Char,20,0} ,
          new Object[] {"@AV52TFCaixa_TipoDeContaCod_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV55TFCaixa_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV56TFCaixa_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV59TFCaixa_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV60TFCaixa_Valor_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00FQ6 ;
          prmH00FQ6 = new Object[] {
          new Object[] {"@lV17Caixa_Documento1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV17Caixa_Documento1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV21Caixa_Documento2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV21Caixa_Documento2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV25Caixa_Documento3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV25Caixa_Documento3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV31TFCaixa_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32TFCaixa_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV35TFCaixa_Documento",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV36TFCaixa_Documento_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV39TFCaixa_Emissao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV40TFCaixa_Emissao_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV45TFCaixa_Vencimento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV46TFCaixa_Vencimento_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV51TFCaixa_TipoDeContaCod",SqlDbType.Char,20,0} ,
          new Object[] {"@AV52TFCaixa_TipoDeContaCod_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV55TFCaixa_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV56TFCaixa_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV59TFCaixa_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV60TFCaixa_Valor_To",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00FQ2", "SELECT TOP 5 [TipodeConta_Codigo] FROM [TipodeConta] WITH (NOLOCK) WHERE UPPER([TipodeConta_Codigo]) like UPPER(@l870TipodeConta_Codigo) ORDER BY [TipodeConta_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FQ2,0,0,true,false )
             ,new CursorDef("H00FQ3", "SELECT TOP 5 [TipodeConta_Codigo] FROM [TipodeConta] WITH (NOLOCK) WHERE UPPER([TipodeConta_Codigo]) like UPPER(@l870TipodeConta_Codigo) ORDER BY [TipodeConta_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FQ3,0,0,true,false )
             ,new CursorDef("H00FQ4", "SELECT TOP 5 [TipodeConta_Codigo] FROM [TipodeConta] WITH (NOLOCK) WHERE UPPER([TipodeConta_Codigo]) like UPPER(@l870TipodeConta_Codigo) ORDER BY [TipodeConta_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FQ4,0,0,true,false )
             ,new CursorDef("H00FQ5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FQ5,11,0,true,false )
             ,new CursorDef("H00FQ6", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FQ6,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 3 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 20) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                return;
             case 4 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[38]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[39]);
                }
                return;
       }
    }

 }

}
