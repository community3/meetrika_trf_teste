/*
               File: PRC_ContratoDoPrestador
        Description: Contrato Do Prestador
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:45.27
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_contratodoprestador : GXProcedure
   {
      public prc_contratodoprestador( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_contratodoprestador( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Contratada_Codigo ,
                           out int aP1_Contrato_Codigo )
      {
         this.A39Contratada_Codigo = aP0_Contratada_Codigo;
         this.AV8Contrato_Codigo = 0 ;
         initialize();
         executePrivate();
         aP0_Contratada_Codigo=this.A39Contratada_Codigo;
         aP1_Contrato_Codigo=this.AV8Contrato_Codigo;
      }

      public int executeUdp( ref int aP0_Contratada_Codigo )
      {
         this.A39Contratada_Codigo = aP0_Contratada_Codigo;
         this.AV8Contrato_Codigo = 0 ;
         initialize();
         executePrivate();
         aP0_Contratada_Codigo=this.A39Contratada_Codigo;
         aP1_Contrato_Codigo=this.AV8Contrato_Codigo;
         return AV8Contrato_Codigo ;
      }

      public void executeSubmit( ref int aP0_Contratada_Codigo ,
                                 out int aP1_Contrato_Codigo )
      {
         prc_contratodoprestador objprc_contratodoprestador;
         objprc_contratodoprestador = new prc_contratodoprestador();
         objprc_contratodoprestador.A39Contratada_Codigo = aP0_Contratada_Codigo;
         objprc_contratodoprestador.AV8Contrato_Codigo = 0 ;
         objprc_contratodoprestador.context.SetSubmitInitialConfig(context);
         objprc_contratodoprestador.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_contratodoprestador);
         aP0_Contratada_Codigo=this.A39Contratada_Codigo;
         aP1_Contrato_Codigo=this.AV8Contrato_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_contratodoprestador)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00CL2 */
         pr_default.execute(0, new Object[] {A39Contratada_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A92Contrato_Ativo = P00CL2_A92Contrato_Ativo[0];
            A74Contrato_Codigo = P00CL2_A74Contrato_Codigo[0];
            AV9q = (short)(AV9q+1);
            AV8Contrato_Codigo = A74Contrato_Codigo;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( AV9q > 1 )
         {
            AV8Contrato_Codigo = 0;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00CL2_A39Contratada_Codigo = new int[1] ;
         P00CL2_A92Contrato_Ativo = new bool[] {false} ;
         P00CL2_A74Contrato_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_contratodoprestador__default(),
            new Object[][] {
                new Object[] {
               P00CL2_A39Contratada_Codigo, P00CL2_A92Contrato_Ativo, P00CL2_A74Contrato_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV9q ;
      private int A39Contratada_Codigo ;
      private int AV8Contrato_Codigo ;
      private int A74Contrato_Codigo ;
      private String scmdbuf ;
      private bool A92Contrato_Ativo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Contratada_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00CL2_A39Contratada_Codigo ;
      private bool[] P00CL2_A92Contrato_Ativo ;
      private int[] P00CL2_A74Contrato_Codigo ;
      private int aP1_Contrato_Codigo ;
   }

   public class prc_contratodoprestador__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00CL2 ;
          prmP00CL2 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00CL2", "SELECT [Contratada_Codigo], [Contrato_Ativo], [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE ([Contratada_Codigo] = @Contratada_Codigo) AND ([Contrato_Ativo] = 1) ORDER BY [Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00CL2,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
