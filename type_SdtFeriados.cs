/*
               File: type_SdtFeriados
        Description: Feriados
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:6:43.96
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Feriados" )]
   [XmlType(TypeName =  "Feriados" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtFeriados : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtFeriados( )
      {
         /* Constructor for serialization */
         gxTv_SdtFeriados_Feriado_data = DateTime.MinValue;
         gxTv_SdtFeriados_Feriado_nome = "";
         gxTv_SdtFeriados_Mode = "";
         gxTv_SdtFeriados_Feriado_data_Z = DateTime.MinValue;
         gxTv_SdtFeriados_Feriado_nome_Z = "";
      }

      public SdtFeriados( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( DateTime AV1175Feriado_Data )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(DateTime)AV1175Feriado_Data});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"Feriado_Data", typeof(DateTime)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Feriados");
         metadata.Set("BT", "Feriados");
         metadata.Set("PK", "[ \"Feriado_Data\" ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Feriado_data_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Feriado_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Feriado_fixo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Feriado_fixo_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtFeriados deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtFeriados)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtFeriados obj ;
         obj = this;
         obj.gxTpr_Feriado_data = deserialized.gxTpr_Feriado_data;
         obj.gxTpr_Feriado_nome = deserialized.gxTpr_Feriado_nome;
         obj.gxTpr_Feriado_fixo = deserialized.gxTpr_Feriado_fixo;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Feriado_data_Z = deserialized.gxTpr_Feriado_data_Z;
         obj.gxTpr_Feriado_nome_Z = deserialized.gxTpr_Feriado_nome_Z;
         obj.gxTpr_Feriado_fixo_Z = deserialized.gxTpr_Feriado_fixo_Z;
         obj.gxTpr_Feriado_fixo_N = deserialized.gxTpr_Feriado_fixo_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Feriado_Data") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtFeriados_Feriado_data = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtFeriados_Feriado_data = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Feriado_Nome") )
               {
                  gxTv_SdtFeriados_Feriado_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Feriado_Fixo") )
               {
                  gxTv_SdtFeriados_Feriado_fixo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtFeriados_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtFeriados_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Feriado_Data_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtFeriados_Feriado_data_Z = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtFeriados_Feriado_data_Z = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Feriado_Nome_Z") )
               {
                  gxTv_SdtFeriados_Feriado_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Feriado_Fixo_Z") )
               {
                  gxTv_SdtFeriados_Feriado_fixo_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Feriado_Fixo_N") )
               {
                  gxTv_SdtFeriados_Feriado_fixo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "Feriados";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         if ( (DateTime.MinValue==gxTv_SdtFeriados_Feriado_data) )
         {
            oWriter.WriteStartElement("Feriado_Data");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtFeriados_Feriado_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtFeriados_Feriado_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtFeriados_Feriado_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("Feriado_Data", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteElement("Feriado_Nome", StringUtil.RTrim( gxTv_SdtFeriados_Feriado_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Feriado_Fixo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtFeriados_Feriado_fixo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtFeriados_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFeriados_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            if ( (DateTime.MinValue==gxTv_SdtFeriados_Feriado_data_Z) )
            {
               oWriter.WriteStartElement("Feriado_Data_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtFeriados_Feriado_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtFeriados_Feriado_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtFeriados_Feriado_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("Feriado_Data_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
               }
            }
            oWriter.WriteElement("Feriado_Nome_Z", StringUtil.RTrim( gxTv_SdtFeriados_Feriado_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Feriado_Fixo_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtFeriados_Feriado_fixo_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Feriado_Fixo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFeriados_Feriado_fixo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtFeriados_Feriado_data)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtFeriados_Feriado_data)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtFeriados_Feriado_data)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("Feriado_Data", sDateCnv, false);
         AddObjectProperty("Feriado_Nome", gxTv_SdtFeriados_Feriado_nome, false);
         AddObjectProperty("Feriado_Fixo", gxTv_SdtFeriados_Feriado_fixo, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtFeriados_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtFeriados_Initialized, false);
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtFeriados_Feriado_data_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtFeriados_Feriado_data_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtFeriados_Feriado_data_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("Feriado_Data_Z", sDateCnv, false);
            AddObjectProperty("Feriado_Nome_Z", gxTv_SdtFeriados_Feriado_nome_Z, false);
            AddObjectProperty("Feriado_Fixo_Z", gxTv_SdtFeriados_Feriado_fixo_Z, false);
            AddObjectProperty("Feriado_Fixo_N", gxTv_SdtFeriados_Feriado_fixo_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Feriado_Data" )]
      [  XmlElement( ElementName = "Feriado_Data"  , IsNullable=true )]
      public string gxTpr_Feriado_data_Nullable
      {
         get {
            if ( gxTv_SdtFeriados_Feriado_data == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtFeriados_Feriado_data).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtFeriados_Feriado_data = DateTime.MinValue;
            else
               gxTv_SdtFeriados_Feriado_data = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Feriado_data
      {
         get {
            return gxTv_SdtFeriados_Feriado_data ;
         }

         set {
            if ( gxTv_SdtFeriados_Feriado_data != value )
            {
               gxTv_SdtFeriados_Mode = "INS";
               this.gxTv_SdtFeriados_Feriado_data_Z_SetNull( );
               this.gxTv_SdtFeriados_Feriado_nome_Z_SetNull( );
               this.gxTv_SdtFeriados_Feriado_fixo_Z_SetNull( );
            }
            gxTv_SdtFeriados_Feriado_data = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "Feriado_Nome" )]
      [  XmlElement( ElementName = "Feriado_Nome"   )]
      public String gxTpr_Feriado_nome
      {
         get {
            return gxTv_SdtFeriados_Feriado_nome ;
         }

         set {
            gxTv_SdtFeriados_Feriado_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Feriado_Fixo" )]
      [  XmlElement( ElementName = "Feriado_Fixo"   )]
      public bool gxTpr_Feriado_fixo
      {
         get {
            return gxTv_SdtFeriados_Feriado_fixo ;
         }

         set {
            gxTv_SdtFeriados_Feriado_fixo_N = 0;
            gxTv_SdtFeriados_Feriado_fixo = value;
         }

      }

      public void gxTv_SdtFeriados_Feriado_fixo_SetNull( )
      {
         gxTv_SdtFeriados_Feriado_fixo_N = 1;
         gxTv_SdtFeriados_Feriado_fixo = false;
         return  ;
      }

      public bool gxTv_SdtFeriados_Feriado_fixo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtFeriados_Mode ;
         }

         set {
            gxTv_SdtFeriados_Mode = (String)(value);
         }

      }

      public void gxTv_SdtFeriados_Mode_SetNull( )
      {
         gxTv_SdtFeriados_Mode = "";
         return  ;
      }

      public bool gxTv_SdtFeriados_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtFeriados_Initialized ;
         }

         set {
            gxTv_SdtFeriados_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtFeriados_Initialized_SetNull( )
      {
         gxTv_SdtFeriados_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtFeriados_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Feriado_Data_Z" )]
      [  XmlElement( ElementName = "Feriado_Data_Z"  , IsNullable=true )]
      public string gxTpr_Feriado_data_Z_Nullable
      {
         get {
            if ( gxTv_SdtFeriados_Feriado_data_Z == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtFeriados_Feriado_data_Z).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtFeriados_Feriado_data_Z = DateTime.MinValue;
            else
               gxTv_SdtFeriados_Feriado_data_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Feriado_data_Z
      {
         get {
            return gxTv_SdtFeriados_Feriado_data_Z ;
         }

         set {
            gxTv_SdtFeriados_Feriado_data_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtFeriados_Feriado_data_Z_SetNull( )
      {
         gxTv_SdtFeriados_Feriado_data_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtFeriados_Feriado_data_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Feriado_Nome_Z" )]
      [  XmlElement( ElementName = "Feriado_Nome_Z"   )]
      public String gxTpr_Feriado_nome_Z
      {
         get {
            return gxTv_SdtFeriados_Feriado_nome_Z ;
         }

         set {
            gxTv_SdtFeriados_Feriado_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtFeriados_Feriado_nome_Z_SetNull( )
      {
         gxTv_SdtFeriados_Feriado_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtFeriados_Feriado_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Feriado_Fixo_Z" )]
      [  XmlElement( ElementName = "Feriado_Fixo_Z"   )]
      public bool gxTpr_Feriado_fixo_Z
      {
         get {
            return gxTv_SdtFeriados_Feriado_fixo_Z ;
         }

         set {
            gxTv_SdtFeriados_Feriado_fixo_Z = value;
         }

      }

      public void gxTv_SdtFeriados_Feriado_fixo_Z_SetNull( )
      {
         gxTv_SdtFeriados_Feriado_fixo_Z = false;
         return  ;
      }

      public bool gxTv_SdtFeriados_Feriado_fixo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Feriado_Fixo_N" )]
      [  XmlElement( ElementName = "Feriado_Fixo_N"   )]
      public short gxTpr_Feriado_fixo_N
      {
         get {
            return gxTv_SdtFeriados_Feriado_fixo_N ;
         }

         set {
            gxTv_SdtFeriados_Feriado_fixo_N = (short)(value);
         }

      }

      public void gxTv_SdtFeriados_Feriado_fixo_N_SetNull( )
      {
         gxTv_SdtFeriados_Feriado_fixo_N = 0;
         return  ;
      }

      public bool gxTv_SdtFeriados_Feriado_fixo_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtFeriados_Feriado_data = DateTime.MinValue;
         gxTv_SdtFeriados_Feriado_nome = "";
         gxTv_SdtFeriados_Mode = "";
         gxTv_SdtFeriados_Feriado_data_Z = DateTime.MinValue;
         gxTv_SdtFeriados_Feriado_nome_Z = "";
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "feriados", "GeneXus.Programs.feriados_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtFeriados_Initialized ;
      private short gxTv_SdtFeriados_Feriado_fixo_N ;
      private short readOk ;
      private short nOutParmCount ;
      private String gxTv_SdtFeriados_Feriado_nome ;
      private String gxTv_SdtFeriados_Mode ;
      private String gxTv_SdtFeriados_Feriado_nome_Z ;
      private String sTagName ;
      private String sDateCnv ;
      private String sNumToPad ;
      private DateTime gxTv_SdtFeriados_Feriado_data ;
      private DateTime gxTv_SdtFeriados_Feriado_data_Z ;
      private bool gxTv_SdtFeriados_Feriado_fixo ;
      private bool gxTv_SdtFeriados_Feriado_fixo_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"Feriados", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtFeriados_RESTInterface : GxGenericCollectionItem<SdtFeriados>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtFeriados_RESTInterface( ) : base()
      {
      }

      public SdtFeriados_RESTInterface( SdtFeriados psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Feriado_Data" , Order = 0 )]
      [GxSeudo()]
      public String gxTpr_Feriado_data
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Feriado_data) ;
         }

         set {
            sdt.gxTpr_Feriado_data = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "Feriado_Nome" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Feriado_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Feriado_nome) ;
         }

         set {
            sdt.gxTpr_Feriado_nome = (String)(value);
         }

      }

      [DataMember( Name = "Feriado_Fixo" , Order = 2 )]
      [GxSeudo()]
      public bool gxTpr_Feriado_fixo
      {
         get {
            return sdt.gxTpr_Feriado_fixo ;
         }

         set {
            sdt.gxTpr_Feriado_fixo = value;
         }

      }

      public SdtFeriados sdt
      {
         get {
            return (SdtFeriados)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtFeriados() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 9 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
