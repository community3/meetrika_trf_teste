/*
               File: PRC_TemCheckList
        Description: Tem Check List
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:54:55.46
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_temchecklist : GXProcedure
   {
      public prc_temchecklist( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_temchecklist( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Check_AreaTrabalhoCod ,
                           ref short aP1_Check_Momento ,
                           out bool aP2_Tem )
      {
         this.A1840Check_AreaTrabalhoCod = aP0_Check_AreaTrabalhoCod;
         this.A1843Check_Momento = aP1_Check_Momento;
         this.AV8Tem = false ;
         initialize();
         executePrivate();
         aP0_Check_AreaTrabalhoCod=this.A1840Check_AreaTrabalhoCod;
         aP1_Check_Momento=this.A1843Check_Momento;
         aP2_Tem=this.AV8Tem;
      }

      public bool executeUdp( ref int aP0_Check_AreaTrabalhoCod ,
                              ref short aP1_Check_Momento )
      {
         this.A1840Check_AreaTrabalhoCod = aP0_Check_AreaTrabalhoCod;
         this.A1843Check_Momento = aP1_Check_Momento;
         this.AV8Tem = false ;
         initialize();
         executePrivate();
         aP0_Check_AreaTrabalhoCod=this.A1840Check_AreaTrabalhoCod;
         aP1_Check_Momento=this.A1843Check_Momento;
         aP2_Tem=this.AV8Tem;
         return AV8Tem ;
      }

      public void executeSubmit( ref int aP0_Check_AreaTrabalhoCod ,
                                 ref short aP1_Check_Momento ,
                                 out bool aP2_Tem )
      {
         prc_temchecklist objprc_temchecklist;
         objprc_temchecklist = new prc_temchecklist();
         objprc_temchecklist.A1840Check_AreaTrabalhoCod = aP0_Check_AreaTrabalhoCod;
         objprc_temchecklist.A1843Check_Momento = aP1_Check_Momento;
         objprc_temchecklist.AV8Tem = false ;
         objprc_temchecklist.context.SetSubmitInitialConfig(context);
         objprc_temchecklist.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_temchecklist);
         aP0_Check_AreaTrabalhoCod=this.A1840Check_AreaTrabalhoCod;
         aP1_Check_Momento=this.A1843Check_Momento;
         aP2_Tem=this.AV8Tem;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_temchecklist)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00T52 */
         pr_default.execute(0, new Object[] {A1840Check_AreaTrabalhoCod, A1843Check_Momento});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1839Check_Codigo = P00T52_A1839Check_Codigo[0];
            AV8Tem = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00T52_A1840Check_AreaTrabalhoCod = new int[1] ;
         P00T52_A1843Check_Momento = new short[1] ;
         P00T52_A1839Check_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_temchecklist__default(),
            new Object[][] {
                new Object[] {
               P00T52_A1840Check_AreaTrabalhoCod, P00T52_A1843Check_Momento, P00T52_A1839Check_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A1843Check_Momento ;
      private int A1840Check_AreaTrabalhoCod ;
      private int A1839Check_Codigo ;
      private String scmdbuf ;
      private bool AV8Tem ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Check_AreaTrabalhoCod ;
      private short aP1_Check_Momento ;
      private IDataStoreProvider pr_default ;
      private int[] P00T52_A1840Check_AreaTrabalhoCod ;
      private short[] P00T52_A1843Check_Momento ;
      private int[] P00T52_A1839Check_Codigo ;
      private bool aP2_Tem ;
   }

   public class prc_temchecklist__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00T52 ;
          prmP00T52 = new Object[] {
          new Object[] {"@Check_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Check_Momento",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00T52", "SELECT TOP 1 [Check_AreaTrabalhoCod], [Check_Momento], [Check_Codigo] FROM [Check] WITH (NOLOCK) WHERE ([Check_AreaTrabalhoCod] = @Check_AreaTrabalhoCod) AND ([Check_Momento] = @Check_Momento) ORDER BY [Check_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00T52,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
       }
    }

 }

}
