/*
               File: PRC_AlteraStatusDmn
        Description: Altera Status Dmn
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:36.93
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_alterastatusdmn : GXProcedure
   {
      public prc_alterastatusdmn( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_alterastatusdmn( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo ,
                           String aP1_Status ,
                           int aP2_UserId )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV8Status = aP1_Status;
         this.AV9UserId = aP2_UserId;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo ,
                                 String aP1_Status ,
                                 int aP2_UserId )
      {
         prc_alterastatusdmn objprc_alterastatusdmn;
         objprc_alterastatusdmn = new prc_alterastatusdmn();
         objprc_alterastatusdmn.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_alterastatusdmn.AV8Status = aP1_Status;
         objprc_alterastatusdmn.AV9UserId = aP2_UserId;
         objprc_alterastatusdmn.context.SetSubmitInitialConfig(context);
         objprc_alterastatusdmn.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_alterastatusdmn);
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_alterastatusdmn)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P008U2 */
         pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A490ContagemResultado_ContratadaCod = P008U2_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P008U2_n490ContagemResultado_ContratadaCod[0];
            A484ContagemResultado_StatusDmn = P008U2_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P008U2_n484ContagemResultado_StatusDmn[0];
            A890ContagemResultado_Responsavel = P008U2_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P008U2_n890ContagemResultado_Responsavel[0];
            A1349ContagemResultado_DataExecucao = P008U2_A1349ContagemResultado_DataExecucao[0];
            n1349ContagemResultado_DataExecucao = P008U2_n1349ContagemResultado_DataExecucao[0];
            A1326ContagemResultado_ContratadaTipoFab = P008U2_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = P008U2_n1326ContagemResultado_ContratadaTipoFab[0];
            A1762ContagemResultado_Entrega = P008U2_A1762ContagemResultado_Entrega[0];
            n1762ContagemResultado_Entrega = P008U2_n1762ContagemResultado_Entrega[0];
            A602ContagemResultado_OSVinculada = P008U2_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P008U2_n602ContagemResultado_OSVinculada[0];
            A1326ContagemResultado_ContratadaTipoFab = P008U2_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = P008U2_n1326ContagemResultado_ContratadaTipoFab[0];
            A484ContagemResultado_StatusDmn = AV8Status;
            n484ContagemResultado_StatusDmn = false;
            if ( AV9UserId > 0 )
            {
               A890ContagemResultado_Responsavel = AV9UserId;
               n890ContagemResultado_Responsavel = false;
            }
            if ( StringUtil.StrCmp(AV8Status, "R") == 0 )
            {
               A1349ContagemResultado_DataExecucao = DateTimeUtil.ServerNow( context, "DEFAULT");
               n1349ContagemResultado_DataExecucao = false;
               if ( StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "S") == 0 )
               {
                  A1762ContagemResultado_Entrega = (short)(A1762ContagemResultado_Entrega+1);
                  n1762ContagemResultado_Entrega = false;
               }
            }
            AV10OSVinculada = A602ContagemResultado_OSVinculada;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P008U3 */
            pr_default.execute(1, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, n1349ContagemResultado_DataExecucao, A1349ContagemResultado_DataExecucao, n1762ContagemResultado_Entrega, A1762ContagemResultado_Entrega, A456ContagemResultado_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            if (true) break;
            /* Using cursor P008U4 */
            pr_default.execute(2, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, n1349ContagemResultado_DataExecucao, A1349ContagemResultado_DataExecucao, n1762ContagemResultado_Entrega, A1762ContagemResultado_Entrega, A456ContagemResultado_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         if ( ( AV10OSVinculada > 0 ) && ( StringUtil.StrCmp(AV8Status, "R") == 0 ) )
         {
            new prc_upddpnhmlg(context ).execute( ref  AV10OSVinculada) ;
            new prc_concluirssosorigem(context ).execute(  A456ContagemResultado_Codigo,  AV9UserId) ;
         }
         context.CommitDataStores( "PRC_AlteraStatusDmn");
         new prc_disparoservicovinculado(context ).execute(  A456ContagemResultado_Codigo,  AV9UserId) ;
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P008U2_A490ContagemResultado_ContratadaCod = new int[1] ;
         P008U2_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P008U2_A456ContagemResultado_Codigo = new int[1] ;
         P008U2_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P008U2_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P008U2_A890ContagemResultado_Responsavel = new int[1] ;
         P008U2_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P008U2_A1349ContagemResultado_DataExecucao = new DateTime[] {DateTime.MinValue} ;
         P008U2_n1349ContagemResultado_DataExecucao = new bool[] {false} ;
         P008U2_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         P008U2_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         P008U2_A1762ContagemResultado_Entrega = new short[1] ;
         P008U2_n1762ContagemResultado_Entrega = new bool[] {false} ;
         P008U2_A602ContagemResultado_OSVinculada = new int[1] ;
         P008U2_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         A484ContagemResultado_StatusDmn = "";
         A1349ContagemResultado_DataExecucao = (DateTime)(DateTime.MinValue);
         A1326ContagemResultado_ContratadaTipoFab = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_alterastatusdmn__default(),
            new Object[][] {
                new Object[] {
               P008U2_A490ContagemResultado_ContratadaCod, P008U2_n490ContagemResultado_ContratadaCod, P008U2_A456ContagemResultado_Codigo, P008U2_A484ContagemResultado_StatusDmn, P008U2_n484ContagemResultado_StatusDmn, P008U2_A890ContagemResultado_Responsavel, P008U2_n890ContagemResultado_Responsavel, P008U2_A1349ContagemResultado_DataExecucao, P008U2_n1349ContagemResultado_DataExecucao, P008U2_A1326ContagemResultado_ContratadaTipoFab,
               P008U2_n1326ContagemResultado_ContratadaTipoFab, P008U2_A1762ContagemResultado_Entrega, P008U2_n1762ContagemResultado_Entrega, P008U2_A602ContagemResultado_OSVinculada, P008U2_n602ContagemResultado_OSVinculada
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A1762ContagemResultado_Entrega ;
      private int A456ContagemResultado_Codigo ;
      private int AV9UserId ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A890ContagemResultado_Responsavel ;
      private int A602ContagemResultado_OSVinculada ;
      private int AV10OSVinculada ;
      private String AV8Status ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private String A1326ContagemResultado_ContratadaTipoFab ;
      private DateTime A1349ContagemResultado_DataExecucao ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n1349ContagemResultado_DataExecucao ;
      private bool n1326ContagemResultado_ContratadaTipoFab ;
      private bool n1762ContagemResultado_Entrega ;
      private bool n602ContagemResultado_OSVinculada ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P008U2_A490ContagemResultado_ContratadaCod ;
      private bool[] P008U2_n490ContagemResultado_ContratadaCod ;
      private int[] P008U2_A456ContagemResultado_Codigo ;
      private String[] P008U2_A484ContagemResultado_StatusDmn ;
      private bool[] P008U2_n484ContagemResultado_StatusDmn ;
      private int[] P008U2_A890ContagemResultado_Responsavel ;
      private bool[] P008U2_n890ContagemResultado_Responsavel ;
      private DateTime[] P008U2_A1349ContagemResultado_DataExecucao ;
      private bool[] P008U2_n1349ContagemResultado_DataExecucao ;
      private String[] P008U2_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] P008U2_n1326ContagemResultado_ContratadaTipoFab ;
      private short[] P008U2_A1762ContagemResultado_Entrega ;
      private bool[] P008U2_n1762ContagemResultado_Entrega ;
      private int[] P008U2_A602ContagemResultado_OSVinculada ;
      private bool[] P008U2_n602ContagemResultado_OSVinculada ;
   }

   public class prc_alterastatusdmn__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP008U2 ;
          prmP008U2 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008U3 ;
          prmP008U3 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataExecucao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Entrega",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008U4 ;
          prmP008U4 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataExecucao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Entrega",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P008U2", "SELECT TOP 1 T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_Responsavel], T1.[ContagemResultado_DataExecucao], T2.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, T1.[ContagemResultado_Entrega], T1.[ContagemResultado_OSVinculada] FROM ([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) WHERE T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008U2,1,0,true,true )
             ,new CursorDef("P008U3", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel, [ContagemResultado_DataExecucao]=@ContagemResultado_DataExecucao, [ContagemResultado_Entrega]=@ContagemResultado_Entrega  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008U3)
             ,new CursorDef("P008U4", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel, [ContagemResultado_DataExecucao]=@ContagemResultado_DataExecucao, [ContagemResultado_Entrega]=@ContagemResultado_Entrega  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008U4)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((short[]) buf[11])[0] = rslt.getShort(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(3, (DateTime)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[7]);
                }
                stmt.SetParameter(5, (int)parms[8]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(3, (DateTime)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[7]);
                }
                stmt.SetParameter(5, (int)parms[8]);
                return;
       }
    }

 }

}
