/*
               File: ContratoServicosPrioridade_BC
        Description: Prioridade
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:31:27.19
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicosprioridade_bc : GXHttpHandler, IGxSilentTrn
   {
      public contratoservicosprioridade_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratoservicosprioridade_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow3L162( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey3L162( ) ;
         standaloneModal( ) ;
         AddRow3L162( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E113L2 */
            E113L2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z1336ContratoServicosPrioridade_Codigo = A1336ContratoServicosPrioridade_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_3L0( )
      {
         BeforeValidate3L162( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls3L162( ) ;
            }
            else
            {
               CheckExtendedTable3L162( ) ;
               if ( AnyError == 0 )
               {
                  ZM3L162( 6) ;
               }
               CloseExtendedTableCursors3L162( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E123L2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV13Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV14GXV1 = 1;
            while ( AV14GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV14GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "ContratoServicosPrioridade_CntSrvCod") == 0 )
               {
                  AV11Insert_ContratoServicosPrioridade_CntSrvCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               AV14GXV1 = (int)(AV14GXV1+1);
            }
         }
      }

      protected void E113L2( )
      {
         /* After Trn Routine */
      }

      protected void ZM3L162( short GX_JID )
      {
         if ( ( GX_JID == 5 ) || ( GX_JID == 0 ) )
         {
            Z2066ContratoServicosPrioridade_Ordem = A2066ContratoServicosPrioridade_Ordem;
            Z1337ContratoServicosPrioridade_Nome = A1337ContratoServicosPrioridade_Nome;
            Z1338ContratoServicosPrioridade_PercValorB = A1338ContratoServicosPrioridade_PercValorB;
            Z1339ContratoServicosPrioridade_PercPrazo = A1339ContratoServicosPrioridade_PercPrazo;
            Z2067ContratoServicosPrioridade_Peso = A2067ContratoServicosPrioridade_Peso;
            Z1335ContratoServicosPrioridade_CntSrvCod = A1335ContratoServicosPrioridade_CntSrvCod;
         }
         if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
         {
         }
         if ( GX_JID == -5 )
         {
            Z1336ContratoServicosPrioridade_Codigo = A1336ContratoServicosPrioridade_Codigo;
            Z2066ContratoServicosPrioridade_Ordem = A2066ContratoServicosPrioridade_Ordem;
            Z1337ContratoServicosPrioridade_Nome = A1337ContratoServicosPrioridade_Nome;
            Z1359ContratoServicosPrioridade_Finalidade = A1359ContratoServicosPrioridade_Finalidade;
            Z1338ContratoServicosPrioridade_PercValorB = A1338ContratoServicosPrioridade_PercValorB;
            Z1339ContratoServicosPrioridade_PercPrazo = A1339ContratoServicosPrioridade_PercPrazo;
            Z2067ContratoServicosPrioridade_Peso = A2067ContratoServicosPrioridade_Peso;
            Z1335ContratoServicosPrioridade_CntSrvCod = A1335ContratoServicosPrioridade_CntSrvCod;
         }
      }

      protected void standaloneNotModal( )
      {
         AV13Pgmname = "ContratoServicosPrioridade_BC";
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            GXt_int1 = A2066ContratoServicosPrioridade_Ordem;
            new prc_maxprioridadeordem(context ).execute( ref  AV11Insert_ContratoServicosPrioridade_CntSrvCod, out  GXt_int1) ;
            A2066ContratoServicosPrioridade_Ordem = (short)(GXt_int1+1);
            n2066ContratoServicosPrioridade_Ordem = false;
         }
      }

      protected void Load3L162( )
      {
         /* Using cursor BC003L5 */
         pr_default.execute(3, new Object[] {A1336ContratoServicosPrioridade_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound162 = 1;
            A2066ContratoServicosPrioridade_Ordem = BC003L5_A2066ContratoServicosPrioridade_Ordem[0];
            n2066ContratoServicosPrioridade_Ordem = BC003L5_n2066ContratoServicosPrioridade_Ordem[0];
            A1337ContratoServicosPrioridade_Nome = BC003L5_A1337ContratoServicosPrioridade_Nome[0];
            A1359ContratoServicosPrioridade_Finalidade = BC003L5_A1359ContratoServicosPrioridade_Finalidade[0];
            n1359ContratoServicosPrioridade_Finalidade = BC003L5_n1359ContratoServicosPrioridade_Finalidade[0];
            A1338ContratoServicosPrioridade_PercValorB = BC003L5_A1338ContratoServicosPrioridade_PercValorB[0];
            n1338ContratoServicosPrioridade_PercValorB = BC003L5_n1338ContratoServicosPrioridade_PercValorB[0];
            A1339ContratoServicosPrioridade_PercPrazo = BC003L5_A1339ContratoServicosPrioridade_PercPrazo[0];
            n1339ContratoServicosPrioridade_PercPrazo = BC003L5_n1339ContratoServicosPrioridade_PercPrazo[0];
            A2067ContratoServicosPrioridade_Peso = BC003L5_A2067ContratoServicosPrioridade_Peso[0];
            n2067ContratoServicosPrioridade_Peso = BC003L5_n2067ContratoServicosPrioridade_Peso[0];
            A1335ContratoServicosPrioridade_CntSrvCod = BC003L5_A1335ContratoServicosPrioridade_CntSrvCod[0];
            ZM3L162( -5) ;
         }
         pr_default.close(3);
         OnLoadActions3L162( ) ;
      }

      protected void OnLoadActions3L162( )
      {
      }

      protected void CheckExtendedTable3L162( )
      {
         standaloneModal( ) ;
         /* Using cursor BC003L4 */
         pr_default.execute(2, new Object[] {A1335ContratoServicosPrioridade_CntSrvCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Prioridades_Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSPRIORIDADE_CNTSRVCOD");
            AnyError = 1;
         }
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors3L162( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey3L162( )
      {
         /* Using cursor BC003L6 */
         pr_default.execute(4, new Object[] {A1336ContratoServicosPrioridade_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound162 = 1;
         }
         else
         {
            RcdFound162 = 0;
         }
         pr_default.close(4);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC003L3 */
         pr_default.execute(1, new Object[] {A1336ContratoServicosPrioridade_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3L162( 5) ;
            RcdFound162 = 1;
            A1336ContratoServicosPrioridade_Codigo = BC003L3_A1336ContratoServicosPrioridade_Codigo[0];
            A2066ContratoServicosPrioridade_Ordem = BC003L3_A2066ContratoServicosPrioridade_Ordem[0];
            n2066ContratoServicosPrioridade_Ordem = BC003L3_n2066ContratoServicosPrioridade_Ordem[0];
            A1337ContratoServicosPrioridade_Nome = BC003L3_A1337ContratoServicosPrioridade_Nome[0];
            A1359ContratoServicosPrioridade_Finalidade = BC003L3_A1359ContratoServicosPrioridade_Finalidade[0];
            n1359ContratoServicosPrioridade_Finalidade = BC003L3_n1359ContratoServicosPrioridade_Finalidade[0];
            A1338ContratoServicosPrioridade_PercValorB = BC003L3_A1338ContratoServicosPrioridade_PercValorB[0];
            n1338ContratoServicosPrioridade_PercValorB = BC003L3_n1338ContratoServicosPrioridade_PercValorB[0];
            A1339ContratoServicosPrioridade_PercPrazo = BC003L3_A1339ContratoServicosPrioridade_PercPrazo[0];
            n1339ContratoServicosPrioridade_PercPrazo = BC003L3_n1339ContratoServicosPrioridade_PercPrazo[0];
            A2067ContratoServicosPrioridade_Peso = BC003L3_A2067ContratoServicosPrioridade_Peso[0];
            n2067ContratoServicosPrioridade_Peso = BC003L3_n2067ContratoServicosPrioridade_Peso[0];
            A1335ContratoServicosPrioridade_CntSrvCod = BC003L3_A1335ContratoServicosPrioridade_CntSrvCod[0];
            Z1336ContratoServicosPrioridade_Codigo = A1336ContratoServicosPrioridade_Codigo;
            sMode162 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load3L162( ) ;
            if ( AnyError == 1 )
            {
               RcdFound162 = 0;
               InitializeNonKey3L162( ) ;
            }
            Gx_mode = sMode162;
         }
         else
         {
            RcdFound162 = 0;
            InitializeNonKey3L162( ) ;
            sMode162 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode162;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey3L162( ) ;
         if ( RcdFound162 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_3L0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency3L162( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC003L2 */
            pr_default.execute(0, new Object[] {A1336ContratoServicosPrioridade_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosPrioridade"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z2066ContratoServicosPrioridade_Ordem != BC003L2_A2066ContratoServicosPrioridade_Ordem[0] ) || ( StringUtil.StrCmp(Z1337ContratoServicosPrioridade_Nome, BC003L2_A1337ContratoServicosPrioridade_Nome[0]) != 0 ) || ( Z1338ContratoServicosPrioridade_PercValorB != BC003L2_A1338ContratoServicosPrioridade_PercValorB[0] ) || ( Z1339ContratoServicosPrioridade_PercPrazo != BC003L2_A1339ContratoServicosPrioridade_PercPrazo[0] ) || ( Z2067ContratoServicosPrioridade_Peso != BC003L2_A2067ContratoServicosPrioridade_Peso[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1335ContratoServicosPrioridade_CntSrvCod != BC003L2_A1335ContratoServicosPrioridade_CntSrvCod[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoServicosPrioridade"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3L162( )
      {
         BeforeValidate3L162( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3L162( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3L162( 0) ;
            CheckOptimisticConcurrency3L162( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3L162( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3L162( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC003L7 */
                     pr_default.execute(5, new Object[] {n2066ContratoServicosPrioridade_Ordem, A2066ContratoServicosPrioridade_Ordem, A1337ContratoServicosPrioridade_Nome, n1359ContratoServicosPrioridade_Finalidade, A1359ContratoServicosPrioridade_Finalidade, n1338ContratoServicosPrioridade_PercValorB, A1338ContratoServicosPrioridade_PercValorB, n1339ContratoServicosPrioridade_PercPrazo, A1339ContratoServicosPrioridade_PercPrazo, n2067ContratoServicosPrioridade_Peso, A2067ContratoServicosPrioridade_Peso, A1335ContratoServicosPrioridade_CntSrvCod});
                     A1336ContratoServicosPrioridade_Codigo = BC003L7_A1336ContratoServicosPrioridade_Codigo[0];
                     pr_default.close(5);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosPrioridade") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3L162( ) ;
            }
            EndLevel3L162( ) ;
         }
         CloseExtendedTableCursors3L162( ) ;
      }

      protected void Update3L162( )
      {
         BeforeValidate3L162( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3L162( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3L162( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3L162( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3L162( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC003L8 */
                     pr_default.execute(6, new Object[] {n2066ContratoServicosPrioridade_Ordem, A2066ContratoServicosPrioridade_Ordem, A1337ContratoServicosPrioridade_Nome, n1359ContratoServicosPrioridade_Finalidade, A1359ContratoServicosPrioridade_Finalidade, n1338ContratoServicosPrioridade_PercValorB, A1338ContratoServicosPrioridade_PercValorB, n1339ContratoServicosPrioridade_PercPrazo, A1339ContratoServicosPrioridade_PercPrazo, n2067ContratoServicosPrioridade_Peso, A2067ContratoServicosPrioridade_Peso, A1335ContratoServicosPrioridade_CntSrvCod, A1336ContratoServicosPrioridade_Codigo});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosPrioridade") ;
                     if ( (pr_default.getStatus(6) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosPrioridade"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3L162( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3L162( ) ;
         }
         CloseExtendedTableCursors3L162( ) ;
      }

      protected void DeferredUpdate3L162( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate3L162( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3L162( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3L162( ) ;
            AfterConfirm3L162( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3L162( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC003L9 */
                  pr_default.execute(7, new Object[] {A1336ContratoServicosPrioridade_Codigo});
                  pr_default.close(7);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosPrioridade") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode162 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel3L162( ) ;
         Gx_mode = sMode162;
      }

      protected void OnDeleteControls3L162( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel3L162( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3L162( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart3L162( )
      {
         /* Scan By routine */
         /* Using cursor BC003L10 */
         pr_default.execute(8, new Object[] {A1336ContratoServicosPrioridade_Codigo});
         RcdFound162 = 0;
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound162 = 1;
            A1336ContratoServicosPrioridade_Codigo = BC003L10_A1336ContratoServicosPrioridade_Codigo[0];
            A2066ContratoServicosPrioridade_Ordem = BC003L10_A2066ContratoServicosPrioridade_Ordem[0];
            n2066ContratoServicosPrioridade_Ordem = BC003L10_n2066ContratoServicosPrioridade_Ordem[0];
            A1337ContratoServicosPrioridade_Nome = BC003L10_A1337ContratoServicosPrioridade_Nome[0];
            A1359ContratoServicosPrioridade_Finalidade = BC003L10_A1359ContratoServicosPrioridade_Finalidade[0];
            n1359ContratoServicosPrioridade_Finalidade = BC003L10_n1359ContratoServicosPrioridade_Finalidade[0];
            A1338ContratoServicosPrioridade_PercValorB = BC003L10_A1338ContratoServicosPrioridade_PercValorB[0];
            n1338ContratoServicosPrioridade_PercValorB = BC003L10_n1338ContratoServicosPrioridade_PercValorB[0];
            A1339ContratoServicosPrioridade_PercPrazo = BC003L10_A1339ContratoServicosPrioridade_PercPrazo[0];
            n1339ContratoServicosPrioridade_PercPrazo = BC003L10_n1339ContratoServicosPrioridade_PercPrazo[0];
            A2067ContratoServicosPrioridade_Peso = BC003L10_A2067ContratoServicosPrioridade_Peso[0];
            n2067ContratoServicosPrioridade_Peso = BC003L10_n2067ContratoServicosPrioridade_Peso[0];
            A1335ContratoServicosPrioridade_CntSrvCod = BC003L10_A1335ContratoServicosPrioridade_CntSrvCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext3L162( )
      {
         /* Scan next routine */
         pr_default.readNext(8);
         RcdFound162 = 0;
         ScanKeyLoad3L162( ) ;
      }

      protected void ScanKeyLoad3L162( )
      {
         sMode162 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound162 = 1;
            A1336ContratoServicosPrioridade_Codigo = BC003L10_A1336ContratoServicosPrioridade_Codigo[0];
            A2066ContratoServicosPrioridade_Ordem = BC003L10_A2066ContratoServicosPrioridade_Ordem[0];
            n2066ContratoServicosPrioridade_Ordem = BC003L10_n2066ContratoServicosPrioridade_Ordem[0];
            A1337ContratoServicosPrioridade_Nome = BC003L10_A1337ContratoServicosPrioridade_Nome[0];
            A1359ContratoServicosPrioridade_Finalidade = BC003L10_A1359ContratoServicosPrioridade_Finalidade[0];
            n1359ContratoServicosPrioridade_Finalidade = BC003L10_n1359ContratoServicosPrioridade_Finalidade[0];
            A1338ContratoServicosPrioridade_PercValorB = BC003L10_A1338ContratoServicosPrioridade_PercValorB[0];
            n1338ContratoServicosPrioridade_PercValorB = BC003L10_n1338ContratoServicosPrioridade_PercValorB[0];
            A1339ContratoServicosPrioridade_PercPrazo = BC003L10_A1339ContratoServicosPrioridade_PercPrazo[0];
            n1339ContratoServicosPrioridade_PercPrazo = BC003L10_n1339ContratoServicosPrioridade_PercPrazo[0];
            A2067ContratoServicosPrioridade_Peso = BC003L10_A2067ContratoServicosPrioridade_Peso[0];
            n2067ContratoServicosPrioridade_Peso = BC003L10_n2067ContratoServicosPrioridade_Peso[0];
            A1335ContratoServicosPrioridade_CntSrvCod = BC003L10_A1335ContratoServicosPrioridade_CntSrvCod[0];
         }
         Gx_mode = sMode162;
      }

      protected void ScanKeyEnd3L162( )
      {
         pr_default.close(8);
      }

      protected void AfterConfirm3L162( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3L162( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3L162( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3L162( )
      {
         /* Before Delete Rules */
         new prc_dltcntsrvprioridade(context ).execute(  A1335ContratoServicosPrioridade_CntSrvCod,  A2066ContratoServicosPrioridade_Ordem) ;
      }

      protected void BeforeComplete3L162( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3L162( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3L162( )
      {
      }

      protected void AddRow3L162( )
      {
         VarsToRow162( bcContratoServicosPrioridade) ;
      }

      protected void ReadRow3L162( )
      {
         RowToVars162( bcContratoServicosPrioridade, 1) ;
      }

      protected void InitializeNonKey3L162( )
      {
         A2066ContratoServicosPrioridade_Ordem = 0;
         n2066ContratoServicosPrioridade_Ordem = false;
         A1335ContratoServicosPrioridade_CntSrvCod = 0;
         A1337ContratoServicosPrioridade_Nome = "";
         A1359ContratoServicosPrioridade_Finalidade = "";
         n1359ContratoServicosPrioridade_Finalidade = false;
         A1338ContratoServicosPrioridade_PercValorB = 0;
         n1338ContratoServicosPrioridade_PercValorB = false;
         A1339ContratoServicosPrioridade_PercPrazo = 0;
         n1339ContratoServicosPrioridade_PercPrazo = false;
         A2067ContratoServicosPrioridade_Peso = 0;
         n2067ContratoServicosPrioridade_Peso = false;
         Z2066ContratoServicosPrioridade_Ordem = 0;
         Z1337ContratoServicosPrioridade_Nome = "";
         Z1338ContratoServicosPrioridade_PercValorB = 0;
         Z1339ContratoServicosPrioridade_PercPrazo = 0;
         Z2067ContratoServicosPrioridade_Peso = 0;
         Z1335ContratoServicosPrioridade_CntSrvCod = 0;
      }

      protected void InitAll3L162( )
      {
         A1336ContratoServicosPrioridade_Codigo = 0;
         InitializeNonKey3L162( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A2066ContratoServicosPrioridade_Ordem = i2066ContratoServicosPrioridade_Ordem;
         n2066ContratoServicosPrioridade_Ordem = false;
      }

      public void VarsToRow162( SdtContratoServicosPrioridade obj162 )
      {
         obj162.gxTpr_Mode = Gx_mode;
         obj162.gxTpr_Contratoservicosprioridade_ordem = A2066ContratoServicosPrioridade_Ordem;
         obj162.gxTpr_Contratoservicosprioridade_cntsrvcod = A1335ContratoServicosPrioridade_CntSrvCod;
         obj162.gxTpr_Contratoservicosprioridade_nome = A1337ContratoServicosPrioridade_Nome;
         obj162.gxTpr_Contratoservicosprioridade_finalidade = A1359ContratoServicosPrioridade_Finalidade;
         obj162.gxTpr_Contratoservicosprioridade_percvalorb = A1338ContratoServicosPrioridade_PercValorB;
         obj162.gxTpr_Contratoservicosprioridade_percprazo = A1339ContratoServicosPrioridade_PercPrazo;
         obj162.gxTpr_Contratoservicosprioridade_peso = A2067ContratoServicosPrioridade_Peso;
         obj162.gxTpr_Contratoservicosprioridade_codigo = A1336ContratoServicosPrioridade_Codigo;
         obj162.gxTpr_Contratoservicosprioridade_codigo_Z = Z1336ContratoServicosPrioridade_Codigo;
         obj162.gxTpr_Contratoservicosprioridade_cntsrvcod_Z = Z1335ContratoServicosPrioridade_CntSrvCod;
         obj162.gxTpr_Contratoservicosprioridade_nome_Z = Z1337ContratoServicosPrioridade_Nome;
         obj162.gxTpr_Contratoservicosprioridade_percvalorb_Z = Z1338ContratoServicosPrioridade_PercValorB;
         obj162.gxTpr_Contratoservicosprioridade_percprazo_Z = Z1339ContratoServicosPrioridade_PercPrazo;
         obj162.gxTpr_Contratoservicosprioridade_ordem_Z = Z2066ContratoServicosPrioridade_Ordem;
         obj162.gxTpr_Contratoservicosprioridade_peso_Z = Z2067ContratoServicosPrioridade_Peso;
         obj162.gxTpr_Contratoservicosprioridade_finalidade_N = (short)(Convert.ToInt16(n1359ContratoServicosPrioridade_Finalidade));
         obj162.gxTpr_Contratoservicosprioridade_percvalorb_N = (short)(Convert.ToInt16(n1338ContratoServicosPrioridade_PercValorB));
         obj162.gxTpr_Contratoservicosprioridade_percprazo_N = (short)(Convert.ToInt16(n1339ContratoServicosPrioridade_PercPrazo));
         obj162.gxTpr_Contratoservicosprioridade_ordem_N = (short)(Convert.ToInt16(n2066ContratoServicosPrioridade_Ordem));
         obj162.gxTpr_Contratoservicosprioridade_peso_N = (short)(Convert.ToInt16(n2067ContratoServicosPrioridade_Peso));
         obj162.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow162( SdtContratoServicosPrioridade obj162 )
      {
         obj162.gxTpr_Contratoservicosprioridade_codigo = A1336ContratoServicosPrioridade_Codigo;
         return  ;
      }

      public void RowToVars162( SdtContratoServicosPrioridade obj162 ,
                                int forceLoad )
      {
         Gx_mode = obj162.gxTpr_Mode;
         A2066ContratoServicosPrioridade_Ordem = obj162.gxTpr_Contratoservicosprioridade_ordem;
         n2066ContratoServicosPrioridade_Ordem = false;
         A1335ContratoServicosPrioridade_CntSrvCod = obj162.gxTpr_Contratoservicosprioridade_cntsrvcod;
         A1337ContratoServicosPrioridade_Nome = obj162.gxTpr_Contratoservicosprioridade_nome;
         A1359ContratoServicosPrioridade_Finalidade = obj162.gxTpr_Contratoservicosprioridade_finalidade;
         n1359ContratoServicosPrioridade_Finalidade = false;
         A1338ContratoServicosPrioridade_PercValorB = obj162.gxTpr_Contratoservicosprioridade_percvalorb;
         n1338ContratoServicosPrioridade_PercValorB = false;
         A1339ContratoServicosPrioridade_PercPrazo = obj162.gxTpr_Contratoservicosprioridade_percprazo;
         n1339ContratoServicosPrioridade_PercPrazo = false;
         A2067ContratoServicosPrioridade_Peso = obj162.gxTpr_Contratoservicosprioridade_peso;
         n2067ContratoServicosPrioridade_Peso = false;
         A1336ContratoServicosPrioridade_Codigo = obj162.gxTpr_Contratoservicosprioridade_codigo;
         Z1336ContratoServicosPrioridade_Codigo = obj162.gxTpr_Contratoservicosprioridade_codigo_Z;
         Z1335ContratoServicosPrioridade_CntSrvCod = obj162.gxTpr_Contratoservicosprioridade_cntsrvcod_Z;
         Z1337ContratoServicosPrioridade_Nome = obj162.gxTpr_Contratoservicosprioridade_nome_Z;
         Z1338ContratoServicosPrioridade_PercValorB = obj162.gxTpr_Contratoservicosprioridade_percvalorb_Z;
         Z1339ContratoServicosPrioridade_PercPrazo = obj162.gxTpr_Contratoservicosprioridade_percprazo_Z;
         Z2066ContratoServicosPrioridade_Ordem = obj162.gxTpr_Contratoservicosprioridade_ordem_Z;
         Z2067ContratoServicosPrioridade_Peso = obj162.gxTpr_Contratoservicosprioridade_peso_Z;
         n1359ContratoServicosPrioridade_Finalidade = (bool)(Convert.ToBoolean(obj162.gxTpr_Contratoservicosprioridade_finalidade_N));
         n1338ContratoServicosPrioridade_PercValorB = (bool)(Convert.ToBoolean(obj162.gxTpr_Contratoservicosprioridade_percvalorb_N));
         n1339ContratoServicosPrioridade_PercPrazo = (bool)(Convert.ToBoolean(obj162.gxTpr_Contratoservicosprioridade_percprazo_N));
         n2066ContratoServicosPrioridade_Ordem = (bool)(Convert.ToBoolean(obj162.gxTpr_Contratoservicosprioridade_ordem_N));
         n2067ContratoServicosPrioridade_Peso = (bool)(Convert.ToBoolean(obj162.gxTpr_Contratoservicosprioridade_peso_N));
         Gx_mode = obj162.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A1336ContratoServicosPrioridade_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey3L162( ) ;
         ScanKeyStart3L162( ) ;
         if ( RcdFound162 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1336ContratoServicosPrioridade_Codigo = A1336ContratoServicosPrioridade_Codigo;
         }
         ZM3L162( -5) ;
         OnLoadActions3L162( ) ;
         AddRow3L162( ) ;
         ScanKeyEnd3L162( ) ;
         if ( RcdFound162 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars162( bcContratoServicosPrioridade, 0) ;
         ScanKeyStart3L162( ) ;
         if ( RcdFound162 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1336ContratoServicosPrioridade_Codigo = A1336ContratoServicosPrioridade_Codigo;
         }
         ZM3L162( -5) ;
         OnLoadActions3L162( ) ;
         AddRow3L162( ) ;
         ScanKeyEnd3L162( ) ;
         if ( RcdFound162 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars162( bcContratoServicosPrioridade, 0) ;
         nKeyPressed = 1;
         GetKey3L162( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert3L162( ) ;
         }
         else
         {
            if ( RcdFound162 == 1 )
            {
               if ( A1336ContratoServicosPrioridade_Codigo != Z1336ContratoServicosPrioridade_Codigo )
               {
                  A1336ContratoServicosPrioridade_Codigo = Z1336ContratoServicosPrioridade_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update3L162( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A1336ContratoServicosPrioridade_Codigo != Z1336ContratoServicosPrioridade_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert3L162( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert3L162( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow162( bcContratoServicosPrioridade) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars162( bcContratoServicosPrioridade, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey3L162( ) ;
         if ( RcdFound162 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A1336ContratoServicosPrioridade_Codigo != Z1336ContratoServicosPrioridade_Codigo )
            {
               A1336ContratoServicosPrioridade_Codigo = Z1336ContratoServicosPrioridade_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A1336ContratoServicosPrioridade_Codigo != Z1336ContratoServicosPrioridade_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         context.RollbackDataStores( "ContratoServicosPrioridade_BC");
         VarsToRow162( bcContratoServicosPrioridade) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcContratoServicosPrioridade.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcContratoServicosPrioridade.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcContratoServicosPrioridade )
         {
            bcContratoServicosPrioridade = (SdtContratoServicosPrioridade)(sdt);
            if ( StringUtil.StrCmp(bcContratoServicosPrioridade.gxTpr_Mode, "") == 0 )
            {
               bcContratoServicosPrioridade.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow162( bcContratoServicosPrioridade) ;
            }
            else
            {
               RowToVars162( bcContratoServicosPrioridade, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcContratoServicosPrioridade.gxTpr_Mode, "") == 0 )
            {
               bcContratoServicosPrioridade.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars162( bcContratoServicosPrioridade, 1) ;
         return  ;
      }

      public SdtContratoServicosPrioridade ContratoServicosPrioridade_BC
      {
         get {
            return bcContratoServicosPrioridade ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV13Pgmname = "";
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z1337ContratoServicosPrioridade_Nome = "";
         A1337ContratoServicosPrioridade_Nome = "";
         Z1359ContratoServicosPrioridade_Finalidade = "";
         A1359ContratoServicosPrioridade_Finalidade = "";
         BC003L5_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         BC003L5_A2066ContratoServicosPrioridade_Ordem = new short[1] ;
         BC003L5_n2066ContratoServicosPrioridade_Ordem = new bool[] {false} ;
         BC003L5_A1337ContratoServicosPrioridade_Nome = new String[] {""} ;
         BC003L5_A1359ContratoServicosPrioridade_Finalidade = new String[] {""} ;
         BC003L5_n1359ContratoServicosPrioridade_Finalidade = new bool[] {false} ;
         BC003L5_A1338ContratoServicosPrioridade_PercValorB = new decimal[1] ;
         BC003L5_n1338ContratoServicosPrioridade_PercValorB = new bool[] {false} ;
         BC003L5_A1339ContratoServicosPrioridade_PercPrazo = new decimal[1] ;
         BC003L5_n1339ContratoServicosPrioridade_PercPrazo = new bool[] {false} ;
         BC003L5_A2067ContratoServicosPrioridade_Peso = new short[1] ;
         BC003L5_n2067ContratoServicosPrioridade_Peso = new bool[] {false} ;
         BC003L5_A1335ContratoServicosPrioridade_CntSrvCod = new int[1] ;
         BC003L4_A1335ContratoServicosPrioridade_CntSrvCod = new int[1] ;
         BC003L6_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         BC003L3_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         BC003L3_A2066ContratoServicosPrioridade_Ordem = new short[1] ;
         BC003L3_n2066ContratoServicosPrioridade_Ordem = new bool[] {false} ;
         BC003L3_A1337ContratoServicosPrioridade_Nome = new String[] {""} ;
         BC003L3_A1359ContratoServicosPrioridade_Finalidade = new String[] {""} ;
         BC003L3_n1359ContratoServicosPrioridade_Finalidade = new bool[] {false} ;
         BC003L3_A1338ContratoServicosPrioridade_PercValorB = new decimal[1] ;
         BC003L3_n1338ContratoServicosPrioridade_PercValorB = new bool[] {false} ;
         BC003L3_A1339ContratoServicosPrioridade_PercPrazo = new decimal[1] ;
         BC003L3_n1339ContratoServicosPrioridade_PercPrazo = new bool[] {false} ;
         BC003L3_A2067ContratoServicosPrioridade_Peso = new short[1] ;
         BC003L3_n2067ContratoServicosPrioridade_Peso = new bool[] {false} ;
         BC003L3_A1335ContratoServicosPrioridade_CntSrvCod = new int[1] ;
         sMode162 = "";
         BC003L2_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         BC003L2_A2066ContratoServicosPrioridade_Ordem = new short[1] ;
         BC003L2_n2066ContratoServicosPrioridade_Ordem = new bool[] {false} ;
         BC003L2_A1337ContratoServicosPrioridade_Nome = new String[] {""} ;
         BC003L2_A1359ContratoServicosPrioridade_Finalidade = new String[] {""} ;
         BC003L2_n1359ContratoServicosPrioridade_Finalidade = new bool[] {false} ;
         BC003L2_A1338ContratoServicosPrioridade_PercValorB = new decimal[1] ;
         BC003L2_n1338ContratoServicosPrioridade_PercValorB = new bool[] {false} ;
         BC003L2_A1339ContratoServicosPrioridade_PercPrazo = new decimal[1] ;
         BC003L2_n1339ContratoServicosPrioridade_PercPrazo = new bool[] {false} ;
         BC003L2_A2067ContratoServicosPrioridade_Peso = new short[1] ;
         BC003L2_n2067ContratoServicosPrioridade_Peso = new bool[] {false} ;
         BC003L2_A1335ContratoServicosPrioridade_CntSrvCod = new int[1] ;
         BC003L7_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         BC003L10_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         BC003L10_A2066ContratoServicosPrioridade_Ordem = new short[1] ;
         BC003L10_n2066ContratoServicosPrioridade_Ordem = new bool[] {false} ;
         BC003L10_A1337ContratoServicosPrioridade_Nome = new String[] {""} ;
         BC003L10_A1359ContratoServicosPrioridade_Finalidade = new String[] {""} ;
         BC003L10_n1359ContratoServicosPrioridade_Finalidade = new bool[] {false} ;
         BC003L10_A1338ContratoServicosPrioridade_PercValorB = new decimal[1] ;
         BC003L10_n1338ContratoServicosPrioridade_PercValorB = new bool[] {false} ;
         BC003L10_A1339ContratoServicosPrioridade_PercPrazo = new decimal[1] ;
         BC003L10_n1339ContratoServicosPrioridade_PercPrazo = new bool[] {false} ;
         BC003L10_A2067ContratoServicosPrioridade_Peso = new short[1] ;
         BC003L10_n2067ContratoServicosPrioridade_Peso = new bool[] {false} ;
         BC003L10_A1335ContratoServicosPrioridade_CntSrvCod = new int[1] ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicosprioridade_bc__default(),
            new Object[][] {
                new Object[] {
               BC003L2_A1336ContratoServicosPrioridade_Codigo, BC003L2_A2066ContratoServicosPrioridade_Ordem, BC003L2_n2066ContratoServicosPrioridade_Ordem, BC003L2_A1337ContratoServicosPrioridade_Nome, BC003L2_A1359ContratoServicosPrioridade_Finalidade, BC003L2_n1359ContratoServicosPrioridade_Finalidade, BC003L2_A1338ContratoServicosPrioridade_PercValorB, BC003L2_n1338ContratoServicosPrioridade_PercValorB, BC003L2_A1339ContratoServicosPrioridade_PercPrazo, BC003L2_n1339ContratoServicosPrioridade_PercPrazo,
               BC003L2_A2067ContratoServicosPrioridade_Peso, BC003L2_n2067ContratoServicosPrioridade_Peso, BC003L2_A1335ContratoServicosPrioridade_CntSrvCod
               }
               , new Object[] {
               BC003L3_A1336ContratoServicosPrioridade_Codigo, BC003L3_A2066ContratoServicosPrioridade_Ordem, BC003L3_n2066ContratoServicosPrioridade_Ordem, BC003L3_A1337ContratoServicosPrioridade_Nome, BC003L3_A1359ContratoServicosPrioridade_Finalidade, BC003L3_n1359ContratoServicosPrioridade_Finalidade, BC003L3_A1338ContratoServicosPrioridade_PercValorB, BC003L3_n1338ContratoServicosPrioridade_PercValorB, BC003L3_A1339ContratoServicosPrioridade_PercPrazo, BC003L3_n1339ContratoServicosPrioridade_PercPrazo,
               BC003L3_A2067ContratoServicosPrioridade_Peso, BC003L3_n2067ContratoServicosPrioridade_Peso, BC003L3_A1335ContratoServicosPrioridade_CntSrvCod
               }
               , new Object[] {
               BC003L4_A1335ContratoServicosPrioridade_CntSrvCod
               }
               , new Object[] {
               BC003L5_A1336ContratoServicosPrioridade_Codigo, BC003L5_A2066ContratoServicosPrioridade_Ordem, BC003L5_n2066ContratoServicosPrioridade_Ordem, BC003L5_A1337ContratoServicosPrioridade_Nome, BC003L5_A1359ContratoServicosPrioridade_Finalidade, BC003L5_n1359ContratoServicosPrioridade_Finalidade, BC003L5_A1338ContratoServicosPrioridade_PercValorB, BC003L5_n1338ContratoServicosPrioridade_PercValorB, BC003L5_A1339ContratoServicosPrioridade_PercPrazo, BC003L5_n1339ContratoServicosPrioridade_PercPrazo,
               BC003L5_A2067ContratoServicosPrioridade_Peso, BC003L5_n2067ContratoServicosPrioridade_Peso, BC003L5_A1335ContratoServicosPrioridade_CntSrvCod
               }
               , new Object[] {
               BC003L6_A1336ContratoServicosPrioridade_Codigo
               }
               , new Object[] {
               BC003L7_A1336ContratoServicosPrioridade_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC003L10_A1336ContratoServicosPrioridade_Codigo, BC003L10_A2066ContratoServicosPrioridade_Ordem, BC003L10_n2066ContratoServicosPrioridade_Ordem, BC003L10_A1337ContratoServicosPrioridade_Nome, BC003L10_A1359ContratoServicosPrioridade_Finalidade, BC003L10_n1359ContratoServicosPrioridade_Finalidade, BC003L10_A1338ContratoServicosPrioridade_PercValorB, BC003L10_n1338ContratoServicosPrioridade_PercValorB, BC003L10_A1339ContratoServicosPrioridade_PercPrazo, BC003L10_n1339ContratoServicosPrioridade_PercPrazo,
               BC003L10_A2067ContratoServicosPrioridade_Peso, BC003L10_n2067ContratoServicosPrioridade_Peso, BC003L10_A1335ContratoServicosPrioridade_CntSrvCod
               }
            }
         );
         AV13Pgmname = "ContratoServicosPrioridade_BC";
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E123L2 */
         E123L2 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short Z2066ContratoServicosPrioridade_Ordem ;
      private short A2066ContratoServicosPrioridade_Ordem ;
      private short Z2067ContratoServicosPrioridade_Peso ;
      private short A2067ContratoServicosPrioridade_Peso ;
      private short GXt_int1 ;
      private short RcdFound162 ;
      private short i2066ContratoServicosPrioridade_Ordem ;
      private int trnEnded ;
      private int Z1336ContratoServicosPrioridade_Codigo ;
      private int A1336ContratoServicosPrioridade_Codigo ;
      private int AV14GXV1 ;
      private int AV11Insert_ContratoServicosPrioridade_CntSrvCod ;
      private int Z1335ContratoServicosPrioridade_CntSrvCod ;
      private int A1335ContratoServicosPrioridade_CntSrvCod ;
      private decimal Z1338ContratoServicosPrioridade_PercValorB ;
      private decimal A1338ContratoServicosPrioridade_PercValorB ;
      private decimal Z1339ContratoServicosPrioridade_PercPrazo ;
      private decimal A1339ContratoServicosPrioridade_PercPrazo ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String AV13Pgmname ;
      private String Z1337ContratoServicosPrioridade_Nome ;
      private String A1337ContratoServicosPrioridade_Nome ;
      private String sMode162 ;
      private bool n2066ContratoServicosPrioridade_Ordem ;
      private bool n1359ContratoServicosPrioridade_Finalidade ;
      private bool n1338ContratoServicosPrioridade_PercValorB ;
      private bool n1339ContratoServicosPrioridade_PercPrazo ;
      private bool n2067ContratoServicosPrioridade_Peso ;
      private bool Gx_longc ;
      private String Z1359ContratoServicosPrioridade_Finalidade ;
      private String A1359ContratoServicosPrioridade_Finalidade ;
      private IGxSession AV10WebSession ;
      private SdtContratoServicosPrioridade bcContratoServicosPrioridade ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC003L5_A1336ContratoServicosPrioridade_Codigo ;
      private short[] BC003L5_A2066ContratoServicosPrioridade_Ordem ;
      private bool[] BC003L5_n2066ContratoServicosPrioridade_Ordem ;
      private String[] BC003L5_A1337ContratoServicosPrioridade_Nome ;
      private String[] BC003L5_A1359ContratoServicosPrioridade_Finalidade ;
      private bool[] BC003L5_n1359ContratoServicosPrioridade_Finalidade ;
      private decimal[] BC003L5_A1338ContratoServicosPrioridade_PercValorB ;
      private bool[] BC003L5_n1338ContratoServicosPrioridade_PercValorB ;
      private decimal[] BC003L5_A1339ContratoServicosPrioridade_PercPrazo ;
      private bool[] BC003L5_n1339ContratoServicosPrioridade_PercPrazo ;
      private short[] BC003L5_A2067ContratoServicosPrioridade_Peso ;
      private bool[] BC003L5_n2067ContratoServicosPrioridade_Peso ;
      private int[] BC003L5_A1335ContratoServicosPrioridade_CntSrvCod ;
      private int[] BC003L4_A1335ContratoServicosPrioridade_CntSrvCod ;
      private int[] BC003L6_A1336ContratoServicosPrioridade_Codigo ;
      private int[] BC003L3_A1336ContratoServicosPrioridade_Codigo ;
      private short[] BC003L3_A2066ContratoServicosPrioridade_Ordem ;
      private bool[] BC003L3_n2066ContratoServicosPrioridade_Ordem ;
      private String[] BC003L3_A1337ContratoServicosPrioridade_Nome ;
      private String[] BC003L3_A1359ContratoServicosPrioridade_Finalidade ;
      private bool[] BC003L3_n1359ContratoServicosPrioridade_Finalidade ;
      private decimal[] BC003L3_A1338ContratoServicosPrioridade_PercValorB ;
      private bool[] BC003L3_n1338ContratoServicosPrioridade_PercValorB ;
      private decimal[] BC003L3_A1339ContratoServicosPrioridade_PercPrazo ;
      private bool[] BC003L3_n1339ContratoServicosPrioridade_PercPrazo ;
      private short[] BC003L3_A2067ContratoServicosPrioridade_Peso ;
      private bool[] BC003L3_n2067ContratoServicosPrioridade_Peso ;
      private int[] BC003L3_A1335ContratoServicosPrioridade_CntSrvCod ;
      private int[] BC003L2_A1336ContratoServicosPrioridade_Codigo ;
      private short[] BC003L2_A2066ContratoServicosPrioridade_Ordem ;
      private bool[] BC003L2_n2066ContratoServicosPrioridade_Ordem ;
      private String[] BC003L2_A1337ContratoServicosPrioridade_Nome ;
      private String[] BC003L2_A1359ContratoServicosPrioridade_Finalidade ;
      private bool[] BC003L2_n1359ContratoServicosPrioridade_Finalidade ;
      private decimal[] BC003L2_A1338ContratoServicosPrioridade_PercValorB ;
      private bool[] BC003L2_n1338ContratoServicosPrioridade_PercValorB ;
      private decimal[] BC003L2_A1339ContratoServicosPrioridade_PercPrazo ;
      private bool[] BC003L2_n1339ContratoServicosPrioridade_PercPrazo ;
      private short[] BC003L2_A2067ContratoServicosPrioridade_Peso ;
      private bool[] BC003L2_n2067ContratoServicosPrioridade_Peso ;
      private int[] BC003L2_A1335ContratoServicosPrioridade_CntSrvCod ;
      private int[] BC003L7_A1336ContratoServicosPrioridade_Codigo ;
      private int[] BC003L10_A1336ContratoServicosPrioridade_Codigo ;
      private short[] BC003L10_A2066ContratoServicosPrioridade_Ordem ;
      private bool[] BC003L10_n2066ContratoServicosPrioridade_Ordem ;
      private String[] BC003L10_A1337ContratoServicosPrioridade_Nome ;
      private String[] BC003L10_A1359ContratoServicosPrioridade_Finalidade ;
      private bool[] BC003L10_n1359ContratoServicosPrioridade_Finalidade ;
      private decimal[] BC003L10_A1338ContratoServicosPrioridade_PercValorB ;
      private bool[] BC003L10_n1338ContratoServicosPrioridade_PercValorB ;
      private decimal[] BC003L10_A1339ContratoServicosPrioridade_PercPrazo ;
      private bool[] BC003L10_n1339ContratoServicosPrioridade_PercPrazo ;
      private short[] BC003L10_A2067ContratoServicosPrioridade_Peso ;
      private bool[] BC003L10_n2067ContratoServicosPrioridade_Peso ;
      private int[] BC003L10_A1335ContratoServicosPrioridade_CntSrvCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class contratoservicosprioridade_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new ForEachCursor(def[8])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC003L5 ;
          prmBC003L5 = new Object[] {
          new Object[] {"@ContratoServicosPrioridade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003L4 ;
          prmBC003L4 = new Object[] {
          new Object[] {"@ContratoServicosPrioridade_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003L6 ;
          prmBC003L6 = new Object[] {
          new Object[] {"@ContratoServicosPrioridade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003L3 ;
          prmBC003L3 = new Object[] {
          new Object[] {"@ContratoServicosPrioridade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003L2 ;
          prmBC003L2 = new Object[] {
          new Object[] {"@ContratoServicosPrioridade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003L7 ;
          prmBC003L7 = new Object[] {
          new Object[] {"@ContratoServicosPrioridade_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContratoServicosPrioridade_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@ContratoServicosPrioridade_Finalidade",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicosPrioridade_PercValorB",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoServicosPrioridade_PercPrazo",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoServicosPrioridade_Peso",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicosPrioridade_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003L8 ;
          prmBC003L8 = new Object[] {
          new Object[] {"@ContratoServicosPrioridade_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContratoServicosPrioridade_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@ContratoServicosPrioridade_Finalidade",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicosPrioridade_PercValorB",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoServicosPrioridade_PercPrazo",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoServicosPrioridade_Peso",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicosPrioridade_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosPrioridade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003L9 ;
          prmBC003L9 = new Object[] {
          new Object[] {"@ContratoServicosPrioridade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003L10 ;
          prmBC003L10 = new Object[] {
          new Object[] {"@ContratoServicosPrioridade_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC003L2", "SELECT [ContratoServicosPrioridade_Codigo], [ContratoServicosPrioridade_Ordem], [ContratoServicosPrioridade_Nome], [ContratoServicosPrioridade_Finalidade], [ContratoServicosPrioridade_PercValorB], [ContratoServicosPrioridade_PercPrazo], [ContratoServicosPrioridade_Peso], [ContratoServicosPrioridade_CntSrvCod] AS ContratoServicosPrioridade_CntSrvCod FROM [ContratoServicosPrioridade] WITH (UPDLOCK) WHERE [ContratoServicosPrioridade_Codigo] = @ContratoServicosPrioridade_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003L2,1,0,true,false )
             ,new CursorDef("BC003L3", "SELECT [ContratoServicosPrioridade_Codigo], [ContratoServicosPrioridade_Ordem], [ContratoServicosPrioridade_Nome], [ContratoServicosPrioridade_Finalidade], [ContratoServicosPrioridade_PercValorB], [ContratoServicosPrioridade_PercPrazo], [ContratoServicosPrioridade_Peso], [ContratoServicosPrioridade_CntSrvCod] AS ContratoServicosPrioridade_CntSrvCod FROM [ContratoServicosPrioridade] WITH (NOLOCK) WHERE [ContratoServicosPrioridade_Codigo] = @ContratoServicosPrioridade_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003L3,1,0,true,false )
             ,new CursorDef("BC003L4", "SELECT [ContratoServicos_Codigo] AS ContratoServicosPrioridade_CntSrvCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicosPrioridade_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003L4,1,0,true,false )
             ,new CursorDef("BC003L5", "SELECT TM1.[ContratoServicosPrioridade_Codigo], TM1.[ContratoServicosPrioridade_Ordem], TM1.[ContratoServicosPrioridade_Nome], TM1.[ContratoServicosPrioridade_Finalidade], TM1.[ContratoServicosPrioridade_PercValorB], TM1.[ContratoServicosPrioridade_PercPrazo], TM1.[ContratoServicosPrioridade_Peso], TM1.[ContratoServicosPrioridade_CntSrvCod] AS ContratoServicosPrioridade_CntSrvCod FROM [ContratoServicosPrioridade] TM1 WITH (NOLOCK) WHERE TM1.[ContratoServicosPrioridade_Codigo] = @ContratoServicosPrioridade_Codigo ORDER BY TM1.[ContratoServicosPrioridade_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003L5,100,0,true,false )
             ,new CursorDef("BC003L6", "SELECT [ContratoServicosPrioridade_Codigo] FROM [ContratoServicosPrioridade] WITH (NOLOCK) WHERE [ContratoServicosPrioridade_Codigo] = @ContratoServicosPrioridade_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003L6,1,0,true,false )
             ,new CursorDef("BC003L7", "INSERT INTO [ContratoServicosPrioridade]([ContratoServicosPrioridade_Ordem], [ContratoServicosPrioridade_Nome], [ContratoServicosPrioridade_Finalidade], [ContratoServicosPrioridade_PercValorB], [ContratoServicosPrioridade_PercPrazo], [ContratoServicosPrioridade_Peso], [ContratoServicosPrioridade_CntSrvCod]) VALUES(@ContratoServicosPrioridade_Ordem, @ContratoServicosPrioridade_Nome, @ContratoServicosPrioridade_Finalidade, @ContratoServicosPrioridade_PercValorB, @ContratoServicosPrioridade_PercPrazo, @ContratoServicosPrioridade_Peso, @ContratoServicosPrioridade_CntSrvCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC003L7)
             ,new CursorDef("BC003L8", "UPDATE [ContratoServicosPrioridade] SET [ContratoServicosPrioridade_Ordem]=@ContratoServicosPrioridade_Ordem, [ContratoServicosPrioridade_Nome]=@ContratoServicosPrioridade_Nome, [ContratoServicosPrioridade_Finalidade]=@ContratoServicosPrioridade_Finalidade, [ContratoServicosPrioridade_PercValorB]=@ContratoServicosPrioridade_PercValorB, [ContratoServicosPrioridade_PercPrazo]=@ContratoServicosPrioridade_PercPrazo, [ContratoServicosPrioridade_Peso]=@ContratoServicosPrioridade_Peso, [ContratoServicosPrioridade_CntSrvCod]=@ContratoServicosPrioridade_CntSrvCod  WHERE [ContratoServicosPrioridade_Codigo] = @ContratoServicosPrioridade_Codigo", GxErrorMask.GX_NOMASK,prmBC003L8)
             ,new CursorDef("BC003L9", "DELETE FROM [ContratoServicosPrioridade]  WHERE [ContratoServicosPrioridade_Codigo] = @ContratoServicosPrioridade_Codigo", GxErrorMask.GX_NOMASK,prmBC003L9)
             ,new CursorDef("BC003L10", "SELECT TM1.[ContratoServicosPrioridade_Codigo], TM1.[ContratoServicosPrioridade_Ordem], TM1.[ContratoServicosPrioridade_Nome], TM1.[ContratoServicosPrioridade_Finalidade], TM1.[ContratoServicosPrioridade_PercValorB], TM1.[ContratoServicosPrioridade_PercPrazo], TM1.[ContratoServicosPrioridade_Peso], TM1.[ContratoServicosPrioridade_CntSrvCod] AS ContratoServicosPrioridade_CntSrvCod FROM [ContratoServicosPrioridade] TM1 WITH (NOLOCK) WHERE TM1.[ContratoServicosPrioridade_Codigo] = @ContratoServicosPrioridade_Codigo ORDER BY TM1.[ContratoServicosPrioridade_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003L10,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((short[]) buf[10])[0] = rslt.getShort(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((short[]) buf[10])[0] = rslt.getShort(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((short[]) buf[10])[0] = rslt.getShort(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((short[]) buf[10])[0] = rslt.getShort(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                stmt.SetParameter(2, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(6, (short)parms[10]);
                }
                stmt.SetParameter(7, (int)parms[11]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                stmt.SetParameter(2, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(6, (short)parms[10]);
                }
                stmt.SetParameter(7, (int)parms[11]);
                stmt.SetParameter(8, (int)parms[12]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
