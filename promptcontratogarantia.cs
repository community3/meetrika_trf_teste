/*
               File: PromptContratoGarantia
        Description: Selecione Contrato Garantia
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:15:0.53
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptcontratogarantia : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptcontratogarantia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptcontratogarantia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutContratoGarantia_Codigo ,
                           ref DateTime aP1_InOutContratoGarantia_DataPagtoGarantia )
      {
         this.AV7InOutContratoGarantia_Codigo = aP0_InOutContratoGarantia_Codigo;
         this.AV8InOutContratoGarantia_DataPagtoGarantia = aP1_InOutContratoGarantia_DataPagtoGarantia;
         executePrivate();
         aP0_InOutContratoGarantia_Codigo=this.AV7InOutContratoGarantia_Codigo;
         aP1_InOutContratoGarantia_DataPagtoGarantia=this.AV8InOutContratoGarantia_DataPagtoGarantia;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_86 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_86_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_86_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16ContratoGarantia_DataPagtoGarantia1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoGarantia_DataPagtoGarantia1", context.localUtil.Format(AV16ContratoGarantia_DataPagtoGarantia1, "99/99/99"));
               AV17ContratoGarantia_DataPagtoGarantia_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoGarantia_DataPagtoGarantia_To1", context.localUtil.Format(AV17ContratoGarantia_DataPagtoGarantia_To1, "99/99/99"));
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20ContratoGarantia_DataPagtoGarantia2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContratoGarantia_DataPagtoGarantia2", context.localUtil.Format(AV20ContratoGarantia_DataPagtoGarantia2, "99/99/99"));
               AV21ContratoGarantia_DataPagtoGarantia_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoGarantia_DataPagtoGarantia_To2", context.localUtil.Format(AV21ContratoGarantia_DataPagtoGarantia_To2, "99/99/99"));
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24ContratoGarantia_DataPagtoGarantia3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratoGarantia_DataPagtoGarantia3", context.localUtil.Format(AV24ContratoGarantia_DataPagtoGarantia3, "99/99/99"));
               AV25ContratoGarantia_DataPagtoGarantia_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoGarantia_DataPagtoGarantia_To3", context.localUtil.Format(AV25ContratoGarantia_DataPagtoGarantia_To3, "99/99/99"));
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV31TFContratoGarantia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFContratoGarantia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContratoGarantia_Codigo), 6, 0)));
               AV32TFContratoGarantia_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoGarantia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoGarantia_Codigo_To), 6, 0)));
               AV35TFContrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFContrato_Codigo), 6, 0)));
               AV36TFContrato_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContrato_Codigo_To), 6, 0)));
               AV39TFContrato_Numero = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContrato_Numero", AV39TFContrato_Numero);
               AV40TFContrato_Numero_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContrato_Numero_Sel", AV40TFContrato_Numero_Sel);
               AV43TFContratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFContratada_Codigo), 6, 0)));
               AV44TFContratada_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratada_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFContratada_Codigo_To), 6, 0)));
               AV47TFContratada_PessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFContratada_PessoaCod), 6, 0)));
               AV48TFContratada_PessoaCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratada_PessoaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFContratada_PessoaCod_To), 6, 0)));
               AV51TFContratada_PessoaNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratada_PessoaNom", AV51TFContratada_PessoaNom);
               AV52TFContratada_PessoaNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContratada_PessoaNom_Sel", AV52TFContratada_PessoaNom_Sel);
               AV55TFContratada_PessoaCNPJ = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratada_PessoaCNPJ", AV55TFContratada_PessoaCNPJ);
               AV56TFContratada_PessoaCNPJ_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContratada_PessoaCNPJ_Sel", AV56TFContratada_PessoaCNPJ_Sel);
               AV59TFContratoGarantia_DataPagtoGarantia = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoGarantia_DataPagtoGarantia", context.localUtil.Format(AV59TFContratoGarantia_DataPagtoGarantia, "99/99/99"));
               AV60TFContratoGarantia_DataPagtoGarantia_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoGarantia_DataPagtoGarantia_To", context.localUtil.Format(AV60TFContratoGarantia_DataPagtoGarantia_To, "99/99/99"));
               AV65TFContratoGarantia_Percentual = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratoGarantia_Percentual", StringUtil.LTrim( StringUtil.Str( AV65TFContratoGarantia_Percentual, 6, 2)));
               AV66TFContratoGarantia_Percentual_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratoGarantia_Percentual_To", StringUtil.LTrim( StringUtil.Str( AV66TFContratoGarantia_Percentual_To, 6, 2)));
               AV69TFContratoGarantia_DataCaucao = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFContratoGarantia_DataCaucao", context.localUtil.Format(AV69TFContratoGarantia_DataCaucao, "99/99/99"));
               AV70TFContratoGarantia_DataCaucao_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratoGarantia_DataCaucao_To", context.localUtil.Format(AV70TFContratoGarantia_DataCaucao_To, "99/99/99"));
               AV75TFContratoGarantia_ValorGarantia = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContratoGarantia_ValorGarantia", StringUtil.LTrim( StringUtil.Str( AV75TFContratoGarantia_ValorGarantia, 18, 5)));
               AV76TFContratoGarantia_ValorGarantia_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContratoGarantia_ValorGarantia_To", StringUtil.LTrim( StringUtil.Str( AV76TFContratoGarantia_ValorGarantia_To, 18, 5)));
               AV79TFContratoGarantia_DataEncerramento = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFContratoGarantia_DataEncerramento", context.localUtil.Format(AV79TFContratoGarantia_DataEncerramento, "99/99/99"));
               AV80TFContratoGarantia_DataEncerramento_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContratoGarantia_DataEncerramento_To", context.localUtil.Format(AV80TFContratoGarantia_DataEncerramento_To, "99/99/99"));
               AV85TFContratoGarantia_ValorEncerramento = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFContratoGarantia_ValorEncerramento", StringUtil.LTrim( StringUtil.Str( AV85TFContratoGarantia_ValorEncerramento, 18, 5)));
               AV86TFContratoGarantia_ValorEncerramento_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFContratoGarantia_ValorEncerramento_To", StringUtil.LTrim( StringUtil.Str( AV86TFContratoGarantia_ValorEncerramento_To, 18, 5)));
               AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace", AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace);
               AV37ddo_Contrato_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_Contrato_CodigoTitleControlIdToReplace", AV37ddo_Contrato_CodigoTitleControlIdToReplace);
               AV41ddo_Contrato_NumeroTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_Contrato_NumeroTitleControlIdToReplace", AV41ddo_Contrato_NumeroTitleControlIdToReplace);
               AV45ddo_Contratada_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_Contratada_CodigoTitleControlIdToReplace", AV45ddo_Contratada_CodigoTitleControlIdToReplace);
               AV49ddo_Contratada_PessoaCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_Contratada_PessoaCodTitleControlIdToReplace", AV49ddo_Contratada_PessoaCodTitleControlIdToReplace);
               AV53ddo_Contratada_PessoaNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_Contratada_PessoaNomTitleControlIdToReplace", AV53ddo_Contratada_PessoaNomTitleControlIdToReplace);
               AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace", AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace);
               AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace", AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace);
               AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace", AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace);
               AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace", AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace);
               AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace", AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace);
               AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace", AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace);
               AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace", AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace);
               AV101Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoGarantia_DataPagtoGarantia1, AV17ContratoGarantia_DataPagtoGarantia_To1, AV19DynamicFiltersSelector2, AV20ContratoGarantia_DataPagtoGarantia2, AV21ContratoGarantia_DataPagtoGarantia_To2, AV23DynamicFiltersSelector3, AV24ContratoGarantia_DataPagtoGarantia3, AV25ContratoGarantia_DataPagtoGarantia_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContratoGarantia_Codigo, AV32TFContratoGarantia_Codigo_To, AV35TFContrato_Codigo, AV36TFContrato_Codigo_To, AV39TFContrato_Numero, AV40TFContrato_Numero_Sel, AV43TFContratada_Codigo, AV44TFContratada_Codigo_To, AV47TFContratada_PessoaCod, AV48TFContratada_PessoaCod_To, AV51TFContratada_PessoaNom, AV52TFContratada_PessoaNom_Sel, AV55TFContratada_PessoaCNPJ, AV56TFContratada_PessoaCNPJ_Sel, AV59TFContratoGarantia_DataPagtoGarantia, AV60TFContratoGarantia_DataPagtoGarantia_To, AV65TFContratoGarantia_Percentual, AV66TFContratoGarantia_Percentual_To, AV69TFContratoGarantia_DataCaucao, AV70TFContratoGarantia_DataCaucao_To, AV75TFContratoGarantia_ValorGarantia, AV76TFContratoGarantia_ValorGarantia_To, AV79TFContratoGarantia_DataEncerramento, AV80TFContratoGarantia_DataEncerramento_To, AV85TFContratoGarantia_ValorEncerramento, AV86TFContratoGarantia_ValorEncerramento_To, AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace, AV37ddo_Contrato_CodigoTitleControlIdToReplace, AV41ddo_Contrato_NumeroTitleControlIdToReplace, AV45ddo_Contratada_CodigoTitleControlIdToReplace, AV49ddo_Contratada_PessoaCodTitleControlIdToReplace, AV53ddo_Contratada_PessoaNomTitleControlIdToReplace, AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace, AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace, AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace, AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace, AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace, AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace, AV101Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutContratoGarantia_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoGarantia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoGarantia_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutContratoGarantia_DataPagtoGarantia = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoGarantia_DataPagtoGarantia", context.localUtil.Format(AV8InOutContratoGarantia_DataPagtoGarantia, "99/99/99"));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA6I2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV101Pgmname = "PromptContratoGarantia";
               context.Gx_err = 0;
               WS6I2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE6I2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282315143");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptcontratogarantia.aspx") + "?" + UrlEncode("" +AV7InOutContratoGarantia_Codigo) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV8InOutContratoGarantia_DataPagtoGarantia))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA1", context.localUtil.Format(AV16ContratoGarantia_DataPagtoGarantia1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1", context.localUtil.Format(AV17ContratoGarantia_DataPagtoGarantia_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA2", context.localUtil.Format(AV20ContratoGarantia_DataPagtoGarantia2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2", context.localUtil.Format(AV21ContratoGarantia_DataPagtoGarantia_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA3", context.localUtil.Format(AV24ContratoGarantia_DataPagtoGarantia3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3", context.localUtil.Format(AV25ContratoGarantia_DataPagtoGarantia_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOGARANTIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31TFContratoGarantia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOGARANTIA_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFContratoGarantia_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFContrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFContrato_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_NUMERO", StringUtil.RTrim( AV39TFContrato_Numero));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_NUMERO_SEL", StringUtil.RTrim( AV40TFContrato_Numero_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43TFContratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44TFContratada_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47TFContratada_PessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOACOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV48TFContratada_PessoaCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOANOM", StringUtil.RTrim( AV51TFContratada_PessoaNom));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOANOM_SEL", StringUtil.RTrim( AV52TFContratada_PessoaNom_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOACNPJ", AV55TFContratada_PessoaCNPJ);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOACNPJ_SEL", AV56TFContratada_PessoaCNPJ_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA", context.localUtil.Format(AV59TFContratoGarantia_DataPagtoGarantia, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO", context.localUtil.Format(AV60TFContratoGarantia_DataPagtoGarantia_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOGARANTIA_PERCENTUAL", StringUtil.LTrim( StringUtil.NToC( AV65TFContratoGarantia_Percentual, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOGARANTIA_PERCENTUAL_TO", StringUtil.LTrim( StringUtil.NToC( AV66TFContratoGarantia_Percentual_To, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOGARANTIA_DATACAUCAO", context.localUtil.Format(AV69TFContratoGarantia_DataCaucao, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOGARANTIA_DATACAUCAO_TO", context.localUtil.Format(AV70TFContratoGarantia_DataCaucao_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOGARANTIA_VALORGARANTIA", StringUtil.LTrim( StringUtil.NToC( AV75TFContratoGarantia_ValorGarantia, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOGARANTIA_VALORGARANTIA_TO", StringUtil.LTrim( StringUtil.NToC( AV76TFContratoGarantia_ValorGarantia_To, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOGARANTIA_DATAENCERRAMENTO", context.localUtil.Format(AV79TFContratoGarantia_DataEncerramento, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO", context.localUtil.Format(AV80TFContratoGarantia_DataEncerramento_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOGARANTIA_VALORENCERRAMENTO", StringUtil.LTrim( StringUtil.NToC( AV85TFContratoGarantia_ValorEncerramento, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO", StringUtil.LTrim( StringUtil.NToC( AV86TFContratoGarantia_ValorEncerramento_To, 18, 5, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_86", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_86), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV90GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV91GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV88DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV88DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOGARANTIA_CODIGOTITLEFILTERDATA", AV30ContratoGarantia_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOGARANTIA_CODIGOTITLEFILTERDATA", AV30ContratoGarantia_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATO_CODIGOTITLEFILTERDATA", AV34Contrato_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATO_CODIGOTITLEFILTERDATA", AV34Contrato_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATO_NUMEROTITLEFILTERDATA", AV38Contrato_NumeroTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATO_NUMEROTITLEFILTERDATA", AV38Contrato_NumeroTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADA_CODIGOTITLEFILTERDATA", AV42Contratada_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADA_CODIGOTITLEFILTERDATA", AV42Contratada_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADA_PESSOACODTITLEFILTERDATA", AV46Contratada_PessoaCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADA_PESSOACODTITLEFILTERDATA", AV46Contratada_PessoaCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADA_PESSOANOMTITLEFILTERDATA", AV50Contratada_PessoaNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADA_PESSOANOMTITLEFILTERDATA", AV50Contratada_PessoaNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADA_PESSOACNPJTITLEFILTERDATA", AV54Contratada_PessoaCNPJTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADA_PESSOACNPJTITLEFILTERDATA", AV54Contratada_PessoaCNPJTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOGARANTIA_DATAPAGTOGARANTIATITLEFILTERDATA", AV58ContratoGarantia_DataPagtoGarantiaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOGARANTIA_DATAPAGTOGARANTIATITLEFILTERDATA", AV58ContratoGarantia_DataPagtoGarantiaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOGARANTIA_PERCENTUALTITLEFILTERDATA", AV64ContratoGarantia_PercentualTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOGARANTIA_PERCENTUALTITLEFILTERDATA", AV64ContratoGarantia_PercentualTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOGARANTIA_DATACAUCAOTITLEFILTERDATA", AV68ContratoGarantia_DataCaucaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOGARANTIA_DATACAUCAOTITLEFILTERDATA", AV68ContratoGarantia_DataCaucaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOGARANTIA_VALORGARANTIATITLEFILTERDATA", AV74ContratoGarantia_ValorGarantiaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOGARANTIA_VALORGARANTIATITLEFILTERDATA", AV74ContratoGarantia_ValorGarantiaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOGARANTIA_DATAENCERRAMENTOTITLEFILTERDATA", AV78ContratoGarantia_DataEncerramentoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOGARANTIA_DATAENCERRAMENTOTITLEFILTERDATA", AV78ContratoGarantia_DataEncerramentoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOGARANTIA_VALORENCERRAMENTOTITLEFILTERDATA", AV84ContratoGarantia_ValorEncerramentoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOGARANTIA_VALORENCERRAMENTOTITLEFILTERDATA", AV84ContratoGarantia_ValorEncerramentoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV101Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATOGARANTIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutContratoGarantia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATOGARANTIA_DATAPAGTOGARANTIA", context.localUtil.DToC( AV8InOutContratoGarantia_DataPagtoGarantia, 0, "/"));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_CODIGO_Caption", StringUtil.RTrim( Ddo_contratogarantia_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_CODIGO_Tooltip", StringUtil.RTrim( Ddo_contratogarantia_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_CODIGO_Cls", StringUtil.RTrim( Ddo_contratogarantia_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_contratogarantia_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratogarantia_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratogarantia_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratogarantia_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_contratogarantia_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratogarantia_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_contratogarantia_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_contratogarantia_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_CODIGO_Filtertype", StringUtil.RTrim( Ddo_contratogarantia_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_contratogarantia_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_contratogarantia_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_CODIGO_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratogarantia_codigo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_CODIGO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratogarantia_codigo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_CODIGO_Sortasc", StringUtil.RTrim( Ddo_contratogarantia_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_contratogarantia_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_CODIGO_Loadingdata", StringUtil.RTrim( Ddo_contratogarantia_codigo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_contratogarantia_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratogarantia_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_contratogarantia_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_CODIGO_Noresultsfound", StringUtil.RTrim( Ddo_contratogarantia_codigo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_contratogarantia_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Caption", StringUtil.RTrim( Ddo_contrato_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Tooltip", StringUtil.RTrim( Ddo_contrato_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Cls", StringUtil.RTrim( Ddo_contrato_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_contrato_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_contrato_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contrato_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contrato_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_contrato_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_contrato_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_contrato_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_contrato_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filtertype", StringUtil.RTrim( Ddo_contrato_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_contrato_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_contrato_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Datalistfixedvalues", StringUtil.RTrim( Ddo_contrato_codigo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contrato_codigo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Sortasc", StringUtil.RTrim( Ddo_contrato_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_contrato_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Loadingdata", StringUtil.RTrim( Ddo_contrato_codigo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_contrato_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_contrato_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_contrato_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Noresultsfound", StringUtil.RTrim( Ddo_contrato_codigo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_contrato_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Caption", StringUtil.RTrim( Ddo_contrato_numero_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Tooltip", StringUtil.RTrim( Ddo_contrato_numero_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Cls", StringUtil.RTrim( Ddo_contrato_numero_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filteredtext_set", StringUtil.RTrim( Ddo_contrato_numero_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Selectedvalue_set", StringUtil.RTrim( Ddo_contrato_numero_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contrato_numero_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contrato_numero_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includesortasc", StringUtil.BoolToStr( Ddo_contrato_numero_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includesortdsc", StringUtil.BoolToStr( Ddo_contrato_numero_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortedstatus", StringUtil.RTrim( Ddo_contrato_numero_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includefilter", StringUtil.BoolToStr( Ddo_contrato_numero_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filtertype", StringUtil.RTrim( Ddo_contrato_numero_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filterisrange", StringUtil.BoolToStr( Ddo_contrato_numero_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includedatalist", StringUtil.BoolToStr( Ddo_contrato_numero_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalisttype", StringUtil.RTrim( Ddo_contrato_numero_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalistfixedvalues", StringUtil.RTrim( Ddo_contrato_numero_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalistproc", StringUtil.RTrim( Ddo_contrato_numero_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contrato_numero_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortasc", StringUtil.RTrim( Ddo_contrato_numero_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortdsc", StringUtil.RTrim( Ddo_contrato_numero_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Loadingdata", StringUtil.RTrim( Ddo_contrato_numero_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Cleanfilter", StringUtil.RTrim( Ddo_contrato_numero_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Rangefilterfrom", StringUtil.RTrim( Ddo_contrato_numero_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Rangefilterto", StringUtil.RTrim( Ddo_contrato_numero_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Noresultsfound", StringUtil.RTrim( Ddo_contrato_numero_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Searchbuttontext", StringUtil.RTrim( Ddo_contrato_numero_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Caption", StringUtil.RTrim( Ddo_contratada_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Tooltip", StringUtil.RTrim( Ddo_contratada_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Cls", StringUtil.RTrim( Ddo_contratada_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_contratada_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratada_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratada_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratada_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_contratada_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratada_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_contratada_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_contratada_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Filtertype", StringUtil.RTrim( Ddo_contratada_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_contratada_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_contratada_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratada_codigo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratada_codigo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Sortasc", StringUtil.RTrim( Ddo_contratada_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_contratada_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Loadingdata", StringUtil.RTrim( Ddo_contratada_codigo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_contratada_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratada_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_contratada_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Noresultsfound", StringUtil.RTrim( Ddo_contratada_codigo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_contratada_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Caption", StringUtil.RTrim( Ddo_contratada_pessoacod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Tooltip", StringUtil.RTrim( Ddo_contratada_pessoacod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Cls", StringUtil.RTrim( Ddo_contratada_pessoacod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Filteredtext_set", StringUtil.RTrim( Ddo_contratada_pessoacod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Filteredtextto_set", StringUtil.RTrim( Ddo_contratada_pessoacod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratada_pessoacod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratada_pessoacod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Includesortasc", StringUtil.BoolToStr( Ddo_contratada_pessoacod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Includesortdsc", StringUtil.BoolToStr( Ddo_contratada_pessoacod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Sortedstatus", StringUtil.RTrim( Ddo_contratada_pessoacod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Includefilter", StringUtil.BoolToStr( Ddo_contratada_pessoacod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Filtertype", StringUtil.RTrim( Ddo_contratada_pessoacod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Filterisrange", StringUtil.BoolToStr( Ddo_contratada_pessoacod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Includedatalist", StringUtil.BoolToStr( Ddo_contratada_pessoacod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratada_pessoacod_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratada_pessoacod_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Sortasc", StringUtil.RTrim( Ddo_contratada_pessoacod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Sortdsc", StringUtil.RTrim( Ddo_contratada_pessoacod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Loadingdata", StringUtil.RTrim( Ddo_contratada_pessoacod_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Cleanfilter", StringUtil.RTrim( Ddo_contratada_pessoacod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Rangefilterfrom", StringUtil.RTrim( Ddo_contratada_pessoacod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Rangefilterto", StringUtil.RTrim( Ddo_contratada_pessoacod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Noresultsfound", StringUtil.RTrim( Ddo_contratada_pessoacod_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Searchbuttontext", StringUtil.RTrim( Ddo_contratada_pessoacod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Caption", StringUtil.RTrim( Ddo_contratada_pessoanom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Tooltip", StringUtil.RTrim( Ddo_contratada_pessoanom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Cls", StringUtil.RTrim( Ddo_contratada_pessoanom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Filteredtext_set", StringUtil.RTrim( Ddo_contratada_pessoanom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Selectedvalue_set", StringUtil.RTrim( Ddo_contratada_pessoanom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratada_pessoanom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratada_pessoanom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Includesortasc", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Includesortdsc", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Sortedstatus", StringUtil.RTrim( Ddo_contratada_pessoanom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Includefilter", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Filtertype", StringUtil.RTrim( Ddo_contratada_pessoanom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Filterisrange", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Includedatalist", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Datalisttype", StringUtil.RTrim( Ddo_contratada_pessoanom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratada_pessoanom_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Datalistproc", StringUtil.RTrim( Ddo_contratada_pessoanom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratada_pessoanom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Sortasc", StringUtil.RTrim( Ddo_contratada_pessoanom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Sortdsc", StringUtil.RTrim( Ddo_contratada_pessoanom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Loadingdata", StringUtil.RTrim( Ddo_contratada_pessoanom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Cleanfilter", StringUtil.RTrim( Ddo_contratada_pessoanom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Rangefilterfrom", StringUtil.RTrim( Ddo_contratada_pessoanom_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Rangefilterto", StringUtil.RTrim( Ddo_contratada_pessoanom_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Noresultsfound", StringUtil.RTrim( Ddo_contratada_pessoanom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Searchbuttontext", StringUtil.RTrim( Ddo_contratada_pessoanom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Caption", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Tooltip", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Cls", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Filteredtext_set", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Selectedvalue_set", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Includesortasc", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Includesortdsc", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Sortedstatus", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Includefilter", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Filtertype", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Filterisrange", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Includedatalist", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Datalisttype", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Datalistproc", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratada_pessoacnpj_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Sortasc", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Sortdsc", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Loadingdata", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Cleanfilter", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Rangefilterfrom", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Rangefilterto", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Noresultsfound", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Searchbuttontext", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Caption", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Tooltip", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Cls", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Filteredtext_set", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Filteredtextto_set", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Includesortasc", StringUtil.BoolToStr( Ddo_contratogarantia_datapagtogarantia_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Includesortdsc", StringUtil.BoolToStr( Ddo_contratogarantia_datapagtogarantia_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Sortedstatus", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Includefilter", StringUtil.BoolToStr( Ddo_contratogarantia_datapagtogarantia_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Filtertype", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Filterisrange", StringUtil.BoolToStr( Ddo_contratogarantia_datapagtogarantia_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Includedatalist", StringUtil.BoolToStr( Ddo_contratogarantia_datapagtogarantia_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratogarantia_datapagtogarantia_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Sortasc", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Sortdsc", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Loadingdata", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Cleanfilter", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Rangefilterfrom", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Rangefilterto", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Noresultsfound", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Searchbuttontext", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Caption", StringUtil.RTrim( Ddo_contratogarantia_percentual_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Tooltip", StringUtil.RTrim( Ddo_contratogarantia_percentual_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Cls", StringUtil.RTrim( Ddo_contratogarantia_percentual_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Filteredtext_set", StringUtil.RTrim( Ddo_contratogarantia_percentual_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Filteredtextto_set", StringUtil.RTrim( Ddo_contratogarantia_percentual_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratogarantia_percentual_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratogarantia_percentual_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Includesortasc", StringUtil.BoolToStr( Ddo_contratogarantia_percentual_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Includesortdsc", StringUtil.BoolToStr( Ddo_contratogarantia_percentual_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Sortedstatus", StringUtil.RTrim( Ddo_contratogarantia_percentual_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Includefilter", StringUtil.BoolToStr( Ddo_contratogarantia_percentual_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Filtertype", StringUtil.RTrim( Ddo_contratogarantia_percentual_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Filterisrange", StringUtil.BoolToStr( Ddo_contratogarantia_percentual_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Includedatalist", StringUtil.BoolToStr( Ddo_contratogarantia_percentual_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratogarantia_percentual_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratogarantia_percentual_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Sortasc", StringUtil.RTrim( Ddo_contratogarantia_percentual_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Sortdsc", StringUtil.RTrim( Ddo_contratogarantia_percentual_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Loadingdata", StringUtil.RTrim( Ddo_contratogarantia_percentual_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Cleanfilter", StringUtil.RTrim( Ddo_contratogarantia_percentual_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Rangefilterfrom", StringUtil.RTrim( Ddo_contratogarantia_percentual_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Rangefilterto", StringUtil.RTrim( Ddo_contratogarantia_percentual_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Noresultsfound", StringUtil.RTrim( Ddo_contratogarantia_percentual_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Searchbuttontext", StringUtil.RTrim( Ddo_contratogarantia_percentual_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Caption", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Tooltip", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Cls", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Filteredtext_set", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Includesortasc", StringUtil.BoolToStr( Ddo_contratogarantia_datacaucao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratogarantia_datacaucao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Sortedstatus", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Includefilter", StringUtil.BoolToStr( Ddo_contratogarantia_datacaucao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Filtertype", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Filterisrange", StringUtil.BoolToStr( Ddo_contratogarantia_datacaucao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Includedatalist", StringUtil.BoolToStr( Ddo_contratogarantia_datacaucao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratogarantia_datacaucao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Sortasc", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Sortdsc", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Loadingdata", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Cleanfilter", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Rangefilterto", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Noresultsfound", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Searchbuttontext", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Caption", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Tooltip", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Cls", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Filteredtext_set", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Filteredtextto_set", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Includesortasc", StringUtil.BoolToStr( Ddo_contratogarantia_valorgarantia_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Includesortdsc", StringUtil.BoolToStr( Ddo_contratogarantia_valorgarantia_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Sortedstatus", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Includefilter", StringUtil.BoolToStr( Ddo_contratogarantia_valorgarantia_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Filtertype", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Filterisrange", StringUtil.BoolToStr( Ddo_contratogarantia_valorgarantia_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Includedatalist", StringUtil.BoolToStr( Ddo_contratogarantia_valorgarantia_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratogarantia_valorgarantia_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Sortasc", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Sortdsc", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Loadingdata", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Cleanfilter", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Rangefilterfrom", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Rangefilterto", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Noresultsfound", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Searchbuttontext", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Caption", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Tooltip", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Cls", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Filteredtext_set", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Includesortasc", StringUtil.BoolToStr( Ddo_contratogarantia_dataencerramento_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratogarantia_dataencerramento_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Sortedstatus", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Includefilter", StringUtil.BoolToStr( Ddo_contratogarantia_dataencerramento_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Filtertype", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Filterisrange", StringUtil.BoolToStr( Ddo_contratogarantia_dataencerramento_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Includedatalist", StringUtil.BoolToStr( Ddo_contratogarantia_dataencerramento_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratogarantia_dataencerramento_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Sortasc", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Sortdsc", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Loadingdata", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Cleanfilter", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Rangefilterto", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Noresultsfound", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Searchbuttontext", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Caption", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Tooltip", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Cls", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Filteredtext_set", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Includesortasc", StringUtil.BoolToStr( Ddo_contratogarantia_valorencerramento_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratogarantia_valorencerramento_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Sortedstatus", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Includefilter", StringUtil.BoolToStr( Ddo_contratogarantia_valorencerramento_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Filtertype", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Filterisrange", StringUtil.BoolToStr( Ddo_contratogarantia_valorencerramento_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Includedatalist", StringUtil.BoolToStr( Ddo_contratogarantia_valorencerramento_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratogarantia_valorencerramento_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Sortasc", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Sortdsc", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Loadingdata", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Cleanfilter", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Rangefilterto", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Noresultsfound", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Searchbuttontext", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_contratogarantia_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_contratogarantia_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratogarantia_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_contrato_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_contrato_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_contrato_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Activeeventkey", StringUtil.RTrim( Ddo_contrato_numero_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filteredtext_get", StringUtil.RTrim( Ddo_contrato_numero_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Selectedvalue_get", StringUtil.RTrim( Ddo_contrato_numero_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_contratada_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_contratada_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratada_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Activeeventkey", StringUtil.RTrim( Ddo_contratada_pessoacod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Filteredtext_get", StringUtil.RTrim( Ddo_contratada_pessoacod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Filteredtextto_get", StringUtil.RTrim( Ddo_contratada_pessoacod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Activeeventkey", StringUtil.RTrim( Ddo_contratada_pessoanom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Filteredtext_get", StringUtil.RTrim( Ddo_contratada_pessoanom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Selectedvalue_get", StringUtil.RTrim( Ddo_contratada_pessoanom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Activeeventkey", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Filteredtext_get", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Selectedvalue_get", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Activeeventkey", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Filteredtext_get", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Filteredtextto_get", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Activeeventkey", StringUtil.RTrim( Ddo_contratogarantia_percentual_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Filteredtext_get", StringUtil.RTrim( Ddo_contratogarantia_percentual_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Filteredtextto_get", StringUtil.RTrim( Ddo_contratogarantia_percentual_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Activeeventkey", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Filteredtext_get", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Activeeventkey", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Filteredtext_get", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Filteredtextto_get", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Activeeventkey", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Filteredtext_get", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Activeeventkey", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Filteredtext_get", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm6I2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptContratoGarantia" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Contrato Garantia" ;
      }

      protected void WB6I0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_6I2( true) ;
         }
         else
         {
            wb_table1_2_6I2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_6I2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(105, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,105);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(106, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratogarantia_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31TFContratoGarantia_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV31TFContratoGarantia_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratogarantia_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratogarantia_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratogarantia_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFContratoGarantia_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV32TFContratoGarantia_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratogarantia_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratogarantia_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFContrato_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV35TFContrato_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFContrato_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV36TFContrato_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_numero_Internalname, StringUtil.RTrim( AV39TFContrato_Numero), StringUtil.RTrim( context.localUtil.Format( AV39TFContrato_Numero, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_numero_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_numero_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_numero_sel_Internalname, StringUtil.RTrim( AV40TFContrato_Numero_Sel), StringUtil.RTrim( context.localUtil.Format( AV40TFContrato_Numero_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,112);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_numero_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_numero_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43TFContratada_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV43TFContratada_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44TFContratada_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV44TFContratada_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoacod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47TFContratada_PessoaCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV47TFContratada_PessoaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoacod_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_pessoacod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoacod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV48TFContratada_PessoaCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV48TFContratada_PessoaCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoacod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_pessoacod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoanom_Internalname, StringUtil.RTrim( AV51TFContratada_PessoaNom), StringUtil.RTrim( context.localUtil.Format( AV51TFContratada_PessoaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,117);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoanom_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratada_pessoanom_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoanom_sel_Internalname, StringUtil.RTrim( AV52TFContratada_PessoaNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV52TFContratada_PessoaNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,118);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoanom_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratada_pessoanom_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoacnpj_Internalname, AV55TFContratada_PessoaCNPJ, StringUtil.RTrim( context.localUtil.Format( AV55TFContratada_PessoaCNPJ, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,119);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoacnpj_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_pessoacnpj_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoacnpj_sel_Internalname, AV56TFContratada_PessoaCNPJ_Sel, StringUtil.RTrim( context.localUtil.Format( AV56TFContratada_PessoaCNPJ_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,120);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoacnpj_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_pessoacnpj_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratogarantia_datapagtogarantia_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratogarantia_datapagtogarantia_Internalname, context.localUtil.Format(AV59TFContratoGarantia_DataPagtoGarantia, "99/99/99"), context.localUtil.Format( AV59TFContratoGarantia_DataPagtoGarantia, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,121);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratogarantia_datapagtogarantia_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratogarantia_datapagtogarantia_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratogarantia_datapagtogarantia_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratogarantia_datapagtogarantia_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratogarantia_datapagtogarantia_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratogarantia_datapagtogarantia_to_Internalname, context.localUtil.Format(AV60TFContratoGarantia_DataPagtoGarantia_To, "99/99/99"), context.localUtil.Format( AV60TFContratoGarantia_DataPagtoGarantia_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,122);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratogarantia_datapagtogarantia_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratogarantia_datapagtogarantia_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratogarantia_datapagtogarantia_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratogarantia_datapagtogarantia_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contratogarantia_datapagtogarantiaauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratogarantia_datapagtogarantiaauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratogarantia_datapagtogarantiaauxdate_Internalname, context.localUtil.Format(AV61DDO_ContratoGarantia_DataPagtoGarantiaAuxDate, "99/99/99"), context.localUtil.Format( AV61DDO_ContratoGarantia_DataPagtoGarantiaAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,124);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratogarantia_datapagtogarantiaauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratogarantia_datapagtogarantiaauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratogarantia_datapagtogarantiaauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratogarantia_datapagtogarantiaauxdateto_Internalname, context.localUtil.Format(AV62DDO_ContratoGarantia_DataPagtoGarantiaAuxDateTo, "99/99/99"), context.localUtil.Format( AV62DDO_ContratoGarantia_DataPagtoGarantiaAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,125);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratogarantia_datapagtogarantiaauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratogarantia_datapagtogarantiaauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratogarantia_percentual_Internalname, StringUtil.LTrim( StringUtil.NToC( AV65TFContratoGarantia_Percentual, 6, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV65TFContratoGarantia_Percentual, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,126);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratogarantia_percentual_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratogarantia_percentual_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratogarantia_percentual_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV66TFContratoGarantia_Percentual_To, 6, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV66TFContratoGarantia_Percentual_To, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,127);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratogarantia_percentual_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratogarantia_percentual_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratogarantia_datacaucao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratogarantia_datacaucao_Internalname, context.localUtil.Format(AV69TFContratoGarantia_DataCaucao, "99/99/99"), context.localUtil.Format( AV69TFContratoGarantia_DataCaucao, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,128);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratogarantia_datacaucao_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratogarantia_datacaucao_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratogarantia_datacaucao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratogarantia_datacaucao_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 129,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratogarantia_datacaucao_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratogarantia_datacaucao_to_Internalname, context.localUtil.Format(AV70TFContratoGarantia_DataCaucao_To, "99/99/99"), context.localUtil.Format( AV70TFContratoGarantia_DataCaucao_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,129);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratogarantia_datacaucao_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratogarantia_datacaucao_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratogarantia_datacaucao_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratogarantia_datacaucao_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contratogarantia_datacaucaoauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratogarantia_datacaucaoauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratogarantia_datacaucaoauxdate_Internalname, context.localUtil.Format(AV71DDO_ContratoGarantia_DataCaucaoAuxDate, "99/99/99"), context.localUtil.Format( AV71DDO_ContratoGarantia_DataCaucaoAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,131);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratogarantia_datacaucaoauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratogarantia_datacaucaoauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 132,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratogarantia_datacaucaoauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratogarantia_datacaucaoauxdateto_Internalname, context.localUtil.Format(AV72DDO_ContratoGarantia_DataCaucaoAuxDateTo, "99/99/99"), context.localUtil.Format( AV72DDO_ContratoGarantia_DataCaucaoAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,132);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratogarantia_datacaucaoauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratogarantia_datacaucaoauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratogarantia_valorgarantia_Internalname, StringUtil.LTrim( StringUtil.NToC( AV75TFContratoGarantia_ValorGarantia, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV75TFContratoGarantia_ValorGarantia, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,133);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratogarantia_valorgarantia_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratogarantia_valorgarantia_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 134,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratogarantia_valorgarantia_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV76TFContratoGarantia_ValorGarantia_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV76TFContratoGarantia_ValorGarantia_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,134);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratogarantia_valorgarantia_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratogarantia_valorgarantia_to_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratogarantia_dataencerramento_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratogarantia_dataencerramento_Internalname, context.localUtil.Format(AV79TFContratoGarantia_DataEncerramento, "99/99/99"), context.localUtil.Format( AV79TFContratoGarantia_DataEncerramento, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,135);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratogarantia_dataencerramento_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratogarantia_dataencerramento_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratogarantia_dataencerramento_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratogarantia_dataencerramento_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 136,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratogarantia_dataencerramento_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratogarantia_dataencerramento_to_Internalname, context.localUtil.Format(AV80TFContratoGarantia_DataEncerramento_To, "99/99/99"), context.localUtil.Format( AV80TFContratoGarantia_DataEncerramento_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,136);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratogarantia_dataencerramento_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratogarantia_dataencerramento_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratogarantia_dataencerramento_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratogarantia_dataencerramento_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contratogarantia_dataencerramentoauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 138,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratogarantia_dataencerramentoauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratogarantia_dataencerramentoauxdate_Internalname, context.localUtil.Format(AV81DDO_ContratoGarantia_DataEncerramentoAuxDate, "99/99/99"), context.localUtil.Format( AV81DDO_ContratoGarantia_DataEncerramentoAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,138);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratogarantia_dataencerramentoauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratogarantia_dataencerramentoauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 139,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratogarantia_dataencerramentoauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratogarantia_dataencerramentoauxdateto_Internalname, context.localUtil.Format(AV82DDO_ContratoGarantia_DataEncerramentoAuxDateTo, "99/99/99"), context.localUtil.Format( AV82DDO_ContratoGarantia_DataEncerramentoAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,139);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratogarantia_dataencerramentoauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratogarantia_dataencerramentoauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 140,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratogarantia_valorencerramento_Internalname, StringUtil.LTrim( StringUtil.NToC( AV85TFContratoGarantia_ValorEncerramento, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV85TFContratoGarantia_ValorEncerramento, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,140);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratogarantia_valorencerramento_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratogarantia_valorencerramento_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 141,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratogarantia_valorencerramento_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV86TFContratoGarantia_ValorEncerramento_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV86TFContratoGarantia_ValorEncerramento_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,141);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratogarantia_valorencerramento_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratogarantia_valorencerramento_to_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOGARANTIA_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 143,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratogarantia_codigotitlecontrolidtoreplace_Internalname, AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,143);\"", 0, edtavDdo_contratogarantia_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoGarantia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATO_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 145,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname, AV37ddo_Contrato_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,145);\"", 0, edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoGarantia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATO_NUMEROContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 147,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname, AV41ddo_Contrato_NumeroTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,147);\"", 0, edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoGarantia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATADA_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 149,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratada_codigotitlecontrolidtoreplace_Internalname, AV45ddo_Contratada_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,149);\"", 0, edtavDdo_contratada_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoGarantia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATADA_PESSOACODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 151,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratada_pessoacodtitlecontrolidtoreplace_Internalname, AV49ddo_Contratada_PessoaCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,151);\"", 0, edtavDdo_contratada_pessoacodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoGarantia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATADA_PESSOANOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 153,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname, AV53ddo_Contratada_PessoaNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,153);\"", 0, edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoGarantia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATADA_PESSOACNPJContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 155,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname, AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,155);\"", 0, edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoGarantia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 157,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratogarantia_datapagtogarantiatitlecontrolidtoreplace_Internalname, AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,157);\"", 0, edtavDdo_contratogarantia_datapagtogarantiatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoGarantia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOGARANTIA_PERCENTUALContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 159,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratogarantia_percentualtitlecontrolidtoreplace_Internalname, AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,159);\"", 0, edtavDdo_contratogarantia_percentualtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoGarantia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOGARANTIA_DATACAUCAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 161,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratogarantia_datacaucaotitlecontrolidtoreplace_Internalname, AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,161);\"", 0, edtavDdo_contratogarantia_datacaucaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoGarantia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOGARANTIA_VALORGARANTIAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 163,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratogarantia_valorgarantiatitlecontrolidtoreplace_Internalname, AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,163);\"", 0, edtavDdo_contratogarantia_valorgarantiatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoGarantia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOGARANTIA_DATAENCERRAMENTOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 165,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratogarantia_dataencerramentotitlecontrolidtoreplace_Internalname, AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,165);\"", 0, edtavDdo_contratogarantia_dataencerramentotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoGarantia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOGARANTIA_VALORENCERRAMENTOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 167,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratogarantia_valorencerramentotitlecontrolidtoreplace_Internalname, AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,167);\"", 0, edtavDdo_contratogarantia_valorencerramentotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoGarantia.htm");
         }
         wbLoad = true;
      }

      protected void START6I2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Contrato Garantia", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP6I0( ) ;
      }

      protected void WS6I2( )
      {
         START6I2( ) ;
         EVT6I2( ) ;
      }

      protected void EVT6I2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E116I2 */
                           E116I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOGARANTIA_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E126I2 */
                           E126I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATO_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E136I2 */
                           E136I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATO_NUMERO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E146I2 */
                           E146I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATADA_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E156I2 */
                           E156I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATADA_PESSOACOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E166I2 */
                           E166I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATADA_PESSOANOM.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E176I2 */
                           E176I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATADA_PESSOACNPJ.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E186I2 */
                           E186I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E196I2 */
                           E196I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOGARANTIA_PERCENTUAL.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E206I2 */
                           E206I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOGARANTIA_DATACAUCAO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E216I2 */
                           E216I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOGARANTIA_VALORGARANTIA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E226I2 */
                           E226I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E236I2 */
                           E236I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E246I2 */
                           E246I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E256I2 */
                           E256I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E266I2 */
                           E266I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E276I2 */
                           E276I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E286I2 */
                           E286I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E296I2 */
                           E296I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E306I2 */
                           E306I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E316I2 */
                           E316I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E326I2 */
                           E326I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E336I2 */
                           E336I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E346I2 */
                           E346I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_86_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
                           SubsflControlProps_862( ) ;
                           AV28Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV100Select_GXI : context.convertURL( context.PathToRelativeUrl( AV28Select))));
                           A101ContratoGarantia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoGarantia_Codigo_Internalname), ",", "."));
                           A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContrato_Codigo_Internalname), ",", "."));
                           A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
                           A39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratada_Codigo_Internalname), ",", "."));
                           A40Contratada_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContratada_PessoaCod_Internalname), ",", "."));
                           A41Contratada_PessoaNom = StringUtil.Upper( cgiGet( edtContratada_PessoaNom_Internalname));
                           n41Contratada_PessoaNom = false;
                           A42Contratada_PessoaCNPJ = cgiGet( edtContratada_PessoaCNPJ_Internalname);
                           n42Contratada_PessoaCNPJ = false;
                           A102ContratoGarantia_DataPagtoGarantia = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContratoGarantia_DataPagtoGarantia_Internalname), 0));
                           A103ContratoGarantia_Percentual = context.localUtil.CToN( cgiGet( edtContratoGarantia_Percentual_Internalname), ",", ".");
                           A104ContratoGarantia_DataCaucao = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContratoGarantia_DataCaucao_Internalname), 0));
                           A105ContratoGarantia_ValorGarantia = context.localUtil.CToN( cgiGet( edtContratoGarantia_ValorGarantia_Internalname), ",", ".");
                           A106ContratoGarantia_DataEncerramento = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContratoGarantia_DataEncerramento_Internalname), 0));
                           A107ContratoGarantia_ValorEncerramento = context.localUtil.CToN( cgiGet( edtContratoGarantia_ValorEncerramento_Internalname), ",", ".");
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E356I2 */
                                 E356I2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E366I2 */
                                 E366I2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E376I2 */
                                 E376I2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratogarantia_datapagtogarantia1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA1"), 0) != AV16ContratoGarantia_DataPagtoGarantia1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratogarantia_datapagtogarantia_to1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1"), 0) != AV17ContratoGarantia_DataPagtoGarantia_To1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratogarantia_datapagtogarantia2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA2"), 0) != AV20ContratoGarantia_DataPagtoGarantia2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratogarantia_datapagtogarantia_to2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2"), 0) != AV21ContratoGarantia_DataPagtoGarantia_To2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratogarantia_datapagtogarantia3 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA3"), 0) != AV24ContratoGarantia_DataPagtoGarantia3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratogarantia_datapagtogarantia_to3 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3"), 0) != AV25ContratoGarantia_DataPagtoGarantia_To3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratogarantia_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOGARANTIA_CODIGO"), ",", ".") != Convert.ToDecimal( AV31TFContratoGarantia_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratogarantia_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOGARANTIA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV32TFContratoGarantia_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontrato_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_CODIGO"), ",", ".") != Convert.ToDecimal( AV35TFContrato_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontrato_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV36TFContrato_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontrato_numero Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO"), AV39TFContrato_Numero) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontrato_numero_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO_SEL"), AV40TFContrato_Numero_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratada_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_CODIGO"), ",", ".") != Convert.ToDecimal( AV43TFContratada_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratada_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV44TFContratada_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratada_pessoacod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_PESSOACOD"), ",", ".") != Convert.ToDecimal( AV47TFContratada_PessoaCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratada_pessoacod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_PESSOACOD_TO"), ",", ".") != Convert.ToDecimal( AV48TFContratada_PessoaCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratada_pessoanom Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOANOM"), AV51TFContratada_PessoaNom) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratada_pessoanom_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOANOM_SEL"), AV52TFContratada_PessoaNom_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratada_pessoacnpj Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOACNPJ"), AV55TFContratada_PessoaCNPJ) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratada_pessoacnpj_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOACNPJ_SEL"), AV56TFContratada_PessoaCNPJ_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratogarantia_datapagtogarantia Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA"), 0) != AV59TFContratoGarantia_DataPagtoGarantia )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratogarantia_datapagtogarantia_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO"), 0) != AV60TFContratoGarantia_DataPagtoGarantia_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratogarantia_percentual Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOGARANTIA_PERCENTUAL"), ",", ".") != AV65TFContratoGarantia_Percentual )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratogarantia_percentual_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOGARANTIA_PERCENTUAL_TO"), ",", ".") != AV66TFContratoGarantia_Percentual_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratogarantia_datacaucao Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOGARANTIA_DATACAUCAO"), 0) != AV69TFContratoGarantia_DataCaucao )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratogarantia_datacaucao_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOGARANTIA_DATACAUCAO_TO"), 0) != AV70TFContratoGarantia_DataCaucao_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratogarantia_valorgarantia Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOGARANTIA_VALORGARANTIA"), ",", ".") != AV75TFContratoGarantia_ValorGarantia )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratogarantia_valorgarantia_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOGARANTIA_VALORGARANTIA_TO"), ",", ".") != AV76TFContratoGarantia_ValorGarantia_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratogarantia_dataencerramento Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOGARANTIA_DATAENCERRAMENTO"), 0) != AV79TFContratoGarantia_DataEncerramento )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratogarantia_dataencerramento_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO"), 0) != AV80TFContratoGarantia_DataEncerramento_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratogarantia_valorencerramento Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOGARANTIA_VALORENCERRAMENTO"), ",", ".") != AV85TFContratoGarantia_ValorEncerramento )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratogarantia_valorencerramento_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO"), ",", ".") != AV86TFContratoGarantia_ValorEncerramento_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E386I2 */
                                       E386I2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE6I2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm6I2( ) ;
            }
         }
      }

      protected void PA6I2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATOGARANTIA_DATAPAGTOGARANTIA", "da Garantia", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTRATOGARANTIA_DATAPAGTOGARANTIA", "da Garantia", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTRATOGARANTIA_DATAPAGTOGARANTIA", "da Garantia", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_862( ) ;
         while ( nGXsfl_86_idx <= nRC_GXsfl_86 )
         {
            sendrow_862( ) ;
            nGXsfl_86_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_86_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_86_idx+1));
            sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
            SubsflControlProps_862( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       DateTime AV16ContratoGarantia_DataPagtoGarantia1 ,
                                       DateTime AV17ContratoGarantia_DataPagtoGarantia_To1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       DateTime AV20ContratoGarantia_DataPagtoGarantia2 ,
                                       DateTime AV21ContratoGarantia_DataPagtoGarantia_To2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       DateTime AV24ContratoGarantia_DataPagtoGarantia3 ,
                                       DateTime AV25ContratoGarantia_DataPagtoGarantia_To3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       int AV31TFContratoGarantia_Codigo ,
                                       int AV32TFContratoGarantia_Codigo_To ,
                                       int AV35TFContrato_Codigo ,
                                       int AV36TFContrato_Codigo_To ,
                                       String AV39TFContrato_Numero ,
                                       String AV40TFContrato_Numero_Sel ,
                                       int AV43TFContratada_Codigo ,
                                       int AV44TFContratada_Codigo_To ,
                                       int AV47TFContratada_PessoaCod ,
                                       int AV48TFContratada_PessoaCod_To ,
                                       String AV51TFContratada_PessoaNom ,
                                       String AV52TFContratada_PessoaNom_Sel ,
                                       String AV55TFContratada_PessoaCNPJ ,
                                       String AV56TFContratada_PessoaCNPJ_Sel ,
                                       DateTime AV59TFContratoGarantia_DataPagtoGarantia ,
                                       DateTime AV60TFContratoGarantia_DataPagtoGarantia_To ,
                                       decimal AV65TFContratoGarantia_Percentual ,
                                       decimal AV66TFContratoGarantia_Percentual_To ,
                                       DateTime AV69TFContratoGarantia_DataCaucao ,
                                       DateTime AV70TFContratoGarantia_DataCaucao_To ,
                                       decimal AV75TFContratoGarantia_ValorGarantia ,
                                       decimal AV76TFContratoGarantia_ValorGarantia_To ,
                                       DateTime AV79TFContratoGarantia_DataEncerramento ,
                                       DateTime AV80TFContratoGarantia_DataEncerramento_To ,
                                       decimal AV85TFContratoGarantia_ValorEncerramento ,
                                       decimal AV86TFContratoGarantia_ValorEncerramento_To ,
                                       String AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace ,
                                       String AV37ddo_Contrato_CodigoTitleControlIdToReplace ,
                                       String AV41ddo_Contrato_NumeroTitleControlIdToReplace ,
                                       String AV45ddo_Contratada_CodigoTitleControlIdToReplace ,
                                       String AV49ddo_Contratada_PessoaCodTitleControlIdToReplace ,
                                       String AV53ddo_Contratada_PessoaNomTitleControlIdToReplace ,
                                       String AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace ,
                                       String AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace ,
                                       String AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace ,
                                       String AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace ,
                                       String AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace ,
                                       String AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace ,
                                       String AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace ,
                                       String AV101Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF6I2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOGARANTIA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A101ContratoGarantia_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOGARANTIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A101ContratoGarantia_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOGARANTIA_DATAPAGTOGARANTIA", GetSecureSignedToken( "", A102ContratoGarantia_DataPagtoGarantia));
         GxWebStd.gx_hidden_field( context, "CONTRATOGARANTIA_DATAPAGTOGARANTIA", context.localUtil.Format(A102ContratoGarantia_DataPagtoGarantia, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOGARANTIA_PERCENTUAL", GetSecureSignedToken( "", context.localUtil.Format( A103ContratoGarantia_Percentual, "ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "CONTRATOGARANTIA_PERCENTUAL", StringUtil.LTrim( StringUtil.NToC( A103ContratoGarantia_Percentual, 6, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOGARANTIA_DATACAUCAO", GetSecureSignedToken( "", A104ContratoGarantia_DataCaucao));
         GxWebStd.gx_hidden_field( context, "CONTRATOGARANTIA_DATACAUCAO", context.localUtil.Format(A104ContratoGarantia_DataCaucao, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOGARANTIA_VALORGARANTIA", GetSecureSignedToken( "", context.localUtil.Format( A105ContratoGarantia_ValorGarantia, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "CONTRATOGARANTIA_VALORGARANTIA", StringUtil.LTrim( StringUtil.NToC( A105ContratoGarantia_ValorGarantia, 18, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOGARANTIA_DATAENCERRAMENTO", GetSecureSignedToken( "", A106ContratoGarantia_DataEncerramento));
         GxWebStd.gx_hidden_field( context, "CONTRATOGARANTIA_DATAENCERRAMENTO", context.localUtil.Format(A106ContratoGarantia_DataEncerramento, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOGARANTIA_VALORENCERRAMENTO", GetSecureSignedToken( "", context.localUtil.Format( A107ContratoGarantia_ValorEncerramento, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "CONTRATOGARANTIA_VALORENCERRAMENTO", StringUtil.LTrim( StringUtil.NToC( A107ContratoGarantia_ValorEncerramento, 18, 5, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF6I2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV101Pgmname = "PromptContratoGarantia";
         context.Gx_err = 0;
      }

      protected void RF6I2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 86;
         /* Execute user event: E366I2 */
         E366I2 ();
         nGXsfl_86_idx = 1;
         sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
         SubsflControlProps_862( ) ;
         nGXsfl_86_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_862( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16ContratoGarantia_DataPagtoGarantia1 ,
                                                 AV17ContratoGarantia_DataPagtoGarantia_To1 ,
                                                 AV18DynamicFiltersEnabled2 ,
                                                 AV19DynamicFiltersSelector2 ,
                                                 AV20ContratoGarantia_DataPagtoGarantia2 ,
                                                 AV21ContratoGarantia_DataPagtoGarantia_To2 ,
                                                 AV22DynamicFiltersEnabled3 ,
                                                 AV23DynamicFiltersSelector3 ,
                                                 AV24ContratoGarantia_DataPagtoGarantia3 ,
                                                 AV25ContratoGarantia_DataPagtoGarantia_To3 ,
                                                 AV31TFContratoGarantia_Codigo ,
                                                 AV32TFContratoGarantia_Codigo_To ,
                                                 AV35TFContrato_Codigo ,
                                                 AV36TFContrato_Codigo_To ,
                                                 AV40TFContrato_Numero_Sel ,
                                                 AV39TFContrato_Numero ,
                                                 AV43TFContratada_Codigo ,
                                                 AV44TFContratada_Codigo_To ,
                                                 AV47TFContratada_PessoaCod ,
                                                 AV48TFContratada_PessoaCod_To ,
                                                 AV52TFContratada_PessoaNom_Sel ,
                                                 AV51TFContratada_PessoaNom ,
                                                 AV56TFContratada_PessoaCNPJ_Sel ,
                                                 AV55TFContratada_PessoaCNPJ ,
                                                 AV59TFContratoGarantia_DataPagtoGarantia ,
                                                 AV60TFContratoGarantia_DataPagtoGarantia_To ,
                                                 AV65TFContratoGarantia_Percentual ,
                                                 AV66TFContratoGarantia_Percentual_To ,
                                                 AV69TFContratoGarantia_DataCaucao ,
                                                 AV70TFContratoGarantia_DataCaucao_To ,
                                                 AV75TFContratoGarantia_ValorGarantia ,
                                                 AV76TFContratoGarantia_ValorGarantia_To ,
                                                 AV79TFContratoGarantia_DataEncerramento ,
                                                 AV80TFContratoGarantia_DataEncerramento_To ,
                                                 AV85TFContratoGarantia_ValorEncerramento ,
                                                 AV86TFContratoGarantia_ValorEncerramento_To ,
                                                 A102ContratoGarantia_DataPagtoGarantia ,
                                                 A101ContratoGarantia_Codigo ,
                                                 A74Contrato_Codigo ,
                                                 A77Contrato_Numero ,
                                                 A39Contratada_Codigo ,
                                                 A40Contratada_PessoaCod ,
                                                 A41Contratada_PessoaNom ,
                                                 A42Contratada_PessoaCNPJ ,
                                                 A103ContratoGarantia_Percentual ,
                                                 A104ContratoGarantia_DataCaucao ,
                                                 A105ContratoGarantia_ValorGarantia ,
                                                 A106ContratoGarantia_DataEncerramento ,
                                                 A107ContratoGarantia_ValorEncerramento ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DECIMAL,
                                                 TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV39TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV39TFContrato_Numero), 20, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContrato_Numero", AV39TFContrato_Numero);
            lV51TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV51TFContratada_PessoaNom), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratada_PessoaNom", AV51TFContratada_PessoaNom);
            lV55TFContratada_PessoaCNPJ = StringUtil.Concat( StringUtil.RTrim( AV55TFContratada_PessoaCNPJ), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratada_PessoaCNPJ", AV55TFContratada_PessoaCNPJ);
            /* Using cursor H006I2 */
            pr_default.execute(0, new Object[] {AV16ContratoGarantia_DataPagtoGarantia1, AV17ContratoGarantia_DataPagtoGarantia_To1, AV20ContratoGarantia_DataPagtoGarantia2, AV21ContratoGarantia_DataPagtoGarantia_To2, AV24ContratoGarantia_DataPagtoGarantia3, AV25ContratoGarantia_DataPagtoGarantia_To3, AV31TFContratoGarantia_Codigo, AV32TFContratoGarantia_Codigo_To, AV35TFContrato_Codigo, AV36TFContrato_Codigo_To, lV39TFContrato_Numero, AV40TFContrato_Numero_Sel, AV43TFContratada_Codigo, AV44TFContratada_Codigo_To, AV47TFContratada_PessoaCod, AV48TFContratada_PessoaCod_To, lV51TFContratada_PessoaNom, AV52TFContratada_PessoaNom_Sel, lV55TFContratada_PessoaCNPJ, AV56TFContratada_PessoaCNPJ_Sel, AV59TFContratoGarantia_DataPagtoGarantia, AV60TFContratoGarantia_DataPagtoGarantia_To, AV65TFContratoGarantia_Percentual, AV66TFContratoGarantia_Percentual_To, AV69TFContratoGarantia_DataCaucao, AV70TFContratoGarantia_DataCaucao_To, AV75TFContratoGarantia_ValorGarantia, AV76TFContratoGarantia_ValorGarantia_To, AV79TFContratoGarantia_DataEncerramento, AV80TFContratoGarantia_DataEncerramento_To, AV85TFContratoGarantia_ValorEncerramento, AV86TFContratoGarantia_ValorEncerramento_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_86_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A107ContratoGarantia_ValorEncerramento = H006I2_A107ContratoGarantia_ValorEncerramento[0];
               A106ContratoGarantia_DataEncerramento = H006I2_A106ContratoGarantia_DataEncerramento[0];
               A105ContratoGarantia_ValorGarantia = H006I2_A105ContratoGarantia_ValorGarantia[0];
               A104ContratoGarantia_DataCaucao = H006I2_A104ContratoGarantia_DataCaucao[0];
               A103ContratoGarantia_Percentual = H006I2_A103ContratoGarantia_Percentual[0];
               A102ContratoGarantia_DataPagtoGarantia = H006I2_A102ContratoGarantia_DataPagtoGarantia[0];
               A42Contratada_PessoaCNPJ = H006I2_A42Contratada_PessoaCNPJ[0];
               n42Contratada_PessoaCNPJ = H006I2_n42Contratada_PessoaCNPJ[0];
               A41Contratada_PessoaNom = H006I2_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H006I2_n41Contratada_PessoaNom[0];
               A40Contratada_PessoaCod = H006I2_A40Contratada_PessoaCod[0];
               A39Contratada_Codigo = H006I2_A39Contratada_Codigo[0];
               A77Contrato_Numero = H006I2_A77Contrato_Numero[0];
               A74Contrato_Codigo = H006I2_A74Contrato_Codigo[0];
               A101ContratoGarantia_Codigo = H006I2_A101ContratoGarantia_Codigo[0];
               A39Contratada_Codigo = H006I2_A39Contratada_Codigo[0];
               A77Contrato_Numero = H006I2_A77Contrato_Numero[0];
               A40Contratada_PessoaCod = H006I2_A40Contratada_PessoaCod[0];
               A42Contratada_PessoaCNPJ = H006I2_A42Contratada_PessoaCNPJ[0];
               n42Contratada_PessoaCNPJ = H006I2_n42Contratada_PessoaCNPJ[0];
               A41Contratada_PessoaNom = H006I2_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H006I2_n41Contratada_PessoaNom[0];
               /* Execute user event: E376I2 */
               E376I2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 86;
            WB6I0( ) ;
         }
         nGXsfl_86_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV16ContratoGarantia_DataPagtoGarantia1 ,
                                              AV17ContratoGarantia_DataPagtoGarantia_To1 ,
                                              AV18DynamicFiltersEnabled2 ,
                                              AV19DynamicFiltersSelector2 ,
                                              AV20ContratoGarantia_DataPagtoGarantia2 ,
                                              AV21ContratoGarantia_DataPagtoGarantia_To2 ,
                                              AV22DynamicFiltersEnabled3 ,
                                              AV23DynamicFiltersSelector3 ,
                                              AV24ContratoGarantia_DataPagtoGarantia3 ,
                                              AV25ContratoGarantia_DataPagtoGarantia_To3 ,
                                              AV31TFContratoGarantia_Codigo ,
                                              AV32TFContratoGarantia_Codigo_To ,
                                              AV35TFContrato_Codigo ,
                                              AV36TFContrato_Codigo_To ,
                                              AV40TFContrato_Numero_Sel ,
                                              AV39TFContrato_Numero ,
                                              AV43TFContratada_Codigo ,
                                              AV44TFContratada_Codigo_To ,
                                              AV47TFContratada_PessoaCod ,
                                              AV48TFContratada_PessoaCod_To ,
                                              AV52TFContratada_PessoaNom_Sel ,
                                              AV51TFContratada_PessoaNom ,
                                              AV56TFContratada_PessoaCNPJ_Sel ,
                                              AV55TFContratada_PessoaCNPJ ,
                                              AV59TFContratoGarantia_DataPagtoGarantia ,
                                              AV60TFContratoGarantia_DataPagtoGarantia_To ,
                                              AV65TFContratoGarantia_Percentual ,
                                              AV66TFContratoGarantia_Percentual_To ,
                                              AV69TFContratoGarantia_DataCaucao ,
                                              AV70TFContratoGarantia_DataCaucao_To ,
                                              AV75TFContratoGarantia_ValorGarantia ,
                                              AV76TFContratoGarantia_ValorGarantia_To ,
                                              AV79TFContratoGarantia_DataEncerramento ,
                                              AV80TFContratoGarantia_DataEncerramento_To ,
                                              AV85TFContratoGarantia_ValorEncerramento ,
                                              AV86TFContratoGarantia_ValorEncerramento_To ,
                                              A102ContratoGarantia_DataPagtoGarantia ,
                                              A101ContratoGarantia_Codigo ,
                                              A74Contrato_Codigo ,
                                              A77Contrato_Numero ,
                                              A39Contratada_Codigo ,
                                              A40Contratada_PessoaCod ,
                                              A41Contratada_PessoaNom ,
                                              A42Contratada_PessoaCNPJ ,
                                              A103ContratoGarantia_Percentual ,
                                              A104ContratoGarantia_DataCaucao ,
                                              A105ContratoGarantia_ValorGarantia ,
                                              A106ContratoGarantia_DataEncerramento ,
                                              A107ContratoGarantia_ValorEncerramento ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DECIMAL,
                                              TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV39TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV39TFContrato_Numero), 20, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContrato_Numero", AV39TFContrato_Numero);
         lV51TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV51TFContratada_PessoaNom), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratada_PessoaNom", AV51TFContratada_PessoaNom);
         lV55TFContratada_PessoaCNPJ = StringUtil.Concat( StringUtil.RTrim( AV55TFContratada_PessoaCNPJ), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratada_PessoaCNPJ", AV55TFContratada_PessoaCNPJ);
         /* Using cursor H006I3 */
         pr_default.execute(1, new Object[] {AV16ContratoGarantia_DataPagtoGarantia1, AV17ContratoGarantia_DataPagtoGarantia_To1, AV20ContratoGarantia_DataPagtoGarantia2, AV21ContratoGarantia_DataPagtoGarantia_To2, AV24ContratoGarantia_DataPagtoGarantia3, AV25ContratoGarantia_DataPagtoGarantia_To3, AV31TFContratoGarantia_Codigo, AV32TFContratoGarantia_Codigo_To, AV35TFContrato_Codigo, AV36TFContrato_Codigo_To, lV39TFContrato_Numero, AV40TFContrato_Numero_Sel, AV43TFContratada_Codigo, AV44TFContratada_Codigo_To, AV47TFContratada_PessoaCod, AV48TFContratada_PessoaCod_To, lV51TFContratada_PessoaNom, AV52TFContratada_PessoaNom_Sel, lV55TFContratada_PessoaCNPJ, AV56TFContratada_PessoaCNPJ_Sel, AV59TFContratoGarantia_DataPagtoGarantia, AV60TFContratoGarantia_DataPagtoGarantia_To, AV65TFContratoGarantia_Percentual, AV66TFContratoGarantia_Percentual_To, AV69TFContratoGarantia_DataCaucao, AV70TFContratoGarantia_DataCaucao_To, AV75TFContratoGarantia_ValorGarantia, AV76TFContratoGarantia_ValorGarantia_To, AV79TFContratoGarantia_DataEncerramento, AV80TFContratoGarantia_DataEncerramento_To, AV85TFContratoGarantia_ValorEncerramento, AV86TFContratoGarantia_ValorEncerramento_To});
         GRID_nRecordCount = H006I3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoGarantia_DataPagtoGarantia1, AV17ContratoGarantia_DataPagtoGarantia_To1, AV19DynamicFiltersSelector2, AV20ContratoGarantia_DataPagtoGarantia2, AV21ContratoGarantia_DataPagtoGarantia_To2, AV23DynamicFiltersSelector3, AV24ContratoGarantia_DataPagtoGarantia3, AV25ContratoGarantia_DataPagtoGarantia_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContratoGarantia_Codigo, AV32TFContratoGarantia_Codigo_To, AV35TFContrato_Codigo, AV36TFContrato_Codigo_To, AV39TFContrato_Numero, AV40TFContrato_Numero_Sel, AV43TFContratada_Codigo, AV44TFContratada_Codigo_To, AV47TFContratada_PessoaCod, AV48TFContratada_PessoaCod_To, AV51TFContratada_PessoaNom, AV52TFContratada_PessoaNom_Sel, AV55TFContratada_PessoaCNPJ, AV56TFContratada_PessoaCNPJ_Sel, AV59TFContratoGarantia_DataPagtoGarantia, AV60TFContratoGarantia_DataPagtoGarantia_To, AV65TFContratoGarantia_Percentual, AV66TFContratoGarantia_Percentual_To, AV69TFContratoGarantia_DataCaucao, AV70TFContratoGarantia_DataCaucao_To, AV75TFContratoGarantia_ValorGarantia, AV76TFContratoGarantia_ValorGarantia_To, AV79TFContratoGarantia_DataEncerramento, AV80TFContratoGarantia_DataEncerramento_To, AV85TFContratoGarantia_ValorEncerramento, AV86TFContratoGarantia_ValorEncerramento_To, AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace, AV37ddo_Contrato_CodigoTitleControlIdToReplace, AV41ddo_Contrato_NumeroTitleControlIdToReplace, AV45ddo_Contratada_CodigoTitleControlIdToReplace, AV49ddo_Contratada_PessoaCodTitleControlIdToReplace, AV53ddo_Contratada_PessoaNomTitleControlIdToReplace, AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace, AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace, AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace, AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace, AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace, AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace, AV101Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoGarantia_DataPagtoGarantia1, AV17ContratoGarantia_DataPagtoGarantia_To1, AV19DynamicFiltersSelector2, AV20ContratoGarantia_DataPagtoGarantia2, AV21ContratoGarantia_DataPagtoGarantia_To2, AV23DynamicFiltersSelector3, AV24ContratoGarantia_DataPagtoGarantia3, AV25ContratoGarantia_DataPagtoGarantia_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContratoGarantia_Codigo, AV32TFContratoGarantia_Codigo_To, AV35TFContrato_Codigo, AV36TFContrato_Codigo_To, AV39TFContrato_Numero, AV40TFContrato_Numero_Sel, AV43TFContratada_Codigo, AV44TFContratada_Codigo_To, AV47TFContratada_PessoaCod, AV48TFContratada_PessoaCod_To, AV51TFContratada_PessoaNom, AV52TFContratada_PessoaNom_Sel, AV55TFContratada_PessoaCNPJ, AV56TFContratada_PessoaCNPJ_Sel, AV59TFContratoGarantia_DataPagtoGarantia, AV60TFContratoGarantia_DataPagtoGarantia_To, AV65TFContratoGarantia_Percentual, AV66TFContratoGarantia_Percentual_To, AV69TFContratoGarantia_DataCaucao, AV70TFContratoGarantia_DataCaucao_To, AV75TFContratoGarantia_ValorGarantia, AV76TFContratoGarantia_ValorGarantia_To, AV79TFContratoGarantia_DataEncerramento, AV80TFContratoGarantia_DataEncerramento_To, AV85TFContratoGarantia_ValorEncerramento, AV86TFContratoGarantia_ValorEncerramento_To, AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace, AV37ddo_Contrato_CodigoTitleControlIdToReplace, AV41ddo_Contrato_NumeroTitleControlIdToReplace, AV45ddo_Contratada_CodigoTitleControlIdToReplace, AV49ddo_Contratada_PessoaCodTitleControlIdToReplace, AV53ddo_Contratada_PessoaNomTitleControlIdToReplace, AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace, AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace, AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace, AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace, AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace, AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace, AV101Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoGarantia_DataPagtoGarantia1, AV17ContratoGarantia_DataPagtoGarantia_To1, AV19DynamicFiltersSelector2, AV20ContratoGarantia_DataPagtoGarantia2, AV21ContratoGarantia_DataPagtoGarantia_To2, AV23DynamicFiltersSelector3, AV24ContratoGarantia_DataPagtoGarantia3, AV25ContratoGarantia_DataPagtoGarantia_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContratoGarantia_Codigo, AV32TFContratoGarantia_Codigo_To, AV35TFContrato_Codigo, AV36TFContrato_Codigo_To, AV39TFContrato_Numero, AV40TFContrato_Numero_Sel, AV43TFContratada_Codigo, AV44TFContratada_Codigo_To, AV47TFContratada_PessoaCod, AV48TFContratada_PessoaCod_To, AV51TFContratada_PessoaNom, AV52TFContratada_PessoaNom_Sel, AV55TFContratada_PessoaCNPJ, AV56TFContratada_PessoaCNPJ_Sel, AV59TFContratoGarantia_DataPagtoGarantia, AV60TFContratoGarantia_DataPagtoGarantia_To, AV65TFContratoGarantia_Percentual, AV66TFContratoGarantia_Percentual_To, AV69TFContratoGarantia_DataCaucao, AV70TFContratoGarantia_DataCaucao_To, AV75TFContratoGarantia_ValorGarantia, AV76TFContratoGarantia_ValorGarantia_To, AV79TFContratoGarantia_DataEncerramento, AV80TFContratoGarantia_DataEncerramento_To, AV85TFContratoGarantia_ValorEncerramento, AV86TFContratoGarantia_ValorEncerramento_To, AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace, AV37ddo_Contrato_CodigoTitleControlIdToReplace, AV41ddo_Contrato_NumeroTitleControlIdToReplace, AV45ddo_Contratada_CodigoTitleControlIdToReplace, AV49ddo_Contratada_PessoaCodTitleControlIdToReplace, AV53ddo_Contratada_PessoaNomTitleControlIdToReplace, AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace, AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace, AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace, AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace, AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace, AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace, AV101Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoGarantia_DataPagtoGarantia1, AV17ContratoGarantia_DataPagtoGarantia_To1, AV19DynamicFiltersSelector2, AV20ContratoGarantia_DataPagtoGarantia2, AV21ContratoGarantia_DataPagtoGarantia_To2, AV23DynamicFiltersSelector3, AV24ContratoGarantia_DataPagtoGarantia3, AV25ContratoGarantia_DataPagtoGarantia_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContratoGarantia_Codigo, AV32TFContratoGarantia_Codigo_To, AV35TFContrato_Codigo, AV36TFContrato_Codigo_To, AV39TFContrato_Numero, AV40TFContrato_Numero_Sel, AV43TFContratada_Codigo, AV44TFContratada_Codigo_To, AV47TFContratada_PessoaCod, AV48TFContratada_PessoaCod_To, AV51TFContratada_PessoaNom, AV52TFContratada_PessoaNom_Sel, AV55TFContratada_PessoaCNPJ, AV56TFContratada_PessoaCNPJ_Sel, AV59TFContratoGarantia_DataPagtoGarantia, AV60TFContratoGarantia_DataPagtoGarantia_To, AV65TFContratoGarantia_Percentual, AV66TFContratoGarantia_Percentual_To, AV69TFContratoGarantia_DataCaucao, AV70TFContratoGarantia_DataCaucao_To, AV75TFContratoGarantia_ValorGarantia, AV76TFContratoGarantia_ValorGarantia_To, AV79TFContratoGarantia_DataEncerramento, AV80TFContratoGarantia_DataEncerramento_To, AV85TFContratoGarantia_ValorEncerramento, AV86TFContratoGarantia_ValorEncerramento_To, AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace, AV37ddo_Contrato_CodigoTitleControlIdToReplace, AV41ddo_Contrato_NumeroTitleControlIdToReplace, AV45ddo_Contratada_CodigoTitleControlIdToReplace, AV49ddo_Contratada_PessoaCodTitleControlIdToReplace, AV53ddo_Contratada_PessoaNomTitleControlIdToReplace, AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace, AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace, AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace, AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace, AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace, AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace, AV101Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoGarantia_DataPagtoGarantia1, AV17ContratoGarantia_DataPagtoGarantia_To1, AV19DynamicFiltersSelector2, AV20ContratoGarantia_DataPagtoGarantia2, AV21ContratoGarantia_DataPagtoGarantia_To2, AV23DynamicFiltersSelector3, AV24ContratoGarantia_DataPagtoGarantia3, AV25ContratoGarantia_DataPagtoGarantia_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContratoGarantia_Codigo, AV32TFContratoGarantia_Codigo_To, AV35TFContrato_Codigo, AV36TFContrato_Codigo_To, AV39TFContrato_Numero, AV40TFContrato_Numero_Sel, AV43TFContratada_Codigo, AV44TFContratada_Codigo_To, AV47TFContratada_PessoaCod, AV48TFContratada_PessoaCod_To, AV51TFContratada_PessoaNom, AV52TFContratada_PessoaNom_Sel, AV55TFContratada_PessoaCNPJ, AV56TFContratada_PessoaCNPJ_Sel, AV59TFContratoGarantia_DataPagtoGarantia, AV60TFContratoGarantia_DataPagtoGarantia_To, AV65TFContratoGarantia_Percentual, AV66TFContratoGarantia_Percentual_To, AV69TFContratoGarantia_DataCaucao, AV70TFContratoGarantia_DataCaucao_To, AV75TFContratoGarantia_ValorGarantia, AV76TFContratoGarantia_ValorGarantia_To, AV79TFContratoGarantia_DataEncerramento, AV80TFContratoGarantia_DataEncerramento_To, AV85TFContratoGarantia_ValorEncerramento, AV86TFContratoGarantia_ValorEncerramento_To, AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace, AV37ddo_Contrato_CodigoTitleControlIdToReplace, AV41ddo_Contrato_NumeroTitleControlIdToReplace, AV45ddo_Contratada_CodigoTitleControlIdToReplace, AV49ddo_Contratada_PessoaCodTitleControlIdToReplace, AV53ddo_Contratada_PessoaNomTitleControlIdToReplace, AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace, AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace, AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace, AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace, AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace, AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace, AV101Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUP6I0( )
      {
         /* Before Start, stand alone formulas. */
         AV101Pgmname = "PromptContratoGarantia";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E356I2 */
         E356I2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV88DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOGARANTIA_CODIGOTITLEFILTERDATA"), AV30ContratoGarantia_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATO_CODIGOTITLEFILTERDATA"), AV34Contrato_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATO_NUMEROTITLEFILTERDATA"), AV38Contrato_NumeroTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATADA_CODIGOTITLEFILTERDATA"), AV42Contratada_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATADA_PESSOACODTITLEFILTERDATA"), AV46Contratada_PessoaCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATADA_PESSOANOMTITLEFILTERDATA"), AV50Contratada_PessoaNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATADA_PESSOACNPJTITLEFILTERDATA"), AV54Contratada_PessoaCNPJTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOGARANTIA_DATAPAGTOGARANTIATITLEFILTERDATA"), AV58ContratoGarantia_DataPagtoGarantiaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOGARANTIA_PERCENTUALTITLEFILTERDATA"), AV64ContratoGarantia_PercentualTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOGARANTIA_DATACAUCAOTITLEFILTERDATA"), AV68ContratoGarantia_DataCaucaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOGARANTIA_VALORGARANTIATITLEFILTERDATA"), AV74ContratoGarantia_ValorGarantiaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOGARANTIA_DATAENCERRAMENTOTITLEFILTERDATA"), AV78ContratoGarantia_DataEncerramentoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOGARANTIA_VALORENCERRAMENTOTITLEFILTERDATA"), AV84ContratoGarantia_ValorEncerramentoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratogarantia_datapagtogarantia1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Garantia_Data Pagto Garantia1"}), 1, "vCONTRATOGARANTIA_DATAPAGTOGARANTIA1");
               GX_FocusControl = edtavContratogarantia_datapagtogarantia1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV16ContratoGarantia_DataPagtoGarantia1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoGarantia_DataPagtoGarantia1", context.localUtil.Format(AV16ContratoGarantia_DataPagtoGarantia1, "99/99/99"));
            }
            else
            {
               AV16ContratoGarantia_DataPagtoGarantia1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratogarantia_datapagtogarantia1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoGarantia_DataPagtoGarantia1", context.localUtil.Format(AV16ContratoGarantia_DataPagtoGarantia1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratogarantia_datapagtogarantia_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Garantia_Data Pagto Garantia_To1"}), 1, "vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1");
               GX_FocusControl = edtavContratogarantia_datapagtogarantia_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17ContratoGarantia_DataPagtoGarantia_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoGarantia_DataPagtoGarantia_To1", context.localUtil.Format(AV17ContratoGarantia_DataPagtoGarantia_To1, "99/99/99"));
            }
            else
            {
               AV17ContratoGarantia_DataPagtoGarantia_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratogarantia_datapagtogarantia_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoGarantia_DataPagtoGarantia_To1", context.localUtil.Format(AV17ContratoGarantia_DataPagtoGarantia_To1, "99/99/99"));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratogarantia_datapagtogarantia2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Garantia_Data Pagto Garantia2"}), 1, "vCONTRATOGARANTIA_DATAPAGTOGARANTIA2");
               GX_FocusControl = edtavContratogarantia_datapagtogarantia2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20ContratoGarantia_DataPagtoGarantia2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContratoGarantia_DataPagtoGarantia2", context.localUtil.Format(AV20ContratoGarantia_DataPagtoGarantia2, "99/99/99"));
            }
            else
            {
               AV20ContratoGarantia_DataPagtoGarantia2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratogarantia_datapagtogarantia2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContratoGarantia_DataPagtoGarantia2", context.localUtil.Format(AV20ContratoGarantia_DataPagtoGarantia2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratogarantia_datapagtogarantia_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Garantia_Data Pagto Garantia_To2"}), 1, "vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2");
               GX_FocusControl = edtavContratogarantia_datapagtogarantia_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21ContratoGarantia_DataPagtoGarantia_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoGarantia_DataPagtoGarantia_To2", context.localUtil.Format(AV21ContratoGarantia_DataPagtoGarantia_To2, "99/99/99"));
            }
            else
            {
               AV21ContratoGarantia_DataPagtoGarantia_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratogarantia_datapagtogarantia_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoGarantia_DataPagtoGarantia_To2", context.localUtil.Format(AV21ContratoGarantia_DataPagtoGarantia_To2, "99/99/99"));
            }
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratogarantia_datapagtogarantia3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Garantia_Data Pagto Garantia3"}), 1, "vCONTRATOGARANTIA_DATAPAGTOGARANTIA3");
               GX_FocusControl = edtavContratogarantia_datapagtogarantia3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24ContratoGarantia_DataPagtoGarantia3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratoGarantia_DataPagtoGarantia3", context.localUtil.Format(AV24ContratoGarantia_DataPagtoGarantia3, "99/99/99"));
            }
            else
            {
               AV24ContratoGarantia_DataPagtoGarantia3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratogarantia_datapagtogarantia3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratoGarantia_DataPagtoGarantia3", context.localUtil.Format(AV24ContratoGarantia_DataPagtoGarantia3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratogarantia_datapagtogarantia_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Garantia_Data Pagto Garantia_To3"}), 1, "vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3");
               GX_FocusControl = edtavContratogarantia_datapagtogarantia_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV25ContratoGarantia_DataPagtoGarantia_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoGarantia_DataPagtoGarantia_To3", context.localUtil.Format(AV25ContratoGarantia_DataPagtoGarantia_To3, "99/99/99"));
            }
            else
            {
               AV25ContratoGarantia_DataPagtoGarantia_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratogarantia_datapagtogarantia_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoGarantia_DataPagtoGarantia_To3", context.localUtil.Format(AV25ContratoGarantia_DataPagtoGarantia_To3, "99/99/99"));
            }
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOGARANTIA_CODIGO");
               GX_FocusControl = edtavTfcontratogarantia_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV31TFContratoGarantia_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFContratoGarantia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContratoGarantia_Codigo), 6, 0)));
            }
            else
            {
               AV31TFContratoGarantia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFContratoGarantia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContratoGarantia_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOGARANTIA_CODIGO_TO");
               GX_FocusControl = edtavTfcontratogarantia_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32TFContratoGarantia_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoGarantia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoGarantia_Codigo_To), 6, 0)));
            }
            else
            {
               AV32TFContratoGarantia_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoGarantia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoGarantia_Codigo_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATO_CODIGO");
               GX_FocusControl = edtavTfcontrato_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35TFContrato_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFContrato_Codigo), 6, 0)));
            }
            else
            {
               AV35TFContrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFContrato_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATO_CODIGO_TO");
               GX_FocusControl = edtavTfcontrato_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36TFContrato_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContrato_Codigo_To), 6, 0)));
            }
            else
            {
               AV36TFContrato_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContrato_Codigo_To), 6, 0)));
            }
            AV39TFContrato_Numero = cgiGet( edtavTfcontrato_numero_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContrato_Numero", AV39TFContrato_Numero);
            AV40TFContrato_Numero_Sel = cgiGet( edtavTfcontrato_numero_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContrato_Numero_Sel", AV40TFContrato_Numero_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATADA_CODIGO");
               GX_FocusControl = edtavTfcontratada_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV43TFContratada_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFContratada_Codigo), 6, 0)));
            }
            else
            {
               AV43TFContratada_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratada_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFContratada_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATADA_CODIGO_TO");
               GX_FocusControl = edtavTfcontratada_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44TFContratada_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratada_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFContratada_Codigo_To), 6, 0)));
            }
            else
            {
               AV44TFContratada_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratada_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratada_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFContratada_Codigo_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_pessoacod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_pessoacod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATADA_PESSOACOD");
               GX_FocusControl = edtavTfcontratada_pessoacod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV47TFContratada_PessoaCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFContratada_PessoaCod), 6, 0)));
            }
            else
            {
               AV47TFContratada_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratada_pessoacod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFContratada_PessoaCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_pessoacod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_pessoacod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATADA_PESSOACOD_TO");
               GX_FocusControl = edtavTfcontratada_pessoacod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48TFContratada_PessoaCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratada_PessoaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFContratada_PessoaCod_To), 6, 0)));
            }
            else
            {
               AV48TFContratada_PessoaCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratada_pessoacod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratada_PessoaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFContratada_PessoaCod_To), 6, 0)));
            }
            AV51TFContratada_PessoaNom = StringUtil.Upper( cgiGet( edtavTfcontratada_pessoanom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratada_PessoaNom", AV51TFContratada_PessoaNom);
            AV52TFContratada_PessoaNom_Sel = StringUtil.Upper( cgiGet( edtavTfcontratada_pessoanom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContratada_PessoaNom_Sel", AV52TFContratada_PessoaNom_Sel);
            AV55TFContratada_PessoaCNPJ = cgiGet( edtavTfcontratada_pessoacnpj_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratada_PessoaCNPJ", AV55TFContratada_PessoaCNPJ);
            AV56TFContratada_PessoaCNPJ_Sel = cgiGet( edtavTfcontratada_pessoacnpj_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContratada_PessoaCNPJ_Sel", AV56TFContratada_PessoaCNPJ_Sel);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratogarantia_datapagtogarantia_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Garantia_Data Pagto Garantia"}), 1, "vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA");
               GX_FocusControl = edtavTfcontratogarantia_datapagtogarantia_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV59TFContratoGarantia_DataPagtoGarantia = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoGarantia_DataPagtoGarantia", context.localUtil.Format(AV59TFContratoGarantia_DataPagtoGarantia, "99/99/99"));
            }
            else
            {
               AV59TFContratoGarantia_DataPagtoGarantia = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratogarantia_datapagtogarantia_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoGarantia_DataPagtoGarantia", context.localUtil.Format(AV59TFContratoGarantia_DataPagtoGarantia, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratogarantia_datapagtogarantia_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Garantia_Data Pagto Garantia_To"}), 1, "vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO");
               GX_FocusControl = edtavTfcontratogarantia_datapagtogarantia_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV60TFContratoGarantia_DataPagtoGarantia_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoGarantia_DataPagtoGarantia_To", context.localUtil.Format(AV60TFContratoGarantia_DataPagtoGarantia_To, "99/99/99"));
            }
            else
            {
               AV60TFContratoGarantia_DataPagtoGarantia_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratogarantia_datapagtogarantia_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoGarantia_DataPagtoGarantia_To", context.localUtil.Format(AV60TFContratoGarantia_DataPagtoGarantia_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratogarantia_datapagtogarantiaauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Garantia_Data Pagto Garantia Aux Date"}), 1, "vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIAAUXDATE");
               GX_FocusControl = edtavDdo_contratogarantia_datapagtogarantiaauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV61DDO_ContratoGarantia_DataPagtoGarantiaAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61DDO_ContratoGarantia_DataPagtoGarantiaAuxDate", context.localUtil.Format(AV61DDO_ContratoGarantia_DataPagtoGarantiaAuxDate, "99/99/99"));
            }
            else
            {
               AV61DDO_ContratoGarantia_DataPagtoGarantiaAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratogarantia_datapagtogarantiaauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61DDO_ContratoGarantia_DataPagtoGarantiaAuxDate", context.localUtil.Format(AV61DDO_ContratoGarantia_DataPagtoGarantiaAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratogarantia_datapagtogarantiaauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Garantia_Data Pagto Garantia Aux Date To"}), 1, "vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIAAUXDATETO");
               GX_FocusControl = edtavDdo_contratogarantia_datapagtogarantiaauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV62DDO_ContratoGarantia_DataPagtoGarantiaAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62DDO_ContratoGarantia_DataPagtoGarantiaAuxDateTo", context.localUtil.Format(AV62DDO_ContratoGarantia_DataPagtoGarantiaAuxDateTo, "99/99/99"));
            }
            else
            {
               AV62DDO_ContratoGarantia_DataPagtoGarantiaAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratogarantia_datapagtogarantiaauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62DDO_ContratoGarantia_DataPagtoGarantiaAuxDateTo", context.localUtil.Format(AV62DDO_ContratoGarantia_DataPagtoGarantiaAuxDateTo, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_percentual_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_percentual_Internalname), ",", ".") > 999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOGARANTIA_PERCENTUAL");
               GX_FocusControl = edtavTfcontratogarantia_percentual_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV65TFContratoGarantia_Percentual = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratoGarantia_Percentual", StringUtil.LTrim( StringUtil.Str( AV65TFContratoGarantia_Percentual, 6, 2)));
            }
            else
            {
               AV65TFContratoGarantia_Percentual = context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_percentual_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratoGarantia_Percentual", StringUtil.LTrim( StringUtil.Str( AV65TFContratoGarantia_Percentual, 6, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_percentual_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_percentual_to_Internalname), ",", ".") > 999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOGARANTIA_PERCENTUAL_TO");
               GX_FocusControl = edtavTfcontratogarantia_percentual_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV66TFContratoGarantia_Percentual_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratoGarantia_Percentual_To", StringUtil.LTrim( StringUtil.Str( AV66TFContratoGarantia_Percentual_To, 6, 2)));
            }
            else
            {
               AV66TFContratoGarantia_Percentual_To = context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_percentual_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratoGarantia_Percentual_To", StringUtil.LTrim( StringUtil.Str( AV66TFContratoGarantia_Percentual_To, 6, 2)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratogarantia_datacaucao_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Garantia_Data Caucao"}), 1, "vTFCONTRATOGARANTIA_DATACAUCAO");
               GX_FocusControl = edtavTfcontratogarantia_datacaucao_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV69TFContratoGarantia_DataCaucao = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFContratoGarantia_DataCaucao", context.localUtil.Format(AV69TFContratoGarantia_DataCaucao, "99/99/99"));
            }
            else
            {
               AV69TFContratoGarantia_DataCaucao = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratogarantia_datacaucao_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFContratoGarantia_DataCaucao", context.localUtil.Format(AV69TFContratoGarantia_DataCaucao, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratogarantia_datacaucao_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Garantia_Data Caucao_To"}), 1, "vTFCONTRATOGARANTIA_DATACAUCAO_TO");
               GX_FocusControl = edtavTfcontratogarantia_datacaucao_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV70TFContratoGarantia_DataCaucao_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratoGarantia_DataCaucao_To", context.localUtil.Format(AV70TFContratoGarantia_DataCaucao_To, "99/99/99"));
            }
            else
            {
               AV70TFContratoGarantia_DataCaucao_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratogarantia_datacaucao_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratoGarantia_DataCaucao_To", context.localUtil.Format(AV70TFContratoGarantia_DataCaucao_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratogarantia_datacaucaoauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Garantia_Data Caucao Aux Date"}), 1, "vDDO_CONTRATOGARANTIA_DATACAUCAOAUXDATE");
               GX_FocusControl = edtavDdo_contratogarantia_datacaucaoauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV71DDO_ContratoGarantia_DataCaucaoAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71DDO_ContratoGarantia_DataCaucaoAuxDate", context.localUtil.Format(AV71DDO_ContratoGarantia_DataCaucaoAuxDate, "99/99/99"));
            }
            else
            {
               AV71DDO_ContratoGarantia_DataCaucaoAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratogarantia_datacaucaoauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71DDO_ContratoGarantia_DataCaucaoAuxDate", context.localUtil.Format(AV71DDO_ContratoGarantia_DataCaucaoAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratogarantia_datacaucaoauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Garantia_Data Caucao Aux Date To"}), 1, "vDDO_CONTRATOGARANTIA_DATACAUCAOAUXDATETO");
               GX_FocusControl = edtavDdo_contratogarantia_datacaucaoauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV72DDO_ContratoGarantia_DataCaucaoAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72DDO_ContratoGarantia_DataCaucaoAuxDateTo", context.localUtil.Format(AV72DDO_ContratoGarantia_DataCaucaoAuxDateTo, "99/99/99"));
            }
            else
            {
               AV72DDO_ContratoGarantia_DataCaucaoAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratogarantia_datacaucaoauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72DDO_ContratoGarantia_DataCaucaoAuxDateTo", context.localUtil.Format(AV72DDO_ContratoGarantia_DataCaucaoAuxDateTo, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_valorgarantia_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_valorgarantia_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOGARANTIA_VALORGARANTIA");
               GX_FocusControl = edtavTfcontratogarantia_valorgarantia_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV75TFContratoGarantia_ValorGarantia = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContratoGarantia_ValorGarantia", StringUtil.LTrim( StringUtil.Str( AV75TFContratoGarantia_ValorGarantia, 18, 5)));
            }
            else
            {
               AV75TFContratoGarantia_ValorGarantia = context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_valorgarantia_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContratoGarantia_ValorGarantia", StringUtil.LTrim( StringUtil.Str( AV75TFContratoGarantia_ValorGarantia, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_valorgarantia_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_valorgarantia_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOGARANTIA_VALORGARANTIA_TO");
               GX_FocusControl = edtavTfcontratogarantia_valorgarantia_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV76TFContratoGarantia_ValorGarantia_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContratoGarantia_ValorGarantia_To", StringUtil.LTrim( StringUtil.Str( AV76TFContratoGarantia_ValorGarantia_To, 18, 5)));
            }
            else
            {
               AV76TFContratoGarantia_ValorGarantia_To = context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_valorgarantia_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContratoGarantia_ValorGarantia_To", StringUtil.LTrim( StringUtil.Str( AV76TFContratoGarantia_ValorGarantia_To, 18, 5)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratogarantia_dataencerramento_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Garantia_Data Encerramento"}), 1, "vTFCONTRATOGARANTIA_DATAENCERRAMENTO");
               GX_FocusControl = edtavTfcontratogarantia_dataencerramento_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV79TFContratoGarantia_DataEncerramento = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFContratoGarantia_DataEncerramento", context.localUtil.Format(AV79TFContratoGarantia_DataEncerramento, "99/99/99"));
            }
            else
            {
               AV79TFContratoGarantia_DataEncerramento = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratogarantia_dataencerramento_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFContratoGarantia_DataEncerramento", context.localUtil.Format(AV79TFContratoGarantia_DataEncerramento, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratogarantia_dataencerramento_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Garantia_Data Encerramento_To"}), 1, "vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO");
               GX_FocusControl = edtavTfcontratogarantia_dataencerramento_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV80TFContratoGarantia_DataEncerramento_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContratoGarantia_DataEncerramento_To", context.localUtil.Format(AV80TFContratoGarantia_DataEncerramento_To, "99/99/99"));
            }
            else
            {
               AV80TFContratoGarantia_DataEncerramento_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratogarantia_dataencerramento_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContratoGarantia_DataEncerramento_To", context.localUtil.Format(AV80TFContratoGarantia_DataEncerramento_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratogarantia_dataencerramentoauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Garantia_Data Encerramento Aux Date"}), 1, "vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOAUXDATE");
               GX_FocusControl = edtavDdo_contratogarantia_dataencerramentoauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV81DDO_ContratoGarantia_DataEncerramentoAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81DDO_ContratoGarantia_DataEncerramentoAuxDate", context.localUtil.Format(AV81DDO_ContratoGarantia_DataEncerramentoAuxDate, "99/99/99"));
            }
            else
            {
               AV81DDO_ContratoGarantia_DataEncerramentoAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratogarantia_dataencerramentoauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81DDO_ContratoGarantia_DataEncerramentoAuxDate", context.localUtil.Format(AV81DDO_ContratoGarantia_DataEncerramentoAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratogarantia_dataencerramentoauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Garantia_Data Encerramento Aux Date To"}), 1, "vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOAUXDATETO");
               GX_FocusControl = edtavDdo_contratogarantia_dataencerramentoauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV82DDO_ContratoGarantia_DataEncerramentoAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82DDO_ContratoGarantia_DataEncerramentoAuxDateTo", context.localUtil.Format(AV82DDO_ContratoGarantia_DataEncerramentoAuxDateTo, "99/99/99"));
            }
            else
            {
               AV82DDO_ContratoGarantia_DataEncerramentoAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratogarantia_dataencerramentoauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82DDO_ContratoGarantia_DataEncerramentoAuxDateTo", context.localUtil.Format(AV82DDO_ContratoGarantia_DataEncerramentoAuxDateTo, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_valorencerramento_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_valorencerramento_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOGARANTIA_VALORENCERRAMENTO");
               GX_FocusControl = edtavTfcontratogarantia_valorencerramento_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV85TFContratoGarantia_ValorEncerramento = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFContratoGarantia_ValorEncerramento", StringUtil.LTrim( StringUtil.Str( AV85TFContratoGarantia_ValorEncerramento, 18, 5)));
            }
            else
            {
               AV85TFContratoGarantia_ValorEncerramento = context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_valorencerramento_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFContratoGarantia_ValorEncerramento", StringUtil.LTrim( StringUtil.Str( AV85TFContratoGarantia_ValorEncerramento, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_valorencerramento_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_valorencerramento_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO");
               GX_FocusControl = edtavTfcontratogarantia_valorencerramento_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV86TFContratoGarantia_ValorEncerramento_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFContratoGarantia_ValorEncerramento_To", StringUtil.LTrim( StringUtil.Str( AV86TFContratoGarantia_ValorEncerramento_To, 18, 5)));
            }
            else
            {
               AV86TFContratoGarantia_ValorEncerramento_To = context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_valorencerramento_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFContratoGarantia_ValorEncerramento_To", StringUtil.LTrim( StringUtil.Str( AV86TFContratoGarantia_ValorEncerramento_To, 18, 5)));
            }
            AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_contratogarantia_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace", AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace);
            AV37ddo_Contrato_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_Contrato_CodigoTitleControlIdToReplace", AV37ddo_Contrato_CodigoTitleControlIdToReplace);
            AV41ddo_Contrato_NumeroTitleControlIdToReplace = cgiGet( edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_Contrato_NumeroTitleControlIdToReplace", AV41ddo_Contrato_NumeroTitleControlIdToReplace);
            AV45ddo_Contratada_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_contratada_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_Contratada_CodigoTitleControlIdToReplace", AV45ddo_Contratada_CodigoTitleControlIdToReplace);
            AV49ddo_Contratada_PessoaCodTitleControlIdToReplace = cgiGet( edtavDdo_contratada_pessoacodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_Contratada_PessoaCodTitleControlIdToReplace", AV49ddo_Contratada_PessoaCodTitleControlIdToReplace);
            AV53ddo_Contratada_PessoaNomTitleControlIdToReplace = cgiGet( edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_Contratada_PessoaNomTitleControlIdToReplace", AV53ddo_Contratada_PessoaNomTitleControlIdToReplace);
            AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace = cgiGet( edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace", AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace);
            AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace = cgiGet( edtavDdo_contratogarantia_datapagtogarantiatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace", AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace);
            AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace = cgiGet( edtavDdo_contratogarantia_percentualtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace", AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace);
            AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace = cgiGet( edtavDdo_contratogarantia_datacaucaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace", AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace);
            AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace = cgiGet( edtavDdo_contratogarantia_valorgarantiatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace", AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace);
            AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace = cgiGet( edtavDdo_contratogarantia_dataencerramentotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace", AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace);
            AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace = cgiGet( edtavDdo_contratogarantia_valorencerramentotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace", AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_86 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_86"), ",", "."));
            AV90GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV91GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contratogarantia_codigo_Caption = cgiGet( "DDO_CONTRATOGARANTIA_CODIGO_Caption");
            Ddo_contratogarantia_codigo_Tooltip = cgiGet( "DDO_CONTRATOGARANTIA_CODIGO_Tooltip");
            Ddo_contratogarantia_codigo_Cls = cgiGet( "DDO_CONTRATOGARANTIA_CODIGO_Cls");
            Ddo_contratogarantia_codigo_Filteredtext_set = cgiGet( "DDO_CONTRATOGARANTIA_CODIGO_Filteredtext_set");
            Ddo_contratogarantia_codigo_Filteredtextto_set = cgiGet( "DDO_CONTRATOGARANTIA_CODIGO_Filteredtextto_set");
            Ddo_contratogarantia_codigo_Dropdownoptionstype = cgiGet( "DDO_CONTRATOGARANTIA_CODIGO_Dropdownoptionstype");
            Ddo_contratogarantia_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOGARANTIA_CODIGO_Titlecontrolidtoreplace");
            Ddo_contratogarantia_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_CODIGO_Includesortasc"));
            Ddo_contratogarantia_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_CODIGO_Includesortdsc"));
            Ddo_contratogarantia_codigo_Sortedstatus = cgiGet( "DDO_CONTRATOGARANTIA_CODIGO_Sortedstatus");
            Ddo_contratogarantia_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_CODIGO_Includefilter"));
            Ddo_contratogarantia_codigo_Filtertype = cgiGet( "DDO_CONTRATOGARANTIA_CODIGO_Filtertype");
            Ddo_contratogarantia_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_CODIGO_Filterisrange"));
            Ddo_contratogarantia_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_CODIGO_Includedatalist"));
            Ddo_contratogarantia_codigo_Datalistfixedvalues = cgiGet( "DDO_CONTRATOGARANTIA_CODIGO_Datalistfixedvalues");
            Ddo_contratogarantia_codigo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOGARANTIA_CODIGO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratogarantia_codigo_Sortasc = cgiGet( "DDO_CONTRATOGARANTIA_CODIGO_Sortasc");
            Ddo_contratogarantia_codigo_Sortdsc = cgiGet( "DDO_CONTRATOGARANTIA_CODIGO_Sortdsc");
            Ddo_contratogarantia_codigo_Loadingdata = cgiGet( "DDO_CONTRATOGARANTIA_CODIGO_Loadingdata");
            Ddo_contratogarantia_codigo_Cleanfilter = cgiGet( "DDO_CONTRATOGARANTIA_CODIGO_Cleanfilter");
            Ddo_contratogarantia_codigo_Rangefilterfrom = cgiGet( "DDO_CONTRATOGARANTIA_CODIGO_Rangefilterfrom");
            Ddo_contratogarantia_codigo_Rangefilterto = cgiGet( "DDO_CONTRATOGARANTIA_CODIGO_Rangefilterto");
            Ddo_contratogarantia_codigo_Noresultsfound = cgiGet( "DDO_CONTRATOGARANTIA_CODIGO_Noresultsfound");
            Ddo_contratogarantia_codigo_Searchbuttontext = cgiGet( "DDO_CONTRATOGARANTIA_CODIGO_Searchbuttontext");
            Ddo_contrato_codigo_Caption = cgiGet( "DDO_CONTRATO_CODIGO_Caption");
            Ddo_contrato_codigo_Tooltip = cgiGet( "DDO_CONTRATO_CODIGO_Tooltip");
            Ddo_contrato_codigo_Cls = cgiGet( "DDO_CONTRATO_CODIGO_Cls");
            Ddo_contrato_codigo_Filteredtext_set = cgiGet( "DDO_CONTRATO_CODIGO_Filteredtext_set");
            Ddo_contrato_codigo_Filteredtextto_set = cgiGet( "DDO_CONTRATO_CODIGO_Filteredtextto_set");
            Ddo_contrato_codigo_Dropdownoptionstype = cgiGet( "DDO_CONTRATO_CODIGO_Dropdownoptionstype");
            Ddo_contrato_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATO_CODIGO_Titlecontrolidtoreplace");
            Ddo_contrato_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Includesortasc"));
            Ddo_contrato_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Includesortdsc"));
            Ddo_contrato_codigo_Sortedstatus = cgiGet( "DDO_CONTRATO_CODIGO_Sortedstatus");
            Ddo_contrato_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Includefilter"));
            Ddo_contrato_codigo_Filtertype = cgiGet( "DDO_CONTRATO_CODIGO_Filtertype");
            Ddo_contrato_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Filterisrange"));
            Ddo_contrato_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Includedatalist"));
            Ddo_contrato_codigo_Datalistfixedvalues = cgiGet( "DDO_CONTRATO_CODIGO_Datalistfixedvalues");
            Ddo_contrato_codigo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATO_CODIGO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contrato_codigo_Sortasc = cgiGet( "DDO_CONTRATO_CODIGO_Sortasc");
            Ddo_contrato_codigo_Sortdsc = cgiGet( "DDO_CONTRATO_CODIGO_Sortdsc");
            Ddo_contrato_codigo_Loadingdata = cgiGet( "DDO_CONTRATO_CODIGO_Loadingdata");
            Ddo_contrato_codigo_Cleanfilter = cgiGet( "DDO_CONTRATO_CODIGO_Cleanfilter");
            Ddo_contrato_codigo_Rangefilterfrom = cgiGet( "DDO_CONTRATO_CODIGO_Rangefilterfrom");
            Ddo_contrato_codigo_Rangefilterto = cgiGet( "DDO_CONTRATO_CODIGO_Rangefilterto");
            Ddo_contrato_codigo_Noresultsfound = cgiGet( "DDO_CONTRATO_CODIGO_Noresultsfound");
            Ddo_contrato_codigo_Searchbuttontext = cgiGet( "DDO_CONTRATO_CODIGO_Searchbuttontext");
            Ddo_contrato_numero_Caption = cgiGet( "DDO_CONTRATO_NUMERO_Caption");
            Ddo_contrato_numero_Tooltip = cgiGet( "DDO_CONTRATO_NUMERO_Tooltip");
            Ddo_contrato_numero_Cls = cgiGet( "DDO_CONTRATO_NUMERO_Cls");
            Ddo_contrato_numero_Filteredtext_set = cgiGet( "DDO_CONTRATO_NUMERO_Filteredtext_set");
            Ddo_contrato_numero_Selectedvalue_set = cgiGet( "DDO_CONTRATO_NUMERO_Selectedvalue_set");
            Ddo_contrato_numero_Dropdownoptionstype = cgiGet( "DDO_CONTRATO_NUMERO_Dropdownoptionstype");
            Ddo_contrato_numero_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATO_NUMERO_Titlecontrolidtoreplace");
            Ddo_contrato_numero_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includesortasc"));
            Ddo_contrato_numero_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includesortdsc"));
            Ddo_contrato_numero_Sortedstatus = cgiGet( "DDO_CONTRATO_NUMERO_Sortedstatus");
            Ddo_contrato_numero_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includefilter"));
            Ddo_contrato_numero_Filtertype = cgiGet( "DDO_CONTRATO_NUMERO_Filtertype");
            Ddo_contrato_numero_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Filterisrange"));
            Ddo_contrato_numero_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includedatalist"));
            Ddo_contrato_numero_Datalisttype = cgiGet( "DDO_CONTRATO_NUMERO_Datalisttype");
            Ddo_contrato_numero_Datalistfixedvalues = cgiGet( "DDO_CONTRATO_NUMERO_Datalistfixedvalues");
            Ddo_contrato_numero_Datalistproc = cgiGet( "DDO_CONTRATO_NUMERO_Datalistproc");
            Ddo_contrato_numero_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATO_NUMERO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contrato_numero_Sortasc = cgiGet( "DDO_CONTRATO_NUMERO_Sortasc");
            Ddo_contrato_numero_Sortdsc = cgiGet( "DDO_CONTRATO_NUMERO_Sortdsc");
            Ddo_contrato_numero_Loadingdata = cgiGet( "DDO_CONTRATO_NUMERO_Loadingdata");
            Ddo_contrato_numero_Cleanfilter = cgiGet( "DDO_CONTRATO_NUMERO_Cleanfilter");
            Ddo_contrato_numero_Rangefilterfrom = cgiGet( "DDO_CONTRATO_NUMERO_Rangefilterfrom");
            Ddo_contrato_numero_Rangefilterto = cgiGet( "DDO_CONTRATO_NUMERO_Rangefilterto");
            Ddo_contrato_numero_Noresultsfound = cgiGet( "DDO_CONTRATO_NUMERO_Noresultsfound");
            Ddo_contrato_numero_Searchbuttontext = cgiGet( "DDO_CONTRATO_NUMERO_Searchbuttontext");
            Ddo_contratada_codigo_Caption = cgiGet( "DDO_CONTRATADA_CODIGO_Caption");
            Ddo_contratada_codigo_Tooltip = cgiGet( "DDO_CONTRATADA_CODIGO_Tooltip");
            Ddo_contratada_codigo_Cls = cgiGet( "DDO_CONTRATADA_CODIGO_Cls");
            Ddo_contratada_codigo_Filteredtext_set = cgiGet( "DDO_CONTRATADA_CODIGO_Filteredtext_set");
            Ddo_contratada_codigo_Filteredtextto_set = cgiGet( "DDO_CONTRATADA_CODIGO_Filteredtextto_set");
            Ddo_contratada_codigo_Dropdownoptionstype = cgiGet( "DDO_CONTRATADA_CODIGO_Dropdownoptionstype");
            Ddo_contratada_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATADA_CODIGO_Titlecontrolidtoreplace");
            Ddo_contratada_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_CODIGO_Includesortasc"));
            Ddo_contratada_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_CODIGO_Includesortdsc"));
            Ddo_contratada_codigo_Sortedstatus = cgiGet( "DDO_CONTRATADA_CODIGO_Sortedstatus");
            Ddo_contratada_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_CODIGO_Includefilter"));
            Ddo_contratada_codigo_Filtertype = cgiGet( "DDO_CONTRATADA_CODIGO_Filtertype");
            Ddo_contratada_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_CODIGO_Filterisrange"));
            Ddo_contratada_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_CODIGO_Includedatalist"));
            Ddo_contratada_codigo_Datalistfixedvalues = cgiGet( "DDO_CONTRATADA_CODIGO_Datalistfixedvalues");
            Ddo_contratada_codigo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATADA_CODIGO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratada_codigo_Sortasc = cgiGet( "DDO_CONTRATADA_CODIGO_Sortasc");
            Ddo_contratada_codigo_Sortdsc = cgiGet( "DDO_CONTRATADA_CODIGO_Sortdsc");
            Ddo_contratada_codigo_Loadingdata = cgiGet( "DDO_CONTRATADA_CODIGO_Loadingdata");
            Ddo_contratada_codigo_Cleanfilter = cgiGet( "DDO_CONTRATADA_CODIGO_Cleanfilter");
            Ddo_contratada_codigo_Rangefilterfrom = cgiGet( "DDO_CONTRATADA_CODIGO_Rangefilterfrom");
            Ddo_contratada_codigo_Rangefilterto = cgiGet( "DDO_CONTRATADA_CODIGO_Rangefilterto");
            Ddo_contratada_codigo_Noresultsfound = cgiGet( "DDO_CONTRATADA_CODIGO_Noresultsfound");
            Ddo_contratada_codigo_Searchbuttontext = cgiGet( "DDO_CONTRATADA_CODIGO_Searchbuttontext");
            Ddo_contratada_pessoacod_Caption = cgiGet( "DDO_CONTRATADA_PESSOACOD_Caption");
            Ddo_contratada_pessoacod_Tooltip = cgiGet( "DDO_CONTRATADA_PESSOACOD_Tooltip");
            Ddo_contratada_pessoacod_Cls = cgiGet( "DDO_CONTRATADA_PESSOACOD_Cls");
            Ddo_contratada_pessoacod_Filteredtext_set = cgiGet( "DDO_CONTRATADA_PESSOACOD_Filteredtext_set");
            Ddo_contratada_pessoacod_Filteredtextto_set = cgiGet( "DDO_CONTRATADA_PESSOACOD_Filteredtextto_set");
            Ddo_contratada_pessoacod_Dropdownoptionstype = cgiGet( "DDO_CONTRATADA_PESSOACOD_Dropdownoptionstype");
            Ddo_contratada_pessoacod_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATADA_PESSOACOD_Titlecontrolidtoreplace");
            Ddo_contratada_pessoacod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACOD_Includesortasc"));
            Ddo_contratada_pessoacod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACOD_Includesortdsc"));
            Ddo_contratada_pessoacod_Sortedstatus = cgiGet( "DDO_CONTRATADA_PESSOACOD_Sortedstatus");
            Ddo_contratada_pessoacod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACOD_Includefilter"));
            Ddo_contratada_pessoacod_Filtertype = cgiGet( "DDO_CONTRATADA_PESSOACOD_Filtertype");
            Ddo_contratada_pessoacod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACOD_Filterisrange"));
            Ddo_contratada_pessoacod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACOD_Includedatalist"));
            Ddo_contratada_pessoacod_Datalistfixedvalues = cgiGet( "DDO_CONTRATADA_PESSOACOD_Datalistfixedvalues");
            Ddo_contratada_pessoacod_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATADA_PESSOACOD_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratada_pessoacod_Sortasc = cgiGet( "DDO_CONTRATADA_PESSOACOD_Sortasc");
            Ddo_contratada_pessoacod_Sortdsc = cgiGet( "DDO_CONTRATADA_PESSOACOD_Sortdsc");
            Ddo_contratada_pessoacod_Loadingdata = cgiGet( "DDO_CONTRATADA_PESSOACOD_Loadingdata");
            Ddo_contratada_pessoacod_Cleanfilter = cgiGet( "DDO_CONTRATADA_PESSOACOD_Cleanfilter");
            Ddo_contratada_pessoacod_Rangefilterfrom = cgiGet( "DDO_CONTRATADA_PESSOACOD_Rangefilterfrom");
            Ddo_contratada_pessoacod_Rangefilterto = cgiGet( "DDO_CONTRATADA_PESSOACOD_Rangefilterto");
            Ddo_contratada_pessoacod_Noresultsfound = cgiGet( "DDO_CONTRATADA_PESSOACOD_Noresultsfound");
            Ddo_contratada_pessoacod_Searchbuttontext = cgiGet( "DDO_CONTRATADA_PESSOACOD_Searchbuttontext");
            Ddo_contratada_pessoanom_Caption = cgiGet( "DDO_CONTRATADA_PESSOANOM_Caption");
            Ddo_contratada_pessoanom_Tooltip = cgiGet( "DDO_CONTRATADA_PESSOANOM_Tooltip");
            Ddo_contratada_pessoanom_Cls = cgiGet( "DDO_CONTRATADA_PESSOANOM_Cls");
            Ddo_contratada_pessoanom_Filteredtext_set = cgiGet( "DDO_CONTRATADA_PESSOANOM_Filteredtext_set");
            Ddo_contratada_pessoanom_Selectedvalue_set = cgiGet( "DDO_CONTRATADA_PESSOANOM_Selectedvalue_set");
            Ddo_contratada_pessoanom_Dropdownoptionstype = cgiGet( "DDO_CONTRATADA_PESSOANOM_Dropdownoptionstype");
            Ddo_contratada_pessoanom_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATADA_PESSOANOM_Titlecontrolidtoreplace");
            Ddo_contratada_pessoanom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Includesortasc"));
            Ddo_contratada_pessoanom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Includesortdsc"));
            Ddo_contratada_pessoanom_Sortedstatus = cgiGet( "DDO_CONTRATADA_PESSOANOM_Sortedstatus");
            Ddo_contratada_pessoanom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Includefilter"));
            Ddo_contratada_pessoanom_Filtertype = cgiGet( "DDO_CONTRATADA_PESSOANOM_Filtertype");
            Ddo_contratada_pessoanom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Filterisrange"));
            Ddo_contratada_pessoanom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Includedatalist"));
            Ddo_contratada_pessoanom_Datalisttype = cgiGet( "DDO_CONTRATADA_PESSOANOM_Datalisttype");
            Ddo_contratada_pessoanom_Datalistfixedvalues = cgiGet( "DDO_CONTRATADA_PESSOANOM_Datalistfixedvalues");
            Ddo_contratada_pessoanom_Datalistproc = cgiGet( "DDO_CONTRATADA_PESSOANOM_Datalistproc");
            Ddo_contratada_pessoanom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATADA_PESSOANOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratada_pessoanom_Sortasc = cgiGet( "DDO_CONTRATADA_PESSOANOM_Sortasc");
            Ddo_contratada_pessoanom_Sortdsc = cgiGet( "DDO_CONTRATADA_PESSOANOM_Sortdsc");
            Ddo_contratada_pessoanom_Loadingdata = cgiGet( "DDO_CONTRATADA_PESSOANOM_Loadingdata");
            Ddo_contratada_pessoanom_Cleanfilter = cgiGet( "DDO_CONTRATADA_PESSOANOM_Cleanfilter");
            Ddo_contratada_pessoanom_Rangefilterfrom = cgiGet( "DDO_CONTRATADA_PESSOANOM_Rangefilterfrom");
            Ddo_contratada_pessoanom_Rangefilterto = cgiGet( "DDO_CONTRATADA_PESSOANOM_Rangefilterto");
            Ddo_contratada_pessoanom_Noresultsfound = cgiGet( "DDO_CONTRATADA_PESSOANOM_Noresultsfound");
            Ddo_contratada_pessoanom_Searchbuttontext = cgiGet( "DDO_CONTRATADA_PESSOANOM_Searchbuttontext");
            Ddo_contratada_pessoacnpj_Caption = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Caption");
            Ddo_contratada_pessoacnpj_Tooltip = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Tooltip");
            Ddo_contratada_pessoacnpj_Cls = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Cls");
            Ddo_contratada_pessoacnpj_Filteredtext_set = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Filteredtext_set");
            Ddo_contratada_pessoacnpj_Selectedvalue_set = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Selectedvalue_set");
            Ddo_contratada_pessoacnpj_Dropdownoptionstype = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Dropdownoptionstype");
            Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Titlecontrolidtoreplace");
            Ddo_contratada_pessoacnpj_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Includesortasc"));
            Ddo_contratada_pessoacnpj_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Includesortdsc"));
            Ddo_contratada_pessoacnpj_Sortedstatus = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Sortedstatus");
            Ddo_contratada_pessoacnpj_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Includefilter"));
            Ddo_contratada_pessoacnpj_Filtertype = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Filtertype");
            Ddo_contratada_pessoacnpj_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Filterisrange"));
            Ddo_contratada_pessoacnpj_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Includedatalist"));
            Ddo_contratada_pessoacnpj_Datalisttype = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Datalisttype");
            Ddo_contratada_pessoacnpj_Datalistfixedvalues = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Datalistfixedvalues");
            Ddo_contratada_pessoacnpj_Datalistproc = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Datalistproc");
            Ddo_contratada_pessoacnpj_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratada_pessoacnpj_Sortasc = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Sortasc");
            Ddo_contratada_pessoacnpj_Sortdsc = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Sortdsc");
            Ddo_contratada_pessoacnpj_Loadingdata = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Loadingdata");
            Ddo_contratada_pessoacnpj_Cleanfilter = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Cleanfilter");
            Ddo_contratada_pessoacnpj_Rangefilterfrom = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Rangefilterfrom");
            Ddo_contratada_pessoacnpj_Rangefilterto = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Rangefilterto");
            Ddo_contratada_pessoacnpj_Noresultsfound = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Noresultsfound");
            Ddo_contratada_pessoacnpj_Searchbuttontext = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Searchbuttontext");
            Ddo_contratogarantia_datapagtogarantia_Caption = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Caption");
            Ddo_contratogarantia_datapagtogarantia_Tooltip = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Tooltip");
            Ddo_contratogarantia_datapagtogarantia_Cls = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Cls");
            Ddo_contratogarantia_datapagtogarantia_Filteredtext_set = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Filteredtext_set");
            Ddo_contratogarantia_datapagtogarantia_Filteredtextto_set = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Filteredtextto_set");
            Ddo_contratogarantia_datapagtogarantia_Dropdownoptionstype = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Dropdownoptionstype");
            Ddo_contratogarantia_datapagtogarantia_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Titlecontrolidtoreplace");
            Ddo_contratogarantia_datapagtogarantia_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Includesortasc"));
            Ddo_contratogarantia_datapagtogarantia_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Includesortdsc"));
            Ddo_contratogarantia_datapagtogarantia_Sortedstatus = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Sortedstatus");
            Ddo_contratogarantia_datapagtogarantia_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Includefilter"));
            Ddo_contratogarantia_datapagtogarantia_Filtertype = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Filtertype");
            Ddo_contratogarantia_datapagtogarantia_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Filterisrange"));
            Ddo_contratogarantia_datapagtogarantia_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Includedatalist"));
            Ddo_contratogarantia_datapagtogarantia_Datalistfixedvalues = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Datalistfixedvalues");
            Ddo_contratogarantia_datapagtogarantia_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratogarantia_datapagtogarantia_Sortasc = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Sortasc");
            Ddo_contratogarantia_datapagtogarantia_Sortdsc = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Sortdsc");
            Ddo_contratogarantia_datapagtogarantia_Loadingdata = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Loadingdata");
            Ddo_contratogarantia_datapagtogarantia_Cleanfilter = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Cleanfilter");
            Ddo_contratogarantia_datapagtogarantia_Rangefilterfrom = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Rangefilterfrom");
            Ddo_contratogarantia_datapagtogarantia_Rangefilterto = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Rangefilterto");
            Ddo_contratogarantia_datapagtogarantia_Noresultsfound = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Noresultsfound");
            Ddo_contratogarantia_datapagtogarantia_Searchbuttontext = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Searchbuttontext");
            Ddo_contratogarantia_percentual_Caption = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Caption");
            Ddo_contratogarantia_percentual_Tooltip = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Tooltip");
            Ddo_contratogarantia_percentual_Cls = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Cls");
            Ddo_contratogarantia_percentual_Filteredtext_set = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Filteredtext_set");
            Ddo_contratogarantia_percentual_Filteredtextto_set = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Filteredtextto_set");
            Ddo_contratogarantia_percentual_Dropdownoptionstype = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Dropdownoptionstype");
            Ddo_contratogarantia_percentual_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Titlecontrolidtoreplace");
            Ddo_contratogarantia_percentual_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Includesortasc"));
            Ddo_contratogarantia_percentual_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Includesortdsc"));
            Ddo_contratogarantia_percentual_Sortedstatus = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Sortedstatus");
            Ddo_contratogarantia_percentual_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Includefilter"));
            Ddo_contratogarantia_percentual_Filtertype = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Filtertype");
            Ddo_contratogarantia_percentual_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Filterisrange"));
            Ddo_contratogarantia_percentual_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Includedatalist"));
            Ddo_contratogarantia_percentual_Datalistfixedvalues = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Datalistfixedvalues");
            Ddo_contratogarantia_percentual_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratogarantia_percentual_Sortasc = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Sortasc");
            Ddo_contratogarantia_percentual_Sortdsc = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Sortdsc");
            Ddo_contratogarantia_percentual_Loadingdata = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Loadingdata");
            Ddo_contratogarantia_percentual_Cleanfilter = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Cleanfilter");
            Ddo_contratogarantia_percentual_Rangefilterfrom = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Rangefilterfrom");
            Ddo_contratogarantia_percentual_Rangefilterto = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Rangefilterto");
            Ddo_contratogarantia_percentual_Noresultsfound = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Noresultsfound");
            Ddo_contratogarantia_percentual_Searchbuttontext = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Searchbuttontext");
            Ddo_contratogarantia_datacaucao_Caption = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Caption");
            Ddo_contratogarantia_datacaucao_Tooltip = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Tooltip");
            Ddo_contratogarantia_datacaucao_Cls = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Cls");
            Ddo_contratogarantia_datacaucao_Filteredtext_set = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Filteredtext_set");
            Ddo_contratogarantia_datacaucao_Filteredtextto_set = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Filteredtextto_set");
            Ddo_contratogarantia_datacaucao_Dropdownoptionstype = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Dropdownoptionstype");
            Ddo_contratogarantia_datacaucao_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Titlecontrolidtoreplace");
            Ddo_contratogarantia_datacaucao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Includesortasc"));
            Ddo_contratogarantia_datacaucao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Includesortdsc"));
            Ddo_contratogarantia_datacaucao_Sortedstatus = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Sortedstatus");
            Ddo_contratogarantia_datacaucao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Includefilter"));
            Ddo_contratogarantia_datacaucao_Filtertype = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Filtertype");
            Ddo_contratogarantia_datacaucao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Filterisrange"));
            Ddo_contratogarantia_datacaucao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Includedatalist"));
            Ddo_contratogarantia_datacaucao_Datalistfixedvalues = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Datalistfixedvalues");
            Ddo_contratogarantia_datacaucao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratogarantia_datacaucao_Sortasc = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Sortasc");
            Ddo_contratogarantia_datacaucao_Sortdsc = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Sortdsc");
            Ddo_contratogarantia_datacaucao_Loadingdata = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Loadingdata");
            Ddo_contratogarantia_datacaucao_Cleanfilter = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Cleanfilter");
            Ddo_contratogarantia_datacaucao_Rangefilterfrom = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Rangefilterfrom");
            Ddo_contratogarantia_datacaucao_Rangefilterto = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Rangefilterto");
            Ddo_contratogarantia_datacaucao_Noresultsfound = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Noresultsfound");
            Ddo_contratogarantia_datacaucao_Searchbuttontext = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Searchbuttontext");
            Ddo_contratogarantia_valorgarantia_Caption = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Caption");
            Ddo_contratogarantia_valorgarantia_Tooltip = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Tooltip");
            Ddo_contratogarantia_valorgarantia_Cls = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Cls");
            Ddo_contratogarantia_valorgarantia_Filteredtext_set = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Filteredtext_set");
            Ddo_contratogarantia_valorgarantia_Filteredtextto_set = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Filteredtextto_set");
            Ddo_contratogarantia_valorgarantia_Dropdownoptionstype = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Dropdownoptionstype");
            Ddo_contratogarantia_valorgarantia_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Titlecontrolidtoreplace");
            Ddo_contratogarantia_valorgarantia_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Includesortasc"));
            Ddo_contratogarantia_valorgarantia_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Includesortdsc"));
            Ddo_contratogarantia_valorgarantia_Sortedstatus = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Sortedstatus");
            Ddo_contratogarantia_valorgarantia_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Includefilter"));
            Ddo_contratogarantia_valorgarantia_Filtertype = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Filtertype");
            Ddo_contratogarantia_valorgarantia_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Filterisrange"));
            Ddo_contratogarantia_valorgarantia_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Includedatalist"));
            Ddo_contratogarantia_valorgarantia_Datalistfixedvalues = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Datalistfixedvalues");
            Ddo_contratogarantia_valorgarantia_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratogarantia_valorgarantia_Sortasc = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Sortasc");
            Ddo_contratogarantia_valorgarantia_Sortdsc = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Sortdsc");
            Ddo_contratogarantia_valorgarantia_Loadingdata = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Loadingdata");
            Ddo_contratogarantia_valorgarantia_Cleanfilter = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Cleanfilter");
            Ddo_contratogarantia_valorgarantia_Rangefilterfrom = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Rangefilterfrom");
            Ddo_contratogarantia_valorgarantia_Rangefilterto = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Rangefilterto");
            Ddo_contratogarantia_valorgarantia_Noresultsfound = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Noresultsfound");
            Ddo_contratogarantia_valorgarantia_Searchbuttontext = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Searchbuttontext");
            Ddo_contratogarantia_dataencerramento_Caption = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Caption");
            Ddo_contratogarantia_dataencerramento_Tooltip = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Tooltip");
            Ddo_contratogarantia_dataencerramento_Cls = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Cls");
            Ddo_contratogarantia_dataencerramento_Filteredtext_set = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Filteredtext_set");
            Ddo_contratogarantia_dataencerramento_Filteredtextto_set = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Filteredtextto_set");
            Ddo_contratogarantia_dataencerramento_Dropdownoptionstype = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Dropdownoptionstype");
            Ddo_contratogarantia_dataencerramento_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Titlecontrolidtoreplace");
            Ddo_contratogarantia_dataencerramento_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Includesortasc"));
            Ddo_contratogarantia_dataencerramento_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Includesortdsc"));
            Ddo_contratogarantia_dataencerramento_Sortedstatus = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Sortedstatus");
            Ddo_contratogarantia_dataencerramento_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Includefilter"));
            Ddo_contratogarantia_dataencerramento_Filtertype = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Filtertype");
            Ddo_contratogarantia_dataencerramento_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Filterisrange"));
            Ddo_contratogarantia_dataencerramento_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Includedatalist"));
            Ddo_contratogarantia_dataencerramento_Datalistfixedvalues = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Datalistfixedvalues");
            Ddo_contratogarantia_dataencerramento_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratogarantia_dataencerramento_Sortasc = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Sortasc");
            Ddo_contratogarantia_dataencerramento_Sortdsc = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Sortdsc");
            Ddo_contratogarantia_dataencerramento_Loadingdata = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Loadingdata");
            Ddo_contratogarantia_dataencerramento_Cleanfilter = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Cleanfilter");
            Ddo_contratogarantia_dataencerramento_Rangefilterfrom = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Rangefilterfrom");
            Ddo_contratogarantia_dataencerramento_Rangefilterto = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Rangefilterto");
            Ddo_contratogarantia_dataencerramento_Noresultsfound = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Noresultsfound");
            Ddo_contratogarantia_dataencerramento_Searchbuttontext = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Searchbuttontext");
            Ddo_contratogarantia_valorencerramento_Caption = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Caption");
            Ddo_contratogarantia_valorencerramento_Tooltip = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Tooltip");
            Ddo_contratogarantia_valorencerramento_Cls = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Cls");
            Ddo_contratogarantia_valorencerramento_Filteredtext_set = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Filteredtext_set");
            Ddo_contratogarantia_valorencerramento_Filteredtextto_set = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Filteredtextto_set");
            Ddo_contratogarantia_valorencerramento_Dropdownoptionstype = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Dropdownoptionstype");
            Ddo_contratogarantia_valorencerramento_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Titlecontrolidtoreplace");
            Ddo_contratogarantia_valorencerramento_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Includesortasc"));
            Ddo_contratogarantia_valorencerramento_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Includesortdsc"));
            Ddo_contratogarantia_valorencerramento_Sortedstatus = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Sortedstatus");
            Ddo_contratogarantia_valorencerramento_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Includefilter"));
            Ddo_contratogarantia_valorencerramento_Filtertype = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Filtertype");
            Ddo_contratogarantia_valorencerramento_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Filterisrange"));
            Ddo_contratogarantia_valorencerramento_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Includedatalist"));
            Ddo_contratogarantia_valorencerramento_Datalistfixedvalues = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Datalistfixedvalues");
            Ddo_contratogarantia_valorencerramento_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratogarantia_valorencerramento_Sortasc = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Sortasc");
            Ddo_contratogarantia_valorencerramento_Sortdsc = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Sortdsc");
            Ddo_contratogarantia_valorencerramento_Loadingdata = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Loadingdata");
            Ddo_contratogarantia_valorencerramento_Cleanfilter = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Cleanfilter");
            Ddo_contratogarantia_valorencerramento_Rangefilterfrom = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Rangefilterfrom");
            Ddo_contratogarantia_valorencerramento_Rangefilterto = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Rangefilterto");
            Ddo_contratogarantia_valorencerramento_Noresultsfound = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Noresultsfound");
            Ddo_contratogarantia_valorencerramento_Searchbuttontext = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contratogarantia_codigo_Activeeventkey = cgiGet( "DDO_CONTRATOGARANTIA_CODIGO_Activeeventkey");
            Ddo_contratogarantia_codigo_Filteredtext_get = cgiGet( "DDO_CONTRATOGARANTIA_CODIGO_Filteredtext_get");
            Ddo_contratogarantia_codigo_Filteredtextto_get = cgiGet( "DDO_CONTRATOGARANTIA_CODIGO_Filteredtextto_get");
            Ddo_contrato_codigo_Activeeventkey = cgiGet( "DDO_CONTRATO_CODIGO_Activeeventkey");
            Ddo_contrato_codigo_Filteredtext_get = cgiGet( "DDO_CONTRATO_CODIGO_Filteredtext_get");
            Ddo_contrato_codigo_Filteredtextto_get = cgiGet( "DDO_CONTRATO_CODIGO_Filteredtextto_get");
            Ddo_contrato_numero_Activeeventkey = cgiGet( "DDO_CONTRATO_NUMERO_Activeeventkey");
            Ddo_contrato_numero_Filteredtext_get = cgiGet( "DDO_CONTRATO_NUMERO_Filteredtext_get");
            Ddo_contrato_numero_Selectedvalue_get = cgiGet( "DDO_CONTRATO_NUMERO_Selectedvalue_get");
            Ddo_contratada_codigo_Activeeventkey = cgiGet( "DDO_CONTRATADA_CODIGO_Activeeventkey");
            Ddo_contratada_codigo_Filteredtext_get = cgiGet( "DDO_CONTRATADA_CODIGO_Filteredtext_get");
            Ddo_contratada_codigo_Filteredtextto_get = cgiGet( "DDO_CONTRATADA_CODIGO_Filteredtextto_get");
            Ddo_contratada_pessoacod_Activeeventkey = cgiGet( "DDO_CONTRATADA_PESSOACOD_Activeeventkey");
            Ddo_contratada_pessoacod_Filteredtext_get = cgiGet( "DDO_CONTRATADA_PESSOACOD_Filteredtext_get");
            Ddo_contratada_pessoacod_Filteredtextto_get = cgiGet( "DDO_CONTRATADA_PESSOACOD_Filteredtextto_get");
            Ddo_contratada_pessoanom_Activeeventkey = cgiGet( "DDO_CONTRATADA_PESSOANOM_Activeeventkey");
            Ddo_contratada_pessoanom_Filteredtext_get = cgiGet( "DDO_CONTRATADA_PESSOANOM_Filteredtext_get");
            Ddo_contratada_pessoanom_Selectedvalue_get = cgiGet( "DDO_CONTRATADA_PESSOANOM_Selectedvalue_get");
            Ddo_contratada_pessoacnpj_Activeeventkey = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Activeeventkey");
            Ddo_contratada_pessoacnpj_Filteredtext_get = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Filteredtext_get");
            Ddo_contratada_pessoacnpj_Selectedvalue_get = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Selectedvalue_get");
            Ddo_contratogarantia_datapagtogarantia_Activeeventkey = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Activeeventkey");
            Ddo_contratogarantia_datapagtogarantia_Filteredtext_get = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Filteredtext_get");
            Ddo_contratogarantia_datapagtogarantia_Filteredtextto_get = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Filteredtextto_get");
            Ddo_contratogarantia_percentual_Activeeventkey = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Activeeventkey");
            Ddo_contratogarantia_percentual_Filteredtext_get = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Filteredtext_get");
            Ddo_contratogarantia_percentual_Filteredtextto_get = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Filteredtextto_get");
            Ddo_contratogarantia_datacaucao_Activeeventkey = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Activeeventkey");
            Ddo_contratogarantia_datacaucao_Filteredtext_get = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Filteredtext_get");
            Ddo_contratogarantia_datacaucao_Filteredtextto_get = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Filteredtextto_get");
            Ddo_contratogarantia_valorgarantia_Activeeventkey = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Activeeventkey");
            Ddo_contratogarantia_valorgarantia_Filteredtext_get = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Filteredtext_get");
            Ddo_contratogarantia_valorgarantia_Filteredtextto_get = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Filteredtextto_get");
            Ddo_contratogarantia_dataencerramento_Activeeventkey = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Activeeventkey");
            Ddo_contratogarantia_dataencerramento_Filteredtext_get = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Filteredtext_get");
            Ddo_contratogarantia_dataencerramento_Filteredtextto_get = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Filteredtextto_get");
            Ddo_contratogarantia_valorencerramento_Activeeventkey = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Activeeventkey");
            Ddo_contratogarantia_valorencerramento_Filteredtext_get = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Filteredtext_get");
            Ddo_contratogarantia_valorencerramento_Filteredtextto_get = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA1"), 0) != AV16ContratoGarantia_DataPagtoGarantia1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1"), 0) != AV17ContratoGarantia_DataPagtoGarantia_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA2"), 0) != AV20ContratoGarantia_DataPagtoGarantia2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2"), 0) != AV21ContratoGarantia_DataPagtoGarantia_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA3"), 0) != AV24ContratoGarantia_DataPagtoGarantia3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3"), 0) != AV25ContratoGarantia_DataPagtoGarantia_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOGARANTIA_CODIGO"), ",", ".") != Convert.ToDecimal( AV31TFContratoGarantia_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOGARANTIA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV32TFContratoGarantia_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_CODIGO"), ",", ".") != Convert.ToDecimal( AV35TFContrato_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV36TFContrato_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO"), AV39TFContrato_Numero) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO_SEL"), AV40TFContrato_Numero_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_CODIGO"), ",", ".") != Convert.ToDecimal( AV43TFContratada_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV44TFContratada_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_PESSOACOD"), ",", ".") != Convert.ToDecimal( AV47TFContratada_PessoaCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_PESSOACOD_TO"), ",", ".") != Convert.ToDecimal( AV48TFContratada_PessoaCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOANOM"), AV51TFContratada_PessoaNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOANOM_SEL"), AV52TFContratada_PessoaNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOACNPJ"), AV55TFContratada_PessoaCNPJ) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOACNPJ_SEL"), AV56TFContratada_PessoaCNPJ_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA"), 0) != AV59TFContratoGarantia_DataPagtoGarantia )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO"), 0) != AV60TFContratoGarantia_DataPagtoGarantia_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOGARANTIA_PERCENTUAL"), ",", ".") != AV65TFContratoGarantia_Percentual )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOGARANTIA_PERCENTUAL_TO"), ",", ".") != AV66TFContratoGarantia_Percentual_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOGARANTIA_DATACAUCAO"), 0) != AV69TFContratoGarantia_DataCaucao )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOGARANTIA_DATACAUCAO_TO"), 0) != AV70TFContratoGarantia_DataCaucao_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOGARANTIA_VALORGARANTIA"), ",", ".") != AV75TFContratoGarantia_ValorGarantia )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOGARANTIA_VALORGARANTIA_TO"), ",", ".") != AV76TFContratoGarantia_ValorGarantia_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOGARANTIA_DATAENCERRAMENTO"), 0) != AV79TFContratoGarantia_DataEncerramento )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO"), 0) != AV80TFContratoGarantia_DataEncerramento_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOGARANTIA_VALORENCERRAMENTO"), ",", ".") != AV85TFContratoGarantia_ValorEncerramento )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO"), ",", ".") != AV86TFContratoGarantia_ValorEncerramento_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E356I2 */
         E356I2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E356I2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "CONTRATOGARANTIA_DATAPAGTOGARANTIA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "CONTRATOGARANTIA_DATAPAGTOGARANTIA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "CONTRATOGARANTIA_DATAPAGTOGARANTIA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfcontratogarantia_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratogarantia_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratogarantia_codigo_Visible), 5, 0)));
         edtavTfcontratogarantia_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratogarantia_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratogarantia_codigo_to_Visible), 5, 0)));
         edtavTfcontrato_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_codigo_Visible), 5, 0)));
         edtavTfcontrato_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_codigo_to_Visible), 5, 0)));
         edtavTfcontrato_numero_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_numero_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_numero_Visible), 5, 0)));
         edtavTfcontrato_numero_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_numero_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_numero_sel_Visible), 5, 0)));
         edtavTfcontratada_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_codigo_Visible), 5, 0)));
         edtavTfcontratada_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_codigo_to_Visible), 5, 0)));
         edtavTfcontratada_pessoacod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoacod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoacod_Visible), 5, 0)));
         edtavTfcontratada_pessoacod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoacod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoacod_to_Visible), 5, 0)));
         edtavTfcontratada_pessoanom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoanom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoanom_Visible), 5, 0)));
         edtavTfcontratada_pessoanom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoanom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoanom_sel_Visible), 5, 0)));
         edtavTfcontratada_pessoacnpj_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoacnpj_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoacnpj_Visible), 5, 0)));
         edtavTfcontratada_pessoacnpj_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoacnpj_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoacnpj_sel_Visible), 5, 0)));
         edtavTfcontratogarantia_datapagtogarantia_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratogarantia_datapagtogarantia_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratogarantia_datapagtogarantia_Visible), 5, 0)));
         edtavTfcontratogarantia_datapagtogarantia_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratogarantia_datapagtogarantia_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratogarantia_datapagtogarantia_to_Visible), 5, 0)));
         edtavTfcontratogarantia_percentual_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratogarantia_percentual_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratogarantia_percentual_Visible), 5, 0)));
         edtavTfcontratogarantia_percentual_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratogarantia_percentual_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratogarantia_percentual_to_Visible), 5, 0)));
         edtavTfcontratogarantia_datacaucao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratogarantia_datacaucao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratogarantia_datacaucao_Visible), 5, 0)));
         edtavTfcontratogarantia_datacaucao_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratogarantia_datacaucao_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratogarantia_datacaucao_to_Visible), 5, 0)));
         edtavTfcontratogarantia_valorgarantia_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratogarantia_valorgarantia_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratogarantia_valorgarantia_Visible), 5, 0)));
         edtavTfcontratogarantia_valorgarantia_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratogarantia_valorgarantia_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratogarantia_valorgarantia_to_Visible), 5, 0)));
         edtavTfcontratogarantia_dataencerramento_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratogarantia_dataencerramento_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratogarantia_dataencerramento_Visible), 5, 0)));
         edtavTfcontratogarantia_dataencerramento_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratogarantia_dataencerramento_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratogarantia_dataencerramento_to_Visible), 5, 0)));
         edtavTfcontratogarantia_valorencerramento_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratogarantia_valorencerramento_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratogarantia_valorencerramento_Visible), 5, 0)));
         edtavTfcontratogarantia_valorencerramento_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratogarantia_valorencerramento_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratogarantia_valorencerramento_to_Visible), 5, 0)));
         Ddo_contratogarantia_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoGarantia_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_codigo_Internalname, "TitleControlIdToReplace", Ddo_contratogarantia_codigo_Titlecontrolidtoreplace);
         AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace = Ddo_contratogarantia_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace", AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace);
         edtavDdo_contratogarantia_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratogarantia_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratogarantia_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contrato_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_Contrato_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "TitleControlIdToReplace", Ddo_contrato_codigo_Titlecontrolidtoreplace);
         AV37ddo_Contrato_CodigoTitleControlIdToReplace = Ddo_contrato_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_Contrato_CodigoTitleControlIdToReplace", AV37ddo_Contrato_CodigoTitleControlIdToReplace);
         edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contrato_numero_Titlecontrolidtoreplace = subGrid_Internalname+"_Contrato_Numero";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "TitleControlIdToReplace", Ddo_contrato_numero_Titlecontrolidtoreplace);
         AV41ddo_Contrato_NumeroTitleControlIdToReplace = Ddo_contrato_numero_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_Contrato_NumeroTitleControlIdToReplace", AV41ddo_Contrato_NumeroTitleControlIdToReplace);
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratada_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratada_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_codigo_Internalname, "TitleControlIdToReplace", Ddo_contratada_codigo_Titlecontrolidtoreplace);
         AV45ddo_Contratada_CodigoTitleControlIdToReplace = Ddo_contratada_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_Contratada_CodigoTitleControlIdToReplace", AV45ddo_Contratada_CodigoTitleControlIdToReplace);
         edtavDdo_contratada_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratada_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratada_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratada_pessoacod_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratada_PessoaCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacod_Internalname, "TitleControlIdToReplace", Ddo_contratada_pessoacod_Titlecontrolidtoreplace);
         AV49ddo_Contratada_PessoaCodTitleControlIdToReplace = Ddo_contratada_pessoacod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_Contratada_PessoaCodTitleControlIdToReplace", AV49ddo_Contratada_PessoaCodTitleControlIdToReplace);
         edtavDdo_contratada_pessoacodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratada_pessoacodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratada_pessoacodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratada_pessoanom_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratada_PessoaNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "TitleControlIdToReplace", Ddo_contratada_pessoanom_Titlecontrolidtoreplace);
         AV53ddo_Contratada_PessoaNomTitleControlIdToReplace = Ddo_contratada_pessoanom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_Contratada_PessoaNomTitleControlIdToReplace", AV53ddo_Contratada_PessoaNomTitleControlIdToReplace);
         edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratada_PessoaCNPJ";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "TitleControlIdToReplace", Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace);
         AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace = Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace", AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace);
         edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratogarantia_datapagtogarantia_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoGarantia_DataPagtoGarantia";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datapagtogarantia_Internalname, "TitleControlIdToReplace", Ddo_contratogarantia_datapagtogarantia_Titlecontrolidtoreplace);
         AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace = Ddo_contratogarantia_datapagtogarantia_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace", AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace);
         edtavDdo_contratogarantia_datapagtogarantiatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratogarantia_datapagtogarantiatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratogarantia_datapagtogarantiatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratogarantia_percentual_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoGarantia_Percentual";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_percentual_Internalname, "TitleControlIdToReplace", Ddo_contratogarantia_percentual_Titlecontrolidtoreplace);
         AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace = Ddo_contratogarantia_percentual_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace", AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace);
         edtavDdo_contratogarantia_percentualtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratogarantia_percentualtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratogarantia_percentualtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratogarantia_datacaucao_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoGarantia_DataCaucao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datacaucao_Internalname, "TitleControlIdToReplace", Ddo_contratogarantia_datacaucao_Titlecontrolidtoreplace);
         AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace = Ddo_contratogarantia_datacaucao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace", AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace);
         edtavDdo_contratogarantia_datacaucaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratogarantia_datacaucaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratogarantia_datacaucaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratogarantia_valorgarantia_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoGarantia_ValorGarantia";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorgarantia_Internalname, "TitleControlIdToReplace", Ddo_contratogarantia_valorgarantia_Titlecontrolidtoreplace);
         AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace = Ddo_contratogarantia_valorgarantia_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace", AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace);
         edtavDdo_contratogarantia_valorgarantiatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratogarantia_valorgarantiatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratogarantia_valorgarantiatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratogarantia_dataencerramento_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoGarantia_DataEncerramento";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_dataencerramento_Internalname, "TitleControlIdToReplace", Ddo_contratogarantia_dataencerramento_Titlecontrolidtoreplace);
         AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace = Ddo_contratogarantia_dataencerramento_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace", AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace);
         edtavDdo_contratogarantia_dataencerramentotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratogarantia_dataencerramentotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratogarantia_dataencerramentotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratogarantia_valorencerramento_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoGarantia_ValorEncerramento";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorencerramento_Internalname, "TitleControlIdToReplace", Ddo_contratogarantia_valorencerramento_Titlecontrolidtoreplace);
         AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace = Ddo_contratogarantia_valorencerramento_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace", AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace);
         edtavDdo_contratogarantia_valorencerramentotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratogarantia_valorencerramentotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratogarantia_valorencerramentotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione uma Garantia";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "da Garantia", 0);
         cmbavOrderedby.addItem("2", "C�digo da Garantia", 0);
         cmbavOrderedby.addItem("3", "Contrato", 0);
         cmbavOrderedby.addItem("4", "N�mero do Contrato", 0);
         cmbavOrderedby.addItem("5", "Contratada", 0);
         cmbavOrderedby.addItem("6", "C�d. da Pessoa", 0);
         cmbavOrderedby.addItem("7", "Nome", 0);
         cmbavOrderedby.addItem("8", " CNPJ da Pessoa", 0);
         cmbavOrderedby.addItem("9", "Percentual", 0);
         cmbavOrderedby.addItem("10", "da Cau��o", 0);
         cmbavOrderedby.addItem("11", "Garantia", 0);
         cmbavOrderedby.addItem("12", "D. Encerraemnto da Garantia", 0);
         cmbavOrderedby.addItem("13", "Valor no Encerramento", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV88DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV88DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E366I2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV30ContratoGarantia_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34Contrato_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38Contrato_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42Contratada_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46Contratada_PessoaCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50Contratada_PessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54Contratada_PessoaCNPJTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58ContratoGarantia_DataPagtoGarantiaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV64ContratoGarantia_PercentualTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV68ContratoGarantia_DataCaucaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV74ContratoGarantia_ValorGarantiaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV78ContratoGarantia_DataEncerramentoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV84ContratoGarantia_ValorEncerramentoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratoGarantia_Codigo_Titleformat = 2;
         edtContratoGarantia_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "C�digo da Garantia", AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoGarantia_Codigo_Internalname, "Title", edtContratoGarantia_Codigo_Title);
         edtContrato_Codigo_Titleformat = 2;
         edtContrato_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Contrato", AV37ddo_Contrato_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Codigo_Internalname, "Title", edtContrato_Codigo_Title);
         edtContrato_Numero_Titleformat = 2;
         edtContrato_Numero_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "N�mero do Contrato", AV41ddo_Contrato_NumeroTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Numero_Internalname, "Title", edtContrato_Numero_Title);
         edtContratada_Codigo_Titleformat = 2;
         edtContratada_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Contratada", AV45ddo_Contratada_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_Codigo_Internalname, "Title", edtContratada_Codigo_Title);
         edtContratada_PessoaCod_Titleformat = 2;
         edtContratada_PessoaCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "C�d. da Pessoa", AV49ddo_Contratada_PessoaCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaCod_Internalname, "Title", edtContratada_PessoaCod_Title);
         edtContratada_PessoaNom_Titleformat = 2;
         edtContratada_PessoaNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV53ddo_Contratada_PessoaNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaNom_Internalname, "Title", edtContratada_PessoaNom_Title);
         edtContratada_PessoaCNPJ_Titleformat = 2;
         edtContratada_PessoaCNPJ_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", " CNPJ da Pessoa", AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaCNPJ_Internalname, "Title", edtContratada_PessoaCNPJ_Title);
         edtContratoGarantia_DataPagtoGarantia_Titleformat = 2;
         edtContratoGarantia_DataPagtoGarantia_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "da Garantia", AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoGarantia_DataPagtoGarantia_Internalname, "Title", edtContratoGarantia_DataPagtoGarantia_Title);
         edtContratoGarantia_Percentual_Titleformat = 2;
         edtContratoGarantia_Percentual_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Percentual", AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoGarantia_Percentual_Internalname, "Title", edtContratoGarantia_Percentual_Title);
         edtContratoGarantia_DataCaucao_Titleformat = 2;
         edtContratoGarantia_DataCaucao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "da Cau��o", AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoGarantia_DataCaucao_Internalname, "Title", edtContratoGarantia_DataCaucao_Title);
         edtContratoGarantia_ValorGarantia_Titleformat = 2;
         edtContratoGarantia_ValorGarantia_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Garantia", AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoGarantia_ValorGarantia_Internalname, "Title", edtContratoGarantia_ValorGarantia_Title);
         edtContratoGarantia_DataEncerramento_Titleformat = 2;
         edtContratoGarantia_DataEncerramento_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "D. Encerraemnto da Garantia", AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoGarantia_DataEncerramento_Internalname, "Title", edtContratoGarantia_DataEncerramento_Title);
         edtContratoGarantia_ValorEncerramento_Titleformat = 2;
         edtContratoGarantia_ValorEncerramento_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Valor no Encerramento", AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoGarantia_ValorEncerramento_Internalname, "Title", edtContratoGarantia_ValorEncerramento_Title);
         AV90GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV90GridCurrentPage), 10, 0)));
         AV91GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV91GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV30ContratoGarantia_CodigoTitleFilterData", AV30ContratoGarantia_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34Contrato_CodigoTitleFilterData", AV34Contrato_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV38Contrato_NumeroTitleFilterData", AV38Contrato_NumeroTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV42Contratada_CodigoTitleFilterData", AV42Contratada_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV46Contratada_PessoaCodTitleFilterData", AV46Contratada_PessoaCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV50Contratada_PessoaNomTitleFilterData", AV50Contratada_PessoaNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV54Contratada_PessoaCNPJTitleFilterData", AV54Contratada_PessoaCNPJTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV58ContratoGarantia_DataPagtoGarantiaTitleFilterData", AV58ContratoGarantia_DataPagtoGarantiaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV64ContratoGarantia_PercentualTitleFilterData", AV64ContratoGarantia_PercentualTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV68ContratoGarantia_DataCaucaoTitleFilterData", AV68ContratoGarantia_DataCaucaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV74ContratoGarantia_ValorGarantiaTitleFilterData", AV74ContratoGarantia_ValorGarantiaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV78ContratoGarantia_DataEncerramentoTitleFilterData", AV78ContratoGarantia_DataEncerramentoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV84ContratoGarantia_ValorEncerramentoTitleFilterData", AV84ContratoGarantia_ValorEncerramentoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E116I2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV89PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV89PageToGo) ;
         }
      }

      protected void E126I2( )
      {
         /* Ddo_contratogarantia_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratogarantia_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratogarantia_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_codigo_Internalname, "SortedStatus", Ddo_contratogarantia_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratogarantia_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratogarantia_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_codigo_Internalname, "SortedStatus", Ddo_contratogarantia_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratogarantia_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV31TFContratoGarantia_Codigo = (int)(NumberUtil.Val( Ddo_contratogarantia_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFContratoGarantia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContratoGarantia_Codigo), 6, 0)));
            AV32TFContratoGarantia_Codigo_To = (int)(NumberUtil.Val( Ddo_contratogarantia_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoGarantia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoGarantia_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E136I2( )
      {
         /* Ddo_contrato_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contrato_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "SortedStatus", Ddo_contrato_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "SortedStatus", Ddo_contrato_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV35TFContrato_Codigo = (int)(NumberUtil.Val( Ddo_contrato_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFContrato_Codigo), 6, 0)));
            AV36TFContrato_Codigo_To = (int)(NumberUtil.Val( Ddo_contrato_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContrato_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E146I2( )
      {
         /* Ddo_contrato_numero_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_numero_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_numero_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV39TFContrato_Numero = Ddo_contrato_numero_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContrato_Numero", AV39TFContrato_Numero);
            AV40TFContrato_Numero_Sel = Ddo_contrato_numero_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContrato_Numero_Sel", AV40TFContrato_Numero_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E156I2( )
      {
         /* Ddo_contratada_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratada_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_codigo_Internalname, "SortedStatus", Ddo_contratada_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_codigo_Internalname, "SortedStatus", Ddo_contratada_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV43TFContratada_Codigo = (int)(NumberUtil.Val( Ddo_contratada_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFContratada_Codigo), 6, 0)));
            AV44TFContratada_Codigo_To = (int)(NumberUtil.Val( Ddo_contratada_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratada_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFContratada_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E166I2( )
      {
         /* Ddo_contratada_pessoacod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratada_pessoacod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoacod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacod_Internalname, "SortedStatus", Ddo_contratada_pessoacod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoacod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoacod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacod_Internalname, "SortedStatus", Ddo_contratada_pessoacod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoacod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV47TFContratada_PessoaCod = (int)(NumberUtil.Val( Ddo_contratada_pessoacod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFContratada_PessoaCod), 6, 0)));
            AV48TFContratada_PessoaCod_To = (int)(NumberUtil.Val( Ddo_contratada_pessoacod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratada_PessoaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFContratada_PessoaCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E176I2( )
      {
         /* Ddo_contratada_pessoanom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratada_pessoanom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoanom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoanom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoanom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoanom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV51TFContratada_PessoaNom = Ddo_contratada_pessoanom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratada_PessoaNom", AV51TFContratada_PessoaNom);
            AV52TFContratada_PessoaNom_Sel = Ddo_contratada_pessoanom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContratada_PessoaNom_Sel", AV52TFContratada_PessoaNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E186I2( )
      {
         /* Ddo_contratada_pessoacnpj_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratada_pessoacnpj_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoacnpj_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SortedStatus", Ddo_contratada_pessoacnpj_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoacnpj_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoacnpj_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SortedStatus", Ddo_contratada_pessoacnpj_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoacnpj_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV55TFContratada_PessoaCNPJ = Ddo_contratada_pessoacnpj_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratada_PessoaCNPJ", AV55TFContratada_PessoaCNPJ);
            AV56TFContratada_PessoaCNPJ_Sel = Ddo_contratada_pessoacnpj_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContratada_PessoaCNPJ_Sel", AV56TFContratada_PessoaCNPJ_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E196I2( )
      {
         /* Ddo_contratogarantia_datapagtogarantia_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratogarantia_datapagtogarantia_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratogarantia_datapagtogarantia_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datapagtogarantia_Internalname, "SortedStatus", Ddo_contratogarantia_datapagtogarantia_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratogarantia_datapagtogarantia_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratogarantia_datapagtogarantia_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datapagtogarantia_Internalname, "SortedStatus", Ddo_contratogarantia_datapagtogarantia_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratogarantia_datapagtogarantia_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV59TFContratoGarantia_DataPagtoGarantia = context.localUtil.CToD( Ddo_contratogarantia_datapagtogarantia_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoGarantia_DataPagtoGarantia", context.localUtil.Format(AV59TFContratoGarantia_DataPagtoGarantia, "99/99/99"));
            AV60TFContratoGarantia_DataPagtoGarantia_To = context.localUtil.CToD( Ddo_contratogarantia_datapagtogarantia_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoGarantia_DataPagtoGarantia_To", context.localUtil.Format(AV60TFContratoGarantia_DataPagtoGarantia_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E206I2( )
      {
         /* Ddo_contratogarantia_percentual_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratogarantia_percentual_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratogarantia_percentual_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_percentual_Internalname, "SortedStatus", Ddo_contratogarantia_percentual_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratogarantia_percentual_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratogarantia_percentual_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_percentual_Internalname, "SortedStatus", Ddo_contratogarantia_percentual_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratogarantia_percentual_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV65TFContratoGarantia_Percentual = NumberUtil.Val( Ddo_contratogarantia_percentual_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratoGarantia_Percentual", StringUtil.LTrim( StringUtil.Str( AV65TFContratoGarantia_Percentual, 6, 2)));
            AV66TFContratoGarantia_Percentual_To = NumberUtil.Val( Ddo_contratogarantia_percentual_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratoGarantia_Percentual_To", StringUtil.LTrim( StringUtil.Str( AV66TFContratoGarantia_Percentual_To, 6, 2)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E216I2( )
      {
         /* Ddo_contratogarantia_datacaucao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratogarantia_datacaucao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 10;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratogarantia_datacaucao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datacaucao_Internalname, "SortedStatus", Ddo_contratogarantia_datacaucao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratogarantia_datacaucao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 10;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratogarantia_datacaucao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datacaucao_Internalname, "SortedStatus", Ddo_contratogarantia_datacaucao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratogarantia_datacaucao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV69TFContratoGarantia_DataCaucao = context.localUtil.CToD( Ddo_contratogarantia_datacaucao_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFContratoGarantia_DataCaucao", context.localUtil.Format(AV69TFContratoGarantia_DataCaucao, "99/99/99"));
            AV70TFContratoGarantia_DataCaucao_To = context.localUtil.CToD( Ddo_contratogarantia_datacaucao_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratoGarantia_DataCaucao_To", context.localUtil.Format(AV70TFContratoGarantia_DataCaucao_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E226I2( )
      {
         /* Ddo_contratogarantia_valorgarantia_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratogarantia_valorgarantia_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 11;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratogarantia_valorgarantia_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorgarantia_Internalname, "SortedStatus", Ddo_contratogarantia_valorgarantia_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratogarantia_valorgarantia_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 11;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratogarantia_valorgarantia_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorgarantia_Internalname, "SortedStatus", Ddo_contratogarantia_valorgarantia_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratogarantia_valorgarantia_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV75TFContratoGarantia_ValorGarantia = NumberUtil.Val( Ddo_contratogarantia_valorgarantia_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContratoGarantia_ValorGarantia", StringUtil.LTrim( StringUtil.Str( AV75TFContratoGarantia_ValorGarantia, 18, 5)));
            AV76TFContratoGarantia_ValorGarantia_To = NumberUtil.Val( Ddo_contratogarantia_valorgarantia_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContratoGarantia_ValorGarantia_To", StringUtil.LTrim( StringUtil.Str( AV76TFContratoGarantia_ValorGarantia_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E236I2( )
      {
         /* Ddo_contratogarantia_dataencerramento_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratogarantia_dataencerramento_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 12;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratogarantia_dataencerramento_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_dataencerramento_Internalname, "SortedStatus", Ddo_contratogarantia_dataencerramento_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratogarantia_dataencerramento_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 12;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratogarantia_dataencerramento_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_dataencerramento_Internalname, "SortedStatus", Ddo_contratogarantia_dataencerramento_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratogarantia_dataencerramento_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV79TFContratoGarantia_DataEncerramento = context.localUtil.CToD( Ddo_contratogarantia_dataencerramento_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFContratoGarantia_DataEncerramento", context.localUtil.Format(AV79TFContratoGarantia_DataEncerramento, "99/99/99"));
            AV80TFContratoGarantia_DataEncerramento_To = context.localUtil.CToD( Ddo_contratogarantia_dataencerramento_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContratoGarantia_DataEncerramento_To", context.localUtil.Format(AV80TFContratoGarantia_DataEncerramento_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E246I2( )
      {
         /* Ddo_contratogarantia_valorencerramento_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratogarantia_valorencerramento_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 13;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratogarantia_valorencerramento_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorencerramento_Internalname, "SortedStatus", Ddo_contratogarantia_valorencerramento_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratogarantia_valorencerramento_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 13;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratogarantia_valorencerramento_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorencerramento_Internalname, "SortedStatus", Ddo_contratogarantia_valorencerramento_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratogarantia_valorencerramento_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV85TFContratoGarantia_ValorEncerramento = NumberUtil.Val( Ddo_contratogarantia_valorencerramento_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFContratoGarantia_ValorEncerramento", StringUtil.LTrim( StringUtil.Str( AV85TFContratoGarantia_ValorEncerramento, 18, 5)));
            AV86TFContratoGarantia_ValorEncerramento_To = NumberUtil.Val( Ddo_contratogarantia_valorencerramento_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFContratoGarantia_ValorEncerramento_To", StringUtil.LTrim( StringUtil.Str( AV86TFContratoGarantia_ValorEncerramento_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E376I2( )
      {
         /* Grid_Load Routine */
         AV28Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV28Select);
         AV100Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 86;
         }
         sendrow_862( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_86_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(86, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E386I2 */
         E386I2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E386I2( )
      {
         /* Enter Routine */
         AV7InOutContratoGarantia_Codigo = A101ContratoGarantia_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoGarantia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoGarantia_Codigo), 6, 0)));
         AV8InOutContratoGarantia_DataPagtoGarantia = A102ContratoGarantia_DataPagtoGarantia;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoGarantia_DataPagtoGarantia", context.localUtil.Format(AV8InOutContratoGarantia_DataPagtoGarantia, "99/99/99"));
         context.setWebReturnParms(new Object[] {(int)AV7InOutContratoGarantia_Codigo,context.localUtil.Format( AV8InOutContratoGarantia_DataPagtoGarantia, "99/99/99")});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E256I2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E306I2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E266I2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoGarantia_DataPagtoGarantia1, AV17ContratoGarantia_DataPagtoGarantia_To1, AV19DynamicFiltersSelector2, AV20ContratoGarantia_DataPagtoGarantia2, AV21ContratoGarantia_DataPagtoGarantia_To2, AV23DynamicFiltersSelector3, AV24ContratoGarantia_DataPagtoGarantia3, AV25ContratoGarantia_DataPagtoGarantia_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContratoGarantia_Codigo, AV32TFContratoGarantia_Codigo_To, AV35TFContrato_Codigo, AV36TFContrato_Codigo_To, AV39TFContrato_Numero, AV40TFContrato_Numero_Sel, AV43TFContratada_Codigo, AV44TFContratada_Codigo_To, AV47TFContratada_PessoaCod, AV48TFContratada_PessoaCod_To, AV51TFContratada_PessoaNom, AV52TFContratada_PessoaNom_Sel, AV55TFContratada_PessoaCNPJ, AV56TFContratada_PessoaCNPJ_Sel, AV59TFContratoGarantia_DataPagtoGarantia, AV60TFContratoGarantia_DataPagtoGarantia_To, AV65TFContratoGarantia_Percentual, AV66TFContratoGarantia_Percentual_To, AV69TFContratoGarantia_DataCaucao, AV70TFContratoGarantia_DataCaucao_To, AV75TFContratoGarantia_ValorGarantia, AV76TFContratoGarantia_ValorGarantia_To, AV79TFContratoGarantia_DataEncerramento, AV80TFContratoGarantia_DataEncerramento_To, AV85TFContratoGarantia_ValorEncerramento, AV86TFContratoGarantia_ValorEncerramento_To, AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace, AV37ddo_Contrato_CodigoTitleControlIdToReplace, AV41ddo_Contrato_NumeroTitleControlIdToReplace, AV45ddo_Contratada_CodigoTitleControlIdToReplace, AV49ddo_Contratada_PessoaCodTitleControlIdToReplace, AV53ddo_Contratada_PessoaNomTitleControlIdToReplace, AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace, AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace, AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace, AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace, AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace, AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace, AV101Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E316I2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E326I2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E276I2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoGarantia_DataPagtoGarantia1, AV17ContratoGarantia_DataPagtoGarantia_To1, AV19DynamicFiltersSelector2, AV20ContratoGarantia_DataPagtoGarantia2, AV21ContratoGarantia_DataPagtoGarantia_To2, AV23DynamicFiltersSelector3, AV24ContratoGarantia_DataPagtoGarantia3, AV25ContratoGarantia_DataPagtoGarantia_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContratoGarantia_Codigo, AV32TFContratoGarantia_Codigo_To, AV35TFContrato_Codigo, AV36TFContrato_Codigo_To, AV39TFContrato_Numero, AV40TFContrato_Numero_Sel, AV43TFContratada_Codigo, AV44TFContratada_Codigo_To, AV47TFContratada_PessoaCod, AV48TFContratada_PessoaCod_To, AV51TFContratada_PessoaNom, AV52TFContratada_PessoaNom_Sel, AV55TFContratada_PessoaCNPJ, AV56TFContratada_PessoaCNPJ_Sel, AV59TFContratoGarantia_DataPagtoGarantia, AV60TFContratoGarantia_DataPagtoGarantia_To, AV65TFContratoGarantia_Percentual, AV66TFContratoGarantia_Percentual_To, AV69TFContratoGarantia_DataCaucao, AV70TFContratoGarantia_DataCaucao_To, AV75TFContratoGarantia_ValorGarantia, AV76TFContratoGarantia_ValorGarantia_To, AV79TFContratoGarantia_DataEncerramento, AV80TFContratoGarantia_DataEncerramento_To, AV85TFContratoGarantia_ValorEncerramento, AV86TFContratoGarantia_ValorEncerramento_To, AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace, AV37ddo_Contrato_CodigoTitleControlIdToReplace, AV41ddo_Contrato_NumeroTitleControlIdToReplace, AV45ddo_Contratada_CodigoTitleControlIdToReplace, AV49ddo_Contratada_PessoaCodTitleControlIdToReplace, AV53ddo_Contratada_PessoaNomTitleControlIdToReplace, AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace, AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace, AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace, AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace, AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace, AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace, AV101Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E336I2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E286I2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoGarantia_DataPagtoGarantia1, AV17ContratoGarantia_DataPagtoGarantia_To1, AV19DynamicFiltersSelector2, AV20ContratoGarantia_DataPagtoGarantia2, AV21ContratoGarantia_DataPagtoGarantia_To2, AV23DynamicFiltersSelector3, AV24ContratoGarantia_DataPagtoGarantia3, AV25ContratoGarantia_DataPagtoGarantia_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContratoGarantia_Codigo, AV32TFContratoGarantia_Codigo_To, AV35TFContrato_Codigo, AV36TFContrato_Codigo_To, AV39TFContrato_Numero, AV40TFContrato_Numero_Sel, AV43TFContratada_Codigo, AV44TFContratada_Codigo_To, AV47TFContratada_PessoaCod, AV48TFContratada_PessoaCod_To, AV51TFContratada_PessoaNom, AV52TFContratada_PessoaNom_Sel, AV55TFContratada_PessoaCNPJ, AV56TFContratada_PessoaCNPJ_Sel, AV59TFContratoGarantia_DataPagtoGarantia, AV60TFContratoGarantia_DataPagtoGarantia_To, AV65TFContratoGarantia_Percentual, AV66TFContratoGarantia_Percentual_To, AV69TFContratoGarantia_DataCaucao, AV70TFContratoGarantia_DataCaucao_To, AV75TFContratoGarantia_ValorGarantia, AV76TFContratoGarantia_ValorGarantia_To, AV79TFContratoGarantia_DataEncerramento, AV80TFContratoGarantia_DataEncerramento_To, AV85TFContratoGarantia_ValorEncerramento, AV86TFContratoGarantia_ValorEncerramento_To, AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace, AV37ddo_Contrato_CodigoTitleControlIdToReplace, AV41ddo_Contrato_NumeroTitleControlIdToReplace, AV45ddo_Contratada_CodigoTitleControlIdToReplace, AV49ddo_Contratada_PessoaCodTitleControlIdToReplace, AV53ddo_Contratada_PessoaNomTitleControlIdToReplace, AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace, AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace, AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace, AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace, AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace, AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace, AV101Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E346I2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E296I2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contratogarantia_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_codigo_Internalname, "SortedStatus", Ddo_contratogarantia_codigo_Sortedstatus);
         Ddo_contrato_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "SortedStatus", Ddo_contrato_codigo_Sortedstatus);
         Ddo_contrato_numero_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
         Ddo_contratada_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_codigo_Internalname, "SortedStatus", Ddo_contratada_codigo_Sortedstatus);
         Ddo_contratada_pessoacod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacod_Internalname, "SortedStatus", Ddo_contratada_pessoacod_Sortedstatus);
         Ddo_contratada_pessoanom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
         Ddo_contratada_pessoacnpj_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SortedStatus", Ddo_contratada_pessoacnpj_Sortedstatus);
         Ddo_contratogarantia_datapagtogarantia_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datapagtogarantia_Internalname, "SortedStatus", Ddo_contratogarantia_datapagtogarantia_Sortedstatus);
         Ddo_contratogarantia_percentual_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_percentual_Internalname, "SortedStatus", Ddo_contratogarantia_percentual_Sortedstatus);
         Ddo_contratogarantia_datacaucao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datacaucao_Internalname, "SortedStatus", Ddo_contratogarantia_datacaucao_Sortedstatus);
         Ddo_contratogarantia_valorgarantia_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorgarantia_Internalname, "SortedStatus", Ddo_contratogarantia_valorgarantia_Sortedstatus);
         Ddo_contratogarantia_dataencerramento_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_dataencerramento_Internalname, "SortedStatus", Ddo_contratogarantia_dataencerramento_Sortedstatus);
         Ddo_contratogarantia_valorencerramento_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorencerramento_Internalname, "SortedStatus", Ddo_contratogarantia_valorencerramento_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_contratogarantia_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_codigo_Internalname, "SortedStatus", Ddo_contratogarantia_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contrato_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "SortedStatus", Ddo_contrato_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_contrato_numero_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_contratada_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_codigo_Internalname, "SortedStatus", Ddo_contratada_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_contratada_pessoacod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacod_Internalname, "SortedStatus", Ddo_contratada_pessoacod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_contratada_pessoanom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
         }
         else if ( AV13OrderedBy == 8 )
         {
            Ddo_contratada_pessoacnpj_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SortedStatus", Ddo_contratada_pessoacnpj_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_contratogarantia_datapagtogarantia_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datapagtogarantia_Internalname, "SortedStatus", Ddo_contratogarantia_datapagtogarantia_Sortedstatus);
         }
         else if ( AV13OrderedBy == 9 )
         {
            Ddo_contratogarantia_percentual_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_percentual_Internalname, "SortedStatus", Ddo_contratogarantia_percentual_Sortedstatus);
         }
         else if ( AV13OrderedBy == 10 )
         {
            Ddo_contratogarantia_datacaucao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datacaucao_Internalname, "SortedStatus", Ddo_contratogarantia_datacaucao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 11 )
         {
            Ddo_contratogarantia_valorgarantia_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorgarantia_Internalname, "SortedStatus", Ddo_contratogarantia_valorgarantia_Sortedstatus);
         }
         else if ( AV13OrderedBy == 12 )
         {
            Ddo_contratogarantia_dataencerramento_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_dataencerramento_Internalname, "SortedStatus", Ddo_contratogarantia_dataencerramento_Sortedstatus);
         }
         else if ( AV13OrderedBy == 13 )
         {
            Ddo_contratogarantia_valorencerramento_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorencerramento_Internalname, "SortedStatus", Ddo_contratogarantia_valorencerramento_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 )
         {
            tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 )
         {
            tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 )
         {
            tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "CONTRATOGARANTIA_DATAPAGTOGARANTIA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20ContratoGarantia_DataPagtoGarantia2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContratoGarantia_DataPagtoGarantia2", context.localUtil.Format(AV20ContratoGarantia_DataPagtoGarantia2, "99/99/99"));
         AV21ContratoGarantia_DataPagtoGarantia_To2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoGarantia_DataPagtoGarantia_To2", context.localUtil.Format(AV21ContratoGarantia_DataPagtoGarantia_To2, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "CONTRATOGARANTIA_DATAPAGTOGARANTIA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24ContratoGarantia_DataPagtoGarantia3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratoGarantia_DataPagtoGarantia3", context.localUtil.Format(AV24ContratoGarantia_DataPagtoGarantia3, "99/99/99"));
         AV25ContratoGarantia_DataPagtoGarantia_To3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoGarantia_DataPagtoGarantia_To3", context.localUtil.Format(AV25ContratoGarantia_DataPagtoGarantia_To3, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV31TFContratoGarantia_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFContratoGarantia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContratoGarantia_Codigo), 6, 0)));
         Ddo_contratogarantia_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_codigo_Internalname, "FilteredText_set", Ddo_contratogarantia_codigo_Filteredtext_set);
         AV32TFContratoGarantia_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoGarantia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoGarantia_Codigo_To), 6, 0)));
         Ddo_contratogarantia_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_codigo_Internalname, "FilteredTextTo_set", Ddo_contratogarantia_codigo_Filteredtextto_set);
         AV35TFContrato_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFContrato_Codigo), 6, 0)));
         Ddo_contrato_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "FilteredText_set", Ddo_contrato_codigo_Filteredtext_set);
         AV36TFContrato_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContrato_Codigo_To), 6, 0)));
         Ddo_contrato_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "FilteredTextTo_set", Ddo_contrato_codigo_Filteredtextto_set);
         AV39TFContrato_Numero = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContrato_Numero", AV39TFContrato_Numero);
         Ddo_contrato_numero_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "FilteredText_set", Ddo_contrato_numero_Filteredtext_set);
         AV40TFContrato_Numero_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContrato_Numero_Sel", AV40TFContrato_Numero_Sel);
         Ddo_contrato_numero_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SelectedValue_set", Ddo_contrato_numero_Selectedvalue_set);
         AV43TFContratada_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFContratada_Codigo), 6, 0)));
         Ddo_contratada_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_codigo_Internalname, "FilteredText_set", Ddo_contratada_codigo_Filteredtext_set);
         AV44TFContratada_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratada_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFContratada_Codigo_To), 6, 0)));
         Ddo_contratada_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_codigo_Internalname, "FilteredTextTo_set", Ddo_contratada_codigo_Filteredtextto_set);
         AV47TFContratada_PessoaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFContratada_PessoaCod), 6, 0)));
         Ddo_contratada_pessoacod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacod_Internalname, "FilteredText_set", Ddo_contratada_pessoacod_Filteredtext_set);
         AV48TFContratada_PessoaCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratada_PessoaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFContratada_PessoaCod_To), 6, 0)));
         Ddo_contratada_pessoacod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacod_Internalname, "FilteredTextTo_set", Ddo_contratada_pessoacod_Filteredtextto_set);
         AV51TFContratada_PessoaNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratada_PessoaNom", AV51TFContratada_PessoaNom);
         Ddo_contratada_pessoanom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "FilteredText_set", Ddo_contratada_pessoanom_Filteredtext_set);
         AV52TFContratada_PessoaNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContratada_PessoaNom_Sel", AV52TFContratada_PessoaNom_Sel);
         Ddo_contratada_pessoanom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SelectedValue_set", Ddo_contratada_pessoanom_Selectedvalue_set);
         AV55TFContratada_PessoaCNPJ = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratada_PessoaCNPJ", AV55TFContratada_PessoaCNPJ);
         Ddo_contratada_pessoacnpj_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "FilteredText_set", Ddo_contratada_pessoacnpj_Filteredtext_set);
         AV56TFContratada_PessoaCNPJ_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContratada_PessoaCNPJ_Sel", AV56TFContratada_PessoaCNPJ_Sel);
         Ddo_contratada_pessoacnpj_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SelectedValue_set", Ddo_contratada_pessoacnpj_Selectedvalue_set);
         AV59TFContratoGarantia_DataPagtoGarantia = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoGarantia_DataPagtoGarantia", context.localUtil.Format(AV59TFContratoGarantia_DataPagtoGarantia, "99/99/99"));
         Ddo_contratogarantia_datapagtogarantia_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datapagtogarantia_Internalname, "FilteredText_set", Ddo_contratogarantia_datapagtogarantia_Filteredtext_set);
         AV60TFContratoGarantia_DataPagtoGarantia_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoGarantia_DataPagtoGarantia_To", context.localUtil.Format(AV60TFContratoGarantia_DataPagtoGarantia_To, "99/99/99"));
         Ddo_contratogarantia_datapagtogarantia_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datapagtogarantia_Internalname, "FilteredTextTo_set", Ddo_contratogarantia_datapagtogarantia_Filteredtextto_set);
         AV65TFContratoGarantia_Percentual = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratoGarantia_Percentual", StringUtil.LTrim( StringUtil.Str( AV65TFContratoGarantia_Percentual, 6, 2)));
         Ddo_contratogarantia_percentual_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_percentual_Internalname, "FilteredText_set", Ddo_contratogarantia_percentual_Filteredtext_set);
         AV66TFContratoGarantia_Percentual_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratoGarantia_Percentual_To", StringUtil.LTrim( StringUtil.Str( AV66TFContratoGarantia_Percentual_To, 6, 2)));
         Ddo_contratogarantia_percentual_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_percentual_Internalname, "FilteredTextTo_set", Ddo_contratogarantia_percentual_Filteredtextto_set);
         AV69TFContratoGarantia_DataCaucao = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFContratoGarantia_DataCaucao", context.localUtil.Format(AV69TFContratoGarantia_DataCaucao, "99/99/99"));
         Ddo_contratogarantia_datacaucao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datacaucao_Internalname, "FilteredText_set", Ddo_contratogarantia_datacaucao_Filteredtext_set);
         AV70TFContratoGarantia_DataCaucao_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratoGarantia_DataCaucao_To", context.localUtil.Format(AV70TFContratoGarantia_DataCaucao_To, "99/99/99"));
         Ddo_contratogarantia_datacaucao_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datacaucao_Internalname, "FilteredTextTo_set", Ddo_contratogarantia_datacaucao_Filteredtextto_set);
         AV75TFContratoGarantia_ValorGarantia = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContratoGarantia_ValorGarantia", StringUtil.LTrim( StringUtil.Str( AV75TFContratoGarantia_ValorGarantia, 18, 5)));
         Ddo_contratogarantia_valorgarantia_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorgarantia_Internalname, "FilteredText_set", Ddo_contratogarantia_valorgarantia_Filteredtext_set);
         AV76TFContratoGarantia_ValorGarantia_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContratoGarantia_ValorGarantia_To", StringUtil.LTrim( StringUtil.Str( AV76TFContratoGarantia_ValorGarantia_To, 18, 5)));
         Ddo_contratogarantia_valorgarantia_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorgarantia_Internalname, "FilteredTextTo_set", Ddo_contratogarantia_valorgarantia_Filteredtextto_set);
         AV79TFContratoGarantia_DataEncerramento = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFContratoGarantia_DataEncerramento", context.localUtil.Format(AV79TFContratoGarantia_DataEncerramento, "99/99/99"));
         Ddo_contratogarantia_dataencerramento_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_dataencerramento_Internalname, "FilteredText_set", Ddo_contratogarantia_dataencerramento_Filteredtext_set);
         AV80TFContratoGarantia_DataEncerramento_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContratoGarantia_DataEncerramento_To", context.localUtil.Format(AV80TFContratoGarantia_DataEncerramento_To, "99/99/99"));
         Ddo_contratogarantia_dataencerramento_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_dataencerramento_Internalname, "FilteredTextTo_set", Ddo_contratogarantia_dataencerramento_Filteredtextto_set);
         AV85TFContratoGarantia_ValorEncerramento = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFContratoGarantia_ValorEncerramento", StringUtil.LTrim( StringUtil.Str( AV85TFContratoGarantia_ValorEncerramento, 18, 5)));
         Ddo_contratogarantia_valorencerramento_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorencerramento_Internalname, "FilteredText_set", Ddo_contratogarantia_valorencerramento_Filteredtext_set);
         AV86TFContratoGarantia_ValorEncerramento_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFContratoGarantia_ValorEncerramento_To", StringUtil.LTrim( StringUtil.Str( AV86TFContratoGarantia_ValorEncerramento_To, 18, 5)));
         Ddo_contratogarantia_valorencerramento_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorencerramento_Internalname, "FilteredTextTo_set", Ddo_contratogarantia_valorencerramento_Filteredtextto_set);
         AV15DynamicFiltersSelector1 = "CONTRATOGARANTIA_DATAPAGTOGARANTIA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16ContratoGarantia_DataPagtoGarantia1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoGarantia_DataPagtoGarantia1", context.localUtil.Format(AV16ContratoGarantia_DataPagtoGarantia1, "99/99/99"));
         AV17ContratoGarantia_DataPagtoGarantia_To1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoGarantia_DataPagtoGarantia_To1", context.localUtil.Format(AV17ContratoGarantia_DataPagtoGarantia_To1, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 )
            {
               AV16ContratoGarantia_DataPagtoGarantia1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoGarantia_DataPagtoGarantia1", context.localUtil.Format(AV16ContratoGarantia_DataPagtoGarantia1, "99/99/99"));
               AV17ContratoGarantia_DataPagtoGarantia_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoGarantia_DataPagtoGarantia_To1", context.localUtil.Format(AV17ContratoGarantia_DataPagtoGarantia_To1, "99/99/99"));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 )
               {
                  AV20ContratoGarantia_DataPagtoGarantia2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContratoGarantia_DataPagtoGarantia2", context.localUtil.Format(AV20ContratoGarantia_DataPagtoGarantia2, "99/99/99"));
                  AV21ContratoGarantia_DataPagtoGarantia_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoGarantia_DataPagtoGarantia_To2", context.localUtil.Format(AV21ContratoGarantia_DataPagtoGarantia_To2, "99/99/99"));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 )
                  {
                     AV24ContratoGarantia_DataPagtoGarantia3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratoGarantia_DataPagtoGarantia3", context.localUtil.Format(AV24ContratoGarantia_DataPagtoGarantia3, "99/99/99"));
                     AV25ContratoGarantia_DataPagtoGarantia_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoGarantia_DataPagtoGarantia_To3", context.localUtil.Format(AV25ContratoGarantia_DataPagtoGarantia_To3, "99/99/99"));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV31TFContratoGarantia_Codigo) && (0==AV32TFContratoGarantia_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOGARANTIA_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV31TFContratoGarantia_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV32TFContratoGarantia_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV35TFContrato_Codigo) && (0==AV36TFContrato_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATO_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV35TFContrato_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV36TFContrato_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFContrato_Numero)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATO_NUMERO";
            AV11GridStateFilterValue.gxTpr_Value = AV39TFContrato_Numero;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFContrato_Numero_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATO_NUMERO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV40TFContrato_Numero_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV43TFContratada_Codigo) && (0==AV44TFContratada_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV43TFContratada_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV44TFContratada_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV47TFContratada_PessoaCod) && (0==AV48TFContratada_PessoaCod_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOACOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV47TFContratada_PessoaCod), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV48TFContratada_PessoaCod_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51TFContratada_PessoaNom)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOANOM";
            AV11GridStateFilterValue.gxTpr_Value = AV51TFContratada_PessoaNom;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFContratada_PessoaNom_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOANOM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV52TFContratada_PessoaNom_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55TFContratada_PessoaCNPJ)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOACNPJ";
            AV11GridStateFilterValue.gxTpr_Value = AV55TFContratada_PessoaCNPJ;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFContratada_PessoaCNPJ_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOACNPJ_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV56TFContratada_PessoaCNPJ_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV59TFContratoGarantia_DataPagtoGarantia) && (DateTime.MinValue==AV60TFContratoGarantia_DataPagtoGarantia_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOGARANTIA_DATAPAGTOGARANTIA";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV59TFContratoGarantia_DataPagtoGarantia, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV60TFContratoGarantia_DataPagtoGarantia_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV65TFContratoGarantia_Percentual) && (Convert.ToDecimal(0)==AV66TFContratoGarantia_Percentual_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOGARANTIA_PERCENTUAL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV65TFContratoGarantia_Percentual, 6, 2);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV66TFContratoGarantia_Percentual_To, 6, 2);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV69TFContratoGarantia_DataCaucao) && (DateTime.MinValue==AV70TFContratoGarantia_DataCaucao_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOGARANTIA_DATACAUCAO";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV69TFContratoGarantia_DataCaucao, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV70TFContratoGarantia_DataCaucao_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV75TFContratoGarantia_ValorGarantia) && (Convert.ToDecimal(0)==AV76TFContratoGarantia_ValorGarantia_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOGARANTIA_VALORGARANTIA";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV75TFContratoGarantia_ValorGarantia, 18, 5);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV76TFContratoGarantia_ValorGarantia_To, 18, 5);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV79TFContratoGarantia_DataEncerramento) && (DateTime.MinValue==AV80TFContratoGarantia_DataEncerramento_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOGARANTIA_DATAENCERRAMENTO";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV79TFContratoGarantia_DataEncerramento, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV80TFContratoGarantia_DataEncerramento_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV85TFContratoGarantia_ValorEncerramento) && (Convert.ToDecimal(0)==AV86TFContratoGarantia_ValorEncerramento_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOGARANTIA_VALORENCERRAMENTO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV85TFContratoGarantia_ValorEncerramento, 18, 5);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV86TFContratoGarantia_ValorEncerramento_To, 18, 5);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV101Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ! ( (DateTime.MinValue==AV16ContratoGarantia_DataPagtoGarantia1) && (DateTime.MinValue==AV17ContratoGarantia_DataPagtoGarantia_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV16ContratoGarantia_DataPagtoGarantia1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV17ContratoGarantia_DataPagtoGarantia_To1, 2, "/");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ! ( (DateTime.MinValue==AV20ContratoGarantia_DataPagtoGarantia2) && (DateTime.MinValue==AV21ContratoGarantia_DataPagtoGarantia_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV20ContratoGarantia_DataPagtoGarantia2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV21ContratoGarantia_DataPagtoGarantia_To2, 2, "/");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ! ( (DateTime.MinValue==AV24ContratoGarantia_DataPagtoGarantia3) && (DateTime.MinValue==AV25ContratoGarantia_DataPagtoGarantia_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV24ContratoGarantia_DataPagtoGarantia3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV25ContratoGarantia_DataPagtoGarantia_To3, 2, "/");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_6I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_6I2( true) ;
         }
         else
         {
            wb_table2_5_6I2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_6I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_80_6I2( true) ;
         }
         else
         {
            wb_table3_80_6I2( false) ;
         }
         return  ;
      }

      protected void wb_table3_80_6I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_6I2e( true) ;
         }
         else
         {
            wb_table1_2_6I2e( false) ;
         }
      }

      protected void wb_table3_80_6I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_83_6I2( true) ;
         }
         else
         {
            wb_table4_83_6I2( false) ;
         }
         return  ;
      }

      protected void wb_table4_83_6I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_80_6I2e( true) ;
         }
         else
         {
            wb_table3_80_6I2e( false) ;
         }
      }

      protected void wb_table4_83_6I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"86\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoGarantia_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoGarantia_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoGarantia_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(127), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_Numero_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_Numero_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_Numero_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_PessoaCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_PessoaCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_PessoaCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_PessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_PessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_PessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_PessoaCNPJ_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_PessoaCNPJ_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_PessoaCNPJ_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoGarantia_DataPagtoGarantia_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoGarantia_DataPagtoGarantia_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoGarantia_DataPagtoGarantia_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoGarantia_Percentual_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoGarantia_Percentual_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoGarantia_Percentual_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoGarantia_DataCaucao_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoGarantia_DataCaucao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoGarantia_DataCaucao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoGarantia_ValorGarantia_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoGarantia_ValorGarantia_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoGarantia_ValorGarantia_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoGarantia_DataEncerramento_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoGarantia_DataEncerramento_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoGarantia_DataEncerramento_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoGarantia_ValorEncerramento_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoGarantia_ValorEncerramento_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoGarantia_ValorEncerramento_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A101ContratoGarantia_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoGarantia_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoGarantia_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A77Contrato_Numero));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_Numero_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_Numero_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_PessoaCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_PessoaCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A41Contratada_PessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_PessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_PessoaNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A42Contratada_PessoaCNPJ);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_PessoaCNPJ_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_PessoaCNPJ_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A102ContratoGarantia_DataPagtoGarantia, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoGarantia_DataPagtoGarantia_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoGarantia_DataPagtoGarantia_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A103ContratoGarantia_Percentual, 6, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoGarantia_Percentual_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoGarantia_Percentual_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A104ContratoGarantia_DataCaucao, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoGarantia_DataCaucao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoGarantia_DataCaucao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A105ContratoGarantia_ValorGarantia, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoGarantia_ValorGarantia_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoGarantia_ValorGarantia_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A106ContratoGarantia_DataEncerramento, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoGarantia_DataEncerramento_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoGarantia_DataEncerramento_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A107ContratoGarantia_ValorEncerramento, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoGarantia_ValorEncerramento_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoGarantia_ValorEncerramento_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 86 )
         {
            wbEnd = 0;
            nRC_GXsfl_86 = (short)(nGXsfl_86_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_83_6I2e( true) ;
         }
         else
         {
            wb_table4_83_6I2e( false) ;
         }
      }

      protected void wb_table2_5_6I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptContratoGarantia.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_6I2( true) ;
         }
         else
         {
            wb_table5_14_6I2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_6I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_6I2e( true) ;
         }
         else
         {
            wb_table2_5_6I2e( false) ;
         }
      }

      protected void wb_table5_14_6I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_6I2( true) ;
         }
         else
         {
            wb_table6_19_6I2( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_6I2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_6I2e( true) ;
         }
         else
         {
            wb_table5_14_6I2e( false) ;
         }
      }

      protected void wb_table6_19_6I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptContratoGarantia.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_6I2( true) ;
         }
         else
         {
            wb_table7_28_6I2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_6I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoGarantia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", "", true, "HLP_PromptContratoGarantia.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_47_6I2( true) ;
         }
         else
         {
            wb_table8_47_6I2( false) ;
         }
         return  ;
      }

      protected void wb_table8_47_6I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoGarantia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", "", true, "HLP_PromptContratoGarantia.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_66_6I2( true) ;
         }
         else
         {
            wb_table9_66_6I2( false) ;
         }
         return  ;
      }

      protected void wb_table9_66_6I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_6I2e( true) ;
         }
         else
         {
            wb_table6_19_6I2e( false) ;
         }
      }

      protected void wb_table9_66_6I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Internalname, tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratogarantia_datapagtogarantia3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratogarantia_datapagtogarantia3_Internalname, context.localUtil.Format(AV24ContratoGarantia_DataPagtoGarantia3, "99/99/99"), context.localUtil.Format( AV24ContratoGarantia_DataPagtoGarantia3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratogarantia_datapagtogarantia3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavContratogarantia_datapagtogarantia3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratogarantia_datapagtogarantia_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratogarantia_datapagtogarantia_to3_Internalname, context.localUtil.Format(AV25ContratoGarantia_DataPagtoGarantia_To3, "99/99/99"), context.localUtil.Format( AV25ContratoGarantia_DataPagtoGarantia_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,73);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratogarantia_datapagtogarantia_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavContratogarantia_datapagtogarantia_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_66_6I2e( true) ;
         }
         else
         {
            wb_table9_66_6I2e( false) ;
         }
      }

      protected void wb_table8_47_6I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Internalname, tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratogarantia_datapagtogarantia2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratogarantia_datapagtogarantia2_Internalname, context.localUtil.Format(AV20ContratoGarantia_DataPagtoGarantia2, "99/99/99"), context.localUtil.Format( AV20ContratoGarantia_DataPagtoGarantia2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratogarantia_datapagtogarantia2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavContratogarantia_datapagtogarantia2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratogarantia_datapagtogarantia_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratogarantia_datapagtogarantia_to2_Internalname, context.localUtil.Format(AV21ContratoGarantia_DataPagtoGarantia_To2, "99/99/99"), context.localUtil.Format( AV21ContratoGarantia_DataPagtoGarantia_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratogarantia_datapagtogarantia_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavContratogarantia_datapagtogarantia_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_47_6I2e( true) ;
         }
         else
         {
            wb_table8_47_6I2e( false) ;
         }
      }

      protected void wb_table7_28_6I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Internalname, tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratogarantia_datapagtogarantia1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratogarantia_datapagtogarantia1_Internalname, context.localUtil.Format(AV16ContratoGarantia_DataPagtoGarantia1, "99/99/99"), context.localUtil.Format( AV16ContratoGarantia_DataPagtoGarantia1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,31);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratogarantia_datapagtogarantia1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavContratogarantia_datapagtogarantia1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratogarantia_datapagtogarantia_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratogarantia_datapagtogarantia_to1_Internalname, context.localUtil.Format(AV17ContratoGarantia_DataPagtoGarantia_To1, "99/99/99"), context.localUtil.Format( AV17ContratoGarantia_DataPagtoGarantia_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,35);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratogarantia_datapagtogarantia_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavContratogarantia_datapagtogarantia_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_6I2e( true) ;
         }
         else
         {
            wb_table7_28_6I2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutContratoGarantia_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoGarantia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoGarantia_Codigo), 6, 0)));
         AV8InOutContratoGarantia_DataPagtoGarantia = (DateTime)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoGarantia_DataPagtoGarantia", context.localUtil.Format(AV8InOutContratoGarantia_DataPagtoGarantia, "99/99/99"));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA6I2( ) ;
         WS6I2( ) ;
         WE6I2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042823154032");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptcontratogarantia.js", "?202042823154034");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_862( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_86_idx;
         edtContratoGarantia_Codigo_Internalname = "CONTRATOGARANTIA_CODIGO_"+sGXsfl_86_idx;
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO_"+sGXsfl_86_idx;
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO_"+sGXsfl_86_idx;
         edtContratada_Codigo_Internalname = "CONTRATADA_CODIGO_"+sGXsfl_86_idx;
         edtContratada_PessoaCod_Internalname = "CONTRATADA_PESSOACOD_"+sGXsfl_86_idx;
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM_"+sGXsfl_86_idx;
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ_"+sGXsfl_86_idx;
         edtContratoGarantia_DataPagtoGarantia_Internalname = "CONTRATOGARANTIA_DATAPAGTOGARANTIA_"+sGXsfl_86_idx;
         edtContratoGarantia_Percentual_Internalname = "CONTRATOGARANTIA_PERCENTUAL_"+sGXsfl_86_idx;
         edtContratoGarantia_DataCaucao_Internalname = "CONTRATOGARANTIA_DATACAUCAO_"+sGXsfl_86_idx;
         edtContratoGarantia_ValorGarantia_Internalname = "CONTRATOGARANTIA_VALORGARANTIA_"+sGXsfl_86_idx;
         edtContratoGarantia_DataEncerramento_Internalname = "CONTRATOGARANTIA_DATAENCERRAMENTO_"+sGXsfl_86_idx;
         edtContratoGarantia_ValorEncerramento_Internalname = "CONTRATOGARANTIA_VALORENCERRAMENTO_"+sGXsfl_86_idx;
      }

      protected void SubsflControlProps_fel_862( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_86_fel_idx;
         edtContratoGarantia_Codigo_Internalname = "CONTRATOGARANTIA_CODIGO_"+sGXsfl_86_fel_idx;
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO_"+sGXsfl_86_fel_idx;
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO_"+sGXsfl_86_fel_idx;
         edtContratada_Codigo_Internalname = "CONTRATADA_CODIGO_"+sGXsfl_86_fel_idx;
         edtContratada_PessoaCod_Internalname = "CONTRATADA_PESSOACOD_"+sGXsfl_86_fel_idx;
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM_"+sGXsfl_86_fel_idx;
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ_"+sGXsfl_86_fel_idx;
         edtContratoGarantia_DataPagtoGarantia_Internalname = "CONTRATOGARANTIA_DATAPAGTOGARANTIA_"+sGXsfl_86_fel_idx;
         edtContratoGarantia_Percentual_Internalname = "CONTRATOGARANTIA_PERCENTUAL_"+sGXsfl_86_fel_idx;
         edtContratoGarantia_DataCaucao_Internalname = "CONTRATOGARANTIA_DATACAUCAO_"+sGXsfl_86_fel_idx;
         edtContratoGarantia_ValorGarantia_Internalname = "CONTRATOGARANTIA_VALORGARANTIA_"+sGXsfl_86_fel_idx;
         edtContratoGarantia_DataEncerramento_Internalname = "CONTRATOGARANTIA_DATAENCERRAMENTO_"+sGXsfl_86_fel_idx;
         edtContratoGarantia_ValorEncerramento_Internalname = "CONTRATOGARANTIA_VALORENCERRAMENTO_"+sGXsfl_86_fel_idx;
      }

      protected void sendrow_862( )
      {
         SubsflControlProps_862( ) ;
         WB6I0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_86_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_86_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_86_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 87,'',false,'',86)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV28Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV100Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV100Select_GXI : context.PathToRelativeUrl( AV28Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_86_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV28Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoGarantia_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A101ContratoGarantia_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A101ContratoGarantia_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoGarantia_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Numero_Internalname,StringUtil.RTrim( A77Contrato_Numero),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Numero_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)127,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)-1,(bool)true,(String)"NumeroContrato",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_PessoaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A40Contratada_PessoaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_PessoaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_PessoaNom_Internalname,StringUtil.RTrim( A41Contratada_PessoaNom),StringUtil.RTrim( context.localUtil.Format( A41Contratada_PessoaNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_PessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_PessoaCNPJ_Internalname,(String)A42Contratada_PessoaCNPJ,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_PessoaCNPJ_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)-1,(bool)true,(String)"Docto",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoGarantia_DataPagtoGarantia_Internalname,context.localUtil.Format(A102ContratoGarantia_DataPagtoGarantia, "99/99/99"),context.localUtil.Format( A102ContratoGarantia_DataPagtoGarantia, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoGarantia_DataPagtoGarantia_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoGarantia_Percentual_Internalname,StringUtil.LTrim( StringUtil.NToC( A103ContratoGarantia_Percentual, 6, 2, ",", "")),context.localUtil.Format( A103ContratoGarantia_Percentual, "ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoGarantia_Percentual_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Percentual",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoGarantia_DataCaucao_Internalname,context.localUtil.Format(A104ContratoGarantia_DataCaucao, "99/99/99"),context.localUtil.Format( A104ContratoGarantia_DataCaucao, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoGarantia_DataCaucao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoGarantia_ValorGarantia_Internalname,StringUtil.LTrim( StringUtil.NToC( A105ContratoGarantia_ValorGarantia, 18, 5, ",", "")),context.localUtil.Format( A105ContratoGarantia_ValorGarantia, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoGarantia_ValorGarantia_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoGarantia_DataEncerramento_Internalname,context.localUtil.Format(A106ContratoGarantia_DataEncerramento, "99/99/99"),context.localUtil.Format( A106ContratoGarantia_DataEncerramento, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoGarantia_DataEncerramento_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoGarantia_ValorEncerramento_Internalname,StringUtil.LTrim( StringUtil.NToC( A107ContratoGarantia_ValorEncerramento, 18, 5, ",", "")),context.localUtil.Format( A107ContratoGarantia_ValorEncerramento, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoGarantia_ValorEncerramento_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOGARANTIA_CODIGO"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( (decimal)(A101ContratoGarantia_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_CODIGO"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOGARANTIA_DATAPAGTOGARANTIA"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, A102ContratoGarantia_DataPagtoGarantia));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOGARANTIA_PERCENTUAL"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( A103ContratoGarantia_Percentual, "ZZ9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOGARANTIA_DATACAUCAO"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, A104ContratoGarantia_DataCaucao));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOGARANTIA_VALORGARANTIA"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( A105ContratoGarantia_ValorGarantia, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOGARANTIA_DATAENCERRAMENTO"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, A106ContratoGarantia_DataEncerramento));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOGARANTIA_VALORENCERRAMENTO"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( A107ContratoGarantia_ValorEncerramento, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GridContainer.AddRow(GridRow);
            nGXsfl_86_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_86_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_86_idx+1));
            sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
            SubsflControlProps_862( ) ;
         }
         /* End function sendrow_862 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavContratogarantia_datapagtogarantia1_Internalname = "vCONTRATOGARANTIA_DATAPAGTOGARANTIA1";
         lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext1_Internalname = "DYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA_RANGEMIDDLETEXT1";
         edtavContratogarantia_datapagtogarantia_to1_Internalname = "vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1";
         tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavContratogarantia_datapagtogarantia2_Internalname = "vCONTRATOGARANTIA_DATAPAGTOGARANTIA2";
         lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext2_Internalname = "DYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA_RANGEMIDDLETEXT2";
         edtavContratogarantia_datapagtogarantia_to2_Internalname = "vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2";
         tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavContratogarantia_datapagtogarantia3_Internalname = "vCONTRATOGARANTIA_DATAPAGTOGARANTIA3";
         lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext3_Internalname = "DYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA_RANGEMIDDLETEXT3";
         edtavContratogarantia_datapagtogarantia_to3_Internalname = "vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3";
         tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtContratoGarantia_Codigo_Internalname = "CONTRATOGARANTIA_CODIGO";
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO";
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO";
         edtContratada_Codigo_Internalname = "CONTRATADA_CODIGO";
         edtContratada_PessoaCod_Internalname = "CONTRATADA_PESSOACOD";
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM";
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ";
         edtContratoGarantia_DataPagtoGarantia_Internalname = "CONTRATOGARANTIA_DATAPAGTOGARANTIA";
         edtContratoGarantia_Percentual_Internalname = "CONTRATOGARANTIA_PERCENTUAL";
         edtContratoGarantia_DataCaucao_Internalname = "CONTRATOGARANTIA_DATACAUCAO";
         edtContratoGarantia_ValorGarantia_Internalname = "CONTRATOGARANTIA_VALORGARANTIA";
         edtContratoGarantia_DataEncerramento_Internalname = "CONTRATOGARANTIA_DATAENCERRAMENTO";
         edtContratoGarantia_ValorEncerramento_Internalname = "CONTRATOGARANTIA_VALORENCERRAMENTO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfcontratogarantia_codigo_Internalname = "vTFCONTRATOGARANTIA_CODIGO";
         edtavTfcontratogarantia_codigo_to_Internalname = "vTFCONTRATOGARANTIA_CODIGO_TO";
         edtavTfcontrato_codigo_Internalname = "vTFCONTRATO_CODIGO";
         edtavTfcontrato_codigo_to_Internalname = "vTFCONTRATO_CODIGO_TO";
         edtavTfcontrato_numero_Internalname = "vTFCONTRATO_NUMERO";
         edtavTfcontrato_numero_sel_Internalname = "vTFCONTRATO_NUMERO_SEL";
         edtavTfcontratada_codigo_Internalname = "vTFCONTRATADA_CODIGO";
         edtavTfcontratada_codigo_to_Internalname = "vTFCONTRATADA_CODIGO_TO";
         edtavTfcontratada_pessoacod_Internalname = "vTFCONTRATADA_PESSOACOD";
         edtavTfcontratada_pessoacod_to_Internalname = "vTFCONTRATADA_PESSOACOD_TO";
         edtavTfcontratada_pessoanom_Internalname = "vTFCONTRATADA_PESSOANOM";
         edtavTfcontratada_pessoanom_sel_Internalname = "vTFCONTRATADA_PESSOANOM_SEL";
         edtavTfcontratada_pessoacnpj_Internalname = "vTFCONTRATADA_PESSOACNPJ";
         edtavTfcontratada_pessoacnpj_sel_Internalname = "vTFCONTRATADA_PESSOACNPJ_SEL";
         edtavTfcontratogarantia_datapagtogarantia_Internalname = "vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA";
         edtavTfcontratogarantia_datapagtogarantia_to_Internalname = "vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO";
         edtavDdo_contratogarantia_datapagtogarantiaauxdate_Internalname = "vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIAAUXDATE";
         edtavDdo_contratogarantia_datapagtogarantiaauxdateto_Internalname = "vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIAAUXDATETO";
         divDdo_contratogarantia_datapagtogarantiaauxdates_Internalname = "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIAAUXDATES";
         edtavTfcontratogarantia_percentual_Internalname = "vTFCONTRATOGARANTIA_PERCENTUAL";
         edtavTfcontratogarantia_percentual_to_Internalname = "vTFCONTRATOGARANTIA_PERCENTUAL_TO";
         edtavTfcontratogarantia_datacaucao_Internalname = "vTFCONTRATOGARANTIA_DATACAUCAO";
         edtavTfcontratogarantia_datacaucao_to_Internalname = "vTFCONTRATOGARANTIA_DATACAUCAO_TO";
         edtavDdo_contratogarantia_datacaucaoauxdate_Internalname = "vDDO_CONTRATOGARANTIA_DATACAUCAOAUXDATE";
         edtavDdo_contratogarantia_datacaucaoauxdateto_Internalname = "vDDO_CONTRATOGARANTIA_DATACAUCAOAUXDATETO";
         divDdo_contratogarantia_datacaucaoauxdates_Internalname = "DDO_CONTRATOGARANTIA_DATACAUCAOAUXDATES";
         edtavTfcontratogarantia_valorgarantia_Internalname = "vTFCONTRATOGARANTIA_VALORGARANTIA";
         edtavTfcontratogarantia_valorgarantia_to_Internalname = "vTFCONTRATOGARANTIA_VALORGARANTIA_TO";
         edtavTfcontratogarantia_dataencerramento_Internalname = "vTFCONTRATOGARANTIA_DATAENCERRAMENTO";
         edtavTfcontratogarantia_dataencerramento_to_Internalname = "vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO";
         edtavDdo_contratogarantia_dataencerramentoauxdate_Internalname = "vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOAUXDATE";
         edtavDdo_contratogarantia_dataencerramentoauxdateto_Internalname = "vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOAUXDATETO";
         divDdo_contratogarantia_dataencerramentoauxdates_Internalname = "DDO_CONTRATOGARANTIA_DATAENCERRAMENTOAUXDATES";
         edtavTfcontratogarantia_valorencerramento_Internalname = "vTFCONTRATOGARANTIA_VALORENCERRAMENTO";
         edtavTfcontratogarantia_valorencerramento_to_Internalname = "vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO";
         Ddo_contratogarantia_codigo_Internalname = "DDO_CONTRATOGARANTIA_CODIGO";
         edtavDdo_contratogarantia_codigotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOGARANTIA_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_contrato_codigo_Internalname = "DDO_CONTRATO_CODIGO";
         edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_contrato_numero_Internalname = "DDO_CONTRATO_NUMERO";
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE";
         Ddo_contratada_codigo_Internalname = "DDO_CONTRATADA_CODIGO";
         edtavDdo_contratada_codigotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_contratada_pessoacod_Internalname = "DDO_CONTRATADA_PESSOACOD";
         edtavDdo_contratada_pessoacodtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE";
         Ddo_contratada_pessoanom_Internalname = "DDO_CONTRATADA_PESSOANOM";
         edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE";
         Ddo_contratada_pessoacnpj_Internalname = "DDO_CONTRATADA_PESSOACNPJ";
         edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE";
         Ddo_contratogarantia_datapagtogarantia_Internalname = "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA";
         edtavDdo_contratogarantia_datapagtogarantiatitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE";
         Ddo_contratogarantia_percentual_Internalname = "DDO_CONTRATOGARANTIA_PERCENTUAL";
         edtavDdo_contratogarantia_percentualtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE";
         Ddo_contratogarantia_datacaucao_Internalname = "DDO_CONTRATOGARANTIA_DATACAUCAO";
         edtavDdo_contratogarantia_datacaucaotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE";
         Ddo_contratogarantia_valorgarantia_Internalname = "DDO_CONTRATOGARANTIA_VALORGARANTIA";
         edtavDdo_contratogarantia_valorgarantiatitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE";
         Ddo_contratogarantia_dataencerramento_Internalname = "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO";
         edtavDdo_contratogarantia_dataencerramentotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE";
         Ddo_contratogarantia_valorencerramento_Internalname = "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO";
         edtavDdo_contratogarantia_valorencerramentotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContratoGarantia_ValorEncerramento_Jsonclick = "";
         edtContratoGarantia_DataEncerramento_Jsonclick = "";
         edtContratoGarantia_ValorGarantia_Jsonclick = "";
         edtContratoGarantia_DataCaucao_Jsonclick = "";
         edtContratoGarantia_Percentual_Jsonclick = "";
         edtContratoGarantia_DataPagtoGarantia_Jsonclick = "";
         edtContratada_PessoaCNPJ_Jsonclick = "";
         edtContratada_PessoaNom_Jsonclick = "";
         edtContratada_PessoaCod_Jsonclick = "";
         edtContratada_Codigo_Jsonclick = "";
         edtContrato_Numero_Jsonclick = "";
         edtContrato_Codigo_Jsonclick = "";
         edtContratoGarantia_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavContratogarantia_datapagtogarantia_to1_Jsonclick = "";
         edtavContratogarantia_datapagtogarantia1_Jsonclick = "";
         edtavContratogarantia_datapagtogarantia_to2_Jsonclick = "";
         edtavContratogarantia_datapagtogarantia2_Jsonclick = "";
         edtavContratogarantia_datapagtogarantia_to3_Jsonclick = "";
         edtavContratogarantia_datapagtogarantia3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtContratoGarantia_ValorEncerramento_Titleformat = 0;
         edtContratoGarantia_DataEncerramento_Titleformat = 0;
         edtContratoGarantia_ValorGarantia_Titleformat = 0;
         edtContratoGarantia_DataCaucao_Titleformat = 0;
         edtContratoGarantia_Percentual_Titleformat = 0;
         edtContratoGarantia_DataPagtoGarantia_Titleformat = 0;
         edtContratada_PessoaCNPJ_Titleformat = 0;
         edtContratada_PessoaNom_Titleformat = 0;
         edtContratada_PessoaCod_Titleformat = 0;
         edtContratada_Codigo_Titleformat = 0;
         edtContrato_Numero_Titleformat = 0;
         edtContrato_Codigo_Titleformat = 0;
         edtContratoGarantia_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Visible = 1;
         tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Visible = 1;
         tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Visible = 1;
         edtContratoGarantia_ValorEncerramento_Title = "Valor no Encerramento";
         edtContratoGarantia_DataEncerramento_Title = "D. Encerraemnto da Garantia";
         edtContratoGarantia_ValorGarantia_Title = "Garantia";
         edtContratoGarantia_DataCaucao_Title = "da Cau��o";
         edtContratoGarantia_Percentual_Title = "Percentual";
         edtContratoGarantia_DataPagtoGarantia_Title = "da Garantia";
         edtContratada_PessoaCNPJ_Title = " CNPJ da Pessoa";
         edtContratada_PessoaNom_Title = "Nome";
         edtContratada_PessoaCod_Title = "C�d. da Pessoa";
         edtContratada_Codigo_Title = "Contratada";
         edtContrato_Numero_Title = "N�mero do Contrato";
         edtContrato_Codigo_Title = "Contrato";
         edtContratoGarantia_Codigo_Title = "C�digo da Garantia";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_contratogarantia_valorencerramentotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratogarantia_dataencerramentotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratogarantia_valorgarantiatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratogarantia_datacaucaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratogarantia_percentualtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratogarantia_datapagtogarantiatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratada_pessoacodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratada_codigotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratogarantia_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratogarantia_valorencerramento_to_Jsonclick = "";
         edtavTfcontratogarantia_valorencerramento_to_Visible = 1;
         edtavTfcontratogarantia_valorencerramento_Jsonclick = "";
         edtavTfcontratogarantia_valorencerramento_Visible = 1;
         edtavDdo_contratogarantia_dataencerramentoauxdateto_Jsonclick = "";
         edtavDdo_contratogarantia_dataencerramentoauxdate_Jsonclick = "";
         edtavTfcontratogarantia_dataencerramento_to_Jsonclick = "";
         edtavTfcontratogarantia_dataencerramento_to_Visible = 1;
         edtavTfcontratogarantia_dataencerramento_Jsonclick = "";
         edtavTfcontratogarantia_dataencerramento_Visible = 1;
         edtavTfcontratogarantia_valorgarantia_to_Jsonclick = "";
         edtavTfcontratogarantia_valorgarantia_to_Visible = 1;
         edtavTfcontratogarantia_valorgarantia_Jsonclick = "";
         edtavTfcontratogarantia_valorgarantia_Visible = 1;
         edtavDdo_contratogarantia_datacaucaoauxdateto_Jsonclick = "";
         edtavDdo_contratogarantia_datacaucaoauxdate_Jsonclick = "";
         edtavTfcontratogarantia_datacaucao_to_Jsonclick = "";
         edtavTfcontratogarantia_datacaucao_to_Visible = 1;
         edtavTfcontratogarantia_datacaucao_Jsonclick = "";
         edtavTfcontratogarantia_datacaucao_Visible = 1;
         edtavTfcontratogarantia_percentual_to_Jsonclick = "";
         edtavTfcontratogarantia_percentual_to_Visible = 1;
         edtavTfcontratogarantia_percentual_Jsonclick = "";
         edtavTfcontratogarantia_percentual_Visible = 1;
         edtavDdo_contratogarantia_datapagtogarantiaauxdateto_Jsonclick = "";
         edtavDdo_contratogarantia_datapagtogarantiaauxdate_Jsonclick = "";
         edtavTfcontratogarantia_datapagtogarantia_to_Jsonclick = "";
         edtavTfcontratogarantia_datapagtogarantia_to_Visible = 1;
         edtavTfcontratogarantia_datapagtogarantia_Jsonclick = "";
         edtavTfcontratogarantia_datapagtogarantia_Visible = 1;
         edtavTfcontratada_pessoacnpj_sel_Jsonclick = "";
         edtavTfcontratada_pessoacnpj_sel_Visible = 1;
         edtavTfcontratada_pessoacnpj_Jsonclick = "";
         edtavTfcontratada_pessoacnpj_Visible = 1;
         edtavTfcontratada_pessoanom_sel_Jsonclick = "";
         edtavTfcontratada_pessoanom_sel_Visible = 1;
         edtavTfcontratada_pessoanom_Jsonclick = "";
         edtavTfcontratada_pessoanom_Visible = 1;
         edtavTfcontratada_pessoacod_to_Jsonclick = "";
         edtavTfcontratada_pessoacod_to_Visible = 1;
         edtavTfcontratada_pessoacod_Jsonclick = "";
         edtavTfcontratada_pessoacod_Visible = 1;
         edtavTfcontratada_codigo_to_Jsonclick = "";
         edtavTfcontratada_codigo_to_Visible = 1;
         edtavTfcontratada_codigo_Jsonclick = "";
         edtavTfcontratada_codigo_Visible = 1;
         edtavTfcontrato_numero_sel_Jsonclick = "";
         edtavTfcontrato_numero_sel_Visible = 1;
         edtavTfcontrato_numero_Jsonclick = "";
         edtavTfcontrato_numero_Visible = 1;
         edtavTfcontrato_codigo_to_Jsonclick = "";
         edtavTfcontrato_codigo_to_Visible = 1;
         edtavTfcontrato_codigo_Jsonclick = "";
         edtavTfcontrato_codigo_Visible = 1;
         edtavTfcontratogarantia_codigo_to_Jsonclick = "";
         edtavTfcontratogarantia_codigo_to_Visible = 1;
         edtavTfcontratogarantia_codigo_Jsonclick = "";
         edtavTfcontratogarantia_codigo_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_contratogarantia_valorencerramento_Searchbuttontext = "Pesquisar";
         Ddo_contratogarantia_valorencerramento_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratogarantia_valorencerramento_Rangefilterto = "At�";
         Ddo_contratogarantia_valorencerramento_Rangefilterfrom = "Desde";
         Ddo_contratogarantia_valorencerramento_Cleanfilter = "Limpar pesquisa";
         Ddo_contratogarantia_valorencerramento_Loadingdata = "Carregando dados...";
         Ddo_contratogarantia_valorencerramento_Sortdsc = "Ordenar de Z � A";
         Ddo_contratogarantia_valorencerramento_Sortasc = "Ordenar de A � Z";
         Ddo_contratogarantia_valorencerramento_Datalistupdateminimumcharacters = 0;
         Ddo_contratogarantia_valorencerramento_Datalistfixedvalues = "";
         Ddo_contratogarantia_valorencerramento_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratogarantia_valorencerramento_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratogarantia_valorencerramento_Filtertype = "Numeric";
         Ddo_contratogarantia_valorencerramento_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratogarantia_valorencerramento_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratogarantia_valorencerramento_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratogarantia_valorencerramento_Titlecontrolidtoreplace = "";
         Ddo_contratogarantia_valorencerramento_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratogarantia_valorencerramento_Cls = "ColumnSettings";
         Ddo_contratogarantia_valorencerramento_Tooltip = "Op��es";
         Ddo_contratogarantia_valorencerramento_Caption = "";
         Ddo_contratogarantia_dataencerramento_Searchbuttontext = "Pesquisar";
         Ddo_contratogarantia_dataencerramento_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratogarantia_dataencerramento_Rangefilterto = "At�";
         Ddo_contratogarantia_dataencerramento_Rangefilterfrom = "Desde";
         Ddo_contratogarantia_dataencerramento_Cleanfilter = "Limpar pesquisa";
         Ddo_contratogarantia_dataencerramento_Loadingdata = "Carregando dados...";
         Ddo_contratogarantia_dataencerramento_Sortdsc = "Ordenar de Z � A";
         Ddo_contratogarantia_dataencerramento_Sortasc = "Ordenar de A � Z";
         Ddo_contratogarantia_dataencerramento_Datalistupdateminimumcharacters = 0;
         Ddo_contratogarantia_dataencerramento_Datalistfixedvalues = "";
         Ddo_contratogarantia_dataencerramento_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratogarantia_dataencerramento_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratogarantia_dataencerramento_Filtertype = "Date";
         Ddo_contratogarantia_dataencerramento_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratogarantia_dataencerramento_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratogarantia_dataencerramento_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratogarantia_dataencerramento_Titlecontrolidtoreplace = "";
         Ddo_contratogarantia_dataencerramento_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratogarantia_dataencerramento_Cls = "ColumnSettings";
         Ddo_contratogarantia_dataencerramento_Tooltip = "Op��es";
         Ddo_contratogarantia_dataencerramento_Caption = "";
         Ddo_contratogarantia_valorgarantia_Searchbuttontext = "Pesquisar";
         Ddo_contratogarantia_valorgarantia_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratogarantia_valorgarantia_Rangefilterto = "At�";
         Ddo_contratogarantia_valorgarantia_Rangefilterfrom = "Desde";
         Ddo_contratogarantia_valorgarantia_Cleanfilter = "Limpar pesquisa";
         Ddo_contratogarantia_valorgarantia_Loadingdata = "Carregando dados...";
         Ddo_contratogarantia_valorgarantia_Sortdsc = "Ordenar de Z � A";
         Ddo_contratogarantia_valorgarantia_Sortasc = "Ordenar de A � Z";
         Ddo_contratogarantia_valorgarantia_Datalistupdateminimumcharacters = 0;
         Ddo_contratogarantia_valorgarantia_Datalistfixedvalues = "";
         Ddo_contratogarantia_valorgarantia_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratogarantia_valorgarantia_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratogarantia_valorgarantia_Filtertype = "Numeric";
         Ddo_contratogarantia_valorgarantia_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratogarantia_valorgarantia_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratogarantia_valorgarantia_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratogarantia_valorgarantia_Titlecontrolidtoreplace = "";
         Ddo_contratogarantia_valorgarantia_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratogarantia_valorgarantia_Cls = "ColumnSettings";
         Ddo_contratogarantia_valorgarantia_Tooltip = "Op��es";
         Ddo_contratogarantia_valorgarantia_Caption = "";
         Ddo_contratogarantia_datacaucao_Searchbuttontext = "Pesquisar";
         Ddo_contratogarantia_datacaucao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratogarantia_datacaucao_Rangefilterto = "At�";
         Ddo_contratogarantia_datacaucao_Rangefilterfrom = "Desde";
         Ddo_contratogarantia_datacaucao_Cleanfilter = "Limpar pesquisa";
         Ddo_contratogarantia_datacaucao_Loadingdata = "Carregando dados...";
         Ddo_contratogarantia_datacaucao_Sortdsc = "Ordenar de Z � A";
         Ddo_contratogarantia_datacaucao_Sortasc = "Ordenar de A � Z";
         Ddo_contratogarantia_datacaucao_Datalistupdateminimumcharacters = 0;
         Ddo_contratogarantia_datacaucao_Datalistfixedvalues = "";
         Ddo_contratogarantia_datacaucao_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratogarantia_datacaucao_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratogarantia_datacaucao_Filtertype = "Date";
         Ddo_contratogarantia_datacaucao_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratogarantia_datacaucao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratogarantia_datacaucao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratogarantia_datacaucao_Titlecontrolidtoreplace = "";
         Ddo_contratogarantia_datacaucao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratogarantia_datacaucao_Cls = "ColumnSettings";
         Ddo_contratogarantia_datacaucao_Tooltip = "Op��es";
         Ddo_contratogarantia_datacaucao_Caption = "";
         Ddo_contratogarantia_percentual_Searchbuttontext = "Pesquisar";
         Ddo_contratogarantia_percentual_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratogarantia_percentual_Rangefilterto = "At�";
         Ddo_contratogarantia_percentual_Rangefilterfrom = "Desde";
         Ddo_contratogarantia_percentual_Cleanfilter = "Limpar pesquisa";
         Ddo_contratogarantia_percentual_Loadingdata = "Carregando dados...";
         Ddo_contratogarantia_percentual_Sortdsc = "Ordenar de Z � A";
         Ddo_contratogarantia_percentual_Sortasc = "Ordenar de A � Z";
         Ddo_contratogarantia_percentual_Datalistupdateminimumcharacters = 0;
         Ddo_contratogarantia_percentual_Datalistfixedvalues = "";
         Ddo_contratogarantia_percentual_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratogarantia_percentual_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratogarantia_percentual_Filtertype = "Numeric";
         Ddo_contratogarantia_percentual_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratogarantia_percentual_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratogarantia_percentual_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratogarantia_percentual_Titlecontrolidtoreplace = "";
         Ddo_contratogarantia_percentual_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratogarantia_percentual_Cls = "ColumnSettings";
         Ddo_contratogarantia_percentual_Tooltip = "Op��es";
         Ddo_contratogarantia_percentual_Caption = "";
         Ddo_contratogarantia_datapagtogarantia_Searchbuttontext = "Pesquisar";
         Ddo_contratogarantia_datapagtogarantia_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratogarantia_datapagtogarantia_Rangefilterto = "At�";
         Ddo_contratogarantia_datapagtogarantia_Rangefilterfrom = "Desde";
         Ddo_contratogarantia_datapagtogarantia_Cleanfilter = "Limpar pesquisa";
         Ddo_contratogarantia_datapagtogarantia_Loadingdata = "Carregando dados...";
         Ddo_contratogarantia_datapagtogarantia_Sortdsc = "Ordenar de Z � A";
         Ddo_contratogarantia_datapagtogarantia_Sortasc = "Ordenar de A � Z";
         Ddo_contratogarantia_datapagtogarantia_Datalistupdateminimumcharacters = 0;
         Ddo_contratogarantia_datapagtogarantia_Datalistfixedvalues = "";
         Ddo_contratogarantia_datapagtogarantia_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratogarantia_datapagtogarantia_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratogarantia_datapagtogarantia_Filtertype = "Date";
         Ddo_contratogarantia_datapagtogarantia_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratogarantia_datapagtogarantia_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratogarantia_datapagtogarantia_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratogarantia_datapagtogarantia_Titlecontrolidtoreplace = "";
         Ddo_contratogarantia_datapagtogarantia_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratogarantia_datapagtogarantia_Cls = "ColumnSettings";
         Ddo_contratogarantia_datapagtogarantia_Tooltip = "Op��es";
         Ddo_contratogarantia_datapagtogarantia_Caption = "";
         Ddo_contratada_pessoacnpj_Searchbuttontext = "Pesquisar";
         Ddo_contratada_pessoacnpj_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratada_pessoacnpj_Rangefilterto = "At�";
         Ddo_contratada_pessoacnpj_Rangefilterfrom = "Desde";
         Ddo_contratada_pessoacnpj_Cleanfilter = "Limpar pesquisa";
         Ddo_contratada_pessoacnpj_Loadingdata = "Carregando dados...";
         Ddo_contratada_pessoacnpj_Sortdsc = "Ordenar de Z � A";
         Ddo_contratada_pessoacnpj_Sortasc = "Ordenar de A � Z";
         Ddo_contratada_pessoacnpj_Datalistupdateminimumcharacters = 0;
         Ddo_contratada_pessoacnpj_Datalistproc = "GetPromptContratoGarantiaFilterData";
         Ddo_contratada_pessoacnpj_Datalistfixedvalues = "";
         Ddo_contratada_pessoacnpj_Datalisttype = "Dynamic";
         Ddo_contratada_pessoacnpj_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacnpj_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratada_pessoacnpj_Filtertype = "Character";
         Ddo_contratada_pessoacnpj_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacnpj_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacnpj_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace = "";
         Ddo_contratada_pessoacnpj_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratada_pessoacnpj_Cls = "ColumnSettings";
         Ddo_contratada_pessoacnpj_Tooltip = "Op��es";
         Ddo_contratada_pessoacnpj_Caption = "";
         Ddo_contratada_pessoanom_Searchbuttontext = "Pesquisar";
         Ddo_contratada_pessoanom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratada_pessoanom_Rangefilterto = "At�";
         Ddo_contratada_pessoanom_Rangefilterfrom = "Desde";
         Ddo_contratada_pessoanom_Cleanfilter = "Limpar pesquisa";
         Ddo_contratada_pessoanom_Loadingdata = "Carregando dados...";
         Ddo_contratada_pessoanom_Sortdsc = "Ordenar de Z � A";
         Ddo_contratada_pessoanom_Sortasc = "Ordenar de A � Z";
         Ddo_contratada_pessoanom_Datalistupdateminimumcharacters = 0;
         Ddo_contratada_pessoanom_Datalistproc = "GetPromptContratoGarantiaFilterData";
         Ddo_contratada_pessoanom_Datalistfixedvalues = "";
         Ddo_contratada_pessoanom_Datalisttype = "Dynamic";
         Ddo_contratada_pessoanom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratada_pessoanom_Filtertype = "Character";
         Ddo_contratada_pessoanom_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Titlecontrolidtoreplace = "";
         Ddo_contratada_pessoanom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratada_pessoanom_Cls = "ColumnSettings";
         Ddo_contratada_pessoanom_Tooltip = "Op��es";
         Ddo_contratada_pessoanom_Caption = "";
         Ddo_contratada_pessoacod_Searchbuttontext = "Pesquisar";
         Ddo_contratada_pessoacod_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratada_pessoacod_Rangefilterto = "At�";
         Ddo_contratada_pessoacod_Rangefilterfrom = "Desde";
         Ddo_contratada_pessoacod_Cleanfilter = "Limpar pesquisa";
         Ddo_contratada_pessoacod_Loadingdata = "Carregando dados...";
         Ddo_contratada_pessoacod_Sortdsc = "Ordenar de Z � A";
         Ddo_contratada_pessoacod_Sortasc = "Ordenar de A � Z";
         Ddo_contratada_pessoacod_Datalistupdateminimumcharacters = 0;
         Ddo_contratada_pessoacod_Datalistfixedvalues = "";
         Ddo_contratada_pessoacod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratada_pessoacod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacod_Filtertype = "Numeric";
         Ddo_contratada_pessoacod_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacod_Titlecontrolidtoreplace = "";
         Ddo_contratada_pessoacod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratada_pessoacod_Cls = "ColumnSettings";
         Ddo_contratada_pessoacod_Tooltip = "Op��es";
         Ddo_contratada_pessoacod_Caption = "";
         Ddo_contratada_codigo_Searchbuttontext = "Pesquisar";
         Ddo_contratada_codigo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratada_codigo_Rangefilterto = "At�";
         Ddo_contratada_codigo_Rangefilterfrom = "Desde";
         Ddo_contratada_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_contratada_codigo_Loadingdata = "Carregando dados...";
         Ddo_contratada_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_contratada_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_contratada_codigo_Datalistupdateminimumcharacters = 0;
         Ddo_contratada_codigo_Datalistfixedvalues = "";
         Ddo_contratada_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratada_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratada_codigo_Filtertype = "Numeric";
         Ddo_contratada_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratada_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratada_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratada_codigo_Titlecontrolidtoreplace = "";
         Ddo_contratada_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratada_codigo_Cls = "ColumnSettings";
         Ddo_contratada_codigo_Tooltip = "Op��es";
         Ddo_contratada_codigo_Caption = "";
         Ddo_contrato_numero_Searchbuttontext = "Pesquisar";
         Ddo_contrato_numero_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contrato_numero_Rangefilterto = "At�";
         Ddo_contrato_numero_Rangefilterfrom = "Desde";
         Ddo_contrato_numero_Cleanfilter = "Limpar pesquisa";
         Ddo_contrato_numero_Loadingdata = "Carregando dados...";
         Ddo_contrato_numero_Sortdsc = "Ordenar de Z � A";
         Ddo_contrato_numero_Sortasc = "Ordenar de A � Z";
         Ddo_contrato_numero_Datalistupdateminimumcharacters = 0;
         Ddo_contrato_numero_Datalistproc = "GetPromptContratoGarantiaFilterData";
         Ddo_contrato_numero_Datalistfixedvalues = "";
         Ddo_contrato_numero_Datalisttype = "Dynamic";
         Ddo_contrato_numero_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contrato_numero_Filtertype = "Character";
         Ddo_contrato_numero_Includefilter = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Titlecontrolidtoreplace = "";
         Ddo_contrato_numero_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contrato_numero_Cls = "ColumnSettings";
         Ddo_contrato_numero_Tooltip = "Op��es";
         Ddo_contrato_numero_Caption = "";
         Ddo_contrato_codigo_Searchbuttontext = "Pesquisar";
         Ddo_contrato_codigo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contrato_codigo_Rangefilterto = "At�";
         Ddo_contrato_codigo_Rangefilterfrom = "Desde";
         Ddo_contrato_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_contrato_codigo_Loadingdata = "Carregando dados...";
         Ddo_contrato_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_contrato_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_contrato_codigo_Datalistupdateminimumcharacters = 0;
         Ddo_contrato_codigo_Datalistfixedvalues = "";
         Ddo_contrato_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contrato_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contrato_codigo_Filtertype = "Numeric";
         Ddo_contrato_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_contrato_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contrato_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contrato_codigo_Titlecontrolidtoreplace = "";
         Ddo_contrato_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contrato_codigo_Cls = "ColumnSettings";
         Ddo_contrato_codigo_Tooltip = "Op��es";
         Ddo_contrato_codigo_Caption = "";
         Ddo_contratogarantia_codigo_Searchbuttontext = "Pesquisar";
         Ddo_contratogarantia_codigo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratogarantia_codigo_Rangefilterto = "At�";
         Ddo_contratogarantia_codigo_Rangefilterfrom = "Desde";
         Ddo_contratogarantia_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_contratogarantia_codigo_Loadingdata = "Carregando dados...";
         Ddo_contratogarantia_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_contratogarantia_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_contratogarantia_codigo_Datalistupdateminimumcharacters = 0;
         Ddo_contratogarantia_codigo_Datalistfixedvalues = "";
         Ddo_contratogarantia_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratogarantia_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratogarantia_codigo_Filtertype = "Numeric";
         Ddo_contratogarantia_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratogarantia_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratogarantia_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratogarantia_codigo_Titlecontrolidtoreplace = "";
         Ddo_contratogarantia_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratogarantia_codigo_Cls = "ColumnSettings";
         Ddo_contratogarantia_codigo_Tooltip = "Op��es";
         Ddo_contratogarantia_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Contrato Garantia";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV31TFContratoGarantia_Codigo',fld:'vTFCONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoGarantia_Codigo_To',fld:'vTFCONTRATOGARANTIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV60TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV65TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV66TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV69TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV70TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV75TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV76TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV79TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV80TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV85TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''}],oparms:[{av:'AV30ContratoGarantia_CodigoTitleFilterData',fld:'vCONTRATOGARANTIA_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV34Contrato_CodigoTitleFilterData',fld:'vCONTRATO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV38Contrato_NumeroTitleFilterData',fld:'vCONTRATO_NUMEROTITLEFILTERDATA',pic:'',nv:null},{av:'AV42Contratada_CodigoTitleFilterData',fld:'vCONTRATADA_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV46Contratada_PessoaCodTitleFilterData',fld:'vCONTRATADA_PESSOACODTITLEFILTERDATA',pic:'',nv:null},{av:'AV50Contratada_PessoaNomTitleFilterData',fld:'vCONTRATADA_PESSOANOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV54Contratada_PessoaCNPJTitleFilterData',fld:'vCONTRATADA_PESSOACNPJTITLEFILTERDATA',pic:'',nv:null},{av:'AV58ContratoGarantia_DataPagtoGarantiaTitleFilterData',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIATITLEFILTERDATA',pic:'',nv:null},{av:'AV64ContratoGarantia_PercentualTitleFilterData',fld:'vCONTRATOGARANTIA_PERCENTUALTITLEFILTERDATA',pic:'',nv:null},{av:'AV68ContratoGarantia_DataCaucaoTitleFilterData',fld:'vCONTRATOGARANTIA_DATACAUCAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV74ContratoGarantia_ValorGarantiaTitleFilterData',fld:'vCONTRATOGARANTIA_VALORGARANTIATITLEFILTERDATA',pic:'',nv:null},{av:'AV78ContratoGarantia_DataEncerramentoTitleFilterData',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTOTITLEFILTERDATA',pic:'',nv:null},{av:'AV84ContratoGarantia_ValorEncerramentoTitleFilterData',fld:'vCONTRATOGARANTIA_VALORENCERRAMENTOTITLEFILTERDATA',pic:'',nv:null},{av:'edtContratoGarantia_Codigo_Titleformat',ctrl:'CONTRATOGARANTIA_CODIGO',prop:'Titleformat'},{av:'edtContratoGarantia_Codigo_Title',ctrl:'CONTRATOGARANTIA_CODIGO',prop:'Title'},{av:'edtContrato_Codigo_Titleformat',ctrl:'CONTRATO_CODIGO',prop:'Titleformat'},{av:'edtContrato_Codigo_Title',ctrl:'CONTRATO_CODIGO',prop:'Title'},{av:'edtContrato_Numero_Titleformat',ctrl:'CONTRATO_NUMERO',prop:'Titleformat'},{av:'edtContrato_Numero_Title',ctrl:'CONTRATO_NUMERO',prop:'Title'},{av:'edtContratada_Codigo_Titleformat',ctrl:'CONTRATADA_CODIGO',prop:'Titleformat'},{av:'edtContratada_Codigo_Title',ctrl:'CONTRATADA_CODIGO',prop:'Title'},{av:'edtContratada_PessoaCod_Titleformat',ctrl:'CONTRATADA_PESSOACOD',prop:'Titleformat'},{av:'edtContratada_PessoaCod_Title',ctrl:'CONTRATADA_PESSOACOD',prop:'Title'},{av:'edtContratada_PessoaNom_Titleformat',ctrl:'CONTRATADA_PESSOANOM',prop:'Titleformat'},{av:'edtContratada_PessoaNom_Title',ctrl:'CONTRATADA_PESSOANOM',prop:'Title'},{av:'edtContratada_PessoaCNPJ_Titleformat',ctrl:'CONTRATADA_PESSOACNPJ',prop:'Titleformat'},{av:'edtContratada_PessoaCNPJ_Title',ctrl:'CONTRATADA_PESSOACNPJ',prop:'Title'},{av:'edtContratoGarantia_DataPagtoGarantia_Titleformat',ctrl:'CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'Titleformat'},{av:'edtContratoGarantia_DataPagtoGarantia_Title',ctrl:'CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'Title'},{av:'edtContratoGarantia_Percentual_Titleformat',ctrl:'CONTRATOGARANTIA_PERCENTUAL',prop:'Titleformat'},{av:'edtContratoGarantia_Percentual_Title',ctrl:'CONTRATOGARANTIA_PERCENTUAL',prop:'Title'},{av:'edtContratoGarantia_DataCaucao_Titleformat',ctrl:'CONTRATOGARANTIA_DATACAUCAO',prop:'Titleformat'},{av:'edtContratoGarantia_DataCaucao_Title',ctrl:'CONTRATOGARANTIA_DATACAUCAO',prop:'Title'},{av:'edtContratoGarantia_ValorGarantia_Titleformat',ctrl:'CONTRATOGARANTIA_VALORGARANTIA',prop:'Titleformat'},{av:'edtContratoGarantia_ValorGarantia_Title',ctrl:'CONTRATOGARANTIA_VALORGARANTIA',prop:'Title'},{av:'edtContratoGarantia_DataEncerramento_Titleformat',ctrl:'CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'Titleformat'},{av:'edtContratoGarantia_DataEncerramento_Title',ctrl:'CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'Title'},{av:'edtContratoGarantia_ValorEncerramento_Titleformat',ctrl:'CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'Titleformat'},{av:'edtContratoGarantia_ValorEncerramento_Title',ctrl:'CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'Title'},{av:'AV90GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV91GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E116I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoGarantia_Codigo',fld:'vTFCONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoGarantia_Codigo_To',fld:'vTFCONTRATOGARANTIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV60TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV65TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV66TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV69TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV70TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV75TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV76TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV79TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV80TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV85TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATOGARANTIA_CODIGO.ONOPTIONCLICKED","{handler:'E126I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoGarantia_Codigo',fld:'vTFCONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoGarantia_Codigo_To',fld:'vTFCONTRATOGARANTIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV60TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV65TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV66TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV69TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV70TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV75TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV76TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV79TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV80TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV85TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratogarantia_codigo_Activeeventkey',ctrl:'DDO_CONTRATOGARANTIA_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_contratogarantia_codigo_Filteredtext_get',ctrl:'DDO_CONTRATOGARANTIA_CODIGO',prop:'FilteredText_get'},{av:'Ddo_contratogarantia_codigo_Filteredtextto_get',ctrl:'DDO_CONTRATOGARANTIA_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratogarantia_codigo_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_CODIGO',prop:'SortedStatus'},{av:'AV31TFContratoGarantia_Codigo',fld:'vTFCONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoGarantia_Codigo_To',fld:'vTFCONTRATOGARANTIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacod_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datapagtogarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_percentual_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datacaucao_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorgarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_dataencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATO_CODIGO.ONOPTIONCLICKED","{handler:'E136I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoGarantia_Codigo',fld:'vTFCONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoGarantia_Codigo_To',fld:'vTFCONTRATOGARANTIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV60TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV65TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV66TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV69TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV70TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV75TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV76TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV79TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV80TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV85TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contrato_codigo_Activeeventkey',ctrl:'DDO_CONTRATO_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_contrato_codigo_Filteredtext_get',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredText_get'},{av:'Ddo_contrato_codigo_Filteredtextto_get',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratogarantia_codigo_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacod_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datapagtogarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_percentual_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datacaucao_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorgarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_dataencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATO_NUMERO.ONOPTIONCLICKED","{handler:'E146I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoGarantia_Codigo',fld:'vTFCONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoGarantia_Codigo_To',fld:'vTFCONTRATOGARANTIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV60TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV65TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV66TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV69TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV70TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV75TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV76TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV79TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV80TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV85TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contrato_numero_Activeeventkey',ctrl:'DDO_CONTRATO_NUMERO',prop:'ActiveEventKey'},{av:'Ddo_contrato_numero_Filteredtext_get',ctrl:'DDO_CONTRATO_NUMERO',prop:'FilteredText_get'},{av:'Ddo_contrato_numero_Selectedvalue_get',ctrl:'DDO_CONTRATO_NUMERO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'Ddo_contratogarantia_codigo_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacod_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datapagtogarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_percentual_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datacaucao_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorgarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_dataencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATADA_CODIGO.ONOPTIONCLICKED","{handler:'E156I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoGarantia_Codigo',fld:'vTFCONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoGarantia_Codigo_To',fld:'vTFCONTRATOGARANTIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV60TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV65TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV66TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV69TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV70TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV75TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV76TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV79TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV80TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV85TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratada_codigo_Activeeventkey',ctrl:'DDO_CONTRATADA_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_contratada_codigo_Filteredtext_get',ctrl:'DDO_CONTRATADA_CODIGO',prop:'FilteredText_get'},{av:'Ddo_contratada_codigo_Filteredtextto_get',ctrl:'DDO_CONTRATADA_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratogarantia_codigo_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacod_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datapagtogarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_percentual_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datacaucao_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorgarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_dataencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATADA_PESSOACOD.ONOPTIONCLICKED","{handler:'E166I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoGarantia_Codigo',fld:'vTFCONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoGarantia_Codigo_To',fld:'vTFCONTRATOGARANTIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV60TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV65TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV66TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV69TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV70TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV75TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV76TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV79TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV80TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV85TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratada_pessoacod_Activeeventkey',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'ActiveEventKey'},{av:'Ddo_contratada_pessoacod_Filteredtext_get',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'FilteredText_get'},{av:'Ddo_contratada_pessoacod_Filteredtextto_get',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratada_pessoacod_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'SortedStatus'},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratogarantia_codigo_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datapagtogarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_percentual_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datacaucao_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorgarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_dataencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATADA_PESSOANOM.ONOPTIONCLICKED","{handler:'E176I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoGarantia_Codigo',fld:'vTFCONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoGarantia_Codigo_To',fld:'vTFCONTRATOGARANTIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV60TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV65TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV66TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV69TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV70TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV75TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV76TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV79TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV80TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV85TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratada_pessoanom_Activeeventkey',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'ActiveEventKey'},{av:'Ddo_contratada_pessoanom_Filteredtext_get',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'FilteredText_get'},{av:'Ddo_contratada_pessoanom_Selectedvalue_get',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_contratogarantia_codigo_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacod_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datapagtogarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_percentual_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datacaucao_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorgarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_dataencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATADA_PESSOACNPJ.ONOPTIONCLICKED","{handler:'E186I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoGarantia_Codigo',fld:'vTFCONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoGarantia_Codigo_To',fld:'vTFCONTRATOGARANTIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV60TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV65TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV66TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV69TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV70TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV75TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV76TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV79TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV80TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV85TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratada_pessoacnpj_Activeeventkey',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'ActiveEventKey'},{av:'Ddo_contratada_pessoacnpj_Filteredtext_get',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'FilteredText_get'},{av:'Ddo_contratada_pessoacnpj_Selectedvalue_get',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'Ddo_contratogarantia_codigo_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacod_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datapagtogarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_percentual_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datacaucao_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorgarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_dataencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA.ONOPTIONCLICKED","{handler:'E196I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoGarantia_Codigo',fld:'vTFCONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoGarantia_Codigo_To',fld:'vTFCONTRATOGARANTIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV60TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV65TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV66TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV69TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV70TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV75TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV76TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV79TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV80TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV85TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratogarantia_datapagtogarantia_Activeeventkey',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'ActiveEventKey'},{av:'Ddo_contratogarantia_datapagtogarantia_Filteredtext_get',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'FilteredText_get'},{av:'Ddo_contratogarantia_datapagtogarantia_Filteredtextto_get',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratogarantia_datapagtogarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'SortedStatus'},{av:'AV59TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV60TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'Ddo_contratogarantia_codigo_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacod_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratogarantia_percentual_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datacaucao_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorgarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_dataencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOGARANTIA_PERCENTUAL.ONOPTIONCLICKED","{handler:'E206I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoGarantia_Codigo',fld:'vTFCONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoGarantia_Codigo_To',fld:'vTFCONTRATOGARANTIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV60TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV65TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV66TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV69TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV70TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV75TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV76TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV79TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV80TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV85TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratogarantia_percentual_Activeeventkey',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'ActiveEventKey'},{av:'Ddo_contratogarantia_percentual_Filteredtext_get',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'FilteredText_get'},{av:'Ddo_contratogarantia_percentual_Filteredtextto_get',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratogarantia_percentual_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'SortedStatus'},{av:'AV65TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV66TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'Ddo_contratogarantia_codigo_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacod_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datapagtogarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datacaucao_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorgarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_dataencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOGARANTIA_DATACAUCAO.ONOPTIONCLICKED","{handler:'E216I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoGarantia_Codigo',fld:'vTFCONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoGarantia_Codigo_To',fld:'vTFCONTRATOGARANTIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV60TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV65TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV66TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV69TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV70TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV75TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV76TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV79TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV80TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV85TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratogarantia_datacaucao_Activeeventkey',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'ActiveEventKey'},{av:'Ddo_contratogarantia_datacaucao_Filteredtext_get',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'FilteredText_get'},{av:'Ddo_contratogarantia_datacaucao_Filteredtextto_get',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratogarantia_datacaucao_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'SortedStatus'},{av:'AV69TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV70TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'Ddo_contratogarantia_codigo_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacod_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datapagtogarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_percentual_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorgarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_dataencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOGARANTIA_VALORGARANTIA.ONOPTIONCLICKED","{handler:'E226I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoGarantia_Codigo',fld:'vTFCONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoGarantia_Codigo_To',fld:'vTFCONTRATOGARANTIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV60TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV65TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV66TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV69TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV70TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV75TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV76TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV79TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV80TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV85TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratogarantia_valorgarantia_Activeeventkey',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'ActiveEventKey'},{av:'Ddo_contratogarantia_valorgarantia_Filteredtext_get',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'FilteredText_get'},{av:'Ddo_contratogarantia_valorgarantia_Filteredtextto_get',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratogarantia_valorgarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'SortedStatus'},{av:'AV75TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV76TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_contratogarantia_codigo_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacod_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datapagtogarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_percentual_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datacaucao_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_dataencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOGARANTIA_DATAENCERRAMENTO.ONOPTIONCLICKED","{handler:'E236I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoGarantia_Codigo',fld:'vTFCONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoGarantia_Codigo_To',fld:'vTFCONTRATOGARANTIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV60TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV65TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV66TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV69TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV70TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV75TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV76TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV79TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV80TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV85TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratogarantia_dataencerramento_Activeeventkey',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'ActiveEventKey'},{av:'Ddo_contratogarantia_dataencerramento_Filteredtext_get',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'FilteredText_get'},{av:'Ddo_contratogarantia_dataencerramento_Filteredtextto_get',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratogarantia_dataencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'SortedStatus'},{av:'AV79TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV80TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'Ddo_contratogarantia_codigo_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacod_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datapagtogarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_percentual_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datacaucao_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorgarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOGARANTIA_VALORENCERRAMENTO.ONOPTIONCLICKED","{handler:'E246I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoGarantia_Codigo',fld:'vTFCONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoGarantia_Codigo_To',fld:'vTFCONTRATOGARANTIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV60TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV65TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV66TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV69TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV70TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV75TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV76TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV79TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV80TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV85TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratogarantia_valorencerramento_Activeeventkey',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'ActiveEventKey'},{av:'Ddo_contratogarantia_valorencerramento_Filteredtext_get',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'FilteredText_get'},{av:'Ddo_contratogarantia_valorencerramento_Filteredtextto_get',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratogarantia_valorencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'SortedStatus'},{av:'AV85TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_contratogarantia_codigo_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacod_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datapagtogarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_percentual_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datacaucao_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorgarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_dataencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E376I2',iparms:[],oparms:[{av:'AV28Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E386I2',iparms:[{av:'A101ContratoGarantia_Codigo',fld:'CONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A102ContratoGarantia_DataPagtoGarantia',fld:'CONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',hsh:true,nv:''}],oparms:[{av:'AV7InOutContratoGarantia_Codigo',fld:'vINOUTCONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutContratoGarantia_DataPagtoGarantia',fld:'vINOUTCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E256I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoGarantia_Codigo',fld:'vTFCONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoGarantia_Codigo_To',fld:'vTFCONTRATOGARANTIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV60TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV65TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV66TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV69TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV70TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV75TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV76TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV79TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV80TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV85TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E306I2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E266I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoGarantia_Codigo',fld:'vTFCONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoGarantia_Codigo_To',fld:'vTFCONTRATOGARANTIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV60TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV65TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV66TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV69TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV70TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV75TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV76TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV79TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV80TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV85TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E316I2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E326I2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E276I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoGarantia_Codigo',fld:'vTFCONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoGarantia_Codigo_To',fld:'vTFCONTRATOGARANTIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV60TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV65TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV66TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV69TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV70TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV75TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV76TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV79TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV80TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV85TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E336I2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E286I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoGarantia_Codigo',fld:'vTFCONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoGarantia_Codigo_To',fld:'vTFCONTRATOGARANTIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV60TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV65TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV66TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV69TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV70TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV75TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV76TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV79TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV80TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV85TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E346I2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E296I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoGarantia_Codigo',fld:'vTFCONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoGarantia_Codigo_To',fld:'vTFCONTRATOGARANTIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV60TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV65TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV66TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV69TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV70TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV75TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV76TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV79TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV80TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV85TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV31TFContratoGarantia_Codigo',fld:'vTFCONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratogarantia_codigo_Filteredtext_set',ctrl:'DDO_CONTRATOGARANTIA_CODIGO',prop:'FilteredText_set'},{av:'AV32TFContratoGarantia_Codigo_To',fld:'vTFCONTRATOGARANTIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratogarantia_codigo_Filteredtextto_set',ctrl:'DDO_CONTRATOGARANTIA_CODIGO',prop:'FilteredTextTo_set'},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contrato_codigo_Filteredtext_set',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredText_set'},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contrato_codigo_Filteredtextto_set',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'Ddo_contrato_numero_Filteredtext_set',ctrl:'DDO_CONTRATO_NUMERO',prop:'FilteredText_set'},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'Ddo_contrato_numero_Selectedvalue_set',ctrl:'DDO_CONTRATO_NUMERO',prop:'SelectedValue_set'},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratada_codigo_Filteredtext_set',ctrl:'DDO_CONTRATADA_CODIGO',prop:'FilteredText_set'},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratada_codigo_Filteredtextto_set',ctrl:'DDO_CONTRATADA_CODIGO',prop:'FilteredTextTo_set'},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratada_pessoacod_Filteredtext_set',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'FilteredText_set'},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratada_pessoacod_Filteredtextto_set',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'FilteredTextTo_set'},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'Ddo_contratada_pessoanom_Filteredtext_set',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'FilteredText_set'},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_contratada_pessoanom_Selectedvalue_set',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SelectedValue_set'},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'Ddo_contratada_pessoacnpj_Filteredtext_set',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'FilteredText_set'},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'Ddo_contratada_pessoacnpj_Selectedvalue_set',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SelectedValue_set'},{av:'AV59TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'Ddo_contratogarantia_datapagtogarantia_Filteredtext_set',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'FilteredText_set'},{av:'AV60TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'Ddo_contratogarantia_datapagtogarantia_Filteredtextto_set',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'FilteredTextTo_set'},{av:'AV65TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'Ddo_contratogarantia_percentual_Filteredtext_set',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'FilteredText_set'},{av:'AV66TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'Ddo_contratogarantia_percentual_Filteredtextto_set',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'FilteredTextTo_set'},{av:'AV69TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'Ddo_contratogarantia_datacaucao_Filteredtext_set',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'FilteredText_set'},{av:'AV70TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'Ddo_contratogarantia_datacaucao_Filteredtextto_set',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'FilteredTextTo_set'},{av:'AV75TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_contratogarantia_valorgarantia_Filteredtext_set',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'FilteredText_set'},{av:'AV76TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_contratogarantia_valorgarantia_Filteredtextto_set',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'FilteredTextTo_set'},{av:'AV79TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'Ddo_contratogarantia_dataencerramento_Filteredtext_set',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'FilteredText_set'},{av:'AV80TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'Ddo_contratogarantia_dataencerramento_Filteredtextto_set',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'FilteredTextTo_set'},{av:'AV85TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_contratogarantia_valorencerramento_Filteredtext_set',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'FilteredText_set'},{av:'AV86TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_contratogarantia_valorencerramento_Filteredtextto_set',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA1',prop:'Visible'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA3',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutContratoGarantia_DataPagtoGarantia = DateTime.MinValue;
         Gridpaginationbar_Selectedpage = "";
         Ddo_contratogarantia_codigo_Activeeventkey = "";
         Ddo_contratogarantia_codigo_Filteredtext_get = "";
         Ddo_contratogarantia_codigo_Filteredtextto_get = "";
         Ddo_contrato_codigo_Activeeventkey = "";
         Ddo_contrato_codigo_Filteredtext_get = "";
         Ddo_contrato_codigo_Filteredtextto_get = "";
         Ddo_contrato_numero_Activeeventkey = "";
         Ddo_contrato_numero_Filteredtext_get = "";
         Ddo_contrato_numero_Selectedvalue_get = "";
         Ddo_contratada_codigo_Activeeventkey = "";
         Ddo_contratada_codigo_Filteredtext_get = "";
         Ddo_contratada_codigo_Filteredtextto_get = "";
         Ddo_contratada_pessoacod_Activeeventkey = "";
         Ddo_contratada_pessoacod_Filteredtext_get = "";
         Ddo_contratada_pessoacod_Filteredtextto_get = "";
         Ddo_contratada_pessoanom_Activeeventkey = "";
         Ddo_contratada_pessoanom_Filteredtext_get = "";
         Ddo_contratada_pessoanom_Selectedvalue_get = "";
         Ddo_contratada_pessoacnpj_Activeeventkey = "";
         Ddo_contratada_pessoacnpj_Filteredtext_get = "";
         Ddo_contratada_pessoacnpj_Selectedvalue_get = "";
         Ddo_contratogarantia_datapagtogarantia_Activeeventkey = "";
         Ddo_contratogarantia_datapagtogarantia_Filteredtext_get = "";
         Ddo_contratogarantia_datapagtogarantia_Filteredtextto_get = "";
         Ddo_contratogarantia_percentual_Activeeventkey = "";
         Ddo_contratogarantia_percentual_Filteredtext_get = "";
         Ddo_contratogarantia_percentual_Filteredtextto_get = "";
         Ddo_contratogarantia_datacaucao_Activeeventkey = "";
         Ddo_contratogarantia_datacaucao_Filteredtext_get = "";
         Ddo_contratogarantia_datacaucao_Filteredtextto_get = "";
         Ddo_contratogarantia_valorgarantia_Activeeventkey = "";
         Ddo_contratogarantia_valorgarantia_Filteredtext_get = "";
         Ddo_contratogarantia_valorgarantia_Filteredtextto_get = "";
         Ddo_contratogarantia_dataencerramento_Activeeventkey = "";
         Ddo_contratogarantia_dataencerramento_Filteredtext_get = "";
         Ddo_contratogarantia_dataencerramento_Filteredtextto_get = "";
         Ddo_contratogarantia_valorencerramento_Activeeventkey = "";
         Ddo_contratogarantia_valorencerramento_Filteredtext_get = "";
         Ddo_contratogarantia_valorencerramento_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV16ContratoGarantia_DataPagtoGarantia1 = DateTime.MinValue;
         AV17ContratoGarantia_DataPagtoGarantia_To1 = DateTime.MinValue;
         AV19DynamicFiltersSelector2 = "";
         AV20ContratoGarantia_DataPagtoGarantia2 = DateTime.MinValue;
         AV21ContratoGarantia_DataPagtoGarantia_To2 = DateTime.MinValue;
         AV23DynamicFiltersSelector3 = "";
         AV24ContratoGarantia_DataPagtoGarantia3 = DateTime.MinValue;
         AV25ContratoGarantia_DataPagtoGarantia_To3 = DateTime.MinValue;
         AV39TFContrato_Numero = "";
         AV40TFContrato_Numero_Sel = "";
         AV51TFContratada_PessoaNom = "";
         AV52TFContratada_PessoaNom_Sel = "";
         AV55TFContratada_PessoaCNPJ = "";
         AV56TFContratada_PessoaCNPJ_Sel = "";
         AV59TFContratoGarantia_DataPagtoGarantia = DateTime.MinValue;
         AV60TFContratoGarantia_DataPagtoGarantia_To = DateTime.MinValue;
         AV69TFContratoGarantia_DataCaucao = DateTime.MinValue;
         AV70TFContratoGarantia_DataCaucao_To = DateTime.MinValue;
         AV79TFContratoGarantia_DataEncerramento = DateTime.MinValue;
         AV80TFContratoGarantia_DataEncerramento_To = DateTime.MinValue;
         AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace = "";
         AV37ddo_Contrato_CodigoTitleControlIdToReplace = "";
         AV41ddo_Contrato_NumeroTitleControlIdToReplace = "";
         AV45ddo_Contratada_CodigoTitleControlIdToReplace = "";
         AV49ddo_Contratada_PessoaCodTitleControlIdToReplace = "";
         AV53ddo_Contratada_PessoaNomTitleControlIdToReplace = "";
         AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace = "";
         AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace = "";
         AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace = "";
         AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace = "";
         AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace = "";
         AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace = "";
         AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace = "";
         AV101Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV88DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV30ContratoGarantia_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34Contrato_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38Contrato_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42Contratada_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46Contratada_PessoaCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50Contratada_PessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54Contratada_PessoaCNPJTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58ContratoGarantia_DataPagtoGarantiaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV64ContratoGarantia_PercentualTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV68ContratoGarantia_DataCaucaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV74ContratoGarantia_ValorGarantiaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV78ContratoGarantia_DataEncerramentoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV84ContratoGarantia_ValorEncerramentoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contratogarantia_codigo_Filteredtext_set = "";
         Ddo_contratogarantia_codigo_Filteredtextto_set = "";
         Ddo_contratogarantia_codigo_Sortedstatus = "";
         Ddo_contrato_codigo_Filteredtext_set = "";
         Ddo_contrato_codigo_Filteredtextto_set = "";
         Ddo_contrato_codigo_Sortedstatus = "";
         Ddo_contrato_numero_Filteredtext_set = "";
         Ddo_contrato_numero_Selectedvalue_set = "";
         Ddo_contrato_numero_Sortedstatus = "";
         Ddo_contratada_codigo_Filteredtext_set = "";
         Ddo_contratada_codigo_Filteredtextto_set = "";
         Ddo_contratada_codigo_Sortedstatus = "";
         Ddo_contratada_pessoacod_Filteredtext_set = "";
         Ddo_contratada_pessoacod_Filteredtextto_set = "";
         Ddo_contratada_pessoacod_Sortedstatus = "";
         Ddo_contratada_pessoanom_Filteredtext_set = "";
         Ddo_contratada_pessoanom_Selectedvalue_set = "";
         Ddo_contratada_pessoanom_Sortedstatus = "";
         Ddo_contratada_pessoacnpj_Filteredtext_set = "";
         Ddo_contratada_pessoacnpj_Selectedvalue_set = "";
         Ddo_contratada_pessoacnpj_Sortedstatus = "";
         Ddo_contratogarantia_datapagtogarantia_Filteredtext_set = "";
         Ddo_contratogarantia_datapagtogarantia_Filteredtextto_set = "";
         Ddo_contratogarantia_datapagtogarantia_Sortedstatus = "";
         Ddo_contratogarantia_percentual_Filteredtext_set = "";
         Ddo_contratogarantia_percentual_Filteredtextto_set = "";
         Ddo_contratogarantia_percentual_Sortedstatus = "";
         Ddo_contratogarantia_datacaucao_Filteredtext_set = "";
         Ddo_contratogarantia_datacaucao_Filteredtextto_set = "";
         Ddo_contratogarantia_datacaucao_Sortedstatus = "";
         Ddo_contratogarantia_valorgarantia_Filteredtext_set = "";
         Ddo_contratogarantia_valorgarantia_Filteredtextto_set = "";
         Ddo_contratogarantia_valorgarantia_Sortedstatus = "";
         Ddo_contratogarantia_dataencerramento_Filteredtext_set = "";
         Ddo_contratogarantia_dataencerramento_Filteredtextto_set = "";
         Ddo_contratogarantia_dataencerramento_Sortedstatus = "";
         Ddo_contratogarantia_valorencerramento_Filteredtext_set = "";
         Ddo_contratogarantia_valorencerramento_Filteredtextto_set = "";
         Ddo_contratogarantia_valorencerramento_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV61DDO_ContratoGarantia_DataPagtoGarantiaAuxDate = DateTime.MinValue;
         AV62DDO_ContratoGarantia_DataPagtoGarantiaAuxDateTo = DateTime.MinValue;
         AV71DDO_ContratoGarantia_DataCaucaoAuxDate = DateTime.MinValue;
         AV72DDO_ContratoGarantia_DataCaucaoAuxDateTo = DateTime.MinValue;
         AV81DDO_ContratoGarantia_DataEncerramentoAuxDate = DateTime.MinValue;
         AV82DDO_ContratoGarantia_DataEncerramentoAuxDateTo = DateTime.MinValue;
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Select = "";
         AV100Select_GXI = "";
         A77Contrato_Numero = "";
         A41Contratada_PessoaNom = "";
         A42Contratada_PessoaCNPJ = "";
         A102ContratoGarantia_DataPagtoGarantia = DateTime.MinValue;
         A104ContratoGarantia_DataCaucao = DateTime.MinValue;
         A106ContratoGarantia_DataEncerramento = DateTime.MinValue;
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV39TFContrato_Numero = "";
         lV51TFContratada_PessoaNom = "";
         lV55TFContratada_PessoaCNPJ = "";
         H006I2_A107ContratoGarantia_ValorEncerramento = new decimal[1] ;
         H006I2_A106ContratoGarantia_DataEncerramento = new DateTime[] {DateTime.MinValue} ;
         H006I2_A105ContratoGarantia_ValorGarantia = new decimal[1] ;
         H006I2_A104ContratoGarantia_DataCaucao = new DateTime[] {DateTime.MinValue} ;
         H006I2_A103ContratoGarantia_Percentual = new decimal[1] ;
         H006I2_A102ContratoGarantia_DataPagtoGarantia = new DateTime[] {DateTime.MinValue} ;
         H006I2_A42Contratada_PessoaCNPJ = new String[] {""} ;
         H006I2_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         H006I2_A41Contratada_PessoaNom = new String[] {""} ;
         H006I2_n41Contratada_PessoaNom = new bool[] {false} ;
         H006I2_A40Contratada_PessoaCod = new int[1] ;
         H006I2_A39Contratada_Codigo = new int[1] ;
         H006I2_A77Contrato_Numero = new String[] {""} ;
         H006I2_A74Contrato_Codigo = new int[1] ;
         H006I2_A101ContratoGarantia_Codigo = new int[1] ;
         H006I3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptcontratogarantia__default(),
            new Object[][] {
                new Object[] {
               H006I2_A107ContratoGarantia_ValorEncerramento, H006I2_A106ContratoGarantia_DataEncerramento, H006I2_A105ContratoGarantia_ValorGarantia, H006I2_A104ContratoGarantia_DataCaucao, H006I2_A103ContratoGarantia_Percentual, H006I2_A102ContratoGarantia_DataPagtoGarantia, H006I2_A42Contratada_PessoaCNPJ, H006I2_n42Contratada_PessoaCNPJ, H006I2_A41Contratada_PessoaNom, H006I2_n41Contratada_PessoaNom,
               H006I2_A40Contratada_PessoaCod, H006I2_A39Contratada_Codigo, H006I2_A77Contrato_Numero, H006I2_A74Contrato_Codigo, H006I2_A101ContratoGarantia_Codigo
               }
               , new Object[] {
               H006I3_AGRID_nRecordCount
               }
            }
         );
         AV101Pgmname = "PromptContratoGarantia";
         /* GeneXus formulas. */
         AV101Pgmname = "PromptContratoGarantia";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_86 ;
      private short nGXsfl_86_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_86_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContratoGarantia_Codigo_Titleformat ;
      private short edtContrato_Codigo_Titleformat ;
      private short edtContrato_Numero_Titleformat ;
      private short edtContratada_Codigo_Titleformat ;
      private short edtContratada_PessoaCod_Titleformat ;
      private short edtContratada_PessoaNom_Titleformat ;
      private short edtContratada_PessoaCNPJ_Titleformat ;
      private short edtContratoGarantia_DataPagtoGarantia_Titleformat ;
      private short edtContratoGarantia_Percentual_Titleformat ;
      private short edtContratoGarantia_DataCaucao_Titleformat ;
      private short edtContratoGarantia_ValorGarantia_Titleformat ;
      private short edtContratoGarantia_DataEncerramento_Titleformat ;
      private short edtContratoGarantia_ValorEncerramento_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutContratoGarantia_Codigo ;
      private int wcpOAV7InOutContratoGarantia_Codigo ;
      private int subGrid_Rows ;
      private int AV31TFContratoGarantia_Codigo ;
      private int AV32TFContratoGarantia_Codigo_To ;
      private int AV35TFContrato_Codigo ;
      private int AV36TFContrato_Codigo_To ;
      private int AV43TFContratada_Codigo ;
      private int AV44TFContratada_Codigo_To ;
      private int AV47TFContratada_PessoaCod ;
      private int AV48TFContratada_PessoaCod_To ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contratogarantia_codigo_Datalistupdateminimumcharacters ;
      private int Ddo_contrato_codigo_Datalistupdateminimumcharacters ;
      private int Ddo_contrato_numero_Datalistupdateminimumcharacters ;
      private int Ddo_contratada_codigo_Datalistupdateminimumcharacters ;
      private int Ddo_contratada_pessoacod_Datalistupdateminimumcharacters ;
      private int Ddo_contratada_pessoanom_Datalistupdateminimumcharacters ;
      private int Ddo_contratada_pessoacnpj_Datalistupdateminimumcharacters ;
      private int Ddo_contratogarantia_datapagtogarantia_Datalistupdateminimumcharacters ;
      private int Ddo_contratogarantia_percentual_Datalistupdateminimumcharacters ;
      private int Ddo_contratogarantia_datacaucao_Datalistupdateminimumcharacters ;
      private int Ddo_contratogarantia_valorgarantia_Datalistupdateminimumcharacters ;
      private int Ddo_contratogarantia_dataencerramento_Datalistupdateminimumcharacters ;
      private int Ddo_contratogarantia_valorencerramento_Datalistupdateminimumcharacters ;
      private int edtavTfcontratogarantia_codigo_Visible ;
      private int edtavTfcontratogarantia_codigo_to_Visible ;
      private int edtavTfcontrato_codigo_Visible ;
      private int edtavTfcontrato_codigo_to_Visible ;
      private int edtavTfcontrato_numero_Visible ;
      private int edtavTfcontrato_numero_sel_Visible ;
      private int edtavTfcontratada_codigo_Visible ;
      private int edtavTfcontratada_codigo_to_Visible ;
      private int edtavTfcontratada_pessoacod_Visible ;
      private int edtavTfcontratada_pessoacod_to_Visible ;
      private int edtavTfcontratada_pessoanom_Visible ;
      private int edtavTfcontratada_pessoanom_sel_Visible ;
      private int edtavTfcontratada_pessoacnpj_Visible ;
      private int edtavTfcontratada_pessoacnpj_sel_Visible ;
      private int edtavTfcontratogarantia_datapagtogarantia_Visible ;
      private int edtavTfcontratogarantia_datapagtogarantia_to_Visible ;
      private int edtavTfcontratogarantia_percentual_Visible ;
      private int edtavTfcontratogarantia_percentual_to_Visible ;
      private int edtavTfcontratogarantia_datacaucao_Visible ;
      private int edtavTfcontratogarantia_datacaucao_to_Visible ;
      private int edtavTfcontratogarantia_valorgarantia_Visible ;
      private int edtavTfcontratogarantia_valorgarantia_to_Visible ;
      private int edtavTfcontratogarantia_dataencerramento_Visible ;
      private int edtavTfcontratogarantia_dataencerramento_to_Visible ;
      private int edtavTfcontratogarantia_valorencerramento_Visible ;
      private int edtavTfcontratogarantia_valorencerramento_to_Visible ;
      private int edtavDdo_contratogarantia_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratada_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratada_pessoacodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratogarantia_datapagtogarantiatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratogarantia_percentualtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratogarantia_datacaucaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratogarantia_valorgarantiatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratogarantia_dataencerramentotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratogarantia_valorencerramentotitlecontrolidtoreplace_Visible ;
      private int A101ContratoGarantia_Codigo ;
      private int A74Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV89PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Visible ;
      private int tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Visible ;
      private int tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV90GridCurrentPage ;
      private long AV91GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV65TFContratoGarantia_Percentual ;
      private decimal AV66TFContratoGarantia_Percentual_To ;
      private decimal AV75TFContratoGarantia_ValorGarantia ;
      private decimal AV76TFContratoGarantia_ValorGarantia_To ;
      private decimal AV85TFContratoGarantia_ValorEncerramento ;
      private decimal AV86TFContratoGarantia_ValorEncerramento_To ;
      private decimal A103ContratoGarantia_Percentual ;
      private decimal A105ContratoGarantia_ValorGarantia ;
      private decimal A107ContratoGarantia_ValorEncerramento ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contratogarantia_codigo_Activeeventkey ;
      private String Ddo_contratogarantia_codigo_Filteredtext_get ;
      private String Ddo_contratogarantia_codigo_Filteredtextto_get ;
      private String Ddo_contrato_codigo_Activeeventkey ;
      private String Ddo_contrato_codigo_Filteredtext_get ;
      private String Ddo_contrato_codigo_Filteredtextto_get ;
      private String Ddo_contrato_numero_Activeeventkey ;
      private String Ddo_contrato_numero_Filteredtext_get ;
      private String Ddo_contrato_numero_Selectedvalue_get ;
      private String Ddo_contratada_codigo_Activeeventkey ;
      private String Ddo_contratada_codigo_Filteredtext_get ;
      private String Ddo_contratada_codigo_Filteredtextto_get ;
      private String Ddo_contratada_pessoacod_Activeeventkey ;
      private String Ddo_contratada_pessoacod_Filteredtext_get ;
      private String Ddo_contratada_pessoacod_Filteredtextto_get ;
      private String Ddo_contratada_pessoanom_Activeeventkey ;
      private String Ddo_contratada_pessoanom_Filteredtext_get ;
      private String Ddo_contratada_pessoanom_Selectedvalue_get ;
      private String Ddo_contratada_pessoacnpj_Activeeventkey ;
      private String Ddo_contratada_pessoacnpj_Filteredtext_get ;
      private String Ddo_contratada_pessoacnpj_Selectedvalue_get ;
      private String Ddo_contratogarantia_datapagtogarantia_Activeeventkey ;
      private String Ddo_contratogarantia_datapagtogarantia_Filteredtext_get ;
      private String Ddo_contratogarantia_datapagtogarantia_Filteredtextto_get ;
      private String Ddo_contratogarantia_percentual_Activeeventkey ;
      private String Ddo_contratogarantia_percentual_Filteredtext_get ;
      private String Ddo_contratogarantia_percentual_Filteredtextto_get ;
      private String Ddo_contratogarantia_datacaucao_Activeeventkey ;
      private String Ddo_contratogarantia_datacaucao_Filteredtext_get ;
      private String Ddo_contratogarantia_datacaucao_Filteredtextto_get ;
      private String Ddo_contratogarantia_valorgarantia_Activeeventkey ;
      private String Ddo_contratogarantia_valorgarantia_Filteredtext_get ;
      private String Ddo_contratogarantia_valorgarantia_Filteredtextto_get ;
      private String Ddo_contratogarantia_dataencerramento_Activeeventkey ;
      private String Ddo_contratogarantia_dataencerramento_Filteredtext_get ;
      private String Ddo_contratogarantia_dataencerramento_Filteredtextto_get ;
      private String Ddo_contratogarantia_valorencerramento_Activeeventkey ;
      private String Ddo_contratogarantia_valorencerramento_Filteredtext_get ;
      private String Ddo_contratogarantia_valorencerramento_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_86_idx="0001" ;
      private String AV39TFContrato_Numero ;
      private String AV40TFContrato_Numero_Sel ;
      private String AV51TFContratada_PessoaNom ;
      private String AV52TFContratada_PessoaNom_Sel ;
      private String AV101Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contratogarantia_codigo_Caption ;
      private String Ddo_contratogarantia_codigo_Tooltip ;
      private String Ddo_contratogarantia_codigo_Cls ;
      private String Ddo_contratogarantia_codigo_Filteredtext_set ;
      private String Ddo_contratogarantia_codigo_Filteredtextto_set ;
      private String Ddo_contratogarantia_codigo_Dropdownoptionstype ;
      private String Ddo_contratogarantia_codigo_Titlecontrolidtoreplace ;
      private String Ddo_contratogarantia_codigo_Sortedstatus ;
      private String Ddo_contratogarantia_codigo_Filtertype ;
      private String Ddo_contratogarantia_codigo_Datalistfixedvalues ;
      private String Ddo_contratogarantia_codigo_Sortasc ;
      private String Ddo_contratogarantia_codigo_Sortdsc ;
      private String Ddo_contratogarantia_codigo_Loadingdata ;
      private String Ddo_contratogarantia_codigo_Cleanfilter ;
      private String Ddo_contratogarantia_codigo_Rangefilterfrom ;
      private String Ddo_contratogarantia_codigo_Rangefilterto ;
      private String Ddo_contratogarantia_codigo_Noresultsfound ;
      private String Ddo_contratogarantia_codigo_Searchbuttontext ;
      private String Ddo_contrato_codigo_Caption ;
      private String Ddo_contrato_codigo_Tooltip ;
      private String Ddo_contrato_codigo_Cls ;
      private String Ddo_contrato_codigo_Filteredtext_set ;
      private String Ddo_contrato_codigo_Filteredtextto_set ;
      private String Ddo_contrato_codigo_Dropdownoptionstype ;
      private String Ddo_contrato_codigo_Titlecontrolidtoreplace ;
      private String Ddo_contrato_codigo_Sortedstatus ;
      private String Ddo_contrato_codigo_Filtertype ;
      private String Ddo_contrato_codigo_Datalistfixedvalues ;
      private String Ddo_contrato_codigo_Sortasc ;
      private String Ddo_contrato_codigo_Sortdsc ;
      private String Ddo_contrato_codigo_Loadingdata ;
      private String Ddo_contrato_codigo_Cleanfilter ;
      private String Ddo_contrato_codigo_Rangefilterfrom ;
      private String Ddo_contrato_codigo_Rangefilterto ;
      private String Ddo_contrato_codigo_Noresultsfound ;
      private String Ddo_contrato_codigo_Searchbuttontext ;
      private String Ddo_contrato_numero_Caption ;
      private String Ddo_contrato_numero_Tooltip ;
      private String Ddo_contrato_numero_Cls ;
      private String Ddo_contrato_numero_Filteredtext_set ;
      private String Ddo_contrato_numero_Selectedvalue_set ;
      private String Ddo_contrato_numero_Dropdownoptionstype ;
      private String Ddo_contrato_numero_Titlecontrolidtoreplace ;
      private String Ddo_contrato_numero_Sortedstatus ;
      private String Ddo_contrato_numero_Filtertype ;
      private String Ddo_contrato_numero_Datalisttype ;
      private String Ddo_contrato_numero_Datalistfixedvalues ;
      private String Ddo_contrato_numero_Datalistproc ;
      private String Ddo_contrato_numero_Sortasc ;
      private String Ddo_contrato_numero_Sortdsc ;
      private String Ddo_contrato_numero_Loadingdata ;
      private String Ddo_contrato_numero_Cleanfilter ;
      private String Ddo_contrato_numero_Rangefilterfrom ;
      private String Ddo_contrato_numero_Rangefilterto ;
      private String Ddo_contrato_numero_Noresultsfound ;
      private String Ddo_contrato_numero_Searchbuttontext ;
      private String Ddo_contratada_codigo_Caption ;
      private String Ddo_contratada_codigo_Tooltip ;
      private String Ddo_contratada_codigo_Cls ;
      private String Ddo_contratada_codigo_Filteredtext_set ;
      private String Ddo_contratada_codigo_Filteredtextto_set ;
      private String Ddo_contratada_codigo_Dropdownoptionstype ;
      private String Ddo_contratada_codigo_Titlecontrolidtoreplace ;
      private String Ddo_contratada_codigo_Sortedstatus ;
      private String Ddo_contratada_codigo_Filtertype ;
      private String Ddo_contratada_codigo_Datalistfixedvalues ;
      private String Ddo_contratada_codigo_Sortasc ;
      private String Ddo_contratada_codigo_Sortdsc ;
      private String Ddo_contratada_codigo_Loadingdata ;
      private String Ddo_contratada_codigo_Cleanfilter ;
      private String Ddo_contratada_codigo_Rangefilterfrom ;
      private String Ddo_contratada_codigo_Rangefilterto ;
      private String Ddo_contratada_codigo_Noresultsfound ;
      private String Ddo_contratada_codigo_Searchbuttontext ;
      private String Ddo_contratada_pessoacod_Caption ;
      private String Ddo_contratada_pessoacod_Tooltip ;
      private String Ddo_contratada_pessoacod_Cls ;
      private String Ddo_contratada_pessoacod_Filteredtext_set ;
      private String Ddo_contratada_pessoacod_Filteredtextto_set ;
      private String Ddo_contratada_pessoacod_Dropdownoptionstype ;
      private String Ddo_contratada_pessoacod_Titlecontrolidtoreplace ;
      private String Ddo_contratada_pessoacod_Sortedstatus ;
      private String Ddo_contratada_pessoacod_Filtertype ;
      private String Ddo_contratada_pessoacod_Datalistfixedvalues ;
      private String Ddo_contratada_pessoacod_Sortasc ;
      private String Ddo_contratada_pessoacod_Sortdsc ;
      private String Ddo_contratada_pessoacod_Loadingdata ;
      private String Ddo_contratada_pessoacod_Cleanfilter ;
      private String Ddo_contratada_pessoacod_Rangefilterfrom ;
      private String Ddo_contratada_pessoacod_Rangefilterto ;
      private String Ddo_contratada_pessoacod_Noresultsfound ;
      private String Ddo_contratada_pessoacod_Searchbuttontext ;
      private String Ddo_contratada_pessoanom_Caption ;
      private String Ddo_contratada_pessoanom_Tooltip ;
      private String Ddo_contratada_pessoanom_Cls ;
      private String Ddo_contratada_pessoanom_Filteredtext_set ;
      private String Ddo_contratada_pessoanom_Selectedvalue_set ;
      private String Ddo_contratada_pessoanom_Dropdownoptionstype ;
      private String Ddo_contratada_pessoanom_Titlecontrolidtoreplace ;
      private String Ddo_contratada_pessoanom_Sortedstatus ;
      private String Ddo_contratada_pessoanom_Filtertype ;
      private String Ddo_contratada_pessoanom_Datalisttype ;
      private String Ddo_contratada_pessoanom_Datalistfixedvalues ;
      private String Ddo_contratada_pessoanom_Datalistproc ;
      private String Ddo_contratada_pessoanom_Sortasc ;
      private String Ddo_contratada_pessoanom_Sortdsc ;
      private String Ddo_contratada_pessoanom_Loadingdata ;
      private String Ddo_contratada_pessoanom_Cleanfilter ;
      private String Ddo_contratada_pessoanom_Rangefilterfrom ;
      private String Ddo_contratada_pessoanom_Rangefilterto ;
      private String Ddo_contratada_pessoanom_Noresultsfound ;
      private String Ddo_contratada_pessoanom_Searchbuttontext ;
      private String Ddo_contratada_pessoacnpj_Caption ;
      private String Ddo_contratada_pessoacnpj_Tooltip ;
      private String Ddo_contratada_pessoacnpj_Cls ;
      private String Ddo_contratada_pessoacnpj_Filteredtext_set ;
      private String Ddo_contratada_pessoacnpj_Selectedvalue_set ;
      private String Ddo_contratada_pessoacnpj_Dropdownoptionstype ;
      private String Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace ;
      private String Ddo_contratada_pessoacnpj_Sortedstatus ;
      private String Ddo_contratada_pessoacnpj_Filtertype ;
      private String Ddo_contratada_pessoacnpj_Datalisttype ;
      private String Ddo_contratada_pessoacnpj_Datalistfixedvalues ;
      private String Ddo_contratada_pessoacnpj_Datalistproc ;
      private String Ddo_contratada_pessoacnpj_Sortasc ;
      private String Ddo_contratada_pessoacnpj_Sortdsc ;
      private String Ddo_contratada_pessoacnpj_Loadingdata ;
      private String Ddo_contratada_pessoacnpj_Cleanfilter ;
      private String Ddo_contratada_pessoacnpj_Rangefilterfrom ;
      private String Ddo_contratada_pessoacnpj_Rangefilterto ;
      private String Ddo_contratada_pessoacnpj_Noresultsfound ;
      private String Ddo_contratada_pessoacnpj_Searchbuttontext ;
      private String Ddo_contratogarantia_datapagtogarantia_Caption ;
      private String Ddo_contratogarantia_datapagtogarantia_Tooltip ;
      private String Ddo_contratogarantia_datapagtogarantia_Cls ;
      private String Ddo_contratogarantia_datapagtogarantia_Filteredtext_set ;
      private String Ddo_contratogarantia_datapagtogarantia_Filteredtextto_set ;
      private String Ddo_contratogarantia_datapagtogarantia_Dropdownoptionstype ;
      private String Ddo_contratogarantia_datapagtogarantia_Titlecontrolidtoreplace ;
      private String Ddo_contratogarantia_datapagtogarantia_Sortedstatus ;
      private String Ddo_contratogarantia_datapagtogarantia_Filtertype ;
      private String Ddo_contratogarantia_datapagtogarantia_Datalistfixedvalues ;
      private String Ddo_contratogarantia_datapagtogarantia_Sortasc ;
      private String Ddo_contratogarantia_datapagtogarantia_Sortdsc ;
      private String Ddo_contratogarantia_datapagtogarantia_Loadingdata ;
      private String Ddo_contratogarantia_datapagtogarantia_Cleanfilter ;
      private String Ddo_contratogarantia_datapagtogarantia_Rangefilterfrom ;
      private String Ddo_contratogarantia_datapagtogarantia_Rangefilterto ;
      private String Ddo_contratogarantia_datapagtogarantia_Noresultsfound ;
      private String Ddo_contratogarantia_datapagtogarantia_Searchbuttontext ;
      private String Ddo_contratogarantia_percentual_Caption ;
      private String Ddo_contratogarantia_percentual_Tooltip ;
      private String Ddo_contratogarantia_percentual_Cls ;
      private String Ddo_contratogarantia_percentual_Filteredtext_set ;
      private String Ddo_contratogarantia_percentual_Filteredtextto_set ;
      private String Ddo_contratogarantia_percentual_Dropdownoptionstype ;
      private String Ddo_contratogarantia_percentual_Titlecontrolidtoreplace ;
      private String Ddo_contratogarantia_percentual_Sortedstatus ;
      private String Ddo_contratogarantia_percentual_Filtertype ;
      private String Ddo_contratogarantia_percentual_Datalistfixedvalues ;
      private String Ddo_contratogarantia_percentual_Sortasc ;
      private String Ddo_contratogarantia_percentual_Sortdsc ;
      private String Ddo_contratogarantia_percentual_Loadingdata ;
      private String Ddo_contratogarantia_percentual_Cleanfilter ;
      private String Ddo_contratogarantia_percentual_Rangefilterfrom ;
      private String Ddo_contratogarantia_percentual_Rangefilterto ;
      private String Ddo_contratogarantia_percentual_Noresultsfound ;
      private String Ddo_contratogarantia_percentual_Searchbuttontext ;
      private String Ddo_contratogarantia_datacaucao_Caption ;
      private String Ddo_contratogarantia_datacaucao_Tooltip ;
      private String Ddo_contratogarantia_datacaucao_Cls ;
      private String Ddo_contratogarantia_datacaucao_Filteredtext_set ;
      private String Ddo_contratogarantia_datacaucao_Filteredtextto_set ;
      private String Ddo_contratogarantia_datacaucao_Dropdownoptionstype ;
      private String Ddo_contratogarantia_datacaucao_Titlecontrolidtoreplace ;
      private String Ddo_contratogarantia_datacaucao_Sortedstatus ;
      private String Ddo_contratogarantia_datacaucao_Filtertype ;
      private String Ddo_contratogarantia_datacaucao_Datalistfixedvalues ;
      private String Ddo_contratogarantia_datacaucao_Sortasc ;
      private String Ddo_contratogarantia_datacaucao_Sortdsc ;
      private String Ddo_contratogarantia_datacaucao_Loadingdata ;
      private String Ddo_contratogarantia_datacaucao_Cleanfilter ;
      private String Ddo_contratogarantia_datacaucao_Rangefilterfrom ;
      private String Ddo_contratogarantia_datacaucao_Rangefilterto ;
      private String Ddo_contratogarantia_datacaucao_Noresultsfound ;
      private String Ddo_contratogarantia_datacaucao_Searchbuttontext ;
      private String Ddo_contratogarantia_valorgarantia_Caption ;
      private String Ddo_contratogarantia_valorgarantia_Tooltip ;
      private String Ddo_contratogarantia_valorgarantia_Cls ;
      private String Ddo_contratogarantia_valorgarantia_Filteredtext_set ;
      private String Ddo_contratogarantia_valorgarantia_Filteredtextto_set ;
      private String Ddo_contratogarantia_valorgarantia_Dropdownoptionstype ;
      private String Ddo_contratogarantia_valorgarantia_Titlecontrolidtoreplace ;
      private String Ddo_contratogarantia_valorgarantia_Sortedstatus ;
      private String Ddo_contratogarantia_valorgarantia_Filtertype ;
      private String Ddo_contratogarantia_valorgarantia_Datalistfixedvalues ;
      private String Ddo_contratogarantia_valorgarantia_Sortasc ;
      private String Ddo_contratogarantia_valorgarantia_Sortdsc ;
      private String Ddo_contratogarantia_valorgarantia_Loadingdata ;
      private String Ddo_contratogarantia_valorgarantia_Cleanfilter ;
      private String Ddo_contratogarantia_valorgarantia_Rangefilterfrom ;
      private String Ddo_contratogarantia_valorgarantia_Rangefilterto ;
      private String Ddo_contratogarantia_valorgarantia_Noresultsfound ;
      private String Ddo_contratogarantia_valorgarantia_Searchbuttontext ;
      private String Ddo_contratogarantia_dataencerramento_Caption ;
      private String Ddo_contratogarantia_dataencerramento_Tooltip ;
      private String Ddo_contratogarantia_dataencerramento_Cls ;
      private String Ddo_contratogarantia_dataencerramento_Filteredtext_set ;
      private String Ddo_contratogarantia_dataencerramento_Filteredtextto_set ;
      private String Ddo_contratogarantia_dataencerramento_Dropdownoptionstype ;
      private String Ddo_contratogarantia_dataencerramento_Titlecontrolidtoreplace ;
      private String Ddo_contratogarantia_dataencerramento_Sortedstatus ;
      private String Ddo_contratogarantia_dataencerramento_Filtertype ;
      private String Ddo_contratogarantia_dataencerramento_Datalistfixedvalues ;
      private String Ddo_contratogarantia_dataencerramento_Sortasc ;
      private String Ddo_contratogarantia_dataencerramento_Sortdsc ;
      private String Ddo_contratogarantia_dataencerramento_Loadingdata ;
      private String Ddo_contratogarantia_dataencerramento_Cleanfilter ;
      private String Ddo_contratogarantia_dataencerramento_Rangefilterfrom ;
      private String Ddo_contratogarantia_dataencerramento_Rangefilterto ;
      private String Ddo_contratogarantia_dataencerramento_Noresultsfound ;
      private String Ddo_contratogarantia_dataencerramento_Searchbuttontext ;
      private String Ddo_contratogarantia_valorencerramento_Caption ;
      private String Ddo_contratogarantia_valorencerramento_Tooltip ;
      private String Ddo_contratogarantia_valorencerramento_Cls ;
      private String Ddo_contratogarantia_valorencerramento_Filteredtext_set ;
      private String Ddo_contratogarantia_valorencerramento_Filteredtextto_set ;
      private String Ddo_contratogarantia_valorencerramento_Dropdownoptionstype ;
      private String Ddo_contratogarantia_valorencerramento_Titlecontrolidtoreplace ;
      private String Ddo_contratogarantia_valorencerramento_Sortedstatus ;
      private String Ddo_contratogarantia_valorencerramento_Filtertype ;
      private String Ddo_contratogarantia_valorencerramento_Datalistfixedvalues ;
      private String Ddo_contratogarantia_valorencerramento_Sortasc ;
      private String Ddo_contratogarantia_valorencerramento_Sortdsc ;
      private String Ddo_contratogarantia_valorencerramento_Loadingdata ;
      private String Ddo_contratogarantia_valorencerramento_Cleanfilter ;
      private String Ddo_contratogarantia_valorencerramento_Rangefilterfrom ;
      private String Ddo_contratogarantia_valorencerramento_Rangefilterto ;
      private String Ddo_contratogarantia_valorencerramento_Noresultsfound ;
      private String Ddo_contratogarantia_valorencerramento_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfcontratogarantia_codigo_Internalname ;
      private String edtavTfcontratogarantia_codigo_Jsonclick ;
      private String edtavTfcontratogarantia_codigo_to_Internalname ;
      private String edtavTfcontratogarantia_codigo_to_Jsonclick ;
      private String edtavTfcontrato_codigo_Internalname ;
      private String edtavTfcontrato_codigo_Jsonclick ;
      private String edtavTfcontrato_codigo_to_Internalname ;
      private String edtavTfcontrato_codigo_to_Jsonclick ;
      private String edtavTfcontrato_numero_Internalname ;
      private String edtavTfcontrato_numero_Jsonclick ;
      private String edtavTfcontrato_numero_sel_Internalname ;
      private String edtavTfcontrato_numero_sel_Jsonclick ;
      private String edtavTfcontratada_codigo_Internalname ;
      private String edtavTfcontratada_codigo_Jsonclick ;
      private String edtavTfcontratada_codigo_to_Internalname ;
      private String edtavTfcontratada_codigo_to_Jsonclick ;
      private String edtavTfcontratada_pessoacod_Internalname ;
      private String edtavTfcontratada_pessoacod_Jsonclick ;
      private String edtavTfcontratada_pessoacod_to_Internalname ;
      private String edtavTfcontratada_pessoacod_to_Jsonclick ;
      private String edtavTfcontratada_pessoanom_Internalname ;
      private String edtavTfcontratada_pessoanom_Jsonclick ;
      private String edtavTfcontratada_pessoanom_sel_Internalname ;
      private String edtavTfcontratada_pessoanom_sel_Jsonclick ;
      private String edtavTfcontratada_pessoacnpj_Internalname ;
      private String edtavTfcontratada_pessoacnpj_Jsonclick ;
      private String edtavTfcontratada_pessoacnpj_sel_Internalname ;
      private String edtavTfcontratada_pessoacnpj_sel_Jsonclick ;
      private String edtavTfcontratogarantia_datapagtogarantia_Internalname ;
      private String edtavTfcontratogarantia_datapagtogarantia_Jsonclick ;
      private String edtavTfcontratogarantia_datapagtogarantia_to_Internalname ;
      private String edtavTfcontratogarantia_datapagtogarantia_to_Jsonclick ;
      private String divDdo_contratogarantia_datapagtogarantiaauxdates_Internalname ;
      private String edtavDdo_contratogarantia_datapagtogarantiaauxdate_Internalname ;
      private String edtavDdo_contratogarantia_datapagtogarantiaauxdate_Jsonclick ;
      private String edtavDdo_contratogarantia_datapagtogarantiaauxdateto_Internalname ;
      private String edtavDdo_contratogarantia_datapagtogarantiaauxdateto_Jsonclick ;
      private String edtavTfcontratogarantia_percentual_Internalname ;
      private String edtavTfcontratogarantia_percentual_Jsonclick ;
      private String edtavTfcontratogarantia_percentual_to_Internalname ;
      private String edtavTfcontratogarantia_percentual_to_Jsonclick ;
      private String edtavTfcontratogarantia_datacaucao_Internalname ;
      private String edtavTfcontratogarantia_datacaucao_Jsonclick ;
      private String edtavTfcontratogarantia_datacaucao_to_Internalname ;
      private String edtavTfcontratogarantia_datacaucao_to_Jsonclick ;
      private String divDdo_contratogarantia_datacaucaoauxdates_Internalname ;
      private String edtavDdo_contratogarantia_datacaucaoauxdate_Internalname ;
      private String edtavDdo_contratogarantia_datacaucaoauxdate_Jsonclick ;
      private String edtavDdo_contratogarantia_datacaucaoauxdateto_Internalname ;
      private String edtavDdo_contratogarantia_datacaucaoauxdateto_Jsonclick ;
      private String edtavTfcontratogarantia_valorgarantia_Internalname ;
      private String edtavTfcontratogarantia_valorgarantia_Jsonclick ;
      private String edtavTfcontratogarantia_valorgarantia_to_Internalname ;
      private String edtavTfcontratogarantia_valorgarantia_to_Jsonclick ;
      private String edtavTfcontratogarantia_dataencerramento_Internalname ;
      private String edtavTfcontratogarantia_dataencerramento_Jsonclick ;
      private String edtavTfcontratogarantia_dataencerramento_to_Internalname ;
      private String edtavTfcontratogarantia_dataencerramento_to_Jsonclick ;
      private String divDdo_contratogarantia_dataencerramentoauxdates_Internalname ;
      private String edtavDdo_contratogarantia_dataencerramentoauxdate_Internalname ;
      private String edtavDdo_contratogarantia_dataencerramentoauxdate_Jsonclick ;
      private String edtavDdo_contratogarantia_dataencerramentoauxdateto_Internalname ;
      private String edtavDdo_contratogarantia_dataencerramentoauxdateto_Jsonclick ;
      private String edtavTfcontratogarantia_valorencerramento_Internalname ;
      private String edtavTfcontratogarantia_valorencerramento_Jsonclick ;
      private String edtavTfcontratogarantia_valorencerramento_to_Internalname ;
      private String edtavTfcontratogarantia_valorencerramento_to_Jsonclick ;
      private String edtavDdo_contratogarantia_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratada_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratada_pessoacodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratogarantia_datapagtogarantiatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratogarantia_percentualtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratogarantia_datacaucaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratogarantia_valorgarantiatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratogarantia_dataencerramentotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratogarantia_valorencerramentotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtContratoGarantia_Codigo_Internalname ;
      private String edtContrato_Codigo_Internalname ;
      private String A77Contrato_Numero ;
      private String edtContrato_Numero_Internalname ;
      private String edtContratada_Codigo_Internalname ;
      private String edtContratada_PessoaCod_Internalname ;
      private String A41Contratada_PessoaNom ;
      private String edtContratada_PessoaNom_Internalname ;
      private String edtContratada_PessoaCNPJ_Internalname ;
      private String edtContratoGarantia_DataPagtoGarantia_Internalname ;
      private String edtContratoGarantia_Percentual_Internalname ;
      private String edtContratoGarantia_DataCaucao_Internalname ;
      private String edtContratoGarantia_ValorGarantia_Internalname ;
      private String edtContratoGarantia_DataEncerramento_Internalname ;
      private String edtContratoGarantia_ValorEncerramento_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV39TFContrato_Numero ;
      private String lV51TFContratada_PessoaNom ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavContratogarantia_datapagtogarantia1_Internalname ;
      private String edtavContratogarantia_datapagtogarantia_to1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavContratogarantia_datapagtogarantia2_Internalname ;
      private String edtavContratogarantia_datapagtogarantia_to2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavContratogarantia_datapagtogarantia3_Internalname ;
      private String edtavContratogarantia_datapagtogarantia_to3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contratogarantia_codigo_Internalname ;
      private String Ddo_contrato_codigo_Internalname ;
      private String Ddo_contrato_numero_Internalname ;
      private String Ddo_contratada_codigo_Internalname ;
      private String Ddo_contratada_pessoacod_Internalname ;
      private String Ddo_contratada_pessoanom_Internalname ;
      private String Ddo_contratada_pessoacnpj_Internalname ;
      private String Ddo_contratogarantia_datapagtogarantia_Internalname ;
      private String Ddo_contratogarantia_percentual_Internalname ;
      private String Ddo_contratogarantia_datacaucao_Internalname ;
      private String Ddo_contratogarantia_valorgarantia_Internalname ;
      private String Ddo_contratogarantia_dataencerramento_Internalname ;
      private String Ddo_contratogarantia_valorencerramento_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContratoGarantia_Codigo_Title ;
      private String edtContrato_Codigo_Title ;
      private String edtContrato_Numero_Title ;
      private String edtContratada_Codigo_Title ;
      private String edtContratada_PessoaCod_Title ;
      private String edtContratada_PessoaNom_Title ;
      private String edtContratada_PessoaCNPJ_Title ;
      private String edtContratoGarantia_DataPagtoGarantia_Title ;
      private String edtContratoGarantia_Percentual_Title ;
      private String edtContratoGarantia_DataCaucao_Title ;
      private String edtContratoGarantia_ValorGarantia_Title ;
      private String edtContratoGarantia_DataEncerramento_Title ;
      private String edtContratoGarantia_ValorEncerramento_Title ;
      private String edtavSelect_Tooltiptext ;
      private String tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Internalname ;
      private String tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Internalname ;
      private String tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavContratogarantia_datapagtogarantia3_Jsonclick ;
      private String lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext3_Internalname ;
      private String lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext3_Jsonclick ;
      private String edtavContratogarantia_datapagtogarantia_to3_Jsonclick ;
      private String edtavContratogarantia_datapagtogarantia2_Jsonclick ;
      private String lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext2_Internalname ;
      private String lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext2_Jsonclick ;
      private String edtavContratogarantia_datapagtogarantia_to2_Jsonclick ;
      private String edtavContratogarantia_datapagtogarantia1_Jsonclick ;
      private String lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext1_Internalname ;
      private String lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext1_Jsonclick ;
      private String edtavContratogarantia_datapagtogarantia_to1_Jsonclick ;
      private String sGXsfl_86_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtContratoGarantia_Codigo_Jsonclick ;
      private String edtContrato_Codigo_Jsonclick ;
      private String edtContrato_Numero_Jsonclick ;
      private String edtContratada_Codigo_Jsonclick ;
      private String edtContratada_PessoaCod_Jsonclick ;
      private String edtContratada_PessoaNom_Jsonclick ;
      private String edtContratada_PessoaCNPJ_Jsonclick ;
      private String edtContratoGarantia_DataPagtoGarantia_Jsonclick ;
      private String edtContratoGarantia_Percentual_Jsonclick ;
      private String edtContratoGarantia_DataCaucao_Jsonclick ;
      private String edtContratoGarantia_ValorGarantia_Jsonclick ;
      private String edtContratoGarantia_DataEncerramento_Jsonclick ;
      private String edtContratoGarantia_ValorEncerramento_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV8InOutContratoGarantia_DataPagtoGarantia ;
      private DateTime wcpOAV8InOutContratoGarantia_DataPagtoGarantia ;
      private DateTime AV16ContratoGarantia_DataPagtoGarantia1 ;
      private DateTime AV17ContratoGarantia_DataPagtoGarantia_To1 ;
      private DateTime AV20ContratoGarantia_DataPagtoGarantia2 ;
      private DateTime AV21ContratoGarantia_DataPagtoGarantia_To2 ;
      private DateTime AV24ContratoGarantia_DataPagtoGarantia3 ;
      private DateTime AV25ContratoGarantia_DataPagtoGarantia_To3 ;
      private DateTime AV59TFContratoGarantia_DataPagtoGarantia ;
      private DateTime AV60TFContratoGarantia_DataPagtoGarantia_To ;
      private DateTime AV69TFContratoGarantia_DataCaucao ;
      private DateTime AV70TFContratoGarantia_DataCaucao_To ;
      private DateTime AV79TFContratoGarantia_DataEncerramento ;
      private DateTime AV80TFContratoGarantia_DataEncerramento_To ;
      private DateTime AV61DDO_ContratoGarantia_DataPagtoGarantiaAuxDate ;
      private DateTime AV62DDO_ContratoGarantia_DataPagtoGarantiaAuxDateTo ;
      private DateTime AV71DDO_ContratoGarantia_DataCaucaoAuxDate ;
      private DateTime AV72DDO_ContratoGarantia_DataCaucaoAuxDateTo ;
      private DateTime AV81DDO_ContratoGarantia_DataEncerramentoAuxDate ;
      private DateTime AV82DDO_ContratoGarantia_DataEncerramentoAuxDateTo ;
      private DateTime A102ContratoGarantia_DataPagtoGarantia ;
      private DateTime A104ContratoGarantia_DataCaucao ;
      private DateTime A106ContratoGarantia_DataEncerramento ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contratogarantia_codigo_Includesortasc ;
      private bool Ddo_contratogarantia_codigo_Includesortdsc ;
      private bool Ddo_contratogarantia_codigo_Includefilter ;
      private bool Ddo_contratogarantia_codigo_Filterisrange ;
      private bool Ddo_contratogarantia_codigo_Includedatalist ;
      private bool Ddo_contrato_codigo_Includesortasc ;
      private bool Ddo_contrato_codigo_Includesortdsc ;
      private bool Ddo_contrato_codigo_Includefilter ;
      private bool Ddo_contrato_codigo_Filterisrange ;
      private bool Ddo_contrato_codigo_Includedatalist ;
      private bool Ddo_contrato_numero_Includesortasc ;
      private bool Ddo_contrato_numero_Includesortdsc ;
      private bool Ddo_contrato_numero_Includefilter ;
      private bool Ddo_contrato_numero_Filterisrange ;
      private bool Ddo_contrato_numero_Includedatalist ;
      private bool Ddo_contratada_codigo_Includesortasc ;
      private bool Ddo_contratada_codigo_Includesortdsc ;
      private bool Ddo_contratada_codigo_Includefilter ;
      private bool Ddo_contratada_codigo_Filterisrange ;
      private bool Ddo_contratada_codigo_Includedatalist ;
      private bool Ddo_contratada_pessoacod_Includesortasc ;
      private bool Ddo_contratada_pessoacod_Includesortdsc ;
      private bool Ddo_contratada_pessoacod_Includefilter ;
      private bool Ddo_contratada_pessoacod_Filterisrange ;
      private bool Ddo_contratada_pessoacod_Includedatalist ;
      private bool Ddo_contratada_pessoanom_Includesortasc ;
      private bool Ddo_contratada_pessoanom_Includesortdsc ;
      private bool Ddo_contratada_pessoanom_Includefilter ;
      private bool Ddo_contratada_pessoanom_Filterisrange ;
      private bool Ddo_contratada_pessoanom_Includedatalist ;
      private bool Ddo_contratada_pessoacnpj_Includesortasc ;
      private bool Ddo_contratada_pessoacnpj_Includesortdsc ;
      private bool Ddo_contratada_pessoacnpj_Includefilter ;
      private bool Ddo_contratada_pessoacnpj_Filterisrange ;
      private bool Ddo_contratada_pessoacnpj_Includedatalist ;
      private bool Ddo_contratogarantia_datapagtogarantia_Includesortasc ;
      private bool Ddo_contratogarantia_datapagtogarantia_Includesortdsc ;
      private bool Ddo_contratogarantia_datapagtogarantia_Includefilter ;
      private bool Ddo_contratogarantia_datapagtogarantia_Filterisrange ;
      private bool Ddo_contratogarantia_datapagtogarantia_Includedatalist ;
      private bool Ddo_contratogarantia_percentual_Includesortasc ;
      private bool Ddo_contratogarantia_percentual_Includesortdsc ;
      private bool Ddo_contratogarantia_percentual_Includefilter ;
      private bool Ddo_contratogarantia_percentual_Filterisrange ;
      private bool Ddo_contratogarantia_percentual_Includedatalist ;
      private bool Ddo_contratogarantia_datacaucao_Includesortasc ;
      private bool Ddo_contratogarantia_datacaucao_Includesortdsc ;
      private bool Ddo_contratogarantia_datacaucao_Includefilter ;
      private bool Ddo_contratogarantia_datacaucao_Filterisrange ;
      private bool Ddo_contratogarantia_datacaucao_Includedatalist ;
      private bool Ddo_contratogarantia_valorgarantia_Includesortasc ;
      private bool Ddo_contratogarantia_valorgarantia_Includesortdsc ;
      private bool Ddo_contratogarantia_valorgarantia_Includefilter ;
      private bool Ddo_contratogarantia_valorgarantia_Filterisrange ;
      private bool Ddo_contratogarantia_valorgarantia_Includedatalist ;
      private bool Ddo_contratogarantia_dataencerramento_Includesortasc ;
      private bool Ddo_contratogarantia_dataencerramento_Includesortdsc ;
      private bool Ddo_contratogarantia_dataencerramento_Includefilter ;
      private bool Ddo_contratogarantia_dataencerramento_Filterisrange ;
      private bool Ddo_contratogarantia_dataencerramento_Includedatalist ;
      private bool Ddo_contratogarantia_valorencerramento_Includesortasc ;
      private bool Ddo_contratogarantia_valorencerramento_Includesortdsc ;
      private bool Ddo_contratogarantia_valorencerramento_Includefilter ;
      private bool Ddo_contratogarantia_valorencerramento_Filterisrange ;
      private bool Ddo_contratogarantia_valorencerramento_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n41Contratada_PessoaNom ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Select_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV55TFContratada_PessoaCNPJ ;
      private String AV56TFContratada_PessoaCNPJ_Sel ;
      private String AV33ddo_ContratoGarantia_CodigoTitleControlIdToReplace ;
      private String AV37ddo_Contrato_CodigoTitleControlIdToReplace ;
      private String AV41ddo_Contrato_NumeroTitleControlIdToReplace ;
      private String AV45ddo_Contratada_CodigoTitleControlIdToReplace ;
      private String AV49ddo_Contratada_PessoaCodTitleControlIdToReplace ;
      private String AV53ddo_Contratada_PessoaNomTitleControlIdToReplace ;
      private String AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace ;
      private String AV63ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace ;
      private String AV67ddo_ContratoGarantia_PercentualTitleControlIdToReplace ;
      private String AV73ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace ;
      private String AV77ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace ;
      private String AV83ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace ;
      private String AV87ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace ;
      private String AV100Select_GXI ;
      private String A42Contratada_PessoaCNPJ ;
      private String lV55TFContratada_PessoaCNPJ ;
      private String AV28Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutContratoGarantia_Codigo ;
      private DateTime aP1_InOutContratoGarantia_DataPagtoGarantia ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private decimal[] H006I2_A107ContratoGarantia_ValorEncerramento ;
      private DateTime[] H006I2_A106ContratoGarantia_DataEncerramento ;
      private decimal[] H006I2_A105ContratoGarantia_ValorGarantia ;
      private DateTime[] H006I2_A104ContratoGarantia_DataCaucao ;
      private decimal[] H006I2_A103ContratoGarantia_Percentual ;
      private DateTime[] H006I2_A102ContratoGarantia_DataPagtoGarantia ;
      private String[] H006I2_A42Contratada_PessoaCNPJ ;
      private bool[] H006I2_n42Contratada_PessoaCNPJ ;
      private String[] H006I2_A41Contratada_PessoaNom ;
      private bool[] H006I2_n41Contratada_PessoaNom ;
      private int[] H006I2_A40Contratada_PessoaCod ;
      private int[] H006I2_A39Contratada_Codigo ;
      private String[] H006I2_A77Contrato_Numero ;
      private int[] H006I2_A74Contrato_Codigo ;
      private int[] H006I2_A101ContratoGarantia_Codigo ;
      private long[] H006I3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV30ContratoGarantia_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV34Contrato_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38Contrato_NumeroTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV42Contratada_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV46Contratada_PessoaCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV50Contratada_PessoaNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV54Contratada_PessoaCNPJTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV58ContratoGarantia_DataPagtoGarantiaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV64ContratoGarantia_PercentualTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV68ContratoGarantia_DataCaucaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV74ContratoGarantia_ValorGarantiaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV78ContratoGarantia_DataEncerramentoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV84ContratoGarantia_ValorEncerramentoTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV88DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptcontratogarantia__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H006I2( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             DateTime AV16ContratoGarantia_DataPagtoGarantia1 ,
                                             DateTime AV17ContratoGarantia_DataPagtoGarantia_To1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             DateTime AV20ContratoGarantia_DataPagtoGarantia2 ,
                                             DateTime AV21ContratoGarantia_DataPagtoGarantia_To2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             DateTime AV24ContratoGarantia_DataPagtoGarantia3 ,
                                             DateTime AV25ContratoGarantia_DataPagtoGarantia_To3 ,
                                             int AV31TFContratoGarantia_Codigo ,
                                             int AV32TFContratoGarantia_Codigo_To ,
                                             int AV35TFContrato_Codigo ,
                                             int AV36TFContrato_Codigo_To ,
                                             String AV40TFContrato_Numero_Sel ,
                                             String AV39TFContrato_Numero ,
                                             int AV43TFContratada_Codigo ,
                                             int AV44TFContratada_Codigo_To ,
                                             int AV47TFContratada_PessoaCod ,
                                             int AV48TFContratada_PessoaCod_To ,
                                             String AV52TFContratada_PessoaNom_Sel ,
                                             String AV51TFContratada_PessoaNom ,
                                             String AV56TFContratada_PessoaCNPJ_Sel ,
                                             String AV55TFContratada_PessoaCNPJ ,
                                             DateTime AV59TFContratoGarantia_DataPagtoGarantia ,
                                             DateTime AV60TFContratoGarantia_DataPagtoGarantia_To ,
                                             decimal AV65TFContratoGarantia_Percentual ,
                                             decimal AV66TFContratoGarantia_Percentual_To ,
                                             DateTime AV69TFContratoGarantia_DataCaucao ,
                                             DateTime AV70TFContratoGarantia_DataCaucao_To ,
                                             decimal AV75TFContratoGarantia_ValorGarantia ,
                                             decimal AV76TFContratoGarantia_ValorGarantia_To ,
                                             DateTime AV79TFContratoGarantia_DataEncerramento ,
                                             DateTime AV80TFContratoGarantia_DataEncerramento_To ,
                                             decimal AV85TFContratoGarantia_ValorEncerramento ,
                                             decimal AV86TFContratoGarantia_ValorEncerramento_To ,
                                             DateTime A102ContratoGarantia_DataPagtoGarantia ,
                                             int A101ContratoGarantia_Codigo ,
                                             int A74Contrato_Codigo ,
                                             String A77Contrato_Numero ,
                                             int A39Contratada_Codigo ,
                                             int A40Contratada_PessoaCod ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             decimal A103ContratoGarantia_Percentual ,
                                             DateTime A104ContratoGarantia_DataCaucao ,
                                             decimal A105ContratoGarantia_ValorGarantia ,
                                             DateTime A106ContratoGarantia_DataEncerramento ,
                                             decimal A107ContratoGarantia_ValorEncerramento ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [37] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[ContratoGarantia_ValorEncerramento], T1.[ContratoGarantia_DataEncerramento], T1.[ContratoGarantia_ValorGarantia], T1.[ContratoGarantia_DataCaucao], T1.[ContratoGarantia_Percentual], T1.[ContratoGarantia_DataPagtoGarantia], T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T4.[Pessoa_Nome] AS Contratada_PessoaNom, T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T2.[Contratada_Codigo], T2.[Contrato_Numero], T1.[Contrato_Codigo], T1.[ContratoGarantia_Codigo]";
         sFromString = " FROM ((([ContratoGarantia] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV16ContratoGarantia_DataPagtoGarantia1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV16ContratoGarantia_DataPagtoGarantia1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV16ContratoGarantia_DataPagtoGarantia1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV17ContratoGarantia_DataPagtoGarantia_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV17ContratoGarantia_DataPagtoGarantia_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV17ContratoGarantia_DataPagtoGarantia_To1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV20ContratoGarantia_DataPagtoGarantia2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV20ContratoGarantia_DataPagtoGarantia2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV20ContratoGarantia_DataPagtoGarantia2)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV21ContratoGarantia_DataPagtoGarantia_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV21ContratoGarantia_DataPagtoGarantia_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV21ContratoGarantia_DataPagtoGarantia_To2)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV24ContratoGarantia_DataPagtoGarantia3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV24ContratoGarantia_DataPagtoGarantia3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV24ContratoGarantia_DataPagtoGarantia3)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV25ContratoGarantia_DataPagtoGarantia_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV25ContratoGarantia_DataPagtoGarantia_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV25ContratoGarantia_DataPagtoGarantia_To3)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (0==AV31TFContratoGarantia_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Codigo] >= @AV31TFContratoGarantia_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Codigo] >= @AV31TFContratoGarantia_Codigo)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! (0==AV32TFContratoGarantia_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Codigo] <= @AV32TFContratoGarantia_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Codigo] <= @AV32TFContratoGarantia_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! (0==AV35TFContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV35TFContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV35TFContrato_Codigo)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! (0==AV36TFContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] <= @AV36TFContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] <= @AV36TFContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV40TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV39TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV39TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV40TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV40TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (0==AV43TFContratada_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratada_Codigo] >= @AV43TFContratada_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratada_Codigo] >= @AV43TFContratada_Codigo)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! (0==AV44TFContratada_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratada_Codigo] <= @AV44TFContratada_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratada_Codigo] <= @AV44TFContratada_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! (0==AV47TFContratada_PessoaCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contratada_PessoaCod] >= @AV47TFContratada_PessoaCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contratada_PessoaCod] >= @AV47TFContratada_PessoaCod)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! (0==AV48TFContratada_PessoaCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contratada_PessoaCod] <= @AV48TFContratada_PessoaCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contratada_PessoaCod] <= @AV48TFContratada_PessoaCod_To)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV52TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51TFContratada_PessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV51TFContratada_PessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV51TFContratada_PessoaNom)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFContratada_PessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV52TFContratada_PessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV52TFContratada_PessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV56TFContratada_PessoaCNPJ_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55TFContratada_PessoaCNPJ)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV55TFContratada_PessoaCNPJ)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV55TFContratada_PessoaCNPJ)";
            }
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFContratada_PessoaCNPJ_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV56TFContratada_PessoaCNPJ_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV56TFContratada_PessoaCNPJ_Sel)";
            }
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV59TFContratoGarantia_DataPagtoGarantia) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV59TFContratoGarantia_DataPagtoGarantia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV59TFContratoGarantia_DataPagtoGarantia)";
            }
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( ! (DateTime.MinValue==AV60TFContratoGarantia_DataPagtoGarantia_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV60TFContratoGarantia_DataPagtoGarantia_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV60TFContratoGarantia_DataPagtoGarantia_To)";
            }
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV65TFContratoGarantia_Percentual) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Percentual] >= @AV65TFContratoGarantia_Percentual)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Percentual] >= @AV65TFContratoGarantia_Percentual)";
            }
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV66TFContratoGarantia_Percentual_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Percentual] <= @AV66TFContratoGarantia_Percentual_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Percentual] <= @AV66TFContratoGarantia_Percentual_To)";
            }
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( ! (DateTime.MinValue==AV69TFContratoGarantia_DataCaucao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] >= @AV69TFContratoGarantia_DataCaucao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] >= @AV69TFContratoGarantia_DataCaucao)";
            }
         }
         else
         {
            GXv_int2[24] = 1;
         }
         if ( ! (DateTime.MinValue==AV70TFContratoGarantia_DataCaucao_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] <= @AV70TFContratoGarantia_DataCaucao_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] <= @AV70TFContratoGarantia_DataCaucao_To)";
            }
         }
         else
         {
            GXv_int2[25] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV75TFContratoGarantia_ValorGarantia) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorGarantia] >= @AV75TFContratoGarantia_ValorGarantia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorGarantia] >= @AV75TFContratoGarantia_ValorGarantia)";
            }
         }
         else
         {
            GXv_int2[26] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV76TFContratoGarantia_ValorGarantia_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorGarantia] <= @AV76TFContratoGarantia_ValorGarantia_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorGarantia] <= @AV76TFContratoGarantia_ValorGarantia_To)";
            }
         }
         else
         {
            GXv_int2[27] = 1;
         }
         if ( ! (DateTime.MinValue==AV79TFContratoGarantia_DataEncerramento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] >= @AV79TFContratoGarantia_DataEncerramento)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] >= @AV79TFContratoGarantia_DataEncerramento)";
            }
         }
         else
         {
            GXv_int2[28] = 1;
         }
         if ( ! (DateTime.MinValue==AV80TFContratoGarantia_DataEncerramento_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] <= @AV80TFContratoGarantia_DataEncerramento_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] <= @AV80TFContratoGarantia_DataEncerramento_To)";
            }
         }
         else
         {
            GXv_int2[29] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV85TFContratoGarantia_ValorEncerramento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorEncerramento] >= @AV85TFContratoGarantia_ValorEncerramento)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorEncerramento] >= @AV85TFContratoGarantia_ValorEncerramento)";
            }
         }
         else
         {
            GXv_int2[30] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV86TFContratoGarantia_ValorEncerramento_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorEncerramento] <= @AV86TFContratoGarantia_ValorEncerramento_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorEncerramento] <= @AV86TFContratoGarantia_ValorEncerramento_To)";
            }
         }
         else
         {
            GXv_int2[31] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoGarantia_DataPagtoGarantia]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoGarantia_DataPagtoGarantia] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoGarantia_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoGarantia_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Numero]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Numero] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contratada_Codigo]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contratada_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Contratada_PessoaCod]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Contratada_PessoaCod] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Docto]";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Docto] DESC";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoGarantia_Percentual]";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoGarantia_Percentual] DESC";
         }
         else if ( ( AV13OrderedBy == 10 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoGarantia_DataCaucao]";
         }
         else if ( ( AV13OrderedBy == 10 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoGarantia_DataCaucao] DESC";
         }
         else if ( ( AV13OrderedBy == 11 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoGarantia_ValorGarantia]";
         }
         else if ( ( AV13OrderedBy == 11 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoGarantia_ValorGarantia] DESC";
         }
         else if ( ( AV13OrderedBy == 12 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoGarantia_DataEncerramento]";
         }
         else if ( ( AV13OrderedBy == 12 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoGarantia_DataEncerramento] DESC";
         }
         else if ( ( AV13OrderedBy == 13 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoGarantia_ValorEncerramento]";
         }
         else if ( ( AV13OrderedBy == 13 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoGarantia_ValorEncerramento] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoGarantia_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H006I3( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             DateTime AV16ContratoGarantia_DataPagtoGarantia1 ,
                                             DateTime AV17ContratoGarantia_DataPagtoGarantia_To1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             DateTime AV20ContratoGarantia_DataPagtoGarantia2 ,
                                             DateTime AV21ContratoGarantia_DataPagtoGarantia_To2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             DateTime AV24ContratoGarantia_DataPagtoGarantia3 ,
                                             DateTime AV25ContratoGarantia_DataPagtoGarantia_To3 ,
                                             int AV31TFContratoGarantia_Codigo ,
                                             int AV32TFContratoGarantia_Codigo_To ,
                                             int AV35TFContrato_Codigo ,
                                             int AV36TFContrato_Codigo_To ,
                                             String AV40TFContrato_Numero_Sel ,
                                             String AV39TFContrato_Numero ,
                                             int AV43TFContratada_Codigo ,
                                             int AV44TFContratada_Codigo_To ,
                                             int AV47TFContratada_PessoaCod ,
                                             int AV48TFContratada_PessoaCod_To ,
                                             String AV52TFContratada_PessoaNom_Sel ,
                                             String AV51TFContratada_PessoaNom ,
                                             String AV56TFContratada_PessoaCNPJ_Sel ,
                                             String AV55TFContratada_PessoaCNPJ ,
                                             DateTime AV59TFContratoGarantia_DataPagtoGarantia ,
                                             DateTime AV60TFContratoGarantia_DataPagtoGarantia_To ,
                                             decimal AV65TFContratoGarantia_Percentual ,
                                             decimal AV66TFContratoGarantia_Percentual_To ,
                                             DateTime AV69TFContratoGarantia_DataCaucao ,
                                             DateTime AV70TFContratoGarantia_DataCaucao_To ,
                                             decimal AV75TFContratoGarantia_ValorGarantia ,
                                             decimal AV76TFContratoGarantia_ValorGarantia_To ,
                                             DateTime AV79TFContratoGarantia_DataEncerramento ,
                                             DateTime AV80TFContratoGarantia_DataEncerramento_To ,
                                             decimal AV85TFContratoGarantia_ValorEncerramento ,
                                             decimal AV86TFContratoGarantia_ValorEncerramento_To ,
                                             DateTime A102ContratoGarantia_DataPagtoGarantia ,
                                             int A101ContratoGarantia_Codigo ,
                                             int A74Contrato_Codigo ,
                                             String A77Contrato_Numero ,
                                             int A39Contratada_Codigo ,
                                             int A40Contratada_PessoaCod ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             decimal A103ContratoGarantia_Percentual ,
                                             DateTime A104ContratoGarantia_DataCaucao ,
                                             decimal A105ContratoGarantia_ValorGarantia ,
                                             DateTime A106ContratoGarantia_DataEncerramento ,
                                             decimal A107ContratoGarantia_ValorEncerramento ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [32] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((([ContratoGarantia] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV16ContratoGarantia_DataPagtoGarantia1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV16ContratoGarantia_DataPagtoGarantia1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV16ContratoGarantia_DataPagtoGarantia1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV17ContratoGarantia_DataPagtoGarantia_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV17ContratoGarantia_DataPagtoGarantia_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV17ContratoGarantia_DataPagtoGarantia_To1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV20ContratoGarantia_DataPagtoGarantia2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV20ContratoGarantia_DataPagtoGarantia2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV20ContratoGarantia_DataPagtoGarantia2)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV21ContratoGarantia_DataPagtoGarantia_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV21ContratoGarantia_DataPagtoGarantia_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV21ContratoGarantia_DataPagtoGarantia_To2)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV24ContratoGarantia_DataPagtoGarantia3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV24ContratoGarantia_DataPagtoGarantia3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV24ContratoGarantia_DataPagtoGarantia3)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV25ContratoGarantia_DataPagtoGarantia_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV25ContratoGarantia_DataPagtoGarantia_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV25ContratoGarantia_DataPagtoGarantia_To3)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (0==AV31TFContratoGarantia_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Codigo] >= @AV31TFContratoGarantia_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Codigo] >= @AV31TFContratoGarantia_Codigo)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! (0==AV32TFContratoGarantia_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Codigo] <= @AV32TFContratoGarantia_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Codigo] <= @AV32TFContratoGarantia_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! (0==AV35TFContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV35TFContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV35TFContrato_Codigo)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! (0==AV36TFContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] <= @AV36TFContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] <= @AV36TFContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV40TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV39TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV39TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV40TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV40TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (0==AV43TFContratada_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratada_Codigo] >= @AV43TFContratada_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratada_Codigo] >= @AV43TFContratada_Codigo)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! (0==AV44TFContratada_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratada_Codigo] <= @AV44TFContratada_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratada_Codigo] <= @AV44TFContratada_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! (0==AV47TFContratada_PessoaCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contratada_PessoaCod] >= @AV47TFContratada_PessoaCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contratada_PessoaCod] >= @AV47TFContratada_PessoaCod)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! (0==AV48TFContratada_PessoaCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contratada_PessoaCod] <= @AV48TFContratada_PessoaCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contratada_PessoaCod] <= @AV48TFContratada_PessoaCod_To)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV52TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51TFContratada_PessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV51TFContratada_PessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV51TFContratada_PessoaNom)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFContratada_PessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV52TFContratada_PessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV52TFContratada_PessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV56TFContratada_PessoaCNPJ_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55TFContratada_PessoaCNPJ)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV55TFContratada_PessoaCNPJ)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV55TFContratada_PessoaCNPJ)";
            }
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFContratada_PessoaCNPJ_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV56TFContratada_PessoaCNPJ_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV56TFContratada_PessoaCNPJ_Sel)";
            }
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV59TFContratoGarantia_DataPagtoGarantia) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV59TFContratoGarantia_DataPagtoGarantia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV59TFContratoGarantia_DataPagtoGarantia)";
            }
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( ! (DateTime.MinValue==AV60TFContratoGarantia_DataPagtoGarantia_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV60TFContratoGarantia_DataPagtoGarantia_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV60TFContratoGarantia_DataPagtoGarantia_To)";
            }
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV65TFContratoGarantia_Percentual) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Percentual] >= @AV65TFContratoGarantia_Percentual)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Percentual] >= @AV65TFContratoGarantia_Percentual)";
            }
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV66TFContratoGarantia_Percentual_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Percentual] <= @AV66TFContratoGarantia_Percentual_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Percentual] <= @AV66TFContratoGarantia_Percentual_To)";
            }
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( ! (DateTime.MinValue==AV69TFContratoGarantia_DataCaucao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] >= @AV69TFContratoGarantia_DataCaucao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] >= @AV69TFContratoGarantia_DataCaucao)";
            }
         }
         else
         {
            GXv_int4[24] = 1;
         }
         if ( ! (DateTime.MinValue==AV70TFContratoGarantia_DataCaucao_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] <= @AV70TFContratoGarantia_DataCaucao_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] <= @AV70TFContratoGarantia_DataCaucao_To)";
            }
         }
         else
         {
            GXv_int4[25] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV75TFContratoGarantia_ValorGarantia) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorGarantia] >= @AV75TFContratoGarantia_ValorGarantia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorGarantia] >= @AV75TFContratoGarantia_ValorGarantia)";
            }
         }
         else
         {
            GXv_int4[26] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV76TFContratoGarantia_ValorGarantia_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorGarantia] <= @AV76TFContratoGarantia_ValorGarantia_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorGarantia] <= @AV76TFContratoGarantia_ValorGarantia_To)";
            }
         }
         else
         {
            GXv_int4[27] = 1;
         }
         if ( ! (DateTime.MinValue==AV79TFContratoGarantia_DataEncerramento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] >= @AV79TFContratoGarantia_DataEncerramento)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] >= @AV79TFContratoGarantia_DataEncerramento)";
            }
         }
         else
         {
            GXv_int4[28] = 1;
         }
         if ( ! (DateTime.MinValue==AV80TFContratoGarantia_DataEncerramento_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] <= @AV80TFContratoGarantia_DataEncerramento_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] <= @AV80TFContratoGarantia_DataEncerramento_To)";
            }
         }
         else
         {
            GXv_int4[29] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV85TFContratoGarantia_ValorEncerramento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorEncerramento] >= @AV85TFContratoGarantia_ValorEncerramento)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorEncerramento] >= @AV85TFContratoGarantia_ValorEncerramento)";
            }
         }
         else
         {
            GXv_int4[30] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV86TFContratoGarantia_ValorEncerramento_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorEncerramento] <= @AV86TFContratoGarantia_ValorEncerramento_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorEncerramento] <= @AV86TFContratoGarantia_ValorEncerramento_To)";
            }
         }
         else
         {
            GXv_int4[31] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 10 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 10 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 11 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 11 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 12 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 12 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 13 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 13 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H006I2(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (decimal)dynConstraints[27] , (decimal)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (decimal)dynConstraints[31] , (decimal)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (decimal)dynConstraints[35] , (decimal)dynConstraints[36] , (DateTime)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (String)dynConstraints[40] , (int)dynConstraints[41] , (int)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (decimal)dynConstraints[45] , (DateTime)dynConstraints[46] , (decimal)dynConstraints[47] , (DateTime)dynConstraints[48] , (decimal)dynConstraints[49] , (short)dynConstraints[50] , (bool)dynConstraints[51] );
               case 1 :
                     return conditional_H006I3(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (decimal)dynConstraints[27] , (decimal)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (decimal)dynConstraints[31] , (decimal)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (decimal)dynConstraints[35] , (decimal)dynConstraints[36] , (DateTime)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (String)dynConstraints[40] , (int)dynConstraints[41] , (int)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (decimal)dynConstraints[45] , (DateTime)dynConstraints[46] , (decimal)dynConstraints[47] , (DateTime)dynConstraints[48] , (decimal)dynConstraints[49] , (short)dynConstraints[50] , (bool)dynConstraints[51] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH006I2 ;
          prmH006I2 = new Object[] {
          new Object[] {"@AV16ContratoGarantia_DataPagtoGarantia1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV17ContratoGarantia_DataPagtoGarantia_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV20ContratoGarantia_DataPagtoGarantia2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV21ContratoGarantia_DataPagtoGarantia_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV24ContratoGarantia_DataPagtoGarantia3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV25ContratoGarantia_DataPagtoGarantia_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV31TFContratoGarantia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32TFContratoGarantia_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35TFContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36TFContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV39TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV40TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV43TFContratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV44TFContratada_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV47TFContratada_PessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV48TFContratada_PessoaCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV51TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV52TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV55TFContratada_PessoaCNPJ",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV56TFContratada_PessoaCNPJ_Sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV59TFContratoGarantia_DataPagtoGarantia",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV60TFContratoGarantia_DataPagtoGarantia_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV65TFContratoGarantia_Percentual",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV66TFContratoGarantia_Percentual_To",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV69TFContratoGarantia_DataCaucao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV70TFContratoGarantia_DataCaucao_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV75TFContratoGarantia_ValorGarantia",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV76TFContratoGarantia_ValorGarantia_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV79TFContratoGarantia_DataEncerramento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV80TFContratoGarantia_DataEncerramento_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV85TFContratoGarantia_ValorEncerramento",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV86TFContratoGarantia_ValorEncerramento_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH006I3 ;
          prmH006I3 = new Object[] {
          new Object[] {"@AV16ContratoGarantia_DataPagtoGarantia1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV17ContratoGarantia_DataPagtoGarantia_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV20ContratoGarantia_DataPagtoGarantia2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV21ContratoGarantia_DataPagtoGarantia_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV24ContratoGarantia_DataPagtoGarantia3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV25ContratoGarantia_DataPagtoGarantia_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV31TFContratoGarantia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32TFContratoGarantia_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35TFContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36TFContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV39TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV40TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV43TFContratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV44TFContratada_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV47TFContratada_PessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV48TFContratada_PessoaCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV51TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV52TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV55TFContratada_PessoaCNPJ",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV56TFContratada_PessoaCNPJ_Sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV59TFContratoGarantia_DataPagtoGarantia",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV60TFContratoGarantia_DataPagtoGarantia_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV65TFContratoGarantia_Percentual",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV66TFContratoGarantia_Percentual_To",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV69TFContratoGarantia_DataCaucao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV70TFContratoGarantia_DataCaucao_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV75TFContratoGarantia_ValorGarantia",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV76TFContratoGarantia_ValorGarantia_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV79TFContratoGarantia_DataEncerramento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV80TFContratoGarantia_DataEncerramento_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV85TFContratoGarantia_ValorEncerramento",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV86TFContratoGarantia_ValorEncerramento_To",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H006I2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006I2,11,0,true,false )
             ,new CursorDef("H006I3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006I3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(6) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(7);
                ((String[]) buf[8])[0] = rslt.getString(8, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                ((int[]) buf[11])[0] = rslt.getInt(10) ;
                ((String[]) buf[12])[0] = rslt.getString(11, 20) ;
                ((int[]) buf[13])[0] = rslt.getInt(12) ;
                ((int[]) buf[14])[0] = rslt.getInt(13) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[50]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[51]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[52]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[57]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[58]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[59]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[60]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[61]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[62]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[63]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[64]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[65]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[66]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[67]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[68]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[69]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[70]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[71]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[72]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[73]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[52]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[53]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[54]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[55]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[56]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[57]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[58]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[59]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[60]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[61]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[62]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[63]);
                }
                return;
       }
    }

 }

}
