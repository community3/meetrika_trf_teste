/*
               File: WWFuncaoAPF
        Description:  Fun��es APF - An�lise de Ponto de Fun��o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:58:8.99
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwfuncaoapf : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwfuncaoapf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwfuncaoapf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavFuncaoapf_tipo1 = new GXCombobox();
         cmbavFuncaoapf_complexidade1 = new GXCombobox();
         cmbavFuncaoapf_ativo1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavFuncaoapf_tipo2 = new GXCombobox();
         cmbavFuncaoapf_complexidade2 = new GXCombobox();
         cmbavFuncaoapf_ativo2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavFuncaoapf_tipo3 = new GXCombobox();
         cmbavFuncaoapf_complexidade3 = new GXCombobox();
         cmbavFuncaoapf_ativo3 = new GXCombobox();
         cmbavDynamicfiltersselector4 = new GXCombobox();
         cmbavFuncaoapf_tipo4 = new GXCombobox();
         cmbavFuncaoapf_complexidade4 = new GXCombobox();
         cmbavFuncaoapf_ativo4 = new GXCombobox();
         cmbFuncaoAPF_Tipo = new GXCombobox();
         cmbFuncaoAPF_Complexidade = new GXCombobox();
         cmbFuncaoAPF_Ativo = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
         chkavDynamicfiltersenabled4 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_96 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_96_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_96_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV17FuncaoAPF_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoAPF_Nome1", AV17FuncaoAPF_Nome1);
               AV76FuncaoAPF_Tipo1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76FuncaoAPF_Tipo1", AV76FuncaoAPF_Tipo1);
               AV55FuncaoAPF_Ativo1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55FuncaoAPF_Ativo1", AV55FuncaoAPF_Ativo1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV21FuncaoAPF_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21FuncaoAPF_Nome2", AV21FuncaoAPF_Nome2);
               AV78FuncaoAPF_Tipo2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78FuncaoAPF_Tipo2", AV78FuncaoAPF_Tipo2);
               AV56FuncaoAPF_Ativo2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56FuncaoAPF_Ativo2", AV56FuncaoAPF_Ativo2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV25FuncaoAPF_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25FuncaoAPF_Nome3", AV25FuncaoAPF_Nome3);
               AV80FuncaoAPF_Tipo3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80FuncaoAPF_Tipo3", AV80FuncaoAPF_Tipo3);
               AV82FuncaoAPF_Ativo3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82FuncaoAPF_Ativo3", AV82FuncaoAPF_Ativo3);
               AV84DynamicFiltersSelector4 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84DynamicFiltersSelector4", AV84DynamicFiltersSelector4);
               AV85FuncaoAPF_Nome4 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85FuncaoAPF_Nome4", AV85FuncaoAPF_Nome4);
               AV86FuncaoAPF_Tipo4 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86FuncaoAPF_Tipo4", AV86FuncaoAPF_Tipo4);
               AV88FuncaoAPF_Ativo4 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88FuncaoAPF_Ativo4", AV88FuncaoAPF_Ativo4);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV83DynamicFiltersEnabled4 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83DynamicFiltersEnabled4", AV83DynamicFiltersEnabled4);
               AV92TFFuncaoAPF_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92TFFuncaoAPF_Nome", AV92TFFuncaoAPF_Nome);
               AV93TFFuncaoAPF_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93TFFuncaoAPF_Nome_Sel", AV93TFFuncaoAPF_Nome_Sel);
               AV116TFFuncaoAPF_FunAPFPaiNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV116TFFuncaoAPF_FunAPFPaiNom", AV116TFFuncaoAPF_FunAPFPaiNom);
               AV117TFFuncaoAPF_FunAPFPaiNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117TFFuncaoAPF_FunAPFPaiNom_Sel", AV117TFFuncaoAPF_FunAPFPaiNom_Sel);
               AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace", AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace);
               AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace", AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace);
               AV102ddo_FuncaoAPF_TDTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102ddo_FuncaoAPF_TDTitleControlIdToReplace", AV102ddo_FuncaoAPF_TDTitleControlIdToReplace);
               AV106ddo_FuncaoAPF_ARTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV106ddo_FuncaoAPF_ARTitleControlIdToReplace", AV106ddo_FuncaoAPF_ARTitleControlIdToReplace);
               AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace", AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace);
               AV114ddo_FuncaoAPF_PFTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV114ddo_FuncaoAPF_PFTitleControlIdToReplace", AV114ddo_FuncaoAPF_PFTitleControlIdToReplace);
               AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace", AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace);
               AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace", AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace);
               AV77FuncaoAPF_Complexidade1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77FuncaoAPF_Complexidade1", AV77FuncaoAPF_Complexidade1);
               AV79FuncaoAPF_Complexidade2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79FuncaoAPF_Complexidade2", AV79FuncaoAPF_Complexidade2);
               AV81FuncaoAPF_Complexidade3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81FuncaoAPF_Complexidade3", AV81FuncaoAPF_Complexidade3);
               AV87FuncaoAPF_Complexidade4 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87FuncaoAPF_Complexidade4", AV87FuncaoAPF_Complexidade4);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV97TFFuncaoAPF_Tipo_Sels);
               AV100TFFuncaoAPF_TD = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100TFFuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(AV100TFFuncaoAPF_TD), 4, 0)));
               AV101TFFuncaoAPF_TD_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101TFFuncaoAPF_TD_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV101TFFuncaoAPF_TD_To), 4, 0)));
               AV104TFFuncaoAPF_AR = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104TFFuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV104TFFuncaoAPF_AR), 4, 0)));
               AV105TFFuncaoAPF_AR_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFFuncaoAPF_AR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105TFFuncaoAPF_AR_To), 4, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV109TFFuncaoAPF_Complexidade_Sels);
               AV112TFFuncaoAPF_PF = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV112TFFuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( AV112TFFuncaoAPF_PF, 14, 5)));
               AV113TFFuncaoAPF_PF_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV113TFFuncaoAPF_PF_To", StringUtil.LTrim( StringUtil.Str( AV113TFFuncaoAPF_PF_To, 14, 5)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV121TFFuncaoAPF_Ativo_Sels);
               AV165Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               A165FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV57FuncaoAPF_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57FuncaoAPF_SistemaCod), 6, 0)));
               A358FuncaoAPF_FunAPFPaiCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n358FuncaoAPF_FunAPFPaiCod = false;
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17FuncaoAPF_Nome1, AV76FuncaoAPF_Tipo1, AV55FuncaoAPF_Ativo1, AV19DynamicFiltersSelector2, AV21FuncaoAPF_Nome2, AV78FuncaoAPF_Tipo2, AV56FuncaoAPF_Ativo2, AV23DynamicFiltersSelector3, AV25FuncaoAPF_Nome3, AV80FuncaoAPF_Tipo3, AV82FuncaoAPF_Ativo3, AV84DynamicFiltersSelector4, AV85FuncaoAPF_Nome4, AV86FuncaoAPF_Tipo4, AV88FuncaoAPF_Ativo4, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV83DynamicFiltersEnabled4, AV92TFFuncaoAPF_Nome, AV93TFFuncaoAPF_Nome_Sel, AV116TFFuncaoAPF_FunAPFPaiNom, AV117TFFuncaoAPF_FunAPFPaiNom_Sel, AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace, AV102ddo_FuncaoAPF_TDTitleControlIdToReplace, AV106ddo_FuncaoAPF_ARTitleControlIdToReplace, AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, AV114ddo_FuncaoAPF_PFTitleControlIdToReplace, AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace, AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace, AV77FuncaoAPF_Complexidade1, AV79FuncaoAPF_Complexidade2, AV81FuncaoAPF_Complexidade3, AV87FuncaoAPF_Complexidade4, AV97TFFuncaoAPF_Tipo_Sels, AV100TFFuncaoAPF_TD, AV101TFFuncaoAPF_TD_To, AV104TFFuncaoAPF_AR, AV105TFFuncaoAPF_AR_To, AV109TFFuncaoAPF_Complexidade_Sels, AV112TFFuncaoAPF_PF, AV113TFFuncaoAPF_PF_To, AV121TFFuncaoAPF_Ativo_Sels, AV165Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A165FuncaoAPF_Codigo, AV57FuncaoAPF_SistemaCod, A358FuncaoAPF_FunAPFPaiCod) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA3Z2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START3Z2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205181258983");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwfuncaoapf.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_NOME1", AV17FuncaoAPF_Nome1);
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_TIPO1", StringUtil.RTrim( AV76FuncaoAPF_Tipo1));
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_ATIVO1", StringUtil.RTrim( AV55FuncaoAPF_Ativo1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_NOME2", AV21FuncaoAPF_Nome2);
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_TIPO2", StringUtil.RTrim( AV78FuncaoAPF_Tipo2));
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_ATIVO2", StringUtil.RTrim( AV56FuncaoAPF_Ativo2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_NOME3", AV25FuncaoAPF_Nome3);
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_TIPO3", StringUtil.RTrim( AV80FuncaoAPF_Tipo3));
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_ATIVO3", StringUtil.RTrim( AV82FuncaoAPF_Ativo3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR4", AV84DynamicFiltersSelector4);
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_NOME4", AV85FuncaoAPF_Nome4);
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_TIPO4", StringUtil.RTrim( AV86FuncaoAPF_Tipo4));
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_ATIVO4", StringUtil.RTrim( AV88FuncaoAPF_Ativo4));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED4", StringUtil.BoolToStr( AV83DynamicFiltersEnabled4));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPF_NOME", AV92TFFuncaoAPF_Nome);
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPF_NOME_SEL", AV93TFFuncaoAPF_Nome_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPF_FUNAPFPAINOM", AV116TFFuncaoAPF_FunAPFPaiNom);
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPF_FUNAPFPAINOM_SEL", AV117TFFuncaoAPF_FunAPFPaiNom_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_96", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_96), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV123DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV123DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAOAPF_NOMETITLEFILTERDATA", AV91FuncaoAPF_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAOAPF_NOMETITLEFILTERDATA", AV91FuncaoAPF_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAOAPF_TIPOTITLEFILTERDATA", AV95FuncaoAPF_TipoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAOAPF_TIPOTITLEFILTERDATA", AV95FuncaoAPF_TipoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAOAPF_TDTITLEFILTERDATA", AV99FuncaoAPF_TDTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAOAPF_TDTITLEFILTERDATA", AV99FuncaoAPF_TDTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAOAPF_ARTITLEFILTERDATA", AV103FuncaoAPF_ARTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAOAPF_ARTITLEFILTERDATA", AV103FuncaoAPF_ARTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAOAPF_COMPLEXIDADETITLEFILTERDATA", AV107FuncaoAPF_ComplexidadeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAOAPF_COMPLEXIDADETITLEFILTERDATA", AV107FuncaoAPF_ComplexidadeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAOAPF_PFTITLEFILTERDATA", AV111FuncaoAPF_PFTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAOAPF_PFTITLEFILTERDATA", AV111FuncaoAPF_PFTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAOAPF_FUNAPFPAINOMTITLEFILTERDATA", AV115FuncaoAPF_FunAPFPaiNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAOAPF_FUNAPFPAINOMTITLEFILTERDATA", AV115FuncaoAPF_FunAPFPaiNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAOAPF_ATIVOTITLEFILTERDATA", AV119FuncaoAPF_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAOAPF_ATIVOTITLEFILTERDATA", AV119FuncaoAPF_AtivoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFFUNCAOAPF_TIPO_SELS", AV97TFFuncaoAPF_Tipo_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFFUNCAOAPF_TIPO_SELS", AV97TFFuncaoAPF_Tipo_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFFUNCAOAPF_COMPLEXIDADE_SELS", AV109TFFuncaoAPF_Complexidade_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFFUNCAOAPF_COMPLEXIDADE_SELS", AV109TFFuncaoAPF_Complexidade_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFFUNCAOAPF_ATIVO_SELS", AV121TFFuncaoAPF_Ativo_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFFUNCAOAPF_ATIVO_SELS", AV121TFFuncaoAPF_Ativo_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV165Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vFUNCAOAPF_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV57FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Caption", StringUtil.RTrim( Ddo_funcaoapf_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Tooltip", StringUtil.RTrim( Ddo_funcaoapf_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Cls", StringUtil.RTrim( Ddo_funcaoapf_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapf_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaoapf_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapf_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapf_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapf_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapf_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Sortedstatus", StringUtil.RTrim( Ddo_funcaoapf_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapf_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Filtertype", StringUtil.RTrim( Ddo_funcaoapf_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapf_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapf_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Datalisttype", StringUtil.RTrim( Ddo_funcaoapf_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Datalistproc", StringUtil.RTrim( Ddo_funcaoapf_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaoapf_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Sortasc", StringUtil.RTrim( Ddo_funcaoapf_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Sortdsc", StringUtil.RTrim( Ddo_funcaoapf_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Loadingdata", StringUtil.RTrim( Ddo_funcaoapf_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapf_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Noresultsfound", StringUtil.RTrim( Ddo_funcaoapf_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapf_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Caption", StringUtil.RTrim( Ddo_funcaoapf_tipo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Tooltip", StringUtil.RTrim( Ddo_funcaoapf_tipo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Cls", StringUtil.RTrim( Ddo_funcaoapf_tipo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaoapf_tipo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapf_tipo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapf_tipo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapf_tipo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapf_tipo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Sortedstatus", StringUtil.RTrim( Ddo_funcaoapf_tipo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapf_tipo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapf_tipo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Datalisttype", StringUtil.RTrim( Ddo_funcaoapf_tipo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Allowmultipleselection", StringUtil.BoolToStr( Ddo_funcaoapf_tipo_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Datalistfixedvalues", StringUtil.RTrim( Ddo_funcaoapf_tipo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Sortasc", StringUtil.RTrim( Ddo_funcaoapf_tipo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Sortdsc", StringUtil.RTrim( Ddo_funcaoapf_tipo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapf_tipo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapf_tipo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Caption", StringUtil.RTrim( Ddo_funcaoapf_td_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Tooltip", StringUtil.RTrim( Ddo_funcaoapf_td_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Cls", StringUtil.RTrim( Ddo_funcaoapf_td_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapf_td_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Filteredtextto_set", StringUtil.RTrim( Ddo_funcaoapf_td_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapf_td_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapf_td_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapf_td_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapf_td_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapf_td_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Filtertype", StringUtil.RTrim( Ddo_funcaoapf_td_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapf_td_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapf_td_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapf_td_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaoapf_td_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Rangefilterto", StringUtil.RTrim( Ddo_funcaoapf_td_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapf_td_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Caption", StringUtil.RTrim( Ddo_funcaoapf_ar_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Tooltip", StringUtil.RTrim( Ddo_funcaoapf_ar_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Cls", StringUtil.RTrim( Ddo_funcaoapf_ar_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapf_ar_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Filteredtextto_set", StringUtil.RTrim( Ddo_funcaoapf_ar_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapf_ar_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapf_ar_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapf_ar_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapf_ar_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapf_ar_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Filtertype", StringUtil.RTrim( Ddo_funcaoapf_ar_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapf_ar_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapf_ar_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapf_ar_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaoapf_ar_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Rangefilterto", StringUtil.RTrim( Ddo_funcaoapf_ar_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapf_ar_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Caption", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Tooltip", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Cls", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapf_complexidade_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapf_complexidade_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapf_complexidade_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapf_complexidade_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Datalisttype", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Allowmultipleselection", StringUtil.BoolToStr( Ddo_funcaoapf_complexidade_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Datalistfixedvalues", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Caption", StringUtil.RTrim( Ddo_funcaoapf_pf_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Tooltip", StringUtil.RTrim( Ddo_funcaoapf_pf_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Cls", StringUtil.RTrim( Ddo_funcaoapf_pf_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapf_pf_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Filteredtextto_set", StringUtil.RTrim( Ddo_funcaoapf_pf_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapf_pf_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapf_pf_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapf_pf_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapf_pf_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapf_pf_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Filtertype", StringUtil.RTrim( Ddo_funcaoapf_pf_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapf_pf_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapf_pf_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapf_pf_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaoapf_pf_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Rangefilterto", StringUtil.RTrim( Ddo_funcaoapf_pf_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapf_pf_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Caption", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Tooltip", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Cls", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapf_funapfpainom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapf_funapfpainom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Sortedstatus", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapf_funapfpainom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Filtertype", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapf_funapfpainom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapf_funapfpainom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Datalisttype", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Datalistproc", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaoapf_funapfpainom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Sortasc", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Sortdsc", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Loadingdata", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Noresultsfound", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Caption", StringUtil.RTrim( Ddo_funcaoapf_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Tooltip", StringUtil.RTrim( Ddo_funcaoapf_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Cls", StringUtil.RTrim( Ddo_funcaoapf_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaoapf_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapf_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapf_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapf_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapf_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_funcaoapf_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapf_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapf_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_funcaoapf_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Allowmultipleselection", StringUtil.BoolToStr( Ddo_funcaoapf_ativo_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_funcaoapf_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Sortasc", StringUtil.RTrim( Ddo_funcaoapf_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_funcaoapf_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapf_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapf_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapf_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapf_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaoapf_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapf_tipo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaoapf_tipo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapf_td_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapf_td_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Filteredtextto_get", StringUtil.RTrim( Ddo_funcaoapf_td_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapf_ar_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapf_ar_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Filteredtextto_get", StringUtil.RTrim( Ddo_funcaoapf_ar_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapf_pf_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapf_pf_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Filteredtextto_get", StringUtil.RTrim( Ddo_funcaoapf_pf_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapf_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaoapf_ativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE3Z2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT3Z2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwfuncaoapf.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWFuncaoAPF" ;
      }

      public override String GetPgmdesc( )
      {
         return " Fun��es APF - An�lise de Ponto de Fun��o" ;
      }

      protected void WB3Z0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_3Z2( true) ;
         }
         else
         {
            wb_table1_2_3Z2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_3Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_96_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(111, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,111);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_96_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(112, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,112);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_96_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled4_Internalname, StringUtil.BoolToStr( AV83DynamicFiltersEnabled4), "", "", chkavDynamicfiltersenabled4.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(113, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,113);\"");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_96_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTffuncaoapf_nome_Internalname, AV92TFFuncaoAPF_Nome, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,114);\"", 0, edtavTffuncaoapf_nome_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWFuncaoAPF.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_96_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTffuncaoapf_nome_sel_Internalname, AV93TFFuncaoAPF_Nome_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,115);\"", 0, edtavTffuncaoapf_nome_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWFuncaoAPF.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_96_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapf_td_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV100TFFuncaoAPF_TD), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV100TFFuncaoAPF_TD), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapf_td_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapf_td_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWFuncaoAPF.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_96_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapf_td_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV101TFFuncaoAPF_TD_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV101TFFuncaoAPF_TD_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,117);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapf_td_to_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapf_td_to_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWFuncaoAPF.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_96_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapf_ar_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV104TFFuncaoAPF_AR), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV104TFFuncaoAPF_AR), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,118);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapf_ar_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapf_ar_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWFuncaoAPF.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_96_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapf_ar_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV105TFFuncaoAPF_AR_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV105TFFuncaoAPF_AR_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,119);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapf_ar_to_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapf_ar_to_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWFuncaoAPF.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'" + sGXsfl_96_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapf_pf_Internalname, StringUtil.LTrim( StringUtil.NToC( AV112TFFuncaoAPF_PF, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV112TFFuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,120);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapf_pf_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapf_pf_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWFuncaoAPF.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_96_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapf_pf_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV113TFFuncaoAPF_PF_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV113TFFuncaoAPF_PF_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,121);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapf_pf_to_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapf_pf_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWFuncaoAPF.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'" + sGXsfl_96_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTffuncaoapf_funapfpainom_Internalname, AV116TFFuncaoAPF_FunAPFPaiNom, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,122);\"", 0, edtavTffuncaoapf_funapfpainom_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWFuncaoAPF.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'" + sGXsfl_96_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTffuncaoapf_funapfpainom_sel_Internalname, AV117TFFuncaoAPF_FunAPFPaiNom_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,123);\"", 0, edtavTffuncaoapf_funapfpainom_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWFuncaoAPF.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAOAPF_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'" + sGXsfl_96_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Internalname, AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,125);\"", 0, edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWFuncaoAPF.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAOAPF_TIPOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'" + sGXsfl_96_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapf_tipotitlecontrolidtoreplace_Internalname, AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,127);\"", 0, edtavDdo_funcaoapf_tipotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWFuncaoAPF.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAOAPF_TDContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 129,'',false,'" + sGXsfl_96_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapf_tdtitlecontrolidtoreplace_Internalname, AV102ddo_FuncaoAPF_TDTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,129);\"", 0, edtavDdo_funcaoapf_tdtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWFuncaoAPF.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAOAPF_ARContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'" + sGXsfl_96_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapf_artitlecontrolidtoreplace_Internalname, AV106ddo_FuncaoAPF_ARTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,131);\"", 0, edtavDdo_funcaoapf_artitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWFuncaoAPF.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAOAPF_COMPLEXIDADEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'" + sGXsfl_96_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Internalname, AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,133);\"", 0, edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWFuncaoAPF.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAOAPF_PFContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'" + sGXsfl_96_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Internalname, AV114ddo_FuncaoAPF_PFTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,135);\"", 0, edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWFuncaoAPF.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAOAPF_FUNAPFPAINOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 137,'',false,'" + sGXsfl_96_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapf_funapfpainomtitlecontrolidtoreplace_Internalname, AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,137);\"", 0, edtavDdo_funcaoapf_funapfpainomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWFuncaoAPF.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAOAPF_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 139,'',false,'" + sGXsfl_96_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapf_ativotitlecontrolidtoreplace_Internalname, AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,139);\"", 0, edtavDdo_funcaoapf_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWFuncaoAPF.htm");
         }
         wbLoad = true;
      }

      protected void START3Z2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Fun��es APF - An�lise de Ponto de Fun��o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP3Z0( ) ;
      }

      protected void WS3Z2( )
      {
         START3Z2( ) ;
         EVT3Z2( ) ;
      }

      protected void EVT3Z2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPF_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E113Z2 */
                              E113Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPF_TIPO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E123Z2 */
                              E123Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPF_TD.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E133Z2 */
                              E133Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPF_AR.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E143Z2 */
                              E143Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPF_COMPLEXIDADE.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E153Z2 */
                              E153Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPF_PF.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E163Z2 */
                              E163Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPF_FUNAPFPAINOM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E173Z2 */
                              E173Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPF_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E183Z2 */
                              E183Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E193Z2 */
                              E193Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E203Z2 */
                              E203Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E213Z2 */
                              E213Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E223Z2 */
                              E223Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS4'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E233Z2 */
                              E233Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E243Z2 */
                              E243Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E253Z2 */
                              E253Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E263Z2 */
                              E263Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E273Z2 */
                              E273Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E283Z2 */
                              E283Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E293Z2 */
                              E293Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E303Z2 */
                              E303Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E313Z2 */
                              E313Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR4.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E323Z2 */
                              E323Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              AV126WWFuncaoAPFDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
                              AV127WWFuncaoAPFDS_2_Funcaoapf_nome1 = AV17FuncaoAPF_Nome1;
                              AV128WWFuncaoAPFDS_3_Funcaoapf_tipo1 = AV76FuncaoAPF_Tipo1;
                              AV129WWFuncaoAPFDS_4_Funcaoapf_complexidade1 = AV77FuncaoAPF_Complexidade1;
                              AV130WWFuncaoAPFDS_5_Funcaoapf_ativo1 = AV55FuncaoAPF_Ativo1;
                              AV131WWFuncaoAPFDS_6_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
                              AV132WWFuncaoAPFDS_7_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
                              AV133WWFuncaoAPFDS_8_Funcaoapf_nome2 = AV21FuncaoAPF_Nome2;
                              AV134WWFuncaoAPFDS_9_Funcaoapf_tipo2 = AV78FuncaoAPF_Tipo2;
                              AV135WWFuncaoAPFDS_10_Funcaoapf_complexidade2 = AV79FuncaoAPF_Complexidade2;
                              AV136WWFuncaoAPFDS_11_Funcaoapf_ativo2 = AV56FuncaoAPF_Ativo2;
                              AV137WWFuncaoAPFDS_12_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
                              AV138WWFuncaoAPFDS_13_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
                              AV139WWFuncaoAPFDS_14_Funcaoapf_nome3 = AV25FuncaoAPF_Nome3;
                              AV140WWFuncaoAPFDS_15_Funcaoapf_tipo3 = AV80FuncaoAPF_Tipo3;
                              AV141WWFuncaoAPFDS_16_Funcaoapf_complexidade3 = AV81FuncaoAPF_Complexidade3;
                              AV142WWFuncaoAPFDS_17_Funcaoapf_ativo3 = AV82FuncaoAPF_Ativo3;
                              AV143WWFuncaoAPFDS_18_Dynamicfiltersenabled4 = AV83DynamicFiltersEnabled4;
                              AV144WWFuncaoAPFDS_19_Dynamicfiltersselector4 = AV84DynamicFiltersSelector4;
                              AV145WWFuncaoAPFDS_20_Funcaoapf_nome4 = AV85FuncaoAPF_Nome4;
                              AV146WWFuncaoAPFDS_21_Funcaoapf_tipo4 = AV86FuncaoAPF_Tipo4;
                              AV147WWFuncaoAPFDS_22_Funcaoapf_complexidade4 = AV87FuncaoAPF_Complexidade4;
                              AV148WWFuncaoAPFDS_23_Funcaoapf_ativo4 = AV88FuncaoAPF_Ativo4;
                              AV149WWFuncaoAPFDS_24_Tffuncaoapf_nome = AV92TFFuncaoAPF_Nome;
                              AV150WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel = AV93TFFuncaoAPF_Nome_Sel;
                              AV151WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels = AV97TFFuncaoAPF_Tipo_Sels;
                              AV152WWFuncaoAPFDS_27_Tffuncaoapf_td = AV100TFFuncaoAPF_TD;
                              AV153WWFuncaoAPFDS_28_Tffuncaoapf_td_to = AV101TFFuncaoAPF_TD_To;
                              AV154WWFuncaoAPFDS_29_Tffuncaoapf_ar = AV104TFFuncaoAPF_AR;
                              AV155WWFuncaoAPFDS_30_Tffuncaoapf_ar_to = AV105TFFuncaoAPF_AR_To;
                              AV156WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels = AV109TFFuncaoAPF_Complexidade_Sels;
                              AV157WWFuncaoAPFDS_32_Tffuncaoapf_pf = AV112TFFuncaoAPF_PF;
                              AV158WWFuncaoAPFDS_33_Tffuncaoapf_pf_to = AV113TFFuncaoAPF_PF_To;
                              AV159WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom = AV116TFFuncaoAPF_FunAPFPaiNom;
                              AV160WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel = AV117TFFuncaoAPF_FunAPFPaiNom_Sel;
                              AV161WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels = AV121TFFuncaoAPF_Ativo_Sels;
                              sEvt = cgiGet( "GRIDPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_96_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_96_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_96_idx), 4, 0)), 4, "0");
                              SubsflControlProps_962( ) ;
                              AV28Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV162Update_GXI : context.convertURL( context.PathToRelativeUrl( AV28Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV163Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              AV64Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV64Display)) ? AV164Display_GXI : context.convertURL( context.PathToRelativeUrl( AV64Display))));
                              A165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_Codigo_Internalname), ",", "."));
                              A166FuncaoAPF_Nome = cgiGet( edtFuncaoAPF_Nome_Internalname);
                              cmbFuncaoAPF_Tipo.Name = cmbFuncaoAPF_Tipo_Internalname;
                              cmbFuncaoAPF_Tipo.CurrentValue = cgiGet( cmbFuncaoAPF_Tipo_Internalname);
                              A184FuncaoAPF_Tipo = cgiGet( cmbFuncaoAPF_Tipo_Internalname);
                              A388FuncaoAPF_TD = (short)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_TD_Internalname), ",", "."));
                              A387FuncaoAPF_AR = (short)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_AR_Internalname), ",", "."));
                              cmbFuncaoAPF_Complexidade.Name = cmbFuncaoAPF_Complexidade_Internalname;
                              cmbFuncaoAPF_Complexidade.CurrentValue = cgiGet( cmbFuncaoAPF_Complexidade_Internalname);
                              A185FuncaoAPF_Complexidade = cgiGet( cmbFuncaoAPF_Complexidade_Internalname);
                              A386FuncaoAPF_PF = context.localUtil.CToN( cgiGet( edtFuncaoAPF_PF_Internalname), ",", ".");
                              A358FuncaoAPF_FunAPFPaiCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_FunAPFPaiCod_Internalname), ",", "."));
                              n358FuncaoAPF_FunAPFPaiCod = false;
                              A363FuncaoAPF_FunAPFPaiNom = cgiGet( edtFuncaoAPF_FunAPFPaiNom_Internalname);
                              n363FuncaoAPF_FunAPFPaiNom = false;
                              cmbFuncaoAPF_Ativo.Name = cmbFuncaoAPF_Ativo_Internalname;
                              cmbFuncaoAPF_Ativo.CurrentValue = cgiGet( cmbFuncaoAPF_Ativo_Internalname);
                              A183FuncaoAPF_Ativo = cgiGet( cmbFuncaoAPF_Ativo_Internalname);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E333Z2 */
                                    E333Z2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E343Z2 */
                                    E343Z2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E353Z2 */
                                    E353Z2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Funcaoapf_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_NOME1"), AV17FuncaoAPF_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Funcaoapf_tipo1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_TIPO1"), AV76FuncaoAPF_Tipo1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Funcaoapf_ativo1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_ATIVO1"), AV55FuncaoAPF_Ativo1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Funcaoapf_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_NOME2"), AV21FuncaoAPF_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Funcaoapf_tipo2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_TIPO2"), AV78FuncaoAPF_Tipo2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Funcaoapf_ativo2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_ATIVO2"), AV56FuncaoAPF_Ativo2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Funcaoapf_nome3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_NOME3"), AV25FuncaoAPF_Nome3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Funcaoapf_tipo3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_TIPO3"), AV80FuncaoAPF_Tipo3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Funcaoapf_ativo3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_ATIVO3"), AV82FuncaoAPF_Ativo3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector4 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR4"), AV84DynamicFiltersSelector4) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Funcaoapf_nome4 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_NOME4"), AV85FuncaoAPF_Nome4) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Funcaoapf_tipo4 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_TIPO4"), AV86FuncaoAPF_Tipo4) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Funcaoapf_ativo4 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_ATIVO4"), AV88FuncaoAPF_Ativo4) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled4 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED4")) != AV83DynamicFiltersEnabled4 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tffuncaoapf_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPF_NOME"), AV92TFFuncaoAPF_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tffuncaoapf_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPF_NOME_SEL"), AV93TFFuncaoAPF_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tffuncaoapf_funapfpainom Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPF_FUNAPFPAINOM"), AV116TFFuncaoAPF_FunAPFPaiNom) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tffuncaoapf_funapfpainom_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPF_FUNAPFPAINOM_SEL"), AV117TFFuncaoAPF_FunAPFPaiNom_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE3Z2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA3Z2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("FUNCAOAPF_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("FUNCAOAPF_TIPO", "Tipo", 0);
            cmbavDynamicfiltersselector1.addItem("FUNCAOAPF_COMPLEXIDADE", "Complexidade", 0);
            cmbavDynamicfiltersselector1.addItem("FUNCAOAPF_ATIVO", "da Fun��o", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavFuncaoapf_tipo1.Name = "vFUNCAOAPF_TIPO1";
            cmbavFuncaoapf_tipo1.WebTags = "";
            cmbavFuncaoapf_tipo1.addItem("", "Todos", 0);
            cmbavFuncaoapf_tipo1.addItem("EE", "EE", 0);
            cmbavFuncaoapf_tipo1.addItem("SE", "SE", 0);
            cmbavFuncaoapf_tipo1.addItem("CE", "CE", 0);
            cmbavFuncaoapf_tipo1.addItem("NM", "NM", 0);
            if ( cmbavFuncaoapf_tipo1.ItemCount > 0 )
            {
               AV76FuncaoAPF_Tipo1 = cmbavFuncaoapf_tipo1.getValidValue(AV76FuncaoAPF_Tipo1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76FuncaoAPF_Tipo1", AV76FuncaoAPF_Tipo1);
            }
            cmbavFuncaoapf_complexidade1.Name = "vFUNCAOAPF_COMPLEXIDADE1";
            cmbavFuncaoapf_complexidade1.WebTags = "";
            cmbavFuncaoapf_complexidade1.addItem("", "Todos", 0);
            cmbavFuncaoapf_complexidade1.addItem("E", "", 0);
            cmbavFuncaoapf_complexidade1.addItem("B", "Baixa", 0);
            cmbavFuncaoapf_complexidade1.addItem("M", "M�dia", 0);
            cmbavFuncaoapf_complexidade1.addItem("A", "Alta", 0);
            if ( cmbavFuncaoapf_complexidade1.ItemCount > 0 )
            {
               AV77FuncaoAPF_Complexidade1 = cmbavFuncaoapf_complexidade1.getValidValue(AV77FuncaoAPF_Complexidade1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77FuncaoAPF_Complexidade1", AV77FuncaoAPF_Complexidade1);
            }
            cmbavFuncaoapf_ativo1.Name = "vFUNCAOAPF_ATIVO1";
            cmbavFuncaoapf_ativo1.WebTags = "";
            cmbavFuncaoapf_ativo1.addItem("", "Todos", 0);
            cmbavFuncaoapf_ativo1.addItem("A", "Ativa", 0);
            cmbavFuncaoapf_ativo1.addItem("E", "Exclu�da", 0);
            cmbavFuncaoapf_ativo1.addItem("R", "Rejeitada", 0);
            if ( cmbavFuncaoapf_ativo1.ItemCount > 0 )
            {
               AV55FuncaoAPF_Ativo1 = cmbavFuncaoapf_ativo1.getValidValue(AV55FuncaoAPF_Ativo1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55FuncaoAPF_Ativo1", AV55FuncaoAPF_Ativo1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("FUNCAOAPF_NOME", "Nome", 0);
            cmbavDynamicfiltersselector2.addItem("FUNCAOAPF_TIPO", "Tipo", 0);
            cmbavDynamicfiltersselector2.addItem("FUNCAOAPF_COMPLEXIDADE", "Complexidade", 0);
            cmbavDynamicfiltersselector2.addItem("FUNCAOAPF_ATIVO", "da Fun��o", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavFuncaoapf_tipo2.Name = "vFUNCAOAPF_TIPO2";
            cmbavFuncaoapf_tipo2.WebTags = "";
            cmbavFuncaoapf_tipo2.addItem("", "Todos", 0);
            cmbavFuncaoapf_tipo2.addItem("EE", "EE", 0);
            cmbavFuncaoapf_tipo2.addItem("SE", "SE", 0);
            cmbavFuncaoapf_tipo2.addItem("CE", "CE", 0);
            cmbavFuncaoapf_tipo2.addItem("NM", "NM", 0);
            if ( cmbavFuncaoapf_tipo2.ItemCount > 0 )
            {
               AV78FuncaoAPF_Tipo2 = cmbavFuncaoapf_tipo2.getValidValue(AV78FuncaoAPF_Tipo2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78FuncaoAPF_Tipo2", AV78FuncaoAPF_Tipo2);
            }
            cmbavFuncaoapf_complexidade2.Name = "vFUNCAOAPF_COMPLEXIDADE2";
            cmbavFuncaoapf_complexidade2.WebTags = "";
            cmbavFuncaoapf_complexidade2.addItem("", "Todos", 0);
            cmbavFuncaoapf_complexidade2.addItem("E", "", 0);
            cmbavFuncaoapf_complexidade2.addItem("B", "Baixa", 0);
            cmbavFuncaoapf_complexidade2.addItem("M", "M�dia", 0);
            cmbavFuncaoapf_complexidade2.addItem("A", "Alta", 0);
            if ( cmbavFuncaoapf_complexidade2.ItemCount > 0 )
            {
               AV79FuncaoAPF_Complexidade2 = cmbavFuncaoapf_complexidade2.getValidValue(AV79FuncaoAPF_Complexidade2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79FuncaoAPF_Complexidade2", AV79FuncaoAPF_Complexidade2);
            }
            cmbavFuncaoapf_ativo2.Name = "vFUNCAOAPF_ATIVO2";
            cmbavFuncaoapf_ativo2.WebTags = "";
            cmbavFuncaoapf_ativo2.addItem("", "Todos", 0);
            cmbavFuncaoapf_ativo2.addItem("A", "Ativa", 0);
            cmbavFuncaoapf_ativo2.addItem("E", "Exclu�da", 0);
            cmbavFuncaoapf_ativo2.addItem("R", "Rejeitada", 0);
            if ( cmbavFuncaoapf_ativo2.ItemCount > 0 )
            {
               AV56FuncaoAPF_Ativo2 = cmbavFuncaoapf_ativo2.getValidValue(AV56FuncaoAPF_Ativo2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56FuncaoAPF_Ativo2", AV56FuncaoAPF_Ativo2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("FUNCAOAPF_NOME", "Nome", 0);
            cmbavDynamicfiltersselector3.addItem("FUNCAOAPF_TIPO", "Tipo", 0);
            cmbavDynamicfiltersselector3.addItem("FUNCAOAPF_COMPLEXIDADE", "Complexidade", 0);
            cmbavDynamicfiltersselector3.addItem("FUNCAOAPF_ATIVO", "da Fun��o", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavFuncaoapf_tipo3.Name = "vFUNCAOAPF_TIPO3";
            cmbavFuncaoapf_tipo3.WebTags = "";
            cmbavFuncaoapf_tipo3.addItem("", "Todos", 0);
            cmbavFuncaoapf_tipo3.addItem("EE", "EE", 0);
            cmbavFuncaoapf_tipo3.addItem("SE", "SE", 0);
            cmbavFuncaoapf_tipo3.addItem("CE", "CE", 0);
            cmbavFuncaoapf_tipo3.addItem("NM", "NM", 0);
            if ( cmbavFuncaoapf_tipo3.ItemCount > 0 )
            {
               AV80FuncaoAPF_Tipo3 = cmbavFuncaoapf_tipo3.getValidValue(AV80FuncaoAPF_Tipo3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80FuncaoAPF_Tipo3", AV80FuncaoAPF_Tipo3);
            }
            cmbavFuncaoapf_complexidade3.Name = "vFUNCAOAPF_COMPLEXIDADE3";
            cmbavFuncaoapf_complexidade3.WebTags = "";
            cmbavFuncaoapf_complexidade3.addItem("", "Todos", 0);
            cmbavFuncaoapf_complexidade3.addItem("E", "", 0);
            cmbavFuncaoapf_complexidade3.addItem("B", "Baixa", 0);
            cmbavFuncaoapf_complexidade3.addItem("M", "M�dia", 0);
            cmbavFuncaoapf_complexidade3.addItem("A", "Alta", 0);
            if ( cmbavFuncaoapf_complexidade3.ItemCount > 0 )
            {
               AV81FuncaoAPF_Complexidade3 = cmbavFuncaoapf_complexidade3.getValidValue(AV81FuncaoAPF_Complexidade3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81FuncaoAPF_Complexidade3", AV81FuncaoAPF_Complexidade3);
            }
            cmbavFuncaoapf_ativo3.Name = "vFUNCAOAPF_ATIVO3";
            cmbavFuncaoapf_ativo3.WebTags = "";
            cmbavFuncaoapf_ativo3.addItem("", "Todos", 0);
            cmbavFuncaoapf_ativo3.addItem("A", "Ativa", 0);
            cmbavFuncaoapf_ativo3.addItem("E", "Exclu�da", 0);
            cmbavFuncaoapf_ativo3.addItem("R", "Rejeitada", 0);
            if ( cmbavFuncaoapf_ativo3.ItemCount > 0 )
            {
               AV82FuncaoAPF_Ativo3 = cmbavFuncaoapf_ativo3.getValidValue(AV82FuncaoAPF_Ativo3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82FuncaoAPF_Ativo3", AV82FuncaoAPF_Ativo3);
            }
            cmbavDynamicfiltersselector4.Name = "vDYNAMICFILTERSSELECTOR4";
            cmbavDynamicfiltersselector4.WebTags = "";
            cmbavDynamicfiltersselector4.addItem("FUNCAOAPF_NOME", "Nome", 0);
            cmbavDynamicfiltersselector4.addItem("FUNCAOAPF_TIPO", "Tipo", 0);
            cmbavDynamicfiltersselector4.addItem("FUNCAOAPF_COMPLEXIDADE", "Complexidade", 0);
            cmbavDynamicfiltersselector4.addItem("FUNCAOAPF_ATIVO", "da Fun��o", 0);
            if ( cmbavDynamicfiltersselector4.ItemCount > 0 )
            {
               AV84DynamicFiltersSelector4 = cmbavDynamicfiltersselector4.getValidValue(AV84DynamicFiltersSelector4);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84DynamicFiltersSelector4", AV84DynamicFiltersSelector4);
            }
            cmbavFuncaoapf_tipo4.Name = "vFUNCAOAPF_TIPO4";
            cmbavFuncaoapf_tipo4.WebTags = "";
            cmbavFuncaoapf_tipo4.addItem("", "Todos", 0);
            cmbavFuncaoapf_tipo4.addItem("EE", "EE", 0);
            cmbavFuncaoapf_tipo4.addItem("SE", "SE", 0);
            cmbavFuncaoapf_tipo4.addItem("CE", "CE", 0);
            cmbavFuncaoapf_tipo4.addItem("NM", "NM", 0);
            if ( cmbavFuncaoapf_tipo4.ItemCount > 0 )
            {
               AV86FuncaoAPF_Tipo4 = cmbavFuncaoapf_tipo4.getValidValue(AV86FuncaoAPF_Tipo4);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86FuncaoAPF_Tipo4", AV86FuncaoAPF_Tipo4);
            }
            cmbavFuncaoapf_complexidade4.Name = "vFUNCAOAPF_COMPLEXIDADE4";
            cmbavFuncaoapf_complexidade4.WebTags = "";
            cmbavFuncaoapf_complexidade4.addItem("", "Todos", 0);
            cmbavFuncaoapf_complexidade4.addItem("E", "", 0);
            cmbavFuncaoapf_complexidade4.addItem("B", "Baixa", 0);
            cmbavFuncaoapf_complexidade4.addItem("M", "M�dia", 0);
            cmbavFuncaoapf_complexidade4.addItem("A", "Alta", 0);
            if ( cmbavFuncaoapf_complexidade4.ItemCount > 0 )
            {
               AV87FuncaoAPF_Complexidade4 = cmbavFuncaoapf_complexidade4.getValidValue(AV87FuncaoAPF_Complexidade4);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87FuncaoAPF_Complexidade4", AV87FuncaoAPF_Complexidade4);
            }
            cmbavFuncaoapf_ativo4.Name = "vFUNCAOAPF_ATIVO4";
            cmbavFuncaoapf_ativo4.WebTags = "";
            cmbavFuncaoapf_ativo4.addItem("", "Todos", 0);
            cmbavFuncaoapf_ativo4.addItem("A", "Ativa", 0);
            cmbavFuncaoapf_ativo4.addItem("E", "Exclu�da", 0);
            cmbavFuncaoapf_ativo4.addItem("R", "Rejeitada", 0);
            if ( cmbavFuncaoapf_ativo4.ItemCount > 0 )
            {
               AV88FuncaoAPF_Ativo4 = cmbavFuncaoapf_ativo4.getValidValue(AV88FuncaoAPF_Ativo4);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88FuncaoAPF_Ativo4", AV88FuncaoAPF_Ativo4);
            }
            GXCCtl = "FUNCAOAPF_TIPO_" + sGXsfl_96_idx;
            cmbFuncaoAPF_Tipo.Name = GXCCtl;
            cmbFuncaoAPF_Tipo.WebTags = "";
            cmbFuncaoAPF_Tipo.addItem("", "(Nenhum)", 0);
            cmbFuncaoAPF_Tipo.addItem("EE", "EE", 0);
            cmbFuncaoAPF_Tipo.addItem("SE", "SE", 0);
            cmbFuncaoAPF_Tipo.addItem("CE", "CE", 0);
            cmbFuncaoAPF_Tipo.addItem("NM", "NM", 0);
            if ( cmbFuncaoAPF_Tipo.ItemCount > 0 )
            {
               A184FuncaoAPF_Tipo = cmbFuncaoAPF_Tipo.getValidValue(A184FuncaoAPF_Tipo);
            }
            GXCCtl = "FUNCAOAPF_COMPLEXIDADE_" + sGXsfl_96_idx;
            cmbFuncaoAPF_Complexidade.Name = GXCCtl;
            cmbFuncaoAPF_Complexidade.WebTags = "";
            cmbFuncaoAPF_Complexidade.addItem("E", "", 0);
            cmbFuncaoAPF_Complexidade.addItem("B", "Baixa", 0);
            cmbFuncaoAPF_Complexidade.addItem("M", "M�dia", 0);
            cmbFuncaoAPF_Complexidade.addItem("A", "Alta", 0);
            if ( cmbFuncaoAPF_Complexidade.ItemCount > 0 )
            {
               A185FuncaoAPF_Complexidade = cmbFuncaoAPF_Complexidade.getValidValue(A185FuncaoAPF_Complexidade);
            }
            GXCCtl = "FUNCAOAPF_ATIVO_" + sGXsfl_96_idx;
            cmbFuncaoAPF_Ativo.Name = GXCCtl;
            cmbFuncaoAPF_Ativo.WebTags = "";
            cmbFuncaoAPF_Ativo.addItem("A", "Ativa", 0);
            cmbFuncaoAPF_Ativo.addItem("E", "Exclu�da", 0);
            cmbFuncaoAPF_Ativo.addItem("R", "Rejeitada", 0);
            if ( cmbFuncaoAPF_Ativo.ItemCount > 0 )
            {
               A183FuncaoAPF_Ativo = cmbFuncaoAPF_Ativo.getValidValue(A183FuncaoAPF_Ativo);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            chkavDynamicfiltersenabled4.Name = "vDYNAMICFILTERSENABLED4";
            chkavDynamicfiltersenabled4.WebTags = "";
            chkavDynamicfiltersenabled4.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled4_Internalname, "TitleCaption", chkavDynamicfiltersenabled4.Caption);
            chkavDynamicfiltersenabled4.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_962( ) ;
         while ( nGXsfl_96_idx <= nRC_GXsfl_96 )
         {
            sendrow_962( ) ;
            nGXsfl_96_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_96_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_96_idx+1));
            sGXsfl_96_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_96_idx), 4, 0)), 4, "0");
            SubsflControlProps_962( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV17FuncaoAPF_Nome1 ,
                                       String AV76FuncaoAPF_Tipo1 ,
                                       String AV55FuncaoAPF_Ativo1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       String AV21FuncaoAPF_Nome2 ,
                                       String AV78FuncaoAPF_Tipo2 ,
                                       String AV56FuncaoAPF_Ativo2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       String AV25FuncaoAPF_Nome3 ,
                                       String AV80FuncaoAPF_Tipo3 ,
                                       String AV82FuncaoAPF_Ativo3 ,
                                       String AV84DynamicFiltersSelector4 ,
                                       String AV85FuncaoAPF_Nome4 ,
                                       String AV86FuncaoAPF_Tipo4 ,
                                       String AV88FuncaoAPF_Ativo4 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       bool AV83DynamicFiltersEnabled4 ,
                                       String AV92TFFuncaoAPF_Nome ,
                                       String AV93TFFuncaoAPF_Nome_Sel ,
                                       String AV116TFFuncaoAPF_FunAPFPaiNom ,
                                       String AV117TFFuncaoAPF_FunAPFPaiNom_Sel ,
                                       String AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace ,
                                       String AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace ,
                                       String AV102ddo_FuncaoAPF_TDTitleControlIdToReplace ,
                                       String AV106ddo_FuncaoAPF_ARTitleControlIdToReplace ,
                                       String AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace ,
                                       String AV114ddo_FuncaoAPF_PFTitleControlIdToReplace ,
                                       String AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace ,
                                       String AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace ,
                                       String AV77FuncaoAPF_Complexidade1 ,
                                       String AV79FuncaoAPF_Complexidade2 ,
                                       String AV81FuncaoAPF_Complexidade3 ,
                                       String AV87FuncaoAPF_Complexidade4 ,
                                       IGxCollection AV97TFFuncaoAPF_Tipo_Sels ,
                                       short AV100TFFuncaoAPF_TD ,
                                       short AV101TFFuncaoAPF_TD_To ,
                                       short AV104TFFuncaoAPF_AR ,
                                       short AV105TFFuncaoAPF_AR_To ,
                                       IGxCollection AV109TFFuncaoAPF_Complexidade_Sels ,
                                       decimal AV112TFFuncaoAPF_PF ,
                                       decimal AV113TFFuncaoAPF_PF_To ,
                                       IGxCollection AV121TFFuncaoAPF_Ativo_Sels ,
                                       String AV165Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       int A165FuncaoAPF_Codigo ,
                                       int AV57FuncaoAPF_SistemaCod ,
                                       int A358FuncaoAPF_FunAPFPaiCod )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF3Z2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A166FuncaoAPF_Nome, ""))));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_NOME", A166FuncaoAPF_Nome);
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_TIPO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A184FuncaoAPF_Tipo, ""))));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_TIPO", StringUtil.RTrim( A184FuncaoAPF_Tipo));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_TD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A388FuncaoAPF_TD), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_TD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A388FuncaoAPF_TD), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_AR", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A387FuncaoAPF_AR), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_AR", StringUtil.LTrim( StringUtil.NToC( (decimal)(A387FuncaoAPF_AR), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_COMPLEXIDADE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A185FuncaoAPF_Complexidade, ""))));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_COMPLEXIDADE", StringUtil.RTrim( A185FuncaoAPF_Complexidade));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_PF", GetSecureSignedToken( "", context.localUtil.Format( A386FuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_PF", StringUtil.LTrim( StringUtil.NToC( A386FuncaoAPF_PF, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_FUNAPFPAICOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A358FuncaoAPF_FunAPFPaiCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_FUNAPFPAICOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A358FuncaoAPF_FunAPFPaiCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_ATIVO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A183FuncaoAPF_Ativo, ""))));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_ATIVO", StringUtil.RTrim( A183FuncaoAPF_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavFuncaoapf_tipo1.ItemCount > 0 )
         {
            AV76FuncaoAPF_Tipo1 = cmbavFuncaoapf_tipo1.getValidValue(AV76FuncaoAPF_Tipo1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76FuncaoAPF_Tipo1", AV76FuncaoAPF_Tipo1);
         }
         if ( cmbavFuncaoapf_complexidade1.ItemCount > 0 )
         {
            AV77FuncaoAPF_Complexidade1 = cmbavFuncaoapf_complexidade1.getValidValue(AV77FuncaoAPF_Complexidade1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77FuncaoAPF_Complexidade1", AV77FuncaoAPF_Complexidade1);
         }
         if ( cmbavFuncaoapf_ativo1.ItemCount > 0 )
         {
            AV55FuncaoAPF_Ativo1 = cmbavFuncaoapf_ativo1.getValidValue(AV55FuncaoAPF_Ativo1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55FuncaoAPF_Ativo1", AV55FuncaoAPF_Ativo1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavFuncaoapf_tipo2.ItemCount > 0 )
         {
            AV78FuncaoAPF_Tipo2 = cmbavFuncaoapf_tipo2.getValidValue(AV78FuncaoAPF_Tipo2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78FuncaoAPF_Tipo2", AV78FuncaoAPF_Tipo2);
         }
         if ( cmbavFuncaoapf_complexidade2.ItemCount > 0 )
         {
            AV79FuncaoAPF_Complexidade2 = cmbavFuncaoapf_complexidade2.getValidValue(AV79FuncaoAPF_Complexidade2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79FuncaoAPF_Complexidade2", AV79FuncaoAPF_Complexidade2);
         }
         if ( cmbavFuncaoapf_ativo2.ItemCount > 0 )
         {
            AV56FuncaoAPF_Ativo2 = cmbavFuncaoapf_ativo2.getValidValue(AV56FuncaoAPF_Ativo2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56FuncaoAPF_Ativo2", AV56FuncaoAPF_Ativo2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavFuncaoapf_tipo3.ItemCount > 0 )
         {
            AV80FuncaoAPF_Tipo3 = cmbavFuncaoapf_tipo3.getValidValue(AV80FuncaoAPF_Tipo3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80FuncaoAPF_Tipo3", AV80FuncaoAPF_Tipo3);
         }
         if ( cmbavFuncaoapf_complexidade3.ItemCount > 0 )
         {
            AV81FuncaoAPF_Complexidade3 = cmbavFuncaoapf_complexidade3.getValidValue(AV81FuncaoAPF_Complexidade3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81FuncaoAPF_Complexidade3", AV81FuncaoAPF_Complexidade3);
         }
         if ( cmbavFuncaoapf_ativo3.ItemCount > 0 )
         {
            AV82FuncaoAPF_Ativo3 = cmbavFuncaoapf_ativo3.getValidValue(AV82FuncaoAPF_Ativo3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82FuncaoAPF_Ativo3", AV82FuncaoAPF_Ativo3);
         }
         if ( cmbavDynamicfiltersselector4.ItemCount > 0 )
         {
            AV84DynamicFiltersSelector4 = cmbavDynamicfiltersselector4.getValidValue(AV84DynamicFiltersSelector4);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84DynamicFiltersSelector4", AV84DynamicFiltersSelector4);
         }
         if ( cmbavFuncaoapf_tipo4.ItemCount > 0 )
         {
            AV86FuncaoAPF_Tipo4 = cmbavFuncaoapf_tipo4.getValidValue(AV86FuncaoAPF_Tipo4);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86FuncaoAPF_Tipo4", AV86FuncaoAPF_Tipo4);
         }
         if ( cmbavFuncaoapf_complexidade4.ItemCount > 0 )
         {
            AV87FuncaoAPF_Complexidade4 = cmbavFuncaoapf_complexidade4.getValidValue(AV87FuncaoAPF_Complexidade4);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87FuncaoAPF_Complexidade4", AV87FuncaoAPF_Complexidade4);
         }
         if ( cmbavFuncaoapf_ativo4.ItemCount > 0 )
         {
            AV88FuncaoAPF_Ativo4 = cmbavFuncaoapf_ativo4.getValidValue(AV88FuncaoAPF_Ativo4);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88FuncaoAPF_Ativo4", AV88FuncaoAPF_Ativo4);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF3Z2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV165Pgmname = "WWFuncaoAPF";
         context.Gx_err = 0;
      }

      protected void RF3Z2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 96;
         /* Execute user event: E343Z2 */
         E343Z2 ();
         nGXsfl_96_idx = 1;
         sGXsfl_96_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_96_idx), 4, 0)), 4, "0");
         SubsflControlProps_962( ) ;
         nGXsfl_96_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_962( ) ;
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A185FuncaoAPF_Complexidade ,
                                                 AV156WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels ,
                                                 A184FuncaoAPF_Tipo ,
                                                 AV151WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels ,
                                                 A183FuncaoAPF_Ativo ,
                                                 AV161WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels ,
                                                 AV126WWFuncaoAPFDS_1_Dynamicfiltersselector1 ,
                                                 AV127WWFuncaoAPFDS_2_Funcaoapf_nome1 ,
                                                 AV128WWFuncaoAPFDS_3_Funcaoapf_tipo1 ,
                                                 AV130WWFuncaoAPFDS_5_Funcaoapf_ativo1 ,
                                                 AV131WWFuncaoAPFDS_6_Dynamicfiltersenabled2 ,
                                                 AV132WWFuncaoAPFDS_7_Dynamicfiltersselector2 ,
                                                 AV133WWFuncaoAPFDS_8_Funcaoapf_nome2 ,
                                                 AV134WWFuncaoAPFDS_9_Funcaoapf_tipo2 ,
                                                 AV136WWFuncaoAPFDS_11_Funcaoapf_ativo2 ,
                                                 AV137WWFuncaoAPFDS_12_Dynamicfiltersenabled3 ,
                                                 AV138WWFuncaoAPFDS_13_Dynamicfiltersselector3 ,
                                                 AV139WWFuncaoAPFDS_14_Funcaoapf_nome3 ,
                                                 AV140WWFuncaoAPFDS_15_Funcaoapf_tipo3 ,
                                                 AV142WWFuncaoAPFDS_17_Funcaoapf_ativo3 ,
                                                 AV143WWFuncaoAPFDS_18_Dynamicfiltersenabled4 ,
                                                 AV144WWFuncaoAPFDS_19_Dynamicfiltersselector4 ,
                                                 AV145WWFuncaoAPFDS_20_Funcaoapf_nome4 ,
                                                 AV146WWFuncaoAPFDS_21_Funcaoapf_tipo4 ,
                                                 AV148WWFuncaoAPFDS_23_Funcaoapf_ativo4 ,
                                                 AV150WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel ,
                                                 AV149WWFuncaoAPFDS_24_Tffuncaoapf_nome ,
                                                 AV151WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels.Count ,
                                                 AV160WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel ,
                                                 AV159WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom ,
                                                 AV161WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels.Count ,
                                                 A166FuncaoAPF_Nome ,
                                                 A363FuncaoAPF_FunAPFPaiNom ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 AV129WWFuncaoAPFDS_4_Funcaoapf_complexidade1 ,
                                                 AV135WWFuncaoAPFDS_10_Funcaoapf_complexidade2 ,
                                                 AV141WWFuncaoAPFDS_16_Funcaoapf_complexidade3 ,
                                                 AV147WWFuncaoAPFDS_22_Funcaoapf_complexidade4 ,
                                                 AV152WWFuncaoAPFDS_27_Tffuncaoapf_td ,
                                                 A388FuncaoAPF_TD ,
                                                 AV153WWFuncaoAPFDS_28_Tffuncaoapf_td_to ,
                                                 AV154WWFuncaoAPFDS_29_Tffuncaoapf_ar ,
                                                 A387FuncaoAPF_AR ,
                                                 AV155WWFuncaoAPFDS_30_Tffuncaoapf_ar_to ,
                                                 AV156WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels.Count ,
                                                 AV157WWFuncaoAPFDS_32_Tffuncaoapf_pf ,
                                                 A386FuncaoAPF_PF ,
                                                 AV158WWFuncaoAPFDS_33_Tffuncaoapf_pf_to },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT,
                                                 TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL
                                                 }
            });
            lV127WWFuncaoAPFDS_2_Funcaoapf_nome1 = StringUtil.Concat( StringUtil.RTrim( AV127WWFuncaoAPFDS_2_Funcaoapf_nome1), "%", "");
            lV133WWFuncaoAPFDS_8_Funcaoapf_nome2 = StringUtil.Concat( StringUtil.RTrim( AV133WWFuncaoAPFDS_8_Funcaoapf_nome2), "%", "");
            lV139WWFuncaoAPFDS_14_Funcaoapf_nome3 = StringUtil.Concat( StringUtil.RTrim( AV139WWFuncaoAPFDS_14_Funcaoapf_nome3), "%", "");
            lV145WWFuncaoAPFDS_20_Funcaoapf_nome4 = StringUtil.Concat( StringUtil.RTrim( AV145WWFuncaoAPFDS_20_Funcaoapf_nome4), "%", "");
            lV149WWFuncaoAPFDS_24_Tffuncaoapf_nome = StringUtil.Concat( StringUtil.RTrim( AV149WWFuncaoAPFDS_24_Tffuncaoapf_nome), "%", "");
            lV159WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom = StringUtil.Concat( StringUtil.RTrim( AV159WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom), "%", "");
            /* Using cursor H003Z2 */
            pr_default.execute(0, new Object[] {AV156WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels.Count, lV127WWFuncaoAPFDS_2_Funcaoapf_nome1, AV128WWFuncaoAPFDS_3_Funcaoapf_tipo1, AV130WWFuncaoAPFDS_5_Funcaoapf_ativo1, lV133WWFuncaoAPFDS_8_Funcaoapf_nome2, AV134WWFuncaoAPFDS_9_Funcaoapf_tipo2, AV136WWFuncaoAPFDS_11_Funcaoapf_ativo2, lV139WWFuncaoAPFDS_14_Funcaoapf_nome3, AV140WWFuncaoAPFDS_15_Funcaoapf_tipo3, AV142WWFuncaoAPFDS_17_Funcaoapf_ativo3, lV145WWFuncaoAPFDS_20_Funcaoapf_nome4, AV146WWFuncaoAPFDS_21_Funcaoapf_tipo4, AV148WWFuncaoAPFDS_23_Funcaoapf_ativo4, lV149WWFuncaoAPFDS_24_Tffuncaoapf_nome, AV150WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel, lV159WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom, AV160WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel});
            nGXsfl_96_idx = 1;
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) ) )
            {
               A183FuncaoAPF_Ativo = H003Z2_A183FuncaoAPF_Ativo[0];
               A363FuncaoAPF_FunAPFPaiNom = H003Z2_A363FuncaoAPF_FunAPFPaiNom[0];
               n363FuncaoAPF_FunAPFPaiNom = H003Z2_n363FuncaoAPF_FunAPFPaiNom[0];
               A358FuncaoAPF_FunAPFPaiCod = H003Z2_A358FuncaoAPF_FunAPFPaiCod[0];
               n358FuncaoAPF_FunAPFPaiCod = H003Z2_n358FuncaoAPF_FunAPFPaiCod[0];
               A184FuncaoAPF_Tipo = H003Z2_A184FuncaoAPF_Tipo[0];
               A166FuncaoAPF_Nome = H003Z2_A166FuncaoAPF_Nome[0];
               A165FuncaoAPF_Codigo = H003Z2_A165FuncaoAPF_Codigo[0];
               A363FuncaoAPF_FunAPFPaiNom = H003Z2_A363FuncaoAPF_FunAPFPaiNom[0];
               n363FuncaoAPF_FunAPFPaiNom = H003Z2_n363FuncaoAPF_FunAPFPaiNom[0];
               GXt_int1 = A388FuncaoAPF_TD;
               new prc_funcaoapf_td(context ).execute(  A165FuncaoAPF_Codigo, out  GXt_int1) ;
               A388FuncaoAPF_TD = GXt_int1;
               if ( (0==AV152WWFuncaoAPFDS_27_Tffuncaoapf_td) || ( ( A388FuncaoAPF_TD >= AV152WWFuncaoAPFDS_27_Tffuncaoapf_td ) ) )
               {
                  if ( (0==AV153WWFuncaoAPFDS_28_Tffuncaoapf_td_to) || ( ( A388FuncaoAPF_TD <= AV153WWFuncaoAPFDS_28_Tffuncaoapf_td_to ) ) )
                  {
                     GXt_int1 = A387FuncaoAPF_AR;
                     new prc_funcaoapf_ar(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_int1) ;
                     A387FuncaoAPF_AR = GXt_int1;
                     if ( (0==AV154WWFuncaoAPFDS_29_Tffuncaoapf_ar) || ( ( A387FuncaoAPF_AR >= AV154WWFuncaoAPFDS_29_Tffuncaoapf_ar ) ) )
                     {
                        if ( (0==AV155WWFuncaoAPFDS_30_Tffuncaoapf_ar_to) || ( ( A387FuncaoAPF_AR <= AV155WWFuncaoAPFDS_30_Tffuncaoapf_ar_to ) ) )
                        {
                           GXt_char2 = A185FuncaoAPF_Complexidade;
                           new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char2) ;
                           A185FuncaoAPF_Complexidade = GXt_char2;
                           if ( ! ( ( StringUtil.StrCmp(AV126WWFuncaoAPFDS_1_Dynamicfiltersselector1, "FUNCAOAPF_COMPLEXIDADE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV129WWFuncaoAPFDS_4_Funcaoapf_complexidade1)) ) ) || ( ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, AV129WWFuncaoAPFDS_4_Funcaoapf_complexidade1) == 0 ) ) )
                           {
                              if ( ! ( AV131WWFuncaoAPFDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132WWFuncaoAPFDS_7_Dynamicfiltersselector2, "FUNCAOAPF_COMPLEXIDADE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV135WWFuncaoAPFDS_10_Funcaoapf_complexidade2)) ) ) || ( ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, AV135WWFuncaoAPFDS_10_Funcaoapf_complexidade2) == 0 ) ) )
                              {
                                 if ( ! ( AV137WWFuncaoAPFDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV138WWFuncaoAPFDS_13_Dynamicfiltersselector3, "FUNCAOAPF_COMPLEXIDADE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV141WWFuncaoAPFDS_16_Funcaoapf_complexidade3)) ) ) || ( ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, AV141WWFuncaoAPFDS_16_Funcaoapf_complexidade3) == 0 ) ) )
                                 {
                                    if ( ! ( AV143WWFuncaoAPFDS_18_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV144WWFuncaoAPFDS_19_Dynamicfiltersselector4, "FUNCAOAPF_COMPLEXIDADE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV147WWFuncaoAPFDS_22_Funcaoapf_complexidade4)) ) ) || ( ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, AV147WWFuncaoAPFDS_22_Funcaoapf_complexidade4) == 0 ) ) )
                                    {
                                       if ( ( AV156WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels.Count <= 0 ) || ( (AV156WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels.IndexOf(StringUtil.RTrim( A185FuncaoAPF_Complexidade))>0) ) )
                                       {
                                          GXt_decimal3 = A386FuncaoAPF_PF;
                                          new prc_fapfpf(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_decimal3) ;
                                          A386FuncaoAPF_PF = GXt_decimal3;
                                          if ( (Convert.ToDecimal(0)==AV157WWFuncaoAPFDS_32_Tffuncaoapf_pf) || ( ( A386FuncaoAPF_PF >= AV157WWFuncaoAPFDS_32_Tffuncaoapf_pf ) ) )
                                          {
                                             if ( (Convert.ToDecimal(0)==AV158WWFuncaoAPFDS_33_Tffuncaoapf_pf_to) || ( ( A386FuncaoAPF_PF <= AV158WWFuncaoAPFDS_33_Tffuncaoapf_pf_to ) ) )
                                             {
                                                /* Execute user event: E353Z2 */
                                                E353Z2 ();
                                             }
                                          }
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 96;
            WB3Z0( ) ;
         }
         nGXsfl_96_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV126WWFuncaoAPFDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV127WWFuncaoAPFDS_2_Funcaoapf_nome1 = AV17FuncaoAPF_Nome1;
         AV128WWFuncaoAPFDS_3_Funcaoapf_tipo1 = AV76FuncaoAPF_Tipo1;
         AV129WWFuncaoAPFDS_4_Funcaoapf_complexidade1 = AV77FuncaoAPF_Complexidade1;
         AV130WWFuncaoAPFDS_5_Funcaoapf_ativo1 = AV55FuncaoAPF_Ativo1;
         AV131WWFuncaoAPFDS_6_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV132WWFuncaoAPFDS_7_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV133WWFuncaoAPFDS_8_Funcaoapf_nome2 = AV21FuncaoAPF_Nome2;
         AV134WWFuncaoAPFDS_9_Funcaoapf_tipo2 = AV78FuncaoAPF_Tipo2;
         AV135WWFuncaoAPFDS_10_Funcaoapf_complexidade2 = AV79FuncaoAPF_Complexidade2;
         AV136WWFuncaoAPFDS_11_Funcaoapf_ativo2 = AV56FuncaoAPF_Ativo2;
         AV137WWFuncaoAPFDS_12_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV138WWFuncaoAPFDS_13_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV139WWFuncaoAPFDS_14_Funcaoapf_nome3 = AV25FuncaoAPF_Nome3;
         AV140WWFuncaoAPFDS_15_Funcaoapf_tipo3 = AV80FuncaoAPF_Tipo3;
         AV141WWFuncaoAPFDS_16_Funcaoapf_complexidade3 = AV81FuncaoAPF_Complexidade3;
         AV142WWFuncaoAPFDS_17_Funcaoapf_ativo3 = AV82FuncaoAPF_Ativo3;
         AV143WWFuncaoAPFDS_18_Dynamicfiltersenabled4 = AV83DynamicFiltersEnabled4;
         AV144WWFuncaoAPFDS_19_Dynamicfiltersselector4 = AV84DynamicFiltersSelector4;
         AV145WWFuncaoAPFDS_20_Funcaoapf_nome4 = AV85FuncaoAPF_Nome4;
         AV146WWFuncaoAPFDS_21_Funcaoapf_tipo4 = AV86FuncaoAPF_Tipo4;
         AV147WWFuncaoAPFDS_22_Funcaoapf_complexidade4 = AV87FuncaoAPF_Complexidade4;
         AV148WWFuncaoAPFDS_23_Funcaoapf_ativo4 = AV88FuncaoAPF_Ativo4;
         AV149WWFuncaoAPFDS_24_Tffuncaoapf_nome = AV92TFFuncaoAPF_Nome;
         AV150WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel = AV93TFFuncaoAPF_Nome_Sel;
         AV151WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels = AV97TFFuncaoAPF_Tipo_Sels;
         AV152WWFuncaoAPFDS_27_Tffuncaoapf_td = AV100TFFuncaoAPF_TD;
         AV153WWFuncaoAPFDS_28_Tffuncaoapf_td_to = AV101TFFuncaoAPF_TD_To;
         AV154WWFuncaoAPFDS_29_Tffuncaoapf_ar = AV104TFFuncaoAPF_AR;
         AV155WWFuncaoAPFDS_30_Tffuncaoapf_ar_to = AV105TFFuncaoAPF_AR_To;
         AV156WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels = AV109TFFuncaoAPF_Complexidade_Sels;
         AV157WWFuncaoAPFDS_32_Tffuncaoapf_pf = AV112TFFuncaoAPF_PF;
         AV158WWFuncaoAPFDS_33_Tffuncaoapf_pf_to = AV113TFFuncaoAPF_PF_To;
         AV159WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom = AV116TFFuncaoAPF_FunAPFPaiNom;
         AV160WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel = AV117TFFuncaoAPF_FunAPFPaiNom_Sel;
         AV161WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels = AV121TFFuncaoAPF_Ativo_Sels;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17FuncaoAPF_Nome1, AV76FuncaoAPF_Tipo1, AV55FuncaoAPF_Ativo1, AV19DynamicFiltersSelector2, AV21FuncaoAPF_Nome2, AV78FuncaoAPF_Tipo2, AV56FuncaoAPF_Ativo2, AV23DynamicFiltersSelector3, AV25FuncaoAPF_Nome3, AV80FuncaoAPF_Tipo3, AV82FuncaoAPF_Ativo3, AV84DynamicFiltersSelector4, AV85FuncaoAPF_Nome4, AV86FuncaoAPF_Tipo4, AV88FuncaoAPF_Ativo4, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV83DynamicFiltersEnabled4, AV92TFFuncaoAPF_Nome, AV93TFFuncaoAPF_Nome_Sel, AV116TFFuncaoAPF_FunAPFPaiNom, AV117TFFuncaoAPF_FunAPFPaiNom_Sel, AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace, AV102ddo_FuncaoAPF_TDTitleControlIdToReplace, AV106ddo_FuncaoAPF_ARTitleControlIdToReplace, AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, AV114ddo_FuncaoAPF_PFTitleControlIdToReplace, AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace, AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace, AV77FuncaoAPF_Complexidade1, AV79FuncaoAPF_Complexidade2, AV81FuncaoAPF_Complexidade3, AV87FuncaoAPF_Complexidade4, AV97TFFuncaoAPF_Tipo_Sels, AV100TFFuncaoAPF_TD, AV101TFFuncaoAPF_TD_To, AV104TFFuncaoAPF_AR, AV105TFFuncaoAPF_AR_To, AV109TFFuncaoAPF_Complexidade_Sels, AV112TFFuncaoAPF_PF, AV113TFFuncaoAPF_PF_To, AV121TFFuncaoAPF_Ativo_Sels, AV165Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A165FuncaoAPF_Codigo, AV57FuncaoAPF_SistemaCod, A358FuncaoAPF_FunAPFPaiCod) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV126WWFuncaoAPFDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV127WWFuncaoAPFDS_2_Funcaoapf_nome1 = AV17FuncaoAPF_Nome1;
         AV128WWFuncaoAPFDS_3_Funcaoapf_tipo1 = AV76FuncaoAPF_Tipo1;
         AV129WWFuncaoAPFDS_4_Funcaoapf_complexidade1 = AV77FuncaoAPF_Complexidade1;
         AV130WWFuncaoAPFDS_5_Funcaoapf_ativo1 = AV55FuncaoAPF_Ativo1;
         AV131WWFuncaoAPFDS_6_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV132WWFuncaoAPFDS_7_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV133WWFuncaoAPFDS_8_Funcaoapf_nome2 = AV21FuncaoAPF_Nome2;
         AV134WWFuncaoAPFDS_9_Funcaoapf_tipo2 = AV78FuncaoAPF_Tipo2;
         AV135WWFuncaoAPFDS_10_Funcaoapf_complexidade2 = AV79FuncaoAPF_Complexidade2;
         AV136WWFuncaoAPFDS_11_Funcaoapf_ativo2 = AV56FuncaoAPF_Ativo2;
         AV137WWFuncaoAPFDS_12_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV138WWFuncaoAPFDS_13_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV139WWFuncaoAPFDS_14_Funcaoapf_nome3 = AV25FuncaoAPF_Nome3;
         AV140WWFuncaoAPFDS_15_Funcaoapf_tipo3 = AV80FuncaoAPF_Tipo3;
         AV141WWFuncaoAPFDS_16_Funcaoapf_complexidade3 = AV81FuncaoAPF_Complexidade3;
         AV142WWFuncaoAPFDS_17_Funcaoapf_ativo3 = AV82FuncaoAPF_Ativo3;
         AV143WWFuncaoAPFDS_18_Dynamicfiltersenabled4 = AV83DynamicFiltersEnabled4;
         AV144WWFuncaoAPFDS_19_Dynamicfiltersselector4 = AV84DynamicFiltersSelector4;
         AV145WWFuncaoAPFDS_20_Funcaoapf_nome4 = AV85FuncaoAPF_Nome4;
         AV146WWFuncaoAPFDS_21_Funcaoapf_tipo4 = AV86FuncaoAPF_Tipo4;
         AV147WWFuncaoAPFDS_22_Funcaoapf_complexidade4 = AV87FuncaoAPF_Complexidade4;
         AV148WWFuncaoAPFDS_23_Funcaoapf_ativo4 = AV88FuncaoAPF_Ativo4;
         AV149WWFuncaoAPFDS_24_Tffuncaoapf_nome = AV92TFFuncaoAPF_Nome;
         AV150WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel = AV93TFFuncaoAPF_Nome_Sel;
         AV151WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels = AV97TFFuncaoAPF_Tipo_Sels;
         AV152WWFuncaoAPFDS_27_Tffuncaoapf_td = AV100TFFuncaoAPF_TD;
         AV153WWFuncaoAPFDS_28_Tffuncaoapf_td_to = AV101TFFuncaoAPF_TD_To;
         AV154WWFuncaoAPFDS_29_Tffuncaoapf_ar = AV104TFFuncaoAPF_AR;
         AV155WWFuncaoAPFDS_30_Tffuncaoapf_ar_to = AV105TFFuncaoAPF_AR_To;
         AV156WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels = AV109TFFuncaoAPF_Complexidade_Sels;
         AV157WWFuncaoAPFDS_32_Tffuncaoapf_pf = AV112TFFuncaoAPF_PF;
         AV158WWFuncaoAPFDS_33_Tffuncaoapf_pf_to = AV113TFFuncaoAPF_PF_To;
         AV159WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom = AV116TFFuncaoAPF_FunAPFPaiNom;
         AV160WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel = AV117TFFuncaoAPF_FunAPFPaiNom_Sel;
         AV161WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels = AV121TFFuncaoAPF_Ativo_Sels;
         if ( GRID_nEOF == 0 )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17FuncaoAPF_Nome1, AV76FuncaoAPF_Tipo1, AV55FuncaoAPF_Ativo1, AV19DynamicFiltersSelector2, AV21FuncaoAPF_Nome2, AV78FuncaoAPF_Tipo2, AV56FuncaoAPF_Ativo2, AV23DynamicFiltersSelector3, AV25FuncaoAPF_Nome3, AV80FuncaoAPF_Tipo3, AV82FuncaoAPF_Ativo3, AV84DynamicFiltersSelector4, AV85FuncaoAPF_Nome4, AV86FuncaoAPF_Tipo4, AV88FuncaoAPF_Ativo4, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV83DynamicFiltersEnabled4, AV92TFFuncaoAPF_Nome, AV93TFFuncaoAPF_Nome_Sel, AV116TFFuncaoAPF_FunAPFPaiNom, AV117TFFuncaoAPF_FunAPFPaiNom_Sel, AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace, AV102ddo_FuncaoAPF_TDTitleControlIdToReplace, AV106ddo_FuncaoAPF_ARTitleControlIdToReplace, AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, AV114ddo_FuncaoAPF_PFTitleControlIdToReplace, AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace, AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace, AV77FuncaoAPF_Complexidade1, AV79FuncaoAPF_Complexidade2, AV81FuncaoAPF_Complexidade3, AV87FuncaoAPF_Complexidade4, AV97TFFuncaoAPF_Tipo_Sels, AV100TFFuncaoAPF_TD, AV101TFFuncaoAPF_TD_To, AV104TFFuncaoAPF_AR, AV105TFFuncaoAPF_AR_To, AV109TFFuncaoAPF_Complexidade_Sels, AV112TFFuncaoAPF_PF, AV113TFFuncaoAPF_PF_To, AV121TFFuncaoAPF_Ativo_Sels, AV165Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A165FuncaoAPF_Codigo, AV57FuncaoAPF_SistemaCod, A358FuncaoAPF_FunAPFPaiCod) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV126WWFuncaoAPFDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV127WWFuncaoAPFDS_2_Funcaoapf_nome1 = AV17FuncaoAPF_Nome1;
         AV128WWFuncaoAPFDS_3_Funcaoapf_tipo1 = AV76FuncaoAPF_Tipo1;
         AV129WWFuncaoAPFDS_4_Funcaoapf_complexidade1 = AV77FuncaoAPF_Complexidade1;
         AV130WWFuncaoAPFDS_5_Funcaoapf_ativo1 = AV55FuncaoAPF_Ativo1;
         AV131WWFuncaoAPFDS_6_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV132WWFuncaoAPFDS_7_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV133WWFuncaoAPFDS_8_Funcaoapf_nome2 = AV21FuncaoAPF_Nome2;
         AV134WWFuncaoAPFDS_9_Funcaoapf_tipo2 = AV78FuncaoAPF_Tipo2;
         AV135WWFuncaoAPFDS_10_Funcaoapf_complexidade2 = AV79FuncaoAPF_Complexidade2;
         AV136WWFuncaoAPFDS_11_Funcaoapf_ativo2 = AV56FuncaoAPF_Ativo2;
         AV137WWFuncaoAPFDS_12_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV138WWFuncaoAPFDS_13_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV139WWFuncaoAPFDS_14_Funcaoapf_nome3 = AV25FuncaoAPF_Nome3;
         AV140WWFuncaoAPFDS_15_Funcaoapf_tipo3 = AV80FuncaoAPF_Tipo3;
         AV141WWFuncaoAPFDS_16_Funcaoapf_complexidade3 = AV81FuncaoAPF_Complexidade3;
         AV142WWFuncaoAPFDS_17_Funcaoapf_ativo3 = AV82FuncaoAPF_Ativo3;
         AV143WWFuncaoAPFDS_18_Dynamicfiltersenabled4 = AV83DynamicFiltersEnabled4;
         AV144WWFuncaoAPFDS_19_Dynamicfiltersselector4 = AV84DynamicFiltersSelector4;
         AV145WWFuncaoAPFDS_20_Funcaoapf_nome4 = AV85FuncaoAPF_Nome4;
         AV146WWFuncaoAPFDS_21_Funcaoapf_tipo4 = AV86FuncaoAPF_Tipo4;
         AV147WWFuncaoAPFDS_22_Funcaoapf_complexidade4 = AV87FuncaoAPF_Complexidade4;
         AV148WWFuncaoAPFDS_23_Funcaoapf_ativo4 = AV88FuncaoAPF_Ativo4;
         AV149WWFuncaoAPFDS_24_Tffuncaoapf_nome = AV92TFFuncaoAPF_Nome;
         AV150WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel = AV93TFFuncaoAPF_Nome_Sel;
         AV151WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels = AV97TFFuncaoAPF_Tipo_Sels;
         AV152WWFuncaoAPFDS_27_Tffuncaoapf_td = AV100TFFuncaoAPF_TD;
         AV153WWFuncaoAPFDS_28_Tffuncaoapf_td_to = AV101TFFuncaoAPF_TD_To;
         AV154WWFuncaoAPFDS_29_Tffuncaoapf_ar = AV104TFFuncaoAPF_AR;
         AV155WWFuncaoAPFDS_30_Tffuncaoapf_ar_to = AV105TFFuncaoAPF_AR_To;
         AV156WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels = AV109TFFuncaoAPF_Complexidade_Sels;
         AV157WWFuncaoAPFDS_32_Tffuncaoapf_pf = AV112TFFuncaoAPF_PF;
         AV158WWFuncaoAPFDS_33_Tffuncaoapf_pf_to = AV113TFFuncaoAPF_PF_To;
         AV159WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom = AV116TFFuncaoAPF_FunAPFPaiNom;
         AV160WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel = AV117TFFuncaoAPF_FunAPFPaiNom_Sel;
         AV161WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels = AV121TFFuncaoAPF_Ativo_Sels;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17FuncaoAPF_Nome1, AV76FuncaoAPF_Tipo1, AV55FuncaoAPF_Ativo1, AV19DynamicFiltersSelector2, AV21FuncaoAPF_Nome2, AV78FuncaoAPF_Tipo2, AV56FuncaoAPF_Ativo2, AV23DynamicFiltersSelector3, AV25FuncaoAPF_Nome3, AV80FuncaoAPF_Tipo3, AV82FuncaoAPF_Ativo3, AV84DynamicFiltersSelector4, AV85FuncaoAPF_Nome4, AV86FuncaoAPF_Tipo4, AV88FuncaoAPF_Ativo4, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV83DynamicFiltersEnabled4, AV92TFFuncaoAPF_Nome, AV93TFFuncaoAPF_Nome_Sel, AV116TFFuncaoAPF_FunAPFPaiNom, AV117TFFuncaoAPF_FunAPFPaiNom_Sel, AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace, AV102ddo_FuncaoAPF_TDTitleControlIdToReplace, AV106ddo_FuncaoAPF_ARTitleControlIdToReplace, AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, AV114ddo_FuncaoAPF_PFTitleControlIdToReplace, AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace, AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace, AV77FuncaoAPF_Complexidade1, AV79FuncaoAPF_Complexidade2, AV81FuncaoAPF_Complexidade3, AV87FuncaoAPF_Complexidade4, AV97TFFuncaoAPF_Tipo_Sels, AV100TFFuncaoAPF_TD, AV101TFFuncaoAPF_TD_To, AV104TFFuncaoAPF_AR, AV105TFFuncaoAPF_AR_To, AV109TFFuncaoAPF_Complexidade_Sels, AV112TFFuncaoAPF_PF, AV113TFFuncaoAPF_PF_To, AV121TFFuncaoAPF_Ativo_Sels, AV165Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A165FuncaoAPF_Codigo, AV57FuncaoAPF_SistemaCod, A358FuncaoAPF_FunAPFPaiCod) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV126WWFuncaoAPFDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV127WWFuncaoAPFDS_2_Funcaoapf_nome1 = AV17FuncaoAPF_Nome1;
         AV128WWFuncaoAPFDS_3_Funcaoapf_tipo1 = AV76FuncaoAPF_Tipo1;
         AV129WWFuncaoAPFDS_4_Funcaoapf_complexidade1 = AV77FuncaoAPF_Complexidade1;
         AV130WWFuncaoAPFDS_5_Funcaoapf_ativo1 = AV55FuncaoAPF_Ativo1;
         AV131WWFuncaoAPFDS_6_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV132WWFuncaoAPFDS_7_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV133WWFuncaoAPFDS_8_Funcaoapf_nome2 = AV21FuncaoAPF_Nome2;
         AV134WWFuncaoAPFDS_9_Funcaoapf_tipo2 = AV78FuncaoAPF_Tipo2;
         AV135WWFuncaoAPFDS_10_Funcaoapf_complexidade2 = AV79FuncaoAPF_Complexidade2;
         AV136WWFuncaoAPFDS_11_Funcaoapf_ativo2 = AV56FuncaoAPF_Ativo2;
         AV137WWFuncaoAPFDS_12_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV138WWFuncaoAPFDS_13_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV139WWFuncaoAPFDS_14_Funcaoapf_nome3 = AV25FuncaoAPF_Nome3;
         AV140WWFuncaoAPFDS_15_Funcaoapf_tipo3 = AV80FuncaoAPF_Tipo3;
         AV141WWFuncaoAPFDS_16_Funcaoapf_complexidade3 = AV81FuncaoAPF_Complexidade3;
         AV142WWFuncaoAPFDS_17_Funcaoapf_ativo3 = AV82FuncaoAPF_Ativo3;
         AV143WWFuncaoAPFDS_18_Dynamicfiltersenabled4 = AV83DynamicFiltersEnabled4;
         AV144WWFuncaoAPFDS_19_Dynamicfiltersselector4 = AV84DynamicFiltersSelector4;
         AV145WWFuncaoAPFDS_20_Funcaoapf_nome4 = AV85FuncaoAPF_Nome4;
         AV146WWFuncaoAPFDS_21_Funcaoapf_tipo4 = AV86FuncaoAPF_Tipo4;
         AV147WWFuncaoAPFDS_22_Funcaoapf_complexidade4 = AV87FuncaoAPF_Complexidade4;
         AV148WWFuncaoAPFDS_23_Funcaoapf_ativo4 = AV88FuncaoAPF_Ativo4;
         AV149WWFuncaoAPFDS_24_Tffuncaoapf_nome = AV92TFFuncaoAPF_Nome;
         AV150WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel = AV93TFFuncaoAPF_Nome_Sel;
         AV151WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels = AV97TFFuncaoAPF_Tipo_Sels;
         AV152WWFuncaoAPFDS_27_Tffuncaoapf_td = AV100TFFuncaoAPF_TD;
         AV153WWFuncaoAPFDS_28_Tffuncaoapf_td_to = AV101TFFuncaoAPF_TD_To;
         AV154WWFuncaoAPFDS_29_Tffuncaoapf_ar = AV104TFFuncaoAPF_AR;
         AV155WWFuncaoAPFDS_30_Tffuncaoapf_ar_to = AV105TFFuncaoAPF_AR_To;
         AV156WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels = AV109TFFuncaoAPF_Complexidade_Sels;
         AV157WWFuncaoAPFDS_32_Tffuncaoapf_pf = AV112TFFuncaoAPF_PF;
         AV158WWFuncaoAPFDS_33_Tffuncaoapf_pf_to = AV113TFFuncaoAPF_PF_To;
         AV159WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom = AV116TFFuncaoAPF_FunAPFPaiNom;
         AV160WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel = AV117TFFuncaoAPF_FunAPFPaiNom_Sel;
         AV161WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels = AV121TFFuncaoAPF_Ativo_Sels;
         subGrid_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17FuncaoAPF_Nome1, AV76FuncaoAPF_Tipo1, AV55FuncaoAPF_Ativo1, AV19DynamicFiltersSelector2, AV21FuncaoAPF_Nome2, AV78FuncaoAPF_Tipo2, AV56FuncaoAPF_Ativo2, AV23DynamicFiltersSelector3, AV25FuncaoAPF_Nome3, AV80FuncaoAPF_Tipo3, AV82FuncaoAPF_Ativo3, AV84DynamicFiltersSelector4, AV85FuncaoAPF_Nome4, AV86FuncaoAPF_Tipo4, AV88FuncaoAPF_Ativo4, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV83DynamicFiltersEnabled4, AV92TFFuncaoAPF_Nome, AV93TFFuncaoAPF_Nome_Sel, AV116TFFuncaoAPF_FunAPFPaiNom, AV117TFFuncaoAPF_FunAPFPaiNom_Sel, AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace, AV102ddo_FuncaoAPF_TDTitleControlIdToReplace, AV106ddo_FuncaoAPF_ARTitleControlIdToReplace, AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, AV114ddo_FuncaoAPF_PFTitleControlIdToReplace, AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace, AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace, AV77FuncaoAPF_Complexidade1, AV79FuncaoAPF_Complexidade2, AV81FuncaoAPF_Complexidade3, AV87FuncaoAPF_Complexidade4, AV97TFFuncaoAPF_Tipo_Sels, AV100TFFuncaoAPF_TD, AV101TFFuncaoAPF_TD_To, AV104TFFuncaoAPF_AR, AV105TFFuncaoAPF_AR_To, AV109TFFuncaoAPF_Complexidade_Sels, AV112TFFuncaoAPF_PF, AV113TFFuncaoAPF_PF_To, AV121TFFuncaoAPF_Ativo_Sels, AV165Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A165FuncaoAPF_Codigo, AV57FuncaoAPF_SistemaCod, A358FuncaoAPF_FunAPFPaiCod) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV126WWFuncaoAPFDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV127WWFuncaoAPFDS_2_Funcaoapf_nome1 = AV17FuncaoAPF_Nome1;
         AV128WWFuncaoAPFDS_3_Funcaoapf_tipo1 = AV76FuncaoAPF_Tipo1;
         AV129WWFuncaoAPFDS_4_Funcaoapf_complexidade1 = AV77FuncaoAPF_Complexidade1;
         AV130WWFuncaoAPFDS_5_Funcaoapf_ativo1 = AV55FuncaoAPF_Ativo1;
         AV131WWFuncaoAPFDS_6_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV132WWFuncaoAPFDS_7_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV133WWFuncaoAPFDS_8_Funcaoapf_nome2 = AV21FuncaoAPF_Nome2;
         AV134WWFuncaoAPFDS_9_Funcaoapf_tipo2 = AV78FuncaoAPF_Tipo2;
         AV135WWFuncaoAPFDS_10_Funcaoapf_complexidade2 = AV79FuncaoAPF_Complexidade2;
         AV136WWFuncaoAPFDS_11_Funcaoapf_ativo2 = AV56FuncaoAPF_Ativo2;
         AV137WWFuncaoAPFDS_12_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV138WWFuncaoAPFDS_13_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV139WWFuncaoAPFDS_14_Funcaoapf_nome3 = AV25FuncaoAPF_Nome3;
         AV140WWFuncaoAPFDS_15_Funcaoapf_tipo3 = AV80FuncaoAPF_Tipo3;
         AV141WWFuncaoAPFDS_16_Funcaoapf_complexidade3 = AV81FuncaoAPF_Complexidade3;
         AV142WWFuncaoAPFDS_17_Funcaoapf_ativo3 = AV82FuncaoAPF_Ativo3;
         AV143WWFuncaoAPFDS_18_Dynamicfiltersenabled4 = AV83DynamicFiltersEnabled4;
         AV144WWFuncaoAPFDS_19_Dynamicfiltersselector4 = AV84DynamicFiltersSelector4;
         AV145WWFuncaoAPFDS_20_Funcaoapf_nome4 = AV85FuncaoAPF_Nome4;
         AV146WWFuncaoAPFDS_21_Funcaoapf_tipo4 = AV86FuncaoAPF_Tipo4;
         AV147WWFuncaoAPFDS_22_Funcaoapf_complexidade4 = AV87FuncaoAPF_Complexidade4;
         AV148WWFuncaoAPFDS_23_Funcaoapf_ativo4 = AV88FuncaoAPF_Ativo4;
         AV149WWFuncaoAPFDS_24_Tffuncaoapf_nome = AV92TFFuncaoAPF_Nome;
         AV150WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel = AV93TFFuncaoAPF_Nome_Sel;
         AV151WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels = AV97TFFuncaoAPF_Tipo_Sels;
         AV152WWFuncaoAPFDS_27_Tffuncaoapf_td = AV100TFFuncaoAPF_TD;
         AV153WWFuncaoAPFDS_28_Tffuncaoapf_td_to = AV101TFFuncaoAPF_TD_To;
         AV154WWFuncaoAPFDS_29_Tffuncaoapf_ar = AV104TFFuncaoAPF_AR;
         AV155WWFuncaoAPFDS_30_Tffuncaoapf_ar_to = AV105TFFuncaoAPF_AR_To;
         AV156WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels = AV109TFFuncaoAPF_Complexidade_Sels;
         AV157WWFuncaoAPFDS_32_Tffuncaoapf_pf = AV112TFFuncaoAPF_PF;
         AV158WWFuncaoAPFDS_33_Tffuncaoapf_pf_to = AV113TFFuncaoAPF_PF_To;
         AV159WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom = AV116TFFuncaoAPF_FunAPFPaiNom;
         AV160WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel = AV117TFFuncaoAPF_FunAPFPaiNom_Sel;
         AV161WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels = AV121TFFuncaoAPF_Ativo_Sels;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17FuncaoAPF_Nome1, AV76FuncaoAPF_Tipo1, AV55FuncaoAPF_Ativo1, AV19DynamicFiltersSelector2, AV21FuncaoAPF_Nome2, AV78FuncaoAPF_Tipo2, AV56FuncaoAPF_Ativo2, AV23DynamicFiltersSelector3, AV25FuncaoAPF_Nome3, AV80FuncaoAPF_Tipo3, AV82FuncaoAPF_Ativo3, AV84DynamicFiltersSelector4, AV85FuncaoAPF_Nome4, AV86FuncaoAPF_Tipo4, AV88FuncaoAPF_Ativo4, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV83DynamicFiltersEnabled4, AV92TFFuncaoAPF_Nome, AV93TFFuncaoAPF_Nome_Sel, AV116TFFuncaoAPF_FunAPFPaiNom, AV117TFFuncaoAPF_FunAPFPaiNom_Sel, AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace, AV102ddo_FuncaoAPF_TDTitleControlIdToReplace, AV106ddo_FuncaoAPF_ARTitleControlIdToReplace, AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, AV114ddo_FuncaoAPF_PFTitleControlIdToReplace, AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace, AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace, AV77FuncaoAPF_Complexidade1, AV79FuncaoAPF_Complexidade2, AV81FuncaoAPF_Complexidade3, AV87FuncaoAPF_Complexidade4, AV97TFFuncaoAPF_Tipo_Sels, AV100TFFuncaoAPF_TD, AV101TFFuncaoAPF_TD_To, AV104TFFuncaoAPF_AR, AV105TFFuncaoAPF_AR_To, AV109TFFuncaoAPF_Complexidade_Sels, AV112TFFuncaoAPF_PF, AV113TFFuncaoAPF_PF_To, AV121TFFuncaoAPF_Ativo_Sels, AV165Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A165FuncaoAPF_Codigo, AV57FuncaoAPF_SistemaCod, A358FuncaoAPF_FunAPFPaiCod) ;
         }
         return (int)(0) ;
      }

      protected void STRUP3Z0( )
      {
         /* Before Start, stand alone formulas. */
         AV165Pgmname = "WWFuncaoAPF";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E333Z2 */
         E333Z2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV123DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAOAPF_NOMETITLEFILTERDATA"), AV91FuncaoAPF_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAOAPF_TIPOTITLEFILTERDATA"), AV95FuncaoAPF_TipoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAOAPF_TDTITLEFILTERDATA"), AV99FuncaoAPF_TDTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAOAPF_ARTITLEFILTERDATA"), AV103FuncaoAPF_ARTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAOAPF_COMPLEXIDADETITLEFILTERDATA"), AV107FuncaoAPF_ComplexidadeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAOAPF_PFTITLEFILTERDATA"), AV111FuncaoAPF_PFTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAOAPF_FUNAPFPAINOMTITLEFILTERDATA"), AV115FuncaoAPF_FunAPFPaiNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAOAPF_ATIVOTITLEFILTERDATA"), AV119FuncaoAPF_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            AV17FuncaoAPF_Nome1 = cgiGet( edtavFuncaoapf_nome1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoAPF_Nome1", AV17FuncaoAPF_Nome1);
            cmbavFuncaoapf_tipo1.Name = cmbavFuncaoapf_tipo1_Internalname;
            cmbavFuncaoapf_tipo1.CurrentValue = cgiGet( cmbavFuncaoapf_tipo1_Internalname);
            AV76FuncaoAPF_Tipo1 = cgiGet( cmbavFuncaoapf_tipo1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76FuncaoAPF_Tipo1", AV76FuncaoAPF_Tipo1);
            cmbavFuncaoapf_complexidade1.Name = cmbavFuncaoapf_complexidade1_Internalname;
            cmbavFuncaoapf_complexidade1.CurrentValue = cgiGet( cmbavFuncaoapf_complexidade1_Internalname);
            AV77FuncaoAPF_Complexidade1 = cgiGet( cmbavFuncaoapf_complexidade1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77FuncaoAPF_Complexidade1", AV77FuncaoAPF_Complexidade1);
            cmbavFuncaoapf_ativo1.Name = cmbavFuncaoapf_ativo1_Internalname;
            cmbavFuncaoapf_ativo1.CurrentValue = cgiGet( cmbavFuncaoapf_ativo1_Internalname);
            AV55FuncaoAPF_Ativo1 = cgiGet( cmbavFuncaoapf_ativo1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55FuncaoAPF_Ativo1", AV55FuncaoAPF_Ativo1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            AV21FuncaoAPF_Nome2 = cgiGet( edtavFuncaoapf_nome2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21FuncaoAPF_Nome2", AV21FuncaoAPF_Nome2);
            cmbavFuncaoapf_tipo2.Name = cmbavFuncaoapf_tipo2_Internalname;
            cmbavFuncaoapf_tipo2.CurrentValue = cgiGet( cmbavFuncaoapf_tipo2_Internalname);
            AV78FuncaoAPF_Tipo2 = cgiGet( cmbavFuncaoapf_tipo2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78FuncaoAPF_Tipo2", AV78FuncaoAPF_Tipo2);
            cmbavFuncaoapf_complexidade2.Name = cmbavFuncaoapf_complexidade2_Internalname;
            cmbavFuncaoapf_complexidade2.CurrentValue = cgiGet( cmbavFuncaoapf_complexidade2_Internalname);
            AV79FuncaoAPF_Complexidade2 = cgiGet( cmbavFuncaoapf_complexidade2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79FuncaoAPF_Complexidade2", AV79FuncaoAPF_Complexidade2);
            cmbavFuncaoapf_ativo2.Name = cmbavFuncaoapf_ativo2_Internalname;
            cmbavFuncaoapf_ativo2.CurrentValue = cgiGet( cmbavFuncaoapf_ativo2_Internalname);
            AV56FuncaoAPF_Ativo2 = cgiGet( cmbavFuncaoapf_ativo2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56FuncaoAPF_Ativo2", AV56FuncaoAPF_Ativo2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            AV25FuncaoAPF_Nome3 = cgiGet( edtavFuncaoapf_nome3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25FuncaoAPF_Nome3", AV25FuncaoAPF_Nome3);
            cmbavFuncaoapf_tipo3.Name = cmbavFuncaoapf_tipo3_Internalname;
            cmbavFuncaoapf_tipo3.CurrentValue = cgiGet( cmbavFuncaoapf_tipo3_Internalname);
            AV80FuncaoAPF_Tipo3 = cgiGet( cmbavFuncaoapf_tipo3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80FuncaoAPF_Tipo3", AV80FuncaoAPF_Tipo3);
            cmbavFuncaoapf_complexidade3.Name = cmbavFuncaoapf_complexidade3_Internalname;
            cmbavFuncaoapf_complexidade3.CurrentValue = cgiGet( cmbavFuncaoapf_complexidade3_Internalname);
            AV81FuncaoAPF_Complexidade3 = cgiGet( cmbavFuncaoapf_complexidade3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81FuncaoAPF_Complexidade3", AV81FuncaoAPF_Complexidade3);
            cmbavFuncaoapf_ativo3.Name = cmbavFuncaoapf_ativo3_Internalname;
            cmbavFuncaoapf_ativo3.CurrentValue = cgiGet( cmbavFuncaoapf_ativo3_Internalname);
            AV82FuncaoAPF_Ativo3 = cgiGet( cmbavFuncaoapf_ativo3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82FuncaoAPF_Ativo3", AV82FuncaoAPF_Ativo3);
            cmbavDynamicfiltersselector4.Name = cmbavDynamicfiltersselector4_Internalname;
            cmbavDynamicfiltersselector4.CurrentValue = cgiGet( cmbavDynamicfiltersselector4_Internalname);
            AV84DynamicFiltersSelector4 = cgiGet( cmbavDynamicfiltersselector4_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84DynamicFiltersSelector4", AV84DynamicFiltersSelector4);
            AV85FuncaoAPF_Nome4 = cgiGet( edtavFuncaoapf_nome4_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85FuncaoAPF_Nome4", AV85FuncaoAPF_Nome4);
            cmbavFuncaoapf_tipo4.Name = cmbavFuncaoapf_tipo4_Internalname;
            cmbavFuncaoapf_tipo4.CurrentValue = cgiGet( cmbavFuncaoapf_tipo4_Internalname);
            AV86FuncaoAPF_Tipo4 = cgiGet( cmbavFuncaoapf_tipo4_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86FuncaoAPF_Tipo4", AV86FuncaoAPF_Tipo4);
            cmbavFuncaoapf_complexidade4.Name = cmbavFuncaoapf_complexidade4_Internalname;
            cmbavFuncaoapf_complexidade4.CurrentValue = cgiGet( cmbavFuncaoapf_complexidade4_Internalname);
            AV87FuncaoAPF_Complexidade4 = cgiGet( cmbavFuncaoapf_complexidade4_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87FuncaoAPF_Complexidade4", AV87FuncaoAPF_Complexidade4);
            cmbavFuncaoapf_ativo4.Name = cmbavFuncaoapf_ativo4_Internalname;
            cmbavFuncaoapf_ativo4.CurrentValue = cgiGet( cmbavFuncaoapf_ativo4_Internalname);
            AV88FuncaoAPF_Ativo4 = cgiGet( cmbavFuncaoapf_ativo4_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88FuncaoAPF_Ativo4", AV88FuncaoAPF_Ativo4);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            AV83DynamicFiltersEnabled4 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled4_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83DynamicFiltersEnabled4", AV83DynamicFiltersEnabled4);
            AV92TFFuncaoAPF_Nome = cgiGet( edtavTffuncaoapf_nome_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92TFFuncaoAPF_Nome", AV92TFFuncaoAPF_Nome);
            AV93TFFuncaoAPF_Nome_Sel = cgiGet( edtavTffuncaoapf_nome_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93TFFuncaoAPF_Nome_Sel", AV93TFFuncaoAPF_Nome_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_td_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_td_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAOAPF_TD");
               GX_FocusControl = edtavTffuncaoapf_td_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV100TFFuncaoAPF_TD = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100TFFuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(AV100TFFuncaoAPF_TD), 4, 0)));
            }
            else
            {
               AV100TFFuncaoAPF_TD = (short)(context.localUtil.CToN( cgiGet( edtavTffuncaoapf_td_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100TFFuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(AV100TFFuncaoAPF_TD), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_td_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_td_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAOAPF_TD_TO");
               GX_FocusControl = edtavTffuncaoapf_td_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV101TFFuncaoAPF_TD_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101TFFuncaoAPF_TD_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV101TFFuncaoAPF_TD_To), 4, 0)));
            }
            else
            {
               AV101TFFuncaoAPF_TD_To = (short)(context.localUtil.CToN( cgiGet( edtavTffuncaoapf_td_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101TFFuncaoAPF_TD_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV101TFFuncaoAPF_TD_To), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_ar_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_ar_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAOAPF_AR");
               GX_FocusControl = edtavTffuncaoapf_ar_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV104TFFuncaoAPF_AR = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104TFFuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV104TFFuncaoAPF_AR), 4, 0)));
            }
            else
            {
               AV104TFFuncaoAPF_AR = (short)(context.localUtil.CToN( cgiGet( edtavTffuncaoapf_ar_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104TFFuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV104TFFuncaoAPF_AR), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_ar_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_ar_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAOAPF_AR_TO");
               GX_FocusControl = edtavTffuncaoapf_ar_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV105TFFuncaoAPF_AR_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFFuncaoAPF_AR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105TFFuncaoAPF_AR_To), 4, 0)));
            }
            else
            {
               AV105TFFuncaoAPF_AR_To = (short)(context.localUtil.CToN( cgiGet( edtavTffuncaoapf_ar_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFFuncaoAPF_AR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105TFFuncaoAPF_AR_To), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_pf_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_pf_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAOAPF_PF");
               GX_FocusControl = edtavTffuncaoapf_pf_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV112TFFuncaoAPF_PF = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV112TFFuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( AV112TFFuncaoAPF_PF, 14, 5)));
            }
            else
            {
               AV112TFFuncaoAPF_PF = context.localUtil.CToN( cgiGet( edtavTffuncaoapf_pf_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV112TFFuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( AV112TFFuncaoAPF_PF, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_pf_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_pf_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAOAPF_PF_TO");
               GX_FocusControl = edtavTffuncaoapf_pf_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV113TFFuncaoAPF_PF_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV113TFFuncaoAPF_PF_To", StringUtil.LTrim( StringUtil.Str( AV113TFFuncaoAPF_PF_To, 14, 5)));
            }
            else
            {
               AV113TFFuncaoAPF_PF_To = context.localUtil.CToN( cgiGet( edtavTffuncaoapf_pf_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV113TFFuncaoAPF_PF_To", StringUtil.LTrim( StringUtil.Str( AV113TFFuncaoAPF_PF_To, 14, 5)));
            }
            AV116TFFuncaoAPF_FunAPFPaiNom = cgiGet( edtavTffuncaoapf_funapfpainom_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV116TFFuncaoAPF_FunAPFPaiNom", AV116TFFuncaoAPF_FunAPFPaiNom);
            AV117TFFuncaoAPF_FunAPFPaiNom_Sel = cgiGet( edtavTffuncaoapf_funapfpainom_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117TFFuncaoAPF_FunAPFPaiNom_Sel", AV117TFFuncaoAPF_FunAPFPaiNom_Sel);
            AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace", AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace);
            AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapf_tipotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace", AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace);
            AV102ddo_FuncaoAPF_TDTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapf_tdtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102ddo_FuncaoAPF_TDTitleControlIdToReplace", AV102ddo_FuncaoAPF_TDTitleControlIdToReplace);
            AV106ddo_FuncaoAPF_ARTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapf_artitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV106ddo_FuncaoAPF_ARTitleControlIdToReplace", AV106ddo_FuncaoAPF_ARTitleControlIdToReplace);
            AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace", AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace);
            AV114ddo_FuncaoAPF_PFTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV114ddo_FuncaoAPF_PFTitleControlIdToReplace", AV114ddo_FuncaoAPF_PFTitleControlIdToReplace);
            AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapf_funapfpainomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace", AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace);
            AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapf_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace", AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_96 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_96"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_funcaoapf_nome_Caption = cgiGet( "DDO_FUNCAOAPF_NOME_Caption");
            Ddo_funcaoapf_nome_Tooltip = cgiGet( "DDO_FUNCAOAPF_NOME_Tooltip");
            Ddo_funcaoapf_nome_Cls = cgiGet( "DDO_FUNCAOAPF_NOME_Cls");
            Ddo_funcaoapf_nome_Filteredtext_set = cgiGet( "DDO_FUNCAOAPF_NOME_Filteredtext_set");
            Ddo_funcaoapf_nome_Selectedvalue_set = cgiGet( "DDO_FUNCAOAPF_NOME_Selectedvalue_set");
            Ddo_funcaoapf_nome_Dropdownoptionstype = cgiGet( "DDO_FUNCAOAPF_NOME_Dropdownoptionstype");
            Ddo_funcaoapf_nome_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAOAPF_NOME_Titlecontrolidtoreplace");
            Ddo_funcaoapf_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_NOME_Includesortasc"));
            Ddo_funcaoapf_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_NOME_Includesortdsc"));
            Ddo_funcaoapf_nome_Sortedstatus = cgiGet( "DDO_FUNCAOAPF_NOME_Sortedstatus");
            Ddo_funcaoapf_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_NOME_Includefilter"));
            Ddo_funcaoapf_nome_Filtertype = cgiGet( "DDO_FUNCAOAPF_NOME_Filtertype");
            Ddo_funcaoapf_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_NOME_Filterisrange"));
            Ddo_funcaoapf_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_NOME_Includedatalist"));
            Ddo_funcaoapf_nome_Datalisttype = cgiGet( "DDO_FUNCAOAPF_NOME_Datalisttype");
            Ddo_funcaoapf_nome_Datalistproc = cgiGet( "DDO_FUNCAOAPF_NOME_Datalistproc");
            Ddo_funcaoapf_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_FUNCAOAPF_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaoapf_nome_Sortasc = cgiGet( "DDO_FUNCAOAPF_NOME_Sortasc");
            Ddo_funcaoapf_nome_Sortdsc = cgiGet( "DDO_FUNCAOAPF_NOME_Sortdsc");
            Ddo_funcaoapf_nome_Loadingdata = cgiGet( "DDO_FUNCAOAPF_NOME_Loadingdata");
            Ddo_funcaoapf_nome_Cleanfilter = cgiGet( "DDO_FUNCAOAPF_NOME_Cleanfilter");
            Ddo_funcaoapf_nome_Noresultsfound = cgiGet( "DDO_FUNCAOAPF_NOME_Noresultsfound");
            Ddo_funcaoapf_nome_Searchbuttontext = cgiGet( "DDO_FUNCAOAPF_NOME_Searchbuttontext");
            Ddo_funcaoapf_tipo_Caption = cgiGet( "DDO_FUNCAOAPF_TIPO_Caption");
            Ddo_funcaoapf_tipo_Tooltip = cgiGet( "DDO_FUNCAOAPF_TIPO_Tooltip");
            Ddo_funcaoapf_tipo_Cls = cgiGet( "DDO_FUNCAOAPF_TIPO_Cls");
            Ddo_funcaoapf_tipo_Selectedvalue_set = cgiGet( "DDO_FUNCAOAPF_TIPO_Selectedvalue_set");
            Ddo_funcaoapf_tipo_Dropdownoptionstype = cgiGet( "DDO_FUNCAOAPF_TIPO_Dropdownoptionstype");
            Ddo_funcaoapf_tipo_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAOAPF_TIPO_Titlecontrolidtoreplace");
            Ddo_funcaoapf_tipo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_TIPO_Includesortasc"));
            Ddo_funcaoapf_tipo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_TIPO_Includesortdsc"));
            Ddo_funcaoapf_tipo_Sortedstatus = cgiGet( "DDO_FUNCAOAPF_TIPO_Sortedstatus");
            Ddo_funcaoapf_tipo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_TIPO_Includefilter"));
            Ddo_funcaoapf_tipo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_TIPO_Includedatalist"));
            Ddo_funcaoapf_tipo_Datalisttype = cgiGet( "DDO_FUNCAOAPF_TIPO_Datalisttype");
            Ddo_funcaoapf_tipo_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_TIPO_Allowmultipleselection"));
            Ddo_funcaoapf_tipo_Datalistfixedvalues = cgiGet( "DDO_FUNCAOAPF_TIPO_Datalistfixedvalues");
            Ddo_funcaoapf_tipo_Sortasc = cgiGet( "DDO_FUNCAOAPF_TIPO_Sortasc");
            Ddo_funcaoapf_tipo_Sortdsc = cgiGet( "DDO_FUNCAOAPF_TIPO_Sortdsc");
            Ddo_funcaoapf_tipo_Cleanfilter = cgiGet( "DDO_FUNCAOAPF_TIPO_Cleanfilter");
            Ddo_funcaoapf_tipo_Searchbuttontext = cgiGet( "DDO_FUNCAOAPF_TIPO_Searchbuttontext");
            Ddo_funcaoapf_td_Caption = cgiGet( "DDO_FUNCAOAPF_TD_Caption");
            Ddo_funcaoapf_td_Tooltip = cgiGet( "DDO_FUNCAOAPF_TD_Tooltip");
            Ddo_funcaoapf_td_Cls = cgiGet( "DDO_FUNCAOAPF_TD_Cls");
            Ddo_funcaoapf_td_Filteredtext_set = cgiGet( "DDO_FUNCAOAPF_TD_Filteredtext_set");
            Ddo_funcaoapf_td_Filteredtextto_set = cgiGet( "DDO_FUNCAOAPF_TD_Filteredtextto_set");
            Ddo_funcaoapf_td_Dropdownoptionstype = cgiGet( "DDO_FUNCAOAPF_TD_Dropdownoptionstype");
            Ddo_funcaoapf_td_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAOAPF_TD_Titlecontrolidtoreplace");
            Ddo_funcaoapf_td_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_TD_Includesortasc"));
            Ddo_funcaoapf_td_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_TD_Includesortdsc"));
            Ddo_funcaoapf_td_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_TD_Includefilter"));
            Ddo_funcaoapf_td_Filtertype = cgiGet( "DDO_FUNCAOAPF_TD_Filtertype");
            Ddo_funcaoapf_td_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_TD_Filterisrange"));
            Ddo_funcaoapf_td_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_TD_Includedatalist"));
            Ddo_funcaoapf_td_Cleanfilter = cgiGet( "DDO_FUNCAOAPF_TD_Cleanfilter");
            Ddo_funcaoapf_td_Rangefilterfrom = cgiGet( "DDO_FUNCAOAPF_TD_Rangefilterfrom");
            Ddo_funcaoapf_td_Rangefilterto = cgiGet( "DDO_FUNCAOAPF_TD_Rangefilterto");
            Ddo_funcaoapf_td_Searchbuttontext = cgiGet( "DDO_FUNCAOAPF_TD_Searchbuttontext");
            Ddo_funcaoapf_ar_Caption = cgiGet( "DDO_FUNCAOAPF_AR_Caption");
            Ddo_funcaoapf_ar_Tooltip = cgiGet( "DDO_FUNCAOAPF_AR_Tooltip");
            Ddo_funcaoapf_ar_Cls = cgiGet( "DDO_FUNCAOAPF_AR_Cls");
            Ddo_funcaoapf_ar_Filteredtext_set = cgiGet( "DDO_FUNCAOAPF_AR_Filteredtext_set");
            Ddo_funcaoapf_ar_Filteredtextto_set = cgiGet( "DDO_FUNCAOAPF_AR_Filteredtextto_set");
            Ddo_funcaoapf_ar_Dropdownoptionstype = cgiGet( "DDO_FUNCAOAPF_AR_Dropdownoptionstype");
            Ddo_funcaoapf_ar_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAOAPF_AR_Titlecontrolidtoreplace");
            Ddo_funcaoapf_ar_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_AR_Includesortasc"));
            Ddo_funcaoapf_ar_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_AR_Includesortdsc"));
            Ddo_funcaoapf_ar_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_AR_Includefilter"));
            Ddo_funcaoapf_ar_Filtertype = cgiGet( "DDO_FUNCAOAPF_AR_Filtertype");
            Ddo_funcaoapf_ar_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_AR_Filterisrange"));
            Ddo_funcaoapf_ar_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_AR_Includedatalist"));
            Ddo_funcaoapf_ar_Cleanfilter = cgiGet( "DDO_FUNCAOAPF_AR_Cleanfilter");
            Ddo_funcaoapf_ar_Rangefilterfrom = cgiGet( "DDO_FUNCAOAPF_AR_Rangefilterfrom");
            Ddo_funcaoapf_ar_Rangefilterto = cgiGet( "DDO_FUNCAOAPF_AR_Rangefilterto");
            Ddo_funcaoapf_ar_Searchbuttontext = cgiGet( "DDO_FUNCAOAPF_AR_Searchbuttontext");
            Ddo_funcaoapf_complexidade_Caption = cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Caption");
            Ddo_funcaoapf_complexidade_Tooltip = cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Tooltip");
            Ddo_funcaoapf_complexidade_Cls = cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Cls");
            Ddo_funcaoapf_complexidade_Selectedvalue_set = cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Selectedvalue_set");
            Ddo_funcaoapf_complexidade_Dropdownoptionstype = cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Dropdownoptionstype");
            Ddo_funcaoapf_complexidade_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Titlecontrolidtoreplace");
            Ddo_funcaoapf_complexidade_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Includesortasc"));
            Ddo_funcaoapf_complexidade_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Includesortdsc"));
            Ddo_funcaoapf_complexidade_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Includefilter"));
            Ddo_funcaoapf_complexidade_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Includedatalist"));
            Ddo_funcaoapf_complexidade_Datalisttype = cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Datalisttype");
            Ddo_funcaoapf_complexidade_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Allowmultipleselection"));
            Ddo_funcaoapf_complexidade_Datalistfixedvalues = cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Datalistfixedvalues");
            Ddo_funcaoapf_complexidade_Cleanfilter = cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Cleanfilter");
            Ddo_funcaoapf_complexidade_Searchbuttontext = cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Searchbuttontext");
            Ddo_funcaoapf_pf_Caption = cgiGet( "DDO_FUNCAOAPF_PF_Caption");
            Ddo_funcaoapf_pf_Tooltip = cgiGet( "DDO_FUNCAOAPF_PF_Tooltip");
            Ddo_funcaoapf_pf_Cls = cgiGet( "DDO_FUNCAOAPF_PF_Cls");
            Ddo_funcaoapf_pf_Filteredtext_set = cgiGet( "DDO_FUNCAOAPF_PF_Filteredtext_set");
            Ddo_funcaoapf_pf_Filteredtextto_set = cgiGet( "DDO_FUNCAOAPF_PF_Filteredtextto_set");
            Ddo_funcaoapf_pf_Dropdownoptionstype = cgiGet( "DDO_FUNCAOAPF_PF_Dropdownoptionstype");
            Ddo_funcaoapf_pf_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAOAPF_PF_Titlecontrolidtoreplace");
            Ddo_funcaoapf_pf_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_PF_Includesortasc"));
            Ddo_funcaoapf_pf_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_PF_Includesortdsc"));
            Ddo_funcaoapf_pf_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_PF_Includefilter"));
            Ddo_funcaoapf_pf_Filtertype = cgiGet( "DDO_FUNCAOAPF_PF_Filtertype");
            Ddo_funcaoapf_pf_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_PF_Filterisrange"));
            Ddo_funcaoapf_pf_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_PF_Includedatalist"));
            Ddo_funcaoapf_pf_Cleanfilter = cgiGet( "DDO_FUNCAOAPF_PF_Cleanfilter");
            Ddo_funcaoapf_pf_Rangefilterfrom = cgiGet( "DDO_FUNCAOAPF_PF_Rangefilterfrom");
            Ddo_funcaoapf_pf_Rangefilterto = cgiGet( "DDO_FUNCAOAPF_PF_Rangefilterto");
            Ddo_funcaoapf_pf_Searchbuttontext = cgiGet( "DDO_FUNCAOAPF_PF_Searchbuttontext");
            Ddo_funcaoapf_funapfpainom_Caption = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Caption");
            Ddo_funcaoapf_funapfpainom_Tooltip = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Tooltip");
            Ddo_funcaoapf_funapfpainom_Cls = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Cls");
            Ddo_funcaoapf_funapfpainom_Filteredtext_set = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Filteredtext_set");
            Ddo_funcaoapf_funapfpainom_Selectedvalue_set = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Selectedvalue_set");
            Ddo_funcaoapf_funapfpainom_Dropdownoptionstype = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Dropdownoptionstype");
            Ddo_funcaoapf_funapfpainom_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Titlecontrolidtoreplace");
            Ddo_funcaoapf_funapfpainom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Includesortasc"));
            Ddo_funcaoapf_funapfpainom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Includesortdsc"));
            Ddo_funcaoapf_funapfpainom_Sortedstatus = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Sortedstatus");
            Ddo_funcaoapf_funapfpainom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Includefilter"));
            Ddo_funcaoapf_funapfpainom_Filtertype = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Filtertype");
            Ddo_funcaoapf_funapfpainom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Filterisrange"));
            Ddo_funcaoapf_funapfpainom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Includedatalist"));
            Ddo_funcaoapf_funapfpainom_Datalisttype = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Datalisttype");
            Ddo_funcaoapf_funapfpainom_Datalistproc = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Datalistproc");
            Ddo_funcaoapf_funapfpainom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaoapf_funapfpainom_Sortasc = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Sortasc");
            Ddo_funcaoapf_funapfpainom_Sortdsc = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Sortdsc");
            Ddo_funcaoapf_funapfpainom_Loadingdata = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Loadingdata");
            Ddo_funcaoapf_funapfpainom_Cleanfilter = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Cleanfilter");
            Ddo_funcaoapf_funapfpainom_Noresultsfound = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Noresultsfound");
            Ddo_funcaoapf_funapfpainom_Searchbuttontext = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Searchbuttontext");
            Ddo_funcaoapf_ativo_Caption = cgiGet( "DDO_FUNCAOAPF_ATIVO_Caption");
            Ddo_funcaoapf_ativo_Tooltip = cgiGet( "DDO_FUNCAOAPF_ATIVO_Tooltip");
            Ddo_funcaoapf_ativo_Cls = cgiGet( "DDO_FUNCAOAPF_ATIVO_Cls");
            Ddo_funcaoapf_ativo_Selectedvalue_set = cgiGet( "DDO_FUNCAOAPF_ATIVO_Selectedvalue_set");
            Ddo_funcaoapf_ativo_Dropdownoptionstype = cgiGet( "DDO_FUNCAOAPF_ATIVO_Dropdownoptionstype");
            Ddo_funcaoapf_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAOAPF_ATIVO_Titlecontrolidtoreplace");
            Ddo_funcaoapf_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_ATIVO_Includesortasc"));
            Ddo_funcaoapf_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_ATIVO_Includesortdsc"));
            Ddo_funcaoapf_ativo_Sortedstatus = cgiGet( "DDO_FUNCAOAPF_ATIVO_Sortedstatus");
            Ddo_funcaoapf_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_ATIVO_Includefilter"));
            Ddo_funcaoapf_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_ATIVO_Includedatalist"));
            Ddo_funcaoapf_ativo_Datalisttype = cgiGet( "DDO_FUNCAOAPF_ATIVO_Datalisttype");
            Ddo_funcaoapf_ativo_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_ATIVO_Allowmultipleselection"));
            Ddo_funcaoapf_ativo_Datalistfixedvalues = cgiGet( "DDO_FUNCAOAPF_ATIVO_Datalistfixedvalues");
            Ddo_funcaoapf_ativo_Sortasc = cgiGet( "DDO_FUNCAOAPF_ATIVO_Sortasc");
            Ddo_funcaoapf_ativo_Sortdsc = cgiGet( "DDO_FUNCAOAPF_ATIVO_Sortdsc");
            Ddo_funcaoapf_ativo_Cleanfilter = cgiGet( "DDO_FUNCAOAPF_ATIVO_Cleanfilter");
            Ddo_funcaoapf_ativo_Searchbuttontext = cgiGet( "DDO_FUNCAOAPF_ATIVO_Searchbuttontext");
            Ddo_funcaoapf_nome_Activeeventkey = cgiGet( "DDO_FUNCAOAPF_NOME_Activeeventkey");
            Ddo_funcaoapf_nome_Filteredtext_get = cgiGet( "DDO_FUNCAOAPF_NOME_Filteredtext_get");
            Ddo_funcaoapf_nome_Selectedvalue_get = cgiGet( "DDO_FUNCAOAPF_NOME_Selectedvalue_get");
            Ddo_funcaoapf_tipo_Activeeventkey = cgiGet( "DDO_FUNCAOAPF_TIPO_Activeeventkey");
            Ddo_funcaoapf_tipo_Selectedvalue_get = cgiGet( "DDO_FUNCAOAPF_TIPO_Selectedvalue_get");
            Ddo_funcaoapf_td_Activeeventkey = cgiGet( "DDO_FUNCAOAPF_TD_Activeeventkey");
            Ddo_funcaoapf_td_Filteredtext_get = cgiGet( "DDO_FUNCAOAPF_TD_Filteredtext_get");
            Ddo_funcaoapf_td_Filteredtextto_get = cgiGet( "DDO_FUNCAOAPF_TD_Filteredtextto_get");
            Ddo_funcaoapf_ar_Activeeventkey = cgiGet( "DDO_FUNCAOAPF_AR_Activeeventkey");
            Ddo_funcaoapf_ar_Filteredtext_get = cgiGet( "DDO_FUNCAOAPF_AR_Filteredtext_get");
            Ddo_funcaoapf_ar_Filteredtextto_get = cgiGet( "DDO_FUNCAOAPF_AR_Filteredtextto_get");
            Ddo_funcaoapf_complexidade_Activeeventkey = cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Activeeventkey");
            Ddo_funcaoapf_complexidade_Selectedvalue_get = cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Selectedvalue_get");
            Ddo_funcaoapf_pf_Activeeventkey = cgiGet( "DDO_FUNCAOAPF_PF_Activeeventkey");
            Ddo_funcaoapf_pf_Filteredtext_get = cgiGet( "DDO_FUNCAOAPF_PF_Filteredtext_get");
            Ddo_funcaoapf_pf_Filteredtextto_get = cgiGet( "DDO_FUNCAOAPF_PF_Filteredtextto_get");
            Ddo_funcaoapf_funapfpainom_Activeeventkey = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Activeeventkey");
            Ddo_funcaoapf_funapfpainom_Filteredtext_get = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Filteredtext_get");
            Ddo_funcaoapf_funapfpainom_Selectedvalue_get = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Selectedvalue_get");
            Ddo_funcaoapf_ativo_Activeeventkey = cgiGet( "DDO_FUNCAOAPF_ATIVO_Activeeventkey");
            Ddo_funcaoapf_ativo_Selectedvalue_get = cgiGet( "DDO_FUNCAOAPF_ATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_NOME1"), AV17FuncaoAPF_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_TIPO1"), AV76FuncaoAPF_Tipo1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_ATIVO1"), AV55FuncaoAPF_Ativo1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_NOME2"), AV21FuncaoAPF_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_TIPO2"), AV78FuncaoAPF_Tipo2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_ATIVO2"), AV56FuncaoAPF_Ativo2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_NOME3"), AV25FuncaoAPF_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_TIPO3"), AV80FuncaoAPF_Tipo3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_ATIVO3"), AV82FuncaoAPF_Ativo3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR4"), AV84DynamicFiltersSelector4) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_NOME4"), AV85FuncaoAPF_Nome4) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_TIPO4"), AV86FuncaoAPF_Tipo4) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_ATIVO4"), AV88FuncaoAPF_Ativo4) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED4")) != AV83DynamicFiltersEnabled4 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPF_NOME"), AV92TFFuncaoAPF_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPF_NOME_SEL"), AV93TFFuncaoAPF_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPF_FUNAPFPAINOM"), AV116TFFuncaoAPF_FunAPFPaiNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPF_FUNAPFPAINOM_SEL"), AV117TFFuncaoAPF_FunAPFPaiNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E333Z2 */
         E333Z2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E333Z2( )
      {
         /* Start Routine */
         subGrid_Rows = 0;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         chkavDynamicfiltersenabled4.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled4_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled4.Visible), 5, 0)));
         AV76FuncaoAPF_Tipo1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76FuncaoAPF_Tipo1", AV76FuncaoAPF_Tipo1);
         AV77FuncaoAPF_Complexidade1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77FuncaoAPF_Complexidade1", AV77FuncaoAPF_Complexidade1);
         AV55FuncaoAPF_Ativo1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55FuncaoAPF_Ativo1", AV55FuncaoAPF_Ativo1);
         AV15DynamicFiltersSelector1 = "FUNCAOAPF_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV78FuncaoAPF_Tipo2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78FuncaoAPF_Tipo2", AV78FuncaoAPF_Tipo2);
         AV79FuncaoAPF_Complexidade2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79FuncaoAPF_Complexidade2", AV79FuncaoAPF_Complexidade2);
         AV56FuncaoAPF_Ativo2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56FuncaoAPF_Ativo2", AV56FuncaoAPF_Ativo2);
         AV19DynamicFiltersSelector2 = "FUNCAOAPF_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV80FuncaoAPF_Tipo3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80FuncaoAPF_Tipo3", AV80FuncaoAPF_Tipo3);
         AV81FuncaoAPF_Complexidade3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81FuncaoAPF_Complexidade3", AV81FuncaoAPF_Complexidade3);
         AV82FuncaoAPF_Ativo3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82FuncaoAPF_Ativo3", AV82FuncaoAPF_Ativo3);
         AV23DynamicFiltersSelector3 = "FUNCAOAPF_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV86FuncaoAPF_Tipo4 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86FuncaoAPF_Tipo4", AV86FuncaoAPF_Tipo4);
         AV87FuncaoAPF_Complexidade4 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87FuncaoAPF_Complexidade4", AV87FuncaoAPF_Complexidade4);
         AV88FuncaoAPF_Ativo4 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88FuncaoAPF_Ativo4", AV88FuncaoAPF_Ativo4);
         AV84DynamicFiltersSelector4 = "FUNCAOAPF_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84DynamicFiltersSelector4", AV84DynamicFiltersSelector4);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS4' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(4)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(4)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgAdddynamicfilters3_Jsonclick = "WWPDynFilterShow(4)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters3_Internalname, "Jsonclick", imgAdddynamicfilters3_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(4)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         imgRemovedynamicfilters4_Jsonclick = "WWPDynFilterHideLast(4)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters4_Internalname, "Jsonclick", imgRemovedynamicfilters4_Jsonclick);
         edtavTffuncaoapf_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapf_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapf_nome_Visible), 5, 0)));
         edtavTffuncaoapf_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapf_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapf_nome_sel_Visible), 5, 0)));
         edtavTffuncaoapf_td_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapf_td_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapf_td_Visible), 5, 0)));
         edtavTffuncaoapf_td_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapf_td_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapf_td_to_Visible), 5, 0)));
         edtavTffuncaoapf_ar_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapf_ar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapf_ar_Visible), 5, 0)));
         edtavTffuncaoapf_ar_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapf_ar_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapf_ar_to_Visible), 5, 0)));
         edtavTffuncaoapf_pf_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapf_pf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapf_pf_Visible), 5, 0)));
         edtavTffuncaoapf_pf_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapf_pf_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapf_pf_to_Visible), 5, 0)));
         edtavTffuncaoapf_funapfpainom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapf_funapfpainom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapf_funapfpainom_Visible), 5, 0)));
         edtavTffuncaoapf_funapfpainom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapf_funapfpainom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapf_funapfpainom_sel_Visible), 5, 0)));
         Ddo_funcaoapf_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPF_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_nome_Internalname, "TitleControlIdToReplace", Ddo_funcaoapf_nome_Titlecontrolidtoreplace);
         AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace = Ddo_funcaoapf_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace", AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace);
         edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaoapf_tipo_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPF_Tipo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_tipo_Internalname, "TitleControlIdToReplace", Ddo_funcaoapf_tipo_Titlecontrolidtoreplace);
         AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace = Ddo_funcaoapf_tipo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace", AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace);
         edtavDdo_funcaoapf_tipotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaoapf_tipotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapf_tipotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaoapf_td_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPF_TD";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_td_Internalname, "TitleControlIdToReplace", Ddo_funcaoapf_td_Titlecontrolidtoreplace);
         AV102ddo_FuncaoAPF_TDTitleControlIdToReplace = Ddo_funcaoapf_td_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102ddo_FuncaoAPF_TDTitleControlIdToReplace", AV102ddo_FuncaoAPF_TDTitleControlIdToReplace);
         edtavDdo_funcaoapf_tdtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaoapf_tdtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapf_tdtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaoapf_ar_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPF_AR";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_ar_Internalname, "TitleControlIdToReplace", Ddo_funcaoapf_ar_Titlecontrolidtoreplace);
         AV106ddo_FuncaoAPF_ARTitleControlIdToReplace = Ddo_funcaoapf_ar_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV106ddo_FuncaoAPF_ARTitleControlIdToReplace", AV106ddo_FuncaoAPF_ARTitleControlIdToReplace);
         edtavDdo_funcaoapf_artitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaoapf_artitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapf_artitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaoapf_complexidade_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPF_Complexidade";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_complexidade_Internalname, "TitleControlIdToReplace", Ddo_funcaoapf_complexidade_Titlecontrolidtoreplace);
         AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace = Ddo_funcaoapf_complexidade_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace", AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace);
         edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaoapf_pf_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPF_PF";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_pf_Internalname, "TitleControlIdToReplace", Ddo_funcaoapf_pf_Titlecontrolidtoreplace);
         AV114ddo_FuncaoAPF_PFTitleControlIdToReplace = Ddo_funcaoapf_pf_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV114ddo_FuncaoAPF_PFTitleControlIdToReplace", AV114ddo_FuncaoAPF_PFTitleControlIdToReplace);
         edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaoapf_funapfpainom_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPF_FunAPFPaiNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_funapfpainom_Internalname, "TitleControlIdToReplace", Ddo_funcaoapf_funapfpainom_Titlecontrolidtoreplace);
         AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace = Ddo_funcaoapf_funapfpainom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace", AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace);
         edtavDdo_funcaoapf_funapfpainomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaoapf_funapfpainomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapf_funapfpainomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaoapf_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPF_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_ativo_Internalname, "TitleControlIdToReplace", Ddo_funcaoapf_ativo_Titlecontrolidtoreplace);
         AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace = Ddo_funcaoapf_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace", AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace);
         edtavDdo_funcaoapf_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaoapf_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapf_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Fun��es de Transa��";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome", 0);
         cmbavOrderedby.addItem("2", "Tipo", 0);
         cmbavOrderedby.addItem("3", "Vinculada com", 0);
         cmbavOrderedby.addItem("4", "Status da Fun��o", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(4)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons4 = AV123DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons4) ;
         AV123DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons4;
      }

      protected void E343Z2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV91FuncaoAPF_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV95FuncaoAPF_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV99FuncaoAPF_TDTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV103FuncaoAPF_ARTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV107FuncaoAPF_ComplexidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV111FuncaoAPF_PFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV115FuncaoAPF_FunAPFPaiNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV119FuncaoAPF_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtFuncaoAPF_Nome_Titleformat = 2;
         edtFuncaoAPF_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_Nome_Internalname, "Title", edtFuncaoAPF_Nome_Title);
         cmbFuncaoAPF_Tipo_Titleformat = 2;
         cmbFuncaoAPF_Tipo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tipo", AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPF_Tipo_Internalname, "Title", cmbFuncaoAPF_Tipo.Title.Text);
         edtFuncaoAPF_TD_Titleformat = 2;
         edtFuncaoAPF_TD_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "DER", AV102ddo_FuncaoAPF_TDTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_TD_Internalname, "Title", edtFuncaoAPF_TD_Title);
         edtFuncaoAPF_AR_Titleformat = 2;
         edtFuncaoAPF_AR_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "ALR", AV106ddo_FuncaoAPF_ARTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_AR_Internalname, "Title", edtFuncaoAPF_AR_Title);
         cmbFuncaoAPF_Complexidade_Titleformat = 2;
         cmbFuncaoAPF_Complexidade.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Complexidade", AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPF_Complexidade_Internalname, "Title", cmbFuncaoAPF_Complexidade.Title.Text);
         edtFuncaoAPF_PF_Titleformat = 2;
         edtFuncaoAPF_PF_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "PF", AV114ddo_FuncaoAPF_PFTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_PF_Internalname, "Title", edtFuncaoAPF_PF_Title);
         edtFuncaoAPF_FunAPFPaiNom_Titleformat = 2;
         edtFuncaoAPF_FunAPFPaiNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Vinculada com", AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_FunAPFPaiNom_Internalname, "Title", edtFuncaoAPF_FunAPFPaiNom_Title);
         cmbFuncaoAPF_Ativo_Titleformat = 2;
         cmbFuncaoAPF_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Status da Fun��o", AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPF_Ativo_Internalname, "Title", cmbFuncaoAPF_Ativo.Title.Text);
         AV126WWFuncaoAPFDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV127WWFuncaoAPFDS_2_Funcaoapf_nome1 = AV17FuncaoAPF_Nome1;
         AV128WWFuncaoAPFDS_3_Funcaoapf_tipo1 = AV76FuncaoAPF_Tipo1;
         AV129WWFuncaoAPFDS_4_Funcaoapf_complexidade1 = AV77FuncaoAPF_Complexidade1;
         AV130WWFuncaoAPFDS_5_Funcaoapf_ativo1 = AV55FuncaoAPF_Ativo1;
         AV131WWFuncaoAPFDS_6_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV132WWFuncaoAPFDS_7_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV133WWFuncaoAPFDS_8_Funcaoapf_nome2 = AV21FuncaoAPF_Nome2;
         AV134WWFuncaoAPFDS_9_Funcaoapf_tipo2 = AV78FuncaoAPF_Tipo2;
         AV135WWFuncaoAPFDS_10_Funcaoapf_complexidade2 = AV79FuncaoAPF_Complexidade2;
         AV136WWFuncaoAPFDS_11_Funcaoapf_ativo2 = AV56FuncaoAPF_Ativo2;
         AV137WWFuncaoAPFDS_12_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV138WWFuncaoAPFDS_13_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV139WWFuncaoAPFDS_14_Funcaoapf_nome3 = AV25FuncaoAPF_Nome3;
         AV140WWFuncaoAPFDS_15_Funcaoapf_tipo3 = AV80FuncaoAPF_Tipo3;
         AV141WWFuncaoAPFDS_16_Funcaoapf_complexidade3 = AV81FuncaoAPF_Complexidade3;
         AV142WWFuncaoAPFDS_17_Funcaoapf_ativo3 = AV82FuncaoAPF_Ativo3;
         AV143WWFuncaoAPFDS_18_Dynamicfiltersenabled4 = AV83DynamicFiltersEnabled4;
         AV144WWFuncaoAPFDS_19_Dynamicfiltersselector4 = AV84DynamicFiltersSelector4;
         AV145WWFuncaoAPFDS_20_Funcaoapf_nome4 = AV85FuncaoAPF_Nome4;
         AV146WWFuncaoAPFDS_21_Funcaoapf_tipo4 = AV86FuncaoAPF_Tipo4;
         AV147WWFuncaoAPFDS_22_Funcaoapf_complexidade4 = AV87FuncaoAPF_Complexidade4;
         AV148WWFuncaoAPFDS_23_Funcaoapf_ativo4 = AV88FuncaoAPF_Ativo4;
         AV149WWFuncaoAPFDS_24_Tffuncaoapf_nome = AV92TFFuncaoAPF_Nome;
         AV150WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel = AV93TFFuncaoAPF_Nome_Sel;
         AV151WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels = AV97TFFuncaoAPF_Tipo_Sels;
         AV152WWFuncaoAPFDS_27_Tffuncaoapf_td = AV100TFFuncaoAPF_TD;
         AV153WWFuncaoAPFDS_28_Tffuncaoapf_td_to = AV101TFFuncaoAPF_TD_To;
         AV154WWFuncaoAPFDS_29_Tffuncaoapf_ar = AV104TFFuncaoAPF_AR;
         AV155WWFuncaoAPFDS_30_Tffuncaoapf_ar_to = AV105TFFuncaoAPF_AR_To;
         AV156WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels = AV109TFFuncaoAPF_Complexidade_Sels;
         AV157WWFuncaoAPFDS_32_Tffuncaoapf_pf = AV112TFFuncaoAPF_PF;
         AV158WWFuncaoAPFDS_33_Tffuncaoapf_pf_to = AV113TFFuncaoAPF_PF_To;
         AV159WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom = AV116TFFuncaoAPF_FunAPFPaiNom;
         AV160WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel = AV117TFFuncaoAPF_FunAPFPaiNom_Sel;
         AV161WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels = AV121TFFuncaoAPF_Ativo_Sels;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV91FuncaoAPF_NomeTitleFilterData", AV91FuncaoAPF_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV95FuncaoAPF_TipoTitleFilterData", AV95FuncaoAPF_TipoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV99FuncaoAPF_TDTitleFilterData", AV99FuncaoAPF_TDTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV103FuncaoAPF_ARTitleFilterData", AV103FuncaoAPF_ARTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV107FuncaoAPF_ComplexidadeTitleFilterData", AV107FuncaoAPF_ComplexidadeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV111FuncaoAPF_PFTitleFilterData", AV111FuncaoAPF_PFTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV115FuncaoAPF_FunAPFPaiNomTitleFilterData", AV115FuncaoAPF_FunAPFPaiNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV119FuncaoAPF_AtivoTitleFilterData", AV119FuncaoAPF_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E113Z2( )
      {
         /* Ddo_funcaoapf_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapf_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaoapf_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_nome_Internalname, "SortedStatus", Ddo_funcaoapf_nome_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapf_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaoapf_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_nome_Internalname, "SortedStatus", Ddo_funcaoapf_nome_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapf_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV92TFFuncaoAPF_Nome = Ddo_funcaoapf_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92TFFuncaoAPF_Nome", AV92TFFuncaoAPF_Nome);
            AV93TFFuncaoAPF_Nome_Sel = Ddo_funcaoapf_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93TFFuncaoAPF_Nome_Sel", AV93TFFuncaoAPF_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E123Z2( )
      {
         /* Ddo_funcaoapf_tipo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapf_tipo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaoapf_tipo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_tipo_Internalname, "SortedStatus", Ddo_funcaoapf_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapf_tipo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaoapf_tipo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_tipo_Internalname, "SortedStatus", Ddo_funcaoapf_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapf_tipo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV96TFFuncaoAPF_Tipo_SelsJson = Ddo_funcaoapf_tipo_Selectedvalue_get;
            AV97TFFuncaoAPF_Tipo_Sels.FromJSonString(AV96TFFuncaoAPF_Tipo_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV97TFFuncaoAPF_Tipo_Sels", AV97TFFuncaoAPF_Tipo_Sels);
      }

      protected void E133Z2( )
      {
         /* Ddo_funcaoapf_td_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapf_td_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV100TFFuncaoAPF_TD = (short)(NumberUtil.Val( Ddo_funcaoapf_td_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100TFFuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(AV100TFFuncaoAPF_TD), 4, 0)));
            AV101TFFuncaoAPF_TD_To = (short)(NumberUtil.Val( Ddo_funcaoapf_td_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101TFFuncaoAPF_TD_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV101TFFuncaoAPF_TD_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E143Z2( )
      {
         /* Ddo_funcaoapf_ar_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapf_ar_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV104TFFuncaoAPF_AR = (short)(NumberUtil.Val( Ddo_funcaoapf_ar_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104TFFuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV104TFFuncaoAPF_AR), 4, 0)));
            AV105TFFuncaoAPF_AR_To = (short)(NumberUtil.Val( Ddo_funcaoapf_ar_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFFuncaoAPF_AR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105TFFuncaoAPF_AR_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E153Z2( )
      {
         /* Ddo_funcaoapf_complexidade_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapf_complexidade_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV108TFFuncaoAPF_Complexidade_SelsJson = Ddo_funcaoapf_complexidade_Selectedvalue_get;
            AV109TFFuncaoAPF_Complexidade_Sels.FromJSonString(AV108TFFuncaoAPF_Complexidade_SelsJson);
            subgrid_firstpage( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV109TFFuncaoAPF_Complexidade_Sels", AV109TFFuncaoAPF_Complexidade_Sels);
      }

      protected void E163Z2( )
      {
         /* Ddo_funcaoapf_pf_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapf_pf_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV112TFFuncaoAPF_PF = NumberUtil.Val( Ddo_funcaoapf_pf_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV112TFFuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( AV112TFFuncaoAPF_PF, 14, 5)));
            AV113TFFuncaoAPF_PF_To = NumberUtil.Val( Ddo_funcaoapf_pf_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV113TFFuncaoAPF_PF_To", StringUtil.LTrim( StringUtil.Str( AV113TFFuncaoAPF_PF_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E173Z2( )
      {
         /* Ddo_funcaoapf_funapfpainom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapf_funapfpainom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaoapf_funapfpainom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_funapfpainom_Internalname, "SortedStatus", Ddo_funcaoapf_funapfpainom_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapf_funapfpainom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaoapf_funapfpainom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_funapfpainom_Internalname, "SortedStatus", Ddo_funcaoapf_funapfpainom_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapf_funapfpainom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV116TFFuncaoAPF_FunAPFPaiNom = Ddo_funcaoapf_funapfpainom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV116TFFuncaoAPF_FunAPFPaiNom", AV116TFFuncaoAPF_FunAPFPaiNom);
            AV117TFFuncaoAPF_FunAPFPaiNom_Sel = Ddo_funcaoapf_funapfpainom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117TFFuncaoAPF_FunAPFPaiNom_Sel", AV117TFFuncaoAPF_FunAPFPaiNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E183Z2( )
      {
         /* Ddo_funcaoapf_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapf_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaoapf_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_ativo_Internalname, "SortedStatus", Ddo_funcaoapf_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapf_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaoapf_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_ativo_Internalname, "SortedStatus", Ddo_funcaoapf_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapf_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV120TFFuncaoAPF_Ativo_SelsJson = Ddo_funcaoapf_ativo_Selectedvalue_get;
            AV121TFFuncaoAPF_Ativo_Sels.FromJSonString(AV120TFFuncaoAPF_Ativo_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV121TFFuncaoAPF_Ativo_Sels", AV121TFFuncaoAPF_Ativo_Sels);
      }

      private void E353Z2( )
      {
         /* Grid_Load Routine */
         AV28Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
         AV162Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("funcaoapf.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A165FuncaoAPF_Codigo) + "," + UrlEncode("" +AV57FuncaoAPF_SistemaCod);
         AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
         AV163Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("funcaoapf.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A165FuncaoAPF_Codigo) + "," + UrlEncode("" +AV57FuncaoAPF_SistemaCod);
         AV64Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV64Display);
         AV164Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = formatLink("viewfuncaoapf.aspx") + "?" + UrlEncode("" +A165FuncaoAPF_Codigo) + "," + UrlEncode("" +AV57FuncaoAPF_SistemaCod) + "," + UrlEncode(StringUtil.RTrim(""));
         edtFuncaoAPF_Nome_Link = formatLink("viewfuncaoapf.aspx") + "?" + UrlEncode("" +A165FuncaoAPF_Codigo) + "," + UrlEncode("" +AV57FuncaoAPF_SistemaCod) + "," + UrlEncode(StringUtil.RTrim(""));
         edtFuncaoAPF_FunAPFPaiNom_Link = formatLink("viewfuncaoapf.aspx") + "?" + UrlEncode("" +A358FuncaoAPF_FunAPFPaiCod) + "," + UrlEncode("" +AV57FuncaoAPF_SistemaCod) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 96;
         }
         if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
         {
            sendrow_962( ) ;
            GRID_nEOF = 1;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
            {
               GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
            }
         }
         if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
         {
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
         }
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_96_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(96, GridRow);
         }
      }

      protected void E193Z2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E263Z2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E203Z2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17FuncaoAPF_Nome1, AV76FuncaoAPF_Tipo1, AV55FuncaoAPF_Ativo1, AV19DynamicFiltersSelector2, AV21FuncaoAPF_Nome2, AV78FuncaoAPF_Tipo2, AV56FuncaoAPF_Ativo2, AV23DynamicFiltersSelector3, AV25FuncaoAPF_Nome3, AV80FuncaoAPF_Tipo3, AV82FuncaoAPF_Ativo3, AV84DynamicFiltersSelector4, AV85FuncaoAPF_Nome4, AV86FuncaoAPF_Tipo4, AV88FuncaoAPF_Ativo4, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV83DynamicFiltersEnabled4, AV92TFFuncaoAPF_Nome, AV93TFFuncaoAPF_Nome_Sel, AV116TFFuncaoAPF_FunAPFPaiNom, AV117TFFuncaoAPF_FunAPFPaiNom_Sel, AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace, AV102ddo_FuncaoAPF_TDTitleControlIdToReplace, AV106ddo_FuncaoAPF_ARTitleControlIdToReplace, AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, AV114ddo_FuncaoAPF_PFTitleControlIdToReplace, AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace, AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace, AV77FuncaoAPF_Complexidade1, AV79FuncaoAPF_Complexidade2, AV81FuncaoAPF_Complexidade3, AV87FuncaoAPF_Complexidade4, AV97TFFuncaoAPF_Tipo_Sels, AV100TFFuncaoAPF_TD, AV101TFFuncaoAPF_TD_To, AV104TFFuncaoAPF_AR, AV105TFFuncaoAPF_AR_To, AV109TFFuncaoAPF_Complexidade_Sels, AV112TFFuncaoAPF_PF, AV113TFFuncaoAPF_PF_To, AV121TFFuncaoAPF_Ativo_Sels, AV165Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A165FuncaoAPF_Codigo, AV57FuncaoAPF_SistemaCod, A358FuncaoAPF_FunAPFPaiCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector4.CurrentValue = StringUtil.RTrim( AV84DynamicFiltersSelector4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector4_Internalname, "Values", cmbavDynamicfiltersselector4.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavFuncaoapf_tipo1.CurrentValue = StringUtil.RTrim( AV76FuncaoAPF_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo1_Internalname, "Values", cmbavFuncaoapf_tipo1.ToJavascriptSource());
         cmbavFuncaoapf_complexidade1.CurrentValue = StringUtil.RTrim( AV77FuncaoAPF_Complexidade1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade1_Internalname, "Values", cmbavFuncaoapf_complexidade1.ToJavascriptSource());
         cmbavFuncaoapf_ativo1.CurrentValue = StringUtil.RTrim( AV55FuncaoAPF_Ativo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo1_Internalname, "Values", cmbavFuncaoapf_ativo1.ToJavascriptSource());
         cmbavFuncaoapf_tipo2.CurrentValue = StringUtil.RTrim( AV78FuncaoAPF_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo2_Internalname, "Values", cmbavFuncaoapf_tipo2.ToJavascriptSource());
         cmbavFuncaoapf_complexidade2.CurrentValue = StringUtil.RTrim( AV79FuncaoAPF_Complexidade2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade2_Internalname, "Values", cmbavFuncaoapf_complexidade2.ToJavascriptSource());
         cmbavFuncaoapf_ativo2.CurrentValue = StringUtil.RTrim( AV56FuncaoAPF_Ativo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo2_Internalname, "Values", cmbavFuncaoapf_ativo2.ToJavascriptSource());
         cmbavFuncaoapf_tipo3.CurrentValue = StringUtil.RTrim( AV80FuncaoAPF_Tipo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo3_Internalname, "Values", cmbavFuncaoapf_tipo3.ToJavascriptSource());
         cmbavFuncaoapf_complexidade3.CurrentValue = StringUtil.RTrim( AV81FuncaoAPF_Complexidade3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade3_Internalname, "Values", cmbavFuncaoapf_complexidade3.ToJavascriptSource());
         cmbavFuncaoapf_ativo3.CurrentValue = StringUtil.RTrim( AV82FuncaoAPF_Ativo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo3_Internalname, "Values", cmbavFuncaoapf_ativo3.ToJavascriptSource());
         cmbavFuncaoapf_tipo4.CurrentValue = StringUtil.RTrim( AV86FuncaoAPF_Tipo4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo4_Internalname, "Values", cmbavFuncaoapf_tipo4.ToJavascriptSource());
         cmbavFuncaoapf_complexidade4.CurrentValue = StringUtil.RTrim( AV87FuncaoAPF_Complexidade4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade4_Internalname, "Values", cmbavFuncaoapf_complexidade4.ToJavascriptSource());
         cmbavFuncaoapf_ativo4.CurrentValue = StringUtil.RTrim( AV88FuncaoAPF_Ativo4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo4_Internalname, "Values", cmbavFuncaoapf_ativo4.ToJavascriptSource());
      }

      protected void E273Z2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E283Z2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E213Z2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17FuncaoAPF_Nome1, AV76FuncaoAPF_Tipo1, AV55FuncaoAPF_Ativo1, AV19DynamicFiltersSelector2, AV21FuncaoAPF_Nome2, AV78FuncaoAPF_Tipo2, AV56FuncaoAPF_Ativo2, AV23DynamicFiltersSelector3, AV25FuncaoAPF_Nome3, AV80FuncaoAPF_Tipo3, AV82FuncaoAPF_Ativo3, AV84DynamicFiltersSelector4, AV85FuncaoAPF_Nome4, AV86FuncaoAPF_Tipo4, AV88FuncaoAPF_Ativo4, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV83DynamicFiltersEnabled4, AV92TFFuncaoAPF_Nome, AV93TFFuncaoAPF_Nome_Sel, AV116TFFuncaoAPF_FunAPFPaiNom, AV117TFFuncaoAPF_FunAPFPaiNom_Sel, AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace, AV102ddo_FuncaoAPF_TDTitleControlIdToReplace, AV106ddo_FuncaoAPF_ARTitleControlIdToReplace, AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, AV114ddo_FuncaoAPF_PFTitleControlIdToReplace, AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace, AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace, AV77FuncaoAPF_Complexidade1, AV79FuncaoAPF_Complexidade2, AV81FuncaoAPF_Complexidade3, AV87FuncaoAPF_Complexidade4, AV97TFFuncaoAPF_Tipo_Sels, AV100TFFuncaoAPF_TD, AV101TFFuncaoAPF_TD_To, AV104TFFuncaoAPF_AR, AV105TFFuncaoAPF_AR_To, AV109TFFuncaoAPF_Complexidade_Sels, AV112TFFuncaoAPF_PF, AV113TFFuncaoAPF_PF_To, AV121TFFuncaoAPF_Ativo_Sels, AV165Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A165FuncaoAPF_Codigo, AV57FuncaoAPF_SistemaCod, A358FuncaoAPF_FunAPFPaiCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector4.CurrentValue = StringUtil.RTrim( AV84DynamicFiltersSelector4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector4_Internalname, "Values", cmbavDynamicfiltersselector4.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavFuncaoapf_tipo1.CurrentValue = StringUtil.RTrim( AV76FuncaoAPF_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo1_Internalname, "Values", cmbavFuncaoapf_tipo1.ToJavascriptSource());
         cmbavFuncaoapf_complexidade1.CurrentValue = StringUtil.RTrim( AV77FuncaoAPF_Complexidade1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade1_Internalname, "Values", cmbavFuncaoapf_complexidade1.ToJavascriptSource());
         cmbavFuncaoapf_ativo1.CurrentValue = StringUtil.RTrim( AV55FuncaoAPF_Ativo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo1_Internalname, "Values", cmbavFuncaoapf_ativo1.ToJavascriptSource());
         cmbavFuncaoapf_tipo2.CurrentValue = StringUtil.RTrim( AV78FuncaoAPF_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo2_Internalname, "Values", cmbavFuncaoapf_tipo2.ToJavascriptSource());
         cmbavFuncaoapf_complexidade2.CurrentValue = StringUtil.RTrim( AV79FuncaoAPF_Complexidade2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade2_Internalname, "Values", cmbavFuncaoapf_complexidade2.ToJavascriptSource());
         cmbavFuncaoapf_ativo2.CurrentValue = StringUtil.RTrim( AV56FuncaoAPF_Ativo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo2_Internalname, "Values", cmbavFuncaoapf_ativo2.ToJavascriptSource());
         cmbavFuncaoapf_tipo3.CurrentValue = StringUtil.RTrim( AV80FuncaoAPF_Tipo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo3_Internalname, "Values", cmbavFuncaoapf_tipo3.ToJavascriptSource());
         cmbavFuncaoapf_complexidade3.CurrentValue = StringUtil.RTrim( AV81FuncaoAPF_Complexidade3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade3_Internalname, "Values", cmbavFuncaoapf_complexidade3.ToJavascriptSource());
         cmbavFuncaoapf_ativo3.CurrentValue = StringUtil.RTrim( AV82FuncaoAPF_Ativo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo3_Internalname, "Values", cmbavFuncaoapf_ativo3.ToJavascriptSource());
         cmbavFuncaoapf_tipo4.CurrentValue = StringUtil.RTrim( AV86FuncaoAPF_Tipo4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo4_Internalname, "Values", cmbavFuncaoapf_tipo4.ToJavascriptSource());
         cmbavFuncaoapf_complexidade4.CurrentValue = StringUtil.RTrim( AV87FuncaoAPF_Complexidade4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade4_Internalname, "Values", cmbavFuncaoapf_complexidade4.ToJavascriptSource());
         cmbavFuncaoapf_ativo4.CurrentValue = StringUtil.RTrim( AV88FuncaoAPF_Ativo4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo4_Internalname, "Values", cmbavFuncaoapf_ativo4.ToJavascriptSource());
      }

      protected void E293Z2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E303Z2( )
      {
         /* 'AddDynamicFilters3' Routine */
         AV83DynamicFiltersEnabled4 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83DynamicFiltersEnabled4", AV83DynamicFiltersEnabled4);
         imgAdddynamicfilters3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters3_Visible), 5, 0)));
         imgRemovedynamicfilters3_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters3_Visible), 5, 0)));
      }

      protected void E223Z2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17FuncaoAPF_Nome1, AV76FuncaoAPF_Tipo1, AV55FuncaoAPF_Ativo1, AV19DynamicFiltersSelector2, AV21FuncaoAPF_Nome2, AV78FuncaoAPF_Tipo2, AV56FuncaoAPF_Ativo2, AV23DynamicFiltersSelector3, AV25FuncaoAPF_Nome3, AV80FuncaoAPF_Tipo3, AV82FuncaoAPF_Ativo3, AV84DynamicFiltersSelector4, AV85FuncaoAPF_Nome4, AV86FuncaoAPF_Tipo4, AV88FuncaoAPF_Ativo4, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV83DynamicFiltersEnabled4, AV92TFFuncaoAPF_Nome, AV93TFFuncaoAPF_Nome_Sel, AV116TFFuncaoAPF_FunAPFPaiNom, AV117TFFuncaoAPF_FunAPFPaiNom_Sel, AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace, AV102ddo_FuncaoAPF_TDTitleControlIdToReplace, AV106ddo_FuncaoAPF_ARTitleControlIdToReplace, AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, AV114ddo_FuncaoAPF_PFTitleControlIdToReplace, AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace, AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace, AV77FuncaoAPF_Complexidade1, AV79FuncaoAPF_Complexidade2, AV81FuncaoAPF_Complexidade3, AV87FuncaoAPF_Complexidade4, AV97TFFuncaoAPF_Tipo_Sels, AV100TFFuncaoAPF_TD, AV101TFFuncaoAPF_TD_To, AV104TFFuncaoAPF_AR, AV105TFFuncaoAPF_AR_To, AV109TFFuncaoAPF_Complexidade_Sels, AV112TFFuncaoAPF_PF, AV113TFFuncaoAPF_PF_To, AV121TFFuncaoAPF_Ativo_Sels, AV165Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A165FuncaoAPF_Codigo, AV57FuncaoAPF_SistemaCod, A358FuncaoAPF_FunAPFPaiCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector4.CurrentValue = StringUtil.RTrim( AV84DynamicFiltersSelector4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector4_Internalname, "Values", cmbavDynamicfiltersselector4.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavFuncaoapf_tipo1.CurrentValue = StringUtil.RTrim( AV76FuncaoAPF_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo1_Internalname, "Values", cmbavFuncaoapf_tipo1.ToJavascriptSource());
         cmbavFuncaoapf_complexidade1.CurrentValue = StringUtil.RTrim( AV77FuncaoAPF_Complexidade1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade1_Internalname, "Values", cmbavFuncaoapf_complexidade1.ToJavascriptSource());
         cmbavFuncaoapf_ativo1.CurrentValue = StringUtil.RTrim( AV55FuncaoAPF_Ativo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo1_Internalname, "Values", cmbavFuncaoapf_ativo1.ToJavascriptSource());
         cmbavFuncaoapf_tipo2.CurrentValue = StringUtil.RTrim( AV78FuncaoAPF_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo2_Internalname, "Values", cmbavFuncaoapf_tipo2.ToJavascriptSource());
         cmbavFuncaoapf_complexidade2.CurrentValue = StringUtil.RTrim( AV79FuncaoAPF_Complexidade2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade2_Internalname, "Values", cmbavFuncaoapf_complexidade2.ToJavascriptSource());
         cmbavFuncaoapf_ativo2.CurrentValue = StringUtil.RTrim( AV56FuncaoAPF_Ativo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo2_Internalname, "Values", cmbavFuncaoapf_ativo2.ToJavascriptSource());
         cmbavFuncaoapf_tipo3.CurrentValue = StringUtil.RTrim( AV80FuncaoAPF_Tipo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo3_Internalname, "Values", cmbavFuncaoapf_tipo3.ToJavascriptSource());
         cmbavFuncaoapf_complexidade3.CurrentValue = StringUtil.RTrim( AV81FuncaoAPF_Complexidade3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade3_Internalname, "Values", cmbavFuncaoapf_complexidade3.ToJavascriptSource());
         cmbavFuncaoapf_ativo3.CurrentValue = StringUtil.RTrim( AV82FuncaoAPF_Ativo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo3_Internalname, "Values", cmbavFuncaoapf_ativo3.ToJavascriptSource());
         cmbavFuncaoapf_tipo4.CurrentValue = StringUtil.RTrim( AV86FuncaoAPF_Tipo4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo4_Internalname, "Values", cmbavFuncaoapf_tipo4.ToJavascriptSource());
         cmbavFuncaoapf_complexidade4.CurrentValue = StringUtil.RTrim( AV87FuncaoAPF_Complexidade4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade4_Internalname, "Values", cmbavFuncaoapf_complexidade4.ToJavascriptSource());
         cmbavFuncaoapf_ativo4.CurrentValue = StringUtil.RTrim( AV88FuncaoAPF_Ativo4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo4_Internalname, "Values", cmbavFuncaoapf_ativo4.ToJavascriptSource());
      }

      protected void E313Z2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E233Z2( )
      {
         /* 'RemoveDynamicFilters4' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV83DynamicFiltersEnabled4 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83DynamicFiltersEnabled4", AV83DynamicFiltersEnabled4);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17FuncaoAPF_Nome1, AV76FuncaoAPF_Tipo1, AV55FuncaoAPF_Ativo1, AV19DynamicFiltersSelector2, AV21FuncaoAPF_Nome2, AV78FuncaoAPF_Tipo2, AV56FuncaoAPF_Ativo2, AV23DynamicFiltersSelector3, AV25FuncaoAPF_Nome3, AV80FuncaoAPF_Tipo3, AV82FuncaoAPF_Ativo3, AV84DynamicFiltersSelector4, AV85FuncaoAPF_Nome4, AV86FuncaoAPF_Tipo4, AV88FuncaoAPF_Ativo4, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV83DynamicFiltersEnabled4, AV92TFFuncaoAPF_Nome, AV93TFFuncaoAPF_Nome_Sel, AV116TFFuncaoAPF_FunAPFPaiNom, AV117TFFuncaoAPF_FunAPFPaiNom_Sel, AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace, AV102ddo_FuncaoAPF_TDTitleControlIdToReplace, AV106ddo_FuncaoAPF_ARTitleControlIdToReplace, AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, AV114ddo_FuncaoAPF_PFTitleControlIdToReplace, AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace, AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace, AV77FuncaoAPF_Complexidade1, AV79FuncaoAPF_Complexidade2, AV81FuncaoAPF_Complexidade3, AV87FuncaoAPF_Complexidade4, AV97TFFuncaoAPF_Tipo_Sels, AV100TFFuncaoAPF_TD, AV101TFFuncaoAPF_TD_To, AV104TFFuncaoAPF_AR, AV105TFFuncaoAPF_AR_To, AV109TFFuncaoAPF_Complexidade_Sels, AV112TFFuncaoAPF_PF, AV113TFFuncaoAPF_PF_To, AV121TFFuncaoAPF_Ativo_Sels, AV165Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A165FuncaoAPF_Codigo, AV57FuncaoAPF_SistemaCod, A358FuncaoAPF_FunAPFPaiCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector4.CurrentValue = StringUtil.RTrim( AV84DynamicFiltersSelector4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector4_Internalname, "Values", cmbavDynamicfiltersselector4.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavFuncaoapf_tipo1.CurrentValue = StringUtil.RTrim( AV76FuncaoAPF_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo1_Internalname, "Values", cmbavFuncaoapf_tipo1.ToJavascriptSource());
         cmbavFuncaoapf_complexidade1.CurrentValue = StringUtil.RTrim( AV77FuncaoAPF_Complexidade1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade1_Internalname, "Values", cmbavFuncaoapf_complexidade1.ToJavascriptSource());
         cmbavFuncaoapf_ativo1.CurrentValue = StringUtil.RTrim( AV55FuncaoAPF_Ativo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo1_Internalname, "Values", cmbavFuncaoapf_ativo1.ToJavascriptSource());
         cmbavFuncaoapf_tipo2.CurrentValue = StringUtil.RTrim( AV78FuncaoAPF_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo2_Internalname, "Values", cmbavFuncaoapf_tipo2.ToJavascriptSource());
         cmbavFuncaoapf_complexidade2.CurrentValue = StringUtil.RTrim( AV79FuncaoAPF_Complexidade2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade2_Internalname, "Values", cmbavFuncaoapf_complexidade2.ToJavascriptSource());
         cmbavFuncaoapf_ativo2.CurrentValue = StringUtil.RTrim( AV56FuncaoAPF_Ativo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo2_Internalname, "Values", cmbavFuncaoapf_ativo2.ToJavascriptSource());
         cmbavFuncaoapf_tipo3.CurrentValue = StringUtil.RTrim( AV80FuncaoAPF_Tipo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo3_Internalname, "Values", cmbavFuncaoapf_tipo3.ToJavascriptSource());
         cmbavFuncaoapf_complexidade3.CurrentValue = StringUtil.RTrim( AV81FuncaoAPF_Complexidade3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade3_Internalname, "Values", cmbavFuncaoapf_complexidade3.ToJavascriptSource());
         cmbavFuncaoapf_ativo3.CurrentValue = StringUtil.RTrim( AV82FuncaoAPF_Ativo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo3_Internalname, "Values", cmbavFuncaoapf_ativo3.ToJavascriptSource());
         cmbavFuncaoapf_tipo4.CurrentValue = StringUtil.RTrim( AV86FuncaoAPF_Tipo4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo4_Internalname, "Values", cmbavFuncaoapf_tipo4.ToJavascriptSource());
         cmbavFuncaoapf_complexidade4.CurrentValue = StringUtil.RTrim( AV87FuncaoAPF_Complexidade4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade4_Internalname, "Values", cmbavFuncaoapf_complexidade4.ToJavascriptSource());
         cmbavFuncaoapf_ativo4.CurrentValue = StringUtil.RTrim( AV88FuncaoAPF_Ativo4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo4_Internalname, "Values", cmbavFuncaoapf_ativo4.ToJavascriptSource());
      }

      protected void E323Z2( )
      {
         /* Dynamicfiltersselector4_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS4' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E243Z2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV97TFFuncaoAPF_Tipo_Sels", AV97TFFuncaoAPF_Tipo_Sels);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV109TFFuncaoAPF_Complexidade_Sels", AV109TFFuncaoAPF_Complexidade_Sels);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV121TFFuncaoAPF_Ativo_Sels", AV121TFFuncaoAPF_Ativo_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector4.CurrentValue = StringUtil.RTrim( AV84DynamicFiltersSelector4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector4_Internalname, "Values", cmbavDynamicfiltersselector4.ToJavascriptSource());
         cmbavFuncaoapf_tipo1.CurrentValue = StringUtil.RTrim( AV76FuncaoAPF_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo1_Internalname, "Values", cmbavFuncaoapf_tipo1.ToJavascriptSource());
         cmbavFuncaoapf_complexidade1.CurrentValue = StringUtil.RTrim( AV77FuncaoAPF_Complexidade1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade1_Internalname, "Values", cmbavFuncaoapf_complexidade1.ToJavascriptSource());
         cmbavFuncaoapf_ativo1.CurrentValue = StringUtil.RTrim( AV55FuncaoAPF_Ativo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo1_Internalname, "Values", cmbavFuncaoapf_ativo1.ToJavascriptSource());
         cmbavFuncaoapf_tipo2.CurrentValue = StringUtil.RTrim( AV78FuncaoAPF_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo2_Internalname, "Values", cmbavFuncaoapf_tipo2.ToJavascriptSource());
         cmbavFuncaoapf_complexidade2.CurrentValue = StringUtil.RTrim( AV79FuncaoAPF_Complexidade2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade2_Internalname, "Values", cmbavFuncaoapf_complexidade2.ToJavascriptSource());
         cmbavFuncaoapf_ativo2.CurrentValue = StringUtil.RTrim( AV56FuncaoAPF_Ativo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo2_Internalname, "Values", cmbavFuncaoapf_ativo2.ToJavascriptSource());
         cmbavFuncaoapf_tipo3.CurrentValue = StringUtil.RTrim( AV80FuncaoAPF_Tipo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo3_Internalname, "Values", cmbavFuncaoapf_tipo3.ToJavascriptSource());
         cmbavFuncaoapf_complexidade3.CurrentValue = StringUtil.RTrim( AV81FuncaoAPF_Complexidade3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade3_Internalname, "Values", cmbavFuncaoapf_complexidade3.ToJavascriptSource());
         cmbavFuncaoapf_ativo3.CurrentValue = StringUtil.RTrim( AV82FuncaoAPF_Ativo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo3_Internalname, "Values", cmbavFuncaoapf_ativo3.ToJavascriptSource());
         cmbavFuncaoapf_tipo4.CurrentValue = StringUtil.RTrim( AV86FuncaoAPF_Tipo4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo4_Internalname, "Values", cmbavFuncaoapf_tipo4.ToJavascriptSource());
         cmbavFuncaoapf_complexidade4.CurrentValue = StringUtil.RTrim( AV87FuncaoAPF_Complexidade4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade4_Internalname, "Values", cmbavFuncaoapf_complexidade4.ToJavascriptSource());
         cmbavFuncaoapf_ativo4.CurrentValue = StringUtil.RTrim( AV88FuncaoAPF_Ativo4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo4_Internalname, "Values", cmbavFuncaoapf_ativo4.ToJavascriptSource());
      }

      protected void E253Z2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("funcaoapf.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +AV57FuncaoAPF_SistemaCod);
         context.wjLocDisableFrm = 1;
      }

      protected void S192( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_funcaoapf_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_nome_Internalname, "SortedStatus", Ddo_funcaoapf_nome_Sortedstatus);
         Ddo_funcaoapf_tipo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_tipo_Internalname, "SortedStatus", Ddo_funcaoapf_tipo_Sortedstatus);
         Ddo_funcaoapf_funapfpainom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_funapfpainom_Internalname, "SortedStatus", Ddo_funcaoapf_funapfpainom_Sortedstatus);
         Ddo_funcaoapf_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_ativo_Internalname, "SortedStatus", Ddo_funcaoapf_ativo_Sortedstatus);
      }

      protected void S172( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_funcaoapf_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_nome_Internalname, "SortedStatus", Ddo_funcaoapf_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_funcaoapf_tipo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_tipo_Internalname, "SortedStatus", Ddo_funcaoapf_tipo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_funcaoapf_funapfpainom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_funapfpainom_Internalname, "SortedStatus", Ddo_funcaoapf_funapfpainom_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_funcaoapf_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_ativo_Internalname, "SortedStatus", Ddo_funcaoapf_ativo_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavFuncaoapf_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome1_Visible), 5, 0)));
         cmbavFuncaoapf_tipo1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo1.Visible), 5, 0)));
         cmbavFuncaoapf_complexidade1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_complexidade1.Visible), 5, 0)));
         cmbavFuncaoapf_ativo1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_ativo1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 )
         {
            edtavFuncaoapf_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_TIPO") == 0 )
         {
            cmbavFuncaoapf_tipo1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_COMPLEXIDADE") == 0 )
         {
            cmbavFuncaoapf_complexidade1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_complexidade1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_ATIVO") == 0 )
         {
            cmbavFuncaoapf_ativo1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_ativo1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavFuncaoapf_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome2_Visible), 5, 0)));
         cmbavFuncaoapf_tipo2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo2.Visible), 5, 0)));
         cmbavFuncaoapf_complexidade2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_complexidade2.Visible), 5, 0)));
         cmbavFuncaoapf_ativo2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_ativo2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 )
         {
            edtavFuncaoapf_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_TIPO") == 0 )
         {
            cmbavFuncaoapf_tipo2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_COMPLEXIDADE") == 0 )
         {
            cmbavFuncaoapf_complexidade2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_complexidade2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_ATIVO") == 0 )
         {
            cmbavFuncaoapf_ativo2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_ativo2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavFuncaoapf_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome3_Visible), 5, 0)));
         cmbavFuncaoapf_tipo3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo3.Visible), 5, 0)));
         cmbavFuncaoapf_complexidade3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_complexidade3.Visible), 5, 0)));
         cmbavFuncaoapf_ativo3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_ativo3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 )
         {
            edtavFuncaoapf_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_TIPO") == 0 )
         {
            cmbavFuncaoapf_tipo3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_COMPLEXIDADE") == 0 )
         {
            cmbavFuncaoapf_complexidade3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_complexidade3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_ATIVO") == 0 )
         {
            cmbavFuncaoapf_ativo3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_ativo3.Visible), 5, 0)));
         }
      }

      protected void S142( )
      {
         /* 'ENABLEDYNAMICFILTERS4' Routine */
         edtavFuncaoapf_nome4_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome4_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome4_Visible), 5, 0)));
         cmbavFuncaoapf_tipo4.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo4_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo4.Visible), 5, 0)));
         cmbavFuncaoapf_complexidade4.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade4_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_complexidade4.Visible), 5, 0)));
         cmbavFuncaoapf_ativo4.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo4_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_ativo4.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV84DynamicFiltersSelector4, "FUNCAOAPF_NOME") == 0 )
         {
            edtavFuncaoapf_nome4_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome4_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome4_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV84DynamicFiltersSelector4, "FUNCAOAPF_TIPO") == 0 )
         {
            cmbavFuncaoapf_tipo4.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo4_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo4.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV84DynamicFiltersSelector4, "FUNCAOAPF_COMPLEXIDADE") == 0 )
         {
            cmbavFuncaoapf_complexidade4.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade4_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_complexidade4.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV84DynamicFiltersSelector4, "FUNCAOAPF_ATIVO") == 0 )
         {
            cmbavFuncaoapf_ativo4.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo4_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_ativo4.Visible), 5, 0)));
         }
      }

      protected void S212( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "FUNCAOAPF_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV21FuncaoAPF_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21FuncaoAPF_Nome2", AV21FuncaoAPF_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "FUNCAOAPF_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV25FuncaoAPF_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25FuncaoAPF_Nome3", AV25FuncaoAPF_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV83DynamicFiltersEnabled4 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83DynamicFiltersEnabled4", AV83DynamicFiltersEnabled4);
         AV84DynamicFiltersSelector4 = "FUNCAOAPF_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84DynamicFiltersSelector4", AV84DynamicFiltersSelector4);
         AV85FuncaoAPF_Nome4 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85FuncaoAPF_Nome4", AV85FuncaoAPF_Nome4);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS4' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S232( )
      {
         /* 'CLEANFILTERS' Routine */
         AV92TFFuncaoAPF_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92TFFuncaoAPF_Nome", AV92TFFuncaoAPF_Nome);
         Ddo_funcaoapf_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_nome_Internalname, "FilteredText_set", Ddo_funcaoapf_nome_Filteredtext_set);
         AV93TFFuncaoAPF_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93TFFuncaoAPF_Nome_Sel", AV93TFFuncaoAPF_Nome_Sel);
         Ddo_funcaoapf_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_nome_Internalname, "SelectedValue_set", Ddo_funcaoapf_nome_Selectedvalue_set);
         AV97TFFuncaoAPF_Tipo_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_funcaoapf_tipo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_tipo_Internalname, "SelectedValue_set", Ddo_funcaoapf_tipo_Selectedvalue_set);
         AV100TFFuncaoAPF_TD = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100TFFuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(AV100TFFuncaoAPF_TD), 4, 0)));
         Ddo_funcaoapf_td_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_td_Internalname, "FilteredText_set", Ddo_funcaoapf_td_Filteredtext_set);
         AV101TFFuncaoAPF_TD_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101TFFuncaoAPF_TD_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV101TFFuncaoAPF_TD_To), 4, 0)));
         Ddo_funcaoapf_td_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_td_Internalname, "FilteredTextTo_set", Ddo_funcaoapf_td_Filteredtextto_set);
         AV104TFFuncaoAPF_AR = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104TFFuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV104TFFuncaoAPF_AR), 4, 0)));
         Ddo_funcaoapf_ar_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_ar_Internalname, "FilteredText_set", Ddo_funcaoapf_ar_Filteredtext_set);
         AV105TFFuncaoAPF_AR_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFFuncaoAPF_AR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105TFFuncaoAPF_AR_To), 4, 0)));
         Ddo_funcaoapf_ar_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_ar_Internalname, "FilteredTextTo_set", Ddo_funcaoapf_ar_Filteredtextto_set);
         AV109TFFuncaoAPF_Complexidade_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_funcaoapf_complexidade_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_complexidade_Internalname, "SelectedValue_set", Ddo_funcaoapf_complexidade_Selectedvalue_set);
         AV112TFFuncaoAPF_PF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV112TFFuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( AV112TFFuncaoAPF_PF, 14, 5)));
         Ddo_funcaoapf_pf_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_pf_Internalname, "FilteredText_set", Ddo_funcaoapf_pf_Filteredtext_set);
         AV113TFFuncaoAPF_PF_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV113TFFuncaoAPF_PF_To", StringUtil.LTrim( StringUtil.Str( AV113TFFuncaoAPF_PF_To, 14, 5)));
         Ddo_funcaoapf_pf_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_pf_Internalname, "FilteredTextTo_set", Ddo_funcaoapf_pf_Filteredtextto_set);
         AV116TFFuncaoAPF_FunAPFPaiNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV116TFFuncaoAPF_FunAPFPaiNom", AV116TFFuncaoAPF_FunAPFPaiNom);
         Ddo_funcaoapf_funapfpainom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_funapfpainom_Internalname, "FilteredText_set", Ddo_funcaoapf_funapfpainom_Filteredtext_set);
         AV117TFFuncaoAPF_FunAPFPaiNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117TFFuncaoAPF_FunAPFPaiNom_Sel", AV117TFFuncaoAPF_FunAPFPaiNom_Sel);
         Ddo_funcaoapf_funapfpainom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_funapfpainom_Internalname, "SelectedValue_set", Ddo_funcaoapf_funapfpainom_Selectedvalue_set);
         AV121TFFuncaoAPF_Ativo_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_funcaoapf_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_ativo_Internalname, "SelectedValue_set", Ddo_funcaoapf_ativo_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "FUNCAOAPF_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV17FuncaoAPF_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoAPF_Nome1", AV17FuncaoAPF_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S162( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get(AV165Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV165Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV30Session.Get(AV165Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S242 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S242( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV166GXV1 = 1;
         while ( AV166GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV166GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_NOME") == 0 )
            {
               AV92TFFuncaoAPF_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92TFFuncaoAPF_Nome", AV92TFFuncaoAPF_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92TFFuncaoAPF_Nome)) )
               {
                  Ddo_funcaoapf_nome_Filteredtext_set = AV92TFFuncaoAPF_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_nome_Internalname, "FilteredText_set", Ddo_funcaoapf_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_NOME_SEL") == 0 )
            {
               AV93TFFuncaoAPF_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93TFFuncaoAPF_Nome_Sel", AV93TFFuncaoAPF_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93TFFuncaoAPF_Nome_Sel)) )
               {
                  Ddo_funcaoapf_nome_Selectedvalue_set = AV93TFFuncaoAPF_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_nome_Internalname, "SelectedValue_set", Ddo_funcaoapf_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_TIPO_SEL") == 0 )
            {
               AV96TFFuncaoAPF_Tipo_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV97TFFuncaoAPF_Tipo_Sels.FromJSonString(AV96TFFuncaoAPF_Tipo_SelsJson);
               if ( ! ( AV97TFFuncaoAPF_Tipo_Sels.Count == 0 ) )
               {
                  Ddo_funcaoapf_tipo_Selectedvalue_set = AV96TFFuncaoAPF_Tipo_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_tipo_Internalname, "SelectedValue_set", Ddo_funcaoapf_tipo_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_TD") == 0 )
            {
               AV100TFFuncaoAPF_TD = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100TFFuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(AV100TFFuncaoAPF_TD), 4, 0)));
               AV101TFFuncaoAPF_TD_To = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101TFFuncaoAPF_TD_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV101TFFuncaoAPF_TD_To), 4, 0)));
               if ( ! (0==AV100TFFuncaoAPF_TD) )
               {
                  Ddo_funcaoapf_td_Filteredtext_set = StringUtil.Str( (decimal)(AV100TFFuncaoAPF_TD), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_td_Internalname, "FilteredText_set", Ddo_funcaoapf_td_Filteredtext_set);
               }
               if ( ! (0==AV101TFFuncaoAPF_TD_To) )
               {
                  Ddo_funcaoapf_td_Filteredtextto_set = StringUtil.Str( (decimal)(AV101TFFuncaoAPF_TD_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_td_Internalname, "FilteredTextTo_set", Ddo_funcaoapf_td_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_AR") == 0 )
            {
               AV104TFFuncaoAPF_AR = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104TFFuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV104TFFuncaoAPF_AR), 4, 0)));
               AV105TFFuncaoAPF_AR_To = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFFuncaoAPF_AR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105TFFuncaoAPF_AR_To), 4, 0)));
               if ( ! (0==AV104TFFuncaoAPF_AR) )
               {
                  Ddo_funcaoapf_ar_Filteredtext_set = StringUtil.Str( (decimal)(AV104TFFuncaoAPF_AR), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_ar_Internalname, "FilteredText_set", Ddo_funcaoapf_ar_Filteredtext_set);
               }
               if ( ! (0==AV105TFFuncaoAPF_AR_To) )
               {
                  Ddo_funcaoapf_ar_Filteredtextto_set = StringUtil.Str( (decimal)(AV105TFFuncaoAPF_AR_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_ar_Internalname, "FilteredTextTo_set", Ddo_funcaoapf_ar_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_COMPLEXIDADE_SEL") == 0 )
            {
               AV108TFFuncaoAPF_Complexidade_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV109TFFuncaoAPF_Complexidade_Sels.FromJSonString(AV108TFFuncaoAPF_Complexidade_SelsJson);
               if ( ! ( AV109TFFuncaoAPF_Complexidade_Sels.Count == 0 ) )
               {
                  Ddo_funcaoapf_complexidade_Selectedvalue_set = AV108TFFuncaoAPF_Complexidade_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_complexidade_Internalname, "SelectedValue_set", Ddo_funcaoapf_complexidade_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_PF") == 0 )
            {
               AV112TFFuncaoAPF_PF = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV112TFFuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( AV112TFFuncaoAPF_PF, 14, 5)));
               AV113TFFuncaoAPF_PF_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV113TFFuncaoAPF_PF_To", StringUtil.LTrim( StringUtil.Str( AV113TFFuncaoAPF_PF_To, 14, 5)));
               if ( ! (Convert.ToDecimal(0)==AV112TFFuncaoAPF_PF) )
               {
                  Ddo_funcaoapf_pf_Filteredtext_set = StringUtil.Str( AV112TFFuncaoAPF_PF, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_pf_Internalname, "FilteredText_set", Ddo_funcaoapf_pf_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV113TFFuncaoAPF_PF_To) )
               {
                  Ddo_funcaoapf_pf_Filteredtextto_set = StringUtil.Str( AV113TFFuncaoAPF_PF_To, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_pf_Internalname, "FilteredTextTo_set", Ddo_funcaoapf_pf_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_FUNAPFPAINOM") == 0 )
            {
               AV116TFFuncaoAPF_FunAPFPaiNom = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV116TFFuncaoAPF_FunAPFPaiNom", AV116TFFuncaoAPF_FunAPFPaiNom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV116TFFuncaoAPF_FunAPFPaiNom)) )
               {
                  Ddo_funcaoapf_funapfpainom_Filteredtext_set = AV116TFFuncaoAPF_FunAPFPaiNom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_funapfpainom_Internalname, "FilteredText_set", Ddo_funcaoapf_funapfpainom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_FUNAPFPAINOM_SEL") == 0 )
            {
               AV117TFFuncaoAPF_FunAPFPaiNom_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117TFFuncaoAPF_FunAPFPaiNom_Sel", AV117TFFuncaoAPF_FunAPFPaiNom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117TFFuncaoAPF_FunAPFPaiNom_Sel)) )
               {
                  Ddo_funcaoapf_funapfpainom_Selectedvalue_set = AV117TFFuncaoAPF_FunAPFPaiNom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_funapfpainom_Internalname, "SelectedValue_set", Ddo_funcaoapf_funapfpainom_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_ATIVO_SEL") == 0 )
            {
               AV120TFFuncaoAPF_Ativo_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV121TFFuncaoAPF_Ativo_Sels.FromJSonString(AV120TFFuncaoAPF_Ativo_SelsJson);
               if ( ! ( AV121TFFuncaoAPF_Ativo_Sels.Count == 0 ) )
               {
                  Ddo_funcaoapf_ativo_Selectedvalue_set = AV120TFFuncaoAPF_Ativo_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_ativo_Internalname, "SelectedValue_set", Ddo_funcaoapf_ativo_Selectedvalue_set);
               }
            }
            AV166GXV1 = (int)(AV166GXV1+1);
         }
      }

      protected void S222( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         imgAdddynamicfilters3_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters3_Visible), 5, 0)));
         imgRemovedynamicfilters3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters3_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 )
            {
               AV17FuncaoAPF_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoAPF_Nome1", AV17FuncaoAPF_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_TIPO") == 0 )
            {
               AV76FuncaoAPF_Tipo1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76FuncaoAPF_Tipo1", AV76FuncaoAPF_Tipo1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_COMPLEXIDADE") == 0 )
            {
               AV77FuncaoAPF_Complexidade1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77FuncaoAPF_Complexidade1", AV77FuncaoAPF_Complexidade1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_ATIVO") == 0 )
            {
               AV55FuncaoAPF_Ativo1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55FuncaoAPF_Ativo1", AV55FuncaoAPF_Ativo1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 )
               {
                  AV21FuncaoAPF_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21FuncaoAPF_Nome2", AV21FuncaoAPF_Nome2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_TIPO") == 0 )
               {
                  AV78FuncaoAPF_Tipo2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78FuncaoAPF_Tipo2", AV78FuncaoAPF_Tipo2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_COMPLEXIDADE") == 0 )
               {
                  AV79FuncaoAPF_Complexidade2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79FuncaoAPF_Complexidade2", AV79FuncaoAPF_Complexidade2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_ATIVO") == 0 )
               {
                  AV56FuncaoAPF_Ativo2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56FuncaoAPF_Ativo2", AV56FuncaoAPF_Ativo2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 )
                  {
                     AV25FuncaoAPF_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25FuncaoAPF_Nome3", AV25FuncaoAPF_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_TIPO") == 0 )
                  {
                     AV80FuncaoAPF_Tipo3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80FuncaoAPF_Tipo3", AV80FuncaoAPF_Tipo3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_COMPLEXIDADE") == 0 )
                  {
                     AV81FuncaoAPF_Complexidade3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81FuncaoAPF_Complexidade3", AV81FuncaoAPF_Complexidade3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_ATIVO") == 0 )
                  {
                     AV82FuncaoAPF_Ativo3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82FuncaoAPF_Ativo3", AV82FuncaoAPF_Ativo3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
                  if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 4 )
                  {
                     lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(4);";
                     context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                     imgAdddynamicfilters3_Visible = 0;
                     context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters3_Visible), 5, 0)));
                     imgRemovedynamicfilters3_Visible = 1;
                     context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters3_Visible), 5, 0)));
                     AV83DynamicFiltersEnabled4 = true;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83DynamicFiltersEnabled4", AV83DynamicFiltersEnabled4);
                     AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(4));
                     AV84DynamicFiltersSelector4 = AV12GridStateDynamicFilter.gxTpr_Selected;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84DynamicFiltersSelector4", AV84DynamicFiltersSelector4);
                     if ( StringUtil.StrCmp(AV84DynamicFiltersSelector4, "FUNCAOAPF_NOME") == 0 )
                     {
                        AV85FuncaoAPF_Nome4 = AV12GridStateDynamicFilter.gxTpr_Value;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85FuncaoAPF_Nome4", AV85FuncaoAPF_Nome4);
                     }
                     else if ( StringUtil.StrCmp(AV84DynamicFiltersSelector4, "FUNCAOAPF_TIPO") == 0 )
                     {
                        AV86FuncaoAPF_Tipo4 = AV12GridStateDynamicFilter.gxTpr_Value;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86FuncaoAPF_Tipo4", AV86FuncaoAPF_Tipo4);
                     }
                     else if ( StringUtil.StrCmp(AV84DynamicFiltersSelector4, "FUNCAOAPF_COMPLEXIDADE") == 0 )
                     {
                        AV87FuncaoAPF_Complexidade4 = AV12GridStateDynamicFilter.gxTpr_Value;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87FuncaoAPF_Complexidade4", AV87FuncaoAPF_Complexidade4);
                     }
                     else if ( StringUtil.StrCmp(AV84DynamicFiltersSelector4, "FUNCAOAPF_ATIVO") == 0 )
                     {
                        AV88FuncaoAPF_Ativo4 = AV12GridStateDynamicFilter.gxTpr_Value;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88FuncaoAPF_Ativo4", AV88FuncaoAPF_Ativo4);
                     }
                     /* Execute user subroutine: 'ENABLEDYNAMICFILTERS4' */
                     S142 ();
                     if ( returnInSub )
                     {
                        returnInSub = true;
                        if (true) return;
                     }
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S182( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV30Session.Get(AV165Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92TFFuncaoAPF_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPF_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV92TFFuncaoAPF_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93TFFuncaoAPF_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPF_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV93TFFuncaoAPF_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV97TFFuncaoAPF_Tipo_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPF_TIPO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV97TFFuncaoAPF_Tipo_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV100TFFuncaoAPF_TD) && (0==AV101TFFuncaoAPF_TD_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPF_TD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV100TFFuncaoAPF_TD), 4, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV101TFFuncaoAPF_TD_To), 4, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV104TFFuncaoAPF_AR) && (0==AV105TFFuncaoAPF_AR_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPF_AR";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV104TFFuncaoAPF_AR), 4, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV105TFFuncaoAPF_AR_To), 4, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV109TFFuncaoAPF_Complexidade_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPF_COMPLEXIDADE_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV109TFFuncaoAPF_Complexidade_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV112TFFuncaoAPF_PF) && (Convert.ToDecimal(0)==AV113TFFuncaoAPF_PF_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPF_PF";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV112TFFuncaoAPF_PF, 14, 5);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV113TFFuncaoAPF_PF_To, 14, 5);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV116TFFuncaoAPF_FunAPFPaiNom)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPF_FUNAPFPAINOM";
            AV11GridStateFilterValue.gxTpr_Value = AV116TFFuncaoAPF_FunAPFPaiNom;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117TFFuncaoAPF_FunAPFPaiNom_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPF_FUNAPFPAINOM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV117TFFuncaoAPF_FunAPFPaiNom_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV121TFFuncaoAPF_Ativo_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPF_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV121TFFuncaoAPF_Ativo_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV165Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S202( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17FuncaoAPF_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17FuncaoAPF_Nome1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_TIPO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV76FuncaoAPF_Tipo1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV76FuncaoAPF_Tipo1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_COMPLEXIDADE") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV77FuncaoAPF_Complexidade1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV77FuncaoAPF_Complexidade1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_ATIVO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV55FuncaoAPF_Ativo1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV55FuncaoAPF_Ativo1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21FuncaoAPF_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21FuncaoAPF_Nome2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_TIPO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV78FuncaoAPF_Tipo2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV78FuncaoAPF_Tipo2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_COMPLEXIDADE") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV79FuncaoAPF_Complexidade2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV79FuncaoAPF_Complexidade2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_ATIVO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV56FuncaoAPF_Ativo2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV56FuncaoAPF_Ativo2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25FuncaoAPF_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25FuncaoAPF_Nome3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_TIPO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV80FuncaoAPF_Tipo3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV80FuncaoAPF_Tipo3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_COMPLEXIDADE") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV81FuncaoAPF_Complexidade3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV81FuncaoAPF_Complexidade3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_ATIVO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV82FuncaoAPF_Ativo3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV82FuncaoAPF_Ativo3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV83DynamicFiltersEnabled4 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV84DynamicFiltersSelector4;
            if ( ( StringUtil.StrCmp(AV84DynamicFiltersSelector4, "FUNCAOAPF_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV85FuncaoAPF_Nome4)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV85FuncaoAPF_Nome4;
            }
            else if ( ( StringUtil.StrCmp(AV84DynamicFiltersSelector4, "FUNCAOAPF_TIPO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV86FuncaoAPF_Tipo4)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV86FuncaoAPF_Tipo4;
            }
            else if ( ( StringUtil.StrCmp(AV84DynamicFiltersSelector4, "FUNCAOAPF_COMPLEXIDADE") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV87FuncaoAPF_Complexidade4)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV87FuncaoAPF_Complexidade4;
            }
            else if ( ( StringUtil.StrCmp(AV84DynamicFiltersSelector4, "FUNCAOAPF_ATIVO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV88FuncaoAPF_Ativo4)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV88FuncaoAPF_Ativo4;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S152( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV165Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "FuncaoAPF";
         AV30Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_3Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_3Z2( true) ;
         }
         else
         {
            wb_table2_8_3Z2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_3Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_93_3Z2( true) ;
         }
         else
         {
            wb_table3_93_3Z2( false) ;
         }
         return  ;
      }

      protected void wb_table3_93_3Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3Z2e( true) ;
         }
         else
         {
            wb_table1_2_3Z2e( false) ;
         }
      }

      protected void wb_table3_93_3Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"96\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "de Transa��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPF_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPF_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPF_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbFuncaoAPF_Tipo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbFuncaoAPF_Tipo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbFuncaoAPF_Tipo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPF_TD_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPF_TD_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPF_TD_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPF_AR_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPF_AR_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPF_AR_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbFuncaoAPF_Complexidade_Titleformat == 0 )
               {
                  context.SendWebValue( cmbFuncaoAPF_Complexidade.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbFuncaoAPF_Complexidade.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPF_PF_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPF_PF_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPF_PF_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPF_FunAPFPaiNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPF_FunAPFPaiNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPF_FunAPFPaiNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbFuncaoAPF_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbFuncaoAPF_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbFuncaoAPF_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV64Display));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A166FuncaoAPF_Nome);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPF_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPF_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtFuncaoAPF_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A184FuncaoAPF_Tipo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbFuncaoAPF_Tipo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbFuncaoAPF_Tipo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A388FuncaoAPF_TD), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPF_TD_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPF_TD_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A387FuncaoAPF_AR), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPF_AR_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPF_AR_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A185FuncaoAPF_Complexidade));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbFuncaoAPF_Complexidade.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbFuncaoAPF_Complexidade_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A386FuncaoAPF_PF, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPF_PF_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPF_PF_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A358FuncaoAPF_FunAPFPaiCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A363FuncaoAPF_FunAPFPaiNom);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPF_FunAPFPaiNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPF_FunAPFPaiNom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtFuncaoAPF_FunAPFPaiNom_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A183FuncaoAPF_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbFuncaoAPF_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbFuncaoAPF_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 96 )
         {
            wbEnd = 0;
            nRC_GXsfl_96 = (short)(nGXsfl_96_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridContainer.AddObjectProperty("GRID_nEOF", GRID_nEOF);
               GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_93_3Z2e( true) ;
         }
         else
         {
            wb_table3_93_3Z2e( false) ;
         }
      }

      protected void wb_table2_8_3Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFuncaoapftitle_Internalname, "Fun��es de Transa��o", "", "", lblFuncaoapftitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table4_13_3Z2( true) ;
         }
         else
         {
            wb_table4_13_3Z2( false) ;
         }
         return  ;
      }

      protected void wb_table4_13_3Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_96_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWFuncaoAPF.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_96_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_23_3Z2( true) ;
         }
         else
         {
            wb_table5_23_3Z2( false) ;
         }
         return  ;
      }

      protected void wb_table5_23_3Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_3Z2e( true) ;
         }
         else
         {
            wb_table2_8_3Z2e( false) ;
         }
      }

      protected void wb_table5_23_3Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_28_3Z2( true) ;
         }
         else
         {
            wb_table6_28_3Z2( false) ;
         }
         return  ;
      }

      protected void wb_table6_28_3Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnatualizar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(96), 2, 0)+","+"null"+");", "Procurar", bttBtnatualizar_Jsonclick, 7, "Procurar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"e363z1_client"+"'", TempTags, "", 2, "HLP_WWFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_23_3Z2e( true) ;
         }
         else
         {
            wb_table5_23_3Z2e( false) ;
         }
      }

      protected void wb_table6_28_3Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_96_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWFuncaoAPF.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_96_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaoapf_nome1_Internalname, AV17FuncaoAPF_Nome1, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", 0, edtavFuncaoapf_nome1_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWFuncaoAPF.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_96_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaoapf_tipo1, cmbavFuncaoapf_tipo1_Internalname, StringUtil.RTrim( AV76FuncaoAPF_Tipo1), 1, cmbavFuncaoapf_tipo1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaoapf_tipo1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "", true, "HLP_WWFuncaoAPF.htm");
            cmbavFuncaoapf_tipo1.CurrentValue = StringUtil.RTrim( AV76FuncaoAPF_Tipo1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo1_Internalname, "Values", (String)(cmbavFuncaoapf_tipo1.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'" + sGXsfl_96_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaoapf_complexidade1, cmbavFuncaoapf_complexidade1_Internalname, StringUtil.RTrim( AV77FuncaoAPF_Complexidade1), 1, cmbavFuncaoapf_complexidade1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaoapf_complexidade1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "", true, "HLP_WWFuncaoAPF.htm");
            cmbavFuncaoapf_complexidade1.CurrentValue = StringUtil.RTrim( AV77FuncaoAPF_Complexidade1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade1_Internalname, "Values", (String)(cmbavFuncaoapf_complexidade1.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_96_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaoapf_ativo1, cmbavFuncaoapf_ativo1_Internalname, StringUtil.RTrim( AV55FuncaoAPF_Ativo1), 1, cmbavFuncaoapf_ativo1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaoapf_ativo1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_WWFuncaoAPF.htm");
            cmbavFuncaoapf_ativo1.CurrentValue = StringUtil.RTrim( AV55FuncaoAPF_Ativo1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo1_Internalname, "Values", (String)(cmbavFuncaoapf_ativo1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFuncaoAPF.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_96_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "", true, "HLP_WWFuncaoAPF.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_96_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaoapf_nome2_Internalname, AV21FuncaoAPF_Nome2, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", 0, edtavFuncaoapf_nome2_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWFuncaoAPF.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'" + sGXsfl_96_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaoapf_tipo2, cmbavFuncaoapf_tipo2_Internalname, StringUtil.RTrim( AV78FuncaoAPF_Tipo2), 1, cmbavFuncaoapf_tipo2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaoapf_tipo2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"", "", true, "HLP_WWFuncaoAPF.htm");
            cmbavFuncaoapf_tipo2.CurrentValue = StringUtil.RTrim( AV78FuncaoAPF_Tipo2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo2_Internalname, "Values", (String)(cmbavFuncaoapf_tipo2.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_96_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaoapf_complexidade2, cmbavFuncaoapf_complexidade2_Internalname, StringUtil.RTrim( AV79FuncaoAPF_Complexidade2), 1, cmbavFuncaoapf_complexidade2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaoapf_complexidade2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", "", true, "HLP_WWFuncaoAPF.htm");
            cmbavFuncaoapf_complexidade2.CurrentValue = StringUtil.RTrim( AV79FuncaoAPF_Complexidade2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade2_Internalname, "Values", (String)(cmbavFuncaoapf_complexidade2.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'" + sGXsfl_96_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaoapf_ativo2, cmbavFuncaoapf_ativo2_Internalname, StringUtil.RTrim( AV56FuncaoAPF_Ativo2), 1, cmbavFuncaoapf_ativo2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaoapf_ativo2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", "", true, "HLP_WWFuncaoAPF.htm");
            cmbavFuncaoapf_ativo2.CurrentValue = StringUtil.RTrim( AV56FuncaoAPF_Ativo2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo2_Internalname, "Values", (String)(cmbavFuncaoapf_ativo2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFuncaoAPF.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'" + sGXsfl_96_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,63);\"", "", true, "HLP_WWFuncaoAPF.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_96_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaoapf_nome3_Internalname, AV25FuncaoAPF_Nome3, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", 0, edtavFuncaoapf_nome3_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWFuncaoAPF.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'" + sGXsfl_96_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaoapf_tipo3, cmbavFuncaoapf_tipo3_Internalname, StringUtil.RTrim( AV80FuncaoAPF_Tipo3), 1, cmbavFuncaoapf_tipo3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaoapf_tipo3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,68);\"", "", true, "HLP_WWFuncaoAPF.htm");
            cmbavFuncaoapf_tipo3.CurrentValue = StringUtil.RTrim( AV80FuncaoAPF_Tipo3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo3_Internalname, "Values", (String)(cmbavFuncaoapf_tipo3.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_96_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaoapf_complexidade3, cmbavFuncaoapf_complexidade3_Internalname, StringUtil.RTrim( AV81FuncaoAPF_Complexidade3), 1, cmbavFuncaoapf_complexidade3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaoapf_complexidade3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", "", true, "HLP_WWFuncaoAPF.htm");
            cmbavFuncaoapf_complexidade3.CurrentValue = StringUtil.RTrim( AV81FuncaoAPF_Complexidade3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade3_Internalname, "Values", (String)(cmbavFuncaoapf_complexidade3.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'" + sGXsfl_96_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaoapf_ativo3, cmbavFuncaoapf_ativo3_Internalname, StringUtil.RTrim( AV82FuncaoAPF_Ativo3), 1, cmbavFuncaoapf_ativo3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaoapf_ativo3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,70);\"", "", true, "HLP_WWFuncaoAPF.htm");
            cmbavFuncaoapf_ativo3.CurrentValue = StringUtil.RTrim( AV82FuncaoAPF_Ativo3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo3_Internalname, "Values", (String)(cmbavFuncaoapf_ativo3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters3_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters3_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFuncaoAPF.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters3_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow4\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix4_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_96_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector4, cmbavDynamicfiltersselector4_Internalname, StringUtil.RTrim( AV84DynamicFiltersSelector4), 1, cmbavDynamicfiltersselector4_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR4.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,78);\"", "", true, "HLP_WWFuncaoAPF.htm");
            cmbavDynamicfiltersselector4.CurrentValue = StringUtil.RTrim( AV84DynamicFiltersSelector4);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector4_Internalname, "Values", (String)(cmbavDynamicfiltersselector4.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle4_Internalname, "valor", "", "", lblDynamicfiltersmiddle4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_96_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaoapf_nome4_Internalname, AV85FuncaoAPF_Nome4, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,82);\"", 0, edtavFuncaoapf_nome4_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWFuncaoAPF.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'" + sGXsfl_96_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaoapf_tipo4, cmbavFuncaoapf_tipo4_Internalname, StringUtil.RTrim( AV86FuncaoAPF_Tipo4), 1, cmbavFuncaoapf_tipo4_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaoapf_tipo4.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,83);\"", "", true, "HLP_WWFuncaoAPF.htm");
            cmbavFuncaoapf_tipo4.CurrentValue = StringUtil.RTrim( AV86FuncaoAPF_Tipo4);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo4_Internalname, "Values", (String)(cmbavFuncaoapf_tipo4.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'" + sGXsfl_96_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaoapf_complexidade4, cmbavFuncaoapf_complexidade4_Internalname, StringUtil.RTrim( AV87FuncaoAPF_Complexidade4), 1, cmbavFuncaoapf_complexidade4_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaoapf_complexidade4.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,84);\"", "", true, "HLP_WWFuncaoAPF.htm");
            cmbavFuncaoapf_complexidade4.CurrentValue = StringUtil.RTrim( AV87FuncaoAPF_Complexidade4);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade4_Internalname, "Values", (String)(cmbavFuncaoapf_complexidade4.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'" + sGXsfl_96_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaoapf_ativo4, cmbavFuncaoapf_ativo4_Internalname, StringUtil.RTrim( AV88FuncaoAPF_Ativo4), 1, cmbavFuncaoapf_ativo4_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaoapf_ativo4.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,85);\"", "", true, "HLP_WWFuncaoAPF.htm");
            cmbavFuncaoapf_ativo4.CurrentValue = StringUtil.RTrim( AV88FuncaoAPF_Ativo4);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo4_Internalname, "Values", (String)(cmbavFuncaoapf_ativo4.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters4_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters4_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS4\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_28_3Z2e( true) ;
         }
         else
         {
            wb_table6_28_3Z2e( false) ;
         }
      }

      protected void wb_table4_13_3Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_3Z2e( true) ;
         }
         else
         {
            wb_table4_13_3Z2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA3Z2( ) ;
         WS3Z2( ) ;
         WE3Z2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202051812583073");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwfuncaoapf.js", "?202051812583073");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_962( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_96_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_96_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_96_idx;
         edtFuncaoAPF_Codigo_Internalname = "FUNCAOAPF_CODIGO_"+sGXsfl_96_idx;
         edtFuncaoAPF_Nome_Internalname = "FUNCAOAPF_NOME_"+sGXsfl_96_idx;
         cmbFuncaoAPF_Tipo_Internalname = "FUNCAOAPF_TIPO_"+sGXsfl_96_idx;
         edtFuncaoAPF_TD_Internalname = "FUNCAOAPF_TD_"+sGXsfl_96_idx;
         edtFuncaoAPF_AR_Internalname = "FUNCAOAPF_AR_"+sGXsfl_96_idx;
         cmbFuncaoAPF_Complexidade_Internalname = "FUNCAOAPF_COMPLEXIDADE_"+sGXsfl_96_idx;
         edtFuncaoAPF_PF_Internalname = "FUNCAOAPF_PF_"+sGXsfl_96_idx;
         edtFuncaoAPF_FunAPFPaiCod_Internalname = "FUNCAOAPF_FUNAPFPAICOD_"+sGXsfl_96_idx;
         edtFuncaoAPF_FunAPFPaiNom_Internalname = "FUNCAOAPF_FUNAPFPAINOM_"+sGXsfl_96_idx;
         cmbFuncaoAPF_Ativo_Internalname = "FUNCAOAPF_ATIVO_"+sGXsfl_96_idx;
      }

      protected void SubsflControlProps_fel_962( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_96_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_96_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_96_fel_idx;
         edtFuncaoAPF_Codigo_Internalname = "FUNCAOAPF_CODIGO_"+sGXsfl_96_fel_idx;
         edtFuncaoAPF_Nome_Internalname = "FUNCAOAPF_NOME_"+sGXsfl_96_fel_idx;
         cmbFuncaoAPF_Tipo_Internalname = "FUNCAOAPF_TIPO_"+sGXsfl_96_fel_idx;
         edtFuncaoAPF_TD_Internalname = "FUNCAOAPF_TD_"+sGXsfl_96_fel_idx;
         edtFuncaoAPF_AR_Internalname = "FUNCAOAPF_AR_"+sGXsfl_96_fel_idx;
         cmbFuncaoAPF_Complexidade_Internalname = "FUNCAOAPF_COMPLEXIDADE_"+sGXsfl_96_fel_idx;
         edtFuncaoAPF_PF_Internalname = "FUNCAOAPF_PF_"+sGXsfl_96_fel_idx;
         edtFuncaoAPF_FunAPFPaiCod_Internalname = "FUNCAOAPF_FUNAPFPAICOD_"+sGXsfl_96_fel_idx;
         edtFuncaoAPF_FunAPFPaiNom_Internalname = "FUNCAOAPF_FUNAPFPAINOM_"+sGXsfl_96_fel_idx;
         cmbFuncaoAPF_Ativo_Internalname = "FUNCAOAPF_ATIVO_"+sGXsfl_96_fel_idx;
      }

      protected void sendrow_962( )
      {
         SubsflControlProps_962( ) ;
         WB3Z0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_96_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_96_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_96_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV162Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV162Update_GXI : context.PathToRelativeUrl( AV28Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV163Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV163Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV64Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV64Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV164Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV64Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV64Display)) ? AV164Display_GXI : context.PathToRelativeUrl( AV64Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV64Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)96,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_Nome_Internalname,(String)A166FuncaoAPF_Nome,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtFuncaoAPF_Nome_Link,(String)"",(String)"",(String)"",(String)edtFuncaoAPF_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)200,(short)0,(short)0,(short)96,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_96_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "FUNCAOAPF_TIPO_" + sGXsfl_96_idx;
               cmbFuncaoAPF_Tipo.Name = GXCCtl;
               cmbFuncaoAPF_Tipo.WebTags = "";
               cmbFuncaoAPF_Tipo.addItem("", "(Nenhum)", 0);
               cmbFuncaoAPF_Tipo.addItem("EE", "EE", 0);
               cmbFuncaoAPF_Tipo.addItem("SE", "SE", 0);
               cmbFuncaoAPF_Tipo.addItem("CE", "CE", 0);
               cmbFuncaoAPF_Tipo.addItem("NM", "NM", 0);
               if ( cmbFuncaoAPF_Tipo.ItemCount > 0 )
               {
                  A184FuncaoAPF_Tipo = cmbFuncaoAPF_Tipo.getValidValue(A184FuncaoAPF_Tipo);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbFuncaoAPF_Tipo,(String)cmbFuncaoAPF_Tipo_Internalname,StringUtil.RTrim( A184FuncaoAPF_Tipo),(short)1,(String)cmbFuncaoAPF_Tipo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbFuncaoAPF_Tipo.CurrentValue = StringUtil.RTrim( A184FuncaoAPF_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPF_Tipo_Internalname, "Values", (String)(cmbFuncaoAPF_Tipo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_TD_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A388FuncaoAPF_TD), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A388FuncaoAPF_TD), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_TD_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)96,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_AR_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A387FuncaoAPF_AR), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A387FuncaoAPF_AR), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_AR_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)96,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            GXCCtl = "FUNCAOAPF_COMPLEXIDADE_" + sGXsfl_96_idx;
            cmbFuncaoAPF_Complexidade.Name = GXCCtl;
            cmbFuncaoAPF_Complexidade.WebTags = "";
            cmbFuncaoAPF_Complexidade.addItem("E", "", 0);
            cmbFuncaoAPF_Complexidade.addItem("B", "Baixa", 0);
            cmbFuncaoAPF_Complexidade.addItem("M", "M�dia", 0);
            cmbFuncaoAPF_Complexidade.addItem("A", "Alta", 0);
            if ( cmbFuncaoAPF_Complexidade.ItemCount > 0 )
            {
               A185FuncaoAPF_Complexidade = cmbFuncaoAPF_Complexidade.getValidValue(A185FuncaoAPF_Complexidade);
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbFuncaoAPF_Complexidade,(String)cmbFuncaoAPF_Complexidade_Internalname,StringUtil.RTrim( A185FuncaoAPF_Complexidade),(short)1,(String)cmbFuncaoAPF_Complexidade_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbFuncaoAPF_Complexidade.CurrentValue = StringUtil.RTrim( A185FuncaoAPF_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPF_Complexidade_Internalname, "Values", (String)(cmbFuncaoAPF_Complexidade.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_PF_Internalname,StringUtil.LTrim( StringUtil.NToC( A386FuncaoAPF_PF, 14, 5, ",", "")),context.localUtil.Format( A386FuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_PF_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)96,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_FunAPFPaiCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A358FuncaoAPF_FunAPFPaiCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A358FuncaoAPF_FunAPFPaiCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_FunAPFPaiCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)96,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_FunAPFPaiNom_Internalname,(String)A363FuncaoAPF_FunAPFPaiNom,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtFuncaoAPF_FunAPFPaiNom_Link,(String)"",(String)"",(String)"",(String)edtFuncaoAPF_FunAPFPaiNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)200,(short)0,(short)0,(short)96,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_96_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "FUNCAOAPF_ATIVO_" + sGXsfl_96_idx;
               cmbFuncaoAPF_Ativo.Name = GXCCtl;
               cmbFuncaoAPF_Ativo.WebTags = "";
               cmbFuncaoAPF_Ativo.addItem("A", "Ativa", 0);
               cmbFuncaoAPF_Ativo.addItem("E", "Exclu�da", 0);
               cmbFuncaoAPF_Ativo.addItem("R", "Rejeitada", 0);
               if ( cmbFuncaoAPF_Ativo.ItemCount > 0 )
               {
                  A183FuncaoAPF_Ativo = cmbFuncaoAPF_Ativo.getValidValue(A183FuncaoAPF_Ativo);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbFuncaoAPF_Ativo,(String)cmbFuncaoAPF_Ativo_Internalname,StringUtil.RTrim( A183FuncaoAPF_Ativo),(short)1,(String)cmbFuncaoAPF_Ativo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbFuncaoAPF_Ativo.CurrentValue = StringUtil.RTrim( A183FuncaoAPF_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPF_Ativo_Internalname, "Values", (String)(cmbFuncaoAPF_Ativo.ToJavascriptSource()));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_CODIGO"+"_"+sGXsfl_96_idx, GetSecureSignedToken( sGXsfl_96_idx, context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_NOME"+"_"+sGXsfl_96_idx, GetSecureSignedToken( sGXsfl_96_idx, StringUtil.RTrim( context.localUtil.Format( A166FuncaoAPF_Nome, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_TIPO"+"_"+sGXsfl_96_idx, GetSecureSignedToken( sGXsfl_96_idx, StringUtil.RTrim( context.localUtil.Format( A184FuncaoAPF_Tipo, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_TD"+"_"+sGXsfl_96_idx, GetSecureSignedToken( sGXsfl_96_idx, context.localUtil.Format( (decimal)(A388FuncaoAPF_TD), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_AR"+"_"+sGXsfl_96_idx, GetSecureSignedToken( sGXsfl_96_idx, context.localUtil.Format( (decimal)(A387FuncaoAPF_AR), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_COMPLEXIDADE"+"_"+sGXsfl_96_idx, GetSecureSignedToken( sGXsfl_96_idx, StringUtil.RTrim( context.localUtil.Format( A185FuncaoAPF_Complexidade, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_PF"+"_"+sGXsfl_96_idx, GetSecureSignedToken( sGXsfl_96_idx, context.localUtil.Format( A386FuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_FUNAPFPAICOD"+"_"+sGXsfl_96_idx, GetSecureSignedToken( sGXsfl_96_idx, context.localUtil.Format( (decimal)(A358FuncaoAPF_FunAPFPaiCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_ATIVO"+"_"+sGXsfl_96_idx, GetSecureSignedToken( sGXsfl_96_idx, StringUtil.RTrim( context.localUtil.Format( A183FuncaoAPF_Ativo, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_96_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_96_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_96_idx+1));
            sGXsfl_96_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_96_idx), 4, 0)), 4, "0");
            SubsflControlProps_962( ) ;
         }
         /* End function sendrow_962 */
      }

      protected void init_default_properties( )
      {
         lblFuncaoapftitle_Internalname = "FUNCAOAPFTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavFuncaoapf_nome1_Internalname = "vFUNCAOAPF_NOME1";
         cmbavFuncaoapf_tipo1_Internalname = "vFUNCAOAPF_TIPO1";
         cmbavFuncaoapf_complexidade1_Internalname = "vFUNCAOAPF_COMPLEXIDADE1";
         cmbavFuncaoapf_ativo1_Internalname = "vFUNCAOAPF_ATIVO1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavFuncaoapf_nome2_Internalname = "vFUNCAOAPF_NOME2";
         cmbavFuncaoapf_tipo2_Internalname = "vFUNCAOAPF_TIPO2";
         cmbavFuncaoapf_complexidade2_Internalname = "vFUNCAOAPF_COMPLEXIDADE2";
         cmbavFuncaoapf_ativo2_Internalname = "vFUNCAOAPF_ATIVO2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavFuncaoapf_nome3_Internalname = "vFUNCAOAPF_NOME3";
         cmbavFuncaoapf_tipo3_Internalname = "vFUNCAOAPF_TIPO3";
         cmbavFuncaoapf_complexidade3_Internalname = "vFUNCAOAPF_COMPLEXIDADE3";
         cmbavFuncaoapf_ativo3_Internalname = "vFUNCAOAPF_ATIVO3";
         imgAdddynamicfilters3_Internalname = "ADDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         lblDynamicfiltersprefix4_Internalname = "DYNAMICFILTERSPREFIX4";
         cmbavDynamicfiltersselector4_Internalname = "vDYNAMICFILTERSSELECTOR4";
         lblDynamicfiltersmiddle4_Internalname = "DYNAMICFILTERSMIDDLE4";
         edtavFuncaoapf_nome4_Internalname = "vFUNCAOAPF_NOME4";
         cmbavFuncaoapf_tipo4_Internalname = "vFUNCAOAPF_TIPO4";
         cmbavFuncaoapf_complexidade4_Internalname = "vFUNCAOAPF_COMPLEXIDADE4";
         cmbavFuncaoapf_ativo4_Internalname = "vFUNCAOAPF_ATIVO4";
         imgRemovedynamicfilters4_Internalname = "REMOVEDYNAMICFILTERS4";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         bttBtnatualizar_Internalname = "BTNATUALIZAR";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtFuncaoAPF_Codigo_Internalname = "FUNCAOAPF_CODIGO";
         edtFuncaoAPF_Nome_Internalname = "FUNCAOAPF_NOME";
         cmbFuncaoAPF_Tipo_Internalname = "FUNCAOAPF_TIPO";
         edtFuncaoAPF_TD_Internalname = "FUNCAOAPF_TD";
         edtFuncaoAPF_AR_Internalname = "FUNCAOAPF_AR";
         cmbFuncaoAPF_Complexidade_Internalname = "FUNCAOAPF_COMPLEXIDADE";
         edtFuncaoAPF_PF_Internalname = "FUNCAOAPF_PF";
         edtFuncaoAPF_FunAPFPaiCod_Internalname = "FUNCAOAPF_FUNAPFPAICOD";
         edtFuncaoAPF_FunAPFPaiNom_Internalname = "FUNCAOAPF_FUNAPFPAINOM";
         cmbFuncaoAPF_Ativo_Internalname = "FUNCAOAPF_ATIVO";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         chkavDynamicfiltersenabled4_Internalname = "vDYNAMICFILTERSENABLED4";
         edtavTffuncaoapf_nome_Internalname = "vTFFUNCAOAPF_NOME";
         edtavTffuncaoapf_nome_sel_Internalname = "vTFFUNCAOAPF_NOME_SEL";
         edtavTffuncaoapf_td_Internalname = "vTFFUNCAOAPF_TD";
         edtavTffuncaoapf_td_to_Internalname = "vTFFUNCAOAPF_TD_TO";
         edtavTffuncaoapf_ar_Internalname = "vTFFUNCAOAPF_AR";
         edtavTffuncaoapf_ar_to_Internalname = "vTFFUNCAOAPF_AR_TO";
         edtavTffuncaoapf_pf_Internalname = "vTFFUNCAOAPF_PF";
         edtavTffuncaoapf_pf_to_Internalname = "vTFFUNCAOAPF_PF_TO";
         edtavTffuncaoapf_funapfpainom_Internalname = "vTFFUNCAOAPF_FUNAPFPAINOM";
         edtavTffuncaoapf_funapfpainom_sel_Internalname = "vTFFUNCAOAPF_FUNAPFPAINOM_SEL";
         Ddo_funcaoapf_nome_Internalname = "DDO_FUNCAOAPF_NOME";
         edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Internalname = "vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE";
         Ddo_funcaoapf_tipo_Internalname = "DDO_FUNCAOAPF_TIPO";
         edtavDdo_funcaoapf_tipotitlecontrolidtoreplace_Internalname = "vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE";
         Ddo_funcaoapf_td_Internalname = "DDO_FUNCAOAPF_TD";
         edtavDdo_funcaoapf_tdtitlecontrolidtoreplace_Internalname = "vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE";
         Ddo_funcaoapf_ar_Internalname = "DDO_FUNCAOAPF_AR";
         edtavDdo_funcaoapf_artitlecontrolidtoreplace_Internalname = "vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE";
         Ddo_funcaoapf_complexidade_Internalname = "DDO_FUNCAOAPF_COMPLEXIDADE";
         edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Internalname = "vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE";
         Ddo_funcaoapf_pf_Internalname = "DDO_FUNCAOAPF_PF";
         edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Internalname = "vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE";
         Ddo_funcaoapf_funapfpainom_Internalname = "DDO_FUNCAOAPF_FUNAPFPAINOM";
         edtavDdo_funcaoapf_funapfpainomtitlecontrolidtoreplace_Internalname = "vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE";
         Ddo_funcaoapf_ativo_Internalname = "DDO_FUNCAOAPF_ATIVO";
         edtavDdo_funcaoapf_ativotitlecontrolidtoreplace_Internalname = "vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbFuncaoAPF_Ativo_Jsonclick = "";
         edtFuncaoAPF_FunAPFPaiNom_Jsonclick = "";
         edtFuncaoAPF_FunAPFPaiCod_Jsonclick = "";
         edtFuncaoAPF_PF_Jsonclick = "";
         cmbFuncaoAPF_Complexidade_Jsonclick = "";
         edtFuncaoAPF_AR_Jsonclick = "";
         edtFuncaoAPF_TD_Jsonclick = "";
         cmbFuncaoAPF_Tipo_Jsonclick = "";
         edtFuncaoAPF_Nome_Jsonclick = "";
         edtFuncaoAPF_Codigo_Jsonclick = "";
         cmbavFuncaoapf_ativo4_Jsonclick = "";
         cmbavFuncaoapf_complexidade4_Jsonclick = "";
         cmbavFuncaoapf_tipo4_Jsonclick = "";
         cmbavDynamicfiltersselector4_Jsonclick = "";
         imgRemovedynamicfilters3_Visible = 1;
         imgAdddynamicfilters3_Visible = 1;
         cmbavFuncaoapf_ativo3_Jsonclick = "";
         cmbavFuncaoapf_complexidade3_Jsonclick = "";
         cmbavFuncaoapf_tipo3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavFuncaoapf_ativo2_Jsonclick = "";
         cmbavFuncaoapf_complexidade2_Jsonclick = "";
         cmbavFuncaoapf_tipo2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavFuncaoapf_ativo1_Jsonclick = "";
         cmbavFuncaoapf_complexidade1_Jsonclick = "";
         cmbavFuncaoapf_tipo1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtFuncaoAPF_FunAPFPaiNom_Link = "";
         edtFuncaoAPF_Nome_Link = "";
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         cmbFuncaoAPF_Ativo_Titleformat = 0;
         edtFuncaoAPF_FunAPFPaiNom_Titleformat = 0;
         edtFuncaoAPF_PF_Titleformat = 0;
         cmbFuncaoAPF_Complexidade_Titleformat = 0;
         edtFuncaoAPF_AR_Titleformat = 0;
         edtFuncaoAPF_TD_Titleformat = 0;
         cmbFuncaoAPF_Tipo_Titleformat = 0;
         edtFuncaoAPF_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavFuncaoapf_ativo4.Visible = 1;
         cmbavFuncaoapf_complexidade4.Visible = 1;
         cmbavFuncaoapf_tipo4.Visible = 1;
         edtavFuncaoapf_nome4_Visible = 1;
         cmbavFuncaoapf_ativo3.Visible = 1;
         cmbavFuncaoapf_complexidade3.Visible = 1;
         cmbavFuncaoapf_tipo3.Visible = 1;
         edtavFuncaoapf_nome3_Visible = 1;
         cmbavFuncaoapf_ativo2.Visible = 1;
         cmbavFuncaoapf_complexidade2.Visible = 1;
         cmbavFuncaoapf_tipo2.Visible = 1;
         edtavFuncaoapf_nome2_Visible = 1;
         cmbavFuncaoapf_ativo1.Visible = 1;
         cmbavFuncaoapf_complexidade1.Visible = 1;
         cmbavFuncaoapf_tipo1.Visible = 1;
         edtavFuncaoapf_nome1_Visible = 1;
         cmbFuncaoAPF_Ativo.Title.Text = "Status da Fun��o";
         edtFuncaoAPF_FunAPFPaiNom_Title = "Vinculada com";
         edtFuncaoAPF_PF_Title = "PF";
         cmbFuncaoAPF_Complexidade.Title.Text = "Complexidade";
         edtFuncaoAPF_AR_Title = "ALR";
         edtFuncaoAPF_TD_Title = "DER";
         cmbFuncaoAPF_Tipo.Title.Text = "Tipo";
         edtFuncaoAPF_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled4.Caption = "";
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_funcaoapf_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapf_funapfpainomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapf_artitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapf_tdtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapf_tipotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Visible = 1;
         edtavTffuncaoapf_funapfpainom_sel_Visible = 1;
         edtavTffuncaoapf_funapfpainom_Visible = 1;
         edtavTffuncaoapf_pf_to_Jsonclick = "";
         edtavTffuncaoapf_pf_to_Visible = 1;
         edtavTffuncaoapf_pf_Jsonclick = "";
         edtavTffuncaoapf_pf_Visible = 1;
         edtavTffuncaoapf_ar_to_Jsonclick = "";
         edtavTffuncaoapf_ar_to_Visible = 1;
         edtavTffuncaoapf_ar_Jsonclick = "";
         edtavTffuncaoapf_ar_Visible = 1;
         edtavTffuncaoapf_td_to_Jsonclick = "";
         edtavTffuncaoapf_td_to_Visible = 1;
         edtavTffuncaoapf_td_Jsonclick = "";
         edtavTffuncaoapf_td_Visible = 1;
         edtavTffuncaoapf_nome_sel_Visible = 1;
         edtavTffuncaoapf_nome_Visible = 1;
         chkavDynamicfiltersenabled4.Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_funcaoapf_ativo_Searchbuttontext = "Filtrar Selecionados";
         Ddo_funcaoapf_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapf_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaoapf_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_funcaoapf_ativo_Datalistfixedvalues = "A:Ativa,E:Exclu�da,R:Rejeitada";
         Ddo_funcaoapf_ativo_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_funcaoapf_ativo_Datalisttype = "FixedValues";
         Ddo_funcaoapf_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaoapf_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_funcaoapf_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaoapf_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaoapf_ativo_Titlecontrolidtoreplace = "";
         Ddo_funcaoapf_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapf_ativo_Cls = "ColumnSettings";
         Ddo_funcaoapf_ativo_Tooltip = "Op��es";
         Ddo_funcaoapf_ativo_Caption = "";
         Ddo_funcaoapf_funapfpainom_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapf_funapfpainom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaoapf_funapfpainom_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapf_funapfpainom_Loadingdata = "Carregando dados...";
         Ddo_funcaoapf_funapfpainom_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaoapf_funapfpainom_Sortasc = "Ordenar de A � Z";
         Ddo_funcaoapf_funapfpainom_Datalistupdateminimumcharacters = 0;
         Ddo_funcaoapf_funapfpainom_Datalistproc = "GetWWFuncaoAPFFilterData";
         Ddo_funcaoapf_funapfpainom_Datalisttype = "Dynamic";
         Ddo_funcaoapf_funapfpainom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaoapf_funapfpainom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcaoapf_funapfpainom_Filtertype = "Character";
         Ddo_funcaoapf_funapfpainom_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapf_funapfpainom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaoapf_funapfpainom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaoapf_funapfpainom_Titlecontrolidtoreplace = "";
         Ddo_funcaoapf_funapfpainom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapf_funapfpainom_Cls = "ColumnSettings";
         Ddo_funcaoapf_funapfpainom_Tooltip = "Op��es";
         Ddo_funcaoapf_funapfpainom_Caption = "";
         Ddo_funcaoapf_pf_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapf_pf_Rangefilterto = "At�";
         Ddo_funcaoapf_pf_Rangefilterfrom = "Desde";
         Ddo_funcaoapf_pf_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapf_pf_Includedatalist = Convert.ToBoolean( 0);
         Ddo_funcaoapf_pf_Filterisrange = Convert.ToBoolean( -1);
         Ddo_funcaoapf_pf_Filtertype = "Numeric";
         Ddo_funcaoapf_pf_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapf_pf_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_funcaoapf_pf_Includesortasc = Convert.ToBoolean( 0);
         Ddo_funcaoapf_pf_Titlecontrolidtoreplace = "";
         Ddo_funcaoapf_pf_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapf_pf_Cls = "ColumnSettings";
         Ddo_funcaoapf_pf_Tooltip = "Op��es";
         Ddo_funcaoapf_pf_Caption = "";
         Ddo_funcaoapf_complexidade_Searchbuttontext = "Filtrar Selecionados";
         Ddo_funcaoapf_complexidade_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapf_complexidade_Datalistfixedvalues = "E:,B:Baixa,M:M�dia,A:Alta";
         Ddo_funcaoapf_complexidade_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_funcaoapf_complexidade_Datalisttype = "FixedValues";
         Ddo_funcaoapf_complexidade_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaoapf_complexidade_Includefilter = Convert.ToBoolean( 0);
         Ddo_funcaoapf_complexidade_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_funcaoapf_complexidade_Includesortasc = Convert.ToBoolean( 0);
         Ddo_funcaoapf_complexidade_Titlecontrolidtoreplace = "";
         Ddo_funcaoapf_complexidade_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapf_complexidade_Cls = "ColumnSettings";
         Ddo_funcaoapf_complexidade_Tooltip = "Op��es";
         Ddo_funcaoapf_complexidade_Caption = "";
         Ddo_funcaoapf_ar_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapf_ar_Rangefilterto = "At�";
         Ddo_funcaoapf_ar_Rangefilterfrom = "Desde";
         Ddo_funcaoapf_ar_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapf_ar_Includedatalist = Convert.ToBoolean( 0);
         Ddo_funcaoapf_ar_Filterisrange = Convert.ToBoolean( -1);
         Ddo_funcaoapf_ar_Filtertype = "Numeric";
         Ddo_funcaoapf_ar_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapf_ar_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_funcaoapf_ar_Includesortasc = Convert.ToBoolean( 0);
         Ddo_funcaoapf_ar_Titlecontrolidtoreplace = "";
         Ddo_funcaoapf_ar_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapf_ar_Cls = "ColumnSettings";
         Ddo_funcaoapf_ar_Tooltip = "Op��es";
         Ddo_funcaoapf_ar_Caption = "";
         Ddo_funcaoapf_td_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapf_td_Rangefilterto = "At�";
         Ddo_funcaoapf_td_Rangefilterfrom = "Desde";
         Ddo_funcaoapf_td_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapf_td_Includedatalist = Convert.ToBoolean( 0);
         Ddo_funcaoapf_td_Filterisrange = Convert.ToBoolean( -1);
         Ddo_funcaoapf_td_Filtertype = "Numeric";
         Ddo_funcaoapf_td_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapf_td_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_funcaoapf_td_Includesortasc = Convert.ToBoolean( 0);
         Ddo_funcaoapf_td_Titlecontrolidtoreplace = "";
         Ddo_funcaoapf_td_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapf_td_Cls = "ColumnSettings";
         Ddo_funcaoapf_td_Tooltip = "Op��es";
         Ddo_funcaoapf_td_Caption = "";
         Ddo_funcaoapf_tipo_Searchbuttontext = "Filtrar Selecionados";
         Ddo_funcaoapf_tipo_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapf_tipo_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaoapf_tipo_Sortasc = "Ordenar de A � Z";
         Ddo_funcaoapf_tipo_Datalistfixedvalues = "EE:EE,SE:SE,CE:CE,NM:NM";
         Ddo_funcaoapf_tipo_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_funcaoapf_tipo_Datalisttype = "FixedValues";
         Ddo_funcaoapf_tipo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaoapf_tipo_Includefilter = Convert.ToBoolean( 0);
         Ddo_funcaoapf_tipo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaoapf_tipo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaoapf_tipo_Titlecontrolidtoreplace = "";
         Ddo_funcaoapf_tipo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapf_tipo_Cls = "ColumnSettings";
         Ddo_funcaoapf_tipo_Tooltip = "Op��es";
         Ddo_funcaoapf_tipo_Caption = "";
         Ddo_funcaoapf_nome_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapf_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaoapf_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapf_nome_Loadingdata = "Carregando dados...";
         Ddo_funcaoapf_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaoapf_nome_Sortasc = "Ordenar de A � Z";
         Ddo_funcaoapf_nome_Datalistupdateminimumcharacters = 0;
         Ddo_funcaoapf_nome_Datalistproc = "GetWWFuncaoAPFFilterData";
         Ddo_funcaoapf_nome_Datalisttype = "Dynamic";
         Ddo_funcaoapf_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaoapf_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcaoapf_nome_Filtertype = "Character";
         Ddo_funcaoapf_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapf_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaoapf_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaoapf_nome_Titlecontrolidtoreplace = "";
         Ddo_funcaoapf_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapf_nome_Cls = "ColumnSettings";
         Ddo_funcaoapf_nome_Tooltip = "Op��es";
         Ddo_funcaoapf_nome_Caption = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Fun��es APF - An�lise de Ponto de Fun��o";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV57FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV76FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV77FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV55FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV78FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV79FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV56FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV80FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV81FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV82FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV83DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV84DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV85FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV86FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV87FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV88FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'AV92TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV93TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV97TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV100TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV101TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV104TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV105TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV109TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV112TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV113TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV116TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV117TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV121TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV91FuncaoAPF_NomeTitleFilterData',fld:'vFUNCAOAPF_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV95FuncaoAPF_TipoTitleFilterData',fld:'vFUNCAOAPF_TIPOTITLEFILTERDATA',pic:'',nv:null},{av:'AV99FuncaoAPF_TDTitleFilterData',fld:'vFUNCAOAPF_TDTITLEFILTERDATA',pic:'',nv:null},{av:'AV103FuncaoAPF_ARTitleFilterData',fld:'vFUNCAOAPF_ARTITLEFILTERDATA',pic:'',nv:null},{av:'AV107FuncaoAPF_ComplexidadeTitleFilterData',fld:'vFUNCAOAPF_COMPLEXIDADETITLEFILTERDATA',pic:'',nv:null},{av:'AV111FuncaoAPF_PFTitleFilterData',fld:'vFUNCAOAPF_PFTITLEFILTERDATA',pic:'',nv:null},{av:'AV115FuncaoAPF_FunAPFPaiNomTitleFilterData',fld:'vFUNCAOAPF_FUNAPFPAINOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV119FuncaoAPF_AtivoTitleFilterData',fld:'vFUNCAOAPF_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'edtFuncaoAPF_Nome_Titleformat',ctrl:'FUNCAOAPF_NOME',prop:'Titleformat'},{av:'edtFuncaoAPF_Nome_Title',ctrl:'FUNCAOAPF_NOME',prop:'Title'},{av:'cmbFuncaoAPF_Tipo'},{av:'edtFuncaoAPF_TD_Titleformat',ctrl:'FUNCAOAPF_TD',prop:'Titleformat'},{av:'edtFuncaoAPF_TD_Title',ctrl:'FUNCAOAPF_TD',prop:'Title'},{av:'edtFuncaoAPF_AR_Titleformat',ctrl:'FUNCAOAPF_AR',prop:'Titleformat'},{av:'edtFuncaoAPF_AR_Title',ctrl:'FUNCAOAPF_AR',prop:'Title'},{av:'cmbFuncaoAPF_Complexidade'},{av:'edtFuncaoAPF_PF_Titleformat',ctrl:'FUNCAOAPF_PF',prop:'Titleformat'},{av:'edtFuncaoAPF_PF_Title',ctrl:'FUNCAOAPF_PF',prop:'Title'},{av:'edtFuncaoAPF_FunAPFPaiNom_Titleformat',ctrl:'FUNCAOAPF_FUNAPFPAINOM',prop:'Titleformat'},{av:'edtFuncaoAPF_FunAPFPaiNom_Title',ctrl:'FUNCAOAPF_FUNAPFPAINOM',prop:'Title'},{av:'cmbFuncaoAPF_Ativo'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("DDO_FUNCAOAPF_NOME.ONOPTIONCLICKED","{handler:'E113Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV76FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV55FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV78FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV56FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV80FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV82FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV84DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV85FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV86FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV88FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV83DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV92TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV93TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV116TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV117TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV79FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV81FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV87FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV97TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV100TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV101TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV104TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV105TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV109TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV112TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV113TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV121TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV57FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_funcaoapf_nome_Activeeventkey',ctrl:'DDO_FUNCAOAPF_NOME',prop:'ActiveEventKey'},{av:'Ddo_funcaoapf_nome_Filteredtext_get',ctrl:'DDO_FUNCAOAPF_NOME',prop:'FilteredText_get'},{av:'Ddo_funcaoapf_nome_Selectedvalue_get',ctrl:'DDO_FUNCAOAPF_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaoapf_nome_Sortedstatus',ctrl:'DDO_FUNCAOAPF_NOME',prop:'SortedStatus'},{av:'AV92TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV93TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'Ddo_funcaoapf_tipo_Sortedstatus',ctrl:'DDO_FUNCAOAPF_TIPO',prop:'SortedStatus'},{av:'Ddo_funcaoapf_funapfpainom_Sortedstatus',ctrl:'DDO_FUNCAOAPF_FUNAPFPAINOM',prop:'SortedStatus'},{av:'Ddo_funcaoapf_ativo_Sortedstatus',ctrl:'DDO_FUNCAOAPF_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAOAPF_TIPO.ONOPTIONCLICKED","{handler:'E123Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV76FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV55FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV78FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV56FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV80FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV82FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV84DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV85FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV86FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV88FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV83DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV92TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV93TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV116TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV117TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV79FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV81FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV87FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV97TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV100TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV101TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV104TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV105TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV109TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV112TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV113TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV121TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV57FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_funcaoapf_tipo_Activeeventkey',ctrl:'DDO_FUNCAOAPF_TIPO',prop:'ActiveEventKey'},{av:'Ddo_funcaoapf_tipo_Selectedvalue_get',ctrl:'DDO_FUNCAOAPF_TIPO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaoapf_tipo_Sortedstatus',ctrl:'DDO_FUNCAOAPF_TIPO',prop:'SortedStatus'},{av:'AV97TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'Ddo_funcaoapf_nome_Sortedstatus',ctrl:'DDO_FUNCAOAPF_NOME',prop:'SortedStatus'},{av:'Ddo_funcaoapf_funapfpainom_Sortedstatus',ctrl:'DDO_FUNCAOAPF_FUNAPFPAINOM',prop:'SortedStatus'},{av:'Ddo_funcaoapf_ativo_Sortedstatus',ctrl:'DDO_FUNCAOAPF_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAOAPF_TD.ONOPTIONCLICKED","{handler:'E133Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV76FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV55FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV78FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV56FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV80FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV82FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV84DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV85FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV86FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV88FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV83DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV92TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV93TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV116TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV117TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV79FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV81FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV87FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV97TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV100TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV101TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV104TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV105TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV109TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV112TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV113TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV121TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV57FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_funcaoapf_td_Activeeventkey',ctrl:'DDO_FUNCAOAPF_TD',prop:'ActiveEventKey'},{av:'Ddo_funcaoapf_td_Filteredtext_get',ctrl:'DDO_FUNCAOAPF_TD',prop:'FilteredText_get'},{av:'Ddo_funcaoapf_td_Filteredtextto_get',ctrl:'DDO_FUNCAOAPF_TD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV100TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV101TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("DDO_FUNCAOAPF_AR.ONOPTIONCLICKED","{handler:'E143Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV76FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV55FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV78FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV56FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV80FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV82FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV84DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV85FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV86FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV88FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV83DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV92TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV93TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV116TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV117TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV79FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV81FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV87FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV97TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV100TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV101TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV104TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV105TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV109TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV112TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV113TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV121TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV57FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_funcaoapf_ar_Activeeventkey',ctrl:'DDO_FUNCAOAPF_AR',prop:'ActiveEventKey'},{av:'Ddo_funcaoapf_ar_Filteredtext_get',ctrl:'DDO_FUNCAOAPF_AR',prop:'FilteredText_get'},{av:'Ddo_funcaoapf_ar_Filteredtextto_get',ctrl:'DDO_FUNCAOAPF_AR',prop:'FilteredTextTo_get'}],oparms:[{av:'AV104TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV105TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("DDO_FUNCAOAPF_COMPLEXIDADE.ONOPTIONCLICKED","{handler:'E153Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV76FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV55FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV78FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV56FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV80FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV82FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV84DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV85FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV86FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV88FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV83DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV92TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV93TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV116TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV117TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV79FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV81FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV87FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV97TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV100TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV101TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV104TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV105TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV109TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV112TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV113TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV121TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV57FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_funcaoapf_complexidade_Activeeventkey',ctrl:'DDO_FUNCAOAPF_COMPLEXIDADE',prop:'ActiveEventKey'},{av:'Ddo_funcaoapf_complexidade_Selectedvalue_get',ctrl:'DDO_FUNCAOAPF_COMPLEXIDADE',prop:'SelectedValue_get'}],oparms:[{av:'AV109TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null}]}");
         setEventMetadata("DDO_FUNCAOAPF_PF.ONOPTIONCLICKED","{handler:'E163Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV76FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV55FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV78FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV56FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV80FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV82FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV84DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV85FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV86FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV88FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV83DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV92TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV93TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV116TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV117TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV79FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV81FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV87FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV97TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV100TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV101TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV104TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV105TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV109TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV112TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV113TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV121TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV57FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_funcaoapf_pf_Activeeventkey',ctrl:'DDO_FUNCAOAPF_PF',prop:'ActiveEventKey'},{av:'Ddo_funcaoapf_pf_Filteredtext_get',ctrl:'DDO_FUNCAOAPF_PF',prop:'FilteredText_get'},{av:'Ddo_funcaoapf_pf_Filteredtextto_get',ctrl:'DDO_FUNCAOAPF_PF',prop:'FilteredTextTo_get'}],oparms:[{av:'AV112TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV113TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         setEventMetadata("DDO_FUNCAOAPF_FUNAPFPAINOM.ONOPTIONCLICKED","{handler:'E173Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV76FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV55FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV78FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV56FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV80FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV82FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV84DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV85FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV86FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV88FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV83DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV92TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV93TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV116TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV117TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV79FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV81FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV87FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV97TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV100TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV101TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV104TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV105TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV109TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV112TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV113TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV121TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV57FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_funcaoapf_funapfpainom_Activeeventkey',ctrl:'DDO_FUNCAOAPF_FUNAPFPAINOM',prop:'ActiveEventKey'},{av:'Ddo_funcaoapf_funapfpainom_Filteredtext_get',ctrl:'DDO_FUNCAOAPF_FUNAPFPAINOM',prop:'FilteredText_get'},{av:'Ddo_funcaoapf_funapfpainom_Selectedvalue_get',ctrl:'DDO_FUNCAOAPF_FUNAPFPAINOM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaoapf_funapfpainom_Sortedstatus',ctrl:'DDO_FUNCAOAPF_FUNAPFPAINOM',prop:'SortedStatus'},{av:'AV116TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV117TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'Ddo_funcaoapf_nome_Sortedstatus',ctrl:'DDO_FUNCAOAPF_NOME',prop:'SortedStatus'},{av:'Ddo_funcaoapf_tipo_Sortedstatus',ctrl:'DDO_FUNCAOAPF_TIPO',prop:'SortedStatus'},{av:'Ddo_funcaoapf_ativo_Sortedstatus',ctrl:'DDO_FUNCAOAPF_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAOAPF_ATIVO.ONOPTIONCLICKED","{handler:'E183Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV76FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV55FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV78FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV56FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV80FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV82FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV84DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV85FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV86FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV88FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV83DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV92TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV93TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV116TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV117TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV79FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV81FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV87FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV97TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV100TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV101TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV104TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV105TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV109TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV112TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV113TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV121TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV57FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_funcaoapf_ativo_Activeeventkey',ctrl:'DDO_FUNCAOAPF_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_funcaoapf_ativo_Selectedvalue_get',ctrl:'DDO_FUNCAOAPF_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaoapf_ativo_Sortedstatus',ctrl:'DDO_FUNCAOAPF_ATIVO',prop:'SortedStatus'},{av:'AV121TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'Ddo_funcaoapf_nome_Sortedstatus',ctrl:'DDO_FUNCAOAPF_NOME',prop:'SortedStatus'},{av:'Ddo_funcaoapf_tipo_Sortedstatus',ctrl:'DDO_FUNCAOAPF_TIPO',prop:'SortedStatus'},{av:'Ddo_funcaoapf_funapfpainom_Sortedstatus',ctrl:'DDO_FUNCAOAPF_FUNAPFPAINOM',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E353Z2',iparms:[{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV57FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV64Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'edtFuncaoAPF_Nome_Link',ctrl:'FUNCAOAPF_NOME',prop:'Link'},{av:'edtFuncaoAPF_FunAPFPaiNom_Link',ctrl:'FUNCAOAPF_FUNAPFPAINOM',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E193Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV76FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV55FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV78FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV56FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV80FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV82FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV84DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV85FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV86FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV88FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV83DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV92TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV93TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV116TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV117TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV79FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV81FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV87FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV97TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV100TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV101TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV104TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV105TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV109TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV112TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV113TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV121TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV57FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E263Z2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E203Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV76FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV55FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV78FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV56FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV80FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV82FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV84DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV85FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV86FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV88FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV83DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV92TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV93TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV116TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV117TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV79FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV81FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV87FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV97TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV100TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV101TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV104TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV105TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV109TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV112TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV113TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV121TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV57FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV83DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV84DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV85FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'imgAdddynamicfilters3_Visible',ctrl:'ADDDYNAMICFILTERS3',prop:'Visible'},{av:'imgRemovedynamicfilters3_Visible',ctrl:'REMOVEDYNAMICFILTERS3',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV76FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV77FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV55FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV78FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV79FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV56FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV80FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV81FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV82FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV86FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV87FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV88FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'cmbavFuncaoapf_tipo2'},{av:'cmbavFuncaoapf_complexidade2'},{av:'cmbavFuncaoapf_ativo2'},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'cmbavFuncaoapf_tipo3'},{av:'cmbavFuncaoapf_complexidade3'},{av:'cmbavFuncaoapf_ativo3'},{av:'edtavFuncaoapf_nome4_Visible',ctrl:'vFUNCAOAPF_NOME4',prop:'Visible'},{av:'cmbavFuncaoapf_tipo4'},{av:'cmbavFuncaoapf_complexidade4'},{av:'cmbavFuncaoapf_ativo4'},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'cmbavFuncaoapf_tipo1'},{av:'cmbavFuncaoapf_complexidade1'},{av:'cmbavFuncaoapf_ativo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E273Z2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'cmbavFuncaoapf_tipo1'},{av:'cmbavFuncaoapf_complexidade1'},{av:'cmbavFuncaoapf_ativo1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E283Z2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E213Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV76FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV55FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV78FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV56FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV80FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV82FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV84DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV85FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV86FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV88FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV83DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV92TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV93TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV116TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV117TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV79FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV81FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV87FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV97TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV100TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV101TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV104TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV105TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV109TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV112TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV113TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV121TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV57FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV83DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV84DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV85FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'imgAdddynamicfilters3_Visible',ctrl:'ADDDYNAMICFILTERS3',prop:'Visible'},{av:'imgRemovedynamicfilters3_Visible',ctrl:'REMOVEDYNAMICFILTERS3',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV76FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV77FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV55FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV78FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV79FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV56FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV80FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV81FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV82FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV86FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV87FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV88FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'cmbavFuncaoapf_tipo2'},{av:'cmbavFuncaoapf_complexidade2'},{av:'cmbavFuncaoapf_ativo2'},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'cmbavFuncaoapf_tipo3'},{av:'cmbavFuncaoapf_complexidade3'},{av:'cmbavFuncaoapf_ativo3'},{av:'edtavFuncaoapf_nome4_Visible',ctrl:'vFUNCAOAPF_NOME4',prop:'Visible'},{av:'cmbavFuncaoapf_tipo4'},{av:'cmbavFuncaoapf_complexidade4'},{av:'cmbavFuncaoapf_ativo4'},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'cmbavFuncaoapf_tipo1'},{av:'cmbavFuncaoapf_complexidade1'},{av:'cmbavFuncaoapf_ativo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E293Z2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'cmbavFuncaoapf_tipo2'},{av:'cmbavFuncaoapf_complexidade2'},{av:'cmbavFuncaoapf_ativo2'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS3'","{handler:'E303Z2',iparms:[],oparms:[{av:'AV83DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'imgAdddynamicfilters3_Visible',ctrl:'ADDDYNAMICFILTERS3',prop:'Visible'},{av:'imgRemovedynamicfilters3_Visible',ctrl:'REMOVEDYNAMICFILTERS3',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E223Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV76FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV55FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV78FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV56FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV80FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV82FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV84DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV85FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV86FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV88FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV83DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV92TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV93TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV116TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV117TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV79FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV81FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV87FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV97TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV100TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV101TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV104TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV105TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV109TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV112TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV113TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV121TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV57FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV83DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV84DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV85FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'imgAdddynamicfilters3_Visible',ctrl:'ADDDYNAMICFILTERS3',prop:'Visible'},{av:'imgRemovedynamicfilters3_Visible',ctrl:'REMOVEDYNAMICFILTERS3',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV76FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV77FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV55FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV78FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV79FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV56FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV80FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV81FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV82FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV86FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV87FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV88FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'cmbavFuncaoapf_tipo2'},{av:'cmbavFuncaoapf_complexidade2'},{av:'cmbavFuncaoapf_ativo2'},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'cmbavFuncaoapf_tipo3'},{av:'cmbavFuncaoapf_complexidade3'},{av:'cmbavFuncaoapf_ativo3'},{av:'edtavFuncaoapf_nome4_Visible',ctrl:'vFUNCAOAPF_NOME4',prop:'Visible'},{av:'cmbavFuncaoapf_tipo4'},{av:'cmbavFuncaoapf_complexidade4'},{av:'cmbavFuncaoapf_ativo4'},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'cmbavFuncaoapf_tipo1'},{av:'cmbavFuncaoapf_complexidade1'},{av:'cmbavFuncaoapf_ativo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E313Z2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'cmbavFuncaoapf_tipo3'},{av:'cmbavFuncaoapf_complexidade3'},{av:'cmbavFuncaoapf_ativo3'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS4'","{handler:'E233Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV76FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV55FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV78FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV56FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV80FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV82FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV84DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV85FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV86FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV88FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV83DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV92TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV93TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV116TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV117TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV79FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV81FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV87FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV97TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV100TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV101TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV104TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV105TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV109TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV112TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV113TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV121TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV57FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV83DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV84DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV85FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'imgAdddynamicfilters3_Visible',ctrl:'ADDDYNAMICFILTERS3',prop:'Visible'},{av:'imgRemovedynamicfilters3_Visible',ctrl:'REMOVEDYNAMICFILTERS3',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV76FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV77FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV55FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV78FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV79FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV56FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV80FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV81FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV82FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV86FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV87FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV88FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'cmbavFuncaoapf_tipo2'},{av:'cmbavFuncaoapf_complexidade2'},{av:'cmbavFuncaoapf_ativo2'},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'cmbavFuncaoapf_tipo3'},{av:'cmbavFuncaoapf_complexidade3'},{av:'cmbavFuncaoapf_ativo3'},{av:'edtavFuncaoapf_nome4_Visible',ctrl:'vFUNCAOAPF_NOME4',prop:'Visible'},{av:'cmbavFuncaoapf_tipo4'},{av:'cmbavFuncaoapf_complexidade4'},{av:'cmbavFuncaoapf_ativo4'},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'cmbavFuncaoapf_tipo1'},{av:'cmbavFuncaoapf_complexidade1'},{av:'cmbavFuncaoapf_ativo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR4.CLICK","{handler:'E323Z2',iparms:[{av:'AV84DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''}],oparms:[{av:'edtavFuncaoapf_nome4_Visible',ctrl:'vFUNCAOAPF_NOME4',prop:'Visible'},{av:'cmbavFuncaoapf_tipo4'},{av:'cmbavFuncaoapf_complexidade4'},{av:'cmbavFuncaoapf_ativo4'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E243Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV76FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV55FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV78FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV56FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV80FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV82FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV84DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV85FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV86FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV88FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV83DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV92TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV93TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV116TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV117TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV79FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV81FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV87FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV97TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV100TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV101TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV104TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV105TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV109TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV112TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV113TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV121TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV57FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV92TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'Ddo_funcaoapf_nome_Filteredtext_set',ctrl:'DDO_FUNCAOAPF_NOME',prop:'FilteredText_set'},{av:'AV93TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'Ddo_funcaoapf_nome_Selectedvalue_set',ctrl:'DDO_FUNCAOAPF_NOME',prop:'SelectedValue_set'},{av:'AV97TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'Ddo_funcaoapf_tipo_Selectedvalue_set',ctrl:'DDO_FUNCAOAPF_TIPO',prop:'SelectedValue_set'},{av:'AV100TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'Ddo_funcaoapf_td_Filteredtext_set',ctrl:'DDO_FUNCAOAPF_TD',prop:'FilteredText_set'},{av:'AV101TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'Ddo_funcaoapf_td_Filteredtextto_set',ctrl:'DDO_FUNCAOAPF_TD',prop:'FilteredTextTo_set'},{av:'AV104TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'Ddo_funcaoapf_ar_Filteredtext_set',ctrl:'DDO_FUNCAOAPF_AR',prop:'FilteredText_set'},{av:'AV105TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'Ddo_funcaoapf_ar_Filteredtextto_set',ctrl:'DDO_FUNCAOAPF_AR',prop:'FilteredTextTo_set'},{av:'AV109TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'Ddo_funcaoapf_complexidade_Selectedvalue_set',ctrl:'DDO_FUNCAOAPF_COMPLEXIDADE',prop:'SelectedValue_set'},{av:'AV112TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_funcaoapf_pf_Filteredtext_set',ctrl:'DDO_FUNCAOAPF_PF',prop:'FilteredText_set'},{av:'AV113TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_funcaoapf_pf_Filteredtextto_set',ctrl:'DDO_FUNCAOAPF_PF',prop:'FilteredTextTo_set'},{av:'AV116TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'Ddo_funcaoapf_funapfpainom_Filteredtext_set',ctrl:'DDO_FUNCAOAPF_FUNAPFPAINOM',prop:'FilteredText_set'},{av:'AV117TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'Ddo_funcaoapf_funapfpainom_Selectedvalue_set',ctrl:'DDO_FUNCAOAPF_FUNAPFPAINOM',prop:'SelectedValue_set'},{av:'AV121TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'Ddo_funcaoapf_ativo_Selectedvalue_set',ctrl:'DDO_FUNCAOAPF_ATIVO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'cmbavFuncaoapf_tipo1'},{av:'cmbavFuncaoapf_complexidade1'},{av:'cmbavFuncaoapf_ativo1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV83DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV84DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV85FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'imgAdddynamicfilters3_Visible',ctrl:'ADDDYNAMICFILTERS3',prop:'Visible'},{av:'imgRemovedynamicfilters3_Visible',ctrl:'REMOVEDYNAMICFILTERS3',prop:'Visible'},{av:'AV76FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV77FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV55FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV78FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV79FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV56FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV80FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV81FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV82FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV86FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV87FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV88FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'cmbavFuncaoapf_tipo2'},{av:'cmbavFuncaoapf_complexidade2'},{av:'cmbavFuncaoapf_ativo2'},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'cmbavFuncaoapf_tipo3'},{av:'cmbavFuncaoapf_complexidade3'},{av:'cmbavFuncaoapf_ativo3'},{av:'edtavFuncaoapf_nome4_Visible',ctrl:'vFUNCAOAPF_NOME4',prop:'Visible'},{av:'cmbavFuncaoapf_tipo4'},{av:'cmbavFuncaoapf_complexidade4'},{av:'cmbavFuncaoapf_ativo4'}]}");
         setEventMetadata("'DOATUALIZAR'","{handler:'E363Z1',iparms:[],oparms:[]}");
         setEventMetadata("'DOINSERT'","{handler:'E253Z2',iparms:[{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV57FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV57FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRID_FIRSTPAGE","{handler:'subgrid_firstpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV57FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV76FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV77FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV55FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV78FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV79FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV56FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV80FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV81FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV82FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV83DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV84DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV85FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV86FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV87FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV88FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'AV92TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV93TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV97TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV100TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV101TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV104TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV105TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV109TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV112TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV113TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV116TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV117TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV121TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV91FuncaoAPF_NomeTitleFilterData',fld:'vFUNCAOAPF_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV95FuncaoAPF_TipoTitleFilterData',fld:'vFUNCAOAPF_TIPOTITLEFILTERDATA',pic:'',nv:null},{av:'AV99FuncaoAPF_TDTitleFilterData',fld:'vFUNCAOAPF_TDTITLEFILTERDATA',pic:'',nv:null},{av:'AV103FuncaoAPF_ARTitleFilterData',fld:'vFUNCAOAPF_ARTITLEFILTERDATA',pic:'',nv:null},{av:'AV107FuncaoAPF_ComplexidadeTitleFilterData',fld:'vFUNCAOAPF_COMPLEXIDADETITLEFILTERDATA',pic:'',nv:null},{av:'AV111FuncaoAPF_PFTitleFilterData',fld:'vFUNCAOAPF_PFTITLEFILTERDATA',pic:'',nv:null},{av:'AV115FuncaoAPF_FunAPFPaiNomTitleFilterData',fld:'vFUNCAOAPF_FUNAPFPAINOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV119FuncaoAPF_AtivoTitleFilterData',fld:'vFUNCAOAPF_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'edtFuncaoAPF_Nome_Titleformat',ctrl:'FUNCAOAPF_NOME',prop:'Titleformat'},{av:'edtFuncaoAPF_Nome_Title',ctrl:'FUNCAOAPF_NOME',prop:'Title'},{av:'cmbFuncaoAPF_Tipo'},{av:'edtFuncaoAPF_TD_Titleformat',ctrl:'FUNCAOAPF_TD',prop:'Titleformat'},{av:'edtFuncaoAPF_TD_Title',ctrl:'FUNCAOAPF_TD',prop:'Title'},{av:'edtFuncaoAPF_AR_Titleformat',ctrl:'FUNCAOAPF_AR',prop:'Titleformat'},{av:'edtFuncaoAPF_AR_Title',ctrl:'FUNCAOAPF_AR',prop:'Title'},{av:'cmbFuncaoAPF_Complexidade'},{av:'edtFuncaoAPF_PF_Titleformat',ctrl:'FUNCAOAPF_PF',prop:'Titleformat'},{av:'edtFuncaoAPF_PF_Title',ctrl:'FUNCAOAPF_PF',prop:'Title'},{av:'edtFuncaoAPF_FunAPFPaiNom_Titleformat',ctrl:'FUNCAOAPF_FUNAPFPAINOM',prop:'Titleformat'},{av:'edtFuncaoAPF_FunAPFPaiNom_Title',ctrl:'FUNCAOAPF_FUNAPFPAINOM',prop:'Title'},{av:'cmbFuncaoAPF_Ativo'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_PREVPAGE","{handler:'subgrid_previouspage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV57FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV76FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV77FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV55FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV78FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV79FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV56FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV80FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV81FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV82FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV83DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV84DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV85FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV86FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV87FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV88FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'AV92TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV93TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV97TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV100TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV101TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV104TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV105TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV109TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV112TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV113TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV116TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV117TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV121TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV91FuncaoAPF_NomeTitleFilterData',fld:'vFUNCAOAPF_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV95FuncaoAPF_TipoTitleFilterData',fld:'vFUNCAOAPF_TIPOTITLEFILTERDATA',pic:'',nv:null},{av:'AV99FuncaoAPF_TDTitleFilterData',fld:'vFUNCAOAPF_TDTITLEFILTERDATA',pic:'',nv:null},{av:'AV103FuncaoAPF_ARTitleFilterData',fld:'vFUNCAOAPF_ARTITLEFILTERDATA',pic:'',nv:null},{av:'AV107FuncaoAPF_ComplexidadeTitleFilterData',fld:'vFUNCAOAPF_COMPLEXIDADETITLEFILTERDATA',pic:'',nv:null},{av:'AV111FuncaoAPF_PFTitleFilterData',fld:'vFUNCAOAPF_PFTITLEFILTERDATA',pic:'',nv:null},{av:'AV115FuncaoAPF_FunAPFPaiNomTitleFilterData',fld:'vFUNCAOAPF_FUNAPFPAINOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV119FuncaoAPF_AtivoTitleFilterData',fld:'vFUNCAOAPF_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'edtFuncaoAPF_Nome_Titleformat',ctrl:'FUNCAOAPF_NOME',prop:'Titleformat'},{av:'edtFuncaoAPF_Nome_Title',ctrl:'FUNCAOAPF_NOME',prop:'Title'},{av:'cmbFuncaoAPF_Tipo'},{av:'edtFuncaoAPF_TD_Titleformat',ctrl:'FUNCAOAPF_TD',prop:'Titleformat'},{av:'edtFuncaoAPF_TD_Title',ctrl:'FUNCAOAPF_TD',prop:'Title'},{av:'edtFuncaoAPF_AR_Titleformat',ctrl:'FUNCAOAPF_AR',prop:'Titleformat'},{av:'edtFuncaoAPF_AR_Title',ctrl:'FUNCAOAPF_AR',prop:'Title'},{av:'cmbFuncaoAPF_Complexidade'},{av:'edtFuncaoAPF_PF_Titleformat',ctrl:'FUNCAOAPF_PF',prop:'Titleformat'},{av:'edtFuncaoAPF_PF_Title',ctrl:'FUNCAOAPF_PF',prop:'Title'},{av:'edtFuncaoAPF_FunAPFPaiNom_Titleformat',ctrl:'FUNCAOAPF_FUNAPFPAINOM',prop:'Titleformat'},{av:'edtFuncaoAPF_FunAPFPaiNom_Title',ctrl:'FUNCAOAPF_FUNAPFPAINOM',prop:'Title'},{av:'cmbFuncaoAPF_Ativo'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_NEXTPAGE","{handler:'subgrid_nextpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV57FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV76FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV77FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV55FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV78FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV79FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV56FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV80FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV81FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV82FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV83DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV84DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV85FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV86FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV87FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV88FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'AV92TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV93TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV97TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV100TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV101TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV104TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV105TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV109TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV112TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV113TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV116TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV117TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV121TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV91FuncaoAPF_NomeTitleFilterData',fld:'vFUNCAOAPF_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV95FuncaoAPF_TipoTitleFilterData',fld:'vFUNCAOAPF_TIPOTITLEFILTERDATA',pic:'',nv:null},{av:'AV99FuncaoAPF_TDTitleFilterData',fld:'vFUNCAOAPF_TDTITLEFILTERDATA',pic:'',nv:null},{av:'AV103FuncaoAPF_ARTitleFilterData',fld:'vFUNCAOAPF_ARTITLEFILTERDATA',pic:'',nv:null},{av:'AV107FuncaoAPF_ComplexidadeTitleFilterData',fld:'vFUNCAOAPF_COMPLEXIDADETITLEFILTERDATA',pic:'',nv:null},{av:'AV111FuncaoAPF_PFTitleFilterData',fld:'vFUNCAOAPF_PFTITLEFILTERDATA',pic:'',nv:null},{av:'AV115FuncaoAPF_FunAPFPaiNomTitleFilterData',fld:'vFUNCAOAPF_FUNAPFPAINOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV119FuncaoAPF_AtivoTitleFilterData',fld:'vFUNCAOAPF_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'edtFuncaoAPF_Nome_Titleformat',ctrl:'FUNCAOAPF_NOME',prop:'Titleformat'},{av:'edtFuncaoAPF_Nome_Title',ctrl:'FUNCAOAPF_NOME',prop:'Title'},{av:'cmbFuncaoAPF_Tipo'},{av:'edtFuncaoAPF_TD_Titleformat',ctrl:'FUNCAOAPF_TD',prop:'Titleformat'},{av:'edtFuncaoAPF_TD_Title',ctrl:'FUNCAOAPF_TD',prop:'Title'},{av:'edtFuncaoAPF_AR_Titleformat',ctrl:'FUNCAOAPF_AR',prop:'Titleformat'},{av:'edtFuncaoAPF_AR_Title',ctrl:'FUNCAOAPF_AR',prop:'Title'},{av:'cmbFuncaoAPF_Complexidade'},{av:'edtFuncaoAPF_PF_Titleformat',ctrl:'FUNCAOAPF_PF',prop:'Titleformat'},{av:'edtFuncaoAPF_PF_Title',ctrl:'FUNCAOAPF_PF',prop:'Title'},{av:'edtFuncaoAPF_FunAPFPaiNom_Titleformat',ctrl:'FUNCAOAPF_FUNAPFPAINOM',prop:'Titleformat'},{av:'edtFuncaoAPF_FunAPFPaiNom_Title',ctrl:'FUNCAOAPF_FUNAPFPAINOM',prop:'Title'},{av:'cmbFuncaoAPF_Ativo'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_LASTPAGE","{handler:'subgrid_lastpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV57FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV76FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV77FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV55FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV78FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV79FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV56FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV80FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV81FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV82FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV83DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV84DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV85FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV86FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV87FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV88FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'AV92TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV93TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV97TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV100TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV101TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV104TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV105TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV109TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV112TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV113TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV116TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV117TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV121TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV91FuncaoAPF_NomeTitleFilterData',fld:'vFUNCAOAPF_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV95FuncaoAPF_TipoTitleFilterData',fld:'vFUNCAOAPF_TIPOTITLEFILTERDATA',pic:'',nv:null},{av:'AV99FuncaoAPF_TDTitleFilterData',fld:'vFUNCAOAPF_TDTITLEFILTERDATA',pic:'',nv:null},{av:'AV103FuncaoAPF_ARTitleFilterData',fld:'vFUNCAOAPF_ARTITLEFILTERDATA',pic:'',nv:null},{av:'AV107FuncaoAPF_ComplexidadeTitleFilterData',fld:'vFUNCAOAPF_COMPLEXIDADETITLEFILTERDATA',pic:'',nv:null},{av:'AV111FuncaoAPF_PFTitleFilterData',fld:'vFUNCAOAPF_PFTITLEFILTERDATA',pic:'',nv:null},{av:'AV115FuncaoAPF_FunAPFPaiNomTitleFilterData',fld:'vFUNCAOAPF_FUNAPFPAINOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV119FuncaoAPF_AtivoTitleFilterData',fld:'vFUNCAOAPF_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'edtFuncaoAPF_Nome_Titleformat',ctrl:'FUNCAOAPF_NOME',prop:'Titleformat'},{av:'edtFuncaoAPF_Nome_Title',ctrl:'FUNCAOAPF_NOME',prop:'Title'},{av:'cmbFuncaoAPF_Tipo'},{av:'edtFuncaoAPF_TD_Titleformat',ctrl:'FUNCAOAPF_TD',prop:'Titleformat'},{av:'edtFuncaoAPF_TD_Title',ctrl:'FUNCAOAPF_TD',prop:'Title'},{av:'edtFuncaoAPF_AR_Titleformat',ctrl:'FUNCAOAPF_AR',prop:'Titleformat'},{av:'edtFuncaoAPF_AR_Title',ctrl:'FUNCAOAPF_AR',prop:'Title'},{av:'cmbFuncaoAPF_Complexidade'},{av:'edtFuncaoAPF_PF_Titleformat',ctrl:'FUNCAOAPF_PF',prop:'Titleformat'},{av:'edtFuncaoAPF_PF_Title',ctrl:'FUNCAOAPF_PF',prop:'Title'},{av:'edtFuncaoAPF_FunAPFPaiNom_Titleformat',ctrl:'FUNCAOAPF_FUNAPFPAINOM',prop:'Titleformat'},{av:'edtFuncaoAPF_FunAPFPaiNom_Title',ctrl:'FUNCAOAPF_FUNAPFPAINOM',prop:'Title'},{av:'cmbFuncaoAPF_Ativo'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Ddo_funcaoapf_nome_Activeeventkey = "";
         Ddo_funcaoapf_nome_Filteredtext_get = "";
         Ddo_funcaoapf_nome_Selectedvalue_get = "";
         Ddo_funcaoapf_tipo_Activeeventkey = "";
         Ddo_funcaoapf_tipo_Selectedvalue_get = "";
         Ddo_funcaoapf_td_Activeeventkey = "";
         Ddo_funcaoapf_td_Filteredtext_get = "";
         Ddo_funcaoapf_td_Filteredtextto_get = "";
         Ddo_funcaoapf_ar_Activeeventkey = "";
         Ddo_funcaoapf_ar_Filteredtext_get = "";
         Ddo_funcaoapf_ar_Filteredtextto_get = "";
         Ddo_funcaoapf_complexidade_Activeeventkey = "";
         Ddo_funcaoapf_complexidade_Selectedvalue_get = "";
         Ddo_funcaoapf_pf_Activeeventkey = "";
         Ddo_funcaoapf_pf_Filteredtext_get = "";
         Ddo_funcaoapf_pf_Filteredtextto_get = "";
         Ddo_funcaoapf_funapfpainom_Activeeventkey = "";
         Ddo_funcaoapf_funapfpainom_Filteredtext_get = "";
         Ddo_funcaoapf_funapfpainom_Selectedvalue_get = "";
         Ddo_funcaoapf_ativo_Activeeventkey = "";
         Ddo_funcaoapf_ativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17FuncaoAPF_Nome1 = "";
         AV76FuncaoAPF_Tipo1 = "";
         AV55FuncaoAPF_Ativo1 = "A";
         AV19DynamicFiltersSelector2 = "";
         AV21FuncaoAPF_Nome2 = "";
         AV78FuncaoAPF_Tipo2 = "";
         AV56FuncaoAPF_Ativo2 = "A";
         AV23DynamicFiltersSelector3 = "";
         AV25FuncaoAPF_Nome3 = "";
         AV80FuncaoAPF_Tipo3 = "";
         AV82FuncaoAPF_Ativo3 = "A";
         AV84DynamicFiltersSelector4 = "";
         AV85FuncaoAPF_Nome4 = "";
         AV86FuncaoAPF_Tipo4 = "";
         AV88FuncaoAPF_Ativo4 = "A";
         AV92TFFuncaoAPF_Nome = "";
         AV93TFFuncaoAPF_Nome_Sel = "";
         AV116TFFuncaoAPF_FunAPFPaiNom = "";
         AV117TFFuncaoAPF_FunAPFPaiNom_Sel = "";
         AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace = "";
         AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace = "";
         AV102ddo_FuncaoAPF_TDTitleControlIdToReplace = "";
         AV106ddo_FuncaoAPF_ARTitleControlIdToReplace = "";
         AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace = "";
         AV114ddo_FuncaoAPF_PFTitleControlIdToReplace = "";
         AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace = "";
         AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace = "";
         AV77FuncaoAPF_Complexidade1 = "";
         AV79FuncaoAPF_Complexidade2 = "";
         AV81FuncaoAPF_Complexidade3 = "";
         AV87FuncaoAPF_Complexidade4 = "";
         AV97TFFuncaoAPF_Tipo_Sels = new GxSimpleCollection();
         AV109TFFuncaoAPF_Complexidade_Sels = new GxSimpleCollection();
         AV121TFFuncaoAPF_Ativo_Sels = new GxSimpleCollection();
         AV165Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV123DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV91FuncaoAPF_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV95FuncaoAPF_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV99FuncaoAPF_TDTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV103FuncaoAPF_ARTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV107FuncaoAPF_ComplexidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV111FuncaoAPF_PFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV115FuncaoAPF_FunAPFPaiNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV119FuncaoAPF_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_funcaoapf_nome_Filteredtext_set = "";
         Ddo_funcaoapf_nome_Selectedvalue_set = "";
         Ddo_funcaoapf_nome_Sortedstatus = "";
         Ddo_funcaoapf_tipo_Selectedvalue_set = "";
         Ddo_funcaoapf_tipo_Sortedstatus = "";
         Ddo_funcaoapf_td_Filteredtext_set = "";
         Ddo_funcaoapf_td_Filteredtextto_set = "";
         Ddo_funcaoapf_ar_Filteredtext_set = "";
         Ddo_funcaoapf_ar_Filteredtextto_set = "";
         Ddo_funcaoapf_complexidade_Selectedvalue_set = "";
         Ddo_funcaoapf_pf_Filteredtext_set = "";
         Ddo_funcaoapf_pf_Filteredtextto_set = "";
         Ddo_funcaoapf_funapfpainom_Filteredtext_set = "";
         Ddo_funcaoapf_funapfpainom_Selectedvalue_set = "";
         Ddo_funcaoapf_funapfpainom_Sortedstatus = "";
         Ddo_funcaoapf_ativo_Selectedvalue_set = "";
         Ddo_funcaoapf_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV126WWFuncaoAPFDS_1_Dynamicfiltersselector1 = "";
         AV127WWFuncaoAPFDS_2_Funcaoapf_nome1 = "";
         AV128WWFuncaoAPFDS_3_Funcaoapf_tipo1 = "";
         AV129WWFuncaoAPFDS_4_Funcaoapf_complexidade1 = "";
         AV130WWFuncaoAPFDS_5_Funcaoapf_ativo1 = "";
         AV132WWFuncaoAPFDS_7_Dynamicfiltersselector2 = "";
         AV133WWFuncaoAPFDS_8_Funcaoapf_nome2 = "";
         AV134WWFuncaoAPFDS_9_Funcaoapf_tipo2 = "";
         AV135WWFuncaoAPFDS_10_Funcaoapf_complexidade2 = "";
         AV136WWFuncaoAPFDS_11_Funcaoapf_ativo2 = "";
         AV138WWFuncaoAPFDS_13_Dynamicfiltersselector3 = "";
         AV139WWFuncaoAPFDS_14_Funcaoapf_nome3 = "";
         AV140WWFuncaoAPFDS_15_Funcaoapf_tipo3 = "";
         AV141WWFuncaoAPFDS_16_Funcaoapf_complexidade3 = "";
         AV142WWFuncaoAPFDS_17_Funcaoapf_ativo3 = "";
         AV144WWFuncaoAPFDS_19_Dynamicfiltersselector4 = "";
         AV145WWFuncaoAPFDS_20_Funcaoapf_nome4 = "";
         AV146WWFuncaoAPFDS_21_Funcaoapf_tipo4 = "";
         AV147WWFuncaoAPFDS_22_Funcaoapf_complexidade4 = "";
         AV148WWFuncaoAPFDS_23_Funcaoapf_ativo4 = "";
         AV149WWFuncaoAPFDS_24_Tffuncaoapf_nome = "";
         AV150WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel = "";
         AV151WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels = new GxSimpleCollection();
         AV156WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels = new GxSimpleCollection();
         AV159WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom = "";
         AV160WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel = "";
         AV161WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels = new GxSimpleCollection();
         AV28Update = "";
         AV162Update_GXI = "";
         AV29Delete = "";
         AV163Delete_GXI = "";
         AV64Display = "";
         AV164Display_GXI = "";
         A166FuncaoAPF_Nome = "";
         A184FuncaoAPF_Tipo = "";
         A185FuncaoAPF_Complexidade = "";
         A363FuncaoAPF_FunAPFPaiNom = "";
         A183FuncaoAPF_Ativo = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV127WWFuncaoAPFDS_2_Funcaoapf_nome1 = "";
         lV133WWFuncaoAPFDS_8_Funcaoapf_nome2 = "";
         lV139WWFuncaoAPFDS_14_Funcaoapf_nome3 = "";
         lV145WWFuncaoAPFDS_20_Funcaoapf_nome4 = "";
         lV149WWFuncaoAPFDS_24_Tffuncaoapf_nome = "";
         lV159WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom = "";
         H003Z2_A183FuncaoAPF_Ativo = new String[] {""} ;
         H003Z2_A363FuncaoAPF_FunAPFPaiNom = new String[] {""} ;
         H003Z2_n363FuncaoAPF_FunAPFPaiNom = new bool[] {false} ;
         H003Z2_A358FuncaoAPF_FunAPFPaiCod = new int[1] ;
         H003Z2_n358FuncaoAPF_FunAPFPaiCod = new bool[] {false} ;
         H003Z2_A184FuncaoAPF_Tipo = new String[] {""} ;
         H003Z2_A166FuncaoAPF_Nome = new String[] {""} ;
         H003Z2_A165FuncaoAPF_Codigo = new int[1] ;
         GXt_char2 = "";
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgAdddynamicfilters3_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgRemovedynamicfilters4_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons4 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV96TFFuncaoAPF_Tipo_SelsJson = "";
         AV108TFFuncaoAPF_Complexidade_SelsJson = "";
         AV120TFFuncaoAPF_Ativo_SelsJson = "";
         GridRow = new GXWebRow();
         AV30Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblFuncaoapftitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         bttBtnatualizar_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfiltersprefix4_Jsonclick = "";
         lblDynamicfiltersmiddle4_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwfuncaoapf__default(),
            new Object[][] {
                new Object[] {
               H003Z2_A183FuncaoAPF_Ativo, H003Z2_A363FuncaoAPF_FunAPFPaiNom, H003Z2_n363FuncaoAPF_FunAPFPaiNom, H003Z2_A358FuncaoAPF_FunAPFPaiCod, H003Z2_n358FuncaoAPF_FunAPFPaiCod, H003Z2_A184FuncaoAPF_Tipo, H003Z2_A166FuncaoAPF_Nome, H003Z2_A165FuncaoAPF_Codigo
               }
            }
         );
         AV165Pgmname = "WWFuncaoAPF";
         /* GeneXus formulas. */
         AV165Pgmname = "WWFuncaoAPF";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_96 ;
      private short nGXsfl_96_idx=1 ;
      private short AV13OrderedBy ;
      private short AV100TFFuncaoAPF_TD ;
      private short AV101TFFuncaoAPF_TD_To ;
      private short AV104TFFuncaoAPF_AR ;
      private short AV105TFFuncaoAPF_AR_To ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short AV152WWFuncaoAPFDS_27_Tffuncaoapf_td ;
      private short AV153WWFuncaoAPFDS_28_Tffuncaoapf_td_to ;
      private short AV154WWFuncaoAPFDS_29_Tffuncaoapf_ar ;
      private short AV155WWFuncaoAPFDS_30_Tffuncaoapf_ar_to ;
      private short A388FuncaoAPF_TD ;
      private short A387FuncaoAPF_AR ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_96_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short GXt_int1 ;
      private short edtFuncaoAPF_Nome_Titleformat ;
      private short cmbFuncaoAPF_Tipo_Titleformat ;
      private short edtFuncaoAPF_TD_Titleformat ;
      private short edtFuncaoAPF_AR_Titleformat ;
      private short cmbFuncaoAPF_Complexidade_Titleformat ;
      private short edtFuncaoAPF_PF_Titleformat ;
      private short edtFuncaoAPF_FunAPFPaiNom_Titleformat ;
      private short cmbFuncaoAPF_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A165FuncaoAPF_Codigo ;
      private int AV57FuncaoAPF_SistemaCod ;
      private int A358FuncaoAPF_FunAPFPaiCod ;
      private int Ddo_funcaoapf_nome_Datalistupdateminimumcharacters ;
      private int Ddo_funcaoapf_funapfpainom_Datalistupdateminimumcharacters ;
      private int edtavTffuncaoapf_nome_Visible ;
      private int edtavTffuncaoapf_nome_sel_Visible ;
      private int edtavTffuncaoapf_td_Visible ;
      private int edtavTffuncaoapf_td_to_Visible ;
      private int edtavTffuncaoapf_ar_Visible ;
      private int edtavTffuncaoapf_ar_to_Visible ;
      private int edtavTffuncaoapf_pf_Visible ;
      private int edtavTffuncaoapf_pf_to_Visible ;
      private int edtavTffuncaoapf_funapfpainom_Visible ;
      private int edtavTffuncaoapf_funapfpainom_sel_Visible ;
      private int edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaoapf_tipotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaoapf_tdtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaoapf_artitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaoapf_funapfpainomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaoapf_ativotitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int AV151WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels_Count ;
      private int AV161WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels_Count ;
      private int AV156WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels_Count ;
      private int edtavOrdereddsc_Visible ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int imgAdddynamicfilters3_Visible ;
      private int imgRemovedynamicfilters3_Visible ;
      private int edtavFuncaoapf_nome1_Visible ;
      private int edtavFuncaoapf_nome2_Visible ;
      private int edtavFuncaoapf_nome3_Visible ;
      private int edtavFuncaoapf_nome4_Visible ;
      private int AV166GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV112TFFuncaoAPF_PF ;
      private decimal AV113TFFuncaoAPF_PF_To ;
      private decimal AV157WWFuncaoAPFDS_32_Tffuncaoapf_pf ;
      private decimal AV158WWFuncaoAPFDS_33_Tffuncaoapf_pf_to ;
      private decimal A386FuncaoAPF_PF ;
      private decimal GXt_decimal3 ;
      private String Ddo_funcaoapf_nome_Activeeventkey ;
      private String Ddo_funcaoapf_nome_Filteredtext_get ;
      private String Ddo_funcaoapf_nome_Selectedvalue_get ;
      private String Ddo_funcaoapf_tipo_Activeeventkey ;
      private String Ddo_funcaoapf_tipo_Selectedvalue_get ;
      private String Ddo_funcaoapf_td_Activeeventkey ;
      private String Ddo_funcaoapf_td_Filteredtext_get ;
      private String Ddo_funcaoapf_td_Filteredtextto_get ;
      private String Ddo_funcaoapf_ar_Activeeventkey ;
      private String Ddo_funcaoapf_ar_Filteredtext_get ;
      private String Ddo_funcaoapf_ar_Filteredtextto_get ;
      private String Ddo_funcaoapf_complexidade_Activeeventkey ;
      private String Ddo_funcaoapf_complexidade_Selectedvalue_get ;
      private String Ddo_funcaoapf_pf_Activeeventkey ;
      private String Ddo_funcaoapf_pf_Filteredtext_get ;
      private String Ddo_funcaoapf_pf_Filteredtextto_get ;
      private String Ddo_funcaoapf_funapfpainom_Activeeventkey ;
      private String Ddo_funcaoapf_funapfpainom_Filteredtext_get ;
      private String Ddo_funcaoapf_funapfpainom_Selectedvalue_get ;
      private String Ddo_funcaoapf_ativo_Activeeventkey ;
      private String Ddo_funcaoapf_ativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_96_idx="0001" ;
      private String AV76FuncaoAPF_Tipo1 ;
      private String AV55FuncaoAPF_Ativo1 ;
      private String AV78FuncaoAPF_Tipo2 ;
      private String AV56FuncaoAPF_Ativo2 ;
      private String AV80FuncaoAPF_Tipo3 ;
      private String AV82FuncaoAPF_Ativo3 ;
      private String AV86FuncaoAPF_Tipo4 ;
      private String AV88FuncaoAPF_Ativo4 ;
      private String AV77FuncaoAPF_Complexidade1 ;
      private String AV79FuncaoAPF_Complexidade2 ;
      private String AV81FuncaoAPF_Complexidade3 ;
      private String AV87FuncaoAPF_Complexidade4 ;
      private String AV165Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_funcaoapf_nome_Caption ;
      private String Ddo_funcaoapf_nome_Tooltip ;
      private String Ddo_funcaoapf_nome_Cls ;
      private String Ddo_funcaoapf_nome_Filteredtext_set ;
      private String Ddo_funcaoapf_nome_Selectedvalue_set ;
      private String Ddo_funcaoapf_nome_Dropdownoptionstype ;
      private String Ddo_funcaoapf_nome_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapf_nome_Sortedstatus ;
      private String Ddo_funcaoapf_nome_Filtertype ;
      private String Ddo_funcaoapf_nome_Datalisttype ;
      private String Ddo_funcaoapf_nome_Datalistproc ;
      private String Ddo_funcaoapf_nome_Sortasc ;
      private String Ddo_funcaoapf_nome_Sortdsc ;
      private String Ddo_funcaoapf_nome_Loadingdata ;
      private String Ddo_funcaoapf_nome_Cleanfilter ;
      private String Ddo_funcaoapf_nome_Noresultsfound ;
      private String Ddo_funcaoapf_nome_Searchbuttontext ;
      private String Ddo_funcaoapf_tipo_Caption ;
      private String Ddo_funcaoapf_tipo_Tooltip ;
      private String Ddo_funcaoapf_tipo_Cls ;
      private String Ddo_funcaoapf_tipo_Selectedvalue_set ;
      private String Ddo_funcaoapf_tipo_Dropdownoptionstype ;
      private String Ddo_funcaoapf_tipo_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapf_tipo_Sortedstatus ;
      private String Ddo_funcaoapf_tipo_Datalisttype ;
      private String Ddo_funcaoapf_tipo_Datalistfixedvalues ;
      private String Ddo_funcaoapf_tipo_Sortasc ;
      private String Ddo_funcaoapf_tipo_Sortdsc ;
      private String Ddo_funcaoapf_tipo_Cleanfilter ;
      private String Ddo_funcaoapf_tipo_Searchbuttontext ;
      private String Ddo_funcaoapf_td_Caption ;
      private String Ddo_funcaoapf_td_Tooltip ;
      private String Ddo_funcaoapf_td_Cls ;
      private String Ddo_funcaoapf_td_Filteredtext_set ;
      private String Ddo_funcaoapf_td_Filteredtextto_set ;
      private String Ddo_funcaoapf_td_Dropdownoptionstype ;
      private String Ddo_funcaoapf_td_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapf_td_Filtertype ;
      private String Ddo_funcaoapf_td_Cleanfilter ;
      private String Ddo_funcaoapf_td_Rangefilterfrom ;
      private String Ddo_funcaoapf_td_Rangefilterto ;
      private String Ddo_funcaoapf_td_Searchbuttontext ;
      private String Ddo_funcaoapf_ar_Caption ;
      private String Ddo_funcaoapf_ar_Tooltip ;
      private String Ddo_funcaoapf_ar_Cls ;
      private String Ddo_funcaoapf_ar_Filteredtext_set ;
      private String Ddo_funcaoapf_ar_Filteredtextto_set ;
      private String Ddo_funcaoapf_ar_Dropdownoptionstype ;
      private String Ddo_funcaoapf_ar_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapf_ar_Filtertype ;
      private String Ddo_funcaoapf_ar_Cleanfilter ;
      private String Ddo_funcaoapf_ar_Rangefilterfrom ;
      private String Ddo_funcaoapf_ar_Rangefilterto ;
      private String Ddo_funcaoapf_ar_Searchbuttontext ;
      private String Ddo_funcaoapf_complexidade_Caption ;
      private String Ddo_funcaoapf_complexidade_Tooltip ;
      private String Ddo_funcaoapf_complexidade_Cls ;
      private String Ddo_funcaoapf_complexidade_Selectedvalue_set ;
      private String Ddo_funcaoapf_complexidade_Dropdownoptionstype ;
      private String Ddo_funcaoapf_complexidade_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapf_complexidade_Datalisttype ;
      private String Ddo_funcaoapf_complexidade_Datalistfixedvalues ;
      private String Ddo_funcaoapf_complexidade_Cleanfilter ;
      private String Ddo_funcaoapf_complexidade_Searchbuttontext ;
      private String Ddo_funcaoapf_pf_Caption ;
      private String Ddo_funcaoapf_pf_Tooltip ;
      private String Ddo_funcaoapf_pf_Cls ;
      private String Ddo_funcaoapf_pf_Filteredtext_set ;
      private String Ddo_funcaoapf_pf_Filteredtextto_set ;
      private String Ddo_funcaoapf_pf_Dropdownoptionstype ;
      private String Ddo_funcaoapf_pf_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapf_pf_Filtertype ;
      private String Ddo_funcaoapf_pf_Cleanfilter ;
      private String Ddo_funcaoapf_pf_Rangefilterfrom ;
      private String Ddo_funcaoapf_pf_Rangefilterto ;
      private String Ddo_funcaoapf_pf_Searchbuttontext ;
      private String Ddo_funcaoapf_funapfpainom_Caption ;
      private String Ddo_funcaoapf_funapfpainom_Tooltip ;
      private String Ddo_funcaoapf_funapfpainom_Cls ;
      private String Ddo_funcaoapf_funapfpainom_Filteredtext_set ;
      private String Ddo_funcaoapf_funapfpainom_Selectedvalue_set ;
      private String Ddo_funcaoapf_funapfpainom_Dropdownoptionstype ;
      private String Ddo_funcaoapf_funapfpainom_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapf_funapfpainom_Sortedstatus ;
      private String Ddo_funcaoapf_funapfpainom_Filtertype ;
      private String Ddo_funcaoapf_funapfpainom_Datalisttype ;
      private String Ddo_funcaoapf_funapfpainom_Datalistproc ;
      private String Ddo_funcaoapf_funapfpainom_Sortasc ;
      private String Ddo_funcaoapf_funapfpainom_Sortdsc ;
      private String Ddo_funcaoapf_funapfpainom_Loadingdata ;
      private String Ddo_funcaoapf_funapfpainom_Cleanfilter ;
      private String Ddo_funcaoapf_funapfpainom_Noresultsfound ;
      private String Ddo_funcaoapf_funapfpainom_Searchbuttontext ;
      private String Ddo_funcaoapf_ativo_Caption ;
      private String Ddo_funcaoapf_ativo_Tooltip ;
      private String Ddo_funcaoapf_ativo_Cls ;
      private String Ddo_funcaoapf_ativo_Selectedvalue_set ;
      private String Ddo_funcaoapf_ativo_Dropdownoptionstype ;
      private String Ddo_funcaoapf_ativo_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapf_ativo_Sortedstatus ;
      private String Ddo_funcaoapf_ativo_Datalisttype ;
      private String Ddo_funcaoapf_ativo_Datalistfixedvalues ;
      private String Ddo_funcaoapf_ativo_Sortasc ;
      private String Ddo_funcaoapf_ativo_Sortdsc ;
      private String Ddo_funcaoapf_ativo_Cleanfilter ;
      private String Ddo_funcaoapf_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String chkavDynamicfiltersenabled4_Internalname ;
      private String edtavTffuncaoapf_nome_Internalname ;
      private String edtavTffuncaoapf_nome_sel_Internalname ;
      private String edtavTffuncaoapf_td_Internalname ;
      private String edtavTffuncaoapf_td_Jsonclick ;
      private String edtavTffuncaoapf_td_to_Internalname ;
      private String edtavTffuncaoapf_td_to_Jsonclick ;
      private String edtavTffuncaoapf_ar_Internalname ;
      private String edtavTffuncaoapf_ar_Jsonclick ;
      private String edtavTffuncaoapf_ar_to_Internalname ;
      private String edtavTffuncaoapf_ar_to_Jsonclick ;
      private String edtavTffuncaoapf_pf_Internalname ;
      private String edtavTffuncaoapf_pf_Jsonclick ;
      private String edtavTffuncaoapf_pf_to_Internalname ;
      private String edtavTffuncaoapf_pf_to_Jsonclick ;
      private String edtavTffuncaoapf_funapfpainom_Internalname ;
      private String edtavTffuncaoapf_funapfpainom_sel_Internalname ;
      private String edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaoapf_tipotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaoapf_tdtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaoapf_artitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaoapf_funapfpainomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaoapf_ativotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV128WWFuncaoAPFDS_3_Funcaoapf_tipo1 ;
      private String AV129WWFuncaoAPFDS_4_Funcaoapf_complexidade1 ;
      private String AV130WWFuncaoAPFDS_5_Funcaoapf_ativo1 ;
      private String AV134WWFuncaoAPFDS_9_Funcaoapf_tipo2 ;
      private String AV135WWFuncaoAPFDS_10_Funcaoapf_complexidade2 ;
      private String AV136WWFuncaoAPFDS_11_Funcaoapf_ativo2 ;
      private String AV140WWFuncaoAPFDS_15_Funcaoapf_tipo3 ;
      private String AV141WWFuncaoAPFDS_16_Funcaoapf_complexidade3 ;
      private String AV142WWFuncaoAPFDS_17_Funcaoapf_ativo3 ;
      private String AV146WWFuncaoAPFDS_21_Funcaoapf_tipo4 ;
      private String AV147WWFuncaoAPFDS_22_Funcaoapf_complexidade4 ;
      private String AV148WWFuncaoAPFDS_23_Funcaoapf_ativo4 ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtFuncaoAPF_Codigo_Internalname ;
      private String edtFuncaoAPF_Nome_Internalname ;
      private String cmbFuncaoAPF_Tipo_Internalname ;
      private String A184FuncaoAPF_Tipo ;
      private String edtFuncaoAPF_TD_Internalname ;
      private String edtFuncaoAPF_AR_Internalname ;
      private String cmbFuncaoAPF_Complexidade_Internalname ;
      private String A185FuncaoAPF_Complexidade ;
      private String edtFuncaoAPF_PF_Internalname ;
      private String edtFuncaoAPF_FunAPFPaiCod_Internalname ;
      private String edtFuncaoAPF_FunAPFPaiNom_Internalname ;
      private String cmbFuncaoAPF_Ativo_Internalname ;
      private String A183FuncaoAPF_Ativo ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String GXt_char2 ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavFuncaoapf_nome1_Internalname ;
      private String cmbavFuncaoapf_tipo1_Internalname ;
      private String cmbavFuncaoapf_complexidade1_Internalname ;
      private String cmbavFuncaoapf_ativo1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavFuncaoapf_nome2_Internalname ;
      private String cmbavFuncaoapf_tipo2_Internalname ;
      private String cmbavFuncaoapf_complexidade2_Internalname ;
      private String cmbavFuncaoapf_ativo2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavFuncaoapf_nome3_Internalname ;
      private String cmbavFuncaoapf_tipo3_Internalname ;
      private String cmbavFuncaoapf_complexidade3_Internalname ;
      private String cmbavFuncaoapf_ativo3_Internalname ;
      private String cmbavDynamicfiltersselector4_Internalname ;
      private String edtavFuncaoapf_nome4_Internalname ;
      private String cmbavFuncaoapf_tipo4_Internalname ;
      private String cmbavFuncaoapf_complexidade4_Internalname ;
      private String cmbavFuncaoapf_ativo4_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgAdddynamicfilters3_Jsonclick ;
      private String imgAdddynamicfilters3_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String imgRemovedynamicfilters4_Jsonclick ;
      private String imgRemovedynamicfilters4_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_funcaoapf_nome_Internalname ;
      private String Ddo_funcaoapf_tipo_Internalname ;
      private String Ddo_funcaoapf_td_Internalname ;
      private String Ddo_funcaoapf_ar_Internalname ;
      private String Ddo_funcaoapf_complexidade_Internalname ;
      private String Ddo_funcaoapf_pf_Internalname ;
      private String Ddo_funcaoapf_funapfpainom_Internalname ;
      private String Ddo_funcaoapf_ativo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtFuncaoAPF_Nome_Title ;
      private String edtFuncaoAPF_TD_Title ;
      private String edtFuncaoAPF_AR_Title ;
      private String edtFuncaoAPF_PF_Title ;
      private String edtFuncaoAPF_FunAPFPaiNom_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtFuncaoAPF_Nome_Link ;
      private String edtFuncaoAPF_FunAPFPaiNom_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblFuncaoapftitle_Internalname ;
      private String lblFuncaoapftitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String bttBtnatualizar_Internalname ;
      private String bttBtnatualizar_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String cmbavFuncaoapf_tipo1_Jsonclick ;
      private String cmbavFuncaoapf_complexidade1_Jsonclick ;
      private String cmbavFuncaoapf_ativo1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String cmbavFuncaoapf_tipo2_Jsonclick ;
      private String cmbavFuncaoapf_complexidade2_Jsonclick ;
      private String cmbavFuncaoapf_ativo2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String cmbavFuncaoapf_tipo3_Jsonclick ;
      private String cmbavFuncaoapf_complexidade3_Jsonclick ;
      private String cmbavFuncaoapf_ativo3_Jsonclick ;
      private String lblDynamicfiltersprefix4_Internalname ;
      private String lblDynamicfiltersprefix4_Jsonclick ;
      private String cmbavDynamicfiltersselector4_Jsonclick ;
      private String lblDynamicfiltersmiddle4_Internalname ;
      private String lblDynamicfiltersmiddle4_Jsonclick ;
      private String cmbavFuncaoapf_tipo4_Jsonclick ;
      private String cmbavFuncaoapf_complexidade4_Jsonclick ;
      private String cmbavFuncaoapf_ativo4_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_96_fel_idx="0001" ;
      private String ROClassString ;
      private String edtFuncaoAPF_Codigo_Jsonclick ;
      private String edtFuncaoAPF_Nome_Jsonclick ;
      private String cmbFuncaoAPF_Tipo_Jsonclick ;
      private String edtFuncaoAPF_TD_Jsonclick ;
      private String edtFuncaoAPF_AR_Jsonclick ;
      private String cmbFuncaoAPF_Complexidade_Jsonclick ;
      private String edtFuncaoAPF_PF_Jsonclick ;
      private String edtFuncaoAPF_FunAPFPaiCod_Jsonclick ;
      private String edtFuncaoAPF_FunAPFPaiNom_Jsonclick ;
      private String cmbFuncaoAPF_Ativo_Jsonclick ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV83DynamicFiltersEnabled4 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool n358FuncaoAPF_FunAPFPaiCod ;
      private bool toggleJsOutput ;
      private bool Ddo_funcaoapf_nome_Includesortasc ;
      private bool Ddo_funcaoapf_nome_Includesortdsc ;
      private bool Ddo_funcaoapf_nome_Includefilter ;
      private bool Ddo_funcaoapf_nome_Filterisrange ;
      private bool Ddo_funcaoapf_nome_Includedatalist ;
      private bool Ddo_funcaoapf_tipo_Includesortasc ;
      private bool Ddo_funcaoapf_tipo_Includesortdsc ;
      private bool Ddo_funcaoapf_tipo_Includefilter ;
      private bool Ddo_funcaoapf_tipo_Includedatalist ;
      private bool Ddo_funcaoapf_tipo_Allowmultipleselection ;
      private bool Ddo_funcaoapf_td_Includesortasc ;
      private bool Ddo_funcaoapf_td_Includesortdsc ;
      private bool Ddo_funcaoapf_td_Includefilter ;
      private bool Ddo_funcaoapf_td_Filterisrange ;
      private bool Ddo_funcaoapf_td_Includedatalist ;
      private bool Ddo_funcaoapf_ar_Includesortasc ;
      private bool Ddo_funcaoapf_ar_Includesortdsc ;
      private bool Ddo_funcaoapf_ar_Includefilter ;
      private bool Ddo_funcaoapf_ar_Filterisrange ;
      private bool Ddo_funcaoapf_ar_Includedatalist ;
      private bool Ddo_funcaoapf_complexidade_Includesortasc ;
      private bool Ddo_funcaoapf_complexidade_Includesortdsc ;
      private bool Ddo_funcaoapf_complexidade_Includefilter ;
      private bool Ddo_funcaoapf_complexidade_Includedatalist ;
      private bool Ddo_funcaoapf_complexidade_Allowmultipleselection ;
      private bool Ddo_funcaoapf_pf_Includesortasc ;
      private bool Ddo_funcaoapf_pf_Includesortdsc ;
      private bool Ddo_funcaoapf_pf_Includefilter ;
      private bool Ddo_funcaoapf_pf_Filterisrange ;
      private bool Ddo_funcaoapf_pf_Includedatalist ;
      private bool Ddo_funcaoapf_funapfpainom_Includesortasc ;
      private bool Ddo_funcaoapf_funapfpainom_Includesortdsc ;
      private bool Ddo_funcaoapf_funapfpainom_Includefilter ;
      private bool Ddo_funcaoapf_funapfpainom_Filterisrange ;
      private bool Ddo_funcaoapf_funapfpainom_Includedatalist ;
      private bool Ddo_funcaoapf_ativo_Includesortasc ;
      private bool Ddo_funcaoapf_ativo_Includesortdsc ;
      private bool Ddo_funcaoapf_ativo_Includefilter ;
      private bool Ddo_funcaoapf_ativo_Includedatalist ;
      private bool Ddo_funcaoapf_ativo_Allowmultipleselection ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV131WWFuncaoAPFDS_6_Dynamicfiltersenabled2 ;
      private bool AV137WWFuncaoAPFDS_12_Dynamicfiltersenabled3 ;
      private bool AV143WWFuncaoAPFDS_18_Dynamicfiltersenabled4 ;
      private bool n363FuncaoAPF_FunAPFPaiNom ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private bool AV64Display_IsBlob ;
      private String AV96TFFuncaoAPF_Tipo_SelsJson ;
      private String AV108TFFuncaoAPF_Complexidade_SelsJson ;
      private String AV120TFFuncaoAPF_Ativo_SelsJson ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV17FuncaoAPF_Nome1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV21FuncaoAPF_Nome2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV25FuncaoAPF_Nome3 ;
      private String AV84DynamicFiltersSelector4 ;
      private String AV85FuncaoAPF_Nome4 ;
      private String AV92TFFuncaoAPF_Nome ;
      private String AV93TFFuncaoAPF_Nome_Sel ;
      private String AV116TFFuncaoAPF_FunAPFPaiNom ;
      private String AV117TFFuncaoAPF_FunAPFPaiNom_Sel ;
      private String AV94ddo_FuncaoAPF_NomeTitleControlIdToReplace ;
      private String AV98ddo_FuncaoAPF_TipoTitleControlIdToReplace ;
      private String AV102ddo_FuncaoAPF_TDTitleControlIdToReplace ;
      private String AV106ddo_FuncaoAPF_ARTitleControlIdToReplace ;
      private String AV110ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace ;
      private String AV114ddo_FuncaoAPF_PFTitleControlIdToReplace ;
      private String AV118ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace ;
      private String AV122ddo_FuncaoAPF_AtivoTitleControlIdToReplace ;
      private String AV126WWFuncaoAPFDS_1_Dynamicfiltersselector1 ;
      private String AV127WWFuncaoAPFDS_2_Funcaoapf_nome1 ;
      private String AV132WWFuncaoAPFDS_7_Dynamicfiltersselector2 ;
      private String AV133WWFuncaoAPFDS_8_Funcaoapf_nome2 ;
      private String AV138WWFuncaoAPFDS_13_Dynamicfiltersselector3 ;
      private String AV139WWFuncaoAPFDS_14_Funcaoapf_nome3 ;
      private String AV144WWFuncaoAPFDS_19_Dynamicfiltersselector4 ;
      private String AV145WWFuncaoAPFDS_20_Funcaoapf_nome4 ;
      private String AV149WWFuncaoAPFDS_24_Tffuncaoapf_nome ;
      private String AV150WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel ;
      private String AV159WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom ;
      private String AV160WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel ;
      private String AV162Update_GXI ;
      private String AV163Delete_GXI ;
      private String AV164Display_GXI ;
      private String A166FuncaoAPF_Nome ;
      private String A363FuncaoAPF_FunAPFPaiNom ;
      private String lV127WWFuncaoAPFDS_2_Funcaoapf_nome1 ;
      private String lV133WWFuncaoAPFDS_8_Funcaoapf_nome2 ;
      private String lV139WWFuncaoAPFDS_14_Funcaoapf_nome3 ;
      private String lV145WWFuncaoAPFDS_20_Funcaoapf_nome4 ;
      private String lV149WWFuncaoAPFDS_24_Tffuncaoapf_nome ;
      private String lV159WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom ;
      private String AV28Update ;
      private String AV29Delete ;
      private String AV64Display ;
      private IGxSession AV30Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavFuncaoapf_tipo1 ;
      private GXCombobox cmbavFuncaoapf_complexidade1 ;
      private GXCombobox cmbavFuncaoapf_ativo1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavFuncaoapf_tipo2 ;
      private GXCombobox cmbavFuncaoapf_complexidade2 ;
      private GXCombobox cmbavFuncaoapf_ativo2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavFuncaoapf_tipo3 ;
      private GXCombobox cmbavFuncaoapf_complexidade3 ;
      private GXCombobox cmbavFuncaoapf_ativo3 ;
      private GXCombobox cmbavDynamicfiltersselector4 ;
      private GXCombobox cmbavFuncaoapf_tipo4 ;
      private GXCombobox cmbavFuncaoapf_complexidade4 ;
      private GXCombobox cmbavFuncaoapf_ativo4 ;
      private GXCombobox cmbFuncaoAPF_Tipo ;
      private GXCombobox cmbFuncaoAPF_Complexidade ;
      private GXCombobox cmbFuncaoAPF_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private GXCheckbox chkavDynamicfiltersenabled4 ;
      private IDataStoreProvider pr_default ;
      private String[] H003Z2_A183FuncaoAPF_Ativo ;
      private String[] H003Z2_A363FuncaoAPF_FunAPFPaiNom ;
      private bool[] H003Z2_n363FuncaoAPF_FunAPFPaiNom ;
      private int[] H003Z2_A358FuncaoAPF_FunAPFPaiCod ;
      private bool[] H003Z2_n358FuncaoAPF_FunAPFPaiCod ;
      private String[] H003Z2_A184FuncaoAPF_Tipo ;
      private String[] H003Z2_A166FuncaoAPF_Nome ;
      private int[] H003Z2_A165FuncaoAPF_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV97TFFuncaoAPF_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV109TFFuncaoAPF_Complexidade_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV121TFFuncaoAPF_Ativo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV151WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV156WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV161WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV91FuncaoAPF_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV95FuncaoAPF_TipoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV99FuncaoAPF_TDTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV103FuncaoAPF_ARTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV107FuncaoAPF_ComplexidadeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV111FuncaoAPF_PFTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV115FuncaoAPF_FunAPFPaiNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV119FuncaoAPF_AtivoTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV123DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons4 ;
   }

   public class wwfuncaoapf__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H003Z2( IGxContext context ,
                                             String A185FuncaoAPF_Complexidade ,
                                             IGxCollection AV156WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels ,
                                             String A184FuncaoAPF_Tipo ,
                                             IGxCollection AV151WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels ,
                                             String A183FuncaoAPF_Ativo ,
                                             IGxCollection AV161WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels ,
                                             String AV126WWFuncaoAPFDS_1_Dynamicfiltersselector1 ,
                                             String AV127WWFuncaoAPFDS_2_Funcaoapf_nome1 ,
                                             String AV128WWFuncaoAPFDS_3_Funcaoapf_tipo1 ,
                                             String AV130WWFuncaoAPFDS_5_Funcaoapf_ativo1 ,
                                             bool AV131WWFuncaoAPFDS_6_Dynamicfiltersenabled2 ,
                                             String AV132WWFuncaoAPFDS_7_Dynamicfiltersselector2 ,
                                             String AV133WWFuncaoAPFDS_8_Funcaoapf_nome2 ,
                                             String AV134WWFuncaoAPFDS_9_Funcaoapf_tipo2 ,
                                             String AV136WWFuncaoAPFDS_11_Funcaoapf_ativo2 ,
                                             bool AV137WWFuncaoAPFDS_12_Dynamicfiltersenabled3 ,
                                             String AV138WWFuncaoAPFDS_13_Dynamicfiltersselector3 ,
                                             String AV139WWFuncaoAPFDS_14_Funcaoapf_nome3 ,
                                             String AV140WWFuncaoAPFDS_15_Funcaoapf_tipo3 ,
                                             String AV142WWFuncaoAPFDS_17_Funcaoapf_ativo3 ,
                                             bool AV143WWFuncaoAPFDS_18_Dynamicfiltersenabled4 ,
                                             String AV144WWFuncaoAPFDS_19_Dynamicfiltersselector4 ,
                                             String AV145WWFuncaoAPFDS_20_Funcaoapf_nome4 ,
                                             String AV146WWFuncaoAPFDS_21_Funcaoapf_tipo4 ,
                                             String AV148WWFuncaoAPFDS_23_Funcaoapf_ativo4 ,
                                             String AV150WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel ,
                                             String AV149WWFuncaoAPFDS_24_Tffuncaoapf_nome ,
                                             int AV151WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels_Count ,
                                             String AV160WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel ,
                                             String AV159WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom ,
                                             int AV161WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels_Count ,
                                             String A166FuncaoAPF_Nome ,
                                             String A363FuncaoAPF_FunAPFPaiNom ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             String AV129WWFuncaoAPFDS_4_Funcaoapf_complexidade1 ,
                                             String AV135WWFuncaoAPFDS_10_Funcaoapf_complexidade2 ,
                                             String AV141WWFuncaoAPFDS_16_Funcaoapf_complexidade3 ,
                                             String AV147WWFuncaoAPFDS_22_Funcaoapf_complexidade4 ,
                                             short AV152WWFuncaoAPFDS_27_Tffuncaoapf_td ,
                                             short A388FuncaoAPF_TD ,
                                             short AV153WWFuncaoAPFDS_28_Tffuncaoapf_td_to ,
                                             short AV154WWFuncaoAPFDS_29_Tffuncaoapf_ar ,
                                             short A387FuncaoAPF_AR ,
                                             short AV155WWFuncaoAPFDS_30_Tffuncaoapf_ar_to ,
                                             int AV156WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels_Count ,
                                             decimal AV157WWFuncaoAPFDS_32_Tffuncaoapf_pf ,
                                             decimal A386FuncaoAPF_PF ,
                                             decimal AV158WWFuncaoAPFDS_33_Tffuncaoapf_pf_to )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [17] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[FuncaoAPF_Ativo], T2.[FuncaoAPF_Nome] AS FuncaoAPF_FunAPFPaiNom, T1.[FuncaoAPF_FunAPFPaiCod] AS FuncaoAPF_FunAPFPaiCod, T1.[FuncaoAPF_Tipo], T1.[FuncaoAPF_Nome], T1.[FuncaoAPF_Codigo] FROM ([FuncoesAPF] T1 WITH (NOLOCK) LEFT JOIN [FuncoesAPF] T2 WITH (NOLOCK) ON T2.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_FunAPFPaiCod])";
         if ( ( StringUtil.StrCmp(AV126WWFuncaoAPFDS_1_Dynamicfiltersselector1, "FUNCAOAPF_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV127WWFuncaoAPFDS_2_Funcaoapf_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV127WWFuncaoAPFDS_2_Funcaoapf_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like @lV127WWFuncaoAPFDS_2_Funcaoapf_nome1 + '%')";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV126WWFuncaoAPFDS_1_Dynamicfiltersselector1, "FUNCAOAPF_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV128WWFuncaoAPFDS_3_Funcaoapf_tipo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Tipo] = @AV128WWFuncaoAPFDS_3_Funcaoapf_tipo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Tipo] = @AV128WWFuncaoAPFDS_3_Funcaoapf_tipo1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV126WWFuncaoAPFDS_1_Dynamicfiltersselector1, "FUNCAOAPF_ATIVO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130WWFuncaoAPFDS_5_Funcaoapf_ativo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Ativo] = @AV130WWFuncaoAPFDS_5_Funcaoapf_ativo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Ativo] = @AV130WWFuncaoAPFDS_5_Funcaoapf_ativo1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( AV131WWFuncaoAPFDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132WWFuncaoAPFDS_7_Dynamicfiltersselector2, "FUNCAOAPF_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV133WWFuncaoAPFDS_8_Funcaoapf_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV133WWFuncaoAPFDS_8_Funcaoapf_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like @lV133WWFuncaoAPFDS_8_Funcaoapf_nome2 + '%')";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV131WWFuncaoAPFDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132WWFuncaoAPFDS_7_Dynamicfiltersselector2, "FUNCAOAPF_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV134WWFuncaoAPFDS_9_Funcaoapf_tipo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Tipo] = @AV134WWFuncaoAPFDS_9_Funcaoapf_tipo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Tipo] = @AV134WWFuncaoAPFDS_9_Funcaoapf_tipo2)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV131WWFuncaoAPFDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132WWFuncaoAPFDS_7_Dynamicfiltersselector2, "FUNCAOAPF_ATIVO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV136WWFuncaoAPFDS_11_Funcaoapf_ativo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Ativo] = @AV136WWFuncaoAPFDS_11_Funcaoapf_ativo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Ativo] = @AV136WWFuncaoAPFDS_11_Funcaoapf_ativo2)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV137WWFuncaoAPFDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV138WWFuncaoAPFDS_13_Dynamicfiltersselector3, "FUNCAOAPF_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV139WWFuncaoAPFDS_14_Funcaoapf_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV139WWFuncaoAPFDS_14_Funcaoapf_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like @lV139WWFuncaoAPFDS_14_Funcaoapf_nome3 + '%')";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV137WWFuncaoAPFDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV138WWFuncaoAPFDS_13_Dynamicfiltersselector3, "FUNCAOAPF_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV140WWFuncaoAPFDS_15_Funcaoapf_tipo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Tipo] = @AV140WWFuncaoAPFDS_15_Funcaoapf_tipo3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Tipo] = @AV140WWFuncaoAPFDS_15_Funcaoapf_tipo3)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV137WWFuncaoAPFDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV138WWFuncaoAPFDS_13_Dynamicfiltersselector3, "FUNCAOAPF_ATIVO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV142WWFuncaoAPFDS_17_Funcaoapf_ativo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Ativo] = @AV142WWFuncaoAPFDS_17_Funcaoapf_ativo3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Ativo] = @AV142WWFuncaoAPFDS_17_Funcaoapf_ativo3)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV143WWFuncaoAPFDS_18_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV144WWFuncaoAPFDS_19_Dynamicfiltersselector4, "FUNCAOAPF_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV145WWFuncaoAPFDS_20_Funcaoapf_nome4)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV145WWFuncaoAPFDS_20_Funcaoapf_nome4 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like @lV145WWFuncaoAPFDS_20_Funcaoapf_nome4 + '%')";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV143WWFuncaoAPFDS_18_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV144WWFuncaoAPFDS_19_Dynamicfiltersselector4, "FUNCAOAPF_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV146WWFuncaoAPFDS_21_Funcaoapf_tipo4)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Tipo] = @AV146WWFuncaoAPFDS_21_Funcaoapf_tipo4)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Tipo] = @AV146WWFuncaoAPFDS_21_Funcaoapf_tipo4)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( AV143WWFuncaoAPFDS_18_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV144WWFuncaoAPFDS_19_Dynamicfiltersselector4, "FUNCAOAPF_ATIVO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV148WWFuncaoAPFDS_23_Funcaoapf_ativo4)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Ativo] = @AV148WWFuncaoAPFDS_23_Funcaoapf_ativo4)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Ativo] = @AV148WWFuncaoAPFDS_23_Funcaoapf_ativo4)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV150WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV149WWFuncaoAPFDS_24_Tffuncaoapf_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV149WWFuncaoAPFDS_24_Tffuncaoapf_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like @lV149WWFuncaoAPFDS_24_Tffuncaoapf_nome)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV150WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] = @AV150WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] = @AV150WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( AV151WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV151WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels, "T1.[FuncaoAPF_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV151WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels, "T1.[FuncaoAPF_Tipo] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV160WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV159WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like @lV159WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] like @lV159WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV160WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] = @AV160WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] = @AV160WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( AV161WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV161WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels, "T1.[FuncaoAPF_Ativo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV161WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels, "T1.[FuncaoAPF_Ativo] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoAPF_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoAPF_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoAPF_Tipo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoAPF_Tipo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T2.[FuncaoAPF_Nome]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T2.[FuncaoAPF_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoAPF_Ativo]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoAPF_Ativo] DESC";
         }
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H003Z2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (bool)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (bool)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (int)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (short)dynConstraints[33] , (bool)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (short)dynConstraints[39] , (short)dynConstraints[40] , (short)dynConstraints[41] , (short)dynConstraints[42] , (short)dynConstraints[43] , (short)dynConstraints[44] , (int)dynConstraints[45] , (decimal)dynConstraints[46] , (decimal)dynConstraints[47] , (decimal)dynConstraints[48] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH003Z2 ;
          prmH003Z2 = new Object[] {
          new Object[] {"@AV156WWFCount",SqlDbType.Int,9,0} ,
          new Object[] {"@lV127WWFuncaoAPFDS_2_Funcaoapf_nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV128WWFuncaoAPFDS_3_Funcaoapf_tipo1",SqlDbType.Char,3,0} ,
          new Object[] {"@AV130WWFuncaoAPFDS_5_Funcaoapf_ativo1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV133WWFuncaoAPFDS_8_Funcaoapf_nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV134WWFuncaoAPFDS_9_Funcaoapf_tipo2",SqlDbType.Char,3,0} ,
          new Object[] {"@AV136WWFuncaoAPFDS_11_Funcaoapf_ativo2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV139WWFuncaoAPFDS_14_Funcaoapf_nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV140WWFuncaoAPFDS_15_Funcaoapf_tipo3",SqlDbType.Char,3,0} ,
          new Object[] {"@AV142WWFuncaoAPFDS_17_Funcaoapf_ativo3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV145WWFuncaoAPFDS_20_Funcaoapf_nome4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV146WWFuncaoAPFDS_21_Funcaoapf_tipo4",SqlDbType.Char,3,0} ,
          new Object[] {"@AV148WWFuncaoAPFDS_23_Funcaoapf_ativo4",SqlDbType.Char,1,0} ,
          new Object[] {"@lV149WWFuncaoAPFDS_24_Tffuncaoapf_nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV150WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV159WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV160WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H003Z2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003Z2,11,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 3) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                return;
       }
    }

 }

}
