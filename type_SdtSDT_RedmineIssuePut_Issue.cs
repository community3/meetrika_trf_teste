/*
               File: type_SdtSDT_RedmineIssuePut_Issue
        Description: SDT_RedmineIssuePut
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:30:6.3
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_RedmineIssuePut.Issue" )]
   [XmlType(TypeName =  "SDT_RedmineIssuePut.Issue" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtSDT_RedmineIssuePut_Issue : GxUserType
   {
      public SdtSDT_RedmineIssuePut_Issue( )
      {
         /* Constructor for serialization */
      }

      public SdtSDT_RedmineIssuePut_Issue( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_RedmineIssuePut_Issue deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_RedmineIssuePut_Issue)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_RedmineIssuePut_Issue obj ;
         obj = this;
         obj.gxTpr_Status_id = deserialized.gxTpr_Status_id;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Status_Id") )
               {
                  gxTv_SdtSDT_RedmineIssuePut_Issue_Status_id = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_RedmineIssuePut.Issue";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Status_Id", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_RedmineIssuePut_Issue_Status_id), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Status_Id", gxTv_SdtSDT_RedmineIssuePut_Issue_Status_id, false);
         return  ;
      }

      [  SoapElement( ElementName = "Status_Id" )]
      [  XmlElement( ElementName = "Status_Id"   )]
      public short gxTpr_Status_id
      {
         get {
            return gxTv_SdtSDT_RedmineIssuePut_Issue_Status_id ;
         }

         set {
            gxTv_SdtSDT_RedmineIssuePut_Issue_Status_id = (short)(value);
         }

      }

      public void initialize( )
      {
         sTagName = "";
         return  ;
      }

      protected short gxTv_SdtSDT_RedmineIssuePut_Issue_Status_id ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected String sTagName ;
   }

   [DataContract(Name = @"SDT_RedmineIssuePut.Issue", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtSDT_RedmineIssuePut_Issue_RESTInterface : GxGenericCollectionItem<SdtSDT_RedmineIssuePut_Issue>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_RedmineIssuePut_Issue_RESTInterface( ) : base()
      {
      }

      public SdtSDT_RedmineIssuePut_Issue_RESTInterface( SdtSDT_RedmineIssuePut_Issue psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Status_Id" , Order = 0 )]
      public Nullable<short> gxTpr_Status_id
      {
         get {
            return sdt.gxTpr_Status_id ;
         }

         set {
            sdt.gxTpr_Status_id = (short)(value.HasValue ? value.Value : 0);
         }

      }

      public SdtSDT_RedmineIssuePut_Issue sdt
      {
         get {
            return (SdtSDT_RedmineIssuePut_Issue)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_RedmineIssuePut_Issue() ;
         }
      }

   }

}
