/*
               File: Usuario_BC
        Description: Usuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:15:40.28
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class usuario_bc : GXHttpHandler, IGxSilentTrn
   {
      public usuario_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public usuario_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow011( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey011( ) ;
         standaloneModal( ) ;
         AddRow011( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E11012 */
            E11012 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z1Usuario_Codigo = A1Usuario_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_010( )
      {
         BeforeValidate011( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls011( ) ;
            }
            else
            {
               CheckExtendedTable011( ) ;
               if ( AnyError == 0 )
               {
                  ZM011( 16) ;
                  ZM011( 17) ;
                  ZM011( 18) ;
               }
               CloseExtendedTableCursors011( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E12012( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         /* Execute user subroutine: 'ATTRIBUTESSECURITYCODE' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV32Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV33GXV1 = 1;
            while ( AV33GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV33GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Usuario_CargoCod") == 0 )
               {
                  AV25Insert_Usuario_CargoCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Usuario_PessoaCod") == 0 )
               {
                  AV14Insert_Usuario_PessoaCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               AV33GXV1 = (int)(AV33GXV1+1);
            }
         }
      }

      protected void E11012( )
      {
         /* After Trn Routine */
         if ( false )
         {
            new wwpbaseobjects.audittransaction(context ).execute(  AV28AuditingObject,  AV32Pgmname) ;
         }
         if ( AV8WWPContext.gxTpr_Areatrabalho_codigo == 0 )
         {
            new prc_parametrossistemaalteraflaglicensiado(context ).execute(  1,  true) ;
         }
      }

      protected void E13012( )
      {
         /* 'DoSelectUO' Routine */
         context.PopUp(formatLink("wp_selecionauo.aspx") + "?" + UrlEncode("" +AV26UnidadeOrganizacional_Codigo), new Object[] {"AV26UnidadeOrganizacional_Codigo"});
      }

      protected void S112( )
      {
         /* 'ATTRIBUTESSECURITYCODE' Routine */
      }

      protected void ZM011( short GX_JID )
      {
         if ( ( GX_JID == 15 ) || ( GX_JID == 0 ) )
         {
            Z341Usuario_UserGamGuid = A341Usuario_UserGamGuid;
            Z2Usuario_Nome = A2Usuario_Nome;
            Z289Usuario_EhContador = A289Usuario_EhContador;
            Z291Usuario_EhContratada = A291Usuario_EhContratada;
            Z292Usuario_EhContratante = A292Usuario_EhContratante;
            Z293Usuario_EhFinanceiro = A293Usuario_EhFinanceiro;
            Z538Usuario_EhGestor = A538Usuario_EhGestor;
            Z1093Usuario_EhPreposto = A1093Usuario_EhPreposto;
            Z1017Usuario_CrtfPath = A1017Usuario_CrtfPath;
            Z1235Usuario_Notificar = A1235Usuario_Notificar;
            Z54Usuario_Ativo = A54Usuario_Ativo;
            Z1647Usuario_Email = A1647Usuario_Email;
            Z1908Usuario_DeFerias = A1908Usuario_DeFerias;
            Z2016Usuario_UltimaArea = A2016Usuario_UltimaArea;
            Z57Usuario_PessoaCod = A57Usuario_PessoaCod;
            Z1073Usuario_CargoCod = A1073Usuario_CargoCod;
            Z66ContratadaUsuario_ContratadaCod = A66ContratadaUsuario_ContratadaCod;
            Z69ContratadaUsuario_UsuarioCod = A69ContratadaUsuario_UsuarioCod;
            Z34Pessoa_Codigo = A34Pessoa_Codigo;
            Z1083Usuario_Entidade = A1083Usuario_Entidade;
            Z290Usuario_EhAuditorFM = A290Usuario_EhAuditorFM;
         }
         if ( ( GX_JID == 16 ) || ( GX_JID == 0 ) )
         {
            Z58Usuario_PessoaNom = A58Usuario_PessoaNom;
            Z59Usuario_PessoaTip = A59Usuario_PessoaTip;
            Z325Usuario_PessoaDoc = A325Usuario_PessoaDoc;
            Z2095Usuario_PessoaTelefone = A2095Usuario_PessoaTelefone;
            Z66ContratadaUsuario_ContratadaCod = A66ContratadaUsuario_ContratadaCod;
            Z69ContratadaUsuario_UsuarioCod = A69ContratadaUsuario_UsuarioCod;
            Z34Pessoa_Codigo = A34Pessoa_Codigo;
            Z1083Usuario_Entidade = A1083Usuario_Entidade;
            Z290Usuario_EhAuditorFM = A290Usuario_EhAuditorFM;
         }
         if ( ( GX_JID == 17 ) || ( GX_JID == 0 ) )
         {
            Z1074Usuario_CargoNom = A1074Usuario_CargoNom;
            Z1075Usuario_CargoUOCod = A1075Usuario_CargoUOCod;
            Z66ContratadaUsuario_ContratadaCod = A66ContratadaUsuario_ContratadaCod;
            Z69ContratadaUsuario_UsuarioCod = A69ContratadaUsuario_UsuarioCod;
            Z34Pessoa_Codigo = A34Pessoa_Codigo;
            Z1083Usuario_Entidade = A1083Usuario_Entidade;
            Z290Usuario_EhAuditorFM = A290Usuario_EhAuditorFM;
         }
         if ( ( GX_JID == 18 ) || ( GX_JID == 0 ) )
         {
            Z1076Usuario_CargoUONom = A1076Usuario_CargoUONom;
            Z66ContratadaUsuario_ContratadaCod = A66ContratadaUsuario_ContratadaCod;
            Z69ContratadaUsuario_UsuarioCod = A69ContratadaUsuario_UsuarioCod;
            Z34Pessoa_Codigo = A34Pessoa_Codigo;
            Z1083Usuario_Entidade = A1083Usuario_Entidade;
            Z290Usuario_EhAuditorFM = A290Usuario_EhAuditorFM;
         }
         if ( GX_JID == -15 )
         {
            Z1Usuario_Codigo = A1Usuario_Codigo;
            Z341Usuario_UserGamGuid = A341Usuario_UserGamGuid;
            Z2Usuario_Nome = A2Usuario_Nome;
            Z289Usuario_EhContador = A289Usuario_EhContador;
            Z291Usuario_EhContratada = A291Usuario_EhContratada;
            Z292Usuario_EhContratante = A292Usuario_EhContratante;
            Z293Usuario_EhFinanceiro = A293Usuario_EhFinanceiro;
            Z538Usuario_EhGestor = A538Usuario_EhGestor;
            Z1093Usuario_EhPreposto = A1093Usuario_EhPreposto;
            Z1017Usuario_CrtfPath = A1017Usuario_CrtfPath;
            Z1235Usuario_Notificar = A1235Usuario_Notificar;
            Z54Usuario_Ativo = A54Usuario_Ativo;
            Z1647Usuario_Email = A1647Usuario_Email;
            Z1716Usuario_Foto = A1716Usuario_Foto;
            Z40000Usuario_Foto_GXI = A40000Usuario_Foto_GXI;
            Z1908Usuario_DeFerias = A1908Usuario_DeFerias;
            Z2016Usuario_UltimaArea = A2016Usuario_UltimaArea;
            Z57Usuario_PessoaCod = A57Usuario_PessoaCod;
            Z1073Usuario_CargoCod = A1073Usuario_CargoCod;
            Z1074Usuario_CargoNom = A1074Usuario_CargoNom;
            Z1075Usuario_CargoUOCod = A1075Usuario_CargoUOCod;
            Z1076Usuario_CargoUONom = A1076Usuario_CargoUONom;
            Z58Usuario_PessoaNom = A58Usuario_PessoaNom;
            Z59Usuario_PessoaTip = A59Usuario_PessoaTip;
            Z325Usuario_PessoaDoc = A325Usuario_PessoaDoc;
            Z2095Usuario_PessoaTelefone = A2095Usuario_PessoaTelefone;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         AV32Pgmname = "Usuario_BC";
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A1908Usuario_DeFerias) && ( Gx_BScreen == 0 ) )
         {
            A1908Usuario_DeFerias = false;
            n1908Usuario_DeFerias = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A54Usuario_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A54Usuario_Ativo = true;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A1235Usuario_Notificar)) && ( Gx_BScreen == 0 ) )
         {
            A1235Usuario_Notificar = "A";
            n1235Usuario_Notificar = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A1093Usuario_EhPreposto) && ( Gx_BScreen == 0 ) )
         {
            A1093Usuario_EhPreposto = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A538Usuario_EhGestor) && ( Gx_BScreen == 0 ) )
         {
            A538Usuario_EhGestor = false;
         }
      }

      protected void Load011( )
      {
         /* Using cursor BC00017 */
         pr_default.execute(5, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound1 = 1;
            A1074Usuario_CargoNom = BC00017_A1074Usuario_CargoNom[0];
            n1074Usuario_CargoNom = BC00017_n1074Usuario_CargoNom[0];
            A1076Usuario_CargoUONom = BC00017_A1076Usuario_CargoUONom[0];
            n1076Usuario_CargoUONom = BC00017_n1076Usuario_CargoUONom[0];
            A58Usuario_PessoaNom = BC00017_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = BC00017_n58Usuario_PessoaNom[0];
            A59Usuario_PessoaTip = BC00017_A59Usuario_PessoaTip[0];
            n59Usuario_PessoaTip = BC00017_n59Usuario_PessoaTip[0];
            A325Usuario_PessoaDoc = BC00017_A325Usuario_PessoaDoc[0];
            n325Usuario_PessoaDoc = BC00017_n325Usuario_PessoaDoc[0];
            A2095Usuario_PessoaTelefone = BC00017_A2095Usuario_PessoaTelefone[0];
            n2095Usuario_PessoaTelefone = BC00017_n2095Usuario_PessoaTelefone[0];
            A341Usuario_UserGamGuid = BC00017_A341Usuario_UserGamGuid[0];
            A2Usuario_Nome = BC00017_A2Usuario_Nome[0];
            n2Usuario_Nome = BC00017_n2Usuario_Nome[0];
            A289Usuario_EhContador = BC00017_A289Usuario_EhContador[0];
            A291Usuario_EhContratada = BC00017_A291Usuario_EhContratada[0];
            A292Usuario_EhContratante = BC00017_A292Usuario_EhContratante[0];
            A293Usuario_EhFinanceiro = BC00017_A293Usuario_EhFinanceiro[0];
            A538Usuario_EhGestor = BC00017_A538Usuario_EhGestor[0];
            A1093Usuario_EhPreposto = BC00017_A1093Usuario_EhPreposto[0];
            A1017Usuario_CrtfPath = BC00017_A1017Usuario_CrtfPath[0];
            n1017Usuario_CrtfPath = BC00017_n1017Usuario_CrtfPath[0];
            A1235Usuario_Notificar = BC00017_A1235Usuario_Notificar[0];
            n1235Usuario_Notificar = BC00017_n1235Usuario_Notificar[0];
            A54Usuario_Ativo = BC00017_A54Usuario_Ativo[0];
            A1647Usuario_Email = BC00017_A1647Usuario_Email[0];
            n1647Usuario_Email = BC00017_n1647Usuario_Email[0];
            A40000Usuario_Foto_GXI = BC00017_A40000Usuario_Foto_GXI[0];
            n40000Usuario_Foto_GXI = BC00017_n40000Usuario_Foto_GXI[0];
            A1908Usuario_DeFerias = BC00017_A1908Usuario_DeFerias[0];
            n1908Usuario_DeFerias = BC00017_n1908Usuario_DeFerias[0];
            A2016Usuario_UltimaArea = BC00017_A2016Usuario_UltimaArea[0];
            n2016Usuario_UltimaArea = BC00017_n2016Usuario_UltimaArea[0];
            A57Usuario_PessoaCod = BC00017_A57Usuario_PessoaCod[0];
            A1073Usuario_CargoCod = BC00017_A1073Usuario_CargoCod[0];
            n1073Usuario_CargoCod = BC00017_n1073Usuario_CargoCod[0];
            A1075Usuario_CargoUOCod = BC00017_A1075Usuario_CargoUOCod[0];
            n1075Usuario_CargoUOCod = BC00017_n1075Usuario_CargoUOCod[0];
            A1716Usuario_Foto = BC00017_A1716Usuario_Foto[0];
            n1716Usuario_Foto = BC00017_n1716Usuario_Foto[0];
            ZM011( -15) ;
         }
         pr_default.close(5);
         OnLoadActions011( ) ;
      }

      protected void OnLoadActions011( )
      {
         GXt_char1 = A1083Usuario_Entidade;
         new prc_entidadedousuario(context ).execute(  A1Usuario_Codigo,  0, out  GXt_char1) ;
         A1083Usuario_Entidade = GXt_char1;
         GXt_boolean2 = A290Usuario_EhAuditorFM;
         new prc_usuarioehauditorfm(context ).execute(  A1Usuario_Codigo, out  GXt_boolean2) ;
         A290Usuario_EhAuditorFM = GXt_boolean2;
      }

      protected void CheckExtendedTable011( )
      {
         standaloneModal( ) ;
         GXt_char1 = A1083Usuario_Entidade;
         new prc_entidadedousuario(context ).execute(  A1Usuario_Codigo,  0, out  GXt_char1) ;
         A1083Usuario_Entidade = GXt_char1;
         GXt_boolean2 = A290Usuario_EhAuditorFM;
         new prc_usuarioehauditorfm(context ).execute(  A1Usuario_Codigo, out  GXt_boolean2) ;
         A290Usuario_EhAuditorFM = GXt_boolean2;
         /* Using cursor BC00015 */
         pr_default.execute(3, new Object[] {n1073Usuario_CargoCod, A1073Usuario_CargoCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A1073Usuario_CargoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'USUARIO_CARGO'.", "ForeignKeyNotFound", 1, "USUARIO_CARGOCOD");
               AnyError = 1;
            }
         }
         A1074Usuario_CargoNom = BC00015_A1074Usuario_CargoNom[0];
         n1074Usuario_CargoNom = BC00015_n1074Usuario_CargoNom[0];
         A1075Usuario_CargoUOCod = BC00015_A1075Usuario_CargoUOCod[0];
         n1075Usuario_CargoUOCod = BC00015_n1075Usuario_CargoUOCod[0];
         pr_default.close(3);
         /* Using cursor BC00016 */
         pr_default.execute(4, new Object[] {n1075Usuario_CargoUOCod, A1075Usuario_CargoUOCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A1075Usuario_CargoUOCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Unidade Organizacional'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1076Usuario_CargoUONom = BC00016_A1076Usuario_CargoUONom[0];
         n1076Usuario_CargoUONom = BC00016_n1076Usuario_CargoUONom[0];
         pr_default.close(4);
         /* Using cursor BC00014 */
         pr_default.execute(2, new Object[] {A57Usuario_PessoaCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Usuario_Pessoa'.", "ForeignKeyNotFound", 1, "USUARIO_PESSOACOD");
            AnyError = 1;
         }
         A58Usuario_PessoaNom = BC00014_A58Usuario_PessoaNom[0];
         n58Usuario_PessoaNom = BC00014_n58Usuario_PessoaNom[0];
         A59Usuario_PessoaTip = BC00014_A59Usuario_PessoaTip[0];
         n59Usuario_PessoaTip = BC00014_n59Usuario_PessoaTip[0];
         A325Usuario_PessoaDoc = BC00014_A325Usuario_PessoaDoc[0];
         n325Usuario_PessoaDoc = BC00014_n325Usuario_PessoaDoc[0];
         A2095Usuario_PessoaTelefone = BC00014_A2095Usuario_PessoaTelefone[0];
         n2095Usuario_PessoaTelefone = BC00014_n2095Usuario_PessoaTelefone[0];
         pr_default.close(2);
         if ( ! ( GxRegex.IsMatch(A1647Usuario_Email,"^((\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*)|(\\s*))$") || String.IsNullOrEmpty(StringUtil.RTrim( A1647Usuario_Email)) ) )
         {
            GX_msglist.addItem("O valor de Usuario_Email n�o coincide com o padr�o especificado", "OutOfRange", 1, "");
            AnyError = 1;
         }
      }

      protected void CloseExtendedTableCursors011( )
      {
         pr_default.close(3);
         pr_default.close(4);
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey011( )
      {
         /* Using cursor BC00018 */
         pr_default.execute(6, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound1 = 1;
         }
         else
         {
            RcdFound1 = 0;
         }
         pr_default.close(6);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00013 */
         pr_default.execute(1, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM011( 15) ;
            RcdFound1 = 1;
            A1Usuario_Codigo = BC00013_A1Usuario_Codigo[0];
            n1Usuario_Codigo = BC00013_n1Usuario_Codigo[0];
            A341Usuario_UserGamGuid = BC00013_A341Usuario_UserGamGuid[0];
            A2Usuario_Nome = BC00013_A2Usuario_Nome[0];
            n2Usuario_Nome = BC00013_n2Usuario_Nome[0];
            A289Usuario_EhContador = BC00013_A289Usuario_EhContador[0];
            A291Usuario_EhContratada = BC00013_A291Usuario_EhContratada[0];
            A292Usuario_EhContratante = BC00013_A292Usuario_EhContratante[0];
            A293Usuario_EhFinanceiro = BC00013_A293Usuario_EhFinanceiro[0];
            A538Usuario_EhGestor = BC00013_A538Usuario_EhGestor[0];
            A1093Usuario_EhPreposto = BC00013_A1093Usuario_EhPreposto[0];
            A1017Usuario_CrtfPath = BC00013_A1017Usuario_CrtfPath[0];
            n1017Usuario_CrtfPath = BC00013_n1017Usuario_CrtfPath[0];
            A1235Usuario_Notificar = BC00013_A1235Usuario_Notificar[0];
            n1235Usuario_Notificar = BC00013_n1235Usuario_Notificar[0];
            A54Usuario_Ativo = BC00013_A54Usuario_Ativo[0];
            A1647Usuario_Email = BC00013_A1647Usuario_Email[0];
            n1647Usuario_Email = BC00013_n1647Usuario_Email[0];
            A40000Usuario_Foto_GXI = BC00013_A40000Usuario_Foto_GXI[0];
            n40000Usuario_Foto_GXI = BC00013_n40000Usuario_Foto_GXI[0];
            A1908Usuario_DeFerias = BC00013_A1908Usuario_DeFerias[0];
            n1908Usuario_DeFerias = BC00013_n1908Usuario_DeFerias[0];
            A2016Usuario_UltimaArea = BC00013_A2016Usuario_UltimaArea[0];
            n2016Usuario_UltimaArea = BC00013_n2016Usuario_UltimaArea[0];
            A57Usuario_PessoaCod = BC00013_A57Usuario_PessoaCod[0];
            A1073Usuario_CargoCod = BC00013_A1073Usuario_CargoCod[0];
            n1073Usuario_CargoCod = BC00013_n1073Usuario_CargoCod[0];
            A1716Usuario_Foto = BC00013_A1716Usuario_Foto[0];
            n1716Usuario_Foto = BC00013_n1716Usuario_Foto[0];
            Z1Usuario_Codigo = A1Usuario_Codigo;
            sMode1 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load011( ) ;
            if ( AnyError == 1 )
            {
               RcdFound1 = 0;
               InitializeNonKey011( ) ;
            }
            Gx_mode = sMode1;
         }
         else
         {
            RcdFound1 = 0;
            InitializeNonKey011( ) ;
            sMode1 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode1;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey011( ) ;
         if ( RcdFound1 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_010( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency011( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00012 */
            pr_default.execute(0, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Usuario"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z341Usuario_UserGamGuid, BC00012_A341Usuario_UserGamGuid[0]) != 0 ) || ( StringUtil.StrCmp(Z2Usuario_Nome, BC00012_A2Usuario_Nome[0]) != 0 ) || ( Z289Usuario_EhContador != BC00012_A289Usuario_EhContador[0] ) || ( Z291Usuario_EhContratada != BC00012_A291Usuario_EhContratada[0] ) || ( Z292Usuario_EhContratante != BC00012_A292Usuario_EhContratante[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z293Usuario_EhFinanceiro != BC00012_A293Usuario_EhFinanceiro[0] ) || ( Z538Usuario_EhGestor != BC00012_A538Usuario_EhGestor[0] ) || ( Z1093Usuario_EhPreposto != BC00012_A1093Usuario_EhPreposto[0] ) || ( StringUtil.StrCmp(Z1017Usuario_CrtfPath, BC00012_A1017Usuario_CrtfPath[0]) != 0 ) || ( StringUtil.StrCmp(Z1235Usuario_Notificar, BC00012_A1235Usuario_Notificar[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z54Usuario_Ativo != BC00012_A54Usuario_Ativo[0] ) || ( StringUtil.StrCmp(Z1647Usuario_Email, BC00012_A1647Usuario_Email[0]) != 0 ) || ( Z1908Usuario_DeFerias != BC00012_A1908Usuario_DeFerias[0] ) || ( Z2016Usuario_UltimaArea != BC00012_A2016Usuario_UltimaArea[0] ) || ( Z57Usuario_PessoaCod != BC00012_A57Usuario_PessoaCod[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1073Usuario_CargoCod != BC00012_A1073Usuario_CargoCod[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Usuario"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert011( )
      {
         BeforeValidate011( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable011( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM011( 0) ;
            CheckOptimisticConcurrency011( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm011( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert011( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00019 */
                     pr_default.execute(7, new Object[] {A341Usuario_UserGamGuid, n2Usuario_Nome, A2Usuario_Nome, A289Usuario_EhContador, A291Usuario_EhContratada, A292Usuario_EhContratante, A293Usuario_EhFinanceiro, A538Usuario_EhGestor, A1093Usuario_EhPreposto, n1017Usuario_CrtfPath, A1017Usuario_CrtfPath, n1235Usuario_Notificar, A1235Usuario_Notificar, A54Usuario_Ativo, n1647Usuario_Email, A1647Usuario_Email, n1716Usuario_Foto, A1716Usuario_Foto, n40000Usuario_Foto_GXI, A40000Usuario_Foto_GXI, n1908Usuario_DeFerias, A1908Usuario_DeFerias, n2016Usuario_UltimaArea, A2016Usuario_UltimaArea, A57Usuario_PessoaCod, n1073Usuario_CargoCod, A1073Usuario_CargoCod});
                     A1Usuario_Codigo = BC00019_A1Usuario_Codigo[0];
                     n1Usuario_Codigo = BC00019_n1Usuario_Codigo[0];
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("Usuario") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load011( ) ;
            }
            EndLevel011( ) ;
         }
         CloseExtendedTableCursors011( ) ;
      }

      protected void Update011( )
      {
         BeforeValidate011( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable011( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency011( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm011( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate011( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000110 */
                     pr_default.execute(8, new Object[] {A341Usuario_UserGamGuid, n2Usuario_Nome, A2Usuario_Nome, A289Usuario_EhContador, A291Usuario_EhContratada, A292Usuario_EhContratante, A293Usuario_EhFinanceiro, A538Usuario_EhGestor, A1093Usuario_EhPreposto, n1017Usuario_CrtfPath, A1017Usuario_CrtfPath, n1235Usuario_Notificar, A1235Usuario_Notificar, A54Usuario_Ativo, n1647Usuario_Email, A1647Usuario_Email, n1908Usuario_DeFerias, A1908Usuario_DeFerias, n2016Usuario_UltimaArea, A2016Usuario_UltimaArea, A57Usuario_PessoaCod, n1073Usuario_CargoCod, A1073Usuario_CargoCod, n1Usuario_Codigo, A1Usuario_Codigo});
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("Usuario") ;
                     if ( (pr_default.getStatus(8) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Usuario"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate011( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel011( ) ;
         }
         CloseExtendedTableCursors011( ) ;
      }

      protected void DeferredUpdate011( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor BC000111 */
            pr_default.execute(9, new Object[] {n1716Usuario_Foto, A1716Usuario_Foto, n40000Usuario_Foto_GXI, A40000Usuario_Foto_GXI, n1Usuario_Codigo, A1Usuario_Codigo});
            pr_default.close(9);
            dsDefault.SmartCacheProvider.SetUpdated("Usuario") ;
         }
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate011( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency011( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls011( ) ;
            AfterConfirm011( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete011( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC000112 */
                  pr_default.execute(10, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("Usuario") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode1 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel011( ) ;
         Gx_mode = sMode1;
      }

      protected void OnDeleteControls011( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            GXt_char1 = A1083Usuario_Entidade;
            new prc_entidadedousuario(context ).execute(  A1Usuario_Codigo,  0, out  GXt_char1) ;
            A1083Usuario_Entidade = GXt_char1;
            GXt_boolean2 = A290Usuario_EhAuditorFM;
            new prc_usuarioehauditorfm(context ).execute(  A1Usuario_Codigo, out  GXt_boolean2) ;
            A290Usuario_EhAuditorFM = GXt_boolean2;
            /* Using cursor BC000113 */
            pr_default.execute(11, new Object[] {n1073Usuario_CargoCod, A1073Usuario_CargoCod});
            A1074Usuario_CargoNom = BC000113_A1074Usuario_CargoNom[0];
            n1074Usuario_CargoNom = BC000113_n1074Usuario_CargoNom[0];
            A1075Usuario_CargoUOCod = BC000113_A1075Usuario_CargoUOCod[0];
            n1075Usuario_CargoUOCod = BC000113_n1075Usuario_CargoUOCod[0];
            pr_default.close(11);
            /* Using cursor BC000114 */
            pr_default.execute(12, new Object[] {n1075Usuario_CargoUOCod, A1075Usuario_CargoUOCod});
            A1076Usuario_CargoUONom = BC000114_A1076Usuario_CargoUONom[0];
            n1076Usuario_CargoUONom = BC000114_n1076Usuario_CargoUONom[0];
            pr_default.close(12);
            /* Using cursor BC000115 */
            pr_default.execute(13, new Object[] {A57Usuario_PessoaCod});
            A58Usuario_PessoaNom = BC000115_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = BC000115_n58Usuario_PessoaNom[0];
            A59Usuario_PessoaTip = BC000115_A59Usuario_PessoaTip[0];
            n59Usuario_PessoaTip = BC000115_n59Usuario_PessoaTip[0];
            A325Usuario_PessoaDoc = BC000115_A325Usuario_PessoaDoc[0];
            n325Usuario_PessoaDoc = BC000115_n325Usuario_PessoaDoc[0];
            A2095Usuario_PessoaTelefone = BC000115_A2095Usuario_PessoaTelefone[0];
            n2095Usuario_PessoaTelefone = BC000115_n2095Usuario_PessoaTelefone[0];
            pr_default.close(13);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor BC000116 */
            pr_default.execute(14, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(14) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Usuario Notifica"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(14);
            /* Using cursor BC000117 */
            pr_default.execute(15, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(15) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado QA"+" ("+"Contagem Resultado QA_Para"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(15);
            /* Using cursor BC000118 */
            pr_default.execute(16, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(16) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado QA"+" ("+"Contagem Resultado QA_Usuario"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(16);
            /* Using cursor BC000119 */
            pr_default.execute(17, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(17) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Auxiliar"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(17);
            /* Using cursor BC000120 */
            pr_default.execute(18, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(18) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {" T180"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(18);
            /* Using cursor BC000121 */
            pr_default.execute(19, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(19) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Gest�o"+" ("+"Gestao_Usuario Gestor"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(19);
            /* Using cursor BC000122 */
            pr_default.execute(20, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(20) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Gest�o"+" ("+"Gestao_Usuario"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(20);
            /* Using cursor BC000123 */
            pr_default.execute(21, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(21) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Custo do Servi�o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(21);
            /* Using cursor BC000124 */
            pr_default.execute(22, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(22) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Destinatario"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(22);
            /* Using cursor BC000125 */
            pr_default.execute(23, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(23) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Notifica��es da Contagem"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(23);
            /* Using cursor BC000126 */
            pr_default.execute(24, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(24) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Sistema"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(24);
            /* Using cursor BC000127 */
            pr_default.execute(25, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(25) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"OS"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(25);
            /* Using cursor BC000128 */
            pr_default.execute(26, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(26) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Nota da OS"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(26);
            /* Using cursor BC000129 */
            pr_default.execute(27, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(27) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Gestor do Contrato"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(27);
            /* Using cursor BC000130 */
            pr_default.execute(28, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(28) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Importa��o Log"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(28);
            /* Using cursor BC000131 */
            pr_default.execute(29, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(29) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(29);
            /* Using cursor BC000132 */
            pr_default.execute(30, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(30) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Solicita��o de Mudan�a"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(30);
            /* Using cursor BC000133 */
            pr_default.execute(31, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(31) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Log de a��o dos Respons�veis"+" ("+"Log Responsavel_Usuario Owner"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(31);
            /* Using cursor BC000134 */
            pr_default.execute(32, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(32) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Log de a��o dos Respons�veis"+" ("+"Log Responsavel_Usuarios"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(32);
            /* Using cursor BC000135 */
            pr_default.execute(33, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(33) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Usuario Servicos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(33);
            /* Using cursor BC000136 */
            pr_default.execute(34, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(34) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Chck Lst Log"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(34);
            /* Using cursor BC000137 */
            pr_default.execute(35, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(35) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Baseline"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(35);
            /* Using cursor BC000138 */
            pr_default.execute(36, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(36) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Erros nas confer�ncias de contagens"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(36);
            /* Using cursor BC000139 */
            pr_default.execute(37, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(37) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Lote"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(37);
            /* Using cursor BC000140 */
            pr_default.execute(38, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(38) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Contagens"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(38);
            /* Using cursor BC000141 */
            pr_default.execute(39, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(39) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Resultado das Contagens"+" ("+"Contagem Resultado_Usuario Responsavel"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(39);
            /* Using cursor BC000142 */
            pr_default.execute(40, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(40) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Resultado das Contagens"+" ("+"Contagem Resultado_Usuario Contador Fab. de Soft"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(40);
            /* Using cursor BC000143 */
            pr_default.execute(41, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(41) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Solicitacoes"+" ("+"Solicitacoes_Usuario Ult"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(41);
            /* Using cursor BC000144 */
            pr_default.execute(42, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(42) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Solicitacoes"+" ("+"ST_Solicitacoes_Usuario"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(42);
            /* Using cursor BC000145 */
            pr_default.execute(43, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(43) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Arquivo a processar:"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(43);
            /* Using cursor BC000146 */
            pr_default.execute(44, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(44) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Item Parecer"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(44);
            /* Using cursor BC000147 */
            pr_default.execute(45, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(45) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(45);
            /* Using cursor BC000148 */
            pr_default.execute(46, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(46) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contratada Usuario"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(46);
            /* Using cursor BC000149 */
            pr_default.execute(47, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(47) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contratante Usuario"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(47);
            /* Using cursor BC000150 */
            pr_default.execute(48, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(48) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Audit"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(48);
            /* Using cursor BC000151 */
            pr_default.execute(49, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(49) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {" T237"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(49);
            /* Using cursor BC000152 */
            pr_default.execute(50, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(50) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Linhas selecionadas numa grid"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(50);
            /* Using cursor BC000153 */
            pr_default.execute(51, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(51) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Usuario x Perfil"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(51);
         }
      }

      protected void EndLevel011( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete011( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart011( )
      {
         /* Scan By routine */
         /* Using cursor BC000154 */
         pr_default.execute(52, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
         RcdFound1 = 0;
         if ( (pr_default.getStatus(52) != 101) )
         {
            RcdFound1 = 1;
            A1Usuario_Codigo = BC000154_A1Usuario_Codigo[0];
            n1Usuario_Codigo = BC000154_n1Usuario_Codigo[0];
            A1074Usuario_CargoNom = BC000154_A1074Usuario_CargoNom[0];
            n1074Usuario_CargoNom = BC000154_n1074Usuario_CargoNom[0];
            A1076Usuario_CargoUONom = BC000154_A1076Usuario_CargoUONom[0];
            n1076Usuario_CargoUONom = BC000154_n1076Usuario_CargoUONom[0];
            A58Usuario_PessoaNom = BC000154_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = BC000154_n58Usuario_PessoaNom[0];
            A59Usuario_PessoaTip = BC000154_A59Usuario_PessoaTip[0];
            n59Usuario_PessoaTip = BC000154_n59Usuario_PessoaTip[0];
            A325Usuario_PessoaDoc = BC000154_A325Usuario_PessoaDoc[0];
            n325Usuario_PessoaDoc = BC000154_n325Usuario_PessoaDoc[0];
            A2095Usuario_PessoaTelefone = BC000154_A2095Usuario_PessoaTelefone[0];
            n2095Usuario_PessoaTelefone = BC000154_n2095Usuario_PessoaTelefone[0];
            A341Usuario_UserGamGuid = BC000154_A341Usuario_UserGamGuid[0];
            A2Usuario_Nome = BC000154_A2Usuario_Nome[0];
            n2Usuario_Nome = BC000154_n2Usuario_Nome[0];
            A289Usuario_EhContador = BC000154_A289Usuario_EhContador[0];
            A291Usuario_EhContratada = BC000154_A291Usuario_EhContratada[0];
            A292Usuario_EhContratante = BC000154_A292Usuario_EhContratante[0];
            A293Usuario_EhFinanceiro = BC000154_A293Usuario_EhFinanceiro[0];
            A538Usuario_EhGestor = BC000154_A538Usuario_EhGestor[0];
            A1093Usuario_EhPreposto = BC000154_A1093Usuario_EhPreposto[0];
            A1017Usuario_CrtfPath = BC000154_A1017Usuario_CrtfPath[0];
            n1017Usuario_CrtfPath = BC000154_n1017Usuario_CrtfPath[0];
            A1235Usuario_Notificar = BC000154_A1235Usuario_Notificar[0];
            n1235Usuario_Notificar = BC000154_n1235Usuario_Notificar[0];
            A54Usuario_Ativo = BC000154_A54Usuario_Ativo[0];
            A1647Usuario_Email = BC000154_A1647Usuario_Email[0];
            n1647Usuario_Email = BC000154_n1647Usuario_Email[0];
            A40000Usuario_Foto_GXI = BC000154_A40000Usuario_Foto_GXI[0];
            n40000Usuario_Foto_GXI = BC000154_n40000Usuario_Foto_GXI[0];
            A1908Usuario_DeFerias = BC000154_A1908Usuario_DeFerias[0];
            n1908Usuario_DeFerias = BC000154_n1908Usuario_DeFerias[0];
            A2016Usuario_UltimaArea = BC000154_A2016Usuario_UltimaArea[0];
            n2016Usuario_UltimaArea = BC000154_n2016Usuario_UltimaArea[0];
            A57Usuario_PessoaCod = BC000154_A57Usuario_PessoaCod[0];
            A1073Usuario_CargoCod = BC000154_A1073Usuario_CargoCod[0];
            n1073Usuario_CargoCod = BC000154_n1073Usuario_CargoCod[0];
            A1075Usuario_CargoUOCod = BC000154_A1075Usuario_CargoUOCod[0];
            n1075Usuario_CargoUOCod = BC000154_n1075Usuario_CargoUOCod[0];
            A1716Usuario_Foto = BC000154_A1716Usuario_Foto[0];
            n1716Usuario_Foto = BC000154_n1716Usuario_Foto[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext011( )
      {
         /* Scan next routine */
         pr_default.readNext(52);
         RcdFound1 = 0;
         ScanKeyLoad011( ) ;
      }

      protected void ScanKeyLoad011( )
      {
         sMode1 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(52) != 101) )
         {
            RcdFound1 = 1;
            A1Usuario_Codigo = BC000154_A1Usuario_Codigo[0];
            n1Usuario_Codigo = BC000154_n1Usuario_Codigo[0];
            A1074Usuario_CargoNom = BC000154_A1074Usuario_CargoNom[0];
            n1074Usuario_CargoNom = BC000154_n1074Usuario_CargoNom[0];
            A1076Usuario_CargoUONom = BC000154_A1076Usuario_CargoUONom[0];
            n1076Usuario_CargoUONom = BC000154_n1076Usuario_CargoUONom[0];
            A58Usuario_PessoaNom = BC000154_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = BC000154_n58Usuario_PessoaNom[0];
            A59Usuario_PessoaTip = BC000154_A59Usuario_PessoaTip[0];
            n59Usuario_PessoaTip = BC000154_n59Usuario_PessoaTip[0];
            A325Usuario_PessoaDoc = BC000154_A325Usuario_PessoaDoc[0];
            n325Usuario_PessoaDoc = BC000154_n325Usuario_PessoaDoc[0];
            A2095Usuario_PessoaTelefone = BC000154_A2095Usuario_PessoaTelefone[0];
            n2095Usuario_PessoaTelefone = BC000154_n2095Usuario_PessoaTelefone[0];
            A341Usuario_UserGamGuid = BC000154_A341Usuario_UserGamGuid[0];
            A2Usuario_Nome = BC000154_A2Usuario_Nome[0];
            n2Usuario_Nome = BC000154_n2Usuario_Nome[0];
            A289Usuario_EhContador = BC000154_A289Usuario_EhContador[0];
            A291Usuario_EhContratada = BC000154_A291Usuario_EhContratada[0];
            A292Usuario_EhContratante = BC000154_A292Usuario_EhContratante[0];
            A293Usuario_EhFinanceiro = BC000154_A293Usuario_EhFinanceiro[0];
            A538Usuario_EhGestor = BC000154_A538Usuario_EhGestor[0];
            A1093Usuario_EhPreposto = BC000154_A1093Usuario_EhPreposto[0];
            A1017Usuario_CrtfPath = BC000154_A1017Usuario_CrtfPath[0];
            n1017Usuario_CrtfPath = BC000154_n1017Usuario_CrtfPath[0];
            A1235Usuario_Notificar = BC000154_A1235Usuario_Notificar[0];
            n1235Usuario_Notificar = BC000154_n1235Usuario_Notificar[0];
            A54Usuario_Ativo = BC000154_A54Usuario_Ativo[0];
            A1647Usuario_Email = BC000154_A1647Usuario_Email[0];
            n1647Usuario_Email = BC000154_n1647Usuario_Email[0];
            A40000Usuario_Foto_GXI = BC000154_A40000Usuario_Foto_GXI[0];
            n40000Usuario_Foto_GXI = BC000154_n40000Usuario_Foto_GXI[0];
            A1908Usuario_DeFerias = BC000154_A1908Usuario_DeFerias[0];
            n1908Usuario_DeFerias = BC000154_n1908Usuario_DeFerias[0];
            A2016Usuario_UltimaArea = BC000154_A2016Usuario_UltimaArea[0];
            n2016Usuario_UltimaArea = BC000154_n2016Usuario_UltimaArea[0];
            A57Usuario_PessoaCod = BC000154_A57Usuario_PessoaCod[0];
            A1073Usuario_CargoCod = BC000154_A1073Usuario_CargoCod[0];
            n1073Usuario_CargoCod = BC000154_n1073Usuario_CargoCod[0];
            A1075Usuario_CargoUOCod = BC000154_A1075Usuario_CargoUOCod[0];
            n1075Usuario_CargoUOCod = BC000154_n1075Usuario_CargoUOCod[0];
            A1716Usuario_Foto = BC000154_A1716Usuario_Foto[0];
            n1716Usuario_Foto = BC000154_n1716Usuario_Foto[0];
         }
         Gx_mode = sMode1;
      }

      protected void ScanKeyEnd011( )
      {
         pr_default.close(52);
      }

      protected void AfterConfirm011( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert011( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate011( )
      {
         /* Before Update Rules */
         new loadauditusuario(context ).execute(  "Y", ref  AV28AuditingObject,  A1Usuario_Codigo,  Gx_mode) ;
      }

      protected void BeforeDelete011( )
      {
         /* Before Delete Rules */
         new loadauditusuario(context ).execute(  "Y", ref  AV28AuditingObject,  A1Usuario_Codigo,  Gx_mode) ;
      }

      protected void BeforeComplete011( )
      {
         /* Before Complete Rules */
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditusuario(context ).execute(  "N", ref  AV28AuditingObject,  A1Usuario_Codigo,  Gx_mode) ;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditusuario(context ).execute(  "N", ref  AV28AuditingObject,  A1Usuario_Codigo,  Gx_mode) ;
         }
      }

      protected void BeforeValidate011( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes011( )
      {
      }

      protected void AddRow011( )
      {
         VarsToRow1( bcUsuario) ;
      }

      protected void ReadRow011( )
      {
         RowToVars1( bcUsuario, 1) ;
      }

      protected void InitializeNonKey011( )
      {
         AV28AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         A290Usuario_EhAuditorFM = false;
         A1083Usuario_Entidade = "";
         A1073Usuario_CargoCod = 0;
         n1073Usuario_CargoCod = false;
         A1074Usuario_CargoNom = "";
         n1074Usuario_CargoNom = false;
         A1075Usuario_CargoUOCod = 0;
         n1075Usuario_CargoUOCod = false;
         A1076Usuario_CargoUONom = "";
         n1076Usuario_CargoUONom = false;
         A57Usuario_PessoaCod = 0;
         A58Usuario_PessoaNom = "";
         n58Usuario_PessoaNom = false;
         A59Usuario_PessoaTip = "";
         n59Usuario_PessoaTip = false;
         A325Usuario_PessoaDoc = "";
         n325Usuario_PessoaDoc = false;
         A2095Usuario_PessoaTelefone = "";
         n2095Usuario_PessoaTelefone = false;
         A341Usuario_UserGamGuid = "";
         A2Usuario_Nome = "";
         n2Usuario_Nome = false;
         A289Usuario_EhContador = false;
         A291Usuario_EhContratada = false;
         A292Usuario_EhContratante = false;
         A293Usuario_EhFinanceiro = false;
         A1017Usuario_CrtfPath = "";
         n1017Usuario_CrtfPath = false;
         A1647Usuario_Email = "";
         n1647Usuario_Email = false;
         A1716Usuario_Foto = "";
         n1716Usuario_Foto = false;
         A40000Usuario_Foto_GXI = "";
         n40000Usuario_Foto_GXI = false;
         A2016Usuario_UltimaArea = 0;
         n2016Usuario_UltimaArea = false;
         A538Usuario_EhGestor = false;
         A1093Usuario_EhPreposto = false;
         A1235Usuario_Notificar = "A";
         n1235Usuario_Notificar = false;
         A54Usuario_Ativo = true;
         A1908Usuario_DeFerias = false;
         n1908Usuario_DeFerias = false;
         Z341Usuario_UserGamGuid = "";
         Z2Usuario_Nome = "";
         Z289Usuario_EhContador = false;
         Z291Usuario_EhContratada = false;
         Z292Usuario_EhContratante = false;
         Z293Usuario_EhFinanceiro = false;
         Z538Usuario_EhGestor = false;
         Z1093Usuario_EhPreposto = false;
         Z1017Usuario_CrtfPath = "";
         Z1235Usuario_Notificar = "";
         Z54Usuario_Ativo = false;
         Z1647Usuario_Email = "";
         Z1908Usuario_DeFerias = false;
         Z2016Usuario_UltimaArea = 0;
         Z57Usuario_PessoaCod = 0;
         Z1073Usuario_CargoCod = 0;
      }

      protected void InitAll011( )
      {
         A1Usuario_Codigo = 0;
         n1Usuario_Codigo = false;
         InitializeNonKey011( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A1908Usuario_DeFerias = i1908Usuario_DeFerias;
         n1908Usuario_DeFerias = false;
         A54Usuario_Ativo = i54Usuario_Ativo;
         A1235Usuario_Notificar = i1235Usuario_Notificar;
         n1235Usuario_Notificar = false;
         A1093Usuario_EhPreposto = i1093Usuario_EhPreposto;
         A538Usuario_EhGestor = i538Usuario_EhGestor;
      }

      public void VarsToRow1( SdtUsuario obj1 )
      {
         obj1.gxTpr_Mode = Gx_mode;
         obj1.gxTpr_Usuario_ehauditorfm = A290Usuario_EhAuditorFM;
         obj1.gxTpr_Usuario_entidade = A1083Usuario_Entidade;
         obj1.gxTpr_Usuario_cargocod = A1073Usuario_CargoCod;
         obj1.gxTpr_Usuario_cargonom = A1074Usuario_CargoNom;
         obj1.gxTpr_Usuario_cargouocod = A1075Usuario_CargoUOCod;
         obj1.gxTpr_Usuario_cargouonom = A1076Usuario_CargoUONom;
         obj1.gxTpr_Usuario_pessoacod = A57Usuario_PessoaCod;
         obj1.gxTpr_Usuario_pessoanom = A58Usuario_PessoaNom;
         obj1.gxTpr_Usuario_pessoatip = A59Usuario_PessoaTip;
         obj1.gxTpr_Usuario_pessoadoc = A325Usuario_PessoaDoc;
         obj1.gxTpr_Usuario_pessoatelefone = A2095Usuario_PessoaTelefone;
         obj1.gxTpr_Usuario_usergamguid = A341Usuario_UserGamGuid;
         obj1.gxTpr_Usuario_nome = A2Usuario_Nome;
         obj1.gxTpr_Usuario_ehcontador = A289Usuario_EhContador;
         obj1.gxTpr_Usuario_ehcontratada = A291Usuario_EhContratada;
         obj1.gxTpr_Usuario_ehcontratante = A292Usuario_EhContratante;
         obj1.gxTpr_Usuario_ehfinanceiro = A293Usuario_EhFinanceiro;
         obj1.gxTpr_Usuario_crtfpath = A1017Usuario_CrtfPath;
         obj1.gxTpr_Usuario_email = A1647Usuario_Email;
         obj1.gxTpr_Usuario_foto = A1716Usuario_Foto;
         obj1.gxTpr_Usuario_foto_gxi = A40000Usuario_Foto_GXI;
         obj1.gxTpr_Usuario_ultimaarea = A2016Usuario_UltimaArea;
         obj1.gxTpr_Usuario_ehgestor = A538Usuario_EhGestor;
         obj1.gxTpr_Usuario_ehpreposto = A1093Usuario_EhPreposto;
         obj1.gxTpr_Usuario_notificar = A1235Usuario_Notificar;
         obj1.gxTpr_Usuario_ativo = A54Usuario_Ativo;
         obj1.gxTpr_Usuario_deferias = A1908Usuario_DeFerias;
         obj1.gxTpr_Usuario_codigo = A1Usuario_Codigo;
         obj1.gxTpr_Usuario_nome_Z = Z2Usuario_Nome;
         obj1.gxTpr_Usuario_usergamguid_Z = Z341Usuario_UserGamGuid;
         obj1.gxTpr_Usuario_codigo_Z = Z1Usuario_Codigo;
         obj1.gxTpr_Usuario_cargocod_Z = Z1073Usuario_CargoCod;
         obj1.gxTpr_Usuario_cargonom_Z = Z1074Usuario_CargoNom;
         obj1.gxTpr_Usuario_cargouocod_Z = Z1075Usuario_CargoUOCod;
         obj1.gxTpr_Usuario_cargouonom_Z = Z1076Usuario_CargoUONom;
         obj1.gxTpr_Usuario_entidade_Z = Z1083Usuario_Entidade;
         obj1.gxTpr_Usuario_pessoacod_Z = Z57Usuario_PessoaCod;
         obj1.gxTpr_Usuario_pessoanom_Z = Z58Usuario_PessoaNom;
         obj1.gxTpr_Usuario_pessoatip_Z = Z59Usuario_PessoaTip;
         obj1.gxTpr_Usuario_pessoadoc_Z = Z325Usuario_PessoaDoc;
         obj1.gxTpr_Usuario_pessoatelefone_Z = Z2095Usuario_PessoaTelefone;
         obj1.gxTpr_Usuario_usergamguid_Z = Z341Usuario_UserGamGuid;
         obj1.gxTpr_Usuario_nome_Z = Z2Usuario_Nome;
         obj1.gxTpr_Usuario_ehcontador_Z = Z289Usuario_EhContador;
         obj1.gxTpr_Usuario_ehauditorfm_Z = Z290Usuario_EhAuditorFM;
         obj1.gxTpr_Usuario_ehcontratada_Z = Z291Usuario_EhContratada;
         obj1.gxTpr_Usuario_ehcontratante_Z = Z292Usuario_EhContratante;
         obj1.gxTpr_Usuario_ehfinanceiro_Z = Z293Usuario_EhFinanceiro;
         obj1.gxTpr_Usuario_ehgestor_Z = Z538Usuario_EhGestor;
         obj1.gxTpr_Usuario_ehpreposto_Z = Z1093Usuario_EhPreposto;
         obj1.gxTpr_Usuario_crtfpath_Z = Z1017Usuario_CrtfPath;
         obj1.gxTpr_Usuario_notificar_Z = Z1235Usuario_Notificar;
         obj1.gxTpr_Usuario_ativo_Z = Z54Usuario_Ativo;
         obj1.gxTpr_Usuario_email_Z = Z1647Usuario_Email;
         obj1.gxTpr_Usuario_deferias_Z = Z1908Usuario_DeFerias;
         obj1.gxTpr_Usuario_ultimaarea_Z = Z2016Usuario_UltimaArea;
         obj1.gxTpr_Usuario_foto_gxi_Z = Z40000Usuario_Foto_GXI;
         obj1.gxTpr_Usuario_codigo_N = (short)(Convert.ToInt16(n1Usuario_Codigo));
         obj1.gxTpr_Usuario_cargocod_N = (short)(Convert.ToInt16(n1073Usuario_CargoCod));
         obj1.gxTpr_Usuario_cargonom_N = (short)(Convert.ToInt16(n1074Usuario_CargoNom));
         obj1.gxTpr_Usuario_cargouocod_N = (short)(Convert.ToInt16(n1075Usuario_CargoUOCod));
         obj1.gxTpr_Usuario_cargouonom_N = (short)(Convert.ToInt16(n1076Usuario_CargoUONom));
         obj1.gxTpr_Usuario_pessoanom_N = (short)(Convert.ToInt16(n58Usuario_PessoaNom));
         obj1.gxTpr_Usuario_pessoatip_N = (short)(Convert.ToInt16(n59Usuario_PessoaTip));
         obj1.gxTpr_Usuario_pessoadoc_N = (short)(Convert.ToInt16(n325Usuario_PessoaDoc));
         obj1.gxTpr_Usuario_pessoatelefone_N = (short)(Convert.ToInt16(n2095Usuario_PessoaTelefone));
         obj1.gxTpr_Usuario_nome_N = (short)(Convert.ToInt16(n2Usuario_Nome));
         obj1.gxTpr_Usuario_crtfpath_N = (short)(Convert.ToInt16(n1017Usuario_CrtfPath));
         obj1.gxTpr_Usuario_notificar_N = (short)(Convert.ToInt16(n1235Usuario_Notificar));
         obj1.gxTpr_Usuario_email_N = (short)(Convert.ToInt16(n1647Usuario_Email));
         obj1.gxTpr_Usuario_foto_N = (short)(Convert.ToInt16(n1716Usuario_Foto));
         obj1.gxTpr_Usuario_deferias_N = (short)(Convert.ToInt16(n1908Usuario_DeFerias));
         obj1.gxTpr_Usuario_ultimaarea_N = (short)(Convert.ToInt16(n2016Usuario_UltimaArea));
         obj1.gxTpr_Usuario_foto_gxi_N = (short)(Convert.ToInt16(n40000Usuario_Foto_GXI));
         obj1.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow1( SdtUsuario obj1 )
      {
         obj1.gxTpr_Usuario_codigo = A1Usuario_Codigo;
         return  ;
      }

      public void RowToVars1( SdtUsuario obj1 ,
                              int forceLoad )
      {
         Gx_mode = obj1.gxTpr_Mode;
         A290Usuario_EhAuditorFM = obj1.gxTpr_Usuario_ehauditorfm;
         A1083Usuario_Entidade = obj1.gxTpr_Usuario_entidade;
         A1073Usuario_CargoCod = obj1.gxTpr_Usuario_cargocod;
         n1073Usuario_CargoCod = false;
         A1074Usuario_CargoNom = obj1.gxTpr_Usuario_cargonom;
         n1074Usuario_CargoNom = false;
         A1075Usuario_CargoUOCod = obj1.gxTpr_Usuario_cargouocod;
         n1075Usuario_CargoUOCod = false;
         A1076Usuario_CargoUONom = obj1.gxTpr_Usuario_cargouonom;
         n1076Usuario_CargoUONom = false;
         A57Usuario_PessoaCod = obj1.gxTpr_Usuario_pessoacod;
         A58Usuario_PessoaNom = obj1.gxTpr_Usuario_pessoanom;
         n58Usuario_PessoaNom = false;
         A59Usuario_PessoaTip = obj1.gxTpr_Usuario_pessoatip;
         n59Usuario_PessoaTip = false;
         A325Usuario_PessoaDoc = obj1.gxTpr_Usuario_pessoadoc;
         n325Usuario_PessoaDoc = false;
         A2095Usuario_PessoaTelefone = obj1.gxTpr_Usuario_pessoatelefone;
         n2095Usuario_PessoaTelefone = false;
         A341Usuario_UserGamGuid = obj1.gxTpr_Usuario_usergamguid;
         A2Usuario_Nome = obj1.gxTpr_Usuario_nome;
         n2Usuario_Nome = false;
         A289Usuario_EhContador = obj1.gxTpr_Usuario_ehcontador;
         A291Usuario_EhContratada = obj1.gxTpr_Usuario_ehcontratada;
         A292Usuario_EhContratante = obj1.gxTpr_Usuario_ehcontratante;
         A293Usuario_EhFinanceiro = obj1.gxTpr_Usuario_ehfinanceiro;
         A1017Usuario_CrtfPath = obj1.gxTpr_Usuario_crtfpath;
         n1017Usuario_CrtfPath = false;
         A1647Usuario_Email = obj1.gxTpr_Usuario_email;
         n1647Usuario_Email = false;
         A1716Usuario_Foto = obj1.gxTpr_Usuario_foto;
         n1716Usuario_Foto = false;
         A40000Usuario_Foto_GXI = obj1.gxTpr_Usuario_foto_gxi;
         n40000Usuario_Foto_GXI = false;
         A2016Usuario_UltimaArea = obj1.gxTpr_Usuario_ultimaarea;
         n2016Usuario_UltimaArea = false;
         A538Usuario_EhGestor = obj1.gxTpr_Usuario_ehgestor;
         A1093Usuario_EhPreposto = obj1.gxTpr_Usuario_ehpreposto;
         A1235Usuario_Notificar = obj1.gxTpr_Usuario_notificar;
         n1235Usuario_Notificar = false;
         A54Usuario_Ativo = obj1.gxTpr_Usuario_ativo;
         A1908Usuario_DeFerias = obj1.gxTpr_Usuario_deferias;
         n1908Usuario_DeFerias = false;
         A1Usuario_Codigo = obj1.gxTpr_Usuario_codigo;
         n1Usuario_Codigo = false;
         Z2Usuario_Nome = obj1.gxTpr_Usuario_nome_Z;
         Z341Usuario_UserGamGuid = obj1.gxTpr_Usuario_usergamguid_Z;
         Z1Usuario_Codigo = obj1.gxTpr_Usuario_codigo_Z;
         Z1073Usuario_CargoCod = obj1.gxTpr_Usuario_cargocod_Z;
         Z1074Usuario_CargoNom = obj1.gxTpr_Usuario_cargonom_Z;
         Z1075Usuario_CargoUOCod = obj1.gxTpr_Usuario_cargouocod_Z;
         Z1076Usuario_CargoUONom = obj1.gxTpr_Usuario_cargouonom_Z;
         Z1083Usuario_Entidade = obj1.gxTpr_Usuario_entidade_Z;
         Z57Usuario_PessoaCod = obj1.gxTpr_Usuario_pessoacod_Z;
         Z58Usuario_PessoaNom = obj1.gxTpr_Usuario_pessoanom_Z;
         Z59Usuario_PessoaTip = obj1.gxTpr_Usuario_pessoatip_Z;
         Z325Usuario_PessoaDoc = obj1.gxTpr_Usuario_pessoadoc_Z;
         Z2095Usuario_PessoaTelefone = obj1.gxTpr_Usuario_pessoatelefone_Z;
         Z341Usuario_UserGamGuid = obj1.gxTpr_Usuario_usergamguid_Z;
         Z2Usuario_Nome = obj1.gxTpr_Usuario_nome_Z;
         Z289Usuario_EhContador = obj1.gxTpr_Usuario_ehcontador_Z;
         Z290Usuario_EhAuditorFM = obj1.gxTpr_Usuario_ehauditorfm_Z;
         Z291Usuario_EhContratada = obj1.gxTpr_Usuario_ehcontratada_Z;
         Z292Usuario_EhContratante = obj1.gxTpr_Usuario_ehcontratante_Z;
         Z293Usuario_EhFinanceiro = obj1.gxTpr_Usuario_ehfinanceiro_Z;
         Z538Usuario_EhGestor = obj1.gxTpr_Usuario_ehgestor_Z;
         Z1093Usuario_EhPreposto = obj1.gxTpr_Usuario_ehpreposto_Z;
         Z1017Usuario_CrtfPath = obj1.gxTpr_Usuario_crtfpath_Z;
         Z1235Usuario_Notificar = obj1.gxTpr_Usuario_notificar_Z;
         Z54Usuario_Ativo = obj1.gxTpr_Usuario_ativo_Z;
         Z1647Usuario_Email = obj1.gxTpr_Usuario_email_Z;
         Z1908Usuario_DeFerias = obj1.gxTpr_Usuario_deferias_Z;
         Z2016Usuario_UltimaArea = obj1.gxTpr_Usuario_ultimaarea_Z;
         Z40000Usuario_Foto_GXI = obj1.gxTpr_Usuario_foto_gxi_Z;
         n1Usuario_Codigo = (bool)(Convert.ToBoolean(obj1.gxTpr_Usuario_codigo_N));
         n1073Usuario_CargoCod = (bool)(Convert.ToBoolean(obj1.gxTpr_Usuario_cargocod_N));
         n1074Usuario_CargoNom = (bool)(Convert.ToBoolean(obj1.gxTpr_Usuario_cargonom_N));
         n1075Usuario_CargoUOCod = (bool)(Convert.ToBoolean(obj1.gxTpr_Usuario_cargouocod_N));
         n1076Usuario_CargoUONom = (bool)(Convert.ToBoolean(obj1.gxTpr_Usuario_cargouonom_N));
         n58Usuario_PessoaNom = (bool)(Convert.ToBoolean(obj1.gxTpr_Usuario_pessoanom_N));
         n59Usuario_PessoaTip = (bool)(Convert.ToBoolean(obj1.gxTpr_Usuario_pessoatip_N));
         n325Usuario_PessoaDoc = (bool)(Convert.ToBoolean(obj1.gxTpr_Usuario_pessoadoc_N));
         n2095Usuario_PessoaTelefone = (bool)(Convert.ToBoolean(obj1.gxTpr_Usuario_pessoatelefone_N));
         n2Usuario_Nome = (bool)(Convert.ToBoolean(obj1.gxTpr_Usuario_nome_N));
         n1017Usuario_CrtfPath = (bool)(Convert.ToBoolean(obj1.gxTpr_Usuario_crtfpath_N));
         n1235Usuario_Notificar = (bool)(Convert.ToBoolean(obj1.gxTpr_Usuario_notificar_N));
         n1647Usuario_Email = (bool)(Convert.ToBoolean(obj1.gxTpr_Usuario_email_N));
         n1716Usuario_Foto = (bool)(Convert.ToBoolean(obj1.gxTpr_Usuario_foto_N));
         n1908Usuario_DeFerias = (bool)(Convert.ToBoolean(obj1.gxTpr_Usuario_deferias_N));
         n2016Usuario_UltimaArea = (bool)(Convert.ToBoolean(obj1.gxTpr_Usuario_ultimaarea_N));
         n40000Usuario_Foto_GXI = (bool)(Convert.ToBoolean(obj1.gxTpr_Usuario_foto_gxi_N));
         Gx_mode = obj1.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A1Usuario_Codigo = (int)getParm(obj,0);
         n1Usuario_Codigo = false;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey011( ) ;
         ScanKeyStart011( ) ;
         if ( RcdFound1 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1Usuario_Codigo = A1Usuario_Codigo;
         }
         ZM011( -15) ;
         OnLoadActions011( ) ;
         AddRow011( ) ;
         ScanKeyEnd011( ) ;
         if ( RcdFound1 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars1( bcUsuario, 0) ;
         ScanKeyStart011( ) ;
         if ( RcdFound1 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1Usuario_Codigo = A1Usuario_Codigo;
         }
         ZM011( -15) ;
         OnLoadActions011( ) ;
         AddRow011( ) ;
         ScanKeyEnd011( ) ;
         if ( RcdFound1 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars1( bcUsuario, 0) ;
         nKeyPressed = 1;
         GetKey011( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert011( ) ;
         }
         else
         {
            if ( RcdFound1 == 1 )
            {
               if ( A1Usuario_Codigo != Z1Usuario_Codigo )
               {
                  A1Usuario_Codigo = Z1Usuario_Codigo;
                  n1Usuario_Codigo = false;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update011( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A1Usuario_Codigo != Z1Usuario_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert011( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert011( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow1( bcUsuario) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars1( bcUsuario, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey011( ) ;
         if ( RcdFound1 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A1Usuario_Codigo != Z1Usuario_Codigo )
            {
               A1Usuario_Codigo = Z1Usuario_Codigo;
               n1Usuario_Codigo = false;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A1Usuario_Codigo != Z1Usuario_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(13);
         pr_default.close(11);
         pr_default.close(12);
         context.RollbackDataStores( "Usuario_BC");
         VarsToRow1( bcUsuario) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcUsuario.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcUsuario.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcUsuario )
         {
            bcUsuario = (SdtUsuario)(sdt);
            if ( StringUtil.StrCmp(bcUsuario.gxTpr_Mode, "") == 0 )
            {
               bcUsuario.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow1( bcUsuario) ;
            }
            else
            {
               RowToVars1( bcUsuario, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcUsuario.gxTpr_Mode, "") == 0 )
            {
               bcUsuario.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars1( bcUsuario, 1) ;
         return  ;
      }

      public SdtUsuario Usuario_BC
      {
         get {
            return bcUsuario ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(13);
         pr_default.close(11);
         pr_default.close(12);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV32Pgmname = "";
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV28AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         Z341Usuario_UserGamGuid = "";
         A341Usuario_UserGamGuid = "";
         Z2Usuario_Nome = "";
         A2Usuario_Nome = "";
         Z1017Usuario_CrtfPath = "";
         A1017Usuario_CrtfPath = "";
         Z1235Usuario_Notificar = "";
         A1235Usuario_Notificar = "";
         Z1647Usuario_Email = "";
         A1647Usuario_Email = "";
         Z1083Usuario_Entidade = "";
         A1083Usuario_Entidade = "";
         Z58Usuario_PessoaNom = "";
         A58Usuario_PessoaNom = "";
         Z59Usuario_PessoaTip = "";
         A59Usuario_PessoaTip = "";
         Z325Usuario_PessoaDoc = "";
         A325Usuario_PessoaDoc = "";
         Z2095Usuario_PessoaTelefone = "";
         A2095Usuario_PessoaTelefone = "";
         Z1074Usuario_CargoNom = "";
         A1074Usuario_CargoNom = "";
         Z1076Usuario_CargoUONom = "";
         A1076Usuario_CargoUONom = "";
         Z1716Usuario_Foto = "";
         A1716Usuario_Foto = "";
         Z40000Usuario_Foto_GXI = "";
         A40000Usuario_Foto_GXI = "";
         BC00017_A1Usuario_Codigo = new int[1] ;
         BC00017_n1Usuario_Codigo = new bool[] {false} ;
         BC00017_A1074Usuario_CargoNom = new String[] {""} ;
         BC00017_n1074Usuario_CargoNom = new bool[] {false} ;
         BC00017_A1076Usuario_CargoUONom = new String[] {""} ;
         BC00017_n1076Usuario_CargoUONom = new bool[] {false} ;
         BC00017_A58Usuario_PessoaNom = new String[] {""} ;
         BC00017_n58Usuario_PessoaNom = new bool[] {false} ;
         BC00017_A59Usuario_PessoaTip = new String[] {""} ;
         BC00017_n59Usuario_PessoaTip = new bool[] {false} ;
         BC00017_A325Usuario_PessoaDoc = new String[] {""} ;
         BC00017_n325Usuario_PessoaDoc = new bool[] {false} ;
         BC00017_A2095Usuario_PessoaTelefone = new String[] {""} ;
         BC00017_n2095Usuario_PessoaTelefone = new bool[] {false} ;
         BC00017_A341Usuario_UserGamGuid = new String[] {""} ;
         BC00017_A2Usuario_Nome = new String[] {""} ;
         BC00017_n2Usuario_Nome = new bool[] {false} ;
         BC00017_A289Usuario_EhContador = new bool[] {false} ;
         BC00017_A291Usuario_EhContratada = new bool[] {false} ;
         BC00017_A292Usuario_EhContratante = new bool[] {false} ;
         BC00017_A293Usuario_EhFinanceiro = new bool[] {false} ;
         BC00017_A538Usuario_EhGestor = new bool[] {false} ;
         BC00017_A1093Usuario_EhPreposto = new bool[] {false} ;
         BC00017_A1017Usuario_CrtfPath = new String[] {""} ;
         BC00017_n1017Usuario_CrtfPath = new bool[] {false} ;
         BC00017_A1235Usuario_Notificar = new String[] {""} ;
         BC00017_n1235Usuario_Notificar = new bool[] {false} ;
         BC00017_A54Usuario_Ativo = new bool[] {false} ;
         BC00017_A1647Usuario_Email = new String[] {""} ;
         BC00017_n1647Usuario_Email = new bool[] {false} ;
         BC00017_A40000Usuario_Foto_GXI = new String[] {""} ;
         BC00017_n40000Usuario_Foto_GXI = new bool[] {false} ;
         BC00017_A1908Usuario_DeFerias = new bool[] {false} ;
         BC00017_n1908Usuario_DeFerias = new bool[] {false} ;
         BC00017_A2016Usuario_UltimaArea = new int[1] ;
         BC00017_n2016Usuario_UltimaArea = new bool[] {false} ;
         BC00017_A57Usuario_PessoaCod = new int[1] ;
         BC00017_A1073Usuario_CargoCod = new int[1] ;
         BC00017_n1073Usuario_CargoCod = new bool[] {false} ;
         BC00017_A1075Usuario_CargoUOCod = new int[1] ;
         BC00017_n1075Usuario_CargoUOCod = new bool[] {false} ;
         BC00017_A1716Usuario_Foto = new String[] {""} ;
         BC00017_n1716Usuario_Foto = new bool[] {false} ;
         BC00015_A1074Usuario_CargoNom = new String[] {""} ;
         BC00015_n1074Usuario_CargoNom = new bool[] {false} ;
         BC00015_A1075Usuario_CargoUOCod = new int[1] ;
         BC00015_n1075Usuario_CargoUOCod = new bool[] {false} ;
         BC00016_A1076Usuario_CargoUONom = new String[] {""} ;
         BC00016_n1076Usuario_CargoUONom = new bool[] {false} ;
         BC00014_A58Usuario_PessoaNom = new String[] {""} ;
         BC00014_n58Usuario_PessoaNom = new bool[] {false} ;
         BC00014_A59Usuario_PessoaTip = new String[] {""} ;
         BC00014_n59Usuario_PessoaTip = new bool[] {false} ;
         BC00014_A325Usuario_PessoaDoc = new String[] {""} ;
         BC00014_n325Usuario_PessoaDoc = new bool[] {false} ;
         BC00014_A2095Usuario_PessoaTelefone = new String[] {""} ;
         BC00014_n2095Usuario_PessoaTelefone = new bool[] {false} ;
         BC00018_A1Usuario_Codigo = new int[1] ;
         BC00018_n1Usuario_Codigo = new bool[] {false} ;
         BC00013_A1Usuario_Codigo = new int[1] ;
         BC00013_n1Usuario_Codigo = new bool[] {false} ;
         BC00013_A341Usuario_UserGamGuid = new String[] {""} ;
         BC00013_A2Usuario_Nome = new String[] {""} ;
         BC00013_n2Usuario_Nome = new bool[] {false} ;
         BC00013_A289Usuario_EhContador = new bool[] {false} ;
         BC00013_A291Usuario_EhContratada = new bool[] {false} ;
         BC00013_A292Usuario_EhContratante = new bool[] {false} ;
         BC00013_A293Usuario_EhFinanceiro = new bool[] {false} ;
         BC00013_A538Usuario_EhGestor = new bool[] {false} ;
         BC00013_A1093Usuario_EhPreposto = new bool[] {false} ;
         BC00013_A1017Usuario_CrtfPath = new String[] {""} ;
         BC00013_n1017Usuario_CrtfPath = new bool[] {false} ;
         BC00013_A1235Usuario_Notificar = new String[] {""} ;
         BC00013_n1235Usuario_Notificar = new bool[] {false} ;
         BC00013_A54Usuario_Ativo = new bool[] {false} ;
         BC00013_A1647Usuario_Email = new String[] {""} ;
         BC00013_n1647Usuario_Email = new bool[] {false} ;
         BC00013_A40000Usuario_Foto_GXI = new String[] {""} ;
         BC00013_n40000Usuario_Foto_GXI = new bool[] {false} ;
         BC00013_A1908Usuario_DeFerias = new bool[] {false} ;
         BC00013_n1908Usuario_DeFerias = new bool[] {false} ;
         BC00013_A2016Usuario_UltimaArea = new int[1] ;
         BC00013_n2016Usuario_UltimaArea = new bool[] {false} ;
         BC00013_A57Usuario_PessoaCod = new int[1] ;
         BC00013_A1073Usuario_CargoCod = new int[1] ;
         BC00013_n1073Usuario_CargoCod = new bool[] {false} ;
         BC00013_A1716Usuario_Foto = new String[] {""} ;
         BC00013_n1716Usuario_Foto = new bool[] {false} ;
         sMode1 = "";
         BC00012_A1Usuario_Codigo = new int[1] ;
         BC00012_n1Usuario_Codigo = new bool[] {false} ;
         BC00012_A341Usuario_UserGamGuid = new String[] {""} ;
         BC00012_A2Usuario_Nome = new String[] {""} ;
         BC00012_n2Usuario_Nome = new bool[] {false} ;
         BC00012_A289Usuario_EhContador = new bool[] {false} ;
         BC00012_A291Usuario_EhContratada = new bool[] {false} ;
         BC00012_A292Usuario_EhContratante = new bool[] {false} ;
         BC00012_A293Usuario_EhFinanceiro = new bool[] {false} ;
         BC00012_A538Usuario_EhGestor = new bool[] {false} ;
         BC00012_A1093Usuario_EhPreposto = new bool[] {false} ;
         BC00012_A1017Usuario_CrtfPath = new String[] {""} ;
         BC00012_n1017Usuario_CrtfPath = new bool[] {false} ;
         BC00012_A1235Usuario_Notificar = new String[] {""} ;
         BC00012_n1235Usuario_Notificar = new bool[] {false} ;
         BC00012_A54Usuario_Ativo = new bool[] {false} ;
         BC00012_A1647Usuario_Email = new String[] {""} ;
         BC00012_n1647Usuario_Email = new bool[] {false} ;
         BC00012_A40000Usuario_Foto_GXI = new String[] {""} ;
         BC00012_n40000Usuario_Foto_GXI = new bool[] {false} ;
         BC00012_A1908Usuario_DeFerias = new bool[] {false} ;
         BC00012_n1908Usuario_DeFerias = new bool[] {false} ;
         BC00012_A2016Usuario_UltimaArea = new int[1] ;
         BC00012_n2016Usuario_UltimaArea = new bool[] {false} ;
         BC00012_A57Usuario_PessoaCod = new int[1] ;
         BC00012_A1073Usuario_CargoCod = new int[1] ;
         BC00012_n1073Usuario_CargoCod = new bool[] {false} ;
         BC00012_A1716Usuario_Foto = new String[] {""} ;
         BC00012_n1716Usuario_Foto = new bool[] {false} ;
         BC00019_A1Usuario_Codigo = new int[1] ;
         BC00019_n1Usuario_Codigo = new bool[] {false} ;
         GXt_char1 = "";
         BC000113_A1074Usuario_CargoNom = new String[] {""} ;
         BC000113_n1074Usuario_CargoNom = new bool[] {false} ;
         BC000113_A1075Usuario_CargoUOCod = new int[1] ;
         BC000113_n1075Usuario_CargoUOCod = new bool[] {false} ;
         BC000114_A1076Usuario_CargoUONom = new String[] {""} ;
         BC000114_n1076Usuario_CargoUONom = new bool[] {false} ;
         BC000115_A58Usuario_PessoaNom = new String[] {""} ;
         BC000115_n58Usuario_PessoaNom = new bool[] {false} ;
         BC000115_A59Usuario_PessoaTip = new String[] {""} ;
         BC000115_n59Usuario_PessoaTip = new bool[] {false} ;
         BC000115_A325Usuario_PessoaDoc = new String[] {""} ;
         BC000115_n325Usuario_PessoaDoc = new bool[] {false} ;
         BC000115_A2095Usuario_PessoaTelefone = new String[] {""} ;
         BC000115_n2095Usuario_PessoaTelefone = new bool[] {false} ;
         BC000116_A2077UsuarioNotifica_Codigo = new int[1] ;
         BC000117_A1984ContagemResultadoQA_Codigo = new int[1] ;
         BC000118_A1984ContagemResultadoQA_Codigo = new int[1] ;
         BC000119_A1824ContratoAuxiliar_ContratoCod = new int[1] ;
         BC000119_A1825ContratoAuxiliar_UsuarioCod = new int[1] ;
         BC000120_A1562HistoricoConsumo_Codigo = new int[1] ;
         BC000121_A1482Gestao_Codigo = new int[1] ;
         BC000122_A1482Gestao_Codigo = new int[1] ;
         BC000123_A1473ContratoServicosCusto_Codigo = new int[1] ;
         BC000124_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         BC000124_A1414ContagemResultadoNotificacao_DestCod = new int[1] ;
         BC000125_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         BC000126_A127Sistema_Codigo = new int[1] ;
         BC000127_A1033ContagemResultadoLiqLog_Codigo = new int[1] ;
         BC000127_A1370ContagemResultadoLiqLogOS_Codigo = new int[1] ;
         BC000128_A1331ContagemResultadoNota_Codigo = new int[1] ;
         BC000129_A1078ContratoGestor_ContratoCod = new int[1] ;
         BC000129_A1079ContratoGestor_UsuarioCod = new int[1] ;
         BC000130_A1041ContagemResultadoImpLog_Codigo = new int[1] ;
         BC000131_A74Contrato_Codigo = new int[1] ;
         BC000132_A996SolicitacaoMudanca_Codigo = new int[1] ;
         BC000133_A1797LogResponsavel_Codigo = new long[1] ;
         BC000134_A1797LogResponsavel_Codigo = new long[1] ;
         BC000135_A828UsuarioServicos_UsuarioCod = new int[1] ;
         BC000135_A829UsuarioServicos_ServicoCod = new int[1] ;
         BC000136_A820ContagemResultadoChckLstLog_Codigo = new int[1] ;
         BC000137_A722Baseline_Codigo = new int[1] ;
         BC000138_A456ContagemResultado_Codigo = new int[1] ;
         BC000138_A579ContagemResultadoErro_Tipo = new String[] {""} ;
         BC000139_A596Lote_Codigo = new int[1] ;
         BC000140_A456ContagemResultado_Codigo = new int[1] ;
         BC000140_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         BC000140_A511ContagemResultado_HoraCnt = new String[] {""} ;
         BC000141_A456ContagemResultado_Codigo = new int[1] ;
         BC000142_A456ContagemResultado_Codigo = new int[1] ;
         BC000143_A439Solicitacoes_Codigo = new int[1] ;
         BC000144_A439Solicitacoes_Codigo = new int[1] ;
         BC000145_A435File_UsuarioCod = new int[1] ;
         BC000145_A504File_Row = new int[1] ;
         BC000146_A243ContagemItemParecer_Codigo = new int[1] ;
         BC000147_A192Contagem_Codigo = new int[1] ;
         BC000148_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         BC000148_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         BC000149_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         BC000149_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         BC000150_A1430AuditId = new long[1] ;
         BC000151_A2175Equipe_Codigo = new int[1] ;
         BC000151_A1Usuario_Codigo = new int[1] ;
         BC000151_n1Usuario_Codigo = new bool[] {false} ;
         BC000152_A1Usuario_Codigo = new int[1] ;
         BC000152_n1Usuario_Codigo = new bool[] {false} ;
         BC000152_A487Selected_Codigo = new int[1] ;
         BC000152_A488Selected_Flag = new bool[] {false} ;
         BC000153_A1Usuario_Codigo = new int[1] ;
         BC000153_n1Usuario_Codigo = new bool[] {false} ;
         BC000153_A3Perfil_Codigo = new int[1] ;
         BC000154_A1Usuario_Codigo = new int[1] ;
         BC000154_n1Usuario_Codigo = new bool[] {false} ;
         BC000154_A1074Usuario_CargoNom = new String[] {""} ;
         BC000154_n1074Usuario_CargoNom = new bool[] {false} ;
         BC000154_A1076Usuario_CargoUONom = new String[] {""} ;
         BC000154_n1076Usuario_CargoUONom = new bool[] {false} ;
         BC000154_A58Usuario_PessoaNom = new String[] {""} ;
         BC000154_n58Usuario_PessoaNom = new bool[] {false} ;
         BC000154_A59Usuario_PessoaTip = new String[] {""} ;
         BC000154_n59Usuario_PessoaTip = new bool[] {false} ;
         BC000154_A325Usuario_PessoaDoc = new String[] {""} ;
         BC000154_n325Usuario_PessoaDoc = new bool[] {false} ;
         BC000154_A2095Usuario_PessoaTelefone = new String[] {""} ;
         BC000154_n2095Usuario_PessoaTelefone = new bool[] {false} ;
         BC000154_A341Usuario_UserGamGuid = new String[] {""} ;
         BC000154_A2Usuario_Nome = new String[] {""} ;
         BC000154_n2Usuario_Nome = new bool[] {false} ;
         BC000154_A289Usuario_EhContador = new bool[] {false} ;
         BC000154_A291Usuario_EhContratada = new bool[] {false} ;
         BC000154_A292Usuario_EhContratante = new bool[] {false} ;
         BC000154_A293Usuario_EhFinanceiro = new bool[] {false} ;
         BC000154_A538Usuario_EhGestor = new bool[] {false} ;
         BC000154_A1093Usuario_EhPreposto = new bool[] {false} ;
         BC000154_A1017Usuario_CrtfPath = new String[] {""} ;
         BC000154_n1017Usuario_CrtfPath = new bool[] {false} ;
         BC000154_A1235Usuario_Notificar = new String[] {""} ;
         BC000154_n1235Usuario_Notificar = new bool[] {false} ;
         BC000154_A54Usuario_Ativo = new bool[] {false} ;
         BC000154_A1647Usuario_Email = new String[] {""} ;
         BC000154_n1647Usuario_Email = new bool[] {false} ;
         BC000154_A40000Usuario_Foto_GXI = new String[] {""} ;
         BC000154_n40000Usuario_Foto_GXI = new bool[] {false} ;
         BC000154_A1908Usuario_DeFerias = new bool[] {false} ;
         BC000154_n1908Usuario_DeFerias = new bool[] {false} ;
         BC000154_A2016Usuario_UltimaArea = new int[1] ;
         BC000154_n2016Usuario_UltimaArea = new bool[] {false} ;
         BC000154_A57Usuario_PessoaCod = new int[1] ;
         BC000154_A1073Usuario_CargoCod = new int[1] ;
         BC000154_n1073Usuario_CargoCod = new bool[] {false} ;
         BC000154_A1075Usuario_CargoUOCod = new int[1] ;
         BC000154_n1075Usuario_CargoUOCod = new bool[] {false} ;
         BC000154_A1716Usuario_Foto = new String[] {""} ;
         BC000154_n1716Usuario_Foto = new bool[] {false} ;
         i1235Usuario_Notificar = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.usuario_bc__default(),
            new Object[][] {
                new Object[] {
               BC00012_A1Usuario_Codigo, BC00012_A341Usuario_UserGamGuid, BC00012_A2Usuario_Nome, BC00012_n2Usuario_Nome, BC00012_A289Usuario_EhContador, BC00012_A291Usuario_EhContratada, BC00012_A292Usuario_EhContratante, BC00012_A293Usuario_EhFinanceiro, BC00012_A538Usuario_EhGestor, BC00012_A1093Usuario_EhPreposto,
               BC00012_A1017Usuario_CrtfPath, BC00012_n1017Usuario_CrtfPath, BC00012_A1235Usuario_Notificar, BC00012_n1235Usuario_Notificar, BC00012_A54Usuario_Ativo, BC00012_A1647Usuario_Email, BC00012_n1647Usuario_Email, BC00012_A40000Usuario_Foto_GXI, BC00012_n40000Usuario_Foto_GXI, BC00012_A1908Usuario_DeFerias,
               BC00012_n1908Usuario_DeFerias, BC00012_A2016Usuario_UltimaArea, BC00012_n2016Usuario_UltimaArea, BC00012_A57Usuario_PessoaCod, BC00012_A1073Usuario_CargoCod, BC00012_n1073Usuario_CargoCod, BC00012_A1716Usuario_Foto, BC00012_n1716Usuario_Foto
               }
               , new Object[] {
               BC00013_A1Usuario_Codigo, BC00013_A341Usuario_UserGamGuid, BC00013_A2Usuario_Nome, BC00013_n2Usuario_Nome, BC00013_A289Usuario_EhContador, BC00013_A291Usuario_EhContratada, BC00013_A292Usuario_EhContratante, BC00013_A293Usuario_EhFinanceiro, BC00013_A538Usuario_EhGestor, BC00013_A1093Usuario_EhPreposto,
               BC00013_A1017Usuario_CrtfPath, BC00013_n1017Usuario_CrtfPath, BC00013_A1235Usuario_Notificar, BC00013_n1235Usuario_Notificar, BC00013_A54Usuario_Ativo, BC00013_A1647Usuario_Email, BC00013_n1647Usuario_Email, BC00013_A40000Usuario_Foto_GXI, BC00013_n40000Usuario_Foto_GXI, BC00013_A1908Usuario_DeFerias,
               BC00013_n1908Usuario_DeFerias, BC00013_A2016Usuario_UltimaArea, BC00013_n2016Usuario_UltimaArea, BC00013_A57Usuario_PessoaCod, BC00013_A1073Usuario_CargoCod, BC00013_n1073Usuario_CargoCod, BC00013_A1716Usuario_Foto, BC00013_n1716Usuario_Foto
               }
               , new Object[] {
               BC00014_A58Usuario_PessoaNom, BC00014_n58Usuario_PessoaNom, BC00014_A59Usuario_PessoaTip, BC00014_n59Usuario_PessoaTip, BC00014_A325Usuario_PessoaDoc, BC00014_n325Usuario_PessoaDoc, BC00014_A2095Usuario_PessoaTelefone, BC00014_n2095Usuario_PessoaTelefone
               }
               , new Object[] {
               BC00015_A1074Usuario_CargoNom, BC00015_n1074Usuario_CargoNom, BC00015_A1075Usuario_CargoUOCod, BC00015_n1075Usuario_CargoUOCod
               }
               , new Object[] {
               BC00016_A1076Usuario_CargoUONom, BC00016_n1076Usuario_CargoUONom
               }
               , new Object[] {
               BC00017_A1Usuario_Codigo, BC00017_A1074Usuario_CargoNom, BC00017_n1074Usuario_CargoNom, BC00017_A1076Usuario_CargoUONom, BC00017_n1076Usuario_CargoUONom, BC00017_A58Usuario_PessoaNom, BC00017_n58Usuario_PessoaNom, BC00017_A59Usuario_PessoaTip, BC00017_n59Usuario_PessoaTip, BC00017_A325Usuario_PessoaDoc,
               BC00017_n325Usuario_PessoaDoc, BC00017_A2095Usuario_PessoaTelefone, BC00017_n2095Usuario_PessoaTelefone, BC00017_A341Usuario_UserGamGuid, BC00017_A2Usuario_Nome, BC00017_n2Usuario_Nome, BC00017_A289Usuario_EhContador, BC00017_A291Usuario_EhContratada, BC00017_A292Usuario_EhContratante, BC00017_A293Usuario_EhFinanceiro,
               BC00017_A538Usuario_EhGestor, BC00017_A1093Usuario_EhPreposto, BC00017_A1017Usuario_CrtfPath, BC00017_n1017Usuario_CrtfPath, BC00017_A1235Usuario_Notificar, BC00017_n1235Usuario_Notificar, BC00017_A54Usuario_Ativo, BC00017_A1647Usuario_Email, BC00017_n1647Usuario_Email, BC00017_A40000Usuario_Foto_GXI,
               BC00017_n40000Usuario_Foto_GXI, BC00017_A1908Usuario_DeFerias, BC00017_n1908Usuario_DeFerias, BC00017_A2016Usuario_UltimaArea, BC00017_n2016Usuario_UltimaArea, BC00017_A57Usuario_PessoaCod, BC00017_A1073Usuario_CargoCod, BC00017_n1073Usuario_CargoCod, BC00017_A1075Usuario_CargoUOCod, BC00017_n1075Usuario_CargoUOCod,
               BC00017_A1716Usuario_Foto, BC00017_n1716Usuario_Foto
               }
               , new Object[] {
               BC00018_A1Usuario_Codigo
               }
               , new Object[] {
               BC00019_A1Usuario_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC000113_A1074Usuario_CargoNom, BC000113_n1074Usuario_CargoNom, BC000113_A1075Usuario_CargoUOCod, BC000113_n1075Usuario_CargoUOCod
               }
               , new Object[] {
               BC000114_A1076Usuario_CargoUONom, BC000114_n1076Usuario_CargoUONom
               }
               , new Object[] {
               BC000115_A58Usuario_PessoaNom, BC000115_n58Usuario_PessoaNom, BC000115_A59Usuario_PessoaTip, BC000115_n59Usuario_PessoaTip, BC000115_A325Usuario_PessoaDoc, BC000115_n325Usuario_PessoaDoc, BC000115_A2095Usuario_PessoaTelefone, BC000115_n2095Usuario_PessoaTelefone
               }
               , new Object[] {
               BC000116_A2077UsuarioNotifica_Codigo
               }
               , new Object[] {
               BC000117_A1984ContagemResultadoQA_Codigo
               }
               , new Object[] {
               BC000118_A1984ContagemResultadoQA_Codigo
               }
               , new Object[] {
               BC000119_A1824ContratoAuxiliar_ContratoCod, BC000119_A1825ContratoAuxiliar_UsuarioCod
               }
               , new Object[] {
               BC000120_A1562HistoricoConsumo_Codigo
               }
               , new Object[] {
               BC000121_A1482Gestao_Codigo
               }
               , new Object[] {
               BC000122_A1482Gestao_Codigo
               }
               , new Object[] {
               BC000123_A1473ContratoServicosCusto_Codigo
               }
               , new Object[] {
               BC000124_A1412ContagemResultadoNotificacao_Codigo, BC000124_A1414ContagemResultadoNotificacao_DestCod
               }
               , new Object[] {
               BC000125_A1412ContagemResultadoNotificacao_Codigo
               }
               , new Object[] {
               BC000126_A127Sistema_Codigo
               }
               , new Object[] {
               BC000127_A1033ContagemResultadoLiqLog_Codigo, BC000127_A1370ContagemResultadoLiqLogOS_Codigo
               }
               , new Object[] {
               BC000128_A1331ContagemResultadoNota_Codigo
               }
               , new Object[] {
               BC000129_A1078ContratoGestor_ContratoCod, BC000129_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               BC000130_A1041ContagemResultadoImpLog_Codigo
               }
               , new Object[] {
               BC000131_A74Contrato_Codigo
               }
               , new Object[] {
               BC000132_A996SolicitacaoMudanca_Codigo
               }
               , new Object[] {
               BC000133_A1797LogResponsavel_Codigo
               }
               , new Object[] {
               BC000134_A1797LogResponsavel_Codigo
               }
               , new Object[] {
               BC000135_A828UsuarioServicos_UsuarioCod, BC000135_A829UsuarioServicos_ServicoCod
               }
               , new Object[] {
               BC000136_A820ContagemResultadoChckLstLog_Codigo
               }
               , new Object[] {
               BC000137_A722Baseline_Codigo
               }
               , new Object[] {
               BC000138_A456ContagemResultado_Codigo, BC000138_A579ContagemResultadoErro_Tipo
               }
               , new Object[] {
               BC000139_A596Lote_Codigo
               }
               , new Object[] {
               BC000140_A456ContagemResultado_Codigo, BC000140_A473ContagemResultado_DataCnt, BC000140_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               BC000141_A456ContagemResultado_Codigo
               }
               , new Object[] {
               BC000142_A456ContagemResultado_Codigo
               }
               , new Object[] {
               BC000143_A439Solicitacoes_Codigo
               }
               , new Object[] {
               BC000144_A439Solicitacoes_Codigo
               }
               , new Object[] {
               BC000145_A435File_UsuarioCod, BC000145_A504File_Row
               }
               , new Object[] {
               BC000146_A243ContagemItemParecer_Codigo
               }
               , new Object[] {
               BC000147_A192Contagem_Codigo
               }
               , new Object[] {
               BC000148_A66ContratadaUsuario_ContratadaCod, BC000148_A69ContratadaUsuario_UsuarioCod
               }
               , new Object[] {
               BC000149_A63ContratanteUsuario_ContratanteCod, BC000149_A60ContratanteUsuario_UsuarioCod
               }
               , new Object[] {
               BC000150_A1430AuditId
               }
               , new Object[] {
               BC000151_A2175Equipe_Codigo, BC000151_A1Usuario_Codigo
               }
               , new Object[] {
               BC000152_A1Usuario_Codigo, BC000152_A487Selected_Codigo, BC000152_A488Selected_Flag
               }
               , new Object[] {
               BC000153_A1Usuario_Codigo, BC000153_A3Perfil_Codigo
               }
               , new Object[] {
               BC000154_A1Usuario_Codigo, BC000154_A1074Usuario_CargoNom, BC000154_n1074Usuario_CargoNom, BC000154_A1076Usuario_CargoUONom, BC000154_n1076Usuario_CargoUONom, BC000154_A58Usuario_PessoaNom, BC000154_n58Usuario_PessoaNom, BC000154_A59Usuario_PessoaTip, BC000154_n59Usuario_PessoaTip, BC000154_A325Usuario_PessoaDoc,
               BC000154_n325Usuario_PessoaDoc, BC000154_A2095Usuario_PessoaTelefone, BC000154_n2095Usuario_PessoaTelefone, BC000154_A341Usuario_UserGamGuid, BC000154_A2Usuario_Nome, BC000154_n2Usuario_Nome, BC000154_A289Usuario_EhContador, BC000154_A291Usuario_EhContratada, BC000154_A292Usuario_EhContratante, BC000154_A293Usuario_EhFinanceiro,
               BC000154_A538Usuario_EhGestor, BC000154_A1093Usuario_EhPreposto, BC000154_A1017Usuario_CrtfPath, BC000154_n1017Usuario_CrtfPath, BC000154_A1235Usuario_Notificar, BC000154_n1235Usuario_Notificar, BC000154_A54Usuario_Ativo, BC000154_A1647Usuario_Email, BC000154_n1647Usuario_Email, BC000154_A40000Usuario_Foto_GXI,
               BC000154_n40000Usuario_Foto_GXI, BC000154_A1908Usuario_DeFerias, BC000154_n1908Usuario_DeFerias, BC000154_A2016Usuario_UltimaArea, BC000154_n2016Usuario_UltimaArea, BC000154_A57Usuario_PessoaCod, BC000154_A1073Usuario_CargoCod, BC000154_n1073Usuario_CargoCod, BC000154_A1075Usuario_CargoUOCod, BC000154_n1075Usuario_CargoUOCod,
               BC000154_A1716Usuario_Foto, BC000154_n1716Usuario_Foto
               }
            }
         );
         Z1908Usuario_DeFerias = false;
         n1908Usuario_DeFerias = false;
         A1908Usuario_DeFerias = false;
         n1908Usuario_DeFerias = false;
         i1908Usuario_DeFerias = false;
         n1908Usuario_DeFerias = false;
         Z54Usuario_Ativo = true;
         A54Usuario_Ativo = true;
         i54Usuario_Ativo = true;
         Z1235Usuario_Notificar = "A";
         n1235Usuario_Notificar = false;
         A1235Usuario_Notificar = "A";
         n1235Usuario_Notificar = false;
         i1235Usuario_Notificar = "A";
         n1235Usuario_Notificar = false;
         Z1093Usuario_EhPreposto = false;
         A1093Usuario_EhPreposto = false;
         i1093Usuario_EhPreposto = false;
         Z538Usuario_EhGestor = false;
         A538Usuario_EhGestor = false;
         i538Usuario_EhGestor = false;
         AV32Pgmname = "Usuario_BC";
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E12012 */
         E12012 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short RcdFound1 ;
      private int trnEnded ;
      private int Z1Usuario_Codigo ;
      private int A1Usuario_Codigo ;
      private int AV33GXV1 ;
      private int AV25Insert_Usuario_CargoCod ;
      private int AV14Insert_Usuario_PessoaCod ;
      private int AV26UnidadeOrganizacional_Codigo ;
      private int Z2016Usuario_UltimaArea ;
      private int A2016Usuario_UltimaArea ;
      private int Z57Usuario_PessoaCod ;
      private int A57Usuario_PessoaCod ;
      private int Z1073Usuario_CargoCod ;
      private int A1073Usuario_CargoCod ;
      private int Z66ContratadaUsuario_ContratadaCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int Z69ContratadaUsuario_UsuarioCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int Z34Pessoa_Codigo ;
      private int A34Pessoa_Codigo ;
      private int Z1075Usuario_CargoUOCod ;
      private int A1075Usuario_CargoUOCod ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String AV32Pgmname ;
      private String Z341Usuario_UserGamGuid ;
      private String A341Usuario_UserGamGuid ;
      private String Z2Usuario_Nome ;
      private String A2Usuario_Nome ;
      private String Z1235Usuario_Notificar ;
      private String A1235Usuario_Notificar ;
      private String Z1083Usuario_Entidade ;
      private String A1083Usuario_Entidade ;
      private String Z58Usuario_PessoaNom ;
      private String A58Usuario_PessoaNom ;
      private String Z59Usuario_PessoaTip ;
      private String A59Usuario_PessoaTip ;
      private String Z2095Usuario_PessoaTelefone ;
      private String A2095Usuario_PessoaTelefone ;
      private String Z1076Usuario_CargoUONom ;
      private String A1076Usuario_CargoUONom ;
      private String sMode1 ;
      private String GXt_char1 ;
      private String i1235Usuario_Notificar ;
      private bool returnInSub ;
      private bool Z289Usuario_EhContador ;
      private bool A289Usuario_EhContador ;
      private bool Z291Usuario_EhContratada ;
      private bool A291Usuario_EhContratada ;
      private bool Z292Usuario_EhContratante ;
      private bool A292Usuario_EhContratante ;
      private bool Z293Usuario_EhFinanceiro ;
      private bool A293Usuario_EhFinanceiro ;
      private bool Z538Usuario_EhGestor ;
      private bool A538Usuario_EhGestor ;
      private bool Z1093Usuario_EhPreposto ;
      private bool A1093Usuario_EhPreposto ;
      private bool Z54Usuario_Ativo ;
      private bool A54Usuario_Ativo ;
      private bool Z1908Usuario_DeFerias ;
      private bool A1908Usuario_DeFerias ;
      private bool Z290Usuario_EhAuditorFM ;
      private bool A290Usuario_EhAuditorFM ;
      private bool n1908Usuario_DeFerias ;
      private bool n1235Usuario_Notificar ;
      private bool n1Usuario_Codigo ;
      private bool n1074Usuario_CargoNom ;
      private bool n1076Usuario_CargoUONom ;
      private bool n58Usuario_PessoaNom ;
      private bool n59Usuario_PessoaTip ;
      private bool n325Usuario_PessoaDoc ;
      private bool n2095Usuario_PessoaTelefone ;
      private bool n2Usuario_Nome ;
      private bool n1017Usuario_CrtfPath ;
      private bool n1647Usuario_Email ;
      private bool n40000Usuario_Foto_GXI ;
      private bool n2016Usuario_UltimaArea ;
      private bool n1073Usuario_CargoCod ;
      private bool n1075Usuario_CargoUOCod ;
      private bool n1716Usuario_Foto ;
      private bool Gx_longc ;
      private bool GXt_boolean2 ;
      private bool i1908Usuario_DeFerias ;
      private bool i54Usuario_Ativo ;
      private bool i1093Usuario_EhPreposto ;
      private bool i538Usuario_EhGestor ;
      private String Z1017Usuario_CrtfPath ;
      private String A1017Usuario_CrtfPath ;
      private String Z1647Usuario_Email ;
      private String A1647Usuario_Email ;
      private String Z325Usuario_PessoaDoc ;
      private String A325Usuario_PessoaDoc ;
      private String Z1074Usuario_CargoNom ;
      private String A1074Usuario_CargoNom ;
      private String Z40000Usuario_Foto_GXI ;
      private String A40000Usuario_Foto_GXI ;
      private String Z1716Usuario_Foto ;
      private String A1716Usuario_Foto ;
      private IGxSession AV10WebSession ;
      private SdtUsuario bcUsuario ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC00017_A1Usuario_Codigo ;
      private bool[] BC00017_n1Usuario_Codigo ;
      private String[] BC00017_A1074Usuario_CargoNom ;
      private bool[] BC00017_n1074Usuario_CargoNom ;
      private String[] BC00017_A1076Usuario_CargoUONom ;
      private bool[] BC00017_n1076Usuario_CargoUONom ;
      private String[] BC00017_A58Usuario_PessoaNom ;
      private bool[] BC00017_n58Usuario_PessoaNom ;
      private String[] BC00017_A59Usuario_PessoaTip ;
      private bool[] BC00017_n59Usuario_PessoaTip ;
      private String[] BC00017_A325Usuario_PessoaDoc ;
      private bool[] BC00017_n325Usuario_PessoaDoc ;
      private String[] BC00017_A2095Usuario_PessoaTelefone ;
      private bool[] BC00017_n2095Usuario_PessoaTelefone ;
      private String[] BC00017_A341Usuario_UserGamGuid ;
      private String[] BC00017_A2Usuario_Nome ;
      private bool[] BC00017_n2Usuario_Nome ;
      private bool[] BC00017_A289Usuario_EhContador ;
      private bool[] BC00017_A291Usuario_EhContratada ;
      private bool[] BC00017_A292Usuario_EhContratante ;
      private bool[] BC00017_A293Usuario_EhFinanceiro ;
      private bool[] BC00017_A538Usuario_EhGestor ;
      private bool[] BC00017_A1093Usuario_EhPreposto ;
      private String[] BC00017_A1017Usuario_CrtfPath ;
      private bool[] BC00017_n1017Usuario_CrtfPath ;
      private String[] BC00017_A1235Usuario_Notificar ;
      private bool[] BC00017_n1235Usuario_Notificar ;
      private bool[] BC00017_A54Usuario_Ativo ;
      private String[] BC00017_A1647Usuario_Email ;
      private bool[] BC00017_n1647Usuario_Email ;
      private String[] BC00017_A40000Usuario_Foto_GXI ;
      private bool[] BC00017_n40000Usuario_Foto_GXI ;
      private bool[] BC00017_A1908Usuario_DeFerias ;
      private bool[] BC00017_n1908Usuario_DeFerias ;
      private int[] BC00017_A2016Usuario_UltimaArea ;
      private bool[] BC00017_n2016Usuario_UltimaArea ;
      private int[] BC00017_A57Usuario_PessoaCod ;
      private int[] BC00017_A1073Usuario_CargoCod ;
      private bool[] BC00017_n1073Usuario_CargoCod ;
      private int[] BC00017_A1075Usuario_CargoUOCod ;
      private bool[] BC00017_n1075Usuario_CargoUOCod ;
      private String[] BC00017_A1716Usuario_Foto ;
      private bool[] BC00017_n1716Usuario_Foto ;
      private String[] BC00015_A1074Usuario_CargoNom ;
      private bool[] BC00015_n1074Usuario_CargoNom ;
      private int[] BC00015_A1075Usuario_CargoUOCod ;
      private bool[] BC00015_n1075Usuario_CargoUOCod ;
      private String[] BC00016_A1076Usuario_CargoUONom ;
      private bool[] BC00016_n1076Usuario_CargoUONom ;
      private String[] BC00014_A58Usuario_PessoaNom ;
      private bool[] BC00014_n58Usuario_PessoaNom ;
      private String[] BC00014_A59Usuario_PessoaTip ;
      private bool[] BC00014_n59Usuario_PessoaTip ;
      private String[] BC00014_A325Usuario_PessoaDoc ;
      private bool[] BC00014_n325Usuario_PessoaDoc ;
      private String[] BC00014_A2095Usuario_PessoaTelefone ;
      private bool[] BC00014_n2095Usuario_PessoaTelefone ;
      private int[] BC00018_A1Usuario_Codigo ;
      private bool[] BC00018_n1Usuario_Codigo ;
      private int[] BC00013_A1Usuario_Codigo ;
      private bool[] BC00013_n1Usuario_Codigo ;
      private String[] BC00013_A341Usuario_UserGamGuid ;
      private String[] BC00013_A2Usuario_Nome ;
      private bool[] BC00013_n2Usuario_Nome ;
      private bool[] BC00013_A289Usuario_EhContador ;
      private bool[] BC00013_A291Usuario_EhContratada ;
      private bool[] BC00013_A292Usuario_EhContratante ;
      private bool[] BC00013_A293Usuario_EhFinanceiro ;
      private bool[] BC00013_A538Usuario_EhGestor ;
      private bool[] BC00013_A1093Usuario_EhPreposto ;
      private String[] BC00013_A1017Usuario_CrtfPath ;
      private bool[] BC00013_n1017Usuario_CrtfPath ;
      private String[] BC00013_A1235Usuario_Notificar ;
      private bool[] BC00013_n1235Usuario_Notificar ;
      private bool[] BC00013_A54Usuario_Ativo ;
      private String[] BC00013_A1647Usuario_Email ;
      private bool[] BC00013_n1647Usuario_Email ;
      private String[] BC00013_A40000Usuario_Foto_GXI ;
      private bool[] BC00013_n40000Usuario_Foto_GXI ;
      private bool[] BC00013_A1908Usuario_DeFerias ;
      private bool[] BC00013_n1908Usuario_DeFerias ;
      private int[] BC00013_A2016Usuario_UltimaArea ;
      private bool[] BC00013_n2016Usuario_UltimaArea ;
      private int[] BC00013_A57Usuario_PessoaCod ;
      private int[] BC00013_A1073Usuario_CargoCod ;
      private bool[] BC00013_n1073Usuario_CargoCod ;
      private String[] BC00013_A1716Usuario_Foto ;
      private bool[] BC00013_n1716Usuario_Foto ;
      private int[] BC00012_A1Usuario_Codigo ;
      private bool[] BC00012_n1Usuario_Codigo ;
      private String[] BC00012_A341Usuario_UserGamGuid ;
      private String[] BC00012_A2Usuario_Nome ;
      private bool[] BC00012_n2Usuario_Nome ;
      private bool[] BC00012_A289Usuario_EhContador ;
      private bool[] BC00012_A291Usuario_EhContratada ;
      private bool[] BC00012_A292Usuario_EhContratante ;
      private bool[] BC00012_A293Usuario_EhFinanceiro ;
      private bool[] BC00012_A538Usuario_EhGestor ;
      private bool[] BC00012_A1093Usuario_EhPreposto ;
      private String[] BC00012_A1017Usuario_CrtfPath ;
      private bool[] BC00012_n1017Usuario_CrtfPath ;
      private String[] BC00012_A1235Usuario_Notificar ;
      private bool[] BC00012_n1235Usuario_Notificar ;
      private bool[] BC00012_A54Usuario_Ativo ;
      private String[] BC00012_A1647Usuario_Email ;
      private bool[] BC00012_n1647Usuario_Email ;
      private String[] BC00012_A40000Usuario_Foto_GXI ;
      private bool[] BC00012_n40000Usuario_Foto_GXI ;
      private bool[] BC00012_A1908Usuario_DeFerias ;
      private bool[] BC00012_n1908Usuario_DeFerias ;
      private int[] BC00012_A2016Usuario_UltimaArea ;
      private bool[] BC00012_n2016Usuario_UltimaArea ;
      private int[] BC00012_A57Usuario_PessoaCod ;
      private int[] BC00012_A1073Usuario_CargoCod ;
      private bool[] BC00012_n1073Usuario_CargoCod ;
      private String[] BC00012_A1716Usuario_Foto ;
      private bool[] BC00012_n1716Usuario_Foto ;
      private int[] BC00019_A1Usuario_Codigo ;
      private bool[] BC00019_n1Usuario_Codigo ;
      private String[] BC000113_A1074Usuario_CargoNom ;
      private bool[] BC000113_n1074Usuario_CargoNom ;
      private int[] BC000113_A1075Usuario_CargoUOCod ;
      private bool[] BC000113_n1075Usuario_CargoUOCod ;
      private String[] BC000114_A1076Usuario_CargoUONom ;
      private bool[] BC000114_n1076Usuario_CargoUONom ;
      private String[] BC000115_A58Usuario_PessoaNom ;
      private bool[] BC000115_n58Usuario_PessoaNom ;
      private String[] BC000115_A59Usuario_PessoaTip ;
      private bool[] BC000115_n59Usuario_PessoaTip ;
      private String[] BC000115_A325Usuario_PessoaDoc ;
      private bool[] BC000115_n325Usuario_PessoaDoc ;
      private String[] BC000115_A2095Usuario_PessoaTelefone ;
      private bool[] BC000115_n2095Usuario_PessoaTelefone ;
      private int[] BC000116_A2077UsuarioNotifica_Codigo ;
      private int[] BC000117_A1984ContagemResultadoQA_Codigo ;
      private int[] BC000118_A1984ContagemResultadoQA_Codigo ;
      private int[] BC000119_A1824ContratoAuxiliar_ContratoCod ;
      private int[] BC000119_A1825ContratoAuxiliar_UsuarioCod ;
      private int[] BC000120_A1562HistoricoConsumo_Codigo ;
      private int[] BC000121_A1482Gestao_Codigo ;
      private int[] BC000122_A1482Gestao_Codigo ;
      private int[] BC000123_A1473ContratoServicosCusto_Codigo ;
      private long[] BC000124_A1412ContagemResultadoNotificacao_Codigo ;
      private int[] BC000124_A1414ContagemResultadoNotificacao_DestCod ;
      private long[] BC000125_A1412ContagemResultadoNotificacao_Codigo ;
      private int[] BC000126_A127Sistema_Codigo ;
      private int[] BC000127_A1033ContagemResultadoLiqLog_Codigo ;
      private int[] BC000127_A1370ContagemResultadoLiqLogOS_Codigo ;
      private int[] BC000128_A1331ContagemResultadoNota_Codigo ;
      private int[] BC000129_A1078ContratoGestor_ContratoCod ;
      private int[] BC000129_A1079ContratoGestor_UsuarioCod ;
      private int[] BC000130_A1041ContagemResultadoImpLog_Codigo ;
      private int[] BC000131_A74Contrato_Codigo ;
      private int[] BC000132_A996SolicitacaoMudanca_Codigo ;
      private long[] BC000133_A1797LogResponsavel_Codigo ;
      private long[] BC000134_A1797LogResponsavel_Codigo ;
      private int[] BC000135_A828UsuarioServicos_UsuarioCod ;
      private int[] BC000135_A829UsuarioServicos_ServicoCod ;
      private int[] BC000136_A820ContagemResultadoChckLstLog_Codigo ;
      private int[] BC000137_A722Baseline_Codigo ;
      private int[] BC000138_A456ContagemResultado_Codigo ;
      private String[] BC000138_A579ContagemResultadoErro_Tipo ;
      private int[] BC000139_A596Lote_Codigo ;
      private int[] BC000140_A456ContagemResultado_Codigo ;
      private DateTime[] BC000140_A473ContagemResultado_DataCnt ;
      private String[] BC000140_A511ContagemResultado_HoraCnt ;
      private int[] BC000141_A456ContagemResultado_Codigo ;
      private int[] BC000142_A456ContagemResultado_Codigo ;
      private int[] BC000143_A439Solicitacoes_Codigo ;
      private int[] BC000144_A439Solicitacoes_Codigo ;
      private int[] BC000145_A435File_UsuarioCod ;
      private int[] BC000145_A504File_Row ;
      private int[] BC000146_A243ContagemItemParecer_Codigo ;
      private int[] BC000147_A192Contagem_Codigo ;
      private int[] BC000148_A66ContratadaUsuario_ContratadaCod ;
      private int[] BC000148_A69ContratadaUsuario_UsuarioCod ;
      private int[] BC000149_A63ContratanteUsuario_ContratanteCod ;
      private int[] BC000149_A60ContratanteUsuario_UsuarioCod ;
      private long[] BC000150_A1430AuditId ;
      private int[] BC000151_A2175Equipe_Codigo ;
      private int[] BC000151_A1Usuario_Codigo ;
      private bool[] BC000151_n1Usuario_Codigo ;
      private int[] BC000152_A1Usuario_Codigo ;
      private bool[] BC000152_n1Usuario_Codigo ;
      private int[] BC000152_A487Selected_Codigo ;
      private bool[] BC000152_A488Selected_Flag ;
      private int[] BC000153_A1Usuario_Codigo ;
      private bool[] BC000153_n1Usuario_Codigo ;
      private int[] BC000153_A3Perfil_Codigo ;
      private int[] BC000154_A1Usuario_Codigo ;
      private bool[] BC000154_n1Usuario_Codigo ;
      private String[] BC000154_A1074Usuario_CargoNom ;
      private bool[] BC000154_n1074Usuario_CargoNom ;
      private String[] BC000154_A1076Usuario_CargoUONom ;
      private bool[] BC000154_n1076Usuario_CargoUONom ;
      private String[] BC000154_A58Usuario_PessoaNom ;
      private bool[] BC000154_n58Usuario_PessoaNom ;
      private String[] BC000154_A59Usuario_PessoaTip ;
      private bool[] BC000154_n59Usuario_PessoaTip ;
      private String[] BC000154_A325Usuario_PessoaDoc ;
      private bool[] BC000154_n325Usuario_PessoaDoc ;
      private String[] BC000154_A2095Usuario_PessoaTelefone ;
      private bool[] BC000154_n2095Usuario_PessoaTelefone ;
      private String[] BC000154_A341Usuario_UserGamGuid ;
      private String[] BC000154_A2Usuario_Nome ;
      private bool[] BC000154_n2Usuario_Nome ;
      private bool[] BC000154_A289Usuario_EhContador ;
      private bool[] BC000154_A291Usuario_EhContratada ;
      private bool[] BC000154_A292Usuario_EhContratante ;
      private bool[] BC000154_A293Usuario_EhFinanceiro ;
      private bool[] BC000154_A538Usuario_EhGestor ;
      private bool[] BC000154_A1093Usuario_EhPreposto ;
      private String[] BC000154_A1017Usuario_CrtfPath ;
      private bool[] BC000154_n1017Usuario_CrtfPath ;
      private String[] BC000154_A1235Usuario_Notificar ;
      private bool[] BC000154_n1235Usuario_Notificar ;
      private bool[] BC000154_A54Usuario_Ativo ;
      private String[] BC000154_A1647Usuario_Email ;
      private bool[] BC000154_n1647Usuario_Email ;
      private String[] BC000154_A40000Usuario_Foto_GXI ;
      private bool[] BC000154_n40000Usuario_Foto_GXI ;
      private bool[] BC000154_A1908Usuario_DeFerias ;
      private bool[] BC000154_n1908Usuario_DeFerias ;
      private int[] BC000154_A2016Usuario_UltimaArea ;
      private bool[] BC000154_n2016Usuario_UltimaArea ;
      private int[] BC000154_A57Usuario_PessoaCod ;
      private int[] BC000154_A1073Usuario_CargoCod ;
      private bool[] BC000154_n1073Usuario_CargoCod ;
      private int[] BC000154_A1075Usuario_CargoUOCod ;
      private bool[] BC000154_n1075Usuario_CargoUOCod ;
      private String[] BC000154_A1716Usuario_Foto ;
      private bool[] BC000154_n1716Usuario_Foto ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtAuditingObject AV28AuditingObject ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class usuario_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
         ,new ForEachCursor(def[32])
         ,new ForEachCursor(def[33])
         ,new ForEachCursor(def[34])
         ,new ForEachCursor(def[35])
         ,new ForEachCursor(def[36])
         ,new ForEachCursor(def[37])
         ,new ForEachCursor(def[38])
         ,new ForEachCursor(def[39])
         ,new ForEachCursor(def[40])
         ,new ForEachCursor(def[41])
         ,new ForEachCursor(def[42])
         ,new ForEachCursor(def[43])
         ,new ForEachCursor(def[44])
         ,new ForEachCursor(def[45])
         ,new ForEachCursor(def[46])
         ,new ForEachCursor(def[47])
         ,new ForEachCursor(def[48])
         ,new ForEachCursor(def[49])
         ,new ForEachCursor(def[50])
         ,new ForEachCursor(def[51])
         ,new ForEachCursor(def[52])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC00017 ;
          prmBC00017 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00015 ;
          prmBC00015 = new Object[] {
          new Object[] {"@Usuario_CargoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00016 ;
          prmBC00016 = new Object[] {
          new Object[] {"@Usuario_CargoUOCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00014 ;
          prmBC00014 = new Object[] {
          new Object[] {"@Usuario_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00018 ;
          prmBC00018 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00013 ;
          prmBC00013 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00012 ;
          prmBC00012 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00019 ;
          prmBC00019 = new Object[] {
          new Object[] {"@Usuario_UserGamGuid",SqlDbType.Char,40,0} ,
          new Object[] {"@Usuario_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Usuario_EhContador",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_EhContratada",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_EhContratante",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_EhFinanceiro",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_EhGestor",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_EhPreposto",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_CrtfPath",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Usuario_Notificar",SqlDbType.Char,1,0} ,
          new Object[] {"@Usuario_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_Email",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Usuario_Foto",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Usuario_Foto_GXI",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@Usuario_DeFerias",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_UltimaArea",SqlDbType.Int,6,0} ,
          new Object[] {"@Usuario_PessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Usuario_CargoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000110 ;
          prmBC000110 = new Object[] {
          new Object[] {"@Usuario_UserGamGuid",SqlDbType.Char,40,0} ,
          new Object[] {"@Usuario_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Usuario_EhContador",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_EhContratada",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_EhContratante",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_EhFinanceiro",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_EhGestor",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_EhPreposto",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_CrtfPath",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Usuario_Notificar",SqlDbType.Char,1,0} ,
          new Object[] {"@Usuario_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_Email",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Usuario_DeFerias",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_UltimaArea",SqlDbType.Int,6,0} ,
          new Object[] {"@Usuario_PessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Usuario_CargoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000111 ;
          prmBC000111 = new Object[] {
          new Object[] {"@Usuario_Foto",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Usuario_Foto_GXI",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000112 ;
          prmBC000112 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000113 ;
          prmBC000113 = new Object[] {
          new Object[] {"@Usuario_CargoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000114 ;
          prmBC000114 = new Object[] {
          new Object[] {"@Usuario_CargoUOCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000115 ;
          prmBC000115 = new Object[] {
          new Object[] {"@Usuario_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000116 ;
          prmBC000116 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000117 ;
          prmBC000117 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000118 ;
          prmBC000118 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000119 ;
          prmBC000119 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000120 ;
          prmBC000120 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000121 ;
          prmBC000121 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000122 ;
          prmBC000122 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000123 ;
          prmBC000123 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000124 ;
          prmBC000124 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000125 ;
          prmBC000125 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000126 ;
          prmBC000126 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000127 ;
          prmBC000127 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000128 ;
          prmBC000128 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000129 ;
          prmBC000129 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000130 ;
          prmBC000130 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000131 ;
          prmBC000131 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000132 ;
          prmBC000132 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000133 ;
          prmBC000133 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000134 ;
          prmBC000134 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000135 ;
          prmBC000135 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000136 ;
          prmBC000136 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000137 ;
          prmBC000137 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000138 ;
          prmBC000138 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000139 ;
          prmBC000139 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000140 ;
          prmBC000140 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000141 ;
          prmBC000141 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000142 ;
          prmBC000142 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000143 ;
          prmBC000143 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000144 ;
          prmBC000144 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000145 ;
          prmBC000145 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000146 ;
          prmBC000146 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000147 ;
          prmBC000147 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000148 ;
          prmBC000148 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000149 ;
          prmBC000149 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000150 ;
          prmBC000150 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000151 ;
          prmBC000151 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000152 ;
          prmBC000152 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000153 ;
          prmBC000153 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000154 ;
          prmBC000154 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC00012", "SELECT [Usuario_Codigo], [Usuario_UserGamGuid], [Usuario_Nome], [Usuario_EhContador], [Usuario_EhContratada], [Usuario_EhContratante], [Usuario_EhFinanceiro], [Usuario_EhGestor], [Usuario_EhPreposto], [Usuario_CrtfPath], [Usuario_Notificar], [Usuario_Ativo], [Usuario_Email], [Usuario_Foto_GXI], [Usuario_DeFerias], [Usuario_UltimaArea], [Usuario_PessoaCod] AS Usuario_PessoaCod, [Usuario_CargoCod] AS Usuario_CargoCod, [Usuario_Foto] FROM [Usuario] WITH (UPDLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00012,1,0,true,false )
             ,new CursorDef("BC00013", "SELECT [Usuario_Codigo], [Usuario_UserGamGuid], [Usuario_Nome], [Usuario_EhContador], [Usuario_EhContratada], [Usuario_EhContratante], [Usuario_EhFinanceiro], [Usuario_EhGestor], [Usuario_EhPreposto], [Usuario_CrtfPath], [Usuario_Notificar], [Usuario_Ativo], [Usuario_Email], [Usuario_Foto_GXI], [Usuario_DeFerias], [Usuario_UltimaArea], [Usuario_PessoaCod] AS Usuario_PessoaCod, [Usuario_CargoCod] AS Usuario_CargoCod, [Usuario_Foto] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00013,1,0,true,false )
             ,new CursorDef("BC00014", "SELECT [Pessoa_Nome] AS Usuario_PessoaNom, [Pessoa_TipoPessoa] AS Usuario_PessoaTip, [Pessoa_Docto] AS Usuario_PessoaDoc, [Pessoa_Telefone] AS Usuario_PessoaTelefone FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Usuario_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00014,1,0,true,false )
             ,new CursorDef("BC00015", "SELECT [Cargo_Nome] AS Usuario_CargoNom, [Cargo_UOCod] AS Usuario_CargoUOCod FROM [Geral_Cargo] WITH (NOLOCK) WHERE [Cargo_Codigo] = @Usuario_CargoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00015,1,0,true,false )
             ,new CursorDef("BC00016", "SELECT [UnidadeOrganizacional_Nome] AS Usuario_CargoUONom FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Codigo] = @Usuario_CargoUOCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00016,1,0,true,false )
             ,new CursorDef("BC00017", "SELECT TM1.[Usuario_Codigo], T2.[Cargo_Nome] AS Usuario_CargoNom, T3.[UnidadeOrganizacional_Nome] AS Usuario_CargoUONom, T4.[Pessoa_Nome] AS Usuario_PessoaNom, T4.[Pessoa_TipoPessoa] AS Usuario_PessoaTip, T4.[Pessoa_Docto] AS Usuario_PessoaDoc, T4.[Pessoa_Telefone] AS Usuario_PessoaTelefone, TM1.[Usuario_UserGamGuid], TM1.[Usuario_Nome], TM1.[Usuario_EhContador], TM1.[Usuario_EhContratada], TM1.[Usuario_EhContratante], TM1.[Usuario_EhFinanceiro], TM1.[Usuario_EhGestor], TM1.[Usuario_EhPreposto], TM1.[Usuario_CrtfPath], TM1.[Usuario_Notificar], TM1.[Usuario_Ativo], TM1.[Usuario_Email], TM1.[Usuario_Foto_GXI], TM1.[Usuario_DeFerias], TM1.[Usuario_UltimaArea], TM1.[Usuario_PessoaCod] AS Usuario_PessoaCod, TM1.[Usuario_CargoCod] AS Usuario_CargoCod, T2.[Cargo_UOCod] AS Usuario_CargoUOCod, TM1.[Usuario_Foto] FROM ((([Usuario] TM1 WITH (NOLOCK) LEFT JOIN [Geral_Cargo] T2 WITH (NOLOCK) ON T2.[Cargo_Codigo] = TM1.[Usuario_CargoCod]) LEFT JOIN [Geral_UnidadeOrganizacional] T3 WITH (NOLOCK) ON T3.[UnidadeOrganizacional_Codigo] = T2.[Cargo_UOCod]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = TM1.[Usuario_PessoaCod]) WHERE TM1.[Usuario_Codigo] = @Usuario_Codigo ORDER BY TM1.[Usuario_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00017,100,0,true,false )
             ,new CursorDef("BC00018", "SELECT [Usuario_Codigo] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00018,1,0,true,false )
             ,new CursorDef("BC00019", "INSERT INTO [Usuario]([Usuario_UserGamGuid], [Usuario_Nome], [Usuario_EhContador], [Usuario_EhContratada], [Usuario_EhContratante], [Usuario_EhFinanceiro], [Usuario_EhGestor], [Usuario_EhPreposto], [Usuario_CrtfPath], [Usuario_Notificar], [Usuario_Ativo], [Usuario_Email], [Usuario_Foto], [Usuario_Foto_GXI], [Usuario_DeFerias], [Usuario_UltimaArea], [Usuario_PessoaCod], [Usuario_CargoCod]) VALUES(@Usuario_UserGamGuid, @Usuario_Nome, @Usuario_EhContador, @Usuario_EhContratada, @Usuario_EhContratante, @Usuario_EhFinanceiro, @Usuario_EhGestor, @Usuario_EhPreposto, @Usuario_CrtfPath, @Usuario_Notificar, @Usuario_Ativo, @Usuario_Email, @Usuario_Foto, @Usuario_Foto_GXI, @Usuario_DeFerias, @Usuario_UltimaArea, @Usuario_PessoaCod, @Usuario_CargoCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC00019)
             ,new CursorDef("BC000110", "UPDATE [Usuario] SET [Usuario_UserGamGuid]=@Usuario_UserGamGuid, [Usuario_Nome]=@Usuario_Nome, [Usuario_EhContador]=@Usuario_EhContador, [Usuario_EhContratada]=@Usuario_EhContratada, [Usuario_EhContratante]=@Usuario_EhContratante, [Usuario_EhFinanceiro]=@Usuario_EhFinanceiro, [Usuario_EhGestor]=@Usuario_EhGestor, [Usuario_EhPreposto]=@Usuario_EhPreposto, [Usuario_CrtfPath]=@Usuario_CrtfPath, [Usuario_Notificar]=@Usuario_Notificar, [Usuario_Ativo]=@Usuario_Ativo, [Usuario_Email]=@Usuario_Email, [Usuario_DeFerias]=@Usuario_DeFerias, [Usuario_UltimaArea]=@Usuario_UltimaArea, [Usuario_PessoaCod]=@Usuario_PessoaCod, [Usuario_CargoCod]=@Usuario_CargoCod  WHERE [Usuario_Codigo] = @Usuario_Codigo", GxErrorMask.GX_NOMASK,prmBC000110)
             ,new CursorDef("BC000111", "UPDATE [Usuario] SET [Usuario_Foto]=@Usuario_Foto, [Usuario_Foto_GXI]=@Usuario_Foto_GXI  WHERE [Usuario_Codigo] = @Usuario_Codigo", GxErrorMask.GX_NOMASK,prmBC000111)
             ,new CursorDef("BC000112", "DELETE FROM [Usuario]  WHERE [Usuario_Codigo] = @Usuario_Codigo", GxErrorMask.GX_NOMASK,prmBC000112)
             ,new CursorDef("BC000113", "SELECT [Cargo_Nome] AS Usuario_CargoNom, [Cargo_UOCod] AS Usuario_CargoUOCod FROM [Geral_Cargo] WITH (NOLOCK) WHERE [Cargo_Codigo] = @Usuario_CargoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000113,1,0,true,false )
             ,new CursorDef("BC000114", "SELECT [UnidadeOrganizacional_Nome] AS Usuario_CargoUONom FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Codigo] = @Usuario_CargoUOCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000114,1,0,true,false )
             ,new CursorDef("BC000115", "SELECT [Pessoa_Nome] AS Usuario_PessoaNom, [Pessoa_TipoPessoa] AS Usuario_PessoaTip, [Pessoa_Docto] AS Usuario_PessoaDoc, [Pessoa_Telefone] AS Usuario_PessoaTelefone FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Usuario_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000115,1,0,true,false )
             ,new CursorDef("BC000116", "SELECT TOP 1 [UsuarioNotifica_Codigo] FROM [UsuarioNotifica] WITH (NOLOCK) WHERE [UsuarioNotifica_UsuarioCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000116,1,0,true,true )
             ,new CursorDef("BC000117", "SELECT TOP 1 [ContagemResultadoQA_Codigo] FROM [ContagemResultadoQA] WITH (NOLOCK) WHERE [ContagemResultadoQA_ParaCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000117,1,0,true,true )
             ,new CursorDef("BC000118", "SELECT TOP 1 [ContagemResultadoQA_Codigo] FROM [ContagemResultadoQA] WITH (NOLOCK) WHERE [ContagemResultadoQA_UserCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000118,1,0,true,true )
             ,new CursorDef("BC000119", "SELECT TOP 1 [ContratoAuxiliar_ContratoCod], [ContratoAuxiliar_UsuarioCod] FROM [ContratoAuxiliar] WITH (NOLOCK) WHERE [ContratoAuxiliar_UsuarioCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000119,1,0,true,true )
             ,new CursorDef("BC000120", "SELECT TOP 1 [HistoricoConsumo_Codigo] FROM [HistoricoConsumo] WITH (NOLOCK) WHERE [HistoricoConsumo_UsuarioCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000120,1,0,true,true )
             ,new CursorDef("BC000121", "SELECT TOP 1 [Gestao_Codigo] FROM [Gestao] WITH (NOLOCK) WHERE [Gestao_GestorCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000121,1,0,true,true )
             ,new CursorDef("BC000122", "SELECT TOP 1 [Gestao_Codigo] FROM [Gestao] WITH (NOLOCK) WHERE [Gestao_UsuarioCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000122,1,0,true,true )
             ,new CursorDef("BC000123", "SELECT TOP 1 [ContratoServicosCusto_Codigo] FROM [ContratoServicosCusto] WITH (NOLOCK) WHERE [ContratoServicosCusto_UsuarioCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000123,1,0,true,true )
             ,new CursorDef("BC000124", "SELECT TOP 1 [ContagemResultadoNotificacao_Codigo], [ContagemResultadoNotificacao_DestCod] FROM [ContagemResultadoNotificacaoDestinatario] WITH (NOLOCK) WHERE [ContagemResultadoNotificacao_DestCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000124,1,0,true,true )
             ,new CursorDef("BC000125", "SELECT TOP 1 [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacao] WITH (NOLOCK) WHERE [ContagemResultadoNotificacao_UsuCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000125,1,0,true,true )
             ,new CursorDef("BC000126", "SELECT TOP 1 [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_ImpUserCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000126,1,0,true,true )
             ,new CursorDef("BC000127", "SELECT TOP 1 [ContagemResultadoLiqLog_Codigo], [ContagemResultadoLiqLogOS_Codigo] FROM [ContagemResultadoLiqLogOS] WITH (NOLOCK) WHERE [ContagemResultadoLiqLogOS_UserCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000127,1,0,true,true )
             ,new CursorDef("BC000128", "SELECT TOP 1 [ContagemResultadoNota_Codigo] FROM [ContagemResultadoNotas] WITH (NOLOCK) WHERE [ContagemResultadoNota_UsuarioCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000128,1,0,true,true )
             ,new CursorDef("BC000129", "SELECT TOP 1 [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_UsuarioCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000129,1,0,true,true )
             ,new CursorDef("BC000130", "SELECT TOP 1 [ContagemResultadoImpLog_Codigo] FROM [ContagemResultadoImpLog] WITH (NOLOCK) WHERE [ContagemResultadoImpLog_UserCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000130,1,0,true,true )
             ,new CursorDef("BC000131", "SELECT TOP 1 [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_PrepostoCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000131,1,0,true,true )
             ,new CursorDef("BC000132", "SELECT TOP 1 [SolicitacaoMudanca_Codigo] FROM [SolicitacaoMudanca] WITH (NOLOCK) WHERE [SolicitacaoMudanca_Solicitante] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000132,1,0,true,true )
             ,new CursorDef("BC000133", "SELECT TOP 1 [LogResponsavel_Codigo] FROM [LogResponsavel] WITH (NOLOCK) WHERE [LogResponsavel_Owner] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000133,1,0,true,true )
             ,new CursorDef("BC000134", "SELECT TOP 1 [LogResponsavel_Codigo] FROM [LogResponsavel] WITH (NOLOCK) WHERE [LogResponsavel_UsuarioCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000134,1,0,true,true )
             ,new CursorDef("BC000135", "SELECT TOP 1 [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] FROM [UsuarioServicos] WITH (NOLOCK) WHERE [UsuarioServicos_UsuarioCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000135,1,0,true,true )
             ,new CursorDef("BC000136", "SELECT TOP 1 [ContagemResultadoChckLstLog_Codigo] FROM [ContagemResultadoChckLstLog] WITH (NOLOCK) WHERE [ContagemResultadoChckLstLog_UsuarioCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000136,1,0,true,true )
             ,new CursorDef("BC000137", "SELECT TOP 1 [Baseline_Codigo] FROM [Baseline] WITH (NOLOCK) WHERE [Baseline_UserCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000137,1,0,true,true )
             ,new CursorDef("BC000138", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultadoErro_Tipo] FROM [ContagemResultadoErro] WITH (NOLOCK) WHERE [ContagemResultadoErro_ContadorFMCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000138,1,0,true,true )
             ,new CursorDef("BC000139", "SELECT TOP 1 [Lote_Codigo] FROM [Lote] WITH (NOLOCK) WHERE [Lote_UserCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000139,1,0,true,true )
             ,new CursorDef("BC000140", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_ContadorFMCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000140,1,0,true,true )
             ,new CursorDef("BC000141", "SELECT TOP 1 [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Responsavel] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000141,1,0,true,true )
             ,new CursorDef("BC000142", "SELECT TOP 1 [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_ContadorFSCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000142,1,0,true,true )
             ,new CursorDef("BC000143", "SELECT TOP 1 [Solicitacoes_Codigo] FROM [Solicitacoes] WITH (NOLOCK) WHERE [Solicitacoes_Usuario_Ult] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000143,1,0,true,true )
             ,new CursorDef("BC000144", "SELECT TOP 1 [Solicitacoes_Codigo] FROM [Solicitacoes] WITH (NOLOCK) WHERE [Solicitacoes_Usuario] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000144,1,0,true,true )
             ,new CursorDef("BC000145", "SELECT TOP 1 [File_UsuarioCod], [File_Row] FROM [Tmp_File] WITH (NOLOCK) WHERE [File_UsuarioCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000145,1,0,true,true )
             ,new CursorDef("BC000146", "SELECT TOP 1 [ContagemItemParecer_Codigo] FROM [ContagemItemParecer] WITH (NOLOCK) WHERE [ContagemItemParecer_UsuarioCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000146,1,0,true,true )
             ,new CursorDef("BC000147", "SELECT TOP 1 [Contagem_Codigo] FROM [Contagem] WITH (NOLOCK) WHERE [Contagem_UsuarioContadorCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000147,1,0,true,true )
             ,new CursorDef("BC000148", "SELECT TOP 1 [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod] FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_UsuarioCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000148,1,0,true,true )
             ,new CursorDef("BC000149", "SELECT TOP 1 [ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod] FROM [ContratanteUsuario] WITH (NOLOCK) WHERE [ContratanteUsuario_UsuarioCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000149,1,0,true,true )
             ,new CursorDef("BC000150", "SELECT TOP 1 [AuditId] FROM [Audit] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000150,1,0,true,true )
             ,new CursorDef("BC000151", "SELECT TOP 1 [Equipe_Codigo], [Usuario_Codigo] FROM [EquipeUsuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000151,1,0,true,true )
             ,new CursorDef("BC000152", "SELECT TOP 1 [Usuario_Codigo], [Selected_Codigo], [Selected_Flag] FROM [Selected] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000152,1,0,true,true )
             ,new CursorDef("BC000153", "SELECT TOP 1 [Usuario_Codigo], [Perfil_Codigo] FROM [UsuarioPerfil] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000153,1,0,true,true )
             ,new CursorDef("BC000154", "SELECT TM1.[Usuario_Codigo], T2.[Cargo_Nome] AS Usuario_CargoNom, T3.[UnidadeOrganizacional_Nome] AS Usuario_CargoUONom, T4.[Pessoa_Nome] AS Usuario_PessoaNom, T4.[Pessoa_TipoPessoa] AS Usuario_PessoaTip, T4.[Pessoa_Docto] AS Usuario_PessoaDoc, T4.[Pessoa_Telefone] AS Usuario_PessoaTelefone, TM1.[Usuario_UserGamGuid], TM1.[Usuario_Nome], TM1.[Usuario_EhContador], TM1.[Usuario_EhContratada], TM1.[Usuario_EhContratante], TM1.[Usuario_EhFinanceiro], TM1.[Usuario_EhGestor], TM1.[Usuario_EhPreposto], TM1.[Usuario_CrtfPath], TM1.[Usuario_Notificar], TM1.[Usuario_Ativo], TM1.[Usuario_Email], TM1.[Usuario_Foto_GXI], TM1.[Usuario_DeFerias], TM1.[Usuario_UltimaArea], TM1.[Usuario_PessoaCod] AS Usuario_PessoaCod, TM1.[Usuario_CargoCod] AS Usuario_CargoCod, T2.[Cargo_UOCod] AS Usuario_CargoUOCod, TM1.[Usuario_Foto] FROM ((([Usuario] TM1 WITH (NOLOCK) LEFT JOIN [Geral_Cargo] T2 WITH (NOLOCK) ON T2.[Cargo_Codigo] = TM1.[Usuario_CargoCod]) LEFT JOIN [Geral_UnidadeOrganizacional] T3 WITH (NOLOCK) ON T3.[UnidadeOrganizacional_Codigo] = T2.[Cargo_UOCod]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = TM1.[Usuario_PessoaCod]) WHERE TM1.[Usuario_Codigo] = @Usuario_Codigo ORDER BY TM1.[Usuario_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000154,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 40) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((bool[]) buf[6])[0] = rslt.getBool(6) ;
                ((bool[]) buf[7])[0] = rslt.getBool(7) ;
                ((bool[]) buf[8])[0] = rslt.getBool(8) ;
                ((bool[]) buf[9])[0] = rslt.getBool(9) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(10);
                ((String[]) buf[12])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(11);
                ((bool[]) buf[14])[0] = rslt.getBool(12) ;
                ((String[]) buf[15])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(13);
                ((String[]) buf[17])[0] = rslt.getMultimediaUri(14) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(14);
                ((bool[]) buf[19])[0] = rslt.getBool(15) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(15);
                ((int[]) buf[21])[0] = rslt.getInt(16) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(16);
                ((int[]) buf[23])[0] = rslt.getInt(17) ;
                ((int[]) buf[24])[0] = rslt.getInt(18) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(18);
                ((String[]) buf[26])[0] = rslt.getMultimediaFile(19, rslt.getVarchar(14)) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(19);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 40) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((bool[]) buf[6])[0] = rslt.getBool(6) ;
                ((bool[]) buf[7])[0] = rslt.getBool(7) ;
                ((bool[]) buf[8])[0] = rslt.getBool(8) ;
                ((bool[]) buf[9])[0] = rslt.getBool(9) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(10);
                ((String[]) buf[12])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(11);
                ((bool[]) buf[14])[0] = rslt.getBool(12) ;
                ((String[]) buf[15])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(13);
                ((String[]) buf[17])[0] = rslt.getMultimediaUri(14) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(14);
                ((bool[]) buf[19])[0] = rslt.getBool(15) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(15);
                ((int[]) buf[21])[0] = rslt.getInt(16) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(16);
                ((int[]) buf[23])[0] = rslt.getInt(17) ;
                ((int[]) buf[24])[0] = rslt.getInt(18) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(18);
                ((String[]) buf[26])[0] = rslt.getMultimediaFile(19, rslt.getVarchar(14)) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(19);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 40) ;
                ((String[]) buf[14])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((bool[]) buf[16])[0] = rslt.getBool(10) ;
                ((bool[]) buf[17])[0] = rslt.getBool(11) ;
                ((bool[]) buf[18])[0] = rslt.getBool(12) ;
                ((bool[]) buf[19])[0] = rslt.getBool(13) ;
                ((bool[]) buf[20])[0] = rslt.getBool(14) ;
                ((bool[]) buf[21])[0] = rslt.getBool(15) ;
                ((String[]) buf[22])[0] = rslt.getVarchar(16) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(16);
                ((String[]) buf[24])[0] = rslt.getString(17, 1) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(17);
                ((bool[]) buf[26])[0] = rslt.getBool(18) ;
                ((String[]) buf[27])[0] = rslt.getVarchar(19) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(19);
                ((String[]) buf[29])[0] = rslt.getMultimediaUri(20) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(20);
                ((bool[]) buf[31])[0] = rslt.getBool(21) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(21);
                ((int[]) buf[33])[0] = rslt.getInt(22) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(22);
                ((int[]) buf[35])[0] = rslt.getInt(23) ;
                ((int[]) buf[36])[0] = rslt.getInt(24) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(24);
                ((int[]) buf[38])[0] = rslt.getInt(25) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(25);
                ((String[]) buf[40])[0] = rslt.getMultimediaFile(26, rslt.getVarchar(20)) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(26);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 22 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 23 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 25 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 26 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 27 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 28 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 29 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 31 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 32 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 33 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 34 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 35 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 36 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                return;
             case 37 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 38 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 5) ;
                return;
             case 39 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 40 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 41 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 42 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 43 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 44 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 45 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 46 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 47 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 48 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 49 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 50 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 51 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 52 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 40) ;
                ((String[]) buf[14])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((bool[]) buf[16])[0] = rslt.getBool(10) ;
                ((bool[]) buf[17])[0] = rslt.getBool(11) ;
                ((bool[]) buf[18])[0] = rslt.getBool(12) ;
                ((bool[]) buf[19])[0] = rslt.getBool(13) ;
                ((bool[]) buf[20])[0] = rslt.getBool(14) ;
                ((bool[]) buf[21])[0] = rslt.getBool(15) ;
                ((String[]) buf[22])[0] = rslt.getVarchar(16) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(16);
                ((String[]) buf[24])[0] = rslt.getString(17, 1) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(17);
                ((bool[]) buf[26])[0] = rslt.getBool(18) ;
                ((String[]) buf[27])[0] = rslt.getVarchar(19) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(19);
                ((String[]) buf[29])[0] = rslt.getMultimediaUri(20) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(20);
                ((bool[]) buf[31])[0] = rslt.getBool(21) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(21);
                ((int[]) buf[33])[0] = rslt.getInt(22) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(22);
                ((int[]) buf[35])[0] = rslt.getInt(23) ;
                ((int[]) buf[36])[0] = rslt.getInt(24) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(24);
                ((int[]) buf[38])[0] = rslt.getInt(25) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(25);
                ((String[]) buf[40])[0] = rslt.getMultimediaFile(26, rslt.getVarchar(20)) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(26);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (bool)parms[3]);
                stmt.SetParameter(4, (bool)parms[4]);
                stmt.SetParameter(5, (bool)parms[5]);
                stmt.SetParameter(6, (bool)parms[6]);
                stmt.SetParameter(7, (bool)parms[7]);
                stmt.SetParameter(8, (bool)parms[8]);
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 10 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[12]);
                }
                stmt.SetParameter(11, (bool)parms[13]);
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 12 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(12, (String)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 13 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(13, (String)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 14 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameterMultimedia(14, (String)parms[19], (String)parms[17]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 15 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(15, (bool)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 16 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(16, (int)parms[23]);
                }
                stmt.SetParameter(17, (int)parms[24]);
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 18 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(18, (int)parms[26]);
                }
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (bool)parms[3]);
                stmt.SetParameter(4, (bool)parms[4]);
                stmt.SetParameter(5, (bool)parms[5]);
                stmt.SetParameter(6, (bool)parms[6]);
                stmt.SetParameter(7, (bool)parms[7]);
                stmt.SetParameter(8, (bool)parms[8]);
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 10 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[12]);
                }
                stmt.SetParameter(11, (bool)parms[13]);
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 12 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(12, (String)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 13 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(13, (bool)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 14 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(14, (int)parms[19]);
                }
                stmt.SetParameter(15, (int)parms[20]);
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 16 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(16, (int)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 17 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(17, (int)parms[24]);
                }
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameterMultimedia(2, (String)parms[3], (String)parms[1]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 18 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 19 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 20 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 21 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 22 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 23 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 24 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 25 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 26 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 27 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 28 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 29 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 31 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 32 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 33 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 34 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 35 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 36 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 37 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 38 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 39 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 40 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 41 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 42 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 43 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 44 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 45 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 46 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 47 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 48 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 49 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 50 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 51 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 52 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
