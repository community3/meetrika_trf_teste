/*
               File: PRC_ClonarRegra
        Description: Clonar Regra
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:3:42.83
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_clonarregra : GXProcedure
   {
      public prc_clonarregra( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_clonarregra( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoSrvVnc_CntSrvCod ,
                           int aP1_Regra )
      {
         this.AV19ContratoSrvVnc_CntSrvCod = aP0_ContratoSrvVnc_CntSrvCod;
         this.AV11Regra = aP1_Regra;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_ContratoSrvVnc_CntSrvCod ,
                                 int aP1_Regra )
      {
         prc_clonarregra objprc_clonarregra;
         objprc_clonarregra = new prc_clonarregra();
         objprc_clonarregra.AV19ContratoSrvVnc_CntSrvCod = aP0_ContratoSrvVnc_CntSrvCod;
         objprc_clonarregra.AV11Regra = aP1_Regra;
         objprc_clonarregra.context.SetSubmitInitialConfig(context);
         objprc_clonarregra.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_clonarregra);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_clonarregra)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Optimized copy (Insert w/Subselect). */
         /* Using cursor P009L2 */
         pr_default.execute(0, new Object[] {AV19ContratoSrvVnc_CntSrvCod, AV11Regra});
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosVnc") ;
         /* End optimized group. */
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_ClonarRegra");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_clonarregra__default(),
            new Object[][] {
                new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV19ContratoSrvVnc_CntSrvCod ;
      private int AV11Regra ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
   }

   public class prc_clonarregra__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new UpdateCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP009L2 ;
          prmP009L2 = new Object[] {
          new Object[] {"@AV19ContratoSrvVnc_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11Regra",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P009L2", "INSERT INTO [ContratoServicosVnc]([ContratoSrvVnc_CntSrvCod], [ContratoSrvVnc_StatusDmn], [ContratoSrvVnc_PrestadoraCod], [ContratoSrvVnc_SemCusto], [ContratoServicosVnc_ClonaSrvOri], [ContratoServicosVnc_NaoClonaInfo], [ContratoServicosVnc_Gestor], [ContratoServicosVnc_Ativo], [ContratoSrvVnc_SrvVncCntSrvCod], [ContratoSrvVnc_SrvVncStatus], [ContratoSrvVnc_VincularCom], [ContratoSrvVnc_NovoStatusDmn], [ContratoSrvVnc_DoStatusDmn], [ContratoSrvVnc_NovoRspDmn], [ContratoServicosVnc_ClonarLink], [ContratoSrvVnc_SrvVncRef], [ContratoServicosVnc_Descricao]) SELECT @AV19ContratoSrvVnc_CntSrvCod AS ContratoSrvVnc_CntSrvCod, [ContratoSrvVnc_StatusDmn], [ContratoSrvVnc_PrestadoraCod], [ContratoSrvVnc_SemCusto], [ContratoServicosVnc_ClonaSrvOri], [ContratoServicosVnc_NaoClonaInfo], [ContratoServicosVnc_Gestor], [ContratoServicosVnc_Ativo], [ContratoSrvVnc_SrvVncCntSrvCod], [ContratoSrvVnc_SrvVncStatus], [ContratoSrvVnc_VincularCom], [ContratoSrvVnc_NovoStatusDmn], [ContratoSrvVnc_DoStatusDmn], [ContratoSrvVnc_NovoRspDmn], [ContratoServicosVnc_ClonarLink], [ContratoSrvVnc_SrvVncRef], [ContratoServicosVnc_Descricao]  FROM [ContratoServicosVnc] WHERE [ContratoSrvVnc_Codigo] = @AV11Regra", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP009L2)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
