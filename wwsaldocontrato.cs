/*
               File: WWSaldoContrato
        Description:  Saldo Contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 13:33:30.6
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwsaldocontrato : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwsaldocontrato( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwsaldocontrato( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_94 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_94_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_94_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16SaldoContrato_VigenciaInicio1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16SaldoContrato_VigenciaInicio1", context.localUtil.Format(AV16SaldoContrato_VigenciaInicio1, "99/99/99"));
               AV17SaldoContrato_VigenciaInicio_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17SaldoContrato_VigenciaInicio_To1", context.localUtil.Format(AV17SaldoContrato_VigenciaInicio_To1, "99/99/99"));
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20SaldoContrato_VigenciaInicio2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20SaldoContrato_VigenciaInicio2", context.localUtil.Format(AV20SaldoContrato_VigenciaInicio2, "99/99/99"));
               AV21SaldoContrato_VigenciaInicio_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21SaldoContrato_VigenciaInicio_To2", context.localUtil.Format(AV21SaldoContrato_VigenciaInicio_To2, "99/99/99"));
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24SaldoContrato_VigenciaInicio3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24SaldoContrato_VigenciaInicio3", context.localUtil.Format(AV24SaldoContrato_VigenciaInicio3, "99/99/99"));
               AV25SaldoContrato_VigenciaInicio_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25SaldoContrato_VigenciaInicio_To3", context.localUtil.Format(AV25SaldoContrato_VigenciaInicio_To3, "99/99/99"));
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV34TFSaldoContrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFSaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34TFSaldoContrato_Codigo), 6, 0)));
               AV35TFSaldoContrato_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFSaldoContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFSaldoContrato_Codigo_To), 6, 0)));
               AV38TFContrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFContrato_Codigo), 6, 0)));
               AV39TFContrato_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFContrato_Codigo_To), 6, 0)));
               AV42TFSaldoContrato_VigenciaInicio = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFSaldoContrato_VigenciaInicio", context.localUtil.Format(AV42TFSaldoContrato_VigenciaInicio, "99/99/99"));
               AV43TFSaldoContrato_VigenciaInicio_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFSaldoContrato_VigenciaInicio_To", context.localUtil.Format(AV43TFSaldoContrato_VigenciaInicio_To, "99/99/99"));
               AV48TFSaldoContrato_VigenciaFim = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFSaldoContrato_VigenciaFim", context.localUtil.Format(AV48TFSaldoContrato_VigenciaFim, "99/99/99"));
               AV49TFSaldoContrato_VigenciaFim_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFSaldoContrato_VigenciaFim_To", context.localUtil.Format(AV49TFSaldoContrato_VigenciaFim_To, "99/99/99"));
               AV54TFSaldoContrato_Credito = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFSaldoContrato_Credito", StringUtil.LTrim( StringUtil.Str( AV54TFSaldoContrato_Credito, 18, 5)));
               AV55TFSaldoContrato_Credito_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFSaldoContrato_Credito_To", StringUtil.LTrim( StringUtil.Str( AV55TFSaldoContrato_Credito_To, 18, 5)));
               AV58TFSaldoContrato_Reservado = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFSaldoContrato_Reservado", StringUtil.LTrim( StringUtil.Str( AV58TFSaldoContrato_Reservado, 18, 5)));
               AV59TFSaldoContrato_Reservado_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFSaldoContrato_Reservado_To", StringUtil.LTrim( StringUtil.Str( AV59TFSaldoContrato_Reservado_To, 18, 5)));
               AV62TFSaldoContrato_Executado = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFSaldoContrato_Executado", StringUtil.LTrim( StringUtil.Str( AV62TFSaldoContrato_Executado, 18, 5)));
               AV63TFSaldoContrato_Executado_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFSaldoContrato_Executado_To", StringUtil.LTrim( StringUtil.Str( AV63TFSaldoContrato_Executado_To, 18, 5)));
               AV66TFSaldoContrato_Saldo = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFSaldoContrato_Saldo", StringUtil.LTrim( StringUtil.Str( AV66TFSaldoContrato_Saldo, 18, 5)));
               AV67TFSaldoContrato_Saldo_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFSaldoContrato_Saldo_To", StringUtil.LTrim( StringUtil.Str( AV67TFSaldoContrato_Saldo_To, 18, 5)));
               AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace", AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace);
               AV40ddo_Contrato_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_Contrato_CodigoTitleControlIdToReplace", AV40ddo_Contrato_CodigoTitleControlIdToReplace);
               AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace", AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace);
               AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace", AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace);
               AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace", AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace);
               AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace", AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace);
               AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace", AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace);
               AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace", AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace);
               AV77Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               A1561SaldoContrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A1560NotaEmpenho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16SaldoContrato_VigenciaInicio1, AV17SaldoContrato_VigenciaInicio_To1, AV19DynamicFiltersSelector2, AV20SaldoContrato_VigenciaInicio2, AV21SaldoContrato_VigenciaInicio_To2, AV23DynamicFiltersSelector3, AV24SaldoContrato_VigenciaInicio3, AV25SaldoContrato_VigenciaInicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFSaldoContrato_Codigo, AV35TFSaldoContrato_Codigo_To, AV38TFContrato_Codigo, AV39TFContrato_Codigo_To, AV42TFSaldoContrato_VigenciaInicio, AV43TFSaldoContrato_VigenciaInicio_To, AV48TFSaldoContrato_VigenciaFim, AV49TFSaldoContrato_VigenciaFim_To, AV54TFSaldoContrato_Credito, AV55TFSaldoContrato_Credito_To, AV58TFSaldoContrato_Reservado, AV59TFSaldoContrato_Reservado_To, AV62TFSaldoContrato_Executado, AV63TFSaldoContrato_Executado_To, AV66TFSaldoContrato_Saldo, AV67TFSaldoContrato_Saldo_To, AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace, AV40ddo_Contrato_CodigoTitleControlIdToReplace, AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace, AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace, AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace, AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace, AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace, AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace, AV77Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1561SaldoContrato_Codigo, A1560NotaEmpenho_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAM52( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTM52( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051813333077");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwsaldocontrato.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vSALDOCONTRATO_VIGENCIAINICIO1", context.localUtil.Format(AV16SaldoContrato_VigenciaInicio1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vSALDOCONTRATO_VIGENCIAINICIO_TO1", context.localUtil.Format(AV17SaldoContrato_VigenciaInicio_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vSALDOCONTRATO_VIGENCIAINICIO2", context.localUtil.Format(AV20SaldoContrato_VigenciaInicio2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vSALDOCONTRATO_VIGENCIAINICIO_TO2", context.localUtil.Format(AV21SaldoContrato_VigenciaInicio_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vSALDOCONTRATO_VIGENCIAINICIO3", context.localUtil.Format(AV24SaldoContrato_VigenciaInicio3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vSALDOCONTRATO_VIGENCIAINICIO_TO3", context.localUtil.Format(AV25SaldoContrato_VigenciaInicio_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSALDOCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34TFSaldoContrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSALDOCONTRATO_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFSaldoContrato_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38TFContrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39TFContrato_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSALDOCONTRATO_VIGENCIAINICIO", context.localUtil.Format(AV42TFSaldoContrato_VigenciaInicio, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSALDOCONTRATO_VIGENCIAINICIO_TO", context.localUtil.Format(AV43TFSaldoContrato_VigenciaInicio_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSALDOCONTRATO_VIGENCIAFIM", context.localUtil.Format(AV48TFSaldoContrato_VigenciaFim, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSALDOCONTRATO_VIGENCIAFIM_TO", context.localUtil.Format(AV49TFSaldoContrato_VigenciaFim_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSALDOCONTRATO_CREDITO", StringUtil.LTrim( StringUtil.NToC( AV54TFSaldoContrato_Credito, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSALDOCONTRATO_CREDITO_TO", StringUtil.LTrim( StringUtil.NToC( AV55TFSaldoContrato_Credito_To, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSALDOCONTRATO_RESERVADO", StringUtil.LTrim( StringUtil.NToC( AV58TFSaldoContrato_Reservado, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSALDOCONTRATO_RESERVADO_TO", StringUtil.LTrim( StringUtil.NToC( AV59TFSaldoContrato_Reservado_To, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSALDOCONTRATO_EXECUTADO", StringUtil.LTrim( StringUtil.NToC( AV62TFSaldoContrato_Executado, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSALDOCONTRATO_EXECUTADO_TO", StringUtil.LTrim( StringUtil.NToC( AV63TFSaldoContrato_Executado_To, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSALDOCONTRATO_SALDO", StringUtil.LTrim( StringUtil.NToC( AV66TFSaldoContrato_Saldo, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSALDOCONTRATO_SALDO_TO", StringUtil.LTrim( StringUtil.NToC( AV67TFSaldoContrato_Saldo_To, 18, 5, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_94", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_94), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV71GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV72GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV69DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV69DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSALDOCONTRATO_CODIGOTITLEFILTERDATA", AV33SaldoContrato_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSALDOCONTRATO_CODIGOTITLEFILTERDATA", AV33SaldoContrato_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATO_CODIGOTITLEFILTERDATA", AV37Contrato_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATO_CODIGOTITLEFILTERDATA", AV37Contrato_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSALDOCONTRATO_VIGENCIAINICIOTITLEFILTERDATA", AV41SaldoContrato_VigenciaInicioTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSALDOCONTRATO_VIGENCIAINICIOTITLEFILTERDATA", AV41SaldoContrato_VigenciaInicioTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSALDOCONTRATO_VIGENCIAFIMTITLEFILTERDATA", AV47SaldoContrato_VigenciaFimTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSALDOCONTRATO_VIGENCIAFIMTITLEFILTERDATA", AV47SaldoContrato_VigenciaFimTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSALDOCONTRATO_CREDITOTITLEFILTERDATA", AV53SaldoContrato_CreditoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSALDOCONTRATO_CREDITOTITLEFILTERDATA", AV53SaldoContrato_CreditoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSALDOCONTRATO_RESERVADOTITLEFILTERDATA", AV57SaldoContrato_ReservadoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSALDOCONTRATO_RESERVADOTITLEFILTERDATA", AV57SaldoContrato_ReservadoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSALDOCONTRATO_EXECUTADOTITLEFILTERDATA", AV61SaldoContrato_ExecutadoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSALDOCONTRATO_EXECUTADOTITLEFILTERDATA", AV61SaldoContrato_ExecutadoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSALDOCONTRATO_SALDOTITLEFILTERDATA", AV65SaldoContrato_SaldoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSALDOCONTRATO_SALDOTITLEFILTERDATA", AV65SaldoContrato_SaldoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV77Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "NOTAEMPENHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1560NotaEmpenho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Caption", StringUtil.RTrim( Ddo_saldocontrato_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Tooltip", StringUtil.RTrim( Ddo_saldocontrato_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Cls", StringUtil.RTrim( Ddo_saldocontrato_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_saldocontrato_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_saldocontrato_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_saldocontrato_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_saldocontrato_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_saldocontrato_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_saldocontrato_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_saldocontrato_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_saldocontrato_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Filtertype", StringUtil.RTrim( Ddo_saldocontrato_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_saldocontrato_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_saldocontrato_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Sortasc", StringUtil.RTrim( Ddo_saldocontrato_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_saldocontrato_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_saldocontrato_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_saldocontrato_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_saldocontrato_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_saldocontrato_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Caption", StringUtil.RTrim( Ddo_contrato_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Tooltip", StringUtil.RTrim( Ddo_contrato_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Cls", StringUtil.RTrim( Ddo_contrato_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_contrato_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_contrato_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contrato_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contrato_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_contrato_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_contrato_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_contrato_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_contrato_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filtertype", StringUtil.RTrim( Ddo_contrato_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_contrato_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_contrato_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Sortasc", StringUtil.RTrim( Ddo_contrato_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_contrato_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_contrato_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_contrato_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_contrato_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_contrato_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAINICIO_Caption", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAINICIO_Tooltip", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAINICIO_Cls", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAINICIO_Filteredtext_set", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAINICIO_Filteredtextto_set", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAINICIO_Dropdownoptionstype", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAINICIO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAINICIO_Includesortasc", StringUtil.BoolToStr( Ddo_saldocontrato_vigenciainicio_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAINICIO_Includesortdsc", StringUtil.BoolToStr( Ddo_saldocontrato_vigenciainicio_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAINICIO_Sortedstatus", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAINICIO_Includefilter", StringUtil.BoolToStr( Ddo_saldocontrato_vigenciainicio_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAINICIO_Filtertype", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAINICIO_Filterisrange", StringUtil.BoolToStr( Ddo_saldocontrato_vigenciainicio_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAINICIO_Includedatalist", StringUtil.BoolToStr( Ddo_saldocontrato_vigenciainicio_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAINICIO_Sortasc", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAINICIO_Sortdsc", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAINICIO_Cleanfilter", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAINICIO_Rangefilterfrom", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAINICIO_Rangefilterto", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAINICIO_Searchbuttontext", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAFIM_Caption", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAFIM_Tooltip", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAFIM_Cls", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAFIM_Filteredtext_set", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAFIM_Filteredtextto_set", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAFIM_Dropdownoptionstype", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAFIM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAFIM_Includesortasc", StringUtil.BoolToStr( Ddo_saldocontrato_vigenciafim_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAFIM_Includesortdsc", StringUtil.BoolToStr( Ddo_saldocontrato_vigenciafim_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAFIM_Sortedstatus", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAFIM_Includefilter", StringUtil.BoolToStr( Ddo_saldocontrato_vigenciafim_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAFIM_Filtertype", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAFIM_Filterisrange", StringUtil.BoolToStr( Ddo_saldocontrato_vigenciafim_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAFIM_Includedatalist", StringUtil.BoolToStr( Ddo_saldocontrato_vigenciafim_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAFIM_Sortasc", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAFIM_Sortdsc", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAFIM_Cleanfilter", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAFIM_Rangefilterfrom", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAFIM_Rangefilterto", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAFIM_Searchbuttontext", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CREDITO_Caption", StringUtil.RTrim( Ddo_saldocontrato_credito_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CREDITO_Tooltip", StringUtil.RTrim( Ddo_saldocontrato_credito_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CREDITO_Cls", StringUtil.RTrim( Ddo_saldocontrato_credito_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CREDITO_Filteredtext_set", StringUtil.RTrim( Ddo_saldocontrato_credito_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CREDITO_Filteredtextto_set", StringUtil.RTrim( Ddo_saldocontrato_credito_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CREDITO_Dropdownoptionstype", StringUtil.RTrim( Ddo_saldocontrato_credito_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CREDITO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_saldocontrato_credito_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CREDITO_Includesortasc", StringUtil.BoolToStr( Ddo_saldocontrato_credito_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CREDITO_Includesortdsc", StringUtil.BoolToStr( Ddo_saldocontrato_credito_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CREDITO_Sortedstatus", StringUtil.RTrim( Ddo_saldocontrato_credito_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CREDITO_Includefilter", StringUtil.BoolToStr( Ddo_saldocontrato_credito_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CREDITO_Filtertype", StringUtil.RTrim( Ddo_saldocontrato_credito_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CREDITO_Filterisrange", StringUtil.BoolToStr( Ddo_saldocontrato_credito_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CREDITO_Includedatalist", StringUtil.BoolToStr( Ddo_saldocontrato_credito_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CREDITO_Sortasc", StringUtil.RTrim( Ddo_saldocontrato_credito_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CREDITO_Sortdsc", StringUtil.RTrim( Ddo_saldocontrato_credito_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CREDITO_Cleanfilter", StringUtil.RTrim( Ddo_saldocontrato_credito_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CREDITO_Rangefilterfrom", StringUtil.RTrim( Ddo_saldocontrato_credito_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CREDITO_Rangefilterto", StringUtil.RTrim( Ddo_saldocontrato_credito_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CREDITO_Searchbuttontext", StringUtil.RTrim( Ddo_saldocontrato_credito_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_RESERVADO_Caption", StringUtil.RTrim( Ddo_saldocontrato_reservado_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_RESERVADO_Tooltip", StringUtil.RTrim( Ddo_saldocontrato_reservado_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_RESERVADO_Cls", StringUtil.RTrim( Ddo_saldocontrato_reservado_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_RESERVADO_Filteredtext_set", StringUtil.RTrim( Ddo_saldocontrato_reservado_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_RESERVADO_Filteredtextto_set", StringUtil.RTrim( Ddo_saldocontrato_reservado_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_RESERVADO_Dropdownoptionstype", StringUtil.RTrim( Ddo_saldocontrato_reservado_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_RESERVADO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_saldocontrato_reservado_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_RESERVADO_Includesortasc", StringUtil.BoolToStr( Ddo_saldocontrato_reservado_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_RESERVADO_Includesortdsc", StringUtil.BoolToStr( Ddo_saldocontrato_reservado_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_RESERVADO_Sortedstatus", StringUtil.RTrim( Ddo_saldocontrato_reservado_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_RESERVADO_Includefilter", StringUtil.BoolToStr( Ddo_saldocontrato_reservado_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_RESERVADO_Filtertype", StringUtil.RTrim( Ddo_saldocontrato_reservado_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_RESERVADO_Filterisrange", StringUtil.BoolToStr( Ddo_saldocontrato_reservado_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_RESERVADO_Includedatalist", StringUtil.BoolToStr( Ddo_saldocontrato_reservado_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_RESERVADO_Sortasc", StringUtil.RTrim( Ddo_saldocontrato_reservado_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_RESERVADO_Sortdsc", StringUtil.RTrim( Ddo_saldocontrato_reservado_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_RESERVADO_Cleanfilter", StringUtil.RTrim( Ddo_saldocontrato_reservado_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_RESERVADO_Rangefilterfrom", StringUtil.RTrim( Ddo_saldocontrato_reservado_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_RESERVADO_Rangefilterto", StringUtil.RTrim( Ddo_saldocontrato_reservado_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_RESERVADO_Searchbuttontext", StringUtil.RTrim( Ddo_saldocontrato_reservado_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_EXECUTADO_Caption", StringUtil.RTrim( Ddo_saldocontrato_executado_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_EXECUTADO_Tooltip", StringUtil.RTrim( Ddo_saldocontrato_executado_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_EXECUTADO_Cls", StringUtil.RTrim( Ddo_saldocontrato_executado_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_EXECUTADO_Filteredtext_set", StringUtil.RTrim( Ddo_saldocontrato_executado_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_EXECUTADO_Filteredtextto_set", StringUtil.RTrim( Ddo_saldocontrato_executado_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_EXECUTADO_Dropdownoptionstype", StringUtil.RTrim( Ddo_saldocontrato_executado_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_EXECUTADO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_saldocontrato_executado_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_EXECUTADO_Includesortasc", StringUtil.BoolToStr( Ddo_saldocontrato_executado_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_EXECUTADO_Includesortdsc", StringUtil.BoolToStr( Ddo_saldocontrato_executado_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_EXECUTADO_Sortedstatus", StringUtil.RTrim( Ddo_saldocontrato_executado_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_EXECUTADO_Includefilter", StringUtil.BoolToStr( Ddo_saldocontrato_executado_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_EXECUTADO_Filtertype", StringUtil.RTrim( Ddo_saldocontrato_executado_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_EXECUTADO_Filterisrange", StringUtil.BoolToStr( Ddo_saldocontrato_executado_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_EXECUTADO_Includedatalist", StringUtil.BoolToStr( Ddo_saldocontrato_executado_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_EXECUTADO_Sortasc", StringUtil.RTrim( Ddo_saldocontrato_executado_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_EXECUTADO_Sortdsc", StringUtil.RTrim( Ddo_saldocontrato_executado_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_EXECUTADO_Cleanfilter", StringUtil.RTrim( Ddo_saldocontrato_executado_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_EXECUTADO_Rangefilterfrom", StringUtil.RTrim( Ddo_saldocontrato_executado_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_EXECUTADO_Rangefilterto", StringUtil.RTrim( Ddo_saldocontrato_executado_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_EXECUTADO_Searchbuttontext", StringUtil.RTrim( Ddo_saldocontrato_executado_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_SALDO_Caption", StringUtil.RTrim( Ddo_saldocontrato_saldo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_SALDO_Tooltip", StringUtil.RTrim( Ddo_saldocontrato_saldo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_SALDO_Cls", StringUtil.RTrim( Ddo_saldocontrato_saldo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_SALDO_Filteredtext_set", StringUtil.RTrim( Ddo_saldocontrato_saldo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_SALDO_Filteredtextto_set", StringUtil.RTrim( Ddo_saldocontrato_saldo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_SALDO_Dropdownoptionstype", StringUtil.RTrim( Ddo_saldocontrato_saldo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_SALDO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_saldocontrato_saldo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_SALDO_Includesortasc", StringUtil.BoolToStr( Ddo_saldocontrato_saldo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_SALDO_Includesortdsc", StringUtil.BoolToStr( Ddo_saldocontrato_saldo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_SALDO_Sortedstatus", StringUtil.RTrim( Ddo_saldocontrato_saldo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_SALDO_Includefilter", StringUtil.BoolToStr( Ddo_saldocontrato_saldo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_SALDO_Filtertype", StringUtil.RTrim( Ddo_saldocontrato_saldo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_SALDO_Filterisrange", StringUtil.BoolToStr( Ddo_saldocontrato_saldo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_SALDO_Includedatalist", StringUtil.BoolToStr( Ddo_saldocontrato_saldo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_SALDO_Sortasc", StringUtil.RTrim( Ddo_saldocontrato_saldo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_SALDO_Sortdsc", StringUtil.RTrim( Ddo_saldocontrato_saldo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_SALDO_Cleanfilter", StringUtil.RTrim( Ddo_saldocontrato_saldo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_SALDO_Rangefilterfrom", StringUtil.RTrim( Ddo_saldocontrato_saldo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_SALDO_Rangefilterto", StringUtil.RTrim( Ddo_saldocontrato_saldo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_SALDO_Searchbuttontext", StringUtil.RTrim( Ddo_saldocontrato_saldo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_saldocontrato_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_saldocontrato_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_saldocontrato_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_contrato_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_contrato_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_contrato_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAINICIO_Activeeventkey", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAINICIO_Filteredtext_get", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAINICIO_Filteredtextto_get", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAFIM_Activeeventkey", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAFIM_Filteredtext_get", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_VIGENCIAFIM_Filteredtextto_get", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CREDITO_Activeeventkey", StringUtil.RTrim( Ddo_saldocontrato_credito_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CREDITO_Filteredtext_get", StringUtil.RTrim( Ddo_saldocontrato_credito_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CREDITO_Filteredtextto_get", StringUtil.RTrim( Ddo_saldocontrato_credito_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_RESERVADO_Activeeventkey", StringUtil.RTrim( Ddo_saldocontrato_reservado_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_RESERVADO_Filteredtext_get", StringUtil.RTrim( Ddo_saldocontrato_reservado_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_RESERVADO_Filteredtextto_get", StringUtil.RTrim( Ddo_saldocontrato_reservado_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_EXECUTADO_Activeeventkey", StringUtil.RTrim( Ddo_saldocontrato_executado_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_EXECUTADO_Filteredtext_get", StringUtil.RTrim( Ddo_saldocontrato_executado_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_EXECUTADO_Filteredtextto_get", StringUtil.RTrim( Ddo_saldocontrato_executado_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_SALDO_Activeeventkey", StringUtil.RTrim( Ddo_saldocontrato_saldo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_SALDO_Filteredtext_get", StringUtil.RTrim( Ddo_saldocontrato_saldo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_SALDO_Filteredtextto_get", StringUtil.RTrim( Ddo_saldocontrato_saldo_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEM52( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTM52( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwsaldocontrato.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWSaldoContrato" ;
      }

      public override String GetPgmdesc( )
      {
         return " Saldo Contrato" ;
      }

      protected void WBM50( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_M52( true) ;
         }
         else
         {
            wb_table1_2_M52( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_M52e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(109, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,109);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(110, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,110);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34TFSaldoContrato_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV34TFSaldoContrato_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSaldoContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFSaldoContrato_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV35TFSaldoContrato_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,112);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSaldoContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38TFContrato_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV38TFContrato_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSaldoContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39TFContrato_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV39TFContrato_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSaldoContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfsaldocontrato_vigenciainicio_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_vigenciainicio_Internalname, context.localUtil.Format(AV42TFSaldoContrato_VigenciaInicio, "99/99/99"), context.localUtil.Format( AV42TFSaldoContrato_VigenciaInicio, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_vigenciainicio_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_vigenciainicio_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSaldoContrato.htm");
            GxWebStd.gx_bitmap( context, edtavTfsaldocontrato_vigenciainicio_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfsaldocontrato_vigenciainicio_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfsaldocontrato_vigenciainicio_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_vigenciainicio_to_Internalname, context.localUtil.Format(AV43TFSaldoContrato_VigenciaInicio_To, "99/99/99"), context.localUtil.Format( AV43TFSaldoContrato_VigenciaInicio_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_vigenciainicio_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_vigenciainicio_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSaldoContrato.htm");
            GxWebStd.gx_bitmap( context, edtavTfsaldocontrato_vigenciainicio_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfsaldocontrato_vigenciainicio_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_saldocontrato_vigenciainicioauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_saldocontrato_vigenciainicioauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_saldocontrato_vigenciainicioauxdate_Internalname, context.localUtil.Format(AV44DDO_SaldoContrato_VigenciaInicioAuxDate, "99/99/99"), context.localUtil.Format( AV44DDO_SaldoContrato_VigenciaInicioAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,118);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_saldocontrato_vigenciainicioauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSaldoContrato.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_saldocontrato_vigenciainicioauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_saldocontrato_vigenciainicioauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_saldocontrato_vigenciainicioauxdateto_Internalname, context.localUtil.Format(AV45DDO_SaldoContrato_VigenciaInicioAuxDateTo, "99/99/99"), context.localUtil.Format( AV45DDO_SaldoContrato_VigenciaInicioAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,119);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_saldocontrato_vigenciainicioauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSaldoContrato.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_saldocontrato_vigenciainicioauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfsaldocontrato_vigenciafim_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_vigenciafim_Internalname, context.localUtil.Format(AV48TFSaldoContrato_VigenciaFim, "99/99/99"), context.localUtil.Format( AV48TFSaldoContrato_VigenciaFim, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,120);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_vigenciafim_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_vigenciafim_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSaldoContrato.htm");
            GxWebStd.gx_bitmap( context, edtavTfsaldocontrato_vigenciafim_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfsaldocontrato_vigenciafim_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfsaldocontrato_vigenciafim_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_vigenciafim_to_Internalname, context.localUtil.Format(AV49TFSaldoContrato_VigenciaFim_To, "99/99/99"), context.localUtil.Format( AV49TFSaldoContrato_VigenciaFim_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,121);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_vigenciafim_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_vigenciafim_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSaldoContrato.htm");
            GxWebStd.gx_bitmap( context, edtavTfsaldocontrato_vigenciafim_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfsaldocontrato_vigenciafim_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_saldocontrato_vigenciafimauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_saldocontrato_vigenciafimauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_saldocontrato_vigenciafimauxdate_Internalname, context.localUtil.Format(AV50DDO_SaldoContrato_VigenciaFimAuxDate, "99/99/99"), context.localUtil.Format( AV50DDO_SaldoContrato_VigenciaFimAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,123);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_saldocontrato_vigenciafimauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSaldoContrato.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_saldocontrato_vigenciafimauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_saldocontrato_vigenciafimauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_saldocontrato_vigenciafimauxdateto_Internalname, context.localUtil.Format(AV51DDO_SaldoContrato_VigenciaFimAuxDateTo, "99/99/99"), context.localUtil.Format( AV51DDO_SaldoContrato_VigenciaFimAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,124);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_saldocontrato_vigenciafimauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSaldoContrato.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_saldocontrato_vigenciafimauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_credito_Internalname, StringUtil.LTrim( StringUtil.NToC( AV54TFSaldoContrato_Credito, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV54TFSaldoContrato_Credito, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,125);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_credito_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_credito_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSaldoContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_credito_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV55TFSaldoContrato_Credito_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV55TFSaldoContrato_Credito_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,126);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_credito_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_credito_to_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSaldoContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_reservado_Internalname, StringUtil.LTrim( StringUtil.NToC( AV58TFSaldoContrato_Reservado, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV58TFSaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,127);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_reservado_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_reservado_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSaldoContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_reservado_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV59TFSaldoContrato_Reservado_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV59TFSaldoContrato_Reservado_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,128);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_reservado_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_reservado_to_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSaldoContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 129,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_executado_Internalname, StringUtil.LTrim( StringUtil.NToC( AV62TFSaldoContrato_Executado, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV62TFSaldoContrato_Executado, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,129);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_executado_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_executado_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSaldoContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 130,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_executado_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV63TFSaldoContrato_Executado_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV63TFSaldoContrato_Executado_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,130);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_executado_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_executado_to_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSaldoContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_saldo_Internalname, StringUtil.LTrim( StringUtil.NToC( AV66TFSaldoContrato_Saldo, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV66TFSaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,131);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_saldo_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_saldo_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSaldoContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 132,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_saldo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV67TFSaldoContrato_Saldo_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV67TFSaldoContrato_Saldo_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,132);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_saldo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_saldo_to_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSaldoContrato.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SALDOCONTRATO_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 134,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Internalname, AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,134);\"", 0, edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWSaldoContrato.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATO_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 136,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname, AV40ddo_Contrato_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,136);\"", 0, edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWSaldoContrato.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SALDOCONTRATO_VIGENCIAINICIOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 138,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_saldocontrato_vigenciainiciotitlecontrolidtoreplace_Internalname, AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,138);\"", 0, edtavDdo_saldocontrato_vigenciainiciotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWSaldoContrato.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SALDOCONTRATO_VIGENCIAFIMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 140,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_saldocontrato_vigenciafimtitlecontrolidtoreplace_Internalname, AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,140);\"", 0, edtavDdo_saldocontrato_vigenciafimtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWSaldoContrato.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SALDOCONTRATO_CREDITOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 142,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_saldocontrato_creditotitlecontrolidtoreplace_Internalname, AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,142);\"", 0, edtavDdo_saldocontrato_creditotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWSaldoContrato.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SALDOCONTRATO_RESERVADOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 144,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_saldocontrato_reservadotitlecontrolidtoreplace_Internalname, AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,144);\"", 0, edtavDdo_saldocontrato_reservadotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWSaldoContrato.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SALDOCONTRATO_EXECUTADOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 146,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_saldocontrato_executadotitlecontrolidtoreplace_Internalname, AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,146);\"", 0, edtavDdo_saldocontrato_executadotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWSaldoContrato.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SALDOCONTRATO_SALDOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 148,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_saldocontrato_saldotitlecontrolidtoreplace_Internalname, AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,148);\"", 0, edtavDdo_saldocontrato_saldotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWSaldoContrato.htm");
         }
         wbLoad = true;
      }

      protected void STARTM52( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Saldo Contrato", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPM50( ) ;
      }

      protected void WSM52( )
      {
         STARTM52( ) ;
         EVTM52( ) ;
      }

      protected void EVTM52( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11M52 */
                              E11M52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SALDOCONTRATO_CODIGO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12M52 */
                              E12M52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATO_CODIGO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13M52 */
                              E13M52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SALDOCONTRATO_VIGENCIAINICIO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14M52 */
                              E14M52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SALDOCONTRATO_VIGENCIAFIM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15M52 */
                              E15M52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SALDOCONTRATO_CREDITO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16M52 */
                              E16M52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SALDOCONTRATO_RESERVADO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17M52 */
                              E17M52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SALDOCONTRATO_EXECUTADO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18M52 */
                              E18M52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SALDOCONTRATO_SALDO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19M52 */
                              E19M52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20M52 */
                              E20M52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21M52 */
                              E21M52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22M52 */
                              E22M52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23M52 */
                              E23M52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24M52 */
                              E24M52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25M52 */
                              E25M52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E26M52 */
                              E26M52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E27M52 */
                              E27M52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E28M52 */
                              E28M52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E29M52 */
                              E29M52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E30M52 */
                              E30M52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_94_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_94_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_94_idx), 4, 0)), 4, "0");
                              SubsflControlProps_942( ) ;
                              AV28Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV75Update_GXI : context.convertURL( context.PathToRelativeUrl( AV28Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV76Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              A1561SaldoContrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSaldoContrato_Codigo_Internalname), ",", "."));
                              A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContrato_Codigo_Internalname), ",", "."));
                              A1571SaldoContrato_VigenciaInicio = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtSaldoContrato_VigenciaInicio_Internalname), 0));
                              A1572SaldoContrato_VigenciaFim = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtSaldoContrato_VigenciaFim_Internalname), 0));
                              A1573SaldoContrato_Credito = context.localUtil.CToN( cgiGet( edtSaldoContrato_Credito_Internalname), ",", ".");
                              A1574SaldoContrato_Reservado = context.localUtil.CToN( cgiGet( edtSaldoContrato_Reservado_Internalname), ",", ".");
                              A1575SaldoContrato_Executado = context.localUtil.CToN( cgiGet( edtSaldoContrato_Executado_Internalname), ",", ".");
                              A1576SaldoContrato_Saldo = context.localUtil.CToN( cgiGet( edtSaldoContrato_Saldo_Internalname), ",", ".");
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E31M52 */
                                    E31M52 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E32M52 */
                                    E32M52 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E33M52 */
                                    E33M52 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Saldocontrato_vigenciainicio1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vSALDOCONTRATO_VIGENCIAINICIO1"), 0) != AV16SaldoContrato_VigenciaInicio1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Saldocontrato_vigenciainicio_to1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vSALDOCONTRATO_VIGENCIAINICIO_TO1"), 0) != AV17SaldoContrato_VigenciaInicio_To1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Saldocontrato_vigenciainicio2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vSALDOCONTRATO_VIGENCIAINICIO2"), 0) != AV20SaldoContrato_VigenciaInicio2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Saldocontrato_vigenciainicio_to2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vSALDOCONTRATO_VIGENCIAINICIO_TO2"), 0) != AV21SaldoContrato_VigenciaInicio_To2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Saldocontrato_vigenciainicio3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vSALDOCONTRATO_VIGENCIAINICIO3"), 0) != AV24SaldoContrato_VigenciaInicio3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Saldocontrato_vigenciainicio_to3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vSALDOCONTRATO_VIGENCIAINICIO_TO3"), 0) != AV25SaldoContrato_VigenciaInicio_To3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsaldocontrato_codigo Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSALDOCONTRATO_CODIGO"), ",", ".") != Convert.ToDecimal( AV34TFSaldoContrato_Codigo )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsaldocontrato_codigo_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSALDOCONTRATO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV35TFSaldoContrato_Codigo_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontrato_codigo Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_CODIGO"), ",", ".") != Convert.ToDecimal( AV38TFContrato_Codigo )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontrato_codigo_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV39TFContrato_Codigo_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsaldocontrato_vigenciainicio Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFSALDOCONTRATO_VIGENCIAINICIO"), 0) != AV42TFSaldoContrato_VigenciaInicio )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsaldocontrato_vigenciainicio_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFSALDOCONTRATO_VIGENCIAINICIO_TO"), 0) != AV43TFSaldoContrato_VigenciaInicio_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsaldocontrato_vigenciafim Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFSALDOCONTRATO_VIGENCIAFIM"), 0) != AV48TFSaldoContrato_VigenciaFim )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsaldocontrato_vigenciafim_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFSALDOCONTRATO_VIGENCIAFIM_TO"), 0) != AV49TFSaldoContrato_VigenciaFim_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsaldocontrato_credito Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFSALDOCONTRATO_CREDITO"), ",", ".") != AV54TFSaldoContrato_Credito )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsaldocontrato_credito_to Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFSALDOCONTRATO_CREDITO_TO"), ",", ".") != AV55TFSaldoContrato_Credito_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsaldocontrato_reservado Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFSALDOCONTRATO_RESERVADO"), ",", ".") != AV58TFSaldoContrato_Reservado )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsaldocontrato_reservado_to Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFSALDOCONTRATO_RESERVADO_TO"), ",", ".") != AV59TFSaldoContrato_Reservado_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsaldocontrato_executado Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFSALDOCONTRATO_EXECUTADO"), ",", ".") != AV62TFSaldoContrato_Executado )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsaldocontrato_executado_to Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFSALDOCONTRATO_EXECUTADO_TO"), ",", ".") != AV63TFSaldoContrato_Executado_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsaldocontrato_saldo Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFSALDOCONTRATO_SALDO"), ",", ".") != AV66TFSaldoContrato_Saldo )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsaldocontrato_saldo_to Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFSALDOCONTRATO_SALDO_TO"), ",", ".") != AV67TFSaldoContrato_Saldo_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEM52( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAM52( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("SALDOCONTRATO_VIGENCIAINICIO", "Inicial", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("SALDOCONTRATO_VIGENCIAINICIO", "Inicial", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("SALDOCONTRATO_VIGENCIAINICIO", "Inicial", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_942( ) ;
         while ( nGXsfl_94_idx <= nRC_GXsfl_94 )
         {
            sendrow_942( ) ;
            nGXsfl_94_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_94_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_94_idx+1));
            sGXsfl_94_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_94_idx), 4, 0)), 4, "0");
            SubsflControlProps_942( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       DateTime AV16SaldoContrato_VigenciaInicio1 ,
                                       DateTime AV17SaldoContrato_VigenciaInicio_To1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       DateTime AV20SaldoContrato_VigenciaInicio2 ,
                                       DateTime AV21SaldoContrato_VigenciaInicio_To2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       DateTime AV24SaldoContrato_VigenciaInicio3 ,
                                       DateTime AV25SaldoContrato_VigenciaInicio_To3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       int AV34TFSaldoContrato_Codigo ,
                                       int AV35TFSaldoContrato_Codigo_To ,
                                       int AV38TFContrato_Codigo ,
                                       int AV39TFContrato_Codigo_To ,
                                       DateTime AV42TFSaldoContrato_VigenciaInicio ,
                                       DateTime AV43TFSaldoContrato_VigenciaInicio_To ,
                                       DateTime AV48TFSaldoContrato_VigenciaFim ,
                                       DateTime AV49TFSaldoContrato_VigenciaFim_To ,
                                       decimal AV54TFSaldoContrato_Credito ,
                                       decimal AV55TFSaldoContrato_Credito_To ,
                                       decimal AV58TFSaldoContrato_Reservado ,
                                       decimal AV59TFSaldoContrato_Reservado_To ,
                                       decimal AV62TFSaldoContrato_Executado ,
                                       decimal AV63TFSaldoContrato_Executado_To ,
                                       decimal AV66TFSaldoContrato_Saldo ,
                                       decimal AV67TFSaldoContrato_Saldo_To ,
                                       String AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace ,
                                       String AV40ddo_Contrato_CodigoTitleControlIdToReplace ,
                                       String AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace ,
                                       String AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace ,
                                       String AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace ,
                                       String AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace ,
                                       String AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace ,
                                       String AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace ,
                                       String AV77Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       int A1561SaldoContrato_Codigo ,
                                       int A1560NotaEmpenho_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFM52( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_SALDOCONTRATO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1561SaldoContrato_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SALDOCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1561SaldoContrato_Codigo), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFM52( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV77Pgmname = "WWSaldoContrato";
         context.Gx_err = 0;
      }

      protected void RFM52( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 94;
         /* Execute user event: E32M52 */
         E32M52 ();
         nGXsfl_94_idx = 1;
         sGXsfl_94_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_94_idx), 4, 0)), 4, "0");
         SubsflControlProps_942( ) ;
         nGXsfl_94_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_942( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16SaldoContrato_VigenciaInicio1 ,
                                                 AV17SaldoContrato_VigenciaInicio_To1 ,
                                                 AV18DynamicFiltersEnabled2 ,
                                                 AV19DynamicFiltersSelector2 ,
                                                 AV20SaldoContrato_VigenciaInicio2 ,
                                                 AV21SaldoContrato_VigenciaInicio_To2 ,
                                                 AV22DynamicFiltersEnabled3 ,
                                                 AV23DynamicFiltersSelector3 ,
                                                 AV24SaldoContrato_VigenciaInicio3 ,
                                                 AV25SaldoContrato_VigenciaInicio_To3 ,
                                                 AV34TFSaldoContrato_Codigo ,
                                                 AV35TFSaldoContrato_Codigo_To ,
                                                 AV38TFContrato_Codigo ,
                                                 AV39TFContrato_Codigo_To ,
                                                 AV42TFSaldoContrato_VigenciaInicio ,
                                                 AV43TFSaldoContrato_VigenciaInicio_To ,
                                                 AV48TFSaldoContrato_VigenciaFim ,
                                                 AV49TFSaldoContrato_VigenciaFim_To ,
                                                 AV54TFSaldoContrato_Credito ,
                                                 AV55TFSaldoContrato_Credito_To ,
                                                 AV58TFSaldoContrato_Reservado ,
                                                 AV59TFSaldoContrato_Reservado_To ,
                                                 AV62TFSaldoContrato_Executado ,
                                                 AV63TFSaldoContrato_Executado_To ,
                                                 AV66TFSaldoContrato_Saldo ,
                                                 AV67TFSaldoContrato_Saldo_To ,
                                                 A1571SaldoContrato_VigenciaInicio ,
                                                 A1561SaldoContrato_Codigo ,
                                                 A74Contrato_Codigo ,
                                                 A1572SaldoContrato_VigenciaFim ,
                                                 A1573SaldoContrato_Credito ,
                                                 A1574SaldoContrato_Reservado ,
                                                 A1575SaldoContrato_Executado ,
                                                 A1576SaldoContrato_Saldo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL,
                                                 TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor H00M52 */
            pr_default.execute(0, new Object[] {AV16SaldoContrato_VigenciaInicio1, AV17SaldoContrato_VigenciaInicio_To1, AV20SaldoContrato_VigenciaInicio2, AV21SaldoContrato_VigenciaInicio_To2, AV24SaldoContrato_VigenciaInicio3, AV25SaldoContrato_VigenciaInicio_To3, AV34TFSaldoContrato_Codigo, AV35TFSaldoContrato_Codigo_To, AV38TFContrato_Codigo, AV39TFContrato_Codigo_To, AV42TFSaldoContrato_VigenciaInicio, AV43TFSaldoContrato_VigenciaInicio_To, AV48TFSaldoContrato_VigenciaFim, AV49TFSaldoContrato_VigenciaFim_To, AV54TFSaldoContrato_Credito, AV55TFSaldoContrato_Credito_To, AV58TFSaldoContrato_Reservado, AV59TFSaldoContrato_Reservado_To, AV62TFSaldoContrato_Executado, AV63TFSaldoContrato_Executado_To, AV66TFSaldoContrato_Saldo, AV67TFSaldoContrato_Saldo_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_94_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1560NotaEmpenho_Codigo = H00M52_A1560NotaEmpenho_Codigo[0];
               A1576SaldoContrato_Saldo = H00M52_A1576SaldoContrato_Saldo[0];
               A1575SaldoContrato_Executado = H00M52_A1575SaldoContrato_Executado[0];
               A1574SaldoContrato_Reservado = H00M52_A1574SaldoContrato_Reservado[0];
               A1573SaldoContrato_Credito = H00M52_A1573SaldoContrato_Credito[0];
               A1572SaldoContrato_VigenciaFim = H00M52_A1572SaldoContrato_VigenciaFim[0];
               A1571SaldoContrato_VigenciaInicio = H00M52_A1571SaldoContrato_VigenciaInicio[0];
               A74Contrato_Codigo = H00M52_A74Contrato_Codigo[0];
               A1561SaldoContrato_Codigo = H00M52_A1561SaldoContrato_Codigo[0];
               A1576SaldoContrato_Saldo = H00M52_A1576SaldoContrato_Saldo[0];
               A1575SaldoContrato_Executado = H00M52_A1575SaldoContrato_Executado[0];
               A1574SaldoContrato_Reservado = H00M52_A1574SaldoContrato_Reservado[0];
               A1573SaldoContrato_Credito = H00M52_A1573SaldoContrato_Credito[0];
               A1572SaldoContrato_VigenciaFim = H00M52_A1572SaldoContrato_VigenciaFim[0];
               A1571SaldoContrato_VigenciaInicio = H00M52_A1571SaldoContrato_VigenciaInicio[0];
               A74Contrato_Codigo = H00M52_A74Contrato_Codigo[0];
               /* Execute user event: E33M52 */
               E33M52 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 94;
            WBM50( ) ;
         }
         nGXsfl_94_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV16SaldoContrato_VigenciaInicio1 ,
                                              AV17SaldoContrato_VigenciaInicio_To1 ,
                                              AV18DynamicFiltersEnabled2 ,
                                              AV19DynamicFiltersSelector2 ,
                                              AV20SaldoContrato_VigenciaInicio2 ,
                                              AV21SaldoContrato_VigenciaInicio_To2 ,
                                              AV22DynamicFiltersEnabled3 ,
                                              AV23DynamicFiltersSelector3 ,
                                              AV24SaldoContrato_VigenciaInicio3 ,
                                              AV25SaldoContrato_VigenciaInicio_To3 ,
                                              AV34TFSaldoContrato_Codigo ,
                                              AV35TFSaldoContrato_Codigo_To ,
                                              AV38TFContrato_Codigo ,
                                              AV39TFContrato_Codigo_To ,
                                              AV42TFSaldoContrato_VigenciaInicio ,
                                              AV43TFSaldoContrato_VigenciaInicio_To ,
                                              AV48TFSaldoContrato_VigenciaFim ,
                                              AV49TFSaldoContrato_VigenciaFim_To ,
                                              AV54TFSaldoContrato_Credito ,
                                              AV55TFSaldoContrato_Credito_To ,
                                              AV58TFSaldoContrato_Reservado ,
                                              AV59TFSaldoContrato_Reservado_To ,
                                              AV62TFSaldoContrato_Executado ,
                                              AV63TFSaldoContrato_Executado_To ,
                                              AV66TFSaldoContrato_Saldo ,
                                              AV67TFSaldoContrato_Saldo_To ,
                                              A1571SaldoContrato_VigenciaInicio ,
                                              A1561SaldoContrato_Codigo ,
                                              A74Contrato_Codigo ,
                                              A1572SaldoContrato_VigenciaFim ,
                                              A1573SaldoContrato_Credito ,
                                              A1574SaldoContrato_Reservado ,
                                              A1575SaldoContrato_Executado ,
                                              A1576SaldoContrato_Saldo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00M53 */
         pr_default.execute(1, new Object[] {AV16SaldoContrato_VigenciaInicio1, AV17SaldoContrato_VigenciaInicio_To1, AV20SaldoContrato_VigenciaInicio2, AV21SaldoContrato_VigenciaInicio_To2, AV24SaldoContrato_VigenciaInicio3, AV25SaldoContrato_VigenciaInicio_To3, AV34TFSaldoContrato_Codigo, AV35TFSaldoContrato_Codigo_To, AV38TFContrato_Codigo, AV39TFContrato_Codigo_To, AV42TFSaldoContrato_VigenciaInicio, AV43TFSaldoContrato_VigenciaInicio_To, AV48TFSaldoContrato_VigenciaFim, AV49TFSaldoContrato_VigenciaFim_To, AV54TFSaldoContrato_Credito, AV55TFSaldoContrato_Credito_To, AV58TFSaldoContrato_Reservado, AV59TFSaldoContrato_Reservado_To, AV62TFSaldoContrato_Executado, AV63TFSaldoContrato_Executado_To, AV66TFSaldoContrato_Saldo, AV67TFSaldoContrato_Saldo_To});
         GRID_nRecordCount = H00M53_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16SaldoContrato_VigenciaInicio1, AV17SaldoContrato_VigenciaInicio_To1, AV19DynamicFiltersSelector2, AV20SaldoContrato_VigenciaInicio2, AV21SaldoContrato_VigenciaInicio_To2, AV23DynamicFiltersSelector3, AV24SaldoContrato_VigenciaInicio3, AV25SaldoContrato_VigenciaInicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFSaldoContrato_Codigo, AV35TFSaldoContrato_Codigo_To, AV38TFContrato_Codigo, AV39TFContrato_Codigo_To, AV42TFSaldoContrato_VigenciaInicio, AV43TFSaldoContrato_VigenciaInicio_To, AV48TFSaldoContrato_VigenciaFim, AV49TFSaldoContrato_VigenciaFim_To, AV54TFSaldoContrato_Credito, AV55TFSaldoContrato_Credito_To, AV58TFSaldoContrato_Reservado, AV59TFSaldoContrato_Reservado_To, AV62TFSaldoContrato_Executado, AV63TFSaldoContrato_Executado_To, AV66TFSaldoContrato_Saldo, AV67TFSaldoContrato_Saldo_To, AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace, AV40ddo_Contrato_CodigoTitleControlIdToReplace, AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace, AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace, AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace, AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace, AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace, AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace, AV77Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1561SaldoContrato_Codigo, A1560NotaEmpenho_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16SaldoContrato_VigenciaInicio1, AV17SaldoContrato_VigenciaInicio_To1, AV19DynamicFiltersSelector2, AV20SaldoContrato_VigenciaInicio2, AV21SaldoContrato_VigenciaInicio_To2, AV23DynamicFiltersSelector3, AV24SaldoContrato_VigenciaInicio3, AV25SaldoContrato_VigenciaInicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFSaldoContrato_Codigo, AV35TFSaldoContrato_Codigo_To, AV38TFContrato_Codigo, AV39TFContrato_Codigo_To, AV42TFSaldoContrato_VigenciaInicio, AV43TFSaldoContrato_VigenciaInicio_To, AV48TFSaldoContrato_VigenciaFim, AV49TFSaldoContrato_VigenciaFim_To, AV54TFSaldoContrato_Credito, AV55TFSaldoContrato_Credito_To, AV58TFSaldoContrato_Reservado, AV59TFSaldoContrato_Reservado_To, AV62TFSaldoContrato_Executado, AV63TFSaldoContrato_Executado_To, AV66TFSaldoContrato_Saldo, AV67TFSaldoContrato_Saldo_To, AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace, AV40ddo_Contrato_CodigoTitleControlIdToReplace, AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace, AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace, AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace, AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace, AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace, AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace, AV77Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1561SaldoContrato_Codigo, A1560NotaEmpenho_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16SaldoContrato_VigenciaInicio1, AV17SaldoContrato_VigenciaInicio_To1, AV19DynamicFiltersSelector2, AV20SaldoContrato_VigenciaInicio2, AV21SaldoContrato_VigenciaInicio_To2, AV23DynamicFiltersSelector3, AV24SaldoContrato_VigenciaInicio3, AV25SaldoContrato_VigenciaInicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFSaldoContrato_Codigo, AV35TFSaldoContrato_Codigo_To, AV38TFContrato_Codigo, AV39TFContrato_Codigo_To, AV42TFSaldoContrato_VigenciaInicio, AV43TFSaldoContrato_VigenciaInicio_To, AV48TFSaldoContrato_VigenciaFim, AV49TFSaldoContrato_VigenciaFim_To, AV54TFSaldoContrato_Credito, AV55TFSaldoContrato_Credito_To, AV58TFSaldoContrato_Reservado, AV59TFSaldoContrato_Reservado_To, AV62TFSaldoContrato_Executado, AV63TFSaldoContrato_Executado_To, AV66TFSaldoContrato_Saldo, AV67TFSaldoContrato_Saldo_To, AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace, AV40ddo_Contrato_CodigoTitleControlIdToReplace, AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace, AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace, AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace, AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace, AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace, AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace, AV77Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1561SaldoContrato_Codigo, A1560NotaEmpenho_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16SaldoContrato_VigenciaInicio1, AV17SaldoContrato_VigenciaInicio_To1, AV19DynamicFiltersSelector2, AV20SaldoContrato_VigenciaInicio2, AV21SaldoContrato_VigenciaInicio_To2, AV23DynamicFiltersSelector3, AV24SaldoContrato_VigenciaInicio3, AV25SaldoContrato_VigenciaInicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFSaldoContrato_Codigo, AV35TFSaldoContrato_Codigo_To, AV38TFContrato_Codigo, AV39TFContrato_Codigo_To, AV42TFSaldoContrato_VigenciaInicio, AV43TFSaldoContrato_VigenciaInicio_To, AV48TFSaldoContrato_VigenciaFim, AV49TFSaldoContrato_VigenciaFim_To, AV54TFSaldoContrato_Credito, AV55TFSaldoContrato_Credito_To, AV58TFSaldoContrato_Reservado, AV59TFSaldoContrato_Reservado_To, AV62TFSaldoContrato_Executado, AV63TFSaldoContrato_Executado_To, AV66TFSaldoContrato_Saldo, AV67TFSaldoContrato_Saldo_To, AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace, AV40ddo_Contrato_CodigoTitleControlIdToReplace, AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace, AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace, AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace, AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace, AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace, AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace, AV77Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1561SaldoContrato_Codigo, A1560NotaEmpenho_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16SaldoContrato_VigenciaInicio1, AV17SaldoContrato_VigenciaInicio_To1, AV19DynamicFiltersSelector2, AV20SaldoContrato_VigenciaInicio2, AV21SaldoContrato_VigenciaInicio_To2, AV23DynamicFiltersSelector3, AV24SaldoContrato_VigenciaInicio3, AV25SaldoContrato_VigenciaInicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFSaldoContrato_Codigo, AV35TFSaldoContrato_Codigo_To, AV38TFContrato_Codigo, AV39TFContrato_Codigo_To, AV42TFSaldoContrato_VigenciaInicio, AV43TFSaldoContrato_VigenciaInicio_To, AV48TFSaldoContrato_VigenciaFim, AV49TFSaldoContrato_VigenciaFim_To, AV54TFSaldoContrato_Credito, AV55TFSaldoContrato_Credito_To, AV58TFSaldoContrato_Reservado, AV59TFSaldoContrato_Reservado_To, AV62TFSaldoContrato_Executado, AV63TFSaldoContrato_Executado_To, AV66TFSaldoContrato_Saldo, AV67TFSaldoContrato_Saldo_To, AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace, AV40ddo_Contrato_CodigoTitleControlIdToReplace, AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace, AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace, AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace, AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace, AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace, AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace, AV77Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1561SaldoContrato_Codigo, A1560NotaEmpenho_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPM50( )
      {
         /* Before Start, stand alone formulas. */
         AV77Pgmname = "WWSaldoContrato";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E31M52 */
         E31M52 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV69DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vSALDOCONTRATO_CODIGOTITLEFILTERDATA"), AV33SaldoContrato_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATO_CODIGOTITLEFILTERDATA"), AV37Contrato_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSALDOCONTRATO_VIGENCIAINICIOTITLEFILTERDATA"), AV41SaldoContrato_VigenciaInicioTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSALDOCONTRATO_VIGENCIAFIMTITLEFILTERDATA"), AV47SaldoContrato_VigenciaFimTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSALDOCONTRATO_CREDITOTITLEFILTERDATA"), AV53SaldoContrato_CreditoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSALDOCONTRATO_RESERVADOTITLEFILTERDATA"), AV57SaldoContrato_ReservadoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSALDOCONTRATO_EXECUTADOTITLEFILTERDATA"), AV61SaldoContrato_ExecutadoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSALDOCONTRATO_SALDOTITLEFILTERDATA"), AV65SaldoContrato_SaldoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( context.localUtil.VCDateTime( cgiGet( edtavSaldocontrato_vigenciainicio1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Saldo Contrato_Vigencia Inicio1"}), 1, "vSALDOCONTRATO_VIGENCIAINICIO1");
               GX_FocusControl = edtavSaldocontrato_vigenciainicio1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV16SaldoContrato_VigenciaInicio1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16SaldoContrato_VigenciaInicio1", context.localUtil.Format(AV16SaldoContrato_VigenciaInicio1, "99/99/99"));
            }
            else
            {
               AV16SaldoContrato_VigenciaInicio1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavSaldocontrato_vigenciainicio1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16SaldoContrato_VigenciaInicio1", context.localUtil.Format(AV16SaldoContrato_VigenciaInicio1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavSaldocontrato_vigenciainicio_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Saldo Contrato_Vigencia Inicio_To1"}), 1, "vSALDOCONTRATO_VIGENCIAINICIO_TO1");
               GX_FocusControl = edtavSaldocontrato_vigenciainicio_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17SaldoContrato_VigenciaInicio_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17SaldoContrato_VigenciaInicio_To1", context.localUtil.Format(AV17SaldoContrato_VigenciaInicio_To1, "99/99/99"));
            }
            else
            {
               AV17SaldoContrato_VigenciaInicio_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavSaldocontrato_vigenciainicio_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17SaldoContrato_VigenciaInicio_To1", context.localUtil.Format(AV17SaldoContrato_VigenciaInicio_To1, "99/99/99"));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            if ( context.localUtil.VCDateTime( cgiGet( edtavSaldocontrato_vigenciainicio2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Saldo Contrato_Vigencia Inicio2"}), 1, "vSALDOCONTRATO_VIGENCIAINICIO2");
               GX_FocusControl = edtavSaldocontrato_vigenciainicio2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20SaldoContrato_VigenciaInicio2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20SaldoContrato_VigenciaInicio2", context.localUtil.Format(AV20SaldoContrato_VigenciaInicio2, "99/99/99"));
            }
            else
            {
               AV20SaldoContrato_VigenciaInicio2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavSaldocontrato_vigenciainicio2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20SaldoContrato_VigenciaInicio2", context.localUtil.Format(AV20SaldoContrato_VigenciaInicio2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavSaldocontrato_vigenciainicio_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Saldo Contrato_Vigencia Inicio_To2"}), 1, "vSALDOCONTRATO_VIGENCIAINICIO_TO2");
               GX_FocusControl = edtavSaldocontrato_vigenciainicio_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21SaldoContrato_VigenciaInicio_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21SaldoContrato_VigenciaInicio_To2", context.localUtil.Format(AV21SaldoContrato_VigenciaInicio_To2, "99/99/99"));
            }
            else
            {
               AV21SaldoContrato_VigenciaInicio_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavSaldocontrato_vigenciainicio_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21SaldoContrato_VigenciaInicio_To2", context.localUtil.Format(AV21SaldoContrato_VigenciaInicio_To2, "99/99/99"));
            }
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            if ( context.localUtil.VCDateTime( cgiGet( edtavSaldocontrato_vigenciainicio3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Saldo Contrato_Vigencia Inicio3"}), 1, "vSALDOCONTRATO_VIGENCIAINICIO3");
               GX_FocusControl = edtavSaldocontrato_vigenciainicio3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24SaldoContrato_VigenciaInicio3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24SaldoContrato_VigenciaInicio3", context.localUtil.Format(AV24SaldoContrato_VigenciaInicio3, "99/99/99"));
            }
            else
            {
               AV24SaldoContrato_VigenciaInicio3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavSaldocontrato_vigenciainicio3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24SaldoContrato_VigenciaInicio3", context.localUtil.Format(AV24SaldoContrato_VigenciaInicio3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavSaldocontrato_vigenciainicio_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Saldo Contrato_Vigencia Inicio_To3"}), 1, "vSALDOCONTRATO_VIGENCIAINICIO_TO3");
               GX_FocusControl = edtavSaldocontrato_vigenciainicio_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV25SaldoContrato_VigenciaInicio_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25SaldoContrato_VigenciaInicio_To3", context.localUtil.Format(AV25SaldoContrato_VigenciaInicio_To3, "99/99/99"));
            }
            else
            {
               AV25SaldoContrato_VigenciaInicio_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavSaldocontrato_vigenciainicio_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25SaldoContrato_VigenciaInicio_To3", context.localUtil.Format(AV25SaldoContrato_VigenciaInicio_To3, "99/99/99"));
            }
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSALDOCONTRATO_CODIGO");
               GX_FocusControl = edtavTfsaldocontrato_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV34TFSaldoContrato_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFSaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34TFSaldoContrato_Codigo), 6, 0)));
            }
            else
            {
               AV34TFSaldoContrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFSaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34TFSaldoContrato_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSALDOCONTRATO_CODIGO_TO");
               GX_FocusControl = edtavTfsaldocontrato_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35TFSaldoContrato_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFSaldoContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFSaldoContrato_Codigo_To), 6, 0)));
            }
            else
            {
               AV35TFSaldoContrato_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFSaldoContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFSaldoContrato_Codigo_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATO_CODIGO");
               GX_FocusControl = edtavTfcontrato_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38TFContrato_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFContrato_Codigo), 6, 0)));
            }
            else
            {
               AV38TFContrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFContrato_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATO_CODIGO_TO");
               GX_FocusControl = edtavTfcontrato_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39TFContrato_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFContrato_Codigo_To), 6, 0)));
            }
            else
            {
               AV39TFContrato_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFContrato_Codigo_To), 6, 0)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfsaldocontrato_vigenciainicio_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFSaldo Contrato_Vigencia Inicio"}), 1, "vTFSALDOCONTRATO_VIGENCIAINICIO");
               GX_FocusControl = edtavTfsaldocontrato_vigenciainicio_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV42TFSaldoContrato_VigenciaInicio = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFSaldoContrato_VigenciaInicio", context.localUtil.Format(AV42TFSaldoContrato_VigenciaInicio, "99/99/99"));
            }
            else
            {
               AV42TFSaldoContrato_VigenciaInicio = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfsaldocontrato_vigenciainicio_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFSaldoContrato_VigenciaInicio", context.localUtil.Format(AV42TFSaldoContrato_VigenciaInicio, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfsaldocontrato_vigenciainicio_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFSaldo Contrato_Vigencia Inicio_To"}), 1, "vTFSALDOCONTRATO_VIGENCIAINICIO_TO");
               GX_FocusControl = edtavTfsaldocontrato_vigenciainicio_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV43TFSaldoContrato_VigenciaInicio_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFSaldoContrato_VigenciaInicio_To", context.localUtil.Format(AV43TFSaldoContrato_VigenciaInicio_To, "99/99/99"));
            }
            else
            {
               AV43TFSaldoContrato_VigenciaInicio_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfsaldocontrato_vigenciainicio_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFSaldoContrato_VigenciaInicio_To", context.localUtil.Format(AV43TFSaldoContrato_VigenciaInicio_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_saldocontrato_vigenciainicioauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Saldo Contrato_Vigencia Inicio Aux Date"}), 1, "vDDO_SALDOCONTRATO_VIGENCIAINICIOAUXDATE");
               GX_FocusControl = edtavDdo_saldocontrato_vigenciainicioauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44DDO_SaldoContrato_VigenciaInicioAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44DDO_SaldoContrato_VigenciaInicioAuxDate", context.localUtil.Format(AV44DDO_SaldoContrato_VigenciaInicioAuxDate, "99/99/99"));
            }
            else
            {
               AV44DDO_SaldoContrato_VigenciaInicioAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_saldocontrato_vigenciainicioauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44DDO_SaldoContrato_VigenciaInicioAuxDate", context.localUtil.Format(AV44DDO_SaldoContrato_VigenciaInicioAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_saldocontrato_vigenciainicioauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Saldo Contrato_Vigencia Inicio Aux Date To"}), 1, "vDDO_SALDOCONTRATO_VIGENCIAINICIOAUXDATETO");
               GX_FocusControl = edtavDdo_saldocontrato_vigenciainicioauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV45DDO_SaldoContrato_VigenciaInicioAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45DDO_SaldoContrato_VigenciaInicioAuxDateTo", context.localUtil.Format(AV45DDO_SaldoContrato_VigenciaInicioAuxDateTo, "99/99/99"));
            }
            else
            {
               AV45DDO_SaldoContrato_VigenciaInicioAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_saldocontrato_vigenciainicioauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45DDO_SaldoContrato_VigenciaInicioAuxDateTo", context.localUtil.Format(AV45DDO_SaldoContrato_VigenciaInicioAuxDateTo, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfsaldocontrato_vigenciafim_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFSaldo Contrato_Vigencia Fim"}), 1, "vTFSALDOCONTRATO_VIGENCIAFIM");
               GX_FocusControl = edtavTfsaldocontrato_vigenciafim_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48TFSaldoContrato_VigenciaFim = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFSaldoContrato_VigenciaFim", context.localUtil.Format(AV48TFSaldoContrato_VigenciaFim, "99/99/99"));
            }
            else
            {
               AV48TFSaldoContrato_VigenciaFim = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfsaldocontrato_vigenciafim_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFSaldoContrato_VigenciaFim", context.localUtil.Format(AV48TFSaldoContrato_VigenciaFim, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfsaldocontrato_vigenciafim_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFSaldo Contrato_Vigencia Fim_To"}), 1, "vTFSALDOCONTRATO_VIGENCIAFIM_TO");
               GX_FocusControl = edtavTfsaldocontrato_vigenciafim_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV49TFSaldoContrato_VigenciaFim_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFSaldoContrato_VigenciaFim_To", context.localUtil.Format(AV49TFSaldoContrato_VigenciaFim_To, "99/99/99"));
            }
            else
            {
               AV49TFSaldoContrato_VigenciaFim_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfsaldocontrato_vigenciafim_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFSaldoContrato_VigenciaFim_To", context.localUtil.Format(AV49TFSaldoContrato_VigenciaFim_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_saldocontrato_vigenciafimauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Saldo Contrato_Vigencia Fim Aux Date"}), 1, "vDDO_SALDOCONTRATO_VIGENCIAFIMAUXDATE");
               GX_FocusControl = edtavDdo_saldocontrato_vigenciafimauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV50DDO_SaldoContrato_VigenciaFimAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50DDO_SaldoContrato_VigenciaFimAuxDate", context.localUtil.Format(AV50DDO_SaldoContrato_VigenciaFimAuxDate, "99/99/99"));
            }
            else
            {
               AV50DDO_SaldoContrato_VigenciaFimAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_saldocontrato_vigenciafimauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50DDO_SaldoContrato_VigenciaFimAuxDate", context.localUtil.Format(AV50DDO_SaldoContrato_VigenciaFimAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_saldocontrato_vigenciafimauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Saldo Contrato_Vigencia Fim Aux Date To"}), 1, "vDDO_SALDOCONTRATO_VIGENCIAFIMAUXDATETO");
               GX_FocusControl = edtavDdo_saldocontrato_vigenciafimauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV51DDO_SaldoContrato_VigenciaFimAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51DDO_SaldoContrato_VigenciaFimAuxDateTo", context.localUtil.Format(AV51DDO_SaldoContrato_VigenciaFimAuxDateTo, "99/99/99"));
            }
            else
            {
               AV51DDO_SaldoContrato_VigenciaFimAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_saldocontrato_vigenciafimauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51DDO_SaldoContrato_VigenciaFimAuxDateTo", context.localUtil.Format(AV51DDO_SaldoContrato_VigenciaFimAuxDateTo, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_credito_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_credito_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSALDOCONTRATO_CREDITO");
               GX_FocusControl = edtavTfsaldocontrato_credito_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV54TFSaldoContrato_Credito = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFSaldoContrato_Credito", StringUtil.LTrim( StringUtil.Str( AV54TFSaldoContrato_Credito, 18, 5)));
            }
            else
            {
               AV54TFSaldoContrato_Credito = context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_credito_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFSaldoContrato_Credito", StringUtil.LTrim( StringUtil.Str( AV54TFSaldoContrato_Credito, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_credito_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_credito_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSALDOCONTRATO_CREDITO_TO");
               GX_FocusControl = edtavTfsaldocontrato_credito_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV55TFSaldoContrato_Credito_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFSaldoContrato_Credito_To", StringUtil.LTrim( StringUtil.Str( AV55TFSaldoContrato_Credito_To, 18, 5)));
            }
            else
            {
               AV55TFSaldoContrato_Credito_To = context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_credito_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFSaldoContrato_Credito_To", StringUtil.LTrim( StringUtil.Str( AV55TFSaldoContrato_Credito_To, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_reservado_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_reservado_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSALDOCONTRATO_RESERVADO");
               GX_FocusControl = edtavTfsaldocontrato_reservado_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV58TFSaldoContrato_Reservado = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFSaldoContrato_Reservado", StringUtil.LTrim( StringUtil.Str( AV58TFSaldoContrato_Reservado, 18, 5)));
            }
            else
            {
               AV58TFSaldoContrato_Reservado = context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_reservado_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFSaldoContrato_Reservado", StringUtil.LTrim( StringUtil.Str( AV58TFSaldoContrato_Reservado, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_reservado_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_reservado_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSALDOCONTRATO_RESERVADO_TO");
               GX_FocusControl = edtavTfsaldocontrato_reservado_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV59TFSaldoContrato_Reservado_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFSaldoContrato_Reservado_To", StringUtil.LTrim( StringUtil.Str( AV59TFSaldoContrato_Reservado_To, 18, 5)));
            }
            else
            {
               AV59TFSaldoContrato_Reservado_To = context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_reservado_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFSaldoContrato_Reservado_To", StringUtil.LTrim( StringUtil.Str( AV59TFSaldoContrato_Reservado_To, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_executado_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_executado_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSALDOCONTRATO_EXECUTADO");
               GX_FocusControl = edtavTfsaldocontrato_executado_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV62TFSaldoContrato_Executado = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFSaldoContrato_Executado", StringUtil.LTrim( StringUtil.Str( AV62TFSaldoContrato_Executado, 18, 5)));
            }
            else
            {
               AV62TFSaldoContrato_Executado = context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_executado_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFSaldoContrato_Executado", StringUtil.LTrim( StringUtil.Str( AV62TFSaldoContrato_Executado, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_executado_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_executado_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSALDOCONTRATO_EXECUTADO_TO");
               GX_FocusControl = edtavTfsaldocontrato_executado_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV63TFSaldoContrato_Executado_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFSaldoContrato_Executado_To", StringUtil.LTrim( StringUtil.Str( AV63TFSaldoContrato_Executado_To, 18, 5)));
            }
            else
            {
               AV63TFSaldoContrato_Executado_To = context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_executado_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFSaldoContrato_Executado_To", StringUtil.LTrim( StringUtil.Str( AV63TFSaldoContrato_Executado_To, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_saldo_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_saldo_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSALDOCONTRATO_SALDO");
               GX_FocusControl = edtavTfsaldocontrato_saldo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV66TFSaldoContrato_Saldo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFSaldoContrato_Saldo", StringUtil.LTrim( StringUtil.Str( AV66TFSaldoContrato_Saldo, 18, 5)));
            }
            else
            {
               AV66TFSaldoContrato_Saldo = context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_saldo_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFSaldoContrato_Saldo", StringUtil.LTrim( StringUtil.Str( AV66TFSaldoContrato_Saldo, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_saldo_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_saldo_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSALDOCONTRATO_SALDO_TO");
               GX_FocusControl = edtavTfsaldocontrato_saldo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV67TFSaldoContrato_Saldo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFSaldoContrato_Saldo_To", StringUtil.LTrim( StringUtil.Str( AV67TFSaldoContrato_Saldo_To, 18, 5)));
            }
            else
            {
               AV67TFSaldoContrato_Saldo_To = context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_saldo_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFSaldoContrato_Saldo_To", StringUtil.LTrim( StringUtil.Str( AV67TFSaldoContrato_Saldo_To, 18, 5)));
            }
            AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace", AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace);
            AV40ddo_Contrato_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_Contrato_CodigoTitleControlIdToReplace", AV40ddo_Contrato_CodigoTitleControlIdToReplace);
            AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace = cgiGet( edtavDdo_saldocontrato_vigenciainiciotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace", AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace);
            AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace = cgiGet( edtavDdo_saldocontrato_vigenciafimtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace", AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace);
            AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace = cgiGet( edtavDdo_saldocontrato_creditotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace", AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace);
            AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace = cgiGet( edtavDdo_saldocontrato_reservadotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace", AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace);
            AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace = cgiGet( edtavDdo_saldocontrato_executadotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace", AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace);
            AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace = cgiGet( edtavDdo_saldocontrato_saldotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace", AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_94 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_94"), ",", "."));
            AV71GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV72GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_saldocontrato_codigo_Caption = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Caption");
            Ddo_saldocontrato_codigo_Tooltip = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Tooltip");
            Ddo_saldocontrato_codigo_Cls = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Cls");
            Ddo_saldocontrato_codigo_Filteredtext_set = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Filteredtext_set");
            Ddo_saldocontrato_codigo_Filteredtextto_set = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Filteredtextto_set");
            Ddo_saldocontrato_codigo_Dropdownoptionstype = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Dropdownoptionstype");
            Ddo_saldocontrato_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Titlecontrolidtoreplace");
            Ddo_saldocontrato_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_CODIGO_Includesortasc"));
            Ddo_saldocontrato_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_CODIGO_Includesortdsc"));
            Ddo_saldocontrato_codigo_Sortedstatus = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Sortedstatus");
            Ddo_saldocontrato_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_CODIGO_Includefilter"));
            Ddo_saldocontrato_codigo_Filtertype = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Filtertype");
            Ddo_saldocontrato_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_CODIGO_Filterisrange"));
            Ddo_saldocontrato_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_CODIGO_Includedatalist"));
            Ddo_saldocontrato_codigo_Sortasc = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Sortasc");
            Ddo_saldocontrato_codigo_Sortdsc = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Sortdsc");
            Ddo_saldocontrato_codigo_Cleanfilter = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Cleanfilter");
            Ddo_saldocontrato_codigo_Rangefilterfrom = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Rangefilterfrom");
            Ddo_saldocontrato_codigo_Rangefilterto = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Rangefilterto");
            Ddo_saldocontrato_codigo_Searchbuttontext = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Searchbuttontext");
            Ddo_contrato_codigo_Caption = cgiGet( "DDO_CONTRATO_CODIGO_Caption");
            Ddo_contrato_codigo_Tooltip = cgiGet( "DDO_CONTRATO_CODIGO_Tooltip");
            Ddo_contrato_codigo_Cls = cgiGet( "DDO_CONTRATO_CODIGO_Cls");
            Ddo_contrato_codigo_Filteredtext_set = cgiGet( "DDO_CONTRATO_CODIGO_Filteredtext_set");
            Ddo_contrato_codigo_Filteredtextto_set = cgiGet( "DDO_CONTRATO_CODIGO_Filteredtextto_set");
            Ddo_contrato_codigo_Dropdownoptionstype = cgiGet( "DDO_CONTRATO_CODIGO_Dropdownoptionstype");
            Ddo_contrato_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATO_CODIGO_Titlecontrolidtoreplace");
            Ddo_contrato_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Includesortasc"));
            Ddo_contrato_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Includesortdsc"));
            Ddo_contrato_codigo_Sortedstatus = cgiGet( "DDO_CONTRATO_CODIGO_Sortedstatus");
            Ddo_contrato_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Includefilter"));
            Ddo_contrato_codigo_Filtertype = cgiGet( "DDO_CONTRATO_CODIGO_Filtertype");
            Ddo_contrato_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Filterisrange"));
            Ddo_contrato_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Includedatalist"));
            Ddo_contrato_codigo_Sortasc = cgiGet( "DDO_CONTRATO_CODIGO_Sortasc");
            Ddo_contrato_codigo_Sortdsc = cgiGet( "DDO_CONTRATO_CODIGO_Sortdsc");
            Ddo_contrato_codigo_Cleanfilter = cgiGet( "DDO_CONTRATO_CODIGO_Cleanfilter");
            Ddo_contrato_codigo_Rangefilterfrom = cgiGet( "DDO_CONTRATO_CODIGO_Rangefilterfrom");
            Ddo_contrato_codigo_Rangefilterto = cgiGet( "DDO_CONTRATO_CODIGO_Rangefilterto");
            Ddo_contrato_codigo_Searchbuttontext = cgiGet( "DDO_CONTRATO_CODIGO_Searchbuttontext");
            Ddo_saldocontrato_vigenciainicio_Caption = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAINICIO_Caption");
            Ddo_saldocontrato_vigenciainicio_Tooltip = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAINICIO_Tooltip");
            Ddo_saldocontrato_vigenciainicio_Cls = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAINICIO_Cls");
            Ddo_saldocontrato_vigenciainicio_Filteredtext_set = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAINICIO_Filteredtext_set");
            Ddo_saldocontrato_vigenciainicio_Filteredtextto_set = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAINICIO_Filteredtextto_set");
            Ddo_saldocontrato_vigenciainicio_Dropdownoptionstype = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAINICIO_Dropdownoptionstype");
            Ddo_saldocontrato_vigenciainicio_Titlecontrolidtoreplace = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAINICIO_Titlecontrolidtoreplace");
            Ddo_saldocontrato_vigenciainicio_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_VIGENCIAINICIO_Includesortasc"));
            Ddo_saldocontrato_vigenciainicio_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_VIGENCIAINICIO_Includesortdsc"));
            Ddo_saldocontrato_vigenciainicio_Sortedstatus = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAINICIO_Sortedstatus");
            Ddo_saldocontrato_vigenciainicio_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_VIGENCIAINICIO_Includefilter"));
            Ddo_saldocontrato_vigenciainicio_Filtertype = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAINICIO_Filtertype");
            Ddo_saldocontrato_vigenciainicio_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_VIGENCIAINICIO_Filterisrange"));
            Ddo_saldocontrato_vigenciainicio_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_VIGENCIAINICIO_Includedatalist"));
            Ddo_saldocontrato_vigenciainicio_Sortasc = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAINICIO_Sortasc");
            Ddo_saldocontrato_vigenciainicio_Sortdsc = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAINICIO_Sortdsc");
            Ddo_saldocontrato_vigenciainicio_Cleanfilter = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAINICIO_Cleanfilter");
            Ddo_saldocontrato_vigenciainicio_Rangefilterfrom = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAINICIO_Rangefilterfrom");
            Ddo_saldocontrato_vigenciainicio_Rangefilterto = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAINICIO_Rangefilterto");
            Ddo_saldocontrato_vigenciainicio_Searchbuttontext = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAINICIO_Searchbuttontext");
            Ddo_saldocontrato_vigenciafim_Caption = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAFIM_Caption");
            Ddo_saldocontrato_vigenciafim_Tooltip = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAFIM_Tooltip");
            Ddo_saldocontrato_vigenciafim_Cls = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAFIM_Cls");
            Ddo_saldocontrato_vigenciafim_Filteredtext_set = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAFIM_Filteredtext_set");
            Ddo_saldocontrato_vigenciafim_Filteredtextto_set = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAFIM_Filteredtextto_set");
            Ddo_saldocontrato_vigenciafim_Dropdownoptionstype = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAFIM_Dropdownoptionstype");
            Ddo_saldocontrato_vigenciafim_Titlecontrolidtoreplace = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAFIM_Titlecontrolidtoreplace");
            Ddo_saldocontrato_vigenciafim_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_VIGENCIAFIM_Includesortasc"));
            Ddo_saldocontrato_vigenciafim_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_VIGENCIAFIM_Includesortdsc"));
            Ddo_saldocontrato_vigenciafim_Sortedstatus = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAFIM_Sortedstatus");
            Ddo_saldocontrato_vigenciafim_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_VIGENCIAFIM_Includefilter"));
            Ddo_saldocontrato_vigenciafim_Filtertype = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAFIM_Filtertype");
            Ddo_saldocontrato_vigenciafim_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_VIGENCIAFIM_Filterisrange"));
            Ddo_saldocontrato_vigenciafim_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_VIGENCIAFIM_Includedatalist"));
            Ddo_saldocontrato_vigenciafim_Sortasc = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAFIM_Sortasc");
            Ddo_saldocontrato_vigenciafim_Sortdsc = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAFIM_Sortdsc");
            Ddo_saldocontrato_vigenciafim_Cleanfilter = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAFIM_Cleanfilter");
            Ddo_saldocontrato_vigenciafim_Rangefilterfrom = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAFIM_Rangefilterfrom");
            Ddo_saldocontrato_vigenciafim_Rangefilterto = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAFIM_Rangefilterto");
            Ddo_saldocontrato_vigenciafim_Searchbuttontext = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAFIM_Searchbuttontext");
            Ddo_saldocontrato_credito_Caption = cgiGet( "DDO_SALDOCONTRATO_CREDITO_Caption");
            Ddo_saldocontrato_credito_Tooltip = cgiGet( "DDO_SALDOCONTRATO_CREDITO_Tooltip");
            Ddo_saldocontrato_credito_Cls = cgiGet( "DDO_SALDOCONTRATO_CREDITO_Cls");
            Ddo_saldocontrato_credito_Filteredtext_set = cgiGet( "DDO_SALDOCONTRATO_CREDITO_Filteredtext_set");
            Ddo_saldocontrato_credito_Filteredtextto_set = cgiGet( "DDO_SALDOCONTRATO_CREDITO_Filteredtextto_set");
            Ddo_saldocontrato_credito_Dropdownoptionstype = cgiGet( "DDO_SALDOCONTRATO_CREDITO_Dropdownoptionstype");
            Ddo_saldocontrato_credito_Titlecontrolidtoreplace = cgiGet( "DDO_SALDOCONTRATO_CREDITO_Titlecontrolidtoreplace");
            Ddo_saldocontrato_credito_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_CREDITO_Includesortasc"));
            Ddo_saldocontrato_credito_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_CREDITO_Includesortdsc"));
            Ddo_saldocontrato_credito_Sortedstatus = cgiGet( "DDO_SALDOCONTRATO_CREDITO_Sortedstatus");
            Ddo_saldocontrato_credito_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_CREDITO_Includefilter"));
            Ddo_saldocontrato_credito_Filtertype = cgiGet( "DDO_SALDOCONTRATO_CREDITO_Filtertype");
            Ddo_saldocontrato_credito_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_CREDITO_Filterisrange"));
            Ddo_saldocontrato_credito_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_CREDITO_Includedatalist"));
            Ddo_saldocontrato_credito_Sortasc = cgiGet( "DDO_SALDOCONTRATO_CREDITO_Sortasc");
            Ddo_saldocontrato_credito_Sortdsc = cgiGet( "DDO_SALDOCONTRATO_CREDITO_Sortdsc");
            Ddo_saldocontrato_credito_Cleanfilter = cgiGet( "DDO_SALDOCONTRATO_CREDITO_Cleanfilter");
            Ddo_saldocontrato_credito_Rangefilterfrom = cgiGet( "DDO_SALDOCONTRATO_CREDITO_Rangefilterfrom");
            Ddo_saldocontrato_credito_Rangefilterto = cgiGet( "DDO_SALDOCONTRATO_CREDITO_Rangefilterto");
            Ddo_saldocontrato_credito_Searchbuttontext = cgiGet( "DDO_SALDOCONTRATO_CREDITO_Searchbuttontext");
            Ddo_saldocontrato_reservado_Caption = cgiGet( "DDO_SALDOCONTRATO_RESERVADO_Caption");
            Ddo_saldocontrato_reservado_Tooltip = cgiGet( "DDO_SALDOCONTRATO_RESERVADO_Tooltip");
            Ddo_saldocontrato_reservado_Cls = cgiGet( "DDO_SALDOCONTRATO_RESERVADO_Cls");
            Ddo_saldocontrato_reservado_Filteredtext_set = cgiGet( "DDO_SALDOCONTRATO_RESERVADO_Filteredtext_set");
            Ddo_saldocontrato_reservado_Filteredtextto_set = cgiGet( "DDO_SALDOCONTRATO_RESERVADO_Filteredtextto_set");
            Ddo_saldocontrato_reservado_Dropdownoptionstype = cgiGet( "DDO_SALDOCONTRATO_RESERVADO_Dropdownoptionstype");
            Ddo_saldocontrato_reservado_Titlecontrolidtoreplace = cgiGet( "DDO_SALDOCONTRATO_RESERVADO_Titlecontrolidtoreplace");
            Ddo_saldocontrato_reservado_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_RESERVADO_Includesortasc"));
            Ddo_saldocontrato_reservado_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_RESERVADO_Includesortdsc"));
            Ddo_saldocontrato_reservado_Sortedstatus = cgiGet( "DDO_SALDOCONTRATO_RESERVADO_Sortedstatus");
            Ddo_saldocontrato_reservado_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_RESERVADO_Includefilter"));
            Ddo_saldocontrato_reservado_Filtertype = cgiGet( "DDO_SALDOCONTRATO_RESERVADO_Filtertype");
            Ddo_saldocontrato_reservado_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_RESERVADO_Filterisrange"));
            Ddo_saldocontrato_reservado_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_RESERVADO_Includedatalist"));
            Ddo_saldocontrato_reservado_Sortasc = cgiGet( "DDO_SALDOCONTRATO_RESERVADO_Sortasc");
            Ddo_saldocontrato_reservado_Sortdsc = cgiGet( "DDO_SALDOCONTRATO_RESERVADO_Sortdsc");
            Ddo_saldocontrato_reservado_Cleanfilter = cgiGet( "DDO_SALDOCONTRATO_RESERVADO_Cleanfilter");
            Ddo_saldocontrato_reservado_Rangefilterfrom = cgiGet( "DDO_SALDOCONTRATO_RESERVADO_Rangefilterfrom");
            Ddo_saldocontrato_reservado_Rangefilterto = cgiGet( "DDO_SALDOCONTRATO_RESERVADO_Rangefilterto");
            Ddo_saldocontrato_reservado_Searchbuttontext = cgiGet( "DDO_SALDOCONTRATO_RESERVADO_Searchbuttontext");
            Ddo_saldocontrato_executado_Caption = cgiGet( "DDO_SALDOCONTRATO_EXECUTADO_Caption");
            Ddo_saldocontrato_executado_Tooltip = cgiGet( "DDO_SALDOCONTRATO_EXECUTADO_Tooltip");
            Ddo_saldocontrato_executado_Cls = cgiGet( "DDO_SALDOCONTRATO_EXECUTADO_Cls");
            Ddo_saldocontrato_executado_Filteredtext_set = cgiGet( "DDO_SALDOCONTRATO_EXECUTADO_Filteredtext_set");
            Ddo_saldocontrato_executado_Filteredtextto_set = cgiGet( "DDO_SALDOCONTRATO_EXECUTADO_Filteredtextto_set");
            Ddo_saldocontrato_executado_Dropdownoptionstype = cgiGet( "DDO_SALDOCONTRATO_EXECUTADO_Dropdownoptionstype");
            Ddo_saldocontrato_executado_Titlecontrolidtoreplace = cgiGet( "DDO_SALDOCONTRATO_EXECUTADO_Titlecontrolidtoreplace");
            Ddo_saldocontrato_executado_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_EXECUTADO_Includesortasc"));
            Ddo_saldocontrato_executado_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_EXECUTADO_Includesortdsc"));
            Ddo_saldocontrato_executado_Sortedstatus = cgiGet( "DDO_SALDOCONTRATO_EXECUTADO_Sortedstatus");
            Ddo_saldocontrato_executado_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_EXECUTADO_Includefilter"));
            Ddo_saldocontrato_executado_Filtertype = cgiGet( "DDO_SALDOCONTRATO_EXECUTADO_Filtertype");
            Ddo_saldocontrato_executado_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_EXECUTADO_Filterisrange"));
            Ddo_saldocontrato_executado_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_EXECUTADO_Includedatalist"));
            Ddo_saldocontrato_executado_Sortasc = cgiGet( "DDO_SALDOCONTRATO_EXECUTADO_Sortasc");
            Ddo_saldocontrato_executado_Sortdsc = cgiGet( "DDO_SALDOCONTRATO_EXECUTADO_Sortdsc");
            Ddo_saldocontrato_executado_Cleanfilter = cgiGet( "DDO_SALDOCONTRATO_EXECUTADO_Cleanfilter");
            Ddo_saldocontrato_executado_Rangefilterfrom = cgiGet( "DDO_SALDOCONTRATO_EXECUTADO_Rangefilterfrom");
            Ddo_saldocontrato_executado_Rangefilterto = cgiGet( "DDO_SALDOCONTRATO_EXECUTADO_Rangefilterto");
            Ddo_saldocontrato_executado_Searchbuttontext = cgiGet( "DDO_SALDOCONTRATO_EXECUTADO_Searchbuttontext");
            Ddo_saldocontrato_saldo_Caption = cgiGet( "DDO_SALDOCONTRATO_SALDO_Caption");
            Ddo_saldocontrato_saldo_Tooltip = cgiGet( "DDO_SALDOCONTRATO_SALDO_Tooltip");
            Ddo_saldocontrato_saldo_Cls = cgiGet( "DDO_SALDOCONTRATO_SALDO_Cls");
            Ddo_saldocontrato_saldo_Filteredtext_set = cgiGet( "DDO_SALDOCONTRATO_SALDO_Filteredtext_set");
            Ddo_saldocontrato_saldo_Filteredtextto_set = cgiGet( "DDO_SALDOCONTRATO_SALDO_Filteredtextto_set");
            Ddo_saldocontrato_saldo_Dropdownoptionstype = cgiGet( "DDO_SALDOCONTRATO_SALDO_Dropdownoptionstype");
            Ddo_saldocontrato_saldo_Titlecontrolidtoreplace = cgiGet( "DDO_SALDOCONTRATO_SALDO_Titlecontrolidtoreplace");
            Ddo_saldocontrato_saldo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_SALDO_Includesortasc"));
            Ddo_saldocontrato_saldo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_SALDO_Includesortdsc"));
            Ddo_saldocontrato_saldo_Sortedstatus = cgiGet( "DDO_SALDOCONTRATO_SALDO_Sortedstatus");
            Ddo_saldocontrato_saldo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_SALDO_Includefilter"));
            Ddo_saldocontrato_saldo_Filtertype = cgiGet( "DDO_SALDOCONTRATO_SALDO_Filtertype");
            Ddo_saldocontrato_saldo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_SALDO_Filterisrange"));
            Ddo_saldocontrato_saldo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_SALDO_Includedatalist"));
            Ddo_saldocontrato_saldo_Sortasc = cgiGet( "DDO_SALDOCONTRATO_SALDO_Sortasc");
            Ddo_saldocontrato_saldo_Sortdsc = cgiGet( "DDO_SALDOCONTRATO_SALDO_Sortdsc");
            Ddo_saldocontrato_saldo_Cleanfilter = cgiGet( "DDO_SALDOCONTRATO_SALDO_Cleanfilter");
            Ddo_saldocontrato_saldo_Rangefilterfrom = cgiGet( "DDO_SALDOCONTRATO_SALDO_Rangefilterfrom");
            Ddo_saldocontrato_saldo_Rangefilterto = cgiGet( "DDO_SALDOCONTRATO_SALDO_Rangefilterto");
            Ddo_saldocontrato_saldo_Searchbuttontext = cgiGet( "DDO_SALDOCONTRATO_SALDO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_saldocontrato_codigo_Activeeventkey = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Activeeventkey");
            Ddo_saldocontrato_codigo_Filteredtext_get = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Filteredtext_get");
            Ddo_saldocontrato_codigo_Filteredtextto_get = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Filteredtextto_get");
            Ddo_contrato_codigo_Activeeventkey = cgiGet( "DDO_CONTRATO_CODIGO_Activeeventkey");
            Ddo_contrato_codigo_Filteredtext_get = cgiGet( "DDO_CONTRATO_CODIGO_Filteredtext_get");
            Ddo_contrato_codigo_Filteredtextto_get = cgiGet( "DDO_CONTRATO_CODIGO_Filteredtextto_get");
            Ddo_saldocontrato_vigenciainicio_Activeeventkey = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAINICIO_Activeeventkey");
            Ddo_saldocontrato_vigenciainicio_Filteredtext_get = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAINICIO_Filteredtext_get");
            Ddo_saldocontrato_vigenciainicio_Filteredtextto_get = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAINICIO_Filteredtextto_get");
            Ddo_saldocontrato_vigenciafim_Activeeventkey = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAFIM_Activeeventkey");
            Ddo_saldocontrato_vigenciafim_Filteredtext_get = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAFIM_Filteredtext_get");
            Ddo_saldocontrato_vigenciafim_Filteredtextto_get = cgiGet( "DDO_SALDOCONTRATO_VIGENCIAFIM_Filteredtextto_get");
            Ddo_saldocontrato_credito_Activeeventkey = cgiGet( "DDO_SALDOCONTRATO_CREDITO_Activeeventkey");
            Ddo_saldocontrato_credito_Filteredtext_get = cgiGet( "DDO_SALDOCONTRATO_CREDITO_Filteredtext_get");
            Ddo_saldocontrato_credito_Filteredtextto_get = cgiGet( "DDO_SALDOCONTRATO_CREDITO_Filteredtextto_get");
            Ddo_saldocontrato_reservado_Activeeventkey = cgiGet( "DDO_SALDOCONTRATO_RESERVADO_Activeeventkey");
            Ddo_saldocontrato_reservado_Filteredtext_get = cgiGet( "DDO_SALDOCONTRATO_RESERVADO_Filteredtext_get");
            Ddo_saldocontrato_reservado_Filteredtextto_get = cgiGet( "DDO_SALDOCONTRATO_RESERVADO_Filteredtextto_get");
            Ddo_saldocontrato_executado_Activeeventkey = cgiGet( "DDO_SALDOCONTRATO_EXECUTADO_Activeeventkey");
            Ddo_saldocontrato_executado_Filteredtext_get = cgiGet( "DDO_SALDOCONTRATO_EXECUTADO_Filteredtext_get");
            Ddo_saldocontrato_executado_Filteredtextto_get = cgiGet( "DDO_SALDOCONTRATO_EXECUTADO_Filteredtextto_get");
            Ddo_saldocontrato_saldo_Activeeventkey = cgiGet( "DDO_SALDOCONTRATO_SALDO_Activeeventkey");
            Ddo_saldocontrato_saldo_Filteredtext_get = cgiGet( "DDO_SALDOCONTRATO_SALDO_Filteredtext_get");
            Ddo_saldocontrato_saldo_Filteredtextto_get = cgiGet( "DDO_SALDOCONTRATO_SALDO_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vSALDOCONTRATO_VIGENCIAINICIO1"), 0) != AV16SaldoContrato_VigenciaInicio1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vSALDOCONTRATO_VIGENCIAINICIO_TO1"), 0) != AV17SaldoContrato_VigenciaInicio_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vSALDOCONTRATO_VIGENCIAINICIO2"), 0) != AV20SaldoContrato_VigenciaInicio2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vSALDOCONTRATO_VIGENCIAINICIO_TO2"), 0) != AV21SaldoContrato_VigenciaInicio_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vSALDOCONTRATO_VIGENCIAINICIO3"), 0) != AV24SaldoContrato_VigenciaInicio3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vSALDOCONTRATO_VIGENCIAINICIO_TO3"), 0) != AV25SaldoContrato_VigenciaInicio_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSALDOCONTRATO_CODIGO"), ",", ".") != Convert.ToDecimal( AV34TFSaldoContrato_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSALDOCONTRATO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV35TFSaldoContrato_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_CODIGO"), ",", ".") != Convert.ToDecimal( AV38TFContrato_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV39TFContrato_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFSALDOCONTRATO_VIGENCIAINICIO"), 0) != AV42TFSaldoContrato_VigenciaInicio )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFSALDOCONTRATO_VIGENCIAINICIO_TO"), 0) != AV43TFSaldoContrato_VigenciaInicio_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFSALDOCONTRATO_VIGENCIAFIM"), 0) != AV48TFSaldoContrato_VigenciaFim )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFSALDOCONTRATO_VIGENCIAFIM_TO"), 0) != AV49TFSaldoContrato_VigenciaFim_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFSALDOCONTRATO_CREDITO"), ",", ".") != AV54TFSaldoContrato_Credito )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFSALDOCONTRATO_CREDITO_TO"), ",", ".") != AV55TFSaldoContrato_Credito_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFSALDOCONTRATO_RESERVADO"), ",", ".") != AV58TFSaldoContrato_Reservado )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFSALDOCONTRATO_RESERVADO_TO"), ",", ".") != AV59TFSaldoContrato_Reservado_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFSALDOCONTRATO_EXECUTADO"), ",", ".") != AV62TFSaldoContrato_Executado )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFSALDOCONTRATO_EXECUTADO_TO"), ",", ".") != AV63TFSaldoContrato_Executado_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFSALDOCONTRATO_SALDO"), ",", ".") != AV66TFSaldoContrato_Saldo )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFSALDOCONTRATO_SALDO_TO"), ",", ".") != AV67TFSaldoContrato_Saldo_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E31M52 */
         E31M52 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E31M52( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "SALDOCONTRATO_VIGENCIAINICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "SALDOCONTRATO_VIGENCIAINICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "SALDOCONTRATO_VIGENCIAINICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfsaldocontrato_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsaldocontrato_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_codigo_Visible), 5, 0)));
         edtavTfsaldocontrato_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsaldocontrato_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_codigo_to_Visible), 5, 0)));
         edtavTfcontrato_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_codigo_Visible), 5, 0)));
         edtavTfcontrato_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_codigo_to_Visible), 5, 0)));
         edtavTfsaldocontrato_vigenciainicio_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsaldocontrato_vigenciainicio_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_vigenciainicio_Visible), 5, 0)));
         edtavTfsaldocontrato_vigenciainicio_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsaldocontrato_vigenciainicio_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_vigenciainicio_to_Visible), 5, 0)));
         edtavTfsaldocontrato_vigenciafim_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsaldocontrato_vigenciafim_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_vigenciafim_Visible), 5, 0)));
         edtavTfsaldocontrato_vigenciafim_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsaldocontrato_vigenciafim_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_vigenciafim_to_Visible), 5, 0)));
         edtavTfsaldocontrato_credito_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsaldocontrato_credito_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_credito_Visible), 5, 0)));
         edtavTfsaldocontrato_credito_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsaldocontrato_credito_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_credito_to_Visible), 5, 0)));
         edtavTfsaldocontrato_reservado_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsaldocontrato_reservado_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_reservado_Visible), 5, 0)));
         edtavTfsaldocontrato_reservado_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsaldocontrato_reservado_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_reservado_to_Visible), 5, 0)));
         edtavTfsaldocontrato_executado_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsaldocontrato_executado_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_executado_Visible), 5, 0)));
         edtavTfsaldocontrato_executado_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsaldocontrato_executado_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_executado_to_Visible), 5, 0)));
         edtavTfsaldocontrato_saldo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsaldocontrato_saldo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_saldo_Visible), 5, 0)));
         edtavTfsaldocontrato_saldo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsaldocontrato_saldo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_saldo_to_Visible), 5, 0)));
         Ddo_saldocontrato_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_SaldoContrato_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_codigo_Internalname, "TitleControlIdToReplace", Ddo_saldocontrato_codigo_Titlecontrolidtoreplace);
         AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace = Ddo_saldocontrato_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace", AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace);
         edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contrato_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_Contrato_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "TitleControlIdToReplace", Ddo_contrato_codigo_Titlecontrolidtoreplace);
         AV40ddo_Contrato_CodigoTitleControlIdToReplace = Ddo_contrato_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_Contrato_CodigoTitleControlIdToReplace", AV40ddo_Contrato_CodigoTitleControlIdToReplace);
         edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_saldocontrato_vigenciainicio_Titlecontrolidtoreplace = subGrid_Internalname+"_SaldoContrato_VigenciaInicio";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_vigenciainicio_Internalname, "TitleControlIdToReplace", Ddo_saldocontrato_vigenciainicio_Titlecontrolidtoreplace);
         AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace = Ddo_saldocontrato_vigenciainicio_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace", AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace);
         edtavDdo_saldocontrato_vigenciainiciotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_saldocontrato_vigenciainiciotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_saldocontrato_vigenciainiciotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_saldocontrato_vigenciafim_Titlecontrolidtoreplace = subGrid_Internalname+"_SaldoContrato_VigenciaFim";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_vigenciafim_Internalname, "TitleControlIdToReplace", Ddo_saldocontrato_vigenciafim_Titlecontrolidtoreplace);
         AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace = Ddo_saldocontrato_vigenciafim_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace", AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace);
         edtavDdo_saldocontrato_vigenciafimtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_saldocontrato_vigenciafimtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_saldocontrato_vigenciafimtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_saldocontrato_credito_Titlecontrolidtoreplace = subGrid_Internalname+"_SaldoContrato_Credito";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_credito_Internalname, "TitleControlIdToReplace", Ddo_saldocontrato_credito_Titlecontrolidtoreplace);
         AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace = Ddo_saldocontrato_credito_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace", AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace);
         edtavDdo_saldocontrato_creditotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_saldocontrato_creditotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_saldocontrato_creditotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_saldocontrato_reservado_Titlecontrolidtoreplace = subGrid_Internalname+"_SaldoContrato_Reservado";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_reservado_Internalname, "TitleControlIdToReplace", Ddo_saldocontrato_reservado_Titlecontrolidtoreplace);
         AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace = Ddo_saldocontrato_reservado_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace", AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace);
         edtavDdo_saldocontrato_reservadotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_saldocontrato_reservadotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_saldocontrato_reservadotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_saldocontrato_executado_Titlecontrolidtoreplace = subGrid_Internalname+"_SaldoContrato_Executado";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_executado_Internalname, "TitleControlIdToReplace", Ddo_saldocontrato_executado_Titlecontrolidtoreplace);
         AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace = Ddo_saldocontrato_executado_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace", AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace);
         edtavDdo_saldocontrato_executadotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_saldocontrato_executadotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_saldocontrato_executadotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_saldocontrato_saldo_Titlecontrolidtoreplace = subGrid_Internalname+"_SaldoContrato_Saldo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_saldo_Internalname, "TitleControlIdToReplace", Ddo_saldocontrato_saldo_Titlecontrolidtoreplace);
         AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace = Ddo_saldocontrato_saldo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace", AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace);
         edtavDdo_saldocontrato_saldotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_saldocontrato_saldotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_saldocontrato_saldotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Saldo Contrato";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Inicial", 0);
         cmbavOrderedby.addItem("2", "Saldo Contrato", 0);
         cmbavOrderedby.addItem("3", "Contrato", 0);
         cmbavOrderedby.addItem("4", "Final", 0);
         cmbavOrderedby.addItem("5", "Cr�dito", 0);
         cmbavOrderedby.addItem("6", "Reservado", 0);
         cmbavOrderedby.addItem("7", "Executado", 0);
         cmbavOrderedby.addItem("8", "Saldo", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV69DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV69DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E32M52( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV33SaldoContrato_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV37Contrato_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV41SaldoContrato_VigenciaInicioTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV47SaldoContrato_VigenciaFimTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV53SaldoContrato_CreditoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV57SaldoContrato_ReservadoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV61SaldoContrato_ExecutadoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV65SaldoContrato_SaldoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtSaldoContrato_Codigo_Titleformat = 2;
         edtSaldoContrato_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Saldo Contrato", AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_Codigo_Internalname, "Title", edtSaldoContrato_Codigo_Title);
         edtContrato_Codigo_Titleformat = 2;
         edtContrato_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Contrato", AV40ddo_Contrato_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Codigo_Internalname, "Title", edtContrato_Codigo_Title);
         edtSaldoContrato_VigenciaInicio_Titleformat = 2;
         edtSaldoContrato_VigenciaInicio_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Inicial", AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_VigenciaInicio_Internalname, "Title", edtSaldoContrato_VigenciaInicio_Title);
         edtSaldoContrato_VigenciaFim_Titleformat = 2;
         edtSaldoContrato_VigenciaFim_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Final", AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_VigenciaFim_Internalname, "Title", edtSaldoContrato_VigenciaFim_Title);
         edtSaldoContrato_Credito_Titleformat = 2;
         edtSaldoContrato_Credito_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Cr�dito", AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_Credito_Internalname, "Title", edtSaldoContrato_Credito_Title);
         edtSaldoContrato_Reservado_Titleformat = 2;
         edtSaldoContrato_Reservado_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Reservado", AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_Reservado_Internalname, "Title", edtSaldoContrato_Reservado_Title);
         edtSaldoContrato_Executado_Titleformat = 2;
         edtSaldoContrato_Executado_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Executado", AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_Executado_Internalname, "Title", edtSaldoContrato_Executado_Title);
         edtSaldoContrato_Saldo_Titleformat = 2;
         edtSaldoContrato_Saldo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Saldo", AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_Saldo_Internalname, "Title", edtSaldoContrato_Saldo_Title);
         AV71GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71GridCurrentPage), 10, 0)));
         AV72GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV72GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV33SaldoContrato_CodigoTitleFilterData", AV33SaldoContrato_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV37Contrato_CodigoTitleFilterData", AV37Contrato_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV41SaldoContrato_VigenciaInicioTitleFilterData", AV41SaldoContrato_VigenciaInicioTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV47SaldoContrato_VigenciaFimTitleFilterData", AV47SaldoContrato_VigenciaFimTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV53SaldoContrato_CreditoTitleFilterData", AV53SaldoContrato_CreditoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV57SaldoContrato_ReservadoTitleFilterData", AV57SaldoContrato_ReservadoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV61SaldoContrato_ExecutadoTitleFilterData", AV61SaldoContrato_ExecutadoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV65SaldoContrato_SaldoTitleFilterData", AV65SaldoContrato_SaldoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11M52( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV70PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV70PageToGo) ;
         }
      }

      protected void E12M52( )
      {
         /* Ddo_saldocontrato_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_saldocontrato_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_codigo_Internalname, "SortedStatus", Ddo_saldocontrato_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_codigo_Internalname, "SortedStatus", Ddo_saldocontrato_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV34TFSaldoContrato_Codigo = (int)(NumberUtil.Val( Ddo_saldocontrato_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFSaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34TFSaldoContrato_Codigo), 6, 0)));
            AV35TFSaldoContrato_Codigo_To = (int)(NumberUtil.Val( Ddo_saldocontrato_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFSaldoContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFSaldoContrato_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13M52( )
      {
         /* Ddo_contrato_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contrato_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "SortedStatus", Ddo_contrato_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "SortedStatus", Ddo_contrato_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV38TFContrato_Codigo = (int)(NumberUtil.Val( Ddo_contrato_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFContrato_Codigo), 6, 0)));
            AV39TFContrato_Codigo_To = (int)(NumberUtil.Val( Ddo_contrato_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFContrato_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14M52( )
      {
         /* Ddo_saldocontrato_vigenciainicio_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_saldocontrato_vigenciainicio_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_vigenciainicio_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_vigenciainicio_Internalname, "SortedStatus", Ddo_saldocontrato_vigenciainicio_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_vigenciainicio_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_vigenciainicio_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_vigenciainicio_Internalname, "SortedStatus", Ddo_saldocontrato_vigenciainicio_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_vigenciainicio_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV42TFSaldoContrato_VigenciaInicio = context.localUtil.CToD( Ddo_saldocontrato_vigenciainicio_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFSaldoContrato_VigenciaInicio", context.localUtil.Format(AV42TFSaldoContrato_VigenciaInicio, "99/99/99"));
            AV43TFSaldoContrato_VigenciaInicio_To = context.localUtil.CToD( Ddo_saldocontrato_vigenciainicio_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFSaldoContrato_VigenciaInicio_To", context.localUtil.Format(AV43TFSaldoContrato_VigenciaInicio_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15M52( )
      {
         /* Ddo_saldocontrato_vigenciafim_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_saldocontrato_vigenciafim_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_vigenciafim_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_vigenciafim_Internalname, "SortedStatus", Ddo_saldocontrato_vigenciafim_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_vigenciafim_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_vigenciafim_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_vigenciafim_Internalname, "SortedStatus", Ddo_saldocontrato_vigenciafim_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_vigenciafim_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV48TFSaldoContrato_VigenciaFim = context.localUtil.CToD( Ddo_saldocontrato_vigenciafim_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFSaldoContrato_VigenciaFim", context.localUtil.Format(AV48TFSaldoContrato_VigenciaFim, "99/99/99"));
            AV49TFSaldoContrato_VigenciaFim_To = context.localUtil.CToD( Ddo_saldocontrato_vigenciafim_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFSaldoContrato_VigenciaFim_To", context.localUtil.Format(AV49TFSaldoContrato_VigenciaFim_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16M52( )
      {
         /* Ddo_saldocontrato_credito_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_saldocontrato_credito_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_credito_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_credito_Internalname, "SortedStatus", Ddo_saldocontrato_credito_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_credito_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_credito_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_credito_Internalname, "SortedStatus", Ddo_saldocontrato_credito_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_credito_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV54TFSaldoContrato_Credito = NumberUtil.Val( Ddo_saldocontrato_credito_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFSaldoContrato_Credito", StringUtil.LTrim( StringUtil.Str( AV54TFSaldoContrato_Credito, 18, 5)));
            AV55TFSaldoContrato_Credito_To = NumberUtil.Val( Ddo_saldocontrato_credito_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFSaldoContrato_Credito_To", StringUtil.LTrim( StringUtil.Str( AV55TFSaldoContrato_Credito_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E17M52( )
      {
         /* Ddo_saldocontrato_reservado_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_saldocontrato_reservado_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_reservado_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_reservado_Internalname, "SortedStatus", Ddo_saldocontrato_reservado_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_reservado_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_reservado_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_reservado_Internalname, "SortedStatus", Ddo_saldocontrato_reservado_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_reservado_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV58TFSaldoContrato_Reservado = NumberUtil.Val( Ddo_saldocontrato_reservado_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFSaldoContrato_Reservado", StringUtil.LTrim( StringUtil.Str( AV58TFSaldoContrato_Reservado, 18, 5)));
            AV59TFSaldoContrato_Reservado_To = NumberUtil.Val( Ddo_saldocontrato_reservado_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFSaldoContrato_Reservado_To", StringUtil.LTrim( StringUtil.Str( AV59TFSaldoContrato_Reservado_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E18M52( )
      {
         /* Ddo_saldocontrato_executado_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_saldocontrato_executado_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_executado_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_executado_Internalname, "SortedStatus", Ddo_saldocontrato_executado_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_executado_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_executado_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_executado_Internalname, "SortedStatus", Ddo_saldocontrato_executado_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_executado_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV62TFSaldoContrato_Executado = NumberUtil.Val( Ddo_saldocontrato_executado_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFSaldoContrato_Executado", StringUtil.LTrim( StringUtil.Str( AV62TFSaldoContrato_Executado, 18, 5)));
            AV63TFSaldoContrato_Executado_To = NumberUtil.Val( Ddo_saldocontrato_executado_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFSaldoContrato_Executado_To", StringUtil.LTrim( StringUtil.Str( AV63TFSaldoContrato_Executado_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E19M52( )
      {
         /* Ddo_saldocontrato_saldo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_saldocontrato_saldo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_saldo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_saldo_Internalname, "SortedStatus", Ddo_saldocontrato_saldo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_saldo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_saldo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_saldo_Internalname, "SortedStatus", Ddo_saldocontrato_saldo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_saldo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV66TFSaldoContrato_Saldo = NumberUtil.Val( Ddo_saldocontrato_saldo_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFSaldoContrato_Saldo", StringUtil.LTrim( StringUtil.Str( AV66TFSaldoContrato_Saldo, 18, 5)));
            AV67TFSaldoContrato_Saldo_To = NumberUtil.Val( Ddo_saldocontrato_saldo_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFSaldoContrato_Saldo_To", StringUtil.LTrim( StringUtil.Str( AV67TFSaldoContrato_Saldo_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E33M52( )
      {
         /* Grid_Load Routine */
         AV28Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
         AV75Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("saldocontrato.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1561SaldoContrato_Codigo);
         AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
         AV76Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("saldocontrato.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1561SaldoContrato_Codigo);
         edtSaldoContrato_Codigo_Link = formatLink("viewnotaempenho.aspx") + "?" + UrlEncode("" +A1560NotaEmpenho_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtSaldoContrato_VigenciaInicio_Link = formatLink("viewsaldocontrato.aspx") + "?" + UrlEncode("" +A1561SaldoContrato_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 94;
         }
         sendrow_942( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_94_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(94, GridRow);
         }
      }

      protected void E20M52( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E26M52( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E21M52( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16SaldoContrato_VigenciaInicio1, AV17SaldoContrato_VigenciaInicio_To1, AV19DynamicFiltersSelector2, AV20SaldoContrato_VigenciaInicio2, AV21SaldoContrato_VigenciaInicio_To2, AV23DynamicFiltersSelector3, AV24SaldoContrato_VigenciaInicio3, AV25SaldoContrato_VigenciaInicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFSaldoContrato_Codigo, AV35TFSaldoContrato_Codigo_To, AV38TFContrato_Codigo, AV39TFContrato_Codigo_To, AV42TFSaldoContrato_VigenciaInicio, AV43TFSaldoContrato_VigenciaInicio_To, AV48TFSaldoContrato_VigenciaFim, AV49TFSaldoContrato_VigenciaFim_To, AV54TFSaldoContrato_Credito, AV55TFSaldoContrato_Credito_To, AV58TFSaldoContrato_Reservado, AV59TFSaldoContrato_Reservado_To, AV62TFSaldoContrato_Executado, AV63TFSaldoContrato_Executado_To, AV66TFSaldoContrato_Saldo, AV67TFSaldoContrato_Saldo_To, AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace, AV40ddo_Contrato_CodigoTitleControlIdToReplace, AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace, AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace, AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace, AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace, AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace, AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace, AV77Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1561SaldoContrato_Codigo, A1560NotaEmpenho_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E27M52( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E28M52( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E22M52( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16SaldoContrato_VigenciaInicio1, AV17SaldoContrato_VigenciaInicio_To1, AV19DynamicFiltersSelector2, AV20SaldoContrato_VigenciaInicio2, AV21SaldoContrato_VigenciaInicio_To2, AV23DynamicFiltersSelector3, AV24SaldoContrato_VigenciaInicio3, AV25SaldoContrato_VigenciaInicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFSaldoContrato_Codigo, AV35TFSaldoContrato_Codigo_To, AV38TFContrato_Codigo, AV39TFContrato_Codigo_To, AV42TFSaldoContrato_VigenciaInicio, AV43TFSaldoContrato_VigenciaInicio_To, AV48TFSaldoContrato_VigenciaFim, AV49TFSaldoContrato_VigenciaFim_To, AV54TFSaldoContrato_Credito, AV55TFSaldoContrato_Credito_To, AV58TFSaldoContrato_Reservado, AV59TFSaldoContrato_Reservado_To, AV62TFSaldoContrato_Executado, AV63TFSaldoContrato_Executado_To, AV66TFSaldoContrato_Saldo, AV67TFSaldoContrato_Saldo_To, AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace, AV40ddo_Contrato_CodigoTitleControlIdToReplace, AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace, AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace, AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace, AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace, AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace, AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace, AV77Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1561SaldoContrato_Codigo, A1560NotaEmpenho_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E29M52( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E23M52( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16SaldoContrato_VigenciaInicio1, AV17SaldoContrato_VigenciaInicio_To1, AV19DynamicFiltersSelector2, AV20SaldoContrato_VigenciaInicio2, AV21SaldoContrato_VigenciaInicio_To2, AV23DynamicFiltersSelector3, AV24SaldoContrato_VigenciaInicio3, AV25SaldoContrato_VigenciaInicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFSaldoContrato_Codigo, AV35TFSaldoContrato_Codigo_To, AV38TFContrato_Codigo, AV39TFContrato_Codigo_To, AV42TFSaldoContrato_VigenciaInicio, AV43TFSaldoContrato_VigenciaInicio_To, AV48TFSaldoContrato_VigenciaFim, AV49TFSaldoContrato_VigenciaFim_To, AV54TFSaldoContrato_Credito, AV55TFSaldoContrato_Credito_To, AV58TFSaldoContrato_Reservado, AV59TFSaldoContrato_Reservado_To, AV62TFSaldoContrato_Executado, AV63TFSaldoContrato_Executado_To, AV66TFSaldoContrato_Saldo, AV67TFSaldoContrato_Saldo_To, AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace, AV40ddo_Contrato_CodigoTitleControlIdToReplace, AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace, AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace, AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace, AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace, AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace, AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace, AV77Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1561SaldoContrato_Codigo, A1560NotaEmpenho_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E30M52( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E24M52( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void E25M52( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("saldocontrato.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_saldocontrato_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_codigo_Internalname, "SortedStatus", Ddo_saldocontrato_codigo_Sortedstatus);
         Ddo_contrato_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "SortedStatus", Ddo_contrato_codigo_Sortedstatus);
         Ddo_saldocontrato_vigenciainicio_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_vigenciainicio_Internalname, "SortedStatus", Ddo_saldocontrato_vigenciainicio_Sortedstatus);
         Ddo_saldocontrato_vigenciafim_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_vigenciafim_Internalname, "SortedStatus", Ddo_saldocontrato_vigenciafim_Sortedstatus);
         Ddo_saldocontrato_credito_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_credito_Internalname, "SortedStatus", Ddo_saldocontrato_credito_Sortedstatus);
         Ddo_saldocontrato_reservado_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_reservado_Internalname, "SortedStatus", Ddo_saldocontrato_reservado_Sortedstatus);
         Ddo_saldocontrato_executado_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_executado_Internalname, "SortedStatus", Ddo_saldocontrato_executado_Sortedstatus);
         Ddo_saldocontrato_saldo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_saldo_Internalname, "SortedStatus", Ddo_saldocontrato_saldo_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_saldocontrato_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_codigo_Internalname, "SortedStatus", Ddo_saldocontrato_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contrato_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "SortedStatus", Ddo_contrato_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_saldocontrato_vigenciainicio_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_vigenciainicio_Internalname, "SortedStatus", Ddo_saldocontrato_vigenciainicio_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_saldocontrato_vigenciafim_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_vigenciafim_Internalname, "SortedStatus", Ddo_saldocontrato_vigenciafim_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_saldocontrato_credito_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_credito_Internalname, "SortedStatus", Ddo_saldocontrato_credito_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_saldocontrato_reservado_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_reservado_Internalname, "SortedStatus", Ddo_saldocontrato_reservado_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_saldocontrato_executado_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_executado_Internalname, "SortedStatus", Ddo_saldocontrato_executado_Sortedstatus);
         }
         else if ( AV13OrderedBy == 8 )
         {
            Ddo_saldocontrato_saldo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_saldo_Internalname, "SortedStatus", Ddo_saldocontrato_saldo_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         tblTablemergeddynamicfilterssaldocontrato_vigenciainicio1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterssaldocontrato_vigenciainicio1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterssaldocontrato_vigenciainicio1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SALDOCONTRATO_VIGENCIAINICIO") == 0 )
         {
            tblTablemergeddynamicfilterssaldocontrato_vigenciainicio1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterssaldocontrato_vigenciainicio1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterssaldocontrato_vigenciainicio1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         tblTablemergeddynamicfilterssaldocontrato_vigenciainicio2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterssaldocontrato_vigenciainicio2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterssaldocontrato_vigenciainicio2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SALDOCONTRATO_VIGENCIAINICIO") == 0 )
         {
            tblTablemergeddynamicfilterssaldocontrato_vigenciainicio2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterssaldocontrato_vigenciainicio2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterssaldocontrato_vigenciainicio2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         tblTablemergeddynamicfilterssaldocontrato_vigenciainicio3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterssaldocontrato_vigenciainicio3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterssaldocontrato_vigenciainicio3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SALDOCONTRATO_VIGENCIAINICIO") == 0 )
         {
            tblTablemergeddynamicfilterssaldocontrato_vigenciainicio3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterssaldocontrato_vigenciainicio3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterssaldocontrato_vigenciainicio3_Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "SALDOCONTRATO_VIGENCIAINICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20SaldoContrato_VigenciaInicio2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20SaldoContrato_VigenciaInicio2", context.localUtil.Format(AV20SaldoContrato_VigenciaInicio2, "99/99/99"));
         AV21SaldoContrato_VigenciaInicio_To2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21SaldoContrato_VigenciaInicio_To2", context.localUtil.Format(AV21SaldoContrato_VigenciaInicio_To2, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "SALDOCONTRATO_VIGENCIAINICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24SaldoContrato_VigenciaInicio3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24SaldoContrato_VigenciaInicio3", context.localUtil.Format(AV24SaldoContrato_VigenciaInicio3, "99/99/99"));
         AV25SaldoContrato_VigenciaInicio_To3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25SaldoContrato_VigenciaInicio_To3", context.localUtil.Format(AV25SaldoContrato_VigenciaInicio_To3, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV34TFSaldoContrato_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFSaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34TFSaldoContrato_Codigo), 6, 0)));
         Ddo_saldocontrato_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_codigo_Internalname, "FilteredText_set", Ddo_saldocontrato_codigo_Filteredtext_set);
         AV35TFSaldoContrato_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFSaldoContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFSaldoContrato_Codigo_To), 6, 0)));
         Ddo_saldocontrato_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_codigo_Internalname, "FilteredTextTo_set", Ddo_saldocontrato_codigo_Filteredtextto_set);
         AV38TFContrato_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFContrato_Codigo), 6, 0)));
         Ddo_contrato_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "FilteredText_set", Ddo_contrato_codigo_Filteredtext_set);
         AV39TFContrato_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFContrato_Codigo_To), 6, 0)));
         Ddo_contrato_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "FilteredTextTo_set", Ddo_contrato_codigo_Filteredtextto_set);
         AV42TFSaldoContrato_VigenciaInicio = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFSaldoContrato_VigenciaInicio", context.localUtil.Format(AV42TFSaldoContrato_VigenciaInicio, "99/99/99"));
         Ddo_saldocontrato_vigenciainicio_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_vigenciainicio_Internalname, "FilteredText_set", Ddo_saldocontrato_vigenciainicio_Filteredtext_set);
         AV43TFSaldoContrato_VigenciaInicio_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFSaldoContrato_VigenciaInicio_To", context.localUtil.Format(AV43TFSaldoContrato_VigenciaInicio_To, "99/99/99"));
         Ddo_saldocontrato_vigenciainicio_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_vigenciainicio_Internalname, "FilteredTextTo_set", Ddo_saldocontrato_vigenciainicio_Filteredtextto_set);
         AV48TFSaldoContrato_VigenciaFim = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFSaldoContrato_VigenciaFim", context.localUtil.Format(AV48TFSaldoContrato_VigenciaFim, "99/99/99"));
         Ddo_saldocontrato_vigenciafim_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_vigenciafim_Internalname, "FilteredText_set", Ddo_saldocontrato_vigenciafim_Filteredtext_set);
         AV49TFSaldoContrato_VigenciaFim_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFSaldoContrato_VigenciaFim_To", context.localUtil.Format(AV49TFSaldoContrato_VigenciaFim_To, "99/99/99"));
         Ddo_saldocontrato_vigenciafim_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_vigenciafim_Internalname, "FilteredTextTo_set", Ddo_saldocontrato_vigenciafim_Filteredtextto_set);
         AV54TFSaldoContrato_Credito = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFSaldoContrato_Credito", StringUtil.LTrim( StringUtil.Str( AV54TFSaldoContrato_Credito, 18, 5)));
         Ddo_saldocontrato_credito_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_credito_Internalname, "FilteredText_set", Ddo_saldocontrato_credito_Filteredtext_set);
         AV55TFSaldoContrato_Credito_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFSaldoContrato_Credito_To", StringUtil.LTrim( StringUtil.Str( AV55TFSaldoContrato_Credito_To, 18, 5)));
         Ddo_saldocontrato_credito_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_credito_Internalname, "FilteredTextTo_set", Ddo_saldocontrato_credito_Filteredtextto_set);
         AV58TFSaldoContrato_Reservado = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFSaldoContrato_Reservado", StringUtil.LTrim( StringUtil.Str( AV58TFSaldoContrato_Reservado, 18, 5)));
         Ddo_saldocontrato_reservado_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_reservado_Internalname, "FilteredText_set", Ddo_saldocontrato_reservado_Filteredtext_set);
         AV59TFSaldoContrato_Reservado_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFSaldoContrato_Reservado_To", StringUtil.LTrim( StringUtil.Str( AV59TFSaldoContrato_Reservado_To, 18, 5)));
         Ddo_saldocontrato_reservado_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_reservado_Internalname, "FilteredTextTo_set", Ddo_saldocontrato_reservado_Filteredtextto_set);
         AV62TFSaldoContrato_Executado = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFSaldoContrato_Executado", StringUtil.LTrim( StringUtil.Str( AV62TFSaldoContrato_Executado, 18, 5)));
         Ddo_saldocontrato_executado_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_executado_Internalname, "FilteredText_set", Ddo_saldocontrato_executado_Filteredtext_set);
         AV63TFSaldoContrato_Executado_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFSaldoContrato_Executado_To", StringUtil.LTrim( StringUtil.Str( AV63TFSaldoContrato_Executado_To, 18, 5)));
         Ddo_saldocontrato_executado_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_executado_Internalname, "FilteredTextTo_set", Ddo_saldocontrato_executado_Filteredtextto_set);
         AV66TFSaldoContrato_Saldo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFSaldoContrato_Saldo", StringUtil.LTrim( StringUtil.Str( AV66TFSaldoContrato_Saldo, 18, 5)));
         Ddo_saldocontrato_saldo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_saldo_Internalname, "FilteredText_set", Ddo_saldocontrato_saldo_Filteredtext_set);
         AV67TFSaldoContrato_Saldo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFSaldoContrato_Saldo_To", StringUtil.LTrim( StringUtil.Str( AV67TFSaldoContrato_Saldo_To, 18, 5)));
         Ddo_saldocontrato_saldo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_saldo_Internalname, "FilteredTextTo_set", Ddo_saldocontrato_saldo_Filteredtextto_set);
         AV15DynamicFiltersSelector1 = "SALDOCONTRATO_VIGENCIAINICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16SaldoContrato_VigenciaInicio1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16SaldoContrato_VigenciaInicio1", context.localUtil.Format(AV16SaldoContrato_VigenciaInicio1, "99/99/99"));
         AV17SaldoContrato_VigenciaInicio_To1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17SaldoContrato_VigenciaInicio_To1", context.localUtil.Format(AV17SaldoContrato_VigenciaInicio_To1, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get(AV77Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV77Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV30Session.Get(AV77Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV78GXV1 = 1;
         while ( AV78GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV78GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_CODIGO") == 0 )
            {
               AV34TFSaldoContrato_Codigo = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFSaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34TFSaldoContrato_Codigo), 6, 0)));
               AV35TFSaldoContrato_Codigo_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFSaldoContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFSaldoContrato_Codigo_To), 6, 0)));
               if ( ! (0==AV34TFSaldoContrato_Codigo) )
               {
                  Ddo_saldocontrato_codigo_Filteredtext_set = StringUtil.Str( (decimal)(AV34TFSaldoContrato_Codigo), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_codigo_Internalname, "FilteredText_set", Ddo_saldocontrato_codigo_Filteredtext_set);
               }
               if ( ! (0==AV35TFSaldoContrato_Codigo_To) )
               {
                  Ddo_saldocontrato_codigo_Filteredtextto_set = StringUtil.Str( (decimal)(AV35TFSaldoContrato_Codigo_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_codigo_Internalname, "FilteredTextTo_set", Ddo_saldocontrato_codigo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATO_CODIGO") == 0 )
            {
               AV38TFContrato_Codigo = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFContrato_Codigo), 6, 0)));
               AV39TFContrato_Codigo_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFContrato_Codigo_To), 6, 0)));
               if ( ! (0==AV38TFContrato_Codigo) )
               {
                  Ddo_contrato_codigo_Filteredtext_set = StringUtil.Str( (decimal)(AV38TFContrato_Codigo), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "FilteredText_set", Ddo_contrato_codigo_Filteredtext_set);
               }
               if ( ! (0==AV39TFContrato_Codigo_To) )
               {
                  Ddo_contrato_codigo_Filteredtextto_set = StringUtil.Str( (decimal)(AV39TFContrato_Codigo_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "FilteredTextTo_set", Ddo_contrato_codigo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_VIGENCIAINICIO") == 0 )
            {
               AV42TFSaldoContrato_VigenciaInicio = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFSaldoContrato_VigenciaInicio", context.localUtil.Format(AV42TFSaldoContrato_VigenciaInicio, "99/99/99"));
               AV43TFSaldoContrato_VigenciaInicio_To = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFSaldoContrato_VigenciaInicio_To", context.localUtil.Format(AV43TFSaldoContrato_VigenciaInicio_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV42TFSaldoContrato_VigenciaInicio) )
               {
                  Ddo_saldocontrato_vigenciainicio_Filteredtext_set = context.localUtil.DToC( AV42TFSaldoContrato_VigenciaInicio, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_vigenciainicio_Internalname, "FilteredText_set", Ddo_saldocontrato_vigenciainicio_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV43TFSaldoContrato_VigenciaInicio_To) )
               {
                  Ddo_saldocontrato_vigenciainicio_Filteredtextto_set = context.localUtil.DToC( AV43TFSaldoContrato_VigenciaInicio_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_vigenciainicio_Internalname, "FilteredTextTo_set", Ddo_saldocontrato_vigenciainicio_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_VIGENCIAFIM") == 0 )
            {
               AV48TFSaldoContrato_VigenciaFim = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFSaldoContrato_VigenciaFim", context.localUtil.Format(AV48TFSaldoContrato_VigenciaFim, "99/99/99"));
               AV49TFSaldoContrato_VigenciaFim_To = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFSaldoContrato_VigenciaFim_To", context.localUtil.Format(AV49TFSaldoContrato_VigenciaFim_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV48TFSaldoContrato_VigenciaFim) )
               {
                  Ddo_saldocontrato_vigenciafim_Filteredtext_set = context.localUtil.DToC( AV48TFSaldoContrato_VigenciaFim, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_vigenciafim_Internalname, "FilteredText_set", Ddo_saldocontrato_vigenciafim_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV49TFSaldoContrato_VigenciaFim_To) )
               {
                  Ddo_saldocontrato_vigenciafim_Filteredtextto_set = context.localUtil.DToC( AV49TFSaldoContrato_VigenciaFim_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_vigenciafim_Internalname, "FilteredTextTo_set", Ddo_saldocontrato_vigenciafim_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_CREDITO") == 0 )
            {
               AV54TFSaldoContrato_Credito = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFSaldoContrato_Credito", StringUtil.LTrim( StringUtil.Str( AV54TFSaldoContrato_Credito, 18, 5)));
               AV55TFSaldoContrato_Credito_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFSaldoContrato_Credito_To", StringUtil.LTrim( StringUtil.Str( AV55TFSaldoContrato_Credito_To, 18, 5)));
               if ( ! (Convert.ToDecimal(0)==AV54TFSaldoContrato_Credito) )
               {
                  Ddo_saldocontrato_credito_Filteredtext_set = StringUtil.Str( AV54TFSaldoContrato_Credito, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_credito_Internalname, "FilteredText_set", Ddo_saldocontrato_credito_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV55TFSaldoContrato_Credito_To) )
               {
                  Ddo_saldocontrato_credito_Filteredtextto_set = StringUtil.Str( AV55TFSaldoContrato_Credito_To, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_credito_Internalname, "FilteredTextTo_set", Ddo_saldocontrato_credito_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_RESERVADO") == 0 )
            {
               AV58TFSaldoContrato_Reservado = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFSaldoContrato_Reservado", StringUtil.LTrim( StringUtil.Str( AV58TFSaldoContrato_Reservado, 18, 5)));
               AV59TFSaldoContrato_Reservado_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFSaldoContrato_Reservado_To", StringUtil.LTrim( StringUtil.Str( AV59TFSaldoContrato_Reservado_To, 18, 5)));
               if ( ! (Convert.ToDecimal(0)==AV58TFSaldoContrato_Reservado) )
               {
                  Ddo_saldocontrato_reservado_Filteredtext_set = StringUtil.Str( AV58TFSaldoContrato_Reservado, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_reservado_Internalname, "FilteredText_set", Ddo_saldocontrato_reservado_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV59TFSaldoContrato_Reservado_To) )
               {
                  Ddo_saldocontrato_reservado_Filteredtextto_set = StringUtil.Str( AV59TFSaldoContrato_Reservado_To, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_reservado_Internalname, "FilteredTextTo_set", Ddo_saldocontrato_reservado_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_EXECUTADO") == 0 )
            {
               AV62TFSaldoContrato_Executado = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFSaldoContrato_Executado", StringUtil.LTrim( StringUtil.Str( AV62TFSaldoContrato_Executado, 18, 5)));
               AV63TFSaldoContrato_Executado_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFSaldoContrato_Executado_To", StringUtil.LTrim( StringUtil.Str( AV63TFSaldoContrato_Executado_To, 18, 5)));
               if ( ! (Convert.ToDecimal(0)==AV62TFSaldoContrato_Executado) )
               {
                  Ddo_saldocontrato_executado_Filteredtext_set = StringUtil.Str( AV62TFSaldoContrato_Executado, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_executado_Internalname, "FilteredText_set", Ddo_saldocontrato_executado_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV63TFSaldoContrato_Executado_To) )
               {
                  Ddo_saldocontrato_executado_Filteredtextto_set = StringUtil.Str( AV63TFSaldoContrato_Executado_To, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_executado_Internalname, "FilteredTextTo_set", Ddo_saldocontrato_executado_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_SALDO") == 0 )
            {
               AV66TFSaldoContrato_Saldo = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFSaldoContrato_Saldo", StringUtil.LTrim( StringUtil.Str( AV66TFSaldoContrato_Saldo, 18, 5)));
               AV67TFSaldoContrato_Saldo_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFSaldoContrato_Saldo_To", StringUtil.LTrim( StringUtil.Str( AV67TFSaldoContrato_Saldo_To, 18, 5)));
               if ( ! (Convert.ToDecimal(0)==AV66TFSaldoContrato_Saldo) )
               {
                  Ddo_saldocontrato_saldo_Filteredtext_set = StringUtil.Str( AV66TFSaldoContrato_Saldo, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_saldo_Internalname, "FilteredText_set", Ddo_saldocontrato_saldo_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV67TFSaldoContrato_Saldo_To) )
               {
                  Ddo_saldocontrato_saldo_Filteredtextto_set = StringUtil.Str( AV67TFSaldoContrato_Saldo_To, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_saldo_Internalname, "FilteredTextTo_set", Ddo_saldocontrato_saldo_Filteredtextto_set);
               }
            }
            AV78GXV1 = (int)(AV78GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SALDOCONTRATO_VIGENCIAINICIO") == 0 )
            {
               AV16SaldoContrato_VigenciaInicio1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16SaldoContrato_VigenciaInicio1", context.localUtil.Format(AV16SaldoContrato_VigenciaInicio1, "99/99/99"));
               AV17SaldoContrato_VigenciaInicio_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17SaldoContrato_VigenciaInicio_To1", context.localUtil.Format(AV17SaldoContrato_VigenciaInicio_To1, "99/99/99"));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SALDOCONTRATO_VIGENCIAINICIO") == 0 )
               {
                  AV20SaldoContrato_VigenciaInicio2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20SaldoContrato_VigenciaInicio2", context.localUtil.Format(AV20SaldoContrato_VigenciaInicio2, "99/99/99"));
                  AV21SaldoContrato_VigenciaInicio_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21SaldoContrato_VigenciaInicio_To2", context.localUtil.Format(AV21SaldoContrato_VigenciaInicio_To2, "99/99/99"));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SALDOCONTRATO_VIGENCIAINICIO") == 0 )
                  {
                     AV24SaldoContrato_VigenciaInicio3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24SaldoContrato_VigenciaInicio3", context.localUtil.Format(AV24SaldoContrato_VigenciaInicio3, "99/99/99"));
                     AV25SaldoContrato_VigenciaInicio_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25SaldoContrato_VigenciaInicio_To3", context.localUtil.Format(AV25SaldoContrato_VigenciaInicio_To3, "99/99/99"));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV30Session.Get(AV77Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV34TFSaldoContrato_Codigo) && (0==AV35TFSaldoContrato_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSALDOCONTRATO_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV34TFSaldoContrato_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV35TFSaldoContrato_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV38TFContrato_Codigo) && (0==AV39TFContrato_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATO_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV38TFContrato_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV39TFContrato_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV42TFSaldoContrato_VigenciaInicio) && (DateTime.MinValue==AV43TFSaldoContrato_VigenciaInicio_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSALDOCONTRATO_VIGENCIAINICIO";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV42TFSaldoContrato_VigenciaInicio, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV43TFSaldoContrato_VigenciaInicio_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV48TFSaldoContrato_VigenciaFim) && (DateTime.MinValue==AV49TFSaldoContrato_VigenciaFim_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSALDOCONTRATO_VIGENCIAFIM";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV48TFSaldoContrato_VigenciaFim, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV49TFSaldoContrato_VigenciaFim_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV54TFSaldoContrato_Credito) && (Convert.ToDecimal(0)==AV55TFSaldoContrato_Credito_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSALDOCONTRATO_CREDITO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV54TFSaldoContrato_Credito, 18, 5);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV55TFSaldoContrato_Credito_To, 18, 5);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV58TFSaldoContrato_Reservado) && (Convert.ToDecimal(0)==AV59TFSaldoContrato_Reservado_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSALDOCONTRATO_RESERVADO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV58TFSaldoContrato_Reservado, 18, 5);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV59TFSaldoContrato_Reservado_To, 18, 5);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV62TFSaldoContrato_Executado) && (Convert.ToDecimal(0)==AV63TFSaldoContrato_Executado_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSALDOCONTRATO_EXECUTADO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV62TFSaldoContrato_Executado, 18, 5);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV63TFSaldoContrato_Executado_To, 18, 5);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV66TFSaldoContrato_Saldo) && (Convert.ToDecimal(0)==AV67TFSaldoContrato_Saldo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSALDOCONTRATO_SALDO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV66TFSaldoContrato_Saldo, 18, 5);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV67TFSaldoContrato_Saldo_To, 18, 5);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV77Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SALDOCONTRATO_VIGENCIAINICIO") == 0 ) && ! ( (DateTime.MinValue==AV16SaldoContrato_VigenciaInicio1) && (DateTime.MinValue==AV17SaldoContrato_VigenciaInicio_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV16SaldoContrato_VigenciaInicio1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV17SaldoContrato_VigenciaInicio_To1, 2, "/");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SALDOCONTRATO_VIGENCIAINICIO") == 0 ) && ! ( (DateTime.MinValue==AV20SaldoContrato_VigenciaInicio2) && (DateTime.MinValue==AV21SaldoContrato_VigenciaInicio_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV20SaldoContrato_VigenciaInicio2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV21SaldoContrato_VigenciaInicio_To2, 2, "/");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SALDOCONTRATO_VIGENCIAINICIO") == 0 ) && ! ( (DateTime.MinValue==AV24SaldoContrato_VigenciaInicio3) && (DateTime.MinValue==AV25SaldoContrato_VigenciaInicio_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV24SaldoContrato_VigenciaInicio3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV25SaldoContrato_VigenciaInicio_To3, 2, "/");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV77Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "SaldoContrato";
         AV30Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_M52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_M52( true) ;
         }
         else
         {
            wb_table2_8_M52( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_M52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_88_M52( true) ;
         }
         else
         {
            wb_table3_88_M52( false) ;
         }
         return  ;
      }

      protected void wb_table3_88_M52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_M52e( true) ;
         }
         else
         {
            wb_table1_2_M52e( false) ;
         }
      }

      protected void wb_table3_88_M52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_91_M52( true) ;
         }
         else
         {
            wb_table4_91_M52( false) ;
         }
         return  ;
      }

      protected void wb_table4_91_M52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_88_M52e( true) ;
         }
         else
         {
            wb_table3_88_M52e( false) ;
         }
      }

      protected void wb_table4_91_M52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"94\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSaldoContrato_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtSaldoContrato_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSaldoContrato_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSaldoContrato_VigenciaInicio_Titleformat == 0 )
               {
                  context.SendWebValue( edtSaldoContrato_VigenciaInicio_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSaldoContrato_VigenciaInicio_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSaldoContrato_VigenciaFim_Titleformat == 0 )
               {
                  context.SendWebValue( edtSaldoContrato_VigenciaFim_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSaldoContrato_VigenciaFim_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSaldoContrato_Credito_Titleformat == 0 )
               {
                  context.SendWebValue( edtSaldoContrato_Credito_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSaldoContrato_Credito_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSaldoContrato_Reservado_Titleformat == 0 )
               {
                  context.SendWebValue( edtSaldoContrato_Reservado_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSaldoContrato_Reservado_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSaldoContrato_Executado_Titleformat == 0 )
               {
                  context.SendWebValue( edtSaldoContrato_Executado_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSaldoContrato_Executado_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSaldoContrato_Saldo_Titleformat == 0 )
               {
                  context.SendWebValue( edtSaldoContrato_Saldo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSaldoContrato_Saldo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1561SaldoContrato_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSaldoContrato_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSaldoContrato_Codigo_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtSaldoContrato_Codigo_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A1571SaldoContrato_VigenciaInicio, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSaldoContrato_VigenciaInicio_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSaldoContrato_VigenciaInicio_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtSaldoContrato_VigenciaInicio_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A1572SaldoContrato_VigenciaFim, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSaldoContrato_VigenciaFim_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSaldoContrato_VigenciaFim_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1573SaldoContrato_Credito, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSaldoContrato_Credito_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSaldoContrato_Credito_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1574SaldoContrato_Reservado, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSaldoContrato_Reservado_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSaldoContrato_Reservado_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1575SaldoContrato_Executado, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSaldoContrato_Executado_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSaldoContrato_Executado_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1576SaldoContrato_Saldo, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSaldoContrato_Saldo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSaldoContrato_Saldo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 94 )
         {
            wbEnd = 0;
            nRC_GXsfl_94 = (short)(nGXsfl_94_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_91_M52e( true) ;
         }
         else
         {
            wb_table4_91_M52e( false) ;
         }
      }

      protected void wb_table2_8_M52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblSaldocontratotitle_Internalname, "Saldo Contrato", "", "", lblSaldocontratotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_13_M52( true) ;
         }
         else
         {
            wb_table5_13_M52( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_M52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWSaldoContrato.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_M52( true) ;
         }
         else
         {
            wb_table6_23_M52( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_M52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_M52e( true) ;
         }
         else
         {
            wb_table2_8_M52e( false) ;
         }
      }

      protected void wb_table6_23_M52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_M52( true) ;
         }
         else
         {
            wb_table7_28_M52( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_M52e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_M52e( true) ;
         }
         else
         {
            wb_table6_23_M52e( false) ;
         }
      }

      protected void wb_table7_28_M52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWSaldoContrato.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_37_M52( true) ;
         }
         else
         {
            wb_table8_37_M52( false) ;
         }
         return  ;
      }

      protected void wb_table8_37_M52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWSaldoContrato.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", "", true, "HLP_WWSaldoContrato.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_56_M52( true) ;
         }
         else
         {
            wb_table9_56_M52( false) ;
         }
         return  ;
      }

      protected void wb_table9_56_M52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWSaldoContrato.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,71);\"", "", true, "HLP_WWSaldoContrato.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_75_M52( true) ;
         }
         else
         {
            wb_table10_75_M52( false) ;
         }
         return  ;
      }

      protected void wb_table10_75_M52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_M52e( true) ;
         }
         else
         {
            wb_table7_28_M52e( false) ;
         }
      }

      protected void wb_table10_75_M52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterssaldocontrato_vigenciainicio3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterssaldocontrato_vigenciainicio3_Internalname, tblTablemergeddynamicfilterssaldocontrato_vigenciainicio3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavSaldocontrato_vigenciainicio3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavSaldocontrato_vigenciainicio3_Internalname, context.localUtil.Format(AV24SaldoContrato_VigenciaInicio3, "99/99/99"), context.localUtil.Format( AV24SaldoContrato_VigenciaInicio3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,78);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSaldocontrato_vigenciainicio3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSaldoContrato.htm");
            GxWebStd.gx_bitmap( context, edtavSaldocontrato_vigenciainicio3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterssaldocontrato_vigenciainicio_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterssaldocontrato_vigenciainicio_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavSaldocontrato_vigenciainicio_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavSaldocontrato_vigenciainicio_to3_Internalname, context.localUtil.Format(AV25SaldoContrato_VigenciaInicio_To3, "99/99/99"), context.localUtil.Format( AV25SaldoContrato_VigenciaInicio_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,82);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSaldocontrato_vigenciainicio_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSaldoContrato.htm");
            GxWebStd.gx_bitmap( context, edtavSaldocontrato_vigenciainicio_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_75_M52e( true) ;
         }
         else
         {
            wb_table10_75_M52e( false) ;
         }
      }

      protected void wb_table9_56_M52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterssaldocontrato_vigenciainicio2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterssaldocontrato_vigenciainicio2_Internalname, tblTablemergeddynamicfilterssaldocontrato_vigenciainicio2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavSaldocontrato_vigenciainicio2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavSaldocontrato_vigenciainicio2_Internalname, context.localUtil.Format(AV20SaldoContrato_VigenciaInicio2, "99/99/99"), context.localUtil.Format( AV20SaldoContrato_VigenciaInicio2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSaldocontrato_vigenciainicio2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSaldoContrato.htm");
            GxWebStd.gx_bitmap( context, edtavSaldocontrato_vigenciainicio2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterssaldocontrato_vigenciainicio_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterssaldocontrato_vigenciainicio_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavSaldocontrato_vigenciainicio_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavSaldocontrato_vigenciainicio_to2_Internalname, context.localUtil.Format(AV21SaldoContrato_VigenciaInicio_To2, "99/99/99"), context.localUtil.Format( AV21SaldoContrato_VigenciaInicio_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,63);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSaldocontrato_vigenciainicio_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSaldoContrato.htm");
            GxWebStd.gx_bitmap( context, edtavSaldocontrato_vigenciainicio_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_56_M52e( true) ;
         }
         else
         {
            wb_table9_56_M52e( false) ;
         }
      }

      protected void wb_table8_37_M52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterssaldocontrato_vigenciainicio1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterssaldocontrato_vigenciainicio1_Internalname, tblTablemergeddynamicfilterssaldocontrato_vigenciainicio1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavSaldocontrato_vigenciainicio1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavSaldocontrato_vigenciainicio1_Internalname, context.localUtil.Format(AV16SaldoContrato_VigenciaInicio1, "99/99/99"), context.localUtil.Format( AV16SaldoContrato_VigenciaInicio1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSaldocontrato_vigenciainicio1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSaldoContrato.htm");
            GxWebStd.gx_bitmap( context, edtavSaldocontrato_vigenciainicio1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterssaldocontrato_vigenciainicio_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterssaldocontrato_vigenciainicio_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavSaldocontrato_vigenciainicio_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavSaldocontrato_vigenciainicio_to1_Internalname, context.localUtil.Format(AV17SaldoContrato_VigenciaInicio_To1, "99/99/99"), context.localUtil.Format( AV17SaldoContrato_VigenciaInicio_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSaldocontrato_vigenciainicio_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSaldoContrato.htm");
            GxWebStd.gx_bitmap( context, edtavSaldocontrato_vigenciainicio_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_37_M52e( true) ;
         }
         else
         {
            wb_table8_37_M52e( false) ;
         }
      }

      protected void wb_table5_13_M52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWSaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_M52e( true) ;
         }
         else
         {
            wb_table5_13_M52e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAM52( ) ;
         WSM52( ) ;
         WEM52( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202051813334717");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwsaldocontrato.js", "?202051813334717");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_942( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_94_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_94_idx;
         edtSaldoContrato_Codigo_Internalname = "SALDOCONTRATO_CODIGO_"+sGXsfl_94_idx;
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO_"+sGXsfl_94_idx;
         edtSaldoContrato_VigenciaInicio_Internalname = "SALDOCONTRATO_VIGENCIAINICIO_"+sGXsfl_94_idx;
         edtSaldoContrato_VigenciaFim_Internalname = "SALDOCONTRATO_VIGENCIAFIM_"+sGXsfl_94_idx;
         edtSaldoContrato_Credito_Internalname = "SALDOCONTRATO_CREDITO_"+sGXsfl_94_idx;
         edtSaldoContrato_Reservado_Internalname = "SALDOCONTRATO_RESERVADO_"+sGXsfl_94_idx;
         edtSaldoContrato_Executado_Internalname = "SALDOCONTRATO_EXECUTADO_"+sGXsfl_94_idx;
         edtSaldoContrato_Saldo_Internalname = "SALDOCONTRATO_SALDO_"+sGXsfl_94_idx;
      }

      protected void SubsflControlProps_fel_942( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_94_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_94_fel_idx;
         edtSaldoContrato_Codigo_Internalname = "SALDOCONTRATO_CODIGO_"+sGXsfl_94_fel_idx;
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO_"+sGXsfl_94_fel_idx;
         edtSaldoContrato_VigenciaInicio_Internalname = "SALDOCONTRATO_VIGENCIAINICIO_"+sGXsfl_94_fel_idx;
         edtSaldoContrato_VigenciaFim_Internalname = "SALDOCONTRATO_VIGENCIAFIM_"+sGXsfl_94_fel_idx;
         edtSaldoContrato_Credito_Internalname = "SALDOCONTRATO_CREDITO_"+sGXsfl_94_fel_idx;
         edtSaldoContrato_Reservado_Internalname = "SALDOCONTRATO_RESERVADO_"+sGXsfl_94_fel_idx;
         edtSaldoContrato_Executado_Internalname = "SALDOCONTRATO_EXECUTADO_"+sGXsfl_94_fel_idx;
         edtSaldoContrato_Saldo_Internalname = "SALDOCONTRATO_SALDO_"+sGXsfl_94_fel_idx;
      }

      protected void sendrow_942( )
      {
         SubsflControlProps_942( ) ;
         WBM50( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_94_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_94_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_94_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV75Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV75Update_GXI : context.PathToRelativeUrl( AV28Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV76Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV76Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSaldoContrato_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1561SaldoContrato_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1561SaldoContrato_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtSaldoContrato_Codigo_Link,(String)"",(String)"",(String)"",(String)edtSaldoContrato_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSaldoContrato_VigenciaInicio_Internalname,context.localUtil.Format(A1571SaldoContrato_VigenciaInicio, "99/99/99"),context.localUtil.Format( A1571SaldoContrato_VigenciaInicio, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtSaldoContrato_VigenciaInicio_Link,(String)"",(String)"",(String)"",(String)edtSaldoContrato_VigenciaInicio_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSaldoContrato_VigenciaFim_Internalname,context.localUtil.Format(A1572SaldoContrato_VigenciaFim, "99/99/99"),context.localUtil.Format( A1572SaldoContrato_VigenciaFim, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSaldoContrato_VigenciaFim_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSaldoContrato_Credito_Internalname,StringUtil.LTrim( StringUtil.NToC( A1573SaldoContrato_Credito, 18, 5, ",", "")),context.localUtil.Format( A1573SaldoContrato_Credito, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSaldoContrato_Credito_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSaldoContrato_Reservado_Internalname,StringUtil.LTrim( StringUtil.NToC( A1574SaldoContrato_Reservado, 18, 5, ",", "")),context.localUtil.Format( A1574SaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSaldoContrato_Reservado_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSaldoContrato_Executado_Internalname,StringUtil.LTrim( StringUtil.NToC( A1575SaldoContrato_Executado, 18, 5, ",", "")),context.localUtil.Format( A1575SaldoContrato_Executado, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSaldoContrato_Executado_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSaldoContrato_Saldo_Internalname,StringUtil.LTrim( StringUtil.NToC( A1576SaldoContrato_Saldo, 18, 5, ",", "")),context.localUtil.Format( A1576SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSaldoContrato_Saldo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_SALDOCONTRATO_CODIGO"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, context.localUtil.Format( (decimal)(A1561SaldoContrato_Codigo), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_94_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_94_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_94_idx+1));
            sGXsfl_94_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_94_idx), 4, 0)), 4, "0");
            SubsflControlProps_942( ) ;
         }
         /* End function sendrow_942 */
      }

      protected void init_default_properties( )
      {
         lblSaldocontratotitle_Internalname = "SALDOCONTRATOTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavSaldocontrato_vigenciainicio1_Internalname = "vSALDOCONTRATO_VIGENCIAINICIO1";
         lblDynamicfilterssaldocontrato_vigenciainicio_rangemiddletext1_Internalname = "DYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO_RANGEMIDDLETEXT1";
         edtavSaldocontrato_vigenciainicio_to1_Internalname = "vSALDOCONTRATO_VIGENCIAINICIO_TO1";
         tblTablemergeddynamicfilterssaldocontrato_vigenciainicio1_Internalname = "TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavSaldocontrato_vigenciainicio2_Internalname = "vSALDOCONTRATO_VIGENCIAINICIO2";
         lblDynamicfilterssaldocontrato_vigenciainicio_rangemiddletext2_Internalname = "DYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO_RANGEMIDDLETEXT2";
         edtavSaldocontrato_vigenciainicio_to2_Internalname = "vSALDOCONTRATO_VIGENCIAINICIO_TO2";
         tblTablemergeddynamicfilterssaldocontrato_vigenciainicio2_Internalname = "TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavSaldocontrato_vigenciainicio3_Internalname = "vSALDOCONTRATO_VIGENCIAINICIO3";
         lblDynamicfilterssaldocontrato_vigenciainicio_rangemiddletext3_Internalname = "DYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO_RANGEMIDDLETEXT3";
         edtavSaldocontrato_vigenciainicio_to3_Internalname = "vSALDOCONTRATO_VIGENCIAINICIO_TO3";
         tblTablemergeddynamicfilterssaldocontrato_vigenciainicio3_Internalname = "TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtSaldoContrato_Codigo_Internalname = "SALDOCONTRATO_CODIGO";
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO";
         edtSaldoContrato_VigenciaInicio_Internalname = "SALDOCONTRATO_VIGENCIAINICIO";
         edtSaldoContrato_VigenciaFim_Internalname = "SALDOCONTRATO_VIGENCIAFIM";
         edtSaldoContrato_Credito_Internalname = "SALDOCONTRATO_CREDITO";
         edtSaldoContrato_Reservado_Internalname = "SALDOCONTRATO_RESERVADO";
         edtSaldoContrato_Executado_Internalname = "SALDOCONTRATO_EXECUTADO";
         edtSaldoContrato_Saldo_Internalname = "SALDOCONTRATO_SALDO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfsaldocontrato_codigo_Internalname = "vTFSALDOCONTRATO_CODIGO";
         edtavTfsaldocontrato_codigo_to_Internalname = "vTFSALDOCONTRATO_CODIGO_TO";
         edtavTfcontrato_codigo_Internalname = "vTFCONTRATO_CODIGO";
         edtavTfcontrato_codigo_to_Internalname = "vTFCONTRATO_CODIGO_TO";
         edtavTfsaldocontrato_vigenciainicio_Internalname = "vTFSALDOCONTRATO_VIGENCIAINICIO";
         edtavTfsaldocontrato_vigenciainicio_to_Internalname = "vTFSALDOCONTRATO_VIGENCIAINICIO_TO";
         edtavDdo_saldocontrato_vigenciainicioauxdate_Internalname = "vDDO_SALDOCONTRATO_VIGENCIAINICIOAUXDATE";
         edtavDdo_saldocontrato_vigenciainicioauxdateto_Internalname = "vDDO_SALDOCONTRATO_VIGENCIAINICIOAUXDATETO";
         divDdo_saldocontrato_vigenciainicioauxdates_Internalname = "DDO_SALDOCONTRATO_VIGENCIAINICIOAUXDATES";
         edtavTfsaldocontrato_vigenciafim_Internalname = "vTFSALDOCONTRATO_VIGENCIAFIM";
         edtavTfsaldocontrato_vigenciafim_to_Internalname = "vTFSALDOCONTRATO_VIGENCIAFIM_TO";
         edtavDdo_saldocontrato_vigenciafimauxdate_Internalname = "vDDO_SALDOCONTRATO_VIGENCIAFIMAUXDATE";
         edtavDdo_saldocontrato_vigenciafimauxdateto_Internalname = "vDDO_SALDOCONTRATO_VIGENCIAFIMAUXDATETO";
         divDdo_saldocontrato_vigenciafimauxdates_Internalname = "DDO_SALDOCONTRATO_VIGENCIAFIMAUXDATES";
         edtavTfsaldocontrato_credito_Internalname = "vTFSALDOCONTRATO_CREDITO";
         edtavTfsaldocontrato_credito_to_Internalname = "vTFSALDOCONTRATO_CREDITO_TO";
         edtavTfsaldocontrato_reservado_Internalname = "vTFSALDOCONTRATO_RESERVADO";
         edtavTfsaldocontrato_reservado_to_Internalname = "vTFSALDOCONTRATO_RESERVADO_TO";
         edtavTfsaldocontrato_executado_Internalname = "vTFSALDOCONTRATO_EXECUTADO";
         edtavTfsaldocontrato_executado_to_Internalname = "vTFSALDOCONTRATO_EXECUTADO_TO";
         edtavTfsaldocontrato_saldo_Internalname = "vTFSALDOCONTRATO_SALDO";
         edtavTfsaldocontrato_saldo_to_Internalname = "vTFSALDOCONTRATO_SALDO_TO";
         Ddo_saldocontrato_codigo_Internalname = "DDO_SALDOCONTRATO_CODIGO";
         edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Internalname = "vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_contrato_codigo_Internalname = "DDO_CONTRATO_CODIGO";
         edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_saldocontrato_vigenciainicio_Internalname = "DDO_SALDOCONTRATO_VIGENCIAINICIO";
         edtavDdo_saldocontrato_vigenciainiciotitlecontrolidtoreplace_Internalname = "vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE";
         Ddo_saldocontrato_vigenciafim_Internalname = "DDO_SALDOCONTRATO_VIGENCIAFIM";
         edtavDdo_saldocontrato_vigenciafimtitlecontrolidtoreplace_Internalname = "vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE";
         Ddo_saldocontrato_credito_Internalname = "DDO_SALDOCONTRATO_CREDITO";
         edtavDdo_saldocontrato_creditotitlecontrolidtoreplace_Internalname = "vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE";
         Ddo_saldocontrato_reservado_Internalname = "DDO_SALDOCONTRATO_RESERVADO";
         edtavDdo_saldocontrato_reservadotitlecontrolidtoreplace_Internalname = "vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE";
         Ddo_saldocontrato_executado_Internalname = "DDO_SALDOCONTRATO_EXECUTADO";
         edtavDdo_saldocontrato_executadotitlecontrolidtoreplace_Internalname = "vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE";
         Ddo_saldocontrato_saldo_Internalname = "DDO_SALDOCONTRATO_SALDO";
         edtavDdo_saldocontrato_saldotitlecontrolidtoreplace_Internalname = "vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtSaldoContrato_Saldo_Jsonclick = "";
         edtSaldoContrato_Executado_Jsonclick = "";
         edtSaldoContrato_Reservado_Jsonclick = "";
         edtSaldoContrato_Credito_Jsonclick = "";
         edtSaldoContrato_VigenciaFim_Jsonclick = "";
         edtSaldoContrato_VigenciaInicio_Jsonclick = "";
         edtContrato_Codigo_Jsonclick = "";
         edtSaldoContrato_Codigo_Jsonclick = "";
         edtavSaldocontrato_vigenciainicio_to1_Jsonclick = "";
         edtavSaldocontrato_vigenciainicio1_Jsonclick = "";
         edtavSaldocontrato_vigenciainicio_to2_Jsonclick = "";
         edtavSaldocontrato_vigenciainicio2_Jsonclick = "";
         edtavSaldocontrato_vigenciainicio_to3_Jsonclick = "";
         edtavSaldocontrato_vigenciainicio3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtSaldoContrato_VigenciaInicio_Link = "";
         edtSaldoContrato_Codigo_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtSaldoContrato_Saldo_Titleformat = 0;
         edtSaldoContrato_Executado_Titleformat = 0;
         edtSaldoContrato_Reservado_Titleformat = 0;
         edtSaldoContrato_Credito_Titleformat = 0;
         edtSaldoContrato_VigenciaFim_Titleformat = 0;
         edtSaldoContrato_VigenciaInicio_Titleformat = 0;
         edtContrato_Codigo_Titleformat = 0;
         edtSaldoContrato_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         tblTablemergeddynamicfilterssaldocontrato_vigenciainicio3_Visible = 1;
         tblTablemergeddynamicfilterssaldocontrato_vigenciainicio2_Visible = 1;
         tblTablemergeddynamicfilterssaldocontrato_vigenciainicio1_Visible = 1;
         edtSaldoContrato_Saldo_Title = "Saldo";
         edtSaldoContrato_Executado_Title = "Executado";
         edtSaldoContrato_Reservado_Title = "Reservado";
         edtSaldoContrato_Credito_Title = "Cr�dito";
         edtSaldoContrato_VigenciaFim_Title = "Final";
         edtSaldoContrato_VigenciaInicio_Title = "Inicial";
         edtContrato_Codigo_Title = "Contrato";
         edtSaldoContrato_Codigo_Title = "Saldo Contrato";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_saldocontrato_saldotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_saldocontrato_executadotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_saldocontrato_reservadotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_saldocontrato_creditotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_saldocontrato_vigenciafimtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_saldocontrato_vigenciainiciotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfsaldocontrato_saldo_to_Jsonclick = "";
         edtavTfsaldocontrato_saldo_to_Visible = 1;
         edtavTfsaldocontrato_saldo_Jsonclick = "";
         edtavTfsaldocontrato_saldo_Visible = 1;
         edtavTfsaldocontrato_executado_to_Jsonclick = "";
         edtavTfsaldocontrato_executado_to_Visible = 1;
         edtavTfsaldocontrato_executado_Jsonclick = "";
         edtavTfsaldocontrato_executado_Visible = 1;
         edtavTfsaldocontrato_reservado_to_Jsonclick = "";
         edtavTfsaldocontrato_reservado_to_Visible = 1;
         edtavTfsaldocontrato_reservado_Jsonclick = "";
         edtavTfsaldocontrato_reservado_Visible = 1;
         edtavTfsaldocontrato_credito_to_Jsonclick = "";
         edtavTfsaldocontrato_credito_to_Visible = 1;
         edtavTfsaldocontrato_credito_Jsonclick = "";
         edtavTfsaldocontrato_credito_Visible = 1;
         edtavDdo_saldocontrato_vigenciafimauxdateto_Jsonclick = "";
         edtavDdo_saldocontrato_vigenciafimauxdate_Jsonclick = "";
         edtavTfsaldocontrato_vigenciafim_to_Jsonclick = "";
         edtavTfsaldocontrato_vigenciafim_to_Visible = 1;
         edtavTfsaldocontrato_vigenciafim_Jsonclick = "";
         edtavTfsaldocontrato_vigenciafim_Visible = 1;
         edtavDdo_saldocontrato_vigenciainicioauxdateto_Jsonclick = "";
         edtavDdo_saldocontrato_vigenciainicioauxdate_Jsonclick = "";
         edtavTfsaldocontrato_vigenciainicio_to_Jsonclick = "";
         edtavTfsaldocontrato_vigenciainicio_to_Visible = 1;
         edtavTfsaldocontrato_vigenciainicio_Jsonclick = "";
         edtavTfsaldocontrato_vigenciainicio_Visible = 1;
         edtavTfcontrato_codigo_to_Jsonclick = "";
         edtavTfcontrato_codigo_to_Visible = 1;
         edtavTfcontrato_codigo_Jsonclick = "";
         edtavTfcontrato_codigo_Visible = 1;
         edtavTfsaldocontrato_codigo_to_Jsonclick = "";
         edtavTfsaldocontrato_codigo_to_Visible = 1;
         edtavTfsaldocontrato_codigo_Jsonclick = "";
         edtavTfsaldocontrato_codigo_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_saldocontrato_saldo_Searchbuttontext = "Pesquisar";
         Ddo_saldocontrato_saldo_Rangefilterto = "At�";
         Ddo_saldocontrato_saldo_Rangefilterfrom = "Desde";
         Ddo_saldocontrato_saldo_Cleanfilter = "Limpar pesquisa";
         Ddo_saldocontrato_saldo_Sortdsc = "Ordenar de Z � A";
         Ddo_saldocontrato_saldo_Sortasc = "Ordenar de A � Z";
         Ddo_saldocontrato_saldo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_saldocontrato_saldo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_saldocontrato_saldo_Filtertype = "Numeric";
         Ddo_saldocontrato_saldo_Includefilter = Convert.ToBoolean( -1);
         Ddo_saldocontrato_saldo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_saldo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_saldo_Titlecontrolidtoreplace = "";
         Ddo_saldocontrato_saldo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_saldocontrato_saldo_Cls = "ColumnSettings";
         Ddo_saldocontrato_saldo_Tooltip = "Op��es";
         Ddo_saldocontrato_saldo_Caption = "";
         Ddo_saldocontrato_executado_Searchbuttontext = "Pesquisar";
         Ddo_saldocontrato_executado_Rangefilterto = "At�";
         Ddo_saldocontrato_executado_Rangefilterfrom = "Desde";
         Ddo_saldocontrato_executado_Cleanfilter = "Limpar pesquisa";
         Ddo_saldocontrato_executado_Sortdsc = "Ordenar de Z � A";
         Ddo_saldocontrato_executado_Sortasc = "Ordenar de A � Z";
         Ddo_saldocontrato_executado_Includedatalist = Convert.ToBoolean( 0);
         Ddo_saldocontrato_executado_Filterisrange = Convert.ToBoolean( -1);
         Ddo_saldocontrato_executado_Filtertype = "Numeric";
         Ddo_saldocontrato_executado_Includefilter = Convert.ToBoolean( -1);
         Ddo_saldocontrato_executado_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_executado_Includesortasc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_executado_Titlecontrolidtoreplace = "";
         Ddo_saldocontrato_executado_Dropdownoptionstype = "GridTitleSettings";
         Ddo_saldocontrato_executado_Cls = "ColumnSettings";
         Ddo_saldocontrato_executado_Tooltip = "Op��es";
         Ddo_saldocontrato_executado_Caption = "";
         Ddo_saldocontrato_reservado_Searchbuttontext = "Pesquisar";
         Ddo_saldocontrato_reservado_Rangefilterto = "At�";
         Ddo_saldocontrato_reservado_Rangefilterfrom = "Desde";
         Ddo_saldocontrato_reservado_Cleanfilter = "Limpar pesquisa";
         Ddo_saldocontrato_reservado_Sortdsc = "Ordenar de Z � A";
         Ddo_saldocontrato_reservado_Sortasc = "Ordenar de A � Z";
         Ddo_saldocontrato_reservado_Includedatalist = Convert.ToBoolean( 0);
         Ddo_saldocontrato_reservado_Filterisrange = Convert.ToBoolean( -1);
         Ddo_saldocontrato_reservado_Filtertype = "Numeric";
         Ddo_saldocontrato_reservado_Includefilter = Convert.ToBoolean( -1);
         Ddo_saldocontrato_reservado_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_reservado_Includesortasc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_reservado_Titlecontrolidtoreplace = "";
         Ddo_saldocontrato_reservado_Dropdownoptionstype = "GridTitleSettings";
         Ddo_saldocontrato_reservado_Cls = "ColumnSettings";
         Ddo_saldocontrato_reservado_Tooltip = "Op��es";
         Ddo_saldocontrato_reservado_Caption = "";
         Ddo_saldocontrato_credito_Searchbuttontext = "Pesquisar";
         Ddo_saldocontrato_credito_Rangefilterto = "At�";
         Ddo_saldocontrato_credito_Rangefilterfrom = "Desde";
         Ddo_saldocontrato_credito_Cleanfilter = "Limpar pesquisa";
         Ddo_saldocontrato_credito_Sortdsc = "Ordenar de Z � A";
         Ddo_saldocontrato_credito_Sortasc = "Ordenar de A � Z";
         Ddo_saldocontrato_credito_Includedatalist = Convert.ToBoolean( 0);
         Ddo_saldocontrato_credito_Filterisrange = Convert.ToBoolean( -1);
         Ddo_saldocontrato_credito_Filtertype = "Numeric";
         Ddo_saldocontrato_credito_Includefilter = Convert.ToBoolean( -1);
         Ddo_saldocontrato_credito_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_credito_Includesortasc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_credito_Titlecontrolidtoreplace = "";
         Ddo_saldocontrato_credito_Dropdownoptionstype = "GridTitleSettings";
         Ddo_saldocontrato_credito_Cls = "ColumnSettings";
         Ddo_saldocontrato_credito_Tooltip = "Op��es";
         Ddo_saldocontrato_credito_Caption = "";
         Ddo_saldocontrato_vigenciafim_Searchbuttontext = "Pesquisar";
         Ddo_saldocontrato_vigenciafim_Rangefilterto = "At�";
         Ddo_saldocontrato_vigenciafim_Rangefilterfrom = "Desde";
         Ddo_saldocontrato_vigenciafim_Cleanfilter = "Limpar pesquisa";
         Ddo_saldocontrato_vigenciafim_Sortdsc = "Ordenar de Z � A";
         Ddo_saldocontrato_vigenciafim_Sortasc = "Ordenar de A � Z";
         Ddo_saldocontrato_vigenciafim_Includedatalist = Convert.ToBoolean( 0);
         Ddo_saldocontrato_vigenciafim_Filterisrange = Convert.ToBoolean( -1);
         Ddo_saldocontrato_vigenciafim_Filtertype = "Date";
         Ddo_saldocontrato_vigenciafim_Includefilter = Convert.ToBoolean( -1);
         Ddo_saldocontrato_vigenciafim_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_vigenciafim_Includesortasc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_vigenciafim_Titlecontrolidtoreplace = "";
         Ddo_saldocontrato_vigenciafim_Dropdownoptionstype = "GridTitleSettings";
         Ddo_saldocontrato_vigenciafim_Cls = "ColumnSettings";
         Ddo_saldocontrato_vigenciafim_Tooltip = "Op��es";
         Ddo_saldocontrato_vigenciafim_Caption = "";
         Ddo_saldocontrato_vigenciainicio_Searchbuttontext = "Pesquisar";
         Ddo_saldocontrato_vigenciainicio_Rangefilterto = "At�";
         Ddo_saldocontrato_vigenciainicio_Rangefilterfrom = "Desde";
         Ddo_saldocontrato_vigenciainicio_Cleanfilter = "Limpar pesquisa";
         Ddo_saldocontrato_vigenciainicio_Sortdsc = "Ordenar de Z � A";
         Ddo_saldocontrato_vigenciainicio_Sortasc = "Ordenar de A � Z";
         Ddo_saldocontrato_vigenciainicio_Includedatalist = Convert.ToBoolean( 0);
         Ddo_saldocontrato_vigenciainicio_Filterisrange = Convert.ToBoolean( -1);
         Ddo_saldocontrato_vigenciainicio_Filtertype = "Date";
         Ddo_saldocontrato_vigenciainicio_Includefilter = Convert.ToBoolean( -1);
         Ddo_saldocontrato_vigenciainicio_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_vigenciainicio_Includesortasc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_vigenciainicio_Titlecontrolidtoreplace = "";
         Ddo_saldocontrato_vigenciainicio_Dropdownoptionstype = "GridTitleSettings";
         Ddo_saldocontrato_vigenciainicio_Cls = "ColumnSettings";
         Ddo_saldocontrato_vigenciainicio_Tooltip = "Op��es";
         Ddo_saldocontrato_vigenciainicio_Caption = "";
         Ddo_contrato_codigo_Searchbuttontext = "Pesquisar";
         Ddo_contrato_codigo_Rangefilterto = "At�";
         Ddo_contrato_codigo_Rangefilterfrom = "Desde";
         Ddo_contrato_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_contrato_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_contrato_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_contrato_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contrato_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contrato_codigo_Filtertype = "Numeric";
         Ddo_contrato_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_contrato_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contrato_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contrato_codigo_Titlecontrolidtoreplace = "";
         Ddo_contrato_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contrato_codigo_Cls = "ColumnSettings";
         Ddo_contrato_codigo_Tooltip = "Op��es";
         Ddo_contrato_codigo_Caption = "";
         Ddo_saldocontrato_codigo_Searchbuttontext = "Pesquisar";
         Ddo_saldocontrato_codigo_Rangefilterto = "At�";
         Ddo_saldocontrato_codigo_Rangefilterfrom = "Desde";
         Ddo_saldocontrato_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_saldocontrato_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_saldocontrato_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_saldocontrato_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_saldocontrato_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_saldocontrato_codigo_Filtertype = "Numeric";
         Ddo_saldocontrato_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_saldocontrato_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_codigo_Titlecontrolidtoreplace = "";
         Ddo_saldocontrato_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_saldocontrato_codigo_Cls = "ColumnSettings";
         Ddo_saldocontrato_codigo_Tooltip = "Op��es";
         Ddo_saldocontrato_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Saldo Contrato";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''}],oparms:[{av:'AV33SaldoContrato_CodigoTitleFilterData',fld:'vSALDOCONTRATO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV37Contrato_CodigoTitleFilterData',fld:'vCONTRATO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV41SaldoContrato_VigenciaInicioTitleFilterData',fld:'vSALDOCONTRATO_VIGENCIAINICIOTITLEFILTERDATA',pic:'',nv:null},{av:'AV47SaldoContrato_VigenciaFimTitleFilterData',fld:'vSALDOCONTRATO_VIGENCIAFIMTITLEFILTERDATA',pic:'',nv:null},{av:'AV53SaldoContrato_CreditoTitleFilterData',fld:'vSALDOCONTRATO_CREDITOTITLEFILTERDATA',pic:'',nv:null},{av:'AV57SaldoContrato_ReservadoTitleFilterData',fld:'vSALDOCONTRATO_RESERVADOTITLEFILTERDATA',pic:'',nv:null},{av:'AV61SaldoContrato_ExecutadoTitleFilterData',fld:'vSALDOCONTRATO_EXECUTADOTITLEFILTERDATA',pic:'',nv:null},{av:'AV65SaldoContrato_SaldoTitleFilterData',fld:'vSALDOCONTRATO_SALDOTITLEFILTERDATA',pic:'',nv:null},{av:'edtSaldoContrato_Codigo_Titleformat',ctrl:'SALDOCONTRATO_CODIGO',prop:'Titleformat'},{av:'edtSaldoContrato_Codigo_Title',ctrl:'SALDOCONTRATO_CODIGO',prop:'Title'},{av:'edtContrato_Codigo_Titleformat',ctrl:'CONTRATO_CODIGO',prop:'Titleformat'},{av:'edtContrato_Codigo_Title',ctrl:'CONTRATO_CODIGO',prop:'Title'},{av:'edtSaldoContrato_VigenciaInicio_Titleformat',ctrl:'SALDOCONTRATO_VIGENCIAINICIO',prop:'Titleformat'},{av:'edtSaldoContrato_VigenciaInicio_Title',ctrl:'SALDOCONTRATO_VIGENCIAINICIO',prop:'Title'},{av:'edtSaldoContrato_VigenciaFim_Titleformat',ctrl:'SALDOCONTRATO_VIGENCIAFIM',prop:'Titleformat'},{av:'edtSaldoContrato_VigenciaFim_Title',ctrl:'SALDOCONTRATO_VIGENCIAFIM',prop:'Title'},{av:'edtSaldoContrato_Credito_Titleformat',ctrl:'SALDOCONTRATO_CREDITO',prop:'Titleformat'},{av:'edtSaldoContrato_Credito_Title',ctrl:'SALDOCONTRATO_CREDITO',prop:'Title'},{av:'edtSaldoContrato_Reservado_Titleformat',ctrl:'SALDOCONTRATO_RESERVADO',prop:'Titleformat'},{av:'edtSaldoContrato_Reservado_Title',ctrl:'SALDOCONTRATO_RESERVADO',prop:'Title'},{av:'edtSaldoContrato_Executado_Titleformat',ctrl:'SALDOCONTRATO_EXECUTADO',prop:'Titleformat'},{av:'edtSaldoContrato_Executado_Title',ctrl:'SALDOCONTRATO_EXECUTADO',prop:'Title'},{av:'edtSaldoContrato_Saldo_Titleformat',ctrl:'SALDOCONTRATO_SALDO',prop:'Titleformat'},{av:'edtSaldoContrato_Saldo_Title',ctrl:'SALDOCONTRATO_SALDO',prop:'Title'},{av:'AV71GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV72GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11M52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_SALDOCONTRATO_CODIGO.ONOPTIONCLICKED","{handler:'E12M52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_saldocontrato_codigo_Activeeventkey',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_saldocontrato_codigo_Filteredtext_get',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'FilteredText_get'},{av:'Ddo_saldocontrato_codigo_Filteredtextto_get',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_saldocontrato_codigo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_vigenciainicio_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_vigenciafim_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'SortedStatus'},{av:'Ddo_saldocontrato_credito_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_reservado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_executado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_saldo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATO_CODIGO.ONOPTIONCLICKED","{handler:'E13M52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contrato_codigo_Activeeventkey',ctrl:'DDO_CONTRATO_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_contrato_codigo_Filteredtext_get',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredText_get'},{av:'Ddo_contrato_codigo_Filteredtextto_get',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_saldocontrato_codigo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_vigenciainicio_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_vigenciafim_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'SortedStatus'},{av:'Ddo_saldocontrato_credito_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_reservado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_executado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_saldo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SALDOCONTRATO_VIGENCIAINICIO.ONOPTIONCLICKED","{handler:'E14M52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_saldocontrato_vigenciainicio_Activeeventkey',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'ActiveEventKey'},{av:'Ddo_saldocontrato_vigenciainicio_Filteredtext_get',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'FilteredText_get'},{av:'Ddo_saldocontrato_vigenciainicio_Filteredtextto_get',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_saldocontrato_vigenciainicio_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'Ddo_saldocontrato_codigo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_vigenciafim_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'SortedStatus'},{av:'Ddo_saldocontrato_credito_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_reservado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_executado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_saldo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SALDOCONTRATO_VIGENCIAFIM.ONOPTIONCLICKED","{handler:'E15M52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_saldocontrato_vigenciafim_Activeeventkey',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'ActiveEventKey'},{av:'Ddo_saldocontrato_vigenciafim_Filteredtext_get',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'FilteredText_get'},{av:'Ddo_saldocontrato_vigenciafim_Filteredtextto_get',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_saldocontrato_vigenciafim_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'SortedStatus'},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'Ddo_saldocontrato_codigo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_vigenciainicio_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_credito_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_reservado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_executado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_saldo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SALDOCONTRATO_CREDITO.ONOPTIONCLICKED","{handler:'E16M52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_saldocontrato_credito_Activeeventkey',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'ActiveEventKey'},{av:'Ddo_saldocontrato_credito_Filteredtext_get',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'FilteredText_get'},{av:'Ddo_saldocontrato_credito_Filteredtextto_get',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_saldocontrato_credito_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'SortedStatus'},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_saldocontrato_codigo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_vigenciainicio_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_vigenciafim_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'SortedStatus'},{av:'Ddo_saldocontrato_reservado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_executado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_saldo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SALDOCONTRATO_RESERVADO.ONOPTIONCLICKED","{handler:'E17M52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_saldocontrato_reservado_Activeeventkey',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'ActiveEventKey'},{av:'Ddo_saldocontrato_reservado_Filteredtext_get',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'FilteredText_get'},{av:'Ddo_saldocontrato_reservado_Filteredtextto_get',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_saldocontrato_reservado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'SortedStatus'},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_saldocontrato_codigo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_vigenciainicio_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_vigenciafim_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'SortedStatus'},{av:'Ddo_saldocontrato_credito_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_executado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_saldo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SALDOCONTRATO_EXECUTADO.ONOPTIONCLICKED","{handler:'E18M52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_saldocontrato_executado_Activeeventkey',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'ActiveEventKey'},{av:'Ddo_saldocontrato_executado_Filteredtext_get',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'FilteredText_get'},{av:'Ddo_saldocontrato_executado_Filteredtextto_get',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_saldocontrato_executado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'SortedStatus'},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_saldocontrato_codigo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_vigenciainicio_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_vigenciafim_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'SortedStatus'},{av:'Ddo_saldocontrato_credito_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_reservado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_saldo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SALDOCONTRATO_SALDO.ONOPTIONCLICKED","{handler:'E19M52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_saldocontrato_saldo_Activeeventkey',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'ActiveEventKey'},{av:'Ddo_saldocontrato_saldo_Filteredtext_get',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'FilteredText_get'},{av:'Ddo_saldocontrato_saldo_Filteredtextto_get',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_saldocontrato_saldo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'SortedStatus'},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_saldocontrato_codigo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_vigenciainicio_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_vigenciafim_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'SortedStatus'},{av:'Ddo_saldocontrato_credito_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_reservado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_executado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E33M52',iparms:[{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtSaldoContrato_Codigo_Link',ctrl:'SALDOCONTRATO_CODIGO',prop:'Link'},{av:'edtSaldoContrato_VigenciaInicio_Link',ctrl:'SALDOCONTRATO_VIGENCIAINICIO',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E20M52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E26M52',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E21M52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfilterssaldocontrato_vigenciainicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterssaldocontrato_vigenciainicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterssaldocontrato_vigenciainicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E27M52',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterssaldocontrato_vigenciainicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E28M52',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E22M52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfilterssaldocontrato_vigenciainicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterssaldocontrato_vigenciainicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterssaldocontrato_vigenciainicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E29M52',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterssaldocontrato_vigenciainicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E23M52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfilterssaldocontrato_vigenciainicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterssaldocontrato_vigenciainicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterssaldocontrato_vigenciainicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E30M52',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterssaldocontrato_vigenciainicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E24M52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_saldocontrato_codigo_Filteredtext_set',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'FilteredText_set'},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_saldocontrato_codigo_Filteredtextto_set',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contrato_codigo_Filteredtext_set',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredText_set'},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contrato_codigo_Filteredtextto_set',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'Ddo_saldocontrato_vigenciainicio_Filteredtext_set',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'FilteredText_set'},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'Ddo_saldocontrato_vigenciainicio_Filteredtextto_set',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'FilteredTextTo_set'},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'Ddo_saldocontrato_vigenciafim_Filteredtext_set',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'FilteredText_set'},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'Ddo_saldocontrato_vigenciafim_Filteredtextto_set',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'FilteredTextTo_set'},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_saldocontrato_credito_Filteredtext_set',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'FilteredText_set'},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_saldocontrato_credito_Filteredtextto_set',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'FilteredTextTo_set'},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_saldocontrato_reservado_Filteredtext_set',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'FilteredText_set'},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_saldocontrato_reservado_Filteredtextto_set',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'FilteredTextTo_set'},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_saldocontrato_executado_Filteredtext_set',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'FilteredText_set'},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_saldocontrato_executado_Filteredtextto_set',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'FilteredTextTo_set'},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_saldocontrato_saldo_Filteredtext_set',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'FilteredText_set'},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_saldocontrato_saldo_Filteredtextto_set',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'tblTablemergeddynamicfilterssaldocontrato_vigenciainicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO1',prop:'Visible'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfilterssaldocontrato_vigenciainicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterssaldocontrato_vigenciainicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO3',prop:'Visible'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E25M52',iparms:[{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_saldocontrato_codigo_Activeeventkey = "";
         Ddo_saldocontrato_codigo_Filteredtext_get = "";
         Ddo_saldocontrato_codigo_Filteredtextto_get = "";
         Ddo_contrato_codigo_Activeeventkey = "";
         Ddo_contrato_codigo_Filteredtext_get = "";
         Ddo_contrato_codigo_Filteredtextto_get = "";
         Ddo_saldocontrato_vigenciainicio_Activeeventkey = "";
         Ddo_saldocontrato_vigenciainicio_Filteredtext_get = "";
         Ddo_saldocontrato_vigenciainicio_Filteredtextto_get = "";
         Ddo_saldocontrato_vigenciafim_Activeeventkey = "";
         Ddo_saldocontrato_vigenciafim_Filteredtext_get = "";
         Ddo_saldocontrato_vigenciafim_Filteredtextto_get = "";
         Ddo_saldocontrato_credito_Activeeventkey = "";
         Ddo_saldocontrato_credito_Filteredtext_get = "";
         Ddo_saldocontrato_credito_Filteredtextto_get = "";
         Ddo_saldocontrato_reservado_Activeeventkey = "";
         Ddo_saldocontrato_reservado_Filteredtext_get = "";
         Ddo_saldocontrato_reservado_Filteredtextto_get = "";
         Ddo_saldocontrato_executado_Activeeventkey = "";
         Ddo_saldocontrato_executado_Filteredtext_get = "";
         Ddo_saldocontrato_executado_Filteredtextto_get = "";
         Ddo_saldocontrato_saldo_Activeeventkey = "";
         Ddo_saldocontrato_saldo_Filteredtext_get = "";
         Ddo_saldocontrato_saldo_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV16SaldoContrato_VigenciaInicio1 = DateTime.MinValue;
         AV17SaldoContrato_VigenciaInicio_To1 = DateTime.MinValue;
         AV19DynamicFiltersSelector2 = "";
         AV20SaldoContrato_VigenciaInicio2 = DateTime.MinValue;
         AV21SaldoContrato_VigenciaInicio_To2 = DateTime.MinValue;
         AV23DynamicFiltersSelector3 = "";
         AV24SaldoContrato_VigenciaInicio3 = DateTime.MinValue;
         AV25SaldoContrato_VigenciaInicio_To3 = DateTime.MinValue;
         AV42TFSaldoContrato_VigenciaInicio = DateTime.MinValue;
         AV43TFSaldoContrato_VigenciaInicio_To = DateTime.MinValue;
         AV48TFSaldoContrato_VigenciaFim = DateTime.MinValue;
         AV49TFSaldoContrato_VigenciaFim_To = DateTime.MinValue;
         AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace = "";
         AV40ddo_Contrato_CodigoTitleControlIdToReplace = "";
         AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace = "";
         AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace = "";
         AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace = "";
         AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace = "";
         AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace = "";
         AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace = "";
         AV77Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV69DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV33SaldoContrato_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV37Contrato_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV41SaldoContrato_VigenciaInicioTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV47SaldoContrato_VigenciaFimTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV53SaldoContrato_CreditoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV57SaldoContrato_ReservadoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV61SaldoContrato_ExecutadoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV65SaldoContrato_SaldoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_saldocontrato_codigo_Filteredtext_set = "";
         Ddo_saldocontrato_codigo_Filteredtextto_set = "";
         Ddo_saldocontrato_codigo_Sortedstatus = "";
         Ddo_contrato_codigo_Filteredtext_set = "";
         Ddo_contrato_codigo_Filteredtextto_set = "";
         Ddo_contrato_codigo_Sortedstatus = "";
         Ddo_saldocontrato_vigenciainicio_Filteredtext_set = "";
         Ddo_saldocontrato_vigenciainicio_Filteredtextto_set = "";
         Ddo_saldocontrato_vigenciainicio_Sortedstatus = "";
         Ddo_saldocontrato_vigenciafim_Filteredtext_set = "";
         Ddo_saldocontrato_vigenciafim_Filteredtextto_set = "";
         Ddo_saldocontrato_vigenciafim_Sortedstatus = "";
         Ddo_saldocontrato_credito_Filteredtext_set = "";
         Ddo_saldocontrato_credito_Filteredtextto_set = "";
         Ddo_saldocontrato_credito_Sortedstatus = "";
         Ddo_saldocontrato_reservado_Filteredtext_set = "";
         Ddo_saldocontrato_reservado_Filteredtextto_set = "";
         Ddo_saldocontrato_reservado_Sortedstatus = "";
         Ddo_saldocontrato_executado_Filteredtext_set = "";
         Ddo_saldocontrato_executado_Filteredtextto_set = "";
         Ddo_saldocontrato_executado_Sortedstatus = "";
         Ddo_saldocontrato_saldo_Filteredtext_set = "";
         Ddo_saldocontrato_saldo_Filteredtextto_set = "";
         Ddo_saldocontrato_saldo_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV44DDO_SaldoContrato_VigenciaInicioAuxDate = DateTime.MinValue;
         AV45DDO_SaldoContrato_VigenciaInicioAuxDateTo = DateTime.MinValue;
         AV50DDO_SaldoContrato_VigenciaFimAuxDate = DateTime.MinValue;
         AV51DDO_SaldoContrato_VigenciaFimAuxDateTo = DateTime.MinValue;
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Update = "";
         AV75Update_GXI = "";
         AV29Delete = "";
         AV76Delete_GXI = "";
         A1571SaldoContrato_VigenciaInicio = DateTime.MinValue;
         A1572SaldoContrato_VigenciaFim = DateTime.MinValue;
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00M52_A1560NotaEmpenho_Codigo = new int[1] ;
         H00M52_A1576SaldoContrato_Saldo = new decimal[1] ;
         H00M52_A1575SaldoContrato_Executado = new decimal[1] ;
         H00M52_A1574SaldoContrato_Reservado = new decimal[1] ;
         H00M52_A1573SaldoContrato_Credito = new decimal[1] ;
         H00M52_A1572SaldoContrato_VigenciaFim = new DateTime[] {DateTime.MinValue} ;
         H00M52_A1571SaldoContrato_VigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         H00M52_A74Contrato_Codigo = new int[1] ;
         H00M52_A1561SaldoContrato_Codigo = new int[1] ;
         H00M53_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV30Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblSaldocontratotitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfilterssaldocontrato_vigenciainicio_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterssaldocontrato_vigenciainicio_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterssaldocontrato_vigenciainicio_rangemiddletext1_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwsaldocontrato__default(),
            new Object[][] {
                new Object[] {
               H00M52_A1560NotaEmpenho_Codigo, H00M52_A1576SaldoContrato_Saldo, H00M52_A1575SaldoContrato_Executado, H00M52_A1574SaldoContrato_Reservado, H00M52_A1573SaldoContrato_Credito, H00M52_A1572SaldoContrato_VigenciaFim, H00M52_A1571SaldoContrato_VigenciaInicio, H00M52_A74Contrato_Codigo, H00M52_A1561SaldoContrato_Codigo
               }
               , new Object[] {
               H00M53_AGRID_nRecordCount
               }
            }
         );
         AV77Pgmname = "WWSaldoContrato";
         /* GeneXus formulas. */
         AV77Pgmname = "WWSaldoContrato";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_94 ;
      private short nGXsfl_94_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_94_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtSaldoContrato_Codigo_Titleformat ;
      private short edtContrato_Codigo_Titleformat ;
      private short edtSaldoContrato_VigenciaInicio_Titleformat ;
      private short edtSaldoContrato_VigenciaFim_Titleformat ;
      private short edtSaldoContrato_Credito_Titleformat ;
      private short edtSaldoContrato_Reservado_Titleformat ;
      private short edtSaldoContrato_Executado_Titleformat ;
      private short edtSaldoContrato_Saldo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV34TFSaldoContrato_Codigo ;
      private int AV35TFSaldoContrato_Codigo_To ;
      private int AV38TFContrato_Codigo ;
      private int AV39TFContrato_Codigo_To ;
      private int A1561SaldoContrato_Codigo ;
      private int A1560NotaEmpenho_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int edtavTfsaldocontrato_codigo_Visible ;
      private int edtavTfsaldocontrato_codigo_to_Visible ;
      private int edtavTfcontrato_codigo_Visible ;
      private int edtavTfcontrato_codigo_to_Visible ;
      private int edtavTfsaldocontrato_vigenciainicio_Visible ;
      private int edtavTfsaldocontrato_vigenciainicio_to_Visible ;
      private int edtavTfsaldocontrato_vigenciafim_Visible ;
      private int edtavTfsaldocontrato_vigenciafim_to_Visible ;
      private int edtavTfsaldocontrato_credito_Visible ;
      private int edtavTfsaldocontrato_credito_to_Visible ;
      private int edtavTfsaldocontrato_reservado_Visible ;
      private int edtavTfsaldocontrato_reservado_to_Visible ;
      private int edtavTfsaldocontrato_executado_Visible ;
      private int edtavTfsaldocontrato_executado_to_Visible ;
      private int edtavTfsaldocontrato_saldo_Visible ;
      private int edtavTfsaldocontrato_saldo_to_Visible ;
      private int edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_saldocontrato_vigenciainiciotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_saldocontrato_vigenciafimtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_saldocontrato_creditotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_saldocontrato_reservadotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_saldocontrato_executadotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_saldocontrato_saldotitlecontrolidtoreplace_Visible ;
      private int A74Contrato_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV70PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int tblTablemergeddynamicfilterssaldocontrato_vigenciainicio1_Visible ;
      private int tblTablemergeddynamicfilterssaldocontrato_vigenciainicio2_Visible ;
      private int tblTablemergeddynamicfilterssaldocontrato_vigenciainicio3_Visible ;
      private int AV78GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV71GridCurrentPage ;
      private long AV72GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV54TFSaldoContrato_Credito ;
      private decimal AV55TFSaldoContrato_Credito_To ;
      private decimal AV58TFSaldoContrato_Reservado ;
      private decimal AV59TFSaldoContrato_Reservado_To ;
      private decimal AV62TFSaldoContrato_Executado ;
      private decimal AV63TFSaldoContrato_Executado_To ;
      private decimal AV66TFSaldoContrato_Saldo ;
      private decimal AV67TFSaldoContrato_Saldo_To ;
      private decimal A1573SaldoContrato_Credito ;
      private decimal A1574SaldoContrato_Reservado ;
      private decimal A1575SaldoContrato_Executado ;
      private decimal A1576SaldoContrato_Saldo ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_saldocontrato_codigo_Activeeventkey ;
      private String Ddo_saldocontrato_codigo_Filteredtext_get ;
      private String Ddo_saldocontrato_codigo_Filteredtextto_get ;
      private String Ddo_contrato_codigo_Activeeventkey ;
      private String Ddo_contrato_codigo_Filteredtext_get ;
      private String Ddo_contrato_codigo_Filteredtextto_get ;
      private String Ddo_saldocontrato_vigenciainicio_Activeeventkey ;
      private String Ddo_saldocontrato_vigenciainicio_Filteredtext_get ;
      private String Ddo_saldocontrato_vigenciainicio_Filteredtextto_get ;
      private String Ddo_saldocontrato_vigenciafim_Activeeventkey ;
      private String Ddo_saldocontrato_vigenciafim_Filteredtext_get ;
      private String Ddo_saldocontrato_vigenciafim_Filteredtextto_get ;
      private String Ddo_saldocontrato_credito_Activeeventkey ;
      private String Ddo_saldocontrato_credito_Filteredtext_get ;
      private String Ddo_saldocontrato_credito_Filteredtextto_get ;
      private String Ddo_saldocontrato_reservado_Activeeventkey ;
      private String Ddo_saldocontrato_reservado_Filteredtext_get ;
      private String Ddo_saldocontrato_reservado_Filteredtextto_get ;
      private String Ddo_saldocontrato_executado_Activeeventkey ;
      private String Ddo_saldocontrato_executado_Filteredtext_get ;
      private String Ddo_saldocontrato_executado_Filteredtextto_get ;
      private String Ddo_saldocontrato_saldo_Activeeventkey ;
      private String Ddo_saldocontrato_saldo_Filteredtext_get ;
      private String Ddo_saldocontrato_saldo_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_94_idx="0001" ;
      private String AV77Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_saldocontrato_codigo_Caption ;
      private String Ddo_saldocontrato_codigo_Tooltip ;
      private String Ddo_saldocontrato_codigo_Cls ;
      private String Ddo_saldocontrato_codigo_Filteredtext_set ;
      private String Ddo_saldocontrato_codigo_Filteredtextto_set ;
      private String Ddo_saldocontrato_codigo_Dropdownoptionstype ;
      private String Ddo_saldocontrato_codigo_Titlecontrolidtoreplace ;
      private String Ddo_saldocontrato_codigo_Sortedstatus ;
      private String Ddo_saldocontrato_codigo_Filtertype ;
      private String Ddo_saldocontrato_codigo_Sortasc ;
      private String Ddo_saldocontrato_codigo_Sortdsc ;
      private String Ddo_saldocontrato_codigo_Cleanfilter ;
      private String Ddo_saldocontrato_codigo_Rangefilterfrom ;
      private String Ddo_saldocontrato_codigo_Rangefilterto ;
      private String Ddo_saldocontrato_codigo_Searchbuttontext ;
      private String Ddo_contrato_codigo_Caption ;
      private String Ddo_contrato_codigo_Tooltip ;
      private String Ddo_contrato_codigo_Cls ;
      private String Ddo_contrato_codigo_Filteredtext_set ;
      private String Ddo_contrato_codigo_Filteredtextto_set ;
      private String Ddo_contrato_codigo_Dropdownoptionstype ;
      private String Ddo_contrato_codigo_Titlecontrolidtoreplace ;
      private String Ddo_contrato_codigo_Sortedstatus ;
      private String Ddo_contrato_codigo_Filtertype ;
      private String Ddo_contrato_codigo_Sortasc ;
      private String Ddo_contrato_codigo_Sortdsc ;
      private String Ddo_contrato_codigo_Cleanfilter ;
      private String Ddo_contrato_codigo_Rangefilterfrom ;
      private String Ddo_contrato_codigo_Rangefilterto ;
      private String Ddo_contrato_codigo_Searchbuttontext ;
      private String Ddo_saldocontrato_vigenciainicio_Caption ;
      private String Ddo_saldocontrato_vigenciainicio_Tooltip ;
      private String Ddo_saldocontrato_vigenciainicio_Cls ;
      private String Ddo_saldocontrato_vigenciainicio_Filteredtext_set ;
      private String Ddo_saldocontrato_vigenciainicio_Filteredtextto_set ;
      private String Ddo_saldocontrato_vigenciainicio_Dropdownoptionstype ;
      private String Ddo_saldocontrato_vigenciainicio_Titlecontrolidtoreplace ;
      private String Ddo_saldocontrato_vigenciainicio_Sortedstatus ;
      private String Ddo_saldocontrato_vigenciainicio_Filtertype ;
      private String Ddo_saldocontrato_vigenciainicio_Sortasc ;
      private String Ddo_saldocontrato_vigenciainicio_Sortdsc ;
      private String Ddo_saldocontrato_vigenciainicio_Cleanfilter ;
      private String Ddo_saldocontrato_vigenciainicio_Rangefilterfrom ;
      private String Ddo_saldocontrato_vigenciainicio_Rangefilterto ;
      private String Ddo_saldocontrato_vigenciainicio_Searchbuttontext ;
      private String Ddo_saldocontrato_vigenciafim_Caption ;
      private String Ddo_saldocontrato_vigenciafim_Tooltip ;
      private String Ddo_saldocontrato_vigenciafim_Cls ;
      private String Ddo_saldocontrato_vigenciafim_Filteredtext_set ;
      private String Ddo_saldocontrato_vigenciafim_Filteredtextto_set ;
      private String Ddo_saldocontrato_vigenciafim_Dropdownoptionstype ;
      private String Ddo_saldocontrato_vigenciafim_Titlecontrolidtoreplace ;
      private String Ddo_saldocontrato_vigenciafim_Sortedstatus ;
      private String Ddo_saldocontrato_vigenciafim_Filtertype ;
      private String Ddo_saldocontrato_vigenciafim_Sortasc ;
      private String Ddo_saldocontrato_vigenciafim_Sortdsc ;
      private String Ddo_saldocontrato_vigenciafim_Cleanfilter ;
      private String Ddo_saldocontrato_vigenciafim_Rangefilterfrom ;
      private String Ddo_saldocontrato_vigenciafim_Rangefilterto ;
      private String Ddo_saldocontrato_vigenciafim_Searchbuttontext ;
      private String Ddo_saldocontrato_credito_Caption ;
      private String Ddo_saldocontrato_credito_Tooltip ;
      private String Ddo_saldocontrato_credito_Cls ;
      private String Ddo_saldocontrato_credito_Filteredtext_set ;
      private String Ddo_saldocontrato_credito_Filteredtextto_set ;
      private String Ddo_saldocontrato_credito_Dropdownoptionstype ;
      private String Ddo_saldocontrato_credito_Titlecontrolidtoreplace ;
      private String Ddo_saldocontrato_credito_Sortedstatus ;
      private String Ddo_saldocontrato_credito_Filtertype ;
      private String Ddo_saldocontrato_credito_Sortasc ;
      private String Ddo_saldocontrato_credito_Sortdsc ;
      private String Ddo_saldocontrato_credito_Cleanfilter ;
      private String Ddo_saldocontrato_credito_Rangefilterfrom ;
      private String Ddo_saldocontrato_credito_Rangefilterto ;
      private String Ddo_saldocontrato_credito_Searchbuttontext ;
      private String Ddo_saldocontrato_reservado_Caption ;
      private String Ddo_saldocontrato_reservado_Tooltip ;
      private String Ddo_saldocontrato_reservado_Cls ;
      private String Ddo_saldocontrato_reservado_Filteredtext_set ;
      private String Ddo_saldocontrato_reservado_Filteredtextto_set ;
      private String Ddo_saldocontrato_reservado_Dropdownoptionstype ;
      private String Ddo_saldocontrato_reservado_Titlecontrolidtoreplace ;
      private String Ddo_saldocontrato_reservado_Sortedstatus ;
      private String Ddo_saldocontrato_reservado_Filtertype ;
      private String Ddo_saldocontrato_reservado_Sortasc ;
      private String Ddo_saldocontrato_reservado_Sortdsc ;
      private String Ddo_saldocontrato_reservado_Cleanfilter ;
      private String Ddo_saldocontrato_reservado_Rangefilterfrom ;
      private String Ddo_saldocontrato_reservado_Rangefilterto ;
      private String Ddo_saldocontrato_reservado_Searchbuttontext ;
      private String Ddo_saldocontrato_executado_Caption ;
      private String Ddo_saldocontrato_executado_Tooltip ;
      private String Ddo_saldocontrato_executado_Cls ;
      private String Ddo_saldocontrato_executado_Filteredtext_set ;
      private String Ddo_saldocontrato_executado_Filteredtextto_set ;
      private String Ddo_saldocontrato_executado_Dropdownoptionstype ;
      private String Ddo_saldocontrato_executado_Titlecontrolidtoreplace ;
      private String Ddo_saldocontrato_executado_Sortedstatus ;
      private String Ddo_saldocontrato_executado_Filtertype ;
      private String Ddo_saldocontrato_executado_Sortasc ;
      private String Ddo_saldocontrato_executado_Sortdsc ;
      private String Ddo_saldocontrato_executado_Cleanfilter ;
      private String Ddo_saldocontrato_executado_Rangefilterfrom ;
      private String Ddo_saldocontrato_executado_Rangefilterto ;
      private String Ddo_saldocontrato_executado_Searchbuttontext ;
      private String Ddo_saldocontrato_saldo_Caption ;
      private String Ddo_saldocontrato_saldo_Tooltip ;
      private String Ddo_saldocontrato_saldo_Cls ;
      private String Ddo_saldocontrato_saldo_Filteredtext_set ;
      private String Ddo_saldocontrato_saldo_Filteredtextto_set ;
      private String Ddo_saldocontrato_saldo_Dropdownoptionstype ;
      private String Ddo_saldocontrato_saldo_Titlecontrolidtoreplace ;
      private String Ddo_saldocontrato_saldo_Sortedstatus ;
      private String Ddo_saldocontrato_saldo_Filtertype ;
      private String Ddo_saldocontrato_saldo_Sortasc ;
      private String Ddo_saldocontrato_saldo_Sortdsc ;
      private String Ddo_saldocontrato_saldo_Cleanfilter ;
      private String Ddo_saldocontrato_saldo_Rangefilterfrom ;
      private String Ddo_saldocontrato_saldo_Rangefilterto ;
      private String Ddo_saldocontrato_saldo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfsaldocontrato_codigo_Internalname ;
      private String edtavTfsaldocontrato_codigo_Jsonclick ;
      private String edtavTfsaldocontrato_codigo_to_Internalname ;
      private String edtavTfsaldocontrato_codigo_to_Jsonclick ;
      private String edtavTfcontrato_codigo_Internalname ;
      private String edtavTfcontrato_codigo_Jsonclick ;
      private String edtavTfcontrato_codigo_to_Internalname ;
      private String edtavTfcontrato_codigo_to_Jsonclick ;
      private String edtavTfsaldocontrato_vigenciainicio_Internalname ;
      private String edtavTfsaldocontrato_vigenciainicio_Jsonclick ;
      private String edtavTfsaldocontrato_vigenciainicio_to_Internalname ;
      private String edtavTfsaldocontrato_vigenciainicio_to_Jsonclick ;
      private String divDdo_saldocontrato_vigenciainicioauxdates_Internalname ;
      private String edtavDdo_saldocontrato_vigenciainicioauxdate_Internalname ;
      private String edtavDdo_saldocontrato_vigenciainicioauxdate_Jsonclick ;
      private String edtavDdo_saldocontrato_vigenciainicioauxdateto_Internalname ;
      private String edtavDdo_saldocontrato_vigenciainicioauxdateto_Jsonclick ;
      private String edtavTfsaldocontrato_vigenciafim_Internalname ;
      private String edtavTfsaldocontrato_vigenciafim_Jsonclick ;
      private String edtavTfsaldocontrato_vigenciafim_to_Internalname ;
      private String edtavTfsaldocontrato_vigenciafim_to_Jsonclick ;
      private String divDdo_saldocontrato_vigenciafimauxdates_Internalname ;
      private String edtavDdo_saldocontrato_vigenciafimauxdate_Internalname ;
      private String edtavDdo_saldocontrato_vigenciafimauxdate_Jsonclick ;
      private String edtavDdo_saldocontrato_vigenciafimauxdateto_Internalname ;
      private String edtavDdo_saldocontrato_vigenciafimauxdateto_Jsonclick ;
      private String edtavTfsaldocontrato_credito_Internalname ;
      private String edtavTfsaldocontrato_credito_Jsonclick ;
      private String edtavTfsaldocontrato_credito_to_Internalname ;
      private String edtavTfsaldocontrato_credito_to_Jsonclick ;
      private String edtavTfsaldocontrato_reservado_Internalname ;
      private String edtavTfsaldocontrato_reservado_Jsonclick ;
      private String edtavTfsaldocontrato_reservado_to_Internalname ;
      private String edtavTfsaldocontrato_reservado_to_Jsonclick ;
      private String edtavTfsaldocontrato_executado_Internalname ;
      private String edtavTfsaldocontrato_executado_Jsonclick ;
      private String edtavTfsaldocontrato_executado_to_Internalname ;
      private String edtavTfsaldocontrato_executado_to_Jsonclick ;
      private String edtavTfsaldocontrato_saldo_Internalname ;
      private String edtavTfsaldocontrato_saldo_Jsonclick ;
      private String edtavTfsaldocontrato_saldo_to_Internalname ;
      private String edtavTfsaldocontrato_saldo_to_Jsonclick ;
      private String edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_saldocontrato_vigenciainiciotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_saldocontrato_vigenciafimtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_saldocontrato_creditotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_saldocontrato_reservadotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_saldocontrato_executadotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_saldocontrato_saldotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtSaldoContrato_Codigo_Internalname ;
      private String edtContrato_Codigo_Internalname ;
      private String edtSaldoContrato_VigenciaInicio_Internalname ;
      private String edtSaldoContrato_VigenciaFim_Internalname ;
      private String edtSaldoContrato_Credito_Internalname ;
      private String edtSaldoContrato_Reservado_Internalname ;
      private String edtSaldoContrato_Executado_Internalname ;
      private String edtSaldoContrato_Saldo_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavSaldocontrato_vigenciainicio1_Internalname ;
      private String edtavSaldocontrato_vigenciainicio_to1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavSaldocontrato_vigenciainicio2_Internalname ;
      private String edtavSaldocontrato_vigenciainicio_to2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavSaldocontrato_vigenciainicio3_Internalname ;
      private String edtavSaldocontrato_vigenciainicio_to3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_saldocontrato_codigo_Internalname ;
      private String Ddo_contrato_codigo_Internalname ;
      private String Ddo_saldocontrato_vigenciainicio_Internalname ;
      private String Ddo_saldocontrato_vigenciafim_Internalname ;
      private String Ddo_saldocontrato_credito_Internalname ;
      private String Ddo_saldocontrato_reservado_Internalname ;
      private String Ddo_saldocontrato_executado_Internalname ;
      private String Ddo_saldocontrato_saldo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtSaldoContrato_Codigo_Title ;
      private String edtContrato_Codigo_Title ;
      private String edtSaldoContrato_VigenciaInicio_Title ;
      private String edtSaldoContrato_VigenciaFim_Title ;
      private String edtSaldoContrato_Credito_Title ;
      private String edtSaldoContrato_Reservado_Title ;
      private String edtSaldoContrato_Executado_Title ;
      private String edtSaldoContrato_Saldo_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtSaldoContrato_Codigo_Link ;
      private String edtSaldoContrato_VigenciaInicio_Link ;
      private String tblTablemergeddynamicfilterssaldocontrato_vigenciainicio1_Internalname ;
      private String tblTablemergeddynamicfilterssaldocontrato_vigenciainicio2_Internalname ;
      private String tblTablemergeddynamicfilterssaldocontrato_vigenciainicio3_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblSaldocontratotitle_Internalname ;
      private String lblSaldocontratotitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavSaldocontrato_vigenciainicio3_Jsonclick ;
      private String lblDynamicfilterssaldocontrato_vigenciainicio_rangemiddletext3_Internalname ;
      private String lblDynamicfilterssaldocontrato_vigenciainicio_rangemiddletext3_Jsonclick ;
      private String edtavSaldocontrato_vigenciainicio_to3_Jsonclick ;
      private String edtavSaldocontrato_vigenciainicio2_Jsonclick ;
      private String lblDynamicfilterssaldocontrato_vigenciainicio_rangemiddletext2_Internalname ;
      private String lblDynamicfilterssaldocontrato_vigenciainicio_rangemiddletext2_Jsonclick ;
      private String edtavSaldocontrato_vigenciainicio_to2_Jsonclick ;
      private String edtavSaldocontrato_vigenciainicio1_Jsonclick ;
      private String lblDynamicfilterssaldocontrato_vigenciainicio_rangemiddletext1_Internalname ;
      private String lblDynamicfilterssaldocontrato_vigenciainicio_rangemiddletext1_Jsonclick ;
      private String edtavSaldocontrato_vigenciainicio_to1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_94_fel_idx="0001" ;
      private String ROClassString ;
      private String edtSaldoContrato_Codigo_Jsonclick ;
      private String edtContrato_Codigo_Jsonclick ;
      private String edtSaldoContrato_VigenciaInicio_Jsonclick ;
      private String edtSaldoContrato_VigenciaFim_Jsonclick ;
      private String edtSaldoContrato_Credito_Jsonclick ;
      private String edtSaldoContrato_Reservado_Jsonclick ;
      private String edtSaldoContrato_Executado_Jsonclick ;
      private String edtSaldoContrato_Saldo_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV16SaldoContrato_VigenciaInicio1 ;
      private DateTime AV17SaldoContrato_VigenciaInicio_To1 ;
      private DateTime AV20SaldoContrato_VigenciaInicio2 ;
      private DateTime AV21SaldoContrato_VigenciaInicio_To2 ;
      private DateTime AV24SaldoContrato_VigenciaInicio3 ;
      private DateTime AV25SaldoContrato_VigenciaInicio_To3 ;
      private DateTime AV42TFSaldoContrato_VigenciaInicio ;
      private DateTime AV43TFSaldoContrato_VigenciaInicio_To ;
      private DateTime AV48TFSaldoContrato_VigenciaFim ;
      private DateTime AV49TFSaldoContrato_VigenciaFim_To ;
      private DateTime AV44DDO_SaldoContrato_VigenciaInicioAuxDate ;
      private DateTime AV45DDO_SaldoContrato_VigenciaInicioAuxDateTo ;
      private DateTime AV50DDO_SaldoContrato_VigenciaFimAuxDate ;
      private DateTime AV51DDO_SaldoContrato_VigenciaFimAuxDateTo ;
      private DateTime A1571SaldoContrato_VigenciaInicio ;
      private DateTime A1572SaldoContrato_VigenciaFim ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_saldocontrato_codigo_Includesortasc ;
      private bool Ddo_saldocontrato_codigo_Includesortdsc ;
      private bool Ddo_saldocontrato_codigo_Includefilter ;
      private bool Ddo_saldocontrato_codigo_Filterisrange ;
      private bool Ddo_saldocontrato_codigo_Includedatalist ;
      private bool Ddo_contrato_codigo_Includesortasc ;
      private bool Ddo_contrato_codigo_Includesortdsc ;
      private bool Ddo_contrato_codigo_Includefilter ;
      private bool Ddo_contrato_codigo_Filterisrange ;
      private bool Ddo_contrato_codigo_Includedatalist ;
      private bool Ddo_saldocontrato_vigenciainicio_Includesortasc ;
      private bool Ddo_saldocontrato_vigenciainicio_Includesortdsc ;
      private bool Ddo_saldocontrato_vigenciainicio_Includefilter ;
      private bool Ddo_saldocontrato_vigenciainicio_Filterisrange ;
      private bool Ddo_saldocontrato_vigenciainicio_Includedatalist ;
      private bool Ddo_saldocontrato_vigenciafim_Includesortasc ;
      private bool Ddo_saldocontrato_vigenciafim_Includesortdsc ;
      private bool Ddo_saldocontrato_vigenciafim_Includefilter ;
      private bool Ddo_saldocontrato_vigenciafim_Filterisrange ;
      private bool Ddo_saldocontrato_vigenciafim_Includedatalist ;
      private bool Ddo_saldocontrato_credito_Includesortasc ;
      private bool Ddo_saldocontrato_credito_Includesortdsc ;
      private bool Ddo_saldocontrato_credito_Includefilter ;
      private bool Ddo_saldocontrato_credito_Filterisrange ;
      private bool Ddo_saldocontrato_credito_Includedatalist ;
      private bool Ddo_saldocontrato_reservado_Includesortasc ;
      private bool Ddo_saldocontrato_reservado_Includesortdsc ;
      private bool Ddo_saldocontrato_reservado_Includefilter ;
      private bool Ddo_saldocontrato_reservado_Filterisrange ;
      private bool Ddo_saldocontrato_reservado_Includedatalist ;
      private bool Ddo_saldocontrato_executado_Includesortasc ;
      private bool Ddo_saldocontrato_executado_Includesortdsc ;
      private bool Ddo_saldocontrato_executado_Includefilter ;
      private bool Ddo_saldocontrato_executado_Filterisrange ;
      private bool Ddo_saldocontrato_executado_Includedatalist ;
      private bool Ddo_saldocontrato_saldo_Includesortasc ;
      private bool Ddo_saldocontrato_saldo_Includesortdsc ;
      private bool Ddo_saldocontrato_saldo_Includefilter ;
      private bool Ddo_saldocontrato_saldo_Filterisrange ;
      private bool Ddo_saldocontrato_saldo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace ;
      private String AV40ddo_Contrato_CodigoTitleControlIdToReplace ;
      private String AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace ;
      private String AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace ;
      private String AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace ;
      private String AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace ;
      private String AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace ;
      private String AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace ;
      private String AV75Update_GXI ;
      private String AV76Delete_GXI ;
      private String AV28Update ;
      private String AV29Delete ;
      private IGxSession AV30Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00M52_A1560NotaEmpenho_Codigo ;
      private decimal[] H00M52_A1576SaldoContrato_Saldo ;
      private decimal[] H00M52_A1575SaldoContrato_Executado ;
      private decimal[] H00M52_A1574SaldoContrato_Reservado ;
      private decimal[] H00M52_A1573SaldoContrato_Credito ;
      private DateTime[] H00M52_A1572SaldoContrato_VigenciaFim ;
      private DateTime[] H00M52_A1571SaldoContrato_VigenciaInicio ;
      private int[] H00M52_A74Contrato_Codigo ;
      private int[] H00M52_A1561SaldoContrato_Codigo ;
      private long[] H00M53_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV33SaldoContrato_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV37Contrato_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV41SaldoContrato_VigenciaInicioTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV47SaldoContrato_VigenciaFimTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV53SaldoContrato_CreditoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV57SaldoContrato_ReservadoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV61SaldoContrato_ExecutadoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV65SaldoContrato_SaldoTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV69DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwsaldocontrato__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00M52( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             DateTime AV16SaldoContrato_VigenciaInicio1 ,
                                             DateTime AV17SaldoContrato_VigenciaInicio_To1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             DateTime AV20SaldoContrato_VigenciaInicio2 ,
                                             DateTime AV21SaldoContrato_VigenciaInicio_To2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             DateTime AV24SaldoContrato_VigenciaInicio3 ,
                                             DateTime AV25SaldoContrato_VigenciaInicio_To3 ,
                                             int AV34TFSaldoContrato_Codigo ,
                                             int AV35TFSaldoContrato_Codigo_To ,
                                             int AV38TFContrato_Codigo ,
                                             int AV39TFContrato_Codigo_To ,
                                             DateTime AV42TFSaldoContrato_VigenciaInicio ,
                                             DateTime AV43TFSaldoContrato_VigenciaInicio_To ,
                                             DateTime AV48TFSaldoContrato_VigenciaFim ,
                                             DateTime AV49TFSaldoContrato_VigenciaFim_To ,
                                             decimal AV54TFSaldoContrato_Credito ,
                                             decimal AV55TFSaldoContrato_Credito_To ,
                                             decimal AV58TFSaldoContrato_Reservado ,
                                             decimal AV59TFSaldoContrato_Reservado_To ,
                                             decimal AV62TFSaldoContrato_Executado ,
                                             decimal AV63TFSaldoContrato_Executado_To ,
                                             decimal AV66TFSaldoContrato_Saldo ,
                                             decimal AV67TFSaldoContrato_Saldo_To ,
                                             DateTime A1571SaldoContrato_VigenciaInicio ,
                                             int A1561SaldoContrato_Codigo ,
                                             int A74Contrato_Codigo ,
                                             DateTime A1572SaldoContrato_VigenciaFim ,
                                             decimal A1573SaldoContrato_Credito ,
                                             decimal A1574SaldoContrato_Reservado ,
                                             decimal A1575SaldoContrato_Executado ,
                                             decimal A1576SaldoContrato_Saldo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [27] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[NotaEmpenho_Codigo], T2.[SaldoContrato_Saldo], T2.[SaldoContrato_Executado], T2.[SaldoContrato_Reservado], T2.[SaldoContrato_Credito], T2.[SaldoContrato_VigenciaFim], T2.[SaldoContrato_VigenciaInicio], T2.[Contrato_Codigo], T1.[SaldoContrato_Codigo]";
         sFromString = " FROM ([NotaEmpenho] T1 WITH (NOLOCK) INNER JOIN [SaldoContrato] T2 WITH (NOLOCK) ON T2.[SaldoContrato_Codigo] = T1.[SaldoContrato_Codigo])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SALDOCONTRATO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV16SaldoContrato_VigenciaInicio1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_VigenciaInicio] >= @AV16SaldoContrato_VigenciaInicio1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_VigenciaInicio] >= @AV16SaldoContrato_VigenciaInicio1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SALDOCONTRATO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV17SaldoContrato_VigenciaInicio_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_VigenciaInicio] <= @AV17SaldoContrato_VigenciaInicio_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_VigenciaInicio] <= @AV17SaldoContrato_VigenciaInicio_To1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SALDOCONTRATO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV20SaldoContrato_VigenciaInicio2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_VigenciaInicio] >= @AV20SaldoContrato_VigenciaInicio2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_VigenciaInicio] >= @AV20SaldoContrato_VigenciaInicio2)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SALDOCONTRATO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV21SaldoContrato_VigenciaInicio_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_VigenciaInicio] <= @AV21SaldoContrato_VigenciaInicio_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_VigenciaInicio] <= @AV21SaldoContrato_VigenciaInicio_To2)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SALDOCONTRATO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV24SaldoContrato_VigenciaInicio3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_VigenciaInicio] >= @AV24SaldoContrato_VigenciaInicio3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_VigenciaInicio] >= @AV24SaldoContrato_VigenciaInicio3)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SALDOCONTRATO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV25SaldoContrato_VigenciaInicio_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_VigenciaInicio] <= @AV25SaldoContrato_VigenciaInicio_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_VigenciaInicio] <= @AV25SaldoContrato_VigenciaInicio_To3)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (0==AV34TFSaldoContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[SaldoContrato_Codigo] >= @AV34TFSaldoContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[SaldoContrato_Codigo] >= @AV34TFSaldoContrato_Codigo)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! (0==AV35TFSaldoContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[SaldoContrato_Codigo] <= @AV35TFSaldoContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[SaldoContrato_Codigo] <= @AV35TFSaldoContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! (0==AV38TFContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Codigo] >= @AV38TFContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Codigo] >= @AV38TFContrato_Codigo)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! (0==AV39TFContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Codigo] <= @AV39TFContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Codigo] <= @AV39TFContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV42TFSaldoContrato_VigenciaInicio) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_VigenciaInicio] >= @AV42TFSaldoContrato_VigenciaInicio)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_VigenciaInicio] >= @AV42TFSaldoContrato_VigenciaInicio)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV43TFSaldoContrato_VigenciaInicio_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_VigenciaInicio] <= @AV43TFSaldoContrato_VigenciaInicio_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_VigenciaInicio] <= @AV43TFSaldoContrato_VigenciaInicio_To)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV48TFSaldoContrato_VigenciaFim) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_VigenciaFim] >= @AV48TFSaldoContrato_VigenciaFim)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_VigenciaFim] >= @AV48TFSaldoContrato_VigenciaFim)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! (DateTime.MinValue==AV49TFSaldoContrato_VigenciaFim_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_VigenciaFim] <= @AV49TFSaldoContrato_VigenciaFim_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_VigenciaFim] <= @AV49TFSaldoContrato_VigenciaFim_To)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV54TFSaldoContrato_Credito) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_Credito] >= @AV54TFSaldoContrato_Credito)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_Credito] >= @AV54TFSaldoContrato_Credito)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV55TFSaldoContrato_Credito_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_Credito] <= @AV55TFSaldoContrato_Credito_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_Credito] <= @AV55TFSaldoContrato_Credito_To)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV58TFSaldoContrato_Reservado) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_Reservado] >= @AV58TFSaldoContrato_Reservado)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_Reservado] >= @AV58TFSaldoContrato_Reservado)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV59TFSaldoContrato_Reservado_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_Reservado] <= @AV59TFSaldoContrato_Reservado_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_Reservado] <= @AV59TFSaldoContrato_Reservado_To)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV62TFSaldoContrato_Executado) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_Executado] >= @AV62TFSaldoContrato_Executado)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_Executado] >= @AV62TFSaldoContrato_Executado)";
            }
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV63TFSaldoContrato_Executado_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_Executado] <= @AV63TFSaldoContrato_Executado_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_Executado] <= @AV63TFSaldoContrato_Executado_To)";
            }
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV66TFSaldoContrato_Saldo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_Saldo] >= @AV66TFSaldoContrato_Saldo)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_Saldo] >= @AV66TFSaldoContrato_Saldo)";
            }
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV67TFSaldoContrato_Saldo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_Saldo] <= @AV67TFSaldoContrato_Saldo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_Saldo] <= @AV67TFSaldoContrato_Saldo_To)";
            }
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[SaldoContrato_VigenciaInicio]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[SaldoContrato_VigenciaInicio] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[SaldoContrato_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[SaldoContrato_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Codigo]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[SaldoContrato_VigenciaFim]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[SaldoContrato_VigenciaFim] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[SaldoContrato_Credito]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[SaldoContrato_Credito] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[SaldoContrato_Reservado]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[SaldoContrato_Reservado] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[SaldoContrato_Executado]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[SaldoContrato_Executado] DESC";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[SaldoContrato_Saldo]";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[SaldoContrato_Saldo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[NotaEmpenho_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00M53( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             DateTime AV16SaldoContrato_VigenciaInicio1 ,
                                             DateTime AV17SaldoContrato_VigenciaInicio_To1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             DateTime AV20SaldoContrato_VigenciaInicio2 ,
                                             DateTime AV21SaldoContrato_VigenciaInicio_To2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             DateTime AV24SaldoContrato_VigenciaInicio3 ,
                                             DateTime AV25SaldoContrato_VigenciaInicio_To3 ,
                                             int AV34TFSaldoContrato_Codigo ,
                                             int AV35TFSaldoContrato_Codigo_To ,
                                             int AV38TFContrato_Codigo ,
                                             int AV39TFContrato_Codigo_To ,
                                             DateTime AV42TFSaldoContrato_VigenciaInicio ,
                                             DateTime AV43TFSaldoContrato_VigenciaInicio_To ,
                                             DateTime AV48TFSaldoContrato_VigenciaFim ,
                                             DateTime AV49TFSaldoContrato_VigenciaFim_To ,
                                             decimal AV54TFSaldoContrato_Credito ,
                                             decimal AV55TFSaldoContrato_Credito_To ,
                                             decimal AV58TFSaldoContrato_Reservado ,
                                             decimal AV59TFSaldoContrato_Reservado_To ,
                                             decimal AV62TFSaldoContrato_Executado ,
                                             decimal AV63TFSaldoContrato_Executado_To ,
                                             decimal AV66TFSaldoContrato_Saldo ,
                                             decimal AV67TFSaldoContrato_Saldo_To ,
                                             DateTime A1571SaldoContrato_VigenciaInicio ,
                                             int A1561SaldoContrato_Codigo ,
                                             int A74Contrato_Codigo ,
                                             DateTime A1572SaldoContrato_VigenciaFim ,
                                             decimal A1573SaldoContrato_Credito ,
                                             decimal A1574SaldoContrato_Reservado ,
                                             decimal A1575SaldoContrato_Executado ,
                                             decimal A1576SaldoContrato_Saldo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [22] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([NotaEmpenho] T1 WITH (NOLOCK) INNER JOIN [SaldoContrato] T2 WITH (NOLOCK) ON T2.[SaldoContrato_Codigo] = T1.[SaldoContrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SALDOCONTRATO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV16SaldoContrato_VigenciaInicio1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_VigenciaInicio] >= @AV16SaldoContrato_VigenciaInicio1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_VigenciaInicio] >= @AV16SaldoContrato_VigenciaInicio1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SALDOCONTRATO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV17SaldoContrato_VigenciaInicio_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_VigenciaInicio] <= @AV17SaldoContrato_VigenciaInicio_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_VigenciaInicio] <= @AV17SaldoContrato_VigenciaInicio_To1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SALDOCONTRATO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV20SaldoContrato_VigenciaInicio2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_VigenciaInicio] >= @AV20SaldoContrato_VigenciaInicio2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_VigenciaInicio] >= @AV20SaldoContrato_VigenciaInicio2)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SALDOCONTRATO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV21SaldoContrato_VigenciaInicio_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_VigenciaInicio] <= @AV21SaldoContrato_VigenciaInicio_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_VigenciaInicio] <= @AV21SaldoContrato_VigenciaInicio_To2)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SALDOCONTRATO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV24SaldoContrato_VigenciaInicio3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_VigenciaInicio] >= @AV24SaldoContrato_VigenciaInicio3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_VigenciaInicio] >= @AV24SaldoContrato_VigenciaInicio3)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SALDOCONTRATO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV25SaldoContrato_VigenciaInicio_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_VigenciaInicio] <= @AV25SaldoContrato_VigenciaInicio_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_VigenciaInicio] <= @AV25SaldoContrato_VigenciaInicio_To3)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (0==AV34TFSaldoContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[SaldoContrato_Codigo] >= @AV34TFSaldoContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[SaldoContrato_Codigo] >= @AV34TFSaldoContrato_Codigo)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! (0==AV35TFSaldoContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[SaldoContrato_Codigo] <= @AV35TFSaldoContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[SaldoContrato_Codigo] <= @AV35TFSaldoContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! (0==AV38TFContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Codigo] >= @AV38TFContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Codigo] >= @AV38TFContrato_Codigo)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! (0==AV39TFContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Codigo] <= @AV39TFContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Codigo] <= @AV39TFContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV42TFSaldoContrato_VigenciaInicio) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_VigenciaInicio] >= @AV42TFSaldoContrato_VigenciaInicio)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_VigenciaInicio] >= @AV42TFSaldoContrato_VigenciaInicio)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV43TFSaldoContrato_VigenciaInicio_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_VigenciaInicio] <= @AV43TFSaldoContrato_VigenciaInicio_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_VigenciaInicio] <= @AV43TFSaldoContrato_VigenciaInicio_To)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV48TFSaldoContrato_VigenciaFim) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_VigenciaFim] >= @AV48TFSaldoContrato_VigenciaFim)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_VigenciaFim] >= @AV48TFSaldoContrato_VigenciaFim)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! (DateTime.MinValue==AV49TFSaldoContrato_VigenciaFim_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_VigenciaFim] <= @AV49TFSaldoContrato_VigenciaFim_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_VigenciaFim] <= @AV49TFSaldoContrato_VigenciaFim_To)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV54TFSaldoContrato_Credito) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_Credito] >= @AV54TFSaldoContrato_Credito)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_Credito] >= @AV54TFSaldoContrato_Credito)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV55TFSaldoContrato_Credito_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_Credito] <= @AV55TFSaldoContrato_Credito_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_Credito] <= @AV55TFSaldoContrato_Credito_To)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV58TFSaldoContrato_Reservado) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_Reservado] >= @AV58TFSaldoContrato_Reservado)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_Reservado] >= @AV58TFSaldoContrato_Reservado)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV59TFSaldoContrato_Reservado_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_Reservado] <= @AV59TFSaldoContrato_Reservado_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_Reservado] <= @AV59TFSaldoContrato_Reservado_To)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV62TFSaldoContrato_Executado) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_Executado] >= @AV62TFSaldoContrato_Executado)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_Executado] >= @AV62TFSaldoContrato_Executado)";
            }
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV63TFSaldoContrato_Executado_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_Executado] <= @AV63TFSaldoContrato_Executado_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_Executado] <= @AV63TFSaldoContrato_Executado_To)";
            }
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV66TFSaldoContrato_Saldo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_Saldo] >= @AV66TFSaldoContrato_Saldo)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_Saldo] >= @AV66TFSaldoContrato_Saldo)";
            }
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV67TFSaldoContrato_Saldo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SaldoContrato_Saldo] <= @AV67TFSaldoContrato_Saldo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SaldoContrato_Saldo] <= @AV67TFSaldoContrato_Saldo_To)";
            }
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00M52(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (decimal)dynConstraints[19] , (decimal)dynConstraints[20] , (decimal)dynConstraints[21] , (decimal)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] , (decimal)dynConstraints[25] , (decimal)dynConstraints[26] , (DateTime)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (DateTime)dynConstraints[30] , (decimal)dynConstraints[31] , (decimal)dynConstraints[32] , (decimal)dynConstraints[33] , (decimal)dynConstraints[34] , (short)dynConstraints[35] , (bool)dynConstraints[36] );
               case 1 :
                     return conditional_H00M53(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (decimal)dynConstraints[19] , (decimal)dynConstraints[20] , (decimal)dynConstraints[21] , (decimal)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] , (decimal)dynConstraints[25] , (decimal)dynConstraints[26] , (DateTime)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (DateTime)dynConstraints[30] , (decimal)dynConstraints[31] , (decimal)dynConstraints[32] , (decimal)dynConstraints[33] , (decimal)dynConstraints[34] , (short)dynConstraints[35] , (bool)dynConstraints[36] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00M52 ;
          prmH00M52 = new Object[] {
          new Object[] {"@AV16SaldoContrato_VigenciaInicio1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV17SaldoContrato_VigenciaInicio_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV20SaldoContrato_VigenciaInicio2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV21SaldoContrato_VigenciaInicio_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV24SaldoContrato_VigenciaInicio3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV25SaldoContrato_VigenciaInicio_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV34TFSaldoContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35TFSaldoContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV38TFContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV39TFContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV42TFSaldoContrato_VigenciaInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV43TFSaldoContrato_VigenciaInicio_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV48TFSaldoContrato_VigenciaFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV49TFSaldoContrato_VigenciaFim_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV54TFSaldoContrato_Credito",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV55TFSaldoContrato_Credito_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV58TFSaldoContrato_Reservado",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV59TFSaldoContrato_Reservado_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV62TFSaldoContrato_Executado",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV63TFSaldoContrato_Executado_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV66TFSaldoContrato_Saldo",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV67TFSaldoContrato_Saldo_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00M53 ;
          prmH00M53 = new Object[] {
          new Object[] {"@AV16SaldoContrato_VigenciaInicio1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV17SaldoContrato_VigenciaInicio_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV20SaldoContrato_VigenciaInicio2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV21SaldoContrato_VigenciaInicio_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV24SaldoContrato_VigenciaInicio3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV25SaldoContrato_VigenciaInicio_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV34TFSaldoContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35TFSaldoContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV38TFContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV39TFContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV42TFSaldoContrato_VigenciaInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV43TFSaldoContrato_VigenciaInicio_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV48TFSaldoContrato_VigenciaFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV49TFSaldoContrato_VigenciaFim_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV54TFSaldoContrato_Credito",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV55TFSaldoContrato_Credito_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV58TFSaldoContrato_Reservado",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV59TFSaldoContrato_Reservado_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV62TFSaldoContrato_Executado",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV63TFSaldoContrato_Executado_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV66TFSaldoContrato_Saldo",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV67TFSaldoContrato_Saldo_To",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00M52", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00M52,11,0,true,false )
             ,new CursorDef("H00M53", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00M53,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(6) ;
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(7) ;
                ((int[]) buf[7])[0] = rslt.getInt(8) ;
                ((int[]) buf[8])[0] = rslt.getInt(9) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[41]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[42]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[43]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[44]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[45]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[46]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[47]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[48]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[50]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[51]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[52]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[22]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[36]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[37]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[38]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[39]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[40]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[41]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[42]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[43]);
                }
                return;
       }
    }

 }

}
