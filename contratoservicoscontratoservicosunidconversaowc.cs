/*
               File: ContratoServicosContratoServicosUnidConversaoWC
        Description: Contrato Servicos Contrato Servicos Unid Conversao WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:1:38.6
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicoscontratoservicosunidconversaowc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratoservicoscontratoservicosunidconversaowc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratoservicoscontratoservicosunidconversaowc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoServicos_Codigo )
      {
         this.AV7ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicos_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7ContratoServicos_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_113 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_113_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_113_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
                  AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
                  AV16DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
                  AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
                  AV18ContratoServicosUnidConversao_Nome1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContratoServicosUnidConversao_Nome1", AV18ContratoServicosUnidConversao_Nome1);
                  AV20DynamicFiltersSelector2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
                  AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV22ContratoServicosUnidConversao_Nome2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22ContratoServicosUnidConversao_Nome2", AV22ContratoServicosUnidConversao_Nome2);
                  AV24DynamicFiltersSelector3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
                  AV25DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
                  AV26ContratoServicosUnidConversao_Nome3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ContratoServicosUnidConversao_Nome3", AV26ContratoServicosUnidConversao_Nome3);
                  AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
                  AV23DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
                  AV7ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicos_Codigo), 6, 0)));
                  AV53Pgmname = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
                  AV28DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersIgnoreFirst", AV28DynamicFiltersIgnoreFirst);
                  AV27DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
                  A160ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
                  A2110ContratoServicosUnidConversao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV36Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Contrato_Codigo), 6, 0)));
                  AV37ContratoServicos_UnidadeContratada = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37ContratoServicos_UnidadeContratada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37ContratoServicos_UnidadeContratada), 6, 0)));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContratoServicosUnidConversao_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ContratoServicosUnidConversao_Nome2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26ContratoServicosUnidConversao_Nome3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV7ContratoServicos_Codigo, AV53Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, AV6WWPContext, A160ContratoServicos_Codigo, A2110ContratoServicosUnidConversao_Codigo, AV36Contrato_Codigo, AV37ContratoServicos_UnidadeContratada, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  forbiddenHiddens = sPrefix + "hsh" + "ContratoServicosContratoServicosUnidConversaoWC";
                  forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9");
                  GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
                  GXUtil.WriteLog("contratoservicoscontratoservicosunidconversaowc:[SendSecurityCheck value for]"+"ContratoServicos_Codigo:"+context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9"));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PASR2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV53Pgmname = "ContratoServicosContratoServicosUnidConversaoWC";
               context.Gx_err = 0;
               edtavServicogrupo_descricao_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavServicogrupo_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicogrupo_descricao_Enabled), 5, 0)));
               edtavUnidademedicao_nome_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUnidademedicao_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidademedicao_nome_Enabled), 5, 0)));
               edtavContratoservicos_servicosigla_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratoservicos_servicosigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicos_servicosigla_Enabled), 5, 0)));
               edtavServico_nome_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavServico_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome_Enabled), 5, 0)));
               edtavContratoservicos_alias_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratoservicos_alias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicos_alias_Enabled), 5, 0)));
               WSSR2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contrato Servicos Contrato Servicos Unid Conversao WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205211813824");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoservicoscontratoservicosunidconversaowc.aspx") + "?" + UrlEncode("" +AV7ContratoServicos_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATOSERVICOSUNIDCONVERSAO_NOME1", StringUtil.RTrim( AV18ContratoServicosUnidConversao_Nome1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATOSERVICOSUNIDCONVERSAO_NOME2", StringUtil.RTrim( AV22ContratoServicosUnidConversao_Nome2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3", AV24DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV25DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATOSERVICOSUNIDCONVERSAO_NOME3", StringUtil.RTrim( AV26ContratoServicosUnidConversao_Nome3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV23DynamicFiltersEnabled3));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_113", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_113), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32GridPageCount), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV53Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSIGNOREFIRST", AV28DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSREMOVING", AV27DynamicFiltersRemoving);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "ContratoServicosContratoServicosUnidConversaoWC";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratoservicoscontratoservicosunidconversaowc:[SendSecurityCheck value for]"+"ContratoServicos_Codigo:"+context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseFormSR2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratoservicoscontratoservicosunidconversaowc.js", "?20205211813851");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratoServicosContratoServicosUnidConversaoWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Servicos Contrato Servicos Unid Conversao WC" ;
      }

      protected void WBSR0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratoservicoscontratoservicosunidconversaowc.aspx");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_SR2( true) ;
         }
         else
         {
            wb_table1_2_SR2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_SR2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContratoServicos_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'" + sPrefix + "',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36Contrato_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV36Contrato_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,125);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavContrato_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'" + sPrefix + "',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoservicos_unidadecontratada_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37ContratoServicos_UnidadeContratada), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV37ContratoServicos_UnidadeContratada), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,126);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoservicos_unidadecontratada_Jsonclick, 0, "Attribute", "", "", "", edtavContratoservicos_unidadecontratada_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'" + sPrefix + "',false,'" + sGXsfl_113_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(127, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,127);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'" + sPrefix + "',false,'" + sGXsfl_113_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV23DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(128, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,128);\"");
         }
         wbLoad = true;
      }

      protected void STARTSR2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contrato Servicos Contrato Servicos Unid Conversao WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPSR0( ) ;
            }
         }
      }

      protected void WSSR2( )
      {
         STARTSR2( ) ;
         EVTSR2( ) ;
      }

      protected void EVTSR2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPSR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPSR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11SR2 */
                                    E11SR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPSR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12SR2 */
                                    E12SR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPSR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13SR2 */
                                    E13SR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPSR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14SR2 */
                                    E14SR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPSR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15SR2 */
                                    E15SR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPSR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16SR2 */
                                    E16SR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPSR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E17SR2 */
                                    E17SR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPSR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E18SR2 */
                                    E18SR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPSR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E19SR2 */
                                    E19SR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPSR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E20SR2 */
                                    E20SR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPSR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E21SR2 */
                                    E21SR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPSR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavServicogrupo_descricao_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPSR0( ) ;
                              }
                              nGXsfl_113_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_113_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_113_idx), 4, 0)), 4, "0");
                              SubsflControlProps_1132( ) ;
                              AV33Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV33Update)) ? AV51Update_GXI : context.convertURL( context.PathToRelativeUrl( AV33Update))));
                              AV34Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV34Delete)) ? AV52Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV34Delete))));
                              A2110ContratoServicosUnidConversao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosUnidConversao_Codigo_Internalname), ",", "."));
                              A2111ContratoServicosUnidConversao_Nome = StringUtil.Upper( cgiGet( edtContratoServicosUnidConversao_Nome_Internalname));
                              n2111ContratoServicosUnidConversao_Nome = false;
                              A2112ContratoServicosUnidConversao_Sigla = StringUtil.Upper( cgiGet( edtContratoServicosUnidConversao_Sigla_Internalname));
                              n2112ContratoServicosUnidConversao_Sigla = false;
                              A2114ContratoServicosUnidConversao_Conversao = context.localUtil.CToN( cgiGet( edtContratoServicosUnidConversao_Conversao_Internalname), ",", ".");
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavServicogrupo_descricao_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E22SR2 */
                                          E22SR2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavServicogrupo_descricao_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E23SR2 */
                                          E23SR2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavServicogrupo_descricao_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E24SR2 */
                                          E24SR2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contratoservicosunidconversao_nome1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATOSERVICOSUNIDCONVERSAO_NOME1"), AV18ContratoServicosUnidConversao_Nome1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contratoservicosunidconversao_nome2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATOSERVICOSUNIDCONVERSAO_NOME2"), AV22ContratoServicosUnidConversao_Nome2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV24DynamicFiltersSelector3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV25DynamicFiltersOperator3 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contratoservicosunidconversao_nome3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATOSERVICOSUNIDCONVERSAO_NOME3"), AV26ContratoServicosUnidConversao_Nome3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV23DynamicFiltersEnabled3 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavServicogrupo_descricao_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPSR0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavServicogrupo_descricao_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WESR2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormSR2( ) ;
            }
         }
      }

      protected void PASR2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATOSERVICOSUNIDCONVERSAO_NOME", "Unidade de Contrata��o", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTRATOSERVICOSUNIDCONVERSAO_NOME", "Unidade de Contrata��o", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTRATOSERVICOSUNIDCONVERSAO_NOME", "Unidade de Contrata��o", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV24DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV24DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV25DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavServicogrupo_descricao_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_1132( ) ;
         while ( nGXsfl_113_idx <= nRC_GXsfl_113 )
         {
            sendrow_1132( ) ;
            nGXsfl_113_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_113_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_113_idx+1));
            sGXsfl_113_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_113_idx), 4, 0)), 4, "0");
            SubsflControlProps_1132( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       short AV17DynamicFiltersOperator1 ,
                                       String AV18ContratoServicosUnidConversao_Nome1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       short AV21DynamicFiltersOperator2 ,
                                       String AV22ContratoServicosUnidConversao_Nome2 ,
                                       String AV24DynamicFiltersSelector3 ,
                                       short AV25DynamicFiltersOperator3 ,
                                       String AV26ContratoServicosUnidConversao_Nome3 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       bool AV23DynamicFiltersEnabled3 ,
                                       int AV7ContratoServicos_Codigo ,
                                       String AV53Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       bool AV28DynamicFiltersIgnoreFirst ,
                                       bool AV27DynamicFiltersRemoving ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       int A160ContratoServicos_Codigo ,
                                       int A2110ContratoServicosUnidConversao_Codigo ,
                                       int AV36Contrato_Codigo ,
                                       int AV37ContratoServicos_UnidadeContratada ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFSR2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSUNIDCONVERSAO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2110ContratoServicosUnidConversao_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSUNIDCONVERSAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2110ContratoServicosUnidConversao_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2114ContratoServicosUnidConversao_Conversao, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO", StringUtil.LTrim( StringUtil.NToC( A2114ContratoServicosUnidConversao_Conversao, 14, 5, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV24DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV24DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV25DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFSR2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV53Pgmname = "ContratoServicosContratoServicosUnidConversaoWC";
         context.Gx_err = 0;
         edtavServicogrupo_descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavServicogrupo_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicogrupo_descricao_Enabled), 5, 0)));
         edtavUnidademedicao_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUnidademedicao_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidademedicao_nome_Enabled), 5, 0)));
         edtavContratoservicos_servicosigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratoservicos_servicosigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicos_servicosigla_Enabled), 5, 0)));
         edtavServico_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavServico_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome_Enabled), 5, 0)));
         edtavContratoservicos_alias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratoservicos_alias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicos_alias_Enabled), 5, 0)));
      }

      protected void RFSR2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 113;
         /* Execute user event: E23SR2 */
         E23SR2 ();
         nGXsfl_113_idx = 1;
         sGXsfl_113_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_113_idx), 4, 0)), 4, "0");
         SubsflControlProps_1132( ) ;
         nGXsfl_113_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_1132( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV17DynamicFiltersOperator1 ,
                                                 AV18ContratoServicosUnidConversao_Nome1 ,
                                                 AV19DynamicFiltersEnabled2 ,
                                                 AV20DynamicFiltersSelector2 ,
                                                 AV21DynamicFiltersOperator2 ,
                                                 AV22ContratoServicosUnidConversao_Nome2 ,
                                                 AV23DynamicFiltersEnabled3 ,
                                                 AV24DynamicFiltersSelector3 ,
                                                 AV25DynamicFiltersOperator3 ,
                                                 AV26ContratoServicosUnidConversao_Nome3 ,
                                                 A2111ContratoServicosUnidConversao_Nome ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc ,
                                                 A160ContratoServicos_Codigo ,
                                                 AV7ContratoServicos_Codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV18ContratoServicosUnidConversao_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18ContratoServicosUnidConversao_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContratoServicosUnidConversao_Nome1", AV18ContratoServicosUnidConversao_Nome1);
            lV18ContratoServicosUnidConversao_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18ContratoServicosUnidConversao_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContratoServicosUnidConversao_Nome1", AV18ContratoServicosUnidConversao_Nome1);
            lV22ContratoServicosUnidConversao_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV22ContratoServicosUnidConversao_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22ContratoServicosUnidConversao_Nome2", AV22ContratoServicosUnidConversao_Nome2);
            lV22ContratoServicosUnidConversao_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV22ContratoServicosUnidConversao_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22ContratoServicosUnidConversao_Nome2", AV22ContratoServicosUnidConversao_Nome2);
            lV26ContratoServicosUnidConversao_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV26ContratoServicosUnidConversao_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ContratoServicosUnidConversao_Nome3", AV26ContratoServicosUnidConversao_Nome3);
            lV26ContratoServicosUnidConversao_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV26ContratoServicosUnidConversao_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ContratoServicosUnidConversao_Nome3", AV26ContratoServicosUnidConversao_Nome3);
            /* Using cursor H00SR2 */
            pr_default.execute(0, new Object[] {AV7ContratoServicos_Codigo, lV18ContratoServicosUnidConversao_Nome1, lV18ContratoServicosUnidConversao_Nome1, lV22ContratoServicosUnidConversao_Nome2, lV22ContratoServicosUnidConversao_Nome2, lV26ContratoServicosUnidConversao_Nome3, lV26ContratoServicosUnidConversao_Nome3, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_113_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A160ContratoServicos_Codigo = H00SR2_A160ContratoServicos_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
               A2114ContratoServicosUnidConversao_Conversao = H00SR2_A2114ContratoServicosUnidConversao_Conversao[0];
               A2112ContratoServicosUnidConversao_Sigla = H00SR2_A2112ContratoServicosUnidConversao_Sigla[0];
               n2112ContratoServicosUnidConversao_Sigla = H00SR2_n2112ContratoServicosUnidConversao_Sigla[0];
               A2111ContratoServicosUnidConversao_Nome = H00SR2_A2111ContratoServicosUnidConversao_Nome[0];
               n2111ContratoServicosUnidConversao_Nome = H00SR2_n2111ContratoServicosUnidConversao_Nome[0];
               A2110ContratoServicosUnidConversao_Codigo = H00SR2_A2110ContratoServicosUnidConversao_Codigo[0];
               A2112ContratoServicosUnidConversao_Sigla = H00SR2_A2112ContratoServicosUnidConversao_Sigla[0];
               n2112ContratoServicosUnidConversao_Sigla = H00SR2_n2112ContratoServicosUnidConversao_Sigla[0];
               A2111ContratoServicosUnidConversao_Nome = H00SR2_A2111ContratoServicosUnidConversao_Nome[0];
               n2111ContratoServicosUnidConversao_Nome = H00SR2_n2111ContratoServicosUnidConversao_Nome[0];
               /* Execute user event: E24SR2 */
               E24SR2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 113;
            WBSR0( ) ;
         }
         nGXsfl_113_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV16DynamicFiltersSelector1 ,
                                              AV17DynamicFiltersOperator1 ,
                                              AV18ContratoServicosUnidConversao_Nome1 ,
                                              AV19DynamicFiltersEnabled2 ,
                                              AV20DynamicFiltersSelector2 ,
                                              AV21DynamicFiltersOperator2 ,
                                              AV22ContratoServicosUnidConversao_Nome2 ,
                                              AV23DynamicFiltersEnabled3 ,
                                              AV24DynamicFiltersSelector3 ,
                                              AV25DynamicFiltersOperator3 ,
                                              AV26ContratoServicosUnidConversao_Nome3 ,
                                              A2111ContratoServicosUnidConversao_Nome ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc ,
                                              A160ContratoServicos_Codigo ,
                                              AV7ContratoServicos_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV18ContratoServicosUnidConversao_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18ContratoServicosUnidConversao_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContratoServicosUnidConversao_Nome1", AV18ContratoServicosUnidConversao_Nome1);
         lV18ContratoServicosUnidConversao_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18ContratoServicosUnidConversao_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContratoServicosUnidConversao_Nome1", AV18ContratoServicosUnidConversao_Nome1);
         lV22ContratoServicosUnidConversao_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV22ContratoServicosUnidConversao_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22ContratoServicosUnidConversao_Nome2", AV22ContratoServicosUnidConversao_Nome2);
         lV22ContratoServicosUnidConversao_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV22ContratoServicosUnidConversao_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22ContratoServicosUnidConversao_Nome2", AV22ContratoServicosUnidConversao_Nome2);
         lV26ContratoServicosUnidConversao_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV26ContratoServicosUnidConversao_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ContratoServicosUnidConversao_Nome3", AV26ContratoServicosUnidConversao_Nome3);
         lV26ContratoServicosUnidConversao_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV26ContratoServicosUnidConversao_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ContratoServicosUnidConversao_Nome3", AV26ContratoServicosUnidConversao_Nome3);
         /* Using cursor H00SR3 */
         pr_default.execute(1, new Object[] {AV7ContratoServicos_Codigo, lV18ContratoServicosUnidConversao_Nome1, lV18ContratoServicosUnidConversao_Nome1, lV22ContratoServicosUnidConversao_Nome2, lV22ContratoServicosUnidConversao_Nome2, lV26ContratoServicosUnidConversao_Nome3, lV26ContratoServicosUnidConversao_Nome3});
         GRID_nRecordCount = H00SR3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContratoServicosUnidConversao_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ContratoServicosUnidConversao_Nome2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26ContratoServicosUnidConversao_Nome3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV7ContratoServicos_Codigo, AV53Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, AV6WWPContext, A160ContratoServicos_Codigo, A2110ContratoServicosUnidConversao_Codigo, AV36Contrato_Codigo, AV37ContratoServicos_UnidadeContratada, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContratoServicosUnidConversao_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ContratoServicosUnidConversao_Nome2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26ContratoServicosUnidConversao_Nome3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV7ContratoServicos_Codigo, AV53Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, AV6WWPContext, A160ContratoServicos_Codigo, A2110ContratoServicosUnidConversao_Codigo, AV36Contrato_Codigo, AV37ContratoServicos_UnidadeContratada, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContratoServicosUnidConversao_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ContratoServicosUnidConversao_Nome2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26ContratoServicosUnidConversao_Nome3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV7ContratoServicos_Codigo, AV53Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, AV6WWPContext, A160ContratoServicos_Codigo, A2110ContratoServicosUnidConversao_Codigo, AV36Contrato_Codigo, AV37ContratoServicos_UnidadeContratada, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContratoServicosUnidConversao_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ContratoServicosUnidConversao_Nome2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26ContratoServicosUnidConversao_Nome3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV7ContratoServicos_Codigo, AV53Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, AV6WWPContext, A160ContratoServicos_Codigo, A2110ContratoServicosUnidConversao_Codigo, AV36Contrato_Codigo, AV37ContratoServicos_UnidadeContratada, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContratoServicosUnidConversao_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ContratoServicosUnidConversao_Nome2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26ContratoServicosUnidConversao_Nome3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV7ContratoServicos_Codigo, AV53Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, AV6WWPContext, A160ContratoServicos_Codigo, A2110ContratoServicosUnidConversao_Codigo, AV36Contrato_Codigo, AV37ContratoServicos_UnidadeContratada, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPSR0( )
      {
         /* Before Start, stand alone formulas. */
         AV53Pgmname = "ContratoServicosContratoServicosUnidConversaoWC";
         context.Gx_err = 0;
         edtavServicogrupo_descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavServicogrupo_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicogrupo_descricao_Enabled), 5, 0)));
         edtavUnidademedicao_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUnidademedicao_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidademedicao_nome_Enabled), 5, 0)));
         edtavContratoservicos_servicosigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratoservicos_servicosigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicos_servicosigla_Enabled), 5, 0)));
         edtavServico_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavServico_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome_Enabled), 5, 0)));
         edtavContratoservicos_alias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratoservicos_alias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicos_alias_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E22SR2 */
         E22SR2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV38ServicoGrupo_Descricao = cgiGet( edtavServicogrupo_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38ServicoGrupo_Descricao", AV38ServicoGrupo_Descricao);
            AV45UnidadeMedicao_Nome = StringUtil.Upper( cgiGet( edtavUnidademedicao_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45UnidadeMedicao_Nome", AV45UnidadeMedicao_Nome);
            AV39ContratoServicos_ServicoSigla = StringUtil.Upper( cgiGet( edtavContratoservicos_servicosigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39ContratoServicos_ServicoSigla", AV39ContratoServicos_ServicoSigla);
            AV40Servico_Nome = StringUtil.Upper( cgiGet( edtavServico_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40Servico_Nome", AV40Servico_Nome);
            AV41ContratoServicos_Alias = StringUtil.Upper( cgiGet( edtavContratoservicos_alias_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41ContratoServicos_Alias", AV41ContratoServicos_Alias);
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV14OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            AV18ContratoServicosUnidConversao_Nome1 = StringUtil.Upper( cgiGet( edtavContratoservicosunidconversao_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContratoServicosUnidConversao_Nome1", AV18ContratoServicosUnidConversao_Nome1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            AV22ContratoServicosUnidConversao_Nome2 = StringUtil.Upper( cgiGet( edtavContratoservicosunidconversao_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22ContratoServicosUnidConversao_Nome2", AV22ContratoServicosUnidConversao_Nome2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV24DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV25DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
            AV26ContratoServicosUnidConversao_Nome3 = StringUtil.Upper( cgiGet( edtavContratoservicosunidconversao_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ContratoServicosUnidConversao_Nome3", AV26ContratoServicosUnidConversao_Nome3);
            A160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicos_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContrato_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContrato_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATO_CODIGO");
               GX_FocusControl = edtavContrato_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36Contrato_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Contrato_Codigo), 6, 0)));
            }
            else
            {
               AV36Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavContrato_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Contrato_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratoservicos_unidadecontratada_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratoservicos_unidadecontratada_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATOSERVICOS_UNIDADECONTRATADA");
               GX_FocusControl = edtavContratoservicos_unidadecontratada_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37ContratoServicos_UnidadeContratada = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37ContratoServicos_UnidadeContratada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37ContratoServicos_UnidadeContratada), 6, 0)));
            }
            else
            {
               AV37ContratoServicos_UnidadeContratada = (int)(context.localUtil.CToN( cgiGet( edtavContratoservicos_unidadecontratada_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37ContratoServicos_UnidadeContratada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37ContratoServicos_UnidadeContratada), 6, 0)));
            }
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV23DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
            /* Read saved values. */
            nRC_GXsfl_113 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_113"), ",", "."));
            AV31GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV32GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ContratoServicos_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "ContratoServicosContratoServicosUnidConversaoWC";
            A160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicos_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("contratoservicoscontratoservicosunidconversaowc:[SecurityCheckFailed value for]"+"ContratoServicos_Codigo:"+context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATOSERVICOSUNIDCONVERSAO_NOME1"), AV18ContratoServicosUnidConversao_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATOSERVICOSUNIDCONVERSAO_NOME2"), AV22ContratoServicosUnidConversao_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV24DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV25DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATOSERVICOSUNIDCONVERSAO_NOME3"), AV26ContratoServicosUnidConversao_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV23DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E22SR2 */
         E22SR2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22SR2( )
      {
         /* Start Routine */
         edtavContrato_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContrato_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_codigo_Visible), 5, 0)));
         edtavContratoservicos_unidadecontratada_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratoservicos_unidadecontratada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicos_unidadecontratada_Visible), 5, 0)));
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV16DynamicFiltersSelector1 = "CONTRATOSERVICOSUNIDCONVERSAO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersSelector2 = "CONTRATOSERVICOSUNIDCONVERSAO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersSelector3 = "CONTRATOSERVICOSUNIDCONVERSAO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtContratoServicos_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicos_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_Codigo_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Unidade de Contrata��o", 0);
         cmbavOrderedby.addItem("2", "Sigla", 0);
         cmbavOrderedby.addItem("3", "Convers�o", 0);
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         /* Using cursor H00SR4 */
         pr_default.execute(2, new Object[] {AV7ContratoServicos_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A155Servico_Codigo = H00SR4_A155Servico_Codigo[0];
            A157ServicoGrupo_Codigo = H00SR4_A157ServicoGrupo_Codigo[0];
            A160ContratoServicos_Codigo = H00SR4_A160ContratoServicos_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            A158ServicoGrupo_Descricao = H00SR4_A158ServicoGrupo_Descricao[0];
            A608Servico_Nome = H00SR4_A608Servico_Nome[0];
            A1858ContratoServicos_Alias = H00SR4_A1858ContratoServicos_Alias[0];
            n1858ContratoServicos_Alias = H00SR4_n1858ContratoServicos_Alias[0];
            A74Contrato_Codigo = H00SR4_A74Contrato_Codigo[0];
            A1212ContratoServicos_UnidadeContratada = H00SR4_A1212ContratoServicos_UnidadeContratada[0];
            A605Servico_Sigla = H00SR4_A605Servico_Sigla[0];
            A157ServicoGrupo_Codigo = H00SR4_A157ServicoGrupo_Codigo[0];
            A608Servico_Nome = H00SR4_A608Servico_Nome[0];
            A605Servico_Sigla = H00SR4_A605Servico_Sigla[0];
            A158ServicoGrupo_Descricao = H00SR4_A158ServicoGrupo_Descricao[0];
            A826ContratoServicos_ServicoSigla = A605Servico_Sigla;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A826ContratoServicos_ServicoSigla", A826ContratoServicos_ServicoSigla);
            AV38ServicoGrupo_Descricao = A158ServicoGrupo_Descricao;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38ServicoGrupo_Descricao", AV38ServicoGrupo_Descricao);
            AV39ContratoServicos_ServicoSigla = A826ContratoServicos_ServicoSigla;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39ContratoServicos_ServicoSigla", AV39ContratoServicos_ServicoSigla);
            AV40Servico_Nome = A608Servico_Nome;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40Servico_Nome", AV40Servico_Nome);
            AV41ContratoServicos_Alias = A1858ContratoServicos_Alias;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41ContratoServicos_Alias", AV41ContratoServicos_Alias);
            AV36Contrato_Codigo = A74Contrato_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Contrato_Codigo), 6, 0)));
            AV37ContratoServicos_UnidadeContratada = A1212ContratoServicos_UnidadeContratada;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37ContratoServicos_UnidadeContratada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37ContratoServicos_UnidadeContratada), 6, 0)));
            /* Execute user subroutine: 'DESCRICAO.UNIDADECONTRATADA' */
            S163 ();
            if ( returnInSub )
            {
               pr_default.close(2);
               returnInSub = true;
               if (true) return;
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
         GXt_boolean1 = AV47IsContratoServicosUnidConversao;
         new prc_iscontratoservicosunidconversao(context ).execute(  AV7ContratoServicos_Codigo, out  GXt_boolean1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicos_Codigo), 6, 0)));
         AV47IsContratoServicosUnidConversao = GXt_boolean1;
         if ( AV47IsContratoServicosUnidConversao )
         {
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
            imgInsert_Tooltiptext = "Unidade de Convers�o j� cadastrada.";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Tooltiptext", imgInsert_Tooltiptext);
         }
         else
         {
            imgInsert_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
            imgInsert_Tooltiptext = "Inserir";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Tooltiptext", imgInsert_Tooltiptext);
         }
      }

      protected void E23SR2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratoServicosUnidConversao_Nome_Titleformat = 2;
         edtContratoServicosUnidConversao_Nome_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 1);' >%5</span>", ((AV14OrderedBy==1) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Unidade de Contrata��o", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosUnidConversao_Nome_Internalname, "Title", edtContratoServicosUnidConversao_Nome_Title);
         edtContratoServicosUnidConversao_Sigla_Titleformat = 2;
         edtContratoServicosUnidConversao_Sigla_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV14OrderedBy==2) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Sigla", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosUnidConversao_Sigla_Internalname, "Title", edtContratoServicosUnidConversao_Sigla_Title);
         edtContratoServicosUnidConversao_Conversao_Titleformat = 2;
         edtContratoServicosUnidConversao_Conversao_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV14OrderedBy==3) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Convers�o", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosUnidConversao_Conversao_Internalname, "Title", edtContratoServicosUnidConversao_Conversao_Title);
         AV31GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31GridCurrentPage), 10, 0)));
         AV32GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E11SR2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV30PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV30PageToGo) ;
         }
      }

      private void E24SR2( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( AV6WWPContext.gxTpr_Update )
         {
            edtavUpdate_Link = formatLink("contratoservicosunidconversao.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A160ContratoServicos_Codigo) + "," + UrlEncode("" +A2110ContratoServicosUnidConversao_Codigo) + "," + UrlEncode("" +AV36Contrato_Codigo) + "," + UrlEncode("" +AV37ContratoServicos_UnidadeContratada);
            AV33Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV33Update);
            AV51Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV33Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV33Update);
            AV51Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( AV6WWPContext.gxTpr_Delete )
         {
            edtavDelete_Link = formatLink("contratoservicosunidconversao.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A160ContratoServicos_Codigo) + "," + UrlEncode("" +A2110ContratoServicosUnidConversao_Codigo) + "," + UrlEncode("" +AV36Contrato_Codigo) + "," + UrlEncode("" +AV37ContratoServicos_UnidadeContratada);
            AV34Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV34Delete);
            AV52Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV34Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV34Delete);
            AV52Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 113;
         }
         sendrow_1132( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_113_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(113, GridRow);
         }
      }

      protected void E12SR2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E17SR2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E13SR2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV27DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV28DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersIgnoreFirst", AV28DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV28DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersIgnoreFirst", AV28DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContratoServicosUnidConversao_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ContratoServicosUnidConversao_Nome2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26ContratoServicosUnidConversao_Nome3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV7ContratoServicos_Codigo, AV53Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, AV6WWPContext, A160ContratoServicos_Codigo, A2110ContratoServicosUnidConversao_Codigo, AV36Contrato_Codigo, AV37ContratoServicos_UnidadeContratada, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E18SR2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19SR2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV23DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E14SR2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV27DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContratoServicosUnidConversao_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ContratoServicosUnidConversao_Nome2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26ContratoServicosUnidConversao_Nome3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV7ContratoServicos_Codigo, AV53Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, AV6WWPContext, A160ContratoServicos_Codigo, A2110ContratoServicosUnidConversao_Codigo, AV36Contrato_Codigo, AV37ContratoServicos_UnidadeContratada, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E20SR2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E15SR2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV27DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV23DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContratoServicosUnidConversao_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ContratoServicosUnidConversao_Nome2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26ContratoServicosUnidConversao_Nome3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV7ContratoServicos_Codigo, AV53Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, AV6WWPContext, A160ContratoServicos_Codigo, A2110ContratoServicosUnidConversao_Codigo, AV36Contrato_Codigo, AV37ContratoServicos_UnidadeContratada, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E21SR2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E16SR2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("contratoservicosunidconversao.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +A160ContratoServicos_Codigo) + "," + UrlEncode("" +0) + "," + UrlEncode("" +AV36Contrato_Codigo) + "," + UrlEncode("" +AV37ContratoServicos_UnidadeContratada);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavContratoservicosunidconversao_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratoservicosunidconversao_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosunidconversao_nome1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOSERVICOSUNIDCONVERSAO_NOME") == 0 )
         {
            edtavContratoservicosunidconversao_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratoservicosunidconversao_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosunidconversao_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavContratoservicosunidconversao_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratoservicosunidconversao_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosunidconversao_nome2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATOSERVICOSUNIDCONVERSAO_NOME") == 0 )
         {
            edtavContratoservicosunidconversao_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratoservicosunidconversao_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosunidconversao_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavContratoservicosunidconversao_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratoservicosunidconversao_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosunidconversao_nome3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATOSERVICOSUNIDCONVERSAO_NOME") == 0 )
         {
            edtavContratoservicosunidconversao_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratoservicosunidconversao_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosunidconversao_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "CONTRATOSERVICOSUNIDCONVERSAO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         AV22ContratoServicosUnidConversao_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22ContratoServicosUnidConversao_Nome2", AV22ContratoServicosUnidConversao_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         AV24DynamicFiltersSelector3 = "CONTRATOSERVICOSUNIDCONVERSAO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
         AV25DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
         AV26ContratoServicosUnidConversao_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ContratoServicosUnidConversao_Nome3", AV26ContratoServicosUnidConversao_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get(AV53Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV53Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV29Session.Get(AV53Pgmname+"GridState"), "");
         }
         AV14OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         AV15OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOSERVICOSUNIDCONVERSAO_NOME") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV18ContratoServicosUnidConversao_Nome1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContratoServicosUnidConversao_Nome1", AV18ContratoServicosUnidConversao_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV13GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATOSERVICOSUNIDCONVERSAO_NOME") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV22ContratoServicosUnidConversao_Nome2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22ContratoServicosUnidConversao_Nome2", AV22ContratoServicosUnidConversao_Nome2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV23DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
                  AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(3));
                  AV24DynamicFiltersSelector3 = AV13GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATOSERVICOSUNIDCONVERSAO_NOME") == 0 )
                  {
                     AV25DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
                     AV26ContratoServicosUnidConversao_Nome3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ContratoServicosUnidConversao_Nome3", AV26ContratoServicosUnidConversao_Nome3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV27DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV29Session.Get(AV53Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV53Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV28DynamicFiltersIgnoreFirst )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOSERVICOSUNIDCONVERSAO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18ContratoServicosUnidConversao_Nome1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV18ContratoServicosUnidConversao_Nome1;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            if ( AV27DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATOSERVICOSUNIDCONVERSAO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV22ContratoServicosUnidConversao_Nome2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV22ContratoServicosUnidConversao_Nome2;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            if ( AV27DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV23DynamicFiltersEnabled3 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV24DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATOSERVICOSUNIDCONVERSAO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV26ContratoServicosUnidConversao_Nome3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV26ContratoServicosUnidConversao_Nome3;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV25DynamicFiltersOperator3;
            }
            if ( AV27DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV53Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ContratoServicosUnidConversao";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ContratoServicos_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContratoServicos_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV29Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void S163( )
      {
         /* 'DESCRICAO.UNIDADECONTRATADA' Routine */
         /* Using cursor H00SR5 */
         pr_default.execute(3, new Object[] {AV37ContratoServicos_UnidadeContratada});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A1197UnidadeMedicao_Codigo = H00SR5_A1197UnidadeMedicao_Codigo[0];
            A1198UnidadeMedicao_Nome = H00SR5_A1198UnidadeMedicao_Nome[0];
            AV45UnidadeMedicao_Nome = A1198UnidadeMedicao_Nome;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45UnidadeMedicao_Nome", AV45UnidadeMedicao_Nome);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(3);
      }

      protected void wb_table1_2_SR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup1_Internalname, "Servi�o", 1, 0, "px", 0, "px", "Group", "", "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            wb_table2_6_SR2( true) ;
         }
         else
         {
            wb_table2_6_SR2( false) ;
         }
         return  ;
      }

      protected void wb_table2_6_SR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup2_Internalname, "Convers�o", 1, 0, "px", 0, "px", "Group", "", "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            wb_table3_37_SR2( true) ;
         }
         else
         {
            wb_table3_37_SR2( false) ;
         }
         return  ;
      }

      protected void wb_table3_37_SR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_SR2e( true) ;
         }
         else
         {
            wb_table1_2_SR2e( false) ;
         }
      }

      protected void wb_table3_37_SR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableconversao_Internalname, tblTableconversao_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table4_40_SR2( true) ;
         }
         else
         {
            wb_table4_40_SR2( false) ;
         }
         return  ;
      }

      protected void wb_table4_40_SR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_110_SR2( true) ;
         }
         else
         {
            wb_table5_110_SR2( false) ;
         }
         return  ;
      }

      protected void wb_table5_110_SR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_37_SR2e( true) ;
         }
         else
         {
            wb_table3_37_SR2e( false) ;
         }
      }

      protected void wb_table5_110_SR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"113\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Unidade de Contrata��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosUnidConversao_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosUnidConversao_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosUnidConversao_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosUnidConversao_Sigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosUnidConversao_Sigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosUnidConversao_Sigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosUnidConversao_Conversao_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosUnidConversao_Conversao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosUnidConversao_Conversao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV33Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV34Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2110ContratoServicosUnidConversao_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A2111ContratoServicosUnidConversao_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosUnidConversao_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosUnidConversao_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A2112ContratoServicosUnidConversao_Sigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosUnidConversao_Sigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosUnidConversao_Sigla_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A2114ContratoServicosUnidConversao_Conversao, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosUnidConversao_Conversao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosUnidConversao_Conversao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 113 )
         {
            wbEnd = 0;
            nRC_GXsfl_113 = (short)(nGXsfl_113_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_110_SR2e( true) ;
         }
         else
         {
            wb_table5_110_SR2e( false) ;
         }
      }

      protected void wb_table4_40_SR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Width100'>") ;
            wb_table6_43_SR2( true) ;
         }
         else
         {
            wb_table6_43_SR2( false) ;
         }
         return  ;
      }

      protected void wb_table6_43_SR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'" + sPrefix + "',false,'" + sGXsfl_113_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'" + sPrefix + "',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table7_53_SR2( true) ;
         }
         else
         {
            wb_table7_53_SR2( false) ;
         }
         return  ;
      }

      protected void wb_table7_53_SR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_40_SR2e( true) ;
         }
         else
         {
            wb_table4_40_SR2e( false) ;
         }
      }

      protected void wb_table7_53_SR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table8_56_SR2( true) ;
         }
         else
         {
            wb_table8_56_SR2( false) ;
         }
         return  ;
      }

      protected void wb_table8_56_SR2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_53_SR2e( true) ;
         }
         else
         {
            wb_table7_53_SR2e( false) ;
         }
      }

      protected void wb_table8_56_SR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'" + sPrefix + "',false,'" + sGXsfl_113_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", "", true, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_65_SR2( true) ;
         }
         else
         {
            wb_table9_65_SR2( false) ;
         }
         return  ;
      }

      protected void wb_table9_65_SR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'" + sPrefix + "',false,'" + sGXsfl_113_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,78);\"", "", true, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_82_SR2( true) ;
         }
         else
         {
            wb_table10_82_SR2( false) ;
         }
         return  ;
      }

      protected void wb_table10_82_SR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'" + sPrefix + "',false,'" + sGXsfl_113_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV24DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,95);\"", "", true, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table11_99_SR2( true) ;
         }
         else
         {
            wb_table11_99_SR2( false) ;
         }
         return  ;
      }

      protected void wb_table11_99_SR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_56_SR2e( true) ;
         }
         else
         {
            wb_table8_56_SR2e( false) ;
         }
      }

      protected void wb_table11_99_SR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'" + sPrefix + "',false,'" + sGXsfl_113_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,102);\"", "", true, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'" + sPrefix + "',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoservicosunidconversao_nome3_Internalname, StringUtil.RTrim( AV26ContratoServicosUnidConversao_Nome3), StringUtil.RTrim( context.localUtil.Format( AV26ContratoServicosUnidConversao_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,104);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoservicosunidconversao_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratoservicosunidconversao_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_99_SR2e( true) ;
         }
         else
         {
            wb_table11_99_SR2e( false) ;
         }
      }

      protected void wb_table10_82_SR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'" + sPrefix + "',false,'" + sGXsfl_113_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,85);\"", "", true, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'" + sPrefix + "',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoservicosunidconversao_nome2_Internalname, StringUtil.RTrim( AV22ContratoServicosUnidConversao_Nome2), StringUtil.RTrim( context.localUtil.Format( AV22ContratoServicosUnidConversao_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,87);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoservicosunidconversao_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratoservicosunidconversao_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_82_SR2e( true) ;
         }
         else
         {
            wb_table10_82_SR2e( false) ;
         }
      }

      protected void wb_table9_65_SR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'" + sPrefix + "',false,'" + sGXsfl_113_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,68);\"", "", true, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'" + sPrefix + "',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoservicosunidconversao_nome1_Internalname, StringUtil.RTrim( AV18ContratoServicosUnidConversao_Nome1), StringUtil.RTrim( context.localUtil.Format( AV18ContratoServicosUnidConversao_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,70);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoservicosunidconversao_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratoservicosunidconversao_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_65_SR2e( true) ;
         }
         else
         {
            wb_table9_65_SR2e( false) ;
         }
      }

      protected void wb_table6_43_SR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, imgInsert_Enabled, "", imgInsert_Tooltiptext, 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_43_SR2e( true) ;
         }
         else
         {
            wb_table6_43_SR2e( false) ;
         }
      }

      protected void wb_table2_6_SR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblServico_Internalname, tblServico_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicogrupo_descricao_Internalname, "Grupo", "", "", lblTextblockservicogrupo_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table12_11_SR2( true) ;
         }
         else
         {
            wb_table12_11_SR2( false) ;
         }
         return  ;
      }

      protected void wb_table12_11_SR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_servicosigla_Internalname, "Servi�o", "", "", lblTextblockcontratoservicos_servicosigla_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'" + sPrefix + "',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoservicos_servicosigla_Internalname, StringUtil.RTrim( AV39ContratoServicos_ServicoSigla), StringUtil.RTrim( context.localUtil.Format( AV39ContratoServicos_ServicoSigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,23);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoservicos_servicosigla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContratoservicos_servicosigla_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_nome_Internalname, "Nome", "", "", lblTextblockservico_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'" + sPrefix + "',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_nome_Internalname, StringUtil.RTrim( AV40Servico_Nome), StringUtil.RTrim( context.localUtil.Format( AV40Servico_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,28);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavServico_nome_Enabled, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_alias_Internalname, "Alias", "", "", lblTextblockcontratoservicos_alias_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'" + sPrefix + "',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoservicos_alias_Internalname, StringUtil.RTrim( AV41ContratoServicos_Alias), StringUtil.RTrim( context.localUtil.Format( AV41ContratoServicos_Alias, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,33);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoservicos_alias_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContratoservicos_alias_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_6_SR2e( true) ;
         }
         else
         {
            wb_table2_6_SR2e( false) ;
         }
      }

      protected void wb_table12_11_SR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedservicogrupo_descricao_Internalname, tblTablemergedservicogrupo_descricao_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServicogrupo_descricao_Internalname, AV38ServicoGrupo_Descricao, StringUtil.RTrim( context.localUtil.Format( AV38ServicoGrupo_Descricao, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,14);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServicogrupo_descricao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavServicogrupo_descricao_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockunidademedicao_nome_Internalname, "Unidade Contratada", "", "", lblTextblockunidademedicao_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUnidademedicao_nome_Internalname, StringUtil.RTrim( AV45UnidadeMedicao_Nome), StringUtil.RTrim( context.localUtil.Format( AV45UnidadeMedicao_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,18);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUnidademedicao_nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavUnidademedicao_nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicosContratoServicosUnidConversaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_11_SR2e( true) ;
         }
         else
         {
            wb_table12_11_SR2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7ContratoServicos_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicos_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PASR2( ) ;
         WSSR2( ) ;
         WESR2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7ContratoServicos_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PASR2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratoservicoscontratoservicosunidconversaowc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PASR2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7ContratoServicos_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicos_Codigo), 6, 0)));
         }
         wcpOAV7ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ContratoServicos_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7ContratoServicos_Codigo != wcpOAV7ContratoServicos_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7ContratoServicos_Codigo = AV7ContratoServicos_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7ContratoServicos_Codigo = cgiGet( sPrefix+"AV7ContratoServicos_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7ContratoServicos_Codigo) > 0 )
         {
            AV7ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7ContratoServicos_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicos_Codigo), 6, 0)));
         }
         else
         {
            AV7ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7ContratoServicos_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PASR2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSSR2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSSR2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7ContratoServicos_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoServicos_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7ContratoServicos_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7ContratoServicos_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7ContratoServicos_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WESR2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205211814093");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratoservicoscontratoservicosunidconversaowc.js", "?20205211814093");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_1132( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_113_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_113_idx;
         edtContratoServicosUnidConversao_Codigo_Internalname = sPrefix+"CONTRATOSERVICOSUNIDCONVERSAO_CODIGO_"+sGXsfl_113_idx;
         edtContratoServicosUnidConversao_Nome_Internalname = sPrefix+"CONTRATOSERVICOSUNIDCONVERSAO_NOME_"+sGXsfl_113_idx;
         edtContratoServicosUnidConversao_Sigla_Internalname = sPrefix+"CONTRATOSERVICOSUNIDCONVERSAO_SIGLA_"+sGXsfl_113_idx;
         edtContratoServicosUnidConversao_Conversao_Internalname = sPrefix+"CONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO_"+sGXsfl_113_idx;
      }

      protected void SubsflControlProps_fel_1132( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_113_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_113_fel_idx;
         edtContratoServicosUnidConversao_Codigo_Internalname = sPrefix+"CONTRATOSERVICOSUNIDCONVERSAO_CODIGO_"+sGXsfl_113_fel_idx;
         edtContratoServicosUnidConversao_Nome_Internalname = sPrefix+"CONTRATOSERVICOSUNIDCONVERSAO_NOME_"+sGXsfl_113_fel_idx;
         edtContratoServicosUnidConversao_Sigla_Internalname = sPrefix+"CONTRATOSERVICOSUNIDCONVERSAO_SIGLA_"+sGXsfl_113_fel_idx;
         edtContratoServicosUnidConversao_Conversao_Internalname = sPrefix+"CONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO_"+sGXsfl_113_fel_idx;
      }

      protected void sendrow_1132( )
      {
         SubsflControlProps_1132( ) ;
         WBSR0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_113_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_113_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_113_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV33Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV33Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV51Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV33Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV33Update)) ? AV51Update_GXI : context.PathToRelativeUrl( AV33Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV33Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV34Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV34Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV52Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV34Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV34Delete)) ? AV52Delete_GXI : context.PathToRelativeUrl( AV34Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV34Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosUnidConversao_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A2110ContratoServicosUnidConversao_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A2110ContratoServicosUnidConversao_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosUnidConversao_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)113,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosUnidConversao_Nome_Internalname,StringUtil.RTrim( A2111ContratoServicosUnidConversao_Nome),StringUtil.RTrim( context.localUtil.Format( A2111ContratoServicosUnidConversao_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosUnidConversao_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)113,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosUnidConversao_Sigla_Internalname,StringUtil.RTrim( A2112ContratoServicosUnidConversao_Sigla),StringUtil.RTrim( context.localUtil.Format( A2112ContratoServicosUnidConversao_Sigla, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosUnidConversao_Sigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)113,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosUnidConversao_Conversao_Internalname,StringUtil.LTrim( StringUtil.NToC( A2114ContratoServicosUnidConversao_Conversao, 14, 5, ",", "")),context.localUtil.Format( A2114ContratoServicosUnidConversao_Conversao, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosUnidConversao_Conversao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)113,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSUNIDCONVERSAO_CODIGO"+"_"+sGXsfl_113_idx, GetSecureSignedToken( sPrefix+sGXsfl_113_idx, context.localUtil.Format( (decimal)(A2110ContratoServicosUnidConversao_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO"+"_"+sGXsfl_113_idx, GetSecureSignedToken( sPrefix+sGXsfl_113_idx, context.localUtil.Format( A2114ContratoServicosUnidConversao_Conversao, "ZZ,ZZZ,ZZ9.999")));
            GridContainer.AddRow(GridRow);
            nGXsfl_113_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_113_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_113_idx+1));
            sGXsfl_113_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_113_idx), 4, 0)), 4, "0");
            SubsflControlProps_1132( ) ;
         }
         /* End function sendrow_1132 */
      }

      protected void init_default_properties( )
      {
         lblTextblockservicogrupo_descricao_Internalname = sPrefix+"TEXTBLOCKSERVICOGRUPO_DESCRICAO";
         edtavServicogrupo_descricao_Internalname = sPrefix+"vSERVICOGRUPO_DESCRICAO";
         lblTextblockunidademedicao_nome_Internalname = sPrefix+"TEXTBLOCKUNIDADEMEDICAO_NOME";
         edtavUnidademedicao_nome_Internalname = sPrefix+"vUNIDADEMEDICAO_NOME";
         tblTablemergedservicogrupo_descricao_Internalname = sPrefix+"TABLEMERGEDSERVICOGRUPO_DESCRICAO";
         lblTextblockcontratoservicos_servicosigla_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_SERVICOSIGLA";
         edtavContratoservicos_servicosigla_Internalname = sPrefix+"vCONTRATOSERVICOS_SERVICOSIGLA";
         lblTextblockservico_nome_Internalname = sPrefix+"TEXTBLOCKSERVICO_NOME";
         edtavServico_nome_Internalname = sPrefix+"vSERVICO_NOME";
         lblTextblockcontratoservicos_alias_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_ALIAS";
         edtavContratoservicos_alias_Internalname = sPrefix+"vCONTRATOSERVICOS_ALIAS";
         tblServico_Internalname = sPrefix+"SERVICO";
         grpUnnamedgroup1_Internalname = sPrefix+"UNNAMEDGROUP1";
         imgInsert_Internalname = sPrefix+"INSERT";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         lblOrderedtext_Internalname = sPrefix+"ORDEREDTEXT";
         cmbavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR1";
         edtavContratoservicosunidconversao_nome1_Internalname = sPrefix+"vCONTRATOSERVICOSUNIDCONVERSAO_NOME1";
         tblTablemergeddynamicfilters1_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = sPrefix+"ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = sPrefix+"REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = sPrefix+"DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR2";
         edtavContratoservicosunidconversao_nome2_Internalname = sPrefix+"vCONTRATOSERVICOSUNIDCONVERSAO_NOME2";
         tblTablemergeddynamicfilters2_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = sPrefix+"ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = sPrefix+"REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = sPrefix+"DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR3";
         edtavContratoservicosunidconversao_nome3_Internalname = sPrefix+"vCONTRATOSERVICOSUNIDCONVERSAO_NOME3";
         tblTablemergeddynamicfilters3_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = sPrefix+"REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = sPrefix+"JSDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtContratoServicosUnidConversao_Codigo_Internalname = sPrefix+"CONTRATOSERVICOSUNIDCONVERSAO_CODIGO";
         edtContratoServicosUnidConversao_Nome_Internalname = sPrefix+"CONTRATOSERVICOSUNIDCONVERSAO_NOME";
         edtContratoServicosUnidConversao_Sigla_Internalname = sPrefix+"CONTRATOSERVICOSUNIDCONVERSAO_SIGLA";
         edtContratoServicosUnidConversao_Conversao_Internalname = sPrefix+"CONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTableconversao_Internalname = sPrefix+"TABLECONVERSAO";
         grpUnnamedgroup2_Internalname = sPrefix+"UNNAMEDGROUP2";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtContratoServicos_Codigo_Internalname = sPrefix+"CONTRATOSERVICOS_CODIGO";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavContrato_codigo_Internalname = sPrefix+"vCONTRATO_CODIGO";
         edtavContratoservicos_unidadecontratada_Internalname = sPrefix+"vCONTRATOSERVICOS_UNIDADECONTRATADA";
         chkavDynamicfiltersenabled2_Internalname = sPrefix+"vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = sPrefix+"vDYNAMICFILTERSENABLED3";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContratoServicosUnidConversao_Conversao_Jsonclick = "";
         edtContratoServicosUnidConversao_Sigla_Jsonclick = "";
         edtContratoServicosUnidConversao_Nome_Jsonclick = "";
         edtContratoServicosUnidConversao_Codigo_Jsonclick = "";
         edtavUnidademedicao_nome_Jsonclick = "";
         edtavUnidademedicao_nome_Enabled = 1;
         edtavServicogrupo_descricao_Jsonclick = "";
         edtavServicogrupo_descricao_Enabled = 1;
         edtavContratoservicos_alias_Jsonclick = "";
         edtavContratoservicos_alias_Enabled = 1;
         edtavServico_nome_Jsonclick = "";
         edtavServico_nome_Enabled = 1;
         edtavContratoservicos_servicosigla_Jsonclick = "";
         edtavContratoservicos_servicosigla_Enabled = 1;
         imgInsert_Enabled = 1;
         edtavContratoservicosunidconversao_nome1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavContratoservicosunidconversao_nome2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavContratoservicosunidconversao_nome3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         edtContratoServicosUnidConversao_Conversao_Titleformat = 0;
         edtContratoServicosUnidConversao_Sigla_Titleformat = 0;
         edtContratoServicosUnidConversao_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavContratoservicosunidconversao_nome3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavContratoservicosunidconversao_nome2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavContratoservicosunidconversao_nome1_Visible = 1;
         edtContratoServicosUnidConversao_Conversao_Title = "Convers�o";
         edtContratoServicosUnidConversao_Sigla_Title = "Sigla";
         edtContratoServicosUnidConversao_Nome_Title = "Unidade de Contrata��o";
         imgInsert_Tooltiptext = "Inserir";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtavContratoservicos_unidadecontratada_Jsonclick = "";
         edtavContratoservicos_unidadecontratada_Visible = 1;
         edtavContrato_codigo_Jsonclick = "";
         edtavContrato_codigo_Visible = 1;
         edtContratoServicos_Codigo_Jsonclick = "";
         edtContratoServicos_Codigo_Visible = 1;
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A2110ContratoServicosUnidConversao_Codigo',fld:'CONTRATOSERVICOSUNIDCONVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV36Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37ContratoServicos_UnidadeContratada',fld:'vCONTRATOSERVICOS_UNIDADECONTRATADA',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV53Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18ContratoServicosUnidConversao_Nome1',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME1',pic:'@!',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22ContratoServicosUnidConversao_Nome2',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26ContratoServicosUnidConversao_Nome3',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME3',pic:'@!',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtContratoServicosUnidConversao_Nome_Titleformat',ctrl:'CONTRATOSERVICOSUNIDCONVERSAO_NOME',prop:'Titleformat'},{av:'edtContratoServicosUnidConversao_Nome_Title',ctrl:'CONTRATOSERVICOSUNIDCONVERSAO_NOME',prop:'Title'},{av:'edtContratoServicosUnidConversao_Sigla_Titleformat',ctrl:'CONTRATOSERVICOSUNIDCONVERSAO_SIGLA',prop:'Titleformat'},{av:'edtContratoServicosUnidConversao_Sigla_Title',ctrl:'CONTRATOSERVICOSUNIDCONVERSAO_SIGLA',prop:'Title'},{av:'edtContratoServicosUnidConversao_Conversao_Titleformat',ctrl:'CONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO',prop:'Titleformat'},{av:'edtContratoServicosUnidConversao_Conversao_Title',ctrl:'CONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO',prop:'Title'},{av:'AV31GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV32GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11SR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContratoServicosUnidConversao_Nome1',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ContratoServicosUnidConversao_Nome2',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26ContratoServicosUnidConversao_Nome3',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A2110ContratoServicosUnidConversao_Codigo',fld:'CONTRATOSERVICOSUNIDCONVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV36Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37ContratoServicos_UnidadeContratada',fld:'vCONTRATOSERVICOS_UNIDADECONTRATADA',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E24SR2',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A2110ContratoServicosUnidConversao_Codigo',fld:'CONTRATOSERVICOSUNIDCONVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV36Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37ContratoServicos_UnidadeContratada',fld:'vCONTRATOSERVICOS_UNIDADECONTRATADA',pic:'ZZZZZ9',nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV33Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV34Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E12SR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContratoServicosUnidConversao_Nome1',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ContratoServicosUnidConversao_Nome2',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26ContratoServicosUnidConversao_Nome3',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A2110ContratoServicosUnidConversao_Codigo',fld:'CONTRATOSERVICOSUNIDCONVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV36Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37ContratoServicos_UnidadeContratada',fld:'vCONTRATOSERVICOS_UNIDADECONTRATADA',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E17SR2',iparms:[],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E13SR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContratoServicosUnidConversao_Nome1',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ContratoServicosUnidConversao_Nome2',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26ContratoServicosUnidConversao_Nome3',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A2110ContratoServicosUnidConversao_Codigo',fld:'CONTRATOSERVICOSUNIDCONVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV36Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37ContratoServicos_UnidadeContratada',fld:'vCONTRATOSERVICOS_UNIDADECONTRATADA',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ContratoServicosUnidConversao_Nome2',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26ContratoServicosUnidConversao_Nome3',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContratoServicosUnidConversao_Nome1',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContratoservicosunidconversao_nome2_Visible',ctrl:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratoservicosunidconversao_nome3_Visible',ctrl:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContratoservicosunidconversao_nome1_Visible',ctrl:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E18SR2',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavContratoservicosunidconversao_nome1_Visible',ctrl:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E19SR2',iparms:[],oparms:[{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E14SR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContratoServicosUnidConversao_Nome1',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ContratoServicosUnidConversao_Nome2',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26ContratoServicosUnidConversao_Nome3',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A2110ContratoServicosUnidConversao_Codigo',fld:'CONTRATOSERVICOSUNIDCONVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV36Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37ContratoServicos_UnidadeContratada',fld:'vCONTRATOSERVICOS_UNIDADECONTRATADA',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ContratoServicosUnidConversao_Nome2',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26ContratoServicosUnidConversao_Nome3',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContratoServicosUnidConversao_Nome1',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContratoservicosunidconversao_nome2_Visible',ctrl:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratoservicosunidconversao_nome3_Visible',ctrl:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContratoservicosunidconversao_nome1_Visible',ctrl:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E20SR2',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavContratoservicosunidconversao_nome2_Visible',ctrl:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E15SR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContratoServicosUnidConversao_Nome1',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ContratoServicosUnidConversao_Nome2',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26ContratoServicosUnidConversao_Nome3',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A2110ContratoServicosUnidConversao_Codigo',fld:'CONTRATOSERVICOSUNIDCONVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV36Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37ContratoServicos_UnidadeContratada',fld:'vCONTRATOSERVICOS_UNIDADECONTRATADA',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ContratoServicosUnidConversao_Nome2',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26ContratoServicosUnidConversao_Nome3',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContratoServicosUnidConversao_Nome1',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContratoservicosunidconversao_nome2_Visible',ctrl:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratoservicosunidconversao_nome3_Visible',ctrl:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContratoservicosunidconversao_nome1_Visible',ctrl:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E21SR2',iparms:[{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavContratoservicosunidconversao_nome3_Visible',ctrl:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E16SR2',iparms:[{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A2110ContratoServicosUnidConversao_Codigo',fld:'CONTRATOSERVICOSUNIDCONVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV36Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37ContratoServicos_UnidadeContratada',fld:'vCONTRATOSERVICOS_UNIDADECONTRATADA',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV37ContratoServicos_UnidadeContratada',fld:'vCONTRATOSERVICOS_UNIDADECONTRATADA',pic:'ZZZZZ9',nv:0},{av:'AV36Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16DynamicFiltersSelector1 = "";
         AV18ContratoServicosUnidConversao_Nome1 = "";
         AV20DynamicFiltersSelector2 = "";
         AV22ContratoServicosUnidConversao_Nome2 = "";
         AV24DynamicFiltersSelector3 = "";
         AV26ContratoServicosUnidConversao_Nome3 = "";
         AV53Pgmname = "";
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         forbiddenHiddens = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV33Update = "";
         AV51Update_GXI = "";
         AV34Delete = "";
         AV52Delete_GXI = "";
         A2111ContratoServicosUnidConversao_Nome = "";
         A2112ContratoServicosUnidConversao_Sigla = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV18ContratoServicosUnidConversao_Nome1 = "";
         lV22ContratoServicosUnidConversao_Nome2 = "";
         lV26ContratoServicosUnidConversao_Nome3 = "";
         H00SR2_A160ContratoServicos_Codigo = new int[1] ;
         H00SR2_A2114ContratoServicosUnidConversao_Conversao = new decimal[1] ;
         H00SR2_A2112ContratoServicosUnidConversao_Sigla = new String[] {""} ;
         H00SR2_n2112ContratoServicosUnidConversao_Sigla = new bool[] {false} ;
         H00SR2_A2111ContratoServicosUnidConversao_Nome = new String[] {""} ;
         H00SR2_n2111ContratoServicosUnidConversao_Nome = new bool[] {false} ;
         H00SR2_A2110ContratoServicosUnidConversao_Codigo = new int[1] ;
         H00SR3_AGRID_nRecordCount = new long[1] ;
         AV38ServicoGrupo_Descricao = "";
         AV45UnidadeMedicao_Nome = "";
         AV39ContratoServicos_ServicoSigla = "";
         AV40Servico_Nome = "";
         AV41ContratoServicos_Alias = "";
         hsh = "";
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         H00SR4_A155Servico_Codigo = new int[1] ;
         H00SR4_A157ServicoGrupo_Codigo = new int[1] ;
         H00SR4_A160ContratoServicos_Codigo = new int[1] ;
         H00SR4_A158ServicoGrupo_Descricao = new String[] {""} ;
         H00SR4_A608Servico_Nome = new String[] {""} ;
         H00SR4_A1858ContratoServicos_Alias = new String[] {""} ;
         H00SR4_n1858ContratoServicos_Alias = new bool[] {false} ;
         H00SR4_A74Contrato_Codigo = new int[1] ;
         H00SR4_A1212ContratoServicos_UnidadeContratada = new int[1] ;
         H00SR4_A605Servico_Sigla = new String[] {""} ;
         A158ServicoGrupo_Descricao = "";
         A608Servico_Nome = "";
         A1858ContratoServicos_Alias = "";
         A605Servico_Sigla = "";
         A826ContratoServicos_ServicoSigla = "";
         GridRow = new GXWebRow();
         AV29Session = context.GetSession();
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         H00SR5_A1197UnidadeMedicao_Codigo = new int[1] ;
         H00SR5_A1198UnidadeMedicao_Nome = new String[] {""} ;
         A1198UnidadeMedicao_Nome = "";
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         lblTextblockservicogrupo_descricao_Jsonclick = "";
         lblTextblockcontratoservicos_servicosigla_Jsonclick = "";
         lblTextblockservico_nome_Jsonclick = "";
         lblTextblockcontratoservicos_alias_Jsonclick = "";
         lblTextblockunidademedicao_nome_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7ContratoServicos_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicoscontratoservicosunidconversaowc__default(),
            new Object[][] {
                new Object[] {
               H00SR2_A160ContratoServicos_Codigo, H00SR2_A2114ContratoServicosUnidConversao_Conversao, H00SR2_A2112ContratoServicosUnidConversao_Sigla, H00SR2_n2112ContratoServicosUnidConversao_Sigla, H00SR2_A2111ContratoServicosUnidConversao_Nome, H00SR2_n2111ContratoServicosUnidConversao_Nome, H00SR2_A2110ContratoServicosUnidConversao_Codigo
               }
               , new Object[] {
               H00SR3_AGRID_nRecordCount
               }
               , new Object[] {
               H00SR4_A155Servico_Codigo, H00SR4_A157ServicoGrupo_Codigo, H00SR4_A160ContratoServicos_Codigo, H00SR4_A158ServicoGrupo_Descricao, H00SR4_A608Servico_Nome, H00SR4_A1858ContratoServicos_Alias, H00SR4_n1858ContratoServicos_Alias, H00SR4_A74Contrato_Codigo, H00SR4_A1212ContratoServicos_UnidadeContratada, H00SR4_A605Servico_Sigla
               }
               , new Object[] {
               H00SR5_A1197UnidadeMedicao_Codigo, H00SR5_A1198UnidadeMedicao_Nome
               }
            }
         );
         AV53Pgmname = "ContratoServicosContratoServicosUnidConversaoWC";
         /* GeneXus formulas. */
         AV53Pgmname = "ContratoServicosContratoServicosUnidConversaoWC";
         context.Gx_err = 0;
         edtavServicogrupo_descricao_Enabled = 0;
         edtavUnidademedicao_nome_Enabled = 0;
         edtavContratoservicos_servicosigla_Enabled = 0;
         edtavServico_nome_Enabled = 0;
         edtavContratoservicos_alias_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_113 ;
      private short nGXsfl_113_idx=1 ;
      private short AV14OrderedBy ;
      private short AV17DynamicFiltersOperator1 ;
      private short AV21DynamicFiltersOperator2 ;
      private short AV25DynamicFiltersOperator3 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_113_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContratoServicosUnidConversao_Nome_Titleformat ;
      private short edtContratoServicosUnidConversao_Sigla_Titleformat ;
      private short edtContratoServicosUnidConversao_Conversao_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7ContratoServicos_Codigo ;
      private int wcpOAV7ContratoServicos_Codigo ;
      private int subGrid_Rows ;
      private int A160ContratoServicos_Codigo ;
      private int A2110ContratoServicosUnidConversao_Codigo ;
      private int AV36Contrato_Codigo ;
      private int AV37ContratoServicos_UnidadeContratada ;
      private int edtavServicogrupo_descricao_Enabled ;
      private int edtavUnidademedicao_nome_Enabled ;
      private int edtavContratoservicos_servicosigla_Enabled ;
      private int edtavServico_nome_Enabled ;
      private int edtavContratoservicos_alias_Enabled ;
      private int Gridpaginationbar_Pagestoshow ;
      private int edtContratoServicos_Codigo_Visible ;
      private int edtavContrato_codigo_Visible ;
      private int edtavContratoservicos_unidadecontratada_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int A155Servico_Codigo ;
      private int A157ServicoGrupo_Codigo ;
      private int A74Contrato_Codigo ;
      private int A1212ContratoServicos_UnidadeContratada ;
      private int imgInsert_Enabled ;
      private int AV30PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavContratoservicosunidconversao_nome1_Visible ;
      private int edtavContratoservicosunidconversao_nome2_Visible ;
      private int edtavContratoservicosunidconversao_nome3_Visible ;
      private int A1197UnidadeMedicao_Codigo ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV31GridCurrentPage ;
      private long AV32GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal A2114ContratoServicosUnidConversao_Conversao ;
      private String Gridpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_113_idx="0001" ;
      private String AV18ContratoServicosUnidConversao_Nome1 ;
      private String AV22ContratoServicosUnidConversao_Nome2 ;
      private String AV26ContratoServicosUnidConversao_Nome3 ;
      private String AV53Pgmname ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String edtavServicogrupo_descricao_Internalname ;
      private String edtavUnidademedicao_nome_Internalname ;
      private String edtavContratoservicos_servicosigla_Internalname ;
      private String edtavServico_nome_Internalname ;
      private String edtavContratoservicos_alias_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String GX_FocusControl ;
      private String edtContratoServicos_Codigo_Internalname ;
      private String edtContratoServicos_Codigo_Jsonclick ;
      private String TempTags ;
      private String edtavContrato_codigo_Internalname ;
      private String edtavContrato_codigo_Jsonclick ;
      private String edtavContratoservicos_unidadecontratada_Internalname ;
      private String edtavContratoservicos_unidadecontratada_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtContratoServicosUnidConversao_Codigo_Internalname ;
      private String A2111ContratoServicosUnidConversao_Nome ;
      private String edtContratoServicosUnidConversao_Nome_Internalname ;
      private String A2112ContratoServicosUnidConversao_Sigla ;
      private String edtContratoServicosUnidConversao_Sigla_Internalname ;
      private String edtContratoServicosUnidConversao_Conversao_Internalname ;
      private String scmdbuf ;
      private String lV18ContratoServicosUnidConversao_Nome1 ;
      private String lV22ContratoServicosUnidConversao_Nome2 ;
      private String lV26ContratoServicosUnidConversao_Nome3 ;
      private String AV45UnidadeMedicao_Nome ;
      private String AV39ContratoServicos_ServicoSigla ;
      private String AV40Servico_Nome ;
      private String AV41ContratoServicos_Alias ;
      private String cmbavOrderedby_Internalname ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavContratoservicosunidconversao_nome1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavContratoservicosunidconversao_nome2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavContratoservicosunidconversao_nome3_Internalname ;
      private String hsh ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String A608Servico_Nome ;
      private String A1858ContratoServicos_Alias ;
      private String A605Servico_Sigla ;
      private String A826ContratoServicos_ServicoSigla ;
      private String imgInsert_Internalname ;
      private String imgInsert_Tooltiptext ;
      private String edtContratoServicosUnidConversao_Nome_Title ;
      private String edtContratoServicosUnidConversao_Sigla_Title ;
      private String edtContratoServicosUnidConversao_Conversao_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String A1198UnidadeMedicao_Nome ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String grpUnnamedgroup1_Internalname ;
      private String grpUnnamedgroup2_Internalname ;
      private String tblTableconversao_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavContratoservicosunidconversao_nome3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavContratoservicosunidconversao_nome2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavContratoservicosunidconversao_nome1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Jsonclick ;
      private String tblServico_Internalname ;
      private String lblTextblockservicogrupo_descricao_Internalname ;
      private String lblTextblockservicogrupo_descricao_Jsonclick ;
      private String lblTextblockcontratoservicos_servicosigla_Internalname ;
      private String lblTextblockcontratoservicos_servicosigla_Jsonclick ;
      private String edtavContratoservicos_servicosigla_Jsonclick ;
      private String lblTextblockservico_nome_Internalname ;
      private String lblTextblockservico_nome_Jsonclick ;
      private String edtavServico_nome_Jsonclick ;
      private String lblTextblockcontratoservicos_alias_Internalname ;
      private String lblTextblockcontratoservicos_alias_Jsonclick ;
      private String edtavContratoservicos_alias_Jsonclick ;
      private String tblTablemergedservicogrupo_descricao_Internalname ;
      private String edtavServicogrupo_descricao_Jsonclick ;
      private String lblTextblockunidademedicao_nome_Internalname ;
      private String lblTextblockunidademedicao_nome_Jsonclick ;
      private String edtavUnidademedicao_nome_Jsonclick ;
      private String sCtrlAV7ContratoServicos_Codigo ;
      private String sGXsfl_113_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContratoServicosUnidConversao_Codigo_Jsonclick ;
      private String edtContratoServicosUnidConversao_Nome_Jsonclick ;
      private String edtContratoServicosUnidConversao_Sigla_Jsonclick ;
      private String edtContratoServicosUnidConversao_Conversao_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV23DynamicFiltersEnabled3 ;
      private bool AV28DynamicFiltersIgnoreFirst ;
      private bool AV27DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n2111ContratoServicosUnidConversao_Nome ;
      private bool n2112ContratoServicosUnidConversao_Sigla ;
      private bool returnInSub ;
      private bool n1858ContratoServicos_Alias ;
      private bool AV47IsContratoServicosUnidConversao ;
      private bool GXt_boolean1 ;
      private bool gx_refresh_fired ;
      private bool AV33Update_IsBlob ;
      private bool AV34Delete_IsBlob ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV24DynamicFiltersSelector3 ;
      private String AV51Update_GXI ;
      private String AV52Delete_GXI ;
      private String AV38ServicoGrupo_Descricao ;
      private String A158ServicoGrupo_Descricao ;
      private String AV33Update ;
      private String AV34Delete ;
      private IGxSession AV29Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00SR2_A160ContratoServicos_Codigo ;
      private decimal[] H00SR2_A2114ContratoServicosUnidConversao_Conversao ;
      private String[] H00SR2_A2112ContratoServicosUnidConversao_Sigla ;
      private bool[] H00SR2_n2112ContratoServicosUnidConversao_Sigla ;
      private String[] H00SR2_A2111ContratoServicosUnidConversao_Nome ;
      private bool[] H00SR2_n2111ContratoServicosUnidConversao_Nome ;
      private int[] H00SR2_A2110ContratoServicosUnidConversao_Codigo ;
      private long[] H00SR3_AGRID_nRecordCount ;
      private int[] H00SR4_A155Servico_Codigo ;
      private int[] H00SR4_A157ServicoGrupo_Codigo ;
      private int[] H00SR4_A160ContratoServicos_Codigo ;
      private String[] H00SR4_A158ServicoGrupo_Descricao ;
      private String[] H00SR4_A608Servico_Nome ;
      private String[] H00SR4_A1858ContratoServicos_Alias ;
      private bool[] H00SR4_n1858ContratoServicos_Alias ;
      private int[] H00SR4_A74Contrato_Codigo ;
      private int[] H00SR4_A1212ContratoServicos_UnidadeContratada ;
      private String[] H00SR4_A605Servico_Sigla ;
      private int[] H00SR5_A1197UnidadeMedicao_Codigo ;
      private String[] H00SR5_A1198UnidadeMedicao_Nome ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class contratoservicoscontratoservicosunidconversaowc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00SR2( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             String AV18ContratoServicosUnidConversao_Nome1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             short AV21DynamicFiltersOperator2 ,
                                             String AV22ContratoServicosUnidConversao_Nome2 ,
                                             bool AV23DynamicFiltersEnabled3 ,
                                             String AV24DynamicFiltersSelector3 ,
                                             short AV25DynamicFiltersOperator3 ,
                                             String AV26ContratoServicosUnidConversao_Nome3 ,
                                             String A2111ContratoServicosUnidConversao_Nome ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A160ContratoServicos_Codigo ,
                                             int AV7ContratoServicos_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [12] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[ContratoServicos_Codigo], T1.[ContratoServicosUnidConversao_Conversao], T2.[UnidadeMedicao_Sigla] AS ContratoServicosUnidConversao_Sigla, T2.[UnidadeMedicao_Nome] AS ContratoServicosUnidConversao_Nome, T1.[ContratoServicosUnidConversao_Codigo] AS ContratoServicosUnidConversao_Codigo";
         sFromString = " FROM ([ContratoServicosUnidConversao] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[ContratoServicosUnidConversao_Codigo])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[ContratoServicos_Codigo] = @AV7ContratoServicos_Codigo)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOSERVICOSUNIDCONVERSAO_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18ContratoServicosUnidConversao_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like @lV18ContratoServicosUnidConversao_Nome1)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOSERVICOSUNIDCONVERSAO_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18ContratoServicosUnidConversao_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like '%' + @lV18ContratoServicosUnidConversao_Nome1)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATOSERVICOSUNIDCONVERSAO_NOME") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22ContratoServicosUnidConversao_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like @lV22ContratoServicosUnidConversao_Nome2)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATOSERVICOSUNIDCONVERSAO_NOME") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22ContratoServicosUnidConversao_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like '%' + @lV22ContratoServicosUnidConversao_Nome2)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATOSERVICOSUNIDCONVERSAO_NOME") == 0 ) && ( AV25DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26ContratoServicosUnidConversao_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like @lV26ContratoServicosUnidConversao_Nome3)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATOSERVICOSUNIDCONVERSAO_NOME") == 0 ) && ( AV25DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26ContratoServicosUnidConversao_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like '%' + @lV26ContratoServicosUnidConversao_Nome3)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicos_Codigo], T2.[UnidadeMedicao_Nome]";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicos_Codigo] DESC, T2.[UnidadeMedicao_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicos_Codigo], T2.[UnidadeMedicao_Sigla]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicos_Codigo] DESC, T2.[UnidadeMedicao_Sigla] DESC";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicos_Codigo], T1.[ContratoServicosUnidConversao_Conversao]";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicos_Codigo] DESC, T1.[ContratoServicosUnidConversao_Conversao] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicos_Codigo], T1.[ContratoServicosUnidConversao_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00SR3( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             String AV18ContratoServicosUnidConversao_Nome1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             short AV21DynamicFiltersOperator2 ,
                                             String AV22ContratoServicosUnidConversao_Nome2 ,
                                             bool AV23DynamicFiltersEnabled3 ,
                                             String AV24DynamicFiltersSelector3 ,
                                             short AV25DynamicFiltersOperator3 ,
                                             String AV26ContratoServicosUnidConversao_Nome3 ,
                                             String A2111ContratoServicosUnidConversao_Nome ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A160ContratoServicos_Codigo ,
                                             int AV7ContratoServicos_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [7] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([ContratoServicosUnidConversao] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[ContratoServicosUnidConversao_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoServicos_Codigo] = @AV7ContratoServicos_Codigo)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOSERVICOSUNIDCONVERSAO_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18ContratoServicosUnidConversao_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like @lV18ContratoServicosUnidConversao_Nome1)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOSERVICOSUNIDCONVERSAO_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18ContratoServicosUnidConversao_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like '%' + @lV18ContratoServicosUnidConversao_Nome1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATOSERVICOSUNIDCONVERSAO_NOME") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22ContratoServicosUnidConversao_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like @lV22ContratoServicosUnidConversao_Nome2)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATOSERVICOSUNIDCONVERSAO_NOME") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22ContratoServicosUnidConversao_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like '%' + @lV22ContratoServicosUnidConversao_Nome2)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATOSERVICOSUNIDCONVERSAO_NOME") == 0 ) && ( AV25DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26ContratoServicosUnidConversao_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like @lV26ContratoServicosUnidConversao_Nome3)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATOSERVICOSUNIDCONVERSAO_NOME") == 0 ) && ( AV25DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26ContratoServicosUnidConversao_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like '%' + @lV26ContratoServicosUnidConversao_Nome3)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00SR2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (short)dynConstraints[12] , (bool)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] );
               case 1 :
                     return conditional_H00SR3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (short)dynConstraints[12] , (bool)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00SR4 ;
          prmH00SR4 = new Object[] {
          new Object[] {"@AV7ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00SR5 ;
          prmH00SR5 = new Object[] {
          new Object[] {"@AV37ContratoServicos_UnidadeContratada",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00SR2 ;
          prmH00SR2 = new Object[] {
          new Object[] {"@AV7ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18ContratoServicosUnidConversao_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV18ContratoServicosUnidConversao_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV22ContratoServicosUnidConversao_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV22ContratoServicosUnidConversao_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV26ContratoServicosUnidConversao_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV26ContratoServicosUnidConversao_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00SR3 ;
          prmH00SR3 = new Object[] {
          new Object[] {"@AV7ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18ContratoServicosUnidConversao_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV18ContratoServicosUnidConversao_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV22ContratoServicosUnidConversao_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV22ContratoServicosUnidConversao_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV26ContratoServicosUnidConversao_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV26ContratoServicosUnidConversao_Nome3",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00SR2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SR2,11,0,true,false )
             ,new CursorDef("H00SR3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SR3,1,0,true,false )
             ,new CursorDef("H00SR4", "SELECT T1.[Servico_Codigo], T2.[ServicoGrupo_Codigo], T1.[ContratoServicos_Codigo], T3.[ServicoGrupo_Descricao], T2.[Servico_Nome], T1.[ContratoServicos_Alias], T1.[Contrato_Codigo], T1.[ContratoServicos_UnidadeContratada], T2.[Servico_Sigla] FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [ServicoGrupo] T3 WITH (NOLOCK) ON T3.[ServicoGrupo_Codigo] = T2.[ServicoGrupo_Codigo]) WHERE T1.[ContratoServicos_Codigo] = @AV7ContratoServicos_Codigo ORDER BY T1.[ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SR4,1,0,true,true )
             ,new CursorDef("H00SR5", "SELECT [UnidadeMedicao_Codigo], [UnidadeMedicao_Nome] FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @AV37ContratoServicos_UnidadeContratada ORDER BY [UnidadeMedicao_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SR5,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                ((String[]) buf[9])[0] = rslt.getString(9, 15) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
