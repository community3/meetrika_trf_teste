/*
               File: PromptContagemResultadoNotificacaoDestinatario
        Description: Select Destinatario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:45:31.50
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptcontagemresultadonotificacaodestinatario : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptcontagemresultadonotificacaodestinatario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptcontagemresultadonotificacaodestinatario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( long aP0_InContagemResultadoNotificacao_Codigo ,
                           ref int aP1_InOutContagemResultadoNotificacao_DestCod ,
                           ref int aP2_InOutContagemResultadoNotificacao_DestPesCod )
      {
         this.AV7InContagemResultadoNotificacao_Codigo = aP0_InContagemResultadoNotificacao_Codigo;
         this.AV8InOutContagemResultadoNotificacao_DestCod = aP1_InOutContagemResultadoNotificacao_DestCod;
         this.AV9InOutContagemResultadoNotificacao_DestPesCod = aP2_InOutContagemResultadoNotificacao_DestPesCod;
         executePrivate();
         aP1_InOutContagemResultadoNotificacao_DestCod=this.AV8InOutContagemResultadoNotificacao_DestCod;
         aP2_InOutContagemResultadoNotificacao_DestPesCod=this.AV9InOutContagemResultadoNotificacao_DestPesCod;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_83 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_83_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_83_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
               AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
               AV16DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
               AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV18ContagemResultadoNotificacao_DestPesCod1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultadoNotificacao_DestPesCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContagemResultadoNotificacao_DestPesCod1), 6, 0)));
               AV19ContagemResultadoNotificacao_DestNome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultadoNotificacao_DestNome1", AV19ContagemResultadoNotificacao_DestNome1);
               AV21DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
               AV23ContagemResultadoNotificacao_DestPesCod2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultadoNotificacao_DestPesCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23ContagemResultadoNotificacao_DestPesCod2), 6, 0)));
               AV24ContagemResultadoNotificacao_DestNome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultadoNotificacao_DestNome2", AV24ContagemResultadoNotificacao_DestNome2);
               AV26DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
               AV27DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
               AV28ContagemResultadoNotificacao_DestPesCod3 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ContagemResultadoNotificacao_DestPesCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28ContagemResultadoNotificacao_DestPesCod3), 6, 0)));
               AV29ContagemResultadoNotificacao_DestNome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultadoNotificacao_DestNome3", AV29ContagemResultadoNotificacao_DestNome3);
               AV20DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV25DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
               AV7InContagemResultadoNotificacao_Codigo = (long)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InContagemResultadoNotificacao_Codigo), 10, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vINCONTAGEMRESULTADONOTIFICACAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7InContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9")));
               A1419ContagemResultadoNotificacao_DestEmail = GetNextPar( );
               n1419ContagemResultadoNotificacao_DestEmail = false;
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemResultadoNotificacao_DestPesCod1, AV19ContagemResultadoNotificacao_DestNome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemResultadoNotificacao_DestPesCod2, AV24ContagemResultadoNotificacao_DestNome2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28ContagemResultadoNotificacao_DestPesCod3, AV29ContagemResultadoNotificacao_DestNome3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7InContagemResultadoNotificacao_Codigo, A1419ContagemResultadoNotificacao_DestEmail) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InContagemResultadoNotificacao_Codigo = (long)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InContagemResultadoNotificacao_Codigo), 10, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vINCONTAGEMRESULTADONOTIFICACAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7InContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutContagemResultadoNotificacao_DestCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContagemResultadoNotificacao_DestCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutContagemResultadoNotificacao_DestCod), 6, 0)));
                  AV9InOutContagemResultadoNotificacao_DestPesCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9InOutContagemResultadoNotificacao_DestPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9InOutContagemResultadoNotificacao_DestPesCod), 6, 0)));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAQS2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WSQS2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEQS2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299453164");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptcontagemresultadonotificacaodestinatario.aspx") + "?" + UrlEncode("" +AV7InContagemResultadoNotificacao_Codigo) + "," + UrlEncode("" +AV8InOutContagemResultadoNotificacao_DestCod) + "," + UrlEncode("" +AV9InOutContagemResultadoNotificacao_DestPesCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18ContagemResultadoNotificacao_DestPesCod1), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADONOTIFICACAO_DESTNOME1", StringUtil.RTrim( AV19ContagemResultadoNotificacao_DestNome1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV21DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23ContagemResultadoNotificacao_DestPesCod2), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADONOTIFICACAO_DESTNOME2", StringUtil.RTrim( AV24ContagemResultadoNotificacao_DestNome2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV26DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV27DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV28ContagemResultadoNotificacao_DestPesCod3), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADONOTIFICACAO_DESTNOME3", StringUtil.RTrim( AV29ContagemResultadoNotificacao_DestNome3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV20DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV25DynamicFiltersEnabled3));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_83", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_83), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35GridPageCount), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINCONTAGEMRESULTADONOTIFICACAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InContagemResultadoNotificacao_Codigo), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV31DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV30DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTCONTAGEMRESULTADONOTIFICACAO_DESTCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8InOutContagemResultadoNotificacao_DestCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9InOutContagemResultadoNotificacao_DestPesCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vINCONTAGEMRESULTADONOTIFICACAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7InContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vINCONTAGEMRESULTADONOTIFICACAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7InContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormQS2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptContagemResultadoNotificacaoDestinatario" ;
      }

      public override String GetPgmdesc( )
      {
         return "Select Destinatario" ;
      }

      protected void WBQS0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_QS2( true) ;
         }
         else
         {
            wb_table1_2_QS2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_QS2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(93, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,93);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV25DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(94, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,94);\"");
         }
         wbLoad = true;
      }

      protected void STARTQS2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Select Destinatario", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPQS0( ) ;
      }

      protected void WSQS2( )
      {
         STARTQS2( ) ;
         EVTQS2( ) ;
      }

      protected void EVTQS2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11QS2 */
                           E11QS2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12QS2 */
                           E12QS2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13QS2 */
                           E13QS2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14QS2 */
                           E14QS2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15QS2 */
                           E15QS2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16QS2 */
                           E16QS2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17QS2 */
                           E17QS2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18QS2 */
                           E18QS2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19QS2 */
                           E19QS2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20QS2 */
                           E20QS2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21QS2 */
                           E21QS2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_83_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_83_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_83_idx), 4, 0)), 4, "0");
                           SubsflControlProps_832( ) ;
                           AV36Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV36Select)) ? AV39Select_GXI : context.convertURL( context.PathToRelativeUrl( AV36Select))));
                           A1414ContagemResultadoNotificacao_DestCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_DestCod_Internalname), ",", "."));
                           A1426ContagemResultadoNotificacao_DestPesCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_DestPesCod_Internalname), ",", "."));
                           n1426ContagemResultadoNotificacao_DestPesCod = false;
                           A1421ContagemResultadoNotificacao_DestNome = StringUtil.Upper( cgiGet( edtContagemResultadoNotificacao_DestNome_Internalname));
                           n1421ContagemResultadoNotificacao_DestNome = false;
                           A1419ContagemResultadoNotificacao_DestEmail = cgiGet( edtContagemResultadoNotificacao_DestEmail_Internalname);
                           n1419ContagemResultadoNotificacao_DestEmail = false;
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E22QS2 */
                                 E22QS2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E23QS2 */
                                 E23QS2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E24QS2 */
                                 E24QS2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultadonotificacao_destpescod1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD1"), ",", ".") != Convert.ToDecimal( AV18ContagemResultadoNotificacao_DestPesCod1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultadonotificacao_destnome1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_DESTNOME1"), AV19ContagemResultadoNotificacao_DestNome1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultadonotificacao_destpescod2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD2"), ",", ".") != Convert.ToDecimal( AV23ContagemResultadoNotificacao_DestPesCod2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultadonotificacao_destnome2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_DESTNOME2"), AV24ContagemResultadoNotificacao_DestNome2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV26DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV27DynamicFiltersOperator3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultadonotificacao_destpescod3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD3"), ",", ".") != Convert.ToDecimal( AV28ContagemResultadoNotificacao_DestPesCod3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultadonotificacao_destnome3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_DESTNOME3"), AV29ContagemResultadoNotificacao_DestNome3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV25DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E25QS2 */
                                       E25QS2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEQS2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormQS2( ) ;
            }
         }
      }

      protected void PAQS2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD", "codigo", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADONOTIFICACAO_DESTNOME", "Destinat�rio", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator1.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD", "codigo", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADONOTIFICACAO_DESTNOME", "Destinat�rio", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator2.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD", "codigo", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADONOTIFICACAO_DESTNOME", "Destinat�rio", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV26DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV26DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator3.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV27DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_832( ) ;
         while ( nGXsfl_83_idx <= nRC_GXsfl_83 )
         {
            sendrow_832( ) ;
            nGXsfl_83_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_83_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_83_idx+1));
            sGXsfl_83_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_83_idx), 4, 0)), 4, "0");
            SubsflControlProps_832( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       short AV17DynamicFiltersOperator1 ,
                                       int AV18ContagemResultadoNotificacao_DestPesCod1 ,
                                       String AV19ContagemResultadoNotificacao_DestNome1 ,
                                       String AV21DynamicFiltersSelector2 ,
                                       short AV22DynamicFiltersOperator2 ,
                                       int AV23ContagemResultadoNotificacao_DestPesCod2 ,
                                       String AV24ContagemResultadoNotificacao_DestNome2 ,
                                       String AV26DynamicFiltersSelector3 ,
                                       short AV27DynamicFiltersOperator3 ,
                                       int AV28ContagemResultadoNotificacao_DestPesCod3 ,
                                       String AV29ContagemResultadoNotificacao_DestNome3 ,
                                       bool AV20DynamicFiltersEnabled2 ,
                                       bool AV25DynamicFiltersEnabled3 ,
                                       long AV7InContagemResultadoNotificacao_Codigo ,
                                       String A1419ContagemResultadoNotificacao_DestEmail )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFQS2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADONOTIFICACAO_DESTCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1414ContagemResultadoNotificacao_DestCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADONOTIFICACAO_DESTCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1414ContagemResultadoNotificacao_DestCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1419ContagemResultadoNotificacao_DestEmail, ""))));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL", A1419ContagemResultadoNotificacao_DestEmail);
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV26DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV26DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV27DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFQS2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFQS2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 83;
         /* Execute user event: E23QS2 */
         E23QS2 ();
         nGXsfl_83_idx = 1;
         sGXsfl_83_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_83_idx), 4, 0)), 4, "0");
         SubsflControlProps_832( ) ;
         nGXsfl_83_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_832( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV17DynamicFiltersOperator1 ,
                                                 AV18ContagemResultadoNotificacao_DestPesCod1 ,
                                                 AV19ContagemResultadoNotificacao_DestNome1 ,
                                                 AV20DynamicFiltersEnabled2 ,
                                                 AV21DynamicFiltersSelector2 ,
                                                 AV22DynamicFiltersOperator2 ,
                                                 AV23ContagemResultadoNotificacao_DestPesCod2 ,
                                                 AV24ContagemResultadoNotificacao_DestNome2 ,
                                                 AV25DynamicFiltersEnabled3 ,
                                                 AV26DynamicFiltersSelector3 ,
                                                 AV27DynamicFiltersOperator3 ,
                                                 AV28ContagemResultadoNotificacao_DestPesCod3 ,
                                                 AV29ContagemResultadoNotificacao_DestNome3 ,
                                                 A1426ContagemResultadoNotificacao_DestPesCod ,
                                                 A1421ContagemResultadoNotificacao_DestNome ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc ,
                                                 A1412ContagemResultadoNotificacao_Codigo ,
                                                 AV7InContagemResultadoNotificacao_Codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                                 TypeConstants.LONG, TypeConstants.LONG
                                                 }
            });
            lV19ContagemResultadoNotificacao_DestNome1 = StringUtil.PadR( StringUtil.RTrim( AV19ContagemResultadoNotificacao_DestNome1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultadoNotificacao_DestNome1", AV19ContagemResultadoNotificacao_DestNome1);
            lV19ContagemResultadoNotificacao_DestNome1 = StringUtil.PadR( StringUtil.RTrim( AV19ContagemResultadoNotificacao_DestNome1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultadoNotificacao_DestNome1", AV19ContagemResultadoNotificacao_DestNome1);
            lV24ContagemResultadoNotificacao_DestNome2 = StringUtil.PadR( StringUtil.RTrim( AV24ContagemResultadoNotificacao_DestNome2), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultadoNotificacao_DestNome2", AV24ContagemResultadoNotificacao_DestNome2);
            lV24ContagemResultadoNotificacao_DestNome2 = StringUtil.PadR( StringUtil.RTrim( AV24ContagemResultadoNotificacao_DestNome2), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultadoNotificacao_DestNome2", AV24ContagemResultadoNotificacao_DestNome2);
            lV29ContagemResultadoNotificacao_DestNome3 = StringUtil.PadR( StringUtil.RTrim( AV29ContagemResultadoNotificacao_DestNome3), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultadoNotificacao_DestNome3", AV29ContagemResultadoNotificacao_DestNome3);
            lV29ContagemResultadoNotificacao_DestNome3 = StringUtil.PadR( StringUtil.RTrim( AV29ContagemResultadoNotificacao_DestNome3), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultadoNotificacao_DestNome3", AV29ContagemResultadoNotificacao_DestNome3);
            /* Using cursor H00QS2 */
            pr_default.execute(0, new Object[] {AV7InContagemResultadoNotificacao_Codigo, AV18ContagemResultadoNotificacao_DestPesCod1, AV18ContagemResultadoNotificacao_DestPesCod1, AV18ContagemResultadoNotificacao_DestPesCod1, lV19ContagemResultadoNotificacao_DestNome1, lV19ContagemResultadoNotificacao_DestNome1, AV23ContagemResultadoNotificacao_DestPesCod2, AV23ContagemResultadoNotificacao_DestPesCod2, AV23ContagemResultadoNotificacao_DestPesCod2, lV24ContagemResultadoNotificacao_DestNome2, lV24ContagemResultadoNotificacao_DestNome2, AV28ContagemResultadoNotificacao_DestPesCod3, AV28ContagemResultadoNotificacao_DestPesCod3, AV28ContagemResultadoNotificacao_DestPesCod3, lV29ContagemResultadoNotificacao_DestNome3, lV29ContagemResultadoNotificacao_DestNome3, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_83_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1412ContagemResultadoNotificacao_Codigo = H00QS2_A1412ContagemResultadoNotificacao_Codigo[0];
               A1416ContagemResultadoNotificacao_DataHora = H00QS2_A1416ContagemResultadoNotificacao_DataHora[0];
               n1416ContagemResultadoNotificacao_DataHora = H00QS2_n1416ContagemResultadoNotificacao_DataHora[0];
               A1419ContagemResultadoNotificacao_DestEmail = H00QS2_A1419ContagemResultadoNotificacao_DestEmail[0];
               n1419ContagemResultadoNotificacao_DestEmail = H00QS2_n1419ContagemResultadoNotificacao_DestEmail[0];
               A1421ContagemResultadoNotificacao_DestNome = H00QS2_A1421ContagemResultadoNotificacao_DestNome[0];
               n1421ContagemResultadoNotificacao_DestNome = H00QS2_n1421ContagemResultadoNotificacao_DestNome[0];
               A1426ContagemResultadoNotificacao_DestPesCod = H00QS2_A1426ContagemResultadoNotificacao_DestPesCod[0];
               n1426ContagemResultadoNotificacao_DestPesCod = H00QS2_n1426ContagemResultadoNotificacao_DestPesCod[0];
               A1414ContagemResultadoNotificacao_DestCod = H00QS2_A1414ContagemResultadoNotificacao_DestCod[0];
               A1416ContagemResultadoNotificacao_DataHora = H00QS2_A1416ContagemResultadoNotificacao_DataHora[0];
               n1416ContagemResultadoNotificacao_DataHora = H00QS2_n1416ContagemResultadoNotificacao_DataHora[0];
               A1426ContagemResultadoNotificacao_DestPesCod = H00QS2_A1426ContagemResultadoNotificacao_DestPesCod[0];
               n1426ContagemResultadoNotificacao_DestPesCod = H00QS2_n1426ContagemResultadoNotificacao_DestPesCod[0];
               A1421ContagemResultadoNotificacao_DestNome = H00QS2_A1421ContagemResultadoNotificacao_DestNome[0];
               n1421ContagemResultadoNotificacao_DestNome = H00QS2_n1421ContagemResultadoNotificacao_DestNome[0];
               /* Execute user event: E24QS2 */
               E24QS2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 83;
            WBQS0( ) ;
         }
         nGXsfl_83_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV16DynamicFiltersSelector1 ,
                                              AV17DynamicFiltersOperator1 ,
                                              AV18ContagemResultadoNotificacao_DestPesCod1 ,
                                              AV19ContagemResultadoNotificacao_DestNome1 ,
                                              AV20DynamicFiltersEnabled2 ,
                                              AV21DynamicFiltersSelector2 ,
                                              AV22DynamicFiltersOperator2 ,
                                              AV23ContagemResultadoNotificacao_DestPesCod2 ,
                                              AV24ContagemResultadoNotificacao_DestNome2 ,
                                              AV25DynamicFiltersEnabled3 ,
                                              AV26DynamicFiltersSelector3 ,
                                              AV27DynamicFiltersOperator3 ,
                                              AV28ContagemResultadoNotificacao_DestPesCod3 ,
                                              AV29ContagemResultadoNotificacao_DestNome3 ,
                                              A1426ContagemResultadoNotificacao_DestPesCod ,
                                              A1421ContagemResultadoNotificacao_DestNome ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc ,
                                              A1412ContagemResultadoNotificacao_Codigo ,
                                              AV7InContagemResultadoNotificacao_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                              TypeConstants.LONG, TypeConstants.LONG
                                              }
         });
         lV19ContagemResultadoNotificacao_DestNome1 = StringUtil.PadR( StringUtil.RTrim( AV19ContagemResultadoNotificacao_DestNome1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultadoNotificacao_DestNome1", AV19ContagemResultadoNotificacao_DestNome1);
         lV19ContagemResultadoNotificacao_DestNome1 = StringUtil.PadR( StringUtil.RTrim( AV19ContagemResultadoNotificacao_DestNome1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultadoNotificacao_DestNome1", AV19ContagemResultadoNotificacao_DestNome1);
         lV24ContagemResultadoNotificacao_DestNome2 = StringUtil.PadR( StringUtil.RTrim( AV24ContagemResultadoNotificacao_DestNome2), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultadoNotificacao_DestNome2", AV24ContagemResultadoNotificacao_DestNome2);
         lV24ContagemResultadoNotificacao_DestNome2 = StringUtil.PadR( StringUtil.RTrim( AV24ContagemResultadoNotificacao_DestNome2), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultadoNotificacao_DestNome2", AV24ContagemResultadoNotificacao_DestNome2);
         lV29ContagemResultadoNotificacao_DestNome3 = StringUtil.PadR( StringUtil.RTrim( AV29ContagemResultadoNotificacao_DestNome3), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultadoNotificacao_DestNome3", AV29ContagemResultadoNotificacao_DestNome3);
         lV29ContagemResultadoNotificacao_DestNome3 = StringUtil.PadR( StringUtil.RTrim( AV29ContagemResultadoNotificacao_DestNome3), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultadoNotificacao_DestNome3", AV29ContagemResultadoNotificacao_DestNome3);
         /* Using cursor H00QS3 */
         pr_default.execute(1, new Object[] {AV7InContagemResultadoNotificacao_Codigo, AV18ContagemResultadoNotificacao_DestPesCod1, AV18ContagemResultadoNotificacao_DestPesCod1, AV18ContagemResultadoNotificacao_DestPesCod1, lV19ContagemResultadoNotificacao_DestNome1, lV19ContagemResultadoNotificacao_DestNome1, AV23ContagemResultadoNotificacao_DestPesCod2, AV23ContagemResultadoNotificacao_DestPesCod2, AV23ContagemResultadoNotificacao_DestPesCod2, lV24ContagemResultadoNotificacao_DestNome2, lV24ContagemResultadoNotificacao_DestNome2, AV28ContagemResultadoNotificacao_DestPesCod3, AV28ContagemResultadoNotificacao_DestPesCod3, AV28ContagemResultadoNotificacao_DestPesCod3, lV29ContagemResultadoNotificacao_DestNome3, lV29ContagemResultadoNotificacao_DestNome3});
         GRID_nRecordCount = H00QS3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemResultadoNotificacao_DestPesCod1, AV19ContagemResultadoNotificacao_DestNome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemResultadoNotificacao_DestPesCod2, AV24ContagemResultadoNotificacao_DestNome2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28ContagemResultadoNotificacao_DestPesCod3, AV29ContagemResultadoNotificacao_DestNome3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7InContagemResultadoNotificacao_Codigo, A1419ContagemResultadoNotificacao_DestEmail) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemResultadoNotificacao_DestPesCod1, AV19ContagemResultadoNotificacao_DestNome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemResultadoNotificacao_DestPesCod2, AV24ContagemResultadoNotificacao_DestNome2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28ContagemResultadoNotificacao_DestPesCod3, AV29ContagemResultadoNotificacao_DestNome3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7InContagemResultadoNotificacao_Codigo, A1419ContagemResultadoNotificacao_DestEmail) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemResultadoNotificacao_DestPesCod1, AV19ContagemResultadoNotificacao_DestNome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemResultadoNotificacao_DestPesCod2, AV24ContagemResultadoNotificacao_DestNome2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28ContagemResultadoNotificacao_DestPesCod3, AV29ContagemResultadoNotificacao_DestNome3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7InContagemResultadoNotificacao_Codigo, A1419ContagemResultadoNotificacao_DestEmail) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemResultadoNotificacao_DestPesCod1, AV19ContagemResultadoNotificacao_DestNome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemResultadoNotificacao_DestPesCod2, AV24ContagemResultadoNotificacao_DestNome2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28ContagemResultadoNotificacao_DestPesCod3, AV29ContagemResultadoNotificacao_DestNome3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7InContagemResultadoNotificacao_Codigo, A1419ContagemResultadoNotificacao_DestEmail) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemResultadoNotificacao_DestPesCod1, AV19ContagemResultadoNotificacao_DestNome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemResultadoNotificacao_DestPesCod2, AV24ContagemResultadoNotificacao_DestNome2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28ContagemResultadoNotificacao_DestPesCod3, AV29ContagemResultadoNotificacao_DestNome3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7InContagemResultadoNotificacao_Codigo, A1419ContagemResultadoNotificacao_DestEmail) ;
         }
         return (int)(0) ;
      }

      protected void STRUPQS0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E22QS2 */
         E22QS2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV14OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultadonotificacao_destpescod1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultadonotificacao_destpescod1_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD1");
               GX_FocusControl = edtavContagemresultadonotificacao_destpescod1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV18ContagemResultadoNotificacao_DestPesCod1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultadoNotificacao_DestPesCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContagemResultadoNotificacao_DestPesCod1), 6, 0)));
            }
            else
            {
               AV18ContagemResultadoNotificacao_DestPesCod1 = (int)(context.localUtil.CToN( cgiGet( edtavContagemresultadonotificacao_destpescod1_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultadoNotificacao_DestPesCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContagemResultadoNotificacao_DestPesCod1), 6, 0)));
            }
            AV19ContagemResultadoNotificacao_DestNome1 = StringUtil.Upper( cgiGet( edtavContagemresultadonotificacao_destnome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultadoNotificacao_DestNome1", AV19ContagemResultadoNotificacao_DestNome1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV21DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultadonotificacao_destpescod2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultadonotificacao_destpescod2_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD2");
               GX_FocusControl = edtavContagemresultadonotificacao_destpescod2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV23ContagemResultadoNotificacao_DestPesCod2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultadoNotificacao_DestPesCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23ContagemResultadoNotificacao_DestPesCod2), 6, 0)));
            }
            else
            {
               AV23ContagemResultadoNotificacao_DestPesCod2 = (int)(context.localUtil.CToN( cgiGet( edtavContagemresultadonotificacao_destpescod2_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultadoNotificacao_DestPesCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23ContagemResultadoNotificacao_DestPesCod2), 6, 0)));
            }
            AV24ContagemResultadoNotificacao_DestNome2 = StringUtil.Upper( cgiGet( edtavContagemresultadonotificacao_destnome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultadoNotificacao_DestNome2", AV24ContagemResultadoNotificacao_DestNome2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV26DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV27DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultadonotificacao_destpescod3_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultadonotificacao_destpescod3_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD3");
               GX_FocusControl = edtavContagemresultadonotificacao_destpescod3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV28ContagemResultadoNotificacao_DestPesCod3 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ContagemResultadoNotificacao_DestPesCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28ContagemResultadoNotificacao_DestPesCod3), 6, 0)));
            }
            else
            {
               AV28ContagemResultadoNotificacao_DestPesCod3 = (int)(context.localUtil.CToN( cgiGet( edtavContagemresultadonotificacao_destpescod3_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ContagemResultadoNotificacao_DestPesCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28ContagemResultadoNotificacao_DestPesCod3), 6, 0)));
            }
            AV29ContagemResultadoNotificacao_DestNome3 = StringUtil.Upper( cgiGet( edtavContagemresultadonotificacao_destnome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultadoNotificacao_DestNome3", AV29ContagemResultadoNotificacao_DestNome3);
            AV20DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
            AV25DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
            /* Read saved values. */
            nRC_GXsfl_83 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_83"), ",", "."));
            AV34GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV35GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD1"), ",", ".") != Convert.ToDecimal( AV18ContagemResultadoNotificacao_DestPesCod1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_DESTNOME1"), AV19ContagemResultadoNotificacao_DestNome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD2"), ",", ".") != Convert.ToDecimal( AV23ContagemResultadoNotificacao_DestPesCod2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_DESTNOME2"), AV24ContagemResultadoNotificacao_DestNome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV26DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV27DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD3"), ",", ".") != Convert.ToDecimal( AV28ContagemResultadoNotificacao_DestPesCod3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_DESTNOME3"), AV29ContagemResultadoNotificacao_DestNome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV25DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E22QS2 */
         E22QS2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22QS2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV16DynamicFiltersSelector1 = "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector2 = "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersSelector3 = "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         Form.Caption = "Select Destinatario";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "da Notifica��o", 0);
         cmbavOrderedby.addItem("2", "C�digo", 0);
         cmbavOrderedby.addItem("3", "codigo", 0);
         cmbavOrderedby.addItem("4", "Destinat�rio", 0);
         cmbavOrderedby.addItem("5", "Email", 0);
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
      }

      protected void E23QS2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator1.addItem("2", ">", 0);
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "<", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "=", 0);
               cmbavDynamicfiltersoperator2.addItem("2", ">", 0);
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            if ( AV25DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "<", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "=", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", ">", 0);
               }
               else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
            }
         }
         edtContagemResultadoNotificacao_DestCod_Titleformat = 2;
         edtContagemResultadoNotificacao_DestCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV14OrderedBy==2) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "C�digo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_DestCod_Internalname, "Title", edtContagemResultadoNotificacao_DestCod_Title);
         edtContagemResultadoNotificacao_DestPesCod_Titleformat = 2;
         edtContagemResultadoNotificacao_DestPesCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV14OrderedBy==3) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "codigo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_DestPesCod_Internalname, "Title", edtContagemResultadoNotificacao_DestPesCod_Title);
         edtContagemResultadoNotificacao_DestNome_Titleformat = 2;
         edtContagemResultadoNotificacao_DestNome_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV14OrderedBy==4) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Destinat�rio", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_DestNome_Internalname, "Title", edtContagemResultadoNotificacao_DestNome_Title);
         edtContagemResultadoNotificacao_DestEmail_Titleformat = 2;
         edtContagemResultadoNotificacao_DestEmail_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 5);' >%5</span>", ((AV14OrderedBy==5) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Email", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_DestEmail_Internalname, "Title", edtContagemResultadoNotificacao_DestEmail_Title);
         AV34GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34GridCurrentPage), 10, 0)));
         AV35GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35GridPageCount), 10, 0)));
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E11QS2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV33PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV33PageToGo) ;
         }
      }

      private void E24QS2( )
      {
         /* Grid_Load Routine */
         AV36Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV36Select);
         AV39Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         edtContagemResultadoNotificacao_DestEmail_Link = "mailto:"+A1419ContagemResultadoNotificacao_DestEmail;
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 83;
         }
         sendrow_832( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_83_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(83, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E25QS2 */
         E25QS2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25QS2( )
      {
         /* Enter Routine */
         AV8InOutContagemResultadoNotificacao_DestCod = A1414ContagemResultadoNotificacao_DestCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContagemResultadoNotificacao_DestCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutContagemResultadoNotificacao_DestCod), 6, 0)));
         AV9InOutContagemResultadoNotificacao_DestPesCod = A1426ContagemResultadoNotificacao_DestPesCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9InOutContagemResultadoNotificacao_DestPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9InOutContagemResultadoNotificacao_DestPesCod), 6, 0)));
         context.setWebReturnParms(new Object[] {(int)AV8InOutContagemResultadoNotificacao_DestCod,(int)AV9InOutContagemResultadoNotificacao_DestPesCod});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E12QS2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E17QS2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV20DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemResultadoNotificacao_DestPesCod1, AV19ContagemResultadoNotificacao_DestNome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemResultadoNotificacao_DestPesCod2, AV24ContagemResultadoNotificacao_DestNome2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28ContagemResultadoNotificacao_DestPesCod3, AV29ContagemResultadoNotificacao_DestNome3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7InContagemResultadoNotificacao_Codigo, A1419ContagemResultadoNotificacao_DestEmail) ;
      }

      protected void E13QS2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV31DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV31DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemResultadoNotificacao_DestPesCod1, AV19ContagemResultadoNotificacao_DestNome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemResultadoNotificacao_DestPesCod2, AV24ContagemResultadoNotificacao_DestNome2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28ContagemResultadoNotificacao_DestPesCod3, AV29ContagemResultadoNotificacao_DestNome3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7InContagemResultadoNotificacao_Codigo, A1419ContagemResultadoNotificacao_DestEmail) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E18QS2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV17DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E19QS2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV25DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemResultadoNotificacao_DestPesCod1, AV19ContagemResultadoNotificacao_DestNome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemResultadoNotificacao_DestPesCod2, AV24ContagemResultadoNotificacao_DestNome2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28ContagemResultadoNotificacao_DestPesCod3, AV29ContagemResultadoNotificacao_DestNome3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7InContagemResultadoNotificacao_Codigo, A1419ContagemResultadoNotificacao_DestEmail) ;
      }

      protected void E14QS2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemResultadoNotificacao_DestPesCod1, AV19ContagemResultadoNotificacao_DestNome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemResultadoNotificacao_DestPesCod2, AV24ContagemResultadoNotificacao_DestNome2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28ContagemResultadoNotificacao_DestPesCod3, AV29ContagemResultadoNotificacao_DestNome3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7InContagemResultadoNotificacao_Codigo, A1419ContagemResultadoNotificacao_DestEmail) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E20QS2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E15QS2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV25DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemResultadoNotificacao_DestPesCod1, AV19ContagemResultadoNotificacao_DestNome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemResultadoNotificacao_DestPesCod2, AV24ContagemResultadoNotificacao_DestNome2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28ContagemResultadoNotificacao_DestPesCod3, AV29ContagemResultadoNotificacao_DestNome3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7InContagemResultadoNotificacao_Codigo, A1419ContagemResultadoNotificacao_DestEmail) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E21QS2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV27DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E16QS2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavContagemresultadonotificacao_destpescod1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadonotificacao_destpescod1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadonotificacao_destpescod1_Visible), 5, 0)));
         edtavContagemresultadonotificacao_destnome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadonotificacao_destnome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadonotificacao_destnome1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 )
         {
            edtavContagemresultadonotificacao_destpescod1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadonotificacao_destpescod1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadonotificacao_destpescod1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 )
         {
            edtavContagemresultadonotificacao_destnome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadonotificacao_destnome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadonotificacao_destnome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavContagemresultadonotificacao_destpescod2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadonotificacao_destpescod2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadonotificacao_destpescod2_Visible), 5, 0)));
         edtavContagemresultadonotificacao_destnome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadonotificacao_destnome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadonotificacao_destnome2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 )
         {
            edtavContagemresultadonotificacao_destpescod2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadonotificacao_destpescod2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadonotificacao_destpescod2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 )
         {
            edtavContagemresultadonotificacao_destnome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadonotificacao_destnome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadonotificacao_destnome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavContagemresultadonotificacao_destpescod3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadonotificacao_destpescod3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadonotificacao_destpescod3_Visible), 5, 0)));
         edtavContagemresultadonotificacao_destnome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadonotificacao_destnome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadonotificacao_destnome3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 )
         {
            edtavContagemresultadonotificacao_destpescod3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadonotificacao_destpescod3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadonotificacao_destpescod3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 )
         {
            edtavContagemresultadonotificacao_destnome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadonotificacao_destnome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadonotificacao_destnome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S162( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         AV21DynamicFiltersSelector2 = "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         AV23ContagemResultadoNotificacao_DestPesCod2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultadoNotificacao_DestPesCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23ContagemResultadoNotificacao_DestPesCod2), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
         AV26DynamicFiltersSelector3 = "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
         AV27DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
         AV28ContagemResultadoNotificacao_DestPesCod3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ContagemResultadoNotificacao_DestPesCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28ContagemResultadoNotificacao_DestPesCod3), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S172( )
      {
         /* 'CLEANFILTERS' Routine */
         AV16DynamicFiltersSelector1 = "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV17DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         AV18ContagemResultadoNotificacao_DestPesCod1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultadoNotificacao_DestPesCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContagemResultadoNotificacao_DestPesCod1), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV18ContagemResultadoNotificacao_DestPesCod1 = (int)(NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultadoNotificacao_DestPesCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContagemResultadoNotificacao_DestPesCod1), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV19ContagemResultadoNotificacao_DestNome1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultadoNotificacao_DestNome1", AV19ContagemResultadoNotificacao_DestNome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV20DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(2));
               AV21DynamicFiltersSelector2 = AV13GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV23ContagemResultadoNotificacao_DestPesCod2 = (int)(NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultadoNotificacao_DestPesCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23ContagemResultadoNotificacao_DestPesCod2), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV24ContagemResultadoNotificacao_DestNome2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultadoNotificacao_DestNome2", AV24ContagemResultadoNotificacao_DestNome2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV25DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
                  AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(3));
                  AV26DynamicFiltersSelector3 = AV13GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 )
                  {
                     AV27DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
                     AV28ContagemResultadoNotificacao_DestPesCod3 = (int)(NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ContagemResultadoNotificacao_DestPesCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28ContagemResultadoNotificacao_DestPesCod3), 6, 0)));
                  }
                  else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 )
                  {
                     AV27DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
                     AV29ContagemResultadoNotificacao_DestNome3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultadoNotificacao_DestNome3", AV29ContagemResultadoNotificacao_DestNome3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV30DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S152( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV31DynamicFiltersIgnoreFirst )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 ) && ! (0==AV18ContagemResultadoNotificacao_DestPesCod1) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV18ContagemResultadoNotificacao_DestPesCod1), 6, 0);
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19ContagemResultadoNotificacao_DestNome1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV19ContagemResultadoNotificacao_DestNome1;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 ) && ! (0==AV23ContagemResultadoNotificacao_DestPesCod2) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV23ContagemResultadoNotificacao_DestPesCod2), 6, 0);
               AV13GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV24ContagemResultadoNotificacao_DestNome2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV24ContagemResultadoNotificacao_DestNome2;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV25DynamicFiltersEnabled3 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV26DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 ) && ! (0==AV28ContagemResultadoNotificacao_DestPesCod3) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV28ContagemResultadoNotificacao_DestPesCod3), 6, 0);
               AV13GridStateDynamicFilter.gxTpr_Operator = AV27DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV29ContagemResultadoNotificacao_DestNome3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV29ContagemResultadoNotificacao_DestNome3;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV27DynamicFiltersOperator3;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_QS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_QS2( true) ;
         }
         else
         {
            wb_table2_5_QS2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_QS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_77_QS2( true) ;
         }
         else
         {
            wb_table3_77_QS2( false) ;
         }
         return  ;
      }

      protected void wb_table3_77_QS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_QS2e( true) ;
         }
         else
         {
            wb_table1_2_QS2e( false) ;
         }
      }

      protected void wb_table3_77_QS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_80_QS2( true) ;
         }
         else
         {
            wb_table4_80_QS2( false) ;
         }
         return  ;
      }

      protected void wb_table4_80_QS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_77_QS2e( true) ;
         }
         else
         {
            wb_table3_77_QS2e( false) ;
         }
      }

      protected void wb_table4_80_QS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"83\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoNotificacao_DestCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoNotificacao_DestCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoNotificacao_DestCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoNotificacao_DestPesCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoNotificacao_DestPesCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoNotificacao_DestPesCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoNotificacao_DestNome_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoNotificacao_DestNome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoNotificacao_DestNome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoNotificacao_DestEmail_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoNotificacao_DestEmail_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoNotificacao_DestEmail_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV36Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1414ContagemResultadoNotificacao_DestCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoNotificacao_DestCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_DestCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1426ContagemResultadoNotificacao_DestPesCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoNotificacao_DestPesCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_DestPesCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1421ContagemResultadoNotificacao_DestNome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoNotificacao_DestNome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_DestNome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1419ContagemResultadoNotificacao_DestEmail);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoNotificacao_DestEmail_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_DestEmail_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContagemResultadoNotificacao_DestEmail_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 83 )
         {
            wbEnd = 0;
            nRC_GXsfl_83 = (short)(nGXsfl_83_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_80_QS2e( true) ;
         }
         else
         {
            wb_table4_80_QS2e( false) ;
         }
      }

      protected void wb_table2_5_QS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoNotificacaoDestinatario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptContagemResultadoNotificacaoDestinatario.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptContagemResultadoNotificacaoDestinatario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_14_QS2( true) ;
         }
         else
         {
            wb_table5_14_QS2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_QS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_QS2e( true) ;
         }
         else
         {
            wb_table2_5_QS2e( false) ;
         }
      }

      protected void wb_table5_14_QS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoNotificacaoDestinatario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_QS2( true) ;
         }
         else
         {
            wb_table6_19_QS2( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_QS2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptContagemResultadoNotificacaoDestinatario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_QS2e( true) ;
         }
         else
         {
            wb_table5_14_QS2e( false) ;
         }
      }

      protected void wb_table6_19_QS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoNotificacaoDestinatario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptContagemResultadoNotificacaoDestinatario.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoNotificacaoDestinatario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_QS2( true) ;
         }
         else
         {
            wb_table7_28_QS2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_QS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoNotificacaoDestinatario.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoNotificacaoDestinatario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoNotificacaoDestinatario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", "", true, "HLP_PromptContagemResultadoNotificacaoDestinatario.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoNotificacaoDestinatario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_46_QS2( true) ;
         }
         else
         {
            wb_table8_46_QS2( false) ;
         }
         return  ;
      }

      protected void wb_table8_46_QS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoNotificacaoDestinatario.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoNotificacaoDestinatario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoNotificacaoDestinatario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV26DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", "", true, "HLP_PromptContagemResultadoNotificacaoDestinatario.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoNotificacaoDestinatario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_64_QS2( true) ;
         }
         else
         {
            wb_table9_64_QS2( false) ;
         }
         return  ;
      }

      protected void wb_table9_64_QS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoNotificacaoDestinatario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_QS2e( true) ;
         }
         else
         {
            wb_table6_19_QS2e( false) ;
         }
      }

      protected void wb_table9_64_QS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "", true, "HLP_PromptContagemResultadoNotificacaoDestinatario.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadonotificacao_destpescod3_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV28ContagemResultadoNotificacao_DestPesCod3), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV28ContagemResultadoNotificacao_DestPesCod3), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadonotificacao_destpescod3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultadonotificacao_destpescod3_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoNotificacaoDestinatario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadonotificacao_destnome3_Internalname, StringUtil.RTrim( AV29ContagemResultadoNotificacao_DestNome3), StringUtil.RTrim( context.localUtil.Format( AV29ContagemResultadoNotificacao_DestNome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,70);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadonotificacao_destnome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultadonotificacao_destnome3_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemResultadoNotificacaoDestinatario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_64_QS2e( true) ;
         }
         else
         {
            wb_table9_64_QS2e( false) ;
         }
      }

      protected void wb_table8_46_QS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_PromptContagemResultadoNotificacaoDestinatario.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadonotificacao_destpescod2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23ContagemResultadoNotificacao_DestPesCod2), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV23ContagemResultadoNotificacao_DestPesCod2), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadonotificacao_destpescod2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultadonotificacao_destpescod2_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoNotificacaoDestinatario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadonotificacao_destnome2_Internalname, StringUtil.RTrim( AV24ContagemResultadoNotificacao_DestNome2), StringUtil.RTrim( context.localUtil.Format( AV24ContagemResultadoNotificacao_DestNome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadonotificacao_destnome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultadonotificacao_destnome2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemResultadoNotificacaoDestinatario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_46_QS2e( true) ;
         }
         else
         {
            wb_table8_46_QS2e( false) ;
         }
      }

      protected void wb_table7_28_QS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_PromptContagemResultadoNotificacaoDestinatario.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadonotificacao_destpescod1_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18ContagemResultadoNotificacao_DestPesCod1), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV18ContagemResultadoNotificacao_DestPesCod1), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadonotificacao_destpescod1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultadonotificacao_destpescod1_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoNotificacaoDestinatario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadonotificacao_destnome1_Internalname, StringUtil.RTrim( AV19ContagemResultadoNotificacao_DestNome1), StringUtil.RTrim( context.localUtil.Format( AV19ContagemResultadoNotificacao_DestNome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadonotificacao_destnome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultadonotificacao_destnome1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemResultadoNotificacaoDestinatario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_QS2e( true) ;
         }
         else
         {
            wb_table7_28_QS2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InContagemResultadoNotificacao_Codigo = Convert.ToInt64(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InContagemResultadoNotificacao_Codigo), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vINCONTAGEMRESULTADONOTIFICACAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7InContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9")));
         AV8InOutContagemResultadoNotificacao_DestCod = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContagemResultadoNotificacao_DestCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutContagemResultadoNotificacao_DestCod), 6, 0)));
         AV9InOutContagemResultadoNotificacao_DestPesCod = Convert.ToInt32(getParm(obj,2));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9InOutContagemResultadoNotificacao_DestPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9InOutContagemResultadoNotificacao_DestPesCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAQS2( ) ;
         WSQS2( ) ;
         WEQS2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299453371");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptcontagemresultadonotificacaodestinatario.js", "?20205299453371");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_832( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_83_idx;
         edtContagemResultadoNotificacao_DestCod_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DESTCOD_"+sGXsfl_83_idx;
         edtContagemResultadoNotificacao_DestPesCod_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD_"+sGXsfl_83_idx;
         edtContagemResultadoNotificacao_DestNome_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DESTNOME_"+sGXsfl_83_idx;
         edtContagemResultadoNotificacao_DestEmail_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL_"+sGXsfl_83_idx;
      }

      protected void SubsflControlProps_fel_832( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_83_fel_idx;
         edtContagemResultadoNotificacao_DestCod_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DESTCOD_"+sGXsfl_83_fel_idx;
         edtContagemResultadoNotificacao_DestPesCod_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD_"+sGXsfl_83_fel_idx;
         edtContagemResultadoNotificacao_DestNome_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DESTNOME_"+sGXsfl_83_fel_idx;
         edtContagemResultadoNotificacao_DestEmail_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL_"+sGXsfl_83_fel_idx;
      }

      protected void sendrow_832( )
      {
         SubsflControlProps_832( ) ;
         WBQS0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_83_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_83_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_83_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 84,'',false,'',83)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV36Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV36Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV39Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV36Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV36Select)) ? AV39Select_GXI : context.PathToRelativeUrl( AV36Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_83_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV36Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_DestCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1414ContagemResultadoNotificacao_DestCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1414ContagemResultadoNotificacao_DestCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_DestCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_DestPesCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1426ContagemResultadoNotificacao_DestPesCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1426ContagemResultadoNotificacao_DestPesCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_DestPesCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_DestNome_Internalname,StringUtil.RTrim( A1421ContagemResultadoNotificacao_DestNome),StringUtil.RTrim( context.localUtil.Format( A1421ContagemResultadoNotificacao_DestNome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_DestNome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_DestEmail_Internalname,(String)A1419ContagemResultadoNotificacao_DestEmail,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtContagemResultadoNotificacao_DestEmail_Link,(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_DestEmail_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"email",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)0,(bool)true,(String)"Email",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADONOTIFICACAO_DESTCOD"+"_"+sGXsfl_83_idx, GetSecureSignedToken( sGXsfl_83_idx, context.localUtil.Format( (decimal)(A1414ContagemResultadoNotificacao_DestCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL"+"_"+sGXsfl_83_idx, GetSecureSignedToken( sGXsfl_83_idx, StringUtil.RTrim( context.localUtil.Format( A1419ContagemResultadoNotificacao_DestEmail, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_83_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_83_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_83_idx+1));
            sGXsfl_83_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_83_idx), 4, 0)), 4, "0");
            SubsflControlProps_832( ) ;
         }
         /* End function sendrow_832 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavContagemresultadonotificacao_destpescod1_Internalname = "vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD1";
         edtavContagemresultadonotificacao_destnome1_Internalname = "vCONTAGEMRESULTADONOTIFICACAO_DESTNOME1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavContagemresultadonotificacao_destpescod2_Internalname = "vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD2";
         edtavContagemresultadonotificacao_destnome2_Internalname = "vCONTAGEMRESULTADONOTIFICACAO_DESTNOME2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavContagemresultadonotificacao_destpescod3_Internalname = "vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD3";
         edtavContagemresultadonotificacao_destnome3_Internalname = "vCONTAGEMRESULTADONOTIFICACAO_DESTNOME3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtContagemResultadoNotificacao_DestCod_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DESTCOD";
         edtContagemResultadoNotificacao_DestPesCod_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD";
         edtContagemResultadoNotificacao_DestNome_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DESTNOME";
         edtContagemResultadoNotificacao_DestEmail_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContagemResultadoNotificacao_DestEmail_Jsonclick = "";
         edtContagemResultadoNotificacao_DestNome_Jsonclick = "";
         edtContagemResultadoNotificacao_DestPesCod_Jsonclick = "";
         edtContagemResultadoNotificacao_DestCod_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavContagemresultadonotificacao_destnome1_Jsonclick = "";
         edtavContagemresultadonotificacao_destpescod1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavContagemresultadonotificacao_destnome2_Jsonclick = "";
         edtavContagemresultadonotificacao_destpescod2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavContagemresultadonotificacao_destnome3_Jsonclick = "";
         edtavContagemresultadonotificacao_destpescod3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtContagemResultadoNotificacao_DestEmail_Link = "";
         edtavSelect_Tooltiptext = "Selecionar";
         edtContagemResultadoNotificacao_DestEmail_Titleformat = 0;
         edtContagemResultadoNotificacao_DestNome_Titleformat = 0;
         edtContagemResultadoNotificacao_DestPesCod_Titleformat = 0;
         edtContagemResultadoNotificacao_DestCod_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavContagemresultadonotificacao_destnome3_Visible = 1;
         edtavContagemresultadonotificacao_destpescod3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavContagemresultadonotificacao_destnome2_Visible = 1;
         edtavContagemresultadonotificacao_destpescod2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavContagemresultadonotificacao_destnome1_Visible = 1;
         edtavContagemresultadonotificacao_destpescod1_Visible = 1;
         edtContagemResultadoNotificacao_DestEmail_Title = "Email";
         edtContagemResultadoNotificacao_DestNome_Title = "Destinat�rio";
         edtContagemResultadoNotificacao_DestPesCod_Title = "codigo";
         edtContagemResultadoNotificacao_DestCod_Title = "C�digo";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Select Destinatario";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemResultadoNotificacao_DestPesCod1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD1',pic:'ZZZZZ9',nv:0},{av:'AV19ContagemResultadoNotificacao_DestNome1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME1',pic:'@!',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemResultadoNotificacao_DestPesCod2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD2',pic:'ZZZZZ9',nv:0},{av:'AV24ContagemResultadoNotificacao_DestNome2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemResultadoNotificacao_DestPesCod3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD3',pic:'ZZZZZ9',nv:0},{av:'AV29ContagemResultadoNotificacao_DestNome3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME3',pic:'@!',nv:''},{av:'AV7InContagemResultadoNotificacao_Codigo',fld:'vINCONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',hsh:true,nv:0},{av:'A1419ContagemResultadoNotificacao_DestEmail',fld:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',pic:'',hsh:true,nv:''},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false}],oparms:[{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtContagemResultadoNotificacao_DestCod_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTCOD',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_DestCod_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTCOD',prop:'Title'},{av:'edtContagemResultadoNotificacao_DestPesCod_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_DestPesCod_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD',prop:'Title'},{av:'edtContagemResultadoNotificacao_DestNome_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTNOME',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_DestNome_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTNOME',prop:'Title'},{av:'edtContagemResultadoNotificacao_DestEmail_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_DestEmail_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',prop:'Title'},{av:'AV34GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV35GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11QS2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemResultadoNotificacao_DestPesCod1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD1',pic:'ZZZZZ9',nv:0},{av:'AV19ContagemResultadoNotificacao_DestNome1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemResultadoNotificacao_DestPesCod2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD2',pic:'ZZZZZ9',nv:0},{av:'AV24ContagemResultadoNotificacao_DestNome2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemResultadoNotificacao_DestPesCod3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD3',pic:'ZZZZZ9',nv:0},{av:'AV29ContagemResultadoNotificacao_DestNome3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7InContagemResultadoNotificacao_Codigo',fld:'vINCONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',hsh:true,nv:0},{av:'A1419ContagemResultadoNotificacao_DestEmail',fld:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',pic:'',hsh:true,nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E24QS2',iparms:[{av:'A1419ContagemResultadoNotificacao_DestEmail',fld:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',pic:'',hsh:true,nv:''}],oparms:[{av:'AV36Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'},{av:'edtContagemResultadoNotificacao_DestEmail_Link',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',prop:'Link'}]}");
         setEventMetadata("ENTER","{handler:'E25QS2',iparms:[{av:'A1414ContagemResultadoNotificacao_DestCod',fld:'CONTAGEMRESULTADONOTIFICACAO_DESTCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1426ContagemResultadoNotificacao_DestPesCod',fld:'CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV8InOutContagemResultadoNotificacao_DestCod',fld:'vINOUTCONTAGEMRESULTADONOTIFICACAO_DESTCOD',pic:'ZZZZZ9',nv:0},{av:'AV9InOutContagemResultadoNotificacao_DestPesCod',fld:'vINOUTCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E12QS2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemResultadoNotificacao_DestPesCod1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD1',pic:'ZZZZZ9',nv:0},{av:'AV19ContagemResultadoNotificacao_DestNome1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemResultadoNotificacao_DestPesCod2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD2',pic:'ZZZZZ9',nv:0},{av:'AV24ContagemResultadoNotificacao_DestNome2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemResultadoNotificacao_DestPesCod3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD3',pic:'ZZZZZ9',nv:0},{av:'AV29ContagemResultadoNotificacao_DestNome3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7InContagemResultadoNotificacao_Codigo',fld:'vINCONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',hsh:true,nv:0},{av:'A1419ContagemResultadoNotificacao_DestEmail',fld:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',pic:'',hsh:true,nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E17QS2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemResultadoNotificacao_DestPesCod1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD1',pic:'ZZZZZ9',nv:0},{av:'AV19ContagemResultadoNotificacao_DestNome1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemResultadoNotificacao_DestPesCod2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD2',pic:'ZZZZZ9',nv:0},{av:'AV24ContagemResultadoNotificacao_DestNome2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemResultadoNotificacao_DestPesCod3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD3',pic:'ZZZZZ9',nv:0},{av:'AV29ContagemResultadoNotificacao_DestNome3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7InContagemResultadoNotificacao_Codigo',fld:'vINCONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',hsh:true,nv:0},{av:'A1419ContagemResultadoNotificacao_DestEmail',fld:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',pic:'',hsh:true,nv:''}],oparms:[{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E13QS2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemResultadoNotificacao_DestPesCod1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD1',pic:'ZZZZZ9',nv:0},{av:'AV19ContagemResultadoNotificacao_DestNome1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemResultadoNotificacao_DestPesCod2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD2',pic:'ZZZZZ9',nv:0},{av:'AV24ContagemResultadoNotificacao_DestNome2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemResultadoNotificacao_DestPesCod3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD3',pic:'ZZZZZ9',nv:0},{av:'AV29ContagemResultadoNotificacao_DestNome3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7InContagemResultadoNotificacao_Codigo',fld:'vINCONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',hsh:true,nv:0},{av:'A1419ContagemResultadoNotificacao_DestEmail',fld:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',pic:'',hsh:true,nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemResultadoNotificacao_DestPesCod2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD2',pic:'ZZZZZ9',nv:0},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemResultadoNotificacao_DestPesCod3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemResultadoNotificacao_DestPesCod1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD1',pic:'ZZZZZ9',nv:0},{av:'AV19ContagemResultadoNotificacao_DestNome1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24ContagemResultadoNotificacao_DestNome2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME2',pic:'@!',nv:''},{av:'AV29ContagemResultadoNotificacao_DestNome3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME3',pic:'@!',nv:''},{av:'edtavContagemresultadonotificacao_destpescod2_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD2',prop:'Visible'},{av:'edtavContagemresultadonotificacao_destnome2_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagemresultadonotificacao_destpescod3_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD3',prop:'Visible'},{av:'edtavContagemresultadonotificacao_destnome3_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContagemresultadonotificacao_destpescod1_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD1',prop:'Visible'},{av:'edtavContagemresultadonotificacao_destnome1_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E18QS2',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavContagemresultadonotificacao_destpescod1_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD1',prop:'Visible'},{av:'edtavContagemresultadonotificacao_destnome1_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E19QS2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemResultadoNotificacao_DestPesCod1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD1',pic:'ZZZZZ9',nv:0},{av:'AV19ContagemResultadoNotificacao_DestNome1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemResultadoNotificacao_DestPesCod2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD2',pic:'ZZZZZ9',nv:0},{av:'AV24ContagemResultadoNotificacao_DestNome2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemResultadoNotificacao_DestPesCod3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD3',pic:'ZZZZZ9',nv:0},{av:'AV29ContagemResultadoNotificacao_DestNome3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7InContagemResultadoNotificacao_Codigo',fld:'vINCONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',hsh:true,nv:0},{av:'A1419ContagemResultadoNotificacao_DestEmail',fld:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',pic:'',hsh:true,nv:''}],oparms:[{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E14QS2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemResultadoNotificacao_DestPesCod1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD1',pic:'ZZZZZ9',nv:0},{av:'AV19ContagemResultadoNotificacao_DestNome1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemResultadoNotificacao_DestPesCod2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD2',pic:'ZZZZZ9',nv:0},{av:'AV24ContagemResultadoNotificacao_DestNome2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemResultadoNotificacao_DestPesCod3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD3',pic:'ZZZZZ9',nv:0},{av:'AV29ContagemResultadoNotificacao_DestNome3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7InContagemResultadoNotificacao_Codigo',fld:'vINCONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',hsh:true,nv:0},{av:'A1419ContagemResultadoNotificacao_DestEmail',fld:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',pic:'',hsh:true,nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemResultadoNotificacao_DestPesCod2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD2',pic:'ZZZZZ9',nv:0},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemResultadoNotificacao_DestPesCod3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemResultadoNotificacao_DestPesCod1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD1',pic:'ZZZZZ9',nv:0},{av:'AV19ContagemResultadoNotificacao_DestNome1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24ContagemResultadoNotificacao_DestNome2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME2',pic:'@!',nv:''},{av:'AV29ContagemResultadoNotificacao_DestNome3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME3',pic:'@!',nv:''},{av:'edtavContagemresultadonotificacao_destpescod2_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD2',prop:'Visible'},{av:'edtavContagemresultadonotificacao_destnome2_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagemresultadonotificacao_destpescod3_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD3',prop:'Visible'},{av:'edtavContagemresultadonotificacao_destnome3_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContagemresultadonotificacao_destpescod1_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD1',prop:'Visible'},{av:'edtavContagemresultadonotificacao_destnome1_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E20QS2',iparms:[{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavContagemresultadonotificacao_destpescod2_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD2',prop:'Visible'},{av:'edtavContagemresultadonotificacao_destnome2_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E15QS2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemResultadoNotificacao_DestPesCod1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD1',pic:'ZZZZZ9',nv:0},{av:'AV19ContagemResultadoNotificacao_DestNome1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemResultadoNotificacao_DestPesCod2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD2',pic:'ZZZZZ9',nv:0},{av:'AV24ContagemResultadoNotificacao_DestNome2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemResultadoNotificacao_DestPesCod3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD3',pic:'ZZZZZ9',nv:0},{av:'AV29ContagemResultadoNotificacao_DestNome3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7InContagemResultadoNotificacao_Codigo',fld:'vINCONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',hsh:true,nv:0},{av:'A1419ContagemResultadoNotificacao_DestEmail',fld:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',pic:'',hsh:true,nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemResultadoNotificacao_DestPesCod2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD2',pic:'ZZZZZ9',nv:0},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemResultadoNotificacao_DestPesCod3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemResultadoNotificacao_DestPesCod1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD1',pic:'ZZZZZ9',nv:0},{av:'AV19ContagemResultadoNotificacao_DestNome1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24ContagemResultadoNotificacao_DestNome2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME2',pic:'@!',nv:''},{av:'AV29ContagemResultadoNotificacao_DestNome3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME3',pic:'@!',nv:''},{av:'edtavContagemresultadonotificacao_destpescod2_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD2',prop:'Visible'},{av:'edtavContagemresultadonotificacao_destnome2_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagemresultadonotificacao_destpescod3_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD3',prop:'Visible'},{av:'edtavContagemresultadonotificacao_destnome3_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContagemresultadonotificacao_destpescod1_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD1',prop:'Visible'},{av:'edtavContagemresultadonotificacao_destnome1_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E21QS2',iparms:[{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtavContagemresultadonotificacao_destpescod3_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD3',prop:'Visible'},{av:'edtavContagemresultadonotificacao_destnome3_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E16QS2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemResultadoNotificacao_DestPesCod1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD1',pic:'ZZZZZ9',nv:0},{av:'AV19ContagemResultadoNotificacao_DestNome1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemResultadoNotificacao_DestPesCod2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD2',pic:'ZZZZZ9',nv:0},{av:'AV24ContagemResultadoNotificacao_DestNome2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemResultadoNotificacao_DestPesCod3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD3',pic:'ZZZZZ9',nv:0},{av:'AV29ContagemResultadoNotificacao_DestNome3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7InContagemResultadoNotificacao_Codigo',fld:'vINCONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',hsh:true,nv:0},{av:'A1419ContagemResultadoNotificacao_DestEmail',fld:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',pic:'',hsh:true,nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemResultadoNotificacao_DestPesCod1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD1',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavContagemresultadonotificacao_destpescod1_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD1',prop:'Visible'},{av:'edtavContagemresultadonotificacao_destnome1_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemResultadoNotificacao_DestPesCod2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD2',pic:'ZZZZZ9',nv:0},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28ContagemResultadoNotificacao_DestPesCod3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV19ContagemResultadoNotificacao_DestNome1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24ContagemResultadoNotificacao_DestNome2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME2',pic:'@!',nv:''},{av:'AV29ContagemResultadoNotificacao_DestNome3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME3',pic:'@!',nv:''},{av:'edtavContagemresultadonotificacao_destpescod2_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD2',prop:'Visible'},{av:'edtavContagemresultadonotificacao_destnome2_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagemresultadonotificacao_destpescod3_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTPESCOD3',prop:'Visible'},{av:'edtavContagemresultadonotificacao_destnome3_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_DESTNOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV16DynamicFiltersSelector1 = "";
         AV19ContagemResultadoNotificacao_DestNome1 = "";
         AV21DynamicFiltersSelector2 = "";
         AV24ContagemResultadoNotificacao_DestNome2 = "";
         AV26DynamicFiltersSelector3 = "";
         AV29ContagemResultadoNotificacao_DestNome3 = "";
         A1419ContagemResultadoNotificacao_DestEmail = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV36Select = "";
         AV39Select_GXI = "";
         A1421ContagemResultadoNotificacao_DestNome = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV19ContagemResultadoNotificacao_DestNome1 = "";
         lV24ContagemResultadoNotificacao_DestNome2 = "";
         lV29ContagemResultadoNotificacao_DestNome3 = "";
         H00QS2_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         H00QS2_A1416ContagemResultadoNotificacao_DataHora = new DateTime[] {DateTime.MinValue} ;
         H00QS2_n1416ContagemResultadoNotificacao_DataHora = new bool[] {false} ;
         H00QS2_A1419ContagemResultadoNotificacao_DestEmail = new String[] {""} ;
         H00QS2_n1419ContagemResultadoNotificacao_DestEmail = new bool[] {false} ;
         H00QS2_A1421ContagemResultadoNotificacao_DestNome = new String[] {""} ;
         H00QS2_n1421ContagemResultadoNotificacao_DestNome = new bool[] {false} ;
         H00QS2_A1426ContagemResultadoNotificacao_DestPesCod = new int[1] ;
         H00QS2_n1426ContagemResultadoNotificacao_DestPesCod = new bool[] {false} ;
         H00QS2_A1414ContagemResultadoNotificacao_DestCod = new int[1] ;
         A1416ContagemResultadoNotificacao_DataHora = (DateTime)(DateTime.MinValue);
         H00QS3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptcontagemresultadonotificacaodestinatario__default(),
            new Object[][] {
                new Object[] {
               H00QS2_A1412ContagemResultadoNotificacao_Codigo, H00QS2_A1416ContagemResultadoNotificacao_DataHora, H00QS2_n1416ContagemResultadoNotificacao_DataHora, H00QS2_A1419ContagemResultadoNotificacao_DestEmail, H00QS2_n1419ContagemResultadoNotificacao_DestEmail, H00QS2_A1421ContagemResultadoNotificacao_DestNome, H00QS2_n1421ContagemResultadoNotificacao_DestNome, H00QS2_A1426ContagemResultadoNotificacao_DestPesCod, H00QS2_n1426ContagemResultadoNotificacao_DestPesCod, H00QS2_A1414ContagemResultadoNotificacao_DestCod
               }
               , new Object[] {
               H00QS3_AGRID_nRecordCount
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_83 ;
      private short nGXsfl_83_idx=1 ;
      private short AV14OrderedBy ;
      private short AV17DynamicFiltersOperator1 ;
      private short AV22DynamicFiltersOperator2 ;
      private short AV27DynamicFiltersOperator3 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_83_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContagemResultadoNotificacao_DestCod_Titleformat ;
      private short edtContagemResultadoNotificacao_DestPesCod_Titleformat ;
      private short edtContagemResultadoNotificacao_DestNome_Titleformat ;
      private short edtContagemResultadoNotificacao_DestEmail_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV8InOutContagemResultadoNotificacao_DestCod ;
      private int AV9InOutContagemResultadoNotificacao_DestPesCod ;
      private int wcpOAV8InOutContagemResultadoNotificacao_DestCod ;
      private int wcpOAV9InOutContagemResultadoNotificacao_DestPesCod ;
      private int subGrid_Rows ;
      private int AV18ContagemResultadoNotificacao_DestPesCod1 ;
      private int AV23ContagemResultadoNotificacao_DestPesCod2 ;
      private int AV28ContagemResultadoNotificacao_DestPesCod3 ;
      private int Gridpaginationbar_Pagestoshow ;
      private int A1414ContagemResultadoNotificacao_DestCod ;
      private int A1426ContagemResultadoNotificacao_DestPesCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV33PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavContagemresultadonotificacao_destpescod1_Visible ;
      private int edtavContagemresultadonotificacao_destnome1_Visible ;
      private int edtavContagemresultadonotificacao_destpescod2_Visible ;
      private int edtavContagemresultadonotificacao_destnome2_Visible ;
      private int edtavContagemresultadonotificacao_destpescod3_Visible ;
      private int edtavContagemresultadonotificacao_destnome3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long AV7InContagemResultadoNotificacao_Codigo ;
      private long wcpOAV7InContagemResultadoNotificacao_Codigo ;
      private long GRID_nFirstRecordOnPage ;
      private long AV34GridCurrentPage ;
      private long AV35GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long A1412ContagemResultadoNotificacao_Codigo ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_83_idx="0001" ;
      private String AV19ContagemResultadoNotificacao_DestNome1 ;
      private String AV24ContagemResultadoNotificacao_DestNome2 ;
      private String AV29ContagemResultadoNotificacao_DestNome3 ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtContagemResultadoNotificacao_DestCod_Internalname ;
      private String edtContagemResultadoNotificacao_DestPesCod_Internalname ;
      private String A1421ContagemResultadoNotificacao_DestNome ;
      private String edtContagemResultadoNotificacao_DestNome_Internalname ;
      private String edtContagemResultadoNotificacao_DestEmail_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV19ContagemResultadoNotificacao_DestNome1 ;
      private String lV24ContagemResultadoNotificacao_DestNome2 ;
      private String lV29ContagemResultadoNotificacao_DestNome3 ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavContagemresultadonotificacao_destpescod1_Internalname ;
      private String edtavContagemresultadonotificacao_destnome1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavContagemresultadonotificacao_destpescod2_Internalname ;
      private String edtavContagemresultadonotificacao_destnome2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavContagemresultadonotificacao_destpescod3_Internalname ;
      private String edtavContagemresultadonotificacao_destnome3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContagemResultadoNotificacao_DestCod_Title ;
      private String edtContagemResultadoNotificacao_DestPesCod_Title ;
      private String edtContagemResultadoNotificacao_DestNome_Title ;
      private String edtContagemResultadoNotificacao_DestEmail_Title ;
      private String edtavSelect_Tooltiptext ;
      private String edtContagemResultadoNotificacao_DestEmail_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavContagemresultadonotificacao_destpescod3_Jsonclick ;
      private String edtavContagemresultadonotificacao_destnome3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavContagemresultadonotificacao_destpescod2_Jsonclick ;
      private String edtavContagemresultadonotificacao_destnome2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavContagemresultadonotificacao_destpescod1_Jsonclick ;
      private String edtavContagemresultadonotificacao_destnome1_Jsonclick ;
      private String sGXsfl_83_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtContagemResultadoNotificacao_DestCod_Jsonclick ;
      private String edtContagemResultadoNotificacao_DestPesCod_Jsonclick ;
      private String edtContagemResultadoNotificacao_DestNome_Jsonclick ;
      private String edtContagemResultadoNotificacao_DestEmail_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime A1416ContagemResultadoNotificacao_DataHora ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool AV20DynamicFiltersEnabled2 ;
      private bool AV25DynamicFiltersEnabled3 ;
      private bool n1419ContagemResultadoNotificacao_DestEmail ;
      private bool toggleJsOutput ;
      private bool AV31DynamicFiltersIgnoreFirst ;
      private bool AV30DynamicFiltersRemoving ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1426ContagemResultadoNotificacao_DestPesCod ;
      private bool n1421ContagemResultadoNotificacao_DestNome ;
      private bool n1416ContagemResultadoNotificacao_DataHora ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV36Select_IsBlob ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV21DynamicFiltersSelector2 ;
      private String AV26DynamicFiltersSelector3 ;
      private String A1419ContagemResultadoNotificacao_DestEmail ;
      private String AV39Select_GXI ;
      private String AV36Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP1_InOutContagemResultadoNotificacao_DestCod ;
      private int aP2_InOutContagemResultadoNotificacao_DestPesCod ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private long[] H00QS2_A1412ContagemResultadoNotificacao_Codigo ;
      private DateTime[] H00QS2_A1416ContagemResultadoNotificacao_DataHora ;
      private bool[] H00QS2_n1416ContagemResultadoNotificacao_DataHora ;
      private String[] H00QS2_A1419ContagemResultadoNotificacao_DestEmail ;
      private bool[] H00QS2_n1419ContagemResultadoNotificacao_DestEmail ;
      private String[] H00QS2_A1421ContagemResultadoNotificacao_DestNome ;
      private bool[] H00QS2_n1421ContagemResultadoNotificacao_DestNome ;
      private int[] H00QS2_A1426ContagemResultadoNotificacao_DestPesCod ;
      private bool[] H00QS2_n1426ContagemResultadoNotificacao_DestPesCod ;
      private int[] H00QS2_A1414ContagemResultadoNotificacao_DestCod ;
      private long[] H00QS3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
   }

   public class promptcontagemresultadonotificacaodestinatario__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00QS2( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             int AV18ContagemResultadoNotificacao_DestPesCod1 ,
                                             String AV19ContagemResultadoNotificacao_DestNome1 ,
                                             bool AV20DynamicFiltersEnabled2 ,
                                             String AV21DynamicFiltersSelector2 ,
                                             short AV22DynamicFiltersOperator2 ,
                                             int AV23ContagemResultadoNotificacao_DestPesCod2 ,
                                             String AV24ContagemResultadoNotificacao_DestNome2 ,
                                             bool AV25DynamicFiltersEnabled3 ,
                                             String AV26DynamicFiltersSelector3 ,
                                             short AV27DynamicFiltersOperator3 ,
                                             int AV28ContagemResultadoNotificacao_DestPesCod3 ,
                                             String AV29ContagemResultadoNotificacao_DestNome3 ,
                                             int A1426ContagemResultadoNotificacao_DestPesCod ,
                                             String A1421ContagemResultadoNotificacao_DestNome ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             long A1412ContagemResultadoNotificacao_Codigo ,
                                             long AV7InContagemResultadoNotificacao_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [21] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[ContagemResultadoNotificacao_Codigo], T2.[ContagemResultadoNotificacao_DataHora], T1.[ContagemResultadoNotificacao_DestEmail], T4.[Pessoa_Nome] AS ContagemResultadoNotificacao_DestNome, T3.[Usuario_PessoaCod] AS ContagemResultadoNotificacao_DestPesCod, T1.[ContagemResultadoNotificacao_DestCod] AS ContagemResultadoNotificacao_DestCod";
         sFromString = " FROM ((([ContagemResultadoNotificacaoDestinatario] T1 WITH (NOLOCK) INNER JOIN [ContagemResultadoNotificacao] T2 WITH (NOLOCK) ON T2.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContagemResultadoNotificacao_DestCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[ContagemResultadoNotificacao_Codigo] = @AV7InContagemResultadoNotificacao_Codigo)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! (0==AV18ContagemResultadoNotificacao_DestPesCod1) ) )
         {
            sWhereString = sWhereString + " and (T3.[Usuario_PessoaCod] < @AV18ContagemResultadoNotificacao_DestPesCod1)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! (0==AV18ContagemResultadoNotificacao_DestPesCod1) ) )
         {
            sWhereString = sWhereString + " and (T3.[Usuario_PessoaCod] = @AV18ContagemResultadoNotificacao_DestPesCod1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 ) && ( AV17DynamicFiltersOperator1 == 2 ) && ( ! (0==AV18ContagemResultadoNotificacao_DestPesCod1) ) )
         {
            sWhereString = sWhereString + " and (T3.[Usuario_PessoaCod] > @AV18ContagemResultadoNotificacao_DestPesCod1)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19ContagemResultadoNotificacao_DestNome1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV19ContagemResultadoNotificacao_DestNome1)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19ContagemResultadoNotificacao_DestNome1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV19ContagemResultadoNotificacao_DestNome1)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! (0==AV23ContagemResultadoNotificacao_DestPesCod2) ) )
         {
            sWhereString = sWhereString + " and (T3.[Usuario_PessoaCod] < @AV23ContagemResultadoNotificacao_DestPesCod2)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! (0==AV23ContagemResultadoNotificacao_DestPesCod2) ) )
         {
            sWhereString = sWhereString + " and (T3.[Usuario_PessoaCod] = @AV23ContagemResultadoNotificacao_DestPesCod2)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 ) && ( AV22DynamicFiltersOperator2 == 2 ) && ( ! (0==AV23ContagemResultadoNotificacao_DestPesCod2) ) )
         {
            sWhereString = sWhereString + " and (T3.[Usuario_PessoaCod] > @AV23ContagemResultadoNotificacao_DestPesCod2)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24ContagemResultadoNotificacao_DestNome2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV24ContagemResultadoNotificacao_DestNome2)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24ContagemResultadoNotificacao_DestNome2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV24ContagemResultadoNotificacao_DestNome2)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 ) && ( AV27DynamicFiltersOperator3 == 0 ) && ( ! (0==AV28ContagemResultadoNotificacao_DestPesCod3) ) )
         {
            sWhereString = sWhereString + " and (T3.[Usuario_PessoaCod] < @AV28ContagemResultadoNotificacao_DestPesCod3)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 ) && ( AV27DynamicFiltersOperator3 == 1 ) && ( ! (0==AV28ContagemResultadoNotificacao_DestPesCod3) ) )
         {
            sWhereString = sWhereString + " and (T3.[Usuario_PessoaCod] = @AV28ContagemResultadoNotificacao_DestPesCod3)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 ) && ( AV27DynamicFiltersOperator3 == 2 ) && ( ! (0==AV28ContagemResultadoNotificacao_DestPesCod3) ) )
         {
            sWhereString = sWhereString + " and (T3.[Usuario_PessoaCod] > @AV28ContagemResultadoNotificacao_DestPesCod3)";
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV27DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29ContagemResultadoNotificacao_DestNome3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV29ContagemResultadoNotificacao_DestNome3)";
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV27DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29ContagemResultadoNotificacao_DestNome3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV29ContagemResultadoNotificacao_DestNome3)";
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( AV14OrderedBy == 1 )
         {
            sOrderString = sOrderString + " ORDER BY T2.[ContagemResultadoNotificacao_DataHora]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_DestCod]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_DestCod] DESC";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Usuario_PessoaCod]";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Usuario_PessoaCod] DESC";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Nome]";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_DestEmail]";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_DestEmail] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_Codigo], T1.[ContagemResultadoNotificacao_DestCod]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H00QS3( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             int AV18ContagemResultadoNotificacao_DestPesCod1 ,
                                             String AV19ContagemResultadoNotificacao_DestNome1 ,
                                             bool AV20DynamicFiltersEnabled2 ,
                                             String AV21DynamicFiltersSelector2 ,
                                             short AV22DynamicFiltersOperator2 ,
                                             int AV23ContagemResultadoNotificacao_DestPesCod2 ,
                                             String AV24ContagemResultadoNotificacao_DestNome2 ,
                                             bool AV25DynamicFiltersEnabled3 ,
                                             String AV26DynamicFiltersSelector3 ,
                                             short AV27DynamicFiltersOperator3 ,
                                             int AV28ContagemResultadoNotificacao_DestPesCod3 ,
                                             String AV29ContagemResultadoNotificacao_DestNome3 ,
                                             int A1426ContagemResultadoNotificacao_DestPesCod ,
                                             String A1421ContagemResultadoNotificacao_DestNome ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             long A1412ContagemResultadoNotificacao_Codigo ,
                                             long AV7InContagemResultadoNotificacao_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [16] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((([ContagemResultadoNotificacaoDestinatario] T1 WITH (NOLOCK) INNER JOIN [ContagemResultadoNotificacao] T2 WITH (NOLOCK) ON T2.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContagemResultadoNotificacao_DestCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultadoNotificacao_Codigo] = @AV7InContagemResultadoNotificacao_Codigo)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! (0==AV18ContagemResultadoNotificacao_DestPesCod1) ) )
         {
            sWhereString = sWhereString + " and (T3.[Usuario_PessoaCod] < @AV18ContagemResultadoNotificacao_DestPesCod1)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! (0==AV18ContagemResultadoNotificacao_DestPesCod1) ) )
         {
            sWhereString = sWhereString + " and (T3.[Usuario_PessoaCod] = @AV18ContagemResultadoNotificacao_DestPesCod1)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 ) && ( AV17DynamicFiltersOperator1 == 2 ) && ( ! (0==AV18ContagemResultadoNotificacao_DestPesCod1) ) )
         {
            sWhereString = sWhereString + " and (T3.[Usuario_PessoaCod] > @AV18ContagemResultadoNotificacao_DestPesCod1)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19ContagemResultadoNotificacao_DestNome1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV19ContagemResultadoNotificacao_DestNome1)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19ContagemResultadoNotificacao_DestNome1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV19ContagemResultadoNotificacao_DestNome1)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! (0==AV23ContagemResultadoNotificacao_DestPesCod2) ) )
         {
            sWhereString = sWhereString + " and (T3.[Usuario_PessoaCod] < @AV23ContagemResultadoNotificacao_DestPesCod2)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! (0==AV23ContagemResultadoNotificacao_DestPesCod2) ) )
         {
            sWhereString = sWhereString + " and (T3.[Usuario_PessoaCod] = @AV23ContagemResultadoNotificacao_DestPesCod2)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 ) && ( AV22DynamicFiltersOperator2 == 2 ) && ( ! (0==AV23ContagemResultadoNotificacao_DestPesCod2) ) )
         {
            sWhereString = sWhereString + " and (T3.[Usuario_PessoaCod] > @AV23ContagemResultadoNotificacao_DestPesCod2)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24ContagemResultadoNotificacao_DestNome2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV24ContagemResultadoNotificacao_DestNome2)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24ContagemResultadoNotificacao_DestNome2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV24ContagemResultadoNotificacao_DestNome2)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 ) && ( AV27DynamicFiltersOperator3 == 0 ) && ( ! (0==AV28ContagemResultadoNotificacao_DestPesCod3) ) )
         {
            sWhereString = sWhereString + " and (T3.[Usuario_PessoaCod] < @AV28ContagemResultadoNotificacao_DestPesCod3)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 ) && ( AV27DynamicFiltersOperator3 == 1 ) && ( ! (0==AV28ContagemResultadoNotificacao_DestPesCod3) ) )
         {
            sWhereString = sWhereString + " and (T3.[Usuario_PessoaCod] = @AV28ContagemResultadoNotificacao_DestPesCod3)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD") == 0 ) && ( AV27DynamicFiltersOperator3 == 2 ) && ( ! (0==AV28ContagemResultadoNotificacao_DestPesCod3) ) )
         {
            sWhereString = sWhereString + " and (T3.[Usuario_PessoaCod] > @AV28ContagemResultadoNotificacao_DestPesCod3)";
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV27DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29ContagemResultadoNotificacao_DestNome3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV29ContagemResultadoNotificacao_DestNome3)";
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV27DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29ContagemResultadoNotificacao_DestNome3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV29ContagemResultadoNotificacao_DestNome3)";
         }
         else
         {
            GXv_int3[15] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV14OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00QS2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (int)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (short)dynConstraints[16] , (bool)dynConstraints[17] , (long)dynConstraints[18] , (long)dynConstraints[19] );
               case 1 :
                     return conditional_H00QS3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (int)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (short)dynConstraints[16] , (bool)dynConstraints[17] , (long)dynConstraints[18] , (long)dynConstraints[19] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00QS2 ;
          prmH00QS2 = new Object[] {
          new Object[] {"@AV7InContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV18ContagemResultadoNotificacao_DestPesCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18ContagemResultadoNotificacao_DestPesCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18ContagemResultadoNotificacao_DestPesCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV19ContagemResultadoNotificacao_DestNome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV19ContagemResultadoNotificacao_DestNome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV23ContagemResultadoNotificacao_DestPesCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV23ContagemResultadoNotificacao_DestPesCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV23ContagemResultadoNotificacao_DestPesCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV24ContagemResultadoNotificacao_DestNome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV24ContagemResultadoNotificacao_DestNome2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV28ContagemResultadoNotificacao_DestPesCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV28ContagemResultadoNotificacao_DestPesCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV28ContagemResultadoNotificacao_DestPesCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV29ContagemResultadoNotificacao_DestNome3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV29ContagemResultadoNotificacao_DestNome3",SqlDbType.Char,100,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00QS3 ;
          prmH00QS3 = new Object[] {
          new Object[] {"@AV7InContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV18ContagemResultadoNotificacao_DestPesCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18ContagemResultadoNotificacao_DestPesCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18ContagemResultadoNotificacao_DestPesCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV19ContagemResultadoNotificacao_DestNome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV19ContagemResultadoNotificacao_DestNome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV23ContagemResultadoNotificacao_DestPesCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV23ContagemResultadoNotificacao_DestPesCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV23ContagemResultadoNotificacao_DestPesCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV24ContagemResultadoNotificacao_DestNome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV24ContagemResultadoNotificacao_DestNome2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV28ContagemResultadoNotificacao_DestPesCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV28ContagemResultadoNotificacao_DestPesCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV28ContagemResultadoNotificacao_DestPesCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV29ContagemResultadoNotificacao_DestNome3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV29ContagemResultadoNotificacao_DestNome3",SqlDbType.Char,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00QS2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QS2,11,0,true,false )
             ,new CursorDef("H00QS3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QS3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                return;
       }
    }

 }

}
