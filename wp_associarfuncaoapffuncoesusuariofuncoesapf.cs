/*
               File: WP_AssociarFuncaoAPFFuncoesUsuarioFuncoesAPF
        Description: Fun��es de Usu�rio
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/4/2020 8:43:25.17
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_associarfuncaoapffuncoesusuariofuncoesapf : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public wp_associarfuncaoapffuncoesusuariofuncoesapf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_associarfuncaoapffuncoesusuariofuncoesapf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_FuncaoAPF_Codigo ,
                           int aP1_Sistema_Codigo )
      {
         this.AV7FuncaoAPF_Codigo = aP0_FuncaoAPF_Codigo;
         this.A127Sistema_Codigo = aP1_Sistema_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         lstavNotassociatedrecords = new GXListbox();
         lstavAssociatedrecords = new GXListbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7FuncaoAPF_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoAPF_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFUNCAOAPF_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7FuncaoAPF_Codigo), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  A127Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9")));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA9J2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WS9J2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE9J2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( "Fun��es de Usu�rio") ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020548432527");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_associarfuncaoapffuncoesusuariofuncoesapf.aspx") + "?" + UrlEncode("" +AV7FuncaoAPF_Codigo) + "," + UrlEncode("" +A127Sistema_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vADDEDKEYLIST", AV20AddedKeyList);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vADDEDKEYLIST", AV20AddedKeyList);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vADDEDDSCLIST", AV22AddedDscList);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vADDEDDSCLIST", AV22AddedDscList);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNOTADDEDKEYLIST", AV21NotAddedKeyList);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNOTADDEDKEYLIST", AV21NotAddedKeyList);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNOTADDEDDSCLIST", AV23NotAddedDscList);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNOTADDEDDSCLIST", AV23NotAddedDscList);
         }
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vFUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOUSUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A161FuncaoUsuario_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCOESUSUARIOFUNCOESAPF", AV11FuncoesUsuarioFuncoesAPF);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCOESUSUARIOFUNCOESAPF", AV11FuncoesUsuarioFuncoesAPF);
         }
         GxWebStd.gx_hidden_field( context, "vFUNCAOUSUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8FuncaoUsuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A166FuncaoAPF_Nome, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A166FuncaoAPF_Nome, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vFUNCAOAPF_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7FuncaoAPF_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_SISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vFUNCAOAPF_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7FuncaoAPF_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_SISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm9J2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "WP_AssociarFuncaoAPFFuncoesUsuarioFuncoesAPF" ;
      }

      public override String GetPgmdesc( )
      {
         return "Fun��es de Usu�rio" ;
      }

      protected void WB9J0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            wb_table1_2_9J2( true) ;
         }
         else
         {
            wb_table1_2_9J2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_9J2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavAddedkeylistxml_Internalname, AV16AddedKeyListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", 0, edtavAddedkeylistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociarFuncaoAPFFuncoesUsuarioFuncoesAPF.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavNotaddedkeylistxml_Internalname, AV17NotAddedKeyListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", 0, edtavNotaddedkeylistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociarFuncaoAPFFuncoesUsuarioFuncoesAPF.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavAddeddsclistxml_Internalname, AV18AddedDscListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", 0, edtavAddeddsclistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociarFuncaoAPFFuncoesUsuarioFuncoesAPF.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavNotaddeddsclistxml_Internalname, AV19NotAddedDscListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", 0, edtavNotaddeddsclistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociarFuncaoAPFFuncoesUsuarioFuncoesAPF.htm");
         }
         wbLoad = true;
      }

      protected void START9J2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Fun��es de Usu�rio", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP9J0( ) ;
      }

      protected void WS9J2( )
      {
         START9J2( ) ;
         EVT9J2( ) ;
      }

      protected void EVT9J2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E119J2 */
                           E119J2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E129J2 */
                           E129J2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                                 /* Execute user event: E139J2 */
                                 E139J2 ();
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E149J2 */
                           E149J2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E159J2 */
                           E159J2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E169J2 */
                           E169J2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E179J2 */
                           E179J2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E189J2 */
                           E189J2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VNOTASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E199J2 */
                           E199J2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E209J2 */
                           E209J2 ();
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E189J2 */
                           E189J2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VNOTASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E199J2 */
                           E199J2 ();
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE9J2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm9J2( ) ;
            }
         }
      }

      protected void PA9J2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            lstavNotassociatedrecords.Name = "vNOTASSOCIATEDRECORDS";
            lstavNotassociatedrecords.WebTags = "";
            if ( lstavNotassociatedrecords.ItemCount > 0 )
            {
               AV24NotAssociatedRecords = (int)(NumberUtil.Val( lstavNotassociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24NotAssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0)));
            }
            lstavAssociatedrecords.Name = "vASSOCIATEDRECORDS";
            lstavAssociatedrecords.WebTags = "";
            if ( lstavAssociatedrecords.ItemCount > 0 )
            {
               AV25AssociatedRecords = (int)(NumberUtil.Val( lstavAssociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = lstavNotassociatedrecords_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( lstavNotassociatedrecords.ItemCount > 0 )
         {
            AV24NotAssociatedRecords = (int)(NumberUtil.Val( lstavNotassociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24NotAssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0)));
         }
         if ( lstavAssociatedrecords.ItemCount > 0 )
         {
            AV25AssociatedRecords = (int)(NumberUtil.Val( lstavAssociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF9J2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF9J2( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E129J2 */
         E129J2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H009J2 */
            pr_default.execute(0, new Object[] {AV7FuncaoAPF_Codigo, A127Sistema_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A360FuncaoAPF_SistemaCod = H009J2_A360FuncaoAPF_SistemaCod[0];
               n360FuncaoAPF_SistemaCod = H009J2_n360FuncaoAPF_SistemaCod[0];
               A165FuncaoAPF_Codigo = H009J2_A165FuncaoAPF_Codigo[0];
               A166FuncaoAPF_Nome = H009J2_A166FuncaoAPF_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAOAPF_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A166FuncaoAPF_Nome, ""))));
               /* Execute user event: E209J2 */
               E209J2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WB9J0( ) ;
         }
      }

      protected void STRUP9J0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E119J2 */
         E119J2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A166FuncaoAPF_Nome = cgiGet( edtFuncaoAPF_Nome_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAOAPF_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A166FuncaoAPF_Nome, ""))));
            lstavNotassociatedrecords.CurrentValue = cgiGet( lstavNotassociatedrecords_Internalname);
            AV24NotAssociatedRecords = (int)(NumberUtil.Val( cgiGet( lstavNotassociatedrecords_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24NotAssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0)));
            lstavAssociatedrecords.CurrentValue = cgiGet( lstavAssociatedrecords_Internalname);
            AV25AssociatedRecords = (int)(NumberUtil.Val( cgiGet( lstavAssociatedrecords_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0)));
            AV16AddedKeyListXml = cgiGet( edtavAddedkeylistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16AddedKeyListXml", AV16AddedKeyListXml);
            AV17NotAddedKeyListXml = cgiGet( edtavNotaddedkeylistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17NotAddedKeyListXml", AV17NotAddedKeyListXml);
            AV18AddedDscListXml = cgiGet( edtavAddeddsclistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AddedDscListXml", AV18AddedDscListXml);
            AV19NotAddedDscListXml = cgiGet( edtavNotaddeddsclistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19NotAddedDscListXml", AV19NotAddedDscListXml);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E119J2 */
         E119J2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E119J2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         if ( StringUtil.StrCmp(AV9HTTPRequest.Method, "GET") == 0 )
         {
            AV29GXLvl8 = 0;
            /* Using cursor H009J3 */
            pr_default.execute(1, new Object[] {AV7FuncaoAPF_Codigo, A127Sistema_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A360FuncaoAPF_SistemaCod = H009J3_A360FuncaoAPF_SistemaCod[0];
               n360FuncaoAPF_SistemaCod = H009J3_n360FuncaoAPF_SistemaCod[0];
               A165FuncaoAPF_Codigo = H009J3_A165FuncaoAPF_Codigo[0];
               AV29GXLvl8 = 1;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            if ( AV29GXLvl8 == 0 )
            {
               GX_msglist.addItem("Registro n�o encontrado.");
            }
            /* Using cursor H009J4 */
            pr_default.execute(2, new Object[] {A127Sistema_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A161FuncaoUsuario_Codigo = H009J4_A161FuncaoUsuario_Codigo[0];
               A162FuncaoUsuario_Nome = H009J4_A162FuncaoUsuario_Nome[0];
               AV8FuncaoUsuario_Codigo = A161FuncaoUsuario_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8FuncaoUsuario_Codigo), 6, 0)));
               /* Execute user subroutine: 'EXIST' */
               S114 ();
               if ( returnInSub )
               {
                  pr_default.close(2);
                  returnInSub = true;
                  if (true) return;
               }
               AV13Description = A162FuncaoUsuario_Nome;
               if ( AV10Exist )
               {
                  AV20AddedKeyList.Add(A161FuncaoUsuario_Codigo, 0);
                  AV22AddedDscList.Add(AV13Description, 0);
               }
               else
               {
                  AV21NotAddedKeyList.Add(A161FuncaoUsuario_Codigo, 0);
                  AV23NotAddedDscList.Add(AV13Description, 0);
               }
               pr_default.readNext(2);
            }
            pr_default.close(2);
            /* Execute user subroutine: 'SAVELISTS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavAddedkeylistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAddedkeylistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAddedkeylistxml_Visible), 5, 0)));
         edtavNotaddedkeylistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNotaddedkeylistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotaddedkeylistxml_Visible), 5, 0)));
         edtavAddeddsclistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAddeddsclistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAddeddsclistxml_Visible), 5, 0)));
         edtavNotaddeddsclistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNotaddeddsclistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotaddeddsclistxml_Visible), 5, 0)));
      }

      protected void E129J2( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         imgImageassociateselected_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateselected_Visible), 5, 0)));
         imgImageassociateall_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateall_Visible), 5, 0)));
         imgImagedisassociateselected_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateselected_Visible), 5, 0)));
         imgImagedisassociateall_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateall_Visible), 5, 0)));
         bttBtn_confirm_Visible = (AV6WWPContext.gxTpr_Insert||AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_confirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_confirm_Visible), 5, 0)));
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22AddedDscList", AV22AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20AddedKeyList", AV20AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV21NotAddedKeyList", AV21NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV23NotAddedDscList", AV23NotAddedDscList);
      }

      public void GXEnter( )
      {
         /* Execute user event: E139J2 */
         E139J2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E139J2( )
      {
         /* Enter Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV12Success = true;
         AV31GXV1 = 1;
         while ( AV31GXV1 <= AV20AddedKeyList.Count )
         {
            AV8FuncaoUsuario_Codigo = (int)(AV20AddedKeyList.GetNumeric(AV31GXV1));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8FuncaoUsuario_Codigo), 6, 0)));
            if ( AV12Success )
            {
               AV10Exist = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Exist", AV10Exist);
               /* Using cursor H009J5 */
               pr_default.execute(3, new Object[] {AV8FuncaoUsuario_Codigo, A127Sistema_Codigo, A165FuncaoAPF_Codigo, AV7FuncaoAPF_Codigo});
               while ( (pr_default.getStatus(3) != 101) )
               {
                  A161FuncaoUsuario_Codigo = H009J5_A161FuncaoUsuario_Codigo[0];
                  AV10Exist = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Exist", AV10Exist);
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(3);
               if ( ! AV10Exist )
               {
                  AV11FuncoesUsuarioFuncoesAPF = new SdtFuncoesUsuarioFuncoesAPF(context);
                  AV11FuncoesUsuarioFuncoesAPF.gxTpr_Funcaoapf_codigo = AV7FuncaoAPF_Codigo;
                  AV11FuncoesUsuarioFuncoesAPF.gxTpr_Funcaousuario_codigo = AV8FuncaoUsuario_Codigo;
                  AV11FuncoesUsuarioFuncoesAPF.Save();
                  if ( ! AV11FuncoesUsuarioFuncoesAPF.Success() )
                  {
                     AV12Success = false;
                  }
               }
            }
            AV31GXV1 = (int)(AV31GXV1+1);
         }
         AV33GXV2 = 1;
         while ( AV33GXV2 <= AV21NotAddedKeyList.Count )
         {
            AV8FuncaoUsuario_Codigo = (int)(AV21NotAddedKeyList.GetNumeric(AV33GXV2));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8FuncaoUsuario_Codigo), 6, 0)));
            if ( AV12Success )
            {
               AV11FuncoesUsuarioFuncoesAPF.Load(AV8FuncaoUsuario_Codigo, AV7FuncaoAPF_Codigo);
               if ( AV11FuncoesUsuarioFuncoesAPF.Success() )
               {
                  AV11FuncoesUsuarioFuncoesAPF.Delete();
                  AV12Success = AV11FuncoesUsuarioFuncoesAPF.Success();
               }
            }
            AV33GXV2 = (int)(AV33GXV2+1);
         }
         if ( AV12Success )
         {
            context.CommitDataStores( "WP_AssociarFuncaoAPFFuncoesUsuarioFuncoesAPF");
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         else
         {
            context.RollbackDataStores( "WP_AssociarFuncaoAPFFuncoesUsuarioFuncoesAPF");
            /* Execute user subroutine: 'SHOW ERROR MESSAGES' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11FuncoesUsuarioFuncoesAPF", AV11FuncoesUsuarioFuncoesAPF);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22AddedDscList", AV22AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20AddedKeyList", AV20AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV21NotAddedKeyList", AV21NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV23NotAddedDscList", AV23NotAddedDscList);
      }

      protected void E149J2( )
      {
         /* 'Disassociate Selected' Routine */
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV21NotAddedKeyList", AV21NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV23NotAddedDscList", AV23NotAddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20AddedKeyList", AV20AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22AddedDscList", AV22AddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E159J2( )
      {
         /* 'Associate selected' Routine */
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20AddedKeyList", AV20AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22AddedDscList", AV22AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV21NotAddedKeyList", AV21NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV23NotAddedDscList", AV23NotAddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E169J2( )
      {
         /* 'Associate All' Routine */
         /* Execute user subroutine: 'ASSOCIATEALL' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20AddedKeyList", AV20AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22AddedDscList", AV22AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV21NotAddedKeyList", AV21NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV23NotAddedDscList", AV23NotAddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E179J2( )
      {
         /* 'Disassociate All' Routine */
         /* Execute user subroutine: 'ASSOCIATEALL' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21NotAddedKeyList = (IGxCollection)(AV20AddedKeyList.Clone());
         AV23NotAddedDscList = (IGxCollection)(AV22AddedDscList.Clone());
         AV22AddedDscList.Clear();
         AV20AddedKeyList.Clear();
         /* Execute user subroutine: 'SAVELISTS' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV21NotAddedKeyList", AV21NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV23NotAddedDscList", AV23NotAddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22AddedDscList", AV22AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20AddedKeyList", AV20AddedKeyList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E189J2( )
      {
         /* Associatedrecords_Dblclick Routine */
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV21NotAddedKeyList", AV21NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV23NotAddedDscList", AV23NotAddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20AddedKeyList", AV20AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22AddedDscList", AV22AddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E199J2( )
      {
         /* Notassociatedrecords_Dblclick Routine */
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20AddedKeyList", AV20AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22AddedDscList", AV22AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV21NotAddedKeyList", AV21NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV23NotAddedDscList", AV23NotAddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void S132( )
      {
         /* 'UPDATEASSOCIATIONVARIABLES' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         lstavAssociatedrecords.removeAllItems();
         lstavNotassociatedrecords.removeAllItems();
         AV14i = 1;
         AV34GXV3 = 1;
         while ( AV34GXV3 <= AV20AddedKeyList.Count )
         {
            AV8FuncaoUsuario_Codigo = (int)(AV20AddedKeyList.GetNumeric(AV34GXV3));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8FuncaoUsuario_Codigo), 6, 0)));
            AV13Description = ((String)AV22AddedDscList.Item(AV14i));
            lstavAssociatedrecords.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV8FuncaoUsuario_Codigo), 6, 0)), StringUtil.Trim( AV13Description), 0);
            AV14i = (int)(AV14i+1);
            AV34GXV3 = (int)(AV34GXV3+1);
         }
         AV14i = 1;
         AV35GXV4 = 1;
         while ( AV35GXV4 <= AV21NotAddedKeyList.Count )
         {
            AV8FuncaoUsuario_Codigo = (int)(AV21NotAddedKeyList.GetNumeric(AV35GXV4));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8FuncaoUsuario_Codigo), 6, 0)));
            AV13Description = ((String)AV23NotAddedDscList.Item(AV14i));
            lstavNotassociatedrecords.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV8FuncaoUsuario_Codigo), 6, 0)), StringUtil.Trim( AV13Description), 0);
            AV14i = (int)(AV14i+1);
            AV35GXV4 = (int)(AV35GXV4+1);
         }
      }

      protected void S152( )
      {
         /* 'SHOW ERROR MESSAGES' Routine */
         AV37GXV6 = 1;
         AV36GXV5 = AV11FuncoesUsuarioFuncoesAPF.GetMessages();
         while ( AV37GXV6 <= AV36GXV5.Count )
         {
            AV15Message = ((SdtMessages_Message)AV36GXV5.Item(AV37GXV6));
            if ( AV15Message.gxTpr_Type == 1 )
            {
               GX_msglist.addItem(AV15Message.gxTpr_Description);
            }
            AV37GXV6 = (int)(AV37GXV6+1);
         }
      }

      protected void S142( )
      {
         /* 'LOADLISTS' Routine */
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16AddedKeyListXml)) )
         {
            AV22AddedDscList.FromXml(AV18AddedDscListXml, "Collection");
            AV20AddedKeyList.FromXml(AV16AddedKeyListXml, "Collection");
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17NotAddedKeyListXml)) )
         {
            AV21NotAddedKeyList.FromXml(AV17NotAddedKeyListXml, "Collection");
            AV23NotAddedDscList.FromXml(AV19NotAddedDscListXml, "Collection");
         }
      }

      protected void S122( )
      {
         /* 'SAVELISTS' Routine */
         if ( AV20AddedKeyList.Count > 0 )
         {
            AV16AddedKeyListXml = AV20AddedKeyList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16AddedKeyListXml", AV16AddedKeyListXml);
            AV18AddedDscListXml = AV22AddedDscList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AddedDscListXml", AV18AddedDscListXml);
         }
         else
         {
            AV16AddedKeyListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16AddedKeyListXml", AV16AddedKeyListXml);
            AV18AddedDscListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AddedDscListXml", AV18AddedDscListXml);
         }
         if ( AV21NotAddedKeyList.Count > 0 )
         {
            AV17NotAddedKeyListXml = AV21NotAddedKeyList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17NotAddedKeyListXml", AV17NotAddedKeyListXml);
            AV19NotAddedDscListXml = AV23NotAddedDscList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19NotAddedDscListXml", AV19NotAddedDscListXml);
         }
         else
         {
            AV17NotAddedKeyListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17NotAddedKeyListXml", AV17NotAddedKeyListXml);
            AV19NotAddedDscListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19NotAddedDscListXml", AV19NotAddedDscListXml);
         }
      }

      protected void S182( )
      {
         /* 'ASSOCIATEALL' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV14i = 1;
         AV26InsertIndex = 1;
         AV38GXV7 = 1;
         while ( AV38GXV7 <= AV21NotAddedKeyList.Count )
         {
            AV8FuncaoUsuario_Codigo = (int)(AV21NotAddedKeyList.GetNumeric(AV38GXV7));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8FuncaoUsuario_Codigo), 6, 0)));
            AV13Description = ((String)AV23NotAddedDscList.Item(AV14i));
            while ( ( AV26InsertIndex <= AV22AddedDscList.Count ) && ( StringUtil.StrCmp(((String)AV22AddedDscList.Item(AV26InsertIndex)), AV13Description) < 0 ) )
            {
               AV26InsertIndex = (int)(AV26InsertIndex+1);
            }
            AV20AddedKeyList.Add(AV8FuncaoUsuario_Codigo, AV26InsertIndex);
            AV22AddedDscList.Add(AV13Description, AV26InsertIndex);
            AV14i = (int)(AV14i+1);
            AV38GXV7 = (int)(AV38GXV7+1);
         }
         AV21NotAddedKeyList.Clear();
         AV23NotAddedDscList.Clear();
         /* Execute user subroutine: 'SAVELISTS' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S172( )
      {
         /* 'ASSOCIATESELECTED' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV14i = 1;
         AV39GXV8 = 1;
         while ( AV39GXV8 <= AV21NotAddedKeyList.Count )
         {
            AV8FuncaoUsuario_Codigo = (int)(AV21NotAddedKeyList.GetNumeric(AV39GXV8));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8FuncaoUsuario_Codigo), 6, 0)));
            if ( AV8FuncaoUsuario_Codigo == AV24NotAssociatedRecords )
            {
               if (true) break;
            }
            AV14i = (int)(AV14i+1);
            AV39GXV8 = (int)(AV39GXV8+1);
         }
         if ( AV14i <= AV21NotAddedKeyList.Count )
         {
            AV13Description = ((String)AV23NotAddedDscList.Item(AV14i));
            AV26InsertIndex = 1;
            while ( ( AV26InsertIndex <= AV22AddedDscList.Count ) && ( StringUtil.StrCmp(((String)AV22AddedDscList.Item(AV26InsertIndex)), AV13Description) < 0 ) )
            {
               AV26InsertIndex = (int)(AV26InsertIndex+1);
            }
            AV20AddedKeyList.Add(AV24NotAssociatedRecords, AV26InsertIndex);
            AV22AddedDscList.Add(AV13Description, AV26InsertIndex);
            AV21NotAddedKeyList.RemoveItem(AV14i);
            AV23NotAddedDscList.RemoveItem(AV14i);
            /* Execute user subroutine: 'SAVELISTS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S162( )
      {
         /* 'DISASSOCIATESELECTED' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV14i = 1;
         AV40GXV9 = 1;
         while ( AV40GXV9 <= AV20AddedKeyList.Count )
         {
            AV8FuncaoUsuario_Codigo = (int)(AV20AddedKeyList.GetNumeric(AV40GXV9));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8FuncaoUsuario_Codigo), 6, 0)));
            if ( AV8FuncaoUsuario_Codigo == AV25AssociatedRecords )
            {
               if (true) break;
            }
            AV14i = (int)(AV14i+1);
            AV40GXV9 = (int)(AV40GXV9+1);
         }
         if ( AV14i <= AV20AddedKeyList.Count )
         {
            AV13Description = ((String)AV22AddedDscList.Item(AV14i));
            AV26InsertIndex = 1;
            while ( ( AV26InsertIndex <= AV23NotAddedDscList.Count ) && ( StringUtil.StrCmp(((String)AV23NotAddedDscList.Item(AV26InsertIndex)), AV13Description) < 0 ) )
            {
               AV26InsertIndex = (int)(AV26InsertIndex+1);
            }
            AV21NotAddedKeyList.Add(AV25AssociatedRecords, AV26InsertIndex);
            AV23NotAddedDscList.Add(AV13Description, AV26InsertIndex);
            AV20AddedKeyList.RemoveItem(AV14i);
            AV22AddedDscList.RemoveItem(AV14i);
            /* Execute user subroutine: 'SAVELISTS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S114( )
      {
         /* 'EXIST' Routine */
         AV41GXLvl334 = 0;
         /* Using cursor H009J6 */
         pr_default.execute(4, new Object[] {AV8FuncaoUsuario_Codigo, AV7FuncaoAPF_Codigo});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A165FuncaoAPF_Codigo = H009J6_A165FuncaoAPF_Codigo[0];
            A161FuncaoUsuario_Codigo = H009J6_A161FuncaoUsuario_Codigo[0];
            AV41GXLvl334 = 1;
            AV10Exist = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Exist", AV10Exist);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(4);
         if ( AV41GXLvl334 == 0 )
         {
            AV10Exist = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Exist", AV10Exist);
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E209J2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_9J2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TextBlockTitleCell'>") ;
            wb_table2_8_9J2( true) ;
         }
         else
         {
            wb_table2_8_9J2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_9J2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_16_9J2( true) ;
         }
         else
         {
            wb_table3_16_9J2( false) ;
         }
         return  ;
      }

      protected void wb_table3_16_9J2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_48_9J2( true) ;
         }
         else
         {
            wb_table4_48_9J2( false) ;
         }
         return  ;
      }

      protected void wb_table4_48_9J2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_9J2e( true) ;
         }
         else
         {
            wb_table1_2_9J2e( false) ;
         }
      }

      protected void wb_table4_48_9J2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_confirm_Internalname, "", "Confirmar", bttBtn_confirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_confirm_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociarFuncaoAPFFuncoesUsuarioFuncoesAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociarFuncaoAPFFuncoesUsuarioFuncoesAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_48_9J2e( true) ;
         }
         else
         {
            wb_table4_48_9J2e( false) ;
         }
      }

      protected void wb_table3_16_9J2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefullcontent_Internalname, tblTablefullcontent_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableAttributesCell'>") ;
            wb_table5_19_9J2( true) ;
         }
         else
         {
            wb_table5_19_9J2( false) ;
         }
         return  ;
      }

      protected void wb_table5_19_9J2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_16_9J2e( true) ;
         }
         else
         {
            wb_table3_16_9J2e( false) ;
         }
      }

      protected void wb_table5_19_9J2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableAssociation", 0, "", "", 4, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblNotassociatedrecordstitle_Internalname, "Fun.de Usu�rio N�o Associadas", "", "", lblNotassociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarFuncaoAPFFuncoesUsuarioFuncoesAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblCenterrecordstitle_Internalname, " ", "", "", lblCenterrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarFuncaoAPFFuncoesUsuarioFuncoesAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociatedrecordstitle_Internalname, "Fun.de Usu�rio Associadas", "", "", lblAssociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarFuncaoAPFFuncoesUsuarioFuncoesAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            /* ListBox */
            GxWebStd.gx_listbox_ctrl1( context, lstavNotassociatedrecords, lstavNotassociatedrecords_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0)), 2, lstavNotassociatedrecords_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 6, "em", 0, "row", "", "AssociationListAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,29);\"", "", true, "HLP_WP_AssociarFuncaoAPFFuncoesUsuarioFuncoesAPF.htm");
            lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", (String)(lstavNotassociatedrecords.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table6_31_9J2( true) ;
         }
         else
         {
            wb_table6_31_9J2( false) ;
         }
         return  ;
      }

      protected void wb_table6_31_9J2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            /* ListBox */
            GxWebStd.gx_listbox_ctrl1( context, lstavAssociatedrecords, lstavAssociatedrecords_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0)), 2, lstavAssociatedrecords_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 6, "em", 0, "row", "", "AssociationListAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", "", true, "HLP_WP_AssociarFuncaoAPFFuncoesUsuarioFuncoesAPF.htm");
            lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", (String)(lstavAssociatedrecords.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_19_9J2e( true) ;
         }
         else
         {
            wb_table5_19_9J2e( false) ;
         }
      }

      protected void wb_table6_31_9J2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateall_Internalname, context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarFuncaoAPFFuncoesUsuarioFuncoesAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateselected_Internalname, context.GetImagePath( "56a5f17b-0bc3-48b5-b303-afa6e0585b6d", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarFuncaoAPFFuncoesUsuarioFuncoesAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateselected_Internalname, context.GetImagePath( "a3800d0c-bf04-4575-bc01-11fe5d7b3525", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarFuncaoAPFFuncoesUsuarioFuncoesAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateall_Internalname, context.GetImagePath( "c619e28f-4b32-4ff9-baaf-b3063fe4f782", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarFuncaoAPFFuncoesUsuarioFuncoesAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_31_9J2e( true) ;
         }
         else
         {
            wb_table6_31_9J2e( false) ;
         }
      }

      protected void wb_table2_8_9J2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedassociationtitle_Internalname, tblTablemergedassociationtitle_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociationtitle_Internalname, "Associar � Fun��es APF - An�lise de Ponto de Fun��o :: ", "", "", lblAssociationtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_AssociarFuncaoAPFFuncoesUsuarioFuncoesAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            ClassString = "AttributeTitleWWP";
            StyleString = "";
            ClassString = "AttributeTitleWWP";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtFuncaoAPF_Nome_Internalname, A166FuncaoAPF_Nome, "", "", 0, 1, 0, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WP_AssociarFuncaoAPFFuncoesUsuarioFuncoesAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_9J2e( true) ;
         }
         else
         {
            wb_table2_8_9J2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7FuncaoAPF_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoAPF_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFUNCAOAPF_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7FuncaoAPF_Codigo), "ZZZZZ9")));
         A127Sistema_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA9J2( ) ;
         WS9J2( ) ;
         WE9J2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020548432596");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_associarfuncaoapffuncoesusuariofuncoesapf.js", "?2020548432596");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblAssociationtitle_Internalname = "ASSOCIATIONTITLE";
         edtFuncaoAPF_Nome_Internalname = "FUNCAOAPF_NOME";
         tblTablemergedassociationtitle_Internalname = "TABLEMERGEDASSOCIATIONTITLE";
         lblNotassociatedrecordstitle_Internalname = "NOTASSOCIATEDRECORDSTITLE";
         lblCenterrecordstitle_Internalname = "CENTERRECORDSTITLE";
         lblAssociatedrecordstitle_Internalname = "ASSOCIATEDRECORDSTITLE";
         lstavNotassociatedrecords_Internalname = "vNOTASSOCIATEDRECORDS";
         imgImageassociateall_Internalname = "IMAGEASSOCIATEALL";
         imgImageassociateselected_Internalname = "IMAGEASSOCIATESELECTED";
         imgImagedisassociateselected_Internalname = "IMAGEDISASSOCIATESELECTED";
         imgImagedisassociateall_Internalname = "IMAGEDISASSOCIATEALL";
         tblTable1_Internalname = "TABLE1";
         lstavAssociatedrecords_Internalname = "vASSOCIATEDRECORDS";
         tblTablecontent_Internalname = "TABLECONTENT";
         tblTablefullcontent_Internalname = "TABLEFULLCONTENT";
         bttBtn_confirm_Internalname = "BTN_CONFIRM";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtavAddedkeylistxml_Internalname = "vADDEDKEYLISTXML";
         edtavNotaddedkeylistxml_Internalname = "vNOTADDEDKEYLISTXML";
         edtavAddeddsclistxml_Internalname = "vADDEDDSCLISTXML";
         edtavNotaddeddsclistxml_Internalname = "vNOTADDEDDSCLISTXML";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         imgImagedisassociateall_Visible = 1;
         imgImagedisassociateselected_Visible = 1;
         imgImageassociateselected_Visible = 1;
         imgImageassociateall_Visible = 1;
         lstavAssociatedrecords_Jsonclick = "";
         lstavNotassociatedrecords_Jsonclick = "";
         bttBtn_confirm_Visible = 1;
         edtavNotaddeddsclistxml_Visible = 1;
         edtavAddeddsclistxml_Visible = 1;
         edtavNotaddedkeylistxml_Visible = 1;
         edtavAddedkeylistxml_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'AV20AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV22AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV16AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV18AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV17NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV19NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'imgImageassociateselected_Visible',ctrl:'IMAGEASSOCIATESELECTED',prop:'Visible'},{av:'imgImageassociateall_Visible',ctrl:'IMAGEASSOCIATEALL',prop:'Visible'},{av:'imgImagedisassociateselected_Visible',ctrl:'IMAGEDISASSOCIATESELECTED',prop:'Visible'},{av:'imgImagedisassociateall_Visible',ctrl:'IMAGEDISASSOCIATEALL',prop:'Visible'},{ctrl:'BTN_CONFIRM',prop:'Visible'},{av:'AV25AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV24NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV8FuncaoUsuario_Codigo',fld:'vFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV22AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV20AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null}]}");
         setEventMetadata("ENTER","{handler:'E139J2',iparms:[{av:'AV20AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A161FuncaoUsuario_Codigo',fld:'FUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV16AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV18AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV17NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV19NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV11FuncoesUsuarioFuncoesAPF',fld:'vFUNCOESUSUARIOFUNCOESAPF',pic:'',nv:null},{av:'AV8FuncaoUsuario_Codigo',fld:'vFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV8FuncaoUsuario_Codigo',fld:'vFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV10Exist',fld:'vEXIST',pic:'',nv:false},{av:'AV11FuncoesUsuarioFuncoesAPF',fld:'vFUNCOESUSUARIOFUNCOESAPF',pic:'',nv:null},{av:'AV22AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV20AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null}]}");
         setEventMetadata("'DISASSOCIATE SELECTED'","{handler:'E149J2',iparms:[{av:'AV20AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV25AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV22AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV16AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV18AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV17NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV19NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV8FuncaoUsuario_Codigo',fld:'vFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV20AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV22AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV16AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV18AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV17NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV19NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV25AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV24NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'ASSOCIATE SELECTED'","{handler:'E159J2',iparms:[{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV24NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV22AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV20AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV16AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV18AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV17NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV19NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV8FuncaoUsuario_Codigo',fld:'vFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV22AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV16AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV18AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV17NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV19NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV25AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV24NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'ASSOCIATE ALL'","{handler:'E169J2',iparms:[{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV22AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV20AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV16AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV18AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV17NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV19NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV8FuncaoUsuario_Codigo',fld:'vFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV22AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV25AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV24NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV16AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV18AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV17NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV19NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}]}");
         setEventMetadata("'DISASSOCIATE ALL'","{handler:'E179J2',iparms:[{av:'AV20AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV22AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV16AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV18AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV17NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV19NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV22AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV20AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV8FuncaoUsuario_Codigo',fld:'vFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV16AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV18AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV17NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV19NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV25AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV24NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VASSOCIATEDRECORDS.DBLCLICK","{handler:'E189J2',iparms:[{av:'AV20AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV25AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV22AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV16AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV18AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV17NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV19NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV8FuncaoUsuario_Codigo',fld:'vFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV20AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV22AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV16AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV18AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV17NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV19NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV25AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV24NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VNOTASSOCIATEDRECORDS.DBLCLICK","{handler:'E199J2',iparms:[{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV24NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV22AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV20AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV16AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV18AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV17NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV19NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV8FuncaoUsuario_Codigo',fld:'vFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV22AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV16AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV18AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV17NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV19NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV25AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV24NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV20AddedKeyList = new GxSimpleCollection();
         AV22AddedDscList = new GxSimpleCollection();
         AV21NotAddedKeyList = new GxSimpleCollection();
         AV23NotAddedDscList = new GxSimpleCollection();
         AV11FuncoesUsuarioFuncoesAPF = new SdtFuncoesUsuarioFuncoesAPF(context);
         A166FuncaoAPF_Nome = "";
         GXKey = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV16AddedKeyListXml = "";
         AV17NotAddedKeyListXml = "";
         AV18AddedDscListXml = "";
         AV19NotAddedDscListXml = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H009J2_A360FuncaoAPF_SistemaCod = new int[1] ;
         H009J2_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         H009J2_A165FuncaoAPF_Codigo = new int[1] ;
         H009J2_A166FuncaoAPF_Nome = new String[] {""} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9HTTPRequest = new GxHttpRequest( context);
         H009J3_A360FuncaoAPF_SistemaCod = new int[1] ;
         H009J3_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         H009J3_A165FuncaoAPF_Codigo = new int[1] ;
         H009J4_A127Sistema_Codigo = new int[1] ;
         H009J4_A161FuncaoUsuario_Codigo = new int[1] ;
         H009J4_A162FuncaoUsuario_Nome = new String[] {""} ;
         A162FuncaoUsuario_Nome = "";
         AV13Description = "";
         H009J5_A127Sistema_Codigo = new int[1] ;
         H009J5_A161FuncaoUsuario_Codigo = new int[1] ;
         AV36GXV5 = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV15Message = new SdtMessages_Message(context);
         H009J6_A165FuncaoAPF_Codigo = new int[1] ;
         H009J6_A161FuncaoUsuario_Codigo = new int[1] ;
         sStyleString = "";
         bttBtn_confirm_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         lblNotassociatedrecordstitle_Jsonclick = "";
         lblCenterrecordstitle_Jsonclick = "";
         lblAssociatedrecordstitle_Jsonclick = "";
         imgImageassociateall_Jsonclick = "";
         imgImageassociateselected_Jsonclick = "";
         imgImagedisassociateselected_Jsonclick = "";
         imgImagedisassociateall_Jsonclick = "";
         lblAssociationtitle_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_associarfuncaoapffuncoesusuariofuncoesapf__default(),
            new Object[][] {
                new Object[] {
               H009J2_A360FuncaoAPF_SistemaCod, H009J2_n360FuncaoAPF_SistemaCod, H009J2_A165FuncaoAPF_Codigo, H009J2_A166FuncaoAPF_Nome
               }
               , new Object[] {
               H009J3_A360FuncaoAPF_SistemaCod, H009J3_n360FuncaoAPF_SistemaCod, H009J3_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               H009J4_A127Sistema_Codigo, H009J4_A161FuncaoUsuario_Codigo, H009J4_A162FuncaoUsuario_Nome
               }
               , new Object[] {
               H009J5_A127Sistema_Codigo, H009J5_A161FuncaoUsuario_Codigo
               }
               , new Object[] {
               H009J6_A165FuncaoAPF_Codigo, H009J6_A161FuncaoUsuario_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nRcdExists_5 ;
      private short nIsMod_5 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV29GXLvl8 ;
      private short AV41GXLvl334 ;
      private short nGXWrapped ;
      private int AV7FuncaoAPF_Codigo ;
      private int A127Sistema_Codigo ;
      private int wcpOAV7FuncaoAPF_Codigo ;
      private int wcpOA127Sistema_Codigo ;
      private int A165FuncaoAPF_Codigo ;
      private int A161FuncaoUsuario_Codigo ;
      private int AV8FuncaoUsuario_Codigo ;
      private int edtavAddedkeylistxml_Visible ;
      private int edtavNotaddedkeylistxml_Visible ;
      private int edtavAddeddsclistxml_Visible ;
      private int edtavNotaddeddsclistxml_Visible ;
      private int AV24NotAssociatedRecords ;
      private int AV25AssociatedRecords ;
      private int A360FuncaoAPF_SistemaCod ;
      private int imgImageassociateselected_Visible ;
      private int imgImageassociateall_Visible ;
      private int imgImagedisassociateselected_Visible ;
      private int imgImagedisassociateall_Visible ;
      private int bttBtn_confirm_Visible ;
      private int AV31GXV1 ;
      private int AV33GXV2 ;
      private int AV14i ;
      private int AV34GXV3 ;
      private int AV35GXV4 ;
      private int AV37GXV6 ;
      private int AV26InsertIndex ;
      private int AV38GXV7 ;
      private int AV39GXV8 ;
      private int AV40GXV9 ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String edtavAddedkeylistxml_Internalname ;
      private String edtavNotaddedkeylistxml_Internalname ;
      private String edtavAddeddsclistxml_Internalname ;
      private String edtavNotaddeddsclistxml_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String lstavNotassociatedrecords_Internalname ;
      private String scmdbuf ;
      private String edtFuncaoAPF_Nome_Internalname ;
      private String lstavAssociatedrecords_Internalname ;
      private String imgImageassociateselected_Internalname ;
      private String imgImageassociateall_Internalname ;
      private String imgImagedisassociateselected_Internalname ;
      private String imgImagedisassociateall_Internalname ;
      private String bttBtn_confirm_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String bttBtn_confirm_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String tblTablefullcontent_Internalname ;
      private String tblTablecontent_Internalname ;
      private String lblNotassociatedrecordstitle_Internalname ;
      private String lblNotassociatedrecordstitle_Jsonclick ;
      private String lblCenterrecordstitle_Internalname ;
      private String lblCenterrecordstitle_Jsonclick ;
      private String lblAssociatedrecordstitle_Internalname ;
      private String lblAssociatedrecordstitle_Jsonclick ;
      private String lstavNotassociatedrecords_Jsonclick ;
      private String lstavAssociatedrecords_Jsonclick ;
      private String tblTable1_Internalname ;
      private String imgImageassociateall_Jsonclick ;
      private String imgImageassociateselected_Jsonclick ;
      private String imgImagedisassociateselected_Jsonclick ;
      private String imgImagedisassociateall_Jsonclick ;
      private String tblTablemergedassociationtitle_Internalname ;
      private String lblAssociationtitle_Internalname ;
      private String lblAssociationtitle_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n360FuncaoAPF_SistemaCod ;
      private bool returnInSub ;
      private bool AV10Exist ;
      private bool AV12Success ;
      private String AV16AddedKeyListXml ;
      private String AV17NotAddedKeyListXml ;
      private String AV18AddedDscListXml ;
      private String AV19NotAddedDscListXml ;
      private String A166FuncaoAPF_Nome ;
      private String A162FuncaoUsuario_Nome ;
      private String AV13Description ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXListbox lstavNotassociatedrecords ;
      private GXListbox lstavAssociatedrecords ;
      private IDataStoreProvider pr_default ;
      private int[] H009J2_A360FuncaoAPF_SistemaCod ;
      private bool[] H009J2_n360FuncaoAPF_SistemaCod ;
      private int[] H009J2_A165FuncaoAPF_Codigo ;
      private String[] H009J2_A166FuncaoAPF_Nome ;
      private int[] H009J3_A360FuncaoAPF_SistemaCod ;
      private bool[] H009J3_n360FuncaoAPF_SistemaCod ;
      private int[] H009J3_A165FuncaoAPF_Codigo ;
      private int[] H009J4_A127Sistema_Codigo ;
      private int[] H009J4_A161FuncaoUsuario_Codigo ;
      private String[] H009J4_A162FuncaoUsuario_Nome ;
      private int[] H009J5_A127Sistema_Codigo ;
      private int[] H009J5_A161FuncaoUsuario_Codigo ;
      private int[] H009J6_A165FuncaoAPF_Codigo ;
      private int[] H009J6_A161FuncaoUsuario_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV9HTTPRequest ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV20AddedKeyList ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV21NotAddedKeyList ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22AddedDscList ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23NotAddedDscList ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV36GXV5 ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private SdtFuncoesUsuarioFuncoesAPF AV11FuncoesUsuarioFuncoesAPF ;
      private SdtMessages_Message AV15Message ;
   }

   public class wp_associarfuncaoapffuncoesusuariofuncoesapf__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH009J2 ;
          prmH009J2 = new Object[] {
          new Object[] {"@AV7FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH009J3 ;
          prmH009J3 = new Object[] {
          new Object[] {"@AV7FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH009J4 ;
          prmH009J4 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH009J5 ;
          prmH009J5 = new Object[] {
          new Object[] {"@AV8FuncaoUsuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV7FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH009J6 ;
          prmH009J6 = new Object[] {
          new Object[] {"@AV8FuncaoUsuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV7FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H009J2", "SELECT [FuncaoAPF_SistemaCod], [FuncaoAPF_Codigo], [FuncaoAPF_Nome] FROM [FuncoesAPF] WITH (NOLOCK) WHERE ([FuncaoAPF_Codigo] = @AV7FuncaoAPF_Codigo) AND ([FuncaoAPF_SistemaCod] = @Sistema_Codigo) ORDER BY [FuncaoAPF_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009J2,1,0,true,true )
             ,new CursorDef("H009J3", "SELECT [FuncaoAPF_SistemaCod], [FuncaoAPF_Codigo] FROM [FuncoesAPF] WITH (NOLOCK) WHERE ([FuncaoAPF_Codigo] = @AV7FuncaoAPF_Codigo) AND ([FuncaoAPF_SistemaCod] = @Sistema_Codigo) ORDER BY [FuncaoAPF_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009J3,1,0,false,true )
             ,new CursorDef("H009J4", "SELECT [Sistema_Codigo], [FuncaoUsuario_Codigo], [FuncaoUsuario_Nome] FROM [ModuloFuncoes] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ORDER BY [FuncaoUsuario_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009J4,100,0,true,false )
             ,new CursorDef("H009J5", "SELECT TOP 1 [Sistema_Codigo], [FuncaoUsuario_Codigo] FROM [ModuloFuncoes] WITH (NOLOCK) WHERE ([FuncaoUsuario_Codigo] = @AV8FuncaoUsuario_Codigo) AND ([Sistema_Codigo] = @Sistema_Codigo) AND (@FuncaoAPF_Codigo = @AV7FuncaoAPF_Codigo) ORDER BY [FuncaoUsuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009J5,1,0,false,true )
             ,new CursorDef("H009J6", "SELECT TOP 1 [FuncaoAPF_Codigo], [FuncaoUsuario_Codigo] FROM [FuncoesUsuarioFuncoesAPF] WITH (NOLOCK) WHERE [FuncaoUsuario_Codigo] = @AV8FuncaoUsuario_Codigo and [FuncaoAPF_Codigo] = @AV7FuncaoAPF_Codigo ORDER BY [FuncaoUsuario_Codigo], [FuncaoAPF_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009J6,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
