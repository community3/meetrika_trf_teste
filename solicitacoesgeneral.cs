/*
               File: SolicitacoesGeneral
        Description: Solicitacoes General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:1:27.32
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class solicitacoesgeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public solicitacoesgeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public solicitacoesgeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Solicitacoes_Codigo )
      {
         this.A439Solicitacoes_Codigo = aP0_Solicitacoes_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         radSolicitacoes_Novo_Projeto = new GXRadio();
         cmbSolicitacoes_Status = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A439Solicitacoes_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A439Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A439Solicitacoes_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A439Solicitacoes_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAAX2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "SolicitacoesGeneral";
               context.Gx_err = 0;
               WSAX2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Solicitacoes General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282312736");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("solicitacoesgeneral.aspx") + "?" + UrlEncode("" +A439Solicitacoes_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA439Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA439Solicitacoes_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMA_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SOLICITACOES_NOVO_PROJETO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A440Solicitacoes_Novo_Projeto, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SOLICITACOES_OBJETIVO", GetSecureSignedToken( sPrefix, A441Solicitacoes_Objetivo));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SOLICITACOES_USUARIO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A442Solicitacoes_Usuario), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SOLICITACOES_DATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A444Solicitacoes_Data, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SOLICITACOES_USUARIO_ULT", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A443Solicitacoes_Usuario_Ult), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SOLICITACOES_DATA_ULT", GetSecureSignedToken( sPrefix, context.localUtil.Format( A445Solicitacoes_Data_Ult, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SOLICITACOES_STATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A446Solicitacoes_Status, ""))));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormAX2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("solicitacoesgeneral.js", "?20204282312739");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "SolicitacoesGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Solicitacoes General" ;
      }

      protected void WBAX0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "solicitacoesgeneral.aspx");
            }
            wb_table1_2_AX2( true) ;
         }
         else
         {
            wb_table1_2_AX2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_AX2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTAX2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Solicitacoes General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPAX0( ) ;
            }
         }
      }

      protected void WSAX2( )
      {
         STARTAX2( ) ;
         EVTAX2( ) ;
      }

      protected void EVTAX2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAX0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAX0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11AX2 */
                                    E11AX2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAX0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12AX2 */
                                    E12AX2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAX0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13AX2 */
                                    E13AX2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAX0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14AX2 */
                                    E14AX2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAX0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAX0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEAX2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormAX2( ) ;
            }
         }
      }

      protected void PAAX2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            radSolicitacoes_Novo_Projeto.Name = "SOLICITACOES_NOVO_PROJETO";
            radSolicitacoes_Novo_Projeto.WebTags = "";
            radSolicitacoes_Novo_Projeto.addItem("1", "Sim", 0);
            radSolicitacoes_Novo_Projeto.addItem("0", "N�o", 0);
            cmbSolicitacoes_Status.Name = "SOLICITACOES_STATUS";
            cmbSolicitacoes_Status.WebTags = "";
            cmbSolicitacoes_Status.addItem("A", "Ativo", 0);
            cmbSolicitacoes_Status.addItem("E", "Excluido", 0);
            cmbSolicitacoes_Status.addItem("R", "Rascunho", 0);
            if ( cmbSolicitacoes_Status.ItemCount > 0 )
            {
               A446Solicitacoes_Status = cmbSolicitacoes_Status.getValidValue(A446Solicitacoes_Status);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A446Solicitacoes_Status", A446Solicitacoes_Status);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SOLICITACOES_STATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A446Solicitacoes_Status, ""))));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbSolicitacoes_Status.ItemCount > 0 )
         {
            A446Solicitacoes_Status = cmbSolicitacoes_Status.getValidValue(A446Solicitacoes_Status);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A446Solicitacoes_Status", A446Solicitacoes_Status);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SOLICITACOES_STATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A446Solicitacoes_Status, ""))));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFAX2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "SolicitacoesGeneral";
         context.Gx_err = 0;
      }

      protected void RFAX2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00AX2 */
            pr_default.execute(0, new Object[] {A439Solicitacoes_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A446Solicitacoes_Status = H00AX2_A446Solicitacoes_Status[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A446Solicitacoes_Status", A446Solicitacoes_Status);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SOLICITACOES_STATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A446Solicitacoes_Status, ""))));
               A445Solicitacoes_Data_Ult = H00AX2_A445Solicitacoes_Data_Ult[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A445Solicitacoes_Data_Ult", context.localUtil.TToC( A445Solicitacoes_Data_Ult, 8, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SOLICITACOES_DATA_ULT", GetSecureSignedToken( sPrefix, context.localUtil.Format( A445Solicitacoes_Data_Ult, "99/99/99 99:99")));
               A443Solicitacoes_Usuario_Ult = H00AX2_A443Solicitacoes_Usuario_Ult[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A443Solicitacoes_Usuario_Ult", StringUtil.LTrim( StringUtil.Str( (decimal)(A443Solicitacoes_Usuario_Ult), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SOLICITACOES_USUARIO_ULT", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A443Solicitacoes_Usuario_Ult), "ZZZZZ9")));
               A444Solicitacoes_Data = H00AX2_A444Solicitacoes_Data[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A444Solicitacoes_Data", context.localUtil.TToC( A444Solicitacoes_Data, 8, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SOLICITACOES_DATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A444Solicitacoes_Data, "99/99/99 99:99")));
               A442Solicitacoes_Usuario = H00AX2_A442Solicitacoes_Usuario[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A442Solicitacoes_Usuario", StringUtil.LTrim( StringUtil.Str( (decimal)(A442Solicitacoes_Usuario), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SOLICITACOES_USUARIO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A442Solicitacoes_Usuario), "ZZZZZ9")));
               A441Solicitacoes_Objetivo = H00AX2_A441Solicitacoes_Objetivo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A441Solicitacoes_Objetivo", A441Solicitacoes_Objetivo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SOLICITACOES_OBJETIVO", GetSecureSignedToken( sPrefix, A441Solicitacoes_Objetivo));
               n441Solicitacoes_Objetivo = H00AX2_n441Solicitacoes_Objetivo[0];
               A440Solicitacoes_Novo_Projeto = H00AX2_A440Solicitacoes_Novo_Projeto[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A440Solicitacoes_Novo_Projeto", A440Solicitacoes_Novo_Projeto);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SOLICITACOES_NOVO_PROJETO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A440Solicitacoes_Novo_Projeto, ""))));
               A155Servico_Codigo = H00AX2_A155Servico_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9")));
               A127Sistema_Codigo = H00AX2_A127Sistema_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9")));
               A39Contratada_Codigo = H00AX2_A39Contratada_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9")));
               /* Execute user event: E12AX2 */
               E12AX2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBAX0( ) ;
         }
      }

      protected void STRUPAX0( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "SolicitacoesGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11AX2 */
         E11AX2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratada_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9")));
            A127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSistema_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9")));
            A155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServico_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9")));
            A440Solicitacoes_Novo_Projeto = cgiGet( radSolicitacoes_Novo_Projeto_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A440Solicitacoes_Novo_Projeto", A440Solicitacoes_Novo_Projeto);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SOLICITACOES_NOVO_PROJETO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A440Solicitacoes_Novo_Projeto, ""))));
            A441Solicitacoes_Objetivo = cgiGet( edtSolicitacoes_Objetivo_Internalname);
            n441Solicitacoes_Objetivo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A441Solicitacoes_Objetivo", A441Solicitacoes_Objetivo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SOLICITACOES_OBJETIVO", GetSecureSignedToken( sPrefix, A441Solicitacoes_Objetivo));
            A442Solicitacoes_Usuario = (int)(context.localUtil.CToN( cgiGet( edtSolicitacoes_Usuario_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A442Solicitacoes_Usuario", StringUtil.LTrim( StringUtil.Str( (decimal)(A442Solicitacoes_Usuario), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SOLICITACOES_USUARIO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A442Solicitacoes_Usuario), "ZZZZZ9")));
            A444Solicitacoes_Data = context.localUtil.CToT( cgiGet( edtSolicitacoes_Data_Internalname), 0);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A444Solicitacoes_Data", context.localUtil.TToC( A444Solicitacoes_Data, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SOLICITACOES_DATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A444Solicitacoes_Data, "99/99/99 99:99")));
            A443Solicitacoes_Usuario_Ult = (int)(context.localUtil.CToN( cgiGet( edtSolicitacoes_Usuario_Ult_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A443Solicitacoes_Usuario_Ult", StringUtil.LTrim( StringUtil.Str( (decimal)(A443Solicitacoes_Usuario_Ult), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SOLICITACOES_USUARIO_ULT", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A443Solicitacoes_Usuario_Ult), "ZZZZZ9")));
            A445Solicitacoes_Data_Ult = context.localUtil.CToT( cgiGet( edtSolicitacoes_Data_Ult_Internalname), 0);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A445Solicitacoes_Data_Ult", context.localUtil.TToC( A445Solicitacoes_Data_Ult, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SOLICITACOES_DATA_ULT", GetSecureSignedToken( sPrefix, context.localUtil.Format( A445Solicitacoes_Data_Ult, "99/99/99 99:99")));
            cmbSolicitacoes_Status.CurrentValue = cgiGet( cmbSolicitacoes_Status_Internalname);
            A446Solicitacoes_Status = cgiGet( cmbSolicitacoes_Status_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A446Solicitacoes_Status", A446Solicitacoes_Status);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SOLICITACOES_STATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A446Solicitacoes_Status, ""))));
            /* Read saved values. */
            wcpOA439Solicitacoes_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA439Solicitacoes_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11AX2 */
         E11AX2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11AX2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12AX2( )
      {
         /* Load Routine */
      }

      protected void E13AX2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("solicitacoes.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A439Solicitacoes_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14AX2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("solicitacoes.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A439Solicitacoes_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Solicitacoes";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "Solicitacoes_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Solicitacoes_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_AX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_AX2( true) ;
         }
         else
         {
            wb_table2_8_AX2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_AX2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_66_AX2( true) ;
         }
         else
         {
            wb_table3_66_AX2( false) ;
         }
         return  ;
      }

      protected void wb_table3_66_AX2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_AX2e( true) ;
         }
         else
         {
            wb_table1_2_AX2e( false) ;
         }
      }

      protected void wb_table3_66_AX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_SolicitacoesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_SolicitacoesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_66_AX2e( true) ;
         }
         else
         {
            wb_table3_66_AX2e( false) ;
         }
      }

      protected void wb_table2_8_AX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicitacoes_codigo_Internalname, "Solicitacoes_Codigo", "", "", lblTextblocksolicitacoes_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacoesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSolicitacoes_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A439Solicitacoes_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A439Solicitacoes_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSolicitacoes_Codigo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SolicitacoesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_codigo_Internalname, "Contratada", "", "", lblTextblockcontratada_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacoesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_Codigo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SolicitacoesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_codigo_Internalname, "Sistema", "", "", lblTextblocksistema_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacoesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSistema_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistema_Codigo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SolicitacoesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_codigo_Internalname, "Servi�o", "", "", lblTextblockservico_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacoesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServico_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_Codigo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SolicitacoesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicitacoes_novo_projeto_Internalname, "Projeto", "", "", lblTextblocksolicitacoes_novo_projeto_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacoesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Radio button */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_radio_ctrl( context, radSolicitacoes_Novo_Projeto, radSolicitacoes_Novo_Projeto_Internalname, StringUtil.RTrim( A440Solicitacoes_Novo_Projeto), "", 1, 0, 0, 0, StyleString, ClassString, "", 0, radSolicitacoes_Novo_Projeto_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "HLP_SolicitacoesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicitacoes_objetivo_Internalname, "Objetivo", "", "", lblTextblocksolicitacoes_objetivo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacoesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtSolicitacoes_Objetivo_Internalname, A441Solicitacoes_Objetivo, "", "", 0, 1, 0, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_SolicitacoesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicitacoes_usuario_Internalname, "da Solicita��es", "", "", lblTextblocksolicitacoes_usuario_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacoesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSolicitacoes_Usuario_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A442Solicitacoes_Usuario), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A442Solicitacoes_Usuario), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSolicitacoes_Usuario_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SolicitacoesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicitacoes_data_Internalname, "de inclus�o", "", "", lblTextblocksolicitacoes_data_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacoesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtSolicitacoes_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtSolicitacoes_Data_Internalname, context.localUtil.TToC( A444Solicitacoes_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A444Solicitacoes_Data, "99/99/99 99:99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSolicitacoes_Data_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SolicitacoesGeneral.htm");
            GxWebStd.gx_bitmap( context, edtSolicitacoes_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_SolicitacoesGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicitacoes_usuario_ult_Internalname, "Ultima Modifica��o", "", "", lblTextblocksolicitacoes_usuario_ult_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacoesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSolicitacoes_Usuario_Ult_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A443Solicitacoes_Usuario_Ult), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A443Solicitacoes_Usuario_Ult), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSolicitacoes_Usuario_Ult_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SolicitacoesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicitacoes_data_ult_Internalname, "Ultima Modifica��o", "", "", lblTextblocksolicitacoes_data_ult_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacoesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtSolicitacoes_Data_Ult_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtSolicitacoes_Data_Ult_Internalname, context.localUtil.TToC( A445Solicitacoes_Data_Ult, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A445Solicitacoes_Data_Ult, "99/99/99 99:99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSolicitacoes_Data_Ult_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SolicitacoesGeneral.htm");
            GxWebStd.gx_bitmap( context, edtSolicitacoes_Data_Ult_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_SolicitacoesGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicitacoes_status_Internalname, "Solicitacoes_Status", "", "", lblTextblocksolicitacoes_status_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacoesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbSolicitacoes_Status, cmbSolicitacoes_Status_Internalname, StringUtil.RTrim( A446Solicitacoes_Status), 1, cmbSolicitacoes_Status_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_SolicitacoesGeneral.htm");
            cmbSolicitacoes_Status.CurrentValue = StringUtil.RTrim( A446Solicitacoes_Status);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbSolicitacoes_Status_Internalname, "Values", (String)(cmbSolicitacoes_Status.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_AX2e( true) ;
         }
         else
         {
            wb_table2_8_AX2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A439Solicitacoes_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A439Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A439Solicitacoes_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAAX2( ) ;
         WSAX2( ) ;
         WEAX2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA439Solicitacoes_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAAX2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "solicitacoesgeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAAX2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A439Solicitacoes_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A439Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A439Solicitacoes_Codigo), 6, 0)));
         }
         wcpOA439Solicitacoes_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA439Solicitacoes_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A439Solicitacoes_Codigo != wcpOA439Solicitacoes_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA439Solicitacoes_Codigo = A439Solicitacoes_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA439Solicitacoes_Codigo = cgiGet( sPrefix+"A439Solicitacoes_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA439Solicitacoes_Codigo) > 0 )
         {
            A439Solicitacoes_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA439Solicitacoes_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A439Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A439Solicitacoes_Codigo), 6, 0)));
         }
         else
         {
            A439Solicitacoes_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A439Solicitacoes_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAAX2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSAX2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSAX2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A439Solicitacoes_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A439Solicitacoes_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA439Solicitacoes_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A439Solicitacoes_Codigo_CTRL", StringUtil.RTrim( sCtrlA439Solicitacoes_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEAX2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282312795");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("solicitacoesgeneral.js", "?20204282312795");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocksolicitacoes_codigo_Internalname = sPrefix+"TEXTBLOCKSOLICITACOES_CODIGO";
         edtSolicitacoes_Codigo_Internalname = sPrefix+"SOLICITACOES_CODIGO";
         lblTextblockcontratada_codigo_Internalname = sPrefix+"TEXTBLOCKCONTRATADA_CODIGO";
         edtContratada_Codigo_Internalname = sPrefix+"CONTRATADA_CODIGO";
         lblTextblocksistema_codigo_Internalname = sPrefix+"TEXTBLOCKSISTEMA_CODIGO";
         edtSistema_Codigo_Internalname = sPrefix+"SISTEMA_CODIGO";
         lblTextblockservico_codigo_Internalname = sPrefix+"TEXTBLOCKSERVICO_CODIGO";
         edtServico_Codigo_Internalname = sPrefix+"SERVICO_CODIGO";
         lblTextblocksolicitacoes_novo_projeto_Internalname = sPrefix+"TEXTBLOCKSOLICITACOES_NOVO_PROJETO";
         radSolicitacoes_Novo_Projeto_Internalname = sPrefix+"SOLICITACOES_NOVO_PROJETO";
         lblTextblocksolicitacoes_objetivo_Internalname = sPrefix+"TEXTBLOCKSOLICITACOES_OBJETIVO";
         edtSolicitacoes_Objetivo_Internalname = sPrefix+"SOLICITACOES_OBJETIVO";
         lblTextblocksolicitacoes_usuario_Internalname = sPrefix+"TEXTBLOCKSOLICITACOES_USUARIO";
         edtSolicitacoes_Usuario_Internalname = sPrefix+"SOLICITACOES_USUARIO";
         lblTextblocksolicitacoes_data_Internalname = sPrefix+"TEXTBLOCKSOLICITACOES_DATA";
         edtSolicitacoes_Data_Internalname = sPrefix+"SOLICITACOES_DATA";
         lblTextblocksolicitacoes_usuario_ult_Internalname = sPrefix+"TEXTBLOCKSOLICITACOES_USUARIO_ULT";
         edtSolicitacoes_Usuario_Ult_Internalname = sPrefix+"SOLICITACOES_USUARIO_ULT";
         lblTextblocksolicitacoes_data_ult_Internalname = sPrefix+"TEXTBLOCKSOLICITACOES_DATA_ULT";
         edtSolicitacoes_Data_Ult_Internalname = sPrefix+"SOLICITACOES_DATA_ULT";
         lblTextblocksolicitacoes_status_Internalname = sPrefix+"TEXTBLOCKSOLICITACOES_STATUS";
         cmbSolicitacoes_Status_Internalname = sPrefix+"SOLICITACOES_STATUS";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         cmbSolicitacoes_Status_Jsonclick = "";
         edtSolicitacoes_Data_Ult_Jsonclick = "";
         edtSolicitacoes_Usuario_Ult_Jsonclick = "";
         edtSolicitacoes_Data_Jsonclick = "";
         edtSolicitacoes_Usuario_Jsonclick = "";
         radSolicitacoes_Novo_Projeto_Jsonclick = "";
         edtServico_Codigo_Jsonclick = "";
         edtSistema_Codigo_Jsonclick = "";
         edtContratada_Codigo_Jsonclick = "";
         edtSolicitacoes_Codigo_Jsonclick = "";
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13AX2',iparms:[{av:'A439Solicitacoes_Codigo',fld:'SOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14AX2',iparms:[{av:'A439Solicitacoes_Codigo',fld:'SOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A440Solicitacoes_Novo_Projeto = "";
         A441Solicitacoes_Objetivo = "";
         A444Solicitacoes_Data = (DateTime)(DateTime.MinValue);
         A445Solicitacoes_Data_Ult = (DateTime)(DateTime.MinValue);
         A446Solicitacoes_Status = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00AX2_A439Solicitacoes_Codigo = new int[1] ;
         H00AX2_A446Solicitacoes_Status = new String[] {""} ;
         H00AX2_A445Solicitacoes_Data_Ult = new DateTime[] {DateTime.MinValue} ;
         H00AX2_A443Solicitacoes_Usuario_Ult = new int[1] ;
         H00AX2_A444Solicitacoes_Data = new DateTime[] {DateTime.MinValue} ;
         H00AX2_A442Solicitacoes_Usuario = new int[1] ;
         H00AX2_A441Solicitacoes_Objetivo = new String[] {""} ;
         H00AX2_n441Solicitacoes_Objetivo = new bool[] {false} ;
         H00AX2_A440Solicitacoes_Novo_Projeto = new String[] {""} ;
         H00AX2_A155Servico_Codigo = new int[1] ;
         H00AX2_A127Sistema_Codigo = new int[1] ;
         H00AX2_A39Contratada_Codigo = new int[1] ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblocksolicitacoes_codigo_Jsonclick = "";
         lblTextblockcontratada_codigo_Jsonclick = "";
         lblTextblocksistema_codigo_Jsonclick = "";
         lblTextblockservico_codigo_Jsonclick = "";
         lblTextblocksolicitacoes_novo_projeto_Jsonclick = "";
         lblTextblocksolicitacoes_objetivo_Jsonclick = "";
         lblTextblocksolicitacoes_usuario_Jsonclick = "";
         lblTextblocksolicitacoes_data_Jsonclick = "";
         lblTextblocksolicitacoes_usuario_ult_Jsonclick = "";
         lblTextblocksolicitacoes_data_ult_Jsonclick = "";
         lblTextblocksolicitacoes_status_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA439Solicitacoes_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.solicitacoesgeneral__default(),
            new Object[][] {
                new Object[] {
               H00AX2_A439Solicitacoes_Codigo, H00AX2_A446Solicitacoes_Status, H00AX2_A445Solicitacoes_Data_Ult, H00AX2_A443Solicitacoes_Usuario_Ult, H00AX2_A444Solicitacoes_Data, H00AX2_A442Solicitacoes_Usuario, H00AX2_A441Solicitacoes_Objetivo, H00AX2_n441Solicitacoes_Objetivo, H00AX2_A440Solicitacoes_Novo_Projeto, H00AX2_A155Servico_Codigo,
               H00AX2_A127Sistema_Codigo, H00AX2_A39Contratada_Codigo
               }
            }
         );
         AV14Pgmname = "SolicitacoesGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "SolicitacoesGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A439Solicitacoes_Codigo ;
      private int wcpOA439Solicitacoes_Codigo ;
      private int A39Contratada_Codigo ;
      private int A127Sistema_Codigo ;
      private int A155Servico_Codigo ;
      private int A442Solicitacoes_Usuario ;
      private int A443Solicitacoes_Usuario_Ult ;
      private int AV7Solicitacoes_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A440Solicitacoes_Novo_Projeto ;
      private String A446Solicitacoes_Status ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String edtContratada_Codigo_Internalname ;
      private String edtSistema_Codigo_Internalname ;
      private String edtServico_Codigo_Internalname ;
      private String radSolicitacoes_Novo_Projeto_Internalname ;
      private String edtSolicitacoes_Objetivo_Internalname ;
      private String edtSolicitacoes_Usuario_Internalname ;
      private String edtSolicitacoes_Data_Internalname ;
      private String edtSolicitacoes_Usuario_Ult_Internalname ;
      private String edtSolicitacoes_Data_Ult_Internalname ;
      private String cmbSolicitacoes_Status_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Internalname ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Internalname ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocksolicitacoes_codigo_Internalname ;
      private String lblTextblocksolicitacoes_codigo_Jsonclick ;
      private String edtSolicitacoes_Codigo_Internalname ;
      private String edtSolicitacoes_Codigo_Jsonclick ;
      private String lblTextblockcontratada_codigo_Internalname ;
      private String lblTextblockcontratada_codigo_Jsonclick ;
      private String edtContratada_Codigo_Jsonclick ;
      private String lblTextblocksistema_codigo_Internalname ;
      private String lblTextblocksistema_codigo_Jsonclick ;
      private String edtSistema_Codigo_Jsonclick ;
      private String lblTextblockservico_codigo_Internalname ;
      private String lblTextblockservico_codigo_Jsonclick ;
      private String edtServico_Codigo_Jsonclick ;
      private String lblTextblocksolicitacoes_novo_projeto_Internalname ;
      private String lblTextblocksolicitacoes_novo_projeto_Jsonclick ;
      private String radSolicitacoes_Novo_Projeto_Jsonclick ;
      private String lblTextblocksolicitacoes_objetivo_Internalname ;
      private String lblTextblocksolicitacoes_objetivo_Jsonclick ;
      private String lblTextblocksolicitacoes_usuario_Internalname ;
      private String lblTextblocksolicitacoes_usuario_Jsonclick ;
      private String edtSolicitacoes_Usuario_Jsonclick ;
      private String lblTextblocksolicitacoes_data_Internalname ;
      private String lblTextblocksolicitacoes_data_Jsonclick ;
      private String edtSolicitacoes_Data_Jsonclick ;
      private String lblTextblocksolicitacoes_usuario_ult_Internalname ;
      private String lblTextblocksolicitacoes_usuario_ult_Jsonclick ;
      private String edtSolicitacoes_Usuario_Ult_Jsonclick ;
      private String lblTextblocksolicitacoes_data_ult_Internalname ;
      private String lblTextblocksolicitacoes_data_ult_Jsonclick ;
      private String edtSolicitacoes_Data_Ult_Jsonclick ;
      private String lblTextblocksolicitacoes_status_Internalname ;
      private String lblTextblocksolicitacoes_status_Jsonclick ;
      private String cmbSolicitacoes_Status_Jsonclick ;
      private String sCtrlA439Solicitacoes_Codigo ;
      private DateTime A444Solicitacoes_Data ;
      private DateTime A445Solicitacoes_Data_Ult ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n441Solicitacoes_Objetivo ;
      private bool returnInSub ;
      private String A441Solicitacoes_Objetivo ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXRadio radSolicitacoes_Novo_Projeto ;
      private GXCombobox cmbSolicitacoes_Status ;
      private IDataStoreProvider pr_default ;
      private int[] H00AX2_A439Solicitacoes_Codigo ;
      private String[] H00AX2_A446Solicitacoes_Status ;
      private DateTime[] H00AX2_A445Solicitacoes_Data_Ult ;
      private int[] H00AX2_A443Solicitacoes_Usuario_Ult ;
      private DateTime[] H00AX2_A444Solicitacoes_Data ;
      private int[] H00AX2_A442Solicitacoes_Usuario ;
      private String[] H00AX2_A441Solicitacoes_Objetivo ;
      private bool[] H00AX2_n441Solicitacoes_Objetivo ;
      private String[] H00AX2_A440Solicitacoes_Novo_Projeto ;
      private int[] H00AX2_A155Servico_Codigo ;
      private int[] H00AX2_A127Sistema_Codigo ;
      private int[] H00AX2_A39Contratada_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class solicitacoesgeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00AX2 ;
          prmH00AX2 = new Object[] {
          new Object[] {"@Solicitacoes_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00AX2", "SELECT [Solicitacoes_Codigo], [Solicitacoes_Status], [Solicitacoes_Data_Ult], [Solicitacoes_Usuario_Ult], [Solicitacoes_Data], [Solicitacoes_Usuario], [Solicitacoes_Objetivo], [Solicitacoes_Novo_Projeto], [Servico_Codigo], [Sistema_Codigo], [Contratada_Codigo] FROM [Solicitacoes] WITH (NOLOCK) WHERE [Solicitacoes_Codigo] = @Solicitacoes_Codigo ORDER BY [Solicitacoes_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AX2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDateTime(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((String[]) buf[6])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(7);
                ((String[]) buf[8])[0] = rslt.getString(8, 1) ;
                ((int[]) buf[9])[0] = rslt.getInt(9) ;
                ((int[]) buf[10])[0] = rslt.getInt(10) ;
                ((int[]) buf[11])[0] = rslt.getInt(11) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
