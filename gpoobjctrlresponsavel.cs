/*
               File: GpoObjCtrlResponsavel
        Description: Gpo Obj Ctrl Responsavel
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:9:20.76
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gpoobjctrlresponsavel : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_2") == 0 )
         {
            A1834GpoObjCtrlResponsavel_CteCteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1834GpoObjCtrlResponsavel_CteCteCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1834GpoObjCtrlResponsavel_CteCteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1834GpoObjCtrlResponsavel_CteCteCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_2( A1834GpoObjCtrlResponsavel_CteCteCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_3") == 0 )
         {
            A1834GpoObjCtrlResponsavel_CteCteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1834GpoObjCtrlResponsavel_CteCteCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1834GpoObjCtrlResponsavel_CteCteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1834GpoObjCtrlResponsavel_CteCteCod), 6, 0)));
            A1835GpoObjCtrlResponsavel_CteUsrCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1835GpoObjCtrlResponsavel_CteUsrCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1835GpoObjCtrlResponsavel_CteUsrCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1835GpoObjCtrlResponsavel_CteUsrCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_3( A1834GpoObjCtrlResponsavel_CteCteCod, A1835GpoObjCtrlResponsavel_CteUsrCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Gpo Obj Ctrl Responsavel", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtGpoObjCtrlResponsavel_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public gpoobjctrlresponsavel( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public gpoobjctrlresponsavel( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_4L202( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_4L202e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_4L202( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_4L202( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_4L202e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Gpo Obj Ctrl Responsavel", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_GpoObjCtrlResponsavel.htm");
            wb_table3_28_4L202( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_4L202e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_4L202e( true) ;
         }
         else
         {
            wb_table1_2_4L202e( false) ;
         }
      }

      protected void wb_table3_28_4L202( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_4L202( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_4L202e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_GpoObjCtrlResponsavel.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_GpoObjCtrlResponsavel.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_GpoObjCtrlResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_4L202e( true) ;
         }
         else
         {
            wb_table3_28_4L202e( false) ;
         }
      }

      protected void wb_table4_34_4L202( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockgpoobjctrlresponsavel_codigo_Internalname, "Ctrl Responsavel_Codigo", "", "", lblTextblockgpoobjctrlresponsavel_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_GpoObjCtrlResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtGpoObjCtrlResponsavel_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1837GpoObjCtrlResponsavel_Codigo), 6, 0, ",", "")), ((edtGpoObjCtrlResponsavel_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1837GpoObjCtrlResponsavel_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1837GpoObjCtrlResponsavel_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGpoObjCtrlResponsavel_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtGpoObjCtrlResponsavel_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_GpoObjCtrlResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockgpoobjctrlresponsavel_gpocod_Internalname, "Responsavel_Gpo Cod", "", "", lblTextblockgpoobjctrlresponsavel_gpocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_GpoObjCtrlResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtGpoObjCtrlResponsavel_GpoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1838GpoObjCtrlResponsavel_GpoCod), 4, 0, ",", "")), ((edtGpoObjCtrlResponsavel_GpoCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1838GpoObjCtrlResponsavel_GpoCod), "ZZZ9")) : context.localUtil.Format( (decimal)(A1838GpoObjCtrlResponsavel_GpoCod), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGpoObjCtrlResponsavel_GpoCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtGpoObjCtrlResponsavel_GpoCod_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GpoObjCtrlResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockgpoobjctrlresponsavel_ctectecod_Internalname, "Contratante", "", "", lblTextblockgpoobjctrlresponsavel_ctectecod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_GpoObjCtrlResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtGpoObjCtrlResponsavel_CteCteCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1834GpoObjCtrlResponsavel_CteCteCod), 6, 0, ",", "")), ((edtGpoObjCtrlResponsavel_CteCteCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1834GpoObjCtrlResponsavel_CteCteCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1834GpoObjCtrlResponsavel_CteCteCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGpoObjCtrlResponsavel_CteCteCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtGpoObjCtrlResponsavel_CteCteCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GpoObjCtrlResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockgpoobjctrlresponsavel_cteusrcod_Internalname, "Usu�rio", "", "", lblTextblockgpoobjctrlresponsavel_cteusrcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_GpoObjCtrlResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtGpoObjCtrlResponsavel_CteUsrCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1835GpoObjCtrlResponsavel_CteUsrCod), 6, 0, ",", "")), ((edtGpoObjCtrlResponsavel_CteUsrCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1835GpoObjCtrlResponsavel_CteUsrCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1835GpoObjCtrlResponsavel_CteUsrCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGpoObjCtrlResponsavel_CteUsrCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtGpoObjCtrlResponsavel_CteUsrCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_GpoObjCtrlResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockgpoobjctrlresponsavel_cteareacod_Internalname, "de Trabalho", "", "", lblTextblockgpoobjctrlresponsavel_cteareacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_GpoObjCtrlResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtGpoObjCtrlResponsavel_CteAreaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1836GpoObjCtrlResponsavel_CteAreaCod), 6, 0, ",", "")), ((edtGpoObjCtrlResponsavel_CteAreaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1836GpoObjCtrlResponsavel_CteAreaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1836GpoObjCtrlResponsavel_CteAreaCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGpoObjCtrlResponsavel_CteAreaCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtGpoObjCtrlResponsavel_CteAreaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GpoObjCtrlResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_4L202e( true) ;
         }
         else
         {
            wb_table4_34_4L202e( false) ;
         }
      }

      protected void wb_table2_5_4L202( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_GpoObjCtrlResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_GpoObjCtrlResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_GpoObjCtrlResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_GpoObjCtrlResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_GpoObjCtrlResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_GpoObjCtrlResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_GpoObjCtrlResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_GpoObjCtrlResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_GpoObjCtrlResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_GpoObjCtrlResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_GpoObjCtrlResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_GpoObjCtrlResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_GpoObjCtrlResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_GpoObjCtrlResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_GpoObjCtrlResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_GpoObjCtrlResponsavel.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_4L202e( true) ;
         }
         else
         {
            wb_table2_5_4L202e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtGpoObjCtrlResponsavel_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtGpoObjCtrlResponsavel_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "GPOOBJCTRLRESPONSAVEL_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtGpoObjCtrlResponsavel_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1837GpoObjCtrlResponsavel_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1837GpoObjCtrlResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1837GpoObjCtrlResponsavel_Codigo), 6, 0)));
               }
               else
               {
                  A1837GpoObjCtrlResponsavel_Codigo = (int)(context.localUtil.CToN( cgiGet( edtGpoObjCtrlResponsavel_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1837GpoObjCtrlResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1837GpoObjCtrlResponsavel_Codigo), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtGpoObjCtrlResponsavel_GpoCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtGpoObjCtrlResponsavel_GpoCod_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "GPOOBJCTRLRESPONSAVEL_GPOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtGpoObjCtrlResponsavel_GpoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1838GpoObjCtrlResponsavel_GpoCod = 0;
                  n1838GpoObjCtrlResponsavel_GpoCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1838GpoObjCtrlResponsavel_GpoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1838GpoObjCtrlResponsavel_GpoCod), 4, 0)));
               }
               else
               {
                  A1838GpoObjCtrlResponsavel_GpoCod = (short)(context.localUtil.CToN( cgiGet( edtGpoObjCtrlResponsavel_GpoCod_Internalname), ",", "."));
                  n1838GpoObjCtrlResponsavel_GpoCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1838GpoObjCtrlResponsavel_GpoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1838GpoObjCtrlResponsavel_GpoCod), 4, 0)));
               }
               n1838GpoObjCtrlResponsavel_GpoCod = ((0==A1838GpoObjCtrlResponsavel_GpoCod) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtGpoObjCtrlResponsavel_CteCteCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtGpoObjCtrlResponsavel_CteCteCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "GPOOBJCTRLRESPONSAVEL_CTECTECOD");
                  AnyError = 1;
                  GX_FocusControl = edtGpoObjCtrlResponsavel_CteCteCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1834GpoObjCtrlResponsavel_CteCteCod = 0;
                  n1834GpoObjCtrlResponsavel_CteCteCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1834GpoObjCtrlResponsavel_CteCteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1834GpoObjCtrlResponsavel_CteCteCod), 6, 0)));
               }
               else
               {
                  A1834GpoObjCtrlResponsavel_CteCteCod = (int)(context.localUtil.CToN( cgiGet( edtGpoObjCtrlResponsavel_CteCteCod_Internalname), ",", "."));
                  n1834GpoObjCtrlResponsavel_CteCteCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1834GpoObjCtrlResponsavel_CteCteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1834GpoObjCtrlResponsavel_CteCteCod), 6, 0)));
               }
               n1834GpoObjCtrlResponsavel_CteCteCod = ((0==A1834GpoObjCtrlResponsavel_CteCteCod) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtGpoObjCtrlResponsavel_CteUsrCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtGpoObjCtrlResponsavel_CteUsrCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "GPOOBJCTRLRESPONSAVEL_CTEUSRCOD");
                  AnyError = 1;
                  GX_FocusControl = edtGpoObjCtrlResponsavel_CteUsrCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1835GpoObjCtrlResponsavel_CteUsrCod = 0;
                  n1835GpoObjCtrlResponsavel_CteUsrCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1835GpoObjCtrlResponsavel_CteUsrCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1835GpoObjCtrlResponsavel_CteUsrCod), 6, 0)));
               }
               else
               {
                  A1835GpoObjCtrlResponsavel_CteUsrCod = (int)(context.localUtil.CToN( cgiGet( edtGpoObjCtrlResponsavel_CteUsrCod_Internalname), ",", "."));
                  n1835GpoObjCtrlResponsavel_CteUsrCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1835GpoObjCtrlResponsavel_CteUsrCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1835GpoObjCtrlResponsavel_CteUsrCod), 6, 0)));
               }
               n1835GpoObjCtrlResponsavel_CteUsrCod = ((0==A1835GpoObjCtrlResponsavel_CteUsrCod) ? true : false);
               A1836GpoObjCtrlResponsavel_CteAreaCod = (int)(context.localUtil.CToN( cgiGet( edtGpoObjCtrlResponsavel_CteAreaCod_Internalname), ",", "."));
               n1836GpoObjCtrlResponsavel_CteAreaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1836GpoObjCtrlResponsavel_CteAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1836GpoObjCtrlResponsavel_CteAreaCod), 6, 0)));
               /* Read saved values. */
               Z1837GpoObjCtrlResponsavel_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1837GpoObjCtrlResponsavel_Codigo"), ",", "."));
               Z1838GpoObjCtrlResponsavel_GpoCod = (short)(context.localUtil.CToN( cgiGet( "Z1838GpoObjCtrlResponsavel_GpoCod"), ",", "."));
               n1838GpoObjCtrlResponsavel_GpoCod = ((0==A1838GpoObjCtrlResponsavel_GpoCod) ? true : false);
               Z1834GpoObjCtrlResponsavel_CteCteCod = (int)(context.localUtil.CToN( cgiGet( "Z1834GpoObjCtrlResponsavel_CteCteCod"), ",", "."));
               n1834GpoObjCtrlResponsavel_CteCteCod = ((0==A1834GpoObjCtrlResponsavel_CteCteCod) ? true : false);
               Z1835GpoObjCtrlResponsavel_CteUsrCod = (int)(context.localUtil.CToN( cgiGet( "Z1835GpoObjCtrlResponsavel_CteUsrCod"), ",", "."));
               n1835GpoObjCtrlResponsavel_CteUsrCod = ((0==A1835GpoObjCtrlResponsavel_CteUsrCod) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A1837GpoObjCtrlResponsavel_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1837GpoObjCtrlResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1837GpoObjCtrlResponsavel_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll4L202( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes4L202( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption4L0( )
      {
      }

      protected void ZM4L202( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1838GpoObjCtrlResponsavel_GpoCod = T004L3_A1838GpoObjCtrlResponsavel_GpoCod[0];
               Z1834GpoObjCtrlResponsavel_CteCteCod = T004L3_A1834GpoObjCtrlResponsavel_CteCteCod[0];
               Z1835GpoObjCtrlResponsavel_CteUsrCod = T004L3_A1835GpoObjCtrlResponsavel_CteUsrCod[0];
            }
            else
            {
               Z1838GpoObjCtrlResponsavel_GpoCod = A1838GpoObjCtrlResponsavel_GpoCod;
               Z1834GpoObjCtrlResponsavel_CteCteCod = A1834GpoObjCtrlResponsavel_CteCteCod;
               Z1835GpoObjCtrlResponsavel_CteUsrCod = A1835GpoObjCtrlResponsavel_CteUsrCod;
            }
         }
         if ( GX_JID == -1 )
         {
            Z1837GpoObjCtrlResponsavel_Codigo = A1837GpoObjCtrlResponsavel_Codigo;
            Z1838GpoObjCtrlResponsavel_GpoCod = A1838GpoObjCtrlResponsavel_GpoCod;
            Z1834GpoObjCtrlResponsavel_CteCteCod = A1834GpoObjCtrlResponsavel_CteCteCod;
            Z1835GpoObjCtrlResponsavel_CteUsrCod = A1835GpoObjCtrlResponsavel_CteUsrCod;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load4L202( )
      {
         /* Using cursor T004L7 */
         pr_default.execute(4, new Object[] {A1837GpoObjCtrlResponsavel_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound202 = 1;
            A1838GpoObjCtrlResponsavel_GpoCod = T004L7_A1838GpoObjCtrlResponsavel_GpoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1838GpoObjCtrlResponsavel_GpoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1838GpoObjCtrlResponsavel_GpoCod), 4, 0)));
            n1838GpoObjCtrlResponsavel_GpoCod = T004L7_n1838GpoObjCtrlResponsavel_GpoCod[0];
            A1834GpoObjCtrlResponsavel_CteCteCod = T004L7_A1834GpoObjCtrlResponsavel_CteCteCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1834GpoObjCtrlResponsavel_CteCteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1834GpoObjCtrlResponsavel_CteCteCod), 6, 0)));
            n1834GpoObjCtrlResponsavel_CteCteCod = T004L7_n1834GpoObjCtrlResponsavel_CteCteCod[0];
            A1835GpoObjCtrlResponsavel_CteUsrCod = T004L7_A1835GpoObjCtrlResponsavel_CteUsrCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1835GpoObjCtrlResponsavel_CteUsrCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1835GpoObjCtrlResponsavel_CteUsrCod), 6, 0)));
            n1835GpoObjCtrlResponsavel_CteUsrCod = T004L7_n1835GpoObjCtrlResponsavel_CteUsrCod[0];
            ZM4L202( -1) ;
         }
         pr_default.close(4);
         OnLoadActions4L202( ) ;
      }

      protected void OnLoadActions4L202( )
      {
         /* Using cursor T004L5 */
         pr_default.execute(2, new Object[] {n1834GpoObjCtrlResponsavel_CteCteCod, A1834GpoObjCtrlResponsavel_CteCteCod});
         if ( (pr_default.getStatus(2) != 101) )
         {
            A1836GpoObjCtrlResponsavel_CteAreaCod = T004L5_A1836GpoObjCtrlResponsavel_CteAreaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1836GpoObjCtrlResponsavel_CteAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1836GpoObjCtrlResponsavel_CteAreaCod), 6, 0)));
            n1836GpoObjCtrlResponsavel_CteAreaCod = T004L5_n1836GpoObjCtrlResponsavel_CteAreaCod[0];
         }
         else
         {
            A1836GpoObjCtrlResponsavel_CteAreaCod = 0;
            n1836GpoObjCtrlResponsavel_CteAreaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1836GpoObjCtrlResponsavel_CteAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1836GpoObjCtrlResponsavel_CteAreaCod), 6, 0)));
         }
         pr_default.close(2);
      }

      protected void CheckExtendedTable4L202( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T004L5 */
         pr_default.execute(2, new Object[] {n1834GpoObjCtrlResponsavel_CteCteCod, A1834GpoObjCtrlResponsavel_CteCteCod});
         if ( (pr_default.getStatus(2) != 101) )
         {
            A1836GpoObjCtrlResponsavel_CteAreaCod = T004L5_A1836GpoObjCtrlResponsavel_CteAreaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1836GpoObjCtrlResponsavel_CteAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1836GpoObjCtrlResponsavel_CteAreaCod), 6, 0)));
            n1836GpoObjCtrlResponsavel_CteAreaCod = T004L5_n1836GpoObjCtrlResponsavel_CteAreaCod[0];
         }
         else
         {
            A1836GpoObjCtrlResponsavel_CteAreaCod = 0;
            n1836GpoObjCtrlResponsavel_CteAreaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1836GpoObjCtrlResponsavel_CteAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1836GpoObjCtrlResponsavel_CteAreaCod), 6, 0)));
         }
         pr_default.close(2);
         /* Using cursor T004L6 */
         pr_default.execute(3, new Object[] {n1834GpoObjCtrlResponsavel_CteCteCod, A1834GpoObjCtrlResponsavel_CteCteCod, n1835GpoObjCtrlResponsavel_CteUsrCod, A1835GpoObjCtrlResponsavel_CteUsrCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A1834GpoObjCtrlResponsavel_CteCteCod) || (0==A1835GpoObjCtrlResponsavel_CteUsrCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Gpo Obj Ctrl Responsavel_Contratante Usuario'.", "ForeignKeyNotFound", 1, "GPOOBJCTRLRESPONSAVEL_CTECTECOD");
               AnyError = 1;
               GX_FocusControl = edtGpoObjCtrlResponsavel_CteCteCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors4L202( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_2( int A1834GpoObjCtrlResponsavel_CteCteCod )
      {
         /* Using cursor T004L9 */
         pr_default.execute(5, new Object[] {n1834GpoObjCtrlResponsavel_CteCteCod, A1834GpoObjCtrlResponsavel_CteCteCod});
         if ( (pr_default.getStatus(5) != 101) )
         {
            A1836GpoObjCtrlResponsavel_CteAreaCod = T004L9_A1836GpoObjCtrlResponsavel_CteAreaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1836GpoObjCtrlResponsavel_CteAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1836GpoObjCtrlResponsavel_CteAreaCod), 6, 0)));
            n1836GpoObjCtrlResponsavel_CteAreaCod = T004L9_n1836GpoObjCtrlResponsavel_CteAreaCod[0];
         }
         else
         {
            A1836GpoObjCtrlResponsavel_CteAreaCod = 0;
            n1836GpoObjCtrlResponsavel_CteAreaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1836GpoObjCtrlResponsavel_CteAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1836GpoObjCtrlResponsavel_CteAreaCod), 6, 0)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1836GpoObjCtrlResponsavel_CteAreaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_3( int A1834GpoObjCtrlResponsavel_CteCteCod ,
                               int A1835GpoObjCtrlResponsavel_CteUsrCod )
      {
         /* Using cursor T004L10 */
         pr_default.execute(6, new Object[] {n1834GpoObjCtrlResponsavel_CteCteCod, A1834GpoObjCtrlResponsavel_CteCteCod, n1835GpoObjCtrlResponsavel_CteUsrCod, A1835GpoObjCtrlResponsavel_CteUsrCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( (0==A1834GpoObjCtrlResponsavel_CteCteCod) || (0==A1835GpoObjCtrlResponsavel_CteUsrCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Gpo Obj Ctrl Responsavel_Contratante Usuario'.", "ForeignKeyNotFound", 1, "GPOOBJCTRLRESPONSAVEL_CTECTECOD");
               AnyError = 1;
               GX_FocusControl = edtGpoObjCtrlResponsavel_CteCteCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey4L202( )
      {
         /* Using cursor T004L11 */
         pr_default.execute(7, new Object[] {A1837GpoObjCtrlResponsavel_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound202 = 1;
         }
         else
         {
            RcdFound202 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T004L3 */
         pr_default.execute(1, new Object[] {A1837GpoObjCtrlResponsavel_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4L202( 1) ;
            RcdFound202 = 1;
            A1837GpoObjCtrlResponsavel_Codigo = T004L3_A1837GpoObjCtrlResponsavel_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1837GpoObjCtrlResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1837GpoObjCtrlResponsavel_Codigo), 6, 0)));
            A1838GpoObjCtrlResponsavel_GpoCod = T004L3_A1838GpoObjCtrlResponsavel_GpoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1838GpoObjCtrlResponsavel_GpoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1838GpoObjCtrlResponsavel_GpoCod), 4, 0)));
            n1838GpoObjCtrlResponsavel_GpoCod = T004L3_n1838GpoObjCtrlResponsavel_GpoCod[0];
            A1834GpoObjCtrlResponsavel_CteCteCod = T004L3_A1834GpoObjCtrlResponsavel_CteCteCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1834GpoObjCtrlResponsavel_CteCteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1834GpoObjCtrlResponsavel_CteCteCod), 6, 0)));
            n1834GpoObjCtrlResponsavel_CteCteCod = T004L3_n1834GpoObjCtrlResponsavel_CteCteCod[0];
            A1835GpoObjCtrlResponsavel_CteUsrCod = T004L3_A1835GpoObjCtrlResponsavel_CteUsrCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1835GpoObjCtrlResponsavel_CteUsrCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1835GpoObjCtrlResponsavel_CteUsrCod), 6, 0)));
            n1835GpoObjCtrlResponsavel_CteUsrCod = T004L3_n1835GpoObjCtrlResponsavel_CteUsrCod[0];
            Z1837GpoObjCtrlResponsavel_Codigo = A1837GpoObjCtrlResponsavel_Codigo;
            sMode202 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load4L202( ) ;
            if ( AnyError == 1 )
            {
               RcdFound202 = 0;
               InitializeNonKey4L202( ) ;
            }
            Gx_mode = sMode202;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound202 = 0;
            InitializeNonKey4L202( ) ;
            sMode202 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode202;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4L202( ) ;
         if ( RcdFound202 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound202 = 0;
         /* Using cursor T004L12 */
         pr_default.execute(8, new Object[] {A1837GpoObjCtrlResponsavel_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T004L12_A1837GpoObjCtrlResponsavel_Codigo[0] < A1837GpoObjCtrlResponsavel_Codigo ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T004L12_A1837GpoObjCtrlResponsavel_Codigo[0] > A1837GpoObjCtrlResponsavel_Codigo ) ) )
            {
               A1837GpoObjCtrlResponsavel_Codigo = T004L12_A1837GpoObjCtrlResponsavel_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1837GpoObjCtrlResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1837GpoObjCtrlResponsavel_Codigo), 6, 0)));
               RcdFound202 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound202 = 0;
         /* Using cursor T004L13 */
         pr_default.execute(9, new Object[] {A1837GpoObjCtrlResponsavel_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T004L13_A1837GpoObjCtrlResponsavel_Codigo[0] > A1837GpoObjCtrlResponsavel_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T004L13_A1837GpoObjCtrlResponsavel_Codigo[0] < A1837GpoObjCtrlResponsavel_Codigo ) ) )
            {
               A1837GpoObjCtrlResponsavel_Codigo = T004L13_A1837GpoObjCtrlResponsavel_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1837GpoObjCtrlResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1837GpoObjCtrlResponsavel_Codigo), 6, 0)));
               RcdFound202 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey4L202( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtGpoObjCtrlResponsavel_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert4L202( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound202 == 1 )
            {
               if ( A1837GpoObjCtrlResponsavel_Codigo != Z1837GpoObjCtrlResponsavel_Codigo )
               {
                  A1837GpoObjCtrlResponsavel_Codigo = Z1837GpoObjCtrlResponsavel_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1837GpoObjCtrlResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1837GpoObjCtrlResponsavel_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "GPOOBJCTRLRESPONSAVEL_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtGpoObjCtrlResponsavel_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtGpoObjCtrlResponsavel_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update4L202( ) ;
                  GX_FocusControl = edtGpoObjCtrlResponsavel_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1837GpoObjCtrlResponsavel_Codigo != Z1837GpoObjCtrlResponsavel_Codigo )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtGpoObjCtrlResponsavel_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert4L202( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "GPOOBJCTRLRESPONSAVEL_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtGpoObjCtrlResponsavel_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtGpoObjCtrlResponsavel_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert4L202( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A1837GpoObjCtrlResponsavel_Codigo != Z1837GpoObjCtrlResponsavel_Codigo )
         {
            A1837GpoObjCtrlResponsavel_Codigo = Z1837GpoObjCtrlResponsavel_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1837GpoObjCtrlResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1837GpoObjCtrlResponsavel_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "GPOOBJCTRLRESPONSAVEL_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtGpoObjCtrlResponsavel_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtGpoObjCtrlResponsavel_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound202 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "GPOOBJCTRLRESPONSAVEL_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtGpoObjCtrlResponsavel_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtGpoObjCtrlResponsavel_GpoCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart4L202( ) ;
         if ( RcdFound202 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtGpoObjCtrlResponsavel_GpoCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd4L202( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound202 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtGpoObjCtrlResponsavel_GpoCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound202 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtGpoObjCtrlResponsavel_GpoCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart4L202( ) ;
         if ( RcdFound202 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound202 != 0 )
            {
               ScanNext4L202( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtGpoObjCtrlResponsavel_GpoCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd4L202( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency4L202( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T004L2 */
            pr_default.execute(0, new Object[] {A1837GpoObjCtrlResponsavel_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"GpoObjCtrlResponsavel"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1838GpoObjCtrlResponsavel_GpoCod != T004L2_A1838GpoObjCtrlResponsavel_GpoCod[0] ) || ( Z1834GpoObjCtrlResponsavel_CteCteCod != T004L2_A1834GpoObjCtrlResponsavel_CteCteCod[0] ) || ( Z1835GpoObjCtrlResponsavel_CteUsrCod != T004L2_A1835GpoObjCtrlResponsavel_CteUsrCod[0] ) )
            {
               if ( Z1838GpoObjCtrlResponsavel_GpoCod != T004L2_A1838GpoObjCtrlResponsavel_GpoCod[0] )
               {
                  GXUtil.WriteLog("gpoobjctrlresponsavel:[seudo value changed for attri]"+"GpoObjCtrlResponsavel_GpoCod");
                  GXUtil.WriteLogRaw("Old: ",Z1838GpoObjCtrlResponsavel_GpoCod);
                  GXUtil.WriteLogRaw("Current: ",T004L2_A1838GpoObjCtrlResponsavel_GpoCod[0]);
               }
               if ( Z1834GpoObjCtrlResponsavel_CteCteCod != T004L2_A1834GpoObjCtrlResponsavel_CteCteCod[0] )
               {
                  GXUtil.WriteLog("gpoobjctrlresponsavel:[seudo value changed for attri]"+"GpoObjCtrlResponsavel_CteCteCod");
                  GXUtil.WriteLogRaw("Old: ",Z1834GpoObjCtrlResponsavel_CteCteCod);
                  GXUtil.WriteLogRaw("Current: ",T004L2_A1834GpoObjCtrlResponsavel_CteCteCod[0]);
               }
               if ( Z1835GpoObjCtrlResponsavel_CteUsrCod != T004L2_A1835GpoObjCtrlResponsavel_CteUsrCod[0] )
               {
                  GXUtil.WriteLog("gpoobjctrlresponsavel:[seudo value changed for attri]"+"GpoObjCtrlResponsavel_CteUsrCod");
                  GXUtil.WriteLogRaw("Old: ",Z1835GpoObjCtrlResponsavel_CteUsrCod);
                  GXUtil.WriteLogRaw("Current: ",T004L2_A1835GpoObjCtrlResponsavel_CteUsrCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"GpoObjCtrlResponsavel"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4L202( )
      {
         BeforeValidate4L202( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4L202( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4L202( 0) ;
            CheckOptimisticConcurrency4L202( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4L202( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4L202( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004L14 */
                     pr_default.execute(10, new Object[] {n1838GpoObjCtrlResponsavel_GpoCod, A1838GpoObjCtrlResponsavel_GpoCod, n1834GpoObjCtrlResponsavel_CteCteCod, A1834GpoObjCtrlResponsavel_CteCteCod, n1835GpoObjCtrlResponsavel_CteUsrCod, A1835GpoObjCtrlResponsavel_CteUsrCod});
                     A1837GpoObjCtrlResponsavel_Codigo = T004L14_A1837GpoObjCtrlResponsavel_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1837GpoObjCtrlResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1837GpoObjCtrlResponsavel_Codigo), 6, 0)));
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("GpoObjCtrlResponsavel") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption4L0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4L202( ) ;
            }
            EndLevel4L202( ) ;
         }
         CloseExtendedTableCursors4L202( ) ;
      }

      protected void Update4L202( )
      {
         BeforeValidate4L202( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4L202( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4L202( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4L202( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4L202( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004L15 */
                     pr_default.execute(11, new Object[] {n1838GpoObjCtrlResponsavel_GpoCod, A1838GpoObjCtrlResponsavel_GpoCod, n1834GpoObjCtrlResponsavel_CteCteCod, A1834GpoObjCtrlResponsavel_CteCteCod, n1835GpoObjCtrlResponsavel_CteUsrCod, A1835GpoObjCtrlResponsavel_CteUsrCod, A1837GpoObjCtrlResponsavel_Codigo});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("GpoObjCtrlResponsavel") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"GpoObjCtrlResponsavel"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate4L202( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption4L0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4L202( ) ;
         }
         CloseExtendedTableCursors4L202( ) ;
      }

      protected void DeferredUpdate4L202( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate4L202( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4L202( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4L202( ) ;
            AfterConfirm4L202( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4L202( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T004L16 */
                  pr_default.execute(12, new Object[] {A1837GpoObjCtrlResponsavel_Codigo});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("GpoObjCtrlResponsavel") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound202 == 0 )
                        {
                           InitAll4L202( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption4L0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode202 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel4L202( ) ;
         Gx_mode = sMode202;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls4L202( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T004L18 */
            pr_default.execute(13, new Object[] {n1834GpoObjCtrlResponsavel_CteCteCod, A1834GpoObjCtrlResponsavel_CteCteCod});
            if ( (pr_default.getStatus(13) != 101) )
            {
               A1836GpoObjCtrlResponsavel_CteAreaCod = T004L18_A1836GpoObjCtrlResponsavel_CteAreaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1836GpoObjCtrlResponsavel_CteAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1836GpoObjCtrlResponsavel_CteAreaCod), 6, 0)));
               n1836GpoObjCtrlResponsavel_CteAreaCod = T004L18_n1836GpoObjCtrlResponsavel_CteAreaCod[0];
            }
            else
            {
               A1836GpoObjCtrlResponsavel_CteAreaCod = 0;
               n1836GpoObjCtrlResponsavel_CteAreaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1836GpoObjCtrlResponsavel_CteAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1836GpoObjCtrlResponsavel_CteAreaCod), 6, 0)));
            }
            pr_default.close(13);
         }
      }

      protected void EndLevel4L202( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4L202( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(13);
            context.CommitDataStores( "GpoObjCtrlResponsavel");
            if ( AnyError == 0 )
            {
               ConfirmValues4L0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(13);
            context.RollbackDataStores( "GpoObjCtrlResponsavel");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart4L202( )
      {
         /* Using cursor T004L19 */
         pr_default.execute(14);
         RcdFound202 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound202 = 1;
            A1837GpoObjCtrlResponsavel_Codigo = T004L19_A1837GpoObjCtrlResponsavel_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1837GpoObjCtrlResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1837GpoObjCtrlResponsavel_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext4L202( )
      {
         /* Scan next routine */
         pr_default.readNext(14);
         RcdFound202 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound202 = 1;
            A1837GpoObjCtrlResponsavel_Codigo = T004L19_A1837GpoObjCtrlResponsavel_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1837GpoObjCtrlResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1837GpoObjCtrlResponsavel_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd4L202( )
      {
         pr_default.close(14);
      }

      protected void AfterConfirm4L202( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4L202( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4L202( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4L202( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4L202( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4L202( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4L202( )
      {
         edtGpoObjCtrlResponsavel_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGpoObjCtrlResponsavel_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGpoObjCtrlResponsavel_Codigo_Enabled), 5, 0)));
         edtGpoObjCtrlResponsavel_GpoCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGpoObjCtrlResponsavel_GpoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGpoObjCtrlResponsavel_GpoCod_Enabled), 5, 0)));
         edtGpoObjCtrlResponsavel_CteCteCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGpoObjCtrlResponsavel_CteCteCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGpoObjCtrlResponsavel_CteCteCod_Enabled), 5, 0)));
         edtGpoObjCtrlResponsavel_CteUsrCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGpoObjCtrlResponsavel_CteUsrCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGpoObjCtrlResponsavel_CteUsrCod_Enabled), 5, 0)));
         edtGpoObjCtrlResponsavel_CteAreaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGpoObjCtrlResponsavel_CteAreaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGpoObjCtrlResponsavel_CteAreaCod_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues4L0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282392158");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("gpoobjctrlresponsavel.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1837GpoObjCtrlResponsavel_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1837GpoObjCtrlResponsavel_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1838GpoObjCtrlResponsavel_GpoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1838GpoObjCtrlResponsavel_GpoCod), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1834GpoObjCtrlResponsavel_CteCteCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1834GpoObjCtrlResponsavel_CteCteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1835GpoObjCtrlResponsavel_CteUsrCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1835GpoObjCtrlResponsavel_CteUsrCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("gpoobjctrlresponsavel.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "GpoObjCtrlResponsavel" ;
      }

      public override String GetPgmdesc( )
      {
         return "Gpo Obj Ctrl Responsavel" ;
      }

      protected void InitializeNonKey4L202( )
      {
         A1838GpoObjCtrlResponsavel_GpoCod = 0;
         n1838GpoObjCtrlResponsavel_GpoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1838GpoObjCtrlResponsavel_GpoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1838GpoObjCtrlResponsavel_GpoCod), 4, 0)));
         n1838GpoObjCtrlResponsavel_GpoCod = ((0==A1838GpoObjCtrlResponsavel_GpoCod) ? true : false);
         A1834GpoObjCtrlResponsavel_CteCteCod = 0;
         n1834GpoObjCtrlResponsavel_CteCteCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1834GpoObjCtrlResponsavel_CteCteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1834GpoObjCtrlResponsavel_CteCteCod), 6, 0)));
         n1834GpoObjCtrlResponsavel_CteCteCod = ((0==A1834GpoObjCtrlResponsavel_CteCteCod) ? true : false);
         A1835GpoObjCtrlResponsavel_CteUsrCod = 0;
         n1835GpoObjCtrlResponsavel_CteUsrCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1835GpoObjCtrlResponsavel_CteUsrCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1835GpoObjCtrlResponsavel_CteUsrCod), 6, 0)));
         n1835GpoObjCtrlResponsavel_CteUsrCod = ((0==A1835GpoObjCtrlResponsavel_CteUsrCod) ? true : false);
         A1836GpoObjCtrlResponsavel_CteAreaCod = 0;
         n1836GpoObjCtrlResponsavel_CteAreaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1836GpoObjCtrlResponsavel_CteAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1836GpoObjCtrlResponsavel_CteAreaCod), 6, 0)));
         Z1838GpoObjCtrlResponsavel_GpoCod = 0;
         Z1834GpoObjCtrlResponsavel_CteCteCod = 0;
         Z1835GpoObjCtrlResponsavel_CteUsrCod = 0;
      }

      protected void InitAll4L202( )
      {
         A1837GpoObjCtrlResponsavel_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1837GpoObjCtrlResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1837GpoObjCtrlResponsavel_Codigo), 6, 0)));
         InitializeNonKey4L202( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282392162");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gpoobjctrlresponsavel.js", "?20204282392162");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockgpoobjctrlresponsavel_codigo_Internalname = "TEXTBLOCKGPOOBJCTRLRESPONSAVEL_CODIGO";
         edtGpoObjCtrlResponsavel_Codigo_Internalname = "GPOOBJCTRLRESPONSAVEL_CODIGO";
         lblTextblockgpoobjctrlresponsavel_gpocod_Internalname = "TEXTBLOCKGPOOBJCTRLRESPONSAVEL_GPOCOD";
         edtGpoObjCtrlResponsavel_GpoCod_Internalname = "GPOOBJCTRLRESPONSAVEL_GPOCOD";
         lblTextblockgpoobjctrlresponsavel_ctectecod_Internalname = "TEXTBLOCKGPOOBJCTRLRESPONSAVEL_CTECTECOD";
         edtGpoObjCtrlResponsavel_CteCteCod_Internalname = "GPOOBJCTRLRESPONSAVEL_CTECTECOD";
         lblTextblockgpoobjctrlresponsavel_cteusrcod_Internalname = "TEXTBLOCKGPOOBJCTRLRESPONSAVEL_CTEUSRCOD";
         edtGpoObjCtrlResponsavel_CteUsrCod_Internalname = "GPOOBJCTRLRESPONSAVEL_CTEUSRCOD";
         lblTextblockgpoobjctrlresponsavel_cteareacod_Internalname = "TEXTBLOCKGPOOBJCTRLRESPONSAVEL_CTEAREACOD";
         edtGpoObjCtrlResponsavel_CteAreaCod_Internalname = "GPOOBJCTRLRESPONSAVEL_CTEAREACOD";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Gpo Obj Ctrl Responsavel";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtGpoObjCtrlResponsavel_CteAreaCod_Jsonclick = "";
         edtGpoObjCtrlResponsavel_CteAreaCod_Enabled = 0;
         edtGpoObjCtrlResponsavel_CteUsrCod_Jsonclick = "";
         edtGpoObjCtrlResponsavel_CteUsrCod_Enabled = 1;
         edtGpoObjCtrlResponsavel_CteCteCod_Jsonclick = "";
         edtGpoObjCtrlResponsavel_CteCteCod_Enabled = 1;
         edtGpoObjCtrlResponsavel_GpoCod_Jsonclick = "";
         edtGpoObjCtrlResponsavel_GpoCod_Enabled = 1;
         edtGpoObjCtrlResponsavel_Codigo_Jsonclick = "";
         edtGpoObjCtrlResponsavel_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtGpoObjCtrlResponsavel_GpoCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Gpoobjctrlresponsavel_codigo( int GX_Parm1 ,
                                                      short GX_Parm2 ,
                                                      int GX_Parm3 ,
                                                      int GX_Parm4 )
      {
         A1837GpoObjCtrlResponsavel_Codigo = GX_Parm1;
         A1838GpoObjCtrlResponsavel_GpoCod = GX_Parm2;
         n1838GpoObjCtrlResponsavel_GpoCod = false;
         A1834GpoObjCtrlResponsavel_CteCteCod = GX_Parm3;
         n1834GpoObjCtrlResponsavel_CteCteCod = false;
         A1835GpoObjCtrlResponsavel_CteUsrCod = GX_Parm4;
         n1835GpoObjCtrlResponsavel_CteUsrCod = false;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1836GpoObjCtrlResponsavel_CteAreaCod = 0;
            n1836GpoObjCtrlResponsavel_CteAreaCod = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1838GpoObjCtrlResponsavel_GpoCod), 4, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1834GpoObjCtrlResponsavel_CteCteCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1835GpoObjCtrlResponsavel_CteUsrCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1836GpoObjCtrlResponsavel_CteAreaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1837GpoObjCtrlResponsavel_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1838GpoObjCtrlResponsavel_GpoCod), 4, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1834GpoObjCtrlResponsavel_CteCteCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1835GpoObjCtrlResponsavel_CteUsrCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1836GpoObjCtrlResponsavel_CteAreaCod), 6, 0, ".", "")));
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Gpoobjctrlresponsavel_ctectecod( int GX_Parm1 ,
                                                         int GX_Parm2 )
      {
         A1834GpoObjCtrlResponsavel_CteCteCod = GX_Parm1;
         n1834GpoObjCtrlResponsavel_CteCteCod = false;
         A1836GpoObjCtrlResponsavel_CteAreaCod = GX_Parm2;
         n1836GpoObjCtrlResponsavel_CteAreaCod = false;
         /* Using cursor T004L18 */
         pr_default.execute(13, new Object[] {n1834GpoObjCtrlResponsavel_CteCteCod, A1834GpoObjCtrlResponsavel_CteCteCod});
         if ( (pr_default.getStatus(13) != 101) )
         {
            A1836GpoObjCtrlResponsavel_CteAreaCod = T004L18_A1836GpoObjCtrlResponsavel_CteAreaCod[0];
            n1836GpoObjCtrlResponsavel_CteAreaCod = T004L18_n1836GpoObjCtrlResponsavel_CteAreaCod[0];
         }
         else
         {
            A1836GpoObjCtrlResponsavel_CteAreaCod = 0;
            n1836GpoObjCtrlResponsavel_CteAreaCod = false;
         }
         pr_default.close(13);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1836GpoObjCtrlResponsavel_CteAreaCod = 0;
            n1836GpoObjCtrlResponsavel_CteAreaCod = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1836GpoObjCtrlResponsavel_CteAreaCod), 6, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Gpoobjctrlresponsavel_cteusrcod( int GX_Parm1 ,
                                                         int GX_Parm2 )
      {
         A1834GpoObjCtrlResponsavel_CteCteCod = GX_Parm1;
         n1834GpoObjCtrlResponsavel_CteCteCod = false;
         A1835GpoObjCtrlResponsavel_CteUsrCod = GX_Parm2;
         n1835GpoObjCtrlResponsavel_CteUsrCod = false;
         /* Using cursor T004L20 */
         pr_default.execute(15, new Object[] {n1834GpoObjCtrlResponsavel_CteCteCod, A1834GpoObjCtrlResponsavel_CteCteCod, n1835GpoObjCtrlResponsavel_CteUsrCod, A1835GpoObjCtrlResponsavel_CteUsrCod});
         if ( (pr_default.getStatus(15) == 101) )
         {
            if ( ! ( (0==A1834GpoObjCtrlResponsavel_CteCteCod) || (0==A1835GpoObjCtrlResponsavel_CteUsrCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Gpo Obj Ctrl Responsavel_Contratante Usuario'.", "ForeignKeyNotFound", 1, "GPOOBJCTRLRESPONSAVEL_CTECTECOD");
               AnyError = 1;
               GX_FocusControl = edtGpoObjCtrlResponsavel_CteCteCod_Internalname;
            }
         }
         pr_default.close(15);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(15);
         pr_default.close(13);
      }

      public override void initialize( )
      {
         sPrefix = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockgpoobjctrlresponsavel_codigo_Jsonclick = "";
         lblTextblockgpoobjctrlresponsavel_gpocod_Jsonclick = "";
         lblTextblockgpoobjctrlresponsavel_ctectecod_Jsonclick = "";
         lblTextblockgpoobjctrlresponsavel_cteusrcod_Jsonclick = "";
         lblTextblockgpoobjctrlresponsavel_cteareacod_Jsonclick = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         T004L7_A1837GpoObjCtrlResponsavel_Codigo = new int[1] ;
         T004L7_A1838GpoObjCtrlResponsavel_GpoCod = new short[1] ;
         T004L7_n1838GpoObjCtrlResponsavel_GpoCod = new bool[] {false} ;
         T004L7_A1834GpoObjCtrlResponsavel_CteCteCod = new int[1] ;
         T004L7_n1834GpoObjCtrlResponsavel_CteCteCod = new bool[] {false} ;
         T004L7_A1835GpoObjCtrlResponsavel_CteUsrCod = new int[1] ;
         T004L7_n1835GpoObjCtrlResponsavel_CteUsrCod = new bool[] {false} ;
         T004L5_A1836GpoObjCtrlResponsavel_CteAreaCod = new int[1] ;
         T004L5_n1836GpoObjCtrlResponsavel_CteAreaCod = new bool[] {false} ;
         T004L6_A1834GpoObjCtrlResponsavel_CteCteCod = new int[1] ;
         T004L6_n1834GpoObjCtrlResponsavel_CteCteCod = new bool[] {false} ;
         T004L9_A1836GpoObjCtrlResponsavel_CteAreaCod = new int[1] ;
         T004L9_n1836GpoObjCtrlResponsavel_CteAreaCod = new bool[] {false} ;
         T004L10_A1834GpoObjCtrlResponsavel_CteCteCod = new int[1] ;
         T004L10_n1834GpoObjCtrlResponsavel_CteCteCod = new bool[] {false} ;
         T004L11_A1837GpoObjCtrlResponsavel_Codigo = new int[1] ;
         T004L3_A1837GpoObjCtrlResponsavel_Codigo = new int[1] ;
         T004L3_A1838GpoObjCtrlResponsavel_GpoCod = new short[1] ;
         T004L3_n1838GpoObjCtrlResponsavel_GpoCod = new bool[] {false} ;
         T004L3_A1834GpoObjCtrlResponsavel_CteCteCod = new int[1] ;
         T004L3_n1834GpoObjCtrlResponsavel_CteCteCod = new bool[] {false} ;
         T004L3_A1835GpoObjCtrlResponsavel_CteUsrCod = new int[1] ;
         T004L3_n1835GpoObjCtrlResponsavel_CteUsrCod = new bool[] {false} ;
         sMode202 = "";
         T004L12_A1837GpoObjCtrlResponsavel_Codigo = new int[1] ;
         T004L13_A1837GpoObjCtrlResponsavel_Codigo = new int[1] ;
         T004L2_A1837GpoObjCtrlResponsavel_Codigo = new int[1] ;
         T004L2_A1838GpoObjCtrlResponsavel_GpoCod = new short[1] ;
         T004L2_n1838GpoObjCtrlResponsavel_GpoCod = new bool[] {false} ;
         T004L2_A1834GpoObjCtrlResponsavel_CteCteCod = new int[1] ;
         T004L2_n1834GpoObjCtrlResponsavel_CteCteCod = new bool[] {false} ;
         T004L2_A1835GpoObjCtrlResponsavel_CteUsrCod = new int[1] ;
         T004L2_n1835GpoObjCtrlResponsavel_CteUsrCod = new bool[] {false} ;
         T004L14_A1837GpoObjCtrlResponsavel_Codigo = new int[1] ;
         T004L18_A1836GpoObjCtrlResponsavel_CteAreaCod = new int[1] ;
         T004L18_n1836GpoObjCtrlResponsavel_CteAreaCod = new bool[] {false} ;
         T004L19_A1837GpoObjCtrlResponsavel_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         T004L20_A1834GpoObjCtrlResponsavel_CteCteCod = new int[1] ;
         T004L20_n1834GpoObjCtrlResponsavel_CteCteCod = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.gpoobjctrlresponsavel__default(),
            new Object[][] {
                new Object[] {
               T004L2_A1837GpoObjCtrlResponsavel_Codigo, T004L2_A1838GpoObjCtrlResponsavel_GpoCod, T004L2_n1838GpoObjCtrlResponsavel_GpoCod, T004L2_A1834GpoObjCtrlResponsavel_CteCteCod, T004L2_n1834GpoObjCtrlResponsavel_CteCteCod, T004L2_A1835GpoObjCtrlResponsavel_CteUsrCod, T004L2_n1835GpoObjCtrlResponsavel_CteUsrCod
               }
               , new Object[] {
               T004L3_A1837GpoObjCtrlResponsavel_Codigo, T004L3_A1838GpoObjCtrlResponsavel_GpoCod, T004L3_n1838GpoObjCtrlResponsavel_GpoCod, T004L3_A1834GpoObjCtrlResponsavel_CteCteCod, T004L3_n1834GpoObjCtrlResponsavel_CteCteCod, T004L3_A1835GpoObjCtrlResponsavel_CteUsrCod, T004L3_n1835GpoObjCtrlResponsavel_CteUsrCod
               }
               , new Object[] {
               T004L5_A1836GpoObjCtrlResponsavel_CteAreaCod, T004L5_n1836GpoObjCtrlResponsavel_CteAreaCod
               }
               , new Object[] {
               T004L6_A1834GpoObjCtrlResponsavel_CteCteCod
               }
               , new Object[] {
               T004L7_A1837GpoObjCtrlResponsavel_Codigo, T004L7_A1838GpoObjCtrlResponsavel_GpoCod, T004L7_n1838GpoObjCtrlResponsavel_GpoCod, T004L7_A1834GpoObjCtrlResponsavel_CteCteCod, T004L7_n1834GpoObjCtrlResponsavel_CteCteCod, T004L7_A1835GpoObjCtrlResponsavel_CteUsrCod, T004L7_n1835GpoObjCtrlResponsavel_CteUsrCod
               }
               , new Object[] {
               T004L9_A1836GpoObjCtrlResponsavel_CteAreaCod, T004L9_n1836GpoObjCtrlResponsavel_CteAreaCod
               }
               , new Object[] {
               T004L10_A1834GpoObjCtrlResponsavel_CteCteCod
               }
               , new Object[] {
               T004L11_A1837GpoObjCtrlResponsavel_Codigo
               }
               , new Object[] {
               T004L12_A1837GpoObjCtrlResponsavel_Codigo
               }
               , new Object[] {
               T004L13_A1837GpoObjCtrlResponsavel_Codigo
               }
               , new Object[] {
               T004L14_A1837GpoObjCtrlResponsavel_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T004L18_A1836GpoObjCtrlResponsavel_CteAreaCod, T004L18_n1836GpoObjCtrlResponsavel_CteAreaCod
               }
               , new Object[] {
               T004L19_A1837GpoObjCtrlResponsavel_Codigo
               }
               , new Object[] {
               T004L20_A1834GpoObjCtrlResponsavel_CteCteCod
               }
            }
         );
      }

      private short Z1838GpoObjCtrlResponsavel_GpoCod ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A1838GpoObjCtrlResponsavel_GpoCod ;
      private short GX_JID ;
      private short RcdFound202 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z1837GpoObjCtrlResponsavel_Codigo ;
      private int Z1834GpoObjCtrlResponsavel_CteCteCod ;
      private int Z1835GpoObjCtrlResponsavel_CteUsrCod ;
      private int A1834GpoObjCtrlResponsavel_CteCteCod ;
      private int A1835GpoObjCtrlResponsavel_CteUsrCod ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int A1837GpoObjCtrlResponsavel_Codigo ;
      private int edtGpoObjCtrlResponsavel_Codigo_Enabled ;
      private int edtGpoObjCtrlResponsavel_GpoCod_Enabled ;
      private int edtGpoObjCtrlResponsavel_CteCteCod_Enabled ;
      private int edtGpoObjCtrlResponsavel_CteUsrCod_Enabled ;
      private int A1836GpoObjCtrlResponsavel_CteAreaCod ;
      private int edtGpoObjCtrlResponsavel_CteAreaCod_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private int Z1836GpoObjCtrlResponsavel_CteAreaCod ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtGpoObjCtrlResponsavel_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockgpoobjctrlresponsavel_codigo_Internalname ;
      private String lblTextblockgpoobjctrlresponsavel_codigo_Jsonclick ;
      private String edtGpoObjCtrlResponsavel_Codigo_Jsonclick ;
      private String lblTextblockgpoobjctrlresponsavel_gpocod_Internalname ;
      private String lblTextblockgpoobjctrlresponsavel_gpocod_Jsonclick ;
      private String edtGpoObjCtrlResponsavel_GpoCod_Internalname ;
      private String edtGpoObjCtrlResponsavel_GpoCod_Jsonclick ;
      private String lblTextblockgpoobjctrlresponsavel_ctectecod_Internalname ;
      private String lblTextblockgpoobjctrlresponsavel_ctectecod_Jsonclick ;
      private String edtGpoObjCtrlResponsavel_CteCteCod_Internalname ;
      private String edtGpoObjCtrlResponsavel_CteCteCod_Jsonclick ;
      private String lblTextblockgpoobjctrlresponsavel_cteusrcod_Internalname ;
      private String lblTextblockgpoobjctrlresponsavel_cteusrcod_Jsonclick ;
      private String edtGpoObjCtrlResponsavel_CteUsrCod_Internalname ;
      private String edtGpoObjCtrlResponsavel_CteUsrCod_Jsonclick ;
      private String lblTextblockgpoobjctrlresponsavel_cteareacod_Internalname ;
      private String lblTextblockgpoobjctrlresponsavel_cteareacod_Jsonclick ;
      private String edtGpoObjCtrlResponsavel_CteAreaCod_Internalname ;
      private String edtGpoObjCtrlResponsavel_CteAreaCod_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode202 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool entryPointCalled ;
      private bool n1834GpoObjCtrlResponsavel_CteCteCod ;
      private bool n1835GpoObjCtrlResponsavel_CteUsrCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1838GpoObjCtrlResponsavel_GpoCod ;
      private bool n1836GpoObjCtrlResponsavel_CteAreaCod ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] T004L7_A1837GpoObjCtrlResponsavel_Codigo ;
      private short[] T004L7_A1838GpoObjCtrlResponsavel_GpoCod ;
      private bool[] T004L7_n1838GpoObjCtrlResponsavel_GpoCod ;
      private int[] T004L7_A1834GpoObjCtrlResponsavel_CteCteCod ;
      private bool[] T004L7_n1834GpoObjCtrlResponsavel_CteCteCod ;
      private int[] T004L7_A1835GpoObjCtrlResponsavel_CteUsrCod ;
      private bool[] T004L7_n1835GpoObjCtrlResponsavel_CteUsrCod ;
      private int[] T004L5_A1836GpoObjCtrlResponsavel_CteAreaCod ;
      private bool[] T004L5_n1836GpoObjCtrlResponsavel_CteAreaCod ;
      private int[] T004L6_A1834GpoObjCtrlResponsavel_CteCteCod ;
      private bool[] T004L6_n1834GpoObjCtrlResponsavel_CteCteCod ;
      private int[] T004L9_A1836GpoObjCtrlResponsavel_CteAreaCod ;
      private bool[] T004L9_n1836GpoObjCtrlResponsavel_CteAreaCod ;
      private int[] T004L10_A1834GpoObjCtrlResponsavel_CteCteCod ;
      private bool[] T004L10_n1834GpoObjCtrlResponsavel_CteCteCod ;
      private int[] T004L11_A1837GpoObjCtrlResponsavel_Codigo ;
      private int[] T004L3_A1837GpoObjCtrlResponsavel_Codigo ;
      private short[] T004L3_A1838GpoObjCtrlResponsavel_GpoCod ;
      private bool[] T004L3_n1838GpoObjCtrlResponsavel_GpoCod ;
      private int[] T004L3_A1834GpoObjCtrlResponsavel_CteCteCod ;
      private bool[] T004L3_n1834GpoObjCtrlResponsavel_CteCteCod ;
      private int[] T004L3_A1835GpoObjCtrlResponsavel_CteUsrCod ;
      private bool[] T004L3_n1835GpoObjCtrlResponsavel_CteUsrCod ;
      private int[] T004L12_A1837GpoObjCtrlResponsavel_Codigo ;
      private int[] T004L13_A1837GpoObjCtrlResponsavel_Codigo ;
      private int[] T004L2_A1837GpoObjCtrlResponsavel_Codigo ;
      private short[] T004L2_A1838GpoObjCtrlResponsavel_GpoCod ;
      private bool[] T004L2_n1838GpoObjCtrlResponsavel_GpoCod ;
      private int[] T004L2_A1834GpoObjCtrlResponsavel_CteCteCod ;
      private bool[] T004L2_n1834GpoObjCtrlResponsavel_CteCteCod ;
      private int[] T004L2_A1835GpoObjCtrlResponsavel_CteUsrCod ;
      private bool[] T004L2_n1835GpoObjCtrlResponsavel_CteUsrCod ;
      private int[] T004L14_A1837GpoObjCtrlResponsavel_Codigo ;
      private int[] T004L18_A1836GpoObjCtrlResponsavel_CteAreaCod ;
      private bool[] T004L18_n1836GpoObjCtrlResponsavel_CteAreaCod ;
      private int[] T004L19_A1837GpoObjCtrlResponsavel_Codigo ;
      private int[] T004L20_A1834GpoObjCtrlResponsavel_CteCteCod ;
      private bool[] T004L20_n1834GpoObjCtrlResponsavel_CteCteCod ;
      private GXWebForm Form ;
   }

   public class gpoobjctrlresponsavel__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT004L7 ;
          prmT004L7 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004L5 ;
          prmT004L5 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_CteCteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004L6 ;
          prmT004L6 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_CteCteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@GpoObjCtrlResponsavel_CteUsrCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004L9 ;
          prmT004L9 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_CteCteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004L10 ;
          prmT004L10 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_CteCteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@GpoObjCtrlResponsavel_CteUsrCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004L11 ;
          prmT004L11 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004L3 ;
          prmT004L3 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004L12 ;
          prmT004L12 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004L13 ;
          prmT004L13 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004L2 ;
          prmT004L2 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004L14 ;
          prmT004L14 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_GpoCod",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@GpoObjCtrlResponsavel_CteCteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@GpoObjCtrlResponsavel_CteUsrCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004L15 ;
          prmT004L15 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_GpoCod",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@GpoObjCtrlResponsavel_CteCteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@GpoObjCtrlResponsavel_CteUsrCod",SqlDbType.Int,6,0} ,
          new Object[] {"@GpoObjCtrlResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004L16 ;
          prmT004L16 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004L19 ;
          prmT004L19 = new Object[] {
          } ;
          Object[] prmT004L18 ;
          prmT004L18 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_CteCteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004L20 ;
          prmT004L20 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_CteCteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@GpoObjCtrlResponsavel_CteUsrCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T004L2", "SELECT [GpoObjCtrlResponsavel_Codigo], [GpoObjCtrlResponsavel_GpoCod], [GpoObjCtrlResponsavel_CteCteCod] AS GpoObjCtrlResponsavel_CteCteCod, [GpoObjCtrlResponsavel_CteUsrCod] AS GpoObjCtrlResponsavel_CteUsrCod FROM [GpoObjCtrlResponsavel] WITH (UPDLOCK) WHERE [GpoObjCtrlResponsavel_Codigo] = @GpoObjCtrlResponsavel_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004L2,1,0,true,false )
             ,new CursorDef("T004L3", "SELECT [GpoObjCtrlResponsavel_Codigo], [GpoObjCtrlResponsavel_GpoCod], [GpoObjCtrlResponsavel_CteCteCod] AS GpoObjCtrlResponsavel_CteCteCod, [GpoObjCtrlResponsavel_CteUsrCod] AS GpoObjCtrlResponsavel_CteUsrCod FROM [GpoObjCtrlResponsavel] WITH (NOLOCK) WHERE [GpoObjCtrlResponsavel_Codigo] = @GpoObjCtrlResponsavel_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004L3,1,0,true,false )
             ,new CursorDef("T004L5", "SELECT COALESCE( T1.[GpoObjCtrlResponsavel_CteAreaCod], 0) AS GpoObjCtrlResponsavel_CteAreaCod FROM (SELECT MIN([AreaTrabalho_Codigo]) AS GpoObjCtrlResponsavel_CteAreaCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [Contratante_Codigo] = @GpoObjCtrlResponsavel_CteCteCod ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmT004L5,1,0,true,false )
             ,new CursorDef("T004L6", "SELECT [ContratanteUsuario_ContratanteCod] AS GpoObjCtrlResponsavel_CteCteCod FROM [ContratanteUsuario] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @GpoObjCtrlResponsavel_CteCteCod AND [ContratanteUsuario_UsuarioCod] = @GpoObjCtrlResponsavel_CteUsrCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004L6,1,0,true,false )
             ,new CursorDef("T004L7", "SELECT TM1.[GpoObjCtrlResponsavel_Codigo], TM1.[GpoObjCtrlResponsavel_GpoCod], TM1.[GpoObjCtrlResponsavel_CteCteCod] AS GpoObjCtrlResponsavel_CteCteCod, TM1.[GpoObjCtrlResponsavel_CteUsrCod] AS GpoObjCtrlResponsavel_CteUsrCod FROM [GpoObjCtrlResponsavel] TM1 WITH (NOLOCK) WHERE TM1.[GpoObjCtrlResponsavel_Codigo] = @GpoObjCtrlResponsavel_Codigo ORDER BY TM1.[GpoObjCtrlResponsavel_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004L7,100,0,true,false )
             ,new CursorDef("T004L9", "SELECT COALESCE( T1.[GpoObjCtrlResponsavel_CteAreaCod], 0) AS GpoObjCtrlResponsavel_CteAreaCod FROM (SELECT MIN([AreaTrabalho_Codigo]) AS GpoObjCtrlResponsavel_CteAreaCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [Contratante_Codigo] = @GpoObjCtrlResponsavel_CteCteCod ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmT004L9,1,0,true,false )
             ,new CursorDef("T004L10", "SELECT [ContratanteUsuario_ContratanteCod] AS GpoObjCtrlResponsavel_CteCteCod FROM [ContratanteUsuario] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @GpoObjCtrlResponsavel_CteCteCod AND [ContratanteUsuario_UsuarioCod] = @GpoObjCtrlResponsavel_CteUsrCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004L10,1,0,true,false )
             ,new CursorDef("T004L11", "SELECT [GpoObjCtrlResponsavel_Codigo] FROM [GpoObjCtrlResponsavel] WITH (NOLOCK) WHERE [GpoObjCtrlResponsavel_Codigo] = @GpoObjCtrlResponsavel_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004L11,1,0,true,false )
             ,new CursorDef("T004L12", "SELECT TOP 1 [GpoObjCtrlResponsavel_Codigo] FROM [GpoObjCtrlResponsavel] WITH (NOLOCK) WHERE ( [GpoObjCtrlResponsavel_Codigo] > @GpoObjCtrlResponsavel_Codigo) ORDER BY [GpoObjCtrlResponsavel_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004L12,1,0,true,true )
             ,new CursorDef("T004L13", "SELECT TOP 1 [GpoObjCtrlResponsavel_Codigo] FROM [GpoObjCtrlResponsavel] WITH (NOLOCK) WHERE ( [GpoObjCtrlResponsavel_Codigo] < @GpoObjCtrlResponsavel_Codigo) ORDER BY [GpoObjCtrlResponsavel_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004L13,1,0,true,true )
             ,new CursorDef("T004L14", "INSERT INTO [GpoObjCtrlResponsavel]([GpoObjCtrlResponsavel_GpoCod], [GpoObjCtrlResponsavel_CteCteCod], [GpoObjCtrlResponsavel_CteUsrCod]) VALUES(@GpoObjCtrlResponsavel_GpoCod, @GpoObjCtrlResponsavel_CteCteCod, @GpoObjCtrlResponsavel_CteUsrCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT004L14)
             ,new CursorDef("T004L15", "UPDATE [GpoObjCtrlResponsavel] SET [GpoObjCtrlResponsavel_GpoCod]=@GpoObjCtrlResponsavel_GpoCod, [GpoObjCtrlResponsavel_CteCteCod]=@GpoObjCtrlResponsavel_CteCteCod, [GpoObjCtrlResponsavel_CteUsrCod]=@GpoObjCtrlResponsavel_CteUsrCod  WHERE [GpoObjCtrlResponsavel_Codigo] = @GpoObjCtrlResponsavel_Codigo", GxErrorMask.GX_NOMASK,prmT004L15)
             ,new CursorDef("T004L16", "DELETE FROM [GpoObjCtrlResponsavel]  WHERE [GpoObjCtrlResponsavel_Codigo] = @GpoObjCtrlResponsavel_Codigo", GxErrorMask.GX_NOMASK,prmT004L16)
             ,new CursorDef("T004L18", "SELECT COALESCE( T1.[GpoObjCtrlResponsavel_CteAreaCod], 0) AS GpoObjCtrlResponsavel_CteAreaCod FROM (SELECT MIN([AreaTrabalho_Codigo]) AS GpoObjCtrlResponsavel_CteAreaCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [Contratante_Codigo] = @GpoObjCtrlResponsavel_CteCteCod ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmT004L18,1,0,true,false )
             ,new CursorDef("T004L19", "SELECT [GpoObjCtrlResponsavel_Codigo] FROM [GpoObjCtrlResponsavel] WITH (NOLOCK) ORDER BY [GpoObjCtrlResponsavel_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004L19,100,0,true,false )
             ,new CursorDef("T004L20", "SELECT [ContratanteUsuario_ContratanteCod] AS GpoObjCtrlResponsavel_CteCteCod FROM [ContratanteUsuario] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @GpoObjCtrlResponsavel_CteCteCod AND [ContratanteUsuario_UsuarioCod] = @GpoObjCtrlResponsavel_CteUsrCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004L20,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                stmt.SetParameter(4, (int)parms[6]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                return;
       }
    }

 }

}
