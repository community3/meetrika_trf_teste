/*
               File: Pessoa_BC
        Description: Pessoas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:27:34.74
       Program type: HTTP procedure
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class pessoa_bc : GXHttpHandler, IGxSilentTrn
   {
      public pessoa_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public pessoa_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow0B12( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey0B12( ) ;
         standaloneModal( ) ;
         AddRow0B12( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E110B2 */
            E110B2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z34Pessoa_Codigo = A34Pessoa_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_0B0( )
      {
         BeforeValidate0B12( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0B12( ) ;
            }
            else
            {
               CheckExtendedTable0B12( ) ;
               if ( AnyError == 0 )
               {
                  ZM0B12( 8) ;
               }
               CloseExtendedTableCursors0B12( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E120B2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV14Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV15GXV1 = 1;
            while ( AV15GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV15GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Pessoa_MunicipioCod") == 0 )
               {
                  AV13Insert_Pessoa_MunicipioCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               AV15GXV1 = (int)(AV15GXV1+1);
            }
         }
      }

      protected void E110B2( )
      {
         /* After Trn Routine */
      }

      protected void ZM0B12( short GX_JID )
      {
         if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
         {
            Z35Pessoa_Nome = A35Pessoa_Nome;
            Z36Pessoa_TipoPessoa = A36Pessoa_TipoPessoa;
            Z37Pessoa_Docto = A37Pessoa_Docto;
            Z518Pessoa_IE = A518Pessoa_IE;
            Z519Pessoa_Endereco = A519Pessoa_Endereco;
            Z521Pessoa_CEP = A521Pessoa_CEP;
            Z522Pessoa_Telefone = A522Pessoa_Telefone;
            Z523Pessoa_Fax = A523Pessoa_Fax;
            Z38Pessoa_Ativo = A38Pessoa_Ativo;
            Z503Pessoa_MunicipioCod = A503Pessoa_MunicipioCod;
         }
         if ( ( GX_JID == 8 ) || ( GX_JID == 0 ) )
         {
            Z520Pessoa_UF = A520Pessoa_UF;
         }
         if ( GX_JID == -6 )
         {
            Z34Pessoa_Codigo = A34Pessoa_Codigo;
            Z35Pessoa_Nome = A35Pessoa_Nome;
            Z36Pessoa_TipoPessoa = A36Pessoa_TipoPessoa;
            Z37Pessoa_Docto = A37Pessoa_Docto;
            Z518Pessoa_IE = A518Pessoa_IE;
            Z519Pessoa_Endereco = A519Pessoa_Endereco;
            Z521Pessoa_CEP = A521Pessoa_CEP;
            Z522Pessoa_Telefone = A522Pessoa_Telefone;
            Z523Pessoa_Fax = A523Pessoa_Fax;
            Z38Pessoa_Ativo = A38Pessoa_Ativo;
            Z503Pessoa_MunicipioCod = A503Pessoa_MunicipioCod;
            Z520Pessoa_UF = A520Pessoa_UF;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         AV14Pgmname = "Pessoa_BC";
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A38Pessoa_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A38Pessoa_Ativo = true;
         }
      }

      protected void Load0B12( )
      {
         /* Using cursor BC000B5 */
         pr_default.execute(3, new Object[] {A34Pessoa_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound12 = 1;
            A35Pessoa_Nome = BC000B5_A35Pessoa_Nome[0];
            A36Pessoa_TipoPessoa = BC000B5_A36Pessoa_TipoPessoa[0];
            A37Pessoa_Docto = BC000B5_A37Pessoa_Docto[0];
            A518Pessoa_IE = BC000B5_A518Pessoa_IE[0];
            n518Pessoa_IE = BC000B5_n518Pessoa_IE[0];
            A519Pessoa_Endereco = BC000B5_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = BC000B5_n519Pessoa_Endereco[0];
            A521Pessoa_CEP = BC000B5_A521Pessoa_CEP[0];
            n521Pessoa_CEP = BC000B5_n521Pessoa_CEP[0];
            A522Pessoa_Telefone = BC000B5_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = BC000B5_n522Pessoa_Telefone[0];
            A523Pessoa_Fax = BC000B5_A523Pessoa_Fax[0];
            n523Pessoa_Fax = BC000B5_n523Pessoa_Fax[0];
            A38Pessoa_Ativo = BC000B5_A38Pessoa_Ativo[0];
            A503Pessoa_MunicipioCod = BC000B5_A503Pessoa_MunicipioCod[0];
            n503Pessoa_MunicipioCod = BC000B5_n503Pessoa_MunicipioCod[0];
            A520Pessoa_UF = BC000B5_A520Pessoa_UF[0];
            n520Pessoa_UF = BC000B5_n520Pessoa_UF[0];
            ZM0B12( -6) ;
         }
         pr_default.close(3);
         OnLoadActions0B12( ) ;
      }

      protected void OnLoadActions0B12( )
      {
         if ( (0==A503Pessoa_MunicipioCod) )
         {
            A503Pessoa_MunicipioCod = 0;
            n503Pessoa_MunicipioCod = false;
            n503Pessoa_MunicipioCod = true;
         }
      }

      protected void CheckExtendedTable0B12( )
      {
         standaloneModal( ) ;
         /* Using cursor BC000B6 */
         pr_default.execute(4, new Object[] {A37Pessoa_Docto, A34Pessoa_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_1004", new   object[]  {"CPF ou CNPJ"}), 1, "");
            AnyError = 1;
         }
         pr_default.close(4);
         if ( ! ( ( StringUtil.StrCmp(A36Pessoa_TipoPessoa, "F") == 0 ) || ( StringUtil.StrCmp(A36Pessoa_TipoPessoa, "J") == 0 ) ) )
         {
            GX_msglist.addItem("Campo <F>�sica ou <J>ur�dica fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( (0==A503Pessoa_MunicipioCod) )
         {
            A503Pessoa_MunicipioCod = 0;
            n503Pessoa_MunicipioCod = false;
            n503Pessoa_MunicipioCod = true;
         }
         /* Using cursor BC000B4 */
         pr_default.execute(2, new Object[] {n503Pessoa_MunicipioCod, A503Pessoa_MunicipioCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A503Pessoa_MunicipioCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Pessoa_Municipio'.", "ForeignKeyNotFound", 1, "PESSOA_MUNICIPIOCOD");
               AnyError = 1;
            }
         }
         A520Pessoa_UF = BC000B4_A520Pessoa_UF[0];
         n520Pessoa_UF = BC000B4_n520Pessoa_UF[0];
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors0B12( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey0B12( )
      {
         /* Using cursor BC000B7 */
         pr_default.execute(5, new Object[] {A34Pessoa_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound12 = 1;
         }
         else
         {
            RcdFound12 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC000B3 */
         pr_default.execute(1, new Object[] {A34Pessoa_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0B12( 6) ;
            RcdFound12 = 1;
            A34Pessoa_Codigo = BC000B3_A34Pessoa_Codigo[0];
            A35Pessoa_Nome = BC000B3_A35Pessoa_Nome[0];
            A36Pessoa_TipoPessoa = BC000B3_A36Pessoa_TipoPessoa[0];
            A37Pessoa_Docto = BC000B3_A37Pessoa_Docto[0];
            A518Pessoa_IE = BC000B3_A518Pessoa_IE[0];
            n518Pessoa_IE = BC000B3_n518Pessoa_IE[0];
            A519Pessoa_Endereco = BC000B3_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = BC000B3_n519Pessoa_Endereco[0];
            A521Pessoa_CEP = BC000B3_A521Pessoa_CEP[0];
            n521Pessoa_CEP = BC000B3_n521Pessoa_CEP[0];
            A522Pessoa_Telefone = BC000B3_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = BC000B3_n522Pessoa_Telefone[0];
            A523Pessoa_Fax = BC000B3_A523Pessoa_Fax[0];
            n523Pessoa_Fax = BC000B3_n523Pessoa_Fax[0];
            A38Pessoa_Ativo = BC000B3_A38Pessoa_Ativo[0];
            A503Pessoa_MunicipioCod = BC000B3_A503Pessoa_MunicipioCod[0];
            n503Pessoa_MunicipioCod = BC000B3_n503Pessoa_MunicipioCod[0];
            Z34Pessoa_Codigo = A34Pessoa_Codigo;
            sMode12 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load0B12( ) ;
            if ( AnyError == 1 )
            {
               RcdFound12 = 0;
               InitializeNonKey0B12( ) ;
            }
            Gx_mode = sMode12;
         }
         else
         {
            RcdFound12 = 0;
            InitializeNonKey0B12( ) ;
            sMode12 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode12;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0B12( ) ;
         if ( RcdFound12 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_0B0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency0B12( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC000B2 */
            pr_default.execute(0, new Object[] {A34Pessoa_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Pessoa"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z35Pessoa_Nome, BC000B2_A35Pessoa_Nome[0]) != 0 ) || ( StringUtil.StrCmp(Z36Pessoa_TipoPessoa, BC000B2_A36Pessoa_TipoPessoa[0]) != 0 ) || ( StringUtil.StrCmp(Z37Pessoa_Docto, BC000B2_A37Pessoa_Docto[0]) != 0 ) || ( StringUtil.StrCmp(Z518Pessoa_IE, BC000B2_A518Pessoa_IE[0]) != 0 ) || ( StringUtil.StrCmp(Z519Pessoa_Endereco, BC000B2_A519Pessoa_Endereco[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z521Pessoa_CEP, BC000B2_A521Pessoa_CEP[0]) != 0 ) || ( StringUtil.StrCmp(Z522Pessoa_Telefone, BC000B2_A522Pessoa_Telefone[0]) != 0 ) || ( StringUtil.StrCmp(Z523Pessoa_Fax, BC000B2_A523Pessoa_Fax[0]) != 0 ) || ( Z38Pessoa_Ativo != BC000B2_A38Pessoa_Ativo[0] ) || ( Z503Pessoa_MunicipioCod != BC000B2_A503Pessoa_MunicipioCod[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Pessoa"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0B12( )
      {
         BeforeValidate0B12( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0B12( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0B12( 0) ;
            CheckOptimisticConcurrency0B12( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0B12( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0B12( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000B8 */
                     pr_default.execute(6, new Object[] {A35Pessoa_Nome, A36Pessoa_TipoPessoa, A37Pessoa_Docto, n518Pessoa_IE, A518Pessoa_IE, n519Pessoa_Endereco, A519Pessoa_Endereco, n521Pessoa_CEP, A521Pessoa_CEP, n522Pessoa_Telefone, A522Pessoa_Telefone, n523Pessoa_Fax, A523Pessoa_Fax, A38Pessoa_Ativo, n503Pessoa_MunicipioCod, A503Pessoa_MunicipioCod});
                     A34Pessoa_Codigo = BC000B8_A34Pessoa_Codigo[0];
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("Pessoa") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0B12( ) ;
            }
            EndLevel0B12( ) ;
         }
         CloseExtendedTableCursors0B12( ) ;
      }

      protected void Update0B12( )
      {
         BeforeValidate0B12( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0B12( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0B12( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0B12( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0B12( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000B9 */
                     pr_default.execute(7, new Object[] {A35Pessoa_Nome, A36Pessoa_TipoPessoa, A37Pessoa_Docto, n518Pessoa_IE, A518Pessoa_IE, n519Pessoa_Endereco, A519Pessoa_Endereco, n521Pessoa_CEP, A521Pessoa_CEP, n522Pessoa_Telefone, A522Pessoa_Telefone, n523Pessoa_Fax, A523Pessoa_Fax, A38Pessoa_Ativo, n503Pessoa_MunicipioCod, A503Pessoa_MunicipioCod, A34Pessoa_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("Pessoa") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Pessoa"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0B12( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0B12( ) ;
         }
         CloseExtendedTableCursors0B12( ) ;
      }

      protected void DeferredUpdate0B12( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate0B12( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0B12( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0B12( ) ;
            AfterConfirm0B12( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0B12( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC000B10 */
                  pr_default.execute(8, new Object[] {A34Pessoa_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("Pessoa") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode12 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel0B12( ) ;
         Gx_mode = sMode12;
      }

      protected void OnDeleteControls0B12( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC000B11 */
            pr_default.execute(9, new Object[] {n503Pessoa_MunicipioCod, A503Pessoa_MunicipioCod});
            A520Pessoa_UF = BC000B11_A520Pessoa_UF[0];
            n520Pessoa_UF = BC000B11_n520Pessoa_UF[0];
            pr_default.close(9);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor BC000B12 */
            pr_default.execute(10, new Object[] {A34Pessoa_Codigo});
            if ( (pr_default.getStatus(10) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Certificados"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(10);
            /* Using cursor BC000B13 */
            pr_default.execute(11, new Object[] {A34Pessoa_Codigo});
            if ( (pr_default.getStatus(11) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contratante"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(11);
            /* Using cursor BC000B14 */
            pr_default.execute(12, new Object[] {A34Pessoa_Codigo});
            if ( (pr_default.getStatus(12) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato Ocorrencia Notificacao"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(12);
            /* Using cursor BC000B15 */
            pr_default.execute(13, new Object[] {A34Pessoa_Codigo});
            if ( (pr_default.getStatus(13) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Usuario"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(13);
            /* Using cursor BC000B16 */
            pr_default.execute(14, new Object[] {A34Pessoa_Codigo});
            if ( (pr_default.getStatus(14) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contratada"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(14);
         }
      }

      protected void EndLevel0B12( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0B12( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(9);
            context.CommitDataStores( "Pessoa_BC");
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(9);
            context.RollbackDataStores( "Pessoa_BC");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart0B12( )
      {
         /* Scan By routine */
         /* Using cursor BC000B17 */
         pr_default.execute(15, new Object[] {A34Pessoa_Codigo});
         RcdFound12 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound12 = 1;
            A34Pessoa_Codigo = BC000B17_A34Pessoa_Codigo[0];
            A35Pessoa_Nome = BC000B17_A35Pessoa_Nome[0];
            A36Pessoa_TipoPessoa = BC000B17_A36Pessoa_TipoPessoa[0];
            A37Pessoa_Docto = BC000B17_A37Pessoa_Docto[0];
            A518Pessoa_IE = BC000B17_A518Pessoa_IE[0];
            n518Pessoa_IE = BC000B17_n518Pessoa_IE[0];
            A519Pessoa_Endereco = BC000B17_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = BC000B17_n519Pessoa_Endereco[0];
            A521Pessoa_CEP = BC000B17_A521Pessoa_CEP[0];
            n521Pessoa_CEP = BC000B17_n521Pessoa_CEP[0];
            A522Pessoa_Telefone = BC000B17_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = BC000B17_n522Pessoa_Telefone[0];
            A523Pessoa_Fax = BC000B17_A523Pessoa_Fax[0];
            n523Pessoa_Fax = BC000B17_n523Pessoa_Fax[0];
            A38Pessoa_Ativo = BC000B17_A38Pessoa_Ativo[0];
            A503Pessoa_MunicipioCod = BC000B17_A503Pessoa_MunicipioCod[0];
            n503Pessoa_MunicipioCod = BC000B17_n503Pessoa_MunicipioCod[0];
            A520Pessoa_UF = BC000B17_A520Pessoa_UF[0];
            n520Pessoa_UF = BC000B17_n520Pessoa_UF[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext0B12( )
      {
         /* Scan next routine */
         pr_default.readNext(15);
         RcdFound12 = 0;
         ScanKeyLoad0B12( ) ;
      }

      protected void ScanKeyLoad0B12( )
      {
         sMode12 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound12 = 1;
            A34Pessoa_Codigo = BC000B17_A34Pessoa_Codigo[0];
            A35Pessoa_Nome = BC000B17_A35Pessoa_Nome[0];
            A36Pessoa_TipoPessoa = BC000B17_A36Pessoa_TipoPessoa[0];
            A37Pessoa_Docto = BC000B17_A37Pessoa_Docto[0];
            A518Pessoa_IE = BC000B17_A518Pessoa_IE[0];
            n518Pessoa_IE = BC000B17_n518Pessoa_IE[0];
            A519Pessoa_Endereco = BC000B17_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = BC000B17_n519Pessoa_Endereco[0];
            A521Pessoa_CEP = BC000B17_A521Pessoa_CEP[0];
            n521Pessoa_CEP = BC000B17_n521Pessoa_CEP[0];
            A522Pessoa_Telefone = BC000B17_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = BC000B17_n522Pessoa_Telefone[0];
            A523Pessoa_Fax = BC000B17_A523Pessoa_Fax[0];
            n523Pessoa_Fax = BC000B17_n523Pessoa_Fax[0];
            A38Pessoa_Ativo = BC000B17_A38Pessoa_Ativo[0];
            A503Pessoa_MunicipioCod = BC000B17_A503Pessoa_MunicipioCod[0];
            n503Pessoa_MunicipioCod = BC000B17_n503Pessoa_MunicipioCod[0];
            A520Pessoa_UF = BC000B17_A520Pessoa_UF[0];
            n520Pessoa_UF = BC000B17_n520Pessoa_UF[0];
         }
         Gx_mode = sMode12;
      }

      protected void ScanKeyEnd0B12( )
      {
         pr_default.close(15);
      }

      protected void AfterConfirm0B12( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0B12( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0B12( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0B12( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0B12( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0B12( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0B12( )
      {
      }

      protected void AddRow0B12( )
      {
         VarsToRow12( bcPessoa) ;
      }

      protected void ReadRow0B12( )
      {
         RowToVars12( bcPessoa, 1) ;
      }

      protected void InitializeNonKey0B12( )
      {
         A35Pessoa_Nome = "";
         A36Pessoa_TipoPessoa = "";
         A37Pessoa_Docto = "";
         A518Pessoa_IE = "";
         n518Pessoa_IE = false;
         A519Pessoa_Endereco = "";
         n519Pessoa_Endereco = false;
         A503Pessoa_MunicipioCod = 0;
         n503Pessoa_MunicipioCod = false;
         A520Pessoa_UF = "";
         n520Pessoa_UF = false;
         A521Pessoa_CEP = "";
         n521Pessoa_CEP = false;
         A522Pessoa_Telefone = "";
         n522Pessoa_Telefone = false;
         A523Pessoa_Fax = "";
         n523Pessoa_Fax = false;
         A38Pessoa_Ativo = true;
         Z35Pessoa_Nome = "";
         Z36Pessoa_TipoPessoa = "";
         Z37Pessoa_Docto = "";
         Z518Pessoa_IE = "";
         Z519Pessoa_Endereco = "";
         Z521Pessoa_CEP = "";
         Z522Pessoa_Telefone = "";
         Z523Pessoa_Fax = "";
         Z38Pessoa_Ativo = false;
         Z503Pessoa_MunicipioCod = 0;
      }

      protected void InitAll0B12( )
      {
         A34Pessoa_Codigo = 0;
         InitializeNonKey0B12( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A38Pessoa_Ativo = i38Pessoa_Ativo;
      }

      public void VarsToRow12( SdtPessoa obj12 )
      {
         obj12.gxTpr_Mode = Gx_mode;
         obj12.gxTpr_Pessoa_nome = A35Pessoa_Nome;
         obj12.gxTpr_Pessoa_tipopessoa = A36Pessoa_TipoPessoa;
         obj12.gxTpr_Pessoa_docto = A37Pessoa_Docto;
         obj12.gxTpr_Pessoa_ie = A518Pessoa_IE;
         obj12.gxTpr_Pessoa_endereco = A519Pessoa_Endereco;
         obj12.gxTpr_Pessoa_municipiocod = A503Pessoa_MunicipioCod;
         obj12.gxTpr_Pessoa_uf = A520Pessoa_UF;
         obj12.gxTpr_Pessoa_cep = A521Pessoa_CEP;
         obj12.gxTpr_Pessoa_telefone = A522Pessoa_Telefone;
         obj12.gxTpr_Pessoa_fax = A523Pessoa_Fax;
         obj12.gxTpr_Pessoa_ativo = A38Pessoa_Ativo;
         obj12.gxTpr_Pessoa_codigo = A34Pessoa_Codigo;
         obj12.gxTpr_Pessoa_codigo_Z = Z34Pessoa_Codigo;
         obj12.gxTpr_Pessoa_nome_Z = Z35Pessoa_Nome;
         obj12.gxTpr_Pessoa_tipopessoa_Z = Z36Pessoa_TipoPessoa;
         obj12.gxTpr_Pessoa_docto_Z = Z37Pessoa_Docto;
         obj12.gxTpr_Pessoa_ie_Z = Z518Pessoa_IE;
         obj12.gxTpr_Pessoa_endereco_Z = Z519Pessoa_Endereco;
         obj12.gxTpr_Pessoa_municipiocod_Z = Z503Pessoa_MunicipioCod;
         obj12.gxTpr_Pessoa_uf_Z = Z520Pessoa_UF;
         obj12.gxTpr_Pessoa_cep_Z = Z521Pessoa_CEP;
         obj12.gxTpr_Pessoa_telefone_Z = Z522Pessoa_Telefone;
         obj12.gxTpr_Pessoa_fax_Z = Z523Pessoa_Fax;
         obj12.gxTpr_Pessoa_ativo_Z = Z38Pessoa_Ativo;
         obj12.gxTpr_Pessoa_ie_N = (short)(Convert.ToInt16(n518Pessoa_IE));
         obj12.gxTpr_Pessoa_endereco_N = (short)(Convert.ToInt16(n519Pessoa_Endereco));
         obj12.gxTpr_Pessoa_municipiocod_N = (short)(Convert.ToInt16(n503Pessoa_MunicipioCod));
         obj12.gxTpr_Pessoa_uf_N = (short)(Convert.ToInt16(n520Pessoa_UF));
         obj12.gxTpr_Pessoa_cep_N = (short)(Convert.ToInt16(n521Pessoa_CEP));
         obj12.gxTpr_Pessoa_telefone_N = (short)(Convert.ToInt16(n522Pessoa_Telefone));
         obj12.gxTpr_Pessoa_fax_N = (short)(Convert.ToInt16(n523Pessoa_Fax));
         obj12.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow12( SdtPessoa obj12 )
      {
         obj12.gxTpr_Pessoa_codigo = A34Pessoa_Codigo;
         return  ;
      }

      public void RowToVars12( SdtPessoa obj12 ,
                               int forceLoad )
      {
         Gx_mode = obj12.gxTpr_Mode;
         A35Pessoa_Nome = obj12.gxTpr_Pessoa_nome;
         A36Pessoa_TipoPessoa = obj12.gxTpr_Pessoa_tipopessoa;
         A37Pessoa_Docto = obj12.gxTpr_Pessoa_docto;
         A518Pessoa_IE = obj12.gxTpr_Pessoa_ie;
         n518Pessoa_IE = false;
         A519Pessoa_Endereco = obj12.gxTpr_Pessoa_endereco;
         n519Pessoa_Endereco = false;
         A503Pessoa_MunicipioCod = obj12.gxTpr_Pessoa_municipiocod;
         n503Pessoa_MunicipioCod = false;
         A520Pessoa_UF = obj12.gxTpr_Pessoa_uf;
         n520Pessoa_UF = false;
         A521Pessoa_CEP = obj12.gxTpr_Pessoa_cep;
         n521Pessoa_CEP = false;
         A522Pessoa_Telefone = obj12.gxTpr_Pessoa_telefone;
         n522Pessoa_Telefone = false;
         A523Pessoa_Fax = obj12.gxTpr_Pessoa_fax;
         n523Pessoa_Fax = false;
         A38Pessoa_Ativo = obj12.gxTpr_Pessoa_ativo;
         A34Pessoa_Codigo = obj12.gxTpr_Pessoa_codigo;
         Z34Pessoa_Codigo = obj12.gxTpr_Pessoa_codigo_Z;
         Z35Pessoa_Nome = obj12.gxTpr_Pessoa_nome_Z;
         Z36Pessoa_TipoPessoa = obj12.gxTpr_Pessoa_tipopessoa_Z;
         Z37Pessoa_Docto = obj12.gxTpr_Pessoa_docto_Z;
         Z518Pessoa_IE = obj12.gxTpr_Pessoa_ie_Z;
         Z519Pessoa_Endereco = obj12.gxTpr_Pessoa_endereco_Z;
         Z503Pessoa_MunicipioCod = obj12.gxTpr_Pessoa_municipiocod_Z;
         Z520Pessoa_UF = obj12.gxTpr_Pessoa_uf_Z;
         Z521Pessoa_CEP = obj12.gxTpr_Pessoa_cep_Z;
         Z522Pessoa_Telefone = obj12.gxTpr_Pessoa_telefone_Z;
         Z523Pessoa_Fax = obj12.gxTpr_Pessoa_fax_Z;
         Z38Pessoa_Ativo = obj12.gxTpr_Pessoa_ativo_Z;
         n518Pessoa_IE = (bool)(Convert.ToBoolean(obj12.gxTpr_Pessoa_ie_N));
         n519Pessoa_Endereco = (bool)(Convert.ToBoolean(obj12.gxTpr_Pessoa_endereco_N));
         n503Pessoa_MunicipioCod = (bool)(Convert.ToBoolean(obj12.gxTpr_Pessoa_municipiocod_N));
         n520Pessoa_UF = (bool)(Convert.ToBoolean(obj12.gxTpr_Pessoa_uf_N));
         n521Pessoa_CEP = (bool)(Convert.ToBoolean(obj12.gxTpr_Pessoa_cep_N));
         n522Pessoa_Telefone = (bool)(Convert.ToBoolean(obj12.gxTpr_Pessoa_telefone_N));
         n523Pessoa_Fax = (bool)(Convert.ToBoolean(obj12.gxTpr_Pessoa_fax_N));
         Gx_mode = obj12.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A34Pessoa_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey0B12( ) ;
         ScanKeyStart0B12( ) ;
         if ( RcdFound12 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z34Pessoa_Codigo = A34Pessoa_Codigo;
         }
         ZM0B12( -6) ;
         OnLoadActions0B12( ) ;
         AddRow0B12( ) ;
         ScanKeyEnd0B12( ) ;
         if ( RcdFound12 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars12( bcPessoa, 0) ;
         ScanKeyStart0B12( ) ;
         if ( RcdFound12 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z34Pessoa_Codigo = A34Pessoa_Codigo;
         }
         ZM0B12( -6) ;
         OnLoadActions0B12( ) ;
         AddRow0B12( ) ;
         ScanKeyEnd0B12( ) ;
         if ( RcdFound12 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars12( bcPessoa, 0) ;
         nKeyPressed = 1;
         GetKey0B12( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert0B12( ) ;
         }
         else
         {
            if ( RcdFound12 == 1 )
            {
               if ( A34Pessoa_Codigo != Z34Pessoa_Codigo )
               {
                  A34Pessoa_Codigo = Z34Pessoa_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update0B12( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A34Pessoa_Codigo != Z34Pessoa_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert0B12( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert0B12( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow12( bcPessoa) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars12( bcPessoa, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey0B12( ) ;
         if ( RcdFound12 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A34Pessoa_Codigo != Z34Pessoa_Codigo )
            {
               A34Pessoa_Codigo = Z34Pessoa_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A34Pessoa_Codigo != Z34Pessoa_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(9);
         context.RollbackDataStores( "Pessoa_BC");
         VarsToRow12( bcPessoa) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcPessoa.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcPessoa.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcPessoa )
         {
            bcPessoa = (SdtPessoa)(sdt);
            if ( StringUtil.StrCmp(bcPessoa.gxTpr_Mode, "") == 0 )
            {
               bcPessoa.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow12( bcPessoa) ;
            }
            else
            {
               RowToVars12( bcPessoa, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcPessoa.gxTpr_Mode, "") == 0 )
            {
               bcPessoa.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars12( bcPessoa, 1) ;
         return  ;
      }

      public SdtPessoa Pessoa_BC
      {
         get {
            return bcPessoa ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
         GxHttpClient GXSoapHTTPClient ;
         GXSoapHTTPClient = new GxHttpClient( context);
         GxHttpRequest GXSoapHTTPRequest ;
         GXSoapHTTPRequest = new GxHttpRequest(context) ;
         GXXMLReader GXSoapXMLReader ;
         GXSoapXMLReader = new GXXMLReader(context.GetPhysicalPath());
         short GXSoapError ;
         GXSoapError = 0;
         GxHttpResponse GXSoapHTTPResponse ;
         GXSoapHTTPResponse = new GxHttpResponse(context) ;
         GXXMLWriter GXSoapXMLWriter ;
         GXSoapXMLWriter = new GXXMLWriter(context.GetPhysicalPath());
         if ( ! context.isAjaxRequest( ) )
         {
            GXSoapHTTPResponse.AppendHeader("Content-type", "text/xml;charset=utf-8");
         }
         if ( StringUtil.StrCmp(StringUtil.Lower( GXSoapHTTPRequest.Method), "get") == 0 )
         {
            if ( StringUtil.StrCmp(StringUtil.Lower( GXSoapHTTPRequest.QueryString), "wsdl") == 0 )
            {
               GXSoapXMLWriter.OpenResponse(GXSoapHTTPResponse);
               GXSoapXMLWriter.WriteStartDocument("utf-8", 0);
               GXSoapXMLWriter.WriteStartElement("definitions");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_BCBC");
               GXSoapXMLWriter.WriteAttribute("targetNamespace", "GxEv3Up14_MeetrikaVs3");
               GXSoapXMLWriter.WriteAttribute("xmlns:wsdlns", "GxEv3Up14_MeetrikaVs3");
               GXSoapXMLWriter.WriteAttribute("xmlns:soap", "http://schemas.xmlsoap.org/wsdl/soap/");
               GXSoapXMLWriter.WriteAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
               GXSoapXMLWriter.WriteAttribute("xmlns", "http://schemas.xmlsoap.org/wsdl/");
               GXSoapXMLWriter.WriteAttribute("xmlns:tns1", "Genexus");
               GXSoapXMLWriter.WriteAttribute("xmlns:tns", "GxEv3Up14_MeetrikaVs3");
               GXSoapXMLWriter.WriteStartElement("types");
               GXSoapXMLWriter.WriteStartElement("schema");
               GXSoapXMLWriter.WriteAttribute("targetNamespace", "Genexus");
               GXSoapXMLWriter.WriteAttribute("xmlns", "http://www.w3.org/2001/XMLSchema");
               GXSoapXMLWriter.WriteAttribute("xmlns:SOAP-ENC", "http://schemas.xmlsoap.org/soap/encoding/");
               GXSoapXMLWriter.WriteAttribute("elementFormDefault", "qualified");
               GXSoapXMLWriter.WriteElement("import", "");
               GXSoapXMLWriter.WriteAttribute("namespace", "GxEv3Up14_MeetrikaVs3");
               GXSoapXMLWriter.WriteStartElement("complexType");
               GXSoapXMLWriter.WriteAttribute("name", "Messages");
               GXSoapXMLWriter.WriteStartElement("sequence");
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("minOccurs", "0");
               GXSoapXMLWriter.WriteAttribute("maxOccurs", "unbounded");
               GXSoapXMLWriter.WriteAttribute("name", "Messages.Message");
               GXSoapXMLWriter.WriteAttribute("type", "tns1:Messages.Message");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("complexType");
               GXSoapXMLWriter.WriteAttribute("name", "Messages.Message");
               GXSoapXMLWriter.WriteStartElement("sequence");
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Id");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Type");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:byte");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Description");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("schema");
               GXSoapXMLWriter.WriteAttribute("targetNamespace", "GxEv3Up14_MeetrikaVs3");
               GXSoapXMLWriter.WriteAttribute("xmlns", "http://www.w3.org/2001/XMLSchema");
               GXSoapXMLWriter.WriteAttribute("xmlns:SOAP-ENC", "http://schemas.xmlsoap.org/soap/encoding/");
               GXSoapXMLWriter.WriteAttribute("elementFormDefault", "qualified");
               GXSoapXMLWriter.WriteElement("import", "");
               GXSoapXMLWriter.WriteAttribute("namespace", "Genexus");
               GXSoapXMLWriter.WriteStartElement("complexType");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa");
               GXSoapXMLWriter.WriteStartElement("sequence");
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_Codigo");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:int");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_Nome");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_TipoPessoa");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_Docto");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_IE");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_Endereco");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_MunicipioCod");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:int");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_UF");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_CEP");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_Telefone");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_Fax");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_Ativo");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:boolean");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Mode");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Initialized");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:short");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_Codigo_Z");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:int");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_Nome_Z");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_TipoPessoa_Z");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_Docto_Z");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_IE_Z");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_Endereco_Z");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_MunicipioCod_Z");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:int");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_UF_Z");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_CEP_Z");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_Telefone_Z");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_Fax_Z");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_Ativo_Z");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:boolean");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_IE_N");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:byte");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_Endereco_N");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:byte");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_MunicipioCod_N");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:byte");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_UF_N");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:byte");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_CEP_N");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:byte");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_Telefone_N");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:byte");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_Fax_N");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:byte");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_BCBC.LoadKeySvc");
               GXSoapXMLWriter.WriteStartElement("complexType");
               GXSoapXMLWriter.WriteStartElement("sequence");
               GXSoapXMLWriter.WriteElement("element", "");
               GXSoapXMLWriter.WriteAttribute("minOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("maxOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_codigo");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:int");
               GXSoapXMLWriter.WriteElement("element", "");
               GXSoapXMLWriter.WriteAttribute("minOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("maxOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("name", "bcPessoa");
               GXSoapXMLWriter.WriteAttribute("type", "tns:Pessoa");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_BCBC.LoadKeySvcResponse");
               GXSoapXMLWriter.WriteStartElement("complexType");
               GXSoapXMLWriter.WriteStartElement("sequence");
               GXSoapXMLWriter.WriteElement("element", "");
               GXSoapXMLWriter.WriteAttribute("minOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("maxOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("name", "bcPessoa");
               GXSoapXMLWriter.WriteAttribute("type", "tns:Pessoa");
               GXSoapXMLWriter.WriteElement("element", "");
               GXSoapXMLWriter.WriteAttribute("minOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("maxOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("name", "bcPessoaMessages");
               GXSoapXMLWriter.WriteAttribute("type", "tns1:Messages");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_BCBC.SaveSvc");
               GXSoapXMLWriter.WriteStartElement("complexType");
               GXSoapXMLWriter.WriteStartElement("sequence");
               GXSoapXMLWriter.WriteElement("element", "");
               GXSoapXMLWriter.WriteAttribute("minOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("maxOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("name", "bcPessoa");
               GXSoapXMLWriter.WriteAttribute("type", "tns:Pessoa");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_BCBC.SaveSvcResponse");
               GXSoapXMLWriter.WriteStartElement("complexType");
               GXSoapXMLWriter.WriteStartElement("sequence");
               GXSoapXMLWriter.WriteElement("element", "");
               GXSoapXMLWriter.WriteAttribute("minOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("maxOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("name", "bcPessoa");
               GXSoapXMLWriter.WriteAttribute("type", "tns:Pessoa");
               GXSoapXMLWriter.WriteElement("element", "");
               GXSoapXMLWriter.WriteAttribute("minOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("maxOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("name", "bcPessoaMessages");
               GXSoapXMLWriter.WriteAttribute("type", "tns1:Messages");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_BCBC.CheckSvc");
               GXSoapXMLWriter.WriteStartElement("complexType");
               GXSoapXMLWriter.WriteStartElement("sequence");
               GXSoapXMLWriter.WriteElement("element", "");
               GXSoapXMLWriter.WriteAttribute("minOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("maxOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("name", "bcPessoa");
               GXSoapXMLWriter.WriteAttribute("type", "tns:Pessoa");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_BCBC.CheckSvcResponse");
               GXSoapXMLWriter.WriteStartElement("complexType");
               GXSoapXMLWriter.WriteStartElement("sequence");
               GXSoapXMLWriter.WriteElement("element", "");
               GXSoapXMLWriter.WriteAttribute("minOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("maxOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("name", "bcPessoa");
               GXSoapXMLWriter.WriteAttribute("type", "tns:Pessoa");
               GXSoapXMLWriter.WriteElement("element", "");
               GXSoapXMLWriter.WriteAttribute("minOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("maxOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("name", "bcPessoaMessages");
               GXSoapXMLWriter.WriteAttribute("type", "tns1:Messages");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_BCBC.DeleteSvc");
               GXSoapXMLWriter.WriteStartElement("complexType");
               GXSoapXMLWriter.WriteStartElement("sequence");
               GXSoapXMLWriter.WriteElement("element", "");
               GXSoapXMLWriter.WriteAttribute("minOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("maxOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("name", "bcPessoa");
               GXSoapXMLWriter.WriteAttribute("type", "tns:Pessoa");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_BCBC.DeleteSvcResponse");
               GXSoapXMLWriter.WriteStartElement("complexType");
               GXSoapXMLWriter.WriteStartElement("sequence");
               GXSoapXMLWriter.WriteElement("element", "");
               GXSoapXMLWriter.WriteAttribute("minOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("maxOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("name", "bcPessoa");
               GXSoapXMLWriter.WriteAttribute("type", "tns:Pessoa");
               GXSoapXMLWriter.WriteElement("element", "");
               GXSoapXMLWriter.WriteAttribute("minOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("maxOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("name", "bcPessoaMessages");
               GXSoapXMLWriter.WriteAttribute("type", "tns1:Messages");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("message");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_BCBC.LoadKeySvcSoapIn");
               GXSoapXMLWriter.WriteElement("part", "");
               GXSoapXMLWriter.WriteAttribute("name", "parameters");
               GXSoapXMLWriter.WriteAttribute("element", "tns:Pessoa_BCBC.LoadKeySvc");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("message");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_BCBC.LoadKeySvcSoapOut");
               GXSoapXMLWriter.WriteElement("part", "");
               GXSoapXMLWriter.WriteAttribute("name", "parameters");
               GXSoapXMLWriter.WriteAttribute("element", "tns:Pessoa_BCBC.LoadKeySvcResponse");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("message");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_BCBC.SaveSvcSoapIn");
               GXSoapXMLWriter.WriteElement("part", "");
               GXSoapXMLWriter.WriteAttribute("name", "parameters");
               GXSoapXMLWriter.WriteAttribute("element", "tns:Pessoa_BCBC.SaveSvc");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("message");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_BCBC.SaveSvcSoapOut");
               GXSoapXMLWriter.WriteElement("part", "");
               GXSoapXMLWriter.WriteAttribute("name", "parameters");
               GXSoapXMLWriter.WriteAttribute("element", "tns:Pessoa_BCBC.SaveSvcResponse");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("message");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_BCBC.CheckSvcSoapIn");
               GXSoapXMLWriter.WriteElement("part", "");
               GXSoapXMLWriter.WriteAttribute("name", "parameters");
               GXSoapXMLWriter.WriteAttribute("element", "tns:Pessoa_BCBC.CheckSvc");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("message");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_BCBC.CheckSvcSoapOut");
               GXSoapXMLWriter.WriteElement("part", "");
               GXSoapXMLWriter.WriteAttribute("name", "parameters");
               GXSoapXMLWriter.WriteAttribute("element", "tns:Pessoa_BCBC.CheckSvcResponse");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("message");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_BCBC.DeleteSvcSoapIn");
               GXSoapXMLWriter.WriteElement("part", "");
               GXSoapXMLWriter.WriteAttribute("name", "parameters");
               GXSoapXMLWriter.WriteAttribute("element", "tns:Pessoa_BCBC.DeleteSvc");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("message");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_BCBC.DeleteSvcSoapOut");
               GXSoapXMLWriter.WriteElement("part", "");
               GXSoapXMLWriter.WriteAttribute("name", "parameters");
               GXSoapXMLWriter.WriteAttribute("element", "tns:Pessoa_BCBC.DeleteSvcResponse");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("portType");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_BCBCSoapPort");
               GXSoapXMLWriter.WriteStartElement("operation");
               GXSoapXMLWriter.WriteAttribute("name", "LoadKeySvc");
               GXSoapXMLWriter.WriteElement("input", "");
               GXSoapXMLWriter.WriteAttribute("message", "wsdlns:"+"Pessoa_BCBC.LoadKeySvcSoapIn");
               GXSoapXMLWriter.WriteElement("output", "");
               GXSoapXMLWriter.WriteAttribute("message", "wsdlns:"+"Pessoa_BCBC.LoadKeySvcSoapOut");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("operation");
               GXSoapXMLWriter.WriteAttribute("name", "SaveSvc");
               GXSoapXMLWriter.WriteElement("input", "");
               GXSoapXMLWriter.WriteAttribute("message", "wsdlns:"+"Pessoa_BCBC.SaveSvcSoapIn");
               GXSoapXMLWriter.WriteElement("output", "");
               GXSoapXMLWriter.WriteAttribute("message", "wsdlns:"+"Pessoa_BCBC.SaveSvcSoapOut");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("operation");
               GXSoapXMLWriter.WriteAttribute("name", "CheckSvc");
               GXSoapXMLWriter.WriteElement("input", "");
               GXSoapXMLWriter.WriteAttribute("message", "wsdlns:"+"Pessoa_BCBC.CheckSvcSoapIn");
               GXSoapXMLWriter.WriteElement("output", "");
               GXSoapXMLWriter.WriteAttribute("message", "wsdlns:"+"Pessoa_BCBC.CheckSvcSoapOut");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("operation");
               GXSoapXMLWriter.WriteAttribute("name", "DeleteSvc");
               GXSoapXMLWriter.WriteElement("input", "");
               GXSoapXMLWriter.WriteAttribute("message", "wsdlns:"+"Pessoa_BCBC.DeleteSvcSoapIn");
               GXSoapXMLWriter.WriteElement("output", "");
               GXSoapXMLWriter.WriteAttribute("message", "wsdlns:"+"Pessoa_BCBC.DeleteSvcSoapOut");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("binding");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_BCBCSoapBinding");
               GXSoapXMLWriter.WriteAttribute("type", "wsdlns:"+"Pessoa_BCBCSoapPort");
               GXSoapXMLWriter.WriteElement("soap:binding", "");
               GXSoapXMLWriter.WriteAttribute("style", "document");
               GXSoapXMLWriter.WriteAttribute("transport", "http://schemas.xmlsoap.org/soap/http");
               GXSoapXMLWriter.WriteStartElement("operation");
               GXSoapXMLWriter.WriteAttribute("name", "LoadKeySvc");
               GXSoapXMLWriter.WriteElement("soap:operation", "");
               GXSoapXMLWriter.WriteAttribute("soapAction", "GxEv3Up14_MeetrikaVs3action/"+"PESSOA_BC.LoadKeySvc");
               GXSoapXMLWriter.WriteStartElement("input");
               GXSoapXMLWriter.WriteElement("soap:body", "");
               GXSoapXMLWriter.WriteAttribute("use", "literal");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("output");
               GXSoapXMLWriter.WriteElement("soap:body", "");
               GXSoapXMLWriter.WriteAttribute("use", "literal");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("operation");
               GXSoapXMLWriter.WriteAttribute("name", "SaveSvc");
               GXSoapXMLWriter.WriteElement("soap:operation", "");
               GXSoapXMLWriter.WriteAttribute("soapAction", "GxEv3Up14_MeetrikaVs3action/"+"PESSOA_BC.SaveSvc");
               GXSoapXMLWriter.WriteStartElement("input");
               GXSoapXMLWriter.WriteElement("soap:body", "");
               GXSoapXMLWriter.WriteAttribute("use", "literal");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("output");
               GXSoapXMLWriter.WriteElement("soap:body", "");
               GXSoapXMLWriter.WriteAttribute("use", "literal");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("operation");
               GXSoapXMLWriter.WriteAttribute("name", "CheckSvc");
               GXSoapXMLWriter.WriteElement("soap:operation", "");
               GXSoapXMLWriter.WriteAttribute("soapAction", "GxEv3Up14_MeetrikaVs3action/"+"PESSOA_BC.CheckSvc");
               GXSoapXMLWriter.WriteStartElement("input");
               GXSoapXMLWriter.WriteElement("soap:body", "");
               GXSoapXMLWriter.WriteAttribute("use", "literal");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("output");
               GXSoapXMLWriter.WriteElement("soap:body", "");
               GXSoapXMLWriter.WriteAttribute("use", "literal");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("operation");
               GXSoapXMLWriter.WriteAttribute("name", "DeleteSvc");
               GXSoapXMLWriter.WriteElement("soap:operation", "");
               GXSoapXMLWriter.WriteAttribute("soapAction", "GxEv3Up14_MeetrikaVs3action/"+"PESSOA_BC.DeleteSvc");
               GXSoapXMLWriter.WriteStartElement("input");
               GXSoapXMLWriter.WriteElement("soap:body", "");
               GXSoapXMLWriter.WriteAttribute("use", "literal");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("output");
               GXSoapXMLWriter.WriteElement("soap:body", "");
               GXSoapXMLWriter.WriteAttribute("use", "literal");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("service");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_BCBC");
               GXSoapXMLWriter.WriteStartElement("port");
               GXSoapXMLWriter.WriteAttribute("name", "Pessoa_BCBCSoapPort");
               GXSoapXMLWriter.WriteAttribute("binding", "wsdlns:"+"Pessoa_BCBCSoapBinding");
               GXSoapXMLWriter.WriteElement("soap:address", "");
               GXSoapXMLWriter.WriteAttribute("location", "http://"+context.GetServerName( )+((context.GetServerPort( )>0)&&(context.GetServerPort( )!=80)&&(context.GetServerPort( )!=443) ? ":"+StringUtil.LTrim( StringUtil.Str( (decimal)(context.GetServerPort( )), 6, 0)) : "")+context.GetScriptPath( )+"pessoa_bc.aspx");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.Close();
               return  ;
            }
            else
            {
               currSoapErr = (short)(-20000);
               currSoapErrmsg = "No SOAP request found. Call " + "http://" + context.GetServerName( ) + ((context.GetServerPort( )>0)&&(context.GetServerPort( )!=80)&&(context.GetServerPort( )!=443) ? ":"+StringUtil.LTrim( StringUtil.Str( (decimal)(context.GetServerPort( )), 6, 0)) : "") + context.GetScriptPath( ) + "pessoa_bc.aspx" + "?wsdl to get the WSDL.";
            }
         }
         if ( currSoapErr == 0 )
         {
            GXSoapXMLReader.OpenRequest(GXSoapHTTPRequest);
            GXSoapXMLReader.IgnoreComments = 1;
            GXSoapError = GXSoapXMLReader.Read();
            while ( GXSoapError > 0 )
            {
               if ( StringUtil.StringSearch( GXSoapXMLReader.Name, "Body", 1) > 0 )
               {
                  if (true) break;
               }
               GXSoapError = GXSoapXMLReader.Read();
            }
            if ( GXSoapError > 0 )
            {
               GXSoapError = GXSoapXMLReader.Read();
               if ( GXSoapError > 0 )
               {
                  currMethod = GXSoapXMLReader.Name;
                  if ( StringUtil.StringSearch( currMethod+"&", "LoadKeySvc&", 1) > 0 )
                  {
                     if ( currSoapErr == 0 )
                     {
                        bcPessoa = new SdtPessoa(context);
                        bcPessoaMessages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
                        sTagName = GXSoapXMLReader.Name;
                        if ( GXSoapXMLReader.IsSimple == 0 )
                        {
                           GXSoapError = GXSoapXMLReader.Read();
                           nOutParmCount = 0;
                           while ( ( ( StringUtil.StrCmp(GXSoapXMLReader.Name, sTagName) != 0 ) || ( GXSoapXMLReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
                           {
                              readOk = 0;
                              if ( StringUtil.StrCmp2( GXSoapXMLReader.LocalName, "Pessoa_codigo") )
                              {
                                 A34Pessoa_Codigo = (int)(NumberUtil.Val( GXSoapXMLReader.Value, "."));
                                 if ( GXSoapError > 0 )
                                 {
                                    readOk = 1;
                                 }
                                 GXSoapError = GXSoapXMLReader.Read();
                              }
                              if ( StringUtil.StrCmp2( GXSoapXMLReader.LocalName, "bcPessoa") )
                              {
                                 if ( bcPessoa == null )
                                 {
                                    bcPessoa = new SdtPessoa(context);
                                 }
                                 if ( ( GXSoapXMLReader.IsSimple == 0 ) || ( GXSoapXMLReader.AttributeCount > 0 ) )
                                 {
                                    GXSoapError = bcPessoa.readxml(GXSoapXMLReader, "bcPessoa");
                                 }
                                 if ( GXSoapError > 0 )
                                 {
                                    readOk = 1;
                                 }
                                 GXSoapError = GXSoapXMLReader.Read();
                              }
                              nOutParmCount = (short)(nOutParmCount+1);
                              if ( readOk == 0 )
                              {
                                 context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                                 context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + GXSoapXMLReader.ReadRawXML();
                                 GXSoapError = (short)(nOutParmCount*-1);
                              }
                           }
                        }
                     }
                  }
                  else if ( StringUtil.StringSearch( currMethod+"&", "SaveSvc&", 1) > 0 )
                  {
                     if ( currSoapErr == 0 )
                     {
                        bcPessoa = new SdtPessoa(context);
                        bcPessoaMessages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
                        sTagName = GXSoapXMLReader.Name;
                        if ( GXSoapXMLReader.IsSimple == 0 )
                        {
                           GXSoapError = GXSoapXMLReader.Read();
                           nOutParmCount = 0;
                           while ( ( ( StringUtil.StrCmp(GXSoapXMLReader.Name, sTagName) != 0 ) || ( GXSoapXMLReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
                           {
                              readOk = 0;
                              if ( StringUtil.StrCmp2( GXSoapXMLReader.LocalName, "bcPessoa") )
                              {
                                 if ( bcPessoa == null )
                                 {
                                    bcPessoa = new SdtPessoa(context);
                                 }
                                 if ( ( GXSoapXMLReader.IsSimple == 0 ) || ( GXSoapXMLReader.AttributeCount > 0 ) )
                                 {
                                    GXSoapError = bcPessoa.readxml(GXSoapXMLReader, "bcPessoa");
                                 }
                                 if ( GXSoapError > 0 )
                                 {
                                    readOk = 1;
                                 }
                                 GXSoapError = GXSoapXMLReader.Read();
                              }
                              nOutParmCount = (short)(nOutParmCount+1);
                              if ( readOk == 0 )
                              {
                                 context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                                 context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + GXSoapXMLReader.ReadRawXML();
                                 GXSoapError = (short)(nOutParmCount*-1);
                              }
                           }
                        }
                     }
                  }
                  else if ( StringUtil.StringSearch( currMethod+"&", "CheckSvc&", 1) > 0 )
                  {
                     if ( currSoapErr == 0 )
                     {
                        bcPessoa = new SdtPessoa(context);
                        bcPessoaMessages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
                        sTagName = GXSoapXMLReader.Name;
                        if ( GXSoapXMLReader.IsSimple == 0 )
                        {
                           GXSoapError = GXSoapXMLReader.Read();
                           nOutParmCount = 0;
                           while ( ( ( StringUtil.StrCmp(GXSoapXMLReader.Name, sTagName) != 0 ) || ( GXSoapXMLReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
                           {
                              readOk = 0;
                              if ( StringUtil.StrCmp2( GXSoapXMLReader.LocalName, "bcPessoa") )
                              {
                                 if ( bcPessoa == null )
                                 {
                                    bcPessoa = new SdtPessoa(context);
                                 }
                                 if ( ( GXSoapXMLReader.IsSimple == 0 ) || ( GXSoapXMLReader.AttributeCount > 0 ) )
                                 {
                                    GXSoapError = bcPessoa.readxml(GXSoapXMLReader, "bcPessoa");
                                 }
                                 if ( GXSoapError > 0 )
                                 {
                                    readOk = 1;
                                 }
                                 GXSoapError = GXSoapXMLReader.Read();
                              }
                              nOutParmCount = (short)(nOutParmCount+1);
                              if ( readOk == 0 )
                              {
                                 context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                                 context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + GXSoapXMLReader.ReadRawXML();
                                 GXSoapError = (short)(nOutParmCount*-1);
                              }
                           }
                        }
                     }
                  }
                  else if ( StringUtil.StringSearch( currMethod+"&", "DeleteSvc&", 1) > 0 )
                  {
                     if ( currSoapErr == 0 )
                     {
                        bcPessoa = new SdtPessoa(context);
                        bcPessoaMessages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
                        sTagName = GXSoapXMLReader.Name;
                        if ( GXSoapXMLReader.IsSimple == 0 )
                        {
                           GXSoapError = GXSoapXMLReader.Read();
                           nOutParmCount = 0;
                           while ( ( ( StringUtil.StrCmp(GXSoapXMLReader.Name, sTagName) != 0 ) || ( GXSoapXMLReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
                           {
                              readOk = 0;
                              if ( StringUtil.StrCmp2( GXSoapXMLReader.LocalName, "bcPessoa") )
                              {
                                 if ( bcPessoa == null )
                                 {
                                    bcPessoa = new SdtPessoa(context);
                                 }
                                 if ( ( GXSoapXMLReader.IsSimple == 0 ) || ( GXSoapXMLReader.AttributeCount > 0 ) )
                                 {
                                    GXSoapError = bcPessoa.readxml(GXSoapXMLReader, "bcPessoa");
                                 }
                                 if ( GXSoapError > 0 )
                                 {
                                    readOk = 1;
                                 }
                                 GXSoapError = GXSoapXMLReader.Read();
                              }
                              nOutParmCount = (short)(nOutParmCount+1);
                              if ( readOk == 0 )
                              {
                                 context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                                 context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + GXSoapXMLReader.ReadRawXML();
                                 GXSoapError = (short)(nOutParmCount*-1);
                              }
                           }
                        }
                     }
                  }
                  else
                  {
                     currSoapErr = (short)(-20002);
                     currSoapErrmsg = "Wrong method called. Expected method: " + "LoadKeySvc,SaveSvc,CheckSvc,DeleteSvc";
                  }
               }
            }
            GXSoapXMLReader.Close();
         }
         if ( currSoapErr == 0 )
         {
            if ( GXSoapError < 0 )
            {
               currSoapErr = (short)(GXSoapError*-1);
               currSoapErrmsg = context.sSOAPErrMsg;
            }
            else
            {
               if ( GXSoapXMLReader.ErrCode > 0 )
               {
                  currSoapErr = (short)(GXSoapXMLReader.ErrCode*-1);
                  currSoapErrmsg = GXSoapXMLReader.ErrDescription;
               }
               else
               {
                  if ( GXSoapError == 0 )
                  {
                     currSoapErr = (short)(-20001);
                     currSoapErrmsg = "Malformed SOAP message.";
                  }
                  else
                  {
                     currSoapErr = 0;
                     currSoapErrmsg = "No error.";
                  }
               }
            }
         }
         if ( currSoapErr == 0 )
         {
            if ( StringUtil.StringSearch( currMethod+"&", "LoadKeySvc&", 1) > 0 )
            {
               bcPessoa.Load(A34Pessoa_Codigo);
               bcPessoaMessages = bcPessoa.GetMessages();
            }
            else if ( StringUtil.StringSearch( currMethod+"&", "SaveSvc&", 1) > 0 )
            {
               bcPessoa.Save();
               bcPessoaMessages = bcPessoa.GetMessages();
            }
            else if ( StringUtil.StringSearch( currMethod+"&", "CheckSvc&", 1) > 0 )
            {
               bcPessoa.Check();
               bcPessoaMessages = bcPessoa.GetMessages();
            }
            else if ( StringUtil.StringSearch( currMethod+"&", "DeleteSvc&", 1) > 0 )
            {
               bcPessoa.Delete();
               bcPessoaMessages = bcPessoa.GetMessages();
            }
         }
         context.CloseConnections() ;
         GXSoapXMLWriter.OpenResponse(GXSoapHTTPResponse);
         GXSoapXMLWriter.WriteStartDocument("utf-8", 0);
         GXSoapXMLWriter.WriteStartElement("SOAP-ENV:Envelope");
         GXSoapXMLWriter.WriteAttribute("xmlns:SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
         GXSoapXMLWriter.WriteAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
         GXSoapXMLWriter.WriteAttribute("xmlns:SOAP-ENC", "http://schemas.xmlsoap.org/soap/encoding/");
         GXSoapXMLWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
         if ( StringUtil.StringSearch( currMethod+"&", "LoadKeySvc&", 1) > 0 )
         {
            GXSoapXMLWriter.WriteStartElement("SOAP-ENV:Body");
            GXSoapXMLWriter.WriteStartElement("Pessoa_BCBC.LoadKeySvcResponse");
            GXSoapXMLWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            if ( currSoapErr == 0 )
            {
               if ( bcPessoa != null )
               {
                  bcPessoa.writexml(GXSoapXMLWriter, "bcPessoa", "[*:nosend]GxEv3Up14_MeetrikaVs3");
               }
               if ( bcPessoaMessages != null )
               {
                  bcPessoaMessages.writexmlcollection(GXSoapXMLWriter, "bcPessoaMessages", "[*:nosend]GxEv3Up14_MeetrikaVs3", "Messages.Message", "Genexus");
               }
            }
            else
            {
               GXSoapXMLWriter.WriteStartElement("SOAP-ENV:Fault");
               GXSoapXMLWriter.WriteElement("faultcode", "SOAP-ENV:Client");
               GXSoapXMLWriter.WriteElement("faultstring", currSoapErrmsg);
               GXSoapXMLWriter.WriteElement("detail", StringUtil.Trim( StringUtil.Str( (decimal)(currSoapErr), 10, 0)));
               GXSoapXMLWriter.WriteEndElement();
            }
            GXSoapXMLWriter.WriteEndElement();
            GXSoapXMLWriter.WriteEndElement();
         }
         else if ( StringUtil.StringSearch( currMethod+"&", "SaveSvc&", 1) > 0 )
         {
            GXSoapXMLWriter.WriteStartElement("SOAP-ENV:Body");
            GXSoapXMLWriter.WriteStartElement("Pessoa_BCBC.SaveSvcResponse");
            GXSoapXMLWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            if ( currSoapErr == 0 )
            {
               if ( bcPessoa != null )
               {
                  bcPessoa.writexml(GXSoapXMLWriter, "bcPessoa", "[*:nosend]GxEv3Up14_MeetrikaVs3");
               }
               if ( bcPessoaMessages != null )
               {
                  bcPessoaMessages.writexmlcollection(GXSoapXMLWriter, "bcPessoaMessages", "[*:nosend]GxEv3Up14_MeetrikaVs3", "Messages.Message", "Genexus");
               }
            }
            else
            {
               GXSoapXMLWriter.WriteStartElement("SOAP-ENV:Fault");
               GXSoapXMLWriter.WriteElement("faultcode", "SOAP-ENV:Client");
               GXSoapXMLWriter.WriteElement("faultstring", currSoapErrmsg);
               GXSoapXMLWriter.WriteElement("detail", StringUtil.Trim( StringUtil.Str( (decimal)(currSoapErr), 10, 0)));
               GXSoapXMLWriter.WriteEndElement();
            }
            GXSoapXMLWriter.WriteEndElement();
            GXSoapXMLWriter.WriteEndElement();
         }
         else if ( StringUtil.StringSearch( currMethod+"&", "CheckSvc&", 1) > 0 )
         {
            GXSoapXMLWriter.WriteStartElement("SOAP-ENV:Body");
            GXSoapXMLWriter.WriteStartElement("Pessoa_BCBC.CheckSvcResponse");
            GXSoapXMLWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            if ( currSoapErr == 0 )
            {
               if ( bcPessoa != null )
               {
                  bcPessoa.writexml(GXSoapXMLWriter, "bcPessoa", "[*:nosend]GxEv3Up14_MeetrikaVs3");
               }
               if ( bcPessoaMessages != null )
               {
                  bcPessoaMessages.writexmlcollection(GXSoapXMLWriter, "bcPessoaMessages", "[*:nosend]GxEv3Up14_MeetrikaVs3", "Messages.Message", "Genexus");
               }
            }
            else
            {
               GXSoapXMLWriter.WriteStartElement("SOAP-ENV:Fault");
               GXSoapXMLWriter.WriteElement("faultcode", "SOAP-ENV:Client");
               GXSoapXMLWriter.WriteElement("faultstring", currSoapErrmsg);
               GXSoapXMLWriter.WriteElement("detail", StringUtil.Trim( StringUtil.Str( (decimal)(currSoapErr), 10, 0)));
               GXSoapXMLWriter.WriteEndElement();
            }
            GXSoapXMLWriter.WriteEndElement();
            GXSoapXMLWriter.WriteEndElement();
         }
         else if ( StringUtil.StringSearch( currMethod+"&", "DeleteSvc&", 1) > 0 )
         {
            GXSoapXMLWriter.WriteStartElement("SOAP-ENV:Body");
            GXSoapXMLWriter.WriteStartElement("Pessoa_BCBC.DeleteSvcResponse");
            GXSoapXMLWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            if ( currSoapErr == 0 )
            {
               if ( bcPessoa != null )
               {
                  bcPessoa.writexml(GXSoapXMLWriter, "bcPessoa", "[*:nosend]GxEv3Up14_MeetrikaVs3");
               }
               if ( bcPessoaMessages != null )
               {
                  bcPessoaMessages.writexmlcollection(GXSoapXMLWriter, "bcPessoaMessages", "[*:nosend]GxEv3Up14_MeetrikaVs3", "Messages.Message", "Genexus");
               }
            }
            else
            {
               GXSoapXMLWriter.WriteStartElement("SOAP-ENV:Fault");
               GXSoapXMLWriter.WriteElement("faultcode", "SOAP-ENV:Client");
               GXSoapXMLWriter.WriteElement("faultstring", currSoapErrmsg);
               GXSoapXMLWriter.WriteElement("detail", StringUtil.Trim( StringUtil.Str( (decimal)(currSoapErr), 10, 0)));
               GXSoapXMLWriter.WriteEndElement();
            }
            GXSoapXMLWriter.WriteEndElement();
            GXSoapXMLWriter.WriteEndElement();
         }
         GXSoapXMLWriter.WriteEndElement();
         GXSoapXMLWriter.Close();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(9);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV14Pgmname = "";
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z35Pessoa_Nome = "";
         A35Pessoa_Nome = "";
         Z36Pessoa_TipoPessoa = "";
         A36Pessoa_TipoPessoa = "";
         Z37Pessoa_Docto = "";
         A37Pessoa_Docto = "";
         Z518Pessoa_IE = "";
         A518Pessoa_IE = "";
         Z519Pessoa_Endereco = "";
         A519Pessoa_Endereco = "";
         Z521Pessoa_CEP = "";
         A521Pessoa_CEP = "";
         Z522Pessoa_Telefone = "";
         A522Pessoa_Telefone = "";
         Z523Pessoa_Fax = "";
         A523Pessoa_Fax = "";
         Z520Pessoa_UF = "";
         A520Pessoa_UF = "";
         BC000B5_A34Pessoa_Codigo = new int[1] ;
         BC000B5_A35Pessoa_Nome = new String[] {""} ;
         BC000B5_A36Pessoa_TipoPessoa = new String[] {""} ;
         BC000B5_A37Pessoa_Docto = new String[] {""} ;
         BC000B5_A518Pessoa_IE = new String[] {""} ;
         BC000B5_n518Pessoa_IE = new bool[] {false} ;
         BC000B5_A519Pessoa_Endereco = new String[] {""} ;
         BC000B5_n519Pessoa_Endereco = new bool[] {false} ;
         BC000B5_A521Pessoa_CEP = new String[] {""} ;
         BC000B5_n521Pessoa_CEP = new bool[] {false} ;
         BC000B5_A522Pessoa_Telefone = new String[] {""} ;
         BC000B5_n522Pessoa_Telefone = new bool[] {false} ;
         BC000B5_A523Pessoa_Fax = new String[] {""} ;
         BC000B5_n523Pessoa_Fax = new bool[] {false} ;
         BC000B5_A38Pessoa_Ativo = new bool[] {false} ;
         BC000B5_A503Pessoa_MunicipioCod = new int[1] ;
         BC000B5_n503Pessoa_MunicipioCod = new bool[] {false} ;
         BC000B5_A520Pessoa_UF = new String[] {""} ;
         BC000B5_n520Pessoa_UF = new bool[] {false} ;
         BC000B6_A37Pessoa_Docto = new String[] {""} ;
         BC000B4_A520Pessoa_UF = new String[] {""} ;
         BC000B4_n520Pessoa_UF = new bool[] {false} ;
         BC000B7_A34Pessoa_Codigo = new int[1] ;
         BC000B3_A34Pessoa_Codigo = new int[1] ;
         BC000B3_A35Pessoa_Nome = new String[] {""} ;
         BC000B3_A36Pessoa_TipoPessoa = new String[] {""} ;
         BC000B3_A37Pessoa_Docto = new String[] {""} ;
         BC000B3_A518Pessoa_IE = new String[] {""} ;
         BC000B3_n518Pessoa_IE = new bool[] {false} ;
         BC000B3_A519Pessoa_Endereco = new String[] {""} ;
         BC000B3_n519Pessoa_Endereco = new bool[] {false} ;
         BC000B3_A521Pessoa_CEP = new String[] {""} ;
         BC000B3_n521Pessoa_CEP = new bool[] {false} ;
         BC000B3_A522Pessoa_Telefone = new String[] {""} ;
         BC000B3_n522Pessoa_Telefone = new bool[] {false} ;
         BC000B3_A523Pessoa_Fax = new String[] {""} ;
         BC000B3_n523Pessoa_Fax = new bool[] {false} ;
         BC000B3_A38Pessoa_Ativo = new bool[] {false} ;
         BC000B3_A503Pessoa_MunicipioCod = new int[1] ;
         BC000B3_n503Pessoa_MunicipioCod = new bool[] {false} ;
         sMode12 = "";
         BC000B2_A34Pessoa_Codigo = new int[1] ;
         BC000B2_A35Pessoa_Nome = new String[] {""} ;
         BC000B2_A36Pessoa_TipoPessoa = new String[] {""} ;
         BC000B2_A37Pessoa_Docto = new String[] {""} ;
         BC000B2_A518Pessoa_IE = new String[] {""} ;
         BC000B2_n518Pessoa_IE = new bool[] {false} ;
         BC000B2_A519Pessoa_Endereco = new String[] {""} ;
         BC000B2_n519Pessoa_Endereco = new bool[] {false} ;
         BC000B2_A521Pessoa_CEP = new String[] {""} ;
         BC000B2_n521Pessoa_CEP = new bool[] {false} ;
         BC000B2_A522Pessoa_Telefone = new String[] {""} ;
         BC000B2_n522Pessoa_Telefone = new bool[] {false} ;
         BC000B2_A523Pessoa_Fax = new String[] {""} ;
         BC000B2_n523Pessoa_Fax = new bool[] {false} ;
         BC000B2_A38Pessoa_Ativo = new bool[] {false} ;
         BC000B2_A503Pessoa_MunicipioCod = new int[1] ;
         BC000B2_n503Pessoa_MunicipioCod = new bool[] {false} ;
         BC000B8_A34Pessoa_Codigo = new int[1] ;
         BC000B11_A520Pessoa_UF = new String[] {""} ;
         BC000B11_n520Pessoa_UF = new bool[] {false} ;
         BC000B12_A2010PessoaCertificado_Codigo = new int[1] ;
         BC000B13_A29Contratante_Codigo = new int[1] ;
         BC000B14_A297ContratoOcorrenciaNotificacao_Codigo = new int[1] ;
         BC000B15_A1Usuario_Codigo = new int[1] ;
         BC000B16_A39Contratada_Codigo = new int[1] ;
         BC000B17_A34Pessoa_Codigo = new int[1] ;
         BC000B17_A35Pessoa_Nome = new String[] {""} ;
         BC000B17_A36Pessoa_TipoPessoa = new String[] {""} ;
         BC000B17_A37Pessoa_Docto = new String[] {""} ;
         BC000B17_A518Pessoa_IE = new String[] {""} ;
         BC000B17_n518Pessoa_IE = new bool[] {false} ;
         BC000B17_A519Pessoa_Endereco = new String[] {""} ;
         BC000B17_n519Pessoa_Endereco = new bool[] {false} ;
         BC000B17_A521Pessoa_CEP = new String[] {""} ;
         BC000B17_n521Pessoa_CEP = new bool[] {false} ;
         BC000B17_A522Pessoa_Telefone = new String[] {""} ;
         BC000B17_n522Pessoa_Telefone = new bool[] {false} ;
         BC000B17_A523Pessoa_Fax = new String[] {""} ;
         BC000B17_n523Pessoa_Fax = new bool[] {false} ;
         BC000B17_A38Pessoa_Ativo = new bool[] {false} ;
         BC000B17_A503Pessoa_MunicipioCod = new int[1] ;
         BC000B17_n503Pessoa_MunicipioCod = new bool[] {false} ;
         BC000B17_A520Pessoa_UF = new String[] {""} ;
         BC000B17_n520Pessoa_UF = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         currSoapErrmsg = "";
         currMethod = "";
         sTagName = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.pessoa_bc__default(),
            new Object[][] {
                new Object[] {
               BC000B2_A34Pessoa_Codigo, BC000B2_A35Pessoa_Nome, BC000B2_A36Pessoa_TipoPessoa, BC000B2_A37Pessoa_Docto, BC000B2_A518Pessoa_IE, BC000B2_n518Pessoa_IE, BC000B2_A519Pessoa_Endereco, BC000B2_n519Pessoa_Endereco, BC000B2_A521Pessoa_CEP, BC000B2_n521Pessoa_CEP,
               BC000B2_A522Pessoa_Telefone, BC000B2_n522Pessoa_Telefone, BC000B2_A523Pessoa_Fax, BC000B2_n523Pessoa_Fax, BC000B2_A38Pessoa_Ativo, BC000B2_A503Pessoa_MunicipioCod, BC000B2_n503Pessoa_MunicipioCod
               }
               , new Object[] {
               BC000B3_A34Pessoa_Codigo, BC000B3_A35Pessoa_Nome, BC000B3_A36Pessoa_TipoPessoa, BC000B3_A37Pessoa_Docto, BC000B3_A518Pessoa_IE, BC000B3_n518Pessoa_IE, BC000B3_A519Pessoa_Endereco, BC000B3_n519Pessoa_Endereco, BC000B3_A521Pessoa_CEP, BC000B3_n521Pessoa_CEP,
               BC000B3_A522Pessoa_Telefone, BC000B3_n522Pessoa_Telefone, BC000B3_A523Pessoa_Fax, BC000B3_n523Pessoa_Fax, BC000B3_A38Pessoa_Ativo, BC000B3_A503Pessoa_MunicipioCod, BC000B3_n503Pessoa_MunicipioCod
               }
               , new Object[] {
               BC000B4_A520Pessoa_UF, BC000B4_n520Pessoa_UF
               }
               , new Object[] {
               BC000B5_A34Pessoa_Codigo, BC000B5_A35Pessoa_Nome, BC000B5_A36Pessoa_TipoPessoa, BC000B5_A37Pessoa_Docto, BC000B5_A518Pessoa_IE, BC000B5_n518Pessoa_IE, BC000B5_A519Pessoa_Endereco, BC000B5_n519Pessoa_Endereco, BC000B5_A521Pessoa_CEP, BC000B5_n521Pessoa_CEP,
               BC000B5_A522Pessoa_Telefone, BC000B5_n522Pessoa_Telefone, BC000B5_A523Pessoa_Fax, BC000B5_n523Pessoa_Fax, BC000B5_A38Pessoa_Ativo, BC000B5_A503Pessoa_MunicipioCod, BC000B5_n503Pessoa_MunicipioCod, BC000B5_A520Pessoa_UF, BC000B5_n520Pessoa_UF
               }
               , new Object[] {
               BC000B6_A37Pessoa_Docto
               }
               , new Object[] {
               BC000B7_A34Pessoa_Codigo
               }
               , new Object[] {
               BC000B8_A34Pessoa_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC000B11_A520Pessoa_UF, BC000B11_n520Pessoa_UF
               }
               , new Object[] {
               BC000B12_A2010PessoaCertificado_Codigo
               }
               , new Object[] {
               BC000B13_A29Contratante_Codigo
               }
               , new Object[] {
               BC000B14_A297ContratoOcorrenciaNotificacao_Codigo
               }
               , new Object[] {
               BC000B15_A1Usuario_Codigo
               }
               , new Object[] {
               BC000B16_A39Contratada_Codigo
               }
               , new Object[] {
               BC000B17_A34Pessoa_Codigo, BC000B17_A35Pessoa_Nome, BC000B17_A36Pessoa_TipoPessoa, BC000B17_A37Pessoa_Docto, BC000B17_A518Pessoa_IE, BC000B17_n518Pessoa_IE, BC000B17_A519Pessoa_Endereco, BC000B17_n519Pessoa_Endereco, BC000B17_A521Pessoa_CEP, BC000B17_n521Pessoa_CEP,
               BC000B17_A522Pessoa_Telefone, BC000B17_n522Pessoa_Telefone, BC000B17_A523Pessoa_Fax, BC000B17_n523Pessoa_Fax, BC000B17_A38Pessoa_Ativo, BC000B17_A503Pessoa_MunicipioCod, BC000B17_n503Pessoa_MunicipioCod, BC000B17_A520Pessoa_UF, BC000B17_n520Pessoa_UF
               }
            }
         );
         Z38Pessoa_Ativo = true;
         A38Pessoa_Ativo = true;
         i38Pessoa_Ativo = true;
         AV14Pgmname = "Pessoa_BC";
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E120B2 */
         E120B2 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short RcdFound12 ;
      private short currSoapErr ;
      private short readOk ;
      private short nOutParmCount ;
      private int trnEnded ;
      private int Z34Pessoa_Codigo ;
      private int A34Pessoa_Codigo ;
      private int AV15GXV1 ;
      private int AV13Insert_Pessoa_MunicipioCod ;
      private int Z503Pessoa_MunicipioCod ;
      private int A503Pessoa_MunicipioCod ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String AV14Pgmname ;
      private String Z35Pessoa_Nome ;
      private String A35Pessoa_Nome ;
      private String Z36Pessoa_TipoPessoa ;
      private String A36Pessoa_TipoPessoa ;
      private String Z518Pessoa_IE ;
      private String A518Pessoa_IE ;
      private String Z521Pessoa_CEP ;
      private String A521Pessoa_CEP ;
      private String Z522Pessoa_Telefone ;
      private String A522Pessoa_Telefone ;
      private String Z523Pessoa_Fax ;
      private String A523Pessoa_Fax ;
      private String Z520Pessoa_UF ;
      private String A520Pessoa_UF ;
      private String sMode12 ;
      private String currSoapErrmsg ;
      private String currMethod ;
      private String sTagName ;
      private bool Z38Pessoa_Ativo ;
      private bool A38Pessoa_Ativo ;
      private bool n518Pessoa_IE ;
      private bool n519Pessoa_Endereco ;
      private bool n521Pessoa_CEP ;
      private bool n522Pessoa_Telefone ;
      private bool n523Pessoa_Fax ;
      private bool n503Pessoa_MunicipioCod ;
      private bool n520Pessoa_UF ;
      private bool Gx_longc ;
      private bool i38Pessoa_Ativo ;
      private String Z37Pessoa_Docto ;
      private String A37Pessoa_Docto ;
      private String Z519Pessoa_Endereco ;
      private String A519Pessoa_Endereco ;
      private IGxSession AV10WebSession ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection bcPessoaMessages ;
      private SdtPessoa bcPessoa=null ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC000B5_A34Pessoa_Codigo ;
      private String[] BC000B5_A35Pessoa_Nome ;
      private String[] BC000B5_A36Pessoa_TipoPessoa ;
      private String[] BC000B5_A37Pessoa_Docto ;
      private String[] BC000B5_A518Pessoa_IE ;
      private bool[] BC000B5_n518Pessoa_IE ;
      private String[] BC000B5_A519Pessoa_Endereco ;
      private bool[] BC000B5_n519Pessoa_Endereco ;
      private String[] BC000B5_A521Pessoa_CEP ;
      private bool[] BC000B5_n521Pessoa_CEP ;
      private String[] BC000B5_A522Pessoa_Telefone ;
      private bool[] BC000B5_n522Pessoa_Telefone ;
      private String[] BC000B5_A523Pessoa_Fax ;
      private bool[] BC000B5_n523Pessoa_Fax ;
      private bool[] BC000B5_A38Pessoa_Ativo ;
      private int[] BC000B5_A503Pessoa_MunicipioCod ;
      private bool[] BC000B5_n503Pessoa_MunicipioCod ;
      private String[] BC000B5_A520Pessoa_UF ;
      private bool[] BC000B5_n520Pessoa_UF ;
      private String[] BC000B6_A37Pessoa_Docto ;
      private String[] BC000B4_A520Pessoa_UF ;
      private bool[] BC000B4_n520Pessoa_UF ;
      private int[] BC000B7_A34Pessoa_Codigo ;
      private int[] BC000B3_A34Pessoa_Codigo ;
      private String[] BC000B3_A35Pessoa_Nome ;
      private String[] BC000B3_A36Pessoa_TipoPessoa ;
      private String[] BC000B3_A37Pessoa_Docto ;
      private String[] BC000B3_A518Pessoa_IE ;
      private bool[] BC000B3_n518Pessoa_IE ;
      private String[] BC000B3_A519Pessoa_Endereco ;
      private bool[] BC000B3_n519Pessoa_Endereco ;
      private String[] BC000B3_A521Pessoa_CEP ;
      private bool[] BC000B3_n521Pessoa_CEP ;
      private String[] BC000B3_A522Pessoa_Telefone ;
      private bool[] BC000B3_n522Pessoa_Telefone ;
      private String[] BC000B3_A523Pessoa_Fax ;
      private bool[] BC000B3_n523Pessoa_Fax ;
      private bool[] BC000B3_A38Pessoa_Ativo ;
      private int[] BC000B3_A503Pessoa_MunicipioCod ;
      private bool[] BC000B3_n503Pessoa_MunicipioCod ;
      private int[] BC000B2_A34Pessoa_Codigo ;
      private String[] BC000B2_A35Pessoa_Nome ;
      private String[] BC000B2_A36Pessoa_TipoPessoa ;
      private String[] BC000B2_A37Pessoa_Docto ;
      private String[] BC000B2_A518Pessoa_IE ;
      private bool[] BC000B2_n518Pessoa_IE ;
      private String[] BC000B2_A519Pessoa_Endereco ;
      private bool[] BC000B2_n519Pessoa_Endereco ;
      private String[] BC000B2_A521Pessoa_CEP ;
      private bool[] BC000B2_n521Pessoa_CEP ;
      private String[] BC000B2_A522Pessoa_Telefone ;
      private bool[] BC000B2_n522Pessoa_Telefone ;
      private String[] BC000B2_A523Pessoa_Fax ;
      private bool[] BC000B2_n523Pessoa_Fax ;
      private bool[] BC000B2_A38Pessoa_Ativo ;
      private int[] BC000B2_A503Pessoa_MunicipioCod ;
      private bool[] BC000B2_n503Pessoa_MunicipioCod ;
      private int[] BC000B8_A34Pessoa_Codigo ;
      private String[] BC000B11_A520Pessoa_UF ;
      private bool[] BC000B11_n520Pessoa_UF ;
      private int[] BC000B12_A2010PessoaCertificado_Codigo ;
      private int[] BC000B13_A29Contratante_Codigo ;
      private int[] BC000B14_A297ContratoOcorrenciaNotificacao_Codigo ;
      private int[] BC000B15_A1Usuario_Codigo ;
      private int[] BC000B16_A39Contratada_Codigo ;
      private int[] BC000B17_A34Pessoa_Codigo ;
      private String[] BC000B17_A35Pessoa_Nome ;
      private String[] BC000B17_A36Pessoa_TipoPessoa ;
      private String[] BC000B17_A37Pessoa_Docto ;
      private String[] BC000B17_A518Pessoa_IE ;
      private bool[] BC000B17_n518Pessoa_IE ;
      private String[] BC000B17_A519Pessoa_Endereco ;
      private bool[] BC000B17_n519Pessoa_Endereco ;
      private String[] BC000B17_A521Pessoa_CEP ;
      private bool[] BC000B17_n521Pessoa_CEP ;
      private String[] BC000B17_A522Pessoa_Telefone ;
      private bool[] BC000B17_n522Pessoa_Telefone ;
      private String[] BC000B17_A523Pessoa_Fax ;
      private bool[] BC000B17_n523Pessoa_Fax ;
      private bool[] BC000B17_A38Pessoa_Ativo ;
      private int[] BC000B17_A503Pessoa_MunicipioCod ;
      private bool[] BC000B17_n503Pessoa_MunicipioCod ;
      private String[] BC000B17_A520Pessoa_UF ;
      private bool[] BC000B17_n520Pessoa_UF ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class pessoa_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC000B5 ;
          prmBC000B5 = new Object[] {
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000B6 ;
          prmBC000B6 = new Object[] {
          new Object[] {"@Pessoa_Docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000B4 ;
          prmBC000B4 = new Object[] {
          new Object[] {"@Pessoa_MunicipioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000B7 ;
          prmBC000B7 = new Object[] {
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000B3 ;
          prmBC000B3 = new Object[] {
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000B2 ;
          prmBC000B2 = new Object[] {
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000B8 ;
          prmBC000B8 = new Object[] {
          new Object[] {"@Pessoa_Nome",SqlDbType.Char,100,0} ,
          new Object[] {"@Pessoa_TipoPessoa",SqlDbType.Char,1,0} ,
          new Object[] {"@Pessoa_Docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@Pessoa_IE",SqlDbType.Char,15,0} ,
          new Object[] {"@Pessoa_Endereco",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Pessoa_CEP",SqlDbType.Char,10,0} ,
          new Object[] {"@Pessoa_Telefone",SqlDbType.Char,15,0} ,
          new Object[] {"@Pessoa_Fax",SqlDbType.Char,15,0} ,
          new Object[] {"@Pessoa_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Pessoa_MunicipioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000B9 ;
          prmBC000B9 = new Object[] {
          new Object[] {"@Pessoa_Nome",SqlDbType.Char,100,0} ,
          new Object[] {"@Pessoa_TipoPessoa",SqlDbType.Char,1,0} ,
          new Object[] {"@Pessoa_Docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@Pessoa_IE",SqlDbType.Char,15,0} ,
          new Object[] {"@Pessoa_Endereco",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Pessoa_CEP",SqlDbType.Char,10,0} ,
          new Object[] {"@Pessoa_Telefone",SqlDbType.Char,15,0} ,
          new Object[] {"@Pessoa_Fax",SqlDbType.Char,15,0} ,
          new Object[] {"@Pessoa_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Pessoa_MunicipioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000B10 ;
          prmBC000B10 = new Object[] {
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000B11 ;
          prmBC000B11 = new Object[] {
          new Object[] {"@Pessoa_MunicipioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000B12 ;
          prmBC000B12 = new Object[] {
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000B13 ;
          prmBC000B13 = new Object[] {
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000B14 ;
          prmBC000B14 = new Object[] {
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000B15 ;
          prmBC000B15 = new Object[] {
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000B16 ;
          prmBC000B16 = new Object[] {
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000B17 ;
          prmBC000B17 = new Object[] {
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC000B2", "SELECT [Pessoa_Codigo], [Pessoa_Nome], [Pessoa_TipoPessoa], [Pessoa_Docto], [Pessoa_IE], [Pessoa_Endereco], [Pessoa_CEP], [Pessoa_Telefone], [Pessoa_Fax], [Pessoa_Ativo], [Pessoa_MunicipioCod] AS Pessoa_MunicipioCod FROM [Pessoa] WITH (UPDLOCK) WHERE [Pessoa_Codigo] = @Pessoa_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000B2,1,0,true,false )
             ,new CursorDef("BC000B3", "SELECT [Pessoa_Codigo], [Pessoa_Nome], [Pessoa_TipoPessoa], [Pessoa_Docto], [Pessoa_IE], [Pessoa_Endereco], [Pessoa_CEP], [Pessoa_Telefone], [Pessoa_Fax], [Pessoa_Ativo], [Pessoa_MunicipioCod] AS Pessoa_MunicipioCod FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Pessoa_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000B3,1,0,true,false )
             ,new CursorDef("BC000B4", "SELECT [Estado_UF] AS Pessoa_UF FROM [Municipio] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Pessoa_MunicipioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000B4,1,0,true,false )
             ,new CursorDef("BC000B5", "SELECT TM1.[Pessoa_Codigo], TM1.[Pessoa_Nome], TM1.[Pessoa_TipoPessoa], TM1.[Pessoa_Docto], TM1.[Pessoa_IE], TM1.[Pessoa_Endereco], TM1.[Pessoa_CEP], TM1.[Pessoa_Telefone], TM1.[Pessoa_Fax], TM1.[Pessoa_Ativo], TM1.[Pessoa_MunicipioCod] AS Pessoa_MunicipioCod, T2.[Estado_UF] AS Pessoa_UF FROM ([Pessoa] TM1 WITH (NOLOCK) LEFT JOIN [Municipio] T2 WITH (NOLOCK) ON T2.[Municipio_Codigo] = TM1.[Pessoa_MunicipioCod]) WHERE TM1.[Pessoa_Codigo] = @Pessoa_Codigo ORDER BY TM1.[Pessoa_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000B5,100,0,true,false )
             ,new CursorDef("BC000B6", "SELECT [Pessoa_Docto] FROM [Pessoa] WITH (NOLOCK) WHERE ([Pessoa_Docto] = @Pessoa_Docto) AND (Not ( [Pessoa_Codigo] = @Pessoa_Codigo)) ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000B6,1,0,true,false )
             ,new CursorDef("BC000B7", "SELECT [Pessoa_Codigo] FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Pessoa_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000B7,1,0,true,false )
             ,new CursorDef("BC000B8", "INSERT INTO [Pessoa]([Pessoa_Nome], [Pessoa_TipoPessoa], [Pessoa_Docto], [Pessoa_IE], [Pessoa_Endereco], [Pessoa_CEP], [Pessoa_Telefone], [Pessoa_Fax], [Pessoa_Ativo], [Pessoa_MunicipioCod]) VALUES(@Pessoa_Nome, @Pessoa_TipoPessoa, @Pessoa_Docto, @Pessoa_IE, @Pessoa_Endereco, @Pessoa_CEP, @Pessoa_Telefone, @Pessoa_Fax, @Pessoa_Ativo, @Pessoa_MunicipioCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC000B8)
             ,new CursorDef("BC000B9", "UPDATE [Pessoa] SET [Pessoa_Nome]=@Pessoa_Nome, [Pessoa_TipoPessoa]=@Pessoa_TipoPessoa, [Pessoa_Docto]=@Pessoa_Docto, [Pessoa_IE]=@Pessoa_IE, [Pessoa_Endereco]=@Pessoa_Endereco, [Pessoa_CEP]=@Pessoa_CEP, [Pessoa_Telefone]=@Pessoa_Telefone, [Pessoa_Fax]=@Pessoa_Fax, [Pessoa_Ativo]=@Pessoa_Ativo, [Pessoa_MunicipioCod]=@Pessoa_MunicipioCod  WHERE [Pessoa_Codigo] = @Pessoa_Codigo", GxErrorMask.GX_NOMASK,prmBC000B9)
             ,new CursorDef("BC000B10", "DELETE FROM [Pessoa]  WHERE [Pessoa_Codigo] = @Pessoa_Codigo", GxErrorMask.GX_NOMASK,prmBC000B10)
             ,new CursorDef("BC000B11", "SELECT [Estado_UF] AS Pessoa_UF FROM [Municipio] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Pessoa_MunicipioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000B11,1,0,true,false )
             ,new CursorDef("BC000B12", "SELECT TOP 1 [PessoaCertificado_Codigo] FROM [PessoaCertificado] WITH (NOLOCK) WHERE [PessoaCertificado_PesCod] = @Pessoa_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000B12,1,0,true,true )
             ,new CursorDef("BC000B13", "SELECT TOP 1 [Contratante_Codigo] FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_PessoaCod] = @Pessoa_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000B13,1,0,true,true )
             ,new CursorDef("BC000B14", "SELECT TOP 1 [ContratoOcorrenciaNotificacao_Codigo] FROM [ContratoOcorrenciaNotificacao] WITH (NOLOCK) WHERE [ContratoOcorrenciaNotificacao_Responsavel] = @Pessoa_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000B14,1,0,true,true )
             ,new CursorDef("BC000B15", "SELECT TOP 1 [Usuario_Codigo] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_PessoaCod] = @Pessoa_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000B15,1,0,true,true )
             ,new CursorDef("BC000B16", "SELECT TOP 1 [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_PessoaCod] = @Pessoa_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000B16,1,0,true,true )
             ,new CursorDef("BC000B17", "SELECT TM1.[Pessoa_Codigo], TM1.[Pessoa_Nome], TM1.[Pessoa_TipoPessoa], TM1.[Pessoa_Docto], TM1.[Pessoa_IE], TM1.[Pessoa_Endereco], TM1.[Pessoa_CEP], TM1.[Pessoa_Telefone], TM1.[Pessoa_Fax], TM1.[Pessoa_Ativo], TM1.[Pessoa_MunicipioCod] AS Pessoa_MunicipioCod, T2.[Estado_UF] AS Pessoa_UF FROM ([Pessoa] TM1 WITH (NOLOCK) LEFT JOIN [Municipio] T2 WITH (NOLOCK) ON T2.[Municipio_Codigo] = TM1.[Pessoa_MunicipioCod]) WHERE TM1.[Pessoa_Codigo] = @Pessoa_Codigo ORDER BY TM1.[Pessoa_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000B17,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 100) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((String[]) buf[12])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((bool[]) buf[14])[0] = rslt.getBool(10) ;
                ((int[]) buf[15])[0] = rslt.getInt(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 100) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((String[]) buf[12])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((bool[]) buf[14])[0] = rslt.getBool(10) ;
                ((int[]) buf[15])[0] = rslt.getInt(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 100) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((String[]) buf[12])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((bool[]) buf[14])[0] = rslt.getBool(10) ;
                ((int[]) buf[15])[0] = rslt.getInt(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getString(12, 2) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 100) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((String[]) buf[12])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((bool[]) buf[14])[0] = rslt.getBool(10) ;
                ((int[]) buf[15])[0] = rslt.getInt(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getString(12, 2) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[12]);
                }
                stmt.SetParameter(9, (bool)parms[13]);
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[15]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[12]);
                }
                stmt.SetParameter(9, (bool)parms[13]);
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[15]);
                }
                stmt.SetParameter(11, (int)parms[16]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
