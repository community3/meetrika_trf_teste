/*
               File: PRC_CaminhoFisico
        Description: Caminho Fisico
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:40.85
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_caminhofisico : GXProcedure
   {
      public prc_caminhofisico( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_caminhofisico( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( out String aP0_PathApp )
      {
         this.AV8PathApp = "" ;
         initialize();
         executePrivate();
         aP0_PathApp=this.AV8PathApp;
      }

      public String executeUdp( )
      {
         this.AV8PathApp = "" ;
         initialize();
         executePrivate();
         aP0_PathApp=this.AV8PathApp;
         return AV8PathApp ;
      }

      public void executeSubmit( out String aP0_PathApp )
      {
         prc_caminhofisico objprc_caminhofisico;
         objprc_caminhofisico = new prc_caminhofisico();
         objprc_caminhofisico.AV8PathApp = "" ;
         objprc_caminhofisico.context.SetSubmitInitialConfig(context);
         objprc_caminhofisico.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_caminhofisico);
         aP0_PathApp=this.AV8PathApp;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_caminhofisico)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private String AV8PathApp ;
      private String aP0_PathApp ;
   }

}
