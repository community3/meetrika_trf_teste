/*
               File: PRC_CarregaSDTDemandas
        Description: Stub for PRC_CarregaSDTDemandas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/3/2020 18:49:52.88
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_carregasdtdemandas : GXProcedure
   {
      public prc_carregasdtdemandas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public prc_carregasdtdemandas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( short aP0_PraLinha ,
                           String aP1_Arquivo ,
                           String aP2_Aba ,
                           short aP3_ColOSFSn ,
                           short aP4_ColNomeSisteman ,
                           short aP5_ColModulon ,
                           short aP6_ColOSFMn ,
                           short aP7_ColDataDmnn ,
                           short aP8_ColDescricaon ,
                           short aP9_ColLinkn ,
                           int aP10_Contratada_Codigo ,
                           int aP11_ContratadaSrv_Codigo ,
                           bool aP12_EhValidacao ,
                           String aP13_OSFM ,
                           ref DateTime aP14_DataOSFM ,
                           String aP15_Observacao ,
                           int aP16_ContadorFSCod ,
                           String aP17_ContadorFSNom ,
                           String aP18_Link ,
                           short aP19_ColPFBFMn ,
                           short aP20_ColPFLFMn ,
                           int aP21_ContadorFMCod ,
                           short aP22_ColContadorFMn ,
                           int aP23_ContratoServicos_Codigo ,
                           int aP24_ServicoVnc_Codigo ,
                           short aP25_ColFiltron ,
                           String aP26_TipoFiltro ,
                           String aP27_Filtro ,
                           short aP28_ColPFBFSDmnn ,
                           short aP29_ColPFLFSDmnn ,
                           short aP30_ColContadorFSn ,
                           DateTime aP31_DataCnt ,
                           short aP32_ColDataCntn ,
                           String aP33_FileName ,
                           short aP34_ColProjeton ,
                           short aP35_ColTipon ,
                           short aP36_ColObservacaon ,
                           DateTime aP37_PrazoEntrega )
      {
         this.AV2PraLinha = aP0_PraLinha;
         this.AV3Arquivo = aP1_Arquivo;
         this.AV4Aba = aP2_Aba;
         this.AV5ColOSFSn = aP3_ColOSFSn;
         this.AV6ColNomeSisteman = aP4_ColNomeSisteman;
         this.AV7ColModulon = aP5_ColModulon;
         this.AV8ColOSFMn = aP6_ColOSFMn;
         this.AV9ColDataDmnn = aP7_ColDataDmnn;
         this.AV10ColDescricaon = aP8_ColDescricaon;
         this.AV11ColLinkn = aP9_ColLinkn;
         this.AV12Contratada_Codigo = aP10_Contratada_Codigo;
         this.AV13ContratadaSrv_Codigo = aP11_ContratadaSrv_Codigo;
         this.AV14EhValidacao = aP12_EhValidacao;
         this.AV15OSFM = aP13_OSFM;
         this.AV16DataOSFM = aP14_DataOSFM;
         this.AV17Observacao = aP15_Observacao;
         this.AV18ContadorFSCod = aP16_ContadorFSCod;
         this.AV19ContadorFSNom = aP17_ContadorFSNom;
         this.AV20Link = aP18_Link;
         this.AV21ColPFBFMn = aP19_ColPFBFMn;
         this.AV22ColPFLFMn = aP20_ColPFLFMn;
         this.AV23ContadorFMCod = aP21_ContadorFMCod;
         this.AV24ColContadorFMn = aP22_ColContadorFMn;
         this.AV25ContratoServicos_Codigo = aP23_ContratoServicos_Codigo;
         this.AV26ServicoVnc_Codigo = aP24_ServicoVnc_Codigo;
         this.AV27ColFiltron = aP25_ColFiltron;
         this.AV28TipoFiltro = aP26_TipoFiltro;
         this.AV29Filtro = aP27_Filtro;
         this.AV30ColPFBFSDmnn = aP28_ColPFBFSDmnn;
         this.AV31ColPFLFSDmnn = aP29_ColPFLFSDmnn;
         this.AV32ColContadorFSn = aP30_ColContadorFSn;
         this.AV33DataCnt = aP31_DataCnt;
         this.AV34ColDataCntn = aP32_ColDataCntn;
         this.AV35FileName = aP33_FileName;
         this.AV36ColProjeton = aP34_ColProjeton;
         this.AV37ColTipon = aP35_ColTipon;
         this.AV38ColObservacaon = aP36_ColObservacaon;
         this.AV39PrazoEntrega = aP37_PrazoEntrega;
         initialize();
         executePrivate();
         aP14_DataOSFM=this.AV16DataOSFM;
      }

      public void executeSubmit( short aP0_PraLinha ,
                                 String aP1_Arquivo ,
                                 String aP2_Aba ,
                                 short aP3_ColOSFSn ,
                                 short aP4_ColNomeSisteman ,
                                 short aP5_ColModulon ,
                                 short aP6_ColOSFMn ,
                                 short aP7_ColDataDmnn ,
                                 short aP8_ColDescricaon ,
                                 short aP9_ColLinkn ,
                                 int aP10_Contratada_Codigo ,
                                 int aP11_ContratadaSrv_Codigo ,
                                 bool aP12_EhValidacao ,
                                 String aP13_OSFM ,
                                 ref DateTime aP14_DataOSFM ,
                                 String aP15_Observacao ,
                                 int aP16_ContadorFSCod ,
                                 String aP17_ContadorFSNom ,
                                 String aP18_Link ,
                                 short aP19_ColPFBFMn ,
                                 short aP20_ColPFLFMn ,
                                 int aP21_ContadorFMCod ,
                                 short aP22_ColContadorFMn ,
                                 int aP23_ContratoServicos_Codigo ,
                                 int aP24_ServicoVnc_Codigo ,
                                 short aP25_ColFiltron ,
                                 String aP26_TipoFiltro ,
                                 String aP27_Filtro ,
                                 short aP28_ColPFBFSDmnn ,
                                 short aP29_ColPFLFSDmnn ,
                                 short aP30_ColContadorFSn ,
                                 DateTime aP31_DataCnt ,
                                 short aP32_ColDataCntn ,
                                 String aP33_FileName ,
                                 short aP34_ColProjeton ,
                                 short aP35_ColTipon ,
                                 short aP36_ColObservacaon ,
                                 DateTime aP37_PrazoEntrega )
      {
         prc_carregasdtdemandas objprc_carregasdtdemandas;
         objprc_carregasdtdemandas = new prc_carregasdtdemandas();
         objprc_carregasdtdemandas.AV2PraLinha = aP0_PraLinha;
         objprc_carregasdtdemandas.AV3Arquivo = aP1_Arquivo;
         objprc_carregasdtdemandas.AV4Aba = aP2_Aba;
         objprc_carregasdtdemandas.AV5ColOSFSn = aP3_ColOSFSn;
         objprc_carregasdtdemandas.AV6ColNomeSisteman = aP4_ColNomeSisteman;
         objprc_carregasdtdemandas.AV7ColModulon = aP5_ColModulon;
         objprc_carregasdtdemandas.AV8ColOSFMn = aP6_ColOSFMn;
         objprc_carregasdtdemandas.AV9ColDataDmnn = aP7_ColDataDmnn;
         objprc_carregasdtdemandas.AV10ColDescricaon = aP8_ColDescricaon;
         objprc_carregasdtdemandas.AV11ColLinkn = aP9_ColLinkn;
         objprc_carregasdtdemandas.AV12Contratada_Codigo = aP10_Contratada_Codigo;
         objprc_carregasdtdemandas.AV13ContratadaSrv_Codigo = aP11_ContratadaSrv_Codigo;
         objprc_carregasdtdemandas.AV14EhValidacao = aP12_EhValidacao;
         objprc_carregasdtdemandas.AV15OSFM = aP13_OSFM;
         objprc_carregasdtdemandas.AV16DataOSFM = aP14_DataOSFM;
         objprc_carregasdtdemandas.AV17Observacao = aP15_Observacao;
         objprc_carregasdtdemandas.AV18ContadorFSCod = aP16_ContadorFSCod;
         objprc_carregasdtdemandas.AV19ContadorFSNom = aP17_ContadorFSNom;
         objprc_carregasdtdemandas.AV20Link = aP18_Link;
         objprc_carregasdtdemandas.AV21ColPFBFMn = aP19_ColPFBFMn;
         objprc_carregasdtdemandas.AV22ColPFLFMn = aP20_ColPFLFMn;
         objprc_carregasdtdemandas.AV23ContadorFMCod = aP21_ContadorFMCod;
         objprc_carregasdtdemandas.AV24ColContadorFMn = aP22_ColContadorFMn;
         objprc_carregasdtdemandas.AV25ContratoServicos_Codigo = aP23_ContratoServicos_Codigo;
         objprc_carregasdtdemandas.AV26ServicoVnc_Codigo = aP24_ServicoVnc_Codigo;
         objprc_carregasdtdemandas.AV27ColFiltron = aP25_ColFiltron;
         objprc_carregasdtdemandas.AV28TipoFiltro = aP26_TipoFiltro;
         objprc_carregasdtdemandas.AV29Filtro = aP27_Filtro;
         objprc_carregasdtdemandas.AV30ColPFBFSDmnn = aP28_ColPFBFSDmnn;
         objprc_carregasdtdemandas.AV31ColPFLFSDmnn = aP29_ColPFLFSDmnn;
         objprc_carregasdtdemandas.AV32ColContadorFSn = aP30_ColContadorFSn;
         objprc_carregasdtdemandas.AV33DataCnt = aP31_DataCnt;
         objprc_carregasdtdemandas.AV34ColDataCntn = aP32_ColDataCntn;
         objprc_carregasdtdemandas.AV35FileName = aP33_FileName;
         objprc_carregasdtdemandas.AV36ColProjeton = aP34_ColProjeton;
         objprc_carregasdtdemandas.AV37ColTipon = aP35_ColTipon;
         objprc_carregasdtdemandas.AV38ColObservacaon = aP36_ColObservacaon;
         objprc_carregasdtdemandas.AV39PrazoEntrega = aP37_PrazoEntrega;
         objprc_carregasdtdemandas.context.SetSubmitInitialConfig(context);
         objprc_carregasdtdemandas.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_carregasdtdemandas);
         aP14_DataOSFM=this.AV16DataOSFM;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_carregasdtdemandas)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(short)AV2PraLinha,(String)AV3Arquivo,(String)AV4Aba,(short)AV5ColOSFSn,(short)AV6ColNomeSisteman,(short)AV7ColModulon,(short)AV8ColOSFMn,(short)AV9ColDataDmnn,(short)AV10ColDescricaon,(short)AV11ColLinkn,(int)AV12Contratada_Codigo,(int)AV13ContratadaSrv_Codigo,(bool)AV14EhValidacao,(String)AV15OSFM,(DateTime)AV16DataOSFM,(String)AV17Observacao,(int)AV18ContadorFSCod,(String)AV19ContadorFSNom,(String)AV20Link,(short)AV21ColPFBFMn,(short)AV22ColPFLFMn,(int)AV23ContadorFMCod,(short)AV24ColContadorFMn,(int)AV25ContratoServicos_Codigo,(int)AV26ServicoVnc_Codigo,(short)AV27ColFiltron,(String)AV28TipoFiltro,(String)AV29Filtro,(short)AV30ColPFBFSDmnn,(short)AV31ColPFLFSDmnn,(short)AV32ColContadorFSn,(DateTime)AV33DataCnt,(short)AV34ColDataCntn,(String)AV35FileName,(short)AV36ColProjeton,(short)AV37ColTipon,(short)AV38ColObservacaon,(DateTime)AV39PrazoEntrega} ;
         ClassLoader.Execute("aprc_carregasdtdemandas","GeneXus.Programs.aprc_carregasdtdemandas", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 38 ) )
         {
            AV16DataOSFM = (DateTime)(args[14]) ;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV2PraLinha ;
      private short AV5ColOSFSn ;
      private short AV6ColNomeSisteman ;
      private short AV7ColModulon ;
      private short AV8ColOSFMn ;
      private short AV9ColDataDmnn ;
      private short AV10ColDescricaon ;
      private short AV11ColLinkn ;
      private short AV21ColPFBFMn ;
      private short AV22ColPFLFMn ;
      private short AV24ColContadorFMn ;
      private short AV27ColFiltron ;
      private short AV30ColPFBFSDmnn ;
      private short AV31ColPFLFSDmnn ;
      private short AV32ColContadorFSn ;
      private short AV34ColDataCntn ;
      private short AV36ColProjeton ;
      private short AV37ColTipon ;
      private short AV38ColObservacaon ;
      private int AV12Contratada_Codigo ;
      private int AV13ContratadaSrv_Codigo ;
      private int AV18ContadorFSCod ;
      private int AV23ContadorFMCod ;
      private int AV25ContratoServicos_Codigo ;
      private int AV26ServicoVnc_Codigo ;
      private String AV3Arquivo ;
      private String AV4Aba ;
      private String AV19ContadorFSNom ;
      private String AV28TipoFiltro ;
      private String AV29Filtro ;
      private String AV35FileName ;
      private DateTime AV39PrazoEntrega ;
      private DateTime AV16DataOSFM ;
      private DateTime AV33DataCnt ;
      private bool AV14EhValidacao ;
      private String AV17Observacao ;
      private String AV20Link ;
      private String AV15OSFM ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private DateTime aP14_DataOSFM ;
      private Object[] args ;
   }

}
