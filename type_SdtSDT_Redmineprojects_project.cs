/*
               File: type_SdtSDT_Redmineprojects_project
        Description: SDT_Redmineprojects
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:30:6.64
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_Redmineprojects.project" )]
   [XmlType(TypeName =  "SDT_Redmineprojects.project" , Namespace = "" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_Redmineprojects_project_parent ))]
   [Serializable]
   public class SdtSDT_Redmineprojects_project : GxUserType
   {
      public SdtSDT_Redmineprojects_project( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_Redmineprojects_project_Name = "";
         gxTv_SdtSDT_Redmineprojects_project_Identifier = "";
         gxTv_SdtSDT_Redmineprojects_project_Description = "";
         gxTv_SdtSDT_Redmineprojects_project_Created_on = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_Redmineprojects_project_Updated_on = (DateTime)(DateTime.MinValue);
      }

      public SdtSDT_Redmineprojects_project( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_Redmineprojects_project deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType());
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_Redmineprojects_project)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_Redmineprojects_project obj ;
         obj = this;
         obj.gxTpr_Id = deserialized.gxTpr_Id;
         obj.gxTpr_Name = deserialized.gxTpr_Name;
         obj.gxTpr_Identifier = deserialized.gxTpr_Identifier;
         obj.gxTpr_Description = deserialized.gxTpr_Description;
         obj.gxTpr_Parent = deserialized.gxTpr_Parent;
         obj.gxTpr_Created_on = deserialized.gxTpr_Created_on;
         obj.gxTpr_Updated_on = deserialized.gxTpr_Updated_on;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "id") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_Redmineprojects_project_Id = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "name") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_Redmineprojects_project_Name = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "identifier") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_Redmineprojects_project_Identifier = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "description") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_Redmineprojects_project_Description = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "parent") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  if ( gxTv_SdtSDT_Redmineprojects_project_Parent == null )
                  {
                     gxTv_SdtSDT_Redmineprojects_project_Parent = new SdtSDT_Redmineprojects_project_parent(context);
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtSDT_Redmineprojects_project_Parent.readxml(oReader, "parent");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "created_on") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_Redmineprojects_project_Created_on = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSDT_Redmineprojects_project_Created_on = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "updated_on") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_Redmineprojects_project_Updated_on = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSDT_Redmineprojects_project_Updated_on = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_Redmineprojects.project";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("id", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Redmineprojects_project_Id), 2, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("name", StringUtil.RTrim( gxTv_SdtSDT_Redmineprojects_project_Name));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("identifier", StringUtil.RTrim( gxTv_SdtSDT_Redmineprojects_project_Identifier));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("description", StringUtil.RTrim( gxTv_SdtSDT_Redmineprojects_project_Description));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         if ( gxTv_SdtSDT_Redmineprojects_project_Parent != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "";
            }
            else
            {
               sNameSpace1 = "";
            }
            gxTv_SdtSDT_Redmineprojects_project_Parent.writexml(oWriter, "parent", sNameSpace1);
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_Redmineprojects_project_Created_on) )
         {
            oWriter.WriteStartElement("created_on");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_Redmineprojects_project_Created_on)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_Redmineprojects_project_Created_on)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_Redmineprojects_project_Created_on)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSDT_Redmineprojects_project_Created_on)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSDT_Redmineprojects_project_Created_on)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSDT_Redmineprojects_project_Created_on)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("created_on", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_Redmineprojects_project_Updated_on) )
         {
            oWriter.WriteStartElement("updated_on");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_Redmineprojects_project_Updated_on)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_Redmineprojects_project_Updated_on)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_Redmineprojects_project_Updated_on)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSDT_Redmineprojects_project_Updated_on)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSDT_Redmineprojects_project_Updated_on)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSDT_Redmineprojects_project_Updated_on)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("updated_on", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("id", gxTv_SdtSDT_Redmineprojects_project_Id, false);
         AddObjectProperty("name", gxTv_SdtSDT_Redmineprojects_project_Name, false);
         AddObjectProperty("identifier", gxTv_SdtSDT_Redmineprojects_project_Identifier, false);
         AddObjectProperty("description", gxTv_SdtSDT_Redmineprojects_project_Description, false);
         if ( gxTv_SdtSDT_Redmineprojects_project_Parent != null )
         {
            AddObjectProperty("parent", gxTv_SdtSDT_Redmineprojects_project_Parent, false);
         }
         datetime_STZ = gxTv_SdtSDT_Redmineprojects_project_Created_on;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("created_on", sDateCnv, false);
         datetime_STZ = gxTv_SdtSDT_Redmineprojects_project_Updated_on;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("updated_on", sDateCnv, false);
         return  ;
      }

      [  SoapElement( ElementName = "id" )]
      [  XmlElement( ElementName = "id" , Namespace = ""  )]
      public short gxTpr_Id
      {
         get {
            return gxTv_SdtSDT_Redmineprojects_project_Id ;
         }

         set {
            gxTv_SdtSDT_Redmineprojects_project_Id = (short)(value);
         }

      }

      [  SoapElement( ElementName = "name" )]
      [  XmlElement( ElementName = "name" , Namespace = ""  )]
      public String gxTpr_Name
      {
         get {
            return gxTv_SdtSDT_Redmineprojects_project_Name ;
         }

         set {
            gxTv_SdtSDT_Redmineprojects_project_Name = (String)(value);
         }

      }

      [  SoapElement( ElementName = "identifier" )]
      [  XmlElement( ElementName = "identifier" , Namespace = ""  )]
      public String gxTpr_Identifier
      {
         get {
            return gxTv_SdtSDT_Redmineprojects_project_Identifier ;
         }

         set {
            gxTv_SdtSDT_Redmineprojects_project_Identifier = (String)(value);
         }

      }

      [  SoapElement( ElementName = "description" )]
      [  XmlElement( ElementName = "description" , Namespace = ""  )]
      public String gxTpr_Description
      {
         get {
            return gxTv_SdtSDT_Redmineprojects_project_Description ;
         }

         set {
            gxTv_SdtSDT_Redmineprojects_project_Description = (String)(value);
         }

      }

      [  SoapElement( ElementName = "parent" )]
      [  XmlElement( ElementName = "parent"   )]
      public SdtSDT_Redmineprojects_project_parent gxTpr_Parent
      {
         get {
            if ( gxTv_SdtSDT_Redmineprojects_project_Parent == null )
            {
               gxTv_SdtSDT_Redmineprojects_project_Parent = new SdtSDT_Redmineprojects_project_parent(context);
            }
            return gxTv_SdtSDT_Redmineprojects_project_Parent ;
         }

         set {
            gxTv_SdtSDT_Redmineprojects_project_Parent = value;
         }

      }

      public void gxTv_SdtSDT_Redmineprojects_project_Parent_SetNull( )
      {
         gxTv_SdtSDT_Redmineprojects_project_Parent = null;
         return  ;
      }

      public bool gxTv_SdtSDT_Redmineprojects_project_Parent_IsNull( )
      {
         if ( gxTv_SdtSDT_Redmineprojects_project_Parent == null )
         {
            return true ;
         }
         return false ;
      }

      [  SoapElement( ElementName = "created_on" )]
      [  XmlElement( ElementName = "created_on" , Namespace = "" , IsNullable=true )]
      public string gxTpr_Created_on_Nullable
      {
         get {
            if ( gxTv_SdtSDT_Redmineprojects_project_Created_on == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSDT_Redmineprojects_project_Created_on).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSDT_Redmineprojects_project_Created_on = DateTime.MinValue;
            else
               gxTv_SdtSDT_Redmineprojects_project_Created_on = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Created_on
      {
         get {
            return gxTv_SdtSDT_Redmineprojects_project_Created_on ;
         }

         set {
            gxTv_SdtSDT_Redmineprojects_project_Created_on = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "updated_on" )]
      [  XmlElement( ElementName = "updated_on" , Namespace = "" , IsNullable=true )]
      public string gxTpr_Updated_on_Nullable
      {
         get {
            if ( gxTv_SdtSDT_Redmineprojects_project_Updated_on == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSDT_Redmineprojects_project_Updated_on).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSDT_Redmineprojects_project_Updated_on = DateTime.MinValue;
            else
               gxTv_SdtSDT_Redmineprojects_project_Updated_on = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Updated_on
      {
         get {
            return gxTv_SdtSDT_Redmineprojects_project_Updated_on ;
         }

         set {
            gxTv_SdtSDT_Redmineprojects_project_Updated_on = (DateTime)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_Redmineprojects_project_Name = "";
         gxTv_SdtSDT_Redmineprojects_project_Identifier = "";
         gxTv_SdtSDT_Redmineprojects_project_Description = "";
         gxTv_SdtSDT_Redmineprojects_project_Created_on = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_Redmineprojects_project_Updated_on = (DateTime)(DateTime.MinValue);
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         datetime_STZ = (DateTime)(DateTime.MinValue);
         return  ;
      }

      protected short gxTv_SdtSDT_Redmineprojects_project_Id ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtSDT_Redmineprojects_project_Name ;
      protected String gxTv_SdtSDT_Redmineprojects_project_Identifier ;
      protected String gxTv_SdtSDT_Redmineprojects_project_Description ;
      protected String sTagName ;
      protected String sDateCnv ;
      protected String sNumToPad ;
      protected DateTime gxTv_SdtSDT_Redmineprojects_project_Created_on ;
      protected DateTime gxTv_SdtSDT_Redmineprojects_project_Updated_on ;
      protected DateTime datetime_STZ ;
      protected SdtSDT_Redmineprojects_project_parent gxTv_SdtSDT_Redmineprojects_project_Parent=null ;
   }

   [DataContract(Name = @"SDT_Redmineprojects.project", Namespace = "")]
   public class SdtSDT_Redmineprojects_project_RESTInterface : GxGenericCollectionItem<SdtSDT_Redmineprojects_project>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_Redmineprojects_project_RESTInterface( ) : base()
      {
      }

      public SdtSDT_Redmineprojects_project_RESTInterface( SdtSDT_Redmineprojects_project psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "id" , Order = 0 )]
      public Nullable<short> gxTpr_Id
      {
         get {
            return sdt.gxTpr_Id ;
         }

         set {
            sdt.gxTpr_Id = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "name" , Order = 1 )]
      public String gxTpr_Name
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Name) ;
         }

         set {
            sdt.gxTpr_Name = (String)(value);
         }

      }

      [DataMember( Name = "identifier" , Order = 2 )]
      public String gxTpr_Identifier
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Identifier) ;
         }

         set {
            sdt.gxTpr_Identifier = (String)(value);
         }

      }

      [DataMember( Name = "description" , Order = 3 )]
      public String gxTpr_Description
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Description) ;
         }

         set {
            sdt.gxTpr_Description = (String)(value);
         }

      }

      [DataMember( Name = "parent" , Order = 4 )]
      public SdtSDT_Redmineprojects_project_parent_RESTInterface gxTpr_Parent
      {
         get {
            return new SdtSDT_Redmineprojects_project_parent_RESTInterface(sdt.gxTpr_Parent) ;
         }

         set {
            sdt.gxTpr_Parent = value.sdt;
         }

      }

      [DataMember( Name = "created_on" , Order = 5 )]
      public String gxTpr_Created_on
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Created_on) ;
         }

         set {
            sdt.gxTpr_Created_on = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "updated_on" , Order = 6 )]
      public String gxTpr_Updated_on
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Updated_on) ;
         }

         set {
            sdt.gxTpr_Updated_on = DateTimeUtil.CToT2( (String)(value));
         }

      }

      public SdtSDT_Redmineprojects_project sdt
      {
         get {
            return (SdtSDT_Redmineprojects_project)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_Redmineprojects_project() ;
         }
      }

   }

}
