/*
               File: ServicoCheck
        Description: Check Lists
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:9:29.21
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class servicocheck : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"SERVICO_CODIGO") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLASERVICO_CODIGO4N204( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_3") == 0 )
         {
            A155Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_3( A155Servico_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_4") == 0 )
         {
            A1839Check_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_4( A1839Check_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynServico_Codigo.Name = "SERVICO_CODIGO";
         dynServico_Codigo.WebTags = "";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Check Lists", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = dynServico_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public servicocheck( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public servicocheck( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynServico_Codigo = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynServico_Codigo.ItemCount > 0 )
         {
            A155Servico_Codigo = (int)(NumberUtil.Val( dynServico_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_4N204( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_4N204e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_4N204( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_4N204( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_4N204e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Check Lists", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_ServicoCheck.htm");
            wb_table3_28_4N204( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_4N204e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_4N204e( true) ;
         }
         else
         {
            wb_table1_2_4N204e( false) ;
         }
      }

      protected void wb_table3_28_4N204( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_4N204( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_4N204e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ServicoCheck.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ServicoCheck.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ServicoCheck.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_4N204e( true) ;
         }
         else
         {
            wb_table3_28_4N204e( false) ;
         }
      }

      protected void wb_table4_34_4N204( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_codigo_Internalname, "Nome", "", "", lblTextblockservico_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ServicoCheck.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynServico_Codigo, dynServico_Codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)), 1, dynServico_Codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynServico_Codigo.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "", true, "HLP_ServicoCheck.htm");
            dynServico_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServico_Codigo_Internalname, "Values", (String)(dynServico_Codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_sigla_Internalname, "Sigla", "", "", lblTextblockservico_sigla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ServicoCheck.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServico_Sigla_Internalname, StringUtil.RTrim( A605Servico_Sigla), StringUtil.RTrim( context.localUtil.Format( A605Servico_Sigla, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_Sigla_Jsonclick, 0, "Attribute", "", "", "", 1, edtServico_Sigla_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_ServicoCheck.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcheck_codigo_Internalname, "Check List", "", "", lblTextblockcheck_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ServicoCheck.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtCheck_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1839Check_Codigo), 6, 0, ",", "")), ((edtCheck_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1839Check_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1839Check_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCheck_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtCheck_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ServicoCheck.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcheck_nome_Internalname, "Nome", "", "", lblTextblockcheck_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ServicoCheck.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtCheck_Nome_Internalname, StringUtil.RTrim( A1841Check_Nome), StringUtil.RTrim( context.localUtil.Format( A1841Check_Nome, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCheck_Nome_Jsonclick, 0, "Attribute", "", "", "", 1, edtCheck_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ServicoCheck.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_4N204e( true) ;
         }
         else
         {
            wb_table4_34_4N204e( false) ;
         }
      }

      protected void wb_table2_5_4N204( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoCheck.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoCheck.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoCheck.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoCheck.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoCheck.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoCheck.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoCheck.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoCheck.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoCheck.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoCheck.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoCheck.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoCheck.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoCheck.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoCheck.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoCheck.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoCheck.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_4N204e( true) ;
         }
         else
         {
            wb_table2_5_4N204e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               dynServico_Codigo.CurrentValue = cgiGet( dynServico_Codigo_Internalname);
               A155Servico_Codigo = (int)(NumberUtil.Val( cgiGet( dynServico_Codigo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
               A605Servico_Sigla = StringUtil.Upper( cgiGet( edtServico_Sigla_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A605Servico_Sigla", A605Servico_Sigla);
               if ( ( ( context.localUtil.CToN( cgiGet( edtCheck_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtCheck_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CHECK_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtCheck_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1839Check_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
               }
               else
               {
                  A1839Check_Codigo = (int)(context.localUtil.CToN( cgiGet( edtCheck_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
               }
               A1841Check_Nome = StringUtil.Upper( cgiGet( edtCheck_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1841Check_Nome", A1841Check_Nome);
               /* Read saved values. */
               Z155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z155Servico_Codigo"), ",", "."));
               Z1839Check_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1839Check_Codigo"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A155Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
                  A1839Check_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll4N204( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes4N204( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption4N0( )
      {
      }

      protected void ZM4N204( short GX_JID )
      {
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
            }
            else
            {
            }
         }
         if ( GX_JID == -2 )
         {
            Z155Servico_Codigo = A155Servico_Codigo;
            Z1839Check_Codigo = A1839Check_Codigo;
            Z605Servico_Sigla = A605Servico_Sigla;
            Z1841Check_Nome = A1841Check_Nome;
         }
      }

      protected void standaloneNotModal( )
      {
         GXASERVICO_CODIGO_html4N204( ) ;
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T004N4 */
            pr_default.execute(2, new Object[] {A155Servico_Codigo});
            A605Servico_Sigla = T004N4_A605Servico_Sigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A605Servico_Sigla", A605Servico_Sigla);
            pr_default.close(2);
         }
      }

      protected void Load4N204( )
      {
         /* Using cursor T004N6 */
         pr_default.execute(4, new Object[] {A155Servico_Codigo, A1839Check_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound204 = 1;
            A605Servico_Sigla = T004N6_A605Servico_Sigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A605Servico_Sigla", A605Servico_Sigla);
            A1841Check_Nome = T004N6_A1841Check_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1841Check_Nome", A1841Check_Nome);
            ZM4N204( -2) ;
         }
         pr_default.close(4);
         OnLoadActions4N204( ) ;
      }

      protected void OnLoadActions4N204( )
      {
      }

      protected void CheckExtendedTable4N204( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T004N4 */
         pr_default.execute(2, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico'.", "ForeignKeyNotFound", 1, "SERVICO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynServico_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A605Servico_Sigla = T004N4_A605Servico_Sigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A605Servico_Sigla", A605Servico_Sigla);
         pr_default.close(2);
         /* Using cursor T004N5 */
         pr_default.execute(3, new Object[] {A1839Check_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe ' T204'.", "ForeignKeyNotFound", 1, "CHECK_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtCheck_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1841Check_Nome = T004N5_A1841Check_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1841Check_Nome", A1841Check_Nome);
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors4N204( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_3( int A155Servico_Codigo )
      {
         /* Using cursor T004N7 */
         pr_default.execute(5, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico'.", "ForeignKeyNotFound", 1, "SERVICO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynServico_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A605Servico_Sigla = T004N7_A605Servico_Sigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A605Servico_Sigla", A605Servico_Sigla);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A605Servico_Sigla))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_4( int A1839Check_Codigo )
      {
         /* Using cursor T004N8 */
         pr_default.execute(6, new Object[] {A1839Check_Codigo});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe ' T204'.", "ForeignKeyNotFound", 1, "CHECK_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtCheck_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1841Check_Nome = T004N8_A1841Check_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1841Check_Nome", A1841Check_Nome);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1841Check_Nome))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey4N204( )
      {
         /* Using cursor T004N9 */
         pr_default.execute(7, new Object[] {A155Servico_Codigo, A1839Check_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound204 = 1;
         }
         else
         {
            RcdFound204 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T004N3 */
         pr_default.execute(1, new Object[] {A155Servico_Codigo, A1839Check_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4N204( 2) ;
            RcdFound204 = 1;
            A155Servico_Codigo = T004N3_A155Servico_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            A1839Check_Codigo = T004N3_A1839Check_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
            Z155Servico_Codigo = A155Servico_Codigo;
            Z1839Check_Codigo = A1839Check_Codigo;
            sMode204 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load4N204( ) ;
            if ( AnyError == 1 )
            {
               RcdFound204 = 0;
               InitializeNonKey4N204( ) ;
            }
            Gx_mode = sMode204;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound204 = 0;
            InitializeNonKey4N204( ) ;
            sMode204 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode204;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4N204( ) ;
         if ( RcdFound204 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound204 = 0;
         /* Using cursor T004N10 */
         pr_default.execute(8, new Object[] {A155Servico_Codigo, A1839Check_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T004N10_A155Servico_Codigo[0] < A155Servico_Codigo ) || ( T004N10_A155Servico_Codigo[0] == A155Servico_Codigo ) && ( T004N10_A1839Check_Codigo[0] < A1839Check_Codigo ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T004N10_A155Servico_Codigo[0] > A155Servico_Codigo ) || ( T004N10_A155Servico_Codigo[0] == A155Servico_Codigo ) && ( T004N10_A1839Check_Codigo[0] > A1839Check_Codigo ) ) )
            {
               A155Servico_Codigo = T004N10_A155Servico_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
               A1839Check_Codigo = T004N10_A1839Check_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
               RcdFound204 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound204 = 0;
         /* Using cursor T004N11 */
         pr_default.execute(9, new Object[] {A155Servico_Codigo, A1839Check_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T004N11_A155Servico_Codigo[0] > A155Servico_Codigo ) || ( T004N11_A155Servico_Codigo[0] == A155Servico_Codigo ) && ( T004N11_A1839Check_Codigo[0] > A1839Check_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T004N11_A155Servico_Codigo[0] < A155Servico_Codigo ) || ( T004N11_A155Servico_Codigo[0] == A155Servico_Codigo ) && ( T004N11_A1839Check_Codigo[0] < A1839Check_Codigo ) ) )
            {
               A155Servico_Codigo = T004N11_A155Servico_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
               A1839Check_Codigo = T004N11_A1839Check_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
               RcdFound204 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey4N204( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = dynServico_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert4N204( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound204 == 1 )
            {
               if ( ( A155Servico_Codigo != Z155Servico_Codigo ) || ( A1839Check_Codigo != Z1839Check_Codigo ) )
               {
                  A155Servico_Codigo = Z155Servico_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
                  A1839Check_Codigo = Z1839Check_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "SERVICO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = dynServico_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = dynServico_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update4N204( ) ;
                  GX_FocusControl = dynServico_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A155Servico_Codigo != Z155Servico_Codigo ) || ( A1839Check_Codigo != Z1839Check_Codigo ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = dynServico_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert4N204( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "SERVICO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = dynServico_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = dynServico_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert4N204( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( ( A155Servico_Codigo != Z155Servico_Codigo ) || ( A1839Check_Codigo != Z1839Check_Codigo ) )
         {
            A155Servico_Codigo = Z155Servico_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            A1839Check_Codigo = Z1839Check_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "SERVICO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynServico_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = dynServico_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound204 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "SERVICO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynServico_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart4N204( ) ;
         if ( RcdFound204 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         ScanEnd4N204( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound204 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound204 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart4N204( ) ;
         if ( RcdFound204 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound204 != 0 )
            {
               ScanNext4N204( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         ScanEnd4N204( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency4N204( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T004N2 */
            pr_default.execute(0, new Object[] {A155Servico_Codigo, A1839Check_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ServicoCheck"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ServicoCheck"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4N204( )
      {
         BeforeValidate4N204( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4N204( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4N204( 0) ;
            CheckOptimisticConcurrency4N204( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4N204( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4N204( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004N12 */
                     pr_default.execute(10, new Object[] {A155Servico_Codigo, A1839Check_Codigo});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("ServicoCheck") ;
                     if ( (pr_default.getStatus(10) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption4N0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4N204( ) ;
            }
            EndLevel4N204( ) ;
         }
         CloseExtendedTableCursors4N204( ) ;
      }

      protected void Update4N204( )
      {
         BeforeValidate4N204( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4N204( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4N204( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4N204( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4N204( ) ;
                  if ( AnyError == 0 )
                  {
                     /* No attributes to update on table [ServicoCheck] */
                     DeferredUpdate4N204( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption4N0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4N204( ) ;
         }
         CloseExtendedTableCursors4N204( ) ;
      }

      protected void DeferredUpdate4N204( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate4N204( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4N204( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4N204( ) ;
            AfterConfirm4N204( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4N204( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T004N13 */
                  pr_default.execute(11, new Object[] {A155Servico_Codigo, A1839Check_Codigo});
                  pr_default.close(11);
                  dsDefault.SmartCacheProvider.SetUpdated("ServicoCheck") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound204 == 0 )
                        {
                           InitAll4N204( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption4N0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode204 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel4N204( ) ;
         Gx_mode = sMode204;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls4N204( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T004N14 */
            pr_default.execute(12, new Object[] {A155Servico_Codigo});
            A605Servico_Sigla = T004N14_A605Servico_Sigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A605Servico_Sigla", A605Servico_Sigla);
            pr_default.close(12);
            /* Using cursor T004N15 */
            pr_default.execute(13, new Object[] {A1839Check_Codigo});
            A1841Check_Nome = T004N15_A1841Check_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1841Check_Nome", A1841Check_Nome);
            pr_default.close(13);
         }
      }

      protected void EndLevel4N204( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4N204( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(12);
            pr_default.close(13);
            context.CommitDataStores( "ServicoCheck");
            if ( AnyError == 0 )
            {
               ConfirmValues4N0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(12);
            pr_default.close(13);
            context.RollbackDataStores( "ServicoCheck");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart4N204( )
      {
         /* Using cursor T004N16 */
         pr_default.execute(14);
         RcdFound204 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound204 = 1;
            A155Servico_Codigo = T004N16_A155Servico_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            A1839Check_Codigo = T004N16_A1839Check_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext4N204( )
      {
         /* Scan next routine */
         pr_default.readNext(14);
         RcdFound204 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound204 = 1;
            A155Servico_Codigo = T004N16_A155Servico_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            A1839Check_Codigo = T004N16_A1839Check_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd4N204( )
      {
         pr_default.close(14);
      }

      protected void AfterConfirm4N204( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4N204( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4N204( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4N204( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4N204( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4N204( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4N204( )
      {
         dynServico_Codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServico_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynServico_Codigo.Enabled), 5, 0)));
         edtServico_Sigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Sigla_Enabled), 5, 0)));
         edtCheck_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCheck_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCheck_Codigo_Enabled), 5, 0)));
         edtCheck_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCheck_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCheck_Nome_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues4N0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020428239303");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("servicocheck.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z155Servico_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z155Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1839Check_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1839Check_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("servicocheck.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "ServicoCheck" ;
      }

      public override String GetPgmdesc( )
      {
         return "Check Lists" ;
      }

      protected void InitializeNonKey4N204( )
      {
         A605Servico_Sigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A605Servico_Sigla", A605Servico_Sigla);
         A1841Check_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1841Check_Nome", A1841Check_Nome);
      }

      protected void InitAll4N204( )
      {
         A155Servico_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         A1839Check_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
         InitializeNonKey4N204( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020428239306");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("servicocheck.js", "?2020428239306");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockservico_codigo_Internalname = "TEXTBLOCKSERVICO_CODIGO";
         dynServico_Codigo_Internalname = "SERVICO_CODIGO";
         lblTextblockservico_sigla_Internalname = "TEXTBLOCKSERVICO_SIGLA";
         edtServico_Sigla_Internalname = "SERVICO_SIGLA";
         lblTextblockcheck_codigo_Internalname = "TEXTBLOCKCHECK_CODIGO";
         edtCheck_Codigo_Internalname = "CHECK_CODIGO";
         lblTextblockcheck_nome_Internalname = "TEXTBLOCKCHECK_NOME";
         edtCheck_Nome_Internalname = "CHECK_NOME";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Check Lists";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtCheck_Nome_Jsonclick = "";
         edtCheck_Nome_Enabled = 0;
         edtCheck_Codigo_Jsonclick = "";
         edtCheck_Codigo_Enabled = 1;
         edtServico_Sigla_Jsonclick = "";
         edtServico_Sigla_Enabled = 0;
         dynServico_Codigo_Jsonclick = "";
         dynServico_Codigo.Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLASERVICO_CODIGO4N204( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLASERVICO_CODIGO_data4N204( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXASERVICO_CODIGO_html4N204( )
      {
         int gxdynajaxvalue ;
         GXDLASERVICO_CODIGO_data4N204( ) ;
         gxdynajaxindex = 1;
         dynServico_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynServico_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLASERVICO_CODIGO_data4N204( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor T004N17 */
         pr_default.execute(15);
         while ( (pr_default.getStatus(15) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T004N17_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T004N17_A608Servico_Nome[0]));
            pr_default.readNext(15);
         }
         pr_default.close(15);
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         /* Using cursor T004N18 */
         pr_default.execute(16, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(16) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico'.", "ForeignKeyNotFound", 1, "SERVICO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynServico_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A605Servico_Sigla = T004N18_A605Servico_Sigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A605Servico_Sigla", A605Servico_Sigla);
         pr_default.close(16);
         /* Using cursor T004N19 */
         pr_default.execute(17, new Object[] {A1839Check_Codigo});
         if ( (pr_default.getStatus(17) == 101) )
         {
            GX_msglist.addItem("N�o existe ' T204'.", "ForeignKeyNotFound", 1, "CHECK_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtCheck_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1841Check_Nome = T004N19_A1841Check_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1841Check_Nome", A1841Check_Nome);
         pr_default.close(17);
         if ( AnyError == 0 )
         {
            GX_FocusControl = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Servico_codigo( GXCombobox dynGX_Parm1 ,
                                        String GX_Parm2 )
      {
         dynServico_Codigo = dynGX_Parm1;
         A155Servico_Codigo = (int)(NumberUtil.Val( dynServico_Codigo.CurrentValue, "."));
         A605Servico_Sigla = GX_Parm2;
         /* Using cursor T004N20 */
         pr_default.execute(18, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(18) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico'.", "ForeignKeyNotFound", 1, "SERVICO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynServico_Codigo_Internalname;
         }
         A605Servico_Sigla = T004N20_A605Servico_Sigla[0];
         pr_default.close(18);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A605Servico_Sigla = "";
         }
         isValidOutput.Add(StringUtil.RTrim( A605Servico_Sigla));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Check_codigo( GXCombobox dynGX_Parm1 ,
                                      int GX_Parm2 ,
                                      String GX_Parm3 )
      {
         dynServico_Codigo = dynGX_Parm1;
         A155Servico_Codigo = (int)(NumberUtil.Val( dynServico_Codigo.CurrentValue, "."));
         A1839Check_Codigo = GX_Parm2;
         A1841Check_Nome = GX_Parm3;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         /* Using cursor T004N21 */
         pr_default.execute(19, new Object[] {A1839Check_Codigo});
         if ( (pr_default.getStatus(19) == 101) )
         {
            GX_msglist.addItem("N�o existe ' T204'.", "ForeignKeyNotFound", 1, "CHECK_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtCheck_Codigo_Internalname;
         }
         A1841Check_Nome = T004N21_A1841Check_Nome[0];
         pr_default.close(19);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A605Servico_Sigla = "";
            A1841Check_Nome = "";
         }
         isValidOutput.Add(StringUtil.RTrim( A605Servico_Sigla));
         isValidOutput.Add(StringUtil.RTrim( A1841Check_Nome));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z155Servico_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1839Check_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.RTrim( Z605Servico_Sigla));
         isValidOutput.Add(StringUtil.RTrim( Z1841Check_Nome));
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(18);
         pr_default.close(16);
         pr_default.close(12);
         pr_default.close(19);
         pr_default.close(17);
         pr_default.close(13);
      }

      public override void initialize( )
      {
         sPrefix = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockservico_codigo_Jsonclick = "";
         lblTextblockservico_sigla_Jsonclick = "";
         A605Servico_Sigla = "";
         lblTextblockcheck_codigo_Jsonclick = "";
         lblTextblockcheck_nome_Jsonclick = "";
         A1841Check_Nome = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z605Servico_Sigla = "";
         Z1841Check_Nome = "";
         T004N4_A605Servico_Sigla = new String[] {""} ;
         T004N6_A605Servico_Sigla = new String[] {""} ;
         T004N6_A1841Check_Nome = new String[] {""} ;
         T004N6_A155Servico_Codigo = new int[1] ;
         T004N6_A1839Check_Codigo = new int[1] ;
         T004N5_A1841Check_Nome = new String[] {""} ;
         T004N7_A605Servico_Sigla = new String[] {""} ;
         T004N8_A1841Check_Nome = new String[] {""} ;
         T004N9_A155Servico_Codigo = new int[1] ;
         T004N9_A1839Check_Codigo = new int[1] ;
         T004N3_A155Servico_Codigo = new int[1] ;
         T004N3_A1839Check_Codigo = new int[1] ;
         sMode204 = "";
         T004N10_A155Servico_Codigo = new int[1] ;
         T004N10_A1839Check_Codigo = new int[1] ;
         T004N11_A155Servico_Codigo = new int[1] ;
         T004N11_A1839Check_Codigo = new int[1] ;
         T004N2_A155Servico_Codigo = new int[1] ;
         T004N2_A1839Check_Codigo = new int[1] ;
         T004N14_A605Servico_Sigla = new String[] {""} ;
         T004N15_A1841Check_Nome = new String[] {""} ;
         T004N16_A155Servico_Codigo = new int[1] ;
         T004N16_A1839Check_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T004N17_A155Servico_Codigo = new int[1] ;
         T004N17_A608Servico_Nome = new String[] {""} ;
         T004N17_A631Servico_Vinculado = new int[1] ;
         T004N17_n631Servico_Vinculado = new bool[] {false} ;
         T004N17_A632Servico_Ativo = new bool[] {false} ;
         T004N18_A605Servico_Sigla = new String[] {""} ;
         T004N19_A1841Check_Nome = new String[] {""} ;
         T004N20_A605Servico_Sigla = new String[] {""} ;
         isValidOutput = new GxUnknownObjectCollection();
         T004N21_A1841Check_Nome = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.servicocheck__default(),
            new Object[][] {
                new Object[] {
               T004N2_A155Servico_Codigo, T004N2_A1839Check_Codigo
               }
               , new Object[] {
               T004N3_A155Servico_Codigo, T004N3_A1839Check_Codigo
               }
               , new Object[] {
               T004N4_A605Servico_Sigla
               }
               , new Object[] {
               T004N5_A1841Check_Nome
               }
               , new Object[] {
               T004N6_A605Servico_Sigla, T004N6_A1841Check_Nome, T004N6_A155Servico_Codigo, T004N6_A1839Check_Codigo
               }
               , new Object[] {
               T004N7_A605Servico_Sigla
               }
               , new Object[] {
               T004N8_A1841Check_Nome
               }
               , new Object[] {
               T004N9_A155Servico_Codigo, T004N9_A1839Check_Codigo
               }
               , new Object[] {
               T004N10_A155Servico_Codigo, T004N10_A1839Check_Codigo
               }
               , new Object[] {
               T004N11_A155Servico_Codigo, T004N11_A1839Check_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T004N14_A605Servico_Sigla
               }
               , new Object[] {
               T004N15_A1841Check_Nome
               }
               , new Object[] {
               T004N16_A155Servico_Codigo, T004N16_A1839Check_Codigo
               }
               , new Object[] {
               T004N17_A155Servico_Codigo, T004N17_A608Servico_Nome, T004N17_A631Servico_Vinculado, T004N17_n631Servico_Vinculado, T004N17_A632Servico_Ativo
               }
               , new Object[] {
               T004N18_A605Servico_Sigla
               }
               , new Object[] {
               T004N19_A1841Check_Nome
               }
               , new Object[] {
               T004N20_A605Servico_Sigla
               }
               , new Object[] {
               T004N21_A1841Check_Nome
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short RcdFound204 ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z155Servico_Codigo ;
      private int Z1839Check_Codigo ;
      private int A155Servico_Codigo ;
      private int A1839Check_Codigo ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int edtServico_Sigla_Enabled ;
      private int edtCheck_Codigo_Enabled ;
      private int edtCheck_Nome_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String dynServico_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockservico_codigo_Internalname ;
      private String lblTextblockservico_codigo_Jsonclick ;
      private String dynServico_Codigo_Jsonclick ;
      private String lblTextblockservico_sigla_Internalname ;
      private String lblTextblockservico_sigla_Jsonclick ;
      private String edtServico_Sigla_Internalname ;
      private String A605Servico_Sigla ;
      private String edtServico_Sigla_Jsonclick ;
      private String lblTextblockcheck_codigo_Internalname ;
      private String lblTextblockcheck_codigo_Jsonclick ;
      private String edtCheck_Codigo_Internalname ;
      private String edtCheck_Codigo_Jsonclick ;
      private String lblTextblockcheck_nome_Internalname ;
      private String lblTextblockcheck_nome_Jsonclick ;
      private String edtCheck_Nome_Internalname ;
      private String A1841Check_Nome ;
      private String edtCheck_Nome_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z605Servico_Sigla ;
      private String Z1841Check_Nome ;
      private String sMode204 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String gxwrpcisep ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynServico_Codigo ;
      private IDataStoreProvider pr_default ;
      private String[] T004N4_A605Servico_Sigla ;
      private String[] T004N6_A605Servico_Sigla ;
      private String[] T004N6_A1841Check_Nome ;
      private int[] T004N6_A155Servico_Codigo ;
      private int[] T004N6_A1839Check_Codigo ;
      private String[] T004N5_A1841Check_Nome ;
      private String[] T004N7_A605Servico_Sigla ;
      private String[] T004N8_A1841Check_Nome ;
      private int[] T004N9_A155Servico_Codigo ;
      private int[] T004N9_A1839Check_Codigo ;
      private int[] T004N3_A155Servico_Codigo ;
      private int[] T004N3_A1839Check_Codigo ;
      private int[] T004N10_A155Servico_Codigo ;
      private int[] T004N10_A1839Check_Codigo ;
      private int[] T004N11_A155Servico_Codigo ;
      private int[] T004N11_A1839Check_Codigo ;
      private int[] T004N2_A155Servico_Codigo ;
      private int[] T004N2_A1839Check_Codigo ;
      private String[] T004N14_A605Servico_Sigla ;
      private String[] T004N15_A1841Check_Nome ;
      private int[] T004N16_A155Servico_Codigo ;
      private int[] T004N16_A1839Check_Codigo ;
      private int[] T004N17_A155Servico_Codigo ;
      private String[] T004N17_A608Servico_Nome ;
      private int[] T004N17_A631Servico_Vinculado ;
      private bool[] T004N17_n631Servico_Vinculado ;
      private bool[] T004N17_A632Servico_Ativo ;
      private String[] T004N18_A605Servico_Sigla ;
      private String[] T004N19_A1841Check_Nome ;
      private String[] T004N20_A605Servico_Sigla ;
      private String[] T004N21_A1841Check_Nome ;
      private GXWebForm Form ;
   }

   public class servicocheck__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT004N6 ;
          prmT004N6 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004N4 ;
          prmT004N4 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004N5 ;
          prmT004N5 = new Object[] {
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004N7 ;
          prmT004N7 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004N8 ;
          prmT004N8 = new Object[] {
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004N9 ;
          prmT004N9 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004N3 ;
          prmT004N3 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004N10 ;
          prmT004N10 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004N11 ;
          prmT004N11 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004N2 ;
          prmT004N2 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004N12 ;
          prmT004N12 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004N13 ;
          prmT004N13 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004N14 ;
          prmT004N14 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004N15 ;
          prmT004N15 = new Object[] {
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004N16 ;
          prmT004N16 = new Object[] {
          } ;
          Object[] prmT004N17 ;
          prmT004N17 = new Object[] {
          } ;
          Object[] prmT004N18 ;
          prmT004N18 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004N19 ;
          prmT004N19 = new Object[] {
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004N20 ;
          prmT004N20 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004N21 ;
          prmT004N21 = new Object[] {
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T004N2", "SELECT [Servico_Codigo], [Check_Codigo] FROM [ServicoCheck] WITH (UPDLOCK) WHERE [Servico_Codigo] = @Servico_Codigo AND [Check_Codigo] = @Check_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004N2,1,0,true,false )
             ,new CursorDef("T004N3", "SELECT [Servico_Codigo], [Check_Codigo] FROM [ServicoCheck] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo AND [Check_Codigo] = @Check_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004N3,1,0,true,false )
             ,new CursorDef("T004N4", "SELECT [Servico_Sigla] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004N4,1,0,true,false )
             ,new CursorDef("T004N5", "SELECT [Check_Nome] FROM [Check] WITH (NOLOCK) WHERE [Check_Codigo] = @Check_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004N5,1,0,true,false )
             ,new CursorDef("T004N6", "SELECT T2.[Servico_Sigla], T3.[Check_Nome], TM1.[Servico_Codigo], TM1.[Check_Codigo] FROM (([ServicoCheck] TM1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = TM1.[Servico_Codigo]) INNER JOIN [Check] T3 WITH (NOLOCK) ON T3.[Check_Codigo] = TM1.[Check_Codigo]) WHERE TM1.[Servico_Codigo] = @Servico_Codigo and TM1.[Check_Codigo] = @Check_Codigo ORDER BY TM1.[Servico_Codigo], TM1.[Check_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004N6,100,0,true,false )
             ,new CursorDef("T004N7", "SELECT [Servico_Sigla] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004N7,1,0,true,false )
             ,new CursorDef("T004N8", "SELECT [Check_Nome] FROM [Check] WITH (NOLOCK) WHERE [Check_Codigo] = @Check_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004N8,1,0,true,false )
             ,new CursorDef("T004N9", "SELECT [Servico_Codigo], [Check_Codigo] FROM [ServicoCheck] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo AND [Check_Codigo] = @Check_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004N9,1,0,true,false )
             ,new CursorDef("T004N10", "SELECT TOP 1 [Servico_Codigo], [Check_Codigo] FROM [ServicoCheck] WITH (NOLOCK) WHERE ( [Servico_Codigo] > @Servico_Codigo or [Servico_Codigo] = @Servico_Codigo and [Check_Codigo] > @Check_Codigo) ORDER BY [Servico_Codigo], [Check_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004N10,1,0,true,true )
             ,new CursorDef("T004N11", "SELECT TOP 1 [Servico_Codigo], [Check_Codigo] FROM [ServicoCheck] WITH (NOLOCK) WHERE ( [Servico_Codigo] < @Servico_Codigo or [Servico_Codigo] = @Servico_Codigo and [Check_Codigo] < @Check_Codigo) ORDER BY [Servico_Codigo] DESC, [Check_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004N11,1,0,true,true )
             ,new CursorDef("T004N12", "INSERT INTO [ServicoCheck]([Servico_Codigo], [Check_Codigo]) VALUES(@Servico_Codigo, @Check_Codigo)", GxErrorMask.GX_NOMASK,prmT004N12)
             ,new CursorDef("T004N13", "DELETE FROM [ServicoCheck]  WHERE [Servico_Codigo] = @Servico_Codigo AND [Check_Codigo] = @Check_Codigo", GxErrorMask.GX_NOMASK,prmT004N13)
             ,new CursorDef("T004N14", "SELECT [Servico_Sigla] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004N14,1,0,true,false )
             ,new CursorDef("T004N15", "SELECT [Check_Nome] FROM [Check] WITH (NOLOCK) WHERE [Check_Codigo] = @Check_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004N15,1,0,true,false )
             ,new CursorDef("T004N16", "SELECT [Servico_Codigo], [Check_Codigo] FROM [ServicoCheck] WITH (NOLOCK) ORDER BY [Servico_Codigo], [Check_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004N16,100,0,true,false )
             ,new CursorDef("T004N17", "SELECT [Servico_Codigo], [Servico_Nome], [Servico_Vinculado], [Servico_Ativo] FROM [Servico] WITH (NOLOCK) WHERE (([Servico_Vinculado] = convert(int, 0))) AND ([Servico_Ativo] = 1) ORDER BY [Servico_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT004N17,0,0,true,false )
             ,new CursorDef("T004N18", "SELECT [Servico_Sigla] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004N18,1,0,true,false )
             ,new CursorDef("T004N19", "SELECT [Check_Nome] FROM [Check] WITH (NOLOCK) WHERE [Check_Codigo] = @Check_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004N19,1,0,true,false )
             ,new CursorDef("T004N20", "SELECT [Servico_Sigla] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004N20,1,0,true,false )
             ,new CursorDef("T004N21", "SELECT [Check_Nome] FROM [Check] WITH (NOLOCK) WHERE [Check_Codigo] = @Check_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004N21,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                return;
             case 16 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                return;
             case 19 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
