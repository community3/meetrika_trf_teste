/*
               File: type_SdtGAMCountryLanguages
        Description: GAMCountryLanguages
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 1:32:7.6
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMCountryLanguages : GxUserType, IGxExternalObject
   {
      public SdtGAMCountryLanguages( )
      {
         initialize();
      }

      public SdtGAMCountryLanguages( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMCountryLanguages_externalReference == null )
         {
            GAMCountryLanguages_externalReference = new Artech.Security.GAMCountryLanguages(context);
         }
         returntostring = "";
         returntostring = (String)(GAMCountryLanguages_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Id
      {
         get {
            if ( GAMCountryLanguages_externalReference == null )
            {
               GAMCountryLanguages_externalReference = new Artech.Security.GAMCountryLanguages(context);
            }
            return GAMCountryLanguages_externalReference.Id ;
         }

         set {
            if ( GAMCountryLanguages_externalReference == null )
            {
               GAMCountryLanguages_externalReference = new Artech.Security.GAMCountryLanguages(context);
            }
            GAMCountryLanguages_externalReference.Id = value;
         }

      }

      public String gxTpr_Name
      {
         get {
            if ( GAMCountryLanguages_externalReference == null )
            {
               GAMCountryLanguages_externalReference = new Artech.Security.GAMCountryLanguages(context);
            }
            return GAMCountryLanguages_externalReference.Name ;
         }

         set {
            if ( GAMCountryLanguages_externalReference == null )
            {
               GAMCountryLanguages_externalReference = new Artech.Security.GAMCountryLanguages(context);
            }
            GAMCountryLanguages_externalReference.Name = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMCountryLanguages_externalReference == null )
            {
               GAMCountryLanguages_externalReference = new Artech.Security.GAMCountryLanguages(context);
            }
            return GAMCountryLanguages_externalReference ;
         }

         set {
            GAMCountryLanguages_externalReference = (Artech.Security.GAMCountryLanguages)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMCountryLanguages GAMCountryLanguages_externalReference=null ;
   }

}
