/*
               File: type_SdtContratoServicos
        Description: Contrato Servicos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:16:50.66
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ContratoServicos" )]
   [XmlType(TypeName =  "ContratoServicos" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtContratoServicos_Rmn ))]
   [Serializable]
   public class SdtContratoServicos : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContratoServicos( )
      {
         /* Constructor for serialization */
         gxTv_SdtContratoServicos_Contrato_numero = "";
         gxTv_SdtContratoServicos_Contrato_numeroata = "";
         gxTv_SdtContratoServicos_Contratada_pessoanom = "";
         gxTv_SdtContratoServicos_Contratada_pessoacnpj = "";
         gxTv_SdtContratoServicos_Contratada_tipofabrica = "";
         gxTv_SdtContratoServicos_Contratoservicos_alias = "";
         gxTv_SdtContratoServicos_Servico_nome = "";
         gxTv_SdtContratoServicos_Servico_sigla = "";
         gxTv_SdtContratoServicos_Servico_tela = "";
         gxTv_SdtContratoServicos_Servico_identificacao = "";
         gxTv_SdtContratoServicos_Contratoservicos_servicosigla = "";
         gxTv_SdtContratoServicos_Servico_descricao = "";
         gxTv_SdtContratoServicos_Servicogrupo_descricao = "";
         gxTv_SdtContratoServicos_Contratoservicos_undcntnome = "";
         gxTv_SdtContratoServicos_Contratoservicos_undcntsgl = "";
         gxTv_SdtContratoServicos_Servicocontrato_faturamento = "";
         gxTv_SdtContratoServicos_Contratoservicos_localexec = "";
         gxTv_SdtContratoServicos_Contratoservicos_tipovnc = "";
         gxTv_SdtContratoServicos_Contratoservicos_prazotipo = "";
         gxTv_SdtContratoServicos_Contratoservicos_prazotpdias = "";
         gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo = "";
         gxTv_SdtContratoServicos_Contratoservicos_momento = "";
         gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc = "";
         gxTv_SdtContratoServicos_Contratoservicos_codigofiscal = "";
         gxTv_SdtContratoServicos_Mode = "";
         gxTv_SdtContratoServicos_Contrato_numero_Z = "";
         gxTv_SdtContratoServicos_Contrato_numeroata_Z = "";
         gxTv_SdtContratoServicos_Contratada_pessoanom_Z = "";
         gxTv_SdtContratoServicos_Contratada_pessoacnpj_Z = "";
         gxTv_SdtContratoServicos_Contratada_tipofabrica_Z = "";
         gxTv_SdtContratoServicos_Contratoservicos_alias_Z = "";
         gxTv_SdtContratoServicos_Servico_nome_Z = "";
         gxTv_SdtContratoServicos_Servico_sigla_Z = "";
         gxTv_SdtContratoServicos_Servico_tela_Z = "";
         gxTv_SdtContratoServicos_Servico_identificacao_Z = "";
         gxTv_SdtContratoServicos_Contratoservicos_servicosigla_Z = "";
         gxTv_SdtContratoServicos_Servicogrupo_descricao_Z = "";
         gxTv_SdtContratoServicos_Contratoservicos_undcntnome_Z = "";
         gxTv_SdtContratoServicos_Contratoservicos_undcntsgl_Z = "";
         gxTv_SdtContratoServicos_Servicocontrato_faturamento_Z = "";
         gxTv_SdtContratoServicos_Contratoservicos_localexec_Z = "";
         gxTv_SdtContratoServicos_Contratoservicos_tipovnc_Z = "";
         gxTv_SdtContratoServicos_Contratoservicos_prazotipo_Z = "";
         gxTv_SdtContratoServicos_Contratoservicos_prazotpdias_Z = "";
         gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo_Z = "";
         gxTv_SdtContratoServicos_Contratoservicos_momento_Z = "";
         gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc_Z = "";
         gxTv_SdtContratoServicos_Contratoservicos_codigofiscal_Z = "";
      }

      public SdtContratoServicos( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV160ContratoServicos_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV160ContratoServicos_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"ContratoServicos_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "ContratoServicos");
         metadata.Set("BT", "ContratoServicos");
         metadata.Set("PK", "[ \"ContratoServicos_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"ContratoServicos_Codigo\" ]");
         metadata.Set("Levels", "[ \"Rmn\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Contrato_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"Servico_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"UnidadeMedicao_Codigo\" ],\"FKMap\":[ \"ContratoServicos_UnidadeContratada-UnidadeMedicao_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Rmn_GxSilentTrnGridCollection" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_numero_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_numeroata_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_ano_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_valorunidadecontratacao_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_areatrabalhocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_pessoacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_pessoanom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_pessoacnpj_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_tipofabrica_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_alias_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_servicocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_sigla_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_tela_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_ativo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_identificacao_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_servicosigla_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_vinculado_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_vinculados_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_responsavel_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicogrupo_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicogrupo_descricao_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_isorigemreferencia_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_unidadecontratada_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_undcntnome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_undcntsgl_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_qtdcontratada_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_vlrunidadecontratada_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_percentual_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicocontrato_faturamento_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_localexec_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_tipovnc_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_hmlsemcnf_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_prazotipo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_prazodias_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_prazotpdias_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_prazos_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_indicadores_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_prazoanalise_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_prazoresposta_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_prazogarantia_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_prazoatendegarantia_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_prazocorrecao_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_prazocorrecaotipo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_prazoinicio_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_prazoimediato_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_produtividade_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_espelhaaceite_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_momento_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_statuspagfnc_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_qntuntcns_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_fatorcnvundcnt_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_naorequeratr_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_indicedivergencia_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_indicedivergencia_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_tmpestanl_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_tmpestexc_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_tmpestcrr_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_tipohierarquia_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_perctmp_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_percpgm_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_perccnc_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_ativo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_codigofiscal_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_limiteproposta_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_calculormn_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_qtdrmn_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_solicitagestorsistema_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_numeroata_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_pessoanom_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_pessoacnpj_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_alias_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_tela_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_descricao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_vinculado_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_undcntnome_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_undcntsgl_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_qtdcontratada_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_percentual_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_tipovnc_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_hmlsemcnf_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_prazotipo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_prazodias_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_prazotpdias_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_prazos_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_indicadores_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_prazoanalise_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_prazoresposta_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_prazogarantia_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_prazoatendegarantia_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_prazocorrecao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_prazocorrecaotipo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_prazoinicio_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_prazoimediato_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_produtividade_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_espelhaaceite_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_momento_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_statuspagfnc_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_qntuntcns_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_fatorcnvundcnt_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_naorequeratr_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_indicedivergencia_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_tmpestanl_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_tmpestexc_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_tmpestcrr_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_tipohierarquia_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_perctmp_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_percpgm_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_perccnc_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_codigofiscal_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_limiteproposta_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_calculormn_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_qtdrmn_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtContratoServicos deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtContratoServicos)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtContratoServicos obj ;
         obj = this;
         obj.gxTpr_Contratoservicos_codigo = deserialized.gxTpr_Contratoservicos_codigo;
         obj.gxTpr_Contrato_codigo = deserialized.gxTpr_Contrato_codigo;
         obj.gxTpr_Contrato_numero = deserialized.gxTpr_Contrato_numero;
         obj.gxTpr_Contrato_numeroata = deserialized.gxTpr_Contrato_numeroata;
         obj.gxTpr_Contrato_ano = deserialized.gxTpr_Contrato_ano;
         obj.gxTpr_Contrato_valorunidadecontratacao = deserialized.gxTpr_Contrato_valorunidadecontratacao;
         obj.gxTpr_Contrato_areatrabalhocod = deserialized.gxTpr_Contrato_areatrabalhocod;
         obj.gxTpr_Contratada_codigo = deserialized.gxTpr_Contratada_codigo;
         obj.gxTpr_Contratada_pessoacod = deserialized.gxTpr_Contratada_pessoacod;
         obj.gxTpr_Contratada_pessoanom = deserialized.gxTpr_Contratada_pessoanom;
         obj.gxTpr_Contratada_pessoacnpj = deserialized.gxTpr_Contratada_pessoacnpj;
         obj.gxTpr_Contratada_tipofabrica = deserialized.gxTpr_Contratada_tipofabrica;
         obj.gxTpr_Servico_codigo = deserialized.gxTpr_Servico_codigo;
         obj.gxTpr_Contratoservicos_alias = deserialized.gxTpr_Contratoservicos_alias;
         obj.gxTpr_Contratoservicos_servicocod = deserialized.gxTpr_Contratoservicos_servicocod;
         obj.gxTpr_Servico_nome = deserialized.gxTpr_Servico_nome;
         obj.gxTpr_Servico_sigla = deserialized.gxTpr_Servico_sigla;
         obj.gxTpr_Servico_tela = deserialized.gxTpr_Servico_tela;
         obj.gxTpr_Servico_ativo = deserialized.gxTpr_Servico_ativo;
         obj.gxTpr_Servico_identificacao = deserialized.gxTpr_Servico_identificacao;
         obj.gxTpr_Contratoservicos_servicosigla = deserialized.gxTpr_Contratoservicos_servicosigla;
         obj.gxTpr_Servico_descricao = deserialized.gxTpr_Servico_descricao;
         obj.gxTpr_Servico_vinculado = deserialized.gxTpr_Servico_vinculado;
         obj.gxTpr_Servico_vinculados = deserialized.gxTpr_Servico_vinculados;
         obj.gxTpr_Servico_responsavel = deserialized.gxTpr_Servico_responsavel;
         obj.gxTpr_Servicogrupo_codigo = deserialized.gxTpr_Servicogrupo_codigo;
         obj.gxTpr_Servicogrupo_descricao = deserialized.gxTpr_Servicogrupo_descricao;
         obj.gxTpr_Servico_isorigemreferencia = deserialized.gxTpr_Servico_isorigemreferencia;
         obj.gxTpr_Contratoservicos_unidadecontratada = deserialized.gxTpr_Contratoservicos_unidadecontratada;
         obj.gxTpr_Contratoservicos_undcntnome = deserialized.gxTpr_Contratoservicos_undcntnome;
         obj.gxTpr_Contratoservicos_undcntsgl = deserialized.gxTpr_Contratoservicos_undcntsgl;
         obj.gxTpr_Servico_qtdcontratada = deserialized.gxTpr_Servico_qtdcontratada;
         obj.gxTpr_Servico_vlrunidadecontratada = deserialized.gxTpr_Servico_vlrunidadecontratada;
         obj.gxTpr_Servico_percentual = deserialized.gxTpr_Servico_percentual;
         obj.gxTpr_Servicocontrato_faturamento = deserialized.gxTpr_Servicocontrato_faturamento;
         obj.gxTpr_Contratoservicos_localexec = deserialized.gxTpr_Contratoservicos_localexec;
         obj.gxTpr_Contratoservicos_tipovnc = deserialized.gxTpr_Contratoservicos_tipovnc;
         obj.gxTpr_Contratoservicos_hmlsemcnf = deserialized.gxTpr_Contratoservicos_hmlsemcnf;
         obj.gxTpr_Contratoservicos_prazotipo = deserialized.gxTpr_Contratoservicos_prazotipo;
         obj.gxTpr_Contratoservicos_prazodias = deserialized.gxTpr_Contratoservicos_prazodias;
         obj.gxTpr_Contratoservicos_prazotpdias = deserialized.gxTpr_Contratoservicos_prazotpdias;
         obj.gxTpr_Contratoservicos_prazos = deserialized.gxTpr_Contratoservicos_prazos;
         obj.gxTpr_Contratoservicos_indicadores = deserialized.gxTpr_Contratoservicos_indicadores;
         obj.gxTpr_Contratoservicos_prazoanalise = deserialized.gxTpr_Contratoservicos_prazoanalise;
         obj.gxTpr_Contratoservicos_prazoresposta = deserialized.gxTpr_Contratoservicos_prazoresposta;
         obj.gxTpr_Contratoservicos_prazogarantia = deserialized.gxTpr_Contratoservicos_prazogarantia;
         obj.gxTpr_Contratoservicos_prazoatendegarantia = deserialized.gxTpr_Contratoservicos_prazoatendegarantia;
         obj.gxTpr_Contratoservicos_prazocorrecao = deserialized.gxTpr_Contratoservicos_prazocorrecao;
         obj.gxTpr_Contratoservicos_prazocorrecaotipo = deserialized.gxTpr_Contratoservicos_prazocorrecaotipo;
         obj.gxTpr_Contratoservicos_prazoinicio = deserialized.gxTpr_Contratoservicos_prazoinicio;
         obj.gxTpr_Contratoservicos_prazoimediato = deserialized.gxTpr_Contratoservicos_prazoimediato;
         obj.gxTpr_Contratoservicos_produtividade = deserialized.gxTpr_Contratoservicos_produtividade;
         obj.gxTpr_Contratoservicos_espelhaaceite = deserialized.gxTpr_Contratoservicos_espelhaaceite;
         obj.gxTpr_Contratoservicos_momento = deserialized.gxTpr_Contratoservicos_momento;
         obj.gxTpr_Contratoservicos_statuspagfnc = deserialized.gxTpr_Contratoservicos_statuspagfnc;
         obj.gxTpr_Contratoservicos_qntuntcns = deserialized.gxTpr_Contratoservicos_qntuntcns;
         obj.gxTpr_Contratoservicos_fatorcnvundcnt = deserialized.gxTpr_Contratoservicos_fatorcnvundcnt;
         obj.gxTpr_Contratoservicos_naorequeratr = deserialized.gxTpr_Contratoservicos_naorequeratr;
         obj.gxTpr_Contrato_indicedivergencia = deserialized.gxTpr_Contrato_indicedivergencia;
         obj.gxTpr_Contratoservicos_indicedivergencia = deserialized.gxTpr_Contratoservicos_indicedivergencia;
         obj.gxTpr_Contratoservicos_tmpestanl = deserialized.gxTpr_Contratoservicos_tmpestanl;
         obj.gxTpr_Contratoservicos_tmpestexc = deserialized.gxTpr_Contratoservicos_tmpestexc;
         obj.gxTpr_Contratoservicos_tmpestcrr = deserialized.gxTpr_Contratoservicos_tmpestcrr;
         obj.gxTpr_Contratoservicos_tipohierarquia = deserialized.gxTpr_Contratoservicos_tipohierarquia;
         obj.gxTpr_Contratoservicos_perctmp = deserialized.gxTpr_Contratoservicos_perctmp;
         obj.gxTpr_Contratoservicos_percpgm = deserialized.gxTpr_Contratoservicos_percpgm;
         obj.gxTpr_Contratoservicos_perccnc = deserialized.gxTpr_Contratoservicos_perccnc;
         obj.gxTpr_Contratoservicos_ativo = deserialized.gxTpr_Contratoservicos_ativo;
         obj.gxTpr_Contratoservicos_codigofiscal = deserialized.gxTpr_Contratoservicos_codigofiscal;
         obj.gxTpr_Contratoservicos_limiteproposta = deserialized.gxTpr_Contratoservicos_limiteproposta;
         obj.gxTpr_Contratoservicos_calculormn = deserialized.gxTpr_Contratoservicos_calculormn;
         obj.gxTpr_Contratoservicos_qtdrmn = deserialized.gxTpr_Contratoservicos_qtdrmn;
         obj.gxTpr_Contratoservicos_solicitagestorsistema = deserialized.gxTpr_Contratoservicos_solicitagestorsistema;
         obj.gxTpr_Rmn = deserialized.gxTpr_Rmn;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Contratoservicos_codigo_Z = deserialized.gxTpr_Contratoservicos_codigo_Z;
         obj.gxTpr_Contrato_codigo_Z = deserialized.gxTpr_Contrato_codigo_Z;
         obj.gxTpr_Contrato_numero_Z = deserialized.gxTpr_Contrato_numero_Z;
         obj.gxTpr_Contrato_numeroata_Z = deserialized.gxTpr_Contrato_numeroata_Z;
         obj.gxTpr_Contrato_ano_Z = deserialized.gxTpr_Contrato_ano_Z;
         obj.gxTpr_Contrato_valorunidadecontratacao_Z = deserialized.gxTpr_Contrato_valorunidadecontratacao_Z;
         obj.gxTpr_Contrato_areatrabalhocod_Z = deserialized.gxTpr_Contrato_areatrabalhocod_Z;
         obj.gxTpr_Contratada_codigo_Z = deserialized.gxTpr_Contratada_codigo_Z;
         obj.gxTpr_Contratada_pessoacod_Z = deserialized.gxTpr_Contratada_pessoacod_Z;
         obj.gxTpr_Contratada_pessoanom_Z = deserialized.gxTpr_Contratada_pessoanom_Z;
         obj.gxTpr_Contratada_pessoacnpj_Z = deserialized.gxTpr_Contratada_pessoacnpj_Z;
         obj.gxTpr_Contratada_tipofabrica_Z = deserialized.gxTpr_Contratada_tipofabrica_Z;
         obj.gxTpr_Servico_codigo_Z = deserialized.gxTpr_Servico_codigo_Z;
         obj.gxTpr_Contratoservicos_alias_Z = deserialized.gxTpr_Contratoservicos_alias_Z;
         obj.gxTpr_Contratoservicos_servicocod_Z = deserialized.gxTpr_Contratoservicos_servicocod_Z;
         obj.gxTpr_Servico_nome_Z = deserialized.gxTpr_Servico_nome_Z;
         obj.gxTpr_Servico_sigla_Z = deserialized.gxTpr_Servico_sigla_Z;
         obj.gxTpr_Servico_tela_Z = deserialized.gxTpr_Servico_tela_Z;
         obj.gxTpr_Servico_ativo_Z = deserialized.gxTpr_Servico_ativo_Z;
         obj.gxTpr_Servico_identificacao_Z = deserialized.gxTpr_Servico_identificacao_Z;
         obj.gxTpr_Contratoservicos_servicosigla_Z = deserialized.gxTpr_Contratoservicos_servicosigla_Z;
         obj.gxTpr_Servico_vinculado_Z = deserialized.gxTpr_Servico_vinculado_Z;
         obj.gxTpr_Servico_vinculados_Z = deserialized.gxTpr_Servico_vinculados_Z;
         obj.gxTpr_Servico_responsavel_Z = deserialized.gxTpr_Servico_responsavel_Z;
         obj.gxTpr_Servicogrupo_codigo_Z = deserialized.gxTpr_Servicogrupo_codigo_Z;
         obj.gxTpr_Servicogrupo_descricao_Z = deserialized.gxTpr_Servicogrupo_descricao_Z;
         obj.gxTpr_Servico_isorigemreferencia_Z = deserialized.gxTpr_Servico_isorigemreferencia_Z;
         obj.gxTpr_Contratoservicos_unidadecontratada_Z = deserialized.gxTpr_Contratoservicos_unidadecontratada_Z;
         obj.gxTpr_Contratoservicos_undcntnome_Z = deserialized.gxTpr_Contratoservicos_undcntnome_Z;
         obj.gxTpr_Contratoservicos_undcntsgl_Z = deserialized.gxTpr_Contratoservicos_undcntsgl_Z;
         obj.gxTpr_Servico_qtdcontratada_Z = deserialized.gxTpr_Servico_qtdcontratada_Z;
         obj.gxTpr_Servico_vlrunidadecontratada_Z = deserialized.gxTpr_Servico_vlrunidadecontratada_Z;
         obj.gxTpr_Servico_percentual_Z = deserialized.gxTpr_Servico_percentual_Z;
         obj.gxTpr_Servicocontrato_faturamento_Z = deserialized.gxTpr_Servicocontrato_faturamento_Z;
         obj.gxTpr_Contratoservicos_localexec_Z = deserialized.gxTpr_Contratoservicos_localexec_Z;
         obj.gxTpr_Contratoservicos_tipovnc_Z = deserialized.gxTpr_Contratoservicos_tipovnc_Z;
         obj.gxTpr_Contratoservicos_hmlsemcnf_Z = deserialized.gxTpr_Contratoservicos_hmlsemcnf_Z;
         obj.gxTpr_Contratoservicos_prazotipo_Z = deserialized.gxTpr_Contratoservicos_prazotipo_Z;
         obj.gxTpr_Contratoservicos_prazodias_Z = deserialized.gxTpr_Contratoservicos_prazodias_Z;
         obj.gxTpr_Contratoservicos_prazotpdias_Z = deserialized.gxTpr_Contratoservicos_prazotpdias_Z;
         obj.gxTpr_Contratoservicos_prazos_Z = deserialized.gxTpr_Contratoservicos_prazos_Z;
         obj.gxTpr_Contratoservicos_indicadores_Z = deserialized.gxTpr_Contratoservicos_indicadores_Z;
         obj.gxTpr_Contratoservicos_prazoanalise_Z = deserialized.gxTpr_Contratoservicos_prazoanalise_Z;
         obj.gxTpr_Contratoservicos_prazoresposta_Z = deserialized.gxTpr_Contratoservicos_prazoresposta_Z;
         obj.gxTpr_Contratoservicos_prazogarantia_Z = deserialized.gxTpr_Contratoservicos_prazogarantia_Z;
         obj.gxTpr_Contratoservicos_prazoatendegarantia_Z = deserialized.gxTpr_Contratoservicos_prazoatendegarantia_Z;
         obj.gxTpr_Contratoservicos_prazocorrecao_Z = deserialized.gxTpr_Contratoservicos_prazocorrecao_Z;
         obj.gxTpr_Contratoservicos_prazocorrecaotipo_Z = deserialized.gxTpr_Contratoservicos_prazocorrecaotipo_Z;
         obj.gxTpr_Contratoservicos_prazoinicio_Z = deserialized.gxTpr_Contratoservicos_prazoinicio_Z;
         obj.gxTpr_Contratoservicos_prazoimediato_Z = deserialized.gxTpr_Contratoservicos_prazoimediato_Z;
         obj.gxTpr_Contratoservicos_produtividade_Z = deserialized.gxTpr_Contratoservicos_produtividade_Z;
         obj.gxTpr_Contratoservicos_espelhaaceite_Z = deserialized.gxTpr_Contratoservicos_espelhaaceite_Z;
         obj.gxTpr_Contratoservicos_momento_Z = deserialized.gxTpr_Contratoservicos_momento_Z;
         obj.gxTpr_Contratoservicos_statuspagfnc_Z = deserialized.gxTpr_Contratoservicos_statuspagfnc_Z;
         obj.gxTpr_Contratoservicos_qntuntcns_Z = deserialized.gxTpr_Contratoservicos_qntuntcns_Z;
         obj.gxTpr_Contratoservicos_fatorcnvundcnt_Z = deserialized.gxTpr_Contratoservicos_fatorcnvundcnt_Z;
         obj.gxTpr_Contratoservicos_naorequeratr_Z = deserialized.gxTpr_Contratoservicos_naorequeratr_Z;
         obj.gxTpr_Contrato_indicedivergencia_Z = deserialized.gxTpr_Contrato_indicedivergencia_Z;
         obj.gxTpr_Contratoservicos_indicedivergencia_Z = deserialized.gxTpr_Contratoservicos_indicedivergencia_Z;
         obj.gxTpr_Contratoservicos_tmpestanl_Z = deserialized.gxTpr_Contratoservicos_tmpestanl_Z;
         obj.gxTpr_Contratoservicos_tmpestexc_Z = deserialized.gxTpr_Contratoservicos_tmpestexc_Z;
         obj.gxTpr_Contratoservicos_tmpestcrr_Z = deserialized.gxTpr_Contratoservicos_tmpestcrr_Z;
         obj.gxTpr_Contratoservicos_tipohierarquia_Z = deserialized.gxTpr_Contratoservicos_tipohierarquia_Z;
         obj.gxTpr_Contratoservicos_perctmp_Z = deserialized.gxTpr_Contratoservicos_perctmp_Z;
         obj.gxTpr_Contratoservicos_percpgm_Z = deserialized.gxTpr_Contratoservicos_percpgm_Z;
         obj.gxTpr_Contratoservicos_perccnc_Z = deserialized.gxTpr_Contratoservicos_perccnc_Z;
         obj.gxTpr_Contratoservicos_ativo_Z = deserialized.gxTpr_Contratoservicos_ativo_Z;
         obj.gxTpr_Contratoservicos_codigofiscal_Z = deserialized.gxTpr_Contratoservicos_codigofiscal_Z;
         obj.gxTpr_Contratoservicos_limiteproposta_Z = deserialized.gxTpr_Contratoservicos_limiteproposta_Z;
         obj.gxTpr_Contratoservicos_calculormn_Z = deserialized.gxTpr_Contratoservicos_calculormn_Z;
         obj.gxTpr_Contratoservicos_qtdrmn_Z = deserialized.gxTpr_Contratoservicos_qtdrmn_Z;
         obj.gxTpr_Contratoservicos_solicitagestorsistema_Z = deserialized.gxTpr_Contratoservicos_solicitagestorsistema_Z;
         obj.gxTpr_Contrato_numeroata_N = deserialized.gxTpr_Contrato_numeroata_N;
         obj.gxTpr_Contratada_pessoanom_N = deserialized.gxTpr_Contratada_pessoanom_N;
         obj.gxTpr_Contratada_pessoacnpj_N = deserialized.gxTpr_Contratada_pessoacnpj_N;
         obj.gxTpr_Contratoservicos_alias_N = deserialized.gxTpr_Contratoservicos_alias_N;
         obj.gxTpr_Servico_tela_N = deserialized.gxTpr_Servico_tela_N;
         obj.gxTpr_Servico_descricao_N = deserialized.gxTpr_Servico_descricao_N;
         obj.gxTpr_Servico_vinculado_N = deserialized.gxTpr_Servico_vinculado_N;
         obj.gxTpr_Contratoservicos_undcntnome_N = deserialized.gxTpr_Contratoservicos_undcntnome_N;
         obj.gxTpr_Contratoservicos_undcntsgl_N = deserialized.gxTpr_Contratoservicos_undcntsgl_N;
         obj.gxTpr_Servico_qtdcontratada_N = deserialized.gxTpr_Servico_qtdcontratada_N;
         obj.gxTpr_Servico_percentual_N = deserialized.gxTpr_Servico_percentual_N;
         obj.gxTpr_Contratoservicos_tipovnc_N = deserialized.gxTpr_Contratoservicos_tipovnc_N;
         obj.gxTpr_Contratoservicos_hmlsemcnf_N = deserialized.gxTpr_Contratoservicos_hmlsemcnf_N;
         obj.gxTpr_Contratoservicos_prazotipo_N = deserialized.gxTpr_Contratoservicos_prazotipo_N;
         obj.gxTpr_Contratoservicos_prazodias_N = deserialized.gxTpr_Contratoservicos_prazodias_N;
         obj.gxTpr_Contratoservicos_prazotpdias_N = deserialized.gxTpr_Contratoservicos_prazotpdias_N;
         obj.gxTpr_Contratoservicos_prazos_N = deserialized.gxTpr_Contratoservicos_prazos_N;
         obj.gxTpr_Contratoservicos_indicadores_N = deserialized.gxTpr_Contratoservicos_indicadores_N;
         obj.gxTpr_Contratoservicos_prazoanalise_N = deserialized.gxTpr_Contratoservicos_prazoanalise_N;
         obj.gxTpr_Contratoservicos_prazoresposta_N = deserialized.gxTpr_Contratoservicos_prazoresposta_N;
         obj.gxTpr_Contratoservicos_prazogarantia_N = deserialized.gxTpr_Contratoservicos_prazogarantia_N;
         obj.gxTpr_Contratoservicos_prazoatendegarantia_N = deserialized.gxTpr_Contratoservicos_prazoatendegarantia_N;
         obj.gxTpr_Contratoservicos_prazocorrecao_N = deserialized.gxTpr_Contratoservicos_prazocorrecao_N;
         obj.gxTpr_Contratoservicos_prazocorrecaotipo_N = deserialized.gxTpr_Contratoservicos_prazocorrecaotipo_N;
         obj.gxTpr_Contratoservicos_prazoinicio_N = deserialized.gxTpr_Contratoservicos_prazoinicio_N;
         obj.gxTpr_Contratoservicos_prazoimediato_N = deserialized.gxTpr_Contratoservicos_prazoimediato_N;
         obj.gxTpr_Contratoservicos_produtividade_N = deserialized.gxTpr_Contratoservicos_produtividade_N;
         obj.gxTpr_Contratoservicos_espelhaaceite_N = deserialized.gxTpr_Contratoservicos_espelhaaceite_N;
         obj.gxTpr_Contratoservicos_momento_N = deserialized.gxTpr_Contratoservicos_momento_N;
         obj.gxTpr_Contratoservicos_statuspagfnc_N = deserialized.gxTpr_Contratoservicos_statuspagfnc_N;
         obj.gxTpr_Contratoservicos_qntuntcns_N = deserialized.gxTpr_Contratoservicos_qntuntcns_N;
         obj.gxTpr_Contratoservicos_fatorcnvundcnt_N = deserialized.gxTpr_Contratoservicos_fatorcnvundcnt_N;
         obj.gxTpr_Contratoservicos_naorequeratr_N = deserialized.gxTpr_Contratoservicos_naorequeratr_N;
         obj.gxTpr_Contratoservicos_indicedivergencia_N = deserialized.gxTpr_Contratoservicos_indicedivergencia_N;
         obj.gxTpr_Contratoservicos_tmpestanl_N = deserialized.gxTpr_Contratoservicos_tmpestanl_N;
         obj.gxTpr_Contratoservicos_tmpestexc_N = deserialized.gxTpr_Contratoservicos_tmpestexc_N;
         obj.gxTpr_Contratoservicos_tmpestcrr_N = deserialized.gxTpr_Contratoservicos_tmpestcrr_N;
         obj.gxTpr_Contratoservicos_tipohierarquia_N = deserialized.gxTpr_Contratoservicos_tipohierarquia_N;
         obj.gxTpr_Contratoservicos_perctmp_N = deserialized.gxTpr_Contratoservicos_perctmp_N;
         obj.gxTpr_Contratoservicos_percpgm_N = deserialized.gxTpr_Contratoservicos_percpgm_N;
         obj.gxTpr_Contratoservicos_perccnc_N = deserialized.gxTpr_Contratoservicos_perccnc_N;
         obj.gxTpr_Contratoservicos_codigofiscal_N = deserialized.gxTpr_Contratoservicos_codigofiscal_N;
         obj.gxTpr_Contratoservicos_limiteproposta_N = deserialized.gxTpr_Contratoservicos_limiteproposta_N;
         obj.gxTpr_Contratoservicos_calculormn_N = deserialized.gxTpr_Contratoservicos_calculormn_N;
         obj.gxTpr_Contratoservicos_qtdrmn_N = deserialized.gxTpr_Contratoservicos_qtdrmn_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_Codigo") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_Codigo") )
               {
                  gxTv_SdtContratoServicos_Contrato_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_Numero") )
               {
                  gxTv_SdtContratoServicos_Contrato_numero = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_NumeroAta") )
               {
                  gxTv_SdtContratoServicos_Contrato_numeroata = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_Ano") )
               {
                  gxTv_SdtContratoServicos_Contrato_ano = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_ValorUnidadeContratacao") )
               {
                  gxTv_SdtContratoServicos_Contrato_valorunidadecontratacao = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_AreaTrabalhoCod") )
               {
                  gxTv_SdtContratoServicos_Contrato_areatrabalhocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_Codigo") )
               {
                  gxTv_SdtContratoServicos_Contratada_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_PessoaCod") )
               {
                  gxTv_SdtContratoServicos_Contratada_pessoacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_PessoaNom") )
               {
                  gxTv_SdtContratoServicos_Contratada_pessoanom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_PessoaCNPJ") )
               {
                  gxTv_SdtContratoServicos_Contratada_pessoacnpj = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_TipoFabrica") )
               {
                  gxTv_SdtContratoServicos_Contratada_tipofabrica = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Codigo") )
               {
                  gxTv_SdtContratoServicos_Servico_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_Alias") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_alias = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_ServicoCod") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_servicocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Nome") )
               {
                  gxTv_SdtContratoServicos_Servico_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Sigla") )
               {
                  gxTv_SdtContratoServicos_Servico_sigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Tela") )
               {
                  gxTv_SdtContratoServicos_Servico_tela = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Ativo") )
               {
                  gxTv_SdtContratoServicos_Servico_ativo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Identificacao") )
               {
                  gxTv_SdtContratoServicos_Servico_identificacao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_ServicoSigla") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_servicosigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Descricao") )
               {
                  gxTv_SdtContratoServicos_Servico_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Vinculado") )
               {
                  gxTv_SdtContratoServicos_Servico_vinculado = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Vinculados") )
               {
                  gxTv_SdtContratoServicos_Servico_vinculados = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Responsavel") )
               {
                  gxTv_SdtContratoServicos_Servico_responsavel = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoGrupo_Codigo") )
               {
                  gxTv_SdtContratoServicos_Servicogrupo_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoGrupo_Descricao") )
               {
                  gxTv_SdtContratoServicos_Servicogrupo_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_IsOrigemReferencia") )
               {
                  gxTv_SdtContratoServicos_Servico_isorigemreferencia = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_UnidadeContratada") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_unidadecontratada = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_UndCntNome") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_undcntnome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_UndCntSgl") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_undcntsgl = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_QtdContratada") )
               {
                  gxTv_SdtContratoServicos_Servico_qtdcontratada = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_VlrUnidadeContratada") )
               {
                  gxTv_SdtContratoServicos_Servico_vlrunidadecontratada = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Percentual") )
               {
                  gxTv_SdtContratoServicos_Servico_percentual = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoContrato_Faturamento") )
               {
                  gxTv_SdtContratoServicos_Servicocontrato_faturamento = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_LocalExec") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_localexec = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_TipoVnc") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_tipovnc = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_HmlSemCnf") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoTipo") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazotipo = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoDias") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazodias = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoTpDias") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazotpdias = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_Prazos") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazos = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_Indicadores") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_indicadores = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoAnalise") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazoanalise = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoResposta") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazoresposta = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoGarantia") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazogarantia = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoAtendeGarantia") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoCorrecao") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoCorrecaoTipo") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoInicio") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazoinicio = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoImediato") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazoimediato = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_Produtividade") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_produtividade = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_EspelhaAceite") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_Momento") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_momento = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_StatusPagFnc") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_QntUntCns") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_qntuntcns = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_FatorCnvUndCnt") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_NaoRequerAtr") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_naorequeratr = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_IndiceDivergencia") )
               {
                  gxTv_SdtContratoServicos_Contrato_indicedivergencia = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_IndiceDivergencia") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_TmpEstAnl") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_tmpestanl = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_TmpEstExc") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_tmpestexc = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_TmpEstCrr") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_TipoHierarquia") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PercTmp") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_perctmp = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PercPgm") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_percpgm = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PercCnc") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_perccnc = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_Ativo") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_ativo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_CodigoFiscal") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_codigofiscal = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_LimiteProposta") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_limiteproposta = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_CalculoRmn") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_calculormn = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_QtdRmn") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_qtdrmn = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_SolicitaGestorSistema") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_solicitagestorsistema = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Rmn") )
               {
                  if ( gxTv_SdtContratoServicos_Rmn == null )
                  {
                     gxTv_SdtContratoServicos_Rmn = new GxSilentTrnGridCollection( context, "ContratoServicos.Rmn", "GxEv3Up14_MeetrikaVs3", "SdtContratoServicos_Rmn", "GeneXus.Programs");
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtContratoServicos_Rmn.readxml(oReader, "Rmn");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtContratoServicos_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtContratoServicos_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_Codigo_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_Codigo_Z") )
               {
                  gxTv_SdtContratoServicos_Contrato_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_Numero_Z") )
               {
                  gxTv_SdtContratoServicos_Contrato_numero_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_NumeroAta_Z") )
               {
                  gxTv_SdtContratoServicos_Contrato_numeroata_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_Ano_Z") )
               {
                  gxTv_SdtContratoServicos_Contrato_ano_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_ValorUnidadeContratacao_Z") )
               {
                  gxTv_SdtContratoServicos_Contrato_valorunidadecontratacao_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_AreaTrabalhoCod_Z") )
               {
                  gxTv_SdtContratoServicos_Contrato_areatrabalhocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_Codigo_Z") )
               {
                  gxTv_SdtContratoServicos_Contratada_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_PessoaCod_Z") )
               {
                  gxTv_SdtContratoServicos_Contratada_pessoacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_PessoaNom_Z") )
               {
                  gxTv_SdtContratoServicos_Contratada_pessoanom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_PessoaCNPJ_Z") )
               {
                  gxTv_SdtContratoServicos_Contratada_pessoacnpj_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_TipoFabrica_Z") )
               {
                  gxTv_SdtContratoServicos_Contratada_tipofabrica_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Codigo_Z") )
               {
                  gxTv_SdtContratoServicos_Servico_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_Alias_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_alias_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_ServicoCod_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_servicocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Nome_Z") )
               {
                  gxTv_SdtContratoServicos_Servico_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Sigla_Z") )
               {
                  gxTv_SdtContratoServicos_Servico_sigla_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Tela_Z") )
               {
                  gxTv_SdtContratoServicos_Servico_tela_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Ativo_Z") )
               {
                  gxTv_SdtContratoServicos_Servico_ativo_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Identificacao_Z") )
               {
                  gxTv_SdtContratoServicos_Servico_identificacao_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_ServicoSigla_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_servicosigla_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Vinculado_Z") )
               {
                  gxTv_SdtContratoServicos_Servico_vinculado_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Vinculados_Z") )
               {
                  gxTv_SdtContratoServicos_Servico_vinculados_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Responsavel_Z") )
               {
                  gxTv_SdtContratoServicos_Servico_responsavel_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoGrupo_Codigo_Z") )
               {
                  gxTv_SdtContratoServicos_Servicogrupo_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoGrupo_Descricao_Z") )
               {
                  gxTv_SdtContratoServicos_Servicogrupo_descricao_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_IsOrigemReferencia_Z") )
               {
                  gxTv_SdtContratoServicos_Servico_isorigemreferencia_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_UnidadeContratada_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_unidadecontratada_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_UndCntNome_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_undcntnome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_UndCntSgl_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_undcntsgl_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_QtdContratada_Z") )
               {
                  gxTv_SdtContratoServicos_Servico_qtdcontratada_Z = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_VlrUnidadeContratada_Z") )
               {
                  gxTv_SdtContratoServicos_Servico_vlrunidadecontratada_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Percentual_Z") )
               {
                  gxTv_SdtContratoServicos_Servico_percentual_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoContrato_Faturamento_Z") )
               {
                  gxTv_SdtContratoServicos_Servicocontrato_faturamento_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_LocalExec_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_localexec_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_TipoVnc_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_tipovnc_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_HmlSemCnf_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoTipo_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazotipo_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoDias_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazodias_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoTpDias_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazotpdias_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_Prazos_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazos_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_Indicadores_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_indicadores_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoAnalise_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazoanalise_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoResposta_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazoresposta_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoGarantia_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazogarantia_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoAtendeGarantia_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoCorrecao_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoCorrecaoTipo_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoInicio_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazoinicio_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoImediato_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazoimediato_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_Produtividade_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_produtividade_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_EspelhaAceite_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_Momento_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_momento_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_StatusPagFnc_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_QntUntCns_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_qntuntcns_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_FatorCnvUndCnt_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_NaoRequerAtr_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_naorequeratr_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_IndiceDivergencia_Z") )
               {
                  gxTv_SdtContratoServicos_Contrato_indicedivergencia_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_IndiceDivergencia_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_TmpEstAnl_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_tmpestanl_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_TmpEstExc_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_tmpestexc_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_TmpEstCrr_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_TipoHierarquia_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PercTmp_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_perctmp_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PercPgm_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_percpgm_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PercCnc_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_perccnc_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_Ativo_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_ativo_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_CodigoFiscal_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_codigofiscal_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_LimiteProposta_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_limiteproposta_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_CalculoRmn_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_calculormn_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_QtdRmn_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_qtdrmn_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_SolicitaGestorSistema_Z") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_solicitagestorsistema_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_NumeroAta_N") )
               {
                  gxTv_SdtContratoServicos_Contrato_numeroata_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_PessoaNom_N") )
               {
                  gxTv_SdtContratoServicos_Contratada_pessoanom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_PessoaCNPJ_N") )
               {
                  gxTv_SdtContratoServicos_Contratada_pessoacnpj_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_Alias_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_alias_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Tela_N") )
               {
                  gxTv_SdtContratoServicos_Servico_tela_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Descricao_N") )
               {
                  gxTv_SdtContratoServicos_Servico_descricao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Vinculado_N") )
               {
                  gxTv_SdtContratoServicos_Servico_vinculado_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_UndCntNome_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_undcntnome_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_UndCntSgl_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_undcntsgl_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_QtdContratada_N") )
               {
                  gxTv_SdtContratoServicos_Servico_qtdcontratada_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Percentual_N") )
               {
                  gxTv_SdtContratoServicos_Servico_percentual_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_TipoVnc_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_tipovnc_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_HmlSemCnf_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoTipo_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazotipo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoDias_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazodias_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoTpDias_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazotpdias_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_Prazos_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazos_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_Indicadores_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_indicadores_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoAnalise_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazoanalise_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoResposta_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazoresposta_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoGarantia_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazogarantia_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoAtendeGarantia_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoCorrecao_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoCorrecaoTipo_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoInicio_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazoinicio_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PrazoImediato_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_prazoimediato_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_Produtividade_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_produtividade_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_EspelhaAceite_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_Momento_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_momento_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_StatusPagFnc_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_QntUntCns_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_qntuntcns_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_FatorCnvUndCnt_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_NaoRequerAtr_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_naorequeratr_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_IndiceDivergencia_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_TmpEstAnl_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_tmpestanl_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_TmpEstExc_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_tmpestexc_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_TmpEstCrr_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_TipoHierarquia_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PercTmp_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_perctmp_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PercPgm_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_percpgm_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_PercCnc_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_perccnc_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_CodigoFiscal_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_codigofiscal_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_LimiteProposta_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_limiteproposta_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_CalculoRmn_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_calculormn_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_QtdRmn_N") )
               {
                  gxTv_SdtContratoServicos_Contratoservicos_qtdrmn_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ContratoServicos";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContratoServicos_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contrato_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contrato_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contrato_Numero", StringUtil.RTrim( gxTv_SdtContratoServicos_Contrato_numero));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contrato_NumeroAta", StringUtil.RTrim( gxTv_SdtContratoServicos_Contrato_numeroata));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contrato_Ano", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contrato_ano), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contrato_ValorUnidadeContratacao", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratoServicos_Contrato_valorunidadecontratacao, 18, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contrato_AreaTrabalhoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contrato_areatrabalhocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratada_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratada_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratada_PessoaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratada_pessoacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratada_PessoaNom", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratada_pessoanom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratada_PessoaCNPJ", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratada_pessoacnpj));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratada_TipoFabrica", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratada_tipofabrica));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Servico_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_Alias", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratoservicos_alias));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_ServicoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_servicocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_Nome", StringUtil.RTrim( gxTv_SdtContratoServicos_Servico_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_Sigla", StringUtil.RTrim( gxTv_SdtContratoServicos_Servico_sigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_Tela", StringUtil.RTrim( gxTv_SdtContratoServicos_Servico_tela));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_Ativo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratoServicos_Servico_ativo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_Identificacao", StringUtil.RTrim( gxTv_SdtContratoServicos_Servico_identificacao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_ServicoSigla", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratoservicos_servicosigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_Descricao", StringUtil.RTrim( gxTv_SdtContratoServicos_Servico_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_Vinculado", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Servico_vinculado), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_Vinculados", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Servico_vinculados), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_Responsavel", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Servico_responsavel), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ServicoGrupo_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Servicogrupo_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ServicoGrupo_Descricao", StringUtil.RTrim( gxTv_SdtContratoServicos_Servicogrupo_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_IsOrigemReferencia", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratoServicos_Servico_isorigemreferencia)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_UnidadeContratada", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_unidadecontratada), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_UndCntNome", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratoservicos_undcntnome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_UndCntSgl", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratoservicos_undcntsgl));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_QtdContratada", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Servico_qtdcontratada), 10, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_VlrUnidadeContratada", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratoServicos_Servico_vlrunidadecontratada, 18, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_Percentual", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratoServicos_Servico_percentual, 7, 3)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ServicoContrato_Faturamento", StringUtil.RTrim( gxTv_SdtContratoServicos_Servicocontrato_faturamento));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_LocalExec", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratoservicos_localexec));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_TipoVnc", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratoservicos_tipovnc));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_HmlSemCnf", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_PrazoTipo", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratoservicos_prazotipo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_PrazoDias", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_prazodias), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_PrazoTpDias", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratoservicos_prazotpdias));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_Prazos", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_prazos), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_Indicadores", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_indicadores), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_PrazoAnalise", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_prazoanalise), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_PrazoResposta", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_prazoresposta), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_PrazoGarantia", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_prazogarantia), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_PrazoAtendeGarantia", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_PrazoCorrecao", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_PrazoCorrecaoTipo", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_PrazoInicio", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_prazoinicio), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_PrazoImediato", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratoServicos_Contratoservicos_prazoimediato)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_Produtividade", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratoServicos_Contratoservicos_produtividade, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_EspelhaAceite", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_Momento", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratoservicos_momento));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_StatusPagFnc", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_QntUntCns", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratoServicos_Contratoservicos_qntuntcns, 9, 4)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_FatorCnvUndCnt", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt, 9, 4)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_NaoRequerAtr", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratoServicos_Contratoservicos_naorequeratr)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contrato_IndiceDivergencia", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratoServicos_Contrato_indicedivergencia, 6, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_IndiceDivergencia", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia, 6, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_TmpEstAnl", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_tmpestanl), 8, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_TmpEstExc", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_tmpestexc), 8, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_TmpEstCrr", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr), 8, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_TipoHierarquia", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_PercTmp", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_perctmp), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_PercPgm", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_percpgm), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_PercCnc", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_perccnc), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_Ativo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratoServicos_Contratoservicos_ativo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_CodigoFiscal", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratoservicos_codigofiscal));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_LimiteProposta", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratoServicos_Contratoservicos_limiteproposta, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_CalculoRmn", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_calculormn), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_QtdRmn", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_qtdrmn), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicos_SolicitaGestorSistema", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratoServicos_Contratoservicos_solicitagestorsistema)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            if ( gxTv_SdtContratoServicos_Rmn != null )
            {
               String sNameSpace1 ;
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") == 0 )
               {
                  sNameSpace1 = "[*:nosend]" + "GxEv3Up14_MeetrikaVs3";
               }
               else
               {
                  sNameSpace1 = "GxEv3Up14_MeetrikaVs3";
               }
               gxTv_SdtContratoServicos_Rmn.writexml(oWriter, "Rmn", sNameSpace1);
            }
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtContratoServicos_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contrato_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contrato_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contrato_Numero_Z", StringUtil.RTrim( gxTv_SdtContratoServicos_Contrato_numero_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contrato_NumeroAta_Z", StringUtil.RTrim( gxTv_SdtContratoServicos_Contrato_numeroata_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contrato_Ano_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contrato_ano_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contrato_ValorUnidadeContratacao_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratoServicos_Contrato_valorunidadecontratacao_Z, 18, 5)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contrato_AreaTrabalhoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contrato_areatrabalhocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratada_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratada_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratada_PessoaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratada_pessoacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratada_PessoaNom_Z", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratada_pessoanom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratada_PessoaCNPJ_Z", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratada_pessoacnpj_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratada_TipoFabrica_Z", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratada_tipofabrica_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Servico_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_Alias_Z", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratoservicos_alias_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_ServicoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_servicocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Nome_Z", StringUtil.RTrim( gxTv_SdtContratoServicos_Servico_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Sigla_Z", StringUtil.RTrim( gxTv_SdtContratoServicos_Servico_sigla_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Tela_Z", StringUtil.RTrim( gxTv_SdtContratoServicos_Servico_tela_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Ativo_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratoServicos_Servico_ativo_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Identificacao_Z", StringUtil.RTrim( gxTv_SdtContratoServicos_Servico_identificacao_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_ServicoSigla_Z", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratoservicos_servicosigla_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Vinculado_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Servico_vinculado_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Vinculados_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Servico_vinculados_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Responsavel_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Servico_responsavel_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ServicoGrupo_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Servicogrupo_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ServicoGrupo_Descricao_Z", StringUtil.RTrim( gxTv_SdtContratoServicos_Servicogrupo_descricao_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_IsOrigemReferencia_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratoServicos_Servico_isorigemreferencia_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_UnidadeContratada_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_unidadecontratada_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_UndCntNome_Z", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratoservicos_undcntnome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_UndCntSgl_Z", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratoservicos_undcntsgl_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_QtdContratada_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Servico_qtdcontratada_Z), 10, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_VlrUnidadeContratada_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratoServicos_Servico_vlrunidadecontratada_Z, 18, 5)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Percentual_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratoServicos_Servico_percentual_Z, 7, 3)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ServicoContrato_Faturamento_Z", StringUtil.RTrim( gxTv_SdtContratoServicos_Servicocontrato_faturamento_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_LocalExec_Z", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratoservicos_localexec_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_TipoVnc_Z", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratoservicos_tipovnc_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_HmlSemCnf_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_PrazoTipo_Z", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratoservicos_prazotipo_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_PrazoDias_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_prazodias_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_PrazoTpDias_Z", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratoservicos_prazotpdias_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_Prazos_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_prazos_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_Indicadores_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_indicadores_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_PrazoAnalise_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_prazoanalise_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_PrazoResposta_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_prazoresposta_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_PrazoGarantia_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_prazogarantia_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_PrazoAtendeGarantia_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_PrazoCorrecao_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_PrazoCorrecaoTipo_Z", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_PrazoInicio_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_prazoinicio_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_PrazoImediato_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratoServicos_Contratoservicos_prazoimediato_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_Produtividade_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratoServicos_Contratoservicos_produtividade_Z, 14, 5)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_EspelhaAceite_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_Momento_Z", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratoservicos_momento_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_StatusPagFnc_Z", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_QntUntCns_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratoServicos_Contratoservicos_qntuntcns_Z, 9, 4)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_FatorCnvUndCnt_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt_Z, 9, 4)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_NaoRequerAtr_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratoServicos_Contratoservicos_naorequeratr_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contrato_IndiceDivergencia_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratoServicos_Contrato_indicedivergencia_Z, 6, 2)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_IndiceDivergencia_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia_Z, 6, 2)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_TmpEstAnl_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_tmpestanl_Z), 8, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_TmpEstExc_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_tmpestexc_Z), 8, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_TmpEstCrr_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr_Z), 8, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_TipoHierarquia_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_PercTmp_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_perctmp_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_PercPgm_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_percpgm_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_PercCnc_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_perccnc_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_Ativo_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratoServicos_Contratoservicos_ativo_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_CodigoFiscal_Z", StringUtil.RTrim( gxTv_SdtContratoServicos_Contratoservicos_codigofiscal_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_LimiteProposta_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratoServicos_Contratoservicos_limiteproposta_Z, 14, 5)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_CalculoRmn_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_calculormn_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_QtdRmn_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_qtdrmn_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_SolicitaGestorSistema_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratoServicos_Contratoservicos_solicitagestorsistema_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contrato_NumeroAta_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contrato_numeroata_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratada_PessoaNom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratada_pessoanom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratada_PessoaCNPJ_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratada_pessoacnpj_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_Alias_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_alias_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Tela_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Servico_tela_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Descricao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Servico_descricao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Vinculado_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Servico_vinculado_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_UndCntNome_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_undcntnome_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_UndCntSgl_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_undcntsgl_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_QtdContratada_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Servico_qtdcontratada_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Percentual_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Servico_percentual_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_TipoVnc_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_tipovnc_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_HmlSemCnf_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_PrazoTipo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_prazotipo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_PrazoDias_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_prazodias_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_PrazoTpDias_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_prazotpdias_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_Prazos_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_prazos_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_Indicadores_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_indicadores_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_PrazoAnalise_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_prazoanalise_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_PrazoResposta_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_prazoresposta_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_PrazoGarantia_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_prazogarantia_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_PrazoAtendeGarantia_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_PrazoCorrecao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_PrazoCorrecaoTipo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_PrazoInicio_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_prazoinicio_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_PrazoImediato_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_prazoimediato_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_Produtividade_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_produtividade_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_EspelhaAceite_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_Momento_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_momento_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_StatusPagFnc_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_QntUntCns_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_qntuntcns_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_FatorCnvUndCnt_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_NaoRequerAtr_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_naorequeratr_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_IndiceDivergencia_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_TmpEstAnl_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_tmpestanl_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_TmpEstExc_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_tmpestexc_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_TmpEstCrr_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_TipoHierarquia_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_PercTmp_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_perctmp_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_PercPgm_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_percpgm_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_PercCnc_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_perccnc_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_CodigoFiscal_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_codigofiscal_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_LimiteProposta_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_limiteproposta_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_CalculoRmn_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_calculormn_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_QtdRmn_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Contratoservicos_qtdrmn_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContratoServicos_Codigo", gxTv_SdtContratoServicos_Contratoservicos_codigo, false);
         AddObjectProperty("Contrato_Codigo", gxTv_SdtContratoServicos_Contrato_codigo, false);
         AddObjectProperty("Contrato_Numero", gxTv_SdtContratoServicos_Contrato_numero, false);
         AddObjectProperty("Contrato_NumeroAta", gxTv_SdtContratoServicos_Contrato_numeroata, false);
         AddObjectProperty("Contrato_Ano", gxTv_SdtContratoServicos_Contrato_ano, false);
         AddObjectProperty("Contrato_ValorUnidadeContratacao", StringUtil.LTrim( StringUtil.Str( gxTv_SdtContratoServicos_Contrato_valorunidadecontratacao, 18, 5)), false);
         AddObjectProperty("Contrato_AreaTrabalhoCod", gxTv_SdtContratoServicos_Contrato_areatrabalhocod, false);
         AddObjectProperty("Contratada_Codigo", gxTv_SdtContratoServicos_Contratada_codigo, false);
         AddObjectProperty("Contratada_PessoaCod", gxTv_SdtContratoServicos_Contratada_pessoacod, false);
         AddObjectProperty("Contratada_PessoaNom", gxTv_SdtContratoServicos_Contratada_pessoanom, false);
         AddObjectProperty("Contratada_PessoaCNPJ", gxTv_SdtContratoServicos_Contratada_pessoacnpj, false);
         AddObjectProperty("Contratada_TipoFabrica", gxTv_SdtContratoServicos_Contratada_tipofabrica, false);
         AddObjectProperty("Servico_Codigo", gxTv_SdtContratoServicos_Servico_codigo, false);
         AddObjectProperty("ContratoServicos_Alias", gxTv_SdtContratoServicos_Contratoservicos_alias, false);
         AddObjectProperty("ContratoServicos_ServicoCod", gxTv_SdtContratoServicos_Contratoservicos_servicocod, false);
         AddObjectProperty("Servico_Nome", gxTv_SdtContratoServicos_Servico_nome, false);
         AddObjectProperty("Servico_Sigla", gxTv_SdtContratoServicos_Servico_sigla, false);
         AddObjectProperty("Servico_Tela", gxTv_SdtContratoServicos_Servico_tela, false);
         AddObjectProperty("Servico_Ativo", gxTv_SdtContratoServicos_Servico_ativo, false);
         AddObjectProperty("Servico_Identificacao", gxTv_SdtContratoServicos_Servico_identificacao, false);
         AddObjectProperty("ContratoServicos_ServicoSigla", gxTv_SdtContratoServicos_Contratoservicos_servicosigla, false);
         AddObjectProperty("Servico_Descricao", gxTv_SdtContratoServicos_Servico_descricao, false);
         AddObjectProperty("Servico_Vinculado", gxTv_SdtContratoServicos_Servico_vinculado, false);
         AddObjectProperty("Servico_Vinculados", gxTv_SdtContratoServicos_Servico_vinculados, false);
         AddObjectProperty("Servico_Responsavel", gxTv_SdtContratoServicos_Servico_responsavel, false);
         AddObjectProperty("ServicoGrupo_Codigo", gxTv_SdtContratoServicos_Servicogrupo_codigo, false);
         AddObjectProperty("ServicoGrupo_Descricao", gxTv_SdtContratoServicos_Servicogrupo_descricao, false);
         AddObjectProperty("Servico_IsOrigemReferencia", gxTv_SdtContratoServicos_Servico_isorigemreferencia, false);
         AddObjectProperty("ContratoServicos_UnidadeContratada", gxTv_SdtContratoServicos_Contratoservicos_unidadecontratada, false);
         AddObjectProperty("ContratoServicos_UndCntNome", gxTv_SdtContratoServicos_Contratoservicos_undcntnome, false);
         AddObjectProperty("ContratoServicos_UndCntSgl", gxTv_SdtContratoServicos_Contratoservicos_undcntsgl, false);
         AddObjectProperty("Servico_QtdContratada", gxTv_SdtContratoServicos_Servico_qtdcontratada, false);
         AddObjectProperty("Servico_VlrUnidadeContratada", StringUtil.LTrim( StringUtil.Str( gxTv_SdtContratoServicos_Servico_vlrunidadecontratada, 18, 5)), false);
         AddObjectProperty("Servico_Percentual", gxTv_SdtContratoServicos_Servico_percentual, false);
         AddObjectProperty("ServicoContrato_Faturamento", gxTv_SdtContratoServicos_Servicocontrato_faturamento, false);
         AddObjectProperty("ContratoServicos_LocalExec", gxTv_SdtContratoServicos_Contratoservicos_localexec, false);
         AddObjectProperty("ContratoServicos_TipoVnc", gxTv_SdtContratoServicos_Contratoservicos_tipovnc, false);
         AddObjectProperty("ContratoServicos_HmlSemCnf", gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf, false);
         AddObjectProperty("ContratoServicos_PrazoTipo", gxTv_SdtContratoServicos_Contratoservicos_prazotipo, false);
         AddObjectProperty("ContratoServicos_PrazoDias", gxTv_SdtContratoServicos_Contratoservicos_prazodias, false);
         AddObjectProperty("ContratoServicos_PrazoTpDias", gxTv_SdtContratoServicos_Contratoservicos_prazotpdias, false);
         AddObjectProperty("ContratoServicos_Prazos", gxTv_SdtContratoServicos_Contratoservicos_prazos, false);
         AddObjectProperty("ContratoServicos_Indicadores", gxTv_SdtContratoServicos_Contratoservicos_indicadores, false);
         AddObjectProperty("ContratoServicos_PrazoAnalise", gxTv_SdtContratoServicos_Contratoservicos_prazoanalise, false);
         AddObjectProperty("ContratoServicos_PrazoResposta", gxTv_SdtContratoServicos_Contratoservicos_prazoresposta, false);
         AddObjectProperty("ContratoServicos_PrazoGarantia", gxTv_SdtContratoServicos_Contratoservicos_prazogarantia, false);
         AddObjectProperty("ContratoServicos_PrazoAtendeGarantia", gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia, false);
         AddObjectProperty("ContratoServicos_PrazoCorrecao", gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao, false);
         AddObjectProperty("ContratoServicos_PrazoCorrecaoTipo", gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo, false);
         AddObjectProperty("ContratoServicos_PrazoInicio", gxTv_SdtContratoServicos_Contratoservicos_prazoinicio, false);
         AddObjectProperty("ContratoServicos_PrazoImediato", gxTv_SdtContratoServicos_Contratoservicos_prazoimediato, false);
         AddObjectProperty("ContratoServicos_Produtividade", gxTv_SdtContratoServicos_Contratoservicos_produtividade, false);
         AddObjectProperty("ContratoServicos_EspelhaAceite", gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite, false);
         AddObjectProperty("ContratoServicos_Momento", gxTv_SdtContratoServicos_Contratoservicos_momento, false);
         AddObjectProperty("ContratoServicos_StatusPagFnc", gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc, false);
         AddObjectProperty("ContratoServicos_QntUntCns", gxTv_SdtContratoServicos_Contratoservicos_qntuntcns, false);
         AddObjectProperty("ContratoServicos_FatorCnvUndCnt", gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt, false);
         AddObjectProperty("ContratoServicos_NaoRequerAtr", gxTv_SdtContratoServicos_Contratoservicos_naorequeratr, false);
         AddObjectProperty("Contrato_IndiceDivergencia", gxTv_SdtContratoServicos_Contrato_indicedivergencia, false);
         AddObjectProperty("ContratoServicos_IndiceDivergencia", gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia, false);
         AddObjectProperty("ContratoServicos_TmpEstAnl", gxTv_SdtContratoServicos_Contratoservicos_tmpestanl, false);
         AddObjectProperty("ContratoServicos_TmpEstExc", gxTv_SdtContratoServicos_Contratoservicos_tmpestexc, false);
         AddObjectProperty("ContratoServicos_TmpEstCrr", gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr, false);
         AddObjectProperty("ContratoServicos_TipoHierarquia", gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia, false);
         AddObjectProperty("ContratoServicos_PercTmp", gxTv_SdtContratoServicos_Contratoservicos_perctmp, false);
         AddObjectProperty("ContratoServicos_PercPgm", gxTv_SdtContratoServicos_Contratoservicos_percpgm, false);
         AddObjectProperty("ContratoServicos_PercCnc", gxTv_SdtContratoServicos_Contratoservicos_perccnc, false);
         AddObjectProperty("ContratoServicos_Ativo", gxTv_SdtContratoServicos_Contratoservicos_ativo, false);
         AddObjectProperty("ContratoServicos_CodigoFiscal", gxTv_SdtContratoServicos_Contratoservicos_codigofiscal, false);
         AddObjectProperty("ContratoServicos_LimiteProposta", gxTv_SdtContratoServicos_Contratoservicos_limiteproposta, false);
         AddObjectProperty("ContratoServicos_CalculoRmn", gxTv_SdtContratoServicos_Contratoservicos_calculormn, false);
         AddObjectProperty("ContratoServicos_QtdRmn", gxTv_SdtContratoServicos_Contratoservicos_qtdrmn, false);
         AddObjectProperty("ContratoServicos_SolicitaGestorSistema", gxTv_SdtContratoServicos_Contratoservicos_solicitagestorsistema, false);
         if ( gxTv_SdtContratoServicos_Rmn != null )
         {
            AddObjectProperty("Rmn", gxTv_SdtContratoServicos_Rmn, includeState);
         }
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtContratoServicos_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtContratoServicos_Initialized, false);
            AddObjectProperty("ContratoServicos_Codigo_Z", gxTv_SdtContratoServicos_Contratoservicos_codigo_Z, false);
            AddObjectProperty("Contrato_Codigo_Z", gxTv_SdtContratoServicos_Contrato_codigo_Z, false);
            AddObjectProperty("Contrato_Numero_Z", gxTv_SdtContratoServicos_Contrato_numero_Z, false);
            AddObjectProperty("Contrato_NumeroAta_Z", gxTv_SdtContratoServicos_Contrato_numeroata_Z, false);
            AddObjectProperty("Contrato_Ano_Z", gxTv_SdtContratoServicos_Contrato_ano_Z, false);
            AddObjectProperty("Contrato_ValorUnidadeContratacao_Z", StringUtil.LTrim( StringUtil.Str( gxTv_SdtContratoServicos_Contrato_valorunidadecontratacao_Z, 18, 5)), false);
            AddObjectProperty("Contrato_AreaTrabalhoCod_Z", gxTv_SdtContratoServicos_Contrato_areatrabalhocod_Z, false);
            AddObjectProperty("Contratada_Codigo_Z", gxTv_SdtContratoServicos_Contratada_codigo_Z, false);
            AddObjectProperty("Contratada_PessoaCod_Z", gxTv_SdtContratoServicos_Contratada_pessoacod_Z, false);
            AddObjectProperty("Contratada_PessoaNom_Z", gxTv_SdtContratoServicos_Contratada_pessoanom_Z, false);
            AddObjectProperty("Contratada_PessoaCNPJ_Z", gxTv_SdtContratoServicos_Contratada_pessoacnpj_Z, false);
            AddObjectProperty("Contratada_TipoFabrica_Z", gxTv_SdtContratoServicos_Contratada_tipofabrica_Z, false);
            AddObjectProperty("Servico_Codigo_Z", gxTv_SdtContratoServicos_Servico_codigo_Z, false);
            AddObjectProperty("ContratoServicos_Alias_Z", gxTv_SdtContratoServicos_Contratoservicos_alias_Z, false);
            AddObjectProperty("ContratoServicos_ServicoCod_Z", gxTv_SdtContratoServicos_Contratoservicos_servicocod_Z, false);
            AddObjectProperty("Servico_Nome_Z", gxTv_SdtContratoServicos_Servico_nome_Z, false);
            AddObjectProperty("Servico_Sigla_Z", gxTv_SdtContratoServicos_Servico_sigla_Z, false);
            AddObjectProperty("Servico_Tela_Z", gxTv_SdtContratoServicos_Servico_tela_Z, false);
            AddObjectProperty("Servico_Ativo_Z", gxTv_SdtContratoServicos_Servico_ativo_Z, false);
            AddObjectProperty("Servico_Identificacao_Z", gxTv_SdtContratoServicos_Servico_identificacao_Z, false);
            AddObjectProperty("ContratoServicos_ServicoSigla_Z", gxTv_SdtContratoServicos_Contratoservicos_servicosigla_Z, false);
            AddObjectProperty("Servico_Vinculado_Z", gxTv_SdtContratoServicos_Servico_vinculado_Z, false);
            AddObjectProperty("Servico_Vinculados_Z", gxTv_SdtContratoServicos_Servico_vinculados_Z, false);
            AddObjectProperty("Servico_Responsavel_Z", gxTv_SdtContratoServicos_Servico_responsavel_Z, false);
            AddObjectProperty("ServicoGrupo_Codigo_Z", gxTv_SdtContratoServicos_Servicogrupo_codigo_Z, false);
            AddObjectProperty("ServicoGrupo_Descricao_Z", gxTv_SdtContratoServicos_Servicogrupo_descricao_Z, false);
            AddObjectProperty("Servico_IsOrigemReferencia_Z", gxTv_SdtContratoServicos_Servico_isorigemreferencia_Z, false);
            AddObjectProperty("ContratoServicos_UnidadeContratada_Z", gxTv_SdtContratoServicos_Contratoservicos_unidadecontratada_Z, false);
            AddObjectProperty("ContratoServicos_UndCntNome_Z", gxTv_SdtContratoServicos_Contratoservicos_undcntnome_Z, false);
            AddObjectProperty("ContratoServicos_UndCntSgl_Z", gxTv_SdtContratoServicos_Contratoservicos_undcntsgl_Z, false);
            AddObjectProperty("Servico_QtdContratada_Z", gxTv_SdtContratoServicos_Servico_qtdcontratada_Z, false);
            AddObjectProperty("Servico_VlrUnidadeContratada_Z", StringUtil.LTrim( StringUtil.Str( gxTv_SdtContratoServicos_Servico_vlrunidadecontratada_Z, 18, 5)), false);
            AddObjectProperty("Servico_Percentual_Z", gxTv_SdtContratoServicos_Servico_percentual_Z, false);
            AddObjectProperty("ServicoContrato_Faturamento_Z", gxTv_SdtContratoServicos_Servicocontrato_faturamento_Z, false);
            AddObjectProperty("ContratoServicos_LocalExec_Z", gxTv_SdtContratoServicos_Contratoservicos_localexec_Z, false);
            AddObjectProperty("ContratoServicos_TipoVnc_Z", gxTv_SdtContratoServicos_Contratoservicos_tipovnc_Z, false);
            AddObjectProperty("ContratoServicos_HmlSemCnf_Z", gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf_Z, false);
            AddObjectProperty("ContratoServicos_PrazoTipo_Z", gxTv_SdtContratoServicos_Contratoservicos_prazotipo_Z, false);
            AddObjectProperty("ContratoServicos_PrazoDias_Z", gxTv_SdtContratoServicos_Contratoservicos_prazodias_Z, false);
            AddObjectProperty("ContratoServicos_PrazoTpDias_Z", gxTv_SdtContratoServicos_Contratoservicos_prazotpdias_Z, false);
            AddObjectProperty("ContratoServicos_Prazos_Z", gxTv_SdtContratoServicos_Contratoservicos_prazos_Z, false);
            AddObjectProperty("ContratoServicos_Indicadores_Z", gxTv_SdtContratoServicos_Contratoservicos_indicadores_Z, false);
            AddObjectProperty("ContratoServicos_PrazoAnalise_Z", gxTv_SdtContratoServicos_Contratoservicos_prazoanalise_Z, false);
            AddObjectProperty("ContratoServicos_PrazoResposta_Z", gxTv_SdtContratoServicos_Contratoservicos_prazoresposta_Z, false);
            AddObjectProperty("ContratoServicos_PrazoGarantia_Z", gxTv_SdtContratoServicos_Contratoservicos_prazogarantia_Z, false);
            AddObjectProperty("ContratoServicos_PrazoAtendeGarantia_Z", gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia_Z, false);
            AddObjectProperty("ContratoServicos_PrazoCorrecao_Z", gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao_Z, false);
            AddObjectProperty("ContratoServicos_PrazoCorrecaoTipo_Z", gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo_Z, false);
            AddObjectProperty("ContratoServicos_PrazoInicio_Z", gxTv_SdtContratoServicos_Contratoservicos_prazoinicio_Z, false);
            AddObjectProperty("ContratoServicos_PrazoImediato_Z", gxTv_SdtContratoServicos_Contratoservicos_prazoimediato_Z, false);
            AddObjectProperty("ContratoServicos_Produtividade_Z", gxTv_SdtContratoServicos_Contratoservicos_produtividade_Z, false);
            AddObjectProperty("ContratoServicos_EspelhaAceite_Z", gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite_Z, false);
            AddObjectProperty("ContratoServicos_Momento_Z", gxTv_SdtContratoServicos_Contratoservicos_momento_Z, false);
            AddObjectProperty("ContratoServicos_StatusPagFnc_Z", gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc_Z, false);
            AddObjectProperty("ContratoServicos_QntUntCns_Z", gxTv_SdtContratoServicos_Contratoservicos_qntuntcns_Z, false);
            AddObjectProperty("ContratoServicos_FatorCnvUndCnt_Z", gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt_Z, false);
            AddObjectProperty("ContratoServicos_NaoRequerAtr_Z", gxTv_SdtContratoServicos_Contratoservicos_naorequeratr_Z, false);
            AddObjectProperty("Contrato_IndiceDivergencia_Z", gxTv_SdtContratoServicos_Contrato_indicedivergencia_Z, false);
            AddObjectProperty("ContratoServicos_IndiceDivergencia_Z", gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia_Z, false);
            AddObjectProperty("ContratoServicos_TmpEstAnl_Z", gxTv_SdtContratoServicos_Contratoservicos_tmpestanl_Z, false);
            AddObjectProperty("ContratoServicos_TmpEstExc_Z", gxTv_SdtContratoServicos_Contratoservicos_tmpestexc_Z, false);
            AddObjectProperty("ContratoServicos_TmpEstCrr_Z", gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr_Z, false);
            AddObjectProperty("ContratoServicos_TipoHierarquia_Z", gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia_Z, false);
            AddObjectProperty("ContratoServicos_PercTmp_Z", gxTv_SdtContratoServicos_Contratoservicos_perctmp_Z, false);
            AddObjectProperty("ContratoServicos_PercPgm_Z", gxTv_SdtContratoServicos_Contratoservicos_percpgm_Z, false);
            AddObjectProperty("ContratoServicos_PercCnc_Z", gxTv_SdtContratoServicos_Contratoservicos_perccnc_Z, false);
            AddObjectProperty("ContratoServicos_Ativo_Z", gxTv_SdtContratoServicos_Contratoservicos_ativo_Z, false);
            AddObjectProperty("ContratoServicos_CodigoFiscal_Z", gxTv_SdtContratoServicos_Contratoservicos_codigofiscal_Z, false);
            AddObjectProperty("ContratoServicos_LimiteProposta_Z", gxTv_SdtContratoServicos_Contratoservicos_limiteproposta_Z, false);
            AddObjectProperty("ContratoServicos_CalculoRmn_Z", gxTv_SdtContratoServicos_Contratoservicos_calculormn_Z, false);
            AddObjectProperty("ContratoServicos_QtdRmn_Z", gxTv_SdtContratoServicos_Contratoservicos_qtdrmn_Z, false);
            AddObjectProperty("ContratoServicos_SolicitaGestorSistema_Z", gxTv_SdtContratoServicos_Contratoservicos_solicitagestorsistema_Z, false);
            AddObjectProperty("Contrato_NumeroAta_N", gxTv_SdtContratoServicos_Contrato_numeroata_N, false);
            AddObjectProperty("Contratada_PessoaNom_N", gxTv_SdtContratoServicos_Contratada_pessoanom_N, false);
            AddObjectProperty("Contratada_PessoaCNPJ_N", gxTv_SdtContratoServicos_Contratada_pessoacnpj_N, false);
            AddObjectProperty("ContratoServicos_Alias_N", gxTv_SdtContratoServicos_Contratoservicos_alias_N, false);
            AddObjectProperty("Servico_Tela_N", gxTv_SdtContratoServicos_Servico_tela_N, false);
            AddObjectProperty("Servico_Descricao_N", gxTv_SdtContratoServicos_Servico_descricao_N, false);
            AddObjectProperty("Servico_Vinculado_N", gxTv_SdtContratoServicos_Servico_vinculado_N, false);
            AddObjectProperty("ContratoServicos_UndCntNome_N", gxTv_SdtContratoServicos_Contratoservicos_undcntnome_N, false);
            AddObjectProperty("ContratoServicos_UndCntSgl_N", gxTv_SdtContratoServicos_Contratoservicos_undcntsgl_N, false);
            AddObjectProperty("Servico_QtdContratada_N", gxTv_SdtContratoServicos_Servico_qtdcontratada_N, false);
            AddObjectProperty("Servico_Percentual_N", gxTv_SdtContratoServicos_Servico_percentual_N, false);
            AddObjectProperty("ContratoServicos_TipoVnc_N", gxTv_SdtContratoServicos_Contratoservicos_tipovnc_N, false);
            AddObjectProperty("ContratoServicos_HmlSemCnf_N", gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf_N, false);
            AddObjectProperty("ContratoServicos_PrazoTipo_N", gxTv_SdtContratoServicos_Contratoservicos_prazotipo_N, false);
            AddObjectProperty("ContratoServicos_PrazoDias_N", gxTv_SdtContratoServicos_Contratoservicos_prazodias_N, false);
            AddObjectProperty("ContratoServicos_PrazoTpDias_N", gxTv_SdtContratoServicos_Contratoservicos_prazotpdias_N, false);
            AddObjectProperty("ContratoServicos_Prazos_N", gxTv_SdtContratoServicos_Contratoservicos_prazos_N, false);
            AddObjectProperty("ContratoServicos_Indicadores_N", gxTv_SdtContratoServicos_Contratoservicos_indicadores_N, false);
            AddObjectProperty("ContratoServicos_PrazoAnalise_N", gxTv_SdtContratoServicos_Contratoservicos_prazoanalise_N, false);
            AddObjectProperty("ContratoServicos_PrazoResposta_N", gxTv_SdtContratoServicos_Contratoservicos_prazoresposta_N, false);
            AddObjectProperty("ContratoServicos_PrazoGarantia_N", gxTv_SdtContratoServicos_Contratoservicos_prazogarantia_N, false);
            AddObjectProperty("ContratoServicos_PrazoAtendeGarantia_N", gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia_N, false);
            AddObjectProperty("ContratoServicos_PrazoCorrecao_N", gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao_N, false);
            AddObjectProperty("ContratoServicos_PrazoCorrecaoTipo_N", gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo_N, false);
            AddObjectProperty("ContratoServicos_PrazoInicio_N", gxTv_SdtContratoServicos_Contratoservicos_prazoinicio_N, false);
            AddObjectProperty("ContratoServicos_PrazoImediato_N", gxTv_SdtContratoServicos_Contratoservicos_prazoimediato_N, false);
            AddObjectProperty("ContratoServicos_Produtividade_N", gxTv_SdtContratoServicos_Contratoservicos_produtividade_N, false);
            AddObjectProperty("ContratoServicos_EspelhaAceite_N", gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite_N, false);
            AddObjectProperty("ContratoServicos_Momento_N", gxTv_SdtContratoServicos_Contratoservicos_momento_N, false);
            AddObjectProperty("ContratoServicos_StatusPagFnc_N", gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc_N, false);
            AddObjectProperty("ContratoServicos_QntUntCns_N", gxTv_SdtContratoServicos_Contratoservicos_qntuntcns_N, false);
            AddObjectProperty("ContratoServicos_FatorCnvUndCnt_N", gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt_N, false);
            AddObjectProperty("ContratoServicos_NaoRequerAtr_N", gxTv_SdtContratoServicos_Contratoservicos_naorequeratr_N, false);
            AddObjectProperty("ContratoServicos_IndiceDivergencia_N", gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia_N, false);
            AddObjectProperty("ContratoServicos_TmpEstAnl_N", gxTv_SdtContratoServicos_Contratoservicos_tmpestanl_N, false);
            AddObjectProperty("ContratoServicos_TmpEstExc_N", gxTv_SdtContratoServicos_Contratoservicos_tmpestexc_N, false);
            AddObjectProperty("ContratoServicos_TmpEstCrr_N", gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr_N, false);
            AddObjectProperty("ContratoServicos_TipoHierarquia_N", gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia_N, false);
            AddObjectProperty("ContratoServicos_PercTmp_N", gxTv_SdtContratoServicos_Contratoservicos_perctmp_N, false);
            AddObjectProperty("ContratoServicos_PercPgm_N", gxTv_SdtContratoServicos_Contratoservicos_percpgm_N, false);
            AddObjectProperty("ContratoServicos_PercCnc_N", gxTv_SdtContratoServicos_Contratoservicos_perccnc_N, false);
            AddObjectProperty("ContratoServicos_CodigoFiscal_N", gxTv_SdtContratoServicos_Contratoservicos_codigofiscal_N, false);
            AddObjectProperty("ContratoServicos_LimiteProposta_N", gxTv_SdtContratoServicos_Contratoservicos_limiteproposta_N, false);
            AddObjectProperty("ContratoServicos_CalculoRmn_N", gxTv_SdtContratoServicos_Contratoservicos_calculormn_N, false);
            AddObjectProperty("ContratoServicos_QtdRmn_N", gxTv_SdtContratoServicos_Contratoservicos_qtdrmn_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ContratoServicos_Codigo" )]
      [  XmlElement( ElementName = "ContratoServicos_Codigo"   )]
      public int gxTpr_Contratoservicos_codigo
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_codigo ;
         }

         set {
            if ( gxTv_SdtContratoServicos_Contratoservicos_codigo != value )
            {
               gxTv_SdtContratoServicos_Mode = "INS";
               this.gxTv_SdtContratoServicos_Contratoservicos_codigo_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contrato_codigo_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contrato_numero_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contrato_numeroata_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contrato_ano_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contrato_valorunidadecontratacao_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contrato_areatrabalhocod_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratada_codigo_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratada_pessoacod_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratada_pessoanom_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratada_pessoacnpj_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratada_tipofabrica_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Servico_codigo_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_alias_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_servicocod_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Servico_nome_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Servico_sigla_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Servico_tela_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Servico_ativo_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Servico_identificacao_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_servicosigla_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Servico_vinculado_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Servico_vinculados_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Servico_responsavel_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Servicogrupo_codigo_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Servicogrupo_descricao_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Servico_isorigemreferencia_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_unidadecontratada_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_undcntnome_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_undcntsgl_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Servico_qtdcontratada_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Servico_vlrunidadecontratada_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Servico_percentual_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Servicocontrato_faturamento_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_localexec_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_tipovnc_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_prazotipo_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_prazodias_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_prazotpdias_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_prazos_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_indicadores_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_prazoanalise_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_prazoresposta_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_prazogarantia_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_prazoinicio_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_prazoimediato_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_produtividade_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_momento_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_qntuntcns_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_naorequeratr_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contrato_indicedivergencia_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_tmpestanl_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_tmpestexc_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_perctmp_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_percpgm_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_perccnc_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_ativo_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_codigofiscal_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_limiteproposta_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_calculormn_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_qtdrmn_Z_SetNull( );
               this.gxTv_SdtContratoServicos_Contratoservicos_solicitagestorsistema_Z_SetNull( );
               if ( gxTv_SdtContratoServicos_Rmn != null )
               {
                  GxSilentTrnGridCollection collectionRmn = gxTv_SdtContratoServicos_Rmn ;
                  SdtContratoServicos_Rmn currItemRmn ;
                  short idx = 1 ;
                  while ( idx <= collectionRmn.Count )
                  {
                     currItemRmn = ((SdtContratoServicos_Rmn)collectionRmn.Item(idx));
                     currItemRmn.gxTpr_Mode = "INS";
                     currItemRmn.gxTpr_Modified = 1;
                     idx = (short)(idx+1);
                  }
               }
            }
            gxTv_SdtContratoServicos_Contratoservicos_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Contrato_Codigo" )]
      [  XmlElement( ElementName = "Contrato_Codigo"   )]
      public int gxTpr_Contrato_codigo
      {
         get {
            return gxTv_SdtContratoServicos_Contrato_codigo ;
         }

         set {
            gxTv_SdtContratoServicos_Contrato_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Contrato_Numero" )]
      [  XmlElement( ElementName = "Contrato_Numero"   )]
      public String gxTpr_Contrato_numero
      {
         get {
            return gxTv_SdtContratoServicos_Contrato_numero ;
         }

         set {
            gxTv_SdtContratoServicos_Contrato_numero = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Contrato_NumeroAta" )]
      [  XmlElement( ElementName = "Contrato_NumeroAta"   )]
      public String gxTpr_Contrato_numeroata
      {
         get {
            return gxTv_SdtContratoServicos_Contrato_numeroata ;
         }

         set {
            gxTv_SdtContratoServicos_Contrato_numeroata_N = 0;
            gxTv_SdtContratoServicos_Contrato_numeroata = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contrato_numeroata_SetNull( )
      {
         gxTv_SdtContratoServicos_Contrato_numeroata_N = 1;
         gxTv_SdtContratoServicos_Contrato_numeroata = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contrato_numeroata_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_Ano" )]
      [  XmlElement( ElementName = "Contrato_Ano"   )]
      public short gxTpr_Contrato_ano
      {
         get {
            return gxTv_SdtContratoServicos_Contrato_ano ;
         }

         set {
            gxTv_SdtContratoServicos_Contrato_ano = (short)(value);
         }

      }

      [  SoapElement( ElementName = "Contrato_ValorUnidadeContratacao" )]
      [  XmlElement( ElementName = "Contrato_ValorUnidadeContratacao"   )]
      public double gxTpr_Contrato_valorunidadecontratacao_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratoServicos_Contrato_valorunidadecontratacao) ;
         }

         set {
            gxTv_SdtContratoServicos_Contrato_valorunidadecontratacao = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contrato_valorunidadecontratacao
      {
         get {
            return gxTv_SdtContratoServicos_Contrato_valorunidadecontratacao ;
         }

         set {
            gxTv_SdtContratoServicos_Contrato_valorunidadecontratacao = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "Contrato_AreaTrabalhoCod" )]
      [  XmlElement( ElementName = "Contrato_AreaTrabalhoCod"   )]
      public int gxTpr_Contrato_areatrabalhocod
      {
         get {
            return gxTv_SdtContratoServicos_Contrato_areatrabalhocod ;
         }

         set {
            gxTv_SdtContratoServicos_Contrato_areatrabalhocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Contratada_Codigo" )]
      [  XmlElement( ElementName = "Contratada_Codigo"   )]
      public int gxTpr_Contratada_codigo
      {
         get {
            return gxTv_SdtContratoServicos_Contratada_codigo ;
         }

         set {
            gxTv_SdtContratoServicos_Contratada_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Contratada_PessoaCod" )]
      [  XmlElement( ElementName = "Contratada_PessoaCod"   )]
      public int gxTpr_Contratada_pessoacod
      {
         get {
            return gxTv_SdtContratoServicos_Contratada_pessoacod ;
         }

         set {
            gxTv_SdtContratoServicos_Contratada_pessoacod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Contratada_PessoaNom" )]
      [  XmlElement( ElementName = "Contratada_PessoaNom"   )]
      public String gxTpr_Contratada_pessoanom
      {
         get {
            return gxTv_SdtContratoServicos_Contratada_pessoanom ;
         }

         set {
            gxTv_SdtContratoServicos_Contratada_pessoanom_N = 0;
            gxTv_SdtContratoServicos_Contratada_pessoanom = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratada_pessoanom_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratada_pessoanom_N = 1;
         gxTv_SdtContratoServicos_Contratada_pessoanom = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratada_pessoanom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_PessoaCNPJ" )]
      [  XmlElement( ElementName = "Contratada_PessoaCNPJ"   )]
      public String gxTpr_Contratada_pessoacnpj
      {
         get {
            return gxTv_SdtContratoServicos_Contratada_pessoacnpj ;
         }

         set {
            gxTv_SdtContratoServicos_Contratada_pessoacnpj_N = 0;
            gxTv_SdtContratoServicos_Contratada_pessoacnpj = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratada_pessoacnpj_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratada_pessoacnpj_N = 1;
         gxTv_SdtContratoServicos_Contratada_pessoacnpj = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratada_pessoacnpj_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_TipoFabrica" )]
      [  XmlElement( ElementName = "Contratada_TipoFabrica"   )]
      public String gxTpr_Contratada_tipofabrica
      {
         get {
            return gxTv_SdtContratoServicos_Contratada_tipofabrica ;
         }

         set {
            gxTv_SdtContratoServicos_Contratada_tipofabrica = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Servico_Codigo" )]
      [  XmlElement( ElementName = "Servico_Codigo"   )]
      public int gxTpr_Servico_codigo
      {
         get {
            return gxTv_SdtContratoServicos_Servico_codigo ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContratoServicos_Alias" )]
      [  XmlElement( ElementName = "ContratoServicos_Alias"   )]
      public String gxTpr_Contratoservicos_alias
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_alias ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_alias_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_alias = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_alias_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_alias_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_alias = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_alias_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_ServicoCod" )]
      [  XmlElement( ElementName = "ContratoServicos_ServicoCod"   )]
      public int gxTpr_Contratoservicos_servicocod
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_servicocod ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_servicocod = (int)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_servicocod_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_servicocod = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_servicocod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Nome" )]
      [  XmlElement( ElementName = "Servico_Nome"   )]
      public String gxTpr_Servico_nome
      {
         get {
            return gxTv_SdtContratoServicos_Servico_nome ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Servico_Sigla" )]
      [  XmlElement( ElementName = "Servico_Sigla"   )]
      public String gxTpr_Servico_sigla
      {
         get {
            return gxTv_SdtContratoServicos_Servico_sigla ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_sigla = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Servico_Tela" )]
      [  XmlElement( ElementName = "Servico_Tela"   )]
      public String gxTpr_Servico_tela
      {
         get {
            return gxTv_SdtContratoServicos_Servico_tela ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_tela_N = 0;
            gxTv_SdtContratoServicos_Servico_tela = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Servico_tela_SetNull( )
      {
         gxTv_SdtContratoServicos_Servico_tela_N = 1;
         gxTv_SdtContratoServicos_Servico_tela = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Servico_tela_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Ativo" )]
      [  XmlElement( ElementName = "Servico_Ativo"   )]
      public bool gxTpr_Servico_ativo
      {
         get {
            return gxTv_SdtContratoServicos_Servico_ativo ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_ativo = value;
         }

      }

      [  SoapElement( ElementName = "Servico_Identificacao" )]
      [  XmlElement( ElementName = "Servico_Identificacao"   )]
      public String gxTpr_Servico_identificacao
      {
         get {
            return gxTv_SdtContratoServicos_Servico_identificacao ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_identificacao = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Servico_identificacao_SetNull( )
      {
         gxTv_SdtContratoServicos_Servico_identificacao = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Servico_identificacao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_ServicoSigla" )]
      [  XmlElement( ElementName = "ContratoServicos_ServicoSigla"   )]
      public String gxTpr_Contratoservicos_servicosigla
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_servicosigla ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_servicosigla = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_servicosigla_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_servicosigla = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_servicosigla_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Descricao" )]
      [  XmlElement( ElementName = "Servico_Descricao"   )]
      public String gxTpr_Servico_descricao
      {
         get {
            return gxTv_SdtContratoServicos_Servico_descricao ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_descricao_N = 0;
            gxTv_SdtContratoServicos_Servico_descricao = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Servico_descricao_SetNull( )
      {
         gxTv_SdtContratoServicos_Servico_descricao_N = 1;
         gxTv_SdtContratoServicos_Servico_descricao = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Servico_descricao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Vinculado" )]
      [  XmlElement( ElementName = "Servico_Vinculado"   )]
      public int gxTpr_Servico_vinculado
      {
         get {
            return gxTv_SdtContratoServicos_Servico_vinculado ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_vinculado_N = 0;
            gxTv_SdtContratoServicos_Servico_vinculado = (int)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Servico_vinculado_SetNull( )
      {
         gxTv_SdtContratoServicos_Servico_vinculado_N = 1;
         gxTv_SdtContratoServicos_Servico_vinculado = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Servico_vinculado_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Vinculados" )]
      [  XmlElement( ElementName = "Servico_Vinculados"   )]
      public short gxTpr_Servico_vinculados
      {
         get {
            return gxTv_SdtContratoServicos_Servico_vinculados ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_vinculados = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Servico_vinculados_SetNull( )
      {
         gxTv_SdtContratoServicos_Servico_vinculados = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Servico_vinculados_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Responsavel" )]
      [  XmlElement( ElementName = "Servico_Responsavel"   )]
      public int gxTpr_Servico_responsavel
      {
         get {
            return gxTv_SdtContratoServicos_Servico_responsavel ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_responsavel = (int)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Servico_responsavel_SetNull( )
      {
         gxTv_SdtContratoServicos_Servico_responsavel = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Servico_responsavel_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoGrupo_Codigo" )]
      [  XmlElement( ElementName = "ServicoGrupo_Codigo"   )]
      public int gxTpr_Servicogrupo_codigo
      {
         get {
            return gxTv_SdtContratoServicos_Servicogrupo_codigo ;
         }

         set {
            gxTv_SdtContratoServicos_Servicogrupo_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ServicoGrupo_Descricao" )]
      [  XmlElement( ElementName = "ServicoGrupo_Descricao"   )]
      public String gxTpr_Servicogrupo_descricao
      {
         get {
            return gxTv_SdtContratoServicos_Servicogrupo_descricao ;
         }

         set {
            gxTv_SdtContratoServicos_Servicogrupo_descricao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Servico_IsOrigemReferencia" )]
      [  XmlElement( ElementName = "Servico_IsOrigemReferencia"   )]
      public bool gxTpr_Servico_isorigemreferencia
      {
         get {
            return gxTv_SdtContratoServicos_Servico_isorigemreferencia ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_isorigemreferencia = value;
         }

      }

      [  SoapElement( ElementName = "ContratoServicos_UnidadeContratada" )]
      [  XmlElement( ElementName = "ContratoServicos_UnidadeContratada"   )]
      public int gxTpr_Contratoservicos_unidadecontratada
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_unidadecontratada ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_unidadecontratada = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContratoServicos_UndCntNome" )]
      [  XmlElement( ElementName = "ContratoServicos_UndCntNome"   )]
      public String gxTpr_Contratoservicos_undcntnome
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_undcntnome ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_undcntnome_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_undcntnome = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_undcntnome_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_undcntnome_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_undcntnome = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_undcntnome_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_UndCntSgl" )]
      [  XmlElement( ElementName = "ContratoServicos_UndCntSgl"   )]
      public String gxTpr_Contratoservicos_undcntsgl
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_undcntsgl ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_undcntsgl_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_undcntsgl = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_undcntsgl_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_undcntsgl_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_undcntsgl = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_undcntsgl_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_QtdContratada" )]
      [  XmlElement( ElementName = "Servico_QtdContratada"   )]
      public long gxTpr_Servico_qtdcontratada
      {
         get {
            return gxTv_SdtContratoServicos_Servico_qtdcontratada ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_qtdcontratada_N = 0;
            gxTv_SdtContratoServicos_Servico_qtdcontratada = (long)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Servico_qtdcontratada_SetNull( )
      {
         gxTv_SdtContratoServicos_Servico_qtdcontratada_N = 1;
         gxTv_SdtContratoServicos_Servico_qtdcontratada = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Servico_qtdcontratada_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_VlrUnidadeContratada" )]
      [  XmlElement( ElementName = "Servico_VlrUnidadeContratada"   )]
      public double gxTpr_Servico_vlrunidadecontratada_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratoServicos_Servico_vlrunidadecontratada) ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_vlrunidadecontratada = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Servico_vlrunidadecontratada
      {
         get {
            return gxTv_SdtContratoServicos_Servico_vlrunidadecontratada ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_vlrunidadecontratada = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "Servico_Percentual" )]
      [  XmlElement( ElementName = "Servico_Percentual"   )]
      public double gxTpr_Servico_percentual_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratoServicos_Servico_percentual) ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_percentual_N = 0;
            gxTv_SdtContratoServicos_Servico_percentual = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Servico_percentual
      {
         get {
            return gxTv_SdtContratoServicos_Servico_percentual ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_percentual_N = 0;
            gxTv_SdtContratoServicos_Servico_percentual = (decimal)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Servico_percentual_SetNull( )
      {
         gxTv_SdtContratoServicos_Servico_percentual_N = 1;
         gxTv_SdtContratoServicos_Servico_percentual = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Servico_percentual_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoContrato_Faturamento" )]
      [  XmlElement( ElementName = "ServicoContrato_Faturamento"   )]
      public String gxTpr_Servicocontrato_faturamento
      {
         get {
            return gxTv_SdtContratoServicos_Servicocontrato_faturamento ;
         }

         set {
            gxTv_SdtContratoServicos_Servicocontrato_faturamento = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContratoServicos_LocalExec" )]
      [  XmlElement( ElementName = "ContratoServicos_LocalExec"   )]
      public String gxTpr_Contratoservicos_localexec
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_localexec ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_localexec = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContratoServicos_TipoVnc" )]
      [  XmlElement( ElementName = "ContratoServicos_TipoVnc"   )]
      public String gxTpr_Contratoservicos_tipovnc
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_tipovnc ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_tipovnc_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_tipovnc = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_tipovnc_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_tipovnc_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_tipovnc = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_tipovnc_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_HmlSemCnf" )]
      [  XmlElement( ElementName = "ContratoServicos_HmlSemCnf"   )]
      public bool gxTpr_Contratoservicos_hmlsemcnf
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf = value;
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf = false;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoTipo" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoTipo"   )]
      public String gxTpr_Contratoservicos_prazotipo
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazotipo ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazotipo_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_prazotipo = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazotipo_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazotipo_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_prazotipo = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazotipo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoDias" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoDias"   )]
      public short gxTpr_Contratoservicos_prazodias
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazodias ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazodias_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_prazodias = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazodias_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazodias_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_prazodias = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazodias_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoTpDias" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoTpDias"   )]
      public String gxTpr_Contratoservicos_prazotpdias
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazotpdias ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazotpdias_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_prazotpdias = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazotpdias_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazotpdias_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_prazotpdias = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazotpdias_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_Prazos" )]
      [  XmlElement( ElementName = "ContratoServicos_Prazos"   )]
      public short gxTpr_Contratoservicos_prazos
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazos ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazos_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_prazos = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazos_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazos_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_prazos = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazos_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_Indicadores" )]
      [  XmlElement( ElementName = "ContratoServicos_Indicadores"   )]
      public short gxTpr_Contratoservicos_indicadores
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_indicadores ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_indicadores_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_indicadores = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_indicadores_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_indicadores_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_indicadores = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_indicadores_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoAnalise" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoAnalise"   )]
      public short gxTpr_Contratoservicos_prazoanalise
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazoanalise ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazoanalise_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_prazoanalise = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazoanalise_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazoanalise_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_prazoanalise = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazoanalise_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoResposta" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoResposta"   )]
      public short gxTpr_Contratoservicos_prazoresposta
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazoresposta ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazoresposta_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_prazoresposta = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazoresposta_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazoresposta_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_prazoresposta = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazoresposta_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoGarantia" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoGarantia"   )]
      public short gxTpr_Contratoservicos_prazogarantia
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazogarantia ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazogarantia_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_prazogarantia = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazogarantia_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazogarantia_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_prazogarantia = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazogarantia_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoAtendeGarantia" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoAtendeGarantia"   )]
      public short gxTpr_Contratoservicos_prazoatendegarantia
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoCorrecao" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoCorrecao"   )]
      public short gxTpr_Contratoservicos_prazocorrecao
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoCorrecaoTipo" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoCorrecaoTipo"   )]
      public String gxTpr_Contratoservicos_prazocorrecaotipo
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoInicio" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoInicio"   )]
      public short gxTpr_Contratoservicos_prazoinicio
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazoinicio ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazoinicio_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_prazoinicio = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazoinicio_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazoinicio_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_prazoinicio = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazoinicio_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoImediato" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoImediato"   )]
      public bool gxTpr_Contratoservicos_prazoimediato
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazoimediato ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazoimediato_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_prazoimediato = value;
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazoimediato_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazoimediato_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_prazoimediato = false;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazoimediato_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_Produtividade" )]
      [  XmlElement( ElementName = "ContratoServicos_Produtividade"   )]
      public double gxTpr_Contratoservicos_produtividade_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratoServicos_Contratoservicos_produtividade) ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_produtividade_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_produtividade = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratoservicos_produtividade
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_produtividade ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_produtividade_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_produtividade = (decimal)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_produtividade_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_produtividade_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_produtividade = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_produtividade_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_EspelhaAceite" )]
      [  XmlElement( ElementName = "ContratoServicos_EspelhaAceite"   )]
      public bool gxTpr_Contratoservicos_espelhaaceite
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite = value;
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite = false;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_Momento" )]
      [  XmlElement( ElementName = "ContratoServicos_Momento"   )]
      public String gxTpr_Contratoservicos_momento
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_momento ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_momento_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_momento = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_momento_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_momento_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_momento = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_momento_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_StatusPagFnc" )]
      [  XmlElement( ElementName = "ContratoServicos_StatusPagFnc"   )]
      public String gxTpr_Contratoservicos_statuspagfnc
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_QntUntCns" )]
      [  XmlElement( ElementName = "ContratoServicos_QntUntCns"   )]
      public double gxTpr_Contratoservicos_qntuntcns_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratoServicos_Contratoservicos_qntuntcns) ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_qntuntcns_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_qntuntcns = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratoservicos_qntuntcns
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_qntuntcns ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_qntuntcns_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_qntuntcns = (decimal)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_qntuntcns_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_qntuntcns_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_qntuntcns = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_qntuntcns_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_FatorCnvUndCnt" )]
      [  XmlElement( ElementName = "ContratoServicos_FatorCnvUndCnt"   )]
      public double gxTpr_Contratoservicos_fatorcnvundcnt_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt) ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratoservicos_fatorcnvundcnt
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt = (decimal)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_NaoRequerAtr" )]
      [  XmlElement( ElementName = "ContratoServicos_NaoRequerAtr"   )]
      public bool gxTpr_Contratoservicos_naorequeratr
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_naorequeratr ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_naorequeratr_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_naorequeratr = value;
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_naorequeratr_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_naorequeratr_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_naorequeratr = false;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_naorequeratr_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_IndiceDivergencia" )]
      [  XmlElement( ElementName = "Contrato_IndiceDivergencia"   )]
      public double gxTpr_Contrato_indicedivergencia_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratoServicos_Contrato_indicedivergencia) ;
         }

         set {
            gxTv_SdtContratoServicos_Contrato_indicedivergencia = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contrato_indicedivergencia
      {
         get {
            return gxTv_SdtContratoServicos_Contrato_indicedivergencia ;
         }

         set {
            gxTv_SdtContratoServicos_Contrato_indicedivergencia = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContratoServicos_IndiceDivergencia" )]
      [  XmlElement( ElementName = "ContratoServicos_IndiceDivergencia"   )]
      public double gxTpr_Contratoservicos_indicedivergencia_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia) ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratoservicos_indicedivergencia
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia = (decimal)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_TmpEstAnl" )]
      [  XmlElement( ElementName = "ContratoServicos_TmpEstAnl"   )]
      public int gxTpr_Contratoservicos_tmpestanl
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_tmpestanl ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_tmpestanl_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_tmpestanl = (int)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_tmpestanl_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_tmpestanl_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_tmpestanl = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_tmpestanl_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_TmpEstExc" )]
      [  XmlElement( ElementName = "ContratoServicos_TmpEstExc"   )]
      public int gxTpr_Contratoservicos_tmpestexc
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_tmpestexc ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_tmpestexc_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_tmpestexc = (int)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_tmpestexc_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_tmpestexc_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_tmpestexc = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_tmpestexc_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_TmpEstCrr" )]
      [  XmlElement( ElementName = "ContratoServicos_TmpEstCrr"   )]
      public int gxTpr_Contratoservicos_tmpestcrr
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr = (int)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_TipoHierarquia" )]
      [  XmlElement( ElementName = "ContratoServicos_TipoHierarquia"   )]
      public short gxTpr_Contratoservicos_tipohierarquia
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PercTmp" )]
      [  XmlElement( ElementName = "ContratoServicos_PercTmp"   )]
      public short gxTpr_Contratoservicos_perctmp
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_perctmp ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_perctmp_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_perctmp = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_perctmp_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_perctmp_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_perctmp = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_perctmp_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PercPgm" )]
      [  XmlElement( ElementName = "ContratoServicos_PercPgm"   )]
      public short gxTpr_Contratoservicos_percpgm
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_percpgm ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_percpgm_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_percpgm = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_percpgm_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_percpgm_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_percpgm = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_percpgm_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PercCnc" )]
      [  XmlElement( ElementName = "ContratoServicos_PercCnc"   )]
      public short gxTpr_Contratoservicos_perccnc
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_perccnc ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_perccnc_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_perccnc = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_perccnc_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_perccnc_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_perccnc = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_perccnc_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_Ativo" )]
      [  XmlElement( ElementName = "ContratoServicos_Ativo"   )]
      public bool gxTpr_Contratoservicos_ativo
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_ativo ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_ativo = value;
         }

      }

      [  SoapElement( ElementName = "ContratoServicos_CodigoFiscal" )]
      [  XmlElement( ElementName = "ContratoServicos_CodigoFiscal"   )]
      public String gxTpr_Contratoservicos_codigofiscal
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_codigofiscal ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_codigofiscal_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_codigofiscal = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_codigofiscal_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_codigofiscal_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_codigofiscal = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_codigofiscal_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_LimiteProposta" )]
      [  XmlElement( ElementName = "ContratoServicos_LimiteProposta"   )]
      public double gxTpr_Contratoservicos_limiteproposta_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratoServicos_Contratoservicos_limiteproposta) ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_limiteproposta_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_limiteproposta = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratoservicos_limiteproposta
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_limiteproposta ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_limiteproposta_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_limiteproposta = (decimal)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_limiteproposta_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_limiteproposta_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_limiteproposta = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_limiteproposta_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_CalculoRmn" )]
      [  XmlElement( ElementName = "ContratoServicos_CalculoRmn"   )]
      public short gxTpr_Contratoservicos_calculormn
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_calculormn ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_calculormn_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_calculormn = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_calculormn_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_calculormn_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_calculormn = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_calculormn_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_QtdRmn" )]
      [  XmlElement( ElementName = "ContratoServicos_QtdRmn"   )]
      public short gxTpr_Contratoservicos_qtdrmn
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_qtdrmn ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_qtdrmn_N = 0;
            gxTv_SdtContratoServicos_Contratoservicos_qtdrmn = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_qtdrmn_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_qtdrmn_N = 1;
         gxTv_SdtContratoServicos_Contratoservicos_qtdrmn = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_qtdrmn_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_SolicitaGestorSistema" )]
      [  XmlElement( ElementName = "ContratoServicos_SolicitaGestorSistema"   )]
      public bool gxTpr_Contratoservicos_solicitagestorsistema
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_solicitagestorsistema ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_solicitagestorsistema = value;
         }

      }

      public class gxTv_SdtContratoServicos_Rmn_SdtContratoServicos_Rmn_80compatibility:SdtContratoServicos_Rmn {}
      [  SoapElement( ElementName = "Rmn" )]
      [  XmlArray( ElementName = "Rmn"  )]
      [  XmlArrayItemAttribute( Type= typeof( SdtContratoServicos_Rmn ), ElementName= "ContratoServicos.Rmn"  , IsNullable=false)]
      public GxSilentTrnGridCollection gxTpr_Rmn_GxSilentTrnGridCollection
      {
         get {
            if ( gxTv_SdtContratoServicos_Rmn == null )
            {
               gxTv_SdtContratoServicos_Rmn = new GxSilentTrnGridCollection( context, "ContratoServicos.Rmn", "GxEv3Up14_MeetrikaVs3", "SdtContratoServicos_Rmn", "GeneXus.Programs");
            }
            return (GxSilentTrnGridCollection)gxTv_SdtContratoServicos_Rmn ;
         }

         set {
            if ( gxTv_SdtContratoServicos_Rmn == null )
            {
               gxTv_SdtContratoServicos_Rmn = new GxSilentTrnGridCollection( context, "ContratoServicos.Rmn", "GxEv3Up14_MeetrikaVs3", "SdtContratoServicos_Rmn", "GeneXus.Programs");
            }
            gxTv_SdtContratoServicos_Rmn = (GxSilentTrnGridCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public GxSilentTrnGridCollection gxTpr_Rmn
      {
         get {
            if ( gxTv_SdtContratoServicos_Rmn == null )
            {
               gxTv_SdtContratoServicos_Rmn = new GxSilentTrnGridCollection( context, "ContratoServicos.Rmn", "GxEv3Up14_MeetrikaVs3", "SdtContratoServicos_Rmn", "GeneXus.Programs");
            }
            return gxTv_SdtContratoServicos_Rmn ;
         }

         set {
            gxTv_SdtContratoServicos_Rmn = value;
         }

      }

      public void gxTv_SdtContratoServicos_Rmn_SetNull( )
      {
         gxTv_SdtContratoServicos_Rmn = null;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Rmn_IsNull( )
      {
         if ( gxTv_SdtContratoServicos_Rmn == null )
         {
            return true ;
         }
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtContratoServicos_Mode ;
         }

         set {
            gxTv_SdtContratoServicos_Mode = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Mode_SetNull( )
      {
         gxTv_SdtContratoServicos_Mode = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtContratoServicos_Initialized ;
         }

         set {
            gxTv_SdtContratoServicos_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Initialized_SetNull( )
      {
         gxTv_SdtContratoServicos_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_Codigo_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_Codigo_Z"   )]
      public int gxTpr_Contratoservicos_codigo_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_codigo_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_codigo_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_Codigo_Z" )]
      [  XmlElement( ElementName = "Contrato_Codigo_Z"   )]
      public int gxTpr_Contrato_codigo_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contrato_codigo_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contrato_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contrato_codigo_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contrato_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contrato_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_Numero_Z" )]
      [  XmlElement( ElementName = "Contrato_Numero_Z"   )]
      public String gxTpr_Contrato_numero_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contrato_numero_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contrato_numero_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contrato_numero_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contrato_numero_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contrato_numero_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_NumeroAta_Z" )]
      [  XmlElement( ElementName = "Contrato_NumeroAta_Z"   )]
      public String gxTpr_Contrato_numeroata_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contrato_numeroata_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contrato_numeroata_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contrato_numeroata_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contrato_numeroata_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contrato_numeroata_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_Ano_Z" )]
      [  XmlElement( ElementName = "Contrato_Ano_Z"   )]
      public short gxTpr_Contrato_ano_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contrato_ano_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contrato_ano_Z = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contrato_ano_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contrato_ano_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contrato_ano_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_ValorUnidadeContratacao_Z" )]
      [  XmlElement( ElementName = "Contrato_ValorUnidadeContratacao_Z"   )]
      public double gxTpr_Contrato_valorunidadecontratacao_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratoServicos_Contrato_valorunidadecontratacao_Z) ;
         }

         set {
            gxTv_SdtContratoServicos_Contrato_valorunidadecontratacao_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contrato_valorunidadecontratacao_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contrato_valorunidadecontratacao_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contrato_valorunidadecontratacao_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contrato_valorunidadecontratacao_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contrato_valorunidadecontratacao_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contrato_valorunidadecontratacao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_AreaTrabalhoCod_Z" )]
      [  XmlElement( ElementName = "Contrato_AreaTrabalhoCod_Z"   )]
      public int gxTpr_Contrato_areatrabalhocod_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contrato_areatrabalhocod_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contrato_areatrabalhocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contrato_areatrabalhocod_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contrato_areatrabalhocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contrato_areatrabalhocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_Codigo_Z" )]
      [  XmlElement( ElementName = "Contratada_Codigo_Z"   )]
      public int gxTpr_Contratada_codigo_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratada_codigo_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratada_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratada_codigo_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratada_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratada_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_PessoaCod_Z" )]
      [  XmlElement( ElementName = "Contratada_PessoaCod_Z"   )]
      public int gxTpr_Contratada_pessoacod_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratada_pessoacod_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratada_pessoacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratada_pessoacod_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratada_pessoacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratada_pessoacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_PessoaNom_Z" )]
      [  XmlElement( ElementName = "Contratada_PessoaNom_Z"   )]
      public String gxTpr_Contratada_pessoanom_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratada_pessoanom_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratada_pessoanom_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratada_pessoanom_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratada_pessoanom_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratada_pessoanom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_PessoaCNPJ_Z" )]
      [  XmlElement( ElementName = "Contratada_PessoaCNPJ_Z"   )]
      public String gxTpr_Contratada_pessoacnpj_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratada_pessoacnpj_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratada_pessoacnpj_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratada_pessoacnpj_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratada_pessoacnpj_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratada_pessoacnpj_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_TipoFabrica_Z" )]
      [  XmlElement( ElementName = "Contratada_TipoFabrica_Z"   )]
      public String gxTpr_Contratada_tipofabrica_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratada_tipofabrica_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratada_tipofabrica_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratada_tipofabrica_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratada_tipofabrica_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratada_tipofabrica_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Codigo_Z" )]
      [  XmlElement( ElementName = "Servico_Codigo_Z"   )]
      public int gxTpr_Servico_codigo_Z
      {
         get {
            return gxTv_SdtContratoServicos_Servico_codigo_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Servico_codigo_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Servico_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Servico_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_Alias_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_Alias_Z"   )]
      public String gxTpr_Contratoservicos_alias_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_alias_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_alias_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_alias_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_alias_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_alias_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_ServicoCod_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_ServicoCod_Z"   )]
      public int gxTpr_Contratoservicos_servicocod_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_servicocod_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_servicocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_servicocod_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_servicocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_servicocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Nome_Z" )]
      [  XmlElement( ElementName = "Servico_Nome_Z"   )]
      public String gxTpr_Servico_nome_Z
      {
         get {
            return gxTv_SdtContratoServicos_Servico_nome_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Servico_nome_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Servico_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Servico_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Sigla_Z" )]
      [  XmlElement( ElementName = "Servico_Sigla_Z"   )]
      public String gxTpr_Servico_sigla_Z
      {
         get {
            return gxTv_SdtContratoServicos_Servico_sigla_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_sigla_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Servico_sigla_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Servico_sigla_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Servico_sigla_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Tela_Z" )]
      [  XmlElement( ElementName = "Servico_Tela_Z"   )]
      public String gxTpr_Servico_tela_Z
      {
         get {
            return gxTv_SdtContratoServicos_Servico_tela_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_tela_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Servico_tela_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Servico_tela_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Servico_tela_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Ativo_Z" )]
      [  XmlElement( ElementName = "Servico_Ativo_Z"   )]
      public bool gxTpr_Servico_ativo_Z
      {
         get {
            return gxTv_SdtContratoServicos_Servico_ativo_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_ativo_Z = value;
         }

      }

      public void gxTv_SdtContratoServicos_Servico_ativo_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Servico_ativo_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Servico_ativo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Identificacao_Z" )]
      [  XmlElement( ElementName = "Servico_Identificacao_Z"   )]
      public String gxTpr_Servico_identificacao_Z
      {
         get {
            return gxTv_SdtContratoServicos_Servico_identificacao_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_identificacao_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Servico_identificacao_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Servico_identificacao_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Servico_identificacao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_ServicoSigla_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_ServicoSigla_Z"   )]
      public String gxTpr_Contratoservicos_servicosigla_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_servicosigla_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_servicosigla_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_servicosigla_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_servicosigla_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_servicosigla_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Vinculado_Z" )]
      [  XmlElement( ElementName = "Servico_Vinculado_Z"   )]
      public int gxTpr_Servico_vinculado_Z
      {
         get {
            return gxTv_SdtContratoServicos_Servico_vinculado_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_vinculado_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Servico_vinculado_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Servico_vinculado_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Servico_vinculado_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Vinculados_Z" )]
      [  XmlElement( ElementName = "Servico_Vinculados_Z"   )]
      public short gxTpr_Servico_vinculados_Z
      {
         get {
            return gxTv_SdtContratoServicos_Servico_vinculados_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_vinculados_Z = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Servico_vinculados_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Servico_vinculados_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Servico_vinculados_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Responsavel_Z" )]
      [  XmlElement( ElementName = "Servico_Responsavel_Z"   )]
      public int gxTpr_Servico_responsavel_Z
      {
         get {
            return gxTv_SdtContratoServicos_Servico_responsavel_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_responsavel_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Servico_responsavel_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Servico_responsavel_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Servico_responsavel_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoGrupo_Codigo_Z" )]
      [  XmlElement( ElementName = "ServicoGrupo_Codigo_Z"   )]
      public int gxTpr_Servicogrupo_codigo_Z
      {
         get {
            return gxTv_SdtContratoServicos_Servicogrupo_codigo_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Servicogrupo_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Servicogrupo_codigo_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Servicogrupo_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Servicogrupo_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoGrupo_Descricao_Z" )]
      [  XmlElement( ElementName = "ServicoGrupo_Descricao_Z"   )]
      public String gxTpr_Servicogrupo_descricao_Z
      {
         get {
            return gxTv_SdtContratoServicos_Servicogrupo_descricao_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Servicogrupo_descricao_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Servicogrupo_descricao_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Servicogrupo_descricao_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Servicogrupo_descricao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_IsOrigemReferencia_Z" )]
      [  XmlElement( ElementName = "Servico_IsOrigemReferencia_Z"   )]
      public bool gxTpr_Servico_isorigemreferencia_Z
      {
         get {
            return gxTv_SdtContratoServicos_Servico_isorigemreferencia_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_isorigemreferencia_Z = value;
         }

      }

      public void gxTv_SdtContratoServicos_Servico_isorigemreferencia_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Servico_isorigemreferencia_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Servico_isorigemreferencia_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_UnidadeContratada_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_UnidadeContratada_Z"   )]
      public int gxTpr_Contratoservicos_unidadecontratada_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_unidadecontratada_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_unidadecontratada_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_unidadecontratada_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_unidadecontratada_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_unidadecontratada_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_UndCntNome_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_UndCntNome_Z"   )]
      public String gxTpr_Contratoservicos_undcntnome_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_undcntnome_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_undcntnome_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_undcntnome_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_undcntnome_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_undcntnome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_UndCntSgl_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_UndCntSgl_Z"   )]
      public String gxTpr_Contratoservicos_undcntsgl_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_undcntsgl_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_undcntsgl_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_undcntsgl_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_undcntsgl_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_undcntsgl_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_QtdContratada_Z" )]
      [  XmlElement( ElementName = "Servico_QtdContratada_Z"   )]
      public long gxTpr_Servico_qtdcontratada_Z
      {
         get {
            return gxTv_SdtContratoServicos_Servico_qtdcontratada_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_qtdcontratada_Z = (long)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Servico_qtdcontratada_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Servico_qtdcontratada_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Servico_qtdcontratada_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_VlrUnidadeContratada_Z" )]
      [  XmlElement( ElementName = "Servico_VlrUnidadeContratada_Z"   )]
      public double gxTpr_Servico_vlrunidadecontratada_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratoServicos_Servico_vlrunidadecontratada_Z) ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_vlrunidadecontratada_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Servico_vlrunidadecontratada_Z
      {
         get {
            return gxTv_SdtContratoServicos_Servico_vlrunidadecontratada_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_vlrunidadecontratada_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Servico_vlrunidadecontratada_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Servico_vlrunidadecontratada_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Servico_vlrunidadecontratada_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Percentual_Z" )]
      [  XmlElement( ElementName = "Servico_Percentual_Z"   )]
      public double gxTpr_Servico_percentual_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratoServicos_Servico_percentual_Z) ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_percentual_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Servico_percentual_Z
      {
         get {
            return gxTv_SdtContratoServicos_Servico_percentual_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_percentual_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Servico_percentual_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Servico_percentual_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Servico_percentual_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoContrato_Faturamento_Z" )]
      [  XmlElement( ElementName = "ServicoContrato_Faturamento_Z"   )]
      public String gxTpr_Servicocontrato_faturamento_Z
      {
         get {
            return gxTv_SdtContratoServicos_Servicocontrato_faturamento_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Servicocontrato_faturamento_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Servicocontrato_faturamento_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Servicocontrato_faturamento_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Servicocontrato_faturamento_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_LocalExec_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_LocalExec_Z"   )]
      public String gxTpr_Contratoservicos_localexec_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_localexec_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_localexec_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_localexec_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_localexec_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_localexec_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_TipoVnc_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_TipoVnc_Z"   )]
      public String gxTpr_Contratoservicos_tipovnc_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_tipovnc_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_tipovnc_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_tipovnc_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_tipovnc_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_tipovnc_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_HmlSemCnf_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_HmlSemCnf_Z"   )]
      public bool gxTpr_Contratoservicos_hmlsemcnf_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf_Z = value;
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoTipo_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoTipo_Z"   )]
      public String gxTpr_Contratoservicos_prazotipo_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazotipo_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazotipo_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazotipo_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazotipo_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazotipo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoDias_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoDias_Z"   )]
      public short gxTpr_Contratoservicos_prazodias_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazodias_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazodias_Z = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazodias_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazodias_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazodias_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoTpDias_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoTpDias_Z"   )]
      public String gxTpr_Contratoservicos_prazotpdias_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazotpdias_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazotpdias_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazotpdias_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazotpdias_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazotpdias_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_Prazos_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_Prazos_Z"   )]
      public short gxTpr_Contratoservicos_prazos_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazos_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazos_Z = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazos_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazos_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazos_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_Indicadores_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_Indicadores_Z"   )]
      public short gxTpr_Contratoservicos_indicadores_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_indicadores_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_indicadores_Z = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_indicadores_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_indicadores_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_indicadores_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoAnalise_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoAnalise_Z"   )]
      public short gxTpr_Contratoservicos_prazoanalise_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazoanalise_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazoanalise_Z = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazoanalise_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazoanalise_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazoanalise_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoResposta_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoResposta_Z"   )]
      public short gxTpr_Contratoservicos_prazoresposta_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazoresposta_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazoresposta_Z = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazoresposta_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazoresposta_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazoresposta_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoGarantia_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoGarantia_Z"   )]
      public short gxTpr_Contratoservicos_prazogarantia_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazogarantia_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazogarantia_Z = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazogarantia_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazogarantia_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazogarantia_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoAtendeGarantia_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoAtendeGarantia_Z"   )]
      public short gxTpr_Contratoservicos_prazoatendegarantia_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia_Z = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoCorrecao_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoCorrecao_Z"   )]
      public short gxTpr_Contratoservicos_prazocorrecao_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao_Z = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoCorrecaoTipo_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoCorrecaoTipo_Z"   )]
      public String gxTpr_Contratoservicos_prazocorrecaotipo_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoInicio_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoInicio_Z"   )]
      public short gxTpr_Contratoservicos_prazoinicio_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazoinicio_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazoinicio_Z = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazoinicio_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazoinicio_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazoinicio_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoImediato_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoImediato_Z"   )]
      public bool gxTpr_Contratoservicos_prazoimediato_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazoimediato_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazoimediato_Z = value;
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazoimediato_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazoimediato_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazoimediato_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_Produtividade_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_Produtividade_Z"   )]
      public double gxTpr_Contratoservicos_produtividade_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratoServicos_Contratoservicos_produtividade_Z) ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_produtividade_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratoservicos_produtividade_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_produtividade_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_produtividade_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_produtividade_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_produtividade_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_produtividade_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_EspelhaAceite_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_EspelhaAceite_Z"   )]
      public bool gxTpr_Contratoservicos_espelhaaceite_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite_Z = value;
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_Momento_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_Momento_Z"   )]
      public String gxTpr_Contratoservicos_momento_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_momento_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_momento_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_momento_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_momento_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_momento_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_StatusPagFnc_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_StatusPagFnc_Z"   )]
      public String gxTpr_Contratoservicos_statuspagfnc_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_QntUntCns_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_QntUntCns_Z"   )]
      public double gxTpr_Contratoservicos_qntuntcns_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratoServicos_Contratoservicos_qntuntcns_Z) ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_qntuntcns_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratoservicos_qntuntcns_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_qntuntcns_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_qntuntcns_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_qntuntcns_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_qntuntcns_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_qntuntcns_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_FatorCnvUndCnt_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_FatorCnvUndCnt_Z"   )]
      public double gxTpr_Contratoservicos_fatorcnvundcnt_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt_Z) ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratoservicos_fatorcnvundcnt_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_NaoRequerAtr_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_NaoRequerAtr_Z"   )]
      public bool gxTpr_Contratoservicos_naorequeratr_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_naorequeratr_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_naorequeratr_Z = value;
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_naorequeratr_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_naorequeratr_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_naorequeratr_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_IndiceDivergencia_Z" )]
      [  XmlElement( ElementName = "Contrato_IndiceDivergencia_Z"   )]
      public double gxTpr_Contrato_indicedivergencia_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratoServicos_Contrato_indicedivergencia_Z) ;
         }

         set {
            gxTv_SdtContratoServicos_Contrato_indicedivergencia_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contrato_indicedivergencia_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contrato_indicedivergencia_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contrato_indicedivergencia_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contrato_indicedivergencia_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contrato_indicedivergencia_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contrato_indicedivergencia_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_IndiceDivergencia_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_IndiceDivergencia_Z"   )]
      public double gxTpr_Contratoservicos_indicedivergencia_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia_Z) ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratoservicos_indicedivergencia_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_TmpEstAnl_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_TmpEstAnl_Z"   )]
      public int gxTpr_Contratoservicos_tmpestanl_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_tmpestanl_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_tmpestanl_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_tmpestanl_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_tmpestanl_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_tmpestanl_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_TmpEstExc_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_TmpEstExc_Z"   )]
      public int gxTpr_Contratoservicos_tmpestexc_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_tmpestexc_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_tmpestexc_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_tmpestexc_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_tmpestexc_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_tmpestexc_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_TmpEstCrr_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_TmpEstCrr_Z"   )]
      public int gxTpr_Contratoservicos_tmpestcrr_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_TipoHierarquia_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_TipoHierarquia_Z"   )]
      public short gxTpr_Contratoservicos_tipohierarquia_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia_Z = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PercTmp_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_PercTmp_Z"   )]
      public short gxTpr_Contratoservicos_perctmp_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_perctmp_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_perctmp_Z = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_perctmp_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_perctmp_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_perctmp_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PercPgm_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_PercPgm_Z"   )]
      public short gxTpr_Contratoservicos_percpgm_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_percpgm_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_percpgm_Z = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_percpgm_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_percpgm_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_percpgm_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PercCnc_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_PercCnc_Z"   )]
      public short gxTpr_Contratoservicos_perccnc_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_perccnc_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_perccnc_Z = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_perccnc_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_perccnc_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_perccnc_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_Ativo_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_Ativo_Z"   )]
      public bool gxTpr_Contratoservicos_ativo_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_ativo_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_ativo_Z = value;
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_ativo_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_ativo_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_ativo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_CodigoFiscal_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_CodigoFiscal_Z"   )]
      public String gxTpr_Contratoservicos_codigofiscal_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_codigofiscal_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_codigofiscal_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_codigofiscal_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_codigofiscal_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_codigofiscal_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_LimiteProposta_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_LimiteProposta_Z"   )]
      public double gxTpr_Contratoservicos_limiteproposta_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratoServicos_Contratoservicos_limiteproposta_Z) ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_limiteproposta_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratoservicos_limiteproposta_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_limiteproposta_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_limiteproposta_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_limiteproposta_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_limiteproposta_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_limiteproposta_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_CalculoRmn_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_CalculoRmn_Z"   )]
      public short gxTpr_Contratoservicos_calculormn_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_calculormn_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_calculormn_Z = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_calculormn_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_calculormn_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_calculormn_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_QtdRmn_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_QtdRmn_Z"   )]
      public short gxTpr_Contratoservicos_qtdrmn_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_qtdrmn_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_qtdrmn_Z = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_qtdrmn_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_qtdrmn_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_qtdrmn_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_SolicitaGestorSistema_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_SolicitaGestorSistema_Z"   )]
      public bool gxTpr_Contratoservicos_solicitagestorsistema_Z
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_solicitagestorsistema_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_solicitagestorsistema_Z = value;
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_solicitagestorsistema_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_solicitagestorsistema_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_solicitagestorsistema_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_NumeroAta_N" )]
      [  XmlElement( ElementName = "Contrato_NumeroAta_N"   )]
      public short gxTpr_Contrato_numeroata_N
      {
         get {
            return gxTv_SdtContratoServicos_Contrato_numeroata_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contrato_numeroata_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contrato_numeroata_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contrato_numeroata_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contrato_numeroata_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_PessoaNom_N" )]
      [  XmlElement( ElementName = "Contratada_PessoaNom_N"   )]
      public short gxTpr_Contratada_pessoanom_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratada_pessoanom_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratada_pessoanom_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratada_pessoanom_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratada_pessoanom_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratada_pessoanom_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_PessoaCNPJ_N" )]
      [  XmlElement( ElementName = "Contratada_PessoaCNPJ_N"   )]
      public short gxTpr_Contratada_pessoacnpj_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratada_pessoacnpj_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratada_pessoacnpj_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratada_pessoacnpj_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratada_pessoacnpj_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratada_pessoacnpj_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_Alias_N" )]
      [  XmlElement( ElementName = "ContratoServicos_Alias_N"   )]
      public short gxTpr_Contratoservicos_alias_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_alias_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_alias_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_alias_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_alias_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_alias_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Tela_N" )]
      [  XmlElement( ElementName = "Servico_Tela_N"   )]
      public short gxTpr_Servico_tela_N
      {
         get {
            return gxTv_SdtContratoServicos_Servico_tela_N ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_tela_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Servico_tela_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Servico_tela_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Servico_tela_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Descricao_N" )]
      [  XmlElement( ElementName = "Servico_Descricao_N"   )]
      public short gxTpr_Servico_descricao_N
      {
         get {
            return gxTv_SdtContratoServicos_Servico_descricao_N ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_descricao_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Servico_descricao_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Servico_descricao_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Servico_descricao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Vinculado_N" )]
      [  XmlElement( ElementName = "Servico_Vinculado_N"   )]
      public short gxTpr_Servico_vinculado_N
      {
         get {
            return gxTv_SdtContratoServicos_Servico_vinculado_N ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_vinculado_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Servico_vinculado_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Servico_vinculado_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Servico_vinculado_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_UndCntNome_N" )]
      [  XmlElement( ElementName = "ContratoServicos_UndCntNome_N"   )]
      public short gxTpr_Contratoservicos_undcntnome_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_undcntnome_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_undcntnome_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_undcntnome_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_undcntnome_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_undcntnome_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_UndCntSgl_N" )]
      [  XmlElement( ElementName = "ContratoServicos_UndCntSgl_N"   )]
      public short gxTpr_Contratoservicos_undcntsgl_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_undcntsgl_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_undcntsgl_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_undcntsgl_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_undcntsgl_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_undcntsgl_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_QtdContratada_N" )]
      [  XmlElement( ElementName = "Servico_QtdContratada_N"   )]
      public short gxTpr_Servico_qtdcontratada_N
      {
         get {
            return gxTv_SdtContratoServicos_Servico_qtdcontratada_N ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_qtdcontratada_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Servico_qtdcontratada_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Servico_qtdcontratada_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Servico_qtdcontratada_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Percentual_N" )]
      [  XmlElement( ElementName = "Servico_Percentual_N"   )]
      public short gxTpr_Servico_percentual_N
      {
         get {
            return gxTv_SdtContratoServicos_Servico_percentual_N ;
         }

         set {
            gxTv_SdtContratoServicos_Servico_percentual_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Servico_percentual_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Servico_percentual_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Servico_percentual_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_TipoVnc_N" )]
      [  XmlElement( ElementName = "ContratoServicos_TipoVnc_N"   )]
      public short gxTpr_Contratoservicos_tipovnc_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_tipovnc_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_tipovnc_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_tipovnc_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_tipovnc_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_tipovnc_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_HmlSemCnf_N" )]
      [  XmlElement( ElementName = "ContratoServicos_HmlSemCnf_N"   )]
      public short gxTpr_Contratoservicos_hmlsemcnf_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoTipo_N" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoTipo_N"   )]
      public short gxTpr_Contratoservicos_prazotipo_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazotipo_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazotipo_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazotipo_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazotipo_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazotipo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoDias_N" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoDias_N"   )]
      public short gxTpr_Contratoservicos_prazodias_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazodias_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazodias_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazodias_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazodias_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazodias_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoTpDias_N" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoTpDias_N"   )]
      public short gxTpr_Contratoservicos_prazotpdias_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazotpdias_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazotpdias_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazotpdias_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazotpdias_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazotpdias_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_Prazos_N" )]
      [  XmlElement( ElementName = "ContratoServicos_Prazos_N"   )]
      public short gxTpr_Contratoservicos_prazos_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazos_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazos_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazos_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazos_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazos_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_Indicadores_N" )]
      [  XmlElement( ElementName = "ContratoServicos_Indicadores_N"   )]
      public short gxTpr_Contratoservicos_indicadores_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_indicadores_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_indicadores_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_indicadores_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_indicadores_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_indicadores_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoAnalise_N" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoAnalise_N"   )]
      public short gxTpr_Contratoservicos_prazoanalise_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazoanalise_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazoanalise_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazoanalise_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazoanalise_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazoanalise_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoResposta_N" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoResposta_N"   )]
      public short gxTpr_Contratoservicos_prazoresposta_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazoresposta_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazoresposta_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazoresposta_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazoresposta_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazoresposta_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoGarantia_N" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoGarantia_N"   )]
      public short gxTpr_Contratoservicos_prazogarantia_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazogarantia_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazogarantia_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazogarantia_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazogarantia_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazogarantia_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoAtendeGarantia_N" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoAtendeGarantia_N"   )]
      public short gxTpr_Contratoservicos_prazoatendegarantia_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoCorrecao_N" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoCorrecao_N"   )]
      public short gxTpr_Contratoservicos_prazocorrecao_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoCorrecaoTipo_N" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoCorrecaoTipo_N"   )]
      public short gxTpr_Contratoservicos_prazocorrecaotipo_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoInicio_N" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoInicio_N"   )]
      public short gxTpr_Contratoservicos_prazoinicio_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazoinicio_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazoinicio_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazoinicio_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazoinicio_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazoinicio_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PrazoImediato_N" )]
      [  XmlElement( ElementName = "ContratoServicos_PrazoImediato_N"   )]
      public short gxTpr_Contratoservicos_prazoimediato_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_prazoimediato_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_prazoimediato_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_prazoimediato_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_prazoimediato_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_prazoimediato_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_Produtividade_N" )]
      [  XmlElement( ElementName = "ContratoServicos_Produtividade_N"   )]
      public short gxTpr_Contratoservicos_produtividade_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_produtividade_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_produtividade_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_produtividade_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_produtividade_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_produtividade_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_EspelhaAceite_N" )]
      [  XmlElement( ElementName = "ContratoServicos_EspelhaAceite_N"   )]
      public short gxTpr_Contratoservicos_espelhaaceite_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_Momento_N" )]
      [  XmlElement( ElementName = "ContratoServicos_Momento_N"   )]
      public short gxTpr_Contratoservicos_momento_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_momento_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_momento_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_momento_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_momento_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_momento_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_StatusPagFnc_N" )]
      [  XmlElement( ElementName = "ContratoServicos_StatusPagFnc_N"   )]
      public short gxTpr_Contratoservicos_statuspagfnc_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_QntUntCns_N" )]
      [  XmlElement( ElementName = "ContratoServicos_QntUntCns_N"   )]
      public short gxTpr_Contratoservicos_qntuntcns_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_qntuntcns_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_qntuntcns_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_qntuntcns_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_qntuntcns_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_qntuntcns_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_FatorCnvUndCnt_N" )]
      [  XmlElement( ElementName = "ContratoServicos_FatorCnvUndCnt_N"   )]
      public short gxTpr_Contratoservicos_fatorcnvundcnt_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_NaoRequerAtr_N" )]
      [  XmlElement( ElementName = "ContratoServicos_NaoRequerAtr_N"   )]
      public short gxTpr_Contratoservicos_naorequeratr_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_naorequeratr_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_naorequeratr_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_naorequeratr_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_naorequeratr_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_naorequeratr_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_IndiceDivergencia_N" )]
      [  XmlElement( ElementName = "ContratoServicos_IndiceDivergencia_N"   )]
      public short gxTpr_Contratoservicos_indicedivergencia_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_TmpEstAnl_N" )]
      [  XmlElement( ElementName = "ContratoServicos_TmpEstAnl_N"   )]
      public short gxTpr_Contratoservicos_tmpestanl_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_tmpestanl_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_tmpestanl_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_tmpestanl_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_tmpestanl_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_tmpestanl_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_TmpEstExc_N" )]
      [  XmlElement( ElementName = "ContratoServicos_TmpEstExc_N"   )]
      public short gxTpr_Contratoservicos_tmpestexc_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_tmpestexc_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_tmpestexc_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_tmpestexc_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_tmpestexc_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_tmpestexc_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_TmpEstCrr_N" )]
      [  XmlElement( ElementName = "ContratoServicos_TmpEstCrr_N"   )]
      public short gxTpr_Contratoservicos_tmpestcrr_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_TipoHierarquia_N" )]
      [  XmlElement( ElementName = "ContratoServicos_TipoHierarquia_N"   )]
      public short gxTpr_Contratoservicos_tipohierarquia_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PercTmp_N" )]
      [  XmlElement( ElementName = "ContratoServicos_PercTmp_N"   )]
      public short gxTpr_Contratoservicos_perctmp_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_perctmp_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_perctmp_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_perctmp_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_perctmp_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_perctmp_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PercPgm_N" )]
      [  XmlElement( ElementName = "ContratoServicos_PercPgm_N"   )]
      public short gxTpr_Contratoservicos_percpgm_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_percpgm_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_percpgm_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_percpgm_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_percpgm_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_percpgm_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_PercCnc_N" )]
      [  XmlElement( ElementName = "ContratoServicos_PercCnc_N"   )]
      public short gxTpr_Contratoservicos_perccnc_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_perccnc_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_perccnc_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_perccnc_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_perccnc_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_perccnc_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_CodigoFiscal_N" )]
      [  XmlElement( ElementName = "ContratoServicos_CodigoFiscal_N"   )]
      public short gxTpr_Contratoservicos_codigofiscal_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_codigofiscal_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_codigofiscal_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_codigofiscal_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_codigofiscal_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_codigofiscal_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_LimiteProposta_N" )]
      [  XmlElement( ElementName = "ContratoServicos_LimiteProposta_N"   )]
      public short gxTpr_Contratoservicos_limiteproposta_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_limiteproposta_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_limiteproposta_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_limiteproposta_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_limiteproposta_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_limiteproposta_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_CalculoRmn_N" )]
      [  XmlElement( ElementName = "ContratoServicos_CalculoRmn_N"   )]
      public short gxTpr_Contratoservicos_calculormn_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_calculormn_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_calculormn_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_calculormn_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_calculormn_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_calculormn_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_QtdRmn_N" )]
      [  XmlElement( ElementName = "ContratoServicos_QtdRmn_N"   )]
      public short gxTpr_Contratoservicos_qtdrmn_N
      {
         get {
            return gxTv_SdtContratoServicos_Contratoservicos_qtdrmn_N ;
         }

         set {
            gxTv_SdtContratoServicos_Contratoservicos_qtdrmn_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Contratoservicos_qtdrmn_N_SetNull( )
      {
         gxTv_SdtContratoServicos_Contratoservicos_qtdrmn_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Contratoservicos_qtdrmn_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtContratoServicos_Contrato_numero = "";
         gxTv_SdtContratoServicos_Contrato_numeroata = "";
         gxTv_SdtContratoServicos_Contratada_pessoanom = "";
         gxTv_SdtContratoServicos_Contratada_pessoacnpj = "";
         gxTv_SdtContratoServicos_Contratada_tipofabrica = "S";
         gxTv_SdtContratoServicos_Contratoservicos_alias = "";
         gxTv_SdtContratoServicos_Servico_nome = "";
         gxTv_SdtContratoServicos_Servico_sigla = "";
         gxTv_SdtContratoServicos_Servico_tela = "";
         gxTv_SdtContratoServicos_Servico_identificacao = "";
         gxTv_SdtContratoServicos_Contratoservicos_servicosigla = "";
         gxTv_SdtContratoServicos_Servico_descricao = "";
         gxTv_SdtContratoServicos_Servicogrupo_descricao = "";
         gxTv_SdtContratoServicos_Contratoservicos_undcntnome = "";
         gxTv_SdtContratoServicos_Contratoservicos_undcntsgl = "";
         gxTv_SdtContratoServicos_Servicocontrato_faturamento = "B";
         gxTv_SdtContratoServicos_Contratoservicos_localexec = "";
         gxTv_SdtContratoServicos_Contratoservicos_tipovnc = "";
         gxTv_SdtContratoServicos_Contratoservicos_prazotipo = "";
         gxTv_SdtContratoServicos_Contratoservicos_prazotpdias = "";
         gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo = "";
         gxTv_SdtContratoServicos_Contratoservicos_momento = "";
         gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc = "";
         gxTv_SdtContratoServicos_Contratoservicos_codigofiscal = "";
         gxTv_SdtContratoServicos_Mode = "";
         gxTv_SdtContratoServicos_Contrato_numero_Z = "";
         gxTv_SdtContratoServicos_Contrato_numeroata_Z = "";
         gxTv_SdtContratoServicos_Contratada_pessoanom_Z = "";
         gxTv_SdtContratoServicos_Contratada_pessoacnpj_Z = "";
         gxTv_SdtContratoServicos_Contratada_tipofabrica_Z = "";
         gxTv_SdtContratoServicos_Contratoservicos_alias_Z = "";
         gxTv_SdtContratoServicos_Servico_nome_Z = "";
         gxTv_SdtContratoServicos_Servico_sigla_Z = "";
         gxTv_SdtContratoServicos_Servico_tela_Z = "";
         gxTv_SdtContratoServicos_Servico_identificacao_Z = "";
         gxTv_SdtContratoServicos_Contratoservicos_servicosigla_Z = "";
         gxTv_SdtContratoServicos_Servicogrupo_descricao_Z = "";
         gxTv_SdtContratoServicos_Contratoservicos_undcntnome_Z = "";
         gxTv_SdtContratoServicos_Contratoservicos_undcntsgl_Z = "";
         gxTv_SdtContratoServicos_Servicocontrato_faturamento_Z = "";
         gxTv_SdtContratoServicos_Contratoservicos_localexec_Z = "";
         gxTv_SdtContratoServicos_Contratoservicos_tipovnc_Z = "";
         gxTv_SdtContratoServicos_Contratoservicos_prazotipo_Z = "";
         gxTv_SdtContratoServicos_Contratoservicos_prazotpdias_Z = "";
         gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo_Z = "";
         gxTv_SdtContratoServicos_Contratoservicos_momento_Z = "";
         gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc_Z = "";
         gxTv_SdtContratoServicos_Contratoservicos_codigofiscal_Z = "";
         gxTv_SdtContratoServicos_Servico_ativo = true;
         gxTv_SdtContratoServicos_Contratoservicos_prazoinicio = 1;
         gxTv_SdtContratoServicos_Contrato_indicedivergencia = (decimal)(10);
         gxTv_SdtContratoServicos_Contratoservicos_ativo = true;
         gxTv_SdtContratoServicos_Contratoservicos_solicitagestorsistema = false;
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "contratoservicos", "GeneXus.Programs.contratoservicos_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtContratoServicos_Contrato_ano ;
      private short gxTv_SdtContratoServicos_Servico_vinculados ;
      private short gxTv_SdtContratoServicos_Contratoservicos_prazodias ;
      private short gxTv_SdtContratoServicos_Contratoservicos_prazos ;
      private short gxTv_SdtContratoServicos_Contratoservicos_indicadores ;
      private short gxTv_SdtContratoServicos_Contratoservicos_prazoanalise ;
      private short gxTv_SdtContratoServicos_Contratoservicos_prazoresposta ;
      private short gxTv_SdtContratoServicos_Contratoservicos_prazogarantia ;
      private short gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia ;
      private short gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao ;
      private short gxTv_SdtContratoServicos_Contratoservicos_prazoinicio ;
      private short gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia ;
      private short gxTv_SdtContratoServicos_Contratoservicos_perctmp ;
      private short gxTv_SdtContratoServicos_Contratoservicos_percpgm ;
      private short gxTv_SdtContratoServicos_Contratoservicos_perccnc ;
      private short gxTv_SdtContratoServicos_Contratoservicos_calculormn ;
      private short gxTv_SdtContratoServicos_Contratoservicos_qtdrmn ;
      private short gxTv_SdtContratoServicos_Initialized ;
      private short gxTv_SdtContratoServicos_Contrato_ano_Z ;
      private short gxTv_SdtContratoServicos_Servico_vinculados_Z ;
      private short gxTv_SdtContratoServicos_Contratoservicos_prazodias_Z ;
      private short gxTv_SdtContratoServicos_Contratoservicos_prazos_Z ;
      private short gxTv_SdtContratoServicos_Contratoservicos_indicadores_Z ;
      private short gxTv_SdtContratoServicos_Contratoservicos_prazoanalise_Z ;
      private short gxTv_SdtContratoServicos_Contratoservicos_prazoresposta_Z ;
      private short gxTv_SdtContratoServicos_Contratoservicos_prazogarantia_Z ;
      private short gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia_Z ;
      private short gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao_Z ;
      private short gxTv_SdtContratoServicos_Contratoservicos_prazoinicio_Z ;
      private short gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia_Z ;
      private short gxTv_SdtContratoServicos_Contratoservicos_perctmp_Z ;
      private short gxTv_SdtContratoServicos_Contratoservicos_percpgm_Z ;
      private short gxTv_SdtContratoServicos_Contratoservicos_perccnc_Z ;
      private short gxTv_SdtContratoServicos_Contratoservicos_calculormn_Z ;
      private short gxTv_SdtContratoServicos_Contratoservicos_qtdrmn_Z ;
      private short gxTv_SdtContratoServicos_Contrato_numeroata_N ;
      private short gxTv_SdtContratoServicos_Contratada_pessoanom_N ;
      private short gxTv_SdtContratoServicos_Contratada_pessoacnpj_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_alias_N ;
      private short gxTv_SdtContratoServicos_Servico_tela_N ;
      private short gxTv_SdtContratoServicos_Servico_descricao_N ;
      private short gxTv_SdtContratoServicos_Servico_vinculado_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_undcntnome_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_undcntsgl_N ;
      private short gxTv_SdtContratoServicos_Servico_qtdcontratada_N ;
      private short gxTv_SdtContratoServicos_Servico_percentual_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_tipovnc_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_prazotipo_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_prazodias_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_prazotpdias_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_prazos_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_indicadores_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_prazoanalise_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_prazoresposta_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_prazogarantia_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_prazoatendegarantia_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_prazocorrecao_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_prazoinicio_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_prazoimediato_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_produtividade_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_momento_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_qntuntcns_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_naorequeratr_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_tmpestanl_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_tmpestexc_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_tipohierarquia_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_perctmp_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_percpgm_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_perccnc_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_codigofiscal_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_limiteproposta_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_calculormn_N ;
      private short gxTv_SdtContratoServicos_Contratoservicos_qtdrmn_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtContratoServicos_Contratoservicos_codigo ;
      private int gxTv_SdtContratoServicos_Contrato_codigo ;
      private int gxTv_SdtContratoServicos_Contrato_areatrabalhocod ;
      private int gxTv_SdtContratoServicos_Contratada_codigo ;
      private int gxTv_SdtContratoServicos_Contratada_pessoacod ;
      private int gxTv_SdtContratoServicos_Servico_codigo ;
      private int gxTv_SdtContratoServicos_Contratoservicos_servicocod ;
      private int gxTv_SdtContratoServicos_Servico_vinculado ;
      private int gxTv_SdtContratoServicos_Servico_responsavel ;
      private int gxTv_SdtContratoServicos_Servicogrupo_codigo ;
      private int gxTv_SdtContratoServicos_Contratoservicos_unidadecontratada ;
      private int gxTv_SdtContratoServicos_Contratoservicos_tmpestanl ;
      private int gxTv_SdtContratoServicos_Contratoservicos_tmpestexc ;
      private int gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr ;
      private int gxTv_SdtContratoServicos_Contratoservicos_codigo_Z ;
      private int gxTv_SdtContratoServicos_Contrato_codigo_Z ;
      private int gxTv_SdtContratoServicos_Contrato_areatrabalhocod_Z ;
      private int gxTv_SdtContratoServicos_Contratada_codigo_Z ;
      private int gxTv_SdtContratoServicos_Contratada_pessoacod_Z ;
      private int gxTv_SdtContratoServicos_Servico_codigo_Z ;
      private int gxTv_SdtContratoServicos_Contratoservicos_servicocod_Z ;
      private int gxTv_SdtContratoServicos_Servico_vinculado_Z ;
      private int gxTv_SdtContratoServicos_Servico_responsavel_Z ;
      private int gxTv_SdtContratoServicos_Servicogrupo_codigo_Z ;
      private int gxTv_SdtContratoServicos_Contratoservicos_unidadecontratada_Z ;
      private int gxTv_SdtContratoServicos_Contratoservicos_tmpestanl_Z ;
      private int gxTv_SdtContratoServicos_Contratoservicos_tmpestexc_Z ;
      private int gxTv_SdtContratoServicos_Contratoservicos_tmpestcrr_Z ;
      private long gxTv_SdtContratoServicos_Servico_qtdcontratada ;
      private long gxTv_SdtContratoServicos_Servico_qtdcontratada_Z ;
      private decimal gxTv_SdtContratoServicos_Contrato_valorunidadecontratacao ;
      private decimal gxTv_SdtContratoServicos_Servico_vlrunidadecontratada ;
      private decimal gxTv_SdtContratoServicos_Servico_percentual ;
      private decimal gxTv_SdtContratoServicos_Contratoservicos_produtividade ;
      private decimal gxTv_SdtContratoServicos_Contratoservicos_qntuntcns ;
      private decimal gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt ;
      private decimal gxTv_SdtContratoServicos_Contrato_indicedivergencia ;
      private decimal gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia ;
      private decimal gxTv_SdtContratoServicos_Contratoservicos_limiteproposta ;
      private decimal gxTv_SdtContratoServicos_Contrato_valorunidadecontratacao_Z ;
      private decimal gxTv_SdtContratoServicos_Servico_vlrunidadecontratada_Z ;
      private decimal gxTv_SdtContratoServicos_Servico_percentual_Z ;
      private decimal gxTv_SdtContratoServicos_Contratoservicos_produtividade_Z ;
      private decimal gxTv_SdtContratoServicos_Contratoservicos_qntuntcns_Z ;
      private decimal gxTv_SdtContratoServicos_Contratoservicos_fatorcnvundcnt_Z ;
      private decimal gxTv_SdtContratoServicos_Contrato_indicedivergencia_Z ;
      private decimal gxTv_SdtContratoServicos_Contratoservicos_indicedivergencia_Z ;
      private decimal gxTv_SdtContratoServicos_Contratoservicos_limiteproposta_Z ;
      private String gxTv_SdtContratoServicos_Contrato_numero ;
      private String gxTv_SdtContratoServicos_Contrato_numeroata ;
      private String gxTv_SdtContratoServicos_Contratada_pessoanom ;
      private String gxTv_SdtContratoServicos_Contratada_tipofabrica ;
      private String gxTv_SdtContratoServicos_Contratoservicos_alias ;
      private String gxTv_SdtContratoServicos_Servico_nome ;
      private String gxTv_SdtContratoServicos_Servico_sigla ;
      private String gxTv_SdtContratoServicos_Servico_tela ;
      private String gxTv_SdtContratoServicos_Contratoservicos_servicosigla ;
      private String gxTv_SdtContratoServicos_Contratoservicos_undcntnome ;
      private String gxTv_SdtContratoServicos_Contratoservicos_undcntsgl ;
      private String gxTv_SdtContratoServicos_Contratoservicos_localexec ;
      private String gxTv_SdtContratoServicos_Contratoservicos_tipovnc ;
      private String gxTv_SdtContratoServicos_Contratoservicos_prazotipo ;
      private String gxTv_SdtContratoServicos_Contratoservicos_prazotpdias ;
      private String gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo ;
      private String gxTv_SdtContratoServicos_Contratoservicos_momento ;
      private String gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc ;
      private String gxTv_SdtContratoServicos_Contratoservicos_codigofiscal ;
      private String gxTv_SdtContratoServicos_Mode ;
      private String gxTv_SdtContratoServicos_Contrato_numero_Z ;
      private String gxTv_SdtContratoServicos_Contrato_numeroata_Z ;
      private String gxTv_SdtContratoServicos_Contratada_pessoanom_Z ;
      private String gxTv_SdtContratoServicos_Contratada_tipofabrica_Z ;
      private String gxTv_SdtContratoServicos_Contratoservicos_alias_Z ;
      private String gxTv_SdtContratoServicos_Servico_nome_Z ;
      private String gxTv_SdtContratoServicos_Servico_sigla_Z ;
      private String gxTv_SdtContratoServicos_Servico_tela_Z ;
      private String gxTv_SdtContratoServicos_Contratoservicos_servicosigla_Z ;
      private String gxTv_SdtContratoServicos_Contratoservicos_undcntnome_Z ;
      private String gxTv_SdtContratoServicos_Contratoservicos_undcntsgl_Z ;
      private String gxTv_SdtContratoServicos_Contratoservicos_localexec_Z ;
      private String gxTv_SdtContratoServicos_Contratoservicos_tipovnc_Z ;
      private String gxTv_SdtContratoServicos_Contratoservicos_prazotipo_Z ;
      private String gxTv_SdtContratoServicos_Contratoservicos_prazotpdias_Z ;
      private String gxTv_SdtContratoServicos_Contratoservicos_prazocorrecaotipo_Z ;
      private String gxTv_SdtContratoServicos_Contratoservicos_momento_Z ;
      private String gxTv_SdtContratoServicos_Contratoservicos_statuspagfnc_Z ;
      private String gxTv_SdtContratoServicos_Contratoservicos_codigofiscal_Z ;
      private String sTagName ;
      private bool gxTv_SdtContratoServicos_Servico_ativo ;
      private bool gxTv_SdtContratoServicos_Servico_isorigemreferencia ;
      private bool gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf ;
      private bool gxTv_SdtContratoServicos_Contratoservicos_prazoimediato ;
      private bool gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite ;
      private bool gxTv_SdtContratoServicos_Contratoservicos_naorequeratr ;
      private bool gxTv_SdtContratoServicos_Contratoservicos_ativo ;
      private bool gxTv_SdtContratoServicos_Contratoservicos_solicitagestorsistema ;
      private bool gxTv_SdtContratoServicos_Servico_ativo_Z ;
      private bool gxTv_SdtContratoServicos_Servico_isorigemreferencia_Z ;
      private bool gxTv_SdtContratoServicos_Contratoservicos_hmlsemcnf_Z ;
      private bool gxTv_SdtContratoServicos_Contratoservicos_prazoimediato_Z ;
      private bool gxTv_SdtContratoServicos_Contratoservicos_espelhaaceite_Z ;
      private bool gxTv_SdtContratoServicos_Contratoservicos_naorequeratr_Z ;
      private bool gxTv_SdtContratoServicos_Contratoservicos_ativo_Z ;
      private bool gxTv_SdtContratoServicos_Contratoservicos_solicitagestorsistema_Z ;
      private String gxTv_SdtContratoServicos_Servico_descricao ;
      private String gxTv_SdtContratoServicos_Contratada_pessoacnpj ;
      private String gxTv_SdtContratoServicos_Servico_identificacao ;
      private String gxTv_SdtContratoServicos_Servicogrupo_descricao ;
      private String gxTv_SdtContratoServicos_Servicocontrato_faturamento ;
      private String gxTv_SdtContratoServicos_Contratada_pessoacnpj_Z ;
      private String gxTv_SdtContratoServicos_Servico_identificacao_Z ;
      private String gxTv_SdtContratoServicos_Servicogrupo_descricao_Z ;
      private String gxTv_SdtContratoServicos_Servicocontrato_faturamento_Z ;
      private Assembly constructorCallingAssembly ;
      [ObjectCollection(ItemType=typeof( SdtContratoServicos_Rmn ))]
      private GxSilentTrnGridCollection gxTv_SdtContratoServicos_Rmn=null ;
   }

   [DataContract(Name = @"ContratoServicos", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtContratoServicos_RESTInterface : GxGenericCollectionItem<SdtContratoServicos>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContratoServicos_RESTInterface( ) : base()
      {
      }

      public SdtContratoServicos_RESTInterface( SdtContratoServicos psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContratoServicos_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratoservicos_codigo
      {
         get {
            return sdt.gxTpr_Contratoservicos_codigo ;
         }

         set {
            sdt.gxTpr_Contratoservicos_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contrato_Codigo" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contrato_codigo
      {
         get {
            return sdt.gxTpr_Contrato_codigo ;
         }

         set {
            sdt.gxTpr_Contrato_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contrato_Numero" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Contrato_numero
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contrato_numero) ;
         }

         set {
            sdt.gxTpr_Contrato_numero = (String)(value);
         }

      }

      [DataMember( Name = "Contrato_NumeroAta" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Contrato_numeroata
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contrato_numeroata) ;
         }

         set {
            sdt.gxTpr_Contrato_numeroata = (String)(value);
         }

      }

      [DataMember( Name = "Contrato_Ano" , Order = 4 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contrato_ano
      {
         get {
            return sdt.gxTpr_Contrato_ano ;
         }

         set {
            sdt.gxTpr_Contrato_ano = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contrato_ValorUnidadeContratacao" , Order = 5 )]
      [GxSeudo()]
      public String gxTpr_Contrato_valorunidadecontratacao
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contrato_valorunidadecontratacao, 18, 5)) ;
         }

         set {
            sdt.gxTpr_Contrato_valorunidadecontratacao = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "Contrato_AreaTrabalhoCod" , Order = 6 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contrato_areatrabalhocod
      {
         get {
            return sdt.gxTpr_Contrato_areatrabalhocod ;
         }

         set {
            sdt.gxTpr_Contrato_areatrabalhocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contratada_Codigo" , Order = 7 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratada_codigo
      {
         get {
            return sdt.gxTpr_Contratada_codigo ;
         }

         set {
            sdt.gxTpr_Contratada_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contratada_PessoaCod" , Order = 8 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratada_pessoacod
      {
         get {
            return sdt.gxTpr_Contratada_pessoacod ;
         }

         set {
            sdt.gxTpr_Contratada_pessoacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contratada_PessoaNom" , Order = 9 )]
      [GxSeudo()]
      public String gxTpr_Contratada_pessoanom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratada_pessoanom) ;
         }

         set {
            sdt.gxTpr_Contratada_pessoanom = (String)(value);
         }

      }

      [DataMember( Name = "Contratada_PessoaCNPJ" , Order = 10 )]
      [GxSeudo()]
      public String gxTpr_Contratada_pessoacnpj
      {
         get {
            return sdt.gxTpr_Contratada_pessoacnpj ;
         }

         set {
            sdt.gxTpr_Contratada_pessoacnpj = (String)(value);
         }

      }

      [DataMember( Name = "Contratada_TipoFabrica" , Order = 11 )]
      [GxSeudo()]
      public String gxTpr_Contratada_tipofabrica
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratada_tipofabrica) ;
         }

         set {
            sdt.gxTpr_Contratada_tipofabrica = (String)(value);
         }

      }

      [DataMember( Name = "Servico_Codigo" , Order = 12 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Servico_codigo
      {
         get {
            return sdt.gxTpr_Servico_codigo ;
         }

         set {
            sdt.gxTpr_Servico_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicos_Alias" , Order = 13 )]
      [GxSeudo()]
      public String gxTpr_Contratoservicos_alias
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratoservicos_alias) ;
         }

         set {
            sdt.gxTpr_Contratoservicos_alias = (String)(value);
         }

      }

      [DataMember( Name = "ContratoServicos_ServicoCod" , Order = 14 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratoservicos_servicocod
      {
         get {
            return sdt.gxTpr_Contratoservicos_servicocod ;
         }

         set {
            sdt.gxTpr_Contratoservicos_servicocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Servico_Nome" , Order = 15 )]
      [GxSeudo()]
      public String gxTpr_Servico_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Servico_nome) ;
         }

         set {
            sdt.gxTpr_Servico_nome = (String)(value);
         }

      }

      [DataMember( Name = "Servico_Sigla" , Order = 16 )]
      [GxSeudo()]
      public String gxTpr_Servico_sigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Servico_sigla) ;
         }

         set {
            sdt.gxTpr_Servico_sigla = (String)(value);
         }

      }

      [DataMember( Name = "Servico_Tela" , Order = 17 )]
      [GxSeudo()]
      public String gxTpr_Servico_tela
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Servico_tela) ;
         }

         set {
            sdt.gxTpr_Servico_tela = (String)(value);
         }

      }

      [DataMember( Name = "Servico_Ativo" , Order = 18 )]
      [GxSeudo()]
      public bool gxTpr_Servico_ativo
      {
         get {
            return sdt.gxTpr_Servico_ativo ;
         }

         set {
            sdt.gxTpr_Servico_ativo = value;
         }

      }

      [DataMember( Name = "Servico_Identificacao" , Order = 19 )]
      [GxSeudo()]
      public String gxTpr_Servico_identificacao
      {
         get {
            return sdt.gxTpr_Servico_identificacao ;
         }

         set {
            sdt.gxTpr_Servico_identificacao = (String)(value);
         }

      }

      [DataMember( Name = "ContratoServicos_ServicoSigla" , Order = 20 )]
      [GxSeudo()]
      public String gxTpr_Contratoservicos_servicosigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratoservicos_servicosigla) ;
         }

         set {
            sdt.gxTpr_Contratoservicos_servicosigla = (String)(value);
         }

      }

      [DataMember( Name = "Servico_Descricao" , Order = 21 )]
      public String gxTpr_Servico_descricao
      {
         get {
            return sdt.gxTpr_Servico_descricao ;
         }

         set {
            sdt.gxTpr_Servico_descricao = (String)(value);
         }

      }

      [DataMember( Name = "Servico_Vinculado" , Order = 22 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Servico_vinculado
      {
         get {
            return sdt.gxTpr_Servico_vinculado ;
         }

         set {
            sdt.gxTpr_Servico_vinculado = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Servico_Vinculados" , Order = 23 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Servico_vinculados
      {
         get {
            return sdt.gxTpr_Servico_vinculados ;
         }

         set {
            sdt.gxTpr_Servico_vinculados = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Servico_Responsavel" , Order = 24 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Servico_responsavel
      {
         get {
            return sdt.gxTpr_Servico_responsavel ;
         }

         set {
            sdt.gxTpr_Servico_responsavel = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ServicoGrupo_Codigo" , Order = 25 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Servicogrupo_codigo
      {
         get {
            return sdt.gxTpr_Servicogrupo_codigo ;
         }

         set {
            sdt.gxTpr_Servicogrupo_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ServicoGrupo_Descricao" , Order = 26 )]
      [GxSeudo()]
      public String gxTpr_Servicogrupo_descricao
      {
         get {
            return sdt.gxTpr_Servicogrupo_descricao ;
         }

         set {
            sdt.gxTpr_Servicogrupo_descricao = (String)(value);
         }

      }

      [DataMember( Name = "Servico_IsOrigemReferencia" , Order = 27 )]
      [GxSeudo()]
      public bool gxTpr_Servico_isorigemreferencia
      {
         get {
            return sdt.gxTpr_Servico_isorigemreferencia ;
         }

         set {
            sdt.gxTpr_Servico_isorigemreferencia = value;
         }

      }

      [DataMember( Name = "ContratoServicos_UnidadeContratada" , Order = 28 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratoservicos_unidadecontratada
      {
         get {
            return sdt.gxTpr_Contratoservicos_unidadecontratada ;
         }

         set {
            sdt.gxTpr_Contratoservicos_unidadecontratada = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicos_UndCntNome" , Order = 29 )]
      [GxSeudo()]
      public String gxTpr_Contratoservicos_undcntnome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratoservicos_undcntnome) ;
         }

         set {
            sdt.gxTpr_Contratoservicos_undcntnome = (String)(value);
         }

      }

      [DataMember( Name = "ContratoServicos_UndCntSgl" , Order = 30 )]
      [GxSeudo()]
      public String gxTpr_Contratoservicos_undcntsgl
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratoservicos_undcntsgl) ;
         }

         set {
            sdt.gxTpr_Contratoservicos_undcntsgl = (String)(value);
         }

      }

      [DataMember( Name = "Servico_QtdContratada" , Order = 31 )]
      [GxSeudo()]
      public String gxTpr_Servico_qtdcontratada
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Servico_qtdcontratada), 10, 0)) ;
         }

         set {
            sdt.gxTpr_Servico_qtdcontratada = (long)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "Servico_VlrUnidadeContratada" , Order = 32 )]
      [GxSeudo()]
      public String gxTpr_Servico_vlrunidadecontratada
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Servico_vlrunidadecontratada, 18, 5)) ;
         }

         set {
            sdt.gxTpr_Servico_vlrunidadecontratada = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "Servico_Percentual" , Order = 33 )]
      [GxSeudo()]
      public String gxTpr_Servico_percentual
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Servico_percentual, 7, 3)) ;
         }

         set {
            sdt.gxTpr_Servico_percentual = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ServicoContrato_Faturamento" , Order = 34 )]
      [GxSeudo()]
      public String gxTpr_Servicocontrato_faturamento
      {
         get {
            return sdt.gxTpr_Servicocontrato_faturamento ;
         }

         set {
            sdt.gxTpr_Servicocontrato_faturamento = (String)(value);
         }

      }

      [DataMember( Name = "ContratoServicos_LocalExec" , Order = 35 )]
      [GxSeudo()]
      public String gxTpr_Contratoservicos_localexec
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratoservicos_localexec) ;
         }

         set {
            sdt.gxTpr_Contratoservicos_localexec = (String)(value);
         }

      }

      [DataMember( Name = "ContratoServicos_TipoVnc" , Order = 36 )]
      [GxSeudo()]
      public String gxTpr_Contratoservicos_tipovnc
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratoservicos_tipovnc) ;
         }

         set {
            sdt.gxTpr_Contratoservicos_tipovnc = (String)(value);
         }

      }

      [DataMember( Name = "ContratoServicos_HmlSemCnf" , Order = 37 )]
      [GxSeudo()]
      public bool gxTpr_Contratoservicos_hmlsemcnf
      {
         get {
            return sdt.gxTpr_Contratoservicos_hmlsemcnf ;
         }

         set {
            sdt.gxTpr_Contratoservicos_hmlsemcnf = value;
         }

      }

      [DataMember( Name = "ContratoServicos_PrazoTipo" , Order = 38 )]
      [GxSeudo()]
      public String gxTpr_Contratoservicos_prazotipo
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratoservicos_prazotipo) ;
         }

         set {
            sdt.gxTpr_Contratoservicos_prazotipo = (String)(value);
         }

      }

      [DataMember( Name = "ContratoServicos_PrazoDias" , Order = 39 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contratoservicos_prazodias
      {
         get {
            return sdt.gxTpr_Contratoservicos_prazodias ;
         }

         set {
            sdt.gxTpr_Contratoservicos_prazodias = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicos_PrazoTpDias" , Order = 40 )]
      [GxSeudo()]
      public String gxTpr_Contratoservicos_prazotpdias
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratoservicos_prazotpdias) ;
         }

         set {
            sdt.gxTpr_Contratoservicos_prazotpdias = (String)(value);
         }

      }

      [DataMember( Name = "ContratoServicos_Prazos" , Order = 41 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contratoservicos_prazos
      {
         get {
            return sdt.gxTpr_Contratoservicos_prazos ;
         }

         set {
            sdt.gxTpr_Contratoservicos_prazos = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicos_Indicadores" , Order = 42 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contratoservicos_indicadores
      {
         get {
            return sdt.gxTpr_Contratoservicos_indicadores ;
         }

         set {
            sdt.gxTpr_Contratoservicos_indicadores = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicos_PrazoAnalise" , Order = 43 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contratoservicos_prazoanalise
      {
         get {
            return sdt.gxTpr_Contratoservicos_prazoanalise ;
         }

         set {
            sdt.gxTpr_Contratoservicos_prazoanalise = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicos_PrazoResposta" , Order = 44 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contratoservicos_prazoresposta
      {
         get {
            return sdt.gxTpr_Contratoservicos_prazoresposta ;
         }

         set {
            sdt.gxTpr_Contratoservicos_prazoresposta = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicos_PrazoGarantia" , Order = 45 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contratoservicos_prazogarantia
      {
         get {
            return sdt.gxTpr_Contratoservicos_prazogarantia ;
         }

         set {
            sdt.gxTpr_Contratoservicos_prazogarantia = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicos_PrazoAtendeGarantia" , Order = 46 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contratoservicos_prazoatendegarantia
      {
         get {
            return sdt.gxTpr_Contratoservicos_prazoatendegarantia ;
         }

         set {
            sdt.gxTpr_Contratoservicos_prazoatendegarantia = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicos_PrazoCorrecao" , Order = 47 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contratoservicos_prazocorrecao
      {
         get {
            return sdt.gxTpr_Contratoservicos_prazocorrecao ;
         }

         set {
            sdt.gxTpr_Contratoservicos_prazocorrecao = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicos_PrazoCorrecaoTipo" , Order = 48 )]
      [GxSeudo()]
      public String gxTpr_Contratoservicos_prazocorrecaotipo
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratoservicos_prazocorrecaotipo) ;
         }

         set {
            sdt.gxTpr_Contratoservicos_prazocorrecaotipo = (String)(value);
         }

      }

      [DataMember( Name = "ContratoServicos_PrazoInicio" , Order = 49 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contratoservicos_prazoinicio
      {
         get {
            return sdt.gxTpr_Contratoservicos_prazoinicio ;
         }

         set {
            sdt.gxTpr_Contratoservicos_prazoinicio = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicos_PrazoImediato" , Order = 50 )]
      [GxSeudo()]
      public bool gxTpr_Contratoservicos_prazoimediato
      {
         get {
            return sdt.gxTpr_Contratoservicos_prazoimediato ;
         }

         set {
            sdt.gxTpr_Contratoservicos_prazoimediato = value;
         }

      }

      [DataMember( Name = "ContratoServicos_Produtividade" , Order = 51 )]
      [GxSeudo()]
      public String gxTpr_Contratoservicos_produtividade
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contratoservicos_produtividade, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Contratoservicos_produtividade = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContratoServicos_EspelhaAceite" , Order = 52 )]
      [GxSeudo()]
      public bool gxTpr_Contratoservicos_espelhaaceite
      {
         get {
            return sdt.gxTpr_Contratoservicos_espelhaaceite ;
         }

         set {
            sdt.gxTpr_Contratoservicos_espelhaaceite = value;
         }

      }

      [DataMember( Name = "ContratoServicos_Momento" , Order = 53 )]
      [GxSeudo()]
      public String gxTpr_Contratoservicos_momento
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratoservicos_momento) ;
         }

         set {
            sdt.gxTpr_Contratoservicos_momento = (String)(value);
         }

      }

      [DataMember( Name = "ContratoServicos_StatusPagFnc" , Order = 54 )]
      [GxSeudo()]
      public String gxTpr_Contratoservicos_statuspagfnc
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratoservicos_statuspagfnc) ;
         }

         set {
            sdt.gxTpr_Contratoservicos_statuspagfnc = (String)(value);
         }

      }

      [DataMember( Name = "ContratoServicos_QntUntCns" , Order = 55 )]
      [GxSeudo()]
      public String gxTpr_Contratoservicos_qntuntcns
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contratoservicos_qntuntcns, 9, 4)) ;
         }

         set {
            sdt.gxTpr_Contratoservicos_qntuntcns = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContratoServicos_FatorCnvUndCnt" , Order = 56 )]
      [GxSeudo()]
      public String gxTpr_Contratoservicos_fatorcnvundcnt
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contratoservicos_fatorcnvundcnt, 9, 4)) ;
         }

         set {
            sdt.gxTpr_Contratoservicos_fatorcnvundcnt = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContratoServicos_NaoRequerAtr" , Order = 57 )]
      [GxSeudo()]
      public bool gxTpr_Contratoservicos_naorequeratr
      {
         get {
            return sdt.gxTpr_Contratoservicos_naorequeratr ;
         }

         set {
            sdt.gxTpr_Contratoservicos_naorequeratr = value;
         }

      }

      [DataMember( Name = "Contrato_IndiceDivergencia" , Order = 58 )]
      [GxSeudo()]
      public Nullable<decimal> gxTpr_Contrato_indicedivergencia
      {
         get {
            return sdt.gxTpr_Contrato_indicedivergencia ;
         }

         set {
            sdt.gxTpr_Contrato_indicedivergencia = (decimal)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicos_IndiceDivergencia" , Order = 59 )]
      [GxSeudo()]
      public Nullable<decimal> gxTpr_Contratoservicos_indicedivergencia
      {
         get {
            return sdt.gxTpr_Contratoservicos_indicedivergencia ;
         }

         set {
            sdt.gxTpr_Contratoservicos_indicedivergencia = (decimal)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicos_TmpEstAnl" , Order = 60 )]
      [GxSeudo()]
      public String gxTpr_Contratoservicos_tmpestanl
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Contratoservicos_tmpestanl), 8, 0)) ;
         }

         set {
            sdt.gxTpr_Contratoservicos_tmpestanl = (int)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "ContratoServicos_TmpEstExc" , Order = 61 )]
      [GxSeudo()]
      public String gxTpr_Contratoservicos_tmpestexc
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Contratoservicos_tmpestexc), 8, 0)) ;
         }

         set {
            sdt.gxTpr_Contratoservicos_tmpestexc = (int)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "ContratoServicos_TmpEstCrr" , Order = 62 )]
      [GxSeudo()]
      public String gxTpr_Contratoservicos_tmpestcrr
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Contratoservicos_tmpestcrr), 8, 0)) ;
         }

         set {
            sdt.gxTpr_Contratoservicos_tmpestcrr = (int)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "ContratoServicos_TipoHierarquia" , Order = 63 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contratoservicos_tipohierarquia
      {
         get {
            return sdt.gxTpr_Contratoservicos_tipohierarquia ;
         }

         set {
            sdt.gxTpr_Contratoservicos_tipohierarquia = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicos_PercTmp" , Order = 64 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contratoservicos_perctmp
      {
         get {
            return sdt.gxTpr_Contratoservicos_perctmp ;
         }

         set {
            sdt.gxTpr_Contratoservicos_perctmp = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicos_PercPgm" , Order = 65 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contratoservicos_percpgm
      {
         get {
            return sdt.gxTpr_Contratoservicos_percpgm ;
         }

         set {
            sdt.gxTpr_Contratoservicos_percpgm = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicos_PercCnc" , Order = 66 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contratoservicos_perccnc
      {
         get {
            return sdt.gxTpr_Contratoservicos_perccnc ;
         }

         set {
            sdt.gxTpr_Contratoservicos_perccnc = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicos_Ativo" , Order = 67 )]
      [GxSeudo()]
      public bool gxTpr_Contratoservicos_ativo
      {
         get {
            return sdt.gxTpr_Contratoservicos_ativo ;
         }

         set {
            sdt.gxTpr_Contratoservicos_ativo = value;
         }

      }

      [DataMember( Name = "ContratoServicos_CodigoFiscal" , Order = 68 )]
      [GxSeudo()]
      public String gxTpr_Contratoservicos_codigofiscal
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratoservicos_codigofiscal) ;
         }

         set {
            sdt.gxTpr_Contratoservicos_codigofiscal = (String)(value);
         }

      }

      [DataMember( Name = "ContratoServicos_LimiteProposta" , Order = 69 )]
      [GxSeudo()]
      public String gxTpr_Contratoservicos_limiteproposta
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contratoservicos_limiteproposta, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Contratoservicos_limiteproposta = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContratoServicos_CalculoRmn" , Order = 70 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contratoservicos_calculormn
      {
         get {
            return sdt.gxTpr_Contratoservicos_calculormn ;
         }

         set {
            sdt.gxTpr_Contratoservicos_calculormn = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicos_QtdRmn" , Order = 71 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contratoservicos_qtdrmn
      {
         get {
            return sdt.gxTpr_Contratoservicos_qtdrmn ;
         }

         set {
            sdt.gxTpr_Contratoservicos_qtdrmn = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicos_SolicitaGestorSistema" , Order = 72 )]
      [GxSeudo()]
      public bool gxTpr_Contratoservicos_solicitagestorsistema
      {
         get {
            return sdt.gxTpr_Contratoservicos_solicitagestorsistema ;
         }

         set {
            sdt.gxTpr_Contratoservicos_solicitagestorsistema = value;
         }

      }

      [DataMember( Name = "Rmn" , Order = 73 )]
      public GxGenericCollection<SdtContratoServicos_Rmn_RESTInterface> gxTpr_Rmn
      {
         get {
            return new GxGenericCollection<SdtContratoServicos_Rmn_RESTInterface>(sdt.gxTpr_Rmn) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Rmn);
         }

      }

      public SdtContratoServicos sdt
      {
         get {
            return (SdtContratoServicos)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtContratoServicos() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 193 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
