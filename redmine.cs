/*
               File: Redmine
        Description: Redmine
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/31/2020 1:13:51.3
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class redmine : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_3") == 0 )
         {
            A1384Redmine_ContratanteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1384Redmine_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1384Redmine_ContratanteCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_3( A1384Redmine_ContratanteCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbRedmine_Secure.Name = "REDMINE_SECURE";
         cmbRedmine_Secure.WebTags = "";
         cmbRedmine_Secure.addItem("0", "http://", 0);
         cmbRedmine_Secure.addItem("1", "https://", 0);
         if ( cmbRedmine_Secure.ItemCount > 0 )
         {
            if ( (0==A1904Redmine_Secure) )
            {
               A1904Redmine_Secure = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1904Redmine_Secure", StringUtil.Str( (decimal)(A1904Redmine_Secure), 1, 0));
            }
            A1904Redmine_Secure = (short)(NumberUtil.Val( cmbRedmine_Secure.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1904Redmine_Secure), 1, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1904Redmine_Secure", StringUtil.Str( (decimal)(A1904Redmine_Secure), 1, 0));
         }
         cmbRedmine_Versao.Name = "REDMINE_VERSAO";
         cmbRedmine_Versao.WebTags = "";
         cmbRedmine_Versao.addItem("144", "1.4.4", 0);
         cmbRedmine_Versao.addItem("223", "2.2.3", 0);
         if ( cmbRedmine_Versao.ItemCount > 0 )
         {
            A1391Redmine_Versao = cmbRedmine_Versao.getValidValue(A1391Redmine_Versao);
            n1391Redmine_Versao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1391Redmine_Versao", A1391Redmine_Versao);
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Redmine", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtRedmine_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public redmine( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public redmine( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbRedmine_Secure = new GXCombobox();
         cmbRedmine_Versao = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbRedmine_Secure.ItemCount > 0 )
         {
            A1904Redmine_Secure = (short)(NumberUtil.Val( cmbRedmine_Secure.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1904Redmine_Secure), 1, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1904Redmine_Secure", StringUtil.Str( (decimal)(A1904Redmine_Secure), 1, 0));
         }
         if ( cmbRedmine_Versao.ItemCount > 0 )
         {
            A1391Redmine_Versao = cmbRedmine_Versao.getValidValue(A1391Redmine_Versao);
            n1391Redmine_Versao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1391Redmine_Versao", A1391Redmine_Versao);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_3M165( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_3M165e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_3M165( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_3M165( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_3M165e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Redmine", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_Redmine.htm");
            wb_table3_28_3M165( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_3M165e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3M165e( true) ;
         }
         else
         {
            wb_table1_2_3M165e( false) ;
         }
      }

      protected void wb_table3_28_3M165( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_3M165( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_3M165e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Redmine.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Redmine.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Redmine.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_3M165e( true) ;
         }
         else
         {
            wb_table3_28_3M165e( false) ;
         }
      }

      protected void wb_table4_34_3M165( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockredmine_codigo_Internalname, "Redmine_Codigo", "", "", lblTextblockredmine_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Redmine.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtRedmine_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1380Redmine_Codigo), 6, 0, ",", "")), ((edtRedmine_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1380Redmine_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1380Redmine_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRedmine_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtRedmine_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Redmine.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockredmine_contratantecod_Internalname, "Cod", "", "", lblTextblockredmine_contratantecod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Redmine.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtRedmine_ContratanteCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1384Redmine_ContratanteCod), 6, 0, ",", "")), ((edtRedmine_ContratanteCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1384Redmine_ContratanteCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1384Redmine_ContratanteCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRedmine_ContratanteCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtRedmine_ContratanteCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Redmine.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockredmine_contratantefan_Internalname, "Fan", "", "", lblTextblockredmine_contratantefan_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Redmine.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtRedmine_ContratanteFan_Internalname, StringUtil.RTrim( A1385Redmine_ContratanteFan), StringUtil.RTrim( context.localUtil.Format( A1385Redmine_ContratanteFan, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRedmine_ContratanteFan_Jsonclick, 0, "BootstrapAttribute100", "", "", "", 1, edtRedmine_ContratanteFan_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_Redmine.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockredmine_user_Internalname, "Usu�rio", "", "", lblTextblockredmine_user_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Redmine.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtRedmine_User_Internalname, StringUtil.RTrim( A1905Redmine_User), StringUtil.RTrim( context.localUtil.Format( A1905Redmine_User, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRedmine_User_Jsonclick, 0, "Attribute", "", "", "", 1, edtRedmine_User_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Redmine.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockredmine_secure_Internalname, "Secure", "", "", lblTextblockredmine_secure_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Redmine.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbRedmine_Secure, cmbRedmine_Secure_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1904Redmine_Secure), 1, 0)), 1, cmbRedmine_Secure_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbRedmine_Secure.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "", true, "HLP_Redmine.htm");
            cmbRedmine_Secure.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1904Redmine_Secure), 1, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbRedmine_Secure_Internalname, "Values", (String)(cmbRedmine_Secure.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockredmine_host_Internalname, "Host", "", "", lblTextblockredmine_host_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Redmine.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtRedmine_Host_Internalname, StringUtil.RTrim( A1381Redmine_Host), StringUtil.RTrim( context.localUtil.Format( A1381Redmine_Host, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRedmine_Host_Jsonclick, 0, "Attribute", "", "", "", 1, edtRedmine_Host_Enabled, 0, "text", "", 250, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Redmine.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockredmine_url_Internalname, "Url", "", "", lblTextblockredmine_url_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Redmine.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtRedmine_Url_Internalname, StringUtil.RTrim( A1382Redmine_Url), StringUtil.RTrim( context.localUtil.Format( A1382Redmine_Url, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRedmine_Url_Jsonclick, 0, "Attribute", "", "", "", 1, edtRedmine_Url_Enabled, 0, "text", "", 250, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Redmine.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockredmine_key_Internalname, "Key", "", "", lblTextblockredmine_key_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Redmine.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtRedmine_Key_Internalname, StringUtil.RTrim( A1383Redmine_Key), StringUtil.RTrim( context.localUtil.Format( A1383Redmine_Key, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRedmine_Key_Jsonclick, 0, "Attribute", "", "", "", 1, edtRedmine_Key_Enabled, 0, "text", "", 250, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Redmine.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockredmine_versao_Internalname, "Vers�o", "", "", lblTextblockredmine_versao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Redmine.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbRedmine_Versao, cmbRedmine_Versao_Internalname, StringUtil.RTrim( A1391Redmine_Versao), 1, cmbRedmine_Versao_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbRedmine_Versao.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,79);\"", "", true, "HLP_Redmine.htm");
            cmbRedmine_Versao.CurrentValue = StringUtil.RTrim( A1391Redmine_Versao);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbRedmine_Versao_Internalname, "Values", (String)(cmbRedmine_Versao.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockredmine_camposistema_Internalname, "de Sistema", "", "", lblTextblockredmine_camposistema_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Redmine.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtRedmine_CampoSistema_Internalname, StringUtil.RTrim( A1906Redmine_CampoSistema), StringUtil.RTrim( context.localUtil.Format( A1906Redmine_CampoSistema, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,84);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRedmine_CampoSistema_Jsonclick, 0, "Attribute", "", "", "", 1, edtRedmine_CampoSistema_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Redmine.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockredmine_camposervico_Internalname, "de Servi�o", "", "", lblTextblockredmine_camposervico_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Redmine.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtRedmine_CampoServico_Internalname, StringUtil.RTrim( A1907Redmine_CampoServico), StringUtil.RTrim( context.localUtil.Format( A1907Redmine_CampoServico, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,89);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRedmine_CampoServico_Jsonclick, 0, "Attribute", "", "", "", 1, edtRedmine_CampoServico_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Redmine.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_3M165e( true) ;
         }
         else
         {
            wb_table4_34_3M165e( false) ;
         }
      }

      protected void wb_table2_5_3M165( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Redmine.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Redmine.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Redmine.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Redmine.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Redmine.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Redmine.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Redmine.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Redmine.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Redmine.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Redmine.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Redmine.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Redmine.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Redmine.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Redmine.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Redmine.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Redmine.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_3M165e( true) ;
         }
         else
         {
            wb_table2_5_3M165e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtRedmine_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtRedmine_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "REDMINE_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtRedmine_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1380Redmine_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1380Redmine_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1380Redmine_Codigo), 6, 0)));
               }
               else
               {
                  A1380Redmine_Codigo = (int)(context.localUtil.CToN( cgiGet( edtRedmine_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1380Redmine_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1380Redmine_Codigo), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtRedmine_ContratanteCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtRedmine_ContratanteCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "REDMINE_CONTRATANTECOD");
                  AnyError = 1;
                  GX_FocusControl = edtRedmine_ContratanteCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1384Redmine_ContratanteCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1384Redmine_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1384Redmine_ContratanteCod), 6, 0)));
               }
               else
               {
                  A1384Redmine_ContratanteCod = (int)(context.localUtil.CToN( cgiGet( edtRedmine_ContratanteCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1384Redmine_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1384Redmine_ContratanteCod), 6, 0)));
               }
               A1385Redmine_ContratanteFan = StringUtil.Upper( cgiGet( edtRedmine_ContratanteFan_Internalname));
               n1385Redmine_ContratanteFan = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1385Redmine_ContratanteFan", A1385Redmine_ContratanteFan);
               A1905Redmine_User = cgiGet( edtRedmine_User_Internalname);
               n1905Redmine_User = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1905Redmine_User", A1905Redmine_User);
               n1905Redmine_User = (String.IsNullOrEmpty(StringUtil.RTrim( A1905Redmine_User)) ? true : false);
               cmbRedmine_Secure.CurrentValue = cgiGet( cmbRedmine_Secure_Internalname);
               A1904Redmine_Secure = (short)(NumberUtil.Val( cgiGet( cmbRedmine_Secure_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1904Redmine_Secure", StringUtil.Str( (decimal)(A1904Redmine_Secure), 1, 0));
               A1381Redmine_Host = cgiGet( edtRedmine_Host_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1381Redmine_Host", A1381Redmine_Host);
               A1382Redmine_Url = cgiGet( edtRedmine_Url_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1382Redmine_Url", A1382Redmine_Url);
               A1383Redmine_Key = cgiGet( edtRedmine_Key_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1383Redmine_Key", A1383Redmine_Key);
               cmbRedmine_Versao.CurrentValue = cgiGet( cmbRedmine_Versao_Internalname);
               A1391Redmine_Versao = cgiGet( cmbRedmine_Versao_Internalname);
               n1391Redmine_Versao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1391Redmine_Versao", A1391Redmine_Versao);
               n1391Redmine_Versao = (String.IsNullOrEmpty(StringUtil.RTrim( A1391Redmine_Versao)) ? true : false);
               A1906Redmine_CampoSistema = cgiGet( edtRedmine_CampoSistema_Internalname);
               n1906Redmine_CampoSistema = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1906Redmine_CampoSistema", A1906Redmine_CampoSistema);
               n1906Redmine_CampoSistema = (String.IsNullOrEmpty(StringUtil.RTrim( A1906Redmine_CampoSistema)) ? true : false);
               A1907Redmine_CampoServico = cgiGet( edtRedmine_CampoServico_Internalname);
               n1907Redmine_CampoServico = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1907Redmine_CampoServico", A1907Redmine_CampoServico);
               n1907Redmine_CampoServico = (String.IsNullOrEmpty(StringUtil.RTrim( A1907Redmine_CampoServico)) ? true : false);
               /* Read saved values. */
               Z1380Redmine_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1380Redmine_Codigo"), ",", "."));
               Z1905Redmine_User = cgiGet( "Z1905Redmine_User");
               n1905Redmine_User = (String.IsNullOrEmpty(StringUtil.RTrim( A1905Redmine_User)) ? true : false);
               Z1904Redmine_Secure = (short)(context.localUtil.CToN( cgiGet( "Z1904Redmine_Secure"), ",", "."));
               Z1381Redmine_Host = cgiGet( "Z1381Redmine_Host");
               Z1382Redmine_Url = cgiGet( "Z1382Redmine_Url");
               Z1383Redmine_Key = cgiGet( "Z1383Redmine_Key");
               Z1391Redmine_Versao = cgiGet( "Z1391Redmine_Versao");
               n1391Redmine_Versao = (String.IsNullOrEmpty(StringUtil.RTrim( A1391Redmine_Versao)) ? true : false);
               Z1906Redmine_CampoSistema = cgiGet( "Z1906Redmine_CampoSistema");
               n1906Redmine_CampoSistema = (String.IsNullOrEmpty(StringUtil.RTrim( A1906Redmine_CampoSistema)) ? true : false);
               Z1907Redmine_CampoServico = cgiGet( "Z1907Redmine_CampoServico");
               n1907Redmine_CampoServico = (String.IsNullOrEmpty(StringUtil.RTrim( A1907Redmine_CampoServico)) ? true : false);
               Z1384Redmine_ContratanteCod = (int)(context.localUtil.CToN( cgiGet( "Z1384Redmine_ContratanteCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A1380Redmine_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1380Redmine_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1380Redmine_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll3M165( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes3M165( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption3M0( )
      {
      }

      protected void ZM3M165( short GX_JID )
      {
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1905Redmine_User = T003M3_A1905Redmine_User[0];
               Z1904Redmine_Secure = T003M3_A1904Redmine_Secure[0];
               Z1381Redmine_Host = T003M3_A1381Redmine_Host[0];
               Z1382Redmine_Url = T003M3_A1382Redmine_Url[0];
               Z1383Redmine_Key = T003M3_A1383Redmine_Key[0];
               Z1391Redmine_Versao = T003M3_A1391Redmine_Versao[0];
               Z1906Redmine_CampoSistema = T003M3_A1906Redmine_CampoSistema[0];
               Z1907Redmine_CampoServico = T003M3_A1907Redmine_CampoServico[0];
               Z1384Redmine_ContratanteCod = T003M3_A1384Redmine_ContratanteCod[0];
            }
            else
            {
               Z1905Redmine_User = A1905Redmine_User;
               Z1904Redmine_Secure = A1904Redmine_Secure;
               Z1381Redmine_Host = A1381Redmine_Host;
               Z1382Redmine_Url = A1382Redmine_Url;
               Z1383Redmine_Key = A1383Redmine_Key;
               Z1391Redmine_Versao = A1391Redmine_Versao;
               Z1906Redmine_CampoSistema = A1906Redmine_CampoSistema;
               Z1907Redmine_CampoServico = A1907Redmine_CampoServico;
               Z1384Redmine_ContratanteCod = A1384Redmine_ContratanteCod;
            }
         }
         if ( GX_JID == -2 )
         {
            Z1380Redmine_Codigo = A1380Redmine_Codigo;
            Z1905Redmine_User = A1905Redmine_User;
            Z1904Redmine_Secure = A1904Redmine_Secure;
            Z1381Redmine_Host = A1381Redmine_Host;
            Z1382Redmine_Url = A1382Redmine_Url;
            Z1383Redmine_Key = A1383Redmine_Key;
            Z1391Redmine_Versao = A1391Redmine_Versao;
            Z1906Redmine_CampoSistema = A1906Redmine_CampoSistema;
            Z1907Redmine_CampoServico = A1907Redmine_CampoServico;
            Z1384Redmine_ContratanteCod = A1384Redmine_ContratanteCod;
            Z1385Redmine_ContratanteFan = A1385Redmine_ContratanteFan;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A1904Redmine_Secure) && ( Gx_BScreen == 0 ) )
         {
            A1904Redmine_Secure = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1904Redmine_Secure", StringUtil.Str( (decimal)(A1904Redmine_Secure), 1, 0));
         }
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load3M165( )
      {
         /* Using cursor T003M5 */
         pr_default.execute(3, new Object[] {A1380Redmine_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound165 = 1;
            A1385Redmine_ContratanteFan = T003M5_A1385Redmine_ContratanteFan[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1385Redmine_ContratanteFan", A1385Redmine_ContratanteFan);
            n1385Redmine_ContratanteFan = T003M5_n1385Redmine_ContratanteFan[0];
            A1905Redmine_User = T003M5_A1905Redmine_User[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1905Redmine_User", A1905Redmine_User);
            n1905Redmine_User = T003M5_n1905Redmine_User[0];
            A1904Redmine_Secure = T003M5_A1904Redmine_Secure[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1904Redmine_Secure", StringUtil.Str( (decimal)(A1904Redmine_Secure), 1, 0));
            A1381Redmine_Host = T003M5_A1381Redmine_Host[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1381Redmine_Host", A1381Redmine_Host);
            A1382Redmine_Url = T003M5_A1382Redmine_Url[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1382Redmine_Url", A1382Redmine_Url);
            A1383Redmine_Key = T003M5_A1383Redmine_Key[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1383Redmine_Key", A1383Redmine_Key);
            A1391Redmine_Versao = T003M5_A1391Redmine_Versao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1391Redmine_Versao", A1391Redmine_Versao);
            n1391Redmine_Versao = T003M5_n1391Redmine_Versao[0];
            A1906Redmine_CampoSistema = T003M5_A1906Redmine_CampoSistema[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1906Redmine_CampoSistema", A1906Redmine_CampoSistema);
            n1906Redmine_CampoSistema = T003M5_n1906Redmine_CampoSistema[0];
            A1907Redmine_CampoServico = T003M5_A1907Redmine_CampoServico[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1907Redmine_CampoServico", A1907Redmine_CampoServico);
            n1907Redmine_CampoServico = T003M5_n1907Redmine_CampoServico[0];
            A1384Redmine_ContratanteCod = T003M5_A1384Redmine_ContratanteCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1384Redmine_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1384Redmine_ContratanteCod), 6, 0)));
            ZM3M165( -2) ;
         }
         pr_default.close(3);
         OnLoadActions3M165( ) ;
      }

      protected void OnLoadActions3M165( )
      {
      }

      protected void CheckExtendedTable3M165( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T003M4 */
         pr_default.execute(2, new Object[] {A1384Redmine_ContratanteCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Redmine do Contratante'.", "ForeignKeyNotFound", 1, "REDMINE_CONTRATANTECOD");
            AnyError = 1;
            GX_FocusControl = edtRedmine_ContratanteCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1385Redmine_ContratanteFan = T003M4_A1385Redmine_ContratanteFan[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1385Redmine_ContratanteFan", A1385Redmine_ContratanteFan);
         n1385Redmine_ContratanteFan = T003M4_n1385Redmine_ContratanteFan[0];
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors3M165( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_3( int A1384Redmine_ContratanteCod )
      {
         /* Using cursor T003M6 */
         pr_default.execute(4, new Object[] {A1384Redmine_ContratanteCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Redmine do Contratante'.", "ForeignKeyNotFound", 1, "REDMINE_CONTRATANTECOD");
            AnyError = 1;
            GX_FocusControl = edtRedmine_ContratanteCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1385Redmine_ContratanteFan = T003M6_A1385Redmine_ContratanteFan[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1385Redmine_ContratanteFan", A1385Redmine_ContratanteFan);
         n1385Redmine_ContratanteFan = T003M6_n1385Redmine_ContratanteFan[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1385Redmine_ContratanteFan))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey3M165( )
      {
         /* Using cursor T003M7 */
         pr_default.execute(5, new Object[] {A1380Redmine_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound165 = 1;
         }
         else
         {
            RcdFound165 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T003M3 */
         pr_default.execute(1, new Object[] {A1380Redmine_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3M165( 2) ;
            RcdFound165 = 1;
            A1380Redmine_Codigo = T003M3_A1380Redmine_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1380Redmine_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1380Redmine_Codigo), 6, 0)));
            A1905Redmine_User = T003M3_A1905Redmine_User[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1905Redmine_User", A1905Redmine_User);
            n1905Redmine_User = T003M3_n1905Redmine_User[0];
            A1904Redmine_Secure = T003M3_A1904Redmine_Secure[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1904Redmine_Secure", StringUtil.Str( (decimal)(A1904Redmine_Secure), 1, 0));
            A1381Redmine_Host = T003M3_A1381Redmine_Host[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1381Redmine_Host", A1381Redmine_Host);
            A1382Redmine_Url = T003M3_A1382Redmine_Url[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1382Redmine_Url", A1382Redmine_Url);
            A1383Redmine_Key = T003M3_A1383Redmine_Key[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1383Redmine_Key", A1383Redmine_Key);
            A1391Redmine_Versao = T003M3_A1391Redmine_Versao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1391Redmine_Versao", A1391Redmine_Versao);
            n1391Redmine_Versao = T003M3_n1391Redmine_Versao[0];
            A1906Redmine_CampoSistema = T003M3_A1906Redmine_CampoSistema[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1906Redmine_CampoSistema", A1906Redmine_CampoSistema);
            n1906Redmine_CampoSistema = T003M3_n1906Redmine_CampoSistema[0];
            A1907Redmine_CampoServico = T003M3_A1907Redmine_CampoServico[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1907Redmine_CampoServico", A1907Redmine_CampoServico);
            n1907Redmine_CampoServico = T003M3_n1907Redmine_CampoServico[0];
            A1384Redmine_ContratanteCod = T003M3_A1384Redmine_ContratanteCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1384Redmine_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1384Redmine_ContratanteCod), 6, 0)));
            Z1380Redmine_Codigo = A1380Redmine_Codigo;
            sMode165 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load3M165( ) ;
            if ( AnyError == 1 )
            {
               RcdFound165 = 0;
               InitializeNonKey3M165( ) ;
            }
            Gx_mode = sMode165;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound165 = 0;
            InitializeNonKey3M165( ) ;
            sMode165 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode165;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey3M165( ) ;
         if ( RcdFound165 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound165 = 0;
         /* Using cursor T003M8 */
         pr_default.execute(6, new Object[] {A1380Redmine_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T003M8_A1380Redmine_Codigo[0] < A1380Redmine_Codigo ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T003M8_A1380Redmine_Codigo[0] > A1380Redmine_Codigo ) ) )
            {
               A1380Redmine_Codigo = T003M8_A1380Redmine_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1380Redmine_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1380Redmine_Codigo), 6, 0)));
               RcdFound165 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound165 = 0;
         /* Using cursor T003M9 */
         pr_default.execute(7, new Object[] {A1380Redmine_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T003M9_A1380Redmine_Codigo[0] > A1380Redmine_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T003M9_A1380Redmine_Codigo[0] < A1380Redmine_Codigo ) ) )
            {
               A1380Redmine_Codigo = T003M9_A1380Redmine_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1380Redmine_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1380Redmine_Codigo), 6, 0)));
               RcdFound165 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey3M165( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtRedmine_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert3M165( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound165 == 1 )
            {
               if ( A1380Redmine_Codigo != Z1380Redmine_Codigo )
               {
                  A1380Redmine_Codigo = Z1380Redmine_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1380Redmine_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1380Redmine_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "REDMINE_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtRedmine_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtRedmine_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update3M165( ) ;
                  GX_FocusControl = edtRedmine_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1380Redmine_Codigo != Z1380Redmine_Codigo )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtRedmine_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert3M165( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "REDMINE_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtRedmine_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtRedmine_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert3M165( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A1380Redmine_Codigo != Z1380Redmine_Codigo )
         {
            A1380Redmine_Codigo = Z1380Redmine_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1380Redmine_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1380Redmine_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "REDMINE_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtRedmine_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtRedmine_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound165 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "REDMINE_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtRedmine_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtRedmine_ContratanteCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart3M165( ) ;
         if ( RcdFound165 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtRedmine_ContratanteCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd3M165( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound165 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtRedmine_ContratanteCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound165 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtRedmine_ContratanteCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart3M165( ) ;
         if ( RcdFound165 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound165 != 0 )
            {
               ScanNext3M165( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtRedmine_ContratanteCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd3M165( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency3M165( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T003M2 */
            pr_default.execute(0, new Object[] {A1380Redmine_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Redmine"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1905Redmine_User, T003M2_A1905Redmine_User[0]) != 0 ) || ( Z1904Redmine_Secure != T003M2_A1904Redmine_Secure[0] ) || ( StringUtil.StrCmp(Z1381Redmine_Host, T003M2_A1381Redmine_Host[0]) != 0 ) || ( StringUtil.StrCmp(Z1382Redmine_Url, T003M2_A1382Redmine_Url[0]) != 0 ) || ( StringUtil.StrCmp(Z1383Redmine_Key, T003M2_A1383Redmine_Key[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z1391Redmine_Versao, T003M2_A1391Redmine_Versao[0]) != 0 ) || ( StringUtil.StrCmp(Z1906Redmine_CampoSistema, T003M2_A1906Redmine_CampoSistema[0]) != 0 ) || ( StringUtil.StrCmp(Z1907Redmine_CampoServico, T003M2_A1907Redmine_CampoServico[0]) != 0 ) || ( Z1384Redmine_ContratanteCod != T003M2_A1384Redmine_ContratanteCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z1905Redmine_User, T003M2_A1905Redmine_User[0]) != 0 )
               {
                  GXUtil.WriteLog("redmine:[seudo value changed for attri]"+"Redmine_User");
                  GXUtil.WriteLogRaw("Old: ",Z1905Redmine_User);
                  GXUtil.WriteLogRaw("Current: ",T003M2_A1905Redmine_User[0]);
               }
               if ( Z1904Redmine_Secure != T003M2_A1904Redmine_Secure[0] )
               {
                  GXUtil.WriteLog("redmine:[seudo value changed for attri]"+"Redmine_Secure");
                  GXUtil.WriteLogRaw("Old: ",Z1904Redmine_Secure);
                  GXUtil.WriteLogRaw("Current: ",T003M2_A1904Redmine_Secure[0]);
               }
               if ( StringUtil.StrCmp(Z1381Redmine_Host, T003M2_A1381Redmine_Host[0]) != 0 )
               {
                  GXUtil.WriteLog("redmine:[seudo value changed for attri]"+"Redmine_Host");
                  GXUtil.WriteLogRaw("Old: ",Z1381Redmine_Host);
                  GXUtil.WriteLogRaw("Current: ",T003M2_A1381Redmine_Host[0]);
               }
               if ( StringUtil.StrCmp(Z1382Redmine_Url, T003M2_A1382Redmine_Url[0]) != 0 )
               {
                  GXUtil.WriteLog("redmine:[seudo value changed for attri]"+"Redmine_Url");
                  GXUtil.WriteLogRaw("Old: ",Z1382Redmine_Url);
                  GXUtil.WriteLogRaw("Current: ",T003M2_A1382Redmine_Url[0]);
               }
               if ( StringUtil.StrCmp(Z1383Redmine_Key, T003M2_A1383Redmine_Key[0]) != 0 )
               {
                  GXUtil.WriteLog("redmine:[seudo value changed for attri]"+"Redmine_Key");
                  GXUtil.WriteLogRaw("Old: ",Z1383Redmine_Key);
                  GXUtil.WriteLogRaw("Current: ",T003M2_A1383Redmine_Key[0]);
               }
               if ( StringUtil.StrCmp(Z1391Redmine_Versao, T003M2_A1391Redmine_Versao[0]) != 0 )
               {
                  GXUtil.WriteLog("redmine:[seudo value changed for attri]"+"Redmine_Versao");
                  GXUtil.WriteLogRaw("Old: ",Z1391Redmine_Versao);
                  GXUtil.WriteLogRaw("Current: ",T003M2_A1391Redmine_Versao[0]);
               }
               if ( StringUtil.StrCmp(Z1906Redmine_CampoSistema, T003M2_A1906Redmine_CampoSistema[0]) != 0 )
               {
                  GXUtil.WriteLog("redmine:[seudo value changed for attri]"+"Redmine_CampoSistema");
                  GXUtil.WriteLogRaw("Old: ",Z1906Redmine_CampoSistema);
                  GXUtil.WriteLogRaw("Current: ",T003M2_A1906Redmine_CampoSistema[0]);
               }
               if ( StringUtil.StrCmp(Z1907Redmine_CampoServico, T003M2_A1907Redmine_CampoServico[0]) != 0 )
               {
                  GXUtil.WriteLog("redmine:[seudo value changed for attri]"+"Redmine_CampoServico");
                  GXUtil.WriteLogRaw("Old: ",Z1907Redmine_CampoServico);
                  GXUtil.WriteLogRaw("Current: ",T003M2_A1907Redmine_CampoServico[0]);
               }
               if ( Z1384Redmine_ContratanteCod != T003M2_A1384Redmine_ContratanteCod[0] )
               {
                  GXUtil.WriteLog("redmine:[seudo value changed for attri]"+"Redmine_ContratanteCod");
                  GXUtil.WriteLogRaw("Old: ",Z1384Redmine_ContratanteCod);
                  GXUtil.WriteLogRaw("Current: ",T003M2_A1384Redmine_ContratanteCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Redmine"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3M165( )
      {
         BeforeValidate3M165( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3M165( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3M165( 0) ;
            CheckOptimisticConcurrency3M165( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3M165( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3M165( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003M10 */
                     pr_default.execute(8, new Object[] {n1905Redmine_User, A1905Redmine_User, A1904Redmine_Secure, A1381Redmine_Host, A1382Redmine_Url, A1383Redmine_Key, n1391Redmine_Versao, A1391Redmine_Versao, n1906Redmine_CampoSistema, A1906Redmine_CampoSistema, n1907Redmine_CampoServico, A1907Redmine_CampoServico, A1384Redmine_ContratanteCod});
                     A1380Redmine_Codigo = T003M10_A1380Redmine_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1380Redmine_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1380Redmine_Codigo), 6, 0)));
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("Redmine") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption3M0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3M165( ) ;
            }
            EndLevel3M165( ) ;
         }
         CloseExtendedTableCursors3M165( ) ;
      }

      protected void Update3M165( )
      {
         BeforeValidate3M165( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3M165( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3M165( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3M165( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3M165( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003M11 */
                     pr_default.execute(9, new Object[] {n1905Redmine_User, A1905Redmine_User, A1904Redmine_Secure, A1381Redmine_Host, A1382Redmine_Url, A1383Redmine_Key, n1391Redmine_Versao, A1391Redmine_Versao, n1906Redmine_CampoSistema, A1906Redmine_CampoSistema, n1907Redmine_CampoServico, A1907Redmine_CampoServico, A1384Redmine_ContratanteCod, A1380Redmine_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("Redmine") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Redmine"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3M165( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption3M0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3M165( ) ;
         }
         CloseExtendedTableCursors3M165( ) ;
      }

      protected void DeferredUpdate3M165( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate3M165( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3M165( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3M165( ) ;
            AfterConfirm3M165( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3M165( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003M12 */
                  pr_default.execute(10, new Object[] {A1380Redmine_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("Redmine") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound165 == 0 )
                        {
                           InitAll3M165( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption3M0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode165 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel3M165( ) ;
         Gx_mode = sMode165;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls3M165( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T003M13 */
            pr_default.execute(11, new Object[] {A1384Redmine_ContratanteCod});
            A1385Redmine_ContratanteFan = T003M13_A1385Redmine_ContratanteFan[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1385Redmine_ContratanteFan", A1385Redmine_ContratanteFan);
            n1385Redmine_ContratanteFan = T003M13_n1385Redmine_ContratanteFan[0];
            pr_default.close(11);
         }
      }

      protected void EndLevel3M165( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3M165( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(11);
            context.CommitDataStores( "Redmine");
            if ( AnyError == 0 )
            {
               ConfirmValues3M0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(11);
            context.RollbackDataStores( "Redmine");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart3M165( )
      {
         /* Using cursor T003M14 */
         pr_default.execute(12);
         RcdFound165 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound165 = 1;
            A1380Redmine_Codigo = T003M14_A1380Redmine_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1380Redmine_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1380Redmine_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext3M165( )
      {
         /* Scan next routine */
         pr_default.readNext(12);
         RcdFound165 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound165 = 1;
            A1380Redmine_Codigo = T003M14_A1380Redmine_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1380Redmine_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1380Redmine_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd3M165( )
      {
         pr_default.close(12);
      }

      protected void AfterConfirm3M165( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3M165( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3M165( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3M165( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3M165( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3M165( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3M165( )
      {
         edtRedmine_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRedmine_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRedmine_Codigo_Enabled), 5, 0)));
         edtRedmine_ContratanteCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRedmine_ContratanteCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRedmine_ContratanteCod_Enabled), 5, 0)));
         edtRedmine_ContratanteFan_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRedmine_ContratanteFan_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRedmine_ContratanteFan_Enabled), 5, 0)));
         edtRedmine_User_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRedmine_User_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRedmine_User_Enabled), 5, 0)));
         cmbRedmine_Secure.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbRedmine_Secure_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbRedmine_Secure.Enabled), 5, 0)));
         edtRedmine_Host_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRedmine_Host_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRedmine_Host_Enabled), 5, 0)));
         edtRedmine_Url_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRedmine_Url_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRedmine_Url_Enabled), 5, 0)));
         edtRedmine_Key_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRedmine_Key_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRedmine_Key_Enabled), 5, 0)));
         cmbRedmine_Versao.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbRedmine_Versao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbRedmine_Versao.Enabled), 5, 0)));
         edtRedmine_CampoSistema_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRedmine_CampoSistema_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRedmine_CampoSistema_Enabled), 5, 0)));
         edtRedmine_CampoServico_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRedmine_CampoServico_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRedmine_CampoServico_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues3M0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205311135191");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("redmine.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1380Redmine_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1380Redmine_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1905Redmine_User", StringUtil.RTrim( Z1905Redmine_User));
         GxWebStd.gx_hidden_field( context, "Z1904Redmine_Secure", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1904Redmine_Secure), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1381Redmine_Host", StringUtil.RTrim( Z1381Redmine_Host));
         GxWebStd.gx_hidden_field( context, "Z1382Redmine_Url", StringUtil.RTrim( Z1382Redmine_Url));
         GxWebStd.gx_hidden_field( context, "Z1383Redmine_Key", StringUtil.RTrim( Z1383Redmine_Key));
         GxWebStd.gx_hidden_field( context, "Z1391Redmine_Versao", StringUtil.RTrim( Z1391Redmine_Versao));
         GxWebStd.gx_hidden_field( context, "Z1906Redmine_CampoSistema", StringUtil.RTrim( Z1906Redmine_CampoSistema));
         GxWebStd.gx_hidden_field( context, "Z1907Redmine_CampoServico", StringUtil.RTrim( Z1907Redmine_CampoServico));
         GxWebStd.gx_hidden_field( context, "Z1384Redmine_ContratanteCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1384Redmine_ContratanteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("redmine.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "Redmine" ;
      }

      public override String GetPgmdesc( )
      {
         return "Redmine" ;
      }

      protected void InitializeNonKey3M165( )
      {
         A1384Redmine_ContratanteCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1384Redmine_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1384Redmine_ContratanteCod), 6, 0)));
         A1385Redmine_ContratanteFan = "";
         n1385Redmine_ContratanteFan = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1385Redmine_ContratanteFan", A1385Redmine_ContratanteFan);
         A1905Redmine_User = "";
         n1905Redmine_User = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1905Redmine_User", A1905Redmine_User);
         n1905Redmine_User = (String.IsNullOrEmpty(StringUtil.RTrim( A1905Redmine_User)) ? true : false);
         A1381Redmine_Host = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1381Redmine_Host", A1381Redmine_Host);
         A1382Redmine_Url = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1382Redmine_Url", A1382Redmine_Url);
         A1383Redmine_Key = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1383Redmine_Key", A1383Redmine_Key);
         A1391Redmine_Versao = "";
         n1391Redmine_Versao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1391Redmine_Versao", A1391Redmine_Versao);
         n1391Redmine_Versao = (String.IsNullOrEmpty(StringUtil.RTrim( A1391Redmine_Versao)) ? true : false);
         A1906Redmine_CampoSistema = "";
         n1906Redmine_CampoSistema = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1906Redmine_CampoSistema", A1906Redmine_CampoSistema);
         n1906Redmine_CampoSistema = (String.IsNullOrEmpty(StringUtil.RTrim( A1906Redmine_CampoSistema)) ? true : false);
         A1907Redmine_CampoServico = "";
         n1907Redmine_CampoServico = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1907Redmine_CampoServico", A1907Redmine_CampoServico);
         n1907Redmine_CampoServico = (String.IsNullOrEmpty(StringUtil.RTrim( A1907Redmine_CampoServico)) ? true : false);
         A1904Redmine_Secure = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1904Redmine_Secure", StringUtil.Str( (decimal)(A1904Redmine_Secure), 1, 0));
         Z1905Redmine_User = "";
         Z1904Redmine_Secure = 0;
         Z1381Redmine_Host = "";
         Z1382Redmine_Url = "";
         Z1383Redmine_Key = "";
         Z1391Redmine_Versao = "";
         Z1906Redmine_CampoSistema = "";
         Z1907Redmine_CampoServico = "";
         Z1384Redmine_ContratanteCod = 0;
      }

      protected void InitAll3M165( )
      {
         A1380Redmine_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1380Redmine_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1380Redmine_Codigo), 6, 0)));
         InitializeNonKey3M165( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A1904Redmine_Secure = i1904Redmine_Secure;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1904Redmine_Secure", StringUtil.Str( (decimal)(A1904Redmine_Secure), 1, 0));
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205311135195");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("redmine.js", "?20205311135195");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockredmine_codigo_Internalname = "TEXTBLOCKREDMINE_CODIGO";
         edtRedmine_Codigo_Internalname = "REDMINE_CODIGO";
         lblTextblockredmine_contratantecod_Internalname = "TEXTBLOCKREDMINE_CONTRATANTECOD";
         edtRedmine_ContratanteCod_Internalname = "REDMINE_CONTRATANTECOD";
         lblTextblockredmine_contratantefan_Internalname = "TEXTBLOCKREDMINE_CONTRATANTEFAN";
         edtRedmine_ContratanteFan_Internalname = "REDMINE_CONTRATANTEFAN";
         lblTextblockredmine_user_Internalname = "TEXTBLOCKREDMINE_USER";
         edtRedmine_User_Internalname = "REDMINE_USER";
         lblTextblockredmine_secure_Internalname = "TEXTBLOCKREDMINE_SECURE";
         cmbRedmine_Secure_Internalname = "REDMINE_SECURE";
         lblTextblockredmine_host_Internalname = "TEXTBLOCKREDMINE_HOST";
         edtRedmine_Host_Internalname = "REDMINE_HOST";
         lblTextblockredmine_url_Internalname = "TEXTBLOCKREDMINE_URL";
         edtRedmine_Url_Internalname = "REDMINE_URL";
         lblTextblockredmine_key_Internalname = "TEXTBLOCKREDMINE_KEY";
         edtRedmine_Key_Internalname = "REDMINE_KEY";
         lblTextblockredmine_versao_Internalname = "TEXTBLOCKREDMINE_VERSAO";
         cmbRedmine_Versao_Internalname = "REDMINE_VERSAO";
         lblTextblockredmine_camposistema_Internalname = "TEXTBLOCKREDMINE_CAMPOSISTEMA";
         edtRedmine_CampoSistema_Internalname = "REDMINE_CAMPOSISTEMA";
         lblTextblockredmine_camposervico_Internalname = "TEXTBLOCKREDMINE_CAMPOSERVICO";
         edtRedmine_CampoServico_Internalname = "REDMINE_CAMPOSERVICO";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Redmine";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtRedmine_CampoServico_Jsonclick = "";
         edtRedmine_CampoServico_Enabled = 1;
         edtRedmine_CampoSistema_Jsonclick = "";
         edtRedmine_CampoSistema_Enabled = 1;
         cmbRedmine_Versao_Jsonclick = "";
         cmbRedmine_Versao.Enabled = 1;
         edtRedmine_Key_Jsonclick = "";
         edtRedmine_Key_Enabled = 1;
         edtRedmine_Url_Jsonclick = "";
         edtRedmine_Url_Enabled = 1;
         edtRedmine_Host_Jsonclick = "";
         edtRedmine_Host_Enabled = 1;
         cmbRedmine_Secure_Jsonclick = "";
         cmbRedmine_Secure.Enabled = 1;
         edtRedmine_User_Jsonclick = "";
         edtRedmine_User_Enabled = 1;
         edtRedmine_ContratanteFan_Jsonclick = "";
         edtRedmine_ContratanteFan_Enabled = 0;
         edtRedmine_ContratanteCod_Jsonclick = "";
         edtRedmine_ContratanteCod_Enabled = 1;
         edtRedmine_Codigo_Jsonclick = "";
         edtRedmine_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtRedmine_ContratanteCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Redmine_codigo( int GX_Parm1 ,
                                        String GX_Parm2 ,
                                        GXCombobox cmbGX_Parm3 ,
                                        String GX_Parm4 ,
                                        String GX_Parm5 ,
                                        String GX_Parm6 ,
                                        GXCombobox cmbGX_Parm7 ,
                                        String GX_Parm8 ,
                                        String GX_Parm9 ,
                                        int GX_Parm10 )
      {
         A1380Redmine_Codigo = GX_Parm1;
         A1905Redmine_User = GX_Parm2;
         n1905Redmine_User = false;
         cmbRedmine_Secure = cmbGX_Parm3;
         A1904Redmine_Secure = (short)(NumberUtil.Val( cmbRedmine_Secure.CurrentValue, "."));
         cmbRedmine_Secure.CurrentValue = StringUtil.Str( (decimal)(A1904Redmine_Secure), 1, 0);
         A1381Redmine_Host = GX_Parm4;
         A1382Redmine_Url = GX_Parm5;
         A1383Redmine_Key = GX_Parm6;
         cmbRedmine_Versao = cmbGX_Parm7;
         A1391Redmine_Versao = cmbRedmine_Versao.CurrentValue;
         n1391Redmine_Versao = false;
         cmbRedmine_Versao.CurrentValue = A1391Redmine_Versao;
         A1906Redmine_CampoSistema = GX_Parm8;
         n1906Redmine_CampoSistema = false;
         A1907Redmine_CampoServico = GX_Parm9;
         n1907Redmine_CampoServico = false;
         A1384Redmine_ContratanteCod = GX_Parm10;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1385Redmine_ContratanteFan = "";
            n1385Redmine_ContratanteFan = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1384Redmine_ContratanteCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A1905Redmine_User));
         cmbRedmine_Secure.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1904Redmine_Secure), 1, 0));
         isValidOutput.Add(cmbRedmine_Secure);
         isValidOutput.Add(StringUtil.RTrim( A1381Redmine_Host));
         isValidOutput.Add(StringUtil.RTrim( A1382Redmine_Url));
         isValidOutput.Add(StringUtil.RTrim( A1383Redmine_Key));
         cmbRedmine_Versao.CurrentValue = A1391Redmine_Versao;
         isValidOutput.Add(cmbRedmine_Versao);
         isValidOutput.Add(StringUtil.RTrim( A1906Redmine_CampoSistema));
         isValidOutput.Add(StringUtil.RTrim( A1907Redmine_CampoServico));
         isValidOutput.Add(StringUtil.RTrim( A1385Redmine_ContratanteFan));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1380Redmine_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1384Redmine_ContratanteCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.RTrim( Z1905Redmine_User));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1904Redmine_Secure), 1, 0, ",", "")));
         isValidOutput.Add(StringUtil.RTrim( Z1381Redmine_Host));
         isValidOutput.Add(StringUtil.RTrim( Z1382Redmine_Url));
         isValidOutput.Add(StringUtil.RTrim( Z1383Redmine_Key));
         isValidOutput.Add(StringUtil.RTrim( Z1391Redmine_Versao));
         isValidOutput.Add(StringUtil.RTrim( Z1906Redmine_CampoSistema));
         isValidOutput.Add(StringUtil.RTrim( Z1907Redmine_CampoServico));
         isValidOutput.Add(StringUtil.RTrim( Z1385Redmine_ContratanteFan));
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Redmine_contratantecod( int GX_Parm1 ,
                                                String GX_Parm2 )
      {
         A1384Redmine_ContratanteCod = GX_Parm1;
         A1385Redmine_ContratanteFan = GX_Parm2;
         n1385Redmine_ContratanteFan = false;
         /* Using cursor T003M13 */
         pr_default.execute(11, new Object[] {A1384Redmine_ContratanteCod});
         if ( (pr_default.getStatus(11) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Redmine do Contratante'.", "ForeignKeyNotFound", 1, "REDMINE_CONTRATANTECOD");
            AnyError = 1;
            GX_FocusControl = edtRedmine_ContratanteCod_Internalname;
         }
         A1385Redmine_ContratanteFan = T003M13_A1385Redmine_ContratanteFan[0];
         n1385Redmine_ContratanteFan = T003M13_n1385Redmine_ContratanteFan[0];
         pr_default.close(11);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1385Redmine_ContratanteFan = "";
            n1385Redmine_ContratanteFan = false;
         }
         isValidOutput.Add(StringUtil.RTrim( A1385Redmine_ContratanteFan));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(11);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z1905Redmine_User = "";
         Z1381Redmine_Host = "";
         Z1382Redmine_Url = "";
         Z1383Redmine_Key = "";
         Z1391Redmine_Versao = "";
         Z1906Redmine_CampoSistema = "";
         Z1907Redmine_CampoServico = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         A1391Redmine_Versao = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockredmine_codigo_Jsonclick = "";
         lblTextblockredmine_contratantecod_Jsonclick = "";
         lblTextblockredmine_contratantefan_Jsonclick = "";
         A1385Redmine_ContratanteFan = "";
         lblTextblockredmine_user_Jsonclick = "";
         A1905Redmine_User = "";
         lblTextblockredmine_secure_Jsonclick = "";
         lblTextblockredmine_host_Jsonclick = "";
         A1381Redmine_Host = "";
         lblTextblockredmine_url_Jsonclick = "";
         A1382Redmine_Url = "";
         lblTextblockredmine_key_Jsonclick = "";
         A1383Redmine_Key = "";
         lblTextblockredmine_versao_Jsonclick = "";
         lblTextblockredmine_camposistema_Jsonclick = "";
         A1906Redmine_CampoSistema = "";
         lblTextblockredmine_camposervico_Jsonclick = "";
         A1907Redmine_CampoServico = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z1385Redmine_ContratanteFan = "";
         T003M5_A1380Redmine_Codigo = new int[1] ;
         T003M5_A1385Redmine_ContratanteFan = new String[] {""} ;
         T003M5_n1385Redmine_ContratanteFan = new bool[] {false} ;
         T003M5_A1905Redmine_User = new String[] {""} ;
         T003M5_n1905Redmine_User = new bool[] {false} ;
         T003M5_A1904Redmine_Secure = new short[1] ;
         T003M5_A1381Redmine_Host = new String[] {""} ;
         T003M5_A1382Redmine_Url = new String[] {""} ;
         T003M5_A1383Redmine_Key = new String[] {""} ;
         T003M5_A1391Redmine_Versao = new String[] {""} ;
         T003M5_n1391Redmine_Versao = new bool[] {false} ;
         T003M5_A1906Redmine_CampoSistema = new String[] {""} ;
         T003M5_n1906Redmine_CampoSistema = new bool[] {false} ;
         T003M5_A1907Redmine_CampoServico = new String[] {""} ;
         T003M5_n1907Redmine_CampoServico = new bool[] {false} ;
         T003M5_A1384Redmine_ContratanteCod = new int[1] ;
         T003M4_A1385Redmine_ContratanteFan = new String[] {""} ;
         T003M4_n1385Redmine_ContratanteFan = new bool[] {false} ;
         T003M6_A1385Redmine_ContratanteFan = new String[] {""} ;
         T003M6_n1385Redmine_ContratanteFan = new bool[] {false} ;
         T003M7_A1380Redmine_Codigo = new int[1] ;
         T003M3_A1380Redmine_Codigo = new int[1] ;
         T003M3_A1905Redmine_User = new String[] {""} ;
         T003M3_n1905Redmine_User = new bool[] {false} ;
         T003M3_A1904Redmine_Secure = new short[1] ;
         T003M3_A1381Redmine_Host = new String[] {""} ;
         T003M3_A1382Redmine_Url = new String[] {""} ;
         T003M3_A1383Redmine_Key = new String[] {""} ;
         T003M3_A1391Redmine_Versao = new String[] {""} ;
         T003M3_n1391Redmine_Versao = new bool[] {false} ;
         T003M3_A1906Redmine_CampoSistema = new String[] {""} ;
         T003M3_n1906Redmine_CampoSistema = new bool[] {false} ;
         T003M3_A1907Redmine_CampoServico = new String[] {""} ;
         T003M3_n1907Redmine_CampoServico = new bool[] {false} ;
         T003M3_A1384Redmine_ContratanteCod = new int[1] ;
         sMode165 = "";
         T003M8_A1380Redmine_Codigo = new int[1] ;
         T003M9_A1380Redmine_Codigo = new int[1] ;
         T003M2_A1380Redmine_Codigo = new int[1] ;
         T003M2_A1905Redmine_User = new String[] {""} ;
         T003M2_n1905Redmine_User = new bool[] {false} ;
         T003M2_A1904Redmine_Secure = new short[1] ;
         T003M2_A1381Redmine_Host = new String[] {""} ;
         T003M2_A1382Redmine_Url = new String[] {""} ;
         T003M2_A1383Redmine_Key = new String[] {""} ;
         T003M2_A1391Redmine_Versao = new String[] {""} ;
         T003M2_n1391Redmine_Versao = new bool[] {false} ;
         T003M2_A1906Redmine_CampoSistema = new String[] {""} ;
         T003M2_n1906Redmine_CampoSistema = new bool[] {false} ;
         T003M2_A1907Redmine_CampoServico = new String[] {""} ;
         T003M2_n1907Redmine_CampoServico = new bool[] {false} ;
         T003M2_A1384Redmine_ContratanteCod = new int[1] ;
         T003M10_A1380Redmine_Codigo = new int[1] ;
         T003M13_A1385Redmine_ContratanteFan = new String[] {""} ;
         T003M13_n1385Redmine_ContratanteFan = new bool[] {false} ;
         T003M14_A1380Redmine_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.redmine__default(),
            new Object[][] {
                new Object[] {
               T003M2_A1380Redmine_Codigo, T003M2_A1905Redmine_User, T003M2_n1905Redmine_User, T003M2_A1904Redmine_Secure, T003M2_A1381Redmine_Host, T003M2_A1382Redmine_Url, T003M2_A1383Redmine_Key, T003M2_A1391Redmine_Versao, T003M2_n1391Redmine_Versao, T003M2_A1906Redmine_CampoSistema,
               T003M2_n1906Redmine_CampoSistema, T003M2_A1907Redmine_CampoServico, T003M2_n1907Redmine_CampoServico, T003M2_A1384Redmine_ContratanteCod
               }
               , new Object[] {
               T003M3_A1380Redmine_Codigo, T003M3_A1905Redmine_User, T003M3_n1905Redmine_User, T003M3_A1904Redmine_Secure, T003M3_A1381Redmine_Host, T003M3_A1382Redmine_Url, T003M3_A1383Redmine_Key, T003M3_A1391Redmine_Versao, T003M3_n1391Redmine_Versao, T003M3_A1906Redmine_CampoSistema,
               T003M3_n1906Redmine_CampoSistema, T003M3_A1907Redmine_CampoServico, T003M3_n1907Redmine_CampoServico, T003M3_A1384Redmine_ContratanteCod
               }
               , new Object[] {
               T003M4_A1385Redmine_ContratanteFan, T003M4_n1385Redmine_ContratanteFan
               }
               , new Object[] {
               T003M5_A1380Redmine_Codigo, T003M5_A1385Redmine_ContratanteFan, T003M5_n1385Redmine_ContratanteFan, T003M5_A1905Redmine_User, T003M5_n1905Redmine_User, T003M5_A1904Redmine_Secure, T003M5_A1381Redmine_Host, T003M5_A1382Redmine_Url, T003M5_A1383Redmine_Key, T003M5_A1391Redmine_Versao,
               T003M5_n1391Redmine_Versao, T003M5_A1906Redmine_CampoSistema, T003M5_n1906Redmine_CampoSistema, T003M5_A1907Redmine_CampoServico, T003M5_n1907Redmine_CampoServico, T003M5_A1384Redmine_ContratanteCod
               }
               , new Object[] {
               T003M6_A1385Redmine_ContratanteFan, T003M6_n1385Redmine_ContratanteFan
               }
               , new Object[] {
               T003M7_A1380Redmine_Codigo
               }
               , new Object[] {
               T003M8_A1380Redmine_Codigo
               }
               , new Object[] {
               T003M9_A1380Redmine_Codigo
               }
               , new Object[] {
               T003M10_A1380Redmine_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003M13_A1385Redmine_ContratanteFan, T003M13_n1385Redmine_ContratanteFan
               }
               , new Object[] {
               T003M14_A1380Redmine_Codigo
               }
            }
         );
         Z1904Redmine_Secure = 0;
         A1904Redmine_Secure = 0;
         i1904Redmine_Secure = 0;
      }

      private short Z1904Redmine_Secure ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short A1904Redmine_Secure ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short GX_JID ;
      private short RcdFound165 ;
      private short gxajaxcallmode ;
      private short i1904Redmine_Secure ;
      private short wbTemp ;
      private int Z1380Redmine_Codigo ;
      private int Z1384Redmine_ContratanteCod ;
      private int A1384Redmine_ContratanteCod ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int A1380Redmine_Codigo ;
      private int edtRedmine_Codigo_Enabled ;
      private int edtRedmine_ContratanteCod_Enabled ;
      private int edtRedmine_ContratanteFan_Enabled ;
      private int edtRedmine_User_Enabled ;
      private int edtRedmine_Host_Enabled ;
      private int edtRedmine_Url_Enabled ;
      private int edtRedmine_Key_Enabled ;
      private int edtRedmine_CampoSistema_Enabled ;
      private int edtRedmine_CampoServico_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private String sPrefix ;
      private String Z1905Redmine_User ;
      private String Z1381Redmine_Host ;
      private String Z1382Redmine_Url ;
      private String Z1383Redmine_Key ;
      private String Z1391Redmine_Versao ;
      private String Z1906Redmine_CampoSistema ;
      private String Z1907Redmine_CampoServico ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String A1391Redmine_Versao ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtRedmine_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockredmine_codigo_Internalname ;
      private String lblTextblockredmine_codigo_Jsonclick ;
      private String edtRedmine_Codigo_Jsonclick ;
      private String lblTextblockredmine_contratantecod_Internalname ;
      private String lblTextblockredmine_contratantecod_Jsonclick ;
      private String edtRedmine_ContratanteCod_Internalname ;
      private String edtRedmine_ContratanteCod_Jsonclick ;
      private String lblTextblockredmine_contratantefan_Internalname ;
      private String lblTextblockredmine_contratantefan_Jsonclick ;
      private String edtRedmine_ContratanteFan_Internalname ;
      private String A1385Redmine_ContratanteFan ;
      private String edtRedmine_ContratanteFan_Jsonclick ;
      private String lblTextblockredmine_user_Internalname ;
      private String lblTextblockredmine_user_Jsonclick ;
      private String edtRedmine_User_Internalname ;
      private String A1905Redmine_User ;
      private String edtRedmine_User_Jsonclick ;
      private String lblTextblockredmine_secure_Internalname ;
      private String lblTextblockredmine_secure_Jsonclick ;
      private String cmbRedmine_Secure_Internalname ;
      private String cmbRedmine_Secure_Jsonclick ;
      private String lblTextblockredmine_host_Internalname ;
      private String lblTextblockredmine_host_Jsonclick ;
      private String edtRedmine_Host_Internalname ;
      private String A1381Redmine_Host ;
      private String edtRedmine_Host_Jsonclick ;
      private String lblTextblockredmine_url_Internalname ;
      private String lblTextblockredmine_url_Jsonclick ;
      private String edtRedmine_Url_Internalname ;
      private String A1382Redmine_Url ;
      private String edtRedmine_Url_Jsonclick ;
      private String lblTextblockredmine_key_Internalname ;
      private String lblTextblockredmine_key_Jsonclick ;
      private String edtRedmine_Key_Internalname ;
      private String A1383Redmine_Key ;
      private String edtRedmine_Key_Jsonclick ;
      private String lblTextblockredmine_versao_Internalname ;
      private String lblTextblockredmine_versao_Jsonclick ;
      private String cmbRedmine_Versao_Internalname ;
      private String cmbRedmine_Versao_Jsonclick ;
      private String lblTextblockredmine_camposistema_Internalname ;
      private String lblTextblockredmine_camposistema_Jsonclick ;
      private String edtRedmine_CampoSistema_Internalname ;
      private String A1906Redmine_CampoSistema ;
      private String edtRedmine_CampoSistema_Jsonclick ;
      private String lblTextblockredmine_camposervico_Internalname ;
      private String lblTextblockredmine_camposervico_Jsonclick ;
      private String edtRedmine_CampoServico_Internalname ;
      private String A1907Redmine_CampoServico ;
      private String edtRedmine_CampoServico_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z1385Redmine_ContratanteFan ;
      private String sMode165 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool n1391Redmine_Versao ;
      private bool wbErr ;
      private bool n1385Redmine_ContratanteFan ;
      private bool n1905Redmine_User ;
      private bool n1906Redmine_CampoSistema ;
      private bool n1907Redmine_CampoServico ;
      private bool Gx_longc ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbRedmine_Secure ;
      private GXCombobox cmbRedmine_Versao ;
      private IDataStoreProvider pr_default ;
      private int[] T003M5_A1380Redmine_Codigo ;
      private String[] T003M5_A1385Redmine_ContratanteFan ;
      private bool[] T003M5_n1385Redmine_ContratanteFan ;
      private String[] T003M5_A1905Redmine_User ;
      private bool[] T003M5_n1905Redmine_User ;
      private short[] T003M5_A1904Redmine_Secure ;
      private String[] T003M5_A1381Redmine_Host ;
      private String[] T003M5_A1382Redmine_Url ;
      private String[] T003M5_A1383Redmine_Key ;
      private String[] T003M5_A1391Redmine_Versao ;
      private bool[] T003M5_n1391Redmine_Versao ;
      private String[] T003M5_A1906Redmine_CampoSistema ;
      private bool[] T003M5_n1906Redmine_CampoSistema ;
      private String[] T003M5_A1907Redmine_CampoServico ;
      private bool[] T003M5_n1907Redmine_CampoServico ;
      private int[] T003M5_A1384Redmine_ContratanteCod ;
      private String[] T003M4_A1385Redmine_ContratanteFan ;
      private bool[] T003M4_n1385Redmine_ContratanteFan ;
      private String[] T003M6_A1385Redmine_ContratanteFan ;
      private bool[] T003M6_n1385Redmine_ContratanteFan ;
      private int[] T003M7_A1380Redmine_Codigo ;
      private int[] T003M3_A1380Redmine_Codigo ;
      private String[] T003M3_A1905Redmine_User ;
      private bool[] T003M3_n1905Redmine_User ;
      private short[] T003M3_A1904Redmine_Secure ;
      private String[] T003M3_A1381Redmine_Host ;
      private String[] T003M3_A1382Redmine_Url ;
      private String[] T003M3_A1383Redmine_Key ;
      private String[] T003M3_A1391Redmine_Versao ;
      private bool[] T003M3_n1391Redmine_Versao ;
      private String[] T003M3_A1906Redmine_CampoSistema ;
      private bool[] T003M3_n1906Redmine_CampoSistema ;
      private String[] T003M3_A1907Redmine_CampoServico ;
      private bool[] T003M3_n1907Redmine_CampoServico ;
      private int[] T003M3_A1384Redmine_ContratanteCod ;
      private int[] T003M8_A1380Redmine_Codigo ;
      private int[] T003M9_A1380Redmine_Codigo ;
      private int[] T003M2_A1380Redmine_Codigo ;
      private String[] T003M2_A1905Redmine_User ;
      private bool[] T003M2_n1905Redmine_User ;
      private short[] T003M2_A1904Redmine_Secure ;
      private String[] T003M2_A1381Redmine_Host ;
      private String[] T003M2_A1382Redmine_Url ;
      private String[] T003M2_A1383Redmine_Key ;
      private String[] T003M2_A1391Redmine_Versao ;
      private bool[] T003M2_n1391Redmine_Versao ;
      private String[] T003M2_A1906Redmine_CampoSistema ;
      private bool[] T003M2_n1906Redmine_CampoSistema ;
      private String[] T003M2_A1907Redmine_CampoServico ;
      private bool[] T003M2_n1907Redmine_CampoServico ;
      private int[] T003M2_A1384Redmine_ContratanteCod ;
      private int[] T003M10_A1380Redmine_Codigo ;
      private String[] T003M13_A1385Redmine_ContratanteFan ;
      private bool[] T003M13_n1385Redmine_ContratanteFan ;
      private int[] T003M14_A1380Redmine_Codigo ;
      private GXWebForm Form ;
   }

   public class redmine__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT003M5 ;
          prmT003M5 = new Object[] {
          new Object[] {"@Redmine_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003M4 ;
          prmT003M4 = new Object[] {
          new Object[] {"@Redmine_ContratanteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003M6 ;
          prmT003M6 = new Object[] {
          new Object[] {"@Redmine_ContratanteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003M7 ;
          prmT003M7 = new Object[] {
          new Object[] {"@Redmine_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003M3 ;
          prmT003M3 = new Object[] {
          new Object[] {"@Redmine_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003M8 ;
          prmT003M8 = new Object[] {
          new Object[] {"@Redmine_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003M9 ;
          prmT003M9 = new Object[] {
          new Object[] {"@Redmine_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003M2 ;
          prmT003M2 = new Object[] {
          new Object[] {"@Redmine_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003M10 ;
          prmT003M10 = new Object[] {
          new Object[] {"@Redmine_User",SqlDbType.Char,50,0} ,
          new Object[] {"@Redmine_Secure",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@Redmine_Host",SqlDbType.Char,100,0} ,
          new Object[] {"@Redmine_Url",SqlDbType.Char,100,0} ,
          new Object[] {"@Redmine_Key",SqlDbType.Char,50,0} ,
          new Object[] {"@Redmine_Versao",SqlDbType.Char,20,0} ,
          new Object[] {"@Redmine_CampoSistema",SqlDbType.Char,50,0} ,
          new Object[] {"@Redmine_CampoServico",SqlDbType.Char,50,0} ,
          new Object[] {"@Redmine_ContratanteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003M11 ;
          prmT003M11 = new Object[] {
          new Object[] {"@Redmine_User",SqlDbType.Char,50,0} ,
          new Object[] {"@Redmine_Secure",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@Redmine_Host",SqlDbType.Char,100,0} ,
          new Object[] {"@Redmine_Url",SqlDbType.Char,100,0} ,
          new Object[] {"@Redmine_Key",SqlDbType.Char,50,0} ,
          new Object[] {"@Redmine_Versao",SqlDbType.Char,20,0} ,
          new Object[] {"@Redmine_CampoSistema",SqlDbType.Char,50,0} ,
          new Object[] {"@Redmine_CampoServico",SqlDbType.Char,50,0} ,
          new Object[] {"@Redmine_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Redmine_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003M12 ;
          prmT003M12 = new Object[] {
          new Object[] {"@Redmine_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003M14 ;
          prmT003M14 = new Object[] {
          } ;
          Object[] prmT003M13 ;
          prmT003M13 = new Object[] {
          new Object[] {"@Redmine_ContratanteCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T003M2", "SELECT [Redmine_Codigo], [Redmine_User], [Redmine_Secure], [Redmine_Host], [Redmine_Url], [Redmine_Key], [Redmine_Versao], [Redmine_CampoSistema], [Redmine_CampoServico], [Redmine_ContratanteCod] AS Redmine_ContratanteCod FROM [Redmine] WITH (UPDLOCK) WHERE [Redmine_Codigo] = @Redmine_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003M2,1,0,true,false )
             ,new CursorDef("T003M3", "SELECT [Redmine_Codigo], [Redmine_User], [Redmine_Secure], [Redmine_Host], [Redmine_Url], [Redmine_Key], [Redmine_Versao], [Redmine_CampoSistema], [Redmine_CampoServico], [Redmine_ContratanteCod] AS Redmine_ContratanteCod FROM [Redmine] WITH (NOLOCK) WHERE [Redmine_Codigo] = @Redmine_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003M3,1,0,true,false )
             ,new CursorDef("T003M4", "SELECT [Contratante_NomeFantasia] AS Redmine_ContratanteFan FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @Redmine_ContratanteCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003M4,1,0,true,false )
             ,new CursorDef("T003M5", "SELECT TM1.[Redmine_Codigo], T2.[Contratante_NomeFantasia] AS Redmine_ContratanteFan, TM1.[Redmine_User], TM1.[Redmine_Secure], TM1.[Redmine_Host], TM1.[Redmine_Url], TM1.[Redmine_Key], TM1.[Redmine_Versao], TM1.[Redmine_CampoSistema], TM1.[Redmine_CampoServico], TM1.[Redmine_ContratanteCod] AS Redmine_ContratanteCod FROM ([Redmine] TM1 WITH (NOLOCK) INNER JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = TM1.[Redmine_ContratanteCod]) WHERE TM1.[Redmine_Codigo] = @Redmine_Codigo ORDER BY TM1.[Redmine_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003M5,100,0,true,false )
             ,new CursorDef("T003M6", "SELECT [Contratante_NomeFantasia] AS Redmine_ContratanteFan FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @Redmine_ContratanteCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003M6,1,0,true,false )
             ,new CursorDef("T003M7", "SELECT [Redmine_Codigo] FROM [Redmine] WITH (NOLOCK) WHERE [Redmine_Codigo] = @Redmine_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003M7,1,0,true,false )
             ,new CursorDef("T003M8", "SELECT TOP 1 [Redmine_Codigo] FROM [Redmine] WITH (NOLOCK) WHERE ( [Redmine_Codigo] > @Redmine_Codigo) ORDER BY [Redmine_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003M8,1,0,true,true )
             ,new CursorDef("T003M9", "SELECT TOP 1 [Redmine_Codigo] FROM [Redmine] WITH (NOLOCK) WHERE ( [Redmine_Codigo] < @Redmine_Codigo) ORDER BY [Redmine_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003M9,1,0,true,true )
             ,new CursorDef("T003M10", "INSERT INTO [Redmine]([Redmine_User], [Redmine_Secure], [Redmine_Host], [Redmine_Url], [Redmine_Key], [Redmine_Versao], [Redmine_CampoSistema], [Redmine_CampoServico], [Redmine_ContratanteCod]) VALUES(@Redmine_User, @Redmine_Secure, @Redmine_Host, @Redmine_Url, @Redmine_Key, @Redmine_Versao, @Redmine_CampoSistema, @Redmine_CampoServico, @Redmine_ContratanteCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT003M10)
             ,new CursorDef("T003M11", "UPDATE [Redmine] SET [Redmine_User]=@Redmine_User, [Redmine_Secure]=@Redmine_Secure, [Redmine_Host]=@Redmine_Host, [Redmine_Url]=@Redmine_Url, [Redmine_Key]=@Redmine_Key, [Redmine_Versao]=@Redmine_Versao, [Redmine_CampoSistema]=@Redmine_CampoSistema, [Redmine_CampoServico]=@Redmine_CampoServico, [Redmine_ContratanteCod]=@Redmine_ContratanteCod  WHERE [Redmine_Codigo] = @Redmine_Codigo", GxErrorMask.GX_NOMASK,prmT003M11)
             ,new CursorDef("T003M12", "DELETE FROM [Redmine]  WHERE [Redmine_Codigo] = @Redmine_Codigo", GxErrorMask.GX_NOMASK,prmT003M12)
             ,new CursorDef("T003M13", "SELECT [Contratante_NomeFantasia] AS Redmine_ContratanteFan FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @Redmine_ContratanteCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003M13,1,0,true,false )
             ,new CursorDef("T003M14", "SELECT [Redmine_Codigo] FROM [Redmine] WITH (NOLOCK) ORDER BY [Redmine_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003M14,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 100) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[7])[0] = rslt.getString(7, 20) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((String[]) buf[9])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((String[]) buf[11])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((int[]) buf[13])[0] = rslt.getInt(10) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 100) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[7])[0] = rslt.getString(7, 20) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((String[]) buf[9])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((String[]) buf[11])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((int[]) buf[13])[0] = rslt.getInt(10) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 100) ;
                ((String[]) buf[7])[0] = rslt.getString(6, 100) ;
                ((String[]) buf[8])[0] = rslt.getString(7, 50) ;
                ((String[]) buf[9])[0] = rslt.getString(8, 20) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((String[]) buf[11])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getString(10, 50) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((int[]) buf[15])[0] = rslt.getInt(11) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (short)parms[2]);
                stmt.SetParameter(3, (String)parms[3]);
                stmt.SetParameter(4, (String)parms[4]);
                stmt.SetParameter(5, (String)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[11]);
                }
                stmt.SetParameter(9, (int)parms[12]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (short)parms[2]);
                stmt.SetParameter(3, (String)parms[3]);
                stmt.SetParameter(4, (String)parms[4]);
                stmt.SetParameter(5, (String)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[11]);
                }
                stmt.SetParameter(9, (int)parms[12]);
                stmt.SetParameter(10, (int)parms[13]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
