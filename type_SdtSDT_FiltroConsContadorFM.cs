/*
               File: type_SdtSDT_FiltroConsContadorFM
        Description: SDT_FiltroConsContadorFM
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/17/2020 15:47:40.13
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_FiltroConsContadorFM" )]
   [XmlType(TypeName =  "SDT_FiltroConsContadorFM" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtSDT_FiltroConsContadorFM : GxUserType
   {
      public SdtSDT_FiltroConsContadorFM( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_FiltroConsContadorFM_Nome = "";
      }

      public SdtSDT_FiltroConsContadorFM( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_FiltroConsContadorFM deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_FiltroConsContadorFM)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_FiltroConsContadorFM obj ;
         obj = this;
         obj.gxTpr_Areatrabalho_codigo = deserialized.gxTpr_Areatrabalho_codigo;
         obj.gxTpr_Contagemresultado_contadorfmcod = deserialized.gxTpr_Contagemresultado_contadorfmcod;
         obj.gxTpr_Mes = deserialized.gxTpr_Mes;
         obj.gxTpr_Ano = deserialized.gxTpr_Ano;
         obj.gxTpr_Abertas = deserialized.gxTpr_Abertas;
         obj.gxTpr_Comerro = deserialized.gxTpr_Comerro;
         obj.gxTpr_Solicitadas = deserialized.gxTpr_Solicitadas;
         obj.gxTpr_Soconfirmadas = deserialized.gxTpr_Soconfirmadas;
         obj.gxTpr_Servico = deserialized.gxTpr_Servico;
         obj.gxTpr_Sistema = deserialized.gxTpr_Sistema;
         obj.gxTpr_Nome = deserialized.gxTpr_Nome;
         obj.gxTpr_Semliquidar = deserialized.gxTpr_Semliquidar;
         obj.gxTpr_Lote = deserialized.gxTpr_Lote;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_Codigo") )
               {
                  gxTv_SdtSDT_FiltroConsContadorFM_Areatrabalho_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContadorFMCod") )
               {
                  gxTv_SdtSDT_FiltroConsContadorFM_Contagemresultado_contadorfmcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mes") )
               {
                  gxTv_SdtSDT_FiltroConsContadorFM_Mes = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Ano") )
               {
                  gxTv_SdtSDT_FiltroConsContadorFM_Ano = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Abertas") )
               {
                  gxTv_SdtSDT_FiltroConsContadorFM_Abertas = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ComErro") )
               {
                  gxTv_SdtSDT_FiltroConsContadorFM_Comerro = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Solicitadas") )
               {
                  gxTv_SdtSDT_FiltroConsContadorFM_Solicitadas = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SoConfirmadas") )
               {
                  gxTv_SdtSDT_FiltroConsContadorFM_Soconfirmadas = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico") )
               {
                  gxTv_SdtSDT_FiltroConsContadorFM_Servico = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema") )
               {
                  gxTv_SdtSDT_FiltroConsContadorFM_Sistema = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Nome") )
               {
                  gxTv_SdtSDT_FiltroConsContadorFM_Nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SemLiquidar") )
               {
                  gxTv_SdtSDT_FiltroConsContadorFM_Semliquidar = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Lote") )
               {
                  gxTv_SdtSDT_FiltroConsContadorFM_Lote = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_FiltroConsContadorFM";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("AreaTrabalho_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_FiltroConsContadorFM_Areatrabalho_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_ContadorFMCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_FiltroConsContadorFM_Contagemresultado_contadorfmcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Mes", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_FiltroConsContadorFM_Mes), 10, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Ano", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_FiltroConsContadorFM_Ano), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Abertas", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_FiltroConsContadorFM_Abertas)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ComErro", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_FiltroConsContadorFM_Comerro)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Solicitadas", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_FiltroConsContadorFM_Solicitadas)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("SoConfirmadas", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_FiltroConsContadorFM_Soconfirmadas)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_FiltroConsContadorFM_Servico), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Sistema", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_FiltroConsContadorFM_Sistema), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Nome", StringUtil.RTrim( gxTv_SdtSDT_FiltroConsContadorFM_Nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("SemLiquidar", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_FiltroConsContadorFM_Semliquidar)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Lote", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_FiltroConsContadorFM_Lote), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("AreaTrabalho_Codigo", gxTv_SdtSDT_FiltroConsContadorFM_Areatrabalho_codigo, false);
         AddObjectProperty("ContagemResultado_ContadorFMCod", gxTv_SdtSDT_FiltroConsContadorFM_Contagemresultado_contadorfmcod, false);
         AddObjectProperty("Mes", gxTv_SdtSDT_FiltroConsContadorFM_Mes, false);
         AddObjectProperty("Ano", gxTv_SdtSDT_FiltroConsContadorFM_Ano, false);
         AddObjectProperty("Abertas", gxTv_SdtSDT_FiltroConsContadorFM_Abertas, false);
         AddObjectProperty("ComErro", gxTv_SdtSDT_FiltroConsContadorFM_Comerro, false);
         AddObjectProperty("Solicitadas", gxTv_SdtSDT_FiltroConsContadorFM_Solicitadas, false);
         AddObjectProperty("SoConfirmadas", gxTv_SdtSDT_FiltroConsContadorFM_Soconfirmadas, false);
         AddObjectProperty("Servico", gxTv_SdtSDT_FiltroConsContadorFM_Servico, false);
         AddObjectProperty("Sistema", gxTv_SdtSDT_FiltroConsContadorFM_Sistema, false);
         AddObjectProperty("Nome", gxTv_SdtSDT_FiltroConsContadorFM_Nome, false);
         AddObjectProperty("SemLiquidar", gxTv_SdtSDT_FiltroConsContadorFM_Semliquidar, false);
         AddObjectProperty("Lote", gxTv_SdtSDT_FiltroConsContadorFM_Lote, false);
         return  ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_Codigo" )]
      [  XmlElement( ElementName = "AreaTrabalho_Codigo"   )]
      public int gxTpr_Areatrabalho_codigo
      {
         get {
            return gxTv_SdtSDT_FiltroConsContadorFM_Areatrabalho_codigo ;
         }

         set {
            gxTv_SdtSDT_FiltroConsContadorFM_Areatrabalho_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ContadorFMCod" )]
      [  XmlElement( ElementName = "ContagemResultado_ContadorFMCod"   )]
      public int gxTpr_Contagemresultado_contadorfmcod
      {
         get {
            return gxTv_SdtSDT_FiltroConsContadorFM_Contagemresultado_contadorfmcod ;
         }

         set {
            gxTv_SdtSDT_FiltroConsContadorFM_Contagemresultado_contadorfmcod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Mes" )]
      [  XmlElement( ElementName = "Mes"   )]
      public long gxTpr_Mes
      {
         get {
            return gxTv_SdtSDT_FiltroConsContadorFM_Mes ;
         }

         set {
            gxTv_SdtSDT_FiltroConsContadorFM_Mes = (long)(value);
         }

      }

      [  SoapElement( ElementName = "Ano" )]
      [  XmlElement( ElementName = "Ano"   )]
      public short gxTpr_Ano
      {
         get {
            return gxTv_SdtSDT_FiltroConsContadorFM_Ano ;
         }

         set {
            gxTv_SdtSDT_FiltroConsContadorFM_Ano = (short)(value);
         }

      }

      [  SoapElement( ElementName = "Abertas" )]
      [  XmlElement( ElementName = "Abertas"   )]
      public bool gxTpr_Abertas
      {
         get {
            return gxTv_SdtSDT_FiltroConsContadorFM_Abertas ;
         }

         set {
            gxTv_SdtSDT_FiltroConsContadorFM_Abertas = value;
         }

      }

      [  SoapElement( ElementName = "ComErro" )]
      [  XmlElement( ElementName = "ComErro"   )]
      public bool gxTpr_Comerro
      {
         get {
            return gxTv_SdtSDT_FiltroConsContadorFM_Comerro ;
         }

         set {
            gxTv_SdtSDT_FiltroConsContadorFM_Comerro = value;
         }

      }

      [  SoapElement( ElementName = "Solicitadas" )]
      [  XmlElement( ElementName = "Solicitadas"   )]
      public bool gxTpr_Solicitadas
      {
         get {
            return gxTv_SdtSDT_FiltroConsContadorFM_Solicitadas ;
         }

         set {
            gxTv_SdtSDT_FiltroConsContadorFM_Solicitadas = value;
         }

      }

      [  SoapElement( ElementName = "SoConfirmadas" )]
      [  XmlElement( ElementName = "SoConfirmadas"   )]
      public bool gxTpr_Soconfirmadas
      {
         get {
            return gxTv_SdtSDT_FiltroConsContadorFM_Soconfirmadas ;
         }

         set {
            gxTv_SdtSDT_FiltroConsContadorFM_Soconfirmadas = value;
         }

      }

      [  SoapElement( ElementName = "Servico" )]
      [  XmlElement( ElementName = "Servico"   )]
      public int gxTpr_Servico
      {
         get {
            return gxTv_SdtSDT_FiltroConsContadorFM_Servico ;
         }

         set {
            gxTv_SdtSDT_FiltroConsContadorFM_Servico = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Sistema" )]
      [  XmlElement( ElementName = "Sistema"   )]
      public int gxTpr_Sistema
      {
         get {
            return gxTv_SdtSDT_FiltroConsContadorFM_Sistema ;
         }

         set {
            gxTv_SdtSDT_FiltroConsContadorFM_Sistema = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Nome" )]
      [  XmlElement( ElementName = "Nome"   )]
      public String gxTpr_Nome
      {
         get {
            return gxTv_SdtSDT_FiltroConsContadorFM_Nome ;
         }

         set {
            gxTv_SdtSDT_FiltroConsContadorFM_Nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "SemLiquidar" )]
      [  XmlElement( ElementName = "SemLiquidar"   )]
      public bool gxTpr_Semliquidar
      {
         get {
            return gxTv_SdtSDT_FiltroConsContadorFM_Semliquidar ;
         }

         set {
            gxTv_SdtSDT_FiltroConsContadorFM_Semliquidar = value;
         }

      }

      [  SoapElement( ElementName = "Lote" )]
      [  XmlElement( ElementName = "Lote"   )]
      public int gxTpr_Lote
      {
         get {
            return gxTv_SdtSDT_FiltroConsContadorFM_Lote ;
         }

         set {
            gxTv_SdtSDT_FiltroConsContadorFM_Lote = (int)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_FiltroConsContadorFM_Nome = "";
         sTagName = "";
         return  ;
      }

      protected short gxTv_SdtSDT_FiltroConsContadorFM_Ano ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_FiltroConsContadorFM_Areatrabalho_codigo ;
      protected int gxTv_SdtSDT_FiltroConsContadorFM_Contagemresultado_contadorfmcod ;
      protected int gxTv_SdtSDT_FiltroConsContadorFM_Servico ;
      protected int gxTv_SdtSDT_FiltroConsContadorFM_Sistema ;
      protected int gxTv_SdtSDT_FiltroConsContadorFM_Lote ;
      protected long gxTv_SdtSDT_FiltroConsContadorFM_Mes ;
      protected String gxTv_SdtSDT_FiltroConsContadorFM_Nome ;
      protected String sTagName ;
      protected bool gxTv_SdtSDT_FiltroConsContadorFM_Abertas ;
      protected bool gxTv_SdtSDT_FiltroConsContadorFM_Comerro ;
      protected bool gxTv_SdtSDT_FiltroConsContadorFM_Solicitadas ;
      protected bool gxTv_SdtSDT_FiltroConsContadorFM_Soconfirmadas ;
      protected bool gxTv_SdtSDT_FiltroConsContadorFM_Semliquidar ;
   }

   [DataContract(Name = @"SDT_FiltroConsContadorFM", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtSDT_FiltroConsContadorFM_RESTInterface : GxGenericCollectionItem<SdtSDT_FiltroConsContadorFM>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_FiltroConsContadorFM_RESTInterface( ) : base()
      {
      }

      public SdtSDT_FiltroConsContadorFM_RESTInterface( SdtSDT_FiltroConsContadorFM psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "AreaTrabalho_Codigo" , Order = 0 )]
      public Nullable<int> gxTpr_Areatrabalho_codigo
      {
         get {
            return sdt.gxTpr_Areatrabalho_codigo ;
         }

         set {
            sdt.gxTpr_Areatrabalho_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_ContadorFMCod" , Order = 1 )]
      public Nullable<int> gxTpr_Contagemresultado_contadorfmcod
      {
         get {
            return sdt.gxTpr_Contagemresultado_contadorfmcod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contadorfmcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Mes" , Order = 2 )]
      public String gxTpr_Mes
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Mes), 10, 0)) ;
         }

         set {
            sdt.gxTpr_Mes = (long)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "Ano" , Order = 3 )]
      public Nullable<short> gxTpr_Ano
      {
         get {
            return sdt.gxTpr_Ano ;
         }

         set {
            sdt.gxTpr_Ano = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Abertas" , Order = 4 )]
      public bool gxTpr_Abertas
      {
         get {
            return sdt.gxTpr_Abertas ;
         }

         set {
            sdt.gxTpr_Abertas = value;
         }

      }

      [DataMember( Name = "ComErro" , Order = 5 )]
      public bool gxTpr_Comerro
      {
         get {
            return sdt.gxTpr_Comerro ;
         }

         set {
            sdt.gxTpr_Comerro = value;
         }

      }

      [DataMember( Name = "Solicitadas" , Order = 6 )]
      public bool gxTpr_Solicitadas
      {
         get {
            return sdt.gxTpr_Solicitadas ;
         }

         set {
            sdt.gxTpr_Solicitadas = value;
         }

      }

      [DataMember( Name = "SoConfirmadas" , Order = 7 )]
      public bool gxTpr_Soconfirmadas
      {
         get {
            return sdt.gxTpr_Soconfirmadas ;
         }

         set {
            sdt.gxTpr_Soconfirmadas = value;
         }

      }

      [DataMember( Name = "Servico" , Order = 8 )]
      public Nullable<int> gxTpr_Servico
      {
         get {
            return sdt.gxTpr_Servico ;
         }

         set {
            sdt.gxTpr_Servico = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Sistema" , Order = 9 )]
      public Nullable<int> gxTpr_Sistema
      {
         get {
            return sdt.gxTpr_Sistema ;
         }

         set {
            sdt.gxTpr_Sistema = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Nome" , Order = 10 )]
      public String gxTpr_Nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Nome) ;
         }

         set {
            sdt.gxTpr_Nome = (String)(value);
         }

      }

      [DataMember( Name = "SemLiquidar" , Order = 11 )]
      public bool gxTpr_Semliquidar
      {
         get {
            return sdt.gxTpr_Semliquidar ;
         }

         set {
            sdt.gxTpr_Semliquidar = value;
         }

      }

      [DataMember( Name = "Lote" , Order = 12 )]
      public Nullable<int> gxTpr_Lote
      {
         get {
            return sdt.gxTpr_Lote ;
         }

         set {
            sdt.gxTpr_Lote = (int)(value.HasValue ? value.Value : 0);
         }

      }

      public SdtSDT_FiltroConsContadorFM sdt
      {
         get {
            return (SdtSDT_FiltroConsContadorFM)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_FiltroConsContadorFM() ;
         }
      }

   }

}
