/*
               File: ContratoOcorrencia
        Description: Contrato Ocorr�ncia
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/6/2020 23:55:17.19
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoocorrencia : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"CONTRATOOCORRENCIA_NAOCNFCOD") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLACONTRATOOCORRENCIA_NAOCNFCOD1F50( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_15") == 0 )
         {
            A2027ContratoOcorrencia_NaoCnfCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n2027ContratoOcorrencia_NaoCnfCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2027ContratoOcorrencia_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2027ContratoOcorrencia_NaoCnfCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_15( A2027ContratoOcorrencia_NaoCnfCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7ContratoOcorrencia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoOcorrencia_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOOCORRENCIA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoOcorrencia_Codigo), "ZZZZZ9")));
               A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynContratoOcorrencia_NaoCnfCod.Name = "CONTRATOOCORRENCIA_NAOCNFCOD";
         dynContratoOcorrencia_NaoCnfCod.WebTags = "";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contrato Ocorr�ncia", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContratoOcorrencia_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contratoocorrencia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratoocorrencia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ContratoOcorrencia_Codigo ,
                           ref int aP2_Contrato_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7ContratoOcorrencia_Codigo = aP1_ContratoOcorrencia_Codigo;
         this.A74Contrato_Codigo = aP2_Contrato_Codigo;
         executePrivate();
         aP2_Contrato_Codigo=this.A74Contrato_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynContratoOcorrencia_NaoCnfCod = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynContratoOcorrencia_NaoCnfCod.ItemCount > 0 )
         {
            A2027ContratoOcorrencia_NaoCnfCod = (int)(NumberUtil.Val( dynContratoOcorrencia_NaoCnfCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2027ContratoOcorrencia_NaoCnfCod), 6, 0))), "."));
            n2027ContratoOcorrencia_NaoCnfCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2027ContratoOcorrencia_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2027ContratoOcorrencia_NaoCnfCod), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_1F50( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1F50e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1F50( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_1F50( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_1F50e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_63_1F50( true) ;
         }
         return  ;
      }

      protected void wb_table3_63_1F50e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1F50e( true) ;
         }
         else
         {
            wb_table1_2_1F50e( false) ;
         }
      }

      protected void wb_table3_63_1F50( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_63_1F50e( true) ;
         }
         else
         {
            wb_table3_63_1F50e( false) ;
         }
      }

      protected void wb_table2_5_1F50( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_1F50( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_1F50e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_1F50e( true) ;
         }
         else
         {
            wb_table2_5_1F50e( false) ;
         }
      }

      protected void wb_table4_13_1F50( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup1_Internalname, "do Contrato", 1, 0, "px", 0, "px", "Group", "", "HLP_ContratoOcorrencia.htm");
            wb_table5_17_1F50( true) ;
         }
         return  ;
      }

      protected void wb_table5_17_1F50e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoocorrencia_data_Internalname, "Data", "", "", lblTextblockcontratoocorrencia_data_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContratoOcorrencia_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContratoOcorrencia_Data_Internalname, context.localUtil.Format(A295ContratoOcorrencia_Data, "99/99/99"), context.localUtil.Format( A295ContratoOcorrencia_Data, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoOcorrencia_Data_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContratoOcorrencia_Data_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_ContratoOcorrencia.htm");
            GxWebStd.gx_bitmap( context, edtContratoOcorrencia_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContratoOcorrencia_Data_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoOcorrencia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoocorrencia_descricao_Internalname, "Descri��o", "", "", lblTextblockcontratoocorrencia_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoOcorrencia_Descricao_Internalname, A296ContratoOcorrencia_Descricao, StringUtil.RTrim( context.localUtil.Format( A296ContratoOcorrencia_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,55);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoOcorrencia_Descricao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoOcorrencia_Descricao_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_ContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoocorrencia_naocnfcod_Internalname, "N�o conformidade", "", "", lblTextblockcontratoocorrencia_naocnfcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynContratoOcorrencia_NaoCnfCod, dynContratoOcorrencia_NaoCnfCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A2027ContratoOcorrencia_NaoCnfCod), 6, 0)), 1, dynContratoOcorrencia_NaoCnfCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynContratoOcorrencia_NaoCnfCod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", "", true, "HLP_ContratoOcorrencia.htm");
            dynContratoOcorrencia_NaoCnfCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2027ContratoOcorrencia_NaoCnfCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratoOcorrencia_NaoCnfCod_Internalname, "Values", (String)(dynContratoOcorrencia_NaoCnfCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_1F50e( true) ;
         }
         else
         {
            wb_table4_13_1F50e( false) ;
         }
      }

      protected void wb_table5_17_1F50( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblContrato_Internalname, tblContrato_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_numero_Internalname, "N� do Contrato", "", "", lblTextblockcontrato_numero_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table6_22_1F50( true) ;
         }
         return  ;
      }

      protected void wb_table6_22_1F50e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoanom_Internalname, "Contratada", "", "", lblTextblockcontratada_pessoanom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table7_38_1F50( true) ;
         }
         return  ;
      }

      protected void wb_table7_38_1F50e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_17_1F50e( true) ;
         }
         else
         {
            wb_table5_17_1F50e( false) ;
         }
      }

      protected void wb_table7_38_1F50( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratada_pessoanom_Internalname, tblTablemergedcontratada_pessoanom_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaNom_Internalname, StringUtil.RTrim( A41Contratada_PessoaNom), StringUtil.RTrim( context.localUtil.Format( A41Contratada_PessoaNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_PessoaNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratada_PessoaNom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoacnpj_Internalname, "CNPJ", "", "", lblTextblockcontratada_pessoacnpj_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaCNPJ_Internalname, A42Contratada_PessoaCNPJ, StringUtil.RTrim( context.localUtil.Format( A42Contratada_PessoaCNPJ, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_PessoaCNPJ_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratada_PessoaCNPJ_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Docto", "left", true, "HLP_ContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_38_1F50e( true) ;
         }
         else
         {
            wb_table7_38_1F50e( false) ;
         }
      }

      protected void wb_table6_22_1F50( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontrato_numero_Internalname, tblTablemergedcontrato_numero_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Numero_Internalname, StringUtil.RTrim( A77Contrato_Numero), StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Numero_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_Numero_Enabled, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "NumeroContrato", "left", true, "HLP_ContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_numeroata_Internalname, "N� da Ata", "", "", lblTextblockcontrato_numeroata_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_NumeroAta_Internalname, StringUtil.RTrim( A78Contrato_NumeroAta), StringUtil.RTrim( context.localUtil.Format( A78Contrato_NumeroAta, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_NumeroAta_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_NumeroAta_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "NumeroAta", "left", true, "HLP_ContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_ano_Internalname, "Ano", "", "", lblTextblockcontrato_ano_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Ano_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A79Contrato_Ano), 4, 0, ",", "")), ((edtContrato_Ano_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A79Contrato_Ano), "ZZZ9")) : context.localUtil.Format( (decimal)(A79Contrato_Ano), "ZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Ano_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_Ano_Enabled, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Ano", "right", false, "HLP_ContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_22_1F50e( true) ;
         }
         else
         {
            wb_table6_22_1F50e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E111F2 */
         E111F2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
               A78Contrato_NumeroAta = cgiGet( edtContrato_NumeroAta_Internalname);
               n78Contrato_NumeroAta = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
               A79Contrato_Ano = (short)(context.localUtil.CToN( cgiGet( edtContrato_Ano_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
               A41Contratada_PessoaNom = StringUtil.Upper( cgiGet( edtContratada_PessoaNom_Internalname));
               n41Contratada_PessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
               A42Contratada_PessoaCNPJ = cgiGet( edtContratada_PessoaCNPJ_Internalname);
               n42Contratada_PessoaCNPJ = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
               if ( context.localUtil.VCDate( cgiGet( edtContratoOcorrencia_Data_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data da Ocorr�ncia"}), 1, "CONTRATOOCORRENCIA_DATA");
                  AnyError = 1;
                  GX_FocusControl = edtContratoOcorrencia_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A295ContratoOcorrencia_Data = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A295ContratoOcorrencia_Data", context.localUtil.Format(A295ContratoOcorrencia_Data, "99/99/99"));
               }
               else
               {
                  A295ContratoOcorrencia_Data = context.localUtil.CToD( cgiGet( edtContratoOcorrencia_Data_Internalname), 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A295ContratoOcorrencia_Data", context.localUtil.Format(A295ContratoOcorrencia_Data, "99/99/99"));
               }
               A296ContratoOcorrencia_Descricao = StringUtil.Upper( cgiGet( edtContratoOcorrencia_Descricao_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A296ContratoOcorrencia_Descricao", A296ContratoOcorrencia_Descricao);
               dynContratoOcorrencia_NaoCnfCod.CurrentValue = cgiGet( dynContratoOcorrencia_NaoCnfCod_Internalname);
               A2027ContratoOcorrencia_NaoCnfCod = (int)(NumberUtil.Val( cgiGet( dynContratoOcorrencia_NaoCnfCod_Internalname), "."));
               n2027ContratoOcorrencia_NaoCnfCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2027ContratoOcorrencia_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2027ContratoOcorrencia_NaoCnfCod), 6, 0)));
               n2027ContratoOcorrencia_NaoCnfCod = ((0==A2027ContratoOcorrencia_NaoCnfCod) ? true : false);
               /* Read saved values. */
               Z294ContratoOcorrencia_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z294ContratoOcorrencia_Codigo"), ",", "."));
               Z295ContratoOcorrencia_Data = context.localUtil.CToD( cgiGet( "Z295ContratoOcorrencia_Data"), 0);
               Z296ContratoOcorrencia_Descricao = cgiGet( "Z296ContratoOcorrencia_Descricao");
               Z2027ContratoOcorrencia_NaoCnfCod = (int)(context.localUtil.CToN( cgiGet( "Z2027ContratoOcorrencia_NaoCnfCod"), ",", "."));
               n2027ContratoOcorrencia_NaoCnfCod = ((0==A2027ContratoOcorrencia_NaoCnfCod) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "N74Contrato_Codigo"), ",", "."));
               N2027ContratoOcorrencia_NaoCnfCod = (int)(context.localUtil.CToN( cgiGet( "N2027ContratoOcorrencia_NaoCnfCod"), ",", "."));
               n2027ContratoOcorrencia_NaoCnfCod = ((0==A2027ContratoOcorrencia_NaoCnfCod) ? true : false);
               AV7ContratoOcorrencia_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTRATOOCORRENCIA_CODIGO"), ",", "."));
               A294ContratoOcorrencia_Codigo = (int)(context.localUtil.CToN( cgiGet( "CONTRATOOCORRENCIA_CODIGO"), ",", "."));
               AV11Insert_Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTRATO_CODIGO"), ",", "."));
               A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "CONTRATO_CODIGO"), ",", "."));
               AV13Insert_ContratoOcorrencia_NaoCnfCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTRATOOCORRENCIA_NAOCNFCOD"), ",", "."));
               A39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( "CONTRATADA_CODIGO"), ",", "."));
               A40Contratada_PessoaCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATADA_PESSOACOD"), ",", "."));
               AV14Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ContratoOcorrencia";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A294ContratoOcorrencia_Codigo), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contratoocorrencia:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("contratoocorrencia:[SecurityCheckFailed value for]"+"ContratoOcorrencia_Codigo:"+context.localUtil.Format( (decimal)(A294ContratoOcorrencia_Codigo), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A294ContratoOcorrencia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A294ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode50 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode50;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound50 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_1F0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E111F2 */
                           E111F2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E121F2 */
                           E121F2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E121F2 */
            E121F2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1F50( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes1F50( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_1F0( )
      {
         BeforeValidate1F50( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1F50( ) ;
            }
            else
            {
               CheckExtendedTable1F50( ) ;
               CloseExtendedTableCursors1F50( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption1F0( )
      {
      }

      protected void E111F2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV14Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV15GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            while ( AV15GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV15GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Contrato_Codigo") == 0 )
               {
                  AV11Insert_Contrato_Codigo = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Contrato_Codigo), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "ContratoOcorrencia_NaoCnfCod") == 0 )
               {
                  AV13Insert_ContratoOcorrencia_NaoCnfCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Insert_ContratoOcorrencia_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Insert_ContratoOcorrencia_NaoCnfCod), 6, 0)));
               }
               AV15GXV1 = (int)(AV15GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            }
         }
      }

      protected void E121F2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwcontratoocorrencia.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)A74Contrato_Codigo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM1F50( short GX_JID )
      {
         if ( ( GX_JID == 13 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z295ContratoOcorrencia_Data = T001F3_A295ContratoOcorrencia_Data[0];
               Z296ContratoOcorrencia_Descricao = T001F3_A296ContratoOcorrencia_Descricao[0];
               Z2027ContratoOcorrencia_NaoCnfCod = T001F3_A2027ContratoOcorrencia_NaoCnfCod[0];
            }
            else
            {
               Z295ContratoOcorrencia_Data = A295ContratoOcorrencia_Data;
               Z296ContratoOcorrencia_Descricao = A296ContratoOcorrencia_Descricao;
               Z2027ContratoOcorrencia_NaoCnfCod = A2027ContratoOcorrencia_NaoCnfCod;
            }
         }
         if ( GX_JID == -13 )
         {
            Z74Contrato_Codigo = A74Contrato_Codigo;
            Z294ContratoOcorrencia_Codigo = A294ContratoOcorrencia_Codigo;
            Z295ContratoOcorrencia_Data = A295ContratoOcorrencia_Data;
            Z296ContratoOcorrencia_Descricao = A296ContratoOcorrencia_Descricao;
            Z2027ContratoOcorrencia_NaoCnfCod = A2027ContratoOcorrencia_NaoCnfCod;
            Z39Contratada_Codigo = A39Contratada_Codigo;
            Z77Contrato_Numero = A77Contrato_Numero;
            Z78Contrato_NumeroAta = A78Contrato_NumeroAta;
            Z79Contrato_Ano = A79Contrato_Ano;
            Z40Contratada_PessoaCod = A40Contratada_PessoaCod;
            Z41Contratada_PessoaNom = A41Contratada_PessoaNom;
            Z42Contratada_PessoaCNPJ = A42Contratada_PessoaCNPJ;
         }
      }

      protected void standaloneNotModal( )
      {
         GXACONTRATOOCORRENCIA_NAOCNFCOD_html1F50( ) ;
         AV14Pgmname = "ContratoOcorrencia";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7ContratoOcorrencia_Codigo) )
         {
            A294ContratoOcorrencia_Codigo = AV7ContratoOcorrencia_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A294ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0)));
         }
         /* Using cursor T001F4 */
         pr_default.execute(2, new Object[] {A74Contrato_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A39Contratada_Codigo = T001F4_A39Contratada_Codigo[0];
         A77Contrato_Numero = T001F4_A77Contrato_Numero[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
         A78Contrato_NumeroAta = T001F4_A78Contrato_NumeroAta[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
         n78Contrato_NumeroAta = T001F4_n78Contrato_NumeroAta[0];
         A79Contrato_Ano = T001F4_A79Contrato_Ano[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
         pr_default.close(2);
         /* Using cursor T001F6 */
         pr_default.execute(4, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contratada'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A40Contratada_PessoaCod = T001F6_A40Contratada_PessoaCod[0];
         pr_default.close(4);
         /* Using cursor T001F7 */
         pr_default.execute(5, new Object[] {A40Contratada_PessoaCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A41Contratada_PessoaNom = T001F7_A41Contratada_PessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
         n41Contratada_PessoaNom = T001F7_n41Contratada_PessoaNom[0];
         A42Contratada_PessoaCNPJ = T001F7_A42Contratada_PessoaCNPJ[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
         n42Contratada_PessoaCNPJ = T001F7_n42Contratada_PessoaCNPJ[0];
         pr_default.close(5);
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_ContratoOcorrencia_NaoCnfCod) )
         {
            dynContratoOcorrencia_NaoCnfCod.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratoOcorrencia_NaoCnfCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContratoOcorrencia_NaoCnfCod.Enabled), 5, 0)));
         }
         else
         {
            dynContratoOcorrencia_NaoCnfCod.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratoOcorrencia_NaoCnfCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContratoOcorrencia_NaoCnfCod.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_ContratoOcorrencia_NaoCnfCod) )
         {
            A2027ContratoOcorrencia_NaoCnfCod = AV13Insert_ContratoOcorrencia_NaoCnfCod;
            n2027ContratoOcorrencia_NaoCnfCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2027ContratoOcorrencia_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2027ContratoOcorrencia_NaoCnfCod), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contrato_Codigo) )
            {
               A74Contrato_Codigo = AV11Insert_Contrato_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            }
         }
      }

      protected void Load1F50( )
      {
         /* Using cursor T001F8 */
         pr_default.execute(6, new Object[] {A74Contrato_Codigo, A294ContratoOcorrencia_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound50 = 1;
            A39Contratada_Codigo = T001F8_A39Contratada_Codigo[0];
            A77Contrato_Numero = T001F8_A77Contrato_Numero[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
            A78Contrato_NumeroAta = T001F8_A78Contrato_NumeroAta[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
            n78Contrato_NumeroAta = T001F8_n78Contrato_NumeroAta[0];
            A79Contrato_Ano = T001F8_A79Contrato_Ano[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
            A41Contratada_PessoaNom = T001F8_A41Contratada_PessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
            n41Contratada_PessoaNom = T001F8_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = T001F8_A42Contratada_PessoaCNPJ[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
            n42Contratada_PessoaCNPJ = T001F8_n42Contratada_PessoaCNPJ[0];
            A295ContratoOcorrencia_Data = T001F8_A295ContratoOcorrencia_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A295ContratoOcorrencia_Data", context.localUtil.Format(A295ContratoOcorrencia_Data, "99/99/99"));
            A296ContratoOcorrencia_Descricao = T001F8_A296ContratoOcorrencia_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A296ContratoOcorrencia_Descricao", A296ContratoOcorrencia_Descricao);
            A2027ContratoOcorrencia_NaoCnfCod = T001F8_A2027ContratoOcorrencia_NaoCnfCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2027ContratoOcorrencia_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2027ContratoOcorrencia_NaoCnfCod), 6, 0)));
            n2027ContratoOcorrencia_NaoCnfCod = T001F8_n2027ContratoOcorrencia_NaoCnfCod[0];
            A40Contratada_PessoaCod = T001F8_A40Contratada_PessoaCod[0];
            ZM1F50( -13) ;
         }
         pr_default.close(6);
         OnLoadActions1F50( ) ;
      }

      protected void OnLoadActions1F50( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contrato_Codigo) )
         {
            A74Contrato_Codigo = AV11Insert_Contrato_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         }
      }

      protected void CheckExtendedTable1F50( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contrato_Codigo) )
         {
            A74Contrato_Codigo = AV11Insert_Contrato_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         }
         if ( ! ( (DateTime.MinValue==A295ContratoOcorrencia_Data) || ( A295ContratoOcorrencia_Data >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data da Ocorr�ncia fora do intervalo", "OutOfRange", 1, "CONTRATOOCORRENCIA_DATA");
            AnyError = 1;
            GX_FocusControl = edtContratoOcorrencia_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (DateTime.MinValue==A295ContratoOcorrencia_Data) )
         {
            GX_msglist.addItem("Data da Ocorr�ncia � obrigat�rio.", 1, "CONTRATOOCORRENCIA_DATA");
            AnyError = 1;
            GX_FocusControl = edtContratoOcorrencia_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A296ContratoOcorrencia_Descricao)) )
         {
            GX_msglist.addItem("Descri��o � obrigat�rio.", 1, "CONTRATOOCORRENCIA_DESCRICAO");
            AnyError = 1;
            GX_FocusControl = edtContratoOcorrencia_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T001F5 */
         pr_default.execute(3, new Object[] {n2027ContratoOcorrencia_NaoCnfCod, A2027ContratoOcorrencia_NaoCnfCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A2027ContratoOcorrencia_NaoCnfCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Ocorrencia_Nao Cnf Cod'.", "ForeignKeyNotFound", 1, "CONTRATOOCORRENCIA_NAOCNFCOD");
               AnyError = 1;
               GX_FocusControl = dynContratoOcorrencia_NaoCnfCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors1F50( )
      {
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_15( int A2027ContratoOcorrencia_NaoCnfCod )
      {
         /* Using cursor T001F9 */
         pr_default.execute(7, new Object[] {n2027ContratoOcorrencia_NaoCnfCod, A2027ContratoOcorrencia_NaoCnfCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            if ( ! ( (0==A2027ContratoOcorrencia_NaoCnfCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Ocorrencia_Nao Cnf Cod'.", "ForeignKeyNotFound", 1, "CONTRATOOCORRENCIA_NAOCNFCOD");
               AnyError = 1;
               GX_FocusControl = dynContratoOcorrencia_NaoCnfCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void GetKey1F50( )
      {
         /* Using cursor T001F10 */
         pr_default.execute(8, new Object[] {A294ContratoOcorrencia_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound50 = 1;
         }
         else
         {
            RcdFound50 = 0;
         }
         pr_default.close(8);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T001F3 */
         pr_default.execute(1, new Object[] {A294ContratoOcorrencia_Codigo});
         if ( (pr_default.getStatus(1) != 101) && ( T001F3_A74Contrato_Codigo[0] == A74Contrato_Codigo ) )
         {
            ZM1F50( 13) ;
            RcdFound50 = 1;
            A294ContratoOcorrencia_Codigo = T001F3_A294ContratoOcorrencia_Codigo[0];
            A295ContratoOcorrencia_Data = T001F3_A295ContratoOcorrencia_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A295ContratoOcorrencia_Data", context.localUtil.Format(A295ContratoOcorrencia_Data, "99/99/99"));
            A296ContratoOcorrencia_Descricao = T001F3_A296ContratoOcorrencia_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A296ContratoOcorrencia_Descricao", A296ContratoOcorrencia_Descricao);
            A2027ContratoOcorrencia_NaoCnfCod = T001F3_A2027ContratoOcorrencia_NaoCnfCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2027ContratoOcorrencia_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2027ContratoOcorrencia_NaoCnfCod), 6, 0)));
            n2027ContratoOcorrencia_NaoCnfCod = T001F3_n2027ContratoOcorrencia_NaoCnfCod[0];
            Z294ContratoOcorrencia_Codigo = A294ContratoOcorrencia_Codigo;
            sMode50 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load1F50( ) ;
            if ( AnyError == 1 )
            {
               RcdFound50 = 0;
               InitializeNonKey1F50( ) ;
            }
            Gx_mode = sMode50;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound50 = 0;
            InitializeNonKey1F50( ) ;
            sMode50 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode50;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1F50( ) ;
         if ( RcdFound50 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound50 = 0;
         /* Using cursor T001F11 */
         pr_default.execute(9, new Object[] {A294ContratoOcorrencia_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T001F11_A294ContratoOcorrencia_Codigo[0] < A294ContratoOcorrencia_Codigo ) ) && ( T001F11_A74Contrato_Codigo[0] == A74Contrato_Codigo ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T001F11_A294ContratoOcorrencia_Codigo[0] > A294ContratoOcorrencia_Codigo ) ) && ( T001F11_A74Contrato_Codigo[0] == A74Contrato_Codigo ) )
            {
               A294ContratoOcorrencia_Codigo = T001F11_A294ContratoOcorrencia_Codigo[0];
               RcdFound50 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void move_previous( )
      {
         RcdFound50 = 0;
         /* Using cursor T001F12 */
         pr_default.execute(10, new Object[] {A294ContratoOcorrencia_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            while ( (pr_default.getStatus(10) != 101) && ( ( T001F12_A294ContratoOcorrencia_Codigo[0] > A294ContratoOcorrencia_Codigo ) ) && ( T001F12_A74Contrato_Codigo[0] == A74Contrato_Codigo ) )
            {
               pr_default.readNext(10);
            }
            if ( (pr_default.getStatus(10) != 101) && ( ( T001F12_A294ContratoOcorrencia_Codigo[0] < A294ContratoOcorrencia_Codigo ) ) && ( T001F12_A74Contrato_Codigo[0] == A74Contrato_Codigo ) )
            {
               A294ContratoOcorrencia_Codigo = T001F12_A294ContratoOcorrencia_Codigo[0];
               RcdFound50 = 1;
            }
         }
         pr_default.close(10);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1F50( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContratoOcorrencia_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert1F50( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound50 == 1 )
            {
               if ( A294ContratoOcorrencia_Codigo != Z294ContratoOcorrencia_Codigo )
               {
                  A294ContratoOcorrencia_Codigo = Z294ContratoOcorrencia_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A294ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContratoOcorrencia_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update1F50( ) ;
                  GX_FocusControl = edtContratoOcorrencia_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A294ContratoOcorrencia_Codigo != Z294ContratoOcorrencia_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtContratoOcorrencia_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert1F50( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                     AnyError = 1;
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtContratoOcorrencia_Data_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert1F50( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A294ContratoOcorrencia_Codigo != Z294ContratoOcorrencia_Codigo )
         {
            A294ContratoOcorrencia_Codigo = Z294ContratoOcorrencia_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A294ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "");
            AnyError = 1;
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContratoOcorrencia_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency1F50( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T001F2 */
            pr_default.execute(0, new Object[] {A294ContratoOcorrencia_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoOcorrencia"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z295ContratoOcorrencia_Data != T001F2_A295ContratoOcorrencia_Data[0] ) || ( StringUtil.StrCmp(Z296ContratoOcorrencia_Descricao, T001F2_A296ContratoOcorrencia_Descricao[0]) != 0 ) || ( Z2027ContratoOcorrencia_NaoCnfCod != T001F2_A2027ContratoOcorrencia_NaoCnfCod[0] ) )
            {
               if ( Z295ContratoOcorrencia_Data != T001F2_A295ContratoOcorrencia_Data[0] )
               {
                  GXUtil.WriteLog("contratoocorrencia:[seudo value changed for attri]"+"ContratoOcorrencia_Data");
                  GXUtil.WriteLogRaw("Old: ",Z295ContratoOcorrencia_Data);
                  GXUtil.WriteLogRaw("Current: ",T001F2_A295ContratoOcorrencia_Data[0]);
               }
               if ( StringUtil.StrCmp(Z296ContratoOcorrencia_Descricao, T001F2_A296ContratoOcorrencia_Descricao[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoocorrencia:[seudo value changed for attri]"+"ContratoOcorrencia_Descricao");
                  GXUtil.WriteLogRaw("Old: ",Z296ContratoOcorrencia_Descricao);
                  GXUtil.WriteLogRaw("Current: ",T001F2_A296ContratoOcorrencia_Descricao[0]);
               }
               if ( Z2027ContratoOcorrencia_NaoCnfCod != T001F2_A2027ContratoOcorrencia_NaoCnfCod[0] )
               {
                  GXUtil.WriteLog("contratoocorrencia:[seudo value changed for attri]"+"ContratoOcorrencia_NaoCnfCod");
                  GXUtil.WriteLogRaw("Old: ",Z2027ContratoOcorrencia_NaoCnfCod);
                  GXUtil.WriteLogRaw("Current: ",T001F2_A2027ContratoOcorrencia_NaoCnfCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoOcorrencia"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1F50( )
      {
         BeforeValidate1F50( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1F50( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1F50( 0) ;
            CheckOptimisticConcurrency1F50( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1F50( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1F50( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001F13 */
                     pr_default.execute(11, new Object[] {A74Contrato_Codigo, A295ContratoOcorrencia_Data, A296ContratoOcorrencia_Descricao, n2027ContratoOcorrencia_NaoCnfCod, A2027ContratoOcorrencia_NaoCnfCod});
                     A294ContratoOcorrencia_Codigo = T001F13_A294ContratoOcorrencia_Codigo[0];
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoOcorrencia") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption1F0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1F50( ) ;
            }
            EndLevel1F50( ) ;
         }
         CloseExtendedTableCursors1F50( ) ;
      }

      protected void Update1F50( )
      {
         BeforeValidate1F50( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1F50( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1F50( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1F50( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1F50( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001F14 */
                     pr_default.execute(12, new Object[] {A74Contrato_Codigo, A295ContratoOcorrencia_Data, A296ContratoOcorrencia_Descricao, n2027ContratoOcorrencia_NaoCnfCod, A2027ContratoOcorrencia_NaoCnfCod, A294ContratoOcorrencia_Codigo});
                     pr_default.close(12);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoOcorrencia") ;
                     if ( (pr_default.getStatus(12) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoOcorrencia"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1F50( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1F50( ) ;
         }
         CloseExtendedTableCursors1F50( ) ;
      }

      protected void DeferredUpdate1F50( )
      {
      }

      protected void delete( )
      {
         BeforeValidate1F50( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1F50( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1F50( ) ;
            AfterConfirm1F50( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1F50( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001F15 */
                  pr_default.execute(13, new Object[] {A294ContratoOcorrencia_Codigo});
                  pr_default.close(13);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoOcorrencia") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode50 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel1F50( ) ;
         Gx_mode = sMode50;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls1F50( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
         if ( AnyError == 0 )
         {
            /* Using cursor T001F16 */
            pr_default.execute(14, new Object[] {A294ContratoOcorrencia_Codigo});
            if ( (pr_default.getStatus(14) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato Ocorrencia Notificacao"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(14);
         }
      }

      protected void EndLevel1F50( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1F50( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "ContratoOcorrencia");
            if ( AnyError == 0 )
            {
               ConfirmValues1F0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "ContratoOcorrencia");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1F50( )
      {
         /* Scan By routine */
         /* Using cursor T001F17 */
         pr_default.execute(15, new Object[] {A74Contrato_Codigo});
         RcdFound50 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound50 = 1;
            A294ContratoOcorrencia_Codigo = T001F17_A294ContratoOcorrencia_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1F50( )
      {
         /* Scan next routine */
         pr_default.readNext(15);
         RcdFound50 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound50 = 1;
            A294ContratoOcorrencia_Codigo = T001F17_A294ContratoOcorrencia_Codigo[0];
         }
      }

      protected void ScanEnd1F50( )
      {
         pr_default.close(15);
      }

      protected void AfterConfirm1F50( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1F50( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1F50( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1F50( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1F50( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1F50( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1F50( )
      {
         edtContrato_Numero_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Numero_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Numero_Enabled), 5, 0)));
         edtContrato_NumeroAta_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_NumeroAta_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_NumeroAta_Enabled), 5, 0)));
         edtContrato_Ano_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Ano_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Ano_Enabled), 5, 0)));
         edtContratada_PessoaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_PessoaNom_Enabled), 5, 0)));
         edtContratada_PessoaCNPJ_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaCNPJ_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_PessoaCNPJ_Enabled), 5, 0)));
         edtContratoOcorrencia_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrencia_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoOcorrencia_Data_Enabled), 5, 0)));
         edtContratoOcorrencia_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrencia_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoOcorrencia_Descricao_Enabled), 5, 0)));
         dynContratoOcorrencia_NaoCnfCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratoOcorrencia_NaoCnfCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContratoOcorrencia_NaoCnfCod.Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues1F0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205623551825");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoocorrencia.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoOcorrencia_Codigo) + "," + UrlEncode("" +A74Contrato_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z294ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z294ContratoOcorrencia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z295ContratoOcorrencia_Data", context.localUtil.DToC( Z295ContratoOcorrencia_Data, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z296ContratoOcorrencia_Descricao", Z296ContratoOcorrencia_Descricao);
         GxWebStd.gx_hidden_field( context, "Z2027ContratoOcorrencia_NaoCnfCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2027ContratoOcorrencia_NaoCnfCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N74Contrato_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N2027ContratoOcorrencia_NaoCnfCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2027ContratoOcorrencia_NaoCnfCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vCONTRATOOCORRENCIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoOcorrencia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOOCORRENCIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTRATOOCORRENCIA_NAOCNFCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Insert_ContratoOcorrencia_NaoCnfCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV14Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOOCORRENCIA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoOcorrencia_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ContratoOcorrencia";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A294ContratoOcorrencia_Codigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratoocorrencia:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("contratoocorrencia:[SendSecurityCheck value for]"+"ContratoOcorrencia_Codigo:"+context.localUtil.Format( (decimal)(A294ContratoOcorrencia_Codigo), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contratoocorrencia.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoOcorrencia_Codigo) + "," + UrlEncode("" +A74Contrato_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "ContratoOcorrencia" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Ocorr�ncia" ;
      }

      protected void InitializeNonKey1F50( )
      {
         A2027ContratoOcorrencia_NaoCnfCod = 0;
         n2027ContratoOcorrencia_NaoCnfCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2027ContratoOcorrencia_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2027ContratoOcorrencia_NaoCnfCod), 6, 0)));
         n2027ContratoOcorrencia_NaoCnfCod = ((0==A2027ContratoOcorrencia_NaoCnfCod) ? true : false);
         A295ContratoOcorrencia_Data = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A295ContratoOcorrencia_Data", context.localUtil.Format(A295ContratoOcorrencia_Data, "99/99/99"));
         A296ContratoOcorrencia_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A296ContratoOcorrencia_Descricao", A296ContratoOcorrencia_Descricao);
         Z295ContratoOcorrencia_Data = DateTime.MinValue;
         Z296ContratoOcorrencia_Descricao = "";
         Z2027ContratoOcorrencia_NaoCnfCod = 0;
      }

      protected void InitAll1F50( )
      {
         A294ContratoOcorrencia_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A294ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0)));
         InitializeNonKey1F50( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205623551843");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contratoocorrencia.js", "?20205623551844");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontrato_numero_Internalname = "TEXTBLOCKCONTRATO_NUMERO";
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO";
         lblTextblockcontrato_numeroata_Internalname = "TEXTBLOCKCONTRATO_NUMEROATA";
         edtContrato_NumeroAta_Internalname = "CONTRATO_NUMEROATA";
         lblTextblockcontrato_ano_Internalname = "TEXTBLOCKCONTRATO_ANO";
         edtContrato_Ano_Internalname = "CONTRATO_ANO";
         tblTablemergedcontrato_numero_Internalname = "TABLEMERGEDCONTRATO_NUMERO";
         lblTextblockcontratada_pessoanom_Internalname = "TEXTBLOCKCONTRATADA_PESSOANOM";
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM";
         lblTextblockcontratada_pessoacnpj_Internalname = "TEXTBLOCKCONTRATADA_PESSOACNPJ";
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ";
         tblTablemergedcontratada_pessoanom_Internalname = "TABLEMERGEDCONTRATADA_PESSOANOM";
         tblContrato_Internalname = "CONTRATO";
         grpUnnamedgroup1_Internalname = "UNNAMEDGROUP1";
         lblTextblockcontratoocorrencia_data_Internalname = "TEXTBLOCKCONTRATOOCORRENCIA_DATA";
         edtContratoOcorrencia_Data_Internalname = "CONTRATOOCORRENCIA_DATA";
         lblTextblockcontratoocorrencia_descricao_Internalname = "TEXTBLOCKCONTRATOOCORRENCIA_DESCRICAO";
         edtContratoOcorrencia_Descricao_Internalname = "CONTRATOOCORRENCIA_DESCRICAO";
         lblTextblockcontratoocorrencia_naocnfcod_Internalname = "TEXTBLOCKCONTRATOOCORRENCIA_NAOCNFCOD";
         dynContratoOcorrencia_NaoCnfCod_Internalname = "CONTRATOOCORRENCIA_NAOCNFCOD";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Ocorr�ncia";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contrato Ocorr�ncia";
         edtContrato_Ano_Jsonclick = "";
         edtContrato_Ano_Enabled = 0;
         edtContrato_NumeroAta_Jsonclick = "";
         edtContrato_NumeroAta_Enabled = 0;
         edtContrato_Numero_Jsonclick = "";
         edtContrato_Numero_Enabled = 0;
         edtContratada_PessoaCNPJ_Jsonclick = "";
         edtContratada_PessoaCNPJ_Enabled = 0;
         edtContratada_PessoaNom_Jsonclick = "";
         edtContratada_PessoaNom_Enabled = 0;
         dynContratoOcorrencia_NaoCnfCod_Jsonclick = "";
         dynContratoOcorrencia_NaoCnfCod.Enabled = 1;
         edtContratoOcorrencia_Descricao_Jsonclick = "";
         edtContratoOcorrencia_Descricao_Enabled = 1;
         edtContratoOcorrencia_Data_Jsonclick = "";
         edtContratoOcorrencia_Data_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLACONTRATOOCORRENCIA_NAOCNFCOD1F50( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLACONTRATOOCORRENCIA_NAOCNFCOD_data1F50( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXACONTRATOOCORRENCIA_NAOCNFCOD_html1F50( )
      {
         int gxdynajaxvalue ;
         GXDLACONTRATOOCORRENCIA_NAOCNFCOD_data1F50( ) ;
         gxdynajaxindex = 1;
         dynContratoOcorrencia_NaoCnfCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynContratoOcorrencia_NaoCnfCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLACONTRATOOCORRENCIA_NAOCNFCOD_data1F50( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor T001F18 */
         pr_default.execute(16);
         while ( (pr_default.getStatus(16) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T001F18_A2027ContratoOcorrencia_NaoCnfCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T001F18_A427NaoConformidade_Nome[0]));
            pr_default.readNext(16);
         }
         pr_default.close(16);
      }

      public void Valid_Contratoocorrencia_naocnfcod( GXCombobox dynGX_Parm1 )
      {
         dynContratoOcorrencia_NaoCnfCod = dynGX_Parm1;
         A2027ContratoOcorrencia_NaoCnfCod = (int)(NumberUtil.Val( dynContratoOcorrencia_NaoCnfCod.CurrentValue, "."));
         n2027ContratoOcorrencia_NaoCnfCod = false;
         /* Using cursor T001F19 */
         pr_default.execute(17, new Object[] {n2027ContratoOcorrencia_NaoCnfCod, A2027ContratoOcorrencia_NaoCnfCod});
         if ( (pr_default.getStatus(17) == 101) )
         {
            if ( ! ( (0==A2027ContratoOcorrencia_NaoCnfCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Ocorrencia_Nao Cnf Cod'.", "ForeignKeyNotFound", 1, "CONTRATOOCORRENCIA_NAOCNFCOD");
               AnyError = 1;
               GX_FocusControl = dynContratoOcorrencia_NaoCnfCod_Internalname;
            }
         }
         pr_default.close(17);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ContratoOcorrencia_Codigo',fld:'vCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E121F2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(17);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z295ContratoOcorrencia_Data = DateTime.MinValue;
         Z296ContratoOcorrencia_Descricao = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockcontratoocorrencia_data_Jsonclick = "";
         A295ContratoOcorrencia_Data = DateTime.MinValue;
         lblTextblockcontratoocorrencia_descricao_Jsonclick = "";
         A296ContratoOcorrencia_Descricao = "";
         lblTextblockcontratoocorrencia_naocnfcod_Jsonclick = "";
         lblTextblockcontrato_numero_Jsonclick = "";
         lblTextblockcontratada_pessoanom_Jsonclick = "";
         A41Contratada_PessoaNom = "";
         lblTextblockcontratada_pessoacnpj_Jsonclick = "";
         A42Contratada_PessoaCNPJ = "";
         A77Contrato_Numero = "";
         lblTextblockcontrato_numeroata_Jsonclick = "";
         A78Contrato_NumeroAta = "";
         lblTextblockcontrato_ano_Jsonclick = "";
         AV14Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode50 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z77Contrato_Numero = "";
         Z78Contrato_NumeroAta = "";
         Z41Contratada_PessoaNom = "";
         Z42Contratada_PessoaCNPJ = "";
         T001F4_A39Contratada_Codigo = new int[1] ;
         T001F4_A77Contrato_Numero = new String[] {""} ;
         T001F4_A78Contrato_NumeroAta = new String[] {""} ;
         T001F4_n78Contrato_NumeroAta = new bool[] {false} ;
         T001F4_A79Contrato_Ano = new short[1] ;
         T001F6_A40Contratada_PessoaCod = new int[1] ;
         T001F7_A41Contratada_PessoaNom = new String[] {""} ;
         T001F7_n41Contratada_PessoaNom = new bool[] {false} ;
         T001F7_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T001F7_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T001F8_A39Contratada_Codigo = new int[1] ;
         T001F8_A74Contrato_Codigo = new int[1] ;
         T001F8_A294ContratoOcorrencia_Codigo = new int[1] ;
         T001F8_A77Contrato_Numero = new String[] {""} ;
         T001F8_A78Contrato_NumeroAta = new String[] {""} ;
         T001F8_n78Contrato_NumeroAta = new bool[] {false} ;
         T001F8_A79Contrato_Ano = new short[1] ;
         T001F8_A41Contratada_PessoaNom = new String[] {""} ;
         T001F8_n41Contratada_PessoaNom = new bool[] {false} ;
         T001F8_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T001F8_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T001F8_A295ContratoOcorrencia_Data = new DateTime[] {DateTime.MinValue} ;
         T001F8_A296ContratoOcorrencia_Descricao = new String[] {""} ;
         T001F8_A2027ContratoOcorrencia_NaoCnfCod = new int[1] ;
         T001F8_n2027ContratoOcorrencia_NaoCnfCod = new bool[] {false} ;
         T001F8_A40Contratada_PessoaCod = new int[1] ;
         T001F5_A2027ContratoOcorrencia_NaoCnfCod = new int[1] ;
         T001F5_n2027ContratoOcorrencia_NaoCnfCod = new bool[] {false} ;
         T001F9_A2027ContratoOcorrencia_NaoCnfCod = new int[1] ;
         T001F9_n2027ContratoOcorrencia_NaoCnfCod = new bool[] {false} ;
         T001F10_A294ContratoOcorrencia_Codigo = new int[1] ;
         T001F3_A74Contrato_Codigo = new int[1] ;
         T001F3_A294ContratoOcorrencia_Codigo = new int[1] ;
         T001F3_A295ContratoOcorrencia_Data = new DateTime[] {DateTime.MinValue} ;
         T001F3_A296ContratoOcorrencia_Descricao = new String[] {""} ;
         T001F3_A2027ContratoOcorrencia_NaoCnfCod = new int[1] ;
         T001F3_n2027ContratoOcorrencia_NaoCnfCod = new bool[] {false} ;
         T001F11_A74Contrato_Codigo = new int[1] ;
         T001F11_A294ContratoOcorrencia_Codigo = new int[1] ;
         T001F12_A74Contrato_Codigo = new int[1] ;
         T001F12_A294ContratoOcorrencia_Codigo = new int[1] ;
         T001F2_A74Contrato_Codigo = new int[1] ;
         T001F2_A294ContratoOcorrencia_Codigo = new int[1] ;
         T001F2_A295ContratoOcorrencia_Data = new DateTime[] {DateTime.MinValue} ;
         T001F2_A296ContratoOcorrencia_Descricao = new String[] {""} ;
         T001F2_A2027ContratoOcorrencia_NaoCnfCod = new int[1] ;
         T001F2_n2027ContratoOcorrencia_NaoCnfCod = new bool[] {false} ;
         T001F13_A294ContratoOcorrencia_Codigo = new int[1] ;
         T001F16_A297ContratoOcorrenciaNotificacao_Codigo = new int[1] ;
         T001F17_A294ContratoOcorrencia_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T001F18_A2027ContratoOcorrencia_NaoCnfCod = new int[1] ;
         T001F18_n2027ContratoOcorrencia_NaoCnfCod = new bool[] {false} ;
         T001F18_A427NaoConformidade_Nome = new String[] {""} ;
         T001F18_n427NaoConformidade_Nome = new bool[] {false} ;
         T001F19_A2027ContratoOcorrencia_NaoCnfCod = new int[1] ;
         T001F19_n2027ContratoOcorrencia_NaoCnfCod = new bool[] {false} ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoocorrencia__default(),
            new Object[][] {
                new Object[] {
               T001F2_A74Contrato_Codigo, T001F2_A294ContratoOcorrencia_Codigo, T001F2_A295ContratoOcorrencia_Data, T001F2_A296ContratoOcorrencia_Descricao, T001F2_A2027ContratoOcorrencia_NaoCnfCod, T001F2_n2027ContratoOcorrencia_NaoCnfCod
               }
               , new Object[] {
               T001F3_A74Contrato_Codigo, T001F3_A294ContratoOcorrencia_Codigo, T001F3_A295ContratoOcorrencia_Data, T001F3_A296ContratoOcorrencia_Descricao, T001F3_A2027ContratoOcorrencia_NaoCnfCod, T001F3_n2027ContratoOcorrencia_NaoCnfCod
               }
               , new Object[] {
               T001F4_A39Contratada_Codigo, T001F4_A77Contrato_Numero, T001F4_A78Contrato_NumeroAta, T001F4_n78Contrato_NumeroAta, T001F4_A79Contrato_Ano
               }
               , new Object[] {
               T001F5_A2027ContratoOcorrencia_NaoCnfCod
               }
               , new Object[] {
               T001F6_A40Contratada_PessoaCod
               }
               , new Object[] {
               T001F7_A41Contratada_PessoaNom, T001F7_n41Contratada_PessoaNom, T001F7_A42Contratada_PessoaCNPJ, T001F7_n42Contratada_PessoaCNPJ
               }
               , new Object[] {
               T001F8_A39Contratada_Codigo, T001F8_A74Contrato_Codigo, T001F8_A294ContratoOcorrencia_Codigo, T001F8_A77Contrato_Numero, T001F8_A78Contrato_NumeroAta, T001F8_n78Contrato_NumeroAta, T001F8_A79Contrato_Ano, T001F8_A41Contratada_PessoaNom, T001F8_n41Contratada_PessoaNom, T001F8_A42Contratada_PessoaCNPJ,
               T001F8_n42Contratada_PessoaCNPJ, T001F8_A295ContratoOcorrencia_Data, T001F8_A296ContratoOcorrencia_Descricao, T001F8_A2027ContratoOcorrencia_NaoCnfCod, T001F8_n2027ContratoOcorrencia_NaoCnfCod, T001F8_A40Contratada_PessoaCod
               }
               , new Object[] {
               T001F9_A2027ContratoOcorrencia_NaoCnfCod
               }
               , new Object[] {
               T001F10_A294ContratoOcorrencia_Codigo
               }
               , new Object[] {
               T001F11_A74Contrato_Codigo, T001F11_A294ContratoOcorrencia_Codigo
               }
               , new Object[] {
               T001F12_A74Contrato_Codigo, T001F12_A294ContratoOcorrencia_Codigo
               }
               , new Object[] {
               T001F13_A294ContratoOcorrencia_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001F16_A297ContratoOcorrenciaNotificacao_Codigo
               }
               , new Object[] {
               T001F17_A294ContratoOcorrencia_Codigo
               }
               , new Object[] {
               T001F18_A2027ContratoOcorrencia_NaoCnfCod, T001F18_A427NaoConformidade_Nome, T001F18_n427NaoConformidade_Nome
               }
               , new Object[] {
               T001F19_A2027ContratoOcorrencia_NaoCnfCod
               }
            }
         );
         N74Contrato_Codigo = 0;
         Z74Contrato_Codigo = 0;
         A74Contrato_Codigo = 0;
         AV14Pgmname = "ContratoOcorrencia";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A79Contrato_Ano ;
      private short RcdFound50 ;
      private short GX_JID ;
      private short Z79Contrato_Ano ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7ContratoOcorrencia_Codigo ;
      private int wcpOA74Contrato_Codigo ;
      private int Z294ContratoOcorrencia_Codigo ;
      private int Z2027ContratoOcorrencia_NaoCnfCod ;
      private int N74Contrato_Codigo ;
      private int N2027ContratoOcorrencia_NaoCnfCod ;
      private int A2027ContratoOcorrencia_NaoCnfCod ;
      private int AV7ContratoOcorrencia_Codigo ;
      private int A74Contrato_Codigo ;
      private int trnEnded ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtContratoOcorrencia_Data_Enabled ;
      private int edtContratoOcorrencia_Descricao_Enabled ;
      private int edtContratada_PessoaNom_Enabled ;
      private int edtContratada_PessoaCNPJ_Enabled ;
      private int edtContrato_Numero_Enabled ;
      private int edtContrato_NumeroAta_Enabled ;
      private int edtContrato_Ano_Enabled ;
      private int A294ContratoOcorrencia_Codigo ;
      private int AV11Insert_Contrato_Codigo ;
      private int AV13Insert_ContratoOcorrencia_NaoCnfCod ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int AV15GXV1 ;
      private int Z74Contrato_Codigo ;
      private int Z39Contratada_Codigo ;
      private int Z40Contratada_PessoaCod ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContratoOcorrencia_Data_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String grpUnnamedgroup1_Internalname ;
      private String lblTextblockcontratoocorrencia_data_Internalname ;
      private String lblTextblockcontratoocorrencia_data_Jsonclick ;
      private String edtContratoOcorrencia_Data_Jsonclick ;
      private String lblTextblockcontratoocorrencia_descricao_Internalname ;
      private String lblTextblockcontratoocorrencia_descricao_Jsonclick ;
      private String edtContratoOcorrencia_Descricao_Internalname ;
      private String edtContratoOcorrencia_Descricao_Jsonclick ;
      private String lblTextblockcontratoocorrencia_naocnfcod_Internalname ;
      private String lblTextblockcontratoocorrencia_naocnfcod_Jsonclick ;
      private String dynContratoOcorrencia_NaoCnfCod_Internalname ;
      private String dynContratoOcorrencia_NaoCnfCod_Jsonclick ;
      private String tblContrato_Internalname ;
      private String lblTextblockcontrato_numero_Internalname ;
      private String lblTextblockcontrato_numero_Jsonclick ;
      private String lblTextblockcontratada_pessoanom_Internalname ;
      private String lblTextblockcontratada_pessoanom_Jsonclick ;
      private String tblTablemergedcontratada_pessoanom_Internalname ;
      private String edtContratada_PessoaNom_Internalname ;
      private String A41Contratada_PessoaNom ;
      private String edtContratada_PessoaNom_Jsonclick ;
      private String lblTextblockcontratada_pessoacnpj_Internalname ;
      private String lblTextblockcontratada_pessoacnpj_Jsonclick ;
      private String edtContratada_PessoaCNPJ_Internalname ;
      private String edtContratada_PessoaCNPJ_Jsonclick ;
      private String tblTablemergedcontrato_numero_Internalname ;
      private String edtContrato_Numero_Internalname ;
      private String A77Contrato_Numero ;
      private String edtContrato_Numero_Jsonclick ;
      private String lblTextblockcontrato_numeroata_Internalname ;
      private String lblTextblockcontrato_numeroata_Jsonclick ;
      private String edtContrato_NumeroAta_Internalname ;
      private String A78Contrato_NumeroAta ;
      private String edtContrato_NumeroAta_Jsonclick ;
      private String lblTextblockcontrato_ano_Internalname ;
      private String lblTextblockcontrato_ano_Jsonclick ;
      private String edtContrato_Ano_Internalname ;
      private String edtContrato_Ano_Jsonclick ;
      private String AV14Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode50 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z77Contrato_Numero ;
      private String Z78Contrato_NumeroAta ;
      private String Z41Contratada_PessoaNom ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private DateTime Z295ContratoOcorrencia_Data ;
      private DateTime A295ContratoOcorrencia_Data ;
      private bool entryPointCalled ;
      private bool n2027ContratoOcorrencia_NaoCnfCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n78Contrato_NumeroAta ;
      private bool n41Contratada_PessoaNom ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private String Z296ContratoOcorrencia_Descricao ;
      private String A296ContratoOcorrencia_Descricao ;
      private String A42Contratada_PessoaCNPJ ;
      private String Z42Contratada_PessoaCNPJ ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP2_Contrato_Codigo ;
      private GXCombobox dynContratoOcorrencia_NaoCnfCod ;
      private IDataStoreProvider pr_default ;
      private int[] T001F4_A39Contratada_Codigo ;
      private String[] T001F4_A77Contrato_Numero ;
      private String[] T001F4_A78Contrato_NumeroAta ;
      private bool[] T001F4_n78Contrato_NumeroAta ;
      private short[] T001F4_A79Contrato_Ano ;
      private int[] T001F6_A40Contratada_PessoaCod ;
      private String[] T001F7_A41Contratada_PessoaNom ;
      private bool[] T001F7_n41Contratada_PessoaNom ;
      private String[] T001F7_A42Contratada_PessoaCNPJ ;
      private bool[] T001F7_n42Contratada_PessoaCNPJ ;
      private int[] T001F8_A39Contratada_Codigo ;
      private int[] T001F8_A74Contrato_Codigo ;
      private int[] T001F8_A294ContratoOcorrencia_Codigo ;
      private String[] T001F8_A77Contrato_Numero ;
      private String[] T001F8_A78Contrato_NumeroAta ;
      private bool[] T001F8_n78Contrato_NumeroAta ;
      private short[] T001F8_A79Contrato_Ano ;
      private String[] T001F8_A41Contratada_PessoaNom ;
      private bool[] T001F8_n41Contratada_PessoaNom ;
      private String[] T001F8_A42Contratada_PessoaCNPJ ;
      private bool[] T001F8_n42Contratada_PessoaCNPJ ;
      private DateTime[] T001F8_A295ContratoOcorrencia_Data ;
      private String[] T001F8_A296ContratoOcorrencia_Descricao ;
      private int[] T001F8_A2027ContratoOcorrencia_NaoCnfCod ;
      private bool[] T001F8_n2027ContratoOcorrencia_NaoCnfCod ;
      private int[] T001F8_A40Contratada_PessoaCod ;
      private int[] T001F5_A2027ContratoOcorrencia_NaoCnfCod ;
      private bool[] T001F5_n2027ContratoOcorrencia_NaoCnfCod ;
      private int[] T001F9_A2027ContratoOcorrencia_NaoCnfCod ;
      private bool[] T001F9_n2027ContratoOcorrencia_NaoCnfCod ;
      private int[] T001F10_A294ContratoOcorrencia_Codigo ;
      private int[] T001F3_A74Contrato_Codigo ;
      private int[] T001F3_A294ContratoOcorrencia_Codigo ;
      private DateTime[] T001F3_A295ContratoOcorrencia_Data ;
      private String[] T001F3_A296ContratoOcorrencia_Descricao ;
      private int[] T001F3_A2027ContratoOcorrencia_NaoCnfCod ;
      private bool[] T001F3_n2027ContratoOcorrencia_NaoCnfCod ;
      private int[] T001F11_A74Contrato_Codigo ;
      private int[] T001F11_A294ContratoOcorrencia_Codigo ;
      private int[] T001F12_A74Contrato_Codigo ;
      private int[] T001F12_A294ContratoOcorrencia_Codigo ;
      private int[] T001F2_A74Contrato_Codigo ;
      private int[] T001F2_A294ContratoOcorrencia_Codigo ;
      private DateTime[] T001F2_A295ContratoOcorrencia_Data ;
      private String[] T001F2_A296ContratoOcorrencia_Descricao ;
      private int[] T001F2_A2027ContratoOcorrencia_NaoCnfCod ;
      private bool[] T001F2_n2027ContratoOcorrencia_NaoCnfCod ;
      private int[] T001F13_A294ContratoOcorrencia_Codigo ;
      private int[] T001F16_A297ContratoOcorrenciaNotificacao_Codigo ;
      private int[] T001F17_A294ContratoOcorrencia_Codigo ;
      private int[] T001F18_A2027ContratoOcorrencia_NaoCnfCod ;
      private bool[] T001F18_n2027ContratoOcorrencia_NaoCnfCod ;
      private String[] T001F18_A427NaoConformidade_Nome ;
      private bool[] T001F18_n427NaoConformidade_Nome ;
      private int[] T001F19_A2027ContratoOcorrencia_NaoCnfCod ;
      private bool[] T001F19_n2027ContratoOcorrencia_NaoCnfCod ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class contratoocorrencia__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT001F4 ;
          prmT001F4 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001F6 ;
          prmT001F6 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001F7 ;
          prmT001F7 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001F8 ;
          prmT001F8 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoOcorrencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001F5 ;
          prmT001F5 = new Object[] {
          new Object[] {"@ContratoOcorrencia_NaoCnfCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001F9 ;
          prmT001F9 = new Object[] {
          new Object[] {"@ContratoOcorrencia_NaoCnfCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001F10 ;
          prmT001F10 = new Object[] {
          new Object[] {"@ContratoOcorrencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001F3 ;
          prmT001F3 = new Object[] {
          new Object[] {"@ContratoOcorrencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001F11 ;
          prmT001F11 = new Object[] {
          new Object[] {"@ContratoOcorrencia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001F12 ;
          prmT001F12 = new Object[] {
          new Object[] {"@ContratoOcorrencia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001F2 ;
          prmT001F2 = new Object[] {
          new Object[] {"@ContratoOcorrencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001F13 ;
          prmT001F13 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoOcorrencia_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoOcorrencia_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ContratoOcorrencia_NaoCnfCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001F14 ;
          prmT001F14 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoOcorrencia_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoOcorrencia_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ContratoOcorrencia_NaoCnfCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoOcorrencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001F15 ;
          prmT001F15 = new Object[] {
          new Object[] {"@ContratoOcorrencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001F16 ;
          prmT001F16 = new Object[] {
          new Object[] {"@ContratoOcorrencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001F17 ;
          prmT001F17 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001F18 ;
          prmT001F18 = new Object[] {
          } ;
          Object[] prmT001F19 ;
          prmT001F19 = new Object[] {
          new Object[] {"@ContratoOcorrencia_NaoCnfCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T001F2", "SELECT [Contrato_Codigo], [ContratoOcorrencia_Codigo], [ContratoOcorrencia_Data], [ContratoOcorrencia_Descricao], [ContratoOcorrencia_NaoCnfCod] AS ContratoOcorrencia_NaoCnfCod FROM [ContratoOcorrencia] WITH (UPDLOCK) WHERE [ContratoOcorrencia_Codigo] = @ContratoOcorrencia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001F2,1,0,true,false )
             ,new CursorDef("T001F3", "SELECT [Contrato_Codigo], [ContratoOcorrencia_Codigo], [ContratoOcorrencia_Data], [ContratoOcorrencia_Descricao], [ContratoOcorrencia_NaoCnfCod] AS ContratoOcorrencia_NaoCnfCod FROM [ContratoOcorrencia] WITH (NOLOCK) WHERE [ContratoOcorrencia_Codigo] = @ContratoOcorrencia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001F3,1,0,true,false )
             ,new CursorDef("T001F4", "SELECT [Contratada_Codigo], [Contrato_Numero], [Contrato_NumeroAta], [Contrato_Ano] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001F4,1,0,true,false )
             ,new CursorDef("T001F5", "SELECT [NaoConformidade_Codigo] AS ContratoOcorrencia_NaoCnfCod FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @ContratoOcorrencia_NaoCnfCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001F5,1,0,true,false )
             ,new CursorDef("T001F6", "SELECT [Contratada_PessoaCod] AS Contratada_PessoaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001F6,1,0,true,false )
             ,new CursorDef("T001F7", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom, [Pessoa_Docto] AS Contratada_PessoaCNPJ FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001F7,1,0,true,false )
             ,new CursorDef("T001F8", "SELECT T2.[Contratada_Codigo], TM1.[Contrato_Codigo], TM1.[ContratoOcorrencia_Codigo], T2.[Contrato_Numero], T2.[Contrato_NumeroAta], T2.[Contrato_Ano], T4.[Pessoa_Nome] AS Contratada_PessoaNom, T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, TM1.[ContratoOcorrencia_Data], TM1.[ContratoOcorrencia_Descricao], TM1.[ContratoOcorrencia_NaoCnfCod] AS ContratoOcorrencia_NaoCnfCod, T3.[Contratada_PessoaCod] AS Contratada_PessoaCod FROM ((([ContratoOcorrencia] TM1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = TM1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod]) WHERE TM1.[Contrato_Codigo] = @Contrato_Codigo and TM1.[ContratoOcorrencia_Codigo] = @ContratoOcorrencia_Codigo ORDER BY TM1.[ContratoOcorrencia_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001F8,100,0,true,false )
             ,new CursorDef("T001F9", "SELECT [NaoConformidade_Codigo] AS ContratoOcorrencia_NaoCnfCod FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @ContratoOcorrencia_NaoCnfCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001F9,1,0,true,false )
             ,new CursorDef("T001F10", "SELECT [ContratoOcorrencia_Codigo] FROM [ContratoOcorrencia] WITH (NOLOCK) WHERE [ContratoOcorrencia_Codigo] = @ContratoOcorrencia_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001F10,1,0,true,false )
             ,new CursorDef("T001F11", "SELECT TOP 1 [Contrato_Codigo], [ContratoOcorrencia_Codigo] FROM [ContratoOcorrencia] WITH (NOLOCK) WHERE ( [ContratoOcorrencia_Codigo] > @ContratoOcorrencia_Codigo) and [Contrato_Codigo] = @Contrato_Codigo ORDER BY [ContratoOcorrencia_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001F11,1,0,true,true )
             ,new CursorDef("T001F12", "SELECT TOP 1 [Contrato_Codigo], [ContratoOcorrencia_Codigo] FROM [ContratoOcorrencia] WITH (NOLOCK) WHERE ( [ContratoOcorrencia_Codigo] < @ContratoOcorrencia_Codigo) and [Contrato_Codigo] = @Contrato_Codigo ORDER BY [ContratoOcorrencia_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001F12,1,0,true,true )
             ,new CursorDef("T001F13", "INSERT INTO [ContratoOcorrencia]([Contrato_Codigo], [ContratoOcorrencia_Data], [ContratoOcorrencia_Descricao], [ContratoOcorrencia_NaoCnfCod]) VALUES(@Contrato_Codigo, @ContratoOcorrencia_Data, @ContratoOcorrencia_Descricao, @ContratoOcorrencia_NaoCnfCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT001F13)
             ,new CursorDef("T001F14", "UPDATE [ContratoOcorrencia] SET [Contrato_Codigo]=@Contrato_Codigo, [ContratoOcorrencia_Data]=@ContratoOcorrencia_Data, [ContratoOcorrencia_Descricao]=@ContratoOcorrencia_Descricao, [ContratoOcorrencia_NaoCnfCod]=@ContratoOcorrencia_NaoCnfCod  WHERE [ContratoOcorrencia_Codigo] = @ContratoOcorrencia_Codigo", GxErrorMask.GX_NOMASK,prmT001F14)
             ,new CursorDef("T001F15", "DELETE FROM [ContratoOcorrencia]  WHERE [ContratoOcorrencia_Codigo] = @ContratoOcorrencia_Codigo", GxErrorMask.GX_NOMASK,prmT001F15)
             ,new CursorDef("T001F16", "SELECT TOP 1 [ContratoOcorrenciaNotificacao_Codigo] FROM [ContratoOcorrenciaNotificacao] WITH (NOLOCK) WHERE [ContratoOcorrencia_Codigo] = @ContratoOcorrencia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001F16,1,0,true,true )
             ,new CursorDef("T001F17", "SELECT [ContratoOcorrencia_Codigo] FROM [ContratoOcorrencia] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [ContratoOcorrencia_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001F17,100,0,true,false )
             ,new CursorDef("T001F18", "SELECT [NaoConformidade_Codigo] AS ContratoOcorrencia_NaoCnfCod, [NaoConformidade_Nome] FROM [NaoConformidade] WITH (NOLOCK) ORDER BY [NaoConformidade_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT001F18,0,0,true,false )
             ,new CursorDef("T001F19", "SELECT [NaoConformidade_Codigo] AS ContratoOcorrencia_NaoCnfCod FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @ContratoOcorrencia_NaoCnfCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001F19,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((short[]) buf[6])[0] = rslt.getShort(6) ;
                ((String[]) buf[7])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((String[]) buf[9])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[11])[0] = rslt.getGXDate(9) ;
                ((String[]) buf[12])[0] = rslt.getVarchar(10) ;
                ((int[]) buf[13])[0] = rslt.getInt(11) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(11);
                ((int[]) buf[15])[0] = rslt.getInt(12) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[4]);
                }
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[4]);
                }
                stmt.SetParameter(5, (int)parms[5]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
