/*
               File: GetContagemItemContagemItemAtributosFSoftwareWCFilterData
        Description: Get Contagem Item Contagem Item Atributos FSoftware WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:54:47.93
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getcontagemitemcontagemitematributosfsoftwarewcfilterdata : GXProcedure
   {
      public getcontagemitemcontagemitematributosfsoftwarewcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getcontagemitemcontagemitematributosfsoftwarewcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV22DDOName = aP0_DDOName;
         this.AV20SearchTxt = aP1_SearchTxt;
         this.AV21SearchTxtTo = aP2_SearchTxtTo;
         this.AV26OptionsJson = "" ;
         this.AV29OptionsDescJson = "" ;
         this.AV31OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV22DDOName = aP0_DDOName;
         this.AV20SearchTxt = aP1_SearchTxt;
         this.AV21SearchTxtTo = aP2_SearchTxtTo;
         this.AV26OptionsJson = "" ;
         this.AV29OptionsDescJson = "" ;
         this.AV31OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
         return AV31OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getcontagemitemcontagemitematributosfsoftwarewcfilterdata objgetcontagemitemcontagemitematributosfsoftwarewcfilterdata;
         objgetcontagemitemcontagemitematributosfsoftwarewcfilterdata = new getcontagemitemcontagemitematributosfsoftwarewcfilterdata();
         objgetcontagemitemcontagemitematributosfsoftwarewcfilterdata.AV22DDOName = aP0_DDOName;
         objgetcontagemitemcontagemitematributosfsoftwarewcfilterdata.AV20SearchTxt = aP1_SearchTxt;
         objgetcontagemitemcontagemitematributosfsoftwarewcfilterdata.AV21SearchTxtTo = aP2_SearchTxtTo;
         objgetcontagemitemcontagemitematributosfsoftwarewcfilterdata.AV26OptionsJson = "" ;
         objgetcontagemitemcontagemitematributosfsoftwarewcfilterdata.AV29OptionsDescJson = "" ;
         objgetcontagemitemcontagemitematributosfsoftwarewcfilterdata.AV31OptionIndexesJson = "" ;
         objgetcontagemitemcontagemitematributosfsoftwarewcfilterdata.context.SetSubmitInitialConfig(context);
         objgetcontagemitemcontagemitematributosfsoftwarewcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetcontagemitemcontagemitematributosfsoftwarewcfilterdata);
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getcontagemitemcontagemitematributosfsoftwarewcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV25Options = (IGxCollection)(new GxSimpleCollection());
         AV28OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV30OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV22DDOName), "DDO_TABELA_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADTABELA_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV26OptionsJson = AV25Options.ToJSonString(false);
         AV29OptionsDescJson = AV28OptionsDesc.ToJSonString(false);
         AV31OptionIndexesJson = AV30OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV33Session.Get("ContagemItemContagemItemAtributosFSoftwareWCGridState"), "") == 0 )
         {
            AV35GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ContagemItemContagemItemAtributosFSoftwareWCGridState"), "");
         }
         else
         {
            AV35GridState.FromXml(AV33Session.Get("ContagemItemContagemItemAtributosFSoftwareWCGridState"), "");
         }
         AV52GXV1 = 1;
         while ( AV52GXV1 <= AV35GridState.gxTpr_Filtervalues.Count )
         {
            AV36GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV35GridState.gxTpr_Filtervalues.Item(AV52GXV1));
            if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFTABELA_NOME") == 0 )
            {
               AV18TFTabela_Nome = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFTABELA_NOME_SEL") == 0 )
            {
               AV19TFTabela_Nome_Sel = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "PARM_&CONTAGEMITEM_LANCAMENTO") == 0 )
            {
               AV49ContagemItem_Lancamento = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
            }
            AV52GXV1 = (int)(AV52GXV1+1);
         }
         if ( AV35GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV37GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV35GridState.gxTpr_Dynamicfilters.Item(1));
            AV38DynamicFiltersSelector1 = AV37GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "CONTAGEMITEM_ATRIBUTOSNOM") == 0 )
            {
               AV39DynamicFiltersOperator1 = AV37GridStateDynamicFilter.gxTpr_Operator;
               AV40ContagemItem_AtributosNom1 = AV37GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV35GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV41DynamicFiltersEnabled2 = true;
               AV37GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV35GridState.gxTpr_Dynamicfilters.Item(2));
               AV42DynamicFiltersSelector2 = AV37GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV42DynamicFiltersSelector2, "CONTAGEMITEM_ATRIBUTOSNOM") == 0 )
               {
                  AV43DynamicFiltersOperator2 = AV37GridStateDynamicFilter.gxTpr_Operator;
                  AV44ContagemItem_AtributosNom2 = AV37GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV35GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV45DynamicFiltersEnabled3 = true;
                  AV37GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV35GridState.gxTpr_Dynamicfilters.Item(3));
                  AV46DynamicFiltersSelector3 = AV37GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "CONTAGEMITEM_ATRIBUTOSNOM") == 0 )
                  {
                     AV47DynamicFiltersOperator3 = AV37GridStateDynamicFilter.gxTpr_Operator;
                     AV48ContagemItem_AtributosNom3 = AV37GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADTABELA_NOMEOPTIONS' Routine */
         AV18TFTabela_Nome = AV20SearchTxt;
         AV19TFTabela_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV38DynamicFiltersSelector1 ,
                                              AV39DynamicFiltersOperator1 ,
                                              AV40ContagemItem_AtributosNom1 ,
                                              AV41DynamicFiltersEnabled2 ,
                                              AV42DynamicFiltersSelector2 ,
                                              AV43DynamicFiltersOperator2 ,
                                              AV44ContagemItem_AtributosNom2 ,
                                              AV45DynamicFiltersEnabled3 ,
                                              AV46DynamicFiltersSelector3 ,
                                              AV47DynamicFiltersOperator3 ,
                                              AV48ContagemItem_AtributosNom3 ,
                                              AV19TFTabela_Nome_Sel ,
                                              AV18TFTabela_Nome ,
                                              A380ContagemItem_AtributosNom ,
                                              A173Tabela_Nome ,
                                              AV49ContagemItem_Lancamento ,
                                              A224ContagemItem_Lancamento },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV40ContagemItem_AtributosNom1 = StringUtil.PadR( StringUtil.RTrim( AV40ContagemItem_AtributosNom1), 50, "%");
         lV40ContagemItem_AtributosNom1 = StringUtil.PadR( StringUtil.RTrim( AV40ContagemItem_AtributosNom1), 50, "%");
         lV44ContagemItem_AtributosNom2 = StringUtil.PadR( StringUtil.RTrim( AV44ContagemItem_AtributosNom2), 50, "%");
         lV44ContagemItem_AtributosNom2 = StringUtil.PadR( StringUtil.RTrim( AV44ContagemItem_AtributosNom2), 50, "%");
         lV48ContagemItem_AtributosNom3 = StringUtil.PadR( StringUtil.RTrim( AV48ContagemItem_AtributosNom3), 50, "%");
         lV48ContagemItem_AtributosNom3 = StringUtil.PadR( StringUtil.RTrim( AV48ContagemItem_AtributosNom3), 50, "%");
         /* Using cursor P00SK2 */
         pr_default.execute(0, new Object[] {AV49ContagemItem_Lancamento, lV40ContagemItem_AtributosNom1, lV40ContagemItem_AtributosNom1, lV44ContagemItem_AtributosNom2, lV44ContagemItem_AtributosNom2, lV48ContagemItem_AtributosNom3, lV48ContagemItem_AtributosNom3});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKSK2 = false;
            A379ContagemItem_AtributosCod = P00SK2_A379ContagemItem_AtributosCod[0];
            A224ContagemItem_Lancamento = P00SK2_A224ContagemItem_Lancamento[0];
            A380ContagemItem_AtributosNom = P00SK2_A380ContagemItem_AtributosNom[0];
            n380ContagemItem_AtributosNom = P00SK2_n380ContagemItem_AtributosNom[0];
            A380ContagemItem_AtributosNom = P00SK2_A380ContagemItem_AtributosNom[0];
            n380ContagemItem_AtributosNom = P00SK2_n380ContagemItem_AtributosNom[0];
            AV32count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00SK2_A224ContagemItem_Lancamento[0] == A224ContagemItem_Lancamento ) )
            {
               BRKSK2 = false;
               A379ContagemItem_AtributosCod = P00SK2_A379ContagemItem_AtributosCod[0];
               AV32count = (long)(AV32count+1);
               BRKSK2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A173Tabela_Nome)) )
            {
               AV24Option = A173Tabela_Nome;
               AV25Options.Add(AV24Option, 0);
               AV30OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV32count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV25Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKSK2 )
            {
               BRKSK2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV25Options = new GxSimpleCollection();
         AV28OptionsDesc = new GxSimpleCollection();
         AV30OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV33Session = context.GetSession();
         AV35GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV36GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV18TFTabela_Nome = "";
         AV19TFTabela_Nome_Sel = "";
         AV37GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV38DynamicFiltersSelector1 = "";
         AV40ContagemItem_AtributosNom1 = "";
         AV42DynamicFiltersSelector2 = "";
         AV44ContagemItem_AtributosNom2 = "";
         AV46DynamicFiltersSelector3 = "";
         AV48ContagemItem_AtributosNom3 = "";
         scmdbuf = "";
         lV40ContagemItem_AtributosNom1 = "";
         lV44ContagemItem_AtributosNom2 = "";
         lV48ContagemItem_AtributosNom3 = "";
         A380ContagemItem_AtributosNom = "";
         A173Tabela_Nome = "";
         P00SK2_A379ContagemItem_AtributosCod = new int[1] ;
         P00SK2_A224ContagemItem_Lancamento = new int[1] ;
         P00SK2_A380ContagemItem_AtributosNom = new String[] {""} ;
         P00SK2_n380ContagemItem_AtributosNom = new bool[] {false} ;
         AV24Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getcontagemitemcontagemitematributosfsoftwarewcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00SK2_A379ContagemItem_AtributosCod, P00SK2_A224ContagemItem_Lancamento, P00SK2_A380ContagemItem_AtributosNom, P00SK2_n380ContagemItem_AtributosNom
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV39DynamicFiltersOperator1 ;
      private short AV43DynamicFiltersOperator2 ;
      private short AV47DynamicFiltersOperator3 ;
      private int AV52GXV1 ;
      private int AV49ContagemItem_Lancamento ;
      private int A224ContagemItem_Lancamento ;
      private int A379ContagemItem_AtributosCod ;
      private long AV32count ;
      private String AV18TFTabela_Nome ;
      private String AV19TFTabela_Nome_Sel ;
      private String AV40ContagemItem_AtributosNom1 ;
      private String AV44ContagemItem_AtributosNom2 ;
      private String AV48ContagemItem_AtributosNom3 ;
      private String scmdbuf ;
      private String lV40ContagemItem_AtributosNom1 ;
      private String lV44ContagemItem_AtributosNom2 ;
      private String lV48ContagemItem_AtributosNom3 ;
      private String A380ContagemItem_AtributosNom ;
      private String A173Tabela_Nome ;
      private bool returnInSub ;
      private bool AV41DynamicFiltersEnabled2 ;
      private bool AV45DynamicFiltersEnabled3 ;
      private bool BRKSK2 ;
      private bool n380ContagemItem_AtributosNom ;
      private String AV31OptionIndexesJson ;
      private String AV26OptionsJson ;
      private String AV29OptionsDescJson ;
      private String AV22DDOName ;
      private String AV20SearchTxt ;
      private String AV21SearchTxtTo ;
      private String AV38DynamicFiltersSelector1 ;
      private String AV42DynamicFiltersSelector2 ;
      private String AV46DynamicFiltersSelector3 ;
      private String AV24Option ;
      private IGxSession AV33Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00SK2_A379ContagemItem_AtributosCod ;
      private int[] P00SK2_A224ContagemItem_Lancamento ;
      private String[] P00SK2_A380ContagemItem_AtributosNom ;
      private bool[] P00SK2_n380ContagemItem_AtributosNom ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV35GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV36GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV37GridStateDynamicFilter ;
   }

   public class getcontagemitemcontagemitematributosfsoftwarewcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00SK2( IGxContext context ,
                                             String AV38DynamicFiltersSelector1 ,
                                             short AV39DynamicFiltersOperator1 ,
                                             String AV40ContagemItem_AtributosNom1 ,
                                             bool AV41DynamicFiltersEnabled2 ,
                                             String AV42DynamicFiltersSelector2 ,
                                             short AV43DynamicFiltersOperator2 ,
                                             String AV44ContagemItem_AtributosNom2 ,
                                             bool AV45DynamicFiltersEnabled3 ,
                                             String AV46DynamicFiltersSelector3 ,
                                             short AV47DynamicFiltersOperator3 ,
                                             String AV48ContagemItem_AtributosNom3 ,
                                             String AV19TFTabela_Nome_Sel ,
                                             String AV18TFTabela_Nome ,
                                             String A380ContagemItem_AtributosNom ,
                                             String A173Tabela_Nome ,
                                             int AV49ContagemItem_Lancamento ,
                                             int A224ContagemItem_Lancamento )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [7] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemItem_AtributosCod] AS ContagemItem_AtributosCod, T1.[ContagemItem_Lancamento], T2.[Atributos_Nome] AS ContagemItem_AtributosNom FROM ([ContagemItemAtributosFSoftware] T1 WITH (NOLOCK) INNER JOIN [Atributos] T2 WITH (NOLOCK) ON T2.[Atributos_Codigo] = T1.[ContagemItem_AtributosCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemItem_Lancamento] = @AV49ContagemItem_Lancamento)";
         if ( ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "CONTAGEMITEM_ATRIBUTOSNOM") == 0 ) && ( ( AV39DynamicFiltersOperator1 == 0 ) || ( AV39DynamicFiltersOperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40ContagemItem_AtributosNom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV40ContagemItem_AtributosNom1)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "CONTAGEMITEM_ATRIBUTOSNOM") == 0 ) && ( ( AV39DynamicFiltersOperator1 == 1 ) || ( AV39DynamicFiltersOperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40ContagemItem_AtributosNom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Atributos_Nome] like '%' + @lV40ContagemItem_AtributosNom1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV41DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector2, "CONTAGEMITEM_ATRIBUTOSNOM") == 0 ) && ( ( AV43DynamicFiltersOperator2 == 0 ) || ( AV43DynamicFiltersOperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ContagemItem_AtributosNom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV44ContagemItem_AtributosNom2)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV41DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector2, "CONTAGEMITEM_ATRIBUTOSNOM") == 0 ) && ( ( AV43DynamicFiltersOperator2 == 1 ) || ( AV43DynamicFiltersOperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ContagemItem_AtributosNom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Atributos_Nome] like '%' + @lV44ContagemItem_AtributosNom2)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV45DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "CONTAGEMITEM_ATRIBUTOSNOM") == 0 ) && ( ( AV47DynamicFiltersOperator3 == 0 ) || ( AV47DynamicFiltersOperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ContagemItem_AtributosNom3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV48ContagemItem_AtributosNom3)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV45DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "CONTAGEMITEM_ATRIBUTOSNOM") == 0 ) && ( ( AV47DynamicFiltersOperator3 == 1 ) || ( AV47DynamicFiltersOperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ContagemItem_AtributosNom3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Atributos_Nome] like '%' + @lV48ContagemItem_AtributosNom3)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemItem_Lancamento]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00SK2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00SK2 ;
          prmP00SK2 = new Object[] {
          new Object[] {"@AV49ContagemItem_Lancamento",SqlDbType.Int,6,0} ,
          new Object[] {"@lV40ContagemItem_AtributosNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV40ContagemItem_AtributosNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV44ContagemItem_AtributosNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV44ContagemItem_AtributosNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV48ContagemItem_AtributosNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV48ContagemItem_AtributosNom3",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00SK2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00SK2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getcontagemitemcontagemitematributosfsoftwarewcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getcontagemitemcontagemitematributosfsoftwarewcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getcontagemitemcontagemitematributosfsoftwarewcfilterdata") )
          {
             return  ;
          }
          getcontagemitemcontagemitematributosfsoftwarewcfilterdata worker = new getcontagemitemcontagemitematributosfsoftwarewcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
