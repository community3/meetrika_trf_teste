/*
               File: GetPromptContagemResultadoContagensFilterData
        Description: Get Prompt Contagem Resultado Contagens Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/15/2020 22:34:18.0
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptcontagemresultadocontagensfilterdata : GXProcedure
   {
      public getpromptcontagemresultadocontagensfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptcontagemresultadocontagensfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV32DDOName = aP0_DDOName;
         this.AV30SearchTxt = aP1_SearchTxt;
         this.AV31SearchTxtTo = aP2_SearchTxtTo;
         this.AV36OptionsJson = "" ;
         this.AV39OptionsDescJson = "" ;
         this.AV41OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV36OptionsJson;
         aP4_OptionsDescJson=this.AV39OptionsDescJson;
         aP5_OptionIndexesJson=this.AV41OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV32DDOName = aP0_DDOName;
         this.AV30SearchTxt = aP1_SearchTxt;
         this.AV31SearchTxtTo = aP2_SearchTxtTo;
         this.AV36OptionsJson = "" ;
         this.AV39OptionsDescJson = "" ;
         this.AV41OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV36OptionsJson;
         aP4_OptionsDescJson=this.AV39OptionsDescJson;
         aP5_OptionIndexesJson=this.AV41OptionIndexesJson;
         return AV41OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptcontagemresultadocontagensfilterdata objgetpromptcontagemresultadocontagensfilterdata;
         objgetpromptcontagemresultadocontagensfilterdata = new getpromptcontagemresultadocontagensfilterdata();
         objgetpromptcontagemresultadocontagensfilterdata.AV32DDOName = aP0_DDOName;
         objgetpromptcontagemresultadocontagensfilterdata.AV30SearchTxt = aP1_SearchTxt;
         objgetpromptcontagemresultadocontagensfilterdata.AV31SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptcontagemresultadocontagensfilterdata.AV36OptionsJson = "" ;
         objgetpromptcontagemresultadocontagensfilterdata.AV39OptionsDescJson = "" ;
         objgetpromptcontagemresultadocontagensfilterdata.AV41OptionIndexesJson = "" ;
         objgetpromptcontagemresultadocontagensfilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptcontagemresultadocontagensfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptcontagemresultadocontagensfilterdata);
         aP3_OptionsJson=this.AV36OptionsJson;
         aP4_OptionsDescJson=this.AV39OptionsDescJson;
         aP5_OptionIndexesJson=this.AV41OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptcontagemresultadocontagensfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV35Options = (IGxCollection)(new GxSimpleCollection());
         AV38OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV40OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV32DDOName), "DDO_CONTAGEMRESULTADO_PARECERTCN") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADO_PARECERTCNOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV36OptionsJson = AV35Options.ToJSonString(false);
         AV39OptionsDescJson = AV38OptionsDesc.ToJSonString(false);
         AV41OptionIndexesJson = AV40OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV43Session.Get("PromptContagemResultadoContagensGridState"), "") == 0 )
         {
            AV45GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptContagemResultadoContagensGridState"), "");
         }
         else
         {
            AV45GridState.FromXml(AV43Session.Get("PromptContagemResultadoContagensGridState"), "");
         }
         AV61GXV1 = 1;
         while ( AV61GXV1 <= AV45GridState.gxTpr_Filtervalues.Count )
         {
            AV46GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV45GridState.gxTpr_Filtervalues.Item(AV61GXV1));
            if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DATACNT") == 0 )
            {
               AV10TFContagemResultado_DataCnt = context.localUtil.CToD( AV46GridStateFilterValue.gxTpr_Value, 2);
               AV11TFContagemResultado_DataCnt_To = context.localUtil.CToD( AV46GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_CONTADORFMCOD") == 0 )
            {
               AV12TFContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Value, "."));
               AV13TFContagemResultado_ContadorFMCod_To = (int)(NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_PFBFS") == 0 )
            {
               AV14TFContagemResultado_PFBFS = NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Value, ".");
               AV15TFContagemResultado_PFBFS_To = NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_PFLFS") == 0 )
            {
               AV16TFContagemResultado_PFLFS = NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Value, ".");
               AV17TFContagemResultado_PFLFS_To = NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_PFBFM") == 0 )
            {
               AV18TFContagemResultado_PFBFM = NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Value, ".");
               AV19TFContagemResultado_PFBFM_To = NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_PFLFM") == 0 )
            {
               AV20TFContagemResultado_PFLFM = NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Value, ".");
               AV21TFContagemResultado_PFLFM_To = NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DIVERGENCIA") == 0 )
            {
               AV22TFContagemResultado_Divergencia = NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Value, ".");
               AV23TFContagemResultado_Divergencia_To = NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_PARECERTCN") == 0 )
            {
               AV24TFContagemResultado_ParecerTcn = AV46GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_PARECERTCN_SEL") == 0 )
            {
               AV25TFContagemResultado_ParecerTcn_Sel = AV46GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_NAOCNFCNTCOD") == 0 )
            {
               AV26TFContagemResultado_NaoCnfCntCod = (int)(NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Value, "."));
               AV27TFContagemResultado_NaoCnfCntCod_To = (int)(NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_STATUSCNT_SEL") == 0 )
            {
               AV28TFContagemResultado_StatusCnt_SelsJson = AV46GridStateFilterValue.gxTpr_Value;
               AV29TFContagemResultado_StatusCnt_Sels.FromJSonString(AV28TFContagemResultado_StatusCnt_SelsJson);
            }
            AV61GXV1 = (int)(AV61GXV1+1);
         }
         if ( AV45GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV47GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV45GridState.gxTpr_Dynamicfilters.Item(1));
            AV48DynamicFiltersSelector1 = AV47GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV48DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATACNT") == 0 )
            {
               AV49ContagemResultado_DataCnt1 = context.localUtil.CToD( AV47GridStateDynamicFilter.gxTpr_Value, 2);
               AV50ContagemResultado_DataCnt_To1 = context.localUtil.CToD( AV47GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            if ( AV45GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV51DynamicFiltersEnabled2 = true;
               AV47GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV45GridState.gxTpr_Dynamicfilters.Item(2));
               AV52DynamicFiltersSelector2 = AV47GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATACNT") == 0 )
               {
                  AV53ContagemResultado_DataCnt2 = context.localUtil.CToD( AV47GridStateDynamicFilter.gxTpr_Value, 2);
                  AV54ContagemResultado_DataCnt_To2 = context.localUtil.CToD( AV47GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               if ( AV45GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV55DynamicFiltersEnabled3 = true;
                  AV47GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV45GridState.gxTpr_Dynamicfilters.Item(3));
                  AV56DynamicFiltersSelector3 = AV47GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV56DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATACNT") == 0 )
                  {
                     AV57ContagemResultado_DataCnt3 = context.localUtil.CToD( AV47GridStateDynamicFilter.gxTpr_Value, 2);
                     AV58ContagemResultado_DataCnt_To3 = context.localUtil.CToD( AV47GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTAGEMRESULTADO_PARECERTCNOPTIONS' Routine */
         AV24TFContagemResultado_ParecerTcn = AV30SearchTxt;
         AV25TFContagemResultado_ParecerTcn_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A483ContagemResultado_StatusCnt ,
                                              AV29TFContagemResultado_StatusCnt_Sels ,
                                              AV48DynamicFiltersSelector1 ,
                                              AV49ContagemResultado_DataCnt1 ,
                                              AV50ContagemResultado_DataCnt_To1 ,
                                              AV51DynamicFiltersEnabled2 ,
                                              AV52DynamicFiltersSelector2 ,
                                              AV53ContagemResultado_DataCnt2 ,
                                              AV54ContagemResultado_DataCnt_To2 ,
                                              AV55DynamicFiltersEnabled3 ,
                                              AV56DynamicFiltersSelector3 ,
                                              AV57ContagemResultado_DataCnt3 ,
                                              AV58ContagemResultado_DataCnt_To3 ,
                                              AV10TFContagemResultado_DataCnt ,
                                              AV11TFContagemResultado_DataCnt_To ,
                                              AV12TFContagemResultado_ContadorFMCod ,
                                              AV13TFContagemResultado_ContadorFMCod_To ,
                                              AV14TFContagemResultado_PFBFS ,
                                              AV15TFContagemResultado_PFBFS_To ,
                                              AV16TFContagemResultado_PFLFS ,
                                              AV17TFContagemResultado_PFLFS_To ,
                                              AV18TFContagemResultado_PFBFM ,
                                              AV19TFContagemResultado_PFBFM_To ,
                                              AV20TFContagemResultado_PFLFM ,
                                              AV21TFContagemResultado_PFLFM_To ,
                                              AV22TFContagemResultado_Divergencia ,
                                              AV23TFContagemResultado_Divergencia_To ,
                                              AV25TFContagemResultado_ParecerTcn_Sel ,
                                              AV24TFContagemResultado_ParecerTcn ,
                                              AV26TFContagemResultado_NaoCnfCntCod ,
                                              AV27TFContagemResultado_NaoCnfCntCod_To ,
                                              AV29TFContagemResultado_StatusCnt_Sels.Count ,
                                              A473ContagemResultado_DataCnt ,
                                              A470ContagemResultado_ContadorFMCod ,
                                              A458ContagemResultado_PFBFS ,
                                              A459ContagemResultado_PFLFS ,
                                              A460ContagemResultado_PFBFM ,
                                              A461ContagemResultado_PFLFM ,
                                              A462ContagemResultado_Divergencia ,
                                              A463ContagemResultado_ParecerTcn ,
                                              A469ContagemResultado_NaoCnfCntCod },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.DATE, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL,
                                              TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV24TFContagemResultado_ParecerTcn = StringUtil.Concat( StringUtil.RTrim( AV24TFContagemResultado_ParecerTcn), "%", "");
         /* Using cursor P00S92 */
         pr_default.execute(0, new Object[] {AV49ContagemResultado_DataCnt1, AV50ContagemResultado_DataCnt_To1, AV53ContagemResultado_DataCnt2, AV54ContagemResultado_DataCnt_To2, AV57ContagemResultado_DataCnt3, AV58ContagemResultado_DataCnt_To3, AV10TFContagemResultado_DataCnt, AV11TFContagemResultado_DataCnt_To, AV12TFContagemResultado_ContadorFMCod, AV13TFContagemResultado_ContadorFMCod_To, AV14TFContagemResultado_PFBFS, AV15TFContagemResultado_PFBFS_To, AV16TFContagemResultado_PFLFS, AV17TFContagemResultado_PFLFS_To, AV18TFContagemResultado_PFBFM, AV19TFContagemResultado_PFBFM_To, AV20TFContagemResultado_PFLFM, AV21TFContagemResultado_PFLFM_To, AV22TFContagemResultado_Divergencia, AV23TFContagemResultado_Divergencia_To, lV24TFContagemResultado_ParecerTcn, AV25TFContagemResultado_ParecerTcn_Sel, AV26TFContagemResultado_NaoCnfCntCod, AV27TFContagemResultado_NaoCnfCntCod_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKS92 = false;
            A463ContagemResultado_ParecerTcn = P00S92_A463ContagemResultado_ParecerTcn[0];
            n463ContagemResultado_ParecerTcn = P00S92_n463ContagemResultado_ParecerTcn[0];
            A483ContagemResultado_StatusCnt = P00S92_A483ContagemResultado_StatusCnt[0];
            A469ContagemResultado_NaoCnfCntCod = P00S92_A469ContagemResultado_NaoCnfCntCod[0];
            n469ContagemResultado_NaoCnfCntCod = P00S92_n469ContagemResultado_NaoCnfCntCod[0];
            A462ContagemResultado_Divergencia = P00S92_A462ContagemResultado_Divergencia[0];
            A461ContagemResultado_PFLFM = P00S92_A461ContagemResultado_PFLFM[0];
            n461ContagemResultado_PFLFM = P00S92_n461ContagemResultado_PFLFM[0];
            A460ContagemResultado_PFBFM = P00S92_A460ContagemResultado_PFBFM[0];
            n460ContagemResultado_PFBFM = P00S92_n460ContagemResultado_PFBFM[0];
            A459ContagemResultado_PFLFS = P00S92_A459ContagemResultado_PFLFS[0];
            n459ContagemResultado_PFLFS = P00S92_n459ContagemResultado_PFLFS[0];
            A458ContagemResultado_PFBFS = P00S92_A458ContagemResultado_PFBFS[0];
            n458ContagemResultado_PFBFS = P00S92_n458ContagemResultado_PFBFS[0];
            A470ContagemResultado_ContadorFMCod = P00S92_A470ContagemResultado_ContadorFMCod[0];
            A473ContagemResultado_DataCnt = P00S92_A473ContagemResultado_DataCnt[0];
            A456ContagemResultado_Codigo = P00S92_A456ContagemResultado_Codigo[0];
            A511ContagemResultado_HoraCnt = P00S92_A511ContagemResultado_HoraCnt[0];
            AV42count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00S92_A463ContagemResultado_ParecerTcn[0], A463ContagemResultado_ParecerTcn) == 0 ) )
            {
               BRKS92 = false;
               A473ContagemResultado_DataCnt = P00S92_A473ContagemResultado_DataCnt[0];
               A456ContagemResultado_Codigo = P00S92_A456ContagemResultado_Codigo[0];
               A511ContagemResultado_HoraCnt = P00S92_A511ContagemResultado_HoraCnt[0];
               AV42count = (long)(AV42count+1);
               BRKS92 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A463ContagemResultado_ParecerTcn)) )
            {
               AV34Option = A463ContagemResultado_ParecerTcn;
               AV35Options.Add(AV34Option, 0);
               AV40OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV42count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV35Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKS92 )
            {
               BRKS92 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV35Options = new GxSimpleCollection();
         AV38OptionsDesc = new GxSimpleCollection();
         AV40OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV43Session = context.GetSession();
         AV45GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV46GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContagemResultado_DataCnt = DateTime.MinValue;
         AV11TFContagemResultado_DataCnt_To = DateTime.MinValue;
         AV24TFContagemResultado_ParecerTcn = "";
         AV25TFContagemResultado_ParecerTcn_Sel = "";
         AV28TFContagemResultado_StatusCnt_SelsJson = "";
         AV29TFContagemResultado_StatusCnt_Sels = new GxSimpleCollection();
         AV47GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV48DynamicFiltersSelector1 = "";
         AV49ContagemResultado_DataCnt1 = DateTime.MinValue;
         AV50ContagemResultado_DataCnt_To1 = DateTime.MinValue;
         AV52DynamicFiltersSelector2 = "";
         AV53ContagemResultado_DataCnt2 = DateTime.MinValue;
         AV54ContagemResultado_DataCnt_To2 = DateTime.MinValue;
         AV56DynamicFiltersSelector3 = "";
         AV57ContagemResultado_DataCnt3 = DateTime.MinValue;
         AV58ContagemResultado_DataCnt_To3 = DateTime.MinValue;
         scmdbuf = "";
         lV24TFContagemResultado_ParecerTcn = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A463ContagemResultado_ParecerTcn = "";
         P00S92_A463ContagemResultado_ParecerTcn = new String[] {""} ;
         P00S92_n463ContagemResultado_ParecerTcn = new bool[] {false} ;
         P00S92_A483ContagemResultado_StatusCnt = new short[1] ;
         P00S92_A469ContagemResultado_NaoCnfCntCod = new int[1] ;
         P00S92_n469ContagemResultado_NaoCnfCntCod = new bool[] {false} ;
         P00S92_A462ContagemResultado_Divergencia = new decimal[1] ;
         P00S92_A461ContagemResultado_PFLFM = new decimal[1] ;
         P00S92_n461ContagemResultado_PFLFM = new bool[] {false} ;
         P00S92_A460ContagemResultado_PFBFM = new decimal[1] ;
         P00S92_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P00S92_A459ContagemResultado_PFLFS = new decimal[1] ;
         P00S92_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P00S92_A458ContagemResultado_PFBFS = new decimal[1] ;
         P00S92_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P00S92_A470ContagemResultado_ContadorFMCod = new int[1] ;
         P00S92_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P00S92_A456ContagemResultado_Codigo = new int[1] ;
         P00S92_A511ContagemResultado_HoraCnt = new String[] {""} ;
         A511ContagemResultado_HoraCnt = "";
         AV34Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptcontagemresultadocontagensfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00S92_A463ContagemResultado_ParecerTcn, P00S92_n463ContagemResultado_ParecerTcn, P00S92_A483ContagemResultado_StatusCnt, P00S92_A469ContagemResultado_NaoCnfCntCod, P00S92_n469ContagemResultado_NaoCnfCntCod, P00S92_A462ContagemResultado_Divergencia, P00S92_A461ContagemResultado_PFLFM, P00S92_n461ContagemResultado_PFLFM, P00S92_A460ContagemResultado_PFBFM, P00S92_n460ContagemResultado_PFBFM,
               P00S92_A459ContagemResultado_PFLFS, P00S92_n459ContagemResultado_PFLFS, P00S92_A458ContagemResultado_PFBFS, P00S92_n458ContagemResultado_PFBFS, P00S92_A470ContagemResultado_ContadorFMCod, P00S92_A473ContagemResultado_DataCnt, P00S92_A456ContagemResultado_Codigo, P00S92_A511ContagemResultado_HoraCnt
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A483ContagemResultado_StatusCnt ;
      private int AV61GXV1 ;
      private int AV12TFContagemResultado_ContadorFMCod ;
      private int AV13TFContagemResultado_ContadorFMCod_To ;
      private int AV26TFContagemResultado_NaoCnfCntCod ;
      private int AV27TFContagemResultado_NaoCnfCntCod_To ;
      private int AV29TFContagemResultado_StatusCnt_Sels_Count ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int A469ContagemResultado_NaoCnfCntCod ;
      private int A456ContagemResultado_Codigo ;
      private long AV42count ;
      private decimal AV14TFContagemResultado_PFBFS ;
      private decimal AV15TFContagemResultado_PFBFS_To ;
      private decimal AV16TFContagemResultado_PFLFS ;
      private decimal AV17TFContagemResultado_PFLFS_To ;
      private decimal AV18TFContagemResultado_PFBFM ;
      private decimal AV19TFContagemResultado_PFBFM_To ;
      private decimal AV20TFContagemResultado_PFLFM ;
      private decimal AV21TFContagemResultado_PFLFM_To ;
      private decimal AV22TFContagemResultado_Divergencia ;
      private decimal AV23TFContagemResultado_Divergencia_To ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A459ContagemResultado_PFLFS ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A461ContagemResultado_PFLFM ;
      private decimal A462ContagemResultado_Divergencia ;
      private String scmdbuf ;
      private String A511ContagemResultado_HoraCnt ;
      private DateTime AV10TFContagemResultado_DataCnt ;
      private DateTime AV11TFContagemResultado_DataCnt_To ;
      private DateTime AV49ContagemResultado_DataCnt1 ;
      private DateTime AV50ContagemResultado_DataCnt_To1 ;
      private DateTime AV53ContagemResultado_DataCnt2 ;
      private DateTime AV54ContagemResultado_DataCnt_To2 ;
      private DateTime AV57ContagemResultado_DataCnt3 ;
      private DateTime AV58ContagemResultado_DataCnt_To3 ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool returnInSub ;
      private bool AV51DynamicFiltersEnabled2 ;
      private bool AV55DynamicFiltersEnabled3 ;
      private bool BRKS92 ;
      private bool n463ContagemResultado_ParecerTcn ;
      private bool n469ContagemResultado_NaoCnfCntCod ;
      private bool n461ContagemResultado_PFLFM ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n459ContagemResultado_PFLFS ;
      private bool n458ContagemResultado_PFBFS ;
      private String AV41OptionIndexesJson ;
      private String AV36OptionsJson ;
      private String AV39OptionsDescJson ;
      private String AV28TFContagemResultado_StatusCnt_SelsJson ;
      private String A463ContagemResultado_ParecerTcn ;
      private String AV32DDOName ;
      private String AV30SearchTxt ;
      private String AV31SearchTxtTo ;
      private String AV24TFContagemResultado_ParecerTcn ;
      private String AV25TFContagemResultado_ParecerTcn_Sel ;
      private String AV48DynamicFiltersSelector1 ;
      private String AV52DynamicFiltersSelector2 ;
      private String AV56DynamicFiltersSelector3 ;
      private String lV24TFContagemResultado_ParecerTcn ;
      private String AV34Option ;
      private IGxSession AV43Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00S92_A463ContagemResultado_ParecerTcn ;
      private bool[] P00S92_n463ContagemResultado_ParecerTcn ;
      private short[] P00S92_A483ContagemResultado_StatusCnt ;
      private int[] P00S92_A469ContagemResultado_NaoCnfCntCod ;
      private bool[] P00S92_n469ContagemResultado_NaoCnfCntCod ;
      private decimal[] P00S92_A462ContagemResultado_Divergencia ;
      private decimal[] P00S92_A461ContagemResultado_PFLFM ;
      private bool[] P00S92_n461ContagemResultado_PFLFM ;
      private decimal[] P00S92_A460ContagemResultado_PFBFM ;
      private bool[] P00S92_n460ContagemResultado_PFBFM ;
      private decimal[] P00S92_A459ContagemResultado_PFLFS ;
      private bool[] P00S92_n459ContagemResultado_PFLFS ;
      private decimal[] P00S92_A458ContagemResultado_PFBFS ;
      private bool[] P00S92_n458ContagemResultado_PFBFS ;
      private int[] P00S92_A470ContagemResultado_ContadorFMCod ;
      private DateTime[] P00S92_A473ContagemResultado_DataCnt ;
      private int[] P00S92_A456ContagemResultado_Codigo ;
      private String[] P00S92_A511ContagemResultado_HoraCnt ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV29TFContagemResultado_StatusCnt_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV35Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV38OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV40OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV45GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV46GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV47GridStateDynamicFilter ;
   }

   public class getpromptcontagemresultadocontagensfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00S92( IGxContext context ,
                                             short A483ContagemResultado_StatusCnt ,
                                             IGxCollection AV29TFContagemResultado_StatusCnt_Sels ,
                                             String AV48DynamicFiltersSelector1 ,
                                             DateTime AV49ContagemResultado_DataCnt1 ,
                                             DateTime AV50ContagemResultado_DataCnt_To1 ,
                                             bool AV51DynamicFiltersEnabled2 ,
                                             String AV52DynamicFiltersSelector2 ,
                                             DateTime AV53ContagemResultado_DataCnt2 ,
                                             DateTime AV54ContagemResultado_DataCnt_To2 ,
                                             bool AV55DynamicFiltersEnabled3 ,
                                             String AV56DynamicFiltersSelector3 ,
                                             DateTime AV57ContagemResultado_DataCnt3 ,
                                             DateTime AV58ContagemResultado_DataCnt_To3 ,
                                             DateTime AV10TFContagemResultado_DataCnt ,
                                             DateTime AV11TFContagemResultado_DataCnt_To ,
                                             int AV12TFContagemResultado_ContadorFMCod ,
                                             int AV13TFContagemResultado_ContadorFMCod_To ,
                                             decimal AV14TFContagemResultado_PFBFS ,
                                             decimal AV15TFContagemResultado_PFBFS_To ,
                                             decimal AV16TFContagemResultado_PFLFS ,
                                             decimal AV17TFContagemResultado_PFLFS_To ,
                                             decimal AV18TFContagemResultado_PFBFM ,
                                             decimal AV19TFContagemResultado_PFBFM_To ,
                                             decimal AV20TFContagemResultado_PFLFM ,
                                             decimal AV21TFContagemResultado_PFLFM_To ,
                                             decimal AV22TFContagemResultado_Divergencia ,
                                             decimal AV23TFContagemResultado_Divergencia_To ,
                                             String AV25TFContagemResultado_ParecerTcn_Sel ,
                                             String AV24TFContagemResultado_ParecerTcn ,
                                             int AV26TFContagemResultado_NaoCnfCntCod ,
                                             int AV27TFContagemResultado_NaoCnfCntCod_To ,
                                             int AV29TFContagemResultado_StatusCnt_Sels_Count ,
                                             DateTime A473ContagemResultado_DataCnt ,
                                             int A470ContagemResultado_ContadorFMCod ,
                                             decimal A458ContagemResultado_PFBFS ,
                                             decimal A459ContagemResultado_PFLFS ,
                                             decimal A460ContagemResultado_PFBFM ,
                                             decimal A461ContagemResultado_PFLFM ,
                                             decimal A462ContagemResultado_Divergencia ,
                                             String A463ContagemResultado_ParecerTcn ,
                                             int A469ContagemResultado_NaoCnfCntCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [24] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [ContagemResultado_ParecerTcn], [ContagemResultado_StatusCnt], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_Divergencia], [ContagemResultado_PFLFM], [ContagemResultado_PFBFM], [ContagemResultado_PFLFS], [ContagemResultado_PFBFS], [ContagemResultado_ContadorFMCod], [ContagemResultado_DataCnt], [ContagemResultado_Codigo], [ContagemResultado_HoraCnt] FROM [ContagemResultadoContagens] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV48DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV49ContagemResultado_DataCnt1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_DataCnt] >= @AV49ContagemResultado_DataCnt1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_DataCnt] >= @AV49ContagemResultado_DataCnt1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV48DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV50ContagemResultado_DataCnt_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_DataCnt] <= @AV50ContagemResultado_DataCnt_To1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_DataCnt] <= @AV50ContagemResultado_DataCnt_To1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV53ContagemResultado_DataCnt2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_DataCnt] >= @AV53ContagemResultado_DataCnt2)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_DataCnt] >= @AV53ContagemResultado_DataCnt2)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV54ContagemResultado_DataCnt_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_DataCnt] <= @AV54ContagemResultado_DataCnt_To2)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_DataCnt] <= @AV54ContagemResultado_DataCnt_To2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV55DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV56DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV57ContagemResultado_DataCnt3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_DataCnt] >= @AV57ContagemResultado_DataCnt3)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_DataCnt] >= @AV57ContagemResultado_DataCnt3)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV55DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV56DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV58ContagemResultado_DataCnt_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_DataCnt] <= @AV58ContagemResultado_DataCnt_To3)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_DataCnt] <= @AV58ContagemResultado_DataCnt_To3)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (DateTime.MinValue==AV10TFContagemResultado_DataCnt) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_DataCnt] >= @AV10TFContagemResultado_DataCnt)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_DataCnt] >= @AV10TFContagemResultado_DataCnt)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! (DateTime.MinValue==AV11TFContagemResultado_DataCnt_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_DataCnt] <= @AV11TFContagemResultado_DataCnt_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_DataCnt] <= @AV11TFContagemResultado_DataCnt_To)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! (0==AV12TFContagemResultado_ContadorFMCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_ContadorFMCod] >= @AV12TFContagemResultado_ContadorFMCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_ContadorFMCod] >= @AV12TFContagemResultado_ContadorFMCod)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (0==AV13TFContagemResultado_ContadorFMCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_ContadorFMCod] <= @AV13TFContagemResultado_ContadorFMCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_ContadorFMCod] <= @AV13TFContagemResultado_ContadorFMCod_To)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV14TFContagemResultado_PFBFS) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_PFBFS] >= @AV14TFContagemResultado_PFBFS)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_PFBFS] >= @AV14TFContagemResultado_PFBFS)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV15TFContagemResultado_PFBFS_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_PFBFS] <= @AV15TFContagemResultado_PFBFS_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_PFBFS] <= @AV15TFContagemResultado_PFBFS_To)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV16TFContagemResultado_PFLFS) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_PFLFS] >= @AV16TFContagemResultado_PFLFS)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_PFLFS] >= @AV16TFContagemResultado_PFLFS)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV17TFContagemResultado_PFLFS_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_PFLFS] <= @AV17TFContagemResultado_PFLFS_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_PFLFS] <= @AV17TFContagemResultado_PFLFS_To)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV18TFContagemResultado_PFBFM) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_PFBFM] >= @AV18TFContagemResultado_PFBFM)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_PFBFM] >= @AV18TFContagemResultado_PFBFM)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV19TFContagemResultado_PFBFM_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_PFBFM] <= @AV19TFContagemResultado_PFBFM_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_PFBFM] <= @AV19TFContagemResultado_PFBFM_To)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV20TFContagemResultado_PFLFM) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_PFLFM] >= @AV20TFContagemResultado_PFLFM)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_PFLFM] >= @AV20TFContagemResultado_PFLFM)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV21TFContagemResultado_PFLFM_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_PFLFM] <= @AV21TFContagemResultado_PFLFM_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_PFLFM] <= @AV21TFContagemResultado_PFLFM_To)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV22TFContagemResultado_Divergencia) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_Divergencia] >= @AV22TFContagemResultado_Divergencia)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_Divergencia] >= @AV22TFContagemResultado_Divergencia)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV23TFContagemResultado_Divergencia_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_Divergencia] <= @AV23TFContagemResultado_Divergencia_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_Divergencia] <= @AV23TFContagemResultado_Divergencia_To)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContagemResultado_ParecerTcn_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContagemResultado_ParecerTcn)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_ParecerTcn] like @lV24TFContagemResultado_ParecerTcn)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_ParecerTcn] like @lV24TFContagemResultado_ParecerTcn)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContagemResultado_ParecerTcn_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_ParecerTcn] = @AV25TFContagemResultado_ParecerTcn_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_ParecerTcn] = @AV25TFContagemResultado_ParecerTcn_Sel)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( ! (0==AV26TFContagemResultado_NaoCnfCntCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_NaoCnfCntCod] >= @AV26TFContagemResultado_NaoCnfCntCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_NaoCnfCntCod] >= @AV26TFContagemResultado_NaoCnfCntCod)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( ! (0==AV27TFContagemResultado_NaoCnfCntCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_NaoCnfCntCod] <= @AV27TFContagemResultado_NaoCnfCntCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_NaoCnfCntCod] <= @AV27TFContagemResultado_NaoCnfCntCod_To)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( AV29TFContagemResultado_StatusCnt_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV29TFContagemResultado_StatusCnt_Sels, "[ContagemResultado_StatusCnt] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV29TFContagemResultado_StatusCnt_Sels, "[ContagemResultado_StatusCnt] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [ContagemResultado_ParecerTcn]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00S92(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (decimal)dynConstraints[17] , (decimal)dynConstraints[18] , (decimal)dynConstraints[19] , (decimal)dynConstraints[20] , (decimal)dynConstraints[21] , (decimal)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] , (decimal)dynConstraints[25] , (decimal)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (int)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (DateTime)dynConstraints[32] , (int)dynConstraints[33] , (decimal)dynConstraints[34] , (decimal)dynConstraints[35] , (decimal)dynConstraints[36] , (decimal)dynConstraints[37] , (decimal)dynConstraints[38] , (String)dynConstraints[39] , (int)dynConstraints[40] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00S92 ;
          prmP00S92 = new Object[] {
          new Object[] {"@AV49ContagemResultado_DataCnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV50ContagemResultado_DataCnt_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV53ContagemResultado_DataCnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV54ContagemResultado_DataCnt_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV57ContagemResultado_DataCnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV58ContagemResultado_DataCnt_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV10TFContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV11TFContagemResultado_DataCnt_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV12TFContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContagemResultado_ContadorFMCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14TFContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV15TFContagemResultado_PFBFS_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV16TFContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV17TFContagemResultado_PFLFS_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV18TFContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV19TFContagemResultado_PFBFM_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV20TFContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV21TFContagemResultado_PFLFM_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV22TFContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV23TFContagemResultado_Divergencia_To",SqlDbType.Decimal,6,2} ,
          new Object[] {"@lV24TFContagemResultado_ParecerTcn",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV25TFContagemResultado_ParecerTcn_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV26TFContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV27TFContagemResultado_NaoCnfCntCod_To",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00S92", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00S92,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((DateTime[]) buf[15])[0] = rslt.getGXDate(10) ;
                ((int[]) buf[16])[0] = rslt.getInt(11) ;
                ((String[]) buf[17])[0] = rslt.getString(12, 5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptcontagemresultadocontagensfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptcontagemresultadocontagensfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptcontagemresultadocontagensfilterdata") )
          {
             return  ;
          }
          getpromptcontagemresultadocontagensfilterdata worker = new getpromptcontagemresultadocontagensfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
