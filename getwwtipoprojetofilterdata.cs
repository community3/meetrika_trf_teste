/*
               File: GetWWTipoProjetoFilterData
        Description: Get WWTipo Projeto Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:54:19.79
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwtipoprojetofilterdata : GXProcedure
   {
      public getwwtipoprojetofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwtipoprojetofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwtipoprojetofilterdata objgetwwtipoprojetofilterdata;
         objgetwwtipoprojetofilterdata = new getwwtipoprojetofilterdata();
         objgetwwtipoprojetofilterdata.AV18DDOName = aP0_DDOName;
         objgetwwtipoprojetofilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetwwtipoprojetofilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetwwtipoprojetofilterdata.AV22OptionsJson = "" ;
         objgetwwtipoprojetofilterdata.AV25OptionsDescJson = "" ;
         objgetwwtipoprojetofilterdata.AV27OptionIndexesJson = "" ;
         objgetwwtipoprojetofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwtipoprojetofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwtipoprojetofilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwtipoprojetofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_TIPOPROJETO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADTIPOPROJETO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("WWTipoProjetoGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWTipoProjetoGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("WWTipoProjetoGridState"), "");
         }
         AV44GXV1 = 1;
         while ( AV44GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV44GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFTIPOPROJETO_NOME") == 0 )
            {
               AV10TFTipoProjeto_Nome = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFTIPOPROJETO_NOME_SEL") == 0 )
            {
               AV11TFTipoProjeto_Nome_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFTIPOPROJETO_PRAZO") == 0 )
            {
               AV12TFTipoProjeto_Prazo = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, ".");
               AV13TFTipoProjeto_Prazo_To = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFTIPOPROJETO_CNSTEXP") == 0 )
            {
               AV14TFTipoProjeto_CnstExp = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, ".");
               AV15TFTipoProjeto_CnstExp_To = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Valueto, ".");
            }
            AV44GXV1 = (int)(AV44GXV1+1);
         }
         if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(1));
            AV34DynamicFiltersSelector1 = AV33GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "TIPOPROJETO_NOME") == 0 )
            {
               AV35TipoProjeto_Nome1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV36DynamicFiltersEnabled2 = true;
               AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(2));
               AV37DynamicFiltersSelector2 = AV33GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "TIPOPROJETO_NOME") == 0 )
               {
                  AV38TipoProjeto_Nome2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV39DynamicFiltersEnabled3 = true;
                  AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(3));
                  AV40DynamicFiltersSelector3 = AV33GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "TIPOPROJETO_NOME") == 0 )
                  {
                     AV41TipoProjeto_Nome3 = AV33GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADTIPOPROJETO_NOMEOPTIONS' Routine */
         AV10TFTipoProjeto_Nome = AV16SearchTxt;
         AV11TFTipoProjeto_Nome_Sel = "";
         AV46WWTipoProjetoDS_1_Dynamicfiltersselector1 = AV34DynamicFiltersSelector1;
         AV47WWTipoProjetoDS_2_Tipoprojeto_nome1 = AV35TipoProjeto_Nome1;
         AV48WWTipoProjetoDS_3_Dynamicfiltersenabled2 = AV36DynamicFiltersEnabled2;
         AV49WWTipoProjetoDS_4_Dynamicfiltersselector2 = AV37DynamicFiltersSelector2;
         AV50WWTipoProjetoDS_5_Tipoprojeto_nome2 = AV38TipoProjeto_Nome2;
         AV51WWTipoProjetoDS_6_Dynamicfiltersenabled3 = AV39DynamicFiltersEnabled3;
         AV52WWTipoProjetoDS_7_Dynamicfiltersselector3 = AV40DynamicFiltersSelector3;
         AV53WWTipoProjetoDS_8_Tipoprojeto_nome3 = AV41TipoProjeto_Nome3;
         AV54WWTipoProjetoDS_9_Tftipoprojeto_nome = AV10TFTipoProjeto_Nome;
         AV55WWTipoProjetoDS_10_Tftipoprojeto_nome_sel = AV11TFTipoProjeto_Nome_Sel;
         AV56WWTipoProjetoDS_11_Tftipoprojeto_prazo = AV12TFTipoProjeto_Prazo;
         AV57WWTipoProjetoDS_12_Tftipoprojeto_prazo_to = AV13TFTipoProjeto_Prazo_To;
         AV58WWTipoProjetoDS_13_Tftipoprojeto_cnstexp = AV14TFTipoProjeto_CnstExp;
         AV59WWTipoProjetoDS_14_Tftipoprojeto_cnstexp_to = AV15TFTipoProjeto_CnstExp_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV46WWTipoProjetoDS_1_Dynamicfiltersselector1 ,
                                              AV47WWTipoProjetoDS_2_Tipoprojeto_nome1 ,
                                              AV48WWTipoProjetoDS_3_Dynamicfiltersenabled2 ,
                                              AV49WWTipoProjetoDS_4_Dynamicfiltersselector2 ,
                                              AV50WWTipoProjetoDS_5_Tipoprojeto_nome2 ,
                                              AV51WWTipoProjetoDS_6_Dynamicfiltersenabled3 ,
                                              AV52WWTipoProjetoDS_7_Dynamicfiltersselector3 ,
                                              AV53WWTipoProjetoDS_8_Tipoprojeto_nome3 ,
                                              AV55WWTipoProjetoDS_10_Tftipoprojeto_nome_sel ,
                                              AV54WWTipoProjetoDS_9_Tftipoprojeto_nome ,
                                              AV56WWTipoProjetoDS_11_Tftipoprojeto_prazo ,
                                              AV57WWTipoProjetoDS_12_Tftipoprojeto_prazo_to ,
                                              AV58WWTipoProjetoDS_13_Tftipoprojeto_cnstexp ,
                                              AV59WWTipoProjetoDS_14_Tftipoprojeto_cnstexp_to ,
                                              A980TipoProjeto_Nome ,
                                              A981TipoProjeto_Prazo ,
                                              A982TipoProjeto_CnstExp },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL
                                              }
         });
         lV47WWTipoProjetoDS_2_Tipoprojeto_nome1 = StringUtil.PadR( StringUtil.RTrim( AV47WWTipoProjetoDS_2_Tipoprojeto_nome1), 50, "%");
         lV50WWTipoProjetoDS_5_Tipoprojeto_nome2 = StringUtil.PadR( StringUtil.RTrim( AV50WWTipoProjetoDS_5_Tipoprojeto_nome2), 50, "%");
         lV53WWTipoProjetoDS_8_Tipoprojeto_nome3 = StringUtil.PadR( StringUtil.RTrim( AV53WWTipoProjetoDS_8_Tipoprojeto_nome3), 50, "%");
         lV54WWTipoProjetoDS_9_Tftipoprojeto_nome = StringUtil.PadR( StringUtil.RTrim( AV54WWTipoProjetoDS_9_Tftipoprojeto_nome), 50, "%");
         /* Using cursor P00PL2 */
         pr_default.execute(0, new Object[] {lV47WWTipoProjetoDS_2_Tipoprojeto_nome1, lV50WWTipoProjetoDS_5_Tipoprojeto_nome2, lV53WWTipoProjetoDS_8_Tipoprojeto_nome3, lV54WWTipoProjetoDS_9_Tftipoprojeto_nome, AV55WWTipoProjetoDS_10_Tftipoprojeto_nome_sel, AV56WWTipoProjetoDS_11_Tftipoprojeto_prazo, AV57WWTipoProjetoDS_12_Tftipoprojeto_prazo_to, AV58WWTipoProjetoDS_13_Tftipoprojeto_cnstexp, AV59WWTipoProjetoDS_14_Tftipoprojeto_cnstexp_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKPL2 = false;
            A980TipoProjeto_Nome = P00PL2_A980TipoProjeto_Nome[0];
            A982TipoProjeto_CnstExp = P00PL2_A982TipoProjeto_CnstExp[0];
            A981TipoProjeto_Prazo = P00PL2_A981TipoProjeto_Prazo[0];
            A979TipoProjeto_Codigo = P00PL2_A979TipoProjeto_Codigo[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00PL2_A980TipoProjeto_Nome[0], A980TipoProjeto_Nome) == 0 ) )
            {
               BRKPL2 = false;
               A979TipoProjeto_Codigo = P00PL2_A979TipoProjeto_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKPL2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A980TipoProjeto_Nome)) )
            {
               AV20Option = A980TipoProjeto_Nome;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKPL2 )
            {
               BRKPL2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFTipoProjeto_Nome = "";
         AV11TFTipoProjeto_Nome_Sel = "";
         AV33GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV34DynamicFiltersSelector1 = "";
         AV35TipoProjeto_Nome1 = "";
         AV37DynamicFiltersSelector2 = "";
         AV38TipoProjeto_Nome2 = "";
         AV40DynamicFiltersSelector3 = "";
         AV41TipoProjeto_Nome3 = "";
         AV46WWTipoProjetoDS_1_Dynamicfiltersselector1 = "";
         AV47WWTipoProjetoDS_2_Tipoprojeto_nome1 = "";
         AV49WWTipoProjetoDS_4_Dynamicfiltersselector2 = "";
         AV50WWTipoProjetoDS_5_Tipoprojeto_nome2 = "";
         AV52WWTipoProjetoDS_7_Dynamicfiltersselector3 = "";
         AV53WWTipoProjetoDS_8_Tipoprojeto_nome3 = "";
         AV54WWTipoProjetoDS_9_Tftipoprojeto_nome = "";
         AV55WWTipoProjetoDS_10_Tftipoprojeto_nome_sel = "";
         scmdbuf = "";
         lV47WWTipoProjetoDS_2_Tipoprojeto_nome1 = "";
         lV50WWTipoProjetoDS_5_Tipoprojeto_nome2 = "";
         lV53WWTipoProjetoDS_8_Tipoprojeto_nome3 = "";
         lV54WWTipoProjetoDS_9_Tftipoprojeto_nome = "";
         A980TipoProjeto_Nome = "";
         P00PL2_A980TipoProjeto_Nome = new String[] {""} ;
         P00PL2_A982TipoProjeto_CnstExp = new decimal[1] ;
         P00PL2_A981TipoProjeto_Prazo = new decimal[1] ;
         P00PL2_A979TipoProjeto_Codigo = new int[1] ;
         AV20Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwtipoprojetofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00PL2_A980TipoProjeto_Nome, P00PL2_A982TipoProjeto_CnstExp, P00PL2_A981TipoProjeto_Prazo, P00PL2_A979TipoProjeto_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV44GXV1 ;
      private int A979TipoProjeto_Codigo ;
      private long AV28count ;
      private decimal AV12TFTipoProjeto_Prazo ;
      private decimal AV13TFTipoProjeto_Prazo_To ;
      private decimal AV14TFTipoProjeto_CnstExp ;
      private decimal AV15TFTipoProjeto_CnstExp_To ;
      private decimal AV56WWTipoProjetoDS_11_Tftipoprojeto_prazo ;
      private decimal AV57WWTipoProjetoDS_12_Tftipoprojeto_prazo_to ;
      private decimal AV58WWTipoProjetoDS_13_Tftipoprojeto_cnstexp ;
      private decimal AV59WWTipoProjetoDS_14_Tftipoprojeto_cnstexp_to ;
      private decimal A981TipoProjeto_Prazo ;
      private decimal A982TipoProjeto_CnstExp ;
      private String AV10TFTipoProjeto_Nome ;
      private String AV11TFTipoProjeto_Nome_Sel ;
      private String AV35TipoProjeto_Nome1 ;
      private String AV38TipoProjeto_Nome2 ;
      private String AV41TipoProjeto_Nome3 ;
      private String AV47WWTipoProjetoDS_2_Tipoprojeto_nome1 ;
      private String AV50WWTipoProjetoDS_5_Tipoprojeto_nome2 ;
      private String AV53WWTipoProjetoDS_8_Tipoprojeto_nome3 ;
      private String AV54WWTipoProjetoDS_9_Tftipoprojeto_nome ;
      private String AV55WWTipoProjetoDS_10_Tftipoprojeto_nome_sel ;
      private String scmdbuf ;
      private String lV47WWTipoProjetoDS_2_Tipoprojeto_nome1 ;
      private String lV50WWTipoProjetoDS_5_Tipoprojeto_nome2 ;
      private String lV53WWTipoProjetoDS_8_Tipoprojeto_nome3 ;
      private String lV54WWTipoProjetoDS_9_Tftipoprojeto_nome ;
      private String A980TipoProjeto_Nome ;
      private bool returnInSub ;
      private bool AV36DynamicFiltersEnabled2 ;
      private bool AV39DynamicFiltersEnabled3 ;
      private bool AV48WWTipoProjetoDS_3_Dynamicfiltersenabled2 ;
      private bool AV51WWTipoProjetoDS_6_Dynamicfiltersenabled3 ;
      private bool BRKPL2 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV34DynamicFiltersSelector1 ;
      private String AV37DynamicFiltersSelector2 ;
      private String AV40DynamicFiltersSelector3 ;
      private String AV46WWTipoProjetoDS_1_Dynamicfiltersselector1 ;
      private String AV49WWTipoProjetoDS_4_Dynamicfiltersselector2 ;
      private String AV52WWTipoProjetoDS_7_Dynamicfiltersselector3 ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00PL2_A980TipoProjeto_Nome ;
      private decimal[] P00PL2_A982TipoProjeto_CnstExp ;
      private decimal[] P00PL2_A981TipoProjeto_Prazo ;
      private int[] P00PL2_A979TipoProjeto_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV33GridStateDynamicFilter ;
   }

   public class getwwtipoprojetofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00PL2( IGxContext context ,
                                             String AV46WWTipoProjetoDS_1_Dynamicfiltersselector1 ,
                                             String AV47WWTipoProjetoDS_2_Tipoprojeto_nome1 ,
                                             bool AV48WWTipoProjetoDS_3_Dynamicfiltersenabled2 ,
                                             String AV49WWTipoProjetoDS_4_Dynamicfiltersselector2 ,
                                             String AV50WWTipoProjetoDS_5_Tipoprojeto_nome2 ,
                                             bool AV51WWTipoProjetoDS_6_Dynamicfiltersenabled3 ,
                                             String AV52WWTipoProjetoDS_7_Dynamicfiltersselector3 ,
                                             String AV53WWTipoProjetoDS_8_Tipoprojeto_nome3 ,
                                             String AV55WWTipoProjetoDS_10_Tftipoprojeto_nome_sel ,
                                             String AV54WWTipoProjetoDS_9_Tftipoprojeto_nome ,
                                             decimal AV56WWTipoProjetoDS_11_Tftipoprojeto_prazo ,
                                             decimal AV57WWTipoProjetoDS_12_Tftipoprojeto_prazo_to ,
                                             decimal AV58WWTipoProjetoDS_13_Tftipoprojeto_cnstexp ,
                                             decimal AV59WWTipoProjetoDS_14_Tftipoprojeto_cnstexp_to ,
                                             String A980TipoProjeto_Nome ,
                                             decimal A981TipoProjeto_Prazo ,
                                             decimal A982TipoProjeto_CnstExp )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [9] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [TipoProjeto_Nome], [TipoProjeto_CnstExp], [TipoProjeto_Prazo], [TipoProjeto_Codigo] FROM [TipoProjeto] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV46WWTipoProjetoDS_1_Dynamicfiltersselector1, "TIPOPROJETO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47WWTipoProjetoDS_2_Tipoprojeto_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Nome] like @lV47WWTipoProjetoDS_2_Tipoprojeto_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Nome] like @lV47WWTipoProjetoDS_2_Tipoprojeto_nome1 + '%')";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( AV48WWTipoProjetoDS_3_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV49WWTipoProjetoDS_4_Dynamicfiltersselector2, "TIPOPROJETO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWTipoProjetoDS_5_Tipoprojeto_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Nome] like @lV50WWTipoProjetoDS_5_Tipoprojeto_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Nome] like @lV50WWTipoProjetoDS_5_Tipoprojeto_nome2 + '%')";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV51WWTipoProjetoDS_6_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV52WWTipoProjetoDS_7_Dynamicfiltersselector3, "TIPOPROJETO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWTipoProjetoDS_8_Tipoprojeto_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Nome] like @lV53WWTipoProjetoDS_8_Tipoprojeto_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Nome] like @lV53WWTipoProjetoDS_8_Tipoprojeto_nome3 + '%')";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV55WWTipoProjetoDS_10_Tftipoprojeto_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWTipoProjetoDS_9_Tftipoprojeto_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Nome] like @lV54WWTipoProjetoDS_9_Tftipoprojeto_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Nome] like @lV54WWTipoProjetoDS_9_Tftipoprojeto_nome)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWTipoProjetoDS_10_Tftipoprojeto_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Nome] = @AV55WWTipoProjetoDS_10_Tftipoprojeto_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Nome] = @AV55WWTipoProjetoDS_10_Tftipoprojeto_nome_sel)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV56WWTipoProjetoDS_11_Tftipoprojeto_prazo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Prazo] >= @AV56WWTipoProjetoDS_11_Tftipoprojeto_prazo)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Prazo] >= @AV56WWTipoProjetoDS_11_Tftipoprojeto_prazo)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV57WWTipoProjetoDS_12_Tftipoprojeto_prazo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Prazo] <= @AV57WWTipoProjetoDS_12_Tftipoprojeto_prazo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Prazo] <= @AV57WWTipoProjetoDS_12_Tftipoprojeto_prazo_to)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV58WWTipoProjetoDS_13_Tftipoprojeto_cnstexp) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_CnstExp] >= @AV58WWTipoProjetoDS_13_Tftipoprojeto_cnstexp)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_CnstExp] >= @AV58WWTipoProjetoDS_13_Tftipoprojeto_cnstexp)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV59WWTipoProjetoDS_14_Tftipoprojeto_cnstexp_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_CnstExp] <= @AV59WWTipoProjetoDS_14_Tftipoprojeto_cnstexp_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_CnstExp] <= @AV59WWTipoProjetoDS_14_Tftipoprojeto_cnstexp_to)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [TipoProjeto_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00PL2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (decimal)dynConstraints[10] , (decimal)dynConstraints[11] , (decimal)dynConstraints[12] , (decimal)dynConstraints[13] , (String)dynConstraints[14] , (decimal)dynConstraints[15] , (decimal)dynConstraints[16] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00PL2 ;
          prmP00PL2 = new Object[] {
          new Object[] {"@lV47WWTipoProjetoDS_2_Tipoprojeto_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV50WWTipoProjetoDS_5_Tipoprojeto_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53WWTipoProjetoDS_8_Tipoprojeto_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWTipoProjetoDS_9_Tftipoprojeto_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV55WWTipoProjetoDS_10_Tftipoprojeto_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV56WWTipoProjetoDS_11_Tftipoprojeto_prazo",SqlDbType.Decimal,5,2} ,
          new Object[] {"@AV57WWTipoProjetoDS_12_Tftipoprojeto_prazo_to",SqlDbType.Decimal,5,2} ,
          new Object[] {"@AV58WWTipoProjetoDS_13_Tftipoprojeto_cnstexp",SqlDbType.Decimal,5,2} ,
          new Object[] {"@AV59WWTipoProjetoDS_14_Tftipoprojeto_cnstexp_to",SqlDbType.Decimal,5,2}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00PL2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00PL2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[17]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwtipoprojetofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwtipoprojetofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwtipoprojetofilterdata") )
          {
             return  ;
          }
          getwwtipoprojetofilterdata worker = new getwwtipoprojetofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
