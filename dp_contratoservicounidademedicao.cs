/*
               File: DP_ContratoServicoUnidadeMedicao
        Description: DP_Contrato Servico Unidade Medicao
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:4:12.33
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class dp_contratoservicounidademedicao : GXProcedure
   {
      public dp_contratoservicounidademedicao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public dp_contratoservicounidademedicao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contrato_Codigo ,
                           out IGxCollection aP1_Gxm2rootcol )
      {
         this.AV5Contrato_Codigo = aP0_Contrato_Codigo;
         this.Gxm2rootcol = new GxObjectCollection( context, "SDT_ContratoServicoUnidadeMedicao", "GxEv3Up14_MeetrikaVs3", "SdtSDT_ContratoServicoUnidadeMedicao", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP1_Gxm2rootcol=this.Gxm2rootcol;
      }

      public IGxCollection executeUdp( int aP0_Contrato_Codigo )
      {
         this.AV5Contrato_Codigo = aP0_Contrato_Codigo;
         this.Gxm2rootcol = new GxObjectCollection( context, "SDT_ContratoServicoUnidadeMedicao", "GxEv3Up14_MeetrikaVs3", "SdtSDT_ContratoServicoUnidadeMedicao", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP1_Gxm2rootcol=this.Gxm2rootcol;
         return Gxm2rootcol ;
      }

      public void executeSubmit( int aP0_Contrato_Codigo ,
                                 out IGxCollection aP1_Gxm2rootcol )
      {
         dp_contratoservicounidademedicao objdp_contratoservicounidademedicao;
         objdp_contratoservicounidademedicao = new dp_contratoservicounidademedicao();
         objdp_contratoservicounidademedicao.AV5Contrato_Codigo = aP0_Contrato_Codigo;
         objdp_contratoservicounidademedicao.Gxm2rootcol = new GxObjectCollection( context, "SDT_ContratoServicoUnidadeMedicao", "GxEv3Up14_MeetrikaVs3", "SdtSDT_ContratoServicoUnidadeMedicao", "GeneXus.Programs") ;
         objdp_contratoservicounidademedicao.context.SetSubmitInitialConfig(context);
         objdp_contratoservicounidademedicao.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objdp_contratoservicounidademedicao);
         aP1_Gxm2rootcol=this.Gxm2rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((dp_contratoservicounidademedicao)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P000E2 */
         pr_default.execute(0, new Object[] {AV5Contrato_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1207ContratoUnidades_ContratoCod = P000E2_A1207ContratoUnidades_ContratoCod[0];
            A1204ContratoUnidades_UndMedCod = P000E2_A1204ContratoUnidades_UndMedCod[0];
            A1205ContratoUnidades_UndMedNom = P000E2_A1205ContratoUnidades_UndMedNom[0];
            n1205ContratoUnidades_UndMedNom = P000E2_n1205ContratoUnidades_UndMedNom[0];
            A1206ContratoUnidades_UndMedSigla = P000E2_A1206ContratoUnidades_UndMedSigla[0];
            n1206ContratoUnidades_UndMedSigla = P000E2_n1206ContratoUnidades_UndMedSigla[0];
            A1205ContratoUnidades_UndMedNom = P000E2_A1205ContratoUnidades_UndMedNom[0];
            n1205ContratoUnidades_UndMedNom = P000E2_n1205ContratoUnidades_UndMedNom[0];
            A1206ContratoUnidades_UndMedSigla = P000E2_A1206ContratoUnidades_UndMedSigla[0];
            n1206ContratoUnidades_UndMedSigla = P000E2_n1206ContratoUnidades_UndMedSigla[0];
            Gxm1sdt_contratoservicounidademedicao = new SdtSDT_ContratoServicoUnidadeMedicao(context);
            Gxm2rootcol.Add(Gxm1sdt_contratoservicounidademedicao, 0);
            Gxm1sdt_contratoservicounidademedicao.gxTpr_Contratoservicos_unidadecontratada = A1204ContratoUnidades_UndMedCod;
            Gxm1sdt_contratoservicounidademedicao.gxTpr_Contratoservicos_unidadecontratadadescricao = A1205ContratoUnidades_UndMedNom;
            Gxm1sdt_contratoservicounidademedicao.gxTpr_Contratoservicos_undcntsgl = A1206ContratoUnidades_UndMedSigla;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P000E2_A1207ContratoUnidades_ContratoCod = new int[1] ;
         P000E2_A1204ContratoUnidades_UndMedCod = new int[1] ;
         P000E2_A1205ContratoUnidades_UndMedNom = new String[] {""} ;
         P000E2_n1205ContratoUnidades_UndMedNom = new bool[] {false} ;
         P000E2_A1206ContratoUnidades_UndMedSigla = new String[] {""} ;
         P000E2_n1206ContratoUnidades_UndMedSigla = new bool[] {false} ;
         A1205ContratoUnidades_UndMedNom = "";
         A1206ContratoUnidades_UndMedSigla = "";
         Gxm1sdt_contratoservicounidademedicao = new SdtSDT_ContratoServicoUnidadeMedicao(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.dp_contratoservicounidademedicao__default(),
            new Object[][] {
                new Object[] {
               P000E2_A1207ContratoUnidades_ContratoCod, P000E2_A1204ContratoUnidades_UndMedCod, P000E2_A1205ContratoUnidades_UndMedNom, P000E2_n1205ContratoUnidades_UndMedNom, P000E2_A1206ContratoUnidades_UndMedSigla, P000E2_n1206ContratoUnidades_UndMedSigla
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV5Contrato_Codigo ;
      private int A1207ContratoUnidades_ContratoCod ;
      private int A1204ContratoUnidades_UndMedCod ;
      private String scmdbuf ;
      private String A1205ContratoUnidades_UndMedNom ;
      private String A1206ContratoUnidades_UndMedSigla ;
      private bool n1205ContratoUnidades_UndMedNom ;
      private bool n1206ContratoUnidades_UndMedSigla ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P000E2_A1207ContratoUnidades_ContratoCod ;
      private int[] P000E2_A1204ContratoUnidades_UndMedCod ;
      private String[] P000E2_A1205ContratoUnidades_UndMedNom ;
      private bool[] P000E2_n1205ContratoUnidades_UndMedNom ;
      private String[] P000E2_A1206ContratoUnidades_UndMedSigla ;
      private bool[] P000E2_n1206ContratoUnidades_UndMedSigla ;
      private IGxCollection aP1_Gxm2rootcol ;
      [ObjectCollection(ItemType=typeof( SdtSDT_ContratoServicoUnidadeMedicao ))]
      private IGxCollection Gxm2rootcol ;
      private SdtSDT_ContratoServicoUnidadeMedicao Gxm1sdt_contratoservicounidademedicao ;
   }

   public class dp_contratoservicounidademedicao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP000E2 ;
          prmP000E2 = new Object[] {
          new Object[] {"@AV5Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P000E2", "SELECT T1.[ContratoUnidades_ContratoCod], T1.[ContratoUnidades_UndMedCod] AS ContratoUnidades_UndMedCod, T2.[UnidadeMedicao_Nome] AS ContratoUnidades_UndMedNom, T2.[UnidadeMedicao_Sigla] AS ContratoUnidades_UndMedSigla FROM ([ContratoUnidades] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[ContratoUnidades_UndMedCod]) WHERE T1.[ContratoUnidades_ContratoCod] = @AV5Contrato_Codigo ORDER BY T1.[ContratoUnidades_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000E2,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
