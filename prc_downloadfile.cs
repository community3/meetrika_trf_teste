/*
               File: PRC_DownloadFile
        Description: Stub for PRC_DownloadFile
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:58.32
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_downloadfile : GXProcedure
   {
      public prc_downloadfile( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public prc_downloadfile( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Path ,
                           String aP1_ContentType ,
                           String aP2_NomeArquivo )
      {
         this.AV2Path = aP0_Path;
         this.AV3ContentType = aP1_ContentType;
         this.AV4NomeArquivo = aP2_NomeArquivo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( String aP0_Path ,
                                 String aP1_ContentType ,
                                 String aP2_NomeArquivo )
      {
         prc_downloadfile objprc_downloadfile;
         objprc_downloadfile = new prc_downloadfile();
         objprc_downloadfile.AV2Path = aP0_Path;
         objprc_downloadfile.AV3ContentType = aP1_ContentType;
         objprc_downloadfile.AV4NomeArquivo = aP2_NomeArquivo;
         objprc_downloadfile.context.SetSubmitInitialConfig(context);
         objprc_downloadfile.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_downloadfile);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_downloadfile)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(String)AV2Path,(String)AV3ContentType,(String)AV4NomeArquivo} ;
         ClassLoader.Execute("aprc_downloadfile","GeneXus.Programs.aprc_downloadfile", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 3 ) )
         {
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private String AV2Path ;
      private String AV3ContentType ;
      private String AV4NomeArquivo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private Object[] args ;
   }

}
