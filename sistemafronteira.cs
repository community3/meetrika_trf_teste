/*
               File: SistemaFronteira
        Description: Sistema Fronteira
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:16.53
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class sistemafronteira : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public sistemafronteira( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public sistemafronteira( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_FuncaoDados_SistemaCod )
      {
         this.AV7FuncaoDados_SistemaCod = aP0_FuncaoDados_SistemaCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavFuncaodados_tipo = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbFuncaoDados_Complexidade = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7FuncaoDados_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoDados_SistemaCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7FuncaoDados_SistemaCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_53 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_53_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_53_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
                  AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
                  AV16DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
                  AV17FuncaoDados_Nome1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17FuncaoDados_Nome1", AV17FuncaoDados_Nome1);
                  AV20DynamicFiltersSelector2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
                  AV21FuncaoDados_Nome2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21FuncaoDados_Nome2", AV21FuncaoDados_Nome2);
                  AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
                  AV30TFFuncaoDados_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFFuncaoDados_Nome", AV30TFFuncaoDados_Nome);
                  AV31TFFuncaoDados_Nome_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TFFuncaoDados_Nome_Sel", AV31TFFuncaoDados_Nome_Sel);
                  AV38TFFuncaoDados_DER = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38TFFuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFFuncaoDados_DER), 4, 0)));
                  AV39TFFuncaoDados_DER_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFFuncaoDados_DER_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFFuncaoDados_DER_To), 4, 0)));
                  AV42TFFuncaoDados_RLR = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42TFFuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42TFFuncaoDados_RLR), 4, 0)));
                  AV43TFFuncaoDados_RLR_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFFuncaoDados_RLR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFFuncaoDados_RLR_To), 4, 0)));
                  AV46TFFuncaoDados_PF = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46TFFuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( AV46TFFuncaoDados_PF, 14, 5)));
                  AV47TFFuncaoDados_PF_To = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFFuncaoDados_PF_To", StringUtil.LTrim( StringUtil.Str( AV47TFFuncaoDados_PF_To, 14, 5)));
                  AV7FuncaoDados_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoDados_SistemaCod), 6, 0)));
                  AV32ddo_FuncaoDados_NomeTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ddo_FuncaoDados_NomeTitleControlIdToReplace", AV32ddo_FuncaoDados_NomeTitleControlIdToReplace);
                  AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace", AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace);
                  AV40ddo_FuncaoDados_DERTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40ddo_FuncaoDados_DERTitleControlIdToReplace", AV40ddo_FuncaoDados_DERTitleControlIdToReplace);
                  AV44ddo_FuncaoDados_RLRTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44ddo_FuncaoDados_RLRTitleControlIdToReplace", AV44ddo_FuncaoDados_RLRTitleControlIdToReplace);
                  AV48ddo_FuncaoDados_PFTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48ddo_FuncaoDados_PFTitleControlIdToReplace", AV48ddo_FuncaoDados_PFTitleControlIdToReplace);
                  AV52Pgmname = GetNextPar( );
                  AV26FuncaoDados_Tipo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26FuncaoDados_Tipo", AV26FuncaoDados_Tipo);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV35TFFuncaoDados_Complexidade_Sels);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
                  AV24DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
                  AV23DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17FuncaoDados_Nome1, AV20DynamicFiltersSelector2, AV21FuncaoDados_Nome2, AV19DynamicFiltersEnabled2, AV30TFFuncaoDados_Nome, AV31TFFuncaoDados_Nome_Sel, AV38TFFuncaoDados_DER, AV39TFFuncaoDados_DER_To, AV42TFFuncaoDados_RLR, AV43TFFuncaoDados_RLR_To, AV46TFFuncaoDados_PF, AV47TFFuncaoDados_PF_To, AV7FuncaoDados_SistemaCod, AV32ddo_FuncaoDados_NomeTitleControlIdToReplace, AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV40ddo_FuncaoDados_DERTitleControlIdToReplace, AV44ddo_FuncaoDados_RLRTitleControlIdToReplace, AV48ddo_FuncaoDados_PFTitleControlIdToReplace, AV52Pgmname, AV26FuncaoDados_Tipo, AV35TFFuncaoDados_Complexidade_Sels, AV11GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PADI2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV52Pgmname = "SistemaFronteira";
               context.Gx_err = 0;
               WSDI2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Sistema Fronteira") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042822501691");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("sistemafronteira.aspx") + "?" + UrlEncode("" +AV7FuncaoDados_SistemaCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vFUNCAODADOS_NOME1", AV17FuncaoDados_Nome1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vFUNCAODADOS_NOME2", AV21FuncaoDados_Nome2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAODADOS_NOME", AV30TFFuncaoDados_Nome);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAODADOS_NOME_SEL", AV31TFFuncaoDados_Nome_Sel);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAODADOS_DER", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38TFFuncaoDados_DER), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAODADOS_DER_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39TFFuncaoDados_DER_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAODADOS_RLR", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV42TFFuncaoDados_RLR), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAODADOS_RLR_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43TFFuncaoDados_RLR_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAODADOS_PF", StringUtil.LTrim( StringUtil.NToC( AV46TFFuncaoDados_PF, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAODADOS_PF_TO", StringUtil.LTrim( StringUtil.NToC( AV47TFFuncaoDados_PF_To, 14, 5, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_53", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_53), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV49DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV49DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vFUNCAODADOS_NOMETITLEFILTERDATA", AV29FuncaoDados_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vFUNCAODADOS_NOMETITLEFILTERDATA", AV29FuncaoDados_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vFUNCAODADOS_COMPLEXIDADETITLEFILTERDATA", AV33FuncaoDados_ComplexidadeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vFUNCAODADOS_COMPLEXIDADETITLEFILTERDATA", AV33FuncaoDados_ComplexidadeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vFUNCAODADOS_DERTITLEFILTERDATA", AV37FuncaoDados_DERTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vFUNCAODADOS_DERTITLEFILTERDATA", AV37FuncaoDados_DERTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vFUNCAODADOS_RLRTITLEFILTERDATA", AV41FuncaoDados_RLRTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vFUNCAODADOS_RLRTITLEFILTERDATA", AV41FuncaoDados_RLRTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vFUNCAODADOS_PFTITLEFILTERDATA", AV45FuncaoDados_PFTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vFUNCAODADOS_PFTITLEFILTERDATA", AV45FuncaoDados_PFTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7FuncaoDados_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vFUNCAODADOS_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7FuncaoDados_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV52Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vTFFUNCAODADOS_COMPLEXIDADE_SELS", AV35TFFuncaoDados_Complexidade_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vTFFUNCAODADOS_COMPLEXIDADE_SELS", AV35TFFuncaoDados_Complexidade_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSIGNOREFIRST", AV24DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSREMOVING", AV23DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"FUNCAODADOS_CONTAR", A755FuncaoDados_Contar);
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Caption", StringUtil.RTrim( Ddo_funcaodados_nome_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Tooltip", StringUtil.RTrim( Ddo_funcaodados_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Cls", StringUtil.RTrim( Ddo_funcaodados_nome_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_funcaodados_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaodados_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaodados_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaodados_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_funcaodados_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaodados_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Sortedstatus", StringUtil.RTrim( Ddo_funcaodados_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Includefilter", StringUtil.BoolToStr( Ddo_funcaodados_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Filtertype", StringUtil.RTrim( Ddo_funcaodados_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_funcaodados_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_funcaodados_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Datalisttype", StringUtil.RTrim( Ddo_funcaodados_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Datalistfixedvalues", StringUtil.RTrim( Ddo_funcaodados_nome_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Datalistproc", StringUtil.RTrim( Ddo_funcaodados_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaodados_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Sortasc", StringUtil.RTrim( Ddo_funcaodados_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Sortdsc", StringUtil.RTrim( Ddo_funcaodados_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Loadingdata", StringUtil.RTrim( Ddo_funcaodados_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Cleanfilter", StringUtil.RTrim( Ddo_funcaodados_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaodados_nome_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Rangefilterto", StringUtil.RTrim( Ddo_funcaodados_nome_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Noresultsfound", StringUtil.RTrim( Ddo_funcaodados_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_funcaodados_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Caption", StringUtil.RTrim( Ddo_funcaodados_complexidade_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Tooltip", StringUtil.RTrim( Ddo_funcaodados_complexidade_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Cls", StringUtil.RTrim( Ddo_funcaodados_complexidade_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaodados_complexidade_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaodados_complexidade_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaodados_complexidade_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Includesortasc", StringUtil.BoolToStr( Ddo_funcaodados_complexidade_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaodados_complexidade_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Includefilter", StringUtil.BoolToStr( Ddo_funcaodados_complexidade_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Filterisrange", StringUtil.BoolToStr( Ddo_funcaodados_complexidade_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Includedatalist", StringUtil.BoolToStr( Ddo_funcaodados_complexidade_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Datalisttype", StringUtil.RTrim( Ddo_funcaodados_complexidade_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Allowmultipleselection", StringUtil.BoolToStr( Ddo_funcaodados_complexidade_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Datalistfixedvalues", StringUtil.RTrim( Ddo_funcaodados_complexidade_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaodados_complexidade_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Sortasc", StringUtil.RTrim( Ddo_funcaodados_complexidade_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Sortdsc", StringUtil.RTrim( Ddo_funcaodados_complexidade_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Loadingdata", StringUtil.RTrim( Ddo_funcaodados_complexidade_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Cleanfilter", StringUtil.RTrim( Ddo_funcaodados_complexidade_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaodados_complexidade_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Rangefilterto", StringUtil.RTrim( Ddo_funcaodados_complexidade_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Noresultsfound", StringUtil.RTrim( Ddo_funcaodados_complexidade_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Searchbuttontext", StringUtil.RTrim( Ddo_funcaodados_complexidade_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Caption", StringUtil.RTrim( Ddo_funcaodados_der_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Tooltip", StringUtil.RTrim( Ddo_funcaodados_der_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Cls", StringUtil.RTrim( Ddo_funcaodados_der_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Filteredtext_set", StringUtil.RTrim( Ddo_funcaodados_der_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Filteredtextto_set", StringUtil.RTrim( Ddo_funcaodados_der_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaodados_der_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaodados_der_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Includesortasc", StringUtil.BoolToStr( Ddo_funcaodados_der_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaodados_der_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Includefilter", StringUtil.BoolToStr( Ddo_funcaodados_der_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Filtertype", StringUtil.RTrim( Ddo_funcaodados_der_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Filterisrange", StringUtil.BoolToStr( Ddo_funcaodados_der_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Includedatalist", StringUtil.BoolToStr( Ddo_funcaodados_der_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Datalistfixedvalues", StringUtil.RTrim( Ddo_funcaodados_der_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaodados_der_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Sortasc", StringUtil.RTrim( Ddo_funcaodados_der_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Sortdsc", StringUtil.RTrim( Ddo_funcaodados_der_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Loadingdata", StringUtil.RTrim( Ddo_funcaodados_der_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Cleanfilter", StringUtil.RTrim( Ddo_funcaodados_der_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaodados_der_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Rangefilterto", StringUtil.RTrim( Ddo_funcaodados_der_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Noresultsfound", StringUtil.RTrim( Ddo_funcaodados_der_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Searchbuttontext", StringUtil.RTrim( Ddo_funcaodados_der_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Caption", StringUtil.RTrim( Ddo_funcaodados_rlr_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Tooltip", StringUtil.RTrim( Ddo_funcaodados_rlr_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Cls", StringUtil.RTrim( Ddo_funcaodados_rlr_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Filteredtext_set", StringUtil.RTrim( Ddo_funcaodados_rlr_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Filteredtextto_set", StringUtil.RTrim( Ddo_funcaodados_rlr_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaodados_rlr_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaodados_rlr_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Includesortasc", StringUtil.BoolToStr( Ddo_funcaodados_rlr_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaodados_rlr_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Includefilter", StringUtil.BoolToStr( Ddo_funcaodados_rlr_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Filtertype", StringUtil.RTrim( Ddo_funcaodados_rlr_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Filterisrange", StringUtil.BoolToStr( Ddo_funcaodados_rlr_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Includedatalist", StringUtil.BoolToStr( Ddo_funcaodados_rlr_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Datalistfixedvalues", StringUtil.RTrim( Ddo_funcaodados_rlr_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaodados_rlr_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Sortasc", StringUtil.RTrim( Ddo_funcaodados_rlr_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Sortdsc", StringUtil.RTrim( Ddo_funcaodados_rlr_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Loadingdata", StringUtil.RTrim( Ddo_funcaodados_rlr_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Cleanfilter", StringUtil.RTrim( Ddo_funcaodados_rlr_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaodados_rlr_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Rangefilterto", StringUtil.RTrim( Ddo_funcaodados_rlr_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Noresultsfound", StringUtil.RTrim( Ddo_funcaodados_rlr_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Searchbuttontext", StringUtil.RTrim( Ddo_funcaodados_rlr_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Caption", StringUtil.RTrim( Ddo_funcaodados_pf_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Tooltip", StringUtil.RTrim( Ddo_funcaodados_pf_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Cls", StringUtil.RTrim( Ddo_funcaodados_pf_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Filteredtext_set", StringUtil.RTrim( Ddo_funcaodados_pf_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Filteredtextto_set", StringUtil.RTrim( Ddo_funcaodados_pf_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaodados_pf_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaodados_pf_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Includesortasc", StringUtil.BoolToStr( Ddo_funcaodados_pf_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaodados_pf_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Includefilter", StringUtil.BoolToStr( Ddo_funcaodados_pf_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Filtertype", StringUtil.RTrim( Ddo_funcaodados_pf_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Filterisrange", StringUtil.BoolToStr( Ddo_funcaodados_pf_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Includedatalist", StringUtil.BoolToStr( Ddo_funcaodados_pf_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Datalistfixedvalues", StringUtil.RTrim( Ddo_funcaodados_pf_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaodados_pf_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Sortasc", StringUtil.RTrim( Ddo_funcaodados_pf_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Sortdsc", StringUtil.RTrim( Ddo_funcaodados_pf_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Loadingdata", StringUtil.RTrim( Ddo_funcaodados_pf_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Cleanfilter", StringUtil.RTrim( Ddo_funcaodados_pf_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaodados_pf_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Rangefilterto", StringUtil.RTrim( Ddo_funcaodados_pf_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Noresultsfound", StringUtil.RTrim( Ddo_funcaodados_pf_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Searchbuttontext", StringUtil.RTrim( Ddo_funcaodados_pf_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Activeeventkey", StringUtil.RTrim( Ddo_funcaodados_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_funcaodados_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaodados_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Activeeventkey", StringUtil.RTrim( Ddo_funcaodados_complexidade_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaodados_complexidade_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Activeeventkey", StringUtil.RTrim( Ddo_funcaodados_der_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Filteredtext_get", StringUtil.RTrim( Ddo_funcaodados_der_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Filteredtextto_get", StringUtil.RTrim( Ddo_funcaodados_der_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Activeeventkey", StringUtil.RTrim( Ddo_funcaodados_rlr_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Filteredtext_get", StringUtil.RTrim( Ddo_funcaodados_rlr_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Filteredtextto_get", StringUtil.RTrim( Ddo_funcaodados_rlr_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Activeeventkey", StringUtil.RTrim( Ddo_funcaodados_pf_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Filteredtext_get", StringUtil.RTrim( Ddo_funcaodados_pf_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Filteredtextto_get", StringUtil.RTrim( Ddo_funcaodados_pf_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormDI2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("sistemafronteira.js", "?202042822501935");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "SistemaFronteira" ;
      }

      public override String GetPgmdesc( )
      {
         return "Sistema Fronteira" ;
      }

      protected void WBDI0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "sistemafronteira.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_DI2( true) ;
         }
         else
         {
            wb_table1_2_DI2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_DI2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'" + sPrefix + "',false,'" + sGXsfl_53_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(64, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'" + sPrefix + "',false,'" + sGXsfl_53_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTffuncaodados_nome_Internalname, AV30TFFuncaoDados_Nome, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", 0, edtavTffuncaodados_nome_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_SistemaFronteira.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'" + sPrefix + "',false,'" + sGXsfl_53_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTffuncaodados_nome_sel_Internalname, AV31TFFuncaoDados_Nome_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,66);\"", 0, edtavTffuncaodados_nome_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_SistemaFronteira.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'" + sPrefix + "',false,'" + sGXsfl_53_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaodados_der_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38TFFuncaoDados_DER), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV38TFFuncaoDados_DER), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,67);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaodados_der_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaodados_der_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaFronteira.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'" + sPrefix + "',false,'" + sGXsfl_53_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaodados_der_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39TFFuncaoDados_DER_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV39TFFuncaoDados_DER_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,68);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaodados_der_to_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaodados_der_to_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaFronteira.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'" + sPrefix + "',false,'" + sGXsfl_53_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaodados_rlr_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV42TFFuncaoDados_RLR), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV42TFFuncaoDados_RLR), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,69);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaodados_rlr_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaodados_rlr_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaFronteira.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'" + sPrefix + "',false,'" + sGXsfl_53_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaodados_rlr_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43TFFuncaoDados_RLR_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV43TFFuncaoDados_RLR_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,70);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaodados_rlr_to_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaodados_rlr_to_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaFronteira.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'" + sPrefix + "',false,'" + sGXsfl_53_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaodados_pf_Internalname, StringUtil.LTrim( StringUtil.NToC( AV46TFFuncaoDados_PF, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV46TFFuncaoDados_PF, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,71);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaodados_pf_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaodados_pf_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaFronteira.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'" + sPrefix + "',false,'" + sGXsfl_53_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaodados_pf_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV47TFFuncaoDados_PF_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV47TFFuncaoDados_PF_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,72);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaodados_pf_to_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaodados_pf_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaFronteira.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_FUNCAODADOS_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'" + sPrefix + "',false,'" + sGXsfl_53_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaodados_nometitlecontrolidtoreplace_Internalname, AV32ddo_FuncaoDados_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", 0, edtavDdo_funcaodados_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SistemaFronteira.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'" + sPrefix + "',false,'" + sGXsfl_53_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Internalname, AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", 0, edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SistemaFronteira.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_FUNCAODADOS_DERContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'" + sPrefix + "',false,'" + sGXsfl_53_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaodados_dertitlecontrolidtoreplace_Internalname, AV40ddo_FuncaoDados_DERTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,78);\"", 0, edtavDdo_funcaodados_dertitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SistemaFronteira.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_FUNCAODADOS_RLRContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'" + sPrefix + "',false,'" + sGXsfl_53_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Internalname, AV44ddo_FuncaoDados_RLRTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,80);\"", 0, edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SistemaFronteira.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_FUNCAODADOS_PFContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'" + sPrefix + "',false,'" + sGXsfl_53_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaodados_pftitlecontrolidtoreplace_Internalname, AV48ddo_FuncaoDados_PFTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,82);\"", 0, edtavDdo_funcaodados_pftitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SistemaFronteira.htm");
         }
         wbLoad = true;
      }

      protected void STARTDI2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Sistema Fronteira", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPDI0( ) ;
            }
         }
      }

      protected void WSDI2( )
      {
         STARTDI2( ) ;
         EVTDI2( ) ;
      }

      protected void EVTDI2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAODADOS_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11DI2 */
                                    E11DI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAODADOS_COMPLEXIDADE.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12DI2 */
                                    E12DI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAODADOS_DER.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13DI2 */
                                    E13DI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAODADOS_RLR.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14DI2 */
                                    E14DI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAODADOS_PF.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15DI2 */
                                    E15DI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16DI2 */
                                    E16DI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E17DI2 */
                                    E17DI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E18DI2 */
                                    E18DI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E19DI2 */
                                    E19DI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E20DI2 */
                                    E20DI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E21DI2 */
                                    E21DI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E22DI2 */
                                    E22DI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDI0( ) ;
                              }
                              sEvt = cgiGet( sPrefix+"GRIDPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDI0( ) ;
                              }
                              nGXsfl_53_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_53_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_53_idx), 4, 0)), 4, "0");
                              SubsflControlProps_532( ) ;
                              A368FuncaoDados_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoDados_Codigo_Internalname), ",", "."));
                              A370FuncaoDados_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoDados_SistemaCod_Internalname), ",", "."));
                              A391FuncaoDados_FuncaoDadosCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoDados_FuncaoDadosCod_Internalname), ",", "."));
                              n391FuncaoDados_FuncaoDadosCod = false;
                              A404FuncaoDados_FuncaoDadosSisCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoDados_FuncaoDadosSisCod_Internalname), ",", "."));
                              n404FuncaoDados_FuncaoDadosSisCod = false;
                              A369FuncaoDados_Nome = cgiGet( edtFuncaoDados_Nome_Internalname);
                              cmbFuncaoDados_Complexidade.Name = cmbFuncaoDados_Complexidade_Internalname;
                              cmbFuncaoDados_Complexidade.CurrentValue = cgiGet( cmbFuncaoDados_Complexidade_Internalname);
                              A376FuncaoDados_Complexidade = cgiGet( cmbFuncaoDados_Complexidade_Internalname);
                              A374FuncaoDados_DER = (short)(context.localUtil.CToN( cgiGet( edtFuncaoDados_DER_Internalname), ",", "."));
                              A375FuncaoDados_RLR = (short)(context.localUtil.CToN( cgiGet( edtFuncaoDados_RLR_Internalname), ",", "."));
                              A377FuncaoDados_PF = context.localUtil.CToN( cgiGet( edtFuncaoDados_PF_Internalname), ",", ".");
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E23DI2 */
                                          E23DI2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E24DI2 */
                                          E24DI2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E25DI2 */
                                          E25DI2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Funcaodados_nome1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAODADOS_NOME1"), AV17FuncaoDados_Nome1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Funcaodados_nome2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAODADOS_NOME2"), AV21FuncaoDados_Nome2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaodados_nome Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_NOME"), AV30TFFuncaoDados_Nome) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaodados_nome_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_NOME_SEL"), AV31TFFuncaoDados_Nome_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaodados_der Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_DER"), ",", ".") != Convert.ToDecimal( AV38TFFuncaoDados_DER )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaodados_der_to Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_DER_TO"), ",", ".") != Convert.ToDecimal( AV39TFFuncaoDados_DER_To )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaodados_rlr Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_RLR"), ",", ".") != Convert.ToDecimal( AV42TFFuncaoDados_RLR )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaodados_rlr_to Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_RLR_TO"), ",", ".") != Convert.ToDecimal( AV43TFFuncaoDados_RLR_To )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaodados_pf Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_PF"), ",", ".") != AV46TFFuncaoDados_PF )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaodados_pf_to Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_PF_TO"), ",", ".") != AV47TFFuncaoDados_PF_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPDI0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEDI2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormDI2( ) ;
            }
         }
      }

      protected void PADI2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            cmbavFuncaodados_tipo.Name = "vFUNCAODADOS_TIPO";
            cmbavFuncaodados_tipo.WebTags = "";
            cmbavFuncaodados_tipo.addItem("", "Todos", 0);
            cmbavFuncaodados_tipo.addItem("ALI", "ALI", 0);
            cmbavFuncaodados_tipo.addItem("AIE", "AIE", 0);
            cmbavFuncaodados_tipo.addItem("DC", "DC", 0);
            if ( cmbavFuncaodados_tipo.ItemCount > 0 )
            {
               AV26FuncaoDados_Tipo = cmbavFuncaodados_tipo.getValidValue(AV26FuncaoDados_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26FuncaoDados_Tipo", AV26FuncaoDados_Tipo);
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("FUNCAODADOS_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("FUNCAODADOS_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            GXCCtl = "FUNCAODADOS_COMPLEXIDADE_" + sGXsfl_53_idx;
            cmbFuncaoDados_Complexidade.Name = GXCCtl;
            cmbFuncaoDados_Complexidade.WebTags = "";
            cmbFuncaoDados_Complexidade.addItem("E", "", 0);
            cmbFuncaoDados_Complexidade.addItem("B", "Baixa", 0);
            cmbFuncaoDados_Complexidade.addItem("M", "M�dia", 0);
            cmbFuncaoDados_Complexidade.addItem("A", "Alta", 0);
            if ( cmbFuncaoDados_Complexidade.ItemCount > 0 )
            {
               A376FuncaoDados_Complexidade = cmbFuncaoDados_Complexidade.getValidValue(A376FuncaoDados_Complexidade);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_532( ) ;
         while ( nGXsfl_53_idx <= nRC_GXsfl_53 )
         {
            sendrow_532( ) ;
            nGXsfl_53_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_53_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_53_idx+1));
            sGXsfl_53_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_53_idx), 4, 0)), 4, "0");
            SubsflControlProps_532( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       String AV17FuncaoDados_Nome1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       String AV21FuncaoDados_Nome2 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       String AV30TFFuncaoDados_Nome ,
                                       String AV31TFFuncaoDados_Nome_Sel ,
                                       short AV38TFFuncaoDados_DER ,
                                       short AV39TFFuncaoDados_DER_To ,
                                       short AV42TFFuncaoDados_RLR ,
                                       short AV43TFFuncaoDados_RLR_To ,
                                       decimal AV46TFFuncaoDados_PF ,
                                       decimal AV47TFFuncaoDados_PF_To ,
                                       int AV7FuncaoDados_SistemaCod ,
                                       String AV32ddo_FuncaoDados_NomeTitleControlIdToReplace ,
                                       String AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace ,
                                       String AV40ddo_FuncaoDados_DERTitleControlIdToReplace ,
                                       String AV44ddo_FuncaoDados_RLRTitleControlIdToReplace ,
                                       String AV48ddo_FuncaoDados_PFTitleControlIdToReplace ,
                                       String AV52Pgmname ,
                                       String AV26FuncaoDados_Tipo ,
                                       IGxCollection AV35TFFuncaoDados_Complexidade_Sels ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       bool AV24DynamicFiltersIgnoreFirst ,
                                       bool AV23DynamicFiltersRemoving ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFDI2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A368FuncaoDados_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_SISTEMACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A370FuncaoDados_SistemaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A370FuncaoDados_SistemaCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_FUNCAODADOSCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A391FuncaoDados_FuncaoDadosCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_FUNCAODADOSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A369FuncaoDados_Nome, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_NOME", A369FuncaoDados_Nome);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_COMPLEXIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A376FuncaoDados_Complexidade, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_COMPLEXIDADE", StringUtil.RTrim( A376FuncaoDados_Complexidade));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_DER", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A374FuncaoDados_DER), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_DER", StringUtil.LTrim( StringUtil.NToC( (decimal)(A374FuncaoDados_DER), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_RLR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A375FuncaoDados_RLR), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_RLR", StringUtil.LTrim( StringUtil.NToC( (decimal)(A375FuncaoDados_RLR), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_PF", GetSecureSignedToken( sPrefix, context.localUtil.Format( A377FuncaoDados_PF, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_PF", StringUtil.LTrim( StringUtil.NToC( A377FuncaoDados_PF, 14, 5, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         if ( cmbavFuncaodados_tipo.ItemCount > 0 )
         {
            AV26FuncaoDados_Tipo = cmbavFuncaodados_tipo.getValidValue(AV26FuncaoDados_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26FuncaoDados_Tipo", AV26FuncaoDados_Tipo);
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFDI2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV52Pgmname = "SistemaFronteira";
         context.Gx_err = 0;
      }

      protected void RFDI2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 53;
         /* Execute user event: E24DI2 */
         E24DI2 ();
         nGXsfl_53_idx = 1;
         sGXsfl_53_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_53_idx), 4, 0)), 4, "0");
         SubsflControlProps_532( ) ;
         nGXsfl_53_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_532( ) ;
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A376FuncaoDados_Complexidade ,
                                                 AV35TFFuncaoDados_Complexidade_Sels ,
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV17FuncaoDados_Nome1 ,
                                                 AV19DynamicFiltersEnabled2 ,
                                                 AV20DynamicFiltersSelector2 ,
                                                 AV21FuncaoDados_Nome2 ,
                                                 AV31TFFuncaoDados_Nome_Sel ,
                                                 AV30TFFuncaoDados_Nome ,
                                                 A369FuncaoDados_Nome ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc ,
                                                 A370FuncaoDados_SistemaCod ,
                                                 AV7FuncaoDados_SistemaCod ,
                                                 A373FuncaoDados_Tipo ,
                                                 AV35TFFuncaoDados_Complexidade_Sels.Count ,
                                                 AV38TFFuncaoDados_DER ,
                                                 A374FuncaoDados_DER ,
                                                 AV39TFFuncaoDados_DER_To ,
                                                 AV42TFFuncaoDados_RLR ,
                                                 A375FuncaoDados_RLR ,
                                                 AV43TFFuncaoDados_RLR_To ,
                                                 AV46TFFuncaoDados_PF ,
                                                 A377FuncaoDados_PF ,
                                                 AV47TFFuncaoDados_PF_To },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT,
                                                 TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL
                                                 }
            });
            lV17FuncaoDados_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV17FuncaoDados_Nome1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17FuncaoDados_Nome1", AV17FuncaoDados_Nome1);
            lV21FuncaoDados_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV21FuncaoDados_Nome2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21FuncaoDados_Nome2", AV21FuncaoDados_Nome2);
            lV30TFFuncaoDados_Nome = StringUtil.Concat( StringUtil.RTrim( AV30TFFuncaoDados_Nome), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFFuncaoDados_Nome", AV30TFFuncaoDados_Nome);
            /* Using cursor H00DI2 */
            pr_default.execute(0, new Object[] {AV7FuncaoDados_SistemaCod, AV35TFFuncaoDados_Complexidade_Sels.Count, lV17FuncaoDados_Nome1, lV21FuncaoDados_Nome2, lV30TFFuncaoDados_Nome, AV31TFFuncaoDados_Nome_Sel});
            nGXsfl_53_idx = 1;
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) ) )
            {
               A373FuncaoDados_Tipo = H00DI2_A373FuncaoDados_Tipo[0];
               A369FuncaoDados_Nome = H00DI2_A369FuncaoDados_Nome[0];
               A404FuncaoDados_FuncaoDadosSisCod = H00DI2_A404FuncaoDados_FuncaoDadosSisCod[0];
               n404FuncaoDados_FuncaoDadosSisCod = H00DI2_n404FuncaoDados_FuncaoDadosSisCod[0];
               A391FuncaoDados_FuncaoDadosCod = H00DI2_A391FuncaoDados_FuncaoDadosCod[0];
               n391FuncaoDados_FuncaoDadosCod = H00DI2_n391FuncaoDados_FuncaoDadosCod[0];
               A370FuncaoDados_SistemaCod = H00DI2_A370FuncaoDados_SistemaCod[0];
               A755FuncaoDados_Contar = H00DI2_A755FuncaoDados_Contar[0];
               n755FuncaoDados_Contar = H00DI2_n755FuncaoDados_Contar[0];
               A368FuncaoDados_Codigo = H00DI2_A368FuncaoDados_Codigo[0];
               A404FuncaoDados_FuncaoDadosSisCod = H00DI2_A404FuncaoDados_FuncaoDadosSisCod[0];
               n404FuncaoDados_FuncaoDadosSisCod = H00DI2_n404FuncaoDados_FuncaoDadosSisCod[0];
               GXt_char1 = A376FuncaoDados_Complexidade;
               new prc_fdcomplexidade(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_char1) ;
               A376FuncaoDados_Complexidade = GXt_char1;
               if ( ( AV35TFFuncaoDados_Complexidade_Sels.Count <= 0 ) || ( (AV35TFFuncaoDados_Complexidade_Sels.IndexOf(StringUtil.RTrim( A376FuncaoDados_Complexidade))>0) ) )
               {
                  GXt_int2 = A374FuncaoDados_DER;
                  new prc_funcaodados_der(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int2) ;
                  A374FuncaoDados_DER = GXt_int2;
                  if ( (0==AV38TFFuncaoDados_DER) || ( ( A374FuncaoDados_DER >= AV38TFFuncaoDados_DER ) ) )
                  {
                     if ( (0==AV39TFFuncaoDados_DER_To) || ( ( A374FuncaoDados_DER <= AV39TFFuncaoDados_DER_To ) ) )
                     {
                        GXt_int2 = A375FuncaoDados_RLR;
                        new prc_funcaodados_rlr(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int2) ;
                        A375FuncaoDados_RLR = GXt_int2;
                        if ( (0==AV42TFFuncaoDados_RLR) || ( ( A375FuncaoDados_RLR >= AV42TFFuncaoDados_RLR ) ) )
                        {
                           if ( (0==AV43TFFuncaoDados_RLR_To) || ( ( A375FuncaoDados_RLR <= AV43TFFuncaoDados_RLR_To ) ) )
                           {
                              if ( A755FuncaoDados_Contar )
                              {
                                 GXt_int2 = (short)(A377FuncaoDados_PF);
                                 new prc_fdpf(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_int2) ;
                                 A377FuncaoDados_PF = (decimal)(GXt_int2);
                              }
                              else
                              {
                                 A377FuncaoDados_PF = 0;
                              }
                              if ( (Convert.ToDecimal(0)==AV46TFFuncaoDados_PF) || ( ( A377FuncaoDados_PF >= AV46TFFuncaoDados_PF ) ) )
                              {
                                 if ( (Convert.ToDecimal(0)==AV47TFFuncaoDados_PF_To) || ( ( A377FuncaoDados_PF <= AV47TFFuncaoDados_PF_To ) ) )
                                 {
                                    /* Execute user event: E25DI2 */
                                    E25DI2 ();
                                 }
                              }
                           }
                        }
                     }
                  }
               }
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 53;
            WBDI0( ) ;
         }
         nGXsfl_53_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17FuncaoDados_Nome1, AV20DynamicFiltersSelector2, AV21FuncaoDados_Nome2, AV19DynamicFiltersEnabled2, AV30TFFuncaoDados_Nome, AV31TFFuncaoDados_Nome_Sel, AV38TFFuncaoDados_DER, AV39TFFuncaoDados_DER_To, AV42TFFuncaoDados_RLR, AV43TFFuncaoDados_RLR_To, AV46TFFuncaoDados_PF, AV47TFFuncaoDados_PF_To, AV7FuncaoDados_SistemaCod, AV32ddo_FuncaoDados_NomeTitleControlIdToReplace, AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV40ddo_FuncaoDados_DERTitleControlIdToReplace, AV44ddo_FuncaoDados_RLRTitleControlIdToReplace, AV48ddo_FuncaoDados_PFTitleControlIdToReplace, AV52Pgmname, AV26FuncaoDados_Tipo, AV35TFFuncaoDados_Complexidade_Sels, AV11GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         if ( GRID_nEOF == 0 )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17FuncaoDados_Nome1, AV20DynamicFiltersSelector2, AV21FuncaoDados_Nome2, AV19DynamicFiltersEnabled2, AV30TFFuncaoDados_Nome, AV31TFFuncaoDados_Nome_Sel, AV38TFFuncaoDados_DER, AV39TFFuncaoDados_DER_To, AV42TFFuncaoDados_RLR, AV43TFFuncaoDados_RLR_To, AV46TFFuncaoDados_PF, AV47TFFuncaoDados_PF_To, AV7FuncaoDados_SistemaCod, AV32ddo_FuncaoDados_NomeTitleControlIdToReplace, AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV40ddo_FuncaoDados_DERTitleControlIdToReplace, AV44ddo_FuncaoDados_RLRTitleControlIdToReplace, AV48ddo_FuncaoDados_PFTitleControlIdToReplace, AV52Pgmname, AV26FuncaoDados_Tipo, AV35TFFuncaoDados_Complexidade_Sels, AV11GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17FuncaoDados_Nome1, AV20DynamicFiltersSelector2, AV21FuncaoDados_Nome2, AV19DynamicFiltersEnabled2, AV30TFFuncaoDados_Nome, AV31TFFuncaoDados_Nome_Sel, AV38TFFuncaoDados_DER, AV39TFFuncaoDados_DER_To, AV42TFFuncaoDados_RLR, AV43TFFuncaoDados_RLR_To, AV46TFFuncaoDados_PF, AV47TFFuncaoDados_PF_To, AV7FuncaoDados_SistemaCod, AV32ddo_FuncaoDados_NomeTitleControlIdToReplace, AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV40ddo_FuncaoDados_DERTitleControlIdToReplace, AV44ddo_FuncaoDados_RLRTitleControlIdToReplace, AV48ddo_FuncaoDados_PFTitleControlIdToReplace, AV52Pgmname, AV26FuncaoDados_Tipo, AV35TFFuncaoDados_Complexidade_Sels, AV11GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         subGrid_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17FuncaoDados_Nome1, AV20DynamicFiltersSelector2, AV21FuncaoDados_Nome2, AV19DynamicFiltersEnabled2, AV30TFFuncaoDados_Nome, AV31TFFuncaoDados_Nome_Sel, AV38TFFuncaoDados_DER, AV39TFFuncaoDados_DER_To, AV42TFFuncaoDados_RLR, AV43TFFuncaoDados_RLR_To, AV46TFFuncaoDados_PF, AV47TFFuncaoDados_PF_To, AV7FuncaoDados_SistemaCod, AV32ddo_FuncaoDados_NomeTitleControlIdToReplace, AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV40ddo_FuncaoDados_DERTitleControlIdToReplace, AV44ddo_FuncaoDados_RLRTitleControlIdToReplace, AV48ddo_FuncaoDados_PFTitleControlIdToReplace, AV52Pgmname, AV26FuncaoDados_Tipo, AV35TFFuncaoDados_Complexidade_Sels, AV11GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17FuncaoDados_Nome1, AV20DynamicFiltersSelector2, AV21FuncaoDados_Nome2, AV19DynamicFiltersEnabled2, AV30TFFuncaoDados_Nome, AV31TFFuncaoDados_Nome_Sel, AV38TFFuncaoDados_DER, AV39TFFuncaoDados_DER_To, AV42TFFuncaoDados_RLR, AV43TFFuncaoDados_RLR_To, AV46TFFuncaoDados_PF, AV47TFFuncaoDados_PF_To, AV7FuncaoDados_SistemaCod, AV32ddo_FuncaoDados_NomeTitleControlIdToReplace, AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV40ddo_FuncaoDados_DERTitleControlIdToReplace, AV44ddo_FuncaoDados_RLRTitleControlIdToReplace, AV48ddo_FuncaoDados_PFTitleControlIdToReplace, AV52Pgmname, AV26FuncaoDados_Tipo, AV35TFFuncaoDados_Complexidade_Sels, AV11GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPDI0( )
      {
         /* Before Start, stand alone formulas. */
         AV52Pgmname = "SistemaFronteira";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E23DI2 */
         E23DI2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV49DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vFUNCAODADOS_NOMETITLEFILTERDATA"), AV29FuncaoDados_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vFUNCAODADOS_COMPLEXIDADETITLEFILTERDATA"), AV33FuncaoDados_ComplexidadeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vFUNCAODADOS_DERTITLEFILTERDATA"), AV37FuncaoDados_DERTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vFUNCAODADOS_RLRTITLEFILTERDATA"), AV41FuncaoDados_RLRTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vFUNCAODADOS_PFTITLEFILTERDATA"), AV45FuncaoDados_PFTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV14OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            cmbavFuncaodados_tipo.Name = cmbavFuncaodados_tipo_Internalname;
            cmbavFuncaodados_tipo.CurrentValue = cgiGet( cmbavFuncaodados_tipo_Internalname);
            AV26FuncaoDados_Tipo = cgiGet( cmbavFuncaodados_tipo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26FuncaoDados_Tipo", AV26FuncaoDados_Tipo);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            AV17FuncaoDados_Nome1 = cgiGet( edtavFuncaodados_nome1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17FuncaoDados_Nome1", AV17FuncaoDados_Nome1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            AV21FuncaoDados_Nome2 = cgiGet( edtavFuncaodados_nome2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21FuncaoDados_Nome2", AV21FuncaoDados_Nome2);
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV30TFFuncaoDados_Nome = cgiGet( edtavTffuncaodados_nome_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFFuncaoDados_Nome", AV30TFFuncaoDados_Nome);
            AV31TFFuncaoDados_Nome_Sel = cgiGet( edtavTffuncaodados_nome_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TFFuncaoDados_Nome_Sel", AV31TFFuncaoDados_Nome_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_der_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_der_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAODADOS_DER");
               GX_FocusControl = edtavTffuncaodados_der_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38TFFuncaoDados_DER = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38TFFuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFFuncaoDados_DER), 4, 0)));
            }
            else
            {
               AV38TFFuncaoDados_DER = (short)(context.localUtil.CToN( cgiGet( edtavTffuncaodados_der_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38TFFuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFFuncaoDados_DER), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_der_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_der_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAODADOS_DER_TO");
               GX_FocusControl = edtavTffuncaodados_der_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39TFFuncaoDados_DER_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFFuncaoDados_DER_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFFuncaoDados_DER_To), 4, 0)));
            }
            else
            {
               AV39TFFuncaoDados_DER_To = (short)(context.localUtil.CToN( cgiGet( edtavTffuncaodados_der_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFFuncaoDados_DER_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFFuncaoDados_DER_To), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_rlr_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_rlr_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAODADOS_RLR");
               GX_FocusControl = edtavTffuncaodados_rlr_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV42TFFuncaoDados_RLR = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42TFFuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42TFFuncaoDados_RLR), 4, 0)));
            }
            else
            {
               AV42TFFuncaoDados_RLR = (short)(context.localUtil.CToN( cgiGet( edtavTffuncaodados_rlr_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42TFFuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42TFFuncaoDados_RLR), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_rlr_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_rlr_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAODADOS_RLR_TO");
               GX_FocusControl = edtavTffuncaodados_rlr_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV43TFFuncaoDados_RLR_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFFuncaoDados_RLR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFFuncaoDados_RLR_To), 4, 0)));
            }
            else
            {
               AV43TFFuncaoDados_RLR_To = (short)(context.localUtil.CToN( cgiGet( edtavTffuncaodados_rlr_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFFuncaoDados_RLR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFFuncaoDados_RLR_To), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_pf_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_pf_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAODADOS_PF");
               GX_FocusControl = edtavTffuncaodados_pf_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV46TFFuncaoDados_PF = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46TFFuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( AV46TFFuncaoDados_PF, 14, 5)));
            }
            else
            {
               AV46TFFuncaoDados_PF = context.localUtil.CToN( cgiGet( edtavTffuncaodados_pf_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46TFFuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( AV46TFFuncaoDados_PF, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_pf_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_pf_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAODADOS_PF_TO");
               GX_FocusControl = edtavTffuncaodados_pf_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV47TFFuncaoDados_PF_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFFuncaoDados_PF_To", StringUtil.LTrim( StringUtil.Str( AV47TFFuncaoDados_PF_To, 14, 5)));
            }
            else
            {
               AV47TFFuncaoDados_PF_To = context.localUtil.CToN( cgiGet( edtavTffuncaodados_pf_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFFuncaoDados_PF_To", StringUtil.LTrim( StringUtil.Str( AV47TFFuncaoDados_PF_To, 14, 5)));
            }
            AV32ddo_FuncaoDados_NomeTitleControlIdToReplace = cgiGet( edtavDdo_funcaodados_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ddo_FuncaoDados_NomeTitleControlIdToReplace", AV32ddo_FuncaoDados_NomeTitleControlIdToReplace);
            AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace = cgiGet( edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace", AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace);
            AV40ddo_FuncaoDados_DERTitleControlIdToReplace = cgiGet( edtavDdo_funcaodados_dertitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40ddo_FuncaoDados_DERTitleControlIdToReplace", AV40ddo_FuncaoDados_DERTitleControlIdToReplace);
            AV44ddo_FuncaoDados_RLRTitleControlIdToReplace = cgiGet( edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44ddo_FuncaoDados_RLRTitleControlIdToReplace", AV44ddo_FuncaoDados_RLRTitleControlIdToReplace);
            AV48ddo_FuncaoDados_PFTitleControlIdToReplace = cgiGet( edtavDdo_funcaodados_pftitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48ddo_FuncaoDados_PFTitleControlIdToReplace", AV48ddo_FuncaoDados_PFTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_53 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_53"), ",", "."));
            wcpOAV7FuncaoDados_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7FuncaoDados_SistemaCod"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_funcaodados_nome_Caption = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Caption");
            Ddo_funcaodados_nome_Tooltip = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Tooltip");
            Ddo_funcaodados_nome_Cls = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Cls");
            Ddo_funcaodados_nome_Filteredtext_set = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Filteredtext_set");
            Ddo_funcaodados_nome_Selectedvalue_set = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Selectedvalue_set");
            Ddo_funcaodados_nome_Dropdownoptionstype = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Dropdownoptionstype");
            Ddo_funcaodados_nome_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Titlecontrolidtoreplace");
            Ddo_funcaodados_nome_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Includesortasc"));
            Ddo_funcaodados_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Includesortdsc"));
            Ddo_funcaodados_nome_Sortedstatus = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Sortedstatus");
            Ddo_funcaodados_nome_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Includefilter"));
            Ddo_funcaodados_nome_Filtertype = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Filtertype");
            Ddo_funcaodados_nome_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Filterisrange"));
            Ddo_funcaodados_nome_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Includedatalist"));
            Ddo_funcaodados_nome_Datalisttype = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Datalisttype");
            Ddo_funcaodados_nome_Datalistfixedvalues = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Datalistfixedvalues");
            Ddo_funcaodados_nome_Datalistproc = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Datalistproc");
            Ddo_funcaodados_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaodados_nome_Sortasc = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Sortasc");
            Ddo_funcaodados_nome_Sortdsc = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Sortdsc");
            Ddo_funcaodados_nome_Loadingdata = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Loadingdata");
            Ddo_funcaodados_nome_Cleanfilter = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Cleanfilter");
            Ddo_funcaodados_nome_Rangefilterfrom = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Rangefilterfrom");
            Ddo_funcaodados_nome_Rangefilterto = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Rangefilterto");
            Ddo_funcaodados_nome_Noresultsfound = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Noresultsfound");
            Ddo_funcaodados_nome_Searchbuttontext = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Searchbuttontext");
            Ddo_funcaodados_complexidade_Caption = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Caption");
            Ddo_funcaodados_complexidade_Tooltip = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Tooltip");
            Ddo_funcaodados_complexidade_Cls = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Cls");
            Ddo_funcaodados_complexidade_Selectedvalue_set = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Selectedvalue_set");
            Ddo_funcaodados_complexidade_Dropdownoptionstype = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Dropdownoptionstype");
            Ddo_funcaodados_complexidade_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Titlecontrolidtoreplace");
            Ddo_funcaodados_complexidade_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Includesortasc"));
            Ddo_funcaodados_complexidade_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Includesortdsc"));
            Ddo_funcaodados_complexidade_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Includefilter"));
            Ddo_funcaodados_complexidade_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Filterisrange"));
            Ddo_funcaodados_complexidade_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Includedatalist"));
            Ddo_funcaodados_complexidade_Datalisttype = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Datalisttype");
            Ddo_funcaodados_complexidade_Allowmultipleselection = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Allowmultipleselection"));
            Ddo_funcaodados_complexidade_Datalistfixedvalues = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Datalistfixedvalues");
            Ddo_funcaodados_complexidade_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaodados_complexidade_Sortasc = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Sortasc");
            Ddo_funcaodados_complexidade_Sortdsc = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Sortdsc");
            Ddo_funcaodados_complexidade_Loadingdata = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Loadingdata");
            Ddo_funcaodados_complexidade_Cleanfilter = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Cleanfilter");
            Ddo_funcaodados_complexidade_Rangefilterfrom = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Rangefilterfrom");
            Ddo_funcaodados_complexidade_Rangefilterto = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Rangefilterto");
            Ddo_funcaodados_complexidade_Noresultsfound = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Noresultsfound");
            Ddo_funcaodados_complexidade_Searchbuttontext = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Searchbuttontext");
            Ddo_funcaodados_der_Caption = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Caption");
            Ddo_funcaodados_der_Tooltip = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Tooltip");
            Ddo_funcaodados_der_Cls = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Cls");
            Ddo_funcaodados_der_Filteredtext_set = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Filteredtext_set");
            Ddo_funcaodados_der_Filteredtextto_set = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Filteredtextto_set");
            Ddo_funcaodados_der_Dropdownoptionstype = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Dropdownoptionstype");
            Ddo_funcaodados_der_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Titlecontrolidtoreplace");
            Ddo_funcaodados_der_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Includesortasc"));
            Ddo_funcaodados_der_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Includesortdsc"));
            Ddo_funcaodados_der_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Includefilter"));
            Ddo_funcaodados_der_Filtertype = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Filtertype");
            Ddo_funcaodados_der_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Filterisrange"));
            Ddo_funcaodados_der_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Includedatalist"));
            Ddo_funcaodados_der_Datalistfixedvalues = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Datalistfixedvalues");
            Ddo_funcaodados_der_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaodados_der_Sortasc = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Sortasc");
            Ddo_funcaodados_der_Sortdsc = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Sortdsc");
            Ddo_funcaodados_der_Loadingdata = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Loadingdata");
            Ddo_funcaodados_der_Cleanfilter = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Cleanfilter");
            Ddo_funcaodados_der_Rangefilterfrom = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Rangefilterfrom");
            Ddo_funcaodados_der_Rangefilterto = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Rangefilterto");
            Ddo_funcaodados_der_Noresultsfound = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Noresultsfound");
            Ddo_funcaodados_der_Searchbuttontext = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Searchbuttontext");
            Ddo_funcaodados_rlr_Caption = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Caption");
            Ddo_funcaodados_rlr_Tooltip = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Tooltip");
            Ddo_funcaodados_rlr_Cls = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Cls");
            Ddo_funcaodados_rlr_Filteredtext_set = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Filteredtext_set");
            Ddo_funcaodados_rlr_Filteredtextto_set = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Filteredtextto_set");
            Ddo_funcaodados_rlr_Dropdownoptionstype = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Dropdownoptionstype");
            Ddo_funcaodados_rlr_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Titlecontrolidtoreplace");
            Ddo_funcaodados_rlr_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Includesortasc"));
            Ddo_funcaodados_rlr_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Includesortdsc"));
            Ddo_funcaodados_rlr_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Includefilter"));
            Ddo_funcaodados_rlr_Filtertype = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Filtertype");
            Ddo_funcaodados_rlr_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Filterisrange"));
            Ddo_funcaodados_rlr_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Includedatalist"));
            Ddo_funcaodados_rlr_Datalistfixedvalues = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Datalistfixedvalues");
            Ddo_funcaodados_rlr_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaodados_rlr_Sortasc = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Sortasc");
            Ddo_funcaodados_rlr_Sortdsc = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Sortdsc");
            Ddo_funcaodados_rlr_Loadingdata = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Loadingdata");
            Ddo_funcaodados_rlr_Cleanfilter = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Cleanfilter");
            Ddo_funcaodados_rlr_Rangefilterfrom = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Rangefilterfrom");
            Ddo_funcaodados_rlr_Rangefilterto = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Rangefilterto");
            Ddo_funcaodados_rlr_Noresultsfound = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Noresultsfound");
            Ddo_funcaodados_rlr_Searchbuttontext = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Searchbuttontext");
            Ddo_funcaodados_pf_Caption = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Caption");
            Ddo_funcaodados_pf_Tooltip = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Tooltip");
            Ddo_funcaodados_pf_Cls = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Cls");
            Ddo_funcaodados_pf_Filteredtext_set = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Filteredtext_set");
            Ddo_funcaodados_pf_Filteredtextto_set = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Filteredtextto_set");
            Ddo_funcaodados_pf_Dropdownoptionstype = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Dropdownoptionstype");
            Ddo_funcaodados_pf_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Titlecontrolidtoreplace");
            Ddo_funcaodados_pf_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Includesortasc"));
            Ddo_funcaodados_pf_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Includesortdsc"));
            Ddo_funcaodados_pf_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Includefilter"));
            Ddo_funcaodados_pf_Filtertype = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Filtertype");
            Ddo_funcaodados_pf_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Filterisrange"));
            Ddo_funcaodados_pf_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Includedatalist"));
            Ddo_funcaodados_pf_Datalistfixedvalues = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Datalistfixedvalues");
            Ddo_funcaodados_pf_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaodados_pf_Sortasc = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Sortasc");
            Ddo_funcaodados_pf_Sortdsc = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Sortdsc");
            Ddo_funcaodados_pf_Loadingdata = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Loadingdata");
            Ddo_funcaodados_pf_Cleanfilter = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Cleanfilter");
            Ddo_funcaodados_pf_Rangefilterfrom = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Rangefilterfrom");
            Ddo_funcaodados_pf_Rangefilterto = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Rangefilterto");
            Ddo_funcaodados_pf_Noresultsfound = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Noresultsfound");
            Ddo_funcaodados_pf_Searchbuttontext = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Searchbuttontext");
            Ddo_funcaodados_nome_Activeeventkey = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Activeeventkey");
            Ddo_funcaodados_nome_Filteredtext_get = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Filteredtext_get");
            Ddo_funcaodados_nome_Selectedvalue_get = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Selectedvalue_get");
            Ddo_funcaodados_complexidade_Activeeventkey = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Activeeventkey");
            Ddo_funcaodados_complexidade_Selectedvalue_get = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Selectedvalue_get");
            Ddo_funcaodados_der_Activeeventkey = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Activeeventkey");
            Ddo_funcaodados_der_Filteredtext_get = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Filteredtext_get");
            Ddo_funcaodados_der_Filteredtextto_get = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Filteredtextto_get");
            Ddo_funcaodados_rlr_Activeeventkey = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Activeeventkey");
            Ddo_funcaodados_rlr_Filteredtext_get = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Filteredtext_get");
            Ddo_funcaodados_rlr_Filteredtextto_get = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Filteredtextto_get");
            Ddo_funcaodados_pf_Activeeventkey = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Activeeventkey");
            Ddo_funcaodados_pf_Filteredtext_get = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Filteredtext_get");
            Ddo_funcaodados_pf_Filteredtextto_get = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAODADOS_NOME1"), AV17FuncaoDados_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAODADOS_NOME2"), AV21FuncaoDados_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_NOME"), AV30TFFuncaoDados_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_NOME_SEL"), AV31TFFuncaoDados_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_DER"), ",", ".") != Convert.ToDecimal( AV38TFFuncaoDados_DER )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_DER_TO"), ",", ".") != Convert.ToDecimal( AV39TFFuncaoDados_DER_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_RLR"), ",", ".") != Convert.ToDecimal( AV42TFFuncaoDados_RLR )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_RLR_TO"), ",", ".") != Convert.ToDecimal( AV43TFFuncaoDados_RLR_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_PF"), ",", ".") != AV46TFFuncaoDados_PF )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_PF_TO"), ",", ".") != AV47TFFuncaoDados_PF_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E23DI2 */
         E23DI2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E23DI2( )
      {
         /* Start Routine */
         subGrid_Rows = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         AV16DynamicFiltersSelector1 = "FUNCAODADOS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersSelector2 = "FUNCAODADOS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         AV26FuncaoDados_Tipo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26FuncaoDados_Tipo", AV26FuncaoDados_Tipo);
         edtavTffuncaodados_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaodados_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_nome_Visible), 5, 0)));
         edtavTffuncaodados_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaodados_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_nome_sel_Visible), 5, 0)));
         edtavTffuncaodados_der_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaodados_der_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_der_Visible), 5, 0)));
         edtavTffuncaodados_der_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaodados_der_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_der_to_Visible), 5, 0)));
         edtavTffuncaodados_rlr_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaodados_rlr_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_rlr_Visible), 5, 0)));
         edtavTffuncaodados_rlr_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaodados_rlr_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_rlr_to_Visible), 5, 0)));
         edtavTffuncaodados_pf_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaodados_pf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_pf_Visible), 5, 0)));
         edtavTffuncaodados_pf_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaodados_pf_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_pf_to_Visible), 5, 0)));
         Ddo_funcaodados_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoDados_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_nome_Internalname, "TitleControlIdToReplace", Ddo_funcaodados_nome_Titlecontrolidtoreplace);
         AV32ddo_FuncaoDados_NomeTitleControlIdToReplace = Ddo_funcaodados_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ddo_FuncaoDados_NomeTitleControlIdToReplace", AV32ddo_FuncaoDados_NomeTitleControlIdToReplace);
         edtavDdo_funcaodados_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_funcaodados_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaodados_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaodados_complexidade_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoDados_Complexidade";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_complexidade_Internalname, "TitleControlIdToReplace", Ddo_funcaodados_complexidade_Titlecontrolidtoreplace);
         AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace = Ddo_funcaodados_complexidade_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace", AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace);
         edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaodados_der_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoDados_DER";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_der_Internalname, "TitleControlIdToReplace", Ddo_funcaodados_der_Titlecontrolidtoreplace);
         AV40ddo_FuncaoDados_DERTitleControlIdToReplace = Ddo_funcaodados_der_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40ddo_FuncaoDados_DERTitleControlIdToReplace", AV40ddo_FuncaoDados_DERTitleControlIdToReplace);
         edtavDdo_funcaodados_dertitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_funcaodados_dertitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaodados_dertitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaodados_rlr_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoDados_RLR";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_rlr_Internalname, "TitleControlIdToReplace", Ddo_funcaodados_rlr_Titlecontrolidtoreplace);
         AV44ddo_FuncaoDados_RLRTitleControlIdToReplace = Ddo_funcaodados_rlr_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44ddo_FuncaoDados_RLRTitleControlIdToReplace", AV44ddo_FuncaoDados_RLRTitleControlIdToReplace);
         edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaodados_pf_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoDados_PF";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_pf_Internalname, "TitleControlIdToReplace", Ddo_funcaodados_pf_Titlecontrolidtoreplace);
         AV48ddo_FuncaoDados_PFTitleControlIdToReplace = Ddo_funcaodados_pf_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48ddo_FuncaoDados_PFTitleControlIdToReplace", AV48ddo_FuncaoDados_PFTitleControlIdToReplace);
         edtavDdo_funcaodados_pftitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_funcaodados_pftitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaodados_pftitlecontrolidtoreplace_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Cronologicamente", 0);
         cmbavOrderedby.addItem("2", "Nome", 0);
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons3 = AV49DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons3) ;
         AV49DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons3;
      }

      protected void E24DI2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV29FuncaoDados_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV33FuncaoDados_ComplexidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV37FuncaoDados_DERTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV41FuncaoDados_RLRTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV45FuncaoDados_PFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtFuncaoDados_Nome_Titleformat = 2;
         edtFuncaoDados_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV32ddo_FuncaoDados_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoDados_Nome_Internalname, "Title", edtFuncaoDados_Nome_Title);
         cmbFuncaoDados_Complexidade_Titleformat = 2;
         cmbFuncaoDados_Complexidade.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Complexidade", AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbFuncaoDados_Complexidade_Internalname, "Title", cmbFuncaoDados_Complexidade.Title.Text);
         edtFuncaoDados_DER_Titleformat = 2;
         edtFuncaoDados_DER_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "DER", AV40ddo_FuncaoDados_DERTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoDados_DER_Internalname, "Title", edtFuncaoDados_DER_Title);
         edtFuncaoDados_RLR_Titleformat = 2;
         edtFuncaoDados_RLR_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "RLR", AV44ddo_FuncaoDados_RLRTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoDados_RLR_Internalname, "Title", edtFuncaoDados_RLR_Title);
         edtFuncaoDados_PF_Titleformat = 2;
         edtFuncaoDados_PF_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "PF", AV48ddo_FuncaoDados_PFTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoDados_PF_Internalname, "Title", edtFuncaoDados_PF_Title);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV29FuncaoDados_NomeTitleFilterData", AV29FuncaoDados_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV33FuncaoDados_ComplexidadeTitleFilterData", AV33FuncaoDados_ComplexidadeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV37FuncaoDados_DERTitleFilterData", AV37FuncaoDados_DERTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV41FuncaoDados_RLRTitleFilterData", AV41FuncaoDados_RLRTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV45FuncaoDados_PFTitleFilterData", AV45FuncaoDados_PFTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E11DI2( )
      {
         /* Ddo_funcaodados_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaodados_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcaodados_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_nome_Internalname, "SortedStatus", Ddo_funcaodados_nome_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_funcaodados_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcaodados_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_nome_Internalname, "SortedStatus", Ddo_funcaodados_nome_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_funcaodados_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV30TFFuncaoDados_Nome = Ddo_funcaodados_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFFuncaoDados_Nome", AV30TFFuncaoDados_Nome);
            AV31TFFuncaoDados_Nome_Sel = Ddo_funcaodados_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TFFuncaoDados_Nome_Sel", AV31TFFuncaoDados_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E12DI2( )
      {
         /* Ddo_funcaodados_complexidade_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaodados_complexidade_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV34TFFuncaoDados_Complexidade_SelsJson = Ddo_funcaodados_complexidade_Selectedvalue_get;
            AV35TFFuncaoDados_Complexidade_Sels.FromJSonString(AV34TFFuncaoDados_Complexidade_SelsJson);
            subgrid_firstpage( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV35TFFuncaoDados_Complexidade_Sels", AV35TFFuncaoDados_Complexidade_Sels);
      }

      protected void E13DI2( )
      {
         /* Ddo_funcaodados_der_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaodados_der_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV38TFFuncaoDados_DER = (short)(NumberUtil.Val( Ddo_funcaodados_der_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38TFFuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFFuncaoDados_DER), 4, 0)));
            AV39TFFuncaoDados_DER_To = (short)(NumberUtil.Val( Ddo_funcaodados_der_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFFuncaoDados_DER_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFFuncaoDados_DER_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E14DI2( )
      {
         /* Ddo_funcaodados_rlr_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaodados_rlr_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV42TFFuncaoDados_RLR = (short)(NumberUtil.Val( Ddo_funcaodados_rlr_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42TFFuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42TFFuncaoDados_RLR), 4, 0)));
            AV43TFFuncaoDados_RLR_To = (short)(NumberUtil.Val( Ddo_funcaodados_rlr_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFFuncaoDados_RLR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFFuncaoDados_RLR_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E15DI2( )
      {
         /* Ddo_funcaodados_pf_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaodados_pf_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV46TFFuncaoDados_PF = NumberUtil.Val( Ddo_funcaodados_pf_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46TFFuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( AV46TFFuncaoDados_PF, 14, 5)));
            AV47TFFuncaoDados_PF_To = NumberUtil.Val( Ddo_funcaodados_pf_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFFuncaoDados_PF_To", StringUtil.LTrim( StringUtil.Str( AV47TFFuncaoDados_PF_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
      }

      private void E25DI2( )
      {
         /* Grid_Load Routine */
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 53;
         }
         if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
         {
            sendrow_532( ) ;
            GRID_nEOF = 1;
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
            {
               GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
            }
         }
         if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
         {
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
         }
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_53_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(53, GridRow);
         }
      }

      protected void E16DI2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E20DI2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E17DI2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV24DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV24DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17FuncaoDados_Nome1, AV20DynamicFiltersSelector2, AV21FuncaoDados_Nome2, AV19DynamicFiltersEnabled2, AV30TFFuncaoDados_Nome, AV31TFFuncaoDados_Nome_Sel, AV38TFFuncaoDados_DER, AV39TFFuncaoDados_DER_To, AV42TFFuncaoDados_RLR, AV43TFFuncaoDados_RLR_To, AV46TFFuncaoDados_PF, AV47TFFuncaoDados_PF_To, AV7FuncaoDados_SistemaCod, AV32ddo_FuncaoDados_NomeTitleControlIdToReplace, AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV40ddo_FuncaoDados_DERTitleControlIdToReplace, AV44ddo_FuncaoDados_RLRTitleControlIdToReplace, AV48ddo_FuncaoDados_PFTitleControlIdToReplace, AV52Pgmname, AV26FuncaoDados_Tipo, AV35TFFuncaoDados_Complexidade_Sels, AV11GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E21DI2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18DI2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17FuncaoDados_Nome1, AV20DynamicFiltersSelector2, AV21FuncaoDados_Nome2, AV19DynamicFiltersEnabled2, AV30TFFuncaoDados_Nome, AV31TFFuncaoDados_Nome_Sel, AV38TFFuncaoDados_DER, AV39TFFuncaoDados_DER_To, AV42TFFuncaoDados_RLR, AV43TFFuncaoDados_RLR_To, AV46TFFuncaoDados_PF, AV47TFFuncaoDados_PF_To, AV7FuncaoDados_SistemaCod, AV32ddo_FuncaoDados_NomeTitleControlIdToReplace, AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV40ddo_FuncaoDados_DERTitleControlIdToReplace, AV44ddo_FuncaoDados_RLRTitleControlIdToReplace, AV48ddo_FuncaoDados_PFTitleControlIdToReplace, AV52Pgmname, AV26FuncaoDados_Tipo, AV35TFFuncaoDados_Complexidade_Sels, AV11GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E22DI2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19DI2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         cmbavFuncaodados_tipo.CurrentValue = StringUtil.RTrim( AV26FuncaoDados_Tipo);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaodados_tipo_Internalname, "Values", cmbavFuncaodados_tipo.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV35TFFuncaoDados_Complexidade_Sels", AV35TFFuncaoDados_Complexidade_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_funcaodados_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_nome_Internalname, "SortedStatus", Ddo_funcaodados_nome_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV14OrderedBy == 2 )
         {
            Ddo_funcaodados_nome_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_nome_Internalname, "SortedStatus", Ddo_funcaodados_nome_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavFuncaodados_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaodados_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_nome1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAODADOS_NOME") == 0 )
         {
            edtavFuncaodados_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaodados_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_nome1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavFuncaodados_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaodados_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_nome2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "FUNCAODADOS_NOME") == 0 )
         {
            edtavFuncaodados_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaodados_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_nome2_Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "FUNCAODADOS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV21FuncaoDados_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21FuncaoDados_Nome2", AV21FuncaoDados_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S212( )
      {
         /* 'CLEANFILTERS' Routine */
         AV26FuncaoDados_Tipo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26FuncaoDados_Tipo", AV26FuncaoDados_Tipo);
         AV30TFFuncaoDados_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFFuncaoDados_Nome", AV30TFFuncaoDados_Nome);
         Ddo_funcaodados_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_nome_Internalname, "FilteredText_set", Ddo_funcaodados_nome_Filteredtext_set);
         AV31TFFuncaoDados_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TFFuncaoDados_Nome_Sel", AV31TFFuncaoDados_Nome_Sel);
         Ddo_funcaodados_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_nome_Internalname, "SelectedValue_set", Ddo_funcaodados_nome_Selectedvalue_set);
         AV35TFFuncaoDados_Complexidade_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_funcaodados_complexidade_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_complexidade_Internalname, "SelectedValue_set", Ddo_funcaodados_complexidade_Selectedvalue_set);
         AV38TFFuncaoDados_DER = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38TFFuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFFuncaoDados_DER), 4, 0)));
         Ddo_funcaodados_der_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_der_Internalname, "FilteredText_set", Ddo_funcaodados_der_Filteredtext_set);
         AV39TFFuncaoDados_DER_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFFuncaoDados_DER_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFFuncaoDados_DER_To), 4, 0)));
         Ddo_funcaodados_der_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_der_Internalname, "FilteredTextTo_set", Ddo_funcaodados_der_Filteredtextto_set);
         AV42TFFuncaoDados_RLR = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42TFFuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42TFFuncaoDados_RLR), 4, 0)));
         Ddo_funcaodados_rlr_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_rlr_Internalname, "FilteredText_set", Ddo_funcaodados_rlr_Filteredtext_set);
         AV43TFFuncaoDados_RLR_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFFuncaoDados_RLR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFFuncaoDados_RLR_To), 4, 0)));
         Ddo_funcaodados_rlr_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_rlr_Internalname, "FilteredTextTo_set", Ddo_funcaodados_rlr_Filteredtextto_set);
         AV46TFFuncaoDados_PF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46TFFuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( AV46TFFuncaoDados_PF, 14, 5)));
         Ddo_funcaodados_pf_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_pf_Internalname, "FilteredText_set", Ddo_funcaodados_pf_Filteredtext_set);
         AV47TFFuncaoDados_PF_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFFuncaoDados_PF_To", StringUtil.LTrim( StringUtil.Str( AV47TFFuncaoDados_PF_To, 14, 5)));
         Ddo_funcaodados_pf_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_pf_Internalname, "FilteredTextTo_set", Ddo_funcaodados_pf_Filteredtextto_set);
         AV16DynamicFiltersSelector1 = "FUNCAODADOS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV17FuncaoDados_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17FuncaoDados_Nome1", AV17FuncaoDados_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV25Session.Get(AV52Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV52Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV25Session.Get(AV52Pgmname+"GridState"), "");
         }
         AV14OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         AV15OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV53GXV1 = 1;
         while ( AV53GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV53GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "FUNCAODADOS_TIPO") == 0 )
            {
               AV26FuncaoDados_Tipo = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26FuncaoDados_Tipo", AV26FuncaoDados_Tipo);
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_NOME") == 0 )
            {
               AV30TFFuncaoDados_Nome = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFFuncaoDados_Nome", AV30TFFuncaoDados_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30TFFuncaoDados_Nome)) )
               {
                  Ddo_funcaodados_nome_Filteredtext_set = AV30TFFuncaoDados_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_nome_Internalname, "FilteredText_set", Ddo_funcaodados_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_NOME_SEL") == 0 )
            {
               AV31TFFuncaoDados_Nome_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TFFuncaoDados_Nome_Sel", AV31TFFuncaoDados_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31TFFuncaoDados_Nome_Sel)) )
               {
                  Ddo_funcaodados_nome_Selectedvalue_set = AV31TFFuncaoDados_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_nome_Internalname, "SelectedValue_set", Ddo_funcaodados_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_COMPLEXIDADE_SEL") == 0 )
            {
               AV34TFFuncaoDados_Complexidade_SelsJson = AV12GridStateFilterValue.gxTpr_Value;
               AV35TFFuncaoDados_Complexidade_Sels.FromJSonString(AV34TFFuncaoDados_Complexidade_SelsJson);
               if ( ! ( AV35TFFuncaoDados_Complexidade_Sels.Count == 0 ) )
               {
                  Ddo_funcaodados_complexidade_Selectedvalue_set = AV34TFFuncaoDados_Complexidade_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_complexidade_Internalname, "SelectedValue_set", Ddo_funcaodados_complexidade_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_DER") == 0 )
            {
               AV38TFFuncaoDados_DER = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38TFFuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFFuncaoDados_DER), 4, 0)));
               AV39TFFuncaoDados_DER_To = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFFuncaoDados_DER_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFFuncaoDados_DER_To), 4, 0)));
               if ( ! (0==AV38TFFuncaoDados_DER) )
               {
                  Ddo_funcaodados_der_Filteredtext_set = StringUtil.Str( (decimal)(AV38TFFuncaoDados_DER), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_der_Internalname, "FilteredText_set", Ddo_funcaodados_der_Filteredtext_set);
               }
               if ( ! (0==AV39TFFuncaoDados_DER_To) )
               {
                  Ddo_funcaodados_der_Filteredtextto_set = StringUtil.Str( (decimal)(AV39TFFuncaoDados_DER_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_der_Internalname, "FilteredTextTo_set", Ddo_funcaodados_der_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_RLR") == 0 )
            {
               AV42TFFuncaoDados_RLR = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42TFFuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42TFFuncaoDados_RLR), 4, 0)));
               AV43TFFuncaoDados_RLR_To = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFFuncaoDados_RLR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFFuncaoDados_RLR_To), 4, 0)));
               if ( ! (0==AV42TFFuncaoDados_RLR) )
               {
                  Ddo_funcaodados_rlr_Filteredtext_set = StringUtil.Str( (decimal)(AV42TFFuncaoDados_RLR), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_rlr_Internalname, "FilteredText_set", Ddo_funcaodados_rlr_Filteredtext_set);
               }
               if ( ! (0==AV43TFFuncaoDados_RLR_To) )
               {
                  Ddo_funcaodados_rlr_Filteredtextto_set = StringUtil.Str( (decimal)(AV43TFFuncaoDados_RLR_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_rlr_Internalname, "FilteredTextTo_set", Ddo_funcaodados_rlr_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_PF") == 0 )
            {
               AV46TFFuncaoDados_PF = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46TFFuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( AV46TFFuncaoDados_PF, 14, 5)));
               AV47TFFuncaoDados_PF_To = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFFuncaoDados_PF_To", StringUtil.LTrim( StringUtil.Str( AV47TFFuncaoDados_PF_To, 14, 5)));
               if ( ! (Convert.ToDecimal(0)==AV46TFFuncaoDados_PF) )
               {
                  Ddo_funcaodados_pf_Filteredtext_set = StringUtil.Str( AV46TFFuncaoDados_PF, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_pf_Internalname, "FilteredText_set", Ddo_funcaodados_pf_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV47TFFuncaoDados_PF_To) )
               {
                  Ddo_funcaodados_pf_Filteredtextto_set = StringUtil.Str( AV47TFFuncaoDados_PF_To, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_pf_Internalname, "FilteredTextTo_set", Ddo_funcaodados_pf_Filteredtextto_set);
               }
            }
            AV53GXV1 = (int)(AV53GXV1+1);
         }
      }

      protected void S202( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAODADOS_NOME") == 0 )
            {
               AV17FuncaoDados_Nome1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17FuncaoDados_Nome1", AV17FuncaoDados_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV13GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "FUNCAODADOS_NOME") == 0 )
               {
                  AV21FuncaoDados_Nome2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21FuncaoDados_Nome2", AV21FuncaoDados_Nome2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV23DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV25Session.Get(AV52Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26FuncaoDados_Tipo)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "FUNCAODADOS_TIPO";
            AV12GridStateFilterValue.gxTpr_Value = AV26FuncaoDados_Tipo;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30TFFuncaoDados_Nome)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_NOME";
            AV12GridStateFilterValue.gxTpr_Value = AV30TFFuncaoDados_Nome;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31TFFuncaoDados_Nome_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_NOME_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV31TFFuncaoDados_Nome_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( AV35TFFuncaoDados_Complexidade_Sels.Count == 0 ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_COMPLEXIDADE_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV35TFFuncaoDados_Complexidade_Sels.ToJSonString(false);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV38TFFuncaoDados_DER) && (0==AV39TFFuncaoDados_DER_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_DER";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV38TFFuncaoDados_DER), 4, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV39TFFuncaoDados_DER_To), 4, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV42TFFuncaoDados_RLR) && (0==AV43TFFuncaoDados_RLR_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_RLR";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV42TFFuncaoDados_RLR), 4, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV43TFFuncaoDados_RLR_To), 4, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV46TFFuncaoDados_PF) && (Convert.ToDecimal(0)==AV47TFFuncaoDados_PF_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_PF";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV46TFFuncaoDados_PF, 14, 5);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV47TFFuncaoDados_PF_To, 14, 5);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7FuncaoDados_SistemaCod) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&FUNCAODADOS_SISTEMACOD";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7FuncaoDados_SistemaCod), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV52Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV24DynamicFiltersIgnoreFirst )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAODADOS_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17FuncaoDados_Nome1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV17FuncaoDados_Nome1;
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "FUNCAODADOS_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21FuncaoDados_Nome2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV21FuncaoDados_Nome2;
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S132( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV52Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "FuncaoDados";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "FuncaoDados_SistemaCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7FuncaoDados_SistemaCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV25Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_DI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table2_5_DI2( true) ;
         }
         else
         {
            wb_table2_5_DI2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_DI2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"53\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo Sistema") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Fun��o de Dados Externa") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Funcao Dados_Funcao Dados Sis Cod") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(450), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoDados_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoDados_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoDados_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbFuncaoDados_Complexidade_Titleformat == 0 )
               {
                  context.SendWebValue( cmbFuncaoDados_Complexidade.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbFuncaoDados_Complexidade.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoDados_DER_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoDados_DER_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoDados_DER_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoDados_RLR_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoDados_RLR_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoDados_RLR_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoDados_PF_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoDados_PF_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoDados_PF_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A368FuncaoDados_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A370FuncaoDados_SistemaCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A404FuncaoDados_FuncaoDadosSisCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A369FuncaoDados_Nome);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoDados_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoDados_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A376FuncaoDados_Complexidade));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbFuncaoDados_Complexidade.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbFuncaoDados_Complexidade_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A374FuncaoDados_DER), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoDados_DER_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoDados_DER_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A375FuncaoDados_RLR), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoDados_RLR_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoDados_RLR_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A377FuncaoDados_PF, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoDados_PF_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoDados_PF_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 53 )
         {
            wbEnd = 0;
            nRC_GXsfl_53 = (short)(nGXsfl_53_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridContainer.AddObjectProperty("GRID_nEOF", GRID_nEOF);
               GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_DI2e( true) ;
         }
         else
         {
            wb_table1_2_DI2e( false) ;
         }
      }

      protected void wb_table2_5_DI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_8_DI2( true) ;
         }
         else
         {
            wb_table3_8_DI2( false) ;
         }
         return  ;
      }

      protected void wb_table3_8_DI2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_SistemaFronteira.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'" + sGXsfl_53_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,14);\"", "", true, "HLP_SistemaFronteira.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'" + sPrefix + "',false,'" + sGXsfl_53_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,15);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_SistemaFronteira.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_17_DI2( true) ;
         }
         else
         {
            wb_table4_17_DI2( false) ;
         }
         return  ;
      }

      protected void wb_table4_17_DI2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_DI2e( true) ;
         }
         else
         {
            wb_table2_5_DI2e( false) ;
         }
      }

      protected void wb_table4_17_DI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaFronteira.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextfuncaodados_tipo_Internalname, "Dados_Tipo", "", "", lblFiltertextfuncaodados_tipo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_SistemaFronteira.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'" + sPrefix + "',false,'" + sGXsfl_53_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaodados_tipo, cmbavFuncaodados_tipo_Internalname, StringUtil.RTrim( AV26FuncaoDados_Tipo), 1, cmbavFuncaodados_tipo_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_SistemaFronteira.htm");
            cmbavFuncaodados_tipo.CurrentValue = StringUtil.RTrim( AV26FuncaoDados_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaodados_tipo_Internalname, "Values", (String)(cmbavFuncaodados_tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_26_DI2( true) ;
         }
         else
         {
            wb_table5_26_DI2( false) ;
         }
         return  ;
      }

      protected void wb_table5_26_DI2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_SistemaFronteira.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_17_DI2e( true) ;
         }
         else
         {
            wb_table4_17_DI2e( false) ;
         }
      }

      protected void wb_table5_26_DI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_SistemaFronteira.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'" + sPrefix + "',false,'" + sGXsfl_53_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_SistemaFronteira.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_SistemaFronteira.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'" + sPrefix + "',false,'" + sGXsfl_53_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaodados_nome1_Internalname, AV17FuncaoDados_Nome1, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,35);\"", 0, edtavFuncaodados_nome1_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_SistemaFronteira.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaFronteira.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaFronteira.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_SistemaFronteira.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'" + sPrefix + "',false,'" + sGXsfl_53_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", "", true, "HLP_SistemaFronteira.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_SistemaFronteira.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'" + sPrefix + "',false,'" + sGXsfl_53_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaodados_nome2_Internalname, AV21FuncaoDados_Nome2, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", 0, edtavFuncaodados_nome2_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_SistemaFronteira.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaFronteira.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_26_DI2e( true) ;
         }
         else
         {
            wb_table5_26_DI2e( false) ;
         }
      }

      protected void wb_table3_8_DI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_8_DI2e( true) ;
         }
         else
         {
            wb_table3_8_DI2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7FuncaoDados_SistemaCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoDados_SistemaCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PADI2( ) ;
         WSDI2( ) ;
         WEDI2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7FuncaoDados_SistemaCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PADI2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "sistemafronteira");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PADI2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7FuncaoDados_SistemaCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoDados_SistemaCod), 6, 0)));
         }
         wcpOAV7FuncaoDados_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7FuncaoDados_SistemaCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7FuncaoDados_SistemaCod != wcpOAV7FuncaoDados_SistemaCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV7FuncaoDados_SistemaCod = AV7FuncaoDados_SistemaCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7FuncaoDados_SistemaCod = cgiGet( sPrefix+"AV7FuncaoDados_SistemaCod_CTRL");
         if ( StringUtil.Len( sCtrlAV7FuncaoDados_SistemaCod) > 0 )
         {
            AV7FuncaoDados_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7FuncaoDados_SistemaCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoDados_SistemaCod), 6, 0)));
         }
         else
         {
            AV7FuncaoDados_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7FuncaoDados_SistemaCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PADI2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSDI2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSDI2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7FuncaoDados_SistemaCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7FuncaoDados_SistemaCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7FuncaoDados_SistemaCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7FuncaoDados_SistemaCod_CTRL", StringUtil.RTrim( sCtrlAV7FuncaoDados_SistemaCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEDI2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042822502620");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("sistemafronteira.js", "?202042822502620");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_532( )
      {
         edtFuncaoDados_Codigo_Internalname = sPrefix+"FUNCAODADOS_CODIGO_"+sGXsfl_53_idx;
         edtFuncaoDados_SistemaCod_Internalname = sPrefix+"FUNCAODADOS_SISTEMACOD_"+sGXsfl_53_idx;
         edtFuncaoDados_FuncaoDadosCod_Internalname = sPrefix+"FUNCAODADOS_FUNCAODADOSCOD_"+sGXsfl_53_idx;
         edtFuncaoDados_FuncaoDadosSisCod_Internalname = sPrefix+"FUNCAODADOS_FUNCAODADOSSISCOD_"+sGXsfl_53_idx;
         edtFuncaoDados_Nome_Internalname = sPrefix+"FUNCAODADOS_NOME_"+sGXsfl_53_idx;
         cmbFuncaoDados_Complexidade_Internalname = sPrefix+"FUNCAODADOS_COMPLEXIDADE_"+sGXsfl_53_idx;
         edtFuncaoDados_DER_Internalname = sPrefix+"FUNCAODADOS_DER_"+sGXsfl_53_idx;
         edtFuncaoDados_RLR_Internalname = sPrefix+"FUNCAODADOS_RLR_"+sGXsfl_53_idx;
         edtFuncaoDados_PF_Internalname = sPrefix+"FUNCAODADOS_PF_"+sGXsfl_53_idx;
      }

      protected void SubsflControlProps_fel_532( )
      {
         edtFuncaoDados_Codigo_Internalname = sPrefix+"FUNCAODADOS_CODIGO_"+sGXsfl_53_fel_idx;
         edtFuncaoDados_SistemaCod_Internalname = sPrefix+"FUNCAODADOS_SISTEMACOD_"+sGXsfl_53_fel_idx;
         edtFuncaoDados_FuncaoDadosCod_Internalname = sPrefix+"FUNCAODADOS_FUNCAODADOSCOD_"+sGXsfl_53_fel_idx;
         edtFuncaoDados_FuncaoDadosSisCod_Internalname = sPrefix+"FUNCAODADOS_FUNCAODADOSSISCOD_"+sGXsfl_53_fel_idx;
         edtFuncaoDados_Nome_Internalname = sPrefix+"FUNCAODADOS_NOME_"+sGXsfl_53_fel_idx;
         cmbFuncaoDados_Complexidade_Internalname = sPrefix+"FUNCAODADOS_COMPLEXIDADE_"+sGXsfl_53_fel_idx;
         edtFuncaoDados_DER_Internalname = sPrefix+"FUNCAODADOS_DER_"+sGXsfl_53_fel_idx;
         edtFuncaoDados_RLR_Internalname = sPrefix+"FUNCAODADOS_RLR_"+sGXsfl_53_fel_idx;
         edtFuncaoDados_PF_Internalname = sPrefix+"FUNCAODADOS_PF_"+sGXsfl_53_fel_idx;
      }

      protected void sendrow_532( )
      {
         SubsflControlProps_532( ) ;
         WBDI0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_53_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_53_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_53_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A368FuncaoDados_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_Codigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)53,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_SistemaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A370FuncaoDados_SistemaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A370FuncaoDados_SistemaCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_SistemaCod_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)53,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_FuncaoDadosCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A391FuncaoDados_FuncaoDadosCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_FuncaoDadosCod_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)53,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_FuncaoDadosSisCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A404FuncaoDados_FuncaoDadosSisCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A404FuncaoDados_FuncaoDadosSisCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_FuncaoDadosSisCod_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)53,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_Nome_Internalname,(String)A369FuncaoDados_Nome,(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_Nome_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)450,(String)"px",(short)17,(String)"px",(short)200,(short)0,(short)0,(short)53,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            GXCCtl = "FUNCAODADOS_COMPLEXIDADE_" + sGXsfl_53_idx;
            cmbFuncaoDados_Complexidade.Name = GXCCtl;
            cmbFuncaoDados_Complexidade.WebTags = "";
            cmbFuncaoDados_Complexidade.addItem("E", "", 0);
            cmbFuncaoDados_Complexidade.addItem("B", "Baixa", 0);
            cmbFuncaoDados_Complexidade.addItem("M", "M�dia", 0);
            cmbFuncaoDados_Complexidade.addItem("A", "Alta", 0);
            if ( cmbFuncaoDados_Complexidade.ItemCount > 0 )
            {
               A376FuncaoDados_Complexidade = cmbFuncaoDados_Complexidade.getValidValue(A376FuncaoDados_Complexidade);
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbFuncaoDados_Complexidade,(String)cmbFuncaoDados_Complexidade_Internalname,StringUtil.RTrim( A376FuncaoDados_Complexidade),(short)1,(String)cmbFuncaoDados_Complexidade_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",(String)"",(String)"",(bool)true});
            cmbFuncaoDados_Complexidade.CurrentValue = StringUtil.RTrim( A376FuncaoDados_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbFuncaoDados_Complexidade_Internalname, "Values", (String)(cmbFuncaoDados_Complexidade.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_DER_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A374FuncaoDados_DER), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A374FuncaoDados_DER), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_DER_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)53,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_RLR_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A375FuncaoDados_RLR), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A375FuncaoDados_RLR), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_RLR_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)53,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_PF_Internalname,StringUtil.LTrim( StringUtil.NToC( A377FuncaoDados_PF, 14, 5, ",", "")),context.localUtil.Format( A377FuncaoDados_PF, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_PF_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)53,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_CODIGO"+"_"+sGXsfl_53_idx, GetSecureSignedToken( sPrefix+sGXsfl_53_idx, context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_SISTEMACOD"+"_"+sGXsfl_53_idx, GetSecureSignedToken( sPrefix+sGXsfl_53_idx, context.localUtil.Format( (decimal)(A370FuncaoDados_SistemaCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_FUNCAODADOSCOD"+"_"+sGXsfl_53_idx, GetSecureSignedToken( sPrefix+sGXsfl_53_idx, context.localUtil.Format( (decimal)(A391FuncaoDados_FuncaoDadosCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_NOME"+"_"+sGXsfl_53_idx, GetSecureSignedToken( sPrefix+sGXsfl_53_idx, StringUtil.RTrim( context.localUtil.Format( A369FuncaoDados_Nome, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_COMPLEXIDADE"+"_"+sGXsfl_53_idx, GetSecureSignedToken( sPrefix+sGXsfl_53_idx, StringUtil.RTrim( context.localUtil.Format( A376FuncaoDados_Complexidade, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_DER"+"_"+sGXsfl_53_idx, GetSecureSignedToken( sPrefix+sGXsfl_53_idx, context.localUtil.Format( (decimal)(A374FuncaoDados_DER), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_RLR"+"_"+sGXsfl_53_idx, GetSecureSignedToken( sPrefix+sGXsfl_53_idx, context.localUtil.Format( (decimal)(A375FuncaoDados_RLR), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_PF"+"_"+sGXsfl_53_idx, GetSecureSignedToken( sPrefix+sGXsfl_53_idx, context.localUtil.Format( A377FuncaoDados_PF, "ZZ,ZZZ,ZZ9.999")));
            GridContainer.AddRow(GridRow);
            nGXsfl_53_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_53_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_53_idx+1));
            sGXsfl_53_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_53_idx), 4, 0)), 4, "0");
            SubsflControlProps_532( ) ;
         }
         /* End function sendrow_532 */
      }

      protected void init_default_properties( )
      {
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         lblOrderedtext_Internalname = sPrefix+"ORDEREDTEXT";
         cmbavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         imgCleanfilters_Internalname = sPrefix+"CLEANFILTERS";
         lblFiltertextfuncaodados_tipo_Internalname = sPrefix+"FILTERTEXTFUNCAODADOS_TIPO";
         cmbavFuncaodados_tipo_Internalname = sPrefix+"vFUNCAODADOS_TIPO";
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         edtavFuncaodados_nome1_Internalname = sPrefix+"vFUNCAODADOS_NOME1";
         imgAdddynamicfilters1_Internalname = sPrefix+"ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = sPrefix+"REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = sPrefix+"DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE2";
         edtavFuncaodados_nome2_Internalname = sPrefix+"vFUNCAODADOS_NOME2";
         imgRemovedynamicfilters2_Internalname = sPrefix+"REMOVEDYNAMICFILTERS2";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = sPrefix+"JSDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtFuncaoDados_Codigo_Internalname = sPrefix+"FUNCAODADOS_CODIGO";
         edtFuncaoDados_SistemaCod_Internalname = sPrefix+"FUNCAODADOS_SISTEMACOD";
         edtFuncaoDados_FuncaoDadosCod_Internalname = sPrefix+"FUNCAODADOS_FUNCAODADOSCOD";
         edtFuncaoDados_FuncaoDadosSisCod_Internalname = sPrefix+"FUNCAODADOS_FUNCAODADOSSISCOD";
         edtFuncaoDados_Nome_Internalname = sPrefix+"FUNCAODADOS_NOME";
         cmbFuncaoDados_Complexidade_Internalname = sPrefix+"FUNCAODADOS_COMPLEXIDADE";
         edtFuncaoDados_DER_Internalname = sPrefix+"FUNCAODADOS_DER";
         edtFuncaoDados_RLR_Internalname = sPrefix+"FUNCAODADOS_RLR";
         edtFuncaoDados_PF_Internalname = sPrefix+"FUNCAODADOS_PF";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = sPrefix+"vDYNAMICFILTERSENABLED2";
         edtavTffuncaodados_nome_Internalname = sPrefix+"vTFFUNCAODADOS_NOME";
         edtavTffuncaodados_nome_sel_Internalname = sPrefix+"vTFFUNCAODADOS_NOME_SEL";
         edtavTffuncaodados_der_Internalname = sPrefix+"vTFFUNCAODADOS_DER";
         edtavTffuncaodados_der_to_Internalname = sPrefix+"vTFFUNCAODADOS_DER_TO";
         edtavTffuncaodados_rlr_Internalname = sPrefix+"vTFFUNCAODADOS_RLR";
         edtavTffuncaodados_rlr_to_Internalname = sPrefix+"vTFFUNCAODADOS_RLR_TO";
         edtavTffuncaodados_pf_Internalname = sPrefix+"vTFFUNCAODADOS_PF";
         edtavTffuncaodados_pf_to_Internalname = sPrefix+"vTFFUNCAODADOS_PF_TO";
         Ddo_funcaodados_nome_Internalname = sPrefix+"DDO_FUNCAODADOS_NOME";
         edtavDdo_funcaodados_nometitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE";
         Ddo_funcaodados_complexidade_Internalname = sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE";
         edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE";
         Ddo_funcaodados_der_Internalname = sPrefix+"DDO_FUNCAODADOS_DER";
         edtavDdo_funcaodados_dertitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE";
         Ddo_funcaodados_rlr_Internalname = sPrefix+"DDO_FUNCAODADOS_RLR";
         edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE";
         Ddo_funcaodados_pf_Internalname = sPrefix+"DDO_FUNCAODADOS_PF";
         edtavDdo_funcaodados_pftitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtFuncaoDados_PF_Jsonclick = "";
         edtFuncaoDados_RLR_Jsonclick = "";
         edtFuncaoDados_DER_Jsonclick = "";
         cmbFuncaoDados_Complexidade_Jsonclick = "";
         edtFuncaoDados_Nome_Jsonclick = "";
         edtFuncaoDados_FuncaoDadosSisCod_Jsonclick = "";
         edtFuncaoDados_FuncaoDadosCod_Jsonclick = "";
         edtFuncaoDados_SistemaCod_Jsonclick = "";
         edtFuncaoDados_Codigo_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         cmbavFuncaodados_tipo_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtFuncaoDados_PF_Titleformat = 0;
         edtFuncaoDados_RLR_Titleformat = 0;
         edtFuncaoDados_DER_Titleformat = 0;
         cmbFuncaoDados_Complexidade_Titleformat = 0;
         edtFuncaoDados_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavFuncaodados_nome2_Visible = 1;
         edtavFuncaodados_nome1_Visible = 1;
         edtFuncaoDados_PF_Title = "PF";
         edtFuncaoDados_RLR_Title = "RLR";
         edtFuncaoDados_DER_Title = "DER";
         cmbFuncaoDados_Complexidade.Title.Text = "Complexidade";
         edtFuncaoDados_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_funcaodados_pftitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaodados_dertitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaodados_nometitlecontrolidtoreplace_Visible = 1;
         edtavTffuncaodados_pf_to_Jsonclick = "";
         edtavTffuncaodados_pf_to_Visible = 1;
         edtavTffuncaodados_pf_Jsonclick = "";
         edtavTffuncaodados_pf_Visible = 1;
         edtavTffuncaodados_rlr_to_Jsonclick = "";
         edtavTffuncaodados_rlr_to_Visible = 1;
         edtavTffuncaodados_rlr_Jsonclick = "";
         edtavTffuncaodados_rlr_Visible = 1;
         edtavTffuncaodados_der_to_Jsonclick = "";
         edtavTffuncaodados_der_to_Visible = 1;
         edtavTffuncaodados_der_Jsonclick = "";
         edtavTffuncaodados_der_Visible = 1;
         edtavTffuncaodados_nome_sel_Visible = 1;
         edtavTffuncaodados_nome_Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_funcaodados_pf_Searchbuttontext = "Pesquisar";
         Ddo_funcaodados_pf_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaodados_pf_Rangefilterto = "At�";
         Ddo_funcaodados_pf_Rangefilterfrom = "Desde";
         Ddo_funcaodados_pf_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaodados_pf_Loadingdata = "Carregando dados...";
         Ddo_funcaodados_pf_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaodados_pf_Sortasc = "Ordenar de A � Z";
         Ddo_funcaodados_pf_Datalistupdateminimumcharacters = 0;
         Ddo_funcaodados_pf_Datalistfixedvalues = "";
         Ddo_funcaodados_pf_Includedatalist = Convert.ToBoolean( 0);
         Ddo_funcaodados_pf_Filterisrange = Convert.ToBoolean( -1);
         Ddo_funcaodados_pf_Filtertype = "Numeric";
         Ddo_funcaodados_pf_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaodados_pf_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_funcaodados_pf_Includesortasc = Convert.ToBoolean( 0);
         Ddo_funcaodados_pf_Titlecontrolidtoreplace = "";
         Ddo_funcaodados_pf_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaodados_pf_Cls = "ColumnSettings";
         Ddo_funcaodados_pf_Tooltip = "Op��es";
         Ddo_funcaodados_pf_Caption = "";
         Ddo_funcaodados_rlr_Searchbuttontext = "Pesquisar";
         Ddo_funcaodados_rlr_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaodados_rlr_Rangefilterto = "At�";
         Ddo_funcaodados_rlr_Rangefilterfrom = "Desde";
         Ddo_funcaodados_rlr_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaodados_rlr_Loadingdata = "Carregando dados...";
         Ddo_funcaodados_rlr_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaodados_rlr_Sortasc = "Ordenar de A � Z";
         Ddo_funcaodados_rlr_Datalistupdateminimumcharacters = 0;
         Ddo_funcaodados_rlr_Datalistfixedvalues = "";
         Ddo_funcaodados_rlr_Includedatalist = Convert.ToBoolean( 0);
         Ddo_funcaodados_rlr_Filterisrange = Convert.ToBoolean( -1);
         Ddo_funcaodados_rlr_Filtertype = "Numeric";
         Ddo_funcaodados_rlr_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaodados_rlr_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_funcaodados_rlr_Includesortasc = Convert.ToBoolean( 0);
         Ddo_funcaodados_rlr_Titlecontrolidtoreplace = "";
         Ddo_funcaodados_rlr_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaodados_rlr_Cls = "ColumnSettings";
         Ddo_funcaodados_rlr_Tooltip = "Op��es";
         Ddo_funcaodados_rlr_Caption = "";
         Ddo_funcaodados_der_Searchbuttontext = "Pesquisar";
         Ddo_funcaodados_der_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaodados_der_Rangefilterto = "At�";
         Ddo_funcaodados_der_Rangefilterfrom = "Desde";
         Ddo_funcaodados_der_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaodados_der_Loadingdata = "Carregando dados...";
         Ddo_funcaodados_der_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaodados_der_Sortasc = "Ordenar de A � Z";
         Ddo_funcaodados_der_Datalistupdateminimumcharacters = 0;
         Ddo_funcaodados_der_Datalistfixedvalues = "";
         Ddo_funcaodados_der_Includedatalist = Convert.ToBoolean( 0);
         Ddo_funcaodados_der_Filterisrange = Convert.ToBoolean( -1);
         Ddo_funcaodados_der_Filtertype = "Numeric";
         Ddo_funcaodados_der_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaodados_der_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_funcaodados_der_Includesortasc = Convert.ToBoolean( 0);
         Ddo_funcaodados_der_Titlecontrolidtoreplace = "";
         Ddo_funcaodados_der_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaodados_der_Cls = "ColumnSettings";
         Ddo_funcaodados_der_Tooltip = "Op��es";
         Ddo_funcaodados_der_Caption = "";
         Ddo_funcaodados_complexidade_Searchbuttontext = "Filtrar Selecionados";
         Ddo_funcaodados_complexidade_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaodados_complexidade_Rangefilterto = "At�";
         Ddo_funcaodados_complexidade_Rangefilterfrom = "Desde";
         Ddo_funcaodados_complexidade_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaodados_complexidade_Loadingdata = "Carregando dados...";
         Ddo_funcaodados_complexidade_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaodados_complexidade_Sortasc = "Ordenar de A � Z";
         Ddo_funcaodados_complexidade_Datalistupdateminimumcharacters = 0;
         Ddo_funcaodados_complexidade_Datalistfixedvalues = "E:,B:Baixa,M:M�dia,A:Alta";
         Ddo_funcaodados_complexidade_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_funcaodados_complexidade_Datalisttype = "FixedValues";
         Ddo_funcaodados_complexidade_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaodados_complexidade_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcaodados_complexidade_Includefilter = Convert.ToBoolean( 0);
         Ddo_funcaodados_complexidade_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_funcaodados_complexidade_Includesortasc = Convert.ToBoolean( 0);
         Ddo_funcaodados_complexidade_Titlecontrolidtoreplace = "";
         Ddo_funcaodados_complexidade_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaodados_complexidade_Cls = "ColumnSettings";
         Ddo_funcaodados_complexidade_Tooltip = "Op��es";
         Ddo_funcaodados_complexidade_Caption = "";
         Ddo_funcaodados_nome_Searchbuttontext = "Pesquisar";
         Ddo_funcaodados_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaodados_nome_Rangefilterto = "At�";
         Ddo_funcaodados_nome_Rangefilterfrom = "Desde";
         Ddo_funcaodados_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaodados_nome_Loadingdata = "Carregando dados...";
         Ddo_funcaodados_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaodados_nome_Sortasc = "Ordenar de A � Z";
         Ddo_funcaodados_nome_Datalistupdateminimumcharacters = 0;
         Ddo_funcaodados_nome_Datalistproc = "GetSistemaFronteiraFilterData";
         Ddo_funcaodados_nome_Datalistfixedvalues = "";
         Ddo_funcaodados_nome_Datalisttype = "Dynamic";
         Ddo_funcaodados_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaodados_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcaodados_nome_Filtertype = "Character";
         Ddo_funcaodados_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaodados_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaodados_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaodados_nome_Titlecontrolidtoreplace = "";
         Ddo_funcaodados_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaodados_nome_Cls = "ColumnSettings";
         Ddo_funcaodados_nome_Tooltip = "Op��es";
         Ddo_funcaodados_nome_Caption = "";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV32ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV26FuncaoDados_Tipo',fld:'vFUNCAODADOS_TIPO',pic:'',nv:''},{av:'AV30TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV31TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV35TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV38TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV39TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV42TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV43TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV46TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV47TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''}],oparms:[{av:'AV29FuncaoDados_NomeTitleFilterData',fld:'vFUNCAODADOS_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV33FuncaoDados_ComplexidadeTitleFilterData',fld:'vFUNCAODADOS_COMPLEXIDADETITLEFILTERDATA',pic:'',nv:null},{av:'AV37FuncaoDados_DERTitleFilterData',fld:'vFUNCAODADOS_DERTITLEFILTERDATA',pic:'',nv:null},{av:'AV41FuncaoDados_RLRTitleFilterData',fld:'vFUNCAODADOS_RLRTITLEFILTERDATA',pic:'',nv:null},{av:'AV45FuncaoDados_PFTitleFilterData',fld:'vFUNCAODADOS_PFTITLEFILTERDATA',pic:'',nv:null},{av:'edtFuncaoDados_Nome_Titleformat',ctrl:'FUNCAODADOS_NOME',prop:'Titleformat'},{av:'edtFuncaoDados_Nome_Title',ctrl:'FUNCAODADOS_NOME',prop:'Title'},{av:'cmbFuncaoDados_Complexidade'},{av:'edtFuncaoDados_DER_Titleformat',ctrl:'FUNCAODADOS_DER',prop:'Titleformat'},{av:'edtFuncaoDados_DER_Title',ctrl:'FUNCAODADOS_DER',prop:'Title'},{av:'edtFuncaoDados_RLR_Titleformat',ctrl:'FUNCAODADOS_RLR',prop:'Titleformat'},{av:'edtFuncaoDados_RLR_Title',ctrl:'FUNCAODADOS_RLR',prop:'Title'},{av:'edtFuncaoDados_PF_Titleformat',ctrl:'FUNCAODADOS_PF',prop:'Titleformat'},{av:'edtFuncaoDados_PF_Title',ctrl:'FUNCAODADOS_PF',prop:'Title'},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("DDO_FUNCAODADOS_NOME.ONOPTIONCLICKED","{handler:'E11DI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV31TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV38TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV39TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV42TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV43TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV46TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV47TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV32ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV26FuncaoDados_Tipo',fld:'vFUNCAODADOS_TIPO',pic:'',nv:''},{av:'AV35TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'sPrefix',nv:''},{av:'Ddo_funcaodados_nome_Activeeventkey',ctrl:'DDO_FUNCAODADOS_NOME',prop:'ActiveEventKey'},{av:'Ddo_funcaodados_nome_Filteredtext_get',ctrl:'DDO_FUNCAODADOS_NOME',prop:'FilteredText_get'},{av:'Ddo_funcaodados_nome_Selectedvalue_get',ctrl:'DDO_FUNCAODADOS_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaodados_nome_Sortedstatus',ctrl:'DDO_FUNCAODADOS_NOME',prop:'SortedStatus'},{av:'AV30TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV31TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''}]}");
         setEventMetadata("DDO_FUNCAODADOS_COMPLEXIDADE.ONOPTIONCLICKED","{handler:'E12DI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV31TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV38TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV39TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV42TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV43TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV46TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV47TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV32ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV26FuncaoDados_Tipo',fld:'vFUNCAODADOS_TIPO',pic:'',nv:''},{av:'AV35TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'sPrefix',nv:''},{av:'Ddo_funcaodados_complexidade_Activeeventkey',ctrl:'DDO_FUNCAODADOS_COMPLEXIDADE',prop:'ActiveEventKey'},{av:'Ddo_funcaodados_complexidade_Selectedvalue_get',ctrl:'DDO_FUNCAODADOS_COMPLEXIDADE',prop:'SelectedValue_get'}],oparms:[{av:'AV35TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null}]}");
         setEventMetadata("DDO_FUNCAODADOS_DER.ONOPTIONCLICKED","{handler:'E13DI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV31TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV38TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV39TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV42TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV43TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV46TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV47TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV32ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV26FuncaoDados_Tipo',fld:'vFUNCAODADOS_TIPO',pic:'',nv:''},{av:'AV35TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'sPrefix',nv:''},{av:'Ddo_funcaodados_der_Activeeventkey',ctrl:'DDO_FUNCAODADOS_DER',prop:'ActiveEventKey'},{av:'Ddo_funcaodados_der_Filteredtext_get',ctrl:'DDO_FUNCAODADOS_DER',prop:'FilteredText_get'},{av:'Ddo_funcaodados_der_Filteredtextto_get',ctrl:'DDO_FUNCAODADOS_DER',prop:'FilteredTextTo_get'}],oparms:[{av:'AV38TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV39TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("DDO_FUNCAODADOS_RLR.ONOPTIONCLICKED","{handler:'E14DI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV31TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV38TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV39TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV42TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV43TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV46TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV47TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV32ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV26FuncaoDados_Tipo',fld:'vFUNCAODADOS_TIPO',pic:'',nv:''},{av:'AV35TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'sPrefix',nv:''},{av:'Ddo_funcaodados_rlr_Activeeventkey',ctrl:'DDO_FUNCAODADOS_RLR',prop:'ActiveEventKey'},{av:'Ddo_funcaodados_rlr_Filteredtext_get',ctrl:'DDO_FUNCAODADOS_RLR',prop:'FilteredText_get'},{av:'Ddo_funcaodados_rlr_Filteredtextto_get',ctrl:'DDO_FUNCAODADOS_RLR',prop:'FilteredTextTo_get'}],oparms:[{av:'AV42TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV43TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("DDO_FUNCAODADOS_PF.ONOPTIONCLICKED","{handler:'E15DI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV31TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV38TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV39TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV42TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV43TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV46TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV47TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV32ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV26FuncaoDados_Tipo',fld:'vFUNCAODADOS_TIPO',pic:'',nv:''},{av:'AV35TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'sPrefix',nv:''},{av:'Ddo_funcaodados_pf_Activeeventkey',ctrl:'DDO_FUNCAODADOS_PF',prop:'ActiveEventKey'},{av:'Ddo_funcaodados_pf_Filteredtext_get',ctrl:'DDO_FUNCAODADOS_PF',prop:'FilteredText_get'},{av:'Ddo_funcaodados_pf_Filteredtextto_get',ctrl:'DDO_FUNCAODADOS_PF',prop:'FilteredTextTo_get'}],oparms:[{av:'AV46TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV47TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         setEventMetadata("GRID.LOAD","{handler:'E25DI2',iparms:[],oparms:[]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E16DI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV31TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV38TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV39TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV42TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV43TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV46TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV47TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV32ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV26FuncaoDados_Tipo',fld:'vFUNCAODADOS_TIPO',pic:'',nv:''},{av:'AV35TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E20DI2',iparms:[],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E17DI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV31TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV38TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV39TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV42TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV43TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV46TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV47TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV32ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV26FuncaoDados_Tipo',fld:'vFUNCAODADOS_TIPO',pic:'',nv:''},{av:'AV35TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'sPrefix',nv:''}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavFuncaodados_nome2_Visible',ctrl:'vFUNCAODADOS_NOME2',prop:'Visible'},{av:'edtavFuncaodados_nome1_Visible',ctrl:'vFUNCAODADOS_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E21DI2',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavFuncaodados_nome1_Visible',ctrl:'vFUNCAODADOS_NOME1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E18DI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV31TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV38TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV39TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV42TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV43TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV46TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV47TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV32ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV26FuncaoDados_Tipo',fld:'vFUNCAODADOS_TIPO',pic:'',nv:''},{av:'AV35TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'sPrefix',nv:''}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavFuncaodados_nome2_Visible',ctrl:'vFUNCAODADOS_NOME2',prop:'Visible'},{av:'edtavFuncaodados_nome1_Visible',ctrl:'vFUNCAODADOS_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E22DI2',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavFuncaodados_nome2_Visible',ctrl:'vFUNCAODADOS_NOME2',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E19DI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV31TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV38TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV39TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV42TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV43TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV46TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV47TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV32ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV26FuncaoDados_Tipo',fld:'vFUNCAODADOS_TIPO',pic:'',nv:''},{av:'AV35TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'sPrefix',nv:''}],oparms:[{av:'AV26FuncaoDados_Tipo',fld:'vFUNCAODADOS_TIPO',pic:'',nv:''},{av:'AV30TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'Ddo_funcaodados_nome_Filteredtext_set',ctrl:'DDO_FUNCAODADOS_NOME',prop:'FilteredText_set'},{av:'AV31TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'Ddo_funcaodados_nome_Selectedvalue_set',ctrl:'DDO_FUNCAODADOS_NOME',prop:'SelectedValue_set'},{av:'AV35TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'Ddo_funcaodados_complexidade_Selectedvalue_set',ctrl:'DDO_FUNCAODADOS_COMPLEXIDADE',prop:'SelectedValue_set'},{av:'AV38TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'Ddo_funcaodados_der_Filteredtext_set',ctrl:'DDO_FUNCAODADOS_DER',prop:'FilteredText_set'},{av:'AV39TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'Ddo_funcaodados_der_Filteredtextto_set',ctrl:'DDO_FUNCAODADOS_DER',prop:'FilteredTextTo_set'},{av:'AV42TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'Ddo_funcaodados_rlr_Filteredtext_set',ctrl:'DDO_FUNCAODADOS_RLR',prop:'FilteredText_set'},{av:'AV43TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'Ddo_funcaodados_rlr_Filteredtextto_set',ctrl:'DDO_FUNCAODADOS_RLR',prop:'FilteredTextTo_set'},{av:'AV46TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_funcaodados_pf_Filteredtext_set',ctrl:'DDO_FUNCAODADOS_PF',prop:'FilteredText_set'},{av:'AV47TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_funcaodados_pf_Filteredtextto_set',ctrl:'DDO_FUNCAODADOS_PF',prop:'FilteredTextTo_set'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavFuncaodados_nome1_Visible',ctrl:'vFUNCAODADOS_NOME1',prop:'Visible'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavFuncaodados_nome2_Visible',ctrl:'vFUNCAODADOS_NOME2',prop:'Visible'}]}");
         setEventMetadata("GRID_FIRSTPAGE","{handler:'subgrid_firstpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV32ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV26FuncaoDados_Tipo',fld:'vFUNCAODADOS_TIPO',pic:'',nv:''},{av:'AV30TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV31TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV35TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV38TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV39TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV42TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV43TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV46TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV47TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''}],oparms:[{av:'AV29FuncaoDados_NomeTitleFilterData',fld:'vFUNCAODADOS_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV33FuncaoDados_ComplexidadeTitleFilterData',fld:'vFUNCAODADOS_COMPLEXIDADETITLEFILTERDATA',pic:'',nv:null},{av:'AV37FuncaoDados_DERTitleFilterData',fld:'vFUNCAODADOS_DERTITLEFILTERDATA',pic:'',nv:null},{av:'AV41FuncaoDados_RLRTitleFilterData',fld:'vFUNCAODADOS_RLRTITLEFILTERDATA',pic:'',nv:null},{av:'AV45FuncaoDados_PFTitleFilterData',fld:'vFUNCAODADOS_PFTITLEFILTERDATA',pic:'',nv:null},{av:'edtFuncaoDados_Nome_Titleformat',ctrl:'FUNCAODADOS_NOME',prop:'Titleformat'},{av:'edtFuncaoDados_Nome_Title',ctrl:'FUNCAODADOS_NOME',prop:'Title'},{av:'cmbFuncaoDados_Complexidade'},{av:'edtFuncaoDados_DER_Titleformat',ctrl:'FUNCAODADOS_DER',prop:'Titleformat'},{av:'edtFuncaoDados_DER_Title',ctrl:'FUNCAODADOS_DER',prop:'Title'},{av:'edtFuncaoDados_RLR_Titleformat',ctrl:'FUNCAODADOS_RLR',prop:'Titleformat'},{av:'edtFuncaoDados_RLR_Title',ctrl:'FUNCAODADOS_RLR',prop:'Title'},{av:'edtFuncaoDados_PF_Titleformat',ctrl:'FUNCAODADOS_PF',prop:'Titleformat'},{av:'edtFuncaoDados_PF_Title',ctrl:'FUNCAODADOS_PF',prop:'Title'},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_PREVPAGE","{handler:'subgrid_previouspage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV32ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV26FuncaoDados_Tipo',fld:'vFUNCAODADOS_TIPO',pic:'',nv:''},{av:'AV30TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV31TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV35TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV38TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV39TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV42TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV43TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV46TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV47TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''}],oparms:[{av:'AV29FuncaoDados_NomeTitleFilterData',fld:'vFUNCAODADOS_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV33FuncaoDados_ComplexidadeTitleFilterData',fld:'vFUNCAODADOS_COMPLEXIDADETITLEFILTERDATA',pic:'',nv:null},{av:'AV37FuncaoDados_DERTitleFilterData',fld:'vFUNCAODADOS_DERTITLEFILTERDATA',pic:'',nv:null},{av:'AV41FuncaoDados_RLRTitleFilterData',fld:'vFUNCAODADOS_RLRTITLEFILTERDATA',pic:'',nv:null},{av:'AV45FuncaoDados_PFTitleFilterData',fld:'vFUNCAODADOS_PFTITLEFILTERDATA',pic:'',nv:null},{av:'edtFuncaoDados_Nome_Titleformat',ctrl:'FUNCAODADOS_NOME',prop:'Titleformat'},{av:'edtFuncaoDados_Nome_Title',ctrl:'FUNCAODADOS_NOME',prop:'Title'},{av:'cmbFuncaoDados_Complexidade'},{av:'edtFuncaoDados_DER_Titleformat',ctrl:'FUNCAODADOS_DER',prop:'Titleformat'},{av:'edtFuncaoDados_DER_Title',ctrl:'FUNCAODADOS_DER',prop:'Title'},{av:'edtFuncaoDados_RLR_Titleformat',ctrl:'FUNCAODADOS_RLR',prop:'Titleformat'},{av:'edtFuncaoDados_RLR_Title',ctrl:'FUNCAODADOS_RLR',prop:'Title'},{av:'edtFuncaoDados_PF_Titleformat',ctrl:'FUNCAODADOS_PF',prop:'Titleformat'},{av:'edtFuncaoDados_PF_Title',ctrl:'FUNCAODADOS_PF',prop:'Title'},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_NEXTPAGE","{handler:'subgrid_nextpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV32ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV26FuncaoDados_Tipo',fld:'vFUNCAODADOS_TIPO',pic:'',nv:''},{av:'AV30TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV31TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV35TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV38TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV39TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV42TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV43TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV46TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV47TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''}],oparms:[{av:'AV29FuncaoDados_NomeTitleFilterData',fld:'vFUNCAODADOS_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV33FuncaoDados_ComplexidadeTitleFilterData',fld:'vFUNCAODADOS_COMPLEXIDADETITLEFILTERDATA',pic:'',nv:null},{av:'AV37FuncaoDados_DERTitleFilterData',fld:'vFUNCAODADOS_DERTITLEFILTERDATA',pic:'',nv:null},{av:'AV41FuncaoDados_RLRTitleFilterData',fld:'vFUNCAODADOS_RLRTITLEFILTERDATA',pic:'',nv:null},{av:'AV45FuncaoDados_PFTitleFilterData',fld:'vFUNCAODADOS_PFTITLEFILTERDATA',pic:'',nv:null},{av:'edtFuncaoDados_Nome_Titleformat',ctrl:'FUNCAODADOS_NOME',prop:'Titleformat'},{av:'edtFuncaoDados_Nome_Title',ctrl:'FUNCAODADOS_NOME',prop:'Title'},{av:'cmbFuncaoDados_Complexidade'},{av:'edtFuncaoDados_DER_Titleformat',ctrl:'FUNCAODADOS_DER',prop:'Titleformat'},{av:'edtFuncaoDados_DER_Title',ctrl:'FUNCAODADOS_DER',prop:'Title'},{av:'edtFuncaoDados_RLR_Titleformat',ctrl:'FUNCAODADOS_RLR',prop:'Titleformat'},{av:'edtFuncaoDados_RLR_Title',ctrl:'FUNCAODADOS_RLR',prop:'Title'},{av:'edtFuncaoDados_PF_Titleformat',ctrl:'FUNCAODADOS_PF',prop:'Titleformat'},{av:'edtFuncaoDados_PF_Title',ctrl:'FUNCAODADOS_PF',prop:'Title'},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_LASTPAGE","{handler:'subgrid_lastpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV32ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV26FuncaoDados_Tipo',fld:'vFUNCAODADOS_TIPO',pic:'',nv:''},{av:'AV30TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV31TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV35TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV38TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV39TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV42TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV43TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV46TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV47TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''}],oparms:[{av:'AV29FuncaoDados_NomeTitleFilterData',fld:'vFUNCAODADOS_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV33FuncaoDados_ComplexidadeTitleFilterData',fld:'vFUNCAODADOS_COMPLEXIDADETITLEFILTERDATA',pic:'',nv:null},{av:'AV37FuncaoDados_DERTitleFilterData',fld:'vFUNCAODADOS_DERTITLEFILTERDATA',pic:'',nv:null},{av:'AV41FuncaoDados_RLRTitleFilterData',fld:'vFUNCAODADOS_RLRTITLEFILTERDATA',pic:'',nv:null},{av:'AV45FuncaoDados_PFTitleFilterData',fld:'vFUNCAODADOS_PFTITLEFILTERDATA',pic:'',nv:null},{av:'edtFuncaoDados_Nome_Titleformat',ctrl:'FUNCAODADOS_NOME',prop:'Titleformat'},{av:'edtFuncaoDados_Nome_Title',ctrl:'FUNCAODADOS_NOME',prop:'Title'},{av:'cmbFuncaoDados_Complexidade'},{av:'edtFuncaoDados_DER_Titleformat',ctrl:'FUNCAODADOS_DER',prop:'Titleformat'},{av:'edtFuncaoDados_DER_Title',ctrl:'FUNCAODADOS_DER',prop:'Title'},{av:'edtFuncaoDados_RLR_Titleformat',ctrl:'FUNCAODADOS_RLR',prop:'Titleformat'},{av:'edtFuncaoDados_RLR_Title',ctrl:'FUNCAODADOS_RLR',prop:'Title'},{av:'edtFuncaoDados_PF_Titleformat',ctrl:'FUNCAODADOS_PF',prop:'Titleformat'},{av:'edtFuncaoDados_PF_Title',ctrl:'FUNCAODADOS_PF',prop:'Title'},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Ddo_funcaodados_nome_Activeeventkey = "";
         Ddo_funcaodados_nome_Filteredtext_get = "";
         Ddo_funcaodados_nome_Selectedvalue_get = "";
         Ddo_funcaodados_complexidade_Activeeventkey = "";
         Ddo_funcaodados_complexidade_Selectedvalue_get = "";
         Ddo_funcaodados_der_Activeeventkey = "";
         Ddo_funcaodados_der_Filteredtext_get = "";
         Ddo_funcaodados_der_Filteredtextto_get = "";
         Ddo_funcaodados_rlr_Activeeventkey = "";
         Ddo_funcaodados_rlr_Filteredtext_get = "";
         Ddo_funcaodados_rlr_Filteredtextto_get = "";
         Ddo_funcaodados_pf_Activeeventkey = "";
         Ddo_funcaodados_pf_Filteredtext_get = "";
         Ddo_funcaodados_pf_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16DynamicFiltersSelector1 = "";
         AV17FuncaoDados_Nome1 = "";
         AV20DynamicFiltersSelector2 = "";
         AV21FuncaoDados_Nome2 = "";
         AV30TFFuncaoDados_Nome = "";
         AV31TFFuncaoDados_Nome_Sel = "";
         AV32ddo_FuncaoDados_NomeTitleControlIdToReplace = "";
         AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace = "";
         AV40ddo_FuncaoDados_DERTitleControlIdToReplace = "";
         AV44ddo_FuncaoDados_RLRTitleControlIdToReplace = "";
         AV48ddo_FuncaoDados_PFTitleControlIdToReplace = "";
         AV52Pgmname = "";
         AV26FuncaoDados_Tipo = "";
         AV35TFFuncaoDados_Complexidade_Sels = new GxSimpleCollection();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV49DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV29FuncaoDados_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV33FuncaoDados_ComplexidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV37FuncaoDados_DERTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV41FuncaoDados_RLRTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV45FuncaoDados_PFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_funcaodados_nome_Filteredtext_set = "";
         Ddo_funcaodados_nome_Selectedvalue_set = "";
         Ddo_funcaodados_nome_Sortedstatus = "";
         Ddo_funcaodados_complexidade_Selectedvalue_set = "";
         Ddo_funcaodados_der_Filteredtext_set = "";
         Ddo_funcaodados_der_Filteredtextto_set = "";
         Ddo_funcaodados_rlr_Filteredtext_set = "";
         Ddo_funcaodados_rlr_Filteredtextto_set = "";
         Ddo_funcaodados_pf_Filteredtext_set = "";
         Ddo_funcaodados_pf_Filteredtextto_set = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A369FuncaoDados_Nome = "";
         A376FuncaoDados_Complexidade = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV17FuncaoDados_Nome1 = "";
         lV21FuncaoDados_Nome2 = "";
         lV30TFFuncaoDados_Nome = "";
         A373FuncaoDados_Tipo = "";
         H00DI2_A373FuncaoDados_Tipo = new String[] {""} ;
         H00DI2_A369FuncaoDados_Nome = new String[] {""} ;
         H00DI2_A404FuncaoDados_FuncaoDadosSisCod = new int[1] ;
         H00DI2_n404FuncaoDados_FuncaoDadosSisCod = new bool[] {false} ;
         H00DI2_A391FuncaoDados_FuncaoDadosCod = new int[1] ;
         H00DI2_n391FuncaoDados_FuncaoDadosCod = new bool[] {false} ;
         H00DI2_A370FuncaoDados_SistemaCod = new int[1] ;
         H00DI2_A755FuncaoDados_Contar = new bool[] {false} ;
         H00DI2_n755FuncaoDados_Contar = new bool[] {false} ;
         H00DI2_A368FuncaoDados_Codigo = new int[1] ;
         GXt_char1 = "";
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons3 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV34TFFuncaoDados_Complexidade_SelsJson = "";
         GridRow = new GXWebRow();
         AV25Session = context.GetSession();
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblFiltertextfuncaodados_tipo_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7FuncaoDados_SistemaCod = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.sistemafronteira__default(),
            new Object[][] {
                new Object[] {
               H00DI2_A373FuncaoDados_Tipo, H00DI2_A369FuncaoDados_Nome, H00DI2_A404FuncaoDados_FuncaoDadosSisCod, H00DI2_n404FuncaoDados_FuncaoDadosSisCod, H00DI2_A391FuncaoDados_FuncaoDadosCod, H00DI2_n391FuncaoDados_FuncaoDadosCod, H00DI2_A370FuncaoDados_SistemaCod, H00DI2_A755FuncaoDados_Contar, H00DI2_n755FuncaoDados_Contar, H00DI2_A368FuncaoDados_Codigo
               }
            }
         );
         AV52Pgmname = "SistemaFronteira";
         /* GeneXus formulas. */
         AV52Pgmname = "SistemaFronteira";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_53 ;
      private short nGXsfl_53_idx=1 ;
      private short AV14OrderedBy ;
      private short AV38TFFuncaoDados_DER ;
      private short AV39TFFuncaoDados_DER_To ;
      private short AV42TFFuncaoDados_RLR ;
      private short AV43TFFuncaoDados_RLR_To ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short A374FuncaoDados_DER ;
      private short A375FuncaoDados_RLR ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_53_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short GXt_int2 ;
      private short edtFuncaoDados_Nome_Titleformat ;
      private short cmbFuncaoDados_Complexidade_Titleformat ;
      private short edtFuncaoDados_DER_Titleformat ;
      private short edtFuncaoDados_RLR_Titleformat ;
      private short edtFuncaoDados_PF_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7FuncaoDados_SistemaCod ;
      private int wcpOAV7FuncaoDados_SistemaCod ;
      private int subGrid_Rows ;
      private int Ddo_funcaodados_nome_Datalistupdateminimumcharacters ;
      private int Ddo_funcaodados_complexidade_Datalistupdateminimumcharacters ;
      private int Ddo_funcaodados_der_Datalistupdateminimumcharacters ;
      private int Ddo_funcaodados_rlr_Datalistupdateminimumcharacters ;
      private int Ddo_funcaodados_pf_Datalistupdateminimumcharacters ;
      private int edtavTffuncaodados_nome_Visible ;
      private int edtavTffuncaodados_nome_sel_Visible ;
      private int edtavTffuncaodados_der_Visible ;
      private int edtavTffuncaodados_der_to_Visible ;
      private int edtavTffuncaodados_rlr_Visible ;
      private int edtavTffuncaodados_rlr_to_Visible ;
      private int edtavTffuncaodados_pf_Visible ;
      private int edtavTffuncaodados_pf_to_Visible ;
      private int edtavDdo_funcaodados_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaodados_dertitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaodados_pftitlecontrolidtoreplace_Visible ;
      private int A368FuncaoDados_Codigo ;
      private int A370FuncaoDados_SistemaCod ;
      private int A391FuncaoDados_FuncaoDadosCod ;
      private int A404FuncaoDados_FuncaoDadosSisCod ;
      private int subGrid_Islastpage ;
      private int AV35TFFuncaoDados_Complexidade_Sels_Count ;
      private int edtavOrdereddsc_Visible ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int edtavFuncaodados_nome1_Visible ;
      private int edtavFuncaodados_nome2_Visible ;
      private int AV53GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV46TFFuncaoDados_PF ;
      private decimal AV47TFFuncaoDados_PF_To ;
      private decimal A377FuncaoDados_PF ;
      private String Ddo_funcaodados_nome_Activeeventkey ;
      private String Ddo_funcaodados_nome_Filteredtext_get ;
      private String Ddo_funcaodados_nome_Selectedvalue_get ;
      private String Ddo_funcaodados_complexidade_Activeeventkey ;
      private String Ddo_funcaodados_complexidade_Selectedvalue_get ;
      private String Ddo_funcaodados_der_Activeeventkey ;
      private String Ddo_funcaodados_der_Filteredtext_get ;
      private String Ddo_funcaodados_der_Filteredtextto_get ;
      private String Ddo_funcaodados_rlr_Activeeventkey ;
      private String Ddo_funcaodados_rlr_Filteredtext_get ;
      private String Ddo_funcaodados_rlr_Filteredtextto_get ;
      private String Ddo_funcaodados_pf_Activeeventkey ;
      private String Ddo_funcaodados_pf_Filteredtext_get ;
      private String Ddo_funcaodados_pf_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_53_idx="0001" ;
      private String AV52Pgmname ;
      private String AV26FuncaoDados_Tipo ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_funcaodados_nome_Caption ;
      private String Ddo_funcaodados_nome_Tooltip ;
      private String Ddo_funcaodados_nome_Cls ;
      private String Ddo_funcaodados_nome_Filteredtext_set ;
      private String Ddo_funcaodados_nome_Selectedvalue_set ;
      private String Ddo_funcaodados_nome_Dropdownoptionstype ;
      private String Ddo_funcaodados_nome_Titlecontrolidtoreplace ;
      private String Ddo_funcaodados_nome_Sortedstatus ;
      private String Ddo_funcaodados_nome_Filtertype ;
      private String Ddo_funcaodados_nome_Datalisttype ;
      private String Ddo_funcaodados_nome_Datalistfixedvalues ;
      private String Ddo_funcaodados_nome_Datalistproc ;
      private String Ddo_funcaodados_nome_Sortasc ;
      private String Ddo_funcaodados_nome_Sortdsc ;
      private String Ddo_funcaodados_nome_Loadingdata ;
      private String Ddo_funcaodados_nome_Cleanfilter ;
      private String Ddo_funcaodados_nome_Rangefilterfrom ;
      private String Ddo_funcaodados_nome_Rangefilterto ;
      private String Ddo_funcaodados_nome_Noresultsfound ;
      private String Ddo_funcaodados_nome_Searchbuttontext ;
      private String Ddo_funcaodados_complexidade_Caption ;
      private String Ddo_funcaodados_complexidade_Tooltip ;
      private String Ddo_funcaodados_complexidade_Cls ;
      private String Ddo_funcaodados_complexidade_Selectedvalue_set ;
      private String Ddo_funcaodados_complexidade_Dropdownoptionstype ;
      private String Ddo_funcaodados_complexidade_Titlecontrolidtoreplace ;
      private String Ddo_funcaodados_complexidade_Datalisttype ;
      private String Ddo_funcaodados_complexidade_Datalistfixedvalues ;
      private String Ddo_funcaodados_complexidade_Sortasc ;
      private String Ddo_funcaodados_complexidade_Sortdsc ;
      private String Ddo_funcaodados_complexidade_Loadingdata ;
      private String Ddo_funcaodados_complexidade_Cleanfilter ;
      private String Ddo_funcaodados_complexidade_Rangefilterfrom ;
      private String Ddo_funcaodados_complexidade_Rangefilterto ;
      private String Ddo_funcaodados_complexidade_Noresultsfound ;
      private String Ddo_funcaodados_complexidade_Searchbuttontext ;
      private String Ddo_funcaodados_der_Caption ;
      private String Ddo_funcaodados_der_Tooltip ;
      private String Ddo_funcaodados_der_Cls ;
      private String Ddo_funcaodados_der_Filteredtext_set ;
      private String Ddo_funcaodados_der_Filteredtextto_set ;
      private String Ddo_funcaodados_der_Dropdownoptionstype ;
      private String Ddo_funcaodados_der_Titlecontrolidtoreplace ;
      private String Ddo_funcaodados_der_Filtertype ;
      private String Ddo_funcaodados_der_Datalistfixedvalues ;
      private String Ddo_funcaodados_der_Sortasc ;
      private String Ddo_funcaodados_der_Sortdsc ;
      private String Ddo_funcaodados_der_Loadingdata ;
      private String Ddo_funcaodados_der_Cleanfilter ;
      private String Ddo_funcaodados_der_Rangefilterfrom ;
      private String Ddo_funcaodados_der_Rangefilterto ;
      private String Ddo_funcaodados_der_Noresultsfound ;
      private String Ddo_funcaodados_der_Searchbuttontext ;
      private String Ddo_funcaodados_rlr_Caption ;
      private String Ddo_funcaodados_rlr_Tooltip ;
      private String Ddo_funcaodados_rlr_Cls ;
      private String Ddo_funcaodados_rlr_Filteredtext_set ;
      private String Ddo_funcaodados_rlr_Filteredtextto_set ;
      private String Ddo_funcaodados_rlr_Dropdownoptionstype ;
      private String Ddo_funcaodados_rlr_Titlecontrolidtoreplace ;
      private String Ddo_funcaodados_rlr_Filtertype ;
      private String Ddo_funcaodados_rlr_Datalistfixedvalues ;
      private String Ddo_funcaodados_rlr_Sortasc ;
      private String Ddo_funcaodados_rlr_Sortdsc ;
      private String Ddo_funcaodados_rlr_Loadingdata ;
      private String Ddo_funcaodados_rlr_Cleanfilter ;
      private String Ddo_funcaodados_rlr_Rangefilterfrom ;
      private String Ddo_funcaodados_rlr_Rangefilterto ;
      private String Ddo_funcaodados_rlr_Noresultsfound ;
      private String Ddo_funcaodados_rlr_Searchbuttontext ;
      private String Ddo_funcaodados_pf_Caption ;
      private String Ddo_funcaodados_pf_Tooltip ;
      private String Ddo_funcaodados_pf_Cls ;
      private String Ddo_funcaodados_pf_Filteredtext_set ;
      private String Ddo_funcaodados_pf_Filteredtextto_set ;
      private String Ddo_funcaodados_pf_Dropdownoptionstype ;
      private String Ddo_funcaodados_pf_Titlecontrolidtoreplace ;
      private String Ddo_funcaodados_pf_Filtertype ;
      private String Ddo_funcaodados_pf_Datalistfixedvalues ;
      private String Ddo_funcaodados_pf_Sortasc ;
      private String Ddo_funcaodados_pf_Sortdsc ;
      private String Ddo_funcaodados_pf_Loadingdata ;
      private String Ddo_funcaodados_pf_Cleanfilter ;
      private String Ddo_funcaodados_pf_Rangefilterfrom ;
      private String Ddo_funcaodados_pf_Rangefilterto ;
      private String Ddo_funcaodados_pf_Noresultsfound ;
      private String Ddo_funcaodados_pf_Searchbuttontext ;
      private String GX_FocusControl ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String edtavTffuncaodados_nome_Internalname ;
      private String edtavTffuncaodados_nome_sel_Internalname ;
      private String edtavTffuncaodados_der_Internalname ;
      private String edtavTffuncaodados_der_Jsonclick ;
      private String edtavTffuncaodados_der_to_Internalname ;
      private String edtavTffuncaodados_der_to_Jsonclick ;
      private String edtavTffuncaodados_rlr_Internalname ;
      private String edtavTffuncaodados_rlr_Jsonclick ;
      private String edtavTffuncaodados_rlr_to_Internalname ;
      private String edtavTffuncaodados_rlr_to_Jsonclick ;
      private String edtavTffuncaodados_pf_Internalname ;
      private String edtavTffuncaodados_pf_Jsonclick ;
      private String edtavTffuncaodados_pf_to_Internalname ;
      private String edtavTffuncaodados_pf_to_Jsonclick ;
      private String edtavDdo_funcaodados_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaodados_dertitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaodados_pftitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavOrderedby_Internalname ;
      private String edtFuncaoDados_Codigo_Internalname ;
      private String edtFuncaoDados_SistemaCod_Internalname ;
      private String edtFuncaoDados_FuncaoDadosCod_Internalname ;
      private String edtFuncaoDados_FuncaoDadosSisCod_Internalname ;
      private String edtFuncaoDados_Nome_Internalname ;
      private String cmbFuncaoDados_Complexidade_Internalname ;
      private String A376FuncaoDados_Complexidade ;
      private String edtFuncaoDados_DER_Internalname ;
      private String edtFuncaoDados_RLR_Internalname ;
      private String edtFuncaoDados_PF_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String A373FuncaoDados_Tipo ;
      private String GXt_char1 ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavFuncaodados_tipo_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavFuncaodados_nome1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavFuncaodados_nome2_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_funcaodados_nome_Internalname ;
      private String Ddo_funcaodados_complexidade_Internalname ;
      private String Ddo_funcaodados_der_Internalname ;
      private String Ddo_funcaodados_rlr_Internalname ;
      private String Ddo_funcaodados_pf_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtFuncaoDados_Nome_Title ;
      private String edtFuncaoDados_DER_Title ;
      private String edtFuncaoDados_RLR_Title ;
      private String edtFuncaoDados_PF_Title ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextfuncaodados_tipo_Internalname ;
      private String lblFiltertextfuncaodados_tipo_Jsonclick ;
      private String cmbavFuncaodados_tipo_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String sCtrlAV7FuncaoDados_SistemaCod ;
      private String sGXsfl_53_fel_idx="0001" ;
      private String ROClassString ;
      private String edtFuncaoDados_Codigo_Jsonclick ;
      private String edtFuncaoDados_SistemaCod_Jsonclick ;
      private String edtFuncaoDados_FuncaoDadosCod_Jsonclick ;
      private String edtFuncaoDados_FuncaoDadosSisCod_Jsonclick ;
      private String edtFuncaoDados_Nome_Jsonclick ;
      private String cmbFuncaoDados_Complexidade_Jsonclick ;
      private String edtFuncaoDados_DER_Jsonclick ;
      private String edtFuncaoDados_RLR_Jsonclick ;
      private String edtFuncaoDados_PF_Jsonclick ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV24DynamicFiltersIgnoreFirst ;
      private bool AV23DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool A755FuncaoDados_Contar ;
      private bool Ddo_funcaodados_nome_Includesortasc ;
      private bool Ddo_funcaodados_nome_Includesortdsc ;
      private bool Ddo_funcaodados_nome_Includefilter ;
      private bool Ddo_funcaodados_nome_Filterisrange ;
      private bool Ddo_funcaodados_nome_Includedatalist ;
      private bool Ddo_funcaodados_complexidade_Includesortasc ;
      private bool Ddo_funcaodados_complexidade_Includesortdsc ;
      private bool Ddo_funcaodados_complexidade_Includefilter ;
      private bool Ddo_funcaodados_complexidade_Filterisrange ;
      private bool Ddo_funcaodados_complexidade_Includedatalist ;
      private bool Ddo_funcaodados_complexidade_Allowmultipleselection ;
      private bool Ddo_funcaodados_der_Includesortasc ;
      private bool Ddo_funcaodados_der_Includesortdsc ;
      private bool Ddo_funcaodados_der_Includefilter ;
      private bool Ddo_funcaodados_der_Filterisrange ;
      private bool Ddo_funcaodados_der_Includedatalist ;
      private bool Ddo_funcaodados_rlr_Includesortasc ;
      private bool Ddo_funcaodados_rlr_Includesortdsc ;
      private bool Ddo_funcaodados_rlr_Includefilter ;
      private bool Ddo_funcaodados_rlr_Filterisrange ;
      private bool Ddo_funcaodados_rlr_Includedatalist ;
      private bool Ddo_funcaodados_pf_Includesortasc ;
      private bool Ddo_funcaodados_pf_Includesortdsc ;
      private bool Ddo_funcaodados_pf_Includefilter ;
      private bool Ddo_funcaodados_pf_Filterisrange ;
      private bool Ddo_funcaodados_pf_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n391FuncaoDados_FuncaoDadosCod ;
      private bool n404FuncaoDados_FuncaoDadosSisCod ;
      private bool n755FuncaoDados_Contar ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String AV34TFFuncaoDados_Complexidade_SelsJson ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV17FuncaoDados_Nome1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV21FuncaoDados_Nome2 ;
      private String AV30TFFuncaoDados_Nome ;
      private String AV31TFFuncaoDados_Nome_Sel ;
      private String AV32ddo_FuncaoDados_NomeTitleControlIdToReplace ;
      private String AV36ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace ;
      private String AV40ddo_FuncaoDados_DERTitleControlIdToReplace ;
      private String AV44ddo_FuncaoDados_RLRTitleControlIdToReplace ;
      private String AV48ddo_FuncaoDados_PFTitleControlIdToReplace ;
      private String A369FuncaoDados_Nome ;
      private String lV17FuncaoDados_Nome1 ;
      private String lV21FuncaoDados_Nome2 ;
      private String lV30TFFuncaoDados_Nome ;
      private IGxSession AV25Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavFuncaodados_tipo ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbFuncaoDados_Complexidade ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private IDataStoreProvider pr_default ;
      private String[] H00DI2_A373FuncaoDados_Tipo ;
      private String[] H00DI2_A369FuncaoDados_Nome ;
      private int[] H00DI2_A404FuncaoDados_FuncaoDadosSisCod ;
      private bool[] H00DI2_n404FuncaoDados_FuncaoDadosSisCod ;
      private int[] H00DI2_A391FuncaoDados_FuncaoDadosCod ;
      private bool[] H00DI2_n391FuncaoDados_FuncaoDadosCod ;
      private int[] H00DI2_A370FuncaoDados_SistemaCod ;
      private bool[] H00DI2_A755FuncaoDados_Contar ;
      private bool[] H00DI2_n755FuncaoDados_Contar ;
      private int[] H00DI2_A368FuncaoDados_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV35TFFuncaoDados_Complexidade_Sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV29FuncaoDados_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV33FuncaoDados_ComplexidadeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV37FuncaoDados_DERTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV41FuncaoDados_RLRTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV45FuncaoDados_PFTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV49DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons3 ;
   }

   public class sistemafronteira__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00DI2( IGxContext context ,
                                             String A376FuncaoDados_Complexidade ,
                                             IGxCollection AV35TFFuncaoDados_Complexidade_Sels ,
                                             String AV16DynamicFiltersSelector1 ,
                                             String AV17FuncaoDados_Nome1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             String AV21FuncaoDados_Nome2 ,
                                             String AV31TFFuncaoDados_Nome_Sel ,
                                             String AV30TFFuncaoDados_Nome ,
                                             String A369FuncaoDados_Nome ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A370FuncaoDados_SistemaCod ,
                                             int AV7FuncaoDados_SistemaCod ,
                                             String A373FuncaoDados_Tipo ,
                                             int AV35TFFuncaoDados_Complexidade_Sels_Count ,
                                             short AV38TFFuncaoDados_DER ,
                                             short A374FuncaoDados_DER ,
                                             short AV39TFFuncaoDados_DER_To ,
                                             short AV42TFFuncaoDados_RLR ,
                                             short A375FuncaoDados_RLR ,
                                             short AV43TFFuncaoDados_RLR_To ,
                                             decimal AV46TFFuncaoDados_PF ,
                                             decimal A377FuncaoDados_PF ,
                                             decimal AV47TFFuncaoDados_PF_To )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [6] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT T1.[FuncaoDados_Tipo], T1.[FuncaoDados_Nome], T2.[FuncaoDados_SistemaCod] AS FuncaoDados_FuncaoDadosSisCod, T1.[FuncaoDados_FuncaoDadosCod] AS FuncaoDados_FuncaoDadosCod, T1.[FuncaoDados_SistemaCod], T1.[FuncaoDados_Contar], T1.[FuncaoDados_Codigo] FROM ([FuncaoDados] T1 WITH (NOLOCK) LEFT JOIN [FuncaoDados] T2 WITH (NOLOCK) ON T2.[FuncaoDados_Codigo] = T1.[FuncaoDados_FuncaoDadosCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[FuncaoDados_SistemaCod] = @AV7FuncaoDados_SistemaCod)";
         scmdbuf = scmdbuf + " and (T1.[FuncaoDados_Tipo] = 'ALI')";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAODADOS_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17FuncaoDados_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] like '%' + @lV17FuncaoDados_Nome1)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "FUNCAODADOS_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21FuncaoDados_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] like '%' + @lV21FuncaoDados_Nome2)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV31TFFuncaoDados_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30TFFuncaoDados_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] like @lV30TFFuncaoDados_Nome)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31TFFuncaoDados_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] = @AV31TFFuncaoDados_Nome_Sel)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV14OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoDados_Codigo]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoDados_SistemaCod], T1.[FuncaoDados_Nome]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoDados_SistemaCod] DESC, T1.[FuncaoDados_Nome] DESC";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00DI2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (short)dynConstraints[10] , (bool)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (short)dynConstraints[16] , (short)dynConstraints[17] , (short)dynConstraints[18] , (short)dynConstraints[19] , (short)dynConstraints[20] , (short)dynConstraints[21] , (decimal)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00DI2 ;
          prmH00DI2 = new Object[] {
          new Object[] {"@AV7FuncaoDados_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35TFFuCount",SqlDbType.Int,9,0} ,
          new Object[] {"@lV17FuncaoDados_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV21FuncaoDados_Nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV30TFFuncaoDados_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV31TFFuncaoDados_Nome_Sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00DI2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DI2,11,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.getBool(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                return;
       }
    }

 }

}
