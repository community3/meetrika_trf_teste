/*
               File: type_SdtSDT_Baselines_Funcao
        Description: SDT_Baselines
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:59.70
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_Baselines.Funcao" )]
   [XmlType(TypeName =  "SDT_Baselines.Funcao" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtSDT_Baselines_Funcao : GxUserType
   {
      public SdtSDT_Baselines_Funcao( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_Baselines_Funcao_Tipo = "";
         gxTv_SdtSDT_Baselines_Funcao_Nome = "";
         gxTv_SdtSDT_Baselines_Funcao_Observacao = "";
      }

      public SdtSDT_Baselines_Funcao( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_Baselines_Funcao deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_Baselines_Funcao)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_Baselines_Funcao obj ;
         obj = this;
         obj.gxTpr_Codigo = deserialized.gxTpr_Codigo;
         obj.gxTpr_Tipo = deserialized.gxTpr_Tipo;
         obj.gxTpr_Nome = deserialized.gxTpr_Nome;
         obj.gxTpr_Der = deserialized.gxTpr_Der;
         obj.gxTpr_Rlr = deserialized.gxTpr_Rlr;
         obj.gxTpr_Observacao = deserialized.gxTpr_Observacao;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Codigo") )
               {
                  gxTv_SdtSDT_Baselines_Funcao_Codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tipo") )
               {
                  gxTv_SdtSDT_Baselines_Funcao_Tipo = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Nome") )
               {
                  gxTv_SdtSDT_Baselines_Funcao_Nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "DER") )
               {
                  gxTv_SdtSDT_Baselines_Funcao_Der = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "RLR") )
               {
                  gxTv_SdtSDT_Baselines_Funcao_Rlr = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Observacao") )
               {
                  gxTv_SdtSDT_Baselines_Funcao_Observacao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_Baselines.Funcao";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Baselines_Funcao_Codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Tipo", StringUtil.RTrim( gxTv_SdtSDT_Baselines_Funcao_Tipo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Nome", StringUtil.RTrim( gxTv_SdtSDT_Baselines_Funcao_Nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("DER", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Baselines_Funcao_Der), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("RLR", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Baselines_Funcao_Rlr), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Observacao", StringUtil.RTrim( gxTv_SdtSDT_Baselines_Funcao_Observacao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Codigo", gxTv_SdtSDT_Baselines_Funcao_Codigo, false);
         AddObjectProperty("Tipo", gxTv_SdtSDT_Baselines_Funcao_Tipo, false);
         AddObjectProperty("Nome", gxTv_SdtSDT_Baselines_Funcao_Nome, false);
         AddObjectProperty("DER", gxTv_SdtSDT_Baselines_Funcao_Der, false);
         AddObjectProperty("RLR", gxTv_SdtSDT_Baselines_Funcao_Rlr, false);
         AddObjectProperty("Observacao", gxTv_SdtSDT_Baselines_Funcao_Observacao, false);
         return  ;
      }

      [  SoapElement( ElementName = "Codigo" )]
      [  XmlElement( ElementName = "Codigo"   )]
      public int gxTpr_Codigo
      {
         get {
            return gxTv_SdtSDT_Baselines_Funcao_Codigo ;
         }

         set {
            gxTv_SdtSDT_Baselines_Funcao_Codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Tipo" )]
      [  XmlElement( ElementName = "Tipo"   )]
      public String gxTpr_Tipo
      {
         get {
            return gxTv_SdtSDT_Baselines_Funcao_Tipo ;
         }

         set {
            gxTv_SdtSDT_Baselines_Funcao_Tipo = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Nome" )]
      [  XmlElement( ElementName = "Nome"   )]
      public String gxTpr_Nome
      {
         get {
            return gxTv_SdtSDT_Baselines_Funcao_Nome ;
         }

         set {
            gxTv_SdtSDT_Baselines_Funcao_Nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "DER" )]
      [  XmlElement( ElementName = "DER"   )]
      public short gxTpr_Der
      {
         get {
            return gxTv_SdtSDT_Baselines_Funcao_Der ;
         }

         set {
            gxTv_SdtSDT_Baselines_Funcao_Der = (short)(value);
         }

      }

      [  SoapElement( ElementName = "RLR" )]
      [  XmlElement( ElementName = "RLR"   )]
      public short gxTpr_Rlr
      {
         get {
            return gxTv_SdtSDT_Baselines_Funcao_Rlr ;
         }

         set {
            gxTv_SdtSDT_Baselines_Funcao_Rlr = (short)(value);
         }

      }

      [  SoapElement( ElementName = "Observacao" )]
      [  XmlElement( ElementName = "Observacao"   )]
      public String gxTpr_Observacao
      {
         get {
            return gxTv_SdtSDT_Baselines_Funcao_Observacao ;
         }

         set {
            gxTv_SdtSDT_Baselines_Funcao_Observacao = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_Baselines_Funcao_Tipo = "";
         gxTv_SdtSDT_Baselines_Funcao_Nome = "";
         gxTv_SdtSDT_Baselines_Funcao_Observacao = "";
         sTagName = "";
         return  ;
      }

      protected short gxTv_SdtSDT_Baselines_Funcao_Der ;
      protected short gxTv_SdtSDT_Baselines_Funcao_Rlr ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_Baselines_Funcao_Codigo ;
      protected String gxTv_SdtSDT_Baselines_Funcao_Tipo ;
      protected String gxTv_SdtSDT_Baselines_Funcao_Nome ;
      protected String sTagName ;
      protected String gxTv_SdtSDT_Baselines_Funcao_Observacao ;
   }

   [DataContract(Name = @"SDT_Baselines.Funcao", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtSDT_Baselines_Funcao_RESTInterface : GxGenericCollectionItem<SdtSDT_Baselines_Funcao>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_Baselines_Funcao_RESTInterface( ) : base()
      {
      }

      public SdtSDT_Baselines_Funcao_RESTInterface( SdtSDT_Baselines_Funcao psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Codigo" , Order = 0 )]
      public Nullable<int> gxTpr_Codigo
      {
         get {
            return sdt.gxTpr_Codigo ;
         }

         set {
            sdt.gxTpr_Codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Tipo" , Order = 1 )]
      public String gxTpr_Tipo
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Tipo) ;
         }

         set {
            sdt.gxTpr_Tipo = (String)(value);
         }

      }

      [DataMember( Name = "Nome" , Order = 2 )]
      public String gxTpr_Nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Nome) ;
         }

         set {
            sdt.gxTpr_Nome = (String)(value);
         }

      }

      [DataMember( Name = "DER" , Order = 3 )]
      public Nullable<short> gxTpr_Der
      {
         get {
            return sdt.gxTpr_Der ;
         }

         set {
            sdt.gxTpr_Der = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "RLR" , Order = 4 )]
      public Nullable<short> gxTpr_Rlr
      {
         get {
            return sdt.gxTpr_Rlr ;
         }

         set {
            sdt.gxTpr_Rlr = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Observacao" , Order = 5 )]
      public String gxTpr_Observacao
      {
         get {
            return sdt.gxTpr_Observacao ;
         }

         set {
            sdt.gxTpr_Observacao = (String)(value);
         }

      }

      public SdtSDT_Baselines_Funcao sdt
      {
         get {
            return (SdtSDT_Baselines_Funcao)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_Baselines_Funcao() ;
         }
      }

   }

}
