/*
               File: type_SdtGAMApplicationPermissionFilter
        Description: GAMApplicationPermissionFilter
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 1:32:6.73
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMApplicationPermissionFilter : GxUserType, IGxExternalObject
   {
      public SdtGAMApplicationPermissionFilter( )
      {
         initialize();
      }

      public SdtGAMApplicationPermissionFilter( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMApplicationPermissionFilter_externalReference == null )
         {
            GAMApplicationPermissionFilter_externalReference = new Artech.Security.GAMApplicationPermissionFilter(context);
         }
         returntostring = "";
         returntostring = (String)(GAMApplicationPermissionFilter_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Guid
      {
         get {
            if ( GAMApplicationPermissionFilter_externalReference == null )
            {
               GAMApplicationPermissionFilter_externalReference = new Artech.Security.GAMApplicationPermissionFilter(context);
            }
            return GAMApplicationPermissionFilter_externalReference.GUID ;
         }

         set {
            if ( GAMApplicationPermissionFilter_externalReference == null )
            {
               GAMApplicationPermissionFilter_externalReference = new Artech.Security.GAMApplicationPermissionFilter(context);
            }
            GAMApplicationPermissionFilter_externalReference.GUID = value;
         }

      }

      public String gxTpr_Name
      {
         get {
            if ( GAMApplicationPermissionFilter_externalReference == null )
            {
               GAMApplicationPermissionFilter_externalReference = new Artech.Security.GAMApplicationPermissionFilter(context);
            }
            return GAMApplicationPermissionFilter_externalReference.Name ;
         }

         set {
            if ( GAMApplicationPermissionFilter_externalReference == null )
            {
               GAMApplicationPermissionFilter_externalReference = new Artech.Security.GAMApplicationPermissionFilter(context);
            }
            GAMApplicationPermissionFilter_externalReference.Name = value;
         }

      }

      public String gxTpr_Name_expression
      {
         get {
            if ( GAMApplicationPermissionFilter_externalReference == null )
            {
               GAMApplicationPermissionFilter_externalReference = new Artech.Security.GAMApplicationPermissionFilter(context);
            }
            return GAMApplicationPermissionFilter_externalReference.Name_Expression ;
         }

         set {
            if ( GAMApplicationPermissionFilter_externalReference == null )
            {
               GAMApplicationPermissionFilter_externalReference = new Artech.Security.GAMApplicationPermissionFilter(context);
            }
            GAMApplicationPermissionFilter_externalReference.Name_Expression = value;
         }

      }

      public String gxTpr_Descripcion
      {
         get {
            if ( GAMApplicationPermissionFilter_externalReference == null )
            {
               GAMApplicationPermissionFilter_externalReference = new Artech.Security.GAMApplicationPermissionFilter(context);
            }
            return GAMApplicationPermissionFilter_externalReference.Descripcion ;
         }

         set {
            if ( GAMApplicationPermissionFilter_externalReference == null )
            {
               GAMApplicationPermissionFilter_externalReference = new Artech.Security.GAMApplicationPermissionFilter(context);
            }
            GAMApplicationPermissionFilter_externalReference.Descripcion = value;
         }

      }

      public String gxTpr_Program
      {
         get {
            if ( GAMApplicationPermissionFilter_externalReference == null )
            {
               GAMApplicationPermissionFilter_externalReference = new Artech.Security.GAMApplicationPermissionFilter(context);
            }
            return GAMApplicationPermissionFilter_externalReference.Program ;
         }

         set {
            if ( GAMApplicationPermissionFilter_externalReference == null )
            {
               GAMApplicationPermissionFilter_externalReference = new Artech.Security.GAMApplicationPermissionFilter(context);
            }
            GAMApplicationPermissionFilter_externalReference.Program = value;
         }

      }

      public String gxTpr_Accesstypedefault
      {
         get {
            if ( GAMApplicationPermissionFilter_externalReference == null )
            {
               GAMApplicationPermissionFilter_externalReference = new Artech.Security.GAMApplicationPermissionFilter(context);
            }
            return GAMApplicationPermissionFilter_externalReference.AccessTypeDefault ;
         }

         set {
            if ( GAMApplicationPermissionFilter_externalReference == null )
            {
               GAMApplicationPermissionFilter_externalReference = new Artech.Security.GAMApplicationPermissionFilter(context);
            }
            GAMApplicationPermissionFilter_externalReference.AccessTypeDefault = value;
         }

      }

      public String gxTpr_Typefilter
      {
         get {
            if ( GAMApplicationPermissionFilter_externalReference == null )
            {
               GAMApplicationPermissionFilter_externalReference = new Artech.Security.GAMApplicationPermissionFilter(context);
            }
            return GAMApplicationPermissionFilter_externalReference.TypeFilter ;
         }

         set {
            if ( GAMApplicationPermissionFilter_externalReference == null )
            {
               GAMApplicationPermissionFilter_externalReference = new Artech.Security.GAMApplicationPermissionFilter(context);
            }
            GAMApplicationPermissionFilter_externalReference.TypeFilter = value;
         }

      }

      public String gxTpr_Isautomaticpermission
      {
         get {
            if ( GAMApplicationPermissionFilter_externalReference == null )
            {
               GAMApplicationPermissionFilter_externalReference = new Artech.Security.GAMApplicationPermissionFilter(context);
            }
            return GAMApplicationPermissionFilter_externalReference.IsAutomaticPermission ;
         }

         set {
            if ( GAMApplicationPermissionFilter_externalReference == null )
            {
               GAMApplicationPermissionFilter_externalReference = new Artech.Security.GAMApplicationPermissionFilter(context);
            }
            GAMApplicationPermissionFilter_externalReference.IsAutomaticPermission = value;
         }

      }

      public bool gxTpr_Loadproperties
      {
         get {
            if ( GAMApplicationPermissionFilter_externalReference == null )
            {
               GAMApplicationPermissionFilter_externalReference = new Artech.Security.GAMApplicationPermissionFilter(context);
            }
            return GAMApplicationPermissionFilter_externalReference.LoadProperties ;
         }

         set {
            if ( GAMApplicationPermissionFilter_externalReference == null )
            {
               GAMApplicationPermissionFilter_externalReference = new Artech.Security.GAMApplicationPermissionFilter(context);
            }
            GAMApplicationPermissionFilter_externalReference.LoadProperties = value;
         }

      }

      public int gxTpr_Start
      {
         get {
            if ( GAMApplicationPermissionFilter_externalReference == null )
            {
               GAMApplicationPermissionFilter_externalReference = new Artech.Security.GAMApplicationPermissionFilter(context);
            }
            return GAMApplicationPermissionFilter_externalReference.Start ;
         }

         set {
            if ( GAMApplicationPermissionFilter_externalReference == null )
            {
               GAMApplicationPermissionFilter_externalReference = new Artech.Security.GAMApplicationPermissionFilter(context);
            }
            GAMApplicationPermissionFilter_externalReference.Start = value;
         }

      }

      public int gxTpr_Limit
      {
         get {
            if ( GAMApplicationPermissionFilter_externalReference == null )
            {
               GAMApplicationPermissionFilter_externalReference = new Artech.Security.GAMApplicationPermissionFilter(context);
            }
            return GAMApplicationPermissionFilter_externalReference.Limit ;
         }

         set {
            if ( GAMApplicationPermissionFilter_externalReference == null )
            {
               GAMApplicationPermissionFilter_externalReference = new Artech.Security.GAMApplicationPermissionFilter(context);
            }
            GAMApplicationPermissionFilter_externalReference.Limit = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMApplicationPermissionFilter_externalReference == null )
            {
               GAMApplicationPermissionFilter_externalReference = new Artech.Security.GAMApplicationPermissionFilter(context);
            }
            return GAMApplicationPermissionFilter_externalReference ;
         }

         set {
            GAMApplicationPermissionFilter_externalReference = (Artech.Security.GAMApplicationPermissionFilter)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMApplicationPermissionFilter GAMApplicationPermissionFilter_externalReference=null ;
   }

}
