/*
               File: QueryViewerChartType
        Description: QueryViewerChartType
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 21:56:35.11
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gxdomainqueryviewercharttype
   {
      private static Hashtable domain = new Hashtable();
      static gxdomainqueryviewercharttype ()
      {
         domain["Column"] = "Column";
         domain["Column3D"] = "Column3D";
         domain["StackedColumn"] = "StackedColumn";
         domain["StackedColumn3D"] = "StackedColumn3D";
         domain["Bar"] = "Bar";
         domain["StackedBar"] = "StackedBar";
         domain["Area"] = "Area";
         domain["StackedArea"] = "StackedArea";
         domain["Line"] = "Line";
         domain["Pie"] = "Pie";
         domain["Pie3D"] = "Pie3D";
         domain["Doughnut"] = "Doughnut";
         domain["Funnel"] = "Funnel";
         domain["ColumnLine"] = "Column Line";
         domain["Column3DLine"] = "Column 3D Line";
         domain["Timeline"] = "Timeline";
      }

      public static string getDescription( IGxContext context ,
                                           String key )
      {
         string rtkey ;
         rtkey = StringUtil.Trim( (String)(key));
         return (string)domain[rtkey] ;
      }

      public static GxSimpleCollection getValues( )
      {
         GxSimpleCollection value = new GxSimpleCollection();
         ArrayList aKeys = new ArrayList(domain.Keys);
         aKeys.Sort();
         foreach (String key in aKeys)
         {
            value.Add(key);
         }
         return value;
      }

   }

}
